package com.mathfriendzy.mypaypal;

/**
 * Created by root on 15/9/15.
 */
public interface MyPayPalCallback {
    void onPaymentSuccess(String successString);
    void onFailureOccurred(String failureString);
    void onResultCancel(String resultCancelString);
    void onPaymentExtrasInvalid(String invalidString);
}
