package com.mathfriendzy.mypaypal;

import java.math.BigDecimal;
import org.json.JSONException;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

public class MyPayPalPayment {
    private static PayPalConfiguration config = null;
    /*private static final String CLIENT_ID =
            "AYeaOxh7R242EGdGC3QqJQDXUpqQWAOClJPVW_BFP68qo87rcCFsOAn0AFxZ5ejIdCHJEA0dfu5Z4pSG";*/

    //Alex account id
    /*private static final String CLIENT_ID =
            "Adp82AR8kUIzftL39v2jYyZ0uLcOwnivOVSCwIhaMTiWA9qievEYduNptIBBQfGG7ADRMk3WTb0leOdw";*/

    //new , for sandbox
    /*private static final String CLIENT_ID =
            "ARzDL5JngP-4kJlnxI-7FrG1rVZoXyIixQu1EXdbsjRuMcQaOMLTqe1E8PmEodjdZwdakcB5xtZOgrfA";*/
    //for live
    private static final String CLIENT_ID =
            "AZqQCaZDqxqwSSY-UlEqqUxjLFdibgwJnj-7hZFPMjbwnkr7JiQSlYncCcjTziG4tQ_b4rlkZusmlLRS";

    private Context context = null;
    public static String USD_CURRENCY_TYPE = "USD";
    public static final int PAYPAL_OPEN_PAYMENT_SCREEN_REQUEST = 9999999;
    private MyPayPalCallback callback = null;

    /*public PayPalConfiguration getPayPalConfig(){
        return config;
    }*/

    public MyPayPalPayment(Context context , MyPayPalCallback callback){
        this.context = context;
        this.callback = callback;
        if(config == null){
            config = new PayPalConfiguration()
                    // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
                    // or live (ENVIRONMENT_PRODUCTION)
                    //.environment(PayPalConfiguration.ENVIRONMENT_NO_NETWORK)
                    //.environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
                    .environment(PayPalConfiguration.ENVIRONMENT_PRODUCTION)
                    .clientId(CLIENT_ID);
        }
    }

    public void startService(){
        Intent intent = new Intent(this.context, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        context.startService(intent);
    }

    public void stopService(){
        context.stopService(new Intent(context, PayPalService.class));
    }

    public void doPayment(String price , String currencyType , String item){
        // PAYMENT_INTENT_SALE will cause the payment to complete immediately.
        // Change PAYMENT_INTENT_SALE to
        //   - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
        //   - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
        //     later via calls from your server.
        PayPalPayment payment = new PayPalPayment(new BigDecimal(price), currencyType, item,
                PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(context, PaymentActivity.class);
        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);
        ((Activity)context).startActivityForResult(intent, PAYPAL_OPEN_PAYMENT_SCREEN_REQUEST);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (resultCode == Activity.RESULT_OK) {
            if(requestCode == PAYPAL_OPEN_PAYMENT_SCREEN_REQUEST){
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        // TODO: send 'confirm' to your server for verification.
                        // see https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                        // for more details.
                        callback.onPaymentSuccess(confirm.toJSONObject().toString(4));
                    } catch (JSONException e) {
                        callback.onFailureOccurred("an extremely unlikely failure occurred: " + e);
                    }
                }
            }
            else if (resultCode == Activity.RESULT_CANCELED) {
                callback.onResultCancel("The user canceled.");
            }
            else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                callback.onPaymentExtrasInvalid("An invalid Payment or" +
                        " PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }
}