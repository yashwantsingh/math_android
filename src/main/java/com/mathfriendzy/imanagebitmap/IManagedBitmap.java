package com.mathfriendzy.imanagebitmap;

import android.graphics.Bitmap;

/**
 * Created by root on 26/8/15.
 */
public interface IManagedBitmap {

    /**
     * Get the underlying {@link Bitmap} object.
     * NEVER call Bitmap.recycle() on this object.
     */
    Bitmap getBitmap();

    /**
     * Decrease the reference counter and recycle the underlying Bitmap
     * if there are no more references.
     */
    void recycle();

    /**
     * Increase the reference counter.
     * @return self
     */
    IManagedBitmap retain();
}
