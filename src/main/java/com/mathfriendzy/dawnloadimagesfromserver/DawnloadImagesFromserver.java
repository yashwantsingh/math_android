package com.mathfriendzy.dawnloadimagesfromserver;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;

import com.mathfriendzy.model.schoolcurriculum.learnigncenter.LearnignCenterSchoolCurriculumServerOperation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SchoolCurriculumLearnignCenterimpl;

public class DawnloadImagesFromserver implements Runnable{

	private ArrayList<String> imageNamelist;
	SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl;
	LearnignCenterSchoolCurriculumServerOperation learningCenterServerobj;
	private byte [] imageFromServer;
	
	public DawnloadImagesFromserver(ArrayList<String> imageNamelist , Context context){
		this.imageNamelist = imageNamelist;
				
		schooloCurriculumImpl 	= new SchoolCurriculumLearnignCenterimpl(context);
		learningCenterServerobj = new LearnignCenterSchoolCurriculumServerOperation();
				
		/*Intent intent = new Intent(context , DownloadImagesService.class);
		intent.putExtra("imageList", imageNamelist);
		context.startService(intent);*/
		//context.startService(new Intent(context,DownloadImagesService.class));
	}
		
	@Override
	public void run() {
		try{
			for(int i = 0 ; i < imageNamelist.size() ; i ++ ){
				schooloCurriculumImpl.openConnection();
				if(!schooloCurriculumImpl.isImageAlreadyDawnloaded(imageNamelist.get(i))){
						imageFromServer = learningCenterServerobj.dawabloadImageFromServer(imageNamelist.get(i));
						schooloCurriculumImpl.insertIntoSchoolCurriculumImageTable(imageNamelist.get(i), imageFromServer);
				}
				schooloCurriculumImpl.closeConnection();
			}
		}catch(Exception e){
			Log.e("DawnloadImagesFromserver", "Error while loading images for word problem " 
					+  e.toString());
		}finally{
			schooloCurriculumImpl.closeConnection();
		}
	}
}
