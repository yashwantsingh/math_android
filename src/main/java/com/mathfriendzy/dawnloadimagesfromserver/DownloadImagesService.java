package com.mathfriendzy.dawnloadimagesfromserver;

import java.util.ArrayList;
import java.util.List;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.mathfriendzy.model.schoolcurriculum.learnigncenter.LearnignCenterSchoolCurriculumServerOperation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SchoolCurriculumLearnignCenterimpl;

public class DownloadImagesService extends Service{

	private ArrayList<String> imageNamelist = null;
	SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl;
	LearnignCenterSchoolCurriculumServerOperation learningCenterServerobj;
	private final int MAX_THREAD = 5;
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		try{
			schooloCurriculumImpl 	= new SchoolCurriculumLearnignCenterimpl(this);
			learningCenterServerobj = 	new LearnignCenterSchoolCurriculumServerOperation();
			imageNamelist = intent.getStringArrayListExtra("imageList");
			DownloadImageTread.counter = 0;
			//Log.e("TAG", "list size " + imageNamelist.size());
			schooloCurriculumImpl.openConnection();
						
			/*for(int i = 0 ; i < imageNamelist.size() ; i ++ ){
				if(CommonUtils.isInternetConnectionAvailable(this)){
					DownloadImageTread thread = new DownloadImageTread
							(imageNamelist.get(i), 
									schooloCurriculumImpl, learningCenterServerobj);
					Thread thread1 = new Thread(thread);
					thread1.start();
				}
			}*/
			
			ArrayList<List<String>> subList = getSubList();
			for(int i = 0 ; i < subList.size() ; i ++ ){
				DownloadImageTread thread = new DownloadImageTread
						(subList.get(i), 
								schooloCurriculumImpl, learningCenterServerobj);
				Thread thread1 = new Thread(thread);
				thread1.start();
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{

		}
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onDestroy() {
		try{
			if(schooloCurriculumImpl != null){
				schooloCurriculumImpl.closeConnection();
			}
		}catch(Exception e){

		}
		super.onDestroy();
	}


	/**
	 * Devide the main image list into sublist, based on how many thread you want to create
	 * @return
	 */
	private ArrayList<List<String>> getSubList(){
		int a = 0;
		int b = imageNamelist.size() / MAX_THREAD;
		int c = b;
		ArrayList<List<String>> subList = new ArrayList<List<String>>();
		for(int i = 0 ; i < MAX_THREAD ; i ++){
			subList.add((List<String>) imageNamelist.subList(a, b));
			a = b;
			b = b + c;
		}
		return subList;
	}
}
