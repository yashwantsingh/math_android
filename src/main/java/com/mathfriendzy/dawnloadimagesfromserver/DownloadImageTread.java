package com.mathfriendzy.dawnloadimagesfromserver;

import java.util.List;

import com.mathfriendzy.model.schoolcurriculum.learnigncenter.LearnignCenterSchoolCurriculumServerOperation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SchoolCurriculumLearnignCenterimpl;

public class DownloadImageTread implements Runnable{

	public static int counter = 0;
	private byte [] imageFromServer;
	private String imageUrl = null;
	private SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl = null;
	private LearnignCenterSchoolCurriculumServerOperation learningCenterServerobj;
	List<String> imageNamelist = null;
	/*public DownloadImageTread(String imageUrl , 
			SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl
			, LearnignCenterSchoolCurriculumServerOperation learningCenterServerobj){
		this.imageUrl = imageUrl;
		this.schooloCurriculumImpl = schooloCurriculumImpl;
		this.learningCenterServerobj = learningCenterServerobj;
	}*/

	public DownloadImageTread(List<String> imageNamelist,
			SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl
			, LearnignCenterSchoolCurriculumServerOperation learningCenterServerobj){
		this.imageNamelist = imageNamelist;
		//this.imageUrl = imageUrl;
		this.schooloCurriculumImpl = schooloCurriculumImpl;
		this.learningCenterServerobj = learningCenterServerobj;
	}

	/*@Override
	public void run() {
		try{
			//schooloCurriculumImpl.openConnection();
			if(!schooloCurriculumImpl.isImageAlreadyDawnloaded(imageUrl)){
				imageFromServer = learningCenterServerobj.dawabloadImageFromServer(imageUrl);
				schooloCurriculumImpl.insertIntoSchoolCurriculumImageTable(imageUrl, imageFromServer);
			}
			//schooloCurriculumImpl.closeConnection();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//schooloCurriculumImpl.closeConnection();
		}
	}*/

	@Override
	public void run() {
		try{
			//schooloCurriculumImpl.openConnection();
			for(int i = 0 ; i < imageNamelist.size() ; i ++ ){
				if(!schooloCurriculumImpl.isImageAlreadyDawnloaded(imageUrl)){
					imageFromServer = learningCenterServerobj.dawabloadImageFromServer(imageNamelist.get(i));
					schooloCurriculumImpl.insertIntoSchoolCurriculumImageTable(imageUrl, imageFromServer);
				}
			}
			//schooloCurriculumImpl.closeConnection();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//schooloCurriculumImpl.closeConnection();
		}
	}
}
