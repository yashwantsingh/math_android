package com.mathfriendzy.dawnloadimagesfromserver;

import android.graphics.Bitmap;
import java.io.File;

/**
 * Created by root on 8/6/15.
 */
public interface DownloadImageCallback {
    void onDownloadComplete(File file , Bitmap bitmap);
}
