package com.mathfriendzy.timer;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by root on 15/4/15.
 */
public class MyTimer {

    private static MyTimer myTimer = null;

    private Timer timer;
    private MyTimerTask myTimerTask;
    private int progress = 0;
    private TimerProgressCallback callback;
    //private final long DELAYTIME  = 1000;//for 1 seconds
    private final long DELAYTIME  = 0;//for 1 seconds
    private final long PERIODTIME = 1000;//for 1 seconds
    private int lastUpdatedCount = 0;
    private boolean isTimerRunning = false;

    public static MyTimer getInstance(TimerProgressCallback callback){
        if(myTimer == null)
            myTimer = new MyTimer(callback);
        return myTimer;
    }

    public void initializeCallback(TimerProgressCallback callback){
        this.callback = callback;
    }

    public MyTimer(TimerProgressCallback callback){
        this.callback = callback;
    }

    private void initializeTimer(){
        if(timer == null)
            timer = new Timer();
    }

    private void initializeMyTimerTask(){
        if(myTimerTask == null)
            myTimerTask = new MyTimerTask();
    }

    private void scheduleTimer(){
        timer.schedule(myTimerTask, DELAYTIME, PERIODTIME);
    }

    public void setLastUpdatedCount(int lastUpdatedCount){
        this.lastUpdatedCount = lastUpdatedCount;
    }

    public int getLastUpdateTimeCunter(){
        return this.lastUpdatedCount;
    }

    public void setProgress(int progress){
        this.progress = progress;
    }

    public void start(){
        if(!isTimerRunning) {
            isTimerRunning = true;
            this.initializeTimer();
            this.initializeMyTimerTask();
            this.scheduleTimer();
        }
    }

    public void stop(){
        if(isTimerRunning) {
            isTimerRunning = false;
            if (myTimerTask != null) {
                myTimerTask.cancel();
                myTimerTask = null;
            }
            if (timer != null) {
                timer.cancel();
                timer = null;
            }
        }
    }

    public boolean isTimerRunning(){
        return isTimerRunning;
    }

    /**
     * get prgress
     * @return
     */
    public int getProgress(){
        return progress;
    }

    /**
     * Release timer
     */
    public void releaseTimer(){
        myTimer = null;
    }

    /**
     * Task
     * @author yashwant
     *
     */
    private class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            /*((Activity) context).runOnUiThread(new Runnable(){
                @Override
                public void run() {*/
            callback.onProgress(progress ++);
                /*}
            });*/
        }
    }
}
