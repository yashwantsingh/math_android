package com.mathfriendzy.timer;

/**
 * Created by root on 5/5/15.
 */
public interface CountDownCallback {
    void onTick(long millisUntilFinished);
    void onFinish();
}
