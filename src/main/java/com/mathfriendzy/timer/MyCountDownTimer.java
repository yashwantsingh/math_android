package com.mathfriendzy.timer;

import android.os.CountDownTimer;

/**
 * Created by root on 5/5/15.
 */
public class MyCountDownTimer extends CountDownTimer{

    private CountDownCallback callback = null;

    public MyCountDownTimer(long millisInFuture, long countDownInterval ,
                     CountDownCallback callback){
        super(millisInFuture , countDownInterval);
        this.callback = callback;
    }

    @Override
    public void onTick(long millisUntilFinished) {
        callback.onTick(millisUntilFinished);
    }

    @Override
    public void onFinish() {
        callback.onFinish();
    }
}
