package com.mathfriendzy.timer;

/**
 * Created by root on 15/4/15.
 */
public interface TimerProgressCallback {
    void onProgress(int progress);
}
