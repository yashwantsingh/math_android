package com.mathfriendzy.regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by root on 21/9/16.
 */
public class MyRegularExpression {

    //For the question number for assign homework
    /*" ',\",&,?, !, @,:,;,#,%,^,(,) $ ";*/
    private static String REG_EXPRESSION = "[\"$&,:;?@#'^()%!-]";
    public static boolean isSpecialCharacterExistInQuestion(String string){
        Pattern regex = Pattern.compile(REG_EXPRESSION);
        Matcher matcher = regex.matcher(string);
        if(matcher.find()){
            return true;
        }
        return false;
    }
}
