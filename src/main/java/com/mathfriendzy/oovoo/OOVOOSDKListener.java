package com.mathfriendzy.oovoo;

import com.oovoo.sdk.api.sdk_error;
import com.oovoo.sdk.interfaces.AVChatListener;
import com.oovoo.sdk.interfaces.Participant;

/**
 * Created by root on 10/10/16.
 */
public interface OOVOOSDKListener {
    void onAuthorized(String message);
    void onLogin(boolean loginStatus);
    void onConferenceStateChange(AVChatListener.ConferenceState conferenceState, sdk_error sdk_error
            , boolean isSendNotification);
    void onParticipantJoined(Participant participant, String s);
}
