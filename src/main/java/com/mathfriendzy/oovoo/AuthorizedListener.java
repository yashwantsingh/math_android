package com.mathfriendzy.oovoo;

/**
 * Created by root on 7/10/16.
 */
public interface AuthorizedListener {
    void onResult(String message);
}
