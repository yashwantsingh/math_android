package com.mathfriendzy.oovoo;

/**
 * Created by root on 10/10/16.
 */
public interface LoginListener {
    void onLogin(boolean loginStatus);
}
