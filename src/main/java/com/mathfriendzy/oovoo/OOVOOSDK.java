package com.mathfriendzy.oovoo;

import android.util.Log;

import com.oovoo.sdk.api.Message;
import com.oovoo.sdk.api.ooVooClient;
import com.oovoo.sdk.api.sdk_error;
import com.oovoo.sdk.interfaces.AVChatListener;
import com.oovoo.sdk.interfaces.AudioControllerListener;
import com.oovoo.sdk.interfaces.Messaging;
import com.oovoo.sdk.interfaces.MessagingListener;
import com.oovoo.sdk.interfaces.Participant;
import com.oovoo.sdk.interfaces.ooVooSdkResult;
import com.oovoo.sdk.interfaces.ooVooSdkResultListener;

import java.util.LinkedHashMap;

/**
 * Created by root on 7/10/16.
 */
public class OOVOOSDK implements AVChatListener, AudioControllerListener, MessagingListener {
    private final String TAG = "OOVOOSDK";
    public static final String  APP_TOKEN = "MDAxMDAxAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaDetAdMfu1kk0KXx%2FZ42qWiWqOODF9XYGV9ORMJ4fRkvsN8%2FCvTui9MEI%2B7b3YC6DrquBssCFN1qO9tY2H1l%2BcPpm3g1mXEKe5%2Fw93z7F%2Bw2xF81CLZfwDxvl1PmD5Aw%3D";
    private LinkedHashMap<String , Participant> participants = new LinkedHashMap<String, Participant>();
    private boolean m_isaudioinited = false;
    private boolean isLoginUser = false;

    private ooVooClient sdk = null;
    private OOVOOSDKListener oovoosdkListener = null;
    public static final String AUTHORIZED = "Authorised";
    public static final String NOT_AUTHORIZED = "Not Authorised";

    public static final String CALLING = "audio_calling";
    public static final String DISCONNECT = "audio_disconnect";
    public static final String REJECT = "audio_reject";

    public OOVOOSDK(){
        try {
            sdk = ooVooClient.sharedInstance();
            this.initListeners();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void initOOVOOSDKListener(OOVOOSDKListener oovoosdkListener){
        this.oovoosdkListener = oovoosdkListener;
    }

    private void initListeners(){
        sdk.getAVChat().setListener(this);
        sdk.getMessaging().setListener(this);
        sdk.getAVChat().getAudioController().setListener(this);
    }

    public boolean isAuthorized(){
        try {
            if(sdk.isAuthorized())
                return true;
            return false;
        }catch (Exception e){
            e.printStackTrace();
            return true;
        }
    }

    public boolean isLoginUser(){
        return isLoginUser;
    }

    public boolean isDeviceSupported(){
        try {
            if(sdk.isDeviceSupported())
                return true;
            return false;
        }catch (Exception e){
            e.printStackTrace();
            return true;
        }
    }

    public void authorizedSDK(){
        try {
            sdk = ooVooClient.sharedInstance();
            sdk.authorizeClient(APP_TOKEN, new ooVooSdkResultListener() {
                @Override
                public void onResult(ooVooSdkResult result) {
                    if (result.getResult() == sdk_error.OK) {
                        printLog(AUTHORIZED);
                        oovoosdkListener.onAuthorized(AUTHORIZED);
                    } else {
                        printLog(NOT_AUTHORIZED);
                        oovoosdkListener.onAuthorized(NOT_AUTHORIZED);
                        //Oops, you are not authorized , you can see reason of error by call result.getDescription()
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /*public void loginUser(String userId){
        sdk.getAccount().login(userId, new ooVooSdkResultListener() {
            @Override
            public void onResult(ooVooSdkResult ooVooSdkResult) {
                if (ooVooSdkResult.getResult() == sdk_error.OK) {
                    printLog("Login Success");
                    isLoginUser = true;
                    oovoosdkListener.onLogin(true);
                } else {
                    printLog("Login Fail");
                    isLoginUser = false;
                    oovoosdkListener.onLogin(false);
                }
            }
        });
    }*/

    public void loginUser(String userId , final LoginListener loginListener){
        sdk.getAccount().login(userId, new ooVooSdkResultListener() {
            @Override
            public void onResult(ooVooSdkResult ooVooSdkResult) {
                if (ooVooSdkResult.getResult() == sdk_error.OK) {
                    printLog("Login Success");
                    isLoginUser = true;
                    loginListener.onLogin(true);
                } else {
                    printLog("Login Fail");
                    isLoginUser = false;
                    loginListener.onLogin(false);
                }
            }
        });
    }

    public void joinConference(String conferenceId , String message){
        sdk.getAVChat().join(conferenceId , message);
    }

    ///////////////////
    @Override
    public void onParticipantJoined(Participant participant, String s) {
        participants.put(participant.getID() , participant);
        oovoosdkListener.onParticipantJoined(participant , s);
    }

    @Override
    public void onParticipantLeft(Participant participant) {
        oovoosdkListener.onConferenceStateChange(AVChatListener.ConferenceState.Disconnected ,
                sdk_error.OK , false);
        participants.remove(participant.getID());
    }

    @Override
    public void onConferenceStateChanged(ConferenceState conferenceState, sdk_error sdk_error) {
        printLog("onConferenceStateChange Change");
        if(conferenceState == AVChatListener.ConferenceState.Joined)
            this.startCall();
        else if(conferenceState == AVChatListener.ConferenceState.Disconnected)
            this.endCall();
        oovoosdkListener.onConferenceStateChange(conferenceState , sdk_error , true);
    }

    @Override
    public void onReceiveData(String s, byte[] bytes) {

    }

    @Override
    public void onConferenceError(sdk_error sdk_error) {

    }

    @Override
    public void onNetworkReliability(int i) {

    }

    @Override
    public void onSecurityState(boolean b) {

    }

    /////////////////////////////////////
    @Override
    public void onAudioTransmitStateChanged(boolean b, sdk_error sdk_error) {

    }

    @Override
    public void onAudioReceiveStateChanged(boolean b, sdk_error sdk_error) {

    }

    @Override
    public void onMicrophoneStateChange(boolean b, sdk_error sdk_error) {

    }

    @Override
    public void onSpeakerStateChange(boolean b, sdk_error sdk_error) {

    }

    @Override
    public void onAudioStateChange(AudioState audioState) {

    }


    /////////////////////////////////////
    @Override
    public void onMessageReceived(Message message) {

    }

    @Override
    public void onMessageAcknowledgementReceived(Messaging.MessageAcknowledgeState messageAcknowledgeState, String s) {

    }

    @Override
    public void onConnectivityStateChange(Messaging.ConnectivityState connectivityState, sdk_error sdk_error, String s) {

    }

    public void startCall(){
        if(!m_isaudioinited) {
            sdk.getAVChat().getAudioController().initAudio(new ooVooSdkResultListener() {
                @Override
                public void onResult(ooVooSdkResult init_audio_result) {
                    printLog("Application - > init audio completion " + init_audio_result);
                    sdk.getAVChat().getAudioController().setRecordMuted(false);
                    sdk.getAVChat().getAudioController().setPlaybackMuted(false);
                    if (init_audio_result.getResult() == com.oovoo.sdk.api.sdk_error.OK) {
                        m_isaudioinited = true;
                    }
                }
            });
        }
    }

    public void endCall(){
        if (m_isaudioinited) {
            sdk.getAVChat().getAudioController().uninitAudio(new ooVooSdkResultListener() {
                @Override
                public void onResult(ooVooSdkResult uninit_audio_result) {
                    printLog("uninitAudio, result = " + uninit_audio_result.getResult());
                    m_isaudioinited = false;
                }
            });
        }
    }

    public boolean isAudioStarted(){
        return m_isaudioinited;
    }

    public void leave(){
        this.endCall();
        sdk.getAVChat().leave();
    }

    public void logoutUser(){
        isLoginUser = false;
        sdk.getAccount().logout();
    }

    private void printLog(String message){
        Log.e(TAG, message);
    }
}
