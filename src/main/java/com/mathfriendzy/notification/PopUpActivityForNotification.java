package com.mathfriendzy.notification;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.helpastudent.ActHelpAStudent;
import com.mathfriendzy.controller.homework.ActHomeWork;
import com.mathfriendzy.controller.multifriendzy.MultiFriendzyRound;
import com.mathfriendzy.controller.multifriendzy.MultiFriendzyWinnerScreen;
import com.mathfriendzy.gcm.ProcessNotification;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.homework.HomeWorkNotification;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;

/**
 * This activity will open dialog for notification when user on the math friendzy application
 * @author Yashwant Singh
 *
 */
public class PopUpActivityForNotification extends Activity implements OnClickListener
{
    private Button btnOk 		= null;
    private Button btnCencel 	= null;
    private TextView txtMessage = null;

    public static final String ACT_CALL_FOR = "actCallFor";
    public static final int CALL_FOR_HOME_WORK = 1;
    public static final int CALL_FOR_CHAT_REQUEST = 2;
    public static final int CALL_FOR_HELP_STUDENT = 3;
    public static final int CALL_FOR_STUDENT_INPUT = 4;
    public static final int CALL_FOR_DISCONNECT_STUDENT = 5;
    public static final int CALL_FOR_DISCONNECT_TUTOR = 6;
    public static final int CALL_FOR_TUTOR_AREA_IMAGE_UPDATE = 7;
    public static final int CALL_FOR_TUTOR_WORK_AREA_IMAGE_UPDATE = 8;
    public static final int CALL_FOR_TUTOR_CANT_HELP_RIGHT_NOW = 9;
    private int callingAct = 0;

    private HomeWorkNotification homeWorkNotificationData = null;
    private RequireHelpResponse chatRequestNotifData = null;
    private HelpSetudentResponse helpStudentNotifData = null;
    private StudentInputNotif studentInputNotifData = null;
    private DisconnectStudentNotif disconnectStudentNotifData = null;
    private DisconnectWithTutorNotif disconnectWithTutorNotif = null;
    private TutorImageUpdateNotif tutorImageUpdateNotif = null;
    private TutorWorkAreaImageUpdateNotif tutorWorkAreaImageUpdateNotif = null;
    private TutorCanthelpRightNowNotifResponse tutorCantHelpNotif = null;

    private final String TAG = this.getClass().getSimpleName();
    private boolean isHideCancelButton = false;
    private boolean isHelpStudentScreenOpen = false;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_up_activity_for_notification);

        this.getIntentValues();
        this.setWidgetsReferences();
        this.setListenerOnWidgets();
        this.setTextFromtranselation();
        this.hideCancelButton(isHideCancelButton);
    }


    /**
     * Get the intent values which are from the previous screen
     */
    private void getIntentValues() {
        callingAct = this.getIntent().getIntExtra(PopUpActivityForNotification.ACT_CALL_FOR, 0);
        if(callingAct == CALL_FOR_HOME_WORK){
            homeWorkNotificationData = (HomeWorkNotification)
                    this.getIntent().getSerializableExtra("notifData");
        }else if(callingAct == CALL_FOR_CHAT_REQUEST){
            //isHideCancelButton = true;
            chatRequestNotifData = (RequireHelpResponse)
                    this.getIntent().getSerializableExtra("notifData");
            isHelpStudentScreenOpen = this.getIntent().getBooleanExtra("isHelpStudentScreenOpen"
                    , false);
        }else if(callingAct == CALL_FOR_HELP_STUDENT){
            isHideCancelButton = true;
            helpStudentNotifData = (HelpSetudentResponse)
                    this.getIntent().getSerializableExtra("notifData");
        }else if(callingAct == CALL_FOR_STUDENT_INPUT){
            isHideCancelButton = true;
            studentInputNotifData = (StudentInputNotif)
                    this.getIntent().getSerializableExtra("notifData");
        }else if(callingAct == CALL_FOR_DISCONNECT_STUDENT){
            isHideCancelButton = true;
            disconnectStudentNotifData = (DisconnectStudentNotif)
                    this.getIntent().getSerializableExtra("notifData");
        }else if(callingAct == CALL_FOR_DISCONNECT_TUTOR){
            isHideCancelButton = true;
            disconnectWithTutorNotif = (DisconnectWithTutorNotif)
                    this.getIntent().getSerializableExtra("notifData");
        }else if(callingAct == CALL_FOR_TUTOR_AREA_IMAGE_UPDATE){
            isHideCancelButton = true;
            tutorImageUpdateNotif = (TutorImageUpdateNotif)
                    this.getIntent().getSerializableExtra("notifData");
        }else if(callingAct == CALL_FOR_TUTOR_WORK_AREA_IMAGE_UPDATE){
            isHideCancelButton = true;
            tutorWorkAreaImageUpdateNotif = (TutorWorkAreaImageUpdateNotif)
                    this.getIntent().getSerializableExtra("notifData");
        }else if(callingAct == CALL_FOR_TUTOR_CANT_HELP_RIGHT_NOW){
            isHideCancelButton = true;
            tutorCantHelpNotif = (TutorCanthelpRightNowNotifResponse)
                    this.getIntent().getSerializableExtra("notifData");
        }
    }

    private void hideCancelButton(boolean isHide){
        if(isHide){
            btnCencel.setVisibility(Button.GONE);
        }else{
            btnCencel.setVisibility(Button.VISIBLE);
        }
    }

    /**
     * This method set widgets references
     */
    private void setWidgetsReferences(){
        btnOk 		= (Button) findViewById(R.id.btnOk);
        btnCencel 	= (Button) findViewById(R.id.btnCencel);
        txtMessage  = (TextView) findViewById(R.id.txtMessageFromNotification);

        if(callingAct == CALL_FOR_HOME_WORK){
            txtMessage.setText(homeWorkNotificationData.getMsg());
        }else if(callingAct == CALL_FOR_CHAT_REQUEST){
            txtMessage.setText(chatRequestNotifData.getMessage());
        }else if(callingAct == CALL_FOR_HELP_STUDENT){
            txtMessage.setText(helpStudentNotifData.getMessage());
        }else if(callingAct == CALL_FOR_STUDENT_INPUT){
            txtMessage.setText(studentInputNotifData.getMessage());
        }else if(callingAct == CALL_FOR_DISCONNECT_STUDENT){
            txtMessage.setText(disconnectStudentNotifData.getMessage());
        }else if(callingAct == CALL_FOR_DISCONNECT_TUTOR){
            txtMessage.setText(disconnectWithTutorNotif.getMessage());
        }else if(callingAct == CALL_FOR_TUTOR_AREA_IMAGE_UPDATE){
            txtMessage.setText(tutorImageUpdateNotif.getMessage());
        }else if(callingAct == CALL_FOR_TUTOR_WORK_AREA_IMAGE_UPDATE){
            txtMessage.setText(tutorWorkAreaImageUpdateNotif.getMessage());
        }else if(callingAct == CALL_FOR_TUTOR_CANT_HELP_RIGHT_NOW){
            txtMessage.setText(this.getCancelTutorCantHelpMessage());
        }else{
            txtMessage.setText(ProcessNotification.sentNotificationData.getMessage());
        }
    }

    /**
     * Return the tutor cant help right now to the student message
     * @return
     */
    private String getCancelTutorCantHelpMessage(){
        String canthelpMessage = tutorCantHelpNotif.getName();
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        canthelpMessage = canthelpMessage + " " + transeletion
                .getTranselationTextByTextIdentifier("tutorNotAvailableRightNow");
        transeletion.closeConnection();
        return canthelpMessage;
    }

    /**
     * This method set text from translation
     */
    private void setTextFromtranselation(){
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        if(callingAct == CALL_FOR_HOME_WORK){
            btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGo"));
            btnCencel.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
        }else if(callingAct == CALL_FOR_CHAT_REQUEST){
            btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleYes"));
            btnCencel.setText(transeletion.getTranselationTextByTextIdentifier("lblNo"));
        }else{
            btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
            btnCencel.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleCancel"));
        }
        transeletion.closeConnection();
    }

    /**
     * This method set listener on widgets
     */
    private void setListenerOnWidgets(){
        btnOk.setOnClickListener(this);
        btnCencel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v){
        switch(v.getId()){
            case R.id.btnOk :
                this.clickOnOkButton();
                finish();
                break;
            case R.id.btnCencel :
                if(callingAct == CALL_FOR_CHAT_REQUEST){
                    if(isHelpStudentScreenOpen){
                        ActHelpAStudent.getCurrentObj().updateWhenUSerPressNoThanksFromNotification();
                    }
                    this.cancelChatRequest();
                }else {
                    ProcessNotification.isFromNotification = false;
                }
                finish();
                break;
        }
    }

    /**
     * This method call when click on ok button
     */
    private void clickOnOkButton(){
        if(callingAct == CALL_FOR_HOME_WORK){
            this.callForHomeWork();
        }else if(callingAct == CALL_FOR_CHAT_REQUEST){
            this.processNotificationForChatRequest();
        }else if(callingAct == CALL_FOR_HELP_STUDENT){
            this.processNotificationForHelpStudent();
        }else if(callingAct == CALL_FOR_STUDENT_INPUT){
            this.processStudentInputNotification();
        }else if(callingAct == CALL_FOR_DISCONNECT_STUDENT){
            this.processDisconnectStudentNotification();
        }else if(callingAct == CALL_FOR_DISCONNECT_TUTOR){
            this.processDisconnectTutorNotification();
        }else if(callingAct == CALL_FOR_TUTOR_AREA_IMAGE_UPDATE){
            this.processTutorAreaImageUpdateNotification();
        }else if(callingAct == CALL_FOR_TUTOR_WORK_AREA_IMAGE_UPDATE){
            this.processTutorWorkAreaImageUpdateNotification();
        }else if(callingAct == CALL_FOR_TUTOR_CANT_HELP_RIGHT_NOW) {
            this.processTutorCanthelpresponse();
        }else{
            if(ProcessNotification.sentNotificationData.getWinnerId().equals("-1")){
                Intent intent = new Intent(this,MultiFriendzyRound.class);
                startActivity(intent);
            }
            else{
                Intent intent = new Intent(this,MultiFriendzyWinnerScreen.class);
                startActivity(intent);
            }
        }
    }

    /**
     * Call when notification for homework
     */
    private void callForHomeWork(){
        Intent intent = new Intent(this, ActHomeWork.class);
        startActivity(intent);
    }

    private void showToast(){
        MathFriendzyHelper.showToast(this , "Under Developement!!!");
    }

    /**
     * Process notification for chat request
     */
    private void processNotificationForChatRequest(){
        if(isHelpStudentScreenOpen){
            ActHelpAStudent.getCurrentObj().updateUIFromNotofication();
        }else{
            startActivity(new Intent(this , ActHelpAStudent.class));
        }
    }

    private void processNotificationForHelpStudent(){
        //startActivity(new Intent(this , ActHomeWork.class));
    }

    private void processStudentInputNotification(){
        //startActivity(new Intent(this , ActHelpAStudent.class));
    }

    private void processDisconnectStudentNotification(){
        //startActivity(new Intent(this , ActHomeWork.class));
    }

    private void processDisconnectTutorNotification() {
        //startActivity(new Intent(this , ActHelpAStudent.class));
    }
    private void processTutorAreaImageUpdateNotification() {
        //startActivity(new Intent(this , ActHelpAStudent.class));
    }
    private void processTutorWorkAreaImageUpdateNotification() {
        //startActivity(new Intent(this , ActHelpAStudent.class));
    }
    private void processTutorCanthelpresponse() {

    }

    @Override
    public void onBackPressed() {
        ProcessNotification.isFromNotification = false;
        super.onBackPressed();
    }

    /**
     * Cancel Chat Request
     */
    private void cancelChatRequest(){
        if(CommonUtils.isInternetConnectionAvailable(this)
                && chatRequestNotifData.getChatReqId() > 0) {
            CancelChatRequestParam param = new CancelChatRequestParam();
            param.setAction("tutorCantHelpRightNow");
            param.setChatRequestId(chatRequestNotifData.getChatReqId());
            new MyAsyckTask(ServerOperation
                    .createPostRequestForCancelChatRequest(param)
                    , null, ServerOperationUtil.CANCEL_CHAT_REQUEST_BY_TUTOR_REQUEST, this,
                    new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {

                        }
                    }, ServerOperationUtil.SIMPLE_DIALOG, false,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        }
    }
}
