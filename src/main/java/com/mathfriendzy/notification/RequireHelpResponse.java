package com.mathfriendzy.notification;

import java.io.Serializable;

/**
 * Created by root on 6/5/15.
 */
public class RequireHelpResponse implements Serializable{
    private String message;
    private int chatReqId;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getChatReqId() {
        return chatReqId;
    }

    public void setChatReqId(int chatReqId) {
        this.chatReqId = chatReqId;
    }
}
