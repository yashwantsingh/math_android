package com.mathfriendzy.notification;

import java.io.Serializable;

/**
 * Created by root on 7/5/15.
 */
public class DisconnectStudentNotif implements Serializable{

    private String message;
    private String disconnectStudent;
    private String chatRequestId;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDisconnectStudent() {
        return disconnectStudent;
    }

    public void setDisconnectStudent(String disconnectStudent) {
        this.disconnectStudent = disconnectStudent;
    }

    public String getChatRequestId() {
        return chatRequestId;
    }

    public void setChatRequestId(String chatRequestId) {
        this.chatRequestId = chatRequestId;
    }
}
