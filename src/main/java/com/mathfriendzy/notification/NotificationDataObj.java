package com.mathfriendzy.notification;

public class NotificationDataObj 
{
	private String message;
	private String userId;
	private String playerId;
	private String countrIso;
	private String oppUserId;
	private String oppPlayerId;
	private String friendzyId;
	private String profileImageId;
	private String opponentMessage;
	private String oppPid;
}
