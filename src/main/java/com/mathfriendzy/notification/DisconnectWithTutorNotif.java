package com.mathfriendzy.notification;

import java.io.Serializable;

/**
 * Created by root on 7/5/15.
 */
public class DisconnectWithTutorNotif implements Serializable{
    private String message;
    private String disconnectTutor;
    private String chatRequestId;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDisconnectTutor() {
        return disconnectTutor;
    }

    public void setDisconnectTutor(String disconnectTutor) {
        this.disconnectTutor = disconnectTutor;
    }

    public String getChatRequestId() {
        return chatRequestId;
    }

    public void setChatRequestId(String chatRequestId) {
        this.chatRequestId = chatRequestId;
    }
}
