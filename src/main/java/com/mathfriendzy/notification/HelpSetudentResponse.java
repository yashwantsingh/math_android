package com.mathfriendzy.notification;

import java.io.Serializable;

/**
 * Created by root on 7/5/15.
 */
public class HelpSetudentResponse implements Serializable{
    private String message;
    private String title;
    private String problem;
    private String helpStud;
    private String chatReqId;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public String getHelpStud() {
        return helpStud;
    }

    public void setHelpStud(String helpStud) {
        this.helpStud = helpStud;
    }

    public String getChatReqId() {
        return chatReqId;
    }

    public void setChatReqId(String chatReqId) {
        this.chatReqId = chatReqId;
    }
}
