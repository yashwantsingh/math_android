package com.mathfriendzy.notification;

import java.io.Serializable;

/**
 * Created by root on 29/6/15.
 */
public class TutorCanthelpRightNowNotifResponse implements Serializable{

    private String message;
    private int chatReqId;
    private String name;
    private int cantHelpNow;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getChatReqId() {
        return chatReqId;
    }

    public void setChatReqId(int chatReqId) {
        this.chatReqId = chatReqId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCantHelpNow() {
        return cantHelpNow;
    }

    public void setCantHelpNow(int cantHelpNow) {
        this.cantHelpNow = cantHelpNow;
    }
}
