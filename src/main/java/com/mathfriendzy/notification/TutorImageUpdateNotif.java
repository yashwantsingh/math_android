package com.mathfriendzy.notification;

import java.io.Serializable;

/**
 * Created by root on 7/5/15.
 */
public class TutorImageUpdateNotif implements Serializable{

    public static final int STUDENT = 1;
    public static final int TUTOR = 2;

    private String message;
    private String tutorImageUpdate;
    private String chatRequestId;
    private String image;
    private int updateFor;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTutorImageUpdate() {
        return tutorImageUpdate;
    }

    public void setTutorImageUpdate(String tutorImageUpdate) {
        this.tutorImageUpdate = tutorImageUpdate;
    }

    public String getChatRequestId() {
        return chatRequestId;
    }

    public void setChatRequestId(String chatRequestId) {
        this.chatRequestId = chatRequestId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getUpdateFor() {
        return updateFor;
    }

    public void setUpdateFor(int updateFor) {
        this.updateFor = updateFor;
    }
}
