package com.mathfriendzy.notification;

import java.io.Serializable;

/**
 * Created by root on 7/5/15.
 */
public class StudentInputNotif implements Serializable{

    private String message;
    private String studentInput;
    private String chatReqId;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStudentInput() {
        return studentInput;
    }

    public void setStudentInput(String studentInput) {
        this.studentInput = studentInput;
    }

    public String getChatReqId() {
        return chatReqId;
    }

    public void setChatReqId(String chatReqId) {
        this.chatReqId = chatReqId;
    }
}
