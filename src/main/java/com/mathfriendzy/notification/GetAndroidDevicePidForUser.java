package com.mathfriendzy.notification;

import org.json.JSONException;
import org.json.JSONObject;

import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;

import android.os.AsyncTask;
import android.util.Log;

/**
 * This asynckTask get udid for the device
 * @author Yashwant Singh
 *
 */
public class GetAndroidDevicePidForUser extends AsyncTask<Void, Void, Void> 
{
	private final String TAG = this.getClass().getSimpleName();
	
	private String userId ; 
	private String udid;//device id
	private SendNotificationTransferObj sendNotificationObj;
	private String pid = null;
	
	
	public GetAndroidDevicePidForUser(String userId , String udid , SendNotificationTransferObj sendNotificationObj)
	{
		this.userId = userId;
		this.udid = udid;
		this.sendNotificationObj = sendNotificationObj;
	}
	
	@Override
	protected void onPreExecute() 
	{
		super.onPreExecute();
	}

	@Override
	protected Void doInBackground(Void... params) 
	{
		pid = getAndroidDevicePidsForUser(userId , udid);
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) 
	{
		//Log.e(TAG, "pid " + pid);
		
		//sendNotificationObj.setDevicePid(pid);
		sendNotificationObj.setSenderDeviceId(pid);
		
		//sent notification
		new SendNotification(sendNotificationObj).execute(null,null,null);	
		
		super.onPostExecute(result);
	}

	/**
	 * This method get the pids for android device
	 * @param userId
	 * @param playerId
	 */
	private String getAndroidDevicePidsForUser(String userId ,String udid)
	{
		String action = "getAndroidDevicePidsForUser";
		String strUrl = ICommonUtils.CONPLETE_URL_FOR_NOTIFICATION + "action=" + action + "&"
						+ "userId=" + userId + "&"
						+ "udid="   + udid   + "&"
						+"appId="   + CommonUtils.APP_ID;
		
		//Log.e(TAG, "inside getAndroidDevicePidsForUser url : " + strUrl);
		
		return this.parseGetAndroidDevicePidJson(CommonUtils.readFromURL(strUrl));
	}
	
	/**
	 * This method parse json for get android device id
	 * @param jsonString
	 * @return
	 */
	private String parseGetAndroidDevicePidJson(String jsonString)
	{
		//Log.e(TAG, "inside parseGetAndroidDevicePidJson json String : " + jsonString);
		
		try 
		{
			JSONObject jObject = new JSONObject(jsonString);
			return jObject.getString("data");
		}
		catch (JSONException e) 
		{
			Log.e("SingleFriendzyServerOperation", "No FriendzyID exist exist :" + e.toString());
			return "";
		}
	}
	
}
