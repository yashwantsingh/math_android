package com.mathfriendzy.notification;

import java.io.Serializable;

/**
 * Created by root on 7/5/15.
 */
public class TutorWorkAreaImageUpdateNotif implements Serializable{
    private String message;
    private String tutorWorkImgUpdate;
    private String chatRequestId;
    private String image;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTutorWorkImgUpdate() {
        return tutorWorkImgUpdate;
    }

    public void setTutorWorkImgUpdate(String tutorWorkImgUpdate) {
        this.tutorWorkImgUpdate = tutorWorkImgUpdate;
    }

    public String getChatRequestId() {
        return chatRequestId;
    }

    public void setChatRequestId(String chatRequestId) {
        this.chatRequestId = chatRequestId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
