package com.mathfriendzy.notification;

/**
 * Created by root on 29/6/15.
 */
public class CancelChatRequestParam {
    private String action;
    private int chatRequestId;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getChatRequestId() {
        return chatRequestId;
    }

    public void setChatRequestId(int chatRequestId) {
        this.chatRequestId = chatRequestId;
    }
}
