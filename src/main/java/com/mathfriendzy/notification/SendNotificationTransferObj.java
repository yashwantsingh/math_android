package com.mathfriendzy.notification;

public class SendNotificationTransferObj 
{
	
	String devicePid;
	String message;
	String userId;
	String playerId;
	String country;
	String oppUserId;
	String oppPlayerId;
	String friendzyId;
	String profileImageId;
	String shareMessage;
	String senderDeviceId;
	String winnerId;
	String iphoneDeviceIds;
	
	//changes for word problem
	int forWord;
	
	//changes for ios notification
	int isNotoficationFromIOS;
	
	
	public int getIsNotoficationFromIOS() {
		return isNotoficationFromIOS;
	}

	public void setIsNotoficationFromIOS(int isNotoficationFromIOS) {
		this.isNotoficationFromIOS = isNotoficationFromIOS;
	}

	public int getForWord() {
		return forWord;
	}
	
	public void setForWord(int forWord) {
		this.forWord = forWord;
	}
	
	public String getIphoneDeviceIds() {
		return iphoneDeviceIds;
	}
	public void setIphoneDeviceIds(String iphoneDeviceIds) {
		this.iphoneDeviceIds = iphoneDeviceIds;
	}
	public String getWinnerId() {
		return winnerId;
	}
	public void setWinnerId(String winnerId) {
		this.winnerId = winnerId;
	}
	public String getDevicePid() {
		return devicePid;
	}
	public void setDevicePid(String devicePid) {
		this.devicePid = devicePid;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getOppUserId() {
		return oppUserId;
	}
	public void setOppUserId(String oppUserId) {
		this.oppUserId = oppUserId;
	}
	public String getOppPlayerId() {
		return oppPlayerId;
	}
	public void setOppPlayerId(String oppPlayerId) {
		this.oppPlayerId = oppPlayerId;
	}
	public String getFriendzyId() {
		return friendzyId;
	}
	public void setFriendzyId(String friendzyId) {
		this.friendzyId = friendzyId;
	}
	public String getProfileImageId() {
		return profileImageId;
	}
	public void setProfileImageId(String profileImageId) {
		this.profileImageId = profileImageId;
	}
	public String getShareMessage() {
		return shareMessage;
	}
	public void setShareMessage(String shareMessage) {
		this.shareMessage = shareMessage;
	}
	public String getSenderDeviceId() {
		return senderDeviceId;
	}
	public void setSenderDeviceId(String senderDeviceId) {
		this.senderDeviceId = senderDeviceId;
	}
	
	
}
