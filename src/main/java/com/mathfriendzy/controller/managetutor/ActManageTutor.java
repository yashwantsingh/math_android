package com.mathfriendzy.controller.managetutor;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.managetutor.mystudent.ActManageTutorMyStudents;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.utils.CommonUtils;

public class ActManageTutor extends ActBase {

    private final String TAG = this.getClass().getSimpleName();
    private Button btnMyStudents = null;
    private Button btnOtherTutor = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_manage_tutor);

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside onCreate()");

        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "outside onCreate()");
    }

    @Override
    protected void setWidgetsReferences() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside setWidgetsReferences()");

        //from base class
        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        btnMyStudents = (Button) findViewById(R.id.btnMyStudents);
        btnOtherTutor = (Button) findViewById(R.id.btnOtherTutor);

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "outside setWidgetsReferences()");
    }

    @Override
    protected void setListenerOnWidgets() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside setListenerOnWidgets()");

        btnMyStudents.setOnClickListener(this);
        btnOtherTutor.setOnClickListener(this);

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "outside setListenerOnWidgets()");
    }

    @Override
    protected void setTextFromTranslation() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside setTextFromTranslation()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("btnManageTutorsTitle"));
        btnMyStudents.setText(transeletion.getTranselationTextByTextIdentifier("resultTitleMyStudents"));
        btnOtherTutor.setText(transeletion.getTranselationTextByTextIdentifier("btnOtherTutorTitle"));
        transeletion.closeConnection();

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "outside setTextFromTranslation()");
    }

    /**
     * Click on My Students
     */
    private void clickOnMyStudent(){
        Intent intent = new Intent(this , ActManageTutorMyStudents.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnMyStudents:
                this.clickOnMyStudent();
                break;
            case R.id.btnOtherTutor:
                break;
        }
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {

    }
}
