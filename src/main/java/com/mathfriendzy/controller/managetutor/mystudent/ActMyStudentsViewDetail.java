package com.mathfriendzy.controller.managetutor.mystudent;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.tutor.ActTutorSession;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.helpastudent.GetPlayerNeedHelpForTutorResponse;
import com.mathfriendzy.model.helpastudent.LoadServiceTimeParam;
import com.mathfriendzy.model.helpastudent.LoadServiceTimeResponse;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.managetutor.mystudents.GetStidentsTutoringDetailForTeacherResponse;
import com.mathfriendzy.model.managetutor.mystudents.GetTutoringDetailForTeacherParam;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ActMyStudentsViewDetail extends ActBase {

    private final String TAG = this.getClass().getSimpleName();
    private ListView lstMyStudents = null;

    private TextView txtStudentName = null;
    private TextView txtGrade = null;
    private TextView txtMyTutors = null;
    private TextView txtStudentTutored = null;
    private TextView txtStudyGroup = null;
    private ImageView imgMyTutor = null;
    private ImageView imgStudentTutored = null;
    private ImageView imgStudyGroup = null;

    //for list header layout
    private TextView txtMyTutor = null;
    private TextView txtRating = null;
    private TextView txtLastModified = null;
    private TextView txtTimeSpent = null;
    private TextView txtView = null;

    private UserPlayerDto player = null;

    public static final int MY_TUTOR = 0;
    public static final int STUDENT_TUTORED = 1;
    public static final int STUDY_GROUP = 2;

    private ArrayList<GetStidentsTutoringDetailForTeacherResponse> tutorList = null;
    private ArrayList<GetStidentsTutoringDetailForTeacherResponse> studentList = null;
    private ArrayList<GetStidentsTutoringDetailForTeacherResponse> groupList = null;

    //text
    private String lblTutorName = null;
    private String lblRating = null;
    private String lblLastModified = null;
    private String lblTimeSpent = null;
    private String lblView = null;
    private String lblStudentsName = null;
    private String lblCreatedBy = null;
    private String lblTitle = null;

    private StudentDetailAdapter adapter = null;

    private Button btnShowMore = null;
    private final int MAX_RECORD_LIMIT = 20;
    private int selectedCategory = MY_TUTOR;
    private GetTutoringDetailForTeacherParam param = null;
    private int isNeedToShowMoreForTutor = 0;
    private int isNeedToShowMoreForStudentTutores = 0;
    private final int YES = 1;
    private final int NO = 0;

    //for total time helping layout
    private RelativeLayout totlaTimeHelpingStudentLayout = null;
    private TextView txtTotalTimeHelpingStudent = null;
    private TextView txtFrom = null;
    private TextView txtFromDate = null;
    private RelativeLayout fromLayout = null;
    private TextView txtTo = null;
    private TextView txtToDate = null;
    private RelativeLayout toLayout = null;
    private TextView txtTime = null;
    private TextView txtHours = null;
    //for load service time
    private String startDate = "";
    private String endDate = "";
    private final String SERVICE_TIME_MY_FORMATE = "yyyy-MM-dd";
    private final String SERVICE_TIME_CONVERTED_FORMATE = "MMM. dd,yyyy";
    private String lblStartDateCantLater = null;
    private RelativeLayout studyGroupsLayout = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_my_students_view_detail);

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside onClick()");

        this.getIntentValues();
        this.init();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.getStudentTutoringDetailForTeacher(true);
        this.loadServiceTime();

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "outside onClick()");
    }

    /**
     * String load service time
     */
    private void loadServiceTime() {
        if(CommonUtils.isInternetConnectionAvailable(this)){
            LoadServiceTimeParam param = new LoadServiceTimeParam();
            param.setAction("getServiceTimeForTutor");
            param.setUserId(player.getParentUserId());
            param.setPlayerId(player.getPlayerid());
            param.setStartDate(startDate);
            param.setEndData(endDate);
            param.setTimeZone(MathFriendzyHelper.getTimeForTutorServiceTime(this));
            new MyAsyckTask(ServerOperation.createPostRequestForLoadServiceTime(param)
                    , null, ServerOperationUtil.GET_SERVICE_TIME_FOR_TUTOR_REQUEST, this,
                    this, ServerOperationUtil.SIMPLE_DIALOG , true ,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        }else{
            CommonUtils.showInternetDialog(this);
        }
    }

    /**
     * Set service time from server
     * @param response
     */
    private void serviceTimeFromServer(LoadServiceTimeResponse response){
        startDate = response.getStartDate();
        endDate   = response.getEndData();
        txtFromDate.setText(response.getStartDate());
        txtToDate.setText(response.getEndData());
        txtTime.setText(this.getServiceTimeString(response));
    }

    private String getServiceTimeString(LoadServiceTimeResponse response){
        long totalTime = Long.parseLong(response.getTutoringTime()) +
                Long.parseLong(response.getIdleTime());
        return MathFriendzyHelper.getTutorTimeSpentText(totalTime);
    }

    /**
     * Get the intent values which are set in the previous screen
     */
    private void getIntentValues() {
        player = (UserPlayerDto) this.getIntent().getSerializableExtra("player");
    }

    /**
     * Initialization
     */
    private void init() {
        groupList = new ArrayList<GetStidentsTutoringDetailForTeacherResponse>();
    }

    @Override
    protected void setWidgetsReferences() {

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside setWidgetsReferences()");

        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        lstMyStudents = (ListView) findViewById(R.id.lstMyStudents);
        txtStudentName = (TextView) findViewById(R.id.txtStudentName);
        txtGrade = (TextView) findViewById(R.id.txtGrade);
        txtMyTutors = (TextView) findViewById(R.id.txtMyTutors);
        txtStudentTutored = (TextView) findViewById(R.id.txtStudentTutored);
        txtStudyGroup = (TextView) findViewById(R.id.txtStudyGroup);

        txtMyTutor = (TextView) findViewById(R.id.txtMyTutor);
        txtRating = (TextView) findViewById(R.id.txtRating);
        txtLastModified = (TextView) findViewById(R.id.txtLastModified);
        txtTimeSpent = (TextView) findViewById(R.id.txtTimeSpent);
        txtView = (TextView) findViewById(R.id.txtView);

        imgMyTutor = (ImageView) findViewById(R.id.imgMyTutor);
        imgStudentTutored = (ImageView) findViewById(R.id.imgStudentTutored);
        imgStudyGroup = (ImageView) findViewById(R.id.imgStudyGroup);

        btnShowMore = (Button) findViewById(R.id.btnShowMore);

        //for total time
        totlaTimeHelpingStudentLayout = (RelativeLayout) findViewById(R.id.totlaTimeHelpingStudentLayout);
        txtTotalTimeHelpingStudent = (TextView) findViewById(R.id.txtTotalTimeHelpingStudent);
        txtFrom = (TextView) findViewById(R.id.txtFrom);
        txtFromDate = (TextView) findViewById(R.id.txtFromDate);
        txtTo = (TextView) findViewById(R.id.txtTo);
        txtToDate = (TextView) findViewById(R.id.txtToDate);
        txtTime = (TextView) findViewById(R.id.txtTime);
        txtHours = (TextView) findViewById(R.id.txtHours);
        fromLayout = (RelativeLayout) findViewById(R.id.fromLayout);
        toLayout = (RelativeLayout) findViewById(R.id.toLayout);


        studyGroupsLayout = (RelativeLayout) findViewById(R.id.studyGroupsLayout);
        studyGroupsLayout.setVisibility(RelativeLayout.INVISIBLE);

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "outside setWidgetsReferences()");
    }

    @Override
    protected void setListenerOnWidgets() {

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside setListenerOnWidgets()");

        imgMyTutor.setOnClickListener(this);
        imgStudentTutored.setOnClickListener(this);
        imgStudyGroup.setOnClickListener(this);
        btnShowMore.setOnClickListener(this);
        fromLayout.setOnClickListener(this);
        toLayout.setOnClickListener(this);

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "outside setListenerOnWidgets()");
    }

    @Override
    protected void setTextFromTranslation() {

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside setTextFromTranslation()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("resultTitleMyStudents"));
        txtMyTutors.setText(transeletion.getTranselationTextByTextIdentifier("lblMyTutors"));
        txtStudentTutored.setText(transeletion.getTranselationTextByTextIdentifier("lblStudentsTutored"));
        txtStudyGroup.setText(transeletion.getTranselationTextByTextIdentifier("lblStudyGroup"));
        txtMyTutor.setText(transeletion.getTranselationTextByTextIdentifier("lblTutorName"));
        txtRating.setText(transeletion.getTranselationTextByTextIdentifier("lblRating"));
        txtLastModified.setText(transeletion.getTranselationTextByTextIdentifier("lblLastModified"));
        txtTimeSpent.setText(transeletion.getTranselationTextByTextIdentifier("lblTimeSpent"));
        txtView.setText(transeletion.getTranselationTextByTextIdentifier("lblView"));
        btnShowMore.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleShowMore"));

        //for student detail
        txtStudentName.setText(transeletion.getTranselationTextByTextIdentifier("mfLblStudentsName") + ": "
                + player.getFirstname() + " " + player.getLastname());
        txtGrade.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade") + ": "
                + player.getGrade());

        lblTutorName = transeletion.getTranselationTextByTextIdentifier("lblTutorName");
        lblRating    = transeletion.getTranselationTextByTextIdentifier("lblRating");
        lblLastModified = transeletion.getTranselationTextByTextIdentifier("lblLastModified");
        lblTimeSpent = transeletion.getTranselationTextByTextIdentifier("lblTimeSpent");
        lblView      = transeletion.getTranselationTextByTextIdentifier("lblView");
        lblStudentsName = transeletion.getTranselationTextByTextIdentifier("lblStudent") + "'s " +
                transeletion.getTranselationTextByTextIdentifier("lblRegName");
        lblCreatedBy = transeletion.getTranselationTextByTextIdentifier("lblCreatedBy");
        lblTitle     = transeletion.getTranselationTextByTextIdentifier("lblTitle");

        //for total time
        txtTotalTimeHelpingStudent.setText(transeletion.getTranselationTextByTextIdentifier("lblHelpingTime"));
        txtFrom.setText(transeletion.getTranselationTextByTextIdentifier("lblFrom") + ":");
        txtTo.setText(transeletion.getTranselationTextByTextIdentifier("lblTo") + ":");
        txtHours.setText(transeletion.getTranselationTextByTextIdentifier("lblHours"));
        lblStartDateCantLater = transeletion.getTranselationTextByTextIdentifier("lblStartDateCantLater");
        transeletion.closeConnection();

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "outside setTextFromTranslation()");
    }

    /**
     * Set header text
     * @param columnHeader1
     * @param columnHeader2
     * @param columnHeader3
     * @param columnHeader4
     * @param columnHeader5
     */
    private void setHeaderText(String columnHeader1 , String columnHeader2 ,
                               String columnHeader3 ,String columnHeader4 , String columnHeader5){
        txtMyTutor.setText(columnHeader1);
        txtRating.setText(columnHeader2);
        txtLastModified.setText(columnHeader3);
        txtTimeSpent.setText(columnHeader4);
        txtView.setText(columnHeader5);
    }

    /**
     * Set category
     * @param selectedCategory
     */
    private void setCategory(int selectedCategory ,
                             ArrayList<GetStidentsTutoringDetailForTeacherResponse> list){
        switch(selectedCategory){
            case MY_TUTOR:
                imgMyTutor.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
                imgStudentTutored.setBackgroundResource(R.drawable.mf_check_box_ipad);
                imgStudyGroup.setBackgroundResource(R.drawable.mf_check_box_ipad);
                this.setHeaderText(lblTutorName , lblRating , lblLastModified , lblTimeSpent , lblView);
                break;
            case STUDENT_TUTORED:
                imgMyTutor.setBackgroundResource(R.drawable.mf_check_box_ipad);
                imgStudentTutored.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
                imgStudyGroup.setBackgroundResource(R.drawable.mf_check_box_ipad);
                this.setHeaderText(lblStudentsName , lblRating , lblLastModified , lblTimeSpent , lblView);
                break;
            case STUDY_GROUP:
                imgMyTutor.setBackgroundResource(R.drawable.mf_check_box_ipad);
                imgStudentTutored.setBackgroundResource(R.drawable.mf_check_box_ipad);
                imgStudyGroup.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
                this.setHeaderText(lblCreatedBy , lblTitle , lblLastModified , lblTimeSpent , lblView);
                break;
        }
        this.setMyTutorList(list , selectedCategory);
    }

    /**
     * Set my tutor list adapter
     */
    private void setMyTutorList(ArrayList<GetStidentsTutoringDetailForTeacherResponse> list ,
                                final int selectedCategory ){
        this.selectedCategory = selectedCategory;
        adapter = new StudentDetailAdapter(this , list ,
                new StudentDetailOnItemClickListener() {
                    @Override
                    public void clickOnView(int position) {
                        //Log.e(TAG , "position " + position + " " + adapter.list.get(position).getTimeSpent());
                        //MathFriendzyHelper.showWarningDialog(ActMyStudentsViewDetail.this, "Under Developement!!!");
                        openTutorSessionForSecondWorkArea(adapter.list.get(position) , selectedCategory);
                    }

                    @Override
                    public void clickOnExclateView(int position) {
                        openSendEmailActivity(adapter.list.get(position) , selectedCategory);
                    }
                });
        lstMyStudents.setAdapter(adapter);
    }

    /**
     * Initialize for the show the tutor session to teacher
     * @param detailObject
     * @return
     */
    private GetPlayerNeedHelpForTutorResponse initializeGetPlayerNeedHelpForTutorResponseFromTheDetailObject
            (GetStidentsTutoringDetailForTeacherResponse detailObject){
        GetPlayerNeedHelpForTutorResponse selectedTutor = new GetPlayerNeedHelpForTutorResponse();
        selectedTutor.setRequestId(detailObject.getRequestId());
        selectedTutor.setPlayerFName(detailObject.getPlayerFName());
        selectedTutor.setPlayerLName(detailObject.getPlayerLName());
        selectedTutor.setGrade(detailObject.getGrade());
        selectedTutor.setChatId(detailObject.getChatId());
        selectedTutor.setChatUserName(detailObject.getChatUserName());
        selectedTutor.setReqDate(detailObject.getReqDate());
        selectedTutor.setMessage(detailObject.getMessage());
        selectedTutor.setIsConnected(detailObject.getIsConnected());
        selectedTutor.setTutorUid(detailObject.getTutorUid());
        selectedTutor.setTutorPid(detailObject.getTutorPid());
        selectedTutor.setChatDialogId(detailObject.getChatDialogId());
        selectedTutor.setIsAnonymous(detailObject.getIsAnonymous());
        selectedTutor.setStars(detailObject.getRating());
        selectedTutor.setTimeSpent(detailObject.getTimeSpent());
        selectedTutor.setStudentImage(detailObject.getStudentImage());
        selectedTutor.setTutorImage(detailObject.getTutorImage());
        selectedTutor.setWorkAreaImage(detailObject.getWorkAreaImageName());
        selectedTutor.setLastUpdatedByTutor(detailObject.getLastUpdatedByTutor());
        selectedTutor.setUserId(detailObject.getTutorUid());
        selectedTutor.setPlayerId(detailObject.getTutorPid());
        return selectedTutor;
    }

    /**
     * Show tutor session for teacher
     * @param getStidentsTutoringDetailForTeacherResponse
     */
    private void openTutorSessionForSecondWorkArea
    (GetStidentsTutoringDetailForTeacherResponse getStidentsTutoringDetailForTeacherResponse
     , int selectedCategory){
        Intent intent = new Intent(this , ActTutorSession.class);
        intent.putExtra("isShowTutorTabToTeacher" , true);
        intent.putExtra("selectedTutor" ,
                this.initializeGetPlayerNeedHelpForTutorResponseFromTheDetailObject
                        (getStidentsTutoringDetailForTeacherResponse));
        intent.putExtra("player" , player);
        intent.putExtra("selectedCategory" , selectedCategory);
        startActivity(intent);
    }


    /**
     * Open send email activity
     * @param getStidentsTutoringDetailForTeacherResponse
     */
    private void openSendEmailActivity(GetStidentsTutoringDetailForTeacherResponse
                                               getStidentsTutoringDetailForTeacherResponse
            , int selectedCategory){
        Intent intent = new Intent(this , ActSendEmailToTutorTeacher.class);
        intent.putExtra("tutorDetail" , getStidentsTutoringDetailForTeacherResponse);
        intent.putExtra("player" , player);
        intent.putExtra("selectedCategory" , selectedCategory);
        startActivity(intent);
    }

    /**
     * Get the student tutoring detail for teacher
     */
    private void getStudentTutoringDetailForTeacher(boolean firstTime){
        if(CommonUtils.isInternetConnectionAvailable(this)){
            param = new GetTutoringDetailForTeacherParam();
            param.setAction("getStudentsTutoringDetailsForTeacher");
            param.setPlayerId(player.getPlayerid());
            param.setUserId(player.getParentUserId());
            param.setFirstTime(firstTime);
            if(!firstTime){
                if(this.selectedCategory == MY_TUTOR){
                    param.setIsTutorList(1);
                }else if(this.selectedCategory == STUDENT_TUTORED){
                    param.setIsTutorList(0);
                }
                param.setOffset(adapter.list.size());
            }
            new MyAsyckTask(ServerOperation.createPostRequestForGetStudentsTutoringDetailsForTeacher(param)
                    , null, ServerOperationUtil.GET_STUDENT_TUTORING_DETAIL_FOR_TEACHER_REQUEST, this,
                    this, ServerOperationUtil.SIMPLE_DIALOG , true ,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        }else{
            CommonUtils.showInternetDialog(this);
        }
    }

    private void setVisibilityOfTotlaTimeHelpingStudentLayout(boolean isVisible){
         if(isVisible){
             totlaTimeHelpingStudentLayout.setVisibility(RelativeLayout.VISIBLE);
         }else{
             totlaTimeHelpingStudentLayout.setVisibility(RelativeLayout.GONE);
         }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgMyTutor:
                this.setVisibilityOfTotlaTimeHelpingStudentLayout(false);
                setCategory(MY_TUTOR, tutorList);
                if(isNeedToShowMoreForTutor == YES){
                    this.setShowMoreVisibility(true);
                }else{
                    this.setShowMoreVisibility(false);
                }
                break;
            case R.id.imgStudentTutored:
                this.setVisibilityOfTotlaTimeHelpingStudentLayout(true);
                setCategory(STUDENT_TUTORED , studentList);
                if(isNeedToShowMoreForStudentTutores == YES){
                    this.setShowMoreVisibility(true);
                }else{
                    this.setShowMoreVisibility(false);
                }
                break;
            case R.id.imgStudyGroup:
                this.setVisibilityOfTotlaTimeHelpingStudentLayout(false);
                setCategory(STUDY_GROUP , groupList);
                break;
            case R.id.btnShowMore:
                this.getStudentTutoringDetailForTeacher(false);
                break;
            case R.id.fromLayout:
                this.clickOnFromLayout();
                break;
            case R.id.toLayout:
                this.clickOnToLayout();
                break;
        }
    }

    /**
     * From Date
     */
    private void clickOnFromLayout(){
        final Calendar calender = MathFriendzyHelper.getCurrentCalender();
        MathFriendzyHelper.showDatePickerDialogPrevDateSelected(this, calender.get(Calendar.YEAR),
                calender.get(Calendar.MONTH), calender.get(Calendar.DAY_OF_MONTH),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);

                        //if (newDate.after(calender)) {
                        setDateText(txtFromDate , year, monthOfYear, dayOfMonth , true);
                        //}
                    }
                }, calender);
    }

    /**
     * To Date
     */
    private void clickOnToLayout(){
        final Calendar calender = MathFriendzyHelper.getCurrentCalender();
        MathFriendzyHelper.showDatePickerDialogPrevDateSelected(this, calender.get(Calendar.YEAR),
                calender.get(Calendar.MONTH), calender.get(Calendar.DAY_OF_MONTH),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        //if (newDate.after(calender)) {
                        setDateText(txtToDate , year, monthOfYear, dayOfMonth , false);
                        //}
                    }
                }, calender);
    }

    /**
     * Set date text
     * @param txtView
     * @param year
     * @param month
     * @param dayOfMonth
     * @param isStartDate
     */
    private void setDateText(TextView txtView , int year , int month , int dayOfMonth ,
                             boolean isStartDate){

        String tempStartDate = startDate;
        String tempEndDate   = endDate;

        month ++;
        String formateString = MathFriendzyHelper.getFourDigitFormatNumber(year) + "-" +
                MathFriendzyHelper.getDoubleDigitFormatNumber(month) + "-" +
                MathFriendzyHelper.getDoubleDigitFormatNumber(dayOfMonth);
        String formatedDate = MathFriendzyHelper.formatDataInGivenFormat(formateString ,
                SERVICE_TIME_MY_FORMATE, SERVICE_TIME_CONVERTED_FORMATE);
        if(isStartDate){
            tempStartDate = formatedDate;
        }else{
            tempEndDate = formatedDate;
        }

        Date fromDate = MathFriendzyHelper.getValidateDate(tempStartDate , SERVICE_TIME_CONVERTED_FORMATE);
        Date toDate   = MathFriendzyHelper.getValidateDate(tempEndDate , SERVICE_TIME_CONVERTED_FORMATE);
        if(fromDate.compareTo(toDate) <= 0){
            startDate = tempStartDate;
            endDate   = tempEndDate;
            txtView.setText(formatedDate);
            this.loadServiceTime();
        }else{
            MathFriendzyHelper.showWarningDialog(this , lblStartDateCantLater);
        }
    }

    private void setShowMoreVisibility(boolean isVisible){
        if(isVisible){
            btnShowMore.setVisibility(Button.VISIBLE);
        }else{
            btnShowMore.setVisibility(Button.GONE);
        }
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
        if(requestCode == ServerOperationUtil.GET_STUDENT_TUTORING_DETAIL_FOR_TEACHER_REQUEST){
            GetStidentsTutoringDetailForTeacherResponse reposne =
                    (GetStidentsTutoringDetailForTeacherResponse) httpResponseBase;
            if(reposne.getResponse().equalsIgnoreCase("success")) {
                if(param.isFirstTime()){
                    tutorList = reposne.getTutorList();
                    studentList = reposne.getStudentList();
                    setCategory(MY_TUTOR , tutorList);
                    if(tutorList.size() >= MAX_RECORD_LIMIT){
                        isNeedToShowMoreForTutor = YES;
                        this.setShowMoreVisibility(true);
                    }else{
                        isNeedToShowMoreForTutor = NO;
                        this.setShowMoreVisibility(false);
                    }

                    if(studentList.size() >= MAX_RECORD_LIMIT){
                        isNeedToShowMoreForStudentTutores = YES;
                    }else{
                        isNeedToShowMoreForStudentTutores = NO;
                    }
                }else{
                    tutorList.addAll(reposne.getTutorList());
                    studentList.addAll(reposne.getStudentList());
                    if(param.getIsTutorList() == 1){//for tutor list
                        if(reposne.getTutorList().size() >= MAX_RECORD_LIMIT){
                            isNeedToShowMoreForTutor = YES;
                            this.setShowMoreVisibility(true);
                        }else{
                            isNeedToShowMoreForTutor = NO;
                            this.setShowMoreVisibility(false);
                        }
                        setCategory(MY_TUTOR , tutorList);
                    }else{//for student tutored list
                        if(reposne.getStudentList().size() >= MAX_RECORD_LIMIT){
                            this.setShowMoreVisibility(true);
                            isNeedToShowMoreForStudentTutores = YES;
                        }else{
                            isNeedToShowMoreForStudentTutores = NO;
                            this.setShowMoreVisibility(false);
                        }
                        setCategory(STUDENT_TUTORED , studentList);
                    }
                }
            }
        }else if(requestCode == ServerOperationUtil.GET_SERVICE_TIME_FOR_TUTOR_REQUEST) {
            LoadServiceTimeResponse response = (LoadServiceTimeResponse) httpResponseBase;
            if(response.getResult().equalsIgnoreCase("success")){
                this.serviceTimeFromServer(response);
            }
        }
    }
}
