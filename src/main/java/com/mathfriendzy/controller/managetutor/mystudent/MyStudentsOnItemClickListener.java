package com.mathfriendzy.controller.managetutor.mystudent;

/**
 * Created by root on 23/4/15.
 */
public interface MyStudentsOnItemClickListener {

    void onViewClick(int position);
    void onEditClick(int position);
    void onCheckTutor(int position);
}
