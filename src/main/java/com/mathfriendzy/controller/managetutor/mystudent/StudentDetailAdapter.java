package com.mathfriendzy.controller.managetutor.mystudent;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mathfriendzy.R;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.managetutor.mystudents.GetStidentsTutoringDetailForTeacherResponse;

import java.util.ArrayList;

/**
 * Created by root on 22/4/15.
 */
public class StudentDetailAdapter extends BaseAdapter{

    private LayoutInflater mInflater = null;
    private ViewHolder vHolder = null;
    public ArrayList<GetStidentsTutoringDetailForTeacherResponse> list = null;
    private final String SERVER_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    //private final String CONVERTED_DATE_FORMAT = "MMM. dd,yyyy HH:mm:ss";
    private final String CONVERTED_DATE_FORMAT = "MMM. dd,yyyy hh:mm aaa";
    private StudentDetailOnItemClickListener callback;

    public StudentDetailAdapter(Context context , ArrayList<GetStidentsTutoringDetailForTeacherResponse> list
    , StudentDetailOnItemClickListener callback){
        mInflater = LayoutInflater.from(context);
        this.list = list;
        this.callback = callback;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        if(view == null){
            view = mInflater.inflate(R.layout.manage_student_my_stduent_detail_list_layout , null);
            vHolder = new ViewHolder();
            vHolder.txtMyTutor = (TextView) view.findViewById(R.id.txtMyTutor);
            vHolder.txtLastModified = (TextView) view.findViewById(R.id.txtLastModified);
            vHolder.txtTimeSpent = (TextView) view.findViewById(R.id.txtTimeSpent);
            vHolder.imgStar1 = (ImageView) view.findViewById(R.id.imgStar1);
            vHolder.imgStar2 = (ImageView) view.findViewById(R.id.imgStar2);
            vHolder.imgStar3 = (ImageView) view.findViewById(R.id.imgStar3);
            vHolder.imgStar4 = (ImageView) view.findViewById(R.id.imgStar4);
            vHolder.imgStar5 = (ImageView) view.findViewById(R.id.imgStar5);
            vHolder.viewLayout = (RelativeLayout) view.findViewById(R.id.viewLayout);
            vHolder.excalteLayout = (RelativeLayout) view.findViewById(R.id.excalteLayout);
            view.setTag(vHolder);
        }else{
            vHolder = (ViewHolder) view.getTag();
        }

        vHolder.txtMyTutor.setText(list.get(position).getPlayerFName() + " " + list.get(position).getPlayerLName());
        vHolder.txtLastModified.setText(MathFriendzyHelper.formatDataInGivenFormatWithDefaultTimeZone
                (list.get(position).getReqDate(), SERVER_DATE_FORMAT, CONVERTED_DATE_FORMAT));
        vHolder.txtTimeSpent.setText(MathFriendzyHelper.getTimeSpentText(list.get(position).getTimeSpent()));
        this.setRating(list.get(position).getRating() , vHolder.imgStar1 , vHolder.imgStar2 ,
                vHolder.imgStar3 , vHolder.imgStar4 , vHolder.imgStar5);
        vHolder.viewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.clickOnView(position);
            }
        });

        vHolder.excalteLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.clickOnExclateView(position);
            }
        });
        return view;
    }

    /**
     * Set rating
     * @param rating
     */
    private void setRating(int rating , ImageView imgStart1 ,
                           ImageView imgStart2 , ImageView imgStart3 , ImageView imgStart4 , ImageView imgStart5){
        switch(rating){
            case 0:
                imgStart1.setBackgroundResource(R.drawable.start_unselected);
                imgStart2.setBackgroundResource(R.drawable.start_unselected);
                imgStart3.setBackgroundResource(R.drawable.start_unselected);
                imgStart4.setBackgroundResource(R.drawable.start_unselected);
                imgStart5.setBackgroundResource(R.drawable.start_unselected);
                break;
            case 1:
                imgStart1.setBackgroundResource(R.drawable.star_selected);
                imgStart2.setBackgroundResource(R.drawable.start_unselected);
                imgStart3.setBackgroundResource(R.drawable.start_unselected);
                imgStart4.setBackgroundResource(R.drawable.start_unselected);
                imgStart5.setBackgroundResource(R.drawable.start_unselected);
                break;
            case 2:
                imgStart1.setBackgroundResource(R.drawable.star_selected);
                imgStart2.setBackgroundResource(R.drawable.star_selected);
                imgStart3.setBackgroundResource(R.drawable.start_unselected);
                imgStart4.setBackgroundResource(R.drawable.start_unselected);
                imgStart5.setBackgroundResource(R.drawable.start_unselected);
                break;
            case 3:
                imgStart1.setBackgroundResource(R.drawable.star_selected);
                imgStart2.setBackgroundResource(R.drawable.star_selected);
                imgStart3.setBackgroundResource(R.drawable.star_selected);
                imgStart4.setBackgroundResource(R.drawable.start_unselected);
                imgStart5.setBackgroundResource(R.drawable.start_unselected);
                break;
            case 4:
                imgStart1.setBackgroundResource(R.drawable.star_selected);
                imgStart2.setBackgroundResource(R.drawable.star_selected);
                imgStart3.setBackgroundResource(R.drawable.star_selected);
                imgStart4.setBackgroundResource(R.drawable.star_selected);
                imgStart5.setBackgroundResource(R.drawable.start_unselected);
                break;
            case 5:
                imgStart1.setBackgroundResource(R.drawable.star_selected);
                imgStart2.setBackgroundResource(R.drawable.star_selected);
                imgStart3.setBackgroundResource(R.drawable.star_selected);
                imgStart4.setBackgroundResource(R.drawable.star_selected);
                imgStart5.setBackgroundResource(R.drawable.star_selected);
                break;
        }
    }

    private class ViewHolder{
        private TextView txtMyTutor;
        private ImageView imgStar1;
        private ImageView imgStar2;
        private ImageView imgStar3;
        private ImageView imgStar4;
        private ImageView imgStar5;
        private TextView txtLastModified;
        private TextView txtTimeSpent;
        private RelativeLayout viewLayout;
        private RelativeLayout excalteLayout;
    }
}
