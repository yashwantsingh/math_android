package com.mathfriendzy.controller.managetutor.mystudent;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.homework.assignhomeworkquiz.ClassesAdapter;
import com.mathfriendzy.controller.managetutor.settutor.ActSetStudentTutor;
import com.mathfriendzy.controller.managetutor.settutor.MainSubject;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.ClassSelectedListener;
import com.mathfriendzy.model.homework.SelectAssignHomeworkClassParam;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.managetutor.mystudents.GetMyStuentWithPasswordResponse;
import com.mathfriendzy.model.managetutor.mystudents.GetStudentWithPasswordParam;
import com.mathfriendzy.model.managetutor.mystudents.MarkAsTutorParam;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.classes.ClassWithName;
import com.mathfriendzy.model.registration.classes.UserClasses;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ActManageTutorMyStudents extends ActBase {

    private final String TAG = this.getClass().getSimpleName();
    private TextView txtStudentName = null;
    private TextView txtTutor = null;
    private TextView txtOnline = null;
    private TextView txtView = null;
    private ListView lstMyStudents = null;
    //private UserPlayerDto selectedPlayer = null;
    private int offset = 0;
    private Button btnShowMore = null;
    //private ArrayList<UserPlayerDto> students = null;
    private MyStudentAdapter adapter = null;

    //Totol Time Helping Student
    private TextView txtTotalTimeHelpingStudent = null;
    private TextView txtFrom = null;
    private TextView txtFromDate = null;
    private TextView txtTo = null;
    private TextView txtToDate = null;
    private Button btnGo = null;
    private RelativeLayout fromLayout = null;
    private RelativeLayout toLayout = null;
    private String startDate = "";
    private String endDate = "";
    private final String SERVICE_TIME_MY_FORMATE = "yyyy-MM-dd";
    private final String SERVICE_TIME_CONVERTED_FORMATE = "MMM. dd, yyyy";
    private String lblStartDateCantLater = null;
    private TextView txtGrade = null;
    //private Spinner spinnerGrade = null;
    private String selectedGrade = MathFriendzyHelper.ALL;
    private ArrayList<Integer> intGradeList = null;

    //class title changes
    private RegistereUserDto registerUserFromPreff = null;
    private TextView txtSelectClass = null;
    private RelativeLayout titleListLayout = null;
    private Button btnClassListDone = null;
    private ListView lstClassesList = null;
    private ClassesAdapter classAdapter = null;

    private final int OPEN_SET_TUTOR_SCREEN_REQUEST = 1001;


    private TextView txtSelectAPrevHW = null;
    private RelativeLayout selectClassLayout = null;
    private TextView txtSelectClassValue = null;
    private TextView txtSubject = null;
    private TextView txtSubjectValues = null;
    private TextView txtClass = null;
    private TextView txtClassValues = null;
    private String defaultSelectedClassId = "0";
    private final int STUDENT_LIMIT = 30;
    private MainSubject mainSubject = null;
    private TextView txtAverageRating = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_manage_tutor_my_students);

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside onCreate()");

        this.init();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        //this.loadMyStudents();
        this.loadInitialStudents();

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "outside onCreate()");
    }

    private void init() {
        //students = new ArrayList<UserPlayerDto>();
        //selectedPlayer = this.getPlayerData();
        registerUserFromPreff = MathFriendzyHelper.getUserFromPreff(this);
        /*startDate  = MathFriendzyHelper.getBackAndForthDateInGivenFormatDate
                (SERVICE_TIME_CONVERTED_FORMATE, 0 , 0 , 0);
        endDate = MathFriendzyHelper.getBackAndForthDateInGivenFormatDate
                (SERVICE_TIME_CONVERTED_FORMATE , 1 , 0 , 0);*/
        this.initClasses();
    }

    /**
     * Load the initial students for the first class for the teacher
     */
    private void loadInitialStudents(){
        try {
            if (classWithNames != null && classWithNames.size() > 0) {
                ArrayList<ClassWithName> initialClassSelectedList = new ArrayList<ClassWithName>();
                initialClassSelectedList.add(MathFriendzyHelper.getLastSelectedClass(this , classWithNames));
                this.setDataAfterClassSelection(initialClassSelectedList);
            } else {

            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void setWidgetsReferences() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside setWidgetsReferences()");

        txtTopbar 		= (TextView) findViewById(R.id.txtTopbar);
        txtStudentName  = (TextView) findViewById(R.id.txtStudentName);
        txtTutor  = (TextView) findViewById(R.id.txtTutor);
        txtOnline  = (TextView) findViewById(R.id.txtOnline);
        txtView  = (TextView) findViewById(R.id.txtView);
        lstMyStudents  = (ListView) findViewById(R.id.lstMyStudents);
        btnShowMore = (Button) findViewById(R.id.btnShowMore);

        //for total time
        txtTotalTimeHelpingStudent = (TextView) findViewById(R.id.txtTotalTimeHelpingStudent);
        txtFrom = (TextView) findViewById(R.id.txtFrom);
        txtFromDate = (TextView) findViewById(R.id.txtFromDate);
        txtTo = (TextView) findViewById(R.id.txtTo);
        txtToDate = (TextView) findViewById(R.id.txtToDate);
        fromLayout = (RelativeLayout) findViewById(R.id.fromLayout);
        toLayout = (RelativeLayout) findViewById(R.id.toLayout);
        btnGo = (Button) findViewById(R.id.btnGo);
        txtGrade = (TextView) findViewById(R.id.txtGrade);
        //spinnerGrade = (Spinner) findViewById(R.id.spinnerGrade);

        //remove the show more button from this screen , based on the 8/7/2015 issue
        setShowMoreButtonVisibility(false);

        //class title changes
        selectClassLayout = (RelativeLayout) findViewById(R.id.selectClassLayout);
        txtSelectClass = (TextView) findViewById(R.id.txtSelectClass);
        txtSelectClassValue = (TextView) findViewById(R.id.txtSelectClassValue);
        titleListLayout = (RelativeLayout) findViewById(R.id.titleListLayout);
        btnClassListDone = (Button) findViewById(R.id.btnClassListDone);
        lstClassesList = (ListView) findViewById(R.id.lstClassesList);


        txtSelectAPrevHW = (TextView) findViewById(R.id.txtSelectAPrevHW);
        txtSubject = (TextView) findViewById(R.id.txtSubject);
        txtSubjectValues = (TextView) findViewById(R.id.txtSubjectValues);
        txtClass = (TextView) findViewById(R.id.txtClass);
        txtClassValues = (TextView) findViewById(R.id.txtClassValues);

        txtAverageRating = (TextView) findViewById(R.id.txtAverageRating);

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "outside setWidgetsReferences()");
    }

    @Override
    protected void setListenerOnWidgets() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside setListenerOnWidgets()");

        btnShowMore.setOnClickListener(this);
        btnGo.setOnClickListener(this);
        fromLayout.setOnClickListener(this);
        toLayout.setOnClickListener(this);

        //class title changes
        selectClassLayout.setOnClickListener(this);
        btnClassListDone.setOnClickListener(this);

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "outside setListenerOnWidgets()");
    }

    @Override
    protected void setTextFromTranslation() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside setTextFromTranslation()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("resultTitleMyStudents"));
        txtStudentName.setText(transeletion.getTranselationTextByTextIdentifier("lblStudent") + "'s " +
                transeletion.getTranselationTextByTextIdentifier("lblRegName"));
        txtTutor.setText(transeletion.getTranselationTextByTextIdentifier("lblTutor"));
        txtOnline.setText(transeletion.getTranselationTextByTextIdentifier("lblOnlineOrNot"));
        txtView.setText(transeletion.getTranselationTextByTextIdentifier("lblView"));
        btnShowMore.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleShowMore"));

        //for total time
        txtTotalTimeHelpingStudent.setText(transeletion.getTranselationTextByTextIdentifier("lblHelpingTime"));
        txtFrom.setText(transeletion.getTranselationTextByTextIdentifier("lblFrom") + ":");
        txtTo.setText(transeletion.getTranselationTextByTextIdentifier("lblTo") + ":");
        btnGo.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGo"));
        lblStartDateCantLater = transeletion.getTranselationTextByTextIdentifier("lblStartDateCantLater");
        txtGrade.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade") + ":");

        //class title changes
        txtSelectClass.setText(transeletion.getTranselationTextByTextIdentifier("lblClass"));
        btnClassListDone.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleDone"));

        txtSubject.setText(transeletion.getTranselationTextByTextIdentifier("lblSubjectHeader"));
        txtClass.setText(transeletion.getTranselationTextByTextIdentifier("lblClass"));
        txtSelectClassValue.setHint(transeletion.getTranselationTextByTextIdentifier("lblSelect"));
        txtSelectAPrevHW.setText("The Easiest Way to Manage Tutors");
        txtAverageRating.setText(transeletion.getTranselationTextByTextIdentifier("lblAverage")
                + " " + transeletion.getTranselationTextByTextIdentifier("lblRating"));
        transeletion.closeConnection();

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "outside setTextFromTranslation()");
    }

    /**
     * Set grade adapter to the spinner
     * @param gradeList
     */
    /*private void setGradeAdapter(ArrayList<Integer> gradeList , String gradeSelectedValue){
        try {
            if (intGradeList == null) {
                intGradeList = gradeList;
            } else {
                for (int i = 0; i < gradeList.size(); i++) {
                    if (!intGradeList.contains(gradeList.get(i)))
                        intGradeList.add(gradeList.get(i));
                }
            }
            Collections.sort(intGradeList);
            ArrayAdapter<String> gradeAdapter = new ArrayAdapter<String>
                    (this, R.layout.spinner_textview_layout, this.getStringGradeList(intGradeList));
            gradeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerGrade.setAdapter(gradeAdapter);
            spinnerGrade.setSelection(gradeAdapter.getPosition(gradeSelectedValue));

            spinnerGrade.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectedGrade = spinnerGrade.getSelectedItem().toString();
                    *//*if(adapter != null){
                        adapter.filterStudent(selectedGrade);
                    }*//*

                    setClassTitle(MathFriendzyHelper.parseInt(selectedGrade));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }catch(Exception e){
            e.printStackTrace();
        }
    }*/

    private ArrayList<String> getStringGradeList(ArrayList<Integer> gradeList){
        ArrayList<String> strGradeList = new ArrayList<String>();
        strGradeList.add(MathFriendzyHelper.ALL);
        try {
            for (int i = 0; i < gradeList.size(); i++) {
                strGradeList.add(gradeList.get(i) + "");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return strGradeList;
    }


    /**
     * Load my students
     */
    private void loadMyStudents(String selectedClass) {
        if (CommonUtils.isInternetConnectionAvailable(this)) {
            GetStudentWithPasswordParam param = new GetStudentWithPasswordParam();
            param.setAction("getStudentsWithPasswordFromClass");
            param.setUserId(CommonUtils.getUserId(this));
            //param.setOffset(offset);
            param.setStartDate(startDate);
            param.setEndDate(endDate);
            //param.setLimit(STUDENT_LIMIT);
            param.setClassId(selectedClass);
            param.setForStudentAccount(false);
            param.setGetTime("1");
            /*if(registerUserFromPreff != null){
                param.setYear(registerUserFromPreff.getSchoolYear());
            }*/
            param.setTimeZone(MathFriendzyHelper.getTimeForTutorServiceTime(this));
            new MyAsyckTask(ServerOperation.createPostRequestForGetStudentsWithPasswordFromClass(param)
                    , null, ServerOperationUtil.GET_MY_STUDENTS_WITH_PASSWORD, this,
                    this, ServerOperationUtil.SIMPLE_DIALOG, true,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        } else {
            CommonUtils.showInternetDialog(this);
        }
    }

    /**
     * Set or notify adapter
     * @param students
     */
    private void setOrNotifiyAdapter(ArrayList<UserPlayerDto> students){

        MyStudentsOnItemClickListener listener = new MyStudentsOnItemClickListener() {
            @Override
            public void onViewClick(int position) {
                viewStudentDetail(adapter.filterStudentList.get(position));
            }

            @Override
            public void onEditClick(int position) {
                MathFriendzyHelper.showWarningDialog(ActManageTutorMyStudents.this ,
                        "Under Developement!!!");
            }

            @Override
            public void onCheckTutor(int position) {
                //adapter.onTutorCheck(position);
                openSetTutorScreen(position);
            }
        };

        if(adapter == null){
            adapter = new MyStudentAdapter(this ,students , listener);
            lstMyStudents.setAdapter(adapter);
        }else{
            adapter.addRecord(students);
            adapter.notifyDataSetChanged();
        }
    }

    private void openSetTutorScreen(int selectedPosition){
        Intent intent = new Intent(this , ActSetStudentTutor.class);
        intent.putExtra("selectedPosition" , selectedPosition);
        intent.putExtra("mainSubjectList" , this.mainSubject);
        if(adapter != null) {
            intent.putExtra("selectedStudent" , adapter.getItemAtIndex(selectedPosition));
        }
        startActivityForResult(intent, OPEN_SET_TUTOR_SCREEN_REQUEST);
    }

    /***
     * View Student detail
     * @param player
     */
    private void viewStudentDetail(UserPlayerDto player){
        Intent intent = new Intent(this , ActMyStudentsViewDetail.class);
        intent.putExtra("player" , player);
        startActivity(intent);
    }

    /**
     * From Date
     */
    private void clickOnFromLayout(){
        final Calendar calender = MathFriendzyHelper.getCurrentCalender();
        MathFriendzyHelper.showDatePickerDialogPrevDateSelected(this, calender.get(Calendar.YEAR),
                calender.get(Calendar.MONTH), calender.get(Calendar.DAY_OF_MONTH),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);

                        //if (newDate.after(calender)) {
                        setDateText(txtFromDate , year, monthOfYear, dayOfMonth , true);
                        //}
                    }
                }, calender);
    }

    /**
     * To Date
     */
    private void clickOnToLayout(){
        final Calendar calender = MathFriendzyHelper.getCurrentCalender();
        MathFriendzyHelper.showDatePickerDialogPrevDateSelected(this, calender.get(Calendar.YEAR),
                calender.get(Calendar.MONTH), calender.get(Calendar.DAY_OF_MONTH),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        //if (newDate.after(calender)) {
                        setDateText(txtToDate , year, monthOfYear, dayOfMonth , false);
                        //}
                    }
                }, calender);
    }

    /**
     * Set date text
     * @param txtView
     * @param year
     * @param month
     * @param dayOfMonth
     * @param isStartDate
     */
    private void setDateText(TextView txtView , int year , int month , int dayOfMonth ,
                             boolean isStartDate){

        String tempStartDate = startDate;
        String tempEndDate   = endDate;

        month ++;
        String formateString = MathFriendzyHelper.getFourDigitFormatNumber(year) + "-" +
                MathFriendzyHelper.getDoubleDigitFormatNumber(month) + "-" +
                MathFriendzyHelper.getDoubleDigitFormatNumber(dayOfMonth);
        String formatedDate = MathFriendzyHelper.formatDataInGivenFormat(formateString ,
                SERVICE_TIME_MY_FORMATE, SERVICE_TIME_CONVERTED_FORMATE);
        if(isStartDate){
            tempStartDate = formatedDate;
        }else{
            tempEndDate = formatedDate;
        }

        Date fromDate = MathFriendzyHelper.getValidateDate(tempStartDate , SERVICE_TIME_CONVERTED_FORMATE);
        Date toDate   = MathFriendzyHelper.getValidateDate(tempEndDate , SERVICE_TIME_CONVERTED_FORMATE);
        if(fromDate.compareTo(toDate) <= 0){
            startDate = tempStartDate;
            endDate   = tempEndDate;
            txtView.setText(formatedDate);
        }else{
            MathFriendzyHelper.showWarningDialog(this , lblStartDateCantLater);
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btnShowMore:
                offset = offset + STUDENT_LIMIT;
                this.loadMyStudents(this.getCommaSeparatedClassIds(selectedClassList));
                break;
            case R.id.btnGo:
                this.clickOnGo();
                break;
            case R.id.fromLayout:
                this.clickOnFromLayout();
                break;
            case R.id.toLayout:
                this.clickOnToLayout();
                break;
            case R.id.selectClassLayout:
                this.clickToSelectClass();
                break;
            case R.id.btnClassListDone:
                //this.clickToClassSelectionDone();
                break;
        }
    }

    /**
     * Click on Go
     */
    private void clickOnGo() {
        offset = 0;
        adapter = null;
        this.loadMyStudents(this.getCommaSeparatedClassIds(selectedClassList));
    }

    /**
     * Set visibility of show more button
     * @param isVisible
     */
    private void setShowMoreButtonVisibility(boolean isVisible){
        if(isVisible){
            btnShowMore.setVisibility(Button.VISIBLE);
        }else{
            btnShowMore.setVisibility(Button.GONE);
        }
    }

    /**
     * To From Date
     * @param response
     */
    private void setToFromDate(GetMyStuentWithPasswordResponse response){
        startDate = response.getStartDate();
        endDate   = response.getEndDate();
        txtFromDate.setText(response.getStartDate());
        txtToDate.setText(response.getEndDate());
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
        if(requestCode == ServerOperationUtil.GET_MY_STUDENTS_WITH_PASSWORD){
            GetMyStuentWithPasswordResponse response =
                    (GetMyStuentWithPasswordResponse) httpResponseBase;
            if(response.getResponse().equalsIgnoreCase("success")){
                this.setToFromDate(response);
                ArrayList<UserPlayerDto> students = response.getUserPlayer();
                this.setOrNotifiyAdapter(students);
            }
        }else if(requestCode == ServerOperationUtil.MARK_AS_TUTOR_REQUEST){
            finish();
        }
    }

    /**
     * Return the data string
     * @return
     */
    private String getDataString(){
        JSONArray jsonArray = new JSONArray();
        if(adapter != null){
            ArrayList<UserPlayerDto> students = adapter.filterStudentList;
            for(int i = 0 ; i < students.size() ; i ++ ){
                CommonUtils.printLog(students.get(i).getFirstname() + " " + students.get(i).isStateChange());
                if(students.get(i).isStateChange()) {
                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("userId" , students.get(i).getParentUserId());
                        jsonObject.put("playerId", students.get(i).getPlayerid());
                        jsonObject.put("isTutor", students.get(i).getIsTutor());
                        jsonObject.put("subjects", MathFriendzyHelper.
                                convertStringArrayListIntoStringWithDelimeter
                                        (students.get(i).getSubjectList(), ","));
                        jsonArray.put(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
        if(jsonArray.length() > 0)
            return jsonArray.toString();
        return "";
    }

    /**
     * Save mark tutor
     */
    private void saveMarkTutor(){
        if(CommonUtils.isInternetConnectionAvailable(this)){
            String dataString = this.getDataString();
            if(dataString.length() > 0) {
                MarkAsTutorParam param = new MarkAsTutorParam();
                param.setAction("markStudentsAsTutorForSubjects");
                param.setData(dataString);
                new MyAsyckTask(ServerOperation.createPostRequestForMarkAsTutor(param)
                        , null, ServerOperationUtil.MARK_AS_TUTOR_REQUEST, this,
                        this, ServerOperationUtil.SIMPLE_DIALOG, true,
                        getString(R.string.please_wait_dialog_msg))
                        .execute();
            }else{
                finish();
            }
        }else{
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        this.saveMarkTutor();
    }

    //below class title changes
    /*private void setClassTitle(int selectedGrade){
        if(selectedGrade == 0){//0 means user select All Option
            if(adapter != null){
                adapter.filterStudent(MathFriendzyHelper.ALL);
            }
            this.setVisibilityOfClassesLayout(false);
            this.setVisibilityOfClassesLayout(false);
            return ;
        }

        if(adapter != null){
            adapter.filterStudent(selectedGrade + "");
        }
        this.setVisibilityOfTitleListLayout(false);
        ArrayList<ClassWithName> classWithNames = this.getClassTitleList(selectedGrade);
        this.clearDataWhenUserSelectOtherGrade(classWithNames);
        if(classWithNames != null && classWithNames.size() > 0){
            this.setVisibilityOfClassesLayout(true);
            this.setClassListAdapter(classWithNames);
        }else{
            this.setVisibilityOfClassesLayout(false);
        }
    }*/

    /*private void clearDataWhenUserSelectOtherGrade
            (ArrayList<ClassWithName> classWithNames){
        classAdapter = null;
        txtSelectClassValue.setText("");
        if(classWithNames != null && classWithNames.size() > 0) {
            for (int i = 0; i < classWithNames.size(); i++) {
                classWithNames.get(i).setSelected(false);
            }
        }
    }*/

    /*private ArrayList<ClassWithName> getClassTitleList(int grade){
        if(registerUserFromPreff != null){
            ArrayList<UserClasses> userClasseses = registerUserFromPreff.getUserClasses();
            for (int i = 0 ; i < userClasseses.size() ; i ++ ){
                UserClasses userClass = userClasseses.get(i);
                if(userClass.getGrade() == grade){
                    return userClass.getClassList();
                }
            }
            return null;
        }else{
            return null;
        }
    }*/

    /*private void setVisibilityOfClassesLayout(boolean isVisible){
        if(isVisible){
            selectClassLayout.setVisibility(RelativeLayout.VISIBLE);
        }else{
            selectClassLayout.setVisibility(RelativeLayout.GONE);
        }
    }

    private void setVisibilityOfTitleListLayout(boolean isVisible){
        if(isVisible){
            titleListLayout.setVisibility(RelativeLayout.VISIBLE);
            lstClassesList.setVisibility(ListView.VISIBLE);
        }else{
            titleListLayout.setVisibility(RelativeLayout.GONE);
            lstClassesList.setVisibility(ListView.GONE);
        }
    }*/

    private ArrayList<ClassWithName> classWithNames = null;
    private ArrayList<ClassWithName> selectedClassList = null;

    private void resetValues() {
        offset = 0;
        adapter = null;
    }

    /**
     * Set the data after user select the class
     * @param selectedClass
     */
    private void setDataAfterClassSelection(ArrayList<ClassWithName> selectedClass){
        selectedClassList = selectedClass;
        if (selectedClass != null && selectedClass.size() > 0) {
            setTextToSelectedClassValues(getCommaSeparatedClassName(selectedClass));
            setTextToSelectedSubjectValues(getCommaSeparatedSubjectName(selectedClass));
        } else {
            setTextToSelectedClassValues("");
            setTextToSelectedSubjectValues("");
        }

        resetValues();
        loadMyStudents(getCommaSeparatedClassIds(selectedClassList));
    }

    private void initClasses(){
        classWithNames = this.getUserClasses();
    }

    private ArrayList<ClassWithName> getUserClasses(){
        try {
            if (registerUserFromPreff != null) {
                ArrayList<UserClasses> userClasseses = registerUserFromPreff.getUserClasses();
                ArrayList<ClassWithName> allGradeClasses = new ArrayList<ClassWithName>();
                for (int i = 0; i < userClasseses.size(); i++) {
                    UserClasses userClass = userClasseses.get(i);
                    allGradeClasses.addAll(userClass.getClassList());
                }
                return allGradeClasses;
            } else {
                return null;
            }
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    private void clickToSelectClass() {
        //this.setVisibilityOfTitleListLayout(true);
        if(!(classWithNames != null && classWithNames.size() > 0)){
            MathFriendzyHelper.showWarningDialog(this ,
                    MathFriendzyHelper.getTreanslationTextById(this , "lblNoClass"));
            return ;
        }
        SelectAssignHomeworkClassParam param = new SelectAssignHomeworkClassParam();
        param.setShowForPrevSaveHomework(true);
        MathFriendzyHelper.showClassSelectionDialog(this, classWithNames, new ClassSelectedListener() {
            @Override
            public void onClassSelectionDone(ArrayList<ClassWithName> selectedClass) {
                setDataAfterClassSelection(selectedClass);
            }

            @Override
            public void onUpdatedClasses(ArrayList<ClassWithName> updatedList) {
                classWithNames = updatedList;
            }
        }, param);
    }

    private void setTextToSelectedClassValues(String commaSeparatedClasses){
        txtSelectClassValue.setText(commaSeparatedClasses);
        txtClassValues.setText(commaSeparatedClasses);
    }

    private void setTextToSelectedSubjectValues(String commaSeparatedSubjects){
        txtSubjectValues.setText(commaSeparatedSubjects);
    }

    private String getCommaSeparatedSubjectName(ArrayList<ClassWithName> selectedClassList){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0 ; i < selectedClassList.size() ; i ++ ){
            stringBuilder.append(stringBuilder.length() > 0 ? "," + selectedClassList.get(i).getSubject()
                    : selectedClassList.get(i).getSubject());
        }
        return stringBuilder.toString();
    }

    /*private void clickToClassSelectionDone() {
        txtSelectClassValue.setText("");
        this.setVisibilityOfTitleListLayout(false);
        ArrayList<ClassWithName> selectedClassList = this.getSelectedClasses();
        if(selectedClassList != null && selectedClassList.size() > 0) {
            txtSelectClassValue.setText(this.getCommaSeparatedClassName(selectedClassList));
            try {
                if (adapter != null)
                    adapter.filterListBasedOnClassId(selectedClassList , this.getSelectedGrade());
            }catch (Exception e){
                e.printStackTrace();
            }
        }else{
            try {
                if (adapter != null)
                    adapter.filterListBasedOnClassId(null , this.getSelectedGrade());
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }*/

    /*private ArrayList<ClassWithName> getSelectedClasses(){
        if(classAdapter != null && classAdapter.getSelectedList().size() > 0){
            return classAdapter.getSelectedList();
        }
        return null;
    }

    private void setClassListAdapter(ArrayList<ClassWithName> classWithNames){
        classAdapter = new ClassesAdapter(this , classWithNames);
        lstClassesList.setAdapter(classAdapter);
    }*/

    private String getCommaSeparatedClassName(ArrayList<ClassWithName> selectedClassList){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0 ; i < selectedClassList.size() ; i ++ ){
            stringBuilder.append(stringBuilder.length() > 0 ? "," + selectedClassList.get(i).getClassName()
                    : selectedClassList.get(i).getClassName());
        }
        return stringBuilder.toString();
    }

    /*private int getSelectedGrade(){
        try{
            return MathFriendzyHelper.parseInt(spinnerGrade.getSelectedItem().toString());
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }*/

    private String getCommaSeparatedClassIds(ArrayList<ClassWithName> selectedClassList) {
        try {
            if(selectedClassList != null && selectedClassList.size() > 0 ) {
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 0; i < selectedClassList.size(); i++) {
                    stringBuilder.append(stringBuilder.length() > 0 ? "," + selectedClassList.get(i).getClassId()
                            : selectedClassList.get(i).getClassId());
                }
                return stringBuilder.toString();
            }
            return defaultSelectedClassId;
        } catch (Exception e) {
            e.printStackTrace();
            return defaultSelectedClassId;
        }
    }

    //end class title changes

    //new check tutor changes
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            switch (requestCode){
                case OPEN_SET_TUTOR_SCREEN_REQUEST:
                    int selectedPosition = data.getIntExtra("selectedPosition" , 0);
                    boolean isMakeTutor = data.getBooleanExtra("isAnyClassSubjectSelected" , false);
                    UserPlayerDto updatesSelectedStudent = (UserPlayerDto)
                            data.getSerializableExtra("updatesSelectedStudent");
                    if(adapter != null){
                        adapter.onTutorCheck(selectedPosition , isMakeTutor , updatesSelectedStudent);
                    }
                    mainSubject = (MainSubject) data.getSerializableExtra("mainSubjectList");
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
