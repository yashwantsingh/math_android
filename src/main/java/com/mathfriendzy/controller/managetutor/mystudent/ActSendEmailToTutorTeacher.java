package com.mathfriendzy.controller.managetutor.mystudent;


import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.managetutor.mystudents.GetHomeworkDetailForChatRequestParam;
import com.mathfriendzy.model.managetutor.mystudents.GetHomeworkDetailsForChatRequestIdResponse;
import com.mathfriendzy.model.managetutor.mystudents.GetStidentsTutoringDetailForTeacherResponse;
import com.mathfriendzy.model.managetutor.mystudents.SendEmailToOtherTeacherParam;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class ActSendEmailToTutorTeacher extends ActBase {

    private final String TAG = this.getClass().getSimpleName();

    private TextView txtSendAMessageToTutor = null;

    private TextView txtStudentTitle = null;
    private TextView txtStudentName = null;
    private TextView txtStudentSchollInfo = null;
    private TextView txtStudentTeacher = null;
    private TextView txtStudentTeacherTitle = null;

    private TextView txtTutorTitle = null;
    private TextView txtTutorName = null;
    private TextView txtTutorSchollInfo = null;
    private TextView txtTutorTeacher = null;
    private TextView txtTutorTeacherTitle = null;

    private TextView txtForTheHomeworkDueOn = null;
    private TextView txtDueDate = null;
    private TextView txtTitled = null;
    private TextView txtTitle = null;
    private TextView txtForProblem = null;
    private TextView txtProblem = null;

    private EditText edtReportProblem = null;
    private Button btnSend = null;

    private GetStidentsTutoringDetailForTeacherResponse studentDetail = null;
    private UserPlayerDto player = null;

    private final int MY_TUTOR = 0;
    private final int STUDENT_TUTORED = 1;
    int selectedCategory = 0;

    private RegistereUserDto loginUser = null;
    private GetHomeworkDetailsForChatRequestIdResponse response = null;
    private String lblPleaseEnterMessageBeforeSending = "Please enter the message before sending email.";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_send_email_to_tutor_teacher);

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside onCreate()");

        this.init();
        this.getIntentValues();
        this.setWidgetsReferences();;
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.setWidgetsValue();
        this.loadHomeworkData();

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "outside onCreate()");
    }

    /**
     * load the home work data
     */
    private void loadHomeworkData() {
        if(CommonUtils.isInternetConnectionAvailable(this)) {
            if (studentDetail != null) {
                GetHomeworkDetailForChatRequestParam param = new GetHomeworkDetailForChatRequestParam();
                param.setAction("getHomeworkDetailsForChatRequestId");
                param.setChatId(studentDetail.getRequestId());
                new MyAsyckTask(ServerOperation.createPostRequestForGetHomeworkDetailsForChatRequestId(param)
                        , null, ServerOperationUtil.GET_HOMEWORK_DETAIL_BY_CHAT_ID_REQUEST, this,
                        this, ServerOperationUtil.SIMPLE_DIALOG, true,
                        getString(R.string.please_wait_dialog_msg))
                        .execute();
            }
        }else{
            CommonUtils.showInternetDialog(this);
        }
    }

    /**
     * Initialization
     */
    private void init(){
        loginUser = CommonUtils.getLoginUser(this);
    }

    private void getIntentValues(){
        studentDetail = (GetStidentsTutoringDetailForTeacherResponse) this.getIntent()
                .getSerializableExtra("tutorDetail");
        player = (UserPlayerDto) this.getIntent().getSerializableExtra("player");
        selectedCategory = this.getIntent().getIntExtra("selectedCategory" , 0);
    }

    @Override
    protected void setWidgetsReferences() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside setWidgetsReferences()");

        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        txtSendAMessageToTutor = (TextView) findViewById(R.id.txtSendAMessageToTutor);
        txtStudentTitle = (TextView) findViewById(R.id.txtStudentTitle);
        txtStudentName = (TextView) findViewById(R.id.txtStudentName);
        txtStudentSchollInfo = (TextView) findViewById(R.id.txtStudentSchollInfo);
        txtStudentTeacher = (TextView) findViewById(R.id.txtStudentTeacher);
        txtTutorTitle = (TextView) findViewById(R.id.txtTutorTitle);
        txtTutorName = (TextView) findViewById(R.id.txtTutorName);
        txtTutorSchollInfo = (TextView) findViewById(R.id.txtTutorSchollInfo);
        txtTutorTeacher = (TextView) findViewById(R.id.txtTutorTeacher);
        txtForTheHomeworkDueOn = (TextView) findViewById(R.id.txtForTheHomeworkDueOn);
        txtDueDate = (TextView) findViewById(R.id.txtDueDate);
        txtTitled = (TextView) findViewById(R.id.txtTitled);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtForProblem = (TextView) findViewById(R.id.txtForProblem);
        txtProblem = (TextView) findViewById(R.id.txtProblem);
        edtReportProblem = (EditText) findViewById(R.id.edtReportProblem);
        btnSend = (Button) findViewById(R.id.btnSend);

        txtStudentTeacherTitle = (TextView) findViewById(R.id.txtStudentTeacherTitle);
        txtTutorTeacherTitle = (TextView) findViewById(R.id.txtTutorTeacherTitle);

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "outside setWidgetsReferences()");
    }

    @Override
    protected void setTextFromTranslation() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside setTextFromTranslation()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("resultTitleMyStudents"));
        txtStudentTitle.setText(transeletion.getTranselationTextByTextIdentifier("lblStudent")
                + "'s " + transeletion.getTranselationTextByTextIdentifier("lblInfo")  + ":"  );
        txtTutorTitle.setText(transeletion.getTranselationTextByTextIdentifier("lblTutorInfo")
        + " " + transeletion.getTranselationTextByTextIdentifier("lblInfo") + ":");
        txtForTheHomeworkDueOn.setText(transeletion.getTranselationTextByTextIdentifier("lblForTheHomeWorkDueOn"));
        txtTitled.setText(transeletion.getTranselationTextByTextIdentifier("lblTitled"));
        txtForProblem.setText(transeletion.getTranselationTextByTextIdentifier("lblForProblem"));
        lblPleaseEnterMessageBeforeSending = transeletion.getTranselationTextByTextIdentifier
                ("lblPleaseEnterMessageBeforeSending");
        btnSend.setText(transeletion.getTranselationTextByTextIdentifier("lblSend"));
        edtReportProblem.setHint(transeletion.getTranselationTextByTextIdentifier("enterMessagePlaceHolderText"));
        txtStudentTeacherTitle.setText(transeletion.getTranselationTextByTextIdentifier("lblStudent") + "'s "
                + transeletion.getTranselationTextByTextIdentifier("lblRegTeacher"));
        txtTutorTeacherTitle.setText(transeletion.getTranselationTextByTextIdentifier("lblTutor") + "'s "
                + transeletion.getTranselationTextByTextIdentifier("lblRegTeacher"));
        if (selectedCategory == MY_TUTOR) {
            txtSendAMessageToTutor.setText(transeletion.getTranselationTextByTextIdentifier("lblSendMessageToTutor")
                    + " " + transeletion.getTranselationTextByTextIdentifier("lblTutorInfo").toLowerCase()
                    + " " + transeletion.getTranselationTextByTextIdentifier("lblTeacherForSession") + ":");
        }else{
            txtSendAMessageToTutor.setText(transeletion.getTranselationTextByTextIdentifier("lblSendMessageToTutor")
                    + " " + transeletion.getTranselationTextByTextIdentifier("lblStudent").toLowerCase()
                    + "'s " + transeletion.getTranselationTextByTextIdentifier("lblTeacherForSession") + ":");
        }

        transeletion.closeConnection();

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "outside setTextFromTranslation()");
    }

    /**
     * Set the widgets value
     */
    private void setWidgetsValue() {
        if (studentDetail != null && player != null) {
            if (selectedCategory == MY_TUTOR) {
                txtStudentName.setText(player.getFirstname() + " " + player.getLastname());
                txtStudentSchollInfo.setText(loginUser.getSchoolName());
                txtStudentTeacher.setText(loginUser.getFirstName() + " " +
                        loginUser.getLastName());
                txtTutorName.setText(studentDetail.getPlayerFName() + " " +
                        studentDetail.getPlayerLName());
                txtTutorSchollInfo.setText(studentDetail.getSchoolName());
                txtTutorTeacher.setText(studentDetail.getTeacherName());
            } else if (selectedCategory == STUDENT_TUTORED) {
                txtStudentName.setText(studentDetail.getPlayerFName() + " " + studentDetail
                        .getPlayerLName());
                txtStudentSchollInfo.setText(studentDetail.getSchoolName());
                txtStudentTeacher.setText(studentDetail.getTeacherName());
                txtTutorName.setText(player.getFirstname() + " " + player.getLastname());
                txtTutorSchollInfo.setText(loginUser.getSchoolName());
                txtTutorTeacher.setText(loginUser.getFirstName() + " " + loginUser.getLastName());
            }
        }
    }

    private void setHomeworkData(GetHomeworkDetailsForChatRequestIdResponse response){
        txtDueDate.setText(response.getHwDate());
        txtTitle.setText(response.getHwTitle());
        txtProblem.setText(response.getProblem());
    }

    @Override
    protected void setListenerOnWidgets() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside setListenerOnWidgets()");

        btnSend.setOnClickListener(this);

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "outside setListenerOnWidgets()");
    }

    /**
     * Return the data json string
     * @return
     */
    private String getDataJsonString(){
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("sender" , loginUser.getEmail());
            jsonObject.put("receiver" , studentDetail.getTeacherEmail());
            /*jsonObject.put("sender" , "yashwants@chromeinfotech.com");
            jsonObject.put("receiver" , "yashwants@chromeinfotech.com");*/
            jsonObject.put("message" , edtReportProblem.getText().toString());

            if (selectedCategory == MY_TUTOR) {
                jsonObject.put("studName" , player.getFirstname() + " " + player.getLastname());
                jsonObject.put("studSchool" , loginUser.getSchoolName());
                jsonObject.put("studTeacher" , loginUser.getFirstName() + " " +
                        loginUser.getLastName());
                jsonObject.put("tutorName" , studentDetail.getPlayerFName() + " " +
                        studentDetail.getPlayerLName());
                jsonObject.put("tutorSchool" , studentDetail.getSchoolName());
                jsonObject.put("tutorTeacher" , studentDetail.getTeacherName());
                jsonObject.put("fromStud", "1");
            }else if(selectedCategory == STUDENT_TUTORED){
                jsonObject.put("studName" , studentDetail.getPlayerFName() + " " + studentDetail
                        .getPlayerLName());
                jsonObject.put("studSchool" , studentDetail.getSchoolName());
                jsonObject.put("studTeacher" , studentDetail.getTeacherName());
                jsonObject.put("tutorName" , player.getFirstname() + " " + player.getLastname());
                jsonObject.put("tutorSchool" , loginUser.getSchoolName());
                jsonObject.put("tutorTeacher" , loginUser.getFirstName() + " " + loginUser.getLastName());
                jsonObject.put("fromStud", "0");
            }

            if(response != null) {
                jsonObject.put("hwDate", response.getHwDate());
                jsonObject.put("hwTitle", response.getHwTitle());
                jsonObject.put("question", response.getProblem());
            }else{
                jsonObject.put("hwDate", "");
                jsonObject.put("hwTitle", "");
                jsonObject.put("question", "");
            }
            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * Send email
     */
    private void sendEmail(){
        String message = edtReportProblem.getText().toString();
        if(message.length() > 0) {
            if (CommonUtils.isInternetConnectionAvailable(this)) {
                SendEmailToOtherTeacherParam param = new SendEmailToOtherTeacherParam();
                param.setAction("sendEmailToOtherTeacher");
                param.setData(this.getDataJsonString());
                //Log.e(TAG , "data json " + this.getDataJsonString());
                new MyAsyckTask(ServerOperation.createPostRequestForSendEmailToOtherTeacher(param)
                        , null, ServerOperationUtil.SEND_EMAIL_TO_OTHER_TEACHER_REQUEST, this,
                        this, ServerOperationUtil.SIMPLE_DIALOG, true,
                        getString(R.string.please_wait_dialog_msg))
                        .execute();
            } else {
                CommonUtils.showInternetDialog(this);
            }
        }else {
            MathFriendzyHelper.showWarningDialog(this , lblPleaseEnterMessageBeforeSending);
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnSend:
                this.sendEmail();
                break;
        }
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
        if(requestCode == ServerOperationUtil.GET_HOMEWORK_DETAIL_BY_CHAT_ID_REQUEST){
            GetHomeworkDetailsForChatRequestIdResponse response =
                    (GetHomeworkDetailsForChatRequestIdResponse) httpResponseBase;
            if(response.getResult().equalsIgnoreCase("success")){
                this.response = response;
                this.setHomeworkData(response);
            }
        }else if(requestCode == ServerOperationUtil.SEND_EMAIL_TO_OTHER_TEACHER_REQUEST){
            finish();
        }
    }
}
