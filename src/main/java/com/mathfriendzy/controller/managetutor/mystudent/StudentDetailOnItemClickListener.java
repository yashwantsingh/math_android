package com.mathfriendzy.controller.managetutor.mystudent;

/**
 * Created by root on 28/4/15.
 */
public interface StudentDetailOnItemClickListener {
    void clickOnView(int position);
    void clickOnExclateView(int position);
}
