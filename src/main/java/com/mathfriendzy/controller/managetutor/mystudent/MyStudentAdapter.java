package com.mathfriendzy.controller.managetutor.mystudent;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.classes.ClassWithName;

import java.util.ArrayList;

/**
 * Created by root on 21/4/15.
 */
public class MyStudentAdapter extends BaseAdapter{

    public ArrayList<UserPlayerDto> students = null;
    public ArrayList<UserPlayerDto> filterStudentList = new ArrayList<>();
    private LayoutInflater mInflator = null;
    private ViewHolder vHolder = null;
    private Context context = null;
    private MyStudentsOnItemClickListener callback = null;

    public MyStudentAdapter(Context context , ArrayList<UserPlayerDto> students ,
                            MyStudentsOnItemClickListener callback){
        this.callback = callback;
        this.context = context;
        mInflator = LayoutInflater.from(context);
        this.students = students;
        this.filterStudentList = students;
    }

    public void addRecord(ArrayList<UserPlayerDto> students){
        this.students.addAll(students);
    }

    @Override
    public int getCount() {
        return filterStudentList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        if(view == null){
            vHolder = new ViewHolder();
            view = mInflator.inflate(R.layout.manage_tutor_my_students_list_layout , null);
            vHolder.rlNameLayout = (RelativeLayout) view.findViewById(R.id.rlNameLayout);
            vHolder.txtStudentName = (TextView) view.findViewById(R.id.txtStudentName);
            vHolder.txtTutor = (TextView) view.findViewById(R.id.txtTutor);
            vHolder.txtOnline = (TextView) view.findViewById(R.id.txtOnline);
            vHolder.txtView = (TextView) view.findViewById(R.id.txtView);
            vHolder.txtEdit = (TextView) view.findViewById(R.id.txtEdit);
            vHolder.viewLayout = (RelativeLayout) view.findViewById(R.id.viewLayout);
            vHolder.editLayout = (RelativeLayout) view.findViewById(R.id.editLayout);
            vHolder.txtAverageRating = (TextView) view.findViewById(R.id.txtAverageRating);
            view.setTag(vHolder);
        }else{
            vHolder = (ViewHolder) view.getTag();
        }
        vHolder.txtStudentName.setText(filterStudentList.get(position).getFirstname()
                + " " + filterStudentList.get(position).getLastname());
        if(this.isTutor(filterStudentList.get(position))){
            vHolder.txtTutor.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
        }else{
            vHolder.txtTutor.setBackgroundResource(R.drawable.mf_check_box_ipad);
        }
        this.setRating(vHolder ,filterStudentList.get(position));
        this.setOnLineStatus(vHolder , filterStudentList.get(position).getInActive());
        vHolder.txtTutor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*filterStudentList.get(position).setStateChange
                        (!filterStudentList.get(position).isStateChange());
                if(isTutor(filterStudentList.get(position))){
                    filterStudentList.get(position).setIsTutor("0");
                }else{
                    filterStudentList.get(position).setIsTutor("1");
                }
                MyStudentAdapter.this.notifyDataSetChanged();*/
                callback.onCheckTutor(position);
            }
        });

        vHolder.viewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onViewClick(position);
            }
        });

        vHolder.txtEdit.setText(MathFriendzyHelper.
                getTimeSpentTextInHHMM(filterStudentList.get(position).getTutoringTime()));

        /*vHolder.txtStudentName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onViewClick(position);
            }
        });*/

        vHolder.rlNameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onViewClick(position);
            }
        });
        /*vHolder.editLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onEditClick(position);
            }
        });*/
        return view;
    }

    private void setRating(ViewHolder viewHolder , UserPlayerDto playerDto){
        try{
            if(playerDto.getRating() == 0){
                viewHolder.txtAverageRating.setText("");
            }else{
                viewHolder.txtAverageRating.setText(playerDto.getRating() + "");
            }

        }catch (Exception e){
            e.printStackTrace();
            viewHolder.txtAverageRating.setText("");
        }
    }

    private class ViewHolder{
        private RelativeLayout rlNameLayout;
        private TextView txtStudentName;
        private TextView txtTutor;
        private TextView txtOnline;
        private TextView txtView;
        private TextView txtEdit;
        private RelativeLayout viewLayout;
        private RelativeLayout editLayout;
        private TextView txtAverageRating;
    }

    private boolean isTutor(UserPlayerDto student){
        try {
            if (student.getIsTutor().equals("1"))
            /*if (student.getSubjectList() != null
                    && student.getSubjectList().size() > 0)*/
                return true;
            return false;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /*public void filterStudent(String selectedGrade){
        this.filterStudentList.clear();
        if(selectedGrade.equalsIgnoreCase(MathFriendzyHelper.ALL)){
           for(int i = 0 ; i < students.size() ; i ++ ) {
                this.filterStudentList.add(students.get(i));
            }
        }else {
            for (int i = 0; i < students.size(); i++) {
                if(selectedGrade.equalsIgnoreCase(students.get(i).getGrade())){
                    filterStudentList.add(students.get(i));
                }
            }
        }
        this.notifyDataSetChanged();
    }*/

    private void setOnLineStatus(ViewHolder viewHolder ,int inActive){
        if(inActive == MathFriendzyHelper.YES){
            viewHolder.txtOnline.setBackgroundResource(R.drawable.red_circle_icon);
        }else{
            viewHolder.txtOnline.setBackgroundResource(R.drawable.offline_icon);
        }
    }

    /*public void filterListBasedOnClassId(ArrayList<ClassWithName> selectedClassList
            , int selectedGrade){
        if(selectedGrade == 0){
            this.filterStudent(MathFriendzyHelper.ALL);
        }else{
            if(selectedClassList == null){
                this.filterStudent(selectedGrade + "");
            }else{
                this.filterStudentList.clear();
                for (int i = 0; i < this.students.size(); i++) {
                    if (this.ifStudentExistForSelectedClass(selectedClassList
                            , this.students.get(i).getClassId())) {
                        filterStudentList.add(students.get(i));
                    }
                }
                this.notifyDataSetChanged();
            }
        }
    }*/

    private boolean ifStudentExistForSelectedClass(ArrayList<ClassWithName> selectedClassList , int classId){
        if(selectedClassList != null && selectedClassList.size() > 0){
            for(int i = 0 ; i < selectedClassList.size() ; i ++ ){
                if(selectedClassList.get(i).getClassId() == classId){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Check/Uncheck the tutor
     * @param position
     */
    public void onTutorCheck(int position , boolean isMakeTutor ,
                             UserPlayerDto updatesSelectedStudent){
        /*filterStudentList.get(position).setStateChange
                (!filterStudentList.get(position).isStateChange());*/
        /*if(isTutor(filterStudentList.get(position))){
            filterStudentList.get(position).setIsTutor("0");
        }else{
            filterStudentList.get(position).setIsTutor("1");
        }*/
        filterStudentList.set(position , updatesSelectedStudent);

        if(this.isStatusChange(position)){
            filterStudentList.get(position).setStateChange(true);
        }else{
            filterStudentList.get(position).setStateChange(false);
        }

        if(isMakeTutor){
            filterStudentList.get(position).setIsTutor("1");
        }else{
            filterStudentList.get(position).setIsTutor("0");
        }
        MyStudentAdapter.this.notifyDataSetChanged();
    }

    private boolean isStatusChange(int position){
        ArrayList<String> sourceList = null;
        if(MathFriendzyHelper.isEmpty(filterStudentList.get(position).getSubjects())){
            sourceList = new ArrayList<String>();//if subject list is blank then create blank list
        }else {
            sourceList = MathFriendzyHelper
                    .getCommaSepratedOptionInArrayList(filterStudentList.get(position).getSubjects(), ",");
        }
        ArrayList<String> destinationList = filterStudentList.get(position).getSubjectList();
        return !MathFriendzyHelper.isEqualLists(sourceList , destinationList);
    }

    /**
     * Return the item at the given index position
     * @param position
     * @return
     */
    public UserPlayerDto getItemAtIndex(int position){
        try{
            return this.filterStudentList.get(position);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
