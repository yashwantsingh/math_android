package com.mathfriendzy.controller.managetutor.settutor;

import java.io.Serializable;

/**
 * Created by root on 29/6/16.
 */
public class ClassSubjects implements Serializable{

    private int subjectId;
    private String classSubjectName;
    private boolean isSelected;

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public String getClassSubjectName() {
        return classSubjectName;
    }

    public void setClassSubjectName(String classSubjectName) {
        this.classSubjectName = classSubjectName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
}
