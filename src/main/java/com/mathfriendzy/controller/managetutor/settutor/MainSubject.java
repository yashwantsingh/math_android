package com.mathfriendzy.controller.managetutor.settutor;

import com.mathfriendzy.serveroperation.HttpResponseBase;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by root on 29/6/16.
 */
public class MainSubject extends HttpResponseBase implements Serializable , Cloneable{

    private String result;
    private int subjectId;
    private String subjectName;
    private boolean isSelected;
    private ArrayList<ClassSubjects> classSubjects;
    private ArrayList<MainSubject> mainSubjects;

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public ArrayList<ClassSubjects> getClassSubjects() {
        return classSubjects;
    }

    public void setClassSubjects(ArrayList<ClassSubjects> classSubjects) {
        this.classSubjects = classSubjects;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public ArrayList<MainSubject> getMainSubjects() {
        return mainSubjects;
    }

    public void setMainSubjects(ArrayList<MainSubject> mainSubjects) {
        this.mainSubjects = mainSubjects;
    }

    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
