package com.mathfriendzy.controller.managetutor.settutor;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;

import java.util.ArrayList;

public class ActSetStudentTutor extends ActBase {

    public ExpandableListView lstSubjectList = null;
    private ArrayList<MainSubject> subjects = null;
    private SubjectAdapter adapter = null;
    private int selectedPosition = 0;
    private MainSubject mainSubject = null;
    private UserPlayerDto selectedStudent = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_set_student_tutor);

        this.getIntentValues();
        this.init();
        this.setWidgetsReferences();
        this.getSubjectsFromServer();
    }

    private void getIntentValues() {
        selectedPosition = this.getIntent().getIntExtra("selectedPosition" , 0);
        this.mainSubject = (MainSubject) this.getIntent().getSerializableExtra("mainSubjectList");
        try{
            selectedStudent = (UserPlayerDto) this.getIntent().getSerializableExtra("selectedStudent");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void init() {
    }

    /**
     * Get class subject from server
     */
    private void getSubjectsFromServer() {
        if(this.mainSubject != null){
            this.setAdapter(this.mainSubject.getMainSubjects());
            return ;
        }

        if(CommonUtils.isInternetConnectionAvailable(this)) {
            GetSubjectParam param = new GetSubjectParam();
            param.setAction("getSubjects");
            new MyAsyckTask(ServerOperation.createPostRequestForGetSubjectDetail(param)
                    , null, ServerOperationUtil.GET_SUBJECTS_FROM_SERVER_REQUEST, this,
                    this, ServerOperationUtil.SIMPLE_DIALOG, true,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        }else{
            CommonUtils.showInternetDialog(this);
        }
    }

    @Override
    protected void setWidgetsReferences() {
        lstSubjectList = (ExpandableListView) findViewById(R.id.lstSubjectList);
    }

    @Override
    protected void setListenerOnWidgets() {

    }

    @Override
    protected void setTextFromTranslation() {

    }

    /**
     * Set the Subject adapter
     */
    private void setAdapter(ArrayList<MainSubject> subjects){
        this.setSelectedItemForAlreadyTutor(subjects);
        adapter = new SubjectAdapter(this , subjects , new SubjectAdapterCallback() {
            @Override
            public void onGroupClick(int position, int tag) {
                switch(tag){
                    case SubjectAdapterCallback.SUBJECT_BUTTON_CLICK:
                        if(lstSubjectList.isGroupExpanded(position)){
                            lstSubjectList.collapseGroup(position);
                        }else {
                            lstSubjectList.expandGroup(position);
                        }
                        break;
                }
            }
        });
        lstSubjectList.setAdapter(adapter);
    }

    /**
     * Set the selected item for already tutor
     * @param mainSubjects
     */
    private void setSelectedItemForAlreadyTutor(ArrayList<MainSubject> mainSubjects){
        ArrayList<String> selectedStudentSubject = this.selectedStudent.getSubjectList();
        if(selectedStudentSubject != null && selectedStudentSubject.size() > 0){
            for(int i = 0 ; i < mainSubjects.size() ; i ++ ){
                MainSubject mainSubject = mainSubjects.get(i);
                ArrayList<ClassSubjects> classSubjects = mainSubject.getClassSubjects();
                if(classSubjects != null && classSubjects.size() > 0) {
                    for (int j = 0; j < classSubjects.size() ; j ++ ){
                        if(selectedStudentSubject.contains(
                                classSubjects.get(j).getSubjectId() + "")){
                            classSubjects.get(j).setSelected(true);
                        }else{
                            classSubjects.get(j).setSelected(false);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
        if(requestCode == ServerOperationUtil.GET_SUBJECTS_FROM_SERVER_REQUEST){
            this.mainSubject = (MainSubject) httpResponseBase;
            if(mainSubject.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)){
                this.setAdapter(mainSubject.getMainSubjects());
            }
        }
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("selectedPosition" , selectedPosition);
        //don't move this line below because selected student subject updated in this method this.isAtLeastOneClassSubjectSelected()
        intent.putExtra("isAnyClassSubjectSelected" ,this.isAtLeastOneClassSubjectSelected());
        intent.putExtra("mainSubjectList" , this.mainSubject);
        intent.putExtra("updatesSelectedStudent" , this.selectedStudent);
        setResult(RESULT_OK , intent);
        finish();
    }

    private boolean isAtLeastOneClassSubjectSelected(){
        boolean isAtLeastOneSelected = false;
        try {
            if (adapter != null) {
                ArrayList<String> selectedSubjects = new ArrayList<String>();
                ArrayList<MainSubject> subjects = adapter.getSubjects();
                for(int i = 0 ; i < subjects.size() ; i ++ ){
                    ArrayList<ClassSubjects> classSubjects = subjects.get(i).getClassSubjects();
                    for(int j = 0 ; j < classSubjects.size() ; j ++ ){
                        if(classSubjects.get(j).isSelected()){
                            isAtLeastOneSelected = true;
                            selectedSubjects.add(classSubjects.get(j).getSubjectId() + "");
                        }
                    }
                }
                this.selectedStudent.setSubjectList(selectedSubjects);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return isAtLeastOneSelected;
    }
}
