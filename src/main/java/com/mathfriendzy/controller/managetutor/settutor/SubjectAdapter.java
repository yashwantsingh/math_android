package com.mathfriendzy.controller.managetutor.settutor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.customview.CustomGridViewForScrollView;

import java.util.ArrayList;

/**
 * Created by root on 29/6/16.
 */
public class SubjectAdapter extends BaseExpandableListAdapter{

    private Context context = null;
    private ArrayList<MainSubject> subjects = null;
    private LayoutInflater mInflater = null;
    private ViewHolder viewHolder = null;
    private SubjectAdapterCallback callback = null;
    private final int MATH = 1 , BUSINESS = 2 , SCIENCE = 3 , LANGUAGE = 4 , GENERAL_ED = 5;
    public SubjectAdapter(Context context , ArrayList<MainSubject> subjects , SubjectAdapterCallback callback){
        this.context = context;
        this.subjects = subjects;
        mInflater = LayoutInflater.from(context);
        this.callback = callback;
    }

    @Override
    public int getGroupCount() {
        return this.subjects.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.subjects.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View view, ViewGroup parent) {
        if(view == null){
            view = mInflater.inflate(R.layout.layout_manage_tutor_make_tutor_item , null);
            viewHolder = new ViewHolder();
            viewHolder.btnSubjectButton = (Button) view.findViewById(R.id.btnSubjectButton);
            viewHolder.grdSubjectView = (CustomGridViewForScrollView) view.findViewById(R.id.grdSubjectView);
            //viewHolder.rlGridLayout = (RelativeLayout) view.findViewById(R.id.rlGridLayout);
            view.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.btnSubjectButton.setText(subjects.get(groupPosition).getSubjectName());
        this.setBackgroundImages(viewHolder , subjects.get(groupPosition));
        viewHolder.btnSubjectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onGroupClick(groupPosition , SubjectAdapterCallback.SUBJECT_BUTTON_CLICK);
            }
        });
        return view;
    }

    /**
     * Set the background image
     * @param viewHolder
     * @param subject
     */
    private void setBackgroundImages(ViewHolder viewHolder , MainSubject subject){
         if(subject.getSubjectId() == MATH){
             viewHolder.btnSubjectButton.setBackgroundResource(R.drawable.new_friendzy_blue_challenge);
         }else if(subject.getSubjectId() == BUSINESS){
             viewHolder.btnSubjectButton.setBackgroundResource(R.drawable.new_friendzy_yellow_challenge);
         }else if(subject.getSubjectId() == SCIENCE){
             viewHolder.btnSubjectButton.setBackgroundResource(R.drawable.new_friendzy_orange_challenge);
         }else if(subject.getSubjectId() == LANGUAGE){
             viewHolder.btnSubjectButton.setBackgroundResource(R.drawable.new_friendzy_green_challenge);
         }else if(subject.getSubjectId() == GENERAL_ED){
             viewHolder.btnSubjectButton.setBackgroundResource(R.drawable.view_detail);
         }
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View view, ViewGroup parent) {
        if(view == null){
            view = mInflater.inflate(R.layout.layout_manage_tutor_make_tutor_item , null);
            viewHolder = new ViewHolder();
            viewHolder.btnSubjectButton = (Button) view.findViewById(R.id.btnSubjectButton);
            viewHolder.grdSubjectView = (CustomGridViewForScrollView) view.findViewById(R.id.grdSubjectView);
            //viewHolder.rlGridLayout = (RelativeLayout) view.findViewById(R.id.rlGridLayout);
            view.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.btnSubjectButton.setVisibility(View.GONE);
        viewHolder.grdSubjectView.setVisibility(View.VISIBLE);
        this.setClassSubjects(viewHolder , subjects.get(groupPosition));
        return view;
    }

    /**
     * Set the subjects according to the main subject
     * @param viewHolder
     * @param mainSubject
     */
    private void setClassSubjects(ViewHolder viewHolder , MainSubject mainSubject){
        SubjectClassAdapter adapter = new SubjectClassAdapter(context ,mainSubject.getClassSubjects());
        viewHolder.grdSubjectView.setAdapter(adapter);
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    private class ViewHolder{
        private Button btnSubjectButton;
        private CustomGridViewForScrollView grdSubjectView;
        //private RelativeLayout rlGridLayout = null;
    }

    public static class SubjectClassAdapter extends BaseAdapter{

        private Context context = null;
        private ArrayList<ClassSubjects> classSubjects = null;
        private LayoutInflater mInflater = null;
        private ClassSubjectViewHolder viewHolder = null;

        public SubjectClassAdapter(Context context , ArrayList<ClassSubjects> classSubjects){
            this.context = context;
            this.classSubjects = classSubjects;
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return this.classSubjects.size();
        }

        @Override
        public Object getItem(int position) {
            return this.classSubjects.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            if(view == null){
                viewHolder = new ClassSubjectViewHolder();
                view = mInflater.inflate(R.layout.layout_manage_tutor_make_tutor_class_subject_item , null);
                //viewHolder.chkSubjectName = (CheckBox) view.findViewById(R.id.chkSubjectName);
                viewHolder.imgCheck = (ImageView) view.findViewById(R.id.imgCheck);
                viewHolder.txtSubjectName = (TextView) view.findViewById(R.id.txtSubjectName);
                view.setTag(viewHolder);
            }else{
                viewHolder = (ClassSubjectViewHolder) view.getTag();
            }
            //viewHolder.chkSubjectName.setText(this.classSubjects.get(position).getClassSubjectName());
            viewHolder.txtSubjectName.setText(this.classSubjects.get(position).getClassSubjectName());
            this.setCheckedImage(viewHolder , this.classSubjects.get(position));
            viewHolder.imgCheck.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    classSubjects.get(position).setSelected
                            (!classSubjects.get(position).isSelected());
                    SubjectClassAdapter.this.notifyDataSetChanged();
                }
            });
            return view;
        }

        private void setCheckedImage(ClassSubjectViewHolder viewHolder , ClassSubjects classSubjects){
            if(classSubjects.isSelected()){
                viewHolder.imgCheck.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
            }else{
                viewHolder.imgCheck.setBackgroundResource(R.drawable.mf_check_box_ipad);
            }
        }
        private class ClassSubjectViewHolder{
            //private CheckBox chkSubjectName;
            private ImageView imgCheck;
            private TextView txtSubjectName;
        }
    }

    public ArrayList<MainSubject> getSubjects(){
        return subjects;
    }
}
