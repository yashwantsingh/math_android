package com.mathfriendzy.controller.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.player.CreateTempPlayerActivity;
import com.mathfriendzy.controller.player.PlayersActivity;
import com.mathfriendzy.controller.registration.RegistrationStep1;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.chooseAvtar.AvtarServerOperation;
import com.mathfriendzy.model.chooseAvtar.ChooseAvtarOpration;
import com.mathfriendzy.model.language.Language;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenteServerOperation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.MathResultTransferObj;
import com.mathfriendzy.model.learningcenter.PlayerEquationLevelObj;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.learningcenter.PurchaseItemObj;
import com.mathfriendzy.model.login.Login;
import com.mathfriendzy.model.login.PurchasedAvtarDto;
import com.mathfriendzy.model.multifriendzy.MultiFriendzyServerOperation;
import com.mathfriendzy.model.player.temp.TempPlayer;
import com.mathfriendzy.model.player.temp.TempPlayerOperation;
import com.mathfriendzy.model.registration.Register;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.notification.AddUserWithAndroidDevice;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;

import java.util.ArrayList;

import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_ACTIVITY_FLAG;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_ID;

/**
 * This class for the user login
 * Here User login and get data from the server and stored it into database
 * @author Yashwant Singh
 *
 */
public class LoginActivity extends ActBaseClass implements OnClickListener {
    private TextView mfTitleHomeScreen = null;
    private TextView txtLogin = null;
    private TextView lblRegEmail = null;
    private TextView lblRegPassword = null;
    private TextView lblForgotPassword = null;

    private EditText edtEMail = null;
    private EditText edtPass = null;
    private Button btnTitleGo = null;
    private Button btnTitleBack = null;

    private ProgressDialog progressDialog = null;
    private String callingActivity = null;
    private String tempPlayerUserName = null;

    private String TAG = this.getClass().getSimpleName();

    private boolean isOpenForInAppPurchase = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (LOGIN_ACTIVITY_FLAG)
            Log.e(TAG, "inside onCreate()");

        callingActivity = this.getIntent().getStringExtra("callingActivity");

        this.getIntentValues();
        this.setWidgetsReferences();
        this.setWidgetsTextValues();
        this.setListemnerOnWidgets();

        if (LOGIN_ACTIVITY_FLAG)
            Log.e(TAG, "outside onCreate()");
    }

    private void getIntentValues(){
        isOpenForInAppPurchase = this.getIntent().getBooleanExtra("isOpenForInAppPurchase" , false);
    }

    private void setWidgetsReferences() {
        if (LOGIN_ACTIVITY_FLAG)
            Log.e(TAG, "inside setWidgetsReferences()");

        mfTitleHomeScreen = (TextView) findViewById(R.id.mfTitleHomeScreen);
        txtLogin = (TextView) findViewById(R.id.alertBtnLogin);
        lblRegEmail = (TextView) findViewById(R.id.lblRegEmail);
        lblRegPassword = (TextView) findViewById(R.id.lblRegPassword);
        lblForgotPassword = (TextView) findViewById(R.id.lblForgotPassword);
        edtEMail = (EditText) findViewById(R.id.edtEMail);
        edtPass = (EditText) findViewById(R.id.edtPass);
        btnTitleGo = (Button) findViewById(R.id.btnTitleGo);
        btnTitleBack = (Button) findViewById(R.id.btnTitleBack);

        //set Done button to keyboard
        CommonUtils.setDoneButtonToEditText(edtEMail);
        CommonUtils.setDoneButtonToEditText(edtPass);
        //end changes

        if (LOGIN_ACTIVITY_FLAG)
            Log.e(TAG, "outside setWidgetsReferences()");
    }

    private void setWidgetsTextValues() {
        if (LOGIN_ACTIVITY_FLAG)
            Log.e(TAG, "inside setWidgetsTextValues()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        mfTitleHomeScreen.setText(transeletion.getTranselationTextByTextIdentifier("mfTitleHomeScreen"));
        txtLogin.setText(transeletion.getTranselationTextByTextIdentifier("alertBtnLogin"));
        lblRegEmail.setText(transeletion.getTranselationTextByTextIdentifier("lblRegEmail") + "/" +
                transeletion.getTranselationTextByTextIdentifier("lblUserName"));
        lblRegPassword.setText(transeletion.getTranselationTextByTextIdentifier("lblRegPassword"));
        lblForgotPassword.setText(transeletion.getTranselationTextByTextIdentifier("lblForgotPassword"));
        btnTitleGo.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGo"));
        btnTitleBack.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBack"));
        transeletion.closeConnection();

        if (LOGIN_ACTIVITY_FLAG)
            Log.e(TAG, "outside setWidgetsTextValues()");
    }

    private void setListemnerOnWidgets() {
        if (LOGIN_ACTIVITY_FLAG)
            Log.e(TAG, "inside setListemnerOnWidgets()");

        btnTitleGo.setOnClickListener(this);
        btnTitleBack.setOnClickListener(this);
        lblForgotPassword.setOnClickListener(this);

        if (LOGIN_ACTIVITY_FLAG)
            Log.e(TAG, "outside setListemnerOnWidgets()");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnTitleGo:
                this.checkEmptyValidation();
                break;
            case R.id.btnTitleBack:
                if (callingActivity.equals("CreateTempPlayerActivity")) {
                    Intent intentMain = new Intent(this, CreateTempPlayerActivity.class);
                    startActivity(intentMain);
                } else if (callingActivity.equals("PlayersActivity")) {
                    Intent intentPlayer = new Intent(this, PlayersActivity.class);
                    startActivity(intentPlayer);
                } else if (callingActivity.equals("RegistrationStep1")) {
                    Intent intentPlayer = new Intent(this, RegistrationStep1.class);
                    startActivity(intentPlayer);
                } else {
                    this.openMainScreen();
                }
                break;
            case R.id.lblForgotPassword:
                DialogGenerator dg = new DialogGenerator(this);
                dg.generateDialogForgatePassword();
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (callingActivity.equals("CreateTempPlayerActivity")) {
                Intent intentMain = new Intent(this, CreateTempPlayerActivity.class);
                startActivity(intentMain);
            } else if (callingActivity.equals("PlayersActivity")) {
                Intent intentPlayer = new Intent(this, PlayersActivity.class);
                startActivity(intentPlayer);
            } else if (callingActivity.equals("RegistrationStep1")) {
                Intent intentPlayer = new Intent(this, RegistrationStep1.class);
                startActivity(intentPlayer);
            } else {
                /*Intent intentMain = new Intent(this, MainActivity.class);
                startActivity(intentMain);*/
                //this.openMainScreen();
                this.finish();
                //openMainScreenAfterLogin();
            }

            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * This method check empty validation for username or email and password
     */
    private void checkEmptyValidation() {
        if (LOGIN_ACTIVITY_FLAG)
            Log.e(TAG, "inside checkEmptyValidation()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();

        if (edtEMail.getText().toString().equals("") || edtPass.getText().toString().equals("")) {
            DialogGenerator dg = new DialogGenerator(this);
            dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgPleaseEnterEmailUsernamePassword"));
        } else {
            this.checkUsernameOrEmail();
        }
        transeletion.closeConnection();

        if (LOGIN_ACTIVITY_FLAG)
            Log.e(TAG, "outside checkEmptyValidation()");
    }

    /**
     * This method check user  wants login by username or by email
     */
    private void checkUsernameOrEmail() {
        if (LOGIN_ACTIVITY_FLAG)
            Log.e(TAG, "inside checkUsernameOrEmail()");

        DialogGenerator dg = new DialogGenerator(this);
        Translation transeletion = new Translation(this);
        transeletion.openConnection();

        String emailOrUserName = edtEMail.getText().toString();
        if (emailOrUserName.contains("@")) {
            if (CommonUtils.isEmailValid(emailOrUserName)) {
                if (CommonUtils.isPasswordValid(edtPass.getText().toString())) {
                    // changes for Internet connection
                    if (CommonUtils.isInternetConnectionAvailable(this)) {
                        new LoginAsynkTaskByEmail(edtEMail.getText().toString(), edtPass.getText().toString()).execute(null, null, null);
                    } else {
                        dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
                    }
                } else {
                    dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgPasswordInvalid"));
                }
            } else {
                dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgEmailIncorrectFormat"));
            }
        } else {
            if (CommonUtils.isInternetConnectionAvailable(this)) {
                new LoginAsyncTaskByUserName(edtEMail.getText().toString(), edtPass.getText().toString()).execute(null, null, null);
            } else {
                dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
            }

        }

        transeletion.closeConnection();

        if (LOGIN_ACTIVITY_FLAG)
            Log.e(TAG, "outside checkUsernameOrEmail()");
    }

    /**
     * If tempplayer exist and user login then the tempplayer information added to the login user
     *
     * @param tempPlayer
     * @param parentUserId
     * @return
     */
    private ArrayList<UserPlayerDto> getTempPlayerData(ArrayList<TempPlayer> tempPlayer, String parentUserId) {
        if (LOGIN_ACTIVITY_FLAG)
            Log.e(TAG, "inside getTempPlayerData()");

        ArrayList<UserPlayerDto> userPlayer = new ArrayList<UserPlayerDto>();
        UserPlayerDto userPlayerDto = null;


        PlayerTotalPointsObj playerObj = null;
        LearningCenterimpl learningCenterimpl = new LearningCenterimpl(this);
        learningCenterimpl.openConn();
        playerObj = learningCenterimpl.getDataFromPlayerTotalPoints(tempPlayer.get(0).getPlayerId() + "");
        learningCenterimpl.closeConn();

        //Log.e(TAG, "total pints " + playerObj.getTotalPoints() + " coins " + playerObj.getCoins());

        for (int i = 0; i < tempPlayer.size(); i++) {
            userPlayerDto = new UserPlayerDto();
            userPlayerDto.setCity(tempPlayer.get(i).getCity());
            //userPlayerDto.setCoin(String.valueOf(tempPlayer.get(i).getCoins()));
            userPlayerDto.setCoin(playerObj.getCoins() + "");
            //userPlayerDto.setCompletelavel(String.valueOf(tempPlayer.get(i).getCompeteLevel()));
            userPlayerDto.setCompletelavel(playerObj.getCompleteLevel() + "");
            userPlayerDto.setFirstname(tempPlayer.get(i).getFirstName());
            userPlayerDto.setGrade(String.valueOf(tempPlayer.get(i).getGrade()));
            userPlayerDto.setImageName(tempPlayer.get(i).getProfileImageName());
            userPlayerDto.setIndexOfAppearance("-1");
            userPlayerDto.setLastname(tempPlayer.get(i).getLastName());
            userPlayerDto.setParentUserId(parentUserId);//id from parent
            userPlayerDto.setPlayerid(String.valueOf(tempPlayer.get(i).getPlayerId()));
            //userPlayerDto.setPoints(String.valueOf(tempPlayer.get(i).getPoints()));
            userPlayerDto.setPoints(playerObj.getTotalPoints() + "");//changes
            userPlayerDto.setSchoolId(String.valueOf(tempPlayer.get(i).getSchoolId()));
            userPlayerDto.setSchoolName(tempPlayer.get(i).getSchoolName());
            userPlayerDto.setStateName(tempPlayer.get(i).getState());
            userPlayerDto.setTeacheLastName(tempPlayer.get(i).getTeacherLastName());
            userPlayerDto.setTeacherFirstName(tempPlayer.get(i).getTeacherFirstName());
            userPlayerDto.setTeacherUserId(String.valueOf(tempPlayer.get(i).getTeacherUserId()));
            userPlayerDto.setUsername(tempPlayer.get(i).getUserName());

            userPlayer.add(userPlayerDto);
        }

        if (LOGIN_ACTIVITY_FLAG)
            Log.e(TAG, "outside getTempPlayerData()");

        return userPlayer;
    }

    /**
     * Return the xml format of the temp player data from tha database
     *
     * @param userPlayerObj
     * @return
     */
    private String getXmlPlayer(ArrayList<UserPlayerDto> userPlayerObj) {
        if (LOGIN_ACTIVITY_FLAG)
            Log.e(TAG, "inside getXmlPlayer()");

        StringBuilder xml = new StringBuilder("");

        for (int i = 0; i < userPlayerObj.size(); i++) {
            xml.append("<player>" +
                    "<playerId>" + userPlayerObj.get(i).getPlayerid() + "</playerId>" +
                    "<fName>" + userPlayerObj.get(i).getFirstname() + "</fName>" +
                    "<lName>" + userPlayerObj.get(i).getLastname() + "</lName>" +
                    "<grade>" + userPlayerObj.get(i).getGrade() + "</grade>" +
                    "<schoolId>" + userPlayerObj.get(i).getSchoolId() + "</schoolId>" +
                    "<teacherId>" + userPlayerObj.get(i).getTeacherUserId() + "</teacherId>" +
                    "<indexOfAppearance>-1</indexOfAppearance>" +
                    "<profileImageId>" + userPlayerObj.get(i).getImageName() + "</profileImageId>" +
                    "<coins>" + userPlayerObj.get(i).getCoin() + "</coins>" +
                    "<points>" + userPlayerObj.get(i).getPoints() + "</points>" +
                    "<userName>" + userPlayerObj.get(i).getUsername() + "</userName>" +
                    "<competeLevel>" + userPlayerObj.get(i).getCompletelavel() + "</competeLevel>" +
                    "</player>");
        }

        if (LOGIN_ACTIVITY_FLAG)
            Log.e(TAG, "outside getXmlPlayer()");

        return xml.toString();
    }


    /**
     * This method call after succesfull login for notification
     */
    private void addUserOnServerWithAndroidDevice() {
        UserRegistrationOperation userObj = new UserRegistrationOperation(this);
        new AddUserWithAndroidDevice(userObj.getUserId(), this).execute(null, null, null);
    }

    /**
     * This method get purchased avtar from server for login user
     */
    private void getPurchasedStatus() {
        UserRegistrationOperation userObj = new UserRegistrationOperation(this);
        new GetPurchasedAvtar(userObj.getUserId()).execute(null, null, null);
    }


    /**
     * This method call when user login success
     */
    private void callMethodAfterLoginSuccess() {

        DialogGenerator dg = new DialogGenerator(LoginActivity.this);
        Translation transeletion = new Translation(LoginActivity.this);
        transeletion.openConnection();

        SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
        SharedPreferences.Editor editor = sheredPreference.edit();
        editor.putBoolean(IS_LOGIN, true);
        editor.commit();

        if (CommonUtils.isInternetConnectionAvailable(LoginActivity.this)) {
            String userId = CommonUtils.getUserId(this);
            if (userId != null && userId.length() > 0) {
                new GetPurchsedItemDetailByUserId(userId, this).execute(null, null, null);
                //MathFriendzyHelper.getVideoInAppStatusSaveIntoSharedPreff(this , userId);
            }
        }

        //add user on server for notification
        addUserOnServerWithAndroidDevice();

        //changes for avtar purchase
        getPurchasedStatus();

        TempPlayerOperation tempObj = new TempPlayerOperation(LoginActivity.this);
        if (tempObj.isTemparyPlayerExist()) {
            if (tempObj.isTempPlayerDeleted()) {
                progressDialog.cancel();

                SharedPreferences sheredPreference1 = getSharedPreferences(IS_CHECKED_PREFF, 0);
                SharedPreferences.Editor editor1 = sheredPreference1.edit();
                editor1.clear();
                editor1.commit();

                //changes for selected single player (by deepak mail)
                UserPlayerOperation userObj = new UserPlayerOperation(LoginActivity.this);
                ArrayList<UserPlayerDto> userPlayerObj = userObj.getUserPlayerData();
                if (userPlayerObj.size() == 1) {
                    SharedPreferences sheredPreference2 = getSharedPreferences(IS_CHECKED_PREFF, 0);
                    SharedPreferences.Editor editor2 = sheredPreference2.edit();
                    editor2.putString(PLAYER_ID, userPlayerObj.get(0).getPlayerid());
                    //changes according to shilpi
                    editor2.putString("userId", userPlayerObj.get(0).getParentUserId());
                    //end changes
                    editor2.commit();
                }
                //end changes

				/*Intent intent = new Intent(LoginActivity.this,MainActivity.class);
				startActivity(intent);*/
                openMainScreenAfterLogin();
            } else {
                //changes for not adding temp player in user account when its already have player in his account
                UserPlayerOperation userPlayerObjOperation = new UserPlayerOperation(LoginActivity.this);
                ArrayList<UserPlayerDto> userPlayerObj = userPlayerObjOperation.getUserPlayerData();
                //end changes

                if (userPlayerObj.size() == 0) {
                    UserRegistrationOperation regObj = new UserRegistrationOperation(LoginActivity.this);
                    RegistereUserDto regUserObj = regObj.getUserData();

                    TempPlayerOperation tempPlayerObj = new TempPlayerOperation(LoginActivity.this);
                    ArrayList<TempPlayer> tempPlayer = tempPlayerObj.getTempPlayerData();

                    tempPlayerUserName = tempPlayer.get(0).getUserName();//changes for update score

                    TempPlayerOperation tempPlayerObj1 = new TempPlayerOperation(LoginActivity.this);
                    tempPlayerObj1.deleteFromTempPlayer();
                    tempPlayerObj1.closeConn();

                    UserPlayerOperation userObj = new UserPlayerOperation(LoginActivity.this);

                    if (userObj.isUserPlayersExist()) {
                        ArrayList<UserPlayerDto> userPlayer = getTempPlayerData(tempPlayer, regUserObj.getUserId());
                        regUserObj.setPlayers("<AllPlayers>" + getXmlPlayer(userPlayer) + "</AllPlayers>");
                    } else {
                        ArrayList<UserPlayerDto> userPlayer = getTempPlayerData(tempPlayer, regUserObj.getUserId());

                        UserPlayerOperation userObj1 = new UserPlayerOperation(LoginActivity.this);
                        ArrayList<UserPlayerDto> userPlayerObj1 = userObj1.getUserPlayerData();

                        for (int i = 0; i < userPlayer.size(); i++) {
                            userPlayerObj1.add(userPlayer.get(i));
                        }

                        regUserObj.setPlayers("<AllPlayers>" + getXmlPlayer(userPlayerObj1) + "</AllPlayers>");
                    }

                    //changes for Internet Connection
                    if (CommonUtils.isInternetConnectionAvailable(LoginActivity.this)) {
                        new UpdateAsynTask(regUserObj).execute(null, null, null);
                    } else {
                        dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
                    }
                } else {//changes for not adding temp player in user account when its already have player in his account

                    progressDialog.cancel();

                    TempPlayerOperation tempPlayerObj1 = new TempPlayerOperation(LoginActivity.this);
                    tempPlayerObj1.deleteFromTempPlayer();
                    tempPlayerObj1.closeConn();

					/*Delete Temp player records*/
                    LearningCenterimpl learnignCenter = new LearningCenterimpl(LoginActivity.this);
                    learnignCenter.openConn();
                    learnignCenter.deleteFromPlayerTotalPoints("0");
                    learnignCenter.deleteFromPlayerEruationLevel("0");
                    learnignCenter.deleteFromMathResult("0", "0");
                    learnignCenter.deleteFromLocalPlayerLevels("0", "0");
                    learnignCenter.deleteFromPurchaseItem("0");
                    learnignCenter.closeConn();

                    SharedPreferences sheredPreference1 = getSharedPreferences(IS_CHECKED_PREFF, 0);
                    SharedPreferences.Editor editor1 = sheredPreference1.edit();
                    editor1.clear();
                    editor1.commit();

                    //changes for selected single player (by deepak mail)
                    UserPlayerOperation userObj = new UserPlayerOperation(LoginActivity.this);
                    ArrayList<UserPlayerDto> userPlayerObj1 = userObj.getUserPlayerData();
                    if (userPlayerObj1.size() == 1) {
                        SharedPreferences sheredPreference2 = getSharedPreferences(IS_CHECKED_PREFF, 0);
                        SharedPreferences.Editor editor2 = sheredPreference2.edit();
                        editor2.putString(PLAYER_ID, userPlayerObj1.get(0).getPlayerid());
                        //changes according to shilpi
                        editor2.putString("userId", userPlayerObj1.get(0).getParentUserId());
                        //end changes
                        editor2.commit();
                    }
                    //end changes

					/*Intent intent = new Intent(LoginActivity.this,MainActivity.class);
					startActivity(intent);*/
                    openMainScreenAfterLogin();
                }
            }
        } else {//changes according to deepak mail
            tempObj.createTempPlayerTable();
            tempObj.closeConn();

            progressDialog.cancel();

            SharedPreferences sheredPreference1 = getSharedPreferences(IS_CHECKED_PREFF, 0);
            SharedPreferences.Editor editor1 = sheredPreference1.edit();
            editor1.clear();
            editor1.commit();

            //changes for selected single player (by deepak mail)
            UserPlayerOperation userObj = new UserPlayerOperation(LoginActivity.this);
            ArrayList<UserPlayerDto> userPlayerObj = userObj.getUserPlayerData();
            if (userPlayerObj.size() == 1) {
                SharedPreferences sheredPreference2 = getSharedPreferences(IS_CHECKED_PREFF, 0);
                SharedPreferences.Editor editor2 = sheredPreference2.edit();
                editor2.putString(PLAYER_ID, userPlayerObj.get(0).getPlayerid());
                //changes according to shilpi
                editor2.putString("userId", userPlayerObj.get(0).getParentUserId());
                //end changes
                editor2.commit();
            }
            //end changes

			/*Intent intent = new Intent(LoginActivity.this,MainActivity.class);
			startActivity(intent);*/
            openMainScreenAfterLogin();
        }
        transeletion.closeConnection();
    }

    /**
     * Asynctask for Login User by email
     *
     * @author Yashwant Singh
     */
    class LoginAsynkTaskByEmail extends AsyncTask<Void, Void, Void> {
        private String email = null;
        private String pass = null;
        private int resultValue = 0;

        LoginAsynkTaskByEmail(String email, String pass) {
            this.email = email;
            this.pass = pass;
        }

        @Override
        protected void onPreExecute() {
            if (LOGIN_ACTIVITY_FLAG)
                Log.e(TAG, "inside LoginAsynkTaskByEmail onPreExecute()");

            progressDialog = CommonUtils.getProgressDialog(LoginActivity.this);
            progressDialog.show();

            if (LOGIN_ACTIVITY_FLAG)
                Log.e(TAG, "outside LoginAsynkTaskByEmail onPreExecute()");

            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (LOGIN_ACTIVITY_FLAG)
                Log.e(TAG, "inside LoginAsynkTaskByEmail doInBackground()");

            Login loginobj = new Login(LoginActivity.this);
            resultValue = loginobj.getLoginDataByEmail(email, pass);

            if (LOGIN_ACTIVITY_FLAG)
                Log.e(TAG, "outside LoginAsynkTaskByEmail doInBackground()");
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            if (LOGIN_ACTIVITY_FLAG)
                Log.e(TAG, "inside LoginAsynkTaskByEmail onPostExecute()");

            DialogGenerator dg = new DialogGenerator(LoginActivity.this);
            Translation transeletion = new Translation(LoginActivity.this);
            transeletion.openConnection();


            if (resultValue == Login.SUCCESS) {
                callMethodAfterLoginSuccess();
            } else if (resultValue == Login.INVALID_EMAIL) {
                progressDialog.cancel();
                dg.generateWarningDialog(transeletion.
                        getTranselationTextByTextIdentifier("alertMsgEmailUsernameNotInSystem"));
            } else if (resultValue == Login.PRINCIPLE_OR_SCHOOL_DISTRICT_ACCOUNT) {
                progressDialog.cancel();
                dg.generateWarningDialog(transeletion.
                        getTranselationTextByTextIdentifier("lblThisAccountCantAccessApp"));

            } else if (resultValue == 0) {
                progressDialog.cancel();
                CommonUtils.showInternetDialog(LoginActivity.this);
            }
            transeletion.closeConnection();

            if (LOGIN_ACTIVITY_FLAG)
                Log.e(TAG, "outside LoginAsynkTaskByEmail onPostExecute()");

            super.onPostExecute(result);
        }
    }

    /**
     * AsyncTask fro user login by username
     *
     * @author Yashwant Singh
     */
    class LoginAsyncTaskByUserName extends AsyncTask<Void, Void, Void> {
        private String userName = null;
        private String pass = null;
        private int resultValue = 0;

        LoginAsyncTaskByUserName(String userName, String pass) {
            this.userName = userName;
            this.pass = pass;
        }

        @Override
        protected void onPreExecute() {
            if (LOGIN_ACTIVITY_FLAG)
                Log.e(TAG, "inside LoginAsyncTaskByUserName onPreExecute()");

            progressDialog = CommonUtils.getProgressDialog(LoginActivity.this);
            progressDialog.show();

            if (LOGIN_ACTIVITY_FLAG)
                Log.e(TAG, "outside LoginAsyncTaskByUserName onPreExecute()");

            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (LOGIN_ACTIVITY_FLAG)
                Log.e(TAG, "inside LoginAsyncTaskByUserName doInBackground()");

            Login loginobj = new Login(LoginActivity.this);
            resultValue = loginobj.getLoginDataByUserName(userName, pass);

            if (LOGIN_ACTIVITY_FLAG)
                Log.e(TAG, "outside LoginAsyncTaskByUserName doInBackground()");

            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            if (LOGIN_ACTIVITY_FLAG)
                Log.e(TAG, "inside LoginAsyncTaskByUserName onPostExecute()");

            //Log.e(TAG, "result value " + resultValue);
            DialogGenerator dg = new DialogGenerator(LoginActivity.this);
            Translation transeletion = new Translation(LoginActivity.this);
            transeletion.openConnection();

            if (resultValue == Login.SUCCESS) {
                callMethodAfterLoginSuccess();
            } else if (resultValue == Login.INVALID_EMAIL) {
                progressDialog.cancel();
                dg.generateWarningDialog(transeletion
                        .getTranselationTextByTextIdentifier("alertMsgEmailUsernameNotInSystem"));
            } else if (resultValue == Login.PRINCIPLE_OR_SCHOOL_DISTRICT_ACCOUNT) {
                progressDialog.cancel();
                dg.generateWarningDialog(transeletion.
                        getTranselationTextByTextIdentifier("lblThisAccountCantAccessApp"));
            } else if (resultValue == 0) {
                progressDialog.cancel();
                CommonUtils.showInternetDialog(LoginActivity.this);
            }
            transeletion.closeConnection();

            if (LOGIN_ACTIVITY_FLAG)
                Log.e(TAG, "inside LoginAsyncTaskByUserName onPostExecute()");

            super.onPostExecute(result);
        }
    }


    /**
     * Update user information on server
     *
     * @author Yashwant Singh
     */
    class UpdateAsynTask extends AsyncTask<Void, Void, Void> {
        private RegistereUserDto regObj = null;
        private int registreationResult = 0;

        UpdateAsynTask(RegistereUserDto regObj) {
            this.regObj = regObj;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (LOGIN_ACTIVITY_FLAG)
                Log.e(TAG, "inside UpdateAsynTask onPreExecute()");

            if (LOGIN_ACTIVITY_FLAG)
                Log.e(TAG, "outside UpdateAsynTask onPreExecute()");
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (LOGIN_ACTIVITY_FLAG)
                Log.e(TAG, "inside UpdateAsynTask doInBackground()");

            Register registerObj = new Register(LoginActivity.this);
            registreationResult = registerObj.updateUserOnserver(regObj);

            if (LOGIN_ACTIVITY_FLAG)
                Log.e(TAG, "outside UpdateAsynTask doInBackground()");

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
			/*progressDialog.cancel();*/

            if (LOGIN_ACTIVITY_FLAG)
                Log.e(TAG, "inside UpdateAsynTask onPostExecute()");

            Translation transeletion = new Translation(LoginActivity.this);
            transeletion.openConnection();
            DialogGenerator dg = new DialogGenerator(LoginActivity.this);

            if (registreationResult == Register.SUCCESS) {
                //updated the equationlevel and totalplayerpointtable
                UserPlayerOperation userOpr = new UserPlayerOperation(LoginActivity.this);
                String playerId = userOpr.getPlayerIdbyUserName(tempPlayerUserName);
                String userId = userOpr.getUserIdbyUserName(tempPlayerUserName);
                userOpr.closeConn();

                LearningCenterimpl learnignCenter = new LearningCenterimpl(LoginActivity.this);
                learnignCenter.openConn();
                learnignCenter.updatePlayerTotalPointsForUserIdandPlayerId(userId, playerId);
                learnignCenter.updatePlayerEquationTabelForUserIdAndPlayerId(userId, playerId);
                learnignCenter.updateMathResultForUserIdAndPlayerId(userId, playerId);
                learnignCenter.closeConn();

                //changes for avtar
                ChooseAvtarOpration opr = new ChooseAvtarOpration();
                opr.openConn(LoginActivity.this);
                opr.updateplayerAvtarStatusForTempPlayer(userId, playerId);
                ArrayList<String> avtarList = opr.getAvtarIds(userId, playerId);
                opr.closeConn();

                if (avtarList.size() > 0) {
                    if (CommonUtils.isInternetConnectionAvailable(LoginActivity.this)) {
                        new SaveAvtar(userId, playerId, avtarList).execute(null, null, null);
                    }
                }
                //end changes for avtar

                //changes for purchased item
                LearningCenterimpl lrarningImpl = new LearningCenterimpl(LoginActivity.this);
                lrarningImpl.openConn();
                lrarningImpl.updatePurchasedItemTable(userId);
                ArrayList<String> purchaseItemList = lrarningImpl.getPurchaseItemIdsByUserId(userId);
                lrarningImpl.closeConn();

                if (purchaseItemList.size() > 0) {
                    if (CommonUtils.isInternetConnectionAvailable(LoginActivity.this)) {
                        new SavePurchasedItems(userId, purchaseItemList).execute(null, null, null);
                    }
                }
                //end

                //changes for update score of temp player
                LearningCenterimpl learningCenterObj = new LearningCenterimpl(LoginActivity.this);
                learningCenterObj.openConn();
                ArrayList<MathResultTransferObj> mathResultList = learningCenterObj.getMathResultData();
                learningCenterObj.closeConn();

                if (mathResultList.size() > 0) {
                    if (CommonUtils.isInternetConnectionAvailable(LoginActivity.this)) {
                        new SaveTempPlayerScoreOnServer(mathResultList).execute(null, null, null);
                    } else {
                        transeletion.openConnection();
                        dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
                        transeletion.closeConnection();
                    }
                } else {
                    progressDialog.cancel();

                    SharedPreferences sheredPreference = getSharedPreferences(IS_CHECKED_PREFF, 0);
                    SharedPreferences.Editor editor = sheredPreference.edit();
                    editor.clear();
                    editor.commit();

                    //changes for selected single player (by deepak mail)
                    UserPlayerOperation userObj = new UserPlayerOperation(LoginActivity.this);
                    ArrayList<UserPlayerDto> userPlayerObj = userObj.getUserPlayerData();

                    if (userPlayerObj.size() == 1) {
                        SharedPreferences sheredPreference2 = getSharedPreferences(IS_CHECKED_PREFF, 0);
                        SharedPreferences.Editor editor2 = sheredPreference2.edit();
                        editor2.putString(PLAYER_ID, userPlayerObj.get(0).getPlayerid());
                        //changes according to shilpi
                        editor2.putString("userId", userPlayerObj.get(0).getParentUserId());
                        //end changes
                        editor2.commit();
                    }
                    //end changes

					/*Intent intent = new Intent(LoginActivity.this,MainActivity.class);
					startActivity(intent);*/
                    openMainScreenAfterLogin();
                }
            } else if (registreationResult == Register.INVALID_CITY) {
                progressDialog.cancel();//changes for score update
                dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgInvalidCity"));
            } else if (registreationResult == Register.INVALID_ZIP) {
                progressDialog.cancel();//changes for score update
                dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgInvalidZipCode"));
            } else if (registreationResult == 0) {
                progressDialog.cancel();
                CommonUtils.showInternetDialog(LoginActivity.this);
            }

            transeletion.closeConnection();

            if (LOGIN_ACTIVITY_FLAG)
                Log.e(TAG, "outside UpdateAsynTask onPostExecute()");

            super.onPostExecute(result);
        }
    }

    /**
     * Save Temp Player result on server
     *
     * @author Yashwant Singh
     */
    class SaveTempPlayerScoreOnServer extends AsyncTask<Void, Void, Void> {

        private ArrayList<MathResultTransferObj> mathobj = null;
        private String playerId = null;
        private String UserId = null;

        SaveTempPlayerScoreOnServer(ArrayList<MathResultTransferObj> mathObj) {
            this.mathobj = mathObj;
        }

        @Override
        protected void onPreExecute() {
            UserPlayerOperation userPlayerObj = new UserPlayerOperation(LoginActivity.this);
            playerId = userPlayerObj.getPlayerIdbyUserName(tempPlayerUserName);
            userPlayerObj.closeConn();

            UserRegistrationOperation userOperation = new UserRegistrationOperation(LoginActivity.this);
            UserId = userOperation.getUserData().getUserId();
            userOperation.closeConn();

            for (int i = 0; i < mathobj.size(); i++) {
                mathobj.get(i).setPlayerId(playerId);
                mathobj.get(i).setUserId(UserId);
            }

            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Register register = new Register(LoginActivity.this);
            register.savePlayerScoreOnServer(mathobj);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            progressDialog.cancel();

            LearningCenterimpl learnignCenter = new LearningCenterimpl(LoginActivity.this);
            learnignCenter.openConn();
            learnignCenter.deleteFromMathResult();
            ArrayList<PlayerEquationLevelObj> playerquationData = learnignCenter.getPlayerEquationLevelDataByPlayerId(playerId);
            learnignCenter.closeConn();

            if (playerquationData.size() > 0) {
                new AddLevelAsynTask(playerquationData).execute(null, null, null);
            }
            //end update
            else {
                SharedPreferences sheredPreference = getSharedPreferences(IS_CHECKED_PREFF, 0);
                SharedPreferences.Editor editor = sheredPreference.edit();
                editor.clear();
                editor.commit();

                //changes for selected single player (by deepak mail)
                UserPlayerOperation userObj = new UserPlayerOperation(LoginActivity.this);
                ArrayList<UserPlayerDto> userPlayerObj = userObj.getUserPlayerData();

                if (userPlayerObj.size() == 1) {
                    SharedPreferences sheredPreference2 = getSharedPreferences(IS_CHECKED_PREFF, 0);
                    SharedPreferences.Editor editor2 = sheredPreference2.edit();
                    editor2.putString(PLAYER_ID, userPlayerObj.get(0).getPlayerid());
                    //changes according to shilpi
                    editor2.putString("userId", userPlayerObj.get(0).getParentUserId());
                    //end changes
                    editor2.commit();
                }
                //end changes

				/*Intent intent = new Intent(LoginActivity.this,MainActivity.class);
				startActivity(intent);*/
                openMainScreenAfterLogin();
            }
            super.onPostExecute(result);
        }
    }

    /**
     * Update Player level data
     *
     * @author Yashwant Singh
     */
    private class AddLevelAsynTask extends AsyncTask<Void, Void, Void> {
        private ArrayList<PlayerEquationLevelObj> playerquationData = null;

        AddLevelAsynTask(ArrayList<PlayerEquationLevelObj> playerquationData) {
            this.playerquationData = playerquationData;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            LearningCenteServerOperation serverOpr = new LearningCenteServerOperation();
            serverOpr.addAllLevel("<levels>" + getLevelXml(playerquationData) + "</levels>");
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            SharedPreferences sheredPreference = getSharedPreferences(IS_CHECKED_PREFF, 0);
            SharedPreferences.Editor editor = sheredPreference.edit();
            editor.clear();
            editor.commit();

            //changes for selected single player (by deepak mail)
            UserPlayerOperation userObj = new UserPlayerOperation(LoginActivity.this);
            ArrayList<UserPlayerDto> userPlayerObj = userObj.getUserPlayerData();

            if (userPlayerObj.size() == 1) {
                SharedPreferences sheredPreference2 = getSharedPreferences(IS_CHECKED_PREFF, 0);
                SharedPreferences.Editor editor2 = sheredPreference2.edit();
                editor2.putString(PLAYER_ID, userPlayerObj.get(0).getPlayerid());
                //changes according to shilpi
                editor2.putString("userId", userPlayerObj.get(0).getParentUserId());
                //end changes
                editor2.commit();
            }

            //end changes
			/*Intent intent = new Intent(LoginActivity.this,MainActivity.class);
			startActivity(intent);*/
            openMainScreenAfterLogin();
            super.onPostExecute(result);
        }
    }

    private String getLevelXml(ArrayList<PlayerEquationLevelObj> playerquationData) {
        StringBuilder xml = new StringBuilder("");

        for (int i = 0; i < playerquationData.size(); i++) {
            xml.append("<userLevel>" +
                    "<uid>" + playerquationData.get(i).getUserId() + "</uid>" +
                    "<pid>" + playerquationData.get(i).getPlayerId() + "</pid>" +
                    "<eqnId>" + playerquationData.get(i).getEquationType() + "</eqnId>" +
                    "<level>" + playerquationData.get(i).getLevel() + "</level>" +
                    "<stars>" + playerquationData.get(i).getStars() + "</stars>" +
                    "</userLevel>");
        }

        return xml.toString();
    }

    //changes for avtar

    /**
     * This class getPurchase avtar from server
     *
     * @author Yashwant Singh
     */
    private class GetPurchasedAvtar extends AsyncTask<Void, Void, Void> {
        private String userId;
        private ArrayList<PurchasedAvtarDto> purchasedAvterList;

        GetPurchasedAvtar(String userId) {
            purchasedAvterList = new ArrayList<PurchasedAvtarDto>();

            this.userId = userId;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Login loginObj = new Login(LoginActivity.this);
            purchasedAvterList = loginObj.getPurchasedAvtar(userId);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            if (purchasedAvterList != null) {
                ChooseAvtarOpration opr = new ChooseAvtarOpration();
                opr.openConn(LoginActivity.this);

                for (int i = 0; i < purchasedAvterList.size(); i++) {
                    opr.insertIntoPlayerActarStatus(purchasedAvterList.get(i).getUserId(), purchasedAvterList.get(i).getPlayerId()
                            , purchasedAvterList.get(i).getActerId(), purchasedAvterList.get(i).getStatus());
                }
                opr.closeConn();
            }
            super.onPostExecute(result);
        }
    }

    /**
     * This class save temp player avtar on server
     *
     * @author Yashwant Singh
     */
    class SaveAvtar extends AsyncTask<Void, Void, Void> {
        private String userId;
        private String playerId;
        private String avtarIds;

        SaveAvtar(String userId, String playerId, ArrayList<String> avtarIdList) {
            this.userId = userId;
            this.playerId = playerId;

            avtarIds = "";

            for (int i = 0; i < avtarIdList.size(); i++) {
                avtarIds = avtarIds + avtarIdList.get(i);
                if (i < avtarIdList.size() - 1)
                    avtarIds = avtarIds + ",";
            }
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            AvtarServerOperation serverObj = new AvtarServerOperation();
            serverObj.saveAvtar(userId, playerId, avtarIds);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
        }
    }

    /**
     * This method save the temp player purchased items on server
     *
     * @author Yashwant Singh
     */
    class SavePurchasedItems extends AsyncTask<Void, Void, Void> {
        private String userId;
        private ArrayList<String> purchaseItemList;
        private String itemsId;

        SavePurchasedItems(String userId, ArrayList<String> purchasedItemList) {
            this.userId = userId;
            this.purchaseItemList = purchasedItemList;

            itemsId = "";
            for (int i = 0; i < purchasedItemList.size(); i++) {
                itemsId = itemsId + purchasedItemList.get(i);
                if (i < purchasedItemList.size() - 1)
                    itemsId = itemsId + ",";
            }
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Login login = new Login(LoginActivity.this);
            login.savePurchasedItem(userId, itemsId);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
        }
    }

    /**
     * This asyncktask get single friendzy detail from server
     *
     * @author Yashwant Singh
     */
    public class GetPurchsedItemDetailByUserId extends AsyncTask<Void, Void, Void> {
        private String userId = null;
        private Context context = null;
        private String resultString = "";
        private ProgressDialog pd;


        public GetPurchsedItemDetailByUserId(String userId, Context context) {
            this.userId = userId;
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            MultiFriendzyServerOperation serverObj = new MultiFriendzyServerOperation();
            resultString = serverObj.getPurchasedItemDetailByUserId(userId);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //pd.cancel();

            try {
                if (resultString != null) {
                    String itemIds = "";
                    ArrayList<String> itemList = new ArrayList<String>();
                    if (resultString != null) {//start if
                        //got app unlock status , changes on 5 May 2014
                        String[] resultValue = resultString.split("&");
                        resultString = resultValue[0].replace("&", "");
                        String appUnlockStatus = resultValue[1].replace("&", "");

                        if (resultString.length() > 0) {
                            //itemIds = result.getItems().replace(",", "");
                            //get item value which is comma separated , add (,) at the last for finish
                            String newItemList = resultString + ",";

                            for (int i = 0; i < newItemList.length(); i++) {
                                if (newItemList.charAt(i) == ',') {
                                    itemList.add(itemIds);
                                    itemIds = "";
                                } else {
                                    itemIds = itemIds + newItemList.charAt(i);
                                }
                            }
                        }
                        ArrayList<PurchaseItemObj> purchaseItem = new ArrayList<PurchaseItemObj>();
                        for (int i = 0; i < itemList.size(); i++) {
                            PurchaseItemObj purchseObj = new PurchaseItemObj();
                            purchseObj.setUserId(userId);
                            purchseObj.setItemId(Integer.parseInt(itemList.get(i)));
                            purchseObj.setStatus(1);
                            purchaseItem.add(purchseObj);
                        }
                        //add app unlick statuc , changes on 5 May 2014
                        try {

                            if (!appUnlockStatus.equals("-1")) {
                                PurchaseItemObj purchseObj = new PurchaseItemObj();
                                purchseObj.setUserId(userId);
                                purchseObj.setItemId(100);
                                purchseObj.setStatus(Integer.parseInt(appUnlockStatus));
                                purchaseItem.add(purchseObj);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Error while inserting app unlock status " + e.toString());
                        }
                        LearningCenterimpl learningCenter = new LearningCenterimpl(context);
                        learningCenter.openConn();
                        learningCenter.deleteFromPurchaseItem();
                        learningCenter.insertIntoPurchaseItem(purchaseItem);
                        learningCenter.closeConn();
                    }//end if
                }
            } catch (Exception e) {
                Log.e(TAG, "Error while getting purchase response " + e.toString());
            }
            super.onPostExecute(result);
        }
    }

    @Override
    protected void onResume() {
        //for show ad dialog
        CommonUtils.showAdDialog(this);
        //end ad dialog
        super.onResume();
    }


    /**
     * Open main screen after login
     */
    private void openMainScreenAfterLogin() {
        try {
            Language languageObj =  new Language(this);
            String langName = languageObj.getLanguageNameById(CommonUtils
                    .getUserLanguageCode(this) + "");
            String langCode = languageObj.getLanguageCodeByName(langName);
            String langId   = languageObj.getLanguageIdByName(langName);
            MathFriendzyHelper.getLanguageTranslation(this , langId , langCode ,
                    CommonUtils.APP_ID , new HttpServerRequest() {
                @Override
                public void onRequestComplete() {
                    openMainScreen();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            this.openMainScreen();
        }
    }

    /**
     * Open main screen
     */
    private void openMainScreen(){
        /*if(isOpenForInAppPurchase){
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
            return ;
        }*/
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
