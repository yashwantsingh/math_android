package com.mathfriendzy.controller.professionaltutoring;

import com.mathfriendzy.model.professionaltutoring.GetPurchagePackagesResponse;

/**
 * Created by root on 7/9/15.
 */
public interface PackageAdapterCallback {
    void clickOnPurchase(GetPurchagePackagesResponse response);
}
