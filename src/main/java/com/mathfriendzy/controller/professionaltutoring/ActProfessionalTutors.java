package com.mathfriendzy.controller.professionaltutoring;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.helpastudent.ActHelpAStudent;
import com.mathfriendzy.controller.player.LoginUserCreatePlayer;
import com.mathfriendzy.controller.player.LoginUserPlayerActivity;
import com.mathfriendzy.controller.player.TeacherPlayer;
import com.mathfriendzy.controller.tutoringsession.ActTutoringSession;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.helpastudent.CheckForTutorParam;
import com.mathfriendzy.model.helpastudent.CheckForTutorResponse;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.professionaltutoring.GetPurchaseTimeInfoResponse;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.MathVersions;

import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_ID;

public class ActProfessionalTutors extends ActBase {

    private final String TAG = this.getClass().getSimpleName();
    private TextView txtProfessionalTutors = null;
    private TextView txtForAsLow = null;
    private TextView txtOutTutorAreSeasoned = null;
    private Button btnPackageAndRates = null;
    private Button btnRequestATutor = null;
    private TextView txtFreeTutors = null;
    private TextView txtTheseAreStudentFromSameSchool = null;
    private Button btnViewDetail = null;
    private Button btnPreviousSession = null;
    private RelativeLayout professionalTutorLayout = null;
    private UserPlayerDto selectedPlayer = null;
    private String lblYouMustRegisterLoginToAccess = null;
    private String lblRequestATutor = null;
    private int totalPurchaseTime = 0;
    private String titleTryItFree = null;
    private GetPurchaseTimeInfoResponse purchaseTimeInfoResponse = null;

    //for math plus change
    private Button btnHelpAStudent = null;
    private Button btnRequestATutorMathPlus = null;
    private TextView txtTutoringSession = null;
    private RegistereUserDto loginUser = null;
    private final String TEACHER = "0";
    private String studentNotATutorAlertTitle = null;
    private String lblOnGoingPrevious = "Ongoing / Previous";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_professional_tutors);

        CommonUtils.printLog(TAG , "inside onCreate()");

        selectedPlayer = this.getPlayerData();
        this.init();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.setVisibilityOfProfessionalTutorLayout();
        this.setVisibilityOfLayout();

        CommonUtils.printLog(TAG , "outside onCreate()");
    }

    private void init() {
        loginUser = CommonUtils.getLoginUser(this);
    }

    private void setVisibilityOfLayout(){
        if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS){
            btnHelpAStudent.setVisibility(Button.VISIBLE);
            txtOutTutorAreSeasoned.setVisibility(TextView.GONE);
            txtTheseAreStudentFromSameSchool.setVisibility(TextView.GONE);
            btnRequestATutor.setVisibility(Button.GONE);
            btnRequestATutorMathPlus.setVisibility(Button.VISIBLE);
            txtTutoringSession.setVisibility(TextView.VISIBLE);
            btnPreviousSession.setText(lblOnGoingPrevious);
        }
    }

    @Override
    protected void onResume() {
        this.getPlayerPurchaseTime();
        super.onResume();
    }

    private void getPlayerPurchaseTime() {
        if(CommonUtils.isInternetConnectionAvailable(this) &&
                selectedPlayer != null) {
            MathFriendzyHelper.getPlayerPurchaseTimeInfo(this,
                    selectedPlayer.getParentUserId(),
                    selectedPlayer.getPlayerid(), this, true);
        }else{
            if(selectedPlayer == null)
                btnRequestATutor.setText(titleTryItFree);
                btnRequestATutorMathPlus.setText(titleTryItFree);
        }
    }

    private void setVisibilityOfProfessionalTutorLayout() {
        /*if(!MathFriendzyHelper.isUserLogin(this)
                || selectedPlayer == null){
            professionalTutorLayout.setVisibility(RelativeLayout.GONE);
        }else{
            professionalTutorLayout.setVisibility(RelativeLayout.VISIBLE);
        }*/
    }

    @Override
    protected void setWidgetsReferences() {
        CommonUtils.printLog(TAG , "inside setWidgetsReferences()");

        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        txtProfessionalTutors = (TextView) findViewById(R.id.txtProfessionalTutors);
        txtForAsLow = (TextView) findViewById(R.id.txtForAsLow);
        txtOutTutorAreSeasoned = (TextView) findViewById(R.id.txtOutTutorAreSeasoned);
        btnPackageAndRates = (Button) findViewById(R.id.btnPackageAndRates);
        btnRequestATutor = (Button) findViewById(R.id.btnRequestATutor);
        txtFreeTutors = (TextView) findViewById(R.id.txtFreeTutors);
        txtTheseAreStudentFromSameSchool = (TextView) findViewById(R.id.txtTheseAreStudentFromSameSchool);
        btnViewDetail = (Button) findViewById(R.id.btnViewDetail);
        btnPreviousSession = (Button) findViewById(R.id.btnPreviousSession);
        professionalTutorLayout = (RelativeLayout) findViewById(R.id.professionalTutorLayout);

        //math plus changes
        btnHelpAStudent = (Button) findViewById(R.id.btnHelpAStudent);
        btnRequestATutorMathPlus = (Button) findViewById(R.id.btnRequestATutorMathPlus);
        txtTutoringSession = (TextView) findViewById(R.id.txtTutoringSession);

        CommonUtils.printLog(TAG , "outside setWidgetsReferences()");
    }

    @Override
    protected void setListenerOnWidgets() {

        CommonUtils.printLog(TAG , "inside setListenerOnWidgets()");

        btnPackageAndRates.setOnClickListener(this);
        btnRequestATutor.setOnClickListener(this);
        btnViewDetail.setOnClickListener(this);
        btnPreviousSession.setOnClickListener(this);

        //math plus changes
        btnHelpAStudent.setOnClickListener(this);
        btnRequestATutorMathPlus.setOnClickListener(this);

        CommonUtils.printLog(TAG , "outside setListenerOnWidgets()");
    }

    @Override
    protected void setTextFromTranslation() {
        CommonUtils.printLog(TAG , "inside setTextFromTranslation()");
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblRequestATutor"));
        txtProfessionalTutors.setText(transeletion.getTranselationTextByTextIdentifier("lblProfessionalTutors"));
        txtForAsLow.setText(transeletion.getTranselationTextByTextIdentifier("lblForAsLowAs"));
        txtOutTutorAreSeasoned.setText(transeletion.getTranselationTextByTextIdentifier("lblOurTutorsAreSeasoned"));
        btnPackageAndRates.setText(transeletion.getTranselationTextByTextIdentifier("titlePackagesRates"));
        //btnRequestATutor.setText(transeletion.getTranselationTextByTextIdentifier("titleTryItFree"));
        btnRequestATutor.setText(transeletion.getTranselationTextByTextIdentifier("lblRequestATutor"));
        btnRequestATutorMathPlus.setText(transeletion.getTranselationTextByTextIdentifier("lblRequestATutor"));

        txtFreeTutors.setText(transeletion.getTranselationTextByTextIdentifier("lblFreeTutors"));
        txtTheseAreStudentFromSameSchool.setText(transeletion.getTranselationTextByTextIdentifier("lblTheseAreStudents"));
        btnViewDetail.setText(transeletion.getTranselationTextByTextIdentifier("titleViewDetails"));
        btnPreviousSession.setText(transeletion.getTranselationTextByTextIdentifier("titlePreviousSessions"));
        lblYouMustRegisterLoginToAccess = transeletion
                .getTranselationTextByTextIdentifier("lblYouMustRegisterLoginToAccess");
        lblRequestATutor = transeletion.getTranselationTextByTextIdentifier("lblRequestATutor");
        titleTryItFree   = transeletion.getTranselationTextByTextIdentifier("titleTryItFree");
        btnHelpAStudent.setText(transeletion.getTranselationTextByTextIdentifier("lblHelpAStudent"));
        txtTutoringSession.setText(transeletion.getTranselationTextByTextIdentifier("btnTutoringSession"));
        studentNotATutorAlertTitle = transeletion
                .getTranselationTextByTextIdentifier("studentNotATutorAlertTitle");
        //lblOnGoingPrevious = transeletion.getTranselationTextByTextIdentifier("lblOnGoingPrevious");
        transeletion.closeConnection();
        CommonUtils.printLog(TAG , "outside setTextFromTranslation()");
    }

    private void setTextForButtonBasedOnTime(GetPurchaseTimeInfoResponse response){
        this.purchaseTimeInfoResponse = response;
        totalPurchaseTime = response.getTime();
        if(response.getIsTimePurchase() == MathFriendzyHelper.NO
                && totalPurchaseTime > 0) {
            btnRequestATutor.setText(titleTryItFree);
            btnRequestATutorMathPlus.setText(titleTryItFree);
        }else{
            btnRequestATutor.setText(lblRequestATutor);
            btnRequestATutorMathPlus.setText(lblRequestATutor);
        }
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
        if(requestCode == ServerOperationUtil.GET_PLAYER_PURCHASE_TIME_INFO){
            GetPurchaseTimeInfoResponse response =
                    (GetPurchaseTimeInfoResponse) httpResponseBase;
            if(response.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)){
                this.setTextForButtonBasedOnTime(response);
            }
        }else if(requestCode == ServerOperationUtil.CHECK_FOR_TUTOR_REQUEST){
            CheckForTutorResponse response = (CheckForTutorResponse) httpResponseBase;
            if(response.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)){
                MathFriendzyHelper.updateIsTutor(this , selectedPlayer , response.getIsTutor());
                if(response.getIsTutor() == MathFriendzyHelper.YES){
                    goForHelpStudentScreen();
                }else{
                    MathFriendzyHelper.warningDialog(this , studentNotATutorAlertTitle);
                }
            }
        }
    }

    private void clickOnPackageAndRates(){
        Intent intent = new Intent(this , ActTutoringPackage.class);
        intent.putExtra("studentPurchaseTime" , totalPurchaseTime);
        startActivity(intent);
    }

    private void clickOnRequestATutor(){
        if(!MathFriendzyHelper.isUserLogin(this)){
            Intent intent = new Intent(this, ActTryItFreeNow.class);
            intent.putExtra("totalPurchaseTime" , totalPurchaseTime);
            startActivity(intent);
            return;
        }

        if(totalPurchaseTime > 0) {
            Intent intent = new Intent(this, ActTryItFreeNow.class);
            intent.putExtra("totalPurchaseTime" , totalPurchaseTime);
            //intent.putExtra("totalPurchaseTime" , purchaseTimeInfoResponse);
            startActivity(intent);
        }else{
            startActivity(new Intent(this , ActTutoringPackage.class));
        }
    }

    private void clickOnViewDetail(){
        //if(MathFriendzyHelper.isUserLogin(this)){
        startActivity(new Intent(this , ActViewDetailForProfessionalTutor.class));
        /*}else{
            MathFriendzyHelper.showLoginRegistrationDialog(this ,
                    lblYouMustRegisterLoginToAccess);
        }*/
    }

    private void clickOnPreviousSession(){
        if(MathFriendzyHelper.isUserLogin(this)){
            startActivity(new Intent(this , ActTutoringSession.class));
        }else{
            MathFriendzyHelper.showLoginRegistrationDialog(this ,
                    lblYouMustRegisterLoginToAccess);
        }
    }

    private void clickOnHelpAStudent(){
        this.checkForStudentSelection(MathFriendzyHelper.DIALOG_SCHOOL_HELP_STUDENT);
    }

    /**
     * This method call when user click on HomeWork
     */
    private void checkForStudentSelection(String checkFor){
        SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
        if(sheredPreference.getBoolean(IS_LOGIN, false))
        {
            UserPlayerOperation userPlayer = new UserPlayerOperation(this);
            if(!userPlayer.isUserPlayersExist())
            {
                SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0);
                if(sheredPreferencePlayer.getString(PLAYER_ID, "").equals("")){
                    UserRegistrationOperation userObj = new UserRegistrationOperation(this);
                    if(userObj.getUserData().getIsParent().equals("0")){//0 for teacher
                        Intent intent = new Intent(this,TeacherPlayer.class);
                        startActivity(intent);
                    }
                    else{
                        Intent intent = new Intent(this,LoginUserPlayerActivity.class);
                        startActivity(intent);
                    }
                }
                else{
                    if(checkFor.equalsIgnoreCase(MathFriendzyHelper.DIALOG_SCHOOL_HELP_STUDENT)){
                        this.checkForTutorForHelpAStudent();
                    }else{
                        //this.checkForUnlock(checkFor);
                    }
                }
            }else{
                Intent intent = new Intent(this,LoginUserCreatePlayer.class);
                startActivity(intent);
            }
        }
        else{
            MathFriendzyHelper.showLoginRegistrationDialog(this , lblYouMustRegisterLoginToAccess);
        }
    }

    /**
     * Check for tutor
     */
    private void checkForTutorForHelpAStudent(){
        if(loginUser != null){
            if(loginUser.getIsParent().equals(TEACHER)){
                goForHelpStudentScreen();
            }else{
                if(CommonUtils.isInternetConnectionAvailable(this)){
                    CheckForTutorParam param = new CheckForTutorParam();
                    param.setAction("checkIfStudnetATutor");
                    param.setUserId(selectedPlayer.getParentUserId());
                    param.setPlayerId(selectedPlayer.getPlayerid());
                    new MyAsyckTask(ServerOperation
                            .createPostRequestForCheckIsTutor(param)
                            , null, ServerOperationUtil.CHECK_FOR_TUTOR_REQUEST, this,
                            this, ServerOperationUtil.SIMPLE_DIALOG , true ,
                            getString(R.string.please_wait_dialog_msg))
                            .execute();
                }else{
                    CommonUtils.showInternetDialog(this);
                }
            }
        }else{
            goForHelpStudentScreen();//never execute but for the rare case
        }
    }

    private void goForHelpStudentScreen(){
        startActivity(new Intent(this , ActHelpAStudent.class));
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnPackageAndRates:
                this.clickOnPackageAndRates();
                break;
            case R.id.btnRequestATutor:
                this.clickOnRequestATutor();
                break;
            case R.id.btnViewDetail:
                this.clickOnViewDetail();
                break;
            case R.id.btnPreviousSession:
                this.clickOnPreviousSession();
                break;
            case R.id.btnHelpAStudent:
                this.clickOnHelpAStudent();
                break;
            case R.id.btnRequestATutorMathPlus:
                this.clickOnRequestATutor();
                break;
        }
    }
}
