package com.mathfriendzy.controller.professionaltutoring;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;

/**
 * Created by root on 5/10/15.
 */
public class CVVInfoTextWatcher implements TextWatcher {
    // Change this to what you want... ' ', '-' etc..
    private static final char space = '/';
    private int numberOfCharacterBeforeSpace = 2;

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        if(s.length() > 0){
            final char c = s.charAt(s.length() - 1);
            if(!(c >= 48 && c <= 57)){
                s.delete(s.length() - 1, s.length());
                return ;
            }
        }
    }
}
