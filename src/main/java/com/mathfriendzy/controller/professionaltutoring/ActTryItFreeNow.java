package com.mathfriendzy.controller.professionaltutoring;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.helpastudent.ActHelpAStudent;
import com.mathfriendzy.controller.player.LoginUserCreatePlayer;
import com.mathfriendzy.controller.player.LoginUserPlayerActivity;
import com.mathfriendzy.controller.player.TeacherPlayer;
import com.mathfriendzy.controller.tutor.ActTutorSession;
import com.mathfriendzy.controller.tutoringsession.ActTutoringSession;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.TeacherFunctionDialogListener;
import com.mathfriendzy.model.helpastudent.CheckForTutorParam;
import com.mathfriendzy.model.helpastudent.CheckForTutorResponse;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.professionaltutoring.GetPlayerPurchaseTimeResponse;
import com.mathfriendzy.model.professionaltutoring.ShouldPaidTutorAllowParam;
import com.mathfriendzy.model.professionaltutoring.ShouldPaisTutorResponse;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.model.tutoringsessions.TutoringSessionsForStudentResponse;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.MathVersions;

import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_ID;

public class ActTryItFreeNow extends ActBase {

    private final String TAG = this.getClass().getSimpleName();
    private TextView txtProfessionalTutors = null;
    private TextView txtSeasonedEducator = null;
    private TextView txtYouCurrentlyHave = null;
    private Button btnGetMoreTime = null;
    private TextView txtTalkToATutorNow = null;
    private EditText edtEnterATitleForQuestion = null;
    private Button btnGo = null;
    private TextView txtWeHaveTutorAvailable = null;
    private int totalPurchaseTime = 0;
    private String alertMsgYouCurrentlyHave = null;
    private String lblMinutesAvailable = null;
    private UserPlayerDto selectedPlayer = null;
    private String lblHours = null;
    private String lblMinutes = null;
    private String lblSeconds = null;
    private String lblYouMustRegisterLoginToAccess = null;
    private TextView txtFreeTutors = null;
    private Button btnViewDetail = null;
    private Button btnHelpAStudent = null;
    private Button btnPreviousSession = null;
    private RegistereUserDto loginUser = null;
    private final String TEACHER = "0";
    private String studentNotATutorAlertTitle = null;
    private String lblPaidTutorNotAvailable = null;
    private ShouldPaisTutorResponse responseFromServer = null;
    private final int GET_MORE_TIME = 1;
    private final int GO = 2;
    private final int ALLOW_SCHOOL_ID = 6728;
    private String lblYouHaveReachedYourTime = null;

    //for new layout change
    private RelativeLayout rlLayoutForSchoolSubscribeUser = null;
    private RelativeLayout layoutNonSubscribeUser = null;

    //For Non Subscribe USer Layout
    private TextView txtNeedHelpWithMath = null;
    private TextView txtTalkToLiveTutor = null;
    private TextView txtProfessionalTutorsForNonSubscribeUser = null;
    private TextView txtDownloadOurSisterApp = null;
    private Button btnTryitFreeNowForNonSubscribeUser = null;
    private TextView txtFreeTutorsForNonSuscribeUser = null;
    private TextView txtStudentInYourSchool = null;
    private Button btnViewDetailForNonSuscribeUser = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_try_it_free_now);

        CommonUtils.printLog(TAG, "inside onCreate()");

        selectedPlayer = this.getPlayerData();

        this.init();
        this.getIntentValues();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.setVisibilityofViewBasedOnVersion();


        CommonUtils.printLog(TAG , "outside onCreate()");
    }


    private void init() {
        loginUser = CommonUtils.getLoginUser(this);
    }

    private void setVisibilityofViewBasedOnVersion(){
        if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS){
            //btnHelpAStudent.setVisibility(Button.VISIBLE);

            //new change Nov19 email from Alex
            btnHelpAStudent.setVisibility(Button.GONE);
            btnPreviousSession.setVisibility(Button.VISIBLE);
        }else if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_TUTOR_PLUS){
            /*btnHelpAStudent.setVisibility(Button.VISIBLE);
            btnPreviousSession.setVisibility(Button.GONE);
            btnHelpAStudent.setBackgroundResource(R.drawable.new_friendzy_green_challenge);*/

            //new change Nov19 email from Alex
            btnHelpAStudent.setVisibility(Button.GONE);
            btnPreviousSession.setVisibility(Button.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        this.getPlayerPurchaseTime();
        //this.setVisibilityOfSubscribeAndNonSubscribeLayout();
        super.onResume();
    }

    private void getPlayerPurchaseTime() {
        if(CommonUtils.isInternetConnectionAvailable(this) &&
                selectedPlayer != null){
            /*GetPlayerPurchaseTimeParam param = new GetPlayerPurchaseTimeParam();
            param.setAction("getPlayersPurchasedTime");
            param.setpId(selectedPlayer.getPlayerid());
            param.setuId(selectedPlayer.getParentUserId());
            new MyAsyckTask(ServerOperation
                    .createPostRequestForGetPlayersPurchasedTime(param)
                    , null, ServerOperationUtil.GET_PLAYER_PURCHASE_TIME, this,
                    this, ServerOperationUtil.SIMPLE_DIALOG , true ,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();*/

            ShouldPaidTutorAllowParam param = new ShouldPaidTutorAllowParam();
            param.setAction("shouldPaidTutorAllow");
            param.setPid(selectedPlayer.getPlayerid());
            param.setUid(selectedPlayer.getParentUserId());
            param.setSchoolId(selectedPlayer.getSchoolId());
            new MyAsyckTask(ServerOperation
                    .createPostRequestForShouldPaidTutorAllow(param)
                    , null, ServerOperationUtil.SHOULD_PAID_TUTOR_ALLOW_REQUEST, this,
                    this, ServerOperationUtil.SIMPLE_DIALOG , true ,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        }
    }

    private void getIntentValues() {
        if(MathFriendzyHelper.isUserLogin(this)) {
            totalPurchaseTime = this.getIntent().getIntExtra("totalPurchaseTime", totalPurchaseTime);
        }else{
            totalPurchaseTime = 600;//show 10 min for the not login user
        }
    }

    @Override
    protected void setWidgetsReferences() {

        CommonUtils.printLog(TAG , "inside setWidgetsReferences()");

        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        txtProfessionalTutors = (TextView) findViewById(R.id.txtProfessionalTutors);
        txtSeasonedEducator = (TextView) findViewById(R.id.txtSeasonedEducator);
        txtYouCurrentlyHave = (TextView) findViewById(R.id.txtYouCurrentlyHave);
        btnGetMoreTime = (Button) findViewById(R.id.btnGetMoreTime);
        txtTalkToATutorNow = (TextView) findViewById(R.id.txtTalkToATutorNow);
        edtEnterATitleForQuestion = (EditText) findViewById(R.id.edtEnterATitleForQuestion);
        btnGo = (Button) findViewById(R.id.btnGo);
        txtWeHaveTutorAvailable = (TextView) findViewById(R.id.txtWeHaveTutorAvailable);
        txtFreeTutors = (TextView) findViewById(R.id.txtFreeTutors);
        btnViewDetail = (Button) findViewById(R.id.btnViewDetail);
        btnHelpAStudent = (Button) findViewById(R.id.btnHelpAStudent);
        btnPreviousSession = (Button) findViewById(R.id.btnPreviousSession);

        rlLayoutForSchoolSubscribeUser = (RelativeLayout) findViewById(R.id.rlLayoutForSchoolSubscribeUser);
        layoutNonSubscribeUser = (RelativeLayout) findViewById(R.id.layoutNonSubscribeUser);

        txtSeasonedEducator.setVisibility(TextView.GONE);

        this.setWidgetsForNonSubscribeUser();
        CommonUtils.printLog(TAG , "outside setWidgetsReferences()");
    }

    private void setWidgetsForNonSubscribeUser(){
        txtNeedHelpWithMath = (TextView) findViewById(R.id.txtNeedHelpWithMath);
        txtTalkToLiveTutor = (TextView) findViewById(R.id.txtTalkToLiveTutor);
        txtProfessionalTutorsForNonSubscribeUser = (TextView) findViewById(R.id.txtProfessionalTutorsForNonSubscribeUser);
        txtDownloadOurSisterApp = (TextView) findViewById(R.id.txtDownloadOurSisterApp);
        btnTryitFreeNowForNonSubscribeUser = (Button) findViewById(R.id.btnTryitFreeNowForNonSubscribeUser);
        txtFreeTutorsForNonSuscribeUser = (TextView) findViewById(R.id.txtFreeTutorsForNonSuscribeUser);
        txtStudentInYourSchool = (TextView) findViewById(R.id.txtStudentInYourSchool);
        btnViewDetailForNonSuscribeUser = (Button) findViewById(R.id.btnViewDetailForNonSuscribeUser);
    }


    @Override
    protected void setListenerOnWidgets() {
        CommonUtils.printLog(TAG , "inside setListenerOnWidgets()");

        btnGetMoreTime.setOnClickListener(this);
        btnGo.setOnClickListener(this);
        btnViewDetail.setOnClickListener(this);
        btnHelpAStudent.setOnClickListener(this);
        btnPreviousSession.setOnClickListener(this);
        txtSeasonedEducator.setOnClickListener(this);

        //For non subscribe user
        btnTryitFreeNowForNonSubscribeUser.setOnClickListener(this);
        btnViewDetailForNonSuscribeUser.setOnClickListener(this);

        CommonUtils.printLog(TAG , "outside setListenerOnWidgets()");
    }

    @Override
    protected void setTextFromTranslation() {
        CommonUtils.printLog(TAG , "inside setTextFromTranslation()");
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblRequestATutor"));
        txtProfessionalTutors.setText(transeletion.getTranselationTextByTextIdentifier("lblProfessionalTutors"));
        txtSeasonedEducator.setText(Html.fromHtml(MathFriendzyHelper.
                convertStringToUnderLineString(transeletion.getTranselationTextByTextIdentifier("lblPlusSoMuchMore"))));
       /*txtYouCurrentlyHave.setText(transeletion.getTranselationTextByTextIdentifier("alertMsgYouCurrentlyHave")
        + " " + MathFriendzyHelper.getTimeSpentText(totalPurchaseTime) + " "
                + transeletion.getTranselationTextByTextIdentifier("lblMinutesAvailable"));*/
        alertMsgYouCurrentlyHave = transeletion.getTranselationTextByTextIdentifier("alertMsgYouCurrentlyHave");
        lblMinutesAvailable = transeletion.getTranselationTextByTextIdentifier("lblMinutesAvailable");
        btnGetMoreTime.setText(transeletion.getTranselationTextByTextIdentifier("titleGetMoreTime"));
        txtTalkToATutorNow.setText(transeletion.getTranselationTextByTextIdentifier("lblTalkToTutorNow"));
        btnGo.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGo"));
        txtWeHaveTutorAvailable.setText(transeletion.getTranselationTextByTextIdentifier("lblWeHaveTutorsAvailable"));
        edtEnterATitleForQuestion.setHint(transeletion.getTranselationTextByTextIdentifier("lblEnterTitleForQuestion"));
        lblHours = transeletion.getTranselationTextByTextIdentifier("lblHours");
        lblMinutes = transeletion.getTranselationTextByTextIdentifier("lblMinutes");
        lblSeconds = transeletion.getTranselationTextByTextIdentifier("lblSeconds");
        lblYouMustRegisterLoginToAccess = transeletion
                .getTranselationTextByTextIdentifier("lblYouMustRegisterLoginToAccess");
        studentNotATutorAlertTitle = transeletion
                .getTranselationTextByTextIdentifier("studentNotATutorAlertTitle");
        btnViewDetail.setText(transeletion.getTranselationTextByTextIdentifier("titleViewDetails"));
        btnHelpAStudent.setText(transeletion.getTranselationTextByTextIdentifier("lblHelpAStudent"));
        txtFreeTutors.setText(transeletion.getTranselationTextByTextIdentifier("lblFreeTutors"));
        lblPaidTutorNotAvailable = transeletion.getTranselationTextByTextIdentifier("lblPaidTutorNotAvailable");
        lblYouHaveReachedYourTime = transeletion.getTranselationTextByTextIdentifier("lblYouHaveReachedYourTime");
        this.setPurchaseTimeText(totalPurchaseTime);
        this.setTranslationTextForNonSubscribeUserWidgets(transeletion);
        this.setBtnPreviousSessionText(transeletion);
        transeletion.closeConnection();
        CommonUtils.printLog(TAG , "outside setTextFromTranslation()");
    }


    private void setTranslationTextForNonSubscribeUserWidgets(Translation transeletion){
        txtNeedHelpWithMath.setText(transeletion.getTranselationTextByTextIdentifier("lblNeedHelpWithMath"));
        txtTalkToLiveTutor.setText(transeletion.getTranselationTextByTextIdentifier("lblTalkToLiveTutor"));
        txtProfessionalTutorsForNonSubscribeUser.setText
                (transeletion.getTranselationTextByTextIdentifier("lblProfessionalTutors"));
        txtDownloadOurSisterApp.setText(transeletion.getTranselationTextByTextIdentifier("lblDownloadSisterApp"));
        btnTryitFreeNowForNonSubscribeUser
                .setText(transeletion.getTranselationTextByTextIdentifier("titleTryItFree"));
        txtFreeTutorsForNonSuscribeUser.setText(transeletion
                .getTranselationTextByTextIdentifier("lblFreeTutors"));
        txtStudentInYourSchool.setText(transeletion.getTranselationTextByTextIdentifier("lblStudentsInYourSchool"));
        btnViewDetailForNonSuscribeUser.setText(transeletion
                .getTranselationTextByTextIdentifier("titleViewDetails"));
    }

    /**
     * Set Purchase Time
     * @param purchaseTime
     */
    private void setPurchaseTimeText(int purchaseTime){
        this.totalPurchaseTime = purchaseTime;
        txtYouCurrentlyHave.setText(alertMsgYouCurrentlyHave + " " +
                MathFriendzyHelper.getConvertedTimeStringText
                        (purchaseTime , lblHours , lblMinutes , lblSeconds)
                + " " + lblMinutesAvailable);
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
        if(requestCode == ServerOperationUtil.GET_PLAYER_PURCHASE_TIME){
            GetPlayerPurchaseTimeResponse response =
                    (GetPlayerPurchaseTimeResponse) httpResponseBase;
            if(response.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)){
                this.setPurchaseTimeText(response.getTime());
            }
        }else if(requestCode == ServerOperationUtil.CHECK_FOR_TUTOR_REQUEST){
            CheckForTutorResponse response = (CheckForTutorResponse) httpResponseBase;
            if(response.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)){
                MathFriendzyHelper.updateIsTutor(this , selectedPlayer , response.getIsTutor());
                if(response.getIsTutor() == MathFriendzyHelper.YES){
                    goForHelpStudentScreen();
                }else{
                    MathFriendzyHelper.warningDialog(this , studentNotATutorAlertTitle);
                    //goForHelpStudentScreen();
                }
            }
        }else if(requestCode == ServerOperationUtil.SHOULD_PAID_TUTOR_ALLOW_REQUEST){
            responseFromServer = (ShouldPaisTutorResponse) httpResponseBase;
            this.setPurchaseTimeText(MathFriendzyHelper.parseInt(responseFromServer.getTime()));
            MathFriendzyHelper.updateSchoolAllowPaidTutor(this , responseFromServer , selectedPlayer);
        }
    }

    private void clickOnGetMoreTime(){
        this.checkForNextScreen(responseFromServer , GET_MORE_TIME);
    }

    private void goForPackageScreen(){
        Intent intent = new Intent(this , ActTutoringPackage.class);
        intent.putExtra("studentPurchaseTime" , totalPurchaseTime);
        startActivity(intent);
    }
    private TutoringSessionsForStudentResponse getTutoringSessionData(){
        TutoringSessionsForStudentResponse obj
                = new TutoringSessionsForStudentResponse();
        obj.setRequestId(0);
        obj.setQueNo("");
        obj.setPaidSession("1");
        return obj;
    }

    private void clickOnGo(){

        if(!MathFriendzyHelper.isUserLogin(this)){
            MathFriendzyHelper.showLoginRegistrationDialog(this ,
                    lblYouMustRegisterLoginToAccess);
            return;
        }

        this.checkForNextScreen(responseFromServer , GO);
    }

    private void checkForNextScreen(ShouldPaisTutorResponse response , int checkFor){
        if(!MathFriendzyHelper.isUserLogin(this)){
            if (checkFor == GO) {
                goToNextSreen();
            } else if (checkFor == GET_MORE_TIME) {
                goForPackageScreen();
            }
            return ;
        }

        try {
            if(response != null) {
                if((!(MathFriendzyHelper.parseInt(response.getTime()) > 0)) && checkFor == GO){
                    MathFriendzyHelper.showWarningDialog(this, lblYouHaveReachedYourTime);
                    return;
                }

                int playerSchoolId = MathFriendzyHelper.parseInt(selectedPlayer.getSchoolId());
                if(playerSchoolId == ALLOW_SCHOOL_ID && response.getSchoolAllowPaidTutor()
                        .equalsIgnoreCase(MathFriendzyHelper.YES + "")){
                    if (checkFor == GO) {
                        goToNextSreen();
                    } else if (checkFor == GET_MORE_TIME) {
                        goForPackageScreen();
                    }
                    return;
                }

                if(!response.getPaidTutorAllow()
                        .equalsIgnoreCase(MathFriendzyHelper.YES + "")){
                    MathFriendzyHelper.showWarningDialog(this, lblPaidTutorNotAvailable);
                    return ;
                }

                if (response.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)) {
                    if (playerSchoolId > MathFriendzyHelper.MAX_ASSUME_SCHOOL_ID
                            && response.getSchoolAllowPaidTutor()
                            .equalsIgnoreCase(MathFriendzyHelper.YES + "")) {
                        if (checkFor == GO) {
                            goToNextSreen();
                        } else if (checkFor == GET_MORE_TIME) {
                            goForPackageScreen();
                        }
                        return;
                    }

                    if (playerSchoolId > MathFriendzyHelper.MAX_ASSUME_SCHOOL_ID) {
                        MathFriendzyHelper.showWarningDialog(this, lblPaidTutorNotAvailable);
                    } else {
                        if (response.getPaidTutorAllow().equalsIgnoreCase(MathFriendzyHelper.YES + "")
                                && response.getStudentWithNoSchoolAllowed()
                                .equalsIgnoreCase(MathFriendzyHelper.YES + "")) {
                            if (checkFor == GO) {
                                goToNextSreen();
                            } else if (checkFor == GET_MORE_TIME) {
                                goForPackageScreen();
                            }
                        } else {
                            MathFriendzyHelper.showWarningDialog(this, lblPaidTutorNotAvailable);
                        }
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
            if (checkFor == GO) {
                goToNextSreen();
            } else if (checkFor == GET_MORE_TIME) {
                goForPackageScreen();
            }
        }
    }

    private void goToNextSreen(){
        String title = edtEnterATitleForQuestion.getText().toString();
        this.clearInputBox();
        Intent intent = new Intent(this , ActTutorSession.class);
        intent.putExtra("isForTutoringSession" , true);
        intent.putExtra("tutorSessionTitle" , title);
        intent.putExtra("isForProfessionalTutoring" , true);
        intent.putExtra("tutoringSessionData" , this.getTutoringSessionData());
        startActivity(intent);
    }

    private void clearInputBox(){
        edtEnterATitleForQuestion.setText("");
    }

    private void clickOnViewDetail(){
        //if(MathFriendzyHelper.isUserLogin(this)){
        startActivity(new Intent(this , ActViewDetailForProfessionalTutor.class));
        /*}else{
            MathFriendzyHelper.showLoginRegistrationDialog(this ,
                    lblYouMustRegisterLoginToAccess);
        }*/
    }

    private void clickOnPreviousSession(){
        if(MathFriendzyHelper.isUserLogin(this)){
            startActivity(new Intent(this , ActTutoringSession.class));
        }else{
            MathFriendzyHelper.showLoginRegistrationDialog(this ,
                    lblYouMustRegisterLoginToAccess);
        }
    }

    private void clickOnHelpAStudent(){
        this.checkForStudentSelection(MathFriendzyHelper.DIALOG_SCHOOL_HELP_STUDENT);
    }

    /**
     * This method call when user click on HomeWork
     */
    private void checkForStudentSelection(String checkFor){
        SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
        if(sheredPreference.getBoolean(IS_LOGIN, false))
        {
            UserPlayerOperation userPlayer = new UserPlayerOperation(this);
            if(!userPlayer.isUserPlayersExist())
            {
                SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0);
                if(sheredPreferencePlayer.getString(PLAYER_ID, "").equals("")){
                    UserRegistrationOperation userObj = new UserRegistrationOperation(this);
                    if(userObj.getUserData().getIsParent().equals("0")){//0 for teacher
                        Intent intent = new Intent(this,TeacherPlayer.class);
                        startActivity(intent);
                    }
                    else{
                        Intent intent = new Intent(this,LoginUserPlayerActivity.class);
                        startActivity(intent);
                    }
                }
                else{
                    if(checkFor.equalsIgnoreCase(MathFriendzyHelper.DIALOG_SCHOOL_HELP_STUDENT)){
                        this.checkForTutorForHelpAStudent();
                    }else{
                        //this.checkForUnlock(checkFor);
                    }
                }
            }else{
                Intent intent = new Intent(this,LoginUserCreatePlayer.class);
                startActivity(intent);
            }
        }
        else{
            MathFriendzyHelper.showLoginRegistrationDialog(this , lblYouMustRegisterLoginToAccess);
        }
    }

    /**
     * Check for tutor
     */
    private void checkForTutorForHelpAStudent(){
        if(loginUser != null){
            if(MathFriendzyHelper.getUserAccountType(this) == MathFriendzyHelper.TEACHER){
                goForHelpStudentScreen();
            }else{
                if(CommonUtils.isInternetConnectionAvailable(this)){
                    CheckForTutorParam param = new CheckForTutorParam();
                    param.setAction("checkIfStudnetATutor");
                    param.setUserId(selectedPlayer.getParentUserId());
                    param.setPlayerId(selectedPlayer.getPlayerid());
                    new MyAsyckTask(ServerOperation
                            .createPostRequestForCheckIsTutor(param)
                            , null, ServerOperationUtil.CHECK_FOR_TUTOR_REQUEST, this,
                            this, ServerOperationUtil.SIMPLE_DIALOG , true ,
                            getString(R.string.please_wait_dialog_msg))
                            .execute();
                }else{
                    CommonUtils.showInternetDialog(this);
                }
            }
        }else{
            goForHelpStudentScreen();//never execute but for the rare case
        }
    }

    private void goForHelpStudentScreen(){
        startActivity(new Intent(this , ActHelpAStudent.class));
    }

    @Override
    public void onClick(View v) {
        this.hideDeviceKeyboard();
        switch(v.getId()){
            case R.id.btnGetMoreTime:
                this.clickOnGetMoreTime();
                break;
            case R.id.btnGo:
                this.clickOnGo();
                //this.goToNextSreen();
                break;
            case R.id.btnViewDetail:
                this.clickOnViewDetail();
                break;
            case R.id.btnHelpAStudent:
                this.clickOnHelpAStudent();
                break;
            case R.id.btnPreviousSession:
                if(this.showTeacherFunctionPopUp
                        (MathFriendzyHelper.DIALOG_TUTORING_SESSION,
                                MathFriendzyHelper.getTextFromButton(btnPreviousSession) , false))
                    return ;
                this.clickOnPreviousSession();
                break;
            case R.id.txtSeasonedEducator:
                this.openWebUrl();
                break;
            case R.id.btnViewDetailForNonSuscribeUser:
                this.clickOnViewDetail();
                break;
            case R.id.btnTryitFreeNowForNonSubscribeUser:
                MathFriendzyHelper.showWarningDialog(this , "Under Development!!!");
                break;

        }
    }

    /**
     * Show the teacher function popup
     * @param popUpFor
     * @return
     */
    private boolean showTeacherFunctionPopUp(final String popUpFor ,
                                             String txtTitle , boolean isShowWatchButton){
        if(MathFriendzyHelper.showTeacherFunctionPopup(this ,
                popUpFor , new TeacherFunctionDialogListener() {

                    @Override
                    public void clickOnWatchVedio(boolean isShow) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void clickOnDontShow(boolean isShow) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void clickOnClose(boolean isShow) {
                        clickOn(popUpFor);
                    }
                } , txtTitle , isShowWatchButton))
            return true;
        return false;
    }

    /**
     * Go after click on close button on Teacher function show vedio
     * @param dialogFor
     */
    private void clickOn(String dialogFor){
        if(MathFriendzyHelper.DIALOG_TUTORING_SESSION.equals(dialogFor)){
            this.clickOnPreviousSession();
        }
    }

    private void openWebUrl() {
        String url = MathFriendzyHelper.MATH_FRIENDZY_WEB_URL;
        if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS){
            url = MathFriendzyHelper.HOMEWORK_FRIENDZY_WEB_URL;
        }else if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_TUTOR_PLUS){
            url = MathFriendzyHelper.MATH_TUTOR_PLUS_WEB_URL;
        }
        MathFriendzyHelper.openUrl(this , url);
    }

    private void setVisibilityOfSubscribeAndNonSubscribeLayout(){
        if(!MathFriendzyHelper.isUserLogin(this)){
            rlLayoutForSchoolSubscribeUser.setVisibility(RelativeLayout.VISIBLE);
            layoutNonSubscribeUser.setVisibility(RelativeLayout.GONE);
        }else {
            if(MathFriendzyHelper.getAppUnlockStatus(MathFriendzyHelper.APP_UNLOCK_ID
                    , CommonUtils.getUserId(this) , this) == MathFriendzyHelper.APP_UNLOCK){
                rlLayoutForSchoolSubscribeUser.setVisibility(RelativeLayout.VISIBLE);
                layoutNonSubscribeUser.setVisibility(RelativeLayout.GONE);
            }else{
                rlLayoutForSchoolSubscribeUser.setVisibility(RelativeLayout.GONE);
                layoutNonSubscribeUser.setVisibility(RelativeLayout.VISIBLE);
            }
        }
    }

    private void setBtnPreviousSessionText(Translation translation){
        if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS
                || MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_TUTOR_PLUS){
            btnPreviousSession.setText(translation.getTranselationTextByTextIdentifier("btnTutoringSession"));
        }else {
            btnPreviousSession.setText(translation.getTranselationTextByTextIdentifier("titlePreviousSessions"));
        }
    }
}
