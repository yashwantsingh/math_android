package com.mathfriendzy.controller.professionaltutoring;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;

/**
 * Created by root on 5/10/15.
 */
public class CardInfoTextWatcher implements TextWatcher {

    // Change this to what you want... ' ', '-' etc..
    private static final char space = ' ';
    private int numberOfCharacterBeforeSpace = 4;

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void afterTextChanged(Editable s) {
       if(s.length() > 0){
           final char c = s.charAt(s.length() - 1);
           if(!(c >= 48 && c <= 57)){
               s.delete(s.length() - 1, s.length());
               return ;
           }
       }

        // Remove spacing char
        if (s.length() > 0 && (s.length() %
                (numberOfCharacterBeforeSpace + 1)) == 0) {
            final char c = s.charAt(s.length() - 1);
            if (space == c) {
                s.delete(s.length() - 1, s.length());
            }
        }
        // Insert char where needed.
        if (s.length() > 0 && (s.length() % (numberOfCharacterBeforeSpace + 1)) == 0) {
            char c = s.charAt(s.length() - 1);
            // Only if its a digit where there should be a space we insert a space
            if (Character.isDigit(c) && TextUtils.split(s.toString(),
                    String.valueOf(space)).length <=
                    (numberOfCharacterBeforeSpace - 1)) {
                s.insert(s.length() - 1, String.valueOf(space));
            }
        }
    }
}
