package com.mathfriendzy.controller.professionaltutoring;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.model.professionaltutoring.GetPurchagePackagesResponse;
import com.mathfriendzy.utils.CommonUtils;

import java.util.ArrayList;

/**
 * Created by root on 7/9/15.
 */
public class TutoringPackageAdapter extends BaseAdapter{

    private LayoutInflater mInflater = null;
    private ListView lstView = null;
    private Context context = null;
    private PackageAdapterCallback callback = null;
    private ViewHolder vHolder = null;
    private ArrayList<GetPurchagePackagesResponse> recurringTypePackage = null;
    private String dollar = "$";
    private String hour = "hour";
    private String lblPurchase = null;

    public TutoringPackageAdapter(Context context  , ListView lstView ,
                                  PackageAdapterCallback callback ,
                                  ArrayList<GetPurchagePackagesResponse> recurringTypePackage
            , String lblPurchase ){
        this.lstView = lstView;
        mInflater = LayoutInflater.from(context);
        this.context = context;
        this.callback = callback;
        this.recurringTypePackage = recurringTypePackage;
        this.lblPurchase = lblPurchase;
    }

    public void setPurchaseButtonText(String purchaseText){
        this.lblPurchase = purchaseText;
    }

    @Override
    public int getCount() {
        this.calculateListViewHeight(recurringTypePackage.size());
        return recurringTypePackage.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        if(view == null){
            view = mInflater.inflate(R.layout.tutoring_package_item_layout , null);
            vHolder = new ViewHolder();
            vHolder.txtHourPerMonth = (TextView) view.findViewById(R.id.txtHourPerMonth);
            vHolder.txtPrice = (TextView) view.findViewById(R.id.txtPrice);
            vHolder.txtPricePerHour = (TextView) view.findViewById(R.id.txtPricePerHour);
            vHolder.btnPurchase = (Button) view.findViewById(R.id.btnPurchase);
            view.setTag(vHolder);
        }else{
            vHolder = (ViewHolder) view.getTag();
        }


        vHolder.txtHourPerMonth.setText(recurringTypePackage.get(position).getDescription());
        vHolder.txtPrice.setText(dollar + recurringTypePackage.get(position).getTotalPrice());
        vHolder.txtPricePerHour.setText(dollar + recurringTypePackage.get(position)
                .getTermPrice() + "/" + hour);
        vHolder.btnPurchase.setText(lblPurchase);
        vHolder.btnPurchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.clickOnPurchase(recurringTypePackage.get(position));
            }
        });
        return view;
    }

    private void calculateListViewHeight(int numberOfCounts){
        ViewGroup.LayoutParams lp = lstView.getLayoutParams();
        int singleItemHeight = (int)context.getResources().
                getDimension(R.dimen.tutoring_package_item_height_for_calculation);
        int totalHeight = (singleItemHeight * numberOfCounts);
        lp.height = totalHeight;
        lstView.setLayoutParams(lp);
    }

    private class ViewHolder{
        private TextView txtHourPerMonth;
        private TextView txtPrice;
        private TextView txtPricePerHour;
        private Button btnPurchase;
    }
}
