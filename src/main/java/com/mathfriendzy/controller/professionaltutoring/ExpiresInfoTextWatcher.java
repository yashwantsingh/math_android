package com.mathfriendzy.controller.professionaltutoring;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.EditText;

import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.utils.CommonUtils;

import java.util.Calendar;

/**
 * Created by root on 5/10/15.
 */
public class ExpiresInfoTextWatcher implements TextWatcher {

    private String current = "";
    private String ddmmyyyy = "MMYY";
    private Calendar cal = Calendar.getInstance();
    private EditText edtText = null;
    private final int MAX_MON = 12;
    private final int MIN_MON = 1;
    private final int MAX_YEAR = 30;
    private int MIN_YEAR = 15;

    public ExpiresInfoTextWatcher(EditText edtText){
        MIN_YEAR = Calendar.getInstance().get(Calendar.YEAR) % 100;
        this.edtText = edtText;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (!s.toString().equals(current)) {
            String clean = s.toString().replaceAll("[^\\d.]", "");
            String cleanC = current.replaceAll("[^\\d.]", "");

            int cl = clean.length();
            int sel = cl;

            for (int i = 2; i <= cl && i < 4; i += 2) {
                sel++;
            }

            //Fix for pressing delete next to a forward slash
            if (clean.equals(cleanC)) sel--;

            if (clean.length() < 5) {

                if (clean.length() == 1) {

                    if (Integer.parseInt(clean.substring(0, 1)) != 1
                            && Integer.parseInt(clean.substring(0, 1)) != 0) {
                        clean = "0" + clean.substring(0, 1);
                        sel = sel + 2;
                    }

                }

                if (clean.length() == 2) {
                    if (Integer.parseInt(clean.substring(0, 2)) > MAX_MON)
                        clean = MAX_MON + "";

                    if (Integer.parseInt(clean.substring(0, 2)) < MIN_MON)
                        clean = "0" + MIN_MON;

                }

                if (clean.length() == 4) {
                    if (Integer.parseInt(clean.substring(2, 4)) < MIN_YEAR)
                        clean = clean.substring(0, 2) + MIN_YEAR;
                    /*if (Integer.parseInt(clean.substring(2, 4)) > MAX_YEAR)
                        clean = clean.substring(0, 2) + MAX_YEAR;*/
                }

                clean = clean + ddmmyyyy.substring(clean.length());
            }
            clean = String.format("%s/%s", clean.substring(0, 2),
                    clean.substring(2, 4));

            sel = sel < 0 ? 0 : sel;

            current = clean;
            edtText.setText(current);
            edtText.setSelection(sel < current.length() ? sel : current.length());
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        /*if(s.length() > 0){
            final char c = s.charAt(s.length() - 1);
            if(!(c >= 48 && c <= 57)){
                s.delete(s.length() - 1, s.length());
                return ;
            }
        }

       // Remove spacing char
        if (s.length() > 0 && (s.length() %
                (numberOfCharacterBeforeSpace + 1)) == 0) {
            final char c = s.charAt(s.length() - 1);
            if (space == c) {
                s.delete(s.length() - 1, s.length());
            }
        }

        // Insert char where needed.
        if (s.length() > 0 && (s.length() % (numberOfCharacterBeforeSpace + 1)) == 0) {
            char c = s.charAt(s.length() - 1);
            // Only if its a digit where there should be a space we insert a space
            if (Character.isDigit(c) && TextUtils.split(s.toString(),
                    String.valueOf(space)).length <=
                    (numberOfCharacterBeforeSpace - 1)) {
                s.insert(s.length() - 1, String.valueOf(space));
            }
        }*/
    }
}
