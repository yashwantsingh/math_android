package com.mathfriendzy.controller.professionaltutoring;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.MathVersions;

public class ActViewDetailForProfessionalTutor extends ActBase {

    private final String TAG = this.getClass().getSimpleName();
    private TextView txtMathOfferAPatentPanding = null;
    private Button btnWatchOurVideos = null;
    private TextView txtIfYourChildSchoolNotParticipating = null;
    private Button btnSendEmailToYourPrincipal = null;
    private String lblEmailToPrincipal = "Email to Principal";
    private String inviteEmailSubject = null;
    private String lblEmailContentToPrincipal = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_view_detail_for_professional_tutor);

        CommonUtils.printLog(TAG , "inside onCreate()");

        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();

        CommonUtils.printLog(TAG , "outside onCreate()");
    }

    @Override
    protected void setWidgetsReferences() {

        CommonUtils.printLog(TAG , "inside setWidgetsReferences()");

        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        txtMathOfferAPatentPanding = (TextView) findViewById(R.id.txtMathOfferAPatentPanding);
        txtIfYourChildSchoolNotParticipating = (TextView) findViewById(R.id.txtIfYourChildSchoolNotParticipating);
        btnWatchOurVideos = (Button) findViewById(R.id.btnWatchOurVideos);
        btnSendEmailToYourPrincipal = (Button) findViewById(R.id.btnSendEmailToYourPrincipal);

        CommonUtils.printLog(TAG , "outside setWidgetsReferences()");
    }

    @Override
    protected void setListenerOnWidgets() {

        CommonUtils.printLog(TAG , "inside setListenerOnWidgets()");

        btnWatchOurVideos.setOnClickListener(this);
        btnSendEmailToYourPrincipal.setOnClickListener(this);

        CommonUtils.printLog(TAG , "outside setListenerOnWidgets()");
    }

    @Override
    protected void setTextFromTranslation() {
        CommonUtils.printLog(TAG , "inside setTextFromTranslation()");
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("mfTitleHomeScreen"));
        if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS) {
            txtMathOfferAPatentPanding.
                    setText(transeletion.getTranselationTextByTextIdentifier("lblThisApp")
                            + " " + transeletion.getTranselationTextByTextIdentifier("lblMFOfferPatent"));
        }else{
            txtMathOfferAPatentPanding.
                    setText(transeletion.getTranselationTextByTextIdentifier("lblOurSisterApp")
                            + " " + transeletion.getTranselationTextByTextIdentifier("lblMFOfferPatent"));
        }
        txtIfYourChildSchoolNotParticipating.
                setText(transeletion.getTranselationTextByTextIdentifier("lblIfYourChildSchool"));
        btnWatchOurVideos.setText(transeletion.getTranselationTextByTextIdentifier("lblWatchOurVideo"));
        btnSendEmailToYourPrincipal.
                setText(transeletion.getTranselationTextByTextIdentifier("lblEmailToPrincipal"));
        inviteEmailSubject = transeletion.getTranselationTextByTextIdentifier("inviteEmailSubject");
        lblEmailContentToPrincipal = transeletion.getTranselationTextByTextIdentifier("lblEmailContentToPrincipal");
        transeletion.closeConnection();
        CommonUtils.printLog(TAG , "outside setTextFromTranslation()");
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {

    }

    private void clickOnWatchOurVideo(){
        MathFriendzyHelper.converUrlIntoEmbaddedAndPlay(this
                , MathFriendzyHelper.PROFESSIONAL_TUTORING_WATCH_VEDIO_URL);
    }

    private void clickOnSendEmailToPrincipal(){
        MathFriendzyHelper.sendEmailUsingEmailChooser(this , "",
                inviteEmailSubject , lblEmailContentToPrincipal ,
                "Send Email...");
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnWatchOurVideos:
                this.clickOnWatchOurVideo();
                break;
            case R.id.btnSendEmailToYourPrincipal:
                this.clickOnSendEmailToPrincipal();
                break;
        }
    }
}
