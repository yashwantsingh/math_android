package com.mathfriendzy.controller.professionaltutoring;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.professionaltutoring.CreditCardPaymentResponse;
import com.mathfriendzy.model.professionaltutoring.GetPurchagePackagesResponse;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;

import org.json.JSONObject;

public class ActCreditCardInfo extends ActBase {

    private final String TAG = this.getClass().getSimpleName();
    private TextView txtPaymentDetail = null;
    private TextView txtCardNumber = null;
    private TextView txtExpires = null;
    private TextView txtCVV = null;

    private EditText edtCardNumber = null;
    private EditText edtExpires = null;
    private EditText edtCVV = null;

    private ImageView imgVisa = null;
    private ImageView imgMasterCard = null;
    private ImageView imgDiscover = null;
    private ImageView imgAmex = null;

    private Button btnPurchase = null;

    private final int NONE = 0;
    private final int VISA_TYPE = 1;
    private final int MASTER_CARD_TYPE = 2;
    private final int DISCOVER_CARD_TYPE = 3;
    private final int AMEX_CARD_TYPE = 4;
    private int selectedType = NONE;

    private String lblPaymentDetail  = "Payment Details";
    private UserPlayerDto selectedPlayer = null;
    private GetPurchagePackagesResponse packageDetail = null;
    private float totalPriceInDollar = 0;
    private String descriptionText = null;
    private int totalPurchaseTimeInSeconds = 0;
    private String VISA_STRING = "visa";
    private String MASTER_STRING = "mastercard";
    private String DISCOVER_STRING = "discover";
    private String AMERICAN_EXPRESS_STRING = "amex";
    private String selectedTypeString = null;
    private String lblPleaseEnterAllInformation = "Please enter all information.";
    private String lblPleaseSelectYourCardType = "Please Select you card type.";

    private final int START_RECURRING_PAYMENT_URL_REQUEST = 90001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_credit_card_info);

        CommonUtils.printLog(TAG , "inside onCreate()");

        selectedPlayer = this.getPlayerData();

        this.getIntentValues();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        //this.selectedType(VISA_TYPE);

        CommonUtils.printLog(TAG , "outside onCreate()");
    }

    private void getIntentValues(){
        packageDetail = (GetPurchagePackagesResponse) this.getIntent()
                .getSerializableExtra("packageDetail");
        totalPriceInDollar = this.getIntent().getFloatExtra("totalPriceInDollar" , 0);
        descriptionText = this.getIntent().getStringExtra("descriptionText");
        totalPurchaseTimeInSeconds = this.getIntent().getIntExtra("totalPurchaseTimeInSeconds" , 0);
    }

    @Override
    protected void setWidgetsReferences() {

        CommonUtils.printLog(TAG , "inside setWidgetsReferences()");

        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        txtPaymentDetail = (TextView) findViewById(R.id.txtPaymentDetail);
        txtCardNumber = (TextView) findViewById(R.id.txtCardNumber);
        txtExpires = (TextView) findViewById(R.id.txtExpires);
        txtCVV = (TextView) findViewById(R.id.txtCVV);

        edtCardNumber = (EditText) findViewById(R.id.edtCardNumber);
        edtExpires = (EditText) findViewById(R.id.edtExpires);
        edtCVV = (EditText) findViewById(R.id.edtCVV);

        imgVisa  = (ImageView) findViewById(R.id.imgVisa);
        imgMasterCard  = (ImageView) findViewById(R.id.imgMasterCard);
        imgDiscover  = (ImageView) findViewById(R.id.imgDiscover);
        imgAmex  = (ImageView) findViewById(R.id.imgAmex);

        btnPurchase = (Button) findViewById(R.id.btnPurchase);

        CommonUtils.printLog(TAG , "outside setWidgetsReferences()");
    }

    @Override
    protected void setListenerOnWidgets() {
        CommonUtils.printLog(TAG , "inside setListenerOnWidgets()");

        btnPurchase.setOnClickListener(this);
        imgVisa.setOnClickListener(this);
        imgMasterCard.setOnClickListener(this);
        imgDiscover.setOnClickListener(this);
        imgAmex.setOnClickListener(this);

        edtCardNumber.addTextChangedListener(new CardInfoTextWatcher());
        edtExpires.addTextChangedListener(new ExpiresInfoTextWatcher(edtExpires));
        edtCVV.addTextChangedListener(new CVVInfoTextWatcher());

        CommonUtils.printLog(TAG , "outside setListenerOnWidgets()");
    }

    @Override
    protected void setTextFromTranslation() {
        CommonUtils.printLog(TAG , "inside setTextFromTranslation()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblPackageScreenTitle"));
        //txtPaymentDetail.setText(transeletion.getTranselationTextByTextIdentifier("lblPaymentDetail"));
        txtPaymentDetail.setText(lblPaymentDetail);
        txtCardNumber.setText(transeletion.getTranselationTextByTextIdentifier("lblCardNum"));
        txtExpires.setText(transeletion.getTranselationTextByTextIdentifier("lblExpireDate"));
        txtCVV.setText(transeletion.getTranselationTextByTextIdentifier("lblCvv"));
        btnPurchase.setText(transeletion.getTranselationTextByTextIdentifier("lblPurchase"));
        lblPleaseSelectYourCardType = transeletion
                .getTranselationTextByTextIdentifier("lblPleaseSelectType");
        lblPleaseEnterAllInformation = transeletion
                .getTranselationTextByTextIdentifier("lblPleaseEnterAllInfo");
        transeletion.closeConnection();

        CommonUtils.printLog(TAG , "outside setTextFromTranslation()");
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
        if(requestCode == ServerOperationUtil.CREDIT_CARD_PAYMENT_REQUEST){
            CreditCardPaymentResponse response = (CreditCardPaymentResponse) httpResponseBase;
            if(response.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)){
                MathFriendzyHelper.showWarningDialog(this , "Payment Success");
            }else{
                MathFriendzyHelper.showWarningDialog(this , response.getMessage());
            }
        }
    }

    private String getCustomParam(String cardNumber , String expires , String cvv){
        try{
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("uId" , selectedPlayer.getParentUserId());
            jsonObject.put("pId" , selectedPlayer.getPlayerid());
            jsonObject.put("packageId" , packageDetail.getId());
            jsonObject.put("time" , totalPurchaseTimeInSeconds + "");
            jsonObject.put("amount" , totalPriceInDollar + "");
            jsonObject.put("itemName" , descriptionText);
            jsonObject.put("desc" , "purchase");
            jsonObject.put("actNum" , cardNumber.replaceAll(" " , ""));
            jsonObject.put("month" , this.getMonth(expires) + "");
            jsonObject.put("year" , "20" + this.getYear(expires));
            jsonObject.put("cvv" , cvv);
            jsonObject.put("type" , this.selectedTypeString);
            jsonObject.put("fromAndroid" , "1");
            return jsonObject.toString();
        }catch(Exception e){
            return "";
        }
    }

    private int getMonth(String expires){
        try{
            int month = MathFriendzyHelper.parseInt(expires.split("/")[0]);
            return month;
        }catch(Exception e){
            return 0;
        }
    }

    private int getYear(String expires){
        try{
            int year = MathFriendzyHelper.parseInt(expires.split("/")[1]);
            return year;
        }catch(Exception e){
            return 0;
        }
    }

    private boolean isLastCharacterDigit(String expires){
        char c = expires.charAt(expires.length() - 1);
        return Character.isDigit(c);
    }

    private void clickOnPurchase(){
        String cardNumber = edtCardNumber.getText().toString();
        String expires = edtExpires.getText().toString();
        String cvv = edtCVV.getText().toString();

        if(MathFriendzyHelper.isEmpty(cardNumber)
                || MathFriendzyHelper.isEmpty(expires)
                || MathFriendzyHelper.isEmpty(cvv)
                || !this.isLastCharacterDigit(expires)){
            MathFriendzyHelper.showWarningDialog(this , lblPleaseEnterAllInformation);
            return;
        }else{
            if(this.selectedType == NONE){
                MathFriendzyHelper.showWarningDialog(this , lblPleaseSelectYourCardType);
                return ;
            }
        }

        if(CommonUtils.isInternetConnectionAvailable(this)) {
            String recurringPaymentCheckOutUrl = ICommonUtils.PAYPAL_CREDIT_CARD_PAYMENT
                    +"&customParams="+ this.getCustomParam(cardNumber, expires, cvv);
            CommonUtils.printLog(TAG , "url " + recurringPaymentCheckOutUrl);
            MathFriendzyHelper.openUrlInWebViewForResult(this ,
                    recurringPaymentCheckOutUrl , START_RECURRING_PAYMENT_URL_REQUEST,true);
            /*CreditCardPaymentParam param = new CreditCardPaymentParam();
            param.setCustomParam(this.getCustomParam(cardNumber, expires, cvv));
            new MyAsyckTask(ServerOperation
                    .createPostRequestForPayWithCreditCard(param)
                    , null, ServerOperationUtil.CREDIT_CARD_PAYMENT_REQUEST, this,
                    this, ServerOperationUtil.SIMPLE_DIALOG, true,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();*/
        }else{
            CommonUtils.showInternetDialog(this);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnPurchase:
                this.clickOnPurchase();
                break;
            case R.id.imgVisa:
                this.selectedType(VISA_TYPE);
                break;
            case R.id.imgMasterCard:
                this.selectedType(MASTER_CARD_TYPE);
                break;
            case R.id.imgDiscover:
                this.selectedType(DISCOVER_CARD_TYPE);
                break;
            case R.id.imgAmex:
                this.selectedType(AMEX_CARD_TYPE);
                break;
        }
    }

    private void selectedType(int selectedType){
        this.selectedType = selectedType;
        switch(selectedType){
            case VISA_TYPE:
                this.selectedTypeString = VISA_STRING;
                imgVisa.setBackgroundResource(R.drawable.visa_b_w);
                imgMasterCard.setBackgroundResource(R.drawable.master_card_color);
                imgDiscover.setBackgroundResource(R.drawable.discover_color);
                imgAmex.setBackgroundResource(R.drawable.amex_color);
                break;
            case MASTER_CARD_TYPE:
                this.selectedTypeString = MASTER_STRING;
                imgVisa.setBackgroundResource(R.drawable.visa_color);
                imgMasterCard.setBackgroundResource(R.drawable.master_card_b_w);
                imgDiscover.setBackgroundResource(R.drawable.discover_color);
                imgAmex.setBackgroundResource(R.drawable.amex_color);
                break;
            case DISCOVER_CARD_TYPE:
                this.selectedTypeString = DISCOVER_STRING;
                imgVisa.setBackgroundResource(R.drawable.visa_color);
                imgMasterCard.setBackgroundResource(R.drawable.master_card_color);
                imgDiscover.setBackgroundResource(R.drawable.discover_b_w);
                imgAmex.setBackgroundResource(R.drawable.amex_color);
                break;
            case AMEX_CARD_TYPE:
                this.selectedTypeString = AMERICAN_EXPRESS_STRING;
                imgVisa.setBackgroundResource(R.drawable.visa_color);
                imgMasterCard.setBackgroundResource(R.drawable.master_card_color);
                imgDiscover.setBackgroundResource(R.drawable.discover_color);
                imgAmex.setBackgroundResource(R.drawable.amex_b_w);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            switch(requestCode){
                case START_RECURRING_PAYMENT_URL_REQUEST:
                    Intent intent = new Intent();
                    setResult(RESULT_OK , intent);
                    finish();
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /*TextWatcher tw = new TextWatcher() {
        private String current = "";
        private String ddmmyyyy = "DDMMYYYY";
        private Calendar cal = Calendar.getInstance();

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (!s.toString().equals(current)) {
                String clean = s.toString().replaceAll("[^\\d.]", "");
                String cleanC = current.replaceAll("[^\\d.]", "");

                int cl = clean.length();
                int sel = cl;
                for (int i = 2; i <= cl && i < 6; i += 2) {
                    sel++;
                }
                //Fix for pressing delete next to a forward slash
                if (clean.equals(cleanC)) sel--;

                if (clean.length() < 8) {
                    clean = clean + ddmmyyyy.substring(clean.length());
                } else {
                    //This part makes sure that when we finish entering numbers
                    //the date is correct, fixing it otherwise
                    int day = Integer.parseInt(clean.substring(0, 2));
                    int mon = Integer.parseInt(clean.substring(2, 4));
                    int year = Integer.parseInt(clean.substring(4, 8));

                    if (mon > 12) mon = 12;
                    cal.set(Calendar.MONTH, mon - 1);
                    year = (year < 1900) ? 1900 : (year > 2100) ? 2100 : year;
                    cal.set(Calendar.YEAR, year);
                    // ^ first set year for the line below to work correctly
                    //with leap years - otherwise, date e.g. 29/02/2012
                    //would be automatically corrected to 28/02/2012

                    day = (day > cal.getActualMaximum(Calendar.DATE)) ? cal.getActualMaximum(Calendar.DATE) : day;
                    clean = String.format("%02d%02d%02d", day, mon, year);
                }

                clean = String.format("%s/%s/%s", clean.substring(0, 2),
                        clean.substring(2, 4),
                        clean.substring(4, 8));

                sel = sel < 0 ? 0 : sel;
                current = clean;
                edtExpires.setText(current);
                edtExpires.setSelection(sel < current.length() ? sel : current.length());
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }

    };*/
}
