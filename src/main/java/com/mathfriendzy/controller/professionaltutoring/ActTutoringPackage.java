package com.mathfriendzy.controller.professionaltutoring;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.helper.YesNoListenerInterface;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.professionaltutoring.CancelProfileParam;
import com.mathfriendzy.model.professionaltutoring.CancelProfileResponse;
import com.mathfriendzy.model.professionaltutoring.GetPurchagePackagesResponse;
import com.mathfriendzy.model.professionaltutoring.GetPurchaseTimeInfoResponse;
import com.mathfriendzy.model.professionaltutoring.GetTutorPackageParam;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;

import java.util.ArrayList;

public class ActTutoringPackage extends ActBase {

    private final String TAG = this.getClass().getSimpleName();
    private TextView txtTutoringPackage = null;
    private TextView txtYourCurrentPackageIs = null;
    private TextView txtYouCurrentlyHave = null;
    private TextView txtYouCurrentlyAdd = null;
    private TextView txtOrUpgradeMonthlyPackage = null;
    private ListView lstRecurringPackages = null;
    private ListView lstSingleTypePackages = null;

    private TextView txtMinutes = null;
    private TextView txtQuantity = null;
    private TextView txtPrice = null;
    private TextView txtTotal = null;
    private String lblPurchase = null;
    private String lblUpgrade = null;

    private int totalMinutesToAdd = 30;
    private final int ACT_SHOPPING_CART_REQUEST = 10001;
    private int totalPurchaseTime = 0;

    private TextView txtYouCanCancelAnyTime = null;
    private TextView txtCancelPackage = null;
    private String lblNoPackageActive = null;
    private String lblMinutesAvailable = null;
    private int studentPurchaseTime = 0;
    private String alertMsgYouCurrentlyHave = null;
    private UserPlayerDto selectedPlayer = null;
    private String lblRenewsOn = null;
    private String recurringPaymentButtonText = null;
    private String CURRENT_RENEW_DATE_FORMAT = "yyyy-MM-dd hh:mm:ss";
    private String REQUIRED_RENEW_DATE_FORMAT = "MMMM, dd yyyy";
    private GetPurchaseTimeInfoResponse purchaseInfoResponse = null;
    private String lblYouHaveNotPurchased = null;
    private String lblHours = null;
    private String lblMinutes = null;
    private String lblSeconds = null;
    private String lblYouMustRegisterLoginToAccess = null;
    private String lblYouDontHaveActivePackage = "You do not have any active package.";
    private TutoringPackageAdapter recurringAdapter = null;
    private String lblYourCurrentPackage = null;

    //package status
    private final int ACTIVE = 1;
    private final int PENDING = 2;
    private final int CANCEL = 3;

    //for new screen change
    private RelativeLayout tutoringPackageTopLayout = null;
    private RelativeLayout professionalTutorTopLayout = null;
    private RelativeLayout customPackageLayout = null;
    private RelativeLayout specialOfferLayout = null;

    private TextView txtProfessionalTutors = null;
    private TextView txtForAsLow = null;
    private TextView txtMonthlyRecurringPackage = null;
    private TextView txtYouCanChangeYourPackage = null;
    private TextView txtSpecialOfferLayout = null;
    private EditText edtEnterSpecialCode = null;
    private Button btnGo = null;

    //action after cancel package done
    private final int FOR_UPGRADE = 1;
    private final int ONLY_CANCEL = 2;

    private boolean isUpgradeRecurringPackage = false;
    private String lblInvalidCode = null;
    private String lblSureToCancalPackage = null;
    private String btnTitleYes = null;
    private String lblNo = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_tutoring_package);
        CommonUtils.printLog(TAG , "inside onCreate()");

        selectedPlayer = this.getPlayerData();
        this.getIntentValues();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.setYouCurrentlyHaveText(studentPurchaseTime , null);

        CommonUtils.printLog(TAG , "outside onCreate()");
    }

    private void setVisibilityOfProfessionalTutorLayouts(GetPurchaseTimeInfoResponse
                                                                 purchaseInfoResponse){
        if(purchaseInfoResponse.getIsCancel().equalsIgnoreCase(CANCEL + "")
                || purchaseInfoResponse.getIsTimePurchase() == MathFriendzyHelper.NO){
            tutoringPackageTopLayout.setVisibility(RelativeLayout.GONE);
            professionalTutorTopLayout.setVisibility(RelativeLayout.VISIBLE);
            customPackageLayout.setVisibility(RelativeLayout.GONE);
            txtOrUpgradeMonthlyPackage.setVisibility(TextView.GONE);
            specialOfferLayout.setVisibility(RelativeLayout.VISIBLE);
            //txtYouCanCancelAnyTime.setVisibility(TextView.GONE);
            //txtCancelPackage.setVisibility(TextView.GONE);
        }else{
            tutoringPackageTopLayout.setVisibility(RelativeLayout.VISIBLE);
            professionalTutorTopLayout.setVisibility(RelativeLayout.GONE);
            customPackageLayout.setVisibility(RelativeLayout.VISIBLE);
            txtOrUpgradeMonthlyPackage.setVisibility(TextView.VISIBLE);
            specialOfferLayout.setVisibility(RelativeLayout.GONE);
            //txtYouCanCancelAnyTime.setVisibility(TextView.VISIBLE);
            //txtCancelPackage.setVisibility(TextView.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        this.getPlayerPurchaseTime();
        super.onResume();
    }

    private void getPlayerPurchaseTime() {
        if(CommonUtils.isInternetConnectionAvailable(this) &&
                selectedPlayer != null){
            MathFriendzyHelper.getPlayerPurchaseTimeInfo(this ,
                    selectedPlayer.getParentUserId() ,
                    selectedPlayer.getPlayerid() , this , true);
        }else{
            if(selectedPlayer == null){
                this.getTutorPackages();
            }
        }
    }

    private void getIntentValues(){
        studentPurchaseTime = this.getIntent().getIntExtra("studentPurchaseTime" , 0);
    }

    private void getTutorPackages() {
        if(CommonUtils.isInternetConnectionAvailable(this)){
            GetTutorPackageParam param = new GetTutorPackageParam();
            param.setAction("getTutorPackages");
            new MyAsyckTask(ServerOperation
                    .createPostRequestForGetTutorPackage(param)
                    , null, ServerOperationUtil.GET_TUTOR_PACKAGES, this,
                    this, ServerOperationUtil.SIMPLE_DIALOG , true ,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        }else{
            CommonUtils.showInternetDialog(this);
        }
    }


    @Override
    protected void setWidgetsReferences() {
        CommonUtils.printLog(TAG , "inside setWidgetsReferences()");
        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        txtTutoringPackage = (TextView) findViewById(R.id.txtTutoringPackage);
        txtYourCurrentPackageIs = (TextView) findViewById(R.id.txtYourCurrentPackageIs);
        txtYouCurrentlyHave = (TextView) findViewById(R.id.txtYouCurrentlyHave);
        txtYouCurrentlyAdd = (TextView) findViewById(R.id.txtYouCurrentlyAdd);
        lstRecurringPackages = (ListView) findViewById(R.id.lstRecurringPackages);
        txtOrUpgradeMonthlyPackage = (TextView) findViewById(R.id.txtOrUpgradeMonthlyPackage);
        lstSingleTypePackages = (ListView) findViewById(R.id.lstSingleTypePackages);

        txtMinutes = (TextView) findViewById(R.id.txtMinutes);
        txtQuantity = (TextView) findViewById(R.id.txtQuantity);
        txtPrice = (TextView) findViewById(R.id.txtPrice);
        txtTotal = (TextView) findViewById(R.id.txtTotal);

        txtYouCanCancelAnyTime = (TextView) findViewById(R.id.txtYouCanCancelAnyTime);
        txtCancelPackage = (TextView) findViewById(R.id.txtCancelPackage);

        tutoringPackageTopLayout = (RelativeLayout) findViewById(R.id.tutoringPackageTopLayout);
        professionalTutorTopLayout = (RelativeLayout) findViewById(R.id.professionalTutorTopLayout);
        customPackageLayout = (RelativeLayout) findViewById(R.id.customPackageLayout);
        specialOfferLayout = (RelativeLayout) findViewById(R.id.specialOfferLayout);

        txtProfessionalTutors = (TextView) findViewById(R.id.txtProfessionalTutors);
        txtForAsLow = (TextView) findViewById(R.id.txtForAsLow);
        txtMonthlyRecurringPackage = (TextView) findViewById(R.id.txtMonthlyRecurringPackage);
        txtYouCanChangeYourPackage = (TextView) findViewById(R.id.txtYouCanChangeYourPackage);
        txtSpecialOfferLayout = (TextView) findViewById(R.id.txtSpecialOfferLayout);
        edtEnterSpecialCode = (EditText) findViewById(R.id.edtEnterSpecialCode);
        btnGo = (Button) findViewById(R.id.btnGo);

        CommonUtils.printLog(TAG , "outside setWidgetsReferences()");
    }

    @Override
    protected void setListenerOnWidgets() {
        CommonUtils.printLog(TAG , "inside setListenerOnWidgets()");
        txtCancelPackage.setOnClickListener(this);
        btnGo.setOnClickListener(this);
        CommonUtils.printLog(TAG , "outside setListenerOnWidgets()");
    }

    @Override
    protected void setTextFromTranslation() {
        CommonUtils.printLog(TAG , "inside setTextFromTranslation()");
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("titlePackagesRates"));
        txtTutoringPackage.setText(transeletion.getTranselationTextByTextIdentifier("lblTutoringPackage"));
        txtYourCurrentPackageIs.setText(transeletion.getTranselationTextByTextIdentifier
                ("lblYourCurrentPackage"));
        txtYouCurrentlyAdd.setText(transeletion.getTranselationTextByTextIdentifier("lblYouCanAdd")
                + " " + totalMinutesToAdd + " "
                + transeletion.getTranselationTextByTextIdentifier("lblMinToExistingPack"));
        txtOrUpgradeMonthlyPackage.setText(transeletion.getTranselationTextByTextIdentifier("lblORUpgradePack"));
        txtMinutes.setText(transeletion.getTranselationTextByTextIdentifier("lblMinutes"));
        txtQuantity.setText(transeletion.getTranselationTextByTextIdentifier("lblQuantity"));
        txtPrice.setText(transeletion.getTranselationTextByTextIdentifier("lblPrice"));
        lblPurchase = transeletion.getTranselationTextByTextIdentifier("lblPurchase");
        lblUpgrade  = transeletion.getTranselationTextByTextIdentifier("lblUpgrade");
        txtYouCanCancelAnyTime.setText(transeletion
                .getTranselationTextByTextIdentifier("lblYouCanCancelPack"));
        txtCancelPackage.setText(Html.fromHtml(MathFriendzyHelper.convertStringToUnderLineString
                (transeletion.getTranselationTextByTextIdentifier("lblCancelPackage"))));
        lblNoPackageActive = transeletion.getTranselationTextByTextIdentifier("lblNoPackageActive");
        lblMinutesAvailable = transeletion.getTranselationTextByTextIdentifier("lblMinutesAvailable");
        alertMsgYouCurrentlyHave = transeletion.getTranselationTextByTextIdentifier("alertMsgYouCurrentlyHave");
        lblRenewsOn = transeletion.getTranselationTextByTextIdentifier("lblRenewsOn");
        lblYouHaveNotPurchased = transeletion
                .getTranselationTextByTextIdentifier("lblYouHaveNotPurchased");

        lblHours = transeletion.getTranselationTextByTextIdentifier("lblHours");
        lblMinutes = transeletion.getTranselationTextByTextIdentifier("lblMinutes");
        lblSeconds = transeletion.getTranselationTextByTextIdentifier("lblSeconds");

        lblYouMustRegisterLoginToAccess = transeletion
                .getTranselationTextByTextIdentifier("lblYouMustRegisterLoginToAccess");
        lblYourCurrentPackage = transeletion.getTranselationTextByTextIdentifier
                ("lblYourCurrentPackage");
        lblYouDontHaveActivePackage = transeletion
                .getTranselationTextByTextIdentifier("lblNoPackageActive");

        txtProfessionalTutors.setText(transeletion.getTranselationTextByTextIdentifier("lblProfessionalTutors"));
        txtForAsLow.setText(transeletion.getTranselationTextByTextIdentifier("text"));
        txtMonthlyRecurringPackage.setText(transeletion.getTranselationTextByTextIdentifier("lblMonthlyRecurringPackage"));
        txtYouCanChangeYourPackage.setText(transeletion.getTranselationTextByTextIdentifier("lblYouCanChangeYourPack"));
        txtSpecialOfferLayout.setText(transeletion.getTranselationTextByTextIdentifier("lblSpecialOffer"));
        btnGo.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGo"));
        lblInvalidCode = transeletion.getTranselationTextByTextIdentifier("lblInvalidCode");
        lblSureToCancalPackage = transeletion.getTranselationTextByTextIdentifier("lblSureToCancalPackage");
        btnTitleYes = transeletion.getTranselationTextByTextIdentifier("btnTitleYes");
        lblNo = transeletion.getTranselationTextByTextIdentifier("lblNo");
        transeletion.closeConnection();

        CommonUtils.printLog(TAG , "outside setTextFromTranslation()");
    }

    /**
     * Set remaining time for student
     * @param purchaseTime
     */
    private void setYouCurrentlyHaveText(int purchaseTime , GetPurchaseTimeInfoResponse response){
        this.totalPurchaseTime = purchaseTime;
        String youCurrentlyHaveMsg = alertMsgYouCurrentlyHave + " " +
                MathFriendzyHelper.getConvertedTimeStringText
                        (purchaseTime , lblHours , lblMinutes , lblSeconds)
                + " " + lblMinutesAvailable;
        String yourCurrentPackage = lblYouDontHaveActivePackage;
        if(response != null){
            if(MathFriendzyHelper.parseInt(response.getPackageId()) > 0){
                String packageMessage = "";
                if(response.getIsCancel().equalsIgnoreCase(CANCEL + "")){
                    packageMessage = lblYouDontHaveActivePackage;
                    yourCurrentPackage = lblYouDontHaveActivePackage;
                    isUpgradeRecurringPackage = false;
                    recurringPaymentButtonText = lblPurchase;
                }else {
                    yourCurrentPackage = lblYourCurrentPackage;
                    packageMessage = response.getPackageName() + ", " + lblRenewsOn + " " +
                            MathFriendzyHelper.formatDataInGivenFormat(response.getNextDate(),
                                    CURRENT_RENEW_DATE_FORMAT, REQUIRED_RENEW_DATE_FORMAT) + ".";
                    isUpgradeRecurringPackage = true;
                    recurringPaymentButtonText = lblUpgrade;
                    youCurrentlyHaveMsg = packageMessage + " " + youCurrentlyHaveMsg;
                }
            }else{
                isUpgradeRecurringPackage = false;
                recurringPaymentButtonText = lblPurchase;
                yourCurrentPackage = lblYouDontHaveActivePackage;
                //youCurrentlyHaveMsg = lblNoPackageActive + " " + youCurrentlyHaveMsg;
            }
        }else{
            isUpgradeRecurringPackage = false;
            recurringPaymentButtonText = lblPurchase;
            yourCurrentPackage = lblYouDontHaveActivePackage;
            //youCurrentlyHaveMsg = lblNoPackageActive + " " + youCurrentlyHaveMsg;
        }
        txtYourCurrentPackageIs.setText(yourCurrentPackage);
        txtYouCurrentlyHave.setText(youCurrentlyHaveMsg);
    }

    private void setSingleTypePackage(ArrayList<GetPurchagePackagesResponse> singleTypePackage){
        SingleTypePackageAdapter adapter = new SingleTypePackageAdapter(this ,
                lstSingleTypePackages ,
                new PackageAdapterCallback() {
                    @Override
                    public void clickOnPurchase(GetPurchagePackagesResponse response) {
                        openShoppingCartScreen(response);
                    }
                } , singleTypePackage , lblPurchase);
        lstSingleTypePackages.setAdapter(adapter);
    }

    private void setRecurringTypePackage(ArrayList<GetPurchagePackagesResponse> recurringTypePackage){
        recurringAdapter = new TutoringPackageAdapter(this , lstRecurringPackages ,
                new PackageAdapterCallback() {
                    @Override
                    public void clickOnPurchase(GetPurchagePackagesResponse response) {
                        openShoppingCartScreen(response);
                    }
                } , recurringTypePackage , recurringPaymentButtonText);
        lstRecurringPackages.setAdapter(recurringAdapter);
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
        if(requestCode == ServerOperationUtil.GET_TUTOR_PACKAGES){
            GetPurchagePackagesResponse response = (GetPurchagePackagesResponse) httpResponseBase;
            if(response.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)){
                this.setSingleTypePackage(response.getSingleTypePackageList());
                this.setRecurringTypePackage(response.getRecurringTypePackageList());
            }
        }else if(requestCode == ServerOperationUtil.GET_PLAYER_PURCHASE_TIME_INFO){
            GetPurchaseTimeInfoResponse response =
                    (GetPurchaseTimeInfoResponse) httpResponseBase;
            if(response.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)){
                this.purchaseInfoResponse = response;
                this.setYouCurrentlyHaveText(response.getTime() , response);
                this.setVisibilityOfProfessionalTutorLayouts(purchaseInfoResponse);
                this.getTutorPackages();
            }
        }else if(requestCode == ServerOperationUtil.CANCEL_PROFILE){
            CancelProfileResponse response = (CancelProfileResponse) httpResponseBase;
            updateUIAfterCancelProfileResponse(response , ONLY_CANCEL , null);
        }
    }

    private void openShoppingCartScreen(final GetPurchagePackagesResponse packageDetail){
        if(!MathFriendzyHelper.isUserLogin(this)){
            MathFriendzyHelper.showLoginRegistrationDialog(this ,
                    lblYouMustRegisterLoginToAccess);
            return;
        }
        /*if(packageDetail.getType().
                equalsIgnoreCase(GetPurchagePackagesResponse.RECURRING_TYPE)){
            if(this.purchaseInfoResponse != null){
                if((MathFriendzyHelper.parseInt(this
                        .purchaseInfoResponse.getPackageId()) > 0)
                        && (!purchaseInfoResponse.getIsCancel()
                        .equalsIgnoreCase(CANCEL + ""))) {
                    this.cancelProfile(new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase,
                                                   int requestCode) {
                            updateUIAfterCancelProfileResponse((CancelProfileResponse)
                                            httpResponseBase,
                                    FOR_UPGRADE , packageDetail);
                        }
                    });
                    return;
                }
            }
        }*/
        this.goForShoppingCartScreen(packageDetail);
    }

    private void goForShoppingCartScreen(GetPurchagePackagesResponse packageDetail){
        Intent intent = new Intent(this , ActShoppingCart.class);
        intent.putExtra("packageDetail" , packageDetail);
        intent.putExtra("isUpgradeRecurringPackage" , isUpgradeRecurringPackage);
        intent.putExtra("purchaseInfoResponse" , purchaseInfoResponse);
        startActivityForResult(intent, ACT_SHOPPING_CART_REQUEST);
    }

    private void clickOnGo(){
        MathFriendzyHelper.showWarningDialog(this , lblInvalidCode);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.txtCancelPackage:
                this.clickOnCancelPackage();
                break;
            case R.id.btnGo:
                this.clickOnGo();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("purchaseTime" , totalPurchaseTime);
        setResult(RESULT_OK , intent);
        finish();
    }

    private void clickOnCancelPackage(){
        if(!MathFriendzyHelper.isUserLogin(this)){
            MathFriendzyHelper.showLoginRegistrationDialog(this ,
                    lblYouMustRegisterLoginToAccess);
            return;
        }

        if(this.purchaseInfoResponse != null){
            if((MathFriendzyHelper.parseInt(this
                    .purchaseInfoResponse.getPackageId()) > 0)
                    && (!purchaseInfoResponse.getIsCancel()
                    .equalsIgnoreCase(CANCEL + ""))) {
                MathFriendzyHelper.yesNoConfirmationDialog(this ,
                        lblSureToCancalPackage , btnTitleYes ,
                        lblNo , new YesNoListenerInterface() {
                            @Override
                            public void onYes() {
                                cancelProfile(ActTutoringPackage.this);
                            }

                            @Override
                            public void onNo() {

                            }
                        });
                //this.cancelProfile(this);
            }else{
                MathFriendzyHelper.showWarningDialog(this , lblYouHaveNotPurchased);
            }
        }
    }

    private void cancelProfile(HttpResponseInterface responseInterface){
        if(!CommonUtils.isInternetConnectionAvailable(this)){
            CommonUtils.showInternetDialog(this);
            return ;
        }

        CancelProfileParam param = new CancelProfileParam();
        param.setProfileId(purchaseInfoResponse.getProfileId());
        new MyAsyckTask(ServerOperation
                .createPostRequestForCancelProfile(param)
                , null, ServerOperationUtil.CANCEL_PROFILE, this,
                responseInterface, ServerOperationUtil.SIMPLE_DIALOG , true ,
                getString(R.string.please_wait_dialog_msg))
                .execute();
    }

    private void updateUIAfterCancelProfileResponse(CancelProfileResponse response ,
                                                    int actionAfterCancel ,
                                                    GetPurchagePackagesResponse packageDetail){
        if(actionAfterCancel == FOR_UPGRADE) {
            if (response.getResponse().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)) {
                purchaseInfoResponse.setIsCancel(CANCEL + "");
                goForShoppingCartScreen(packageDetail);
            } else {
                MathFriendzyHelper.showWarningDialog(ActTutoringPackage.this,
                        response.getError());
            }
        }else if(actionAfterCancel == ONLY_CANCEL){
            if(response.getResponse().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)){
                purchaseInfoResponse.setIsCancel(CANCEL + "");
                this.setYouCurrentlyHaveText(this.studentPurchaseTime , purchaseInfoResponse);
                this.setVisibilityOfProfessionalTutorLayouts(purchaseInfoResponse);
                if(recurringAdapter != null){
                    recurringAdapter.setPurchaseButtonText(recurringPaymentButtonText);
                    recurringAdapter.notifyDataSetChanged();
                }
            }else{
                MathFriendzyHelper.showWarningDialog(this , response.getError());
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            switch(requestCode){
                case ACT_SHOPPING_CART_REQUEST:
                    totalPurchaseTime = data.getIntExtra("totalPurchaseTime", 0);
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private boolean isPackageCancelOrNotPurchased(){
        try {
            if (this.purchaseInfoResponse.getIsCancel().equalsIgnoreCase(CANCEL + "")
                    || this.purchaseInfoResponse.getIsTimePurchase() == MathFriendzyHelper.NO)
                return true;
            return false;
        }catch (Exception e){
            return false;
        }
    }
}
