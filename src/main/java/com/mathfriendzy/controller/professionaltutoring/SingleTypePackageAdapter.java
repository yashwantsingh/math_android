package com.mathfriendzy.controller.professionaltutoring;

import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.professionaltutoring.GetPurchagePackagesResponse;
import com.mathfriendzy.utils.CommonUtils;

import java.util.ArrayList;

/**
 * Created by root on 8/9/15.
 */
class SingleTypePackageAdapter extends BaseAdapter{

    private LayoutInflater mInflater = null;
    private ListView lstView = null;
    private Context context = null;
    private PackageAdapterCallback callback = null;
    private ViewHolder vHolder = null;
    private ArrayList<GetPurchagePackagesResponse> singleTypePackages = null;
    private String dollar = "$";
    private String hour = "hour";
    private String lblPurchase = null;

    public SingleTypePackageAdapter(Context context  , ListView lstView ,
                                    PackageAdapterCallback callback ,
                                    ArrayList<GetPurchagePackagesResponse> singleTypePackages
            , String lblPurchase){
        this.lstView = lstView;
        mInflater = LayoutInflater.from(context);
        this.context = context;
        this.callback = callback;
        this.singleTypePackages = singleTypePackages;
        this.lblPurchase = lblPurchase;
    }

    @Override
    public int getCount() {
        this.calculateListViewHeight(singleTypePackages.size());
        return singleTypePackages.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        if(view == null){
            view = mInflater.inflate(R.layout.tutoring_package_custome_quantity_layout , null);
            vHolder = new ViewHolder();
            vHolder.txtMinutesValue = (TextView) view.findViewById(R.id.txtMinutesValue);
            vHolder.txtQuantityValue = (EditText) view.findViewById(R.id.txtQuantityValue);
            vHolder.txtPriceValue = (TextView) view.findViewById(R.id.txtPriceValue);
            vHolder.txtTotalValue = (TextView) view.findViewById(R.id.txtTotalValue);
            vHolder.btnPurchase = (Button) view.findViewById(R.id.btnPurchase);
            view.setTag(vHolder);
        }else{
            vHolder = (ViewHolder) view.getTag();
        }
        vHolder.txtMinutesValue.setText(this.getMinutes
                (singleTypePackages.get(position).getTimePurchased()));
        vHolder.txtPriceValue.setText(dollar + singleTypePackages.get(position).getTermPrice());
        vHolder.txtTotalValue.setText(dollar + singleTypePackages.get(position).getTotalPrice());
        vHolder.btnPurchase.setText(lblPurchase);
        vHolder.txtQuantityValue.setText(singleTypePackages.get(position).getQuantity() + "");

        vHolder.btnPurchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int quantity = MathFriendzyHelper.parseInt(vHolder
                        .txtQuantityValue.getText().toString());
                singleTypePackages.get(position).setQuantity(quantity);
                callback.clickOnPurchase(singleTypePackages.get(position));
            }
        });
        vHolder.txtQuantityValue.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    int termPrice = singleTypePackages.get(position).getTermPrice();
                    int quantity = MathFriendzyHelper.parseInt
                            (vHolder.txtQuantityValue.getText().toString());
                    int totalPrice = termPrice * quantity;
                    singleTypePackages.get(position).setTotalPrice(totalPrice);
                    singleTypePackages.get(position).setQuantity(quantity);
                    SingleTypePackageAdapter.this.notifyDataSetChanged();
                }
                return false;
            }
        });
        return view;
    }

    private void calculateListViewHeight(int numberOfCounts){
        ViewGroup.LayoutParams lp = lstView.getLayoutParams();
        int singleItemHeight = (int)context.getResources().
                getDimension(R.dimen.tutoring_single_type_package_layout_height_for_calculation);
        int totalHeight = (singleItemHeight * numberOfCounts);
        lp.height = totalHeight;
        lstView.setLayoutParams(lp);
    }

    private String getMinutes(int purchaseTime){
        try{
            return ((int)purchaseTime / 60) + "";
        }catch(Exception e){
            e.printStackTrace();
            return "0";
        }
    }

    private class ViewHolder{
        private TextView txtMinutesValue;
        private EditText txtQuantityValue;
        private TextView txtPriceValue;
        private TextView txtTotalValue;
        private Button btnPurchase;
    }
}
