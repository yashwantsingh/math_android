package com.mathfriendzy.controller.professionaltutoring;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.helper.YesNoListenerInterface;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.professionaltutoring.AddPurchaseTimeParam;
import com.mathfriendzy.model.professionaltutoring.CancelProfileParam;
import com.mathfriendzy.model.professionaltutoring.CancelProfileResponse;
import com.mathfriendzy.model.professionaltutoring.GetPurchagePackagesResponse;
import com.mathfriendzy.model.professionaltutoring.GetPurchaseTimeInfoResponse;
import com.mathfriendzy.model.professionaltutoring.PurchaseTimeCallback;
import com.mathfriendzy.model.professionaltutoring.UpdatePurchaseTimeResponse;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.mypaypal.MyPayPalCallback;
import com.mathfriendzy.mypaypal.MyPayPalPayment;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;

import org.json.JSONObject;

public class ActShoppingCart extends ActBase implements MyPayPalCallback{

    private final String TAG = this.getClass().getSimpleName();
    private TextView txtYourShoppingCart = null;
    private TextView txtDescription = null;
    private TextView txtAmount = null;
    private TextView txtDescriptionValue = null;
    private TextView txtAmountValue = null;
    private GetPurchagePackagesResponse packageDetail = null;
    private String dollar = "$";
    private Button btnPurchase = null;
    private int totalPurchaseTime = 0;
    private UserPlayerDto selectedPlayer = null;
    private int totalTimePurchaseInMinutes = 0;
    private float totalPriceInDollar = 0;
    private String descriptionText = null;
    private String lblMinutes = null;
    private int totalPurchaseTimeInSeconds = 0;
    private String lblYourTransactionWasSuccessFull = "Your transaction was successful!";
    //for paypal configuration , only for single payment
    private MyPayPalPayment payPalPaymentObj = null;
    private final int START_RECURRING_PAYMENT_URL_REQUEST = 90002;
    private final int START_PAY_WITH_CARD_REQUEST = 90003;
    private GetPurchaseTimeInfoResponse purchaseInfoResponse = null;
    //package status
    private final int ACTIVE = 1;
    private final int PENDING = 2;
    private final int CANCEL = 3;
    private boolean isUpgradeRecurringPackage = false;

    private String lblWeHaveCancelledPackage = null;
    private String btnTitleYes = null;
    private String lblNo = null;
    private Button btnPayWithCard = null;

    private String lblPay = "Pay";
    private String lblPayWithPaypal = "Pay With Paypal";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_shopping_cart);

        CommonUtils.printLog(TAG , "inside onCreate()");
        selectedPlayer = this.getPlayerData();

        this.init();
        this.getIntentValues();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.initializeDisplayDetail();
        this.setShoppingCartData();
        this.startPayPalServiceService();
        this.setPayWithButtonVisibility();
        this.doPaymentForSinglePurchaseAtStarting();

        CommonUtils.printLog(TAG , "outside onCreate()");
    }

    private void initializeDisplayDetail() {
        this.initializeDescriptionText();
        this.initializePriceInDollar();
    }

    private void init(){
        payPalPaymentObj = new MyPayPalPayment(this , this);
    }

    private void startPayPalServiceService(){
        payPalPaymentObj.startService();
    }

    private void stopPayPalService(){
        payPalPaymentObj.stopService();
    }

    private void getIntentValues() {
        packageDetail = (GetPurchagePackagesResponse)
                this.getIntent().getSerializableExtra("packageDetail");
        isUpgradeRecurringPackage = this.getIntent().getBooleanExtra("isUpgradeRecurringPackage" ,
                false);
        purchaseInfoResponse = (GetPurchaseTimeInfoResponse)
                this.getIntent().getSerializableExtra("purchaseInfoResponse");
        this.initializeDescriptionText();
        this.initializePriceInDollar();
    }

    private void initializeDescriptionText(){
        try{
            totalPurchaseTimeInSeconds = packageDetail.getTimePurchased() * packageDetail.getQuantity();
            if(packageDetail.getType().equalsIgnoreCase(GetPurchagePackagesResponse.SINGLE_TYPE)){
                totalTimePurchaseInMinutes = (int)packageDetail.getTimePurchased() / 60;
                descriptionText =  (totalTimePurchaseInMinutes * packageDetail.getQuantity())
                        + " " + lblMinutes;
            }else{
                descriptionText = packageDetail.getDescription();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Private initialize total price
     */
    private void initializePriceInDollar(){
        try{
            if(packageDetail.getType().equalsIgnoreCase(GetPurchagePackagesResponse.SINGLE_TYPE)) {
                totalPriceInDollar = packageDetail.getTermPrice()
                        * packageDetail.getQuantity();
            }else{
                totalPriceInDollar = packageDetail.getTotalPrice();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void setWidgetsReferences() {
        CommonUtils.printLog(TAG , "inside setWidgetsReferences()");

        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        txtYourShoppingCart = (TextView) findViewById(R.id.txtYourShoppingCart);
        txtDescription = (TextView) findViewById(R.id.txtDescription);
        txtAmount = (TextView) findViewById(R.id.txtAmount);
        txtDescriptionValue = (TextView) findViewById(R.id.txtDescriptionValue);
        txtAmountValue = (TextView) findViewById(R.id.txtAmountValue);
        btnPurchase = (Button) findViewById(R.id.btnPurchase);
        btnPayWithCard = (Button) findViewById(R.id.btnPayWithCard);

        CommonUtils.printLog(TAG , "outside setWidgetsReferences()");
    }

    @Override
    protected void setListenerOnWidgets() {
        CommonUtils.printLog(TAG , "inside setListenerOnWidgets()");
        btnPurchase.setOnClickListener(this);
        btnPayWithCard.setOnClickListener(this);
        CommonUtils.printLog(TAG , "outside setListenerOnWidgets()");
    }

    @Override
    protected void setTextFromTranslation() {
        CommonUtils.printLog(TAG , "inside setTextFromTranslation()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblPaypalGateway"));
        txtYourShoppingCart.setText(transeletion.
                getTranselationTextByTextIdentifier("lblYourShoppingCart"));
        txtDescription.setText(transeletion.getTranselationTextByTextIdentifier("lblDescription"));
        txtAmount.setText(transeletion.getTranselationTextByTextIdentifier("lblAmount"));
        lblMinutes = transeletion.getTranselationTextByTextIdentifier("lblMinutes");
        lblWeHaveCancelledPackage = transeletion.getTranselationTextByTextIdentifier("lblWeHaveCancelledPackage");
        btnTitleYes = transeletion.getTranselationTextByTextIdentifier("btnTitleYes");
        lblNo = transeletion.getTranselationTextByTextIdentifier("lblNo");
        btnPayWithCard.setText(transeletion.getTranselationTextByTextIdentifier("lblPayWithCreditCard"));
        //lblPay = transeletion.getTranselationTextByTextIdentifier("lblPay");
        //lblPayWithPaypal = transeletion.getTranselationTextByTextIdentifier("lblPayWithPaypal");
        transeletion.closeConnection();
        CommonUtils.printLog(TAG , "outside setTextFromTranslation()");
    }

    private void setShoppingCartData(){
        txtDescriptionValue.setText(descriptionText);
        txtAmountValue.setText(dollar + MathFriendzyHelper
                .getFloatWithTwoDigitAfterDecimalPoint(totalPriceInDollar));
    }

    private void setPayWithButtonVisibility(){
        if(packageDetail.getType().equalsIgnoreCase
                (GetPurchagePackagesResponse.SINGLE_TYPE)){
            btnPurchase.setText(lblPay);
            btnPayWithCard.setVisibility(Button.GONE);
        }else{
            btnPurchase.setText(lblPayWithPaypal);
            btnPayWithCard.setVisibility(Button. VISIBLE);
        }
    }

    private void doPaymentForSinglePurchaseAtStarting(){
        if(packageDetail.getType().equalsIgnoreCase
                (GetPurchagePackagesResponse.SINGLE_TYPE)) {
            this.clickOnPurchase();
        }
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {

    }

    private String getCheckOutCustomDetail(){
        try{
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("uId" , selectedPlayer.getParentUserId());
            jsonObject.put("pId" , selectedPlayer.getPlayerid());
            jsonObject.put("packageId" , packageDetail.getId());
            jsonObject.put("time" , totalPurchaseTimeInSeconds);
            return jsonObject.toString();
        }catch(Exception e){
            return "";
        }
    }

    private void clickOnPurchase(){
        if(CommonUtils.isInternetConnectionAvailable(this)) {
            if(packageDetail.getType().equalsIgnoreCase(GetPurchagePackagesResponse.SINGLE_TYPE)){
                payPalPaymentObj.doPayment(totalPriceInDollar + ""
                        , MyPayPalPayment.USD_CURRENCY_TYPE , descriptionText);
            }else {
                this.cancelProfile(new HttpResponseInterface() {
                    @Override
                    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                        if (httpResponseBase != null) {
                            CancelProfileResponse response = (CancelProfileResponse) httpResponseBase;
                            if (response.getResponse().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)) {
                                purchaseInfoResponse.setIsCancel(CANCEL + "");
                                openUrlInWevView();
                            } else {
                                MathFriendzyHelper.showWarningDialog(ActShoppingCart.this,
                                        response.getError());
                            }
                        } else {
                            openUrlInWevView();
                        }
                    }
                });
                //openUrlInWevView();
            }
        }else{
            CommonUtils.showInternetDialog(this);
        }
    }

    private void openUrlInWevView(){
        String recurringPaymentCheckOutUrl = ICommonUtils.PAYPAL_CHECKOUT
                + "amount=" + totalPriceInDollar + "&itemName=" +
                descriptionText
                +"&customParams="+ this.getCheckOutCustomDetail();
        CommonUtils.printLog(TAG , "Url " + recurringPaymentCheckOutUrl);
        MathFriendzyHelper.openUrlInWebViewForResult(ActShoppingCart.this ,
                recurringPaymentCheckOutUrl , START_RECURRING_PAYMENT_URL_REQUEST , true);
    }

    private void clickOnPayWithCard(){
        Intent intent = new Intent(this , ActCreditCardInfo.class);
        intent.putExtra("packageDetail" , packageDetail);
        intent.putExtra("totalPriceInDollar" , totalPriceInDollar);
        intent.putExtra("descriptionText" , descriptionText);
        intent.putExtra("totalPurchaseTimeInSeconds" , totalPurchaseTimeInSeconds);
        startActivityForResult(intent , START_PAY_WITH_CARD_REQUEST);
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnPurchase:
                this.clickOnPurchase();
                break;
            case R.id.btnPayWithCard:
                this.cancelProfile(new HttpResponseInterface() {
                    @Override
                    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                        if (httpResponseBase != null) {
                            CancelProfileResponse response = (CancelProfileResponse) httpResponseBase;
                            if (response.getResponse().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)) {
                                purchaseInfoResponse.setIsCancel(CANCEL + "");
                                clickOnPayWithCard();
                            } else {
                                MathFriendzyHelper.showWarningDialog(ActShoppingCart.this,
                                        response.getError());
                            }
                        } else {
                            clickOnPayWithCard();
                        }
                    }
                });
                break;
        }
    }

    private void clickOnBackPress(){
       /*if(packageDetail.getType().
                equalsIgnoreCase(GetPurchagePackagesResponse.RECURRING_TYPE)){
            if(isUpgradeRecurringPackage){
                MathFriendzyHelper.yesNoConfirmationDialog(this , lblWeHaveCancelledPackage ,
                        btnTitleYes , lblNo , new YesNoListenerInterface() {
                            @Override
                            public void onYes() {
                                goOnBackPress();
                            }

                            @Override
                            public void onNo() {

                            }
                        });
            }else{
                this.goOnBackPress();
            }
        }else{*/
        this.goOnBackPress();
        //}
    }

    private void goOnBackPress(){
        Intent intent = new Intent();
        intent.putExtra("totalPurchaseTime" , totalPurchaseTime);
        setResult(RESULT_OK , intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        this.clickOnBackPress();
    }

    @Override
    protected void onDestroy() {
        this.stopPayPalService();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            switch(requestCode){
                case MyPayPalPayment.PAYPAL_OPEN_PAYMENT_SCREEN_REQUEST:
                    payPalPaymentObj.onActivityResult(requestCode, resultCode, data);
                    break;
                case START_RECURRING_PAYMENT_URL_REQUEST:
                    finish();
                    break;
                case START_PAY_WITH_CARD_REQUEST:
                    finish();
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPaymentSuccess(String successString) {
        if(CommonUtils.isInternetConnectionAvailable(this)) {
            if (selectedPlayer != null) {
                int purchaseTime = totalPurchaseTimeInSeconds;
                AddPurchaseTimeParam param = new AddPurchaseTimeParam();
                param.setAction("addPurchasedTimeForPlayer");
                param.setUserId(selectedPlayer.getParentUserId());
                param.setPlayerId(selectedPlayer.getPlayerid());
                param.setPurchaseTime(purchaseTime);
                param.setPurchaseInfo(successString);
                MathFriendzyHelper.updatePurchaseTimeForStudent(
                        this , param , new PurchaseTimeCallback() {
                            @Override
                            public void onPurchase
                                    (UpdatePurchaseTimeResponse response) {
                                totalPurchaseTime = response.getTime();
                                MathFriendzyHelper.showToast(ActShoppingCart.this ,
                                        lblYourTransactionWasSuccessFull);
                            }
                        });
            }
        }else{
            CommonUtils.showInternetDialog(this);
        }
    }

    @Override
    public void onFailureOccurred(String failureString) {
        MathFriendzyHelper.showToast(this , failureString);
    }

    @Override
    public void onResultCancel(String resultCancelString) {
        MathFriendzyHelper.showToast(this , resultCancelString);
    }

    @Override
    public void onPaymentExtrasInvalid(String invalidString) {
        MathFriendzyHelper.showToast(this , invalidString);
    }

    private void cancelProfile(final HttpResponseInterface responseInterface){
        if(!CommonUtils.isInternetConnectionAvailable(this)){
            CommonUtils.showInternetDialog(this);
            return ;
        }
        if(this.purchaseInfoResponse != null){
            if((MathFriendzyHelper.parseInt(this
                    .purchaseInfoResponse.getPackageId()) > 0)
                    && (!purchaseInfoResponse.getIsCancel()
                    .equalsIgnoreCase(CANCEL + ""))) {
                MathFriendzyHelper.yesNoConfirmationDialog(this , lblWeHaveCancelledPackage ,
                        btnTitleYes , lblNo , new YesNoListenerInterface() {
                            @Override
                            public void onYes() {
                                CancelProfileParam param = new CancelProfileParam();
                                param.setProfileId(purchaseInfoResponse.getProfileId());
                                new MyAsyckTask(ServerOperation
                                        .createPostRequestForCancelProfile(param)
                                        , null, ServerOperationUtil.CANCEL_PROFILE, ActShoppingCart.this,
                                        responseInterface, ServerOperationUtil.SIMPLE_DIALOG , true ,
                                        getString(R.string.please_wait_dialog_msg))
                                        .execute();
                            }
                            @Override
                            public void onNo() {

                            }
                        });
            }else{
                responseInterface.serverResponse(null , -1);
            }
        }else{
            responseInterface.serverResponse(null , -1);
        }
    }
}

