package com.mathfriendzy.controller.tutoringsession;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.tutoringsessions.TutoringSessionsForStudentResponse;
import com.mathfriendzy.serveroperation.serveroperationparamresponseclasses.GetActiveInActiveStatusOfStudentForTutorResponse;

import java.util.ArrayList;

/**
 * Created by root on 22/7/15.
 */
public class TutoringSessionStudentListAdapter extends BaseAdapter{

    private LayoutInflater mInFlator = null;
    private ArrayList<TutoringSessionsForStudentResponse> originalList = null;
    private ArrayList<TutoringSessionsForStudentResponse> filterList =
            new ArrayList<TutoringSessionsForStudentResponse>();
    private final String SERVER_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private final String CONVERTED_DATE_FORMAT = "MMM. dd, yyyy hh:mm aa";
    private final String CONVERTED_DATE_FORMAT_FOR_TITLE = "MMM. dd, yyyy";
    private String filterKey = "";
    private ViewHolder vHolder = null;
    private String lblAnonymous ;
    private String mfLblProblem ;
    private TutoringSessionAdapterCallback callback = null;

    public static final int VIEW_TAG = 1;

    TutoringSessionStudentListAdapter(Context context ,
                                      ArrayList<TutoringSessionsForStudentResponse> originalList
            , String lblAnonymous , String mfLblProblem , TutoringSessionAdapterCallback callback){
        this.lblAnonymous = lblAnonymous;
        this.mfLblProblem = mfLblProblem;
        this.callback = callback;
        mInFlator = LayoutInflater.from(context);
        this.originalList = originalList;
        this.filterList(filterKey);
    }

    public void addRecords(ArrayList<TutoringSessionsForStudentResponse> list){
        originalList.addAll(list);
        this.filterList(filterKey);
    }

    @Override
    public int getCount() {
        return filterList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        if(view == null){
            view = mInFlator.inflate(R.layout.tutoring_session_list_item_layout , null);
            vHolder = new ViewHolder();
            vHolder.txtStudentName = (TextView) view.findViewById(R.id.txtStudentName);
            vHolder.txtStudentLastModified = (TextView) view.findViewById(R.id.txtStudentLastModified);
            vHolder.txtStudentOnline = (TextView) view.findViewById(R.id.txtStudentOnline);
            vHolder.txtStudentView = (TextView) view.findViewById(R.id.txtStudentView);
            vHolder.txtTitle = (TextView) view.findViewById(R.id.txtTitle);
            vHolder.imgStar1 = (ImageView) view.findViewById(R.id.imgStar1);
            vHolder.imgStar2 = (ImageView) view.findViewById(R.id.imgStar2);
            vHolder.imgStar3 = (ImageView) view.findViewById(R.id.imgStar3);
            vHolder.imgStar4 = (ImageView) view.findViewById(R.id.imgStar4);
            vHolder.imgStar5 = (ImageView) view.findViewById(R.id.imgStar5);
            vHolder.tutoringStudentViewLayout = (RelativeLayout) view.findViewById(R.id.tutoringStudentViewLayout);
            view.setTag(vHolder);
        }else{
            vHolder = (ViewHolder) view.getTag();
        }

        this.setStudentName(vHolder , filterList.get(position));
        this.setRating(filterList.get(position).getRating() , vHolder.imgStar1 , vHolder.imgStar2 ,
                vHolder.imgStar3 , vHolder.imgStar4 , vHolder.imgStar5);
        vHolder.txtStudentLastModified.setText(MathFriendzyHelper.formatDataInGivenFormatWithDefaultTimeZone
                (filterList.get(position).getReqDate(), SERVER_DATE_FORMAT, CONVERTED_DATE_FORMAT));
        this.setOnLineStatus(vHolder , filterList.get(position));
        this.setOpponentInputStatus(vHolder , filterList.get(position));
        this.setTitle(vHolder , filterList.get(position));


        vHolder.tutoringStudentViewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(callback != null){
                    callback.onClick(VIEW_TAG , filterList.get(position));
                }
            }
        });
        return view;
    }

    public void filterList(String key){
        filterKey = key;
        filterList.clear();
        if(MathFriendzyHelper.isEmpty(key)){
            for(int i = 0 ; i < originalList.size() ; i ++ ){
                filterList.add(originalList.get(i));
            }
        }else{
            for(int i = 0 ; i < originalList.size() ; i ++ ){
                if(originalList.get(i).getPaidSession()
                        .equalsIgnoreCase(MathFriendzyHelper.YES + "")){
                    if (originalList.get(i).getPaidSessionTitle().toLowerCase().
                            contains(filterKey.toLowerCase()))
                        filterList.add(originalList.get(i));
                }else {
                    if (originalList.get(i).getHwTitle().toLowerCase().
                            contains(filterKey.toLowerCase()))
                        filterList.add(originalList.get(i));
                }
            }
        }
        this.notifyDataSetChanged();
    }

    /**
     * Change the layout to add time spent in the new layout
     * So change the student name as time spent text
     * @param viewHolder
     * @param connectedTutor
     */
    private void setStudentName(ViewHolder viewHolder ,
                                TutoringSessionsForStudentResponse connectedTutor) {
        try {
            /*if(connectedTutor.getIsAnonymous() == MathFriendzyHelper.YES){
                viewHolder.txtStudentName.setText(lblAnonymous);
            }else{
                viewHolder.txtStudentName.setText(connectedTutor.getTutorName());
            }*/
            viewHolder.txtStudentName.setText(MathFriendzyHelper
                    .getTimeSpentText(MathFriendzyHelper
                            .parseInt(connectedTutor.getTimeSpent())
                            + connectedTutor.getTimeSpentStudent()));
        }catch(Exception e){
            e.printStackTrace();
            viewHolder.txtStudentName.setText(0 + "");
        }
    }

    private void setOnLineStatus(ViewHolder viewHolder ,
                                 TutoringSessionsForStudentResponse connectedTutor){
        if(connectedTutor.getTutorOnline() == MathFriendzyHelper.YES){
            viewHolder.txtStudentOnline.setBackgroundResource(R.drawable.online_icon);
        }else {
            if (connectedTutor.getInActive() == MathFriendzyHelper.YES) {
                viewHolder.txtStudentOnline.setBackgroundResource(R.drawable.red_circle_icon);
            } else {
                viewHolder.txtStudentOnline.setBackgroundResource(R.drawable.offline_icon);
            }
        }
    }

    /**
     * Set opponent status
     * @param viewHolder
     * @param connectedTutor
     */
    private void setOpponentInputStatus(ViewHolder viewHolder ,
                                        TutoringSessionsForStudentResponse connectedTutor){
        if(connectedTutor.getOpponentInput() == MathFriendzyHelper.YES){
            viewHolder.txtStudentView.setBackgroundResource(R.drawable.small_pencil_with_green_background);
        }else{
            viewHolder.txtStudentView.setBackgroundResource(R.drawable.small_pencil);
        }
    }

    private void setTitle(ViewHolder viewHolder ,
                          TutoringSessionsForStudentResponse connectedTutor){
        try {
            if(connectedTutor.getPaidSession().equalsIgnoreCase(MathFriendzyHelper.YES + "")){
                viewHolder.txtTitle.setText(connectedTutor.getPaidSessionTitle());
            }else {
                if (MathFriendzyHelper.isEmpty(connectedTutor.getHwTitle())) {
                    viewHolder.txtTitle.setText("");
                    return;
                }
                String title = connectedTutor.getHwDate() + ", " + connectedTutor.getHwTitle()
                        + ", " + mfLblProblem + " #: " + connectedTutor.getQueNo();
                viewHolder.txtTitle.setText(title);
            }
        }catch(Exception e){
            e.printStackTrace();
            viewHolder.txtTitle.setText("");
        }
    }

    /**
     * Set rating
     * @param rating
     */
    private void setRating(int rating , ImageView imgStart1 ,
                           ImageView imgStart2 , ImageView imgStart3 , ImageView imgStart4 , ImageView imgStart5){
        switch(rating){
            case 0:
                imgStart1.setBackgroundResource(R.drawable.start_unselected);
                imgStart2.setBackgroundResource(R.drawable.start_unselected);
                imgStart3.setBackgroundResource(R.drawable.start_unselected);
                imgStart4.setBackgroundResource(R.drawable.start_unselected);
                imgStart5.setBackgroundResource(R.drawable.start_unselected);
                break;
            case 1:
                imgStart1.setBackgroundResource(R.drawable.star_selected);
                imgStart2.setBackgroundResource(R.drawable.start_unselected);
                imgStart3.setBackgroundResource(R.drawable.start_unselected);
                imgStart4.setBackgroundResource(R.drawable.start_unselected);
                imgStart5.setBackgroundResource(R.drawable.start_unselected);
                break;
            case 2:
                imgStart1.setBackgroundResource(R.drawable.star_selected);
                imgStart2.setBackgroundResource(R.drawable.star_selected);
                imgStart3.setBackgroundResource(R.drawable.start_unselected);
                imgStart4.setBackgroundResource(R.drawable.start_unselected);
                imgStart5.setBackgroundResource(R.drawable.start_unselected);
                break;
            case 3:
                imgStart1.setBackgroundResource(R.drawable.star_selected);
                imgStart2.setBackgroundResource(R.drawable.star_selected);
                imgStart3.setBackgroundResource(R.drawable.star_selected);
                imgStart4.setBackgroundResource(R.drawable.start_unselected);
                imgStart5.setBackgroundResource(R.drawable.start_unselected);
                break;
            case 4:
                imgStart1.setBackgroundResource(R.drawable.star_selected);
                imgStart2.setBackgroundResource(R.drawable.star_selected);
                imgStart3.setBackgroundResource(R.drawable.star_selected);
                imgStart4.setBackgroundResource(R.drawable.star_selected);
                imgStart5.setBackgroundResource(R.drawable.start_unselected);
                break;
            case 5:
                imgStart1.setBackgroundResource(R.drawable.star_selected);
                imgStart2.setBackgroundResource(R.drawable.star_selected);
                imgStart3.setBackgroundResource(R.drawable.star_selected);
                imgStart4.setBackgroundResource(R.drawable.star_selected);
                imgStart5.setBackgroundResource(R.drawable.star_selected);
                break;
        }
    }

    private class ViewHolder{
        private TextView txtStudentName;
        private TextView txtStudentLastModified;
        private TextView txtStudentOnline;
        private TextView txtStudentView;
        private ImageView imgStar1;
        private ImageView imgStar2;
        private ImageView imgStar3;
        private ImageView imgStar4;
        private ImageView imgStar5;
        private TextView txtTitle;
        private RelativeLayout tutoringStudentViewLayout;
    }


    public ArrayList<TutoringSessionsForStudentResponse> getTutoringSessionList(){
        return this.filterList;
    }


    public void updatedStatusAndNotify(ArrayList<GetActiveInActiveStatusOfStudentForTutorResponse>
                                               updatedActiveInActiveList){
        if(this.filterList != null && this.filterList.size() > 0) {
            for (int i = 0; i < this.filterList.size() ; i ++ ){
                TutoringSessionsForStudentResponse student =  this.getActiveInActiveStatus(filterList.get(i)
                        , updatedActiveInActiveList);
                if(student != null){
                    filterList.get(i).setInActive(student.getInActive());
                    filterList.get(i).setTutorOnline(student.getTutorOnline());
                }else{
                    filterList.get(i).setInActive(1);
                    filterList.get(i).setTutorOnline(MathFriendzyHelper.NO);
                }
            }
            this.notifyDataSetChanged();
        }
    }

    private TutoringSessionsForStudentResponse getActiveInActiveStatus(TutoringSessionsForStudentResponse student ,
                                                                      ArrayList<GetActiveInActiveStatusOfStudentForTutorResponse>
                                                                              updatedActiveInActiveList){
        for(int i = 0 ; i < updatedActiveInActiveList.size() ; i ++ ){
            GetActiveInActiveStatusOfStudentForTutorResponse status = updatedActiveInActiveList.get(i);
            if(/*status.getUserId().equalsIgnoreCase(student.getTutorUid()) &&*/
                    status.getPlayerId().equalsIgnoreCase(student.getTutorPid())){
                student.setInActive(0);
                if(status.getOnlineRequestId() == student.getRequestId()){
                    student.setTutorOnline(MathFriendzyHelper.YES);
                }else{
                    student.setTutorOnline(MathFriendzyHelper.NO);
                }
                return student;
            }
        }
        return null;
    }
}
