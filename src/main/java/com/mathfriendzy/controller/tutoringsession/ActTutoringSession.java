package com.mathfriendzy.controller.tutoringsession;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.tutor.ActTutorSession;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.tutoringsessions.TutoringSessionsForStudentParam;
import com.mathfriendzy.model.tutoringsessions.TutoringSessionsForStudentResponse;
import com.mathfriendzy.notification.HelpSetudentResponse;
import com.mathfriendzy.notification.TutorImageUpdateNotif;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.serveroperation.serveroperationparamresponseclasses.GetActiveInActiveStatusOfStudentForTutorResponse;
import com.mathfriendzy.socketprograming.ClientCallback;
import com.mathfriendzy.socketprograming.MyGlobalWebSocket;
import com.mathfriendzy.utils.CommonUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ActTutoringSession extends ActBase implements ClientCallback {

    private final String TAG = this.getClass().getSimpleName();

    private TextView txtTutoringSessionShowAll = null;
    private EditText edtSearch = null;
    private Button btnSearch = null;
    private TextView txtSearchOnHomeworkDescription = null;
    private TextView txtStudentName = null;
    private TextView txtStudentRating = null;
    private TextView txtStudentLastModified = null;
    private TextView txtStudentOnline = null;
    private TextView txtStudentView = null;
    private int offset = 0;
    private final int MAX_LIMIT = 20;
    private Button btnShowMore = null;
    private ListView studentList = null;

    private UserPlayerDto selectedPlayer = null;
    private TutoringSessionStudentListAdapter adapter = null;

    private String lblAnonymous = null;
    private String mfLblProblem = null;

    private String alertMsgNoTutoringSessionFound = null;

    private final int OPEN_TUTOR_AREA_REQUEST = 1001;
    private TutoringSessionsForStudentResponse clickedObj = null;

    private ListView studentListWithTitleSearch = null;
    private RelativeLayout btnShowMoreLayout = null;

    private static ActTutoringSession currentObj = null;

    private final String USER_PLAYER_IS_SEPARATOR = "_";
    private Handler getStudentActiveInActiveStatus = null;
    private Runnable runnableThread = null;
    private final long GET_ACTIVE_INACTIVE_STATUS_REFRESH_TIME = 1000 * 10;

    private boolean isViewMainListView = false;
    private TutoringSessionStudentListAdapter searchListAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_tutoring_session);

        CommonUtils.printLog(TAG , "inside onCreate()");

        this.initializeCurrentObj();
        selectedPlayer = this.getPlayerData();

        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.loadTutoringSessionForStudent();
        this.initializeHandlerAndStart();

        CommonUtils.printLog(TAG , "outside onCreate()");
    }

    /**
     * Load the tutoring sessions for the student
     */
    private void loadTutoringSessionForStudent() {
        if(CommonUtils.isInternetConnectionAvailable(this)) {
            if(selectedPlayer != null) {
                TutoringSessionsForStudentParam param =
                        new TutoringSessionsForStudentParam();
                param.setAction("tutoringSessionsForStudent");
                param.setUserId(selectedPlayer.getParentUserId());
                param.setPlayerId(selectedPlayer.getPlayerid());
                param.setLimit(MAX_LIMIT);
                param.setOffset(offset);
                new MyAsyckTask(ServerOperation.createPostRequestForLoadServiceTime(param)
                        , null, ServerOperationUtil.GET_TUTOR_SESSION_FOR_STUDENT_REQUEST, this,
                        this, ServerOperationUtil.SIMPLE_DIALOG , true ,
                        getString(R.string.please_wait_dialog_msg))
                        .execute();
            }
        }else{
            CommonUtils.showInternetDialog(this);
        }
    }

    private void setListAdapter(ArrayList<TutoringSessionsForStudentResponse> list){
        this.setListViewVisibility(true);
        if(adapter == null){
            if(list.size() == 0) {
                MathFriendzyHelper.showWarningDialog(this,
                        alertMsgNoTutoringSessionFound, new HttpServerRequest() {
                            @Override
                            public void onRequestComplete() {
                                finish();
                            }
                        });
            }

            adapter = new TutoringSessionStudentListAdapter(this , list ,
                    lblAnonymous , mfLblProblem , new TutoringSessionAdapterCallback() {
                @Override
                public void onClick(int tag , TutoringSessionsForStudentResponse clickObj) {
                    switch(tag){
                        case TutoringSessionStudentListAdapter.VIEW_TAG:
                            clickOnView(clickObj);
                            break;
                    }
                }
            });
            studentList.setAdapter(adapter);
        }else{
            adapter.addRecords(list);
            adapter.notifyDataSetChanged();
        }
    }

    private void setListForSearch(ArrayList<TutoringSessionsForStudentResponse> list){
        this.setListViewVisibility(false);
        searchListAdapter = new TutoringSessionStudentListAdapter(this , list ,
                lblAnonymous , mfLblProblem , new TutoringSessionAdapterCallback() {
            @Override
            public void onClick(int tag , TutoringSessionsForStudentResponse clickObj) {
                switch(tag){
                    case TutoringSessionStudentListAdapter.VIEW_TAG:
                        clickOnView(clickObj);
                        break;
                }
            }
        });
        studentListWithTitleSearch.setAdapter(searchListAdapter);
    }

    /**
     * Click To View
     * @param clickObj
     */
    private void clickOnView(TutoringSessionsForStudentResponse clickObj){
        //MathFriendzyHelper.showWarningDialog(this , clickObj.getReqDate());
        this.clickedObj = clickObj;
        if(clickObj.getOpponentInput() == MathFriendzyHelper.YES){
            clickObj.setOpponentInput(MathFriendzyHelper.NO);
            this.updateTutorSessionActiveInput();
        }
        this.notifyAdapter();
        Intent intent = new Intent(this , ActTutorSession.class);
        intent.putExtra("isForTutoringSession" , true);
        intent.putExtra("tutoringSessionData" , clickObj);
        startActivityForResult(intent, OPEN_TUTOR_AREA_REQUEST);
    }

    /**
     * Update tutoring session input status
     */
    private void updateTutorSessionActiveInput(){
        int totalNumberOfActiveInput = MathFriendzyHelper.getTutorSessionResponses(this,
                selectedPlayer.getParentUserId()
                , selectedPlayer.getPlayerid());
        if (totalNumberOfActiveInput > 0) {
            totalNumberOfActiveInput--;
            MathFriendzyHelper.saveTutorSessionResponse(this, selectedPlayer.getParentUserId()
                    , selectedPlayer.getPlayerid(), totalNumberOfActiveInput);
        }
    }

    @Override
    protected void setWidgetsReferences() {
        CommonUtils.printLog(TAG , "inside setWidgetsReferences()");

        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        txtTutoringSessionShowAll = (TextView) findViewById(R.id.txtTutoringSessionShowAll);
        txtSearchOnHomeworkDescription = (TextView) findViewById(R.id.txtSearchOnHomeworkDescription);
        txtStudentName = (TextView) findViewById(R.id.txtStudentName);
        txtStudentRating = (TextView) findViewById(R.id.txtStudentRating);
        txtStudentLastModified = (TextView) findViewById(R.id.txtStudentLastModified);
        txtStudentOnline = (TextView) findViewById(R.id.txtStudentOnline);
        txtStudentView = (TextView) findViewById(R.id.txtStudentView);
        edtSearch = (EditText) findViewById(R.id.edtSearch);
        btnSearch = (Button) findViewById(R.id.btnSearch);
        btnShowMore = (Button) findViewById(R.id.btnShowMore);
        studentList = (ListView) findViewById(R.id.studentList);
        studentListWithTitleSearch = (ListView) findViewById(R.id.studentListWithTitleSearch);
        btnShowMoreLayout = (RelativeLayout) findViewById(R.id.btnShowMoreLayout);

        txtSearchOnHomeworkDescription.setVisibility(View.GONE);
        CommonUtils.printLog(TAG , "outside setWidgetsReferences()");
    }

    @Override
    protected void setListenerOnWidgets() {

        CommonUtils.printLog(TAG , "inside setListenerOnWidgets()");

        btnShowMore.setOnClickListener(this);
        btnSearch.setOnClickListener(this);

        CommonUtils.printLog(TAG , "outside setListenerOnWidgets()");
    }

    @Override
    protected void setTextFromTranslation() {

        CommonUtils.printLog(TAG , "inside setTextFromTranslation()");
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("btnTutoringSession"));
        txtTutoringSessionShowAll.setText(transeletion.getTranselationTextByTextIdentifier("btnTutoringSession")
                + "\n" + transeletion.getTranselationTextByTextIdentifier("lblTutoringSessionDescription"));
        btnSearch.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleSearch"));
        txtSearchOnHomeworkDescription.setText(transeletion
                .getTranselationTextByTextIdentifier("lblTutoringSessionSearchDescription"));
        /*txtStudentName.setText(transeletion.getTranselationTextByTextIdentifier("lblStudent")
         + "'s " + transeletion.getTranselationTextByTextIdentifier("lblRegName"));*/

        /**
         * Change the layout to add time spent in the new layout
         * So change the student name as time spent text
         */
        txtStudentName.setText(transeletion.getTranselationTextByTextIdentifier("lblTimeSpent"));
        txtStudentRating.setText(transeletion.getTranselationTextByTextIdentifier("lblRating"));
        txtStudentLastModified.setText(transeletion.getTranselationTextByTextIdentifier("lblLastModified"));
        txtStudentOnline.setText(transeletion.getTranselationTextByTextIdentifier("lblOnlineOrNot"));
        txtStudentView.setText(transeletion.getTranselationTextByTextIdentifier("lblView"));
        btnShowMore.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleShowMore"));

        lblAnonymous = transeletion.getTranselationTextByTextIdentifier("lblAnonymous");
        mfLblProblem = transeletion.getTranselationTextByTextIdentifier("mfLblProblem");
        alertMsgNoTutoringSessionFound = transeletion
                .getTranselationTextByTextIdentifier("alertMsgNoTutoringSessionFound");

        edtSearch.setHint(transeletion
                .getTranselationTextByTextIdentifier("lblTutoringSessionSearchDescription"));
        transeletion.closeConnection();
        CommonUtils.printLog(TAG , "outside setTextFromTranslation()");
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
        if(requestCode == ServerOperationUtil.GET_TUTOR_SESSION_FOR_STUDENT_REQUEST){
            TutoringSessionsForStudentResponse response =
                    (TutoringSessionsForStudentResponse) httpResponseBase;
            if(response.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)){
                if(offset == 0){
                    adapter = null;
                }
                if(response.getList().size() >= MAX_LIMIT){
                    this.setVisibilityOfShowMoreButton(true);
                }else{
                    this.setVisibilityOfShowMoreButton(false);
                }
                this.setListAdapter(response.getList());

                this.createGlobalRoomObjectOrGetUserStatus();
            }
        }else if(requestCode == ServerOperationUtil.GET_TUTORING_SESSION_WITH_TITLE_REQUEST){
            TutoringSessionsForStudentResponse response =
                    (TutoringSessionsForStudentResponse) httpResponseBase;
            if(response.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)) {
                if(response.getList() != null && response.getList().size() > 0) {
                    this.setListForSearch(response.getList());
                    this.createGlobalRoomObjectOrGetUserStatus();
                }else{
                    MathFriendzyHelper.showWarningDialog(this , alertMsgNoTutoringSessionFound);
                }
            }
        }
    }

    private void setVisibilityOfShowMoreButton(boolean bValue){
        if(bValue){
            btnShowMore.setVisibility(Button.VISIBLE);
        }else{
            btnShowMore.setVisibility(Button.INVISIBLE);
        }
    }

    private void clickOnShowMore(){
        offset = offset + MAX_LIMIT;
        this.loadTutoringSessionForStudent();
    }

    private void setListViewVisibility(boolean isViewMainListView){
        this.isViewMainListView = isViewMainListView;
        if(isViewMainListView){
            studentList.setVisibility(ListView.VISIBLE);
            studentListWithTitleSearch.setVisibility(ListView.GONE);
            btnShowMoreLayout.setVisibility(RelativeLayout.VISIBLE);
        }else{
            studentList.setVisibility(ListView.GONE);
            studentListWithTitleSearch.setVisibility(ListView.VISIBLE);
            btnShowMoreLayout.setVisibility(RelativeLayout.INVISIBLE);
        }
    }

    private void clickOnSearch(){
       /* String filterKey = edtSearch.getText().toString().trim();
        if(adapter != null){
            adapter.filterList(filterKey);
        }*/

        this.hideDeviceKeyboard();
        String filterKey = edtSearch.getText().toString().trim();
        if(MathFriendzyHelper.isEmpty(filterKey)){
            setListViewVisibility(true);
        }else{
            if(CommonUtils.isInternetConnectionAvailable(this)) {
                TutoringSessionsForStudentParam param =
                        new TutoringSessionsForStudentParam();
                param.setAction("tutoringSessionsForStudentWithTitle");
                param.setUserId(selectedPlayer.getParentUserId());
                param.setPlayerId(selectedPlayer.getPlayerid());
                param.setTitle(filterKey);
                new MyAsyckTask(ServerOperation.createPostRequestForGetTutorSessionByTitle(param)
                        , null, ServerOperationUtil.GET_TUTORING_SESSION_WITH_TITLE_REQUEST, this,
                        this, ServerOperationUtil.SIMPLE_DIALOG, true,
                        getString(R.string.please_wait_dialog_msg))
                        .execute();
            }else{
                /*if(adapter != null){
                    adapter.filterList(filterKey);
                }*/
                CommonUtils.showInternetDialog(this);
            }
        }

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnShowMore:
                this.clickOnShowMore();
                break;
            case R.id.btnSearch:
                this.clickOnSearch();
                break;
        }
    }

    private void notifyAdapter(){
        if(adapter != null){
            adapter.notifyDataSetChanged();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            switch(requestCode){
                case OPEN_TUTOR_AREA_REQUEST:
                    /*int numberOfStarRated = data.getIntExtra("numberOfStarRated" , 0);
                    int timeSpent = data.getIntExtra("timeSpent" , 0);
                    boolean isNotifyAdapter = false;
                    if(numberOfStarRated > 0 && clickedObj != null){
                        clickedObj.setRating(numberOfStarRated);
                        isNotifyAdapter = true;
                    }
                    if(timeSpent > 0 && clickedObj != null){
                        int totalTimeSpent = timeSpent
                                + MathFriendzyHelper.parseInt(clickedObj.getTimeSpent());
                        clickedObj.setTimeSpent(totalTimeSpent + "");
                        isNotifyAdapter = true;
                    }
                    if(isNotifyAdapter){
                        this.notifyAdapter();
                    }*/

                    //new change for the time spent update on 24/09/2015 ,
                    //above code commented
                    offset = 0;
                    this.loadTutoringSessionForStudent();
                    //end change
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initializeCurrentObj(){
        currentObj = this;
    }

    public static ActTutoringSession getCurrentObj(){
        return currentObj;
    }

    /**
     * Update the UI after the tutor responsed
     * @param response
     */
    public void updateUIAfterTutorInput(final HelpSetudentResponse response){
        try{
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    offset = 0;
                    loadTutoringSessionForStudent();
                }
            });
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Update the UI after the tutor responsed
     * @param response
     */
    public void updateUIAfterTutorInputForImageUpdate(final TutorImageUpdateNotif response){
        try{
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    offset = 0;
                    loadTutoringSessionForStudent();
                }
            });
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private ArrayList<String> getUniqueStudentDetail(){
        ArrayList<String> uniqueList = new ArrayList<String>();
        if(this.isViewMainListView) {
            if (adapter != null && adapter.getTutoringSessionList().size() > 0) {
                ArrayList<TutoringSessionsForStudentResponse> tutoringSessionList =
                        adapter.getTutoringSessionList();
                for (int i = 0; i < tutoringSessionList.size(); i++) {
                    TutoringSessionsForStudentResponse object = tutoringSessionList.get(i);
                    String uniqueKey = object.getTutorUid() + USER_PLAYER_IS_SEPARATOR + object.getTutorPid();
                    if (!uniqueList.contains(uniqueKey)) {
                        uniqueList.add(uniqueKey);
                    }
                }
            }
        }else{
            if (searchListAdapter != null && searchListAdapter.getTutoringSessionList().size() > 0) {
                ArrayList<TutoringSessionsForStudentResponse> tutoringSessionList =
                        searchListAdapter.getTutoringSessionList();
                for (int i = 0; i < tutoringSessionList.size(); i++) {
                    TutoringSessionsForStudentResponse object = tutoringSessionList.get(i);
                    String uniqueKey = object.getTutorUid() + USER_PLAYER_IS_SEPARATOR + object.getTutorPid();
                    if (!uniqueList.contains(uniqueKey)) {
                        uniqueList.add(uniqueKey);
                    }
                }
            }
        }
        return uniqueList;
    }

    private String getUniqueStudentIdJsonStringForTheList(ArrayList<String> studentList){
        try{
            JSONArray jsonArray = new JSONArray();
            for(int i = 0 ; i < studentList.size() ; i ++ ) {
                String key = studentList.get(i);
                String userId = key.split(USER_PLAYER_IS_SEPARATOR)[0];
                String playerId = key.split(USER_PLAYER_IS_SEPARATOR)[1];
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("userId" , userId);
                jsonObject.put("playerId" , playerId);
                jsonArray.put(jsonObject);
            }
            return jsonArray.toString();
        }catch (Exception e){
            e.printStackTrace();
            return "[]";
        }
    }

    private void initializeHandlerAndStart(){
        getStudentActiveInActiveStatus = new Handler();
        runnableThread = new Runnable() {
            @Override
            public void run() {
                getInactiveStatusOfPlayersForTutor();
                getStudentActiveInActiveStatus.postDelayed(this,
                        GET_ACTIVE_INACTIVE_STATUS_REFRESH_TIME);
            }
        };
        getStudentActiveInActiveStatus.postDelayed(runnableThread ,
                GET_ACTIVE_INACTIVE_STATUS_REFRESH_TIME);
    }

    private void removeActiveInActiveHandler(){
        if(getStudentActiveInActiveStatus != null){
            getStudentActiveInActiveStatus.removeCallbacksAndMessages(null);
        }
    }


    private void getInactiveStatusOfPlayersForTutor(){
        this.createGlobalRoomObjectOrGetUserStatus();
        /*GetActiveInActiveStatusForTutorParam param = new GetActiveInActiveStatusForTutorParam();
        param.setAction("getInactiveStatusOfTutorsForStudent");
        param.setData(this.getUniqueStudentIdJsonStringForTheList(this.getUniqueStudentDetail()));
        if(CommonUtils.isInternetConnectionAvailable(this)){
            new MyAsyckTask(ServerOperation.createPostRequestForGetInactiveStatusOfPlayersForTutor(param)
                    , null, ServerOperationUtil.GET_INACTIVE_STATUS_OF_PLAYER_FOR_TUTOR, this,
                    new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                            GetActiveInActiveStatusOfStudentForTutorResponse response =
                                    (GetActiveInActiveStatusOfStudentForTutorResponse) httpResponseBase;
                            if(response != null) {
                                if (adapter != null) {
                                    adapter.updatedStatusAndNotify(response.getList());
                                }
                            }
                            getStudentActiveInActiveStatus.postDelayed(runnableThread,
                                    GET_ACTIVE_INACTIVE_STATUS_REFRESH_TIME);
                        }
                    }, ServerOperationUtil.SIMPLE_DIALOG , false ,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        }*/
    }

    @Override
    public void onBackPressed() {
        this.removeActiveInActiveHandler();
        super.onBackPressed();
    }


    //Online Status via socket
    private void createGlobalRoomObjectOrGetUserStatus() {
        if(MyGlobalWebSocket.getInstance() != null
                && MyGlobalWebSocket.getInstance().isSocketConnected()){
            MyGlobalWebSocket.getInstance().initializeCallback(this);
            MyGlobalWebSocket.getInstance().getOnlineStatus(MathFriendzyHelper.SERVER_PREFIX ,
                    this.getUniqueStudentIdJsonForGlobalRoom(this.getUniqueStudentDetail()));
        }else{
            UserPlayerDto selectedPlayerData = MathFriendzyHelper.getSelectedPlayer(this);
            if(selectedPlayerData != null){
                MyGlobalWebSocket.getInstance(this , this , 0 , selectedPlayerData.getPlayerid());
            }
        }
    }

    private JSONArray getUniqueStudentIdJsonForGlobalRoom(ArrayList<String> studentList){
        JSONArray jsonArray = new JSONArray();
        try{
            for(int i = 0 ; i < studentList.size() ; i ++ ) {
                String key = studentList.get(i);
                String playerId = key.split(USER_PLAYER_IS_SEPARATOR)[1];
                jsonArray.put(playerId);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return jsonArray;
    }

    @Override
    public void onConnected() {
        MyGlobalWebSocket.getInstance().getOnlineStatus(MathFriendzyHelper.SERVER_PREFIX ,
                this.getUniqueStudentIdJsonForGlobalRoom(this.getUniqueStudentDetail()));
    }

    @Override
    public void onConnectionFail() {

    }

    @Override
    public void onMessageSent(boolean isSuccess) {

    }

    @Override
    public void onMessageRecieved(String message) {
        try {
            CommonUtils.printLog(TAG, "Message Recieve from socket " + message);
            GetActiveInActiveStatusOfStudentForTutorResponse response =
                    MathFriendzyHelper.getActiveInActiveResponse(message);
            if (response != null) {
                if (adapter != null) {
                    adapter.updatedStatusAndNotify(response.getList());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}


