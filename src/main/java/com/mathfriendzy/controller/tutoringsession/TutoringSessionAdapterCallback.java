package com.mathfriendzy.controller.tutoringsession;

import com.mathfriendzy.model.tutoringsessions.TutoringSessionsForStudentResponse;

/**
 * Created by root on 22/7/15.
 */
public interface TutoringSessionAdapterCallback {
    public void onClick(int tag , TutoringSessionsForStudentResponse clickObj);
}
