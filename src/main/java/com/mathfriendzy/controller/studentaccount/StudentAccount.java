package com.mathfriendzy.controller.studentaccount;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.homework.assignhomeworkquiz.ClassesAdapter;
import com.mathfriendzy.controller.managetutor.mystudent.MyStudentsOnItemClickListener;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.ClassSelectedListener;
import com.mathfriendzy.model.homework.SelectAssignHomeworkClassParam;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.managetutor.mystudents.GetMyStuentWithPasswordResponse;
import com.mathfriendzy.model.managetutor.mystudents.GetStudentWithPasswordParam;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.classes.ClassWithName;
import com.mathfriendzy.model.registration.classes.UserClasses;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;

import java.util.ArrayList;

import static com.mathfriendzy.utils.ICommonUtils.STUDENT_ACCOUNT;


/**
 *
 * This activity will show the student account
 * @author Yashwant Singh
 * This activity for new students account feature
 * added on 28 Apr 2014
 *
 */
public class StudentAccount extends ActBase implements OnClickListener {

    private TextView resultTitleMyStudents = null;
    private Button btnTitleBack = null;
    private ProgressDialog progressDialog = null;

    private TextView txtStudentdName = null;
    private ArrayList<RelativeLayout> layoutList = null;
    private final String TAG = this.getClass().getSimpleName();
    private int offSet = 0;

    //private TextView txtGrade 		= null;
    //private Spinner  spinnerGrade 	= null;
    private Button btnShowMore = null;
    private ListView lstTeacherStudents = null;
    private String gradeSelectedValue = "All";//by default All is selected changes in setGradeAdapterData() method

    private final int CHANGES_PASS_REQUEST = 1;

    //class title changes
    //private RegistereUserDto registerUserFromPreff = null;
    private TextView txtSelectClass = null;
    private RelativeLayout titleListLayout = null;
    private Button btnClassListDone = null;
    private ListView lstClassesList = null;
    private ClassesAdapter classAdapter = null;
    private StudentAccountAdapter adapter = null;
    private String selectedGrade = MathFriendzyHelper.ALL;
    private ArrayList<Integer> intGradeList = null;
    private int offset = 0;


    private RelativeLayout selectClassLayout = null;
    private TextView txtSelectClassValue = null;
    private TextView txtSubject = null;
    private TextView txtSubjectValues = null;
    private TextView txtClass = null;
    private TextView txtClassValues = null;

    private String defaultSelectedClassId = "0";

    private final int STUDENT_LIMIT = 30;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_students);

        if (STUDENT_ACCOUNT)
            Log.e(TAG, "inside onCreate()");

        //finalStudentList = new ArrayList<UserPlayerDto>();

        this.init();
        this.setWidgetsReferences();
        this.setTextValues();
        this.setListenerOnWidgets();
        this.loadInitialStudents();

        if (STUDENT_ACCOUNT)
            Log.e(TAG, "outside onCreate()");
    }

    private void init() {
        registerUserFromPreff = MathFriendzyHelper.getUserFromPreff(this);
        this.initClasses();
    }

    /**
     * This method set the widgets references from the layout to the widgets objects
     */
    protected void setWidgetsReferences() {
        if (STUDENT_ACCOUNT)
            Log.e(TAG, "inside setWidgetsReferences()");

        resultTitleMyStudents = (TextView) findViewById(R.id.resultTitleMyStudents);
        btnTitleBack = (Button) findViewById(R.id.btnTitleBack);

        //for show by Grade , on Apr 08 2014
        //txtGrade              = (TextView) findViewById(R.id.txtGrade);
        //spinnerGrade          = (Spinner) findViewById(R.id.spinnerGrade);
        btnShowMore = (Button) findViewById(R.id.btnShowMore);
        lstTeacherStudents = (ListView) findViewById(R.id.lstTeacherStudents);

        //remove the show more button from this screen , based on the 8/7/2015 issue
        this.setShowMoreButtonVisibility(false);

        //class title changes
        selectClassLayout = (RelativeLayout) findViewById(R.id.selectClassLayout);
        txtSelectClass = (TextView) findViewById(R.id.txtSelectClass);
        txtSelectClassValue = (TextView) findViewById(R.id.txtSelectClassValue);
        titleListLayout = (RelativeLayout) findViewById(R.id.titleListLayout);
        btnClassListDone = (Button) findViewById(R.id.btnClassListDone);
        lstClassesList = (ListView) findViewById(R.id.lstClassesList);

        txtSubject = (TextView) findViewById(R.id.txtSubject);
        txtSubjectValues = (TextView) findViewById(R.id.txtSubjectValues);
        txtClass = (TextView) findViewById(R.id.txtClass);
        txtClassValues = (TextView) findViewById(R.id.txtClassValues);

        if (STUDENT_ACCOUNT)
            Log.e(TAG, "outside setWidgetsReferences()");
    }

    /**
     * This method set the widgets text values from the Translation
     */
    private void setTextValues() {
        if (STUDENT_ACCOUNT)
            Log.e(TAG, "inside setTextValues()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        resultTitleMyStudents.setText(transeletion.getTranselationTextByTextIdentifier("resultTitleMyStudents"));
        btnTitleBack.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBack"));
        //txtGrade.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade") + ":");
        btnShowMore.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleShowMore"));

        //class title changes
        txtSelectClass.setText(transeletion.getTranselationTextByTextIdentifier("lblClass"));
        btnClassListDone.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleDone"));
        txtSelectClassValue.setHint(transeletion.getTranselationTextByTextIdentifier("lblClass")
                + " " + transeletion.getTranselationTextByTextIdentifier("lblClass"));
        transeletion.closeConnection();

        if (STUDENT_ACCOUNT)
            Log.e(TAG, "outside setTextValues()");
    }

    /**
     * this method set the listener on widgets
     */
    protected void setListenerOnWidgets() {
        if (STUDENT_ACCOUNT)
            Log.e(TAG, "inside setListenerOnWidgets()");

        btnTitleBack.setOnClickListener(this);
        btnShowMore.setOnClickListener(this);

        //class title changes
        selectClassLayout.setOnClickListener(this);
        btnClassListDone.setOnClickListener(this);

        if (STUDENT_ACCOUNT)
            Log.e(TAG, "outside setListenerOnWidgets()");
    }

    @Override
    protected void setTextFromTranslation() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnTitleBack:
                break;
            case R.id.btnShowMore:
                offset = offset + STUDENT_LIMIT;
                this.loadMyStudents(this.getCommaSeparatedClassIds(selectedClassList));
                break;
            case R.id.selectClassLayout:
                this.clickToSelectClass();
                break;
            /*case R.id.btnClassListDone:
                this.clickToClassSelectionDone();
                break;*/
        }
    }

    /**
     * Set visibility of show more button
     *
     * @param isVisible
     */
    private void setShowMoreButtonVisibility(boolean isVisible) {
        if (isVisible) {
            btnShowMore.setVisibility(Button.VISIBLE);
        } else {
            btnShowMore.setVisibility(Button.GONE);
        }
    }

    /**
     * Load the initial students for the first class for the teacher
     */
    private void loadInitialStudents(){
        try {
            if (classWithNames != null && classWithNames.size() > 0) {
                ArrayList<ClassWithName> initialClassSelectedList = new ArrayList<ClassWithName>();
                initialClassSelectedList.add(MathFriendzyHelper.getLastSelectedClass(this , classWithNames));
                this.setDataAfterClassSelection(initialClassSelectedList);
                //this.loadMyStudents(this.getCommaSeparatedClassIds(initialClassSelectedList));
            } else {

            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Load my students
     */
    private void loadMyStudents(String selectedClass) {
        if (CommonUtils.isInternetConnectionAvailable(this)) {
            GetStudentWithPasswordParam param = new GetStudentWithPasswordParam();
            param.setAction("getStudentsWithPasswordFromClass");
            param.setUserId(CommonUtils.getUserId(this));
            /*param.setOffset(offset);
            param.setStartDate("");
            param.setEndDate("");
            param.setLimit(STUDENT_LIMIT);*/
            param.setClassId(selectedClass);
            param.setForStudentAccount(true);
            /*if(registerUserFromPreff != null){
                param.setYear(registerUserFromPreff.getSchoolYear());
            }*/
            //param.setTimeZone(MathFriendzyHelper.getTimeForTutorServiceTime(this));
            new MyAsyckTask(ServerOperation.createPostRequestForGetStudentsWithPasswordFromClass(param)
                    , null, ServerOperationUtil.GET_MY_STUDENTS_WITH_PASSWORD, this,
                    this, ServerOperationUtil.SIMPLE_DIALOG, true,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        } else {
            CommonUtils.showInternetDialog(this);
        }
    }


    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
        if (requestCode == ServerOperationUtil.GET_MY_STUDENTS_WITH_PASSWORD) {
            GetMyStuentWithPasswordResponse response =
                    (GetMyStuentWithPasswordResponse) httpResponseBase;
            if (response.getResponse().equalsIgnoreCase("success")) {
                ArrayList<UserPlayerDto> students = response.getUserPlayer();
                /*if(students.size() >= STUDENT_LIMIT)
                    setShowMoreButtonVisibility(true);
                else
                    setShowMoreButtonVisibility(false);*/
                this.setOrNotifiyAdapter(students);
                //this.setGradeAdapter(response.getGradeList() , selectedGrade);
                //this.setGradeAdapter(response.getIntGradeList() , selectedGrade);
                //setGradeAdapter(response.getGradeList() , MathFriendzyHelper.ALL);
            }
        }
    }

    /**
     * Set or notify adapter
     *
     * @param students
     */
    private void setOrNotifiyAdapter(ArrayList<UserPlayerDto> students) {

        MyStudentsOnItemClickListener listener = new MyStudentsOnItemClickListener() {
            @Override
            public void onViewClick(int position) {
                viewStudentDetail(adapter.filterStudentList.get(position));
            }

            @Override
            public void onEditClick(int position) {
                /*MathFriendzyHelper.showWarningDialog(ActManageTutorMyStudents.this ,
                        "Under Developement!!!");*/
            }

            @Override
            public void onCheckTutor(int position) {

            }
        };

        if (adapter == null) {
            adapter = new StudentAccountAdapter(this, students, listener);
            lstTeacherStudents.setAdapter(adapter);
        } else {
            adapter.addRecord(students);
            adapter.notifyDataSetChanged();
        }
    }

    private void viewStudentDetail(UserPlayerDto userPlayerDto) {
        Intent intent = new Intent(StudentAccount.this, StudentInformation.class);
        intent.putExtra("playerInfo", userPlayerDto);
        startActivityForResult(intent, CHANGES_PASS_REQUEST);
    }

    /**
     * Set grade adapter to the spinner
     *
     * @param gradeList
     */
    /*private void setGradeAdapter(ArrayList<Integer> gradeList , String gradeSelectedValue){
        try {
            if (intGradeList == null) {
                intGradeList = gradeList;
            } else {
                for (int i = 0; i < gradeList.size(); i++) {
                    if (!intGradeList.contains(gradeList.get(i)))
                        intGradeList.add(gradeList.get(i));
                }
            }
            Collections.sort(intGradeList);
            ArrayAdapter<String> gradeAdapter = new ArrayAdapter<String>
                    (this, R.layout.spinner_textview_layout, this.getStringGradeList(intGradeList));
            gradeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerGrade.setAdapter(gradeAdapter);
            spinnerGrade.setSelection(gradeAdapter.getPosition(gradeSelectedValue));

            spinnerGrade.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectedGrade = spinnerGrade.getSelectedItem().toString();
                   *//* if(adapter != null){
                        adapter.filterStudent(selectedGrade);
                    }*//*

                    setClassTitle(MathFriendzyHelper.parseInt(selectedGrade));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }catch(Exception e){
            e.printStackTrace();
        }
    }*/
    private ArrayList<String> getStringGradeList(ArrayList<Integer> gradeList) {
        ArrayList<String> strGradeList = new ArrayList<String>();
        strGradeList.add(MathFriendzyHelper.ALL);
        try {
            for (int i = 0; i < gradeList.size(); i++) {
                strGradeList.add(gradeList.get(i) + "");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strGradeList;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == CHANGES_PASS_REQUEST) {
                String newPassword = data.getStringExtra("newPassword");
                UserPlayerDto playedInfo = (UserPlayerDto) data.getSerializableExtra("playerInfo");
                if (adapter != null) {
                    for (int i = 0; i < adapter.filterStudentList.size(); i++) {
                        if (adapter.filterStudentList.get(i).getParentUserId().equals(playedInfo.getParentUserId())) {
                            adapter.filterStudentList.get(i).setPassword(newPassword);
                        }
                    }
                    adapter.notifyDataSetChanged();
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    //below class title changes
    private void setClassTitle(int selectedGrade) {
        if (selectedGrade == 0) {//0 means user select All Option
            if (adapter != null) {
                adapter.filterStudent(MathFriendzyHelper.ALL);
            }
            this.setVisibilityOfClassesLayout(false);
            this.setVisibilityOfClassesLayout(false);
            return;
        }

        if (adapter != null) {
            adapter.filterStudent(selectedGrade + "");
        }
        this.setVisibilityOfTitleListLayout(false);
        ArrayList<ClassWithName> classWithNames = this.getClassTitleList(selectedGrade);
        this.clearDataWhenUserSelectOtherGrade(classWithNames);
        if (classWithNames != null && classWithNames.size() > 0) {
            this.setVisibilityOfClassesLayout(true);
            this.setClassListAdapter(classWithNames);
        } else {
            this.setVisibilityOfClassesLayout(false);
        }
    }

    private void clearDataWhenUserSelectOtherGrade
            (ArrayList<ClassWithName> classWithNames) {
        classAdapter = null;
        txtSelectClassValue.setText("");
        if (classWithNames != null && classWithNames.size() > 0) {
            for (int i = 0; i < classWithNames.size(); i++) {
                classWithNames.get(i).setSelected(false);
            }
        }
    }

    private ArrayList<ClassWithName> getClassTitleList(int grade) {
        if (registerUserFromPreff != null) {
            ArrayList<UserClasses> userClasseses = registerUserFromPreff.getUserClasses();
            for (int i = 0; i < userClasseses.size(); i++) {
                UserClasses userClass = userClasseses.get(i);
                if (userClass.getGrade() == grade) {
                    return userClass.getClassList();
                }
            }
            return null;
        } else {
            return null;
        }
    }

    private void setVisibilityOfClassesLayout(boolean isVisible) {
        if (isVisible) {
            selectClassLayout.setVisibility(RelativeLayout.VISIBLE);
        } else {
            selectClassLayout.setVisibility(RelativeLayout.GONE);
        }
    }

    private void setVisibilityOfTitleListLayout(boolean isVisible) {
        if (isVisible) {
            titleListLayout.setVisibility(RelativeLayout.VISIBLE);
            lstClassesList.setVisibility(ListView.VISIBLE);
        } else {
            titleListLayout.setVisibility(RelativeLayout.GONE);
            lstClassesList.setVisibility(ListView.GONE);
        }
    }

    /*private void clickToSelectClass() {
        this.setVisibilityOfTitleListLayout(true);
    }

    private void clickToClassSelectionDone() {
        txtSelectClassValue.setText("");
        this.setVisibilityOfTitleListLayout(false);
        ArrayList<ClassWithName> selectedClassList = this.getSelectedClasses();
        if(selectedClassList != null && selectedClassList.size() > 0) {
            txtSelectClassValue.setText(this.getCommaSeparatedClassName(selectedClassList));
            try {
                if (adapter != null)
                    adapter.filterListBasedOnClassId(selectedClassList , this.getSelectedGrade());
            }catch (Exception e){
                e.printStackTrace();
            }
        }else{
            try {
                if (adapter != null)
                    adapter.filterListBasedOnClassId(null , this.getSelectedGrade());
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }*/

    private ArrayList<ClassWithName> getSelectedClasses() {
        if (classAdapter != null && classAdapter.getSelectedList().size() > 0) {
            return classAdapter.getSelectedList();
        }
        return null;
    }

    private void setClassListAdapter(ArrayList<ClassWithName> classWithNames) {
        classAdapter = new ClassesAdapter(this, classWithNames);
        lstClassesList.setAdapter(classAdapter);
    }

    /*private String getCommaSeparatedClassName(ArrayList<ClassWithName> selectedClassList){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0 ; i < selectedClassList.size() ; i ++ ){
            stringBuilder.append(stringBuilder.length() > 0 ? "," + selectedClassList.get(i).getClassName()
                    : selectedClassList.get(i).getClassName());
        }
        return stringBuilder.toString();
    }*/

    /*private int getSelectedGrade(){
        try{
            return MathFriendzyHelper.parseInt(spinnerGrade.getSelectedItem().toString());
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }*/
    //end class title changes

    private ArrayList<ClassWithName> classWithNames = null;
    private ArrayList<ClassWithName> selectedClassList = null;
    private RegistereUserDto registerUserFromPreff = null;

    private void initClasses() {
        classWithNames = this.getUserClasses();
    }

    private ArrayList<ClassWithName> getUserClasses() {
        try {
            if (registerUserFromPreff != null) {
                ArrayList<UserClasses> userClasseses = registerUserFromPreff.getUserClasses();
                ArrayList<ClassWithName> allGradeClasses = new ArrayList<ClassWithName>();
                for (int i = 0; i < userClasseses.size(); i++) {
                    UserClasses userClass = userClasseses.get(i);
                    allGradeClasses.addAll(userClass.getClassList());
                }
                return allGradeClasses;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void clickToSelectClass() {
        if(!(classWithNames != null && classWithNames.size() > 0)){
            MathFriendzyHelper.showWarningDialog(this ,
                    MathFriendzyHelper.getTreanslationTextById(this , "lblNoClass"));
            return ;
        }

        SelectAssignHomeworkClassParam param = new SelectAssignHomeworkClassParam();
        param.setShowForPrevSaveHomework(true);
        MathFriendzyHelper.showClassSelectionDialog(this, classWithNames, new ClassSelectedListener() {
            @Override
            public void onClassSelectionDone(ArrayList<ClassWithName> selectedClass) {
                setDataAfterClassSelection(selectedClass);
            }

            @Override
            public void onUpdatedClasses(ArrayList<ClassWithName> updatedList) {
                classWithNames = updatedList;
            }
        }, param);
    }

    /**
     * Set the data after user select the class
     * @param selectedClass
     */
    private void setDataAfterClassSelection(ArrayList<ClassWithName> selectedClass){
        selectedClassList = selectedClass;
        if (selectedClass != null && selectedClass.size() > 0) {
            setTextToSelectedClassValues(getCommaSeparatedClassName(selectedClass));
            setTextToSelectedSubjectValues(getCommaSeparatedSubjectName(selectedClass));
        } else {
            setTextToSelectedClassValues("");
            setTextToSelectedSubjectValues("");
        }

        resetValues();
        loadMyStudents(getCommaSeparatedClassIds(selectedClassList));
    }

    private void resetValues() {
        adapter = null;
    }

    private void setTextToSelectedClassValues(String commaSeparatedClasses) {
        txtSelectClassValue.setText(commaSeparatedClasses);
        txtClassValues.setText(commaSeparatedClasses);
    }

    private void setTextToSelectedSubjectValues(String commaSeparatedSubjects) {
        txtSubjectValues.setText(commaSeparatedSubjects);
    }

    private String getCommaSeparatedClassName(ArrayList<ClassWithName> selectedClassList) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < selectedClassList.size(); i++) {
            stringBuilder.append(stringBuilder.length() > 0 ? "," + selectedClassList.get(i).getClassName()
                    : selectedClassList.get(i).getClassName());
        }
        return stringBuilder.toString();
    }

    private String getCommaSeparatedSubjectName(ArrayList<ClassWithName> selectedClassList) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < selectedClassList.size(); i++) {
            stringBuilder.append(stringBuilder.length() > 0 ? "," + selectedClassList.get(i).getSubject()
                    : selectedClassList.get(i).getSubject());
        }
        return stringBuilder.toString();
    }

    private String getCommaSeparatedClassIds(ArrayList<ClassWithName> selectedClassList) {
        try {
            if(selectedClassList != null && selectedClassList.size() > 0 ) {
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 0; i < selectedClassList.size(); i++) {
                    stringBuilder.append(stringBuilder.length() > 0 ? "," + selectedClassList.get(i).getClassId()
                            : selectedClassList.get(i).getClassId());
                }
                return stringBuilder.toString();
            }
            return defaultSelectedClassId;
        } catch (Exception e) {
            e.printStackTrace();
            return defaultSelectedClassId;
        }
    }
}

