package com.mathfriendzy.controller.studentaccount;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ICommonUtils;


/**
 * This Activivity will show the student information 
 * @author Yashwant Singh
 * Added on 28 Apr 2014
 */
public class StudentInformation extends ActBaseClass implements OnClickListener{

	private TextView mfTitleHomeScreen;
	private TextView textStudentInfo;
	private TextView lblFirstName;
	private TextView lblLastName;
	private TextView lblRegEmail;
	private TextView lblRegPassword;

	private EditText edtFirstName;
	private EditText edtLastName;
	private EditText edtEmail;//for user name
	private EditText edtPass;

	private Button btnSave;

	private final String TAG = this.getClass().getSimpleName();

	private UserPlayerDto playedInfo = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_student_information);

		if(ICommonUtils.STUDENT_REPORT)
			Log.e(TAG, "inside onCreate()");

		this.getIntentValues();
		this.setWidgetsReferences();
		this.setEditabilityOfEdittext();
		this.setTextValues();
		this.setPlayerInfo();
		this.setListenerOnWidgets();
		
		if(ICommonUtils.STUDENT_REPORT)
			Log.e(TAG, "outside onCreate()");

	}

	/**
	 * This method get the intent values which are set in previous screen
	 */
	private void getIntentValues(){
		playedInfo = (UserPlayerDto) this.getIntent().getSerializableExtra("playerInfo");
	}

	/**
	 * This method set the text values from translation
	 */
	private void setTextValues() {

		if(ICommonUtils.STUDENT_REPORT)
			Log.e(TAG, "inside setTextValues()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		mfTitleHomeScreen.setText(transeletion.getTranselationTextByTextIdentifier("resultTitleMyStudents"));
		textStudentInfo.setText(transeletion.getTranselationTextByTextIdentifier("lblStudent")
				+ " " + transeletion.getTranselationTextByTextIdentifier("infoTitle") + ":");
		lblFirstName.setText(transeletion.getTranselationTextByTextIdentifier("lblFirstName") + ":");
		lblLastName.setText(transeletion.getTranselationTextByTextIdentifier("lblLastName") + ":");
		lblRegEmail.setText(transeletion.getTranselationTextByTextIdentifier("lblUserName") + ":");
		lblRegPassword.setText(transeletion.getTranselationTextByTextIdentifier("lblRegPassword") + ":");
		btnSave.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleSave"));
		transeletion.closeConnection();

		if(ICommonUtils.STUDENT_REPORT)
			Log.e(TAG, "outside setTextValues()");

	}

	/**
	 * This method set the widgets references from the xml
	 */
	private void setWidgetsReferences() {

		if(ICommonUtils.STUDENT_REPORT)
			Log.e(TAG, "inside setWidgetsReferences()");

		mfTitleHomeScreen = (TextView) findViewById(R.id.mfTitleHomeScreen);
		textStudentInfo = (TextView) findViewById(R.id.textStudentInfo);
		lblFirstName = (TextView) findViewById(R.id.lblFirstName);
		lblLastName = (TextView) findViewById(R.id.lblLastName);
		lblRegEmail = (TextView) findViewById(R.id.lblRegEmail);
		lblRegPassword = (TextView) findViewById(R.id.lblRegPassword);

		edtFirstName = (EditText) findViewById(R.id.edtFirstName);
		edtLastName = (EditText) findViewById(R.id.edtLastName);
		edtEmail = (EditText) findViewById(R.id.edtEmail);
		edtPass = (EditText) findViewById(R.id.edtPass);

		btnSave = (Button) findViewById(R.id.btnSave);

		if(ICommonUtils.STUDENT_REPORT)
			Log.e(TAG, "outside setWidgetsReferences()");

	}

	/**
	 * This method set th eplayer info which are from the last screen
	 */
	private void setPlayerInfo(){

		if(ICommonUtils.STUDENT_REPORT)
			Log.e(TAG, "inside setPlayerInfo()");

		if(playedInfo != null){
			edtFirstName.setText(playedInfo.getFirstname());
			edtLastName.setText(playedInfo.getLastname());
			edtEmail.setText(playedInfo.getUsername());
			edtPass.setText(playedInfo.getPassword());

            CommonUtils.printLog(TAG , "Password " + playedInfo.getPassword());
		}

		if(ICommonUtils.STUDENT_REPORT)
			Log.e(TAG, "outside setPlayerInfo()");
	}

	/**
	 * This method set the editable value for edittext	
	 */
	private void setEditabilityOfEdittext(){
		
		edtFirstName.setInputType(0);
		edtLastName.setInputType(0);
		edtEmail.setInputType(0);
		
		edtFirstName.setFocusable(false);
		edtLastName.setFocusable(false);
		edtEmail.setFocusable(false);
	}


	/**
	 * This method set the listener on widgets
	 */
	private void setListenerOnWidgets() {

		if(ICommonUtils.STUDENT_REPORT)
			Log.e(TAG, "inside setListenerOnWidgets()");

		btnSave.setOnClickListener(this);
		edtFirstName.setOnClickListener(this);
		edtLastName.setOnClickListener(this);
		edtEmail.setOnClickListener(this);
				
		if(ICommonUtils.STUDENT_REPORT)
			Log.e(TAG, "outside setListenerOnWidgets()");

	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnSave :
			this.clickOnSave();
			break;
		case R.id.edtFirstName :
			this.showCanNotEditDialog();
			break;
		case R.id.edtLastName :
			this.showCanNotEditDialog();
			break;
		case R.id.edtEmail :
			this.showCanNotEditDialog();
			break;
		}
	}

	/**
	 * This method show when user click on
	 * edit field which are not editable
	 */
	private void showCanNotEditDialog(){
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		DialogGenerator dg = new DialogGenerator(this);
		dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("lblYouCanNotChangeThisField"));
		transeletion.closeConnection();
	}
	
	/**
	 * This method call when user click on save button to update info
	 */
	private void clickOnSave(){

		if(CommonUtils.isInternetConnectionAvailable(this)){
			if(CommonUtils.isPasswordValid(edtPass.getText().toString())){
				new SavePassword(edtPass.getText().toString(), playedInfo).execute();
			}else{
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				DialogGenerator dg = new DialogGenerator(this);
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgPasswordInvalid"));
				transeletion.closeConnection();
			}
		}else{
			CommonUtils.showInternetDialog(this);
		}
	}


	/**
	 * This asynctask save the updated password
	 * @author Yashwant Singh
	 *
	 */
	class SavePassword extends AsyncTask<Void, Void, Integer>{

		private UserPlayerDto playedInfo = null;
		private String newPassword = null;
		private ProgressDialog pd = null;
		private final int SUCCESS = 1;

		SavePassword(String newPassword , UserPlayerDto playedInfo){
			this.newPassword = newPassword;
			this.playedInfo  = playedInfo;
		}

		@Override
		protected void onPreExecute() {
			pd = CommonUtils.getProgressDialog(StudentInformation.this);
			pd.show();

			super.onPreExecute();
		}

		@Override
		protected Integer doInBackground(Void... params) {
			StudentAcountServerOperation serverObj = new StudentAcountServerOperation();
			int response = serverObj.savePassword(newPassword, playedInfo);
			return response;
		}

		@Override
		protected void onPostExecute(Integer result) {
			pd.cancel();
						
			if(result.intValue() == SUCCESS){
				Intent intent = new Intent();
				intent.putExtra("newPassword",	newPassword);
				intent.putExtra("playerInfo",	playedInfo);
				setResult(RESULT_OK , intent);
				finish();
			}else{
				DialogGenerator dg = new DialogGenerator(StudentInformation.this);
				dg.generateWarningDialog("Password not updated successfully!");
			}
			
			super.onPostExecute(result);
		}
	}
	
	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}
}
