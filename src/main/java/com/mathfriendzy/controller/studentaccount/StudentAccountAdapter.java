package com.mathfriendzy.controller.studentaccount;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.managetutor.mystudent.MyStudentsOnItemClickListener;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.classes.ClassWithName;

import java.util.ArrayList;

/**
 * Created by root on 30/3/16.
 */
public class StudentAccountAdapter extends BaseAdapter{
    public ArrayList<UserPlayerDto> students = null;
    public ArrayList<UserPlayerDto> filterStudentList = new ArrayList<>();
    private LayoutInflater mInflator = null;
    private ViewHolder vHolder = null;
    private Context context = null;
    private MyStudentsOnItemClickListener callback = null;

    public StudentAccountAdapter(Context context , ArrayList<UserPlayerDto> students ,
                            MyStudentsOnItemClickListener callback){
        this.callback = callback;
        this.context = context;
        mInflator = LayoutInflater.from(context);
        this.students = students;
        this.filterStudentList = students;
    }

    public void addRecord(ArrayList<UserPlayerDto> students){
        this.students.addAll(students);
    }

    @Override
    public int getCount() {
        return filterStudentList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        if(view == null){
            vHolder = new ViewHolder();
            view = mInflator.inflate(R.layout.studentslistlayout , null);
            vHolder.txtStudentName = (TextView) view.findViewById(R.id.txtStudentName);
            vHolder.studentListLayout = (RelativeLayout) view.findViewById(R.id.studentListLayout);
            view.setTag(vHolder);
        }else{
            vHolder = (ViewHolder) view.getTag();
        }
        vHolder.txtStudentName.setText(filterStudentList.get(position).getFirstname()
                + " " + filterStudentList.get(position).getLastname());
        vHolder.studentListLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onViewClick(position);
            }
        });
        return view;
    }

    private class ViewHolder{
        private TextView txtStudentName;
        private RelativeLayout studentListLayout;
    }

    public void filterStudent(String selectedGrade){
        this.filterStudentList.clear();
        if(selectedGrade.equalsIgnoreCase(MathFriendzyHelper.ALL)){
            for(int i = 0 ; i < students.size() ; i ++ ) {
                this.filterStudentList.add(students.get(i));
            }
        }else {
            for (int i = 0; i < students.size(); i++) {
                if(selectedGrade.equalsIgnoreCase(students.get(i).getGrade())){
                    filterStudentList.add(students.get(i));
                }
            }
        }
        this.notifyDataSetChanged();
    }

    public void filterListBasedOnClassId(ArrayList<ClassWithName> selectedClassList
            , int selectedGrade){
        if(selectedGrade == 0){
            this.filterStudent(MathFriendzyHelper.ALL);
        }else{
            if(selectedClassList == null){
                this.filterStudent(selectedGrade + "");
            }else{
                this.filterStudentList.clear();
                for (int i = 0; i < this.students.size(); i++) {
                    if (this.ifStudentExistForSelectedClass(selectedClassList
                            , this.students.get(i).getClassId())) {
                        filterStudentList.add(students.get(i));
                    }
                }
                this.notifyDataSetChanged();
            }
        }
    }

    private boolean ifStudentExistForSelectedClass(ArrayList<ClassWithName> selectedClassList , int classId){
        if(selectedClassList != null && selectedClassList.size() > 0){
            for(int i = 0 ; i < selectedClassList.size() ; i ++ ){
                if(selectedClassList.get(i).getClassId() == classId){
                    return true;
                }
            }
        }
        return false;
    }
}
