package com.mathfriendzy.controller.studentaccount;

import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.utils.CommonUtils;

/**
 * This class contains the server operation related to the student account
 * @author Yashwant Singh
 *
 */
public class StudentAcountServerOperation {
	
	/**
	 * This method save the user password on server
	 * @param newPassword
	 * @param playerInfo
	 */
	public int savePassword(String newPassword , UserPlayerDto playerInfo){
		String action = "updatePasswordForUser";
		String strURL = COMPLETE_URL + "action=" + action + "&"
				+ "userId=" + playerInfo.getParentUserId() + "&"
				+ "password=" + newPassword;
		//Log.e("", "url " + strURL);
		return this.parseTheJsonStringFronUpdatePassword(CommonUtils.readFromURL(strURL));
	}
	
	/**
	 * This method parse the json string for the update new password
	 * @param jsonString
	 */
	private int parseTheJsonStringFronUpdatePassword(String jsonString){
		//Log.e("", "response " + jsonString);		
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			if(jsonObj.getString("result").equals("success"))
				return 1;
			return 0;
		} catch (JSONException e) {
			Log.e("TAG", "inside parseTheJsonStringFronUpdatePassword " +
					" Error while parsing : " + e.toString());
		}
		return 0;
	}
		
}
