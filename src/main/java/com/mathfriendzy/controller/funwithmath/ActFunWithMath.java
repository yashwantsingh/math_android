package com.mathfriendzy.controller.funwithmath;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.learningcenter.NewLearnignCenter;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.multifriendzy.MultiFriendzyMain;
import com.mathfriendzy.controller.player.CreateTeacherPlayerActivity;
import com.mathfriendzy.controller.player.LoginUserCreatePlayer;
import com.mathfriendzy.controller.player.LoginUserPlayerActivity;
import com.mathfriendzy.controller.player.TeacherPlayer;
import com.mathfriendzy.controller.singlefriendzy.SingleFriendzyMain;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.CheckTempPlayerListener;
import com.mathfriendzy.listener.OnRequestComplete;
import com.mathfriendzy.listener.TeacherFunctionDialogListener;
import com.mathfriendzy.model.friendzy.FriendzyServerOperation;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.singleFriendzy.SingleFriendzyImpl;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.utils.CommonUtils;

public class ActFunWithMath extends ActBase {

    private final String TAG = this.getClass().getSimpleName();
    private TextView txtFunWithMath = null;
    private RelativeLayout learningCenterLayout = null;
    private TextView lblLearningCenter = null;
    private RelativeLayout singleFriendzyLayout = null;
    private TextView lblSingleFriendzy = null;
    private RelativeLayout multiFriendzyLayout = null;
    private TextView lblMultiFriendzy = null;

    private final int LEARNING_CENTER = 1;
    private final int SINGLE_FRIENDYZY = 2;
    private final int MULTI_FRIENDZY = 3;
    private final int AFTER_TEMP_PLAYER_CREATED = 1;
    private final int NO_TEMP_PLAYER_CREATED = 0;

    private UserPlayerDto selectedPlayer = null;
    private final int MAX_ITEM_ID_FOR_ALL_APP = 100;
    private final int MAX_ITEM_ID = 11;
    private ProgressDialog pd = null;

    private String alertMsgYouMustRegisterLoginToAccessThisFeature = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_fun_with_math);

        CommonUtils.printLog(TAG , "inside onCreate()");

        selectedPlayer = this.getPlayerData();

        this.init();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();

        CommonUtils.printLog(TAG , "outside onCreate()");
    }

    private void init() {
        pd = MathFriendzyHelper.getProgressDialog(this);
    }


    @Override
    protected void setWidgetsReferences() {
        CommonUtils.printLog(TAG , "inside setWidgetsReferences()");
        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        txtFunWithMath = (TextView) findViewById(R.id.txtFunWithMath);
        lblLearningCenter = (TextView) findViewById(R.id.lblLearningCenter);
        lblSingleFriendzy = (TextView) findViewById(R.id.lblSingleFriendzy);
        lblMultiFriendzy = (TextView) findViewById(R.id.lblMultiFriendzy);
        learningCenterLayout = (RelativeLayout) findViewById(R.id.learningCenterLayout);
        singleFriendzyLayout = (RelativeLayout) findViewById(R.id.singleFriendzyLayout);
        multiFriendzyLayout = (RelativeLayout) findViewById(R.id.multiFriendzyLayout);
        CommonUtils.printLog(TAG , "outside setWidgetsReferences()");

    }

    @Override
    protected void setListenerOnWidgets() {
        CommonUtils.printLog(TAG , "inside setListenerOnWidgets()");
        learningCenterLayout.setOnClickListener(this);
        singleFriendzyLayout.setOnClickListener(this);
        multiFriendzyLayout.setOnClickListener(this);
        CommonUtils.printLog(TAG , "outside setListenerOnWidgets()");
    }

    @Override
    protected void setTextFromTranslation() {
        CommonUtils.printLog(TAG , "inside setTextFromTranslation()");
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblFunWithMath"));
        txtFunWithMath.setText(transeletion.getTranselationTextByTextIdentifier("lblFunWithMath"));
        lblLearningCenter.setText(transeletion.getTranselationTextByTextIdentifier("lblLearningCenter"));
        lblSingleFriendzy.setText(transeletion.getTranselationTextByTextIdentifier("lblSingleFriendzy"));
        lblMultiFriendzy.setText(transeletion.getTranselationTextByTextIdentifier("lblBestOf5Friendzy"));
        alertMsgYouMustRegisterLoginToAccessThisFeature = transeletion
                .getTranselationTextByTextIdentifier("alertMsgYouMustRegisterLoginToAccessThisFeature");
        transeletion.closeConnection();
        CommonUtils.printLog(TAG , "outside setTextFromTranslation()");

    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {

    }

    @Override
    public void onClick(View v) {
        selectedPlayer = this.getPlayerData();
        if(selectedPlayer != null){
            MathFriendzyHelper.saveLoginPlayerDataIntoSharedPreff(this);
        }

        switch(v.getId()){
            case R.id.learningCenterLayout:
                if(this.showTeacherFunctionPopUp
                        (MathFriendzyHelper.DIALOG_LEARNING_CENTER
                                , MathFriendzyHelper.getTextFromTextView(lblLearningCenter) , false))
                    return ;
                this.checkForTempPlayer(LEARNING_CENTER);
                break;
            case R.id.singleFriendzyLayout:
                if(this.showTeacherFunctionPopUp
                        (MathFriendzyHelper.DIALOG_SINGLE_FRIENDZY,
                                MathFriendzyHelper.getTextFromTextView(lblSingleFriendzy) , false))
                    return ;
                this.checkForTempPlayer(SINGLE_FRIENDYZY);
                break;
            case R.id.multiFriendzyLayout:
                if(this.showTeacherFunctionPopUp
                        (MathFriendzyHelper.DIALOG_MULTI_FRIENDZY ,
                                MathFriendzyHelper.getTextFromTextView(lblMultiFriendzy) , false))
                    return ;
                this.checkForTempPlayer(MULTI_FRIENDZY);
                break;
        }
    }

    private void checkForTempPlayer(final int clickFor){
        if(clickFor == MULTI_FRIENDZY){
            if(!CommonUtils.isInternetConnectionAvailable(this)){
                CommonUtils.showInternetDialog(this);
                return;
            }
        }

        if(MathFriendzyHelper.isTempraroryPlayerCreated(this)){
            switch(clickFor){
                case LEARNING_CENTER:
                    goForLearningCenter(NO_TEMP_PLAYER_CREATED , clickFor);
                    break;
                case SINGLE_FRIENDYZY:
                    goForSingleFriendzy(NO_TEMP_PLAYER_CREATED , clickFor);
                    break;
                case MULTI_FRIENDZY:
                    goForMultiFriendzy(NO_TEMP_PLAYER_CREATED , clickFor);
                    break;
            }
        }else{
            MathFriendzyHelper.checkForTempPlayerAndCreteNotExist(this ,
                    new CheckTempPlayerListener() {
                        @Override
                        public void onDone() {
                            switch(clickFor){
                                case LEARNING_CENTER:
                                    goForLearningCenter(AFTER_TEMP_PLAYER_CREATED , clickFor);
                                    break;
                                case SINGLE_FRIENDYZY:
                                    goForSingleFriendzy(AFTER_TEMP_PLAYER_CREATED , clickFor);
                                    break;
                                case MULTI_FRIENDZY:
                                    goForMultiFriendzy(AFTER_TEMP_PLAYER_CREATED , clickFor);
                                    break;
                            }
                        }
                    });
        }
    }

    private void goForLearningCenter(int tempPlayerOperation , int clickFor){
        if(tempPlayerOperation == AFTER_TEMP_PLAYER_CREATED){
            startActivity(clickFor);
        }else{
            this.doOperationBeforeLearningCenter();
        }
    }

    private void goForSingleFriendzy(int tempPlayerOperation , int clickFor){
        if(tempPlayerOperation == AFTER_TEMP_PLAYER_CREATED){
            startActivity(clickFor);
        }else{
            this.doOperationBeforeSingleFriendzy();
        }
    }

    private void goForMultiFriendzy(int tempPlayerOperation , int clickFor){
        if(tempPlayerOperation == AFTER_TEMP_PLAYER_CREATED){
            startActivity(clickFor);
        }else{
            this.doOperationBeforeOPenMultiFriendzy();
        }
    }



    private void openTeacherPlayerActivity(){
        Intent intent = new Intent(this,TeacherPlayer.class);
        startActivity(intent);
    }

    private void openLoginUserPlayerActivity(){
        Intent intent = new Intent(this,LoginUserPlayerActivity.class);
        startActivity(intent);
    }

    private void openCreateTempPlayerScreen(){
        /*Intent intent = new Intent(this,CreateTempPlayerActivity.class);
        startActivity(intent);*/
        MathFriendzyHelper.openCreateTempPlayerActivity(this , alertMsgYouMustRegisterLoginToAccessThisFeature);
    }

    private void openCreateTeacherPlayerActivity(){
        Intent intent = new Intent(this,CreateTeacherPlayerActivity.class);
        startActivity(intent);
    }

    private void openLoginUserCreatePlayerActivity(){
        Intent intent = new Intent(this,LoginUserCreatePlayer.class);
        startActivity(intent);
    }

    private void startActivity(int clickFor){
        Intent intent = null;
        switch(clickFor) {
            case LEARNING_CENTER:
                intent  = new Intent(this,NewLearnignCenter.class);
                startActivity(intent);
                break;
            case SINGLE_FRIENDYZY:
                intent  = new Intent(this,SingleFriendzyMain.class);
                startActivity(intent);
                break;
            case MULTI_FRIENDZY:
                intent  = new Intent(this,MultiFriendzyMain.class);
                startActivity(intent);
                break;
        }
    }


    //learning center
    private void doOperationBeforeLearningCenter(){
        if(MathFriendzyHelper.isUserLogin(this)){
            if(selectedPlayer != null){
                this.clickOnLearningCenter();
            }else{
                if(!MathFriendzyHelper.isUserPlayerExist(this)){
                    if(MathFriendzyHelper.getUserAccountType(this)
                            == MathFriendzyHelper.TEACHER){
                        this.openCreateTeacherPlayerActivity();
                    }else{
                        this.openLoginUserCreatePlayerActivity();
                    }
                }else {
                    if (MathFriendzyHelper.getUserAccountType(this)
                            == MathFriendzyHelper.TEACHER) {
                        openTeacherPlayerActivity();
                    } else {
                        MainActivity.IS_CALL_FROM_LEARNING_CENTER = 1;
                        NewLearnignCenter.isCallFromMainActivity = true;
                        openLoginUserPlayerActivity();
                    }
                }
            }
        }else{
            if(!MathFriendzyHelper.isTempPlayerDeleted(this)){
                MathFriendzyHelper.setTempPlayerAsCurrentPlayer(this);
                this.clickOnLearningCenter();
            }else{
                this.openCreateTempPlayerScreen();
            }
        }
    }

    private Activity getActivityObject(){
        return this;
    }

    private void clickOnLearningCenter(){
        MathFriendzyHelper.showProgressDialog(pd);
        NewLearnignCenter.isCallFromMainActivity = true;
        if(MathFriendzyHelper.isTempPlayerDeleted(this)){
            this.setActivePlayerDataForFriendzyChallenge(selectedPlayer.getParentUserId(),
                    selectedPlayer.getPlayerid());
            if(CommonUtils.isInternetConnectionAvailable(this)){
                if(MathFriendzyHelper.isLocalPlayerDataExist(this ,
                        selectedPlayer.getParentUserId() , selectedPlayer.getPlayerid())){
                    this.addLevelAsynckTask(new OnRequestComplete() {
                        @Override
                        public void onComplete() {
                            if(MathFriendzyHelper.isWordProblemDataExistForLearnignCenter(getActivityObject() ,
                                    selectedPlayer.getParentUserId() , selectedPlayer.getPlayerid())){
                                addWordProblem(new OnRequestComplete() {
                                    @Override
                                    public void onComplete() {
                                        getPlayerDataFromServer(new OnRequestComplete() {
                                            @Override
                                            public void onComplete() {
                                                getWordProblemLevelDetail(new OnRequestComplete() {
                                                    @Override
                                                    public void onComplete() {
                                                        MathFriendzyHelper.hideProgressDialog(pd);
                                                        downloadQuestions(new HttpServerRequest() {
                                                            @Override
                                                            public void onRequestComplete() {
                                                                startActivity(LEARNING_CENTER);
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }else{
                                getPlayerDataFromServer(new OnRequestComplete() {
                                    @Override
                                    public void onComplete() {
                                        getWordProblemLevelDetail(new OnRequestComplete() {
                                            @Override
                                            public void onComplete() {
                                                MathFriendzyHelper.hideProgressDialog(pd);
                                                downloadQuestions(new HttpServerRequest() {
                                                    @Override
                                                    public void onRequestComplete() {
                                                        startActivity(LEARNING_CENTER);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    });
                }else{
                    if(MathFriendzyHelper.isWordProblemDataExistForLearnignCenter(getActivityObject() ,
                            selectedPlayer.getParentUserId() , selectedPlayer.getPlayerid())){
                        addWordProblem(new OnRequestComplete() {
                            @Override
                            public void onComplete() {
                                getPlayerDataFromServer(new OnRequestComplete() {
                                    @Override
                                    public void onComplete() {
                                        getWordProblemLevelDetail(new OnRequestComplete() {
                                            @Override
                                            public void onComplete() {
                                                MathFriendzyHelper.hideProgressDialog(pd);
                                                downloadQuestions(new HttpServerRequest() {
                                                    @Override
                                                    public void onRequestComplete() {
                                                        startActivity(LEARNING_CENTER);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }else{
                        getPlayerDataFromServer(new OnRequestComplete() {
                            @Override
                            public void onComplete() {
                                getWordProblemLevelDetail(new OnRequestComplete() {
                                    @Override
                                    public void onComplete() {
                                        MathFriendzyHelper.hideProgressDialog(pd);
                                        downloadQuestions(new HttpServerRequest() {
                                            @Override
                                            public void onRequestComplete() {
                                                startActivity(LEARNING_CENTER);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                }
            }else{
                MathFriendzyHelper.hideProgressDialog(pd);
                this.startActivity(LEARNING_CENTER);
            }
        }else{
            MathFriendzyHelper.hideProgressDialog(pd);
            this.startActivity(LEARNING_CENTER);
        }
    }

    private void addLevelAsynckTask(final OnRequestComplete listener){
        //MathFriendzyHelper.showProgressDialog(pd);
        MathFriendzyHelper.addLevelAsynckTask(MathFriendzyHelper
                        .getPlayerLevelEquationDataById(this, selectedPlayer.getPlayerid()) ,
                new OnRequestComplete() {
                    @Override
                    public void onComplete() {
                        //MathFriendzyHelper.hideProgressDialog(pd);
                        MathFriendzyHelper.deleteFromLocalPlayerLevel(ActFunWithMath.this ,
                                selectedPlayer.getParentUserId() , selectedPlayer.getPlayerid());
                        listener.onComplete();
                    }
                });
    }

    private void addWordProblem(final OnRequestComplete listener){
        //MathFriendzyHelper.showProgressDialog(pd);
        MathFriendzyHelper.addAllWordProblemLevel(getActivityObject() ,
                selectedPlayer.getParentUserId() ,
                selectedPlayer.getPlayerid() , new OnRequestComplete() {
                    @Override
                    public void onComplete() {
                        //MathFriendzyHelper.hideProgressDialog(pd);
                        MathFriendzyHelper.deleteFromLocalWordProblemLevel
                                (getActivityObject() , selectedPlayer.getParentUserId() ,
                                        selectedPlayer.getPlayerid());
                    }
                });
    }

    private void getPlayerDataFromServer(final OnRequestComplete listener){
        //MathFriendzyHelper.showProgressDialog(pd);
        MathFriendzyHelper.getPlayerDataFromServer(getActivityObject() ,
                selectedPlayer.getParentUserId() ,
                selectedPlayer.getPlayerid() , new OnRequestComplete() {
                    @Override
                    public void onComplete() {
                        //MathFriendzyHelper.hideProgressDialog(pd);
                        listener.onComplete();
                    }
                });
    }

    private void getWordProblemLevelDetail(final OnRequestComplete listener){
        //MathFriendzyHelper.showProgressDialog(pd);
        MathFriendzyHelper.getWordProblemLevelDetail
                (getActivityObject() ,
                        selectedPlayer.getParentUserId() ,
                        selectedPlayer.getPlayerid() , new OnRequestComplete() {
                            @Override
                            public void onComplete() {
                                //MathFriendzyHelper.hideProgressDialog(pd);
                                listener.onComplete();
                            }
                        });

    }

    private void downloadQuestions(final HttpServerRequest listener){
        MathFriendzyHelper.downLoadQuestionForWordProblem(getActivityObject() ,
                MathFriendzyHelper.parseInt(selectedPlayer.getGrade()) ,
                new HttpServerRequest() {
                    @Override
                    public void onRequestComplete() {
                        listener.onRequestComplete();
                    }
                });
    }

    //end learning center

    //single friendzy
    private void doOperationBeforeSingleFriendzy(){
        if(MathFriendzyHelper.isUserLogin(this)){
            if(selectedPlayer != null){
                this.setActivePlayerDataForFriendzyChallenge(selectedPlayer.getParentUserId() ,
                        selectedPlayer.getPlayerid());
                SingleFriendzyImpl singleimpl = new SingleFriendzyImpl(this);
                singleimpl.openConn();
                int itemId = singleimpl.getItemId(selectedPlayer.getParentUserId());
                singleimpl.closeConn();

                if(itemId != MAX_ITEM_ID){
                    if(CommonUtils.isInternetConnectionAvailable(this)){
                        MathFriendzyHelper.showProgressDialog(pd);
                        MathFriendzyHelper.getSingleFriendzyDetail(this ,
                                selectedPlayer.getParentUserId() ,
                                selectedPlayer.getPlayerid() , new OnRequestComplete() {
                                    @Override
                                    public void onComplete() {
                                        MathFriendzyHelper.hideProgressDialog(pd);
                                        startActivity(SINGLE_FRIENDYZY);
                                    }
                                });
                    }else{
                        this.startActivity(SINGLE_FRIENDYZY);
                    }
                }else{
                    if(CommonUtils.isInternetConnectionAvailable(this)){
                        MathFriendzyHelper.showProgressDialog(pd);
                        MathFriendzyHelper.getPointsLevelDetail(this,
                                selectedPlayer.getParentUserId(),
                                selectedPlayer.getPlayerid(), new OnRequestComplete() {
                                    @Override
                                    public void onComplete() {
                                        MathFriendzyHelper.hideProgressDialog(pd);
                                        startActivity(SINGLE_FRIENDYZY);
                                    }
                                });
                    }else{
                        this.startActivity(SINGLE_FRIENDYZY);
                    }
                }
            }else{
                if(!MathFriendzyHelper.isUserPlayerExist(this)){
                    if(MathFriendzyHelper.getUserAccountType(this)
                            == MathFriendzyHelper.TEACHER){
                        this.openCreateTeacherPlayerActivity();
                    }else{
                        this.openLoginUserCreatePlayerActivity();
                    }
                }else {
                    if (MathFriendzyHelper.getUserAccountType(this)
                            == MathFriendzyHelper.TEACHER) {
                        openTeacherPlayerActivity();
                    } else {
                        MainActivity.IS_CALL_FROM_SINGLE_FRIENDZY = 1;
                        openLoginUserPlayerActivity();
                    }
                }
            }
        }else{
            if(!MathFriendzyHelper.isTempPlayerDeleted(this)){
                MathFriendzyHelper.setTempPlayerAsCurrentPlayer(this);
                this.startActivity(SINGLE_FRIENDYZY);
            }else{
                this.openCreateTempPlayerScreen();
            }
        }
    }
    //end single friendzy


    //multi-friendzy
    private void doOperationBeforeOPenMultiFriendzy(){
        if(MathFriendzyHelper.isUserLogin(this)){
            if(selectedPlayer != null){
                this.setActivePlayerDataForFriendzyChallenge(selectedPlayer.getParentUserId() ,
                        selectedPlayer.getPlayerid());
                SingleFriendzyImpl singleimpl = new SingleFriendzyImpl(this);
                singleimpl.openConn();
                int itemId = singleimpl.getItemIdForMultiFriendzy(selectedPlayer.getParentUserId());
                singleimpl.closeConn();

                if(itemId != MAX_ITEM_ID_FOR_ALL_APP){
                    if(CommonUtils.isInternetConnectionAvailable(this)){
                        MathFriendzyHelper.getPurchasedItemDetail(this ,
                                selectedPlayer.getParentUserId());
                    }
                }
                this.startActivity(MULTI_FRIENDZY);
            }else{
                if(!MathFriendzyHelper.isUserPlayerExist(this)){
                    if(MathFriendzyHelper.getUserAccountType(this)
                            == MathFriendzyHelper.TEACHER){
                        this.openCreateTeacherPlayerActivity();
                    }else{
                        this.openLoginUserCreatePlayerActivity();
                    }
                }else {
                    if (MathFriendzyHelper.getUserAccountType(this)
                            == MathFriendzyHelper.TEACHER) {
                        openTeacherPlayerActivity();
                    } else {
                        MainActivity.IS_CALL_FROM_MULTIFRIENDZY = 1;
                        openLoginUserPlayerActivity();
                    }
                }
            }
        }else{
            if(!MathFriendzyHelper.isTempPlayerDeleted(this)){
                MathFriendzyHelper.setTempPlayerAsCurrentPlayer(this);
                this.startActivity(MULTI_FRIENDZY);
            }else{
                this.openCreateTempPlayerScreen();
            }
        }
    }

    /**
     * This method set the friendzy challenge equation data
     * @param userId
     * @param playerId
     */
    private void setActivePlayerDataForFriendzyChallenge(String userId , String playerId){
        //changes for Friendzy challenge
        if(CommonUtils.isActivePlayer(userId,playerId)) {
            //get challenger id
            String challengerId = CommonUtils.getActivePlayerChallengerId
                    (userId, playerId);
            if(CommonUtils.isInternetConnectionAvailable(this)){
                new GetUpdatedEquationsForChallenge(userId
                        , playerId , challengerId).execute(null,null,null);
            }else{
                try{
                    MainActivity.equationsObj.setEquationIds(CommonUtils.getActivePlayerChallengerId
                            (playerId , challengerId));
                }catch(Exception e){

                }
            }
        }
        //end changes
    }

    /**
     * This AsyncTask get updated equations details for friendzy
     * @author Yashwant Singh
     *
     */
    class GetUpdatedEquationsForChallenge extends AsyncTask<Void, Void, Void> {

        private String challengerId;
        private String playerId;
        GetUpdatedEquationsForChallenge(String userId , String playerId , String challengerId){
			/*challengerId = CommonUtils.getActivePlayerChallengerId(userId, playerId);*/
            this.challengerId = challengerId;
            this.playerId = playerId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            FriendzyServerOperation friendzyServerObj = new FriendzyServerOperation();
            MainActivity.equationsObj = friendzyServerObj.getUpdatedEquationsForChallenge(challengerId);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            try{
                CommonUtils.updateEndDateForFriendzyChallenge(playerId,
                        MainActivity.equationsObj.getEndDate(),
                        challengerId);
            }catch (Exception e) {
                Log.e(TAG, "Error while server response " + e.toString());
            }
            super.onPostExecute(result);
        }
    }

    /**
     * Show the teacher function popup
     * @param popUpFor
     * @return
     */
    private boolean showTeacherFunctionPopUp(final String popUpFor ,
                                             String txtTitle , boolean isShowWatchButton){
        if(MathFriendzyHelper.showTeacherFunctionPopup(this ,
                popUpFor , new TeacherFunctionDialogListener() {

                    @Override
                    public void clickOnWatchVedio(boolean isShow) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void clickOnDontShow(boolean isShow) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void clickOnClose(boolean isShow) {
                        clickOn(popUpFor);
                    }
                } , txtTitle , isShowWatchButton))
            return true;
        return false;
    }

    /**
     * Go after click on close button on Teacher function show vedio
     * @param dialogFor
     */
    private void clickOn(String dialogFor){
        if(MathFriendzyHelper.DIALOG_LEARNING_CENTER.equals(dialogFor)){
            this.checkForTempPlayer(LEARNING_CENTER);
        }else if(MathFriendzyHelper.DIALOG_SINGLE_FRIENDZY.equals(dialogFor)){
            this.checkForTempPlayer(SINGLE_FRIENDYZY);
        }else if(MathFriendzyHelper.DIALOG_MULTI_FRIENDZY.equals(dialogFor)){
            this.checkForTempPlayer(MULTI_FRIENDZY);
        }
    }
}
