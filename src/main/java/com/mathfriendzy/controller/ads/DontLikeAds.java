package com.mathfriendzy.controller.ads;

import android.content.Context;
import android.graphics.PixelFormat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.MyApplication;

/**
 * Created by root on 13/10/15.
 */
public class DontLikeAds {
    private static DontLikeAds dontLikeAds = null;
    private static Context myContext = null;
    private View view = null;
    private WindowManager wm = null;
    private static int NUMBER_OF_SCREEN_COUNTER = 0;
    private static final int MAX_SCREEN_COUNTER = 4;


    public static DontLikeAds getInstance(Context context){
        myContext = context;
        if(dontLikeAds == null)
            dontLikeAds = new DontLikeAds();
        return dontLikeAds;
    }

    public void showDontLikeAds(){
        NUMBER_OF_SCREEN_COUNTER ++ ;
        if(NUMBER_OF_SCREEN_COUNTER == MAX_SCREEN_COUNTER){
            this.showViewInWM(myContext);
        }
    }

    private View getView(Context context){
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.dialog_click_on_proceed_for_coins, null);

        TextView txtMsg  = (TextView) dialogView.findViewById(R.id.txtYouneedMoreCoins);
        Button btnCancel = (Button) dialogView.findViewById(R.id.btnNoThanks);
        Button btnOk     = (Button) dialogView.findViewById(R.id.btnYes);

        Translation transeletion = new Translation(context);
        transeletion.openConnection();
        //txtMsg.setText(transeletion.getTranselationTextByTextIdentifier("lblMakeAppAdsFree"));
        txtMsg.setText("Don't like ads?  Please upgrade to any of our categories and you will not receive any more ads.");
        btnCancel.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleCancel"));
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        transeletion.closeConnection();

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeViewFromWM(wm , view);
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeViewFromWM(wm , view);
            }
        });
        return dialogView;
    }

    private void showViewInWM(Context context){
        try{
            WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    0,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                            | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                    PixelFormat.TRANSLUCENT);
            params.gravity = Gravity.CENTER;
            wm = (WindowManager) MyApplication.getAppContext()
                    .getSystemService(Context.WINDOW_SERVICE);
            view = this.getView(context);
            wm.addView(view, params);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void removeViewFromWM(WindowManager wm , View view){
        if(wm != null && view != null){
            NUMBER_OF_SCREEN_COUNTER = 0;
            wm.removeView(view);
        }
    }
}
