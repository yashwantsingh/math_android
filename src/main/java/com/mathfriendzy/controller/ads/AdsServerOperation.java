package com.mathfriendzy.controller.ads;

import android.util.Log;

import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.utils.CommonUtils;

import org.json.JSONException;
import org.json.JSONObject;

import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;

/**
 * This class contains the server operation related to the server
 * @author Yashwant Singh
 *
 */
public class AdsServerOperation {

	private final String TAG = this.getClass().getSimpleName();
	
	/**
	 * This method get the ads frequesncies means how ads will be shown after number of screen
	 * @return
	 */
	public AddFrequency getFrequesncies(){
		String strUrl = COMPLETE_URL + "action=getAdsFrequencies&appId="
                + CommonUtils.APP_ID;
		return this.parseResponseJsonString(CommonUtils.readFromURL(strUrl));
	}

	/**
	 * This metod parse the json String for getting frequency
	 * @param jsonString
	 * @return
	 */
	private AddFrequency parseResponseJsonString(String jsonString){
        if(jsonString != null && jsonString.length() > 0){
            try {
                if(CommonUtils.LOG_ON)
                    Log.e(TAG , "frequency response " + jsonString);
                AddFrequency frequesncyObj = new AddFrequency();
                JSONObject json = new JSONObject(jsonString);
                frequesncyObj.setData(json.getInt("data"));
                frequesncyObj.setTimeIntervalFullAd(json.getInt("timeIntervalFullAd"));
                frequesncyObj.setTimeIntervalFullAdForPaid(json.getInt("timeIntervalFullAdForPaid"));
                if(MathFriendzyHelper.checkForKeyExistInJsonObj(json, "houseAdFrequency")){
                    frequesncyObj.setHouseAdsFrequemcy(json.getInt("houseAdFrequency"));
                }
                if(MathFriendzyHelper.checkForKeyExistInJsonObj(json, "rateUsFrequency")){
                    frequesncyObj.setRatePopUpFrequency(json.getInt("rateUsFrequency"));
                }

                if(MathFriendzyHelper.checkForKeyExistInJsonObj(json, "houseAdImage")){
                    frequesncyObj.setHouseAdImageName(json.getString("houseAdImage"));
                }

                if(MathFriendzyHelper.checkForKeyExistInJsonObj(json, "houseAdUrl")){
                    frequesncyObj.setHouseAdUrl(json.getString("houseAdUrl"));
                }

                if(MathFriendzyHelper.checkForKeyExistInJsonObj(json, "houseAdUrlAndroid")){
                    frequesncyObj.setHoudeAdAndroidUrl(json.getString("houseAdUrlAndroid"));
                }
                return frequesncyObj;
            } catch (JSONException e) {
                Log.e(TAG, "inside parseResponseJsonString : " +
                        "Error while parsing " + e.toString());
                return null;
            }
        }
        return null;
	}
	
	/**
	 * This method update the add purchase status after purchasing any coins pack
	 * @param userId
	 */
	public void updateAdsPurchaseStatus(String userId){
		String strUrl = COMPLETE_URL + "action=updateAdsPurchaseStatus" + "&" 
						+ "userId=" + userId + "&" 
				 		+ "appId=" + CommonUtils.APP_ID;
		this.parseJsonStringForUpdateAdsPurchaseStatus(CommonUtils.readFromURL(strUrl));
	}
	
	/**
	 * This method parse the json String for the updateAdsPurchaseStatus
	 * @param jsonString
	 */
	private void parseJsonStringForUpdateAdsPurchaseStatus(String jsonString){
		Log.e(TAG, "inside parseJsonStringForUpdateAdsPurchaseStatus json String " + jsonString);
	}
}
