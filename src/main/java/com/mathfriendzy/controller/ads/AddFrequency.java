package com.mathfriendzy.controller.ads;

/**
 * Created by root on 22/9/15.
 */
public class AddFrequency {
    private int data;
    private int timeIntervalFullAd;
    private int timeIntervalFullAdForPaid;
    private int houseAdsFrequemcy;
    private int ratePopUpFrequency;
    private String houseAdImageName;
    private String houseAdUrl;
    private String houdeAdAndroidUrl;

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public int getTimeIntervalFullAd() {
        return timeIntervalFullAd;
    }

    public void setTimeIntervalFullAd(int timeIntervalFullAd) {
        this.timeIntervalFullAd = timeIntervalFullAd;
    }

    public int getTimeIntervalFullAdForPaid() {
        return timeIntervalFullAdForPaid;
    }

    public void setTimeIntervalFullAdForPaid(int timeIntervalFullAdForPaid) {
        this.timeIntervalFullAdForPaid = timeIntervalFullAdForPaid;
    }

    public int getHouseAdsFrequemcy() {
        return houseAdsFrequemcy;
    }

    public void setHouseAdsFrequemcy(int houseAdsFrequemcy) {
        this.houseAdsFrequemcy = houseAdsFrequemcy;
    }

    public int getRatePopUpFrequency() {
        return ratePopUpFrequency;
    }

    public void setRatePopUpFrequency(int ratePopUpFrequency) {
        this.ratePopUpFrequency = ratePopUpFrequency;
    }

    public String getHouseAdImageName() {
        return houseAdImageName;
    }

    public void setHouseAdImageName(String houseAdImageName) {
        this.houseAdImageName = houseAdImageName;
    }

    public String getHouseAdUrl() {
        return houseAdUrl;
    }

    public void setHouseAdUrl(String houseAdUrl) {
        this.houseAdUrl = houseAdUrl;
    }

    public String getHoudeAdAndroidUrl() {
        return houdeAdAndroidUrl;
    }

    public void setHoudeAdAndroidUrl(String houdeAdAndroidUrl) {
        this.houdeAdAndroidUrl = houdeAdAndroidUrl;
    }
}
