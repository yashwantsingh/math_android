package com.mathfriendzy.controller.ads;

import com.mathfriendzy.utils.ICommonUtils;

public class AdsResponseFileParser {
	
	public static CustomeHouseAdsData parseCustomeHouseAdsData(String response){
		CustomeHouseAdsData customeAdsData = new CustomeHouseAdsData();		
		customeAdsData.setImageName("icon_mathfriendzy");
		//customeAdsData.setLink("http://www.mathtutorplus.com/");
        customeAdsData.setLink(ICommonUtils.HTTP + "://www.mathtutorplus.com/");
		return customeAdsData;
	}
}
