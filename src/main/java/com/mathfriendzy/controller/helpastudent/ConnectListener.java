package com.mathfriendzy.controller.helpastudent;

import com.mathfriendzy.model.helpastudent.GetPlayerNeedHelpForTutorResponse;

/**
 * Created by root on 24/4/15.
 */
public interface ConnectListener {
    void clickOnView(GetPlayerNeedHelpForTutorResponse connectedTutor);
}
