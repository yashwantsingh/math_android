package com.mathfriendzy.controller.helpastudent;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.tutor.ActTutorSession;
import com.mathfriendzy.customview.FiveStarRateView;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.OnRequestComplete;
import com.mathfriendzy.model.helpastudent.AddServiceTimeForTutorParam;
import com.mathfriendzy.model.helpastudent.ConnectWithStudentParam;
import com.mathfriendzy.model.helpastudent.ConnectWithTutorResponse;
import com.mathfriendzy.model.helpastudent.GetPlayerNeedHelpForTutorParam;
import com.mathfriendzy.model.helpastudent.GetPlayerNeedHelpForTutorResponse;
import com.mathfriendzy.model.helpastudent.LoadServiceTimeParam;
import com.mathfriendzy.model.helpastudent.LoadServiceTimeResponse;
import com.mathfriendzy.model.helpastudent.UpdateChatRequestWithDialogParam;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.tutor.QBLoginSuccessCallback;
import com.mathfriendzy.model.tutor.RegisterUserOnQBCallback;
import com.mathfriendzy.quickblox.QBCreateSession;
import com.mathfriendzy.quickblox.QBDialogs;
import com.mathfriendzy.quickblox.QuickBloxSingleton;
import com.mathfriendzy.quickblox.core.ChatManager;
import com.mathfriendzy.quickblox.core.GroupChatManagerImpl;
import com.mathfriendzy.quickblox.core.MyChatCallBack;
import com.mathfriendzy.quickblox.core.PrivateChatManagerImpl;
import com.mathfriendzy.quickblox.qbcallbacks.QBCreateDialogCallback;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.serveroperation.serveroperationparamresponseclasses.GetActiveInActiveStatusOfStudentForTutorResponse;
import com.mathfriendzy.socketprograming.ClientCallback;
import com.mathfriendzy.socketprograming.MyGlobalWebSocket;
import com.mathfriendzy.timer.MyTimer;
import com.mathfriendzy.timer.TimerProgressCallback;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.users.model.QBUser;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static com.mathfriendzy.helper.MathFriendzyHelper.isUserAlreadyRegisterOnQuickBlox;
import static com.mathfriendzy.helper.MathFriendzyHelper.loginUserToQuickBlox;
import static com.mathfriendzy.helper.MathFriendzyHelper.registerUserToQuickBlox;


public class ActHelpAStudent extends ActBase implements MyChatCallBack , TimerProgressCallback , ClientCallback {

    private final String TAG = this.getClass().getSimpleName();
    private ImageView imgInfo = null;
    private TextView txtStudentIAmHelping = null;
    private TextView txtStudentWhoNeedHelp = null;
    private TextView txtHelperName = null;
    private TextView txtHelperGrade = null;
    private TextView txtHelperLastModified = null;
    private TextView txtHelperOnlineOrNot = null;
    private TextView txtHelperView = null;
    private TextView txtHelperDisconnect = null;
    private TextView txtNeedHelperName = null;
    private TextView txtNeedHelperGrade = null;
    private TextView txtNeedHelperLastModified = null;
    private TextView txtNeedHelperOnlineOrNot = null;
    private TextView txtNeedHelperConnect = null;

    private UserPlayerDto selectedPlayer = null;

    private LinearLayout connectedStudentsLayout = null;
    private LinearLayout disconnectedStudentsLayout = null;
    private ArrayList<ViewHolder> connectedViewHolderList = null;
    private ViewHolder clickedViewHolder = null;
    private ArrayList<ViewHolder> disconnectedViewHolderList = null;
    private final int ADD_TUTOR_IN_LIST_SLEEP_TIME = 100;
    private final String SERVER_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private final String CONVERTED_DATE_FORMAT = "MMM. dd,yyyy HH:mm:ss";
    private ArrayList<GetPlayerNeedHelpForTutorResponse> connectedTutorList = null;
    private ArrayList<GetPlayerNeedHelpForTutorResponse> disConnectedTutorList = null;

    private String lblNoChatAvailableWithStudent = null;

    //for qb Chat
    private ChatManager chat;
    private String initialMessage = "";
    private GetPlayerNeedHelpForTutorResponse connectedTutor = null;

    private final int YES = 1;
    private final int NO = 0;
    private String anonymousText = "Anonymous";

    //for time spent data
    private final int STAR_ACT_TUTOR_SEESION = 1001;

    private Button btnShowMoreHelpingStudent = null;
    private Button btnShowMoreNeedHelpStudent = null;

    private ConnectedListAdapter connetedAdapter = null;
    private DisconnectedListAdapter disConnectedAdapter = null;
    private ListView lstConnectedStudents = null;
    private ListView lstDisconnectedStudents = null;
    private int MAX_OFFSET_LIMIT = 20;
    private GetPlayerNeedHelpForTutorParam param = null;


    //for total time
    private TextView txtTotalTimeHelpingStudent = null;
    private TextView txtFrom = null;
    private TextView txtFromDate = null;
    private RelativeLayout fromLayout = null;
    private TextView txtTo = null;
    private TextView txtToDate = null;
    private RelativeLayout toLayout = null;
    private TextView txtTime = null;
    private TextView txtHours = null;

    //for load service time
    private String startDate = "";
    private String endDate = "";
    private final String SERVICE_TIME_MY_FORMATE = "yyyy-MM-dd";
    private final String SERVICE_TIME_CONVERTED_FORMATE = "MMM. dd,yyyy";
    private String lblStartDateCantLater = null;

    private RelativeLayout needHelpMainLayout = null;
    private RelativeLayout iAmHereLayout = null;

    //from I am Here leyout
    private TextView txtStudentsWhoNeedHelp = null;
    private TextView txtNoStudents = null;
    private TextView txtServiceHours = null;
    private Button btnAmHere = null;
    private TextView txtTimeAmHere = null;
    private TextView txtTapAgain = null;
    private final int COUNT_DOWN_TIME = 300;
    private MyTimer timer = null;

    private String timeFormat = "HH:mm:ss";
    private String startTime = "07:00:00";
    private String endTime   = "21:00:00";
    private long totalServiceTime = 0;

    private static ActHelpAStudent currentObj = null;

    //for account type
    private final String TEACHER = "0";
    private RegistereUserDto loginUser = null;

    private Handler checkActiveInactiveIamHareStatus = null;
    private final long ACTIVE_INACTIVE_TIMER_DIFF_FOR_I_AM_HARE = 1000 * 60;
    private boolean isIamHareActive = false;

    private TextView txtAskingPersonalInfo = null;
    private ProgressDialog progDialog = null;

    private String lblNoStudentRequireHelp = null;
    private String studentNotATutorAlertTitle = null;

    private ImageView imgStar1 = null;
    private ImageView imgStar2 = null;
    private ImageView imgStar3 = null;
    private ImageView imgStar4 = null;
    private ImageView imgStar5 = null;

    private FiveStarRateView fiveStartRatingView = null;
    private  TextView txtAverageRating = null;

    private final String USER_PLAYER_IS_SEPARATOR = "_";
    private Handler getStudentActiveInActiveStatus = null;
    private Runnable runnableThread = null;
    private final long GET_ACTIVE_INACTIVE_STATUS_REFRESH_TIME = 1000 * 10;

    //private boolean isUpdateFromNotification = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_help_astudent);

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside onCreate()");

        selectedPlayer = this.getPlayerData();

        this.initializeCurrentObj();
        this.init();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.loadDataFromServer();
        //this.checkForRegisterOnQB();
        this.initializeHandlerAndStart();
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "outside onCreate()");
    }

    private void checkForRegisterOnQB(){
        this.checkForSelectedPlayerAlreadyRegistered(new OnRequestComplete() {
            @Override
            public void onComplete() {
                //Log.e(TAG , "Register success");
            }
        });
    }

    /**
     * Load the data from server
     */
    private void loadDataFromServer(){
        try {
            if (selectedPlayer != null) {
                this.getPlayersNeedHelpTutor(true, 0);
                this.loadServiceTime();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        MathFriendzyHelper.initializeProcessDialog(this);
        super.onResume();
    }

    private void init(){

        loginUser = CommonUtils.getLoginUser(this);
        timer = MyTimer.getInstance(this);
        timer.initializeCallback(this);

        connectedTutorList = new ArrayList<GetPlayerNeedHelpForTutorResponse>();
        disConnectedTutorList = new ArrayList<GetPlayerNeedHelpForTutorResponse>();
        connectedViewHolderList = new ArrayList<ViewHolder>();
        disconnectedViewHolderList = new ArrayList<ViewHolder>();

        checkActiveInactiveIamHareStatus = new Handler();

        fiveStartRatingView = new FiveStarRateView(this);
    }

    @Override
    protected void setWidgetsReferences() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside setWidgetsReferences()");

        txtTopbar 		= (TextView) findViewById(R.id.txtTopbar);
        imgInfo         = (ImageView) findViewById(R.id.imgInfo);
        txtStudentIAmHelping    = (TextView) findViewById(R.id.txtStudentIAmHelping);
        txtStudentWhoNeedHelp   = (TextView) findViewById(R.id.txtStudentWhoNeedHelp);
        txtHelperName   = (TextView) findViewById(R.id.txtHelperName);
        txtHelperGrade  = (TextView) findViewById(R.id.txtHelperGrade);
        txtHelperLastModified   = (TextView) findViewById(R.id.txtHelperLastModified);
        txtHelperOnlineOrNot    = (TextView) findViewById(R.id.txtHelperOnlineOrNot);
        txtHelperView   = (TextView) findViewById(R.id.txtHelperView);
        txtHelperDisconnect     = (TextView) findViewById(R.id.txtHelperDisconnect);
        txtNeedHelperName       = (TextView) findViewById(R.id.txtNeedHelperName);
        txtNeedHelperGrade      = (TextView) findViewById(R.id.txtNeedHelperGrade);
        txtNeedHelperLastModified = (TextView) findViewById(R.id.txtNeedHelperLastModified);
        txtNeedHelperOnlineOrNot= (TextView) findViewById(R.id.txtNeedHelperOnlineOrNot);
        txtNeedHelperConnect    = (TextView) findViewById(R.id.txtNeedHelperConnect);

        connectedStudentsLayout = (LinearLayout) findViewById(R.id.connectedStudentsLayout);
        disconnectedStudentsLayout = (LinearLayout) findViewById(R.id.disconnectedStudentsLayout);

        btnShowMoreHelpingStudent = (Button) findViewById(R.id.btnShowMoreHelpingStudent);
        btnShowMoreNeedHelpStudent = (Button) findViewById(R.id.btnShowMoreNeedHelpStudent);

        lstConnectedStudents = (ListView) findViewById(R.id.lstConnectedStudents);
        lstDisconnectedStudents = (ListView) findViewById(R.id.lstDisconnectedStudents);


        //for total time
        txtTotalTimeHelpingStudent = (TextView) findViewById(R.id.txtTotalTimeHelpingStudent);
        txtFrom = (TextView) findViewById(R.id.txtFrom);
        txtFromDate = (TextView) findViewById(R.id.txtFromDate);
        txtTo = (TextView) findViewById(R.id.txtTo);
        txtToDate = (TextView) findViewById(R.id.txtToDate);
        txtTime = (TextView) findViewById(R.id.txtTime);
        txtHours = (TextView) findViewById(R.id.txtHours);
        fromLayout = (RelativeLayout) findViewById(R.id.fromLayout);
        toLayout = (RelativeLayout) findViewById(R.id.toLayout);


        //for i am here layout
        needHelpMainLayout = (RelativeLayout) findViewById(R.id.needHelpMainLayout);
        iAmHereLayout = (RelativeLayout) findViewById(R.id.iAmHereLayout);

        //from I am here layout
        txtStudentsWhoNeedHelp = (TextView)  findViewById(R.id.txtStudentsWhoNeedHelp);
        txtNoStudents = (TextView)  findViewById(R.id.txtNoStudents);
        txtServiceHours = (TextView)  findViewById(R.id.txtServiceHours);
        btnAmHere = (Button )  findViewById(R.id.btnAmHere);
        txtTimeAmHere = (TextView)  findViewById(R.id.txtTimeAmHere);
        txtTapAgain = (TextView) findViewById(R.id.txtTapAgain);

        txtAskingPersonalInfo = (TextView) findViewById(R.id.txtAskingPersonalInfo);

        txtAverageRating = (TextView) findViewById(R.id.txtAverageRating);
        fiveStartRatingView.setWidgetsReferences();

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "outside setWidgetsReferences()");
    }

    @Override
    protected void setTextFromTranslation() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside setTextFromTranslation()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblHelpAStudent"));
        txtStudentIAmHelping.setText(transeletion.getTranselationTextByTextIdentifier("lblStudentsIAmHelping"));
        txtStudentWhoNeedHelp.setText(transeletion.getTranselationTextByTextIdentifier("lblStudentsWhoNeedHelp"));
        txtHelperName.setText(transeletion.getTranselationTextByTextIdentifier("lblRegName"));
        txtHelperGrade.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade"));
        txtHelperLastModified.setText(transeletion.getTranselationTextByTextIdentifier("lblLastModified"));
        txtHelperOnlineOrNot.setText(transeletion.getTranselationTextByTextIdentifier("lblOnlineOrNot"));
        txtHelperView.setText(transeletion.getTranselationTextByTextIdentifier("lblView"));
        //txtHelperDisconnect.setText(transeletion.getTranselationTextByTextIdentifier("lblDisc"));
        txtNeedHelperName.setText(transeletion.getTranselationTextByTextIdentifier("lblRegName"));
        txtNeedHelperGrade.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade"));
        txtNeedHelperLastModified.setText(transeletion.getTranselationTextByTextIdentifier("lblLastModified"));
        txtNeedHelperOnlineOrNot.setText(transeletion.getTranselationTextByTextIdentifier("lblOnlineOrNot"));
        txtNeedHelperConnect.setText(transeletion.getTranselationTextByTextIdentifier("lblFbConnect"));

        btnShowMoreHelpingStudent.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleShowMore"));
        btnShowMoreNeedHelpStudent.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleShowMore"));

        lblNoChatAvailableWithStudent = transeletion.getTranselationTextByTextIdentifier
                ("lblNoChatAvailableWithStudent");
        anonymousText = transeletion.getTranselationTextByTextIdentifier("lblAnonymous");

        //for total time
        txtTotalTimeHelpingStudent.setText(transeletion.getTranselationTextByTextIdentifier("lblHelpingTime"));
        txtFrom.setText(transeletion.getTranselationTextByTextIdentifier("lblFrom") + ":");
        txtTo.setText(transeletion.getTranselationTextByTextIdentifier("lblTo") + ":");
        txtHours.setText(transeletion.getTranselationTextByTextIdentifier("lblHours"));
        lblStartDateCantLater = transeletion.getTranselationTextByTextIdentifier("lblStartDateCantLater");


        //from i am here layout
        txtStudentsWhoNeedHelp.setText(transeletion.getTranselationTextByTextIdentifier
                ("lblStudentsWhoNeedHelp"));
        txtNoStudents.setText(transeletion.getTranselationTextByTextIdentifier("lblNoStudentRequireHelp"));
        txtServiceHours.setText(transeletion.getTranselationTextByTextIdentifier("lblServiceHoursAndCreditDetail"));
        btnAmHere.setText(transeletion.getTranselationTextByTextIdentifier("btnIAmHereTitle"));
        txtTapAgain.setText(transeletion.getTranselationTextByTextIdentifier("lblForIAmHere"));
        txtAskingPersonalInfo.setText(transeletion.getTranselationTextByTextIdentifier("lblAskingPersonalInfo"));
        lblNoStudentRequireHelp = transeletion.getTranselationTextByTextIdentifier("lblNoStudentRequireHelp");
        studentNotATutorAlertTitle = transeletion.getTranselationTextByTextIdentifier("studentNotATutorAlertTitle");
        /*txtNoStudents.setText("There are no students who require help at this time.");
        txtServiceHours.setText("But you can still get \"Service Hour\" or school credit between 2:00 PM and 9:00 PM by tapping on the \"I Am Here\" button and keep hitting it before the counter gets to zero. If a student requires your help, you must help the student or you won't get credit for this time.");
        btnAmHere.setText("I AM HERE");
        txtTapAgain.setText("Tap again before the counter gets to zero to get credit for this time.");*/
        txtAverageRating.setText(transeletion.getTranselationTextByTextIdentifier("lblAverage")
                + " " + transeletion.getTranselationTextByTextIdentifier("lblRating") + ":");
        transeletion.closeConnection();

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "outside setTextFromTranslation()");
    }

    @Override
    protected void setListenerOnWidgets() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside setListenerOnWidgets()");

        imgInfo.setOnClickListener(this);
        btnShowMoreHelpingStudent.setOnClickListener(this);
        btnShowMoreNeedHelpStudent.setOnClickListener(this);
        fromLayout.setOnClickListener(this);
        toLayout.setOnClickListener(this);
        btnAmHere.setOnClickListener(this);

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "outside setListenerOnWidgets()");
    }

    /**
     * Up
     */
    public void updateUIFromNotofication(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //isUpdateFromNotification = true;
                getPlayersNeedHelpTutor(true , 0);
            }
        });
    }

    public void updateWhenUSerPressNoThanksFromNotification(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                updateUIAndAddServiceTimeWhenNeeded();
            }
        });
    }

    private void updateUIAndAddServiceTimeWhenNeeded() {
        this.setActiveInactiveStatusOfIamHareButton(false);
        this.removeIamHareHandlerCallback();
        this.stopTimerwhenClickOnConnectedList();
    }

    /**
     * Get player need help
     */
    public void getPlayersNeedHelpTutor(boolean firstTime , int isConnect){
        GetPlayerNeedHelpForTutorParam param = new GetPlayerNeedHelpForTutorParam();
        param.setAction("getPlayersNeedHelpForTutor");
        param.setForSubject(1);
        if (loginUser != null
                && loginUser.getIsParent().equalsIgnoreCase(TEACHER)) {
            param.setIsTutor("1");
        } else {
            param.setIsTutor(selectedPlayer.getIsTutor());
        }

        param.setPlayerId(selectedPlayer.getPlayerid());
        param.setUserId(selectedPlayer.getParentUserId());
        param.setGrade(selectedPlayer.getGrade());
        param.setFirstTime(firstTime);
        if (!firstTime) {
            param.setIsConnected(isConnect);
            if (isConnect == 1) {//for connected list
                if (connetedAdapter != null)
                    param.setOffSet(connetedAdapter.connectedTutors.size());
                else
                    param.setOffSet(0);
            } else {//for disconnected list
                if (disConnectedAdapter != null)
                    param.setOffSet(disConnectedAdapter.disconnectedTutors.size());
                else
                    param.setOffSet(0);
            }
        }
        this.param = param;
        if (CommonUtils.isInternetConnectionAvailable(this)) {
            new MyAsyckTask(ServerOperation
                    .createPostRequestForGetPlayersNeedHelpTutor(param)
                    , null, ServerOperationUtil.GET_PLAYER_NEED_HELP_FOR_TUTOR_REQUEST, this,
                    this, ServerOperationUtil.SIMPLE_DIALOG, true,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        } else {
            CommonUtils.showInternetDialog(this);
        }
    }


    /**
     * Create connected tutor view
     * @param connectedTutor
     * @return
     */
    private ViewContainer getConnectedView(final GetPlayerNeedHelpForTutorResponse connectedTutor){
        ViewContainer viewContainer = new ViewContainer();
        ViewHolder viewHolder = new ViewHolder();
        View view = LayoutInflater.from(this).inflate(R.layout.helping_student_layout , null);
        viewHolder.txtName = (TextView) view.findViewById(R.id.txtHelperName);
        viewHolder.txtGrade = (TextView) view.findViewById(R.id.txtHelperGrade);
        viewHolder.txtLastModified = (TextView) view.findViewById(R.id.txtHelperLastModified);
        viewHolder.txtTimeSpent = (TextView) view.findViewById(R.id.txtHelperTimeSpent);
        viewHolder.txtOnline = (TextView) view.findViewById(R.id.txtHelperOnlineOrNot);
        viewHolder.txtView = (TextView) view.findViewById(R.id.txtHelperView);
        viewHolder.txtDisconnect = (TextView) view.findViewById(R.id.txtHelperDisconnect);

        if(connectedTutor.getIsAnonymous() == YES){
            viewHolder.txtName.setText(anonymousText);
        }else {
            viewHolder.txtName.setText(connectedTutor.getPlayerFName() + " "
                    + connectedTutor.getPlayerLName());
        }

        viewHolder.txtGrade.setText(connectedTutor.getGrade() + "");
        viewHolder.txtLastModified.setText(MathFriendzyHelper.formatDataInGivenFormatWithDefaultTimeZone
                (connectedTutor.getReqDate(), SERVER_DATE_FORMAT, CONVERTED_DATE_FORMAT));
        viewHolder.txtTimeSpent.setText(MathFriendzyHelper.getTimeSpentText(connectedTutor.getTimeSpent()));

        viewHolder.txtView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i  = 0 ; i < connectedViewHolderList.size() ; i ++ ){
                    if(v == connectedViewHolderList.get(i).txtView){
                        //Log.e(TAG , "index " + i);
                        clickedViewHolder = connectedViewHolderList.get(i);
                        clickToViewHelpingStudent(connectedTutor , true);
                    }
                }
            }
        });

        viewContainer.view = view;
        viewContainer.viewHolder = viewHolder;
        return viewContainer;
    }

    /**
     * Create disconnected tutor view
     * @param disConnectedTutor
     * @return
     */
    private ViewContainer getDisconnectedView(final GetPlayerNeedHelpForTutorResponse disConnectedTutor){
        ViewContainer viewContainer = new ViewContainer();
        ViewHolder viewHolder = new ViewHolder();
        View view = LayoutInflater.from(this).inflate(R.layout.need_help_student_layout , null);
        viewHolder.txtName = (TextView) view.findViewById(R.id.txtNeedHelperName);
        viewHolder.txtGrade = (TextView) view.findViewById(R.id.txtNeedHelperGrade);
        viewHolder.txtLastModified = (TextView) view.findViewById(R.id.txtNeedHelperLastModified);
        viewHolder.txtOnline = (TextView) view.findViewById(R.id.txtNeedHelperOnlineOrNot);
        viewHolder.txtConnect = (TextView) view.findViewById(R.id.txtNeedHelperConnect);

        if(disConnectedTutor.getIsAnonymous() == YES){
            viewHolder.txtName.setText(anonymousText);
        }else {
            viewHolder.txtName.setText(disConnectedTutor.getPlayerFName() + " "
                    + disConnectedTutor.getPlayerLName());
        }

        viewHolder.txtGrade.setText(disConnectedTutor.getGrade() + "");
        viewHolder.txtLastModified.setText(MathFriendzyHelper.formatDataInGivenFormatWithDefaultTimeZone
                (disConnectedTutor.getReqDate(), SERVER_DATE_FORMAT, CONVERTED_DATE_FORMAT));


        viewHolder.txtConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < disconnectedViewHolderList.size(); i++) {
                    if (disconnectedViewHolderList.get(i).txtConnect == v) {
                        if(CommonUtils.isInternetConnectionAvailable(ActHelpAStudent.this)){
                            final int index = i;
                            ConnectWithStudentParam param = new ConnectWithStudentParam();
                            param.setAction("connectWithStudent");
                            param.setData(getConnectWithStudentJson(disConnectedTutor));

                            new MyAsyckTask(ServerOperation
                                    .createPostRequestForConnectWithStudent(param)
                                    , null, ServerOperationUtil.CONNECT_WITH_STUDENT_REQUEST,
                                    ActHelpAStudent.this,
                                    new HttpResponseInterface() {
                                        @Override
                                        public void serverResponse(HttpResponseBase httpResponseBase,
                                                                   int requestCode) {
                                            if(requestCode ==
                                                    ServerOperationUtil.CONNECT_WITH_STUDENT_REQUEST){
                                                ConnectWithTutorResponse response =
                                                        (ConnectWithTutorResponse) httpResponseBase;
                                                if(response.getResult().equalsIgnoreCase("success")){
                                                    int newIndex = getNewIndex(disConnectedTutor);
                                                    connectedTutorList.add(newIndex , disConnectedTutor);
                                                    addConnectedView(disConnectedTutor, newIndex);
                                                    disconnectedStudentsLayout.removeViewAt(index);
                                                    disconnectedViewHolderList.remove(index);
                                                    disConnectedTutorList.remove(index);
                                                    disconnectedStudentsLayout.invalidate();
                                                    clickToViewHelpingStudent(disConnectedTutor , false);
                                                    clickedViewHolder = connectedViewHolderList.get(newIndex);
                                                }
                                            }
                                        }
                                    }, ServerOperationUtil.SIMPLE_DIALOG , true ,
                                    getString(R.string.please_wait_dialog_msg))
                                    .execute();
                        }else{
                            CommonUtils.showInternetDialog(ActHelpAStudent.this);
                        }
                        break;
                    }
                }
            }
        });
        viewContainer.view = view;
        viewContainer.viewHolder = viewHolder;
        return viewContainer;
    }

    /**
     * Set number of active oponent input
     * @param connectedTutor
     */
    private void setActiveOponentInputResponse(GetPlayerNeedHelpForTutorResponse connectedTutor
            , boolean isFromConnectedList){
        try {
            if (!isFromConnectedList || connectedTutor.getOpponentInput()
                    == MathFriendzyHelper.YES) {
                int totalNumberOfActiveInput = MathFriendzyHelper.getTutorAreaResponses(this,
                        selectedPlayer.getParentUserId()
                        , selectedPlayer.getPlayerid());
                if (totalNumberOfActiveInput > 0) {
                    totalNumberOfActiveInput--;
                    MathFriendzyHelper.saveTutorAreaResponse(this, selectedPlayer.getParentUserId()
                            , selectedPlayer.getPlayerid(), totalNumberOfActiveInput);
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void showProgressDialog(){
        progDialog = new ProgressDialog(this);
        progDialog.setIndeterminate(false);//back button does not work
        progDialog.setCancelable(false);
        progDialog.setMessage("Please Wait...");
        progDialog.show();
    }

    private void hideProgressDialog(){
        if(progDialog != null && progDialog.isShowing())
            progDialog.cancel();
    }

    private void checkForSelectedPlayerAlreadyRegistered(final OnRequestComplete callback){
        if (isUserAlreadyRegisterOnQuickBlox(selectedPlayer)) {
            callback.onComplete();
        } else {
            showProgressDialog();
            registerUserToQuickBlox(selectedPlayer, this,
                    new RegisterUserOnQBCallback() {
                        @Override
                        public void onSuccess(QBUser qbUser, Bundle bundle) {
                            selectedPlayer = getPlayerData();
                            hideProgressDialog();
                            callback.onComplete();
                        }
                    });
        }
    }

    private void clickToViewHelpingStudent(final GetPlayerNeedHelpForTutorResponse connectedTutor ,
                                           final boolean fromConnectedList){
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "chat dialog id " + connectedTutor.getChatDialogId());
        if(fromConnectedList){
            this.stopTimerwhenClickOnConnectedList();
        }

        if(fromConnectedList){
            connectedTutor.setFromDisconnectedList(false);
        }else{
            connectedTutor.setFromDisconnectedList(true);
        }
        ActHelpAStudent.this.goForTutorScreen(connectedTutor);

       /* this.checkForSelectedPlayerAlreadyRegistered(new OnRequestComplete() {
            @Override
            public void onComplete() {
                ActHelpAStudent.this.connectedTutor = connectedTutor;
                ActHelpAStudent.this.setActiveOponentInputResponse(connectedTutor
                        , fromConnectedList);
                if(fromConnectedList){
                    ActHelpAStudent.this.connectedTutor.setFromDisconnectedList(false);
                    if(connectedTutor.getChatDialogId() != null
                            && connectedTutor.getChatDialogId().length() > 0){
                        ActHelpAStudent.this.goForTutorScreen(connectedTutor);
                    }else{
                        clickToViewHelpingStudent(connectedTutor , false);
                    }
                }else{
                    ActHelpAStudent.this.connectedTutor.setFromDisconnectedList(true);
                    if(connectedTutor.getChatDialogId() != null
                            && connectedTutor.getChatDialogId().length() > 0){
                        ActHelpAStudent.this.goForTutorScreen(connectedTutor);
                    }else{
                        QBCreateSession.getInstance(ActHelpAStudent.this)
                                .logoutFromChat();//logout the current login user

                        if(CommonUtils.LOG_ON)
                            Log.e(TAG , "connectedTutor.getChatId() " + connectedTutor.getChatId()
                                    + " " + connectedTutor.getChatUserName());

                        //create new player based on the conneted student
                        final UserPlayerDto createPlayerFromConnectedResponse = new UserPlayerDto();
                        createPlayerFromConnectedResponse.setChatId(connectedTutor.getChatId());
                        createPlayerFromConnectedResponse.setChatUserName(connectedTutor.getChatUserName());
                        createPlayerFromConnectedResponse.setFirstname(connectedTutor.getPlayerFName());
                        createPlayerFromConnectedResponse.setLastname(connectedTutor.getPlayerLName());
                        createPlayerFromConnectedResponse.setParentUserId(connectedTutor.getUserId());
                        createPlayerFromConnectedResponse.setPlayerid(connectedTutor.getPlayerId());
                        initialMessage = connectedTutor.getMessage();

                        if(MathFriendzyHelper.isUserAlreadyRegisterOnQuickBlox
                                (createPlayerFromConnectedResponse)){
                            ActHelpAStudent.this.loginToQuickBlox
                                    (createPlayerFromConnectedResponse);
                        }else{
                            MathFriendzyHelper.showDialog();
                            registerUserToQuickBlox(createPlayerFromConnectedResponse,
                                    ActHelpAStudent.this,
                                    new RegisterUserOnQBCallback() {
                                        @Override
                                        public void onSuccess(QBUser qbUser, Bundle bundle) {
                                            MathFriendzyHelper.dismissDialog();
                                           *//*createPlayerFromConnectedResponse.setChatId
                                                    (qbUser.getId() + "");
                                            createPlayerFromConnectedResponse
                                                    .setChatUserName(qbUser.getLogin());*//*
                                            loginToQuickBlox(createPlayerFromConnectedResponse);
                                            //loginSuccess(createPlayerFromConnectedResponse);
                                        }
                                    });
                        }
                    }
                }
            }
        });*/
    }

    private void loginToQuickBlox(final UserPlayerDto createPlayerFromConnectedResponse){
        MathFriendzyHelper.showDialog();
        loginUserToQuickBlox(createPlayerFromConnectedResponse,
                this, new QBLoginSuccessCallback() {
                    @Override
                    public void onLoginSuccess() {
                        MathFriendzyHelper.dismissDialog();
                        loginSuccess(createPlayerFromConnectedResponse);
                    }
                });
    }

    private void loginSuccess(UserPlayerDto createPlayerFromConnectedResponse ){
        this.createChatDialog(createPlayerFromConnectedResponse);
    }

    /**
     * Create chat dialog
     */
    private void createChatDialog(final UserPlayerDto createPlayerFromConnectedResponse ){
        try {
            MathFriendzyHelper.showDialog();
            ArrayList<Integer> occupentIds = new ArrayList<Integer>();
            occupentIds.add(MathFriendzyHelper.parseInt(selectedPlayer.getChatId()));
            new QBDialogs(this).createDialog("MathQBDialog", occupentIds,
                    new QBCreateDialogCallback() {
                        @Override
                        public void onSuccess(QBDialog dialog, Bundle args) {

                            if (CommonUtils.LOG_ON)
                                Log.e(TAG, "Dialog Created succesfully! " + dialog.getDialogId());

                            connectedTutor.setChatDialogId(dialog.getDialogId());
                            updateChatRequestWithDialog(dialog.getDialogId());
                            MathFriendzyHelper.dismissDialog();
                            joinChat(dialog , createPlayerFromConnectedResponse);
                        }
                    });
        }catch(Exception e){
            MathFriendzyHelper.dismissDialog();
        }
    }

    /**
     * Return the data json for the update chat request with dialog
     * @param newChatDialogId
     * @return
     */
    private String getUpdateChatRequestWithDialogData(String newChatDialogId){
        JSONObject jsonObject = new JSONObject();
        try {
            if(connectedTutor != null) {
                jsonObject.put("requestId", connectedTutor.getRequestId());
                jsonObject.put("reqUserId", connectedTutor.getUserId());
                jsonObject.put("reqPlayerId", connectedTutor.getPlayerId());
                jsonObject.put("requestDate", connectedTutor.getReqDate());
                jsonObject.put("message", connectedTutor.getMessage());
                jsonObject.put("isConnected", 1);
                jsonObject.put("tutorUserId", selectedPlayer.getParentUserId());
                jsonObject.put("tutorPlayerId", selectedPlayer.getPlayerid());
                jsonObject.put("chatDialogId", newChatDialogId);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
        return jsonObject.toString();
    }

    /**
     * Update the new dialog id on the server
     * @param newDialogId
     */
    private void updateChatRequestWithDialog(String newDialogId){
        UpdateChatRequestWithDialogParam param = new UpdateChatRequestWithDialogParam();
        param.setAction("updateChatRequestWithDialog");
        param.setData(this.getUpdateChatRequestWithDialogData(newDialogId));

        if(CommonUtils.isInternetConnectionAvailable(this)){
            new MyAsyckTask(ServerOperation.createPostRequestForUpdateChatRequestWithDialog(param)
                    , null, ServerOperationUtil.UPDATE_CHAT_REQUEST_WITH_DIALOG_REQUEST, this,
                    this, ServerOperationUtil.SIMPLE_DIALOG , false ,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        }
    }


    /**
     * Join the quick blox chat
     * @param dialog
     */
    private void joinChat(final QBDialog dialog ,
                          final UserPlayerDto createPlayerFromConnectedResponse){

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "joinChat " + dialog.getType());

        if(dialog.getType().equals(QBDialogType.GROUP)){
            MathFriendzyHelper.showDialog();
            chat = new GroupChatManagerImpl(this , this);
            ((GroupChatManagerImpl) chat).joinGroupChat(dialog, new QBEntityCallbackImpl() {
                @Override
                public void onSuccess() {
                    //Log.e(TAG , "join group success");
                    //loadChatHistory(dialog);

                    QBChatMessage chatMessage = new QBChatMessage();
                    chatMessage.setBody(initialMessage);
                    if(connectedTutor.getIsAnonymous() == 1)//for anonymous
                        chatMessage.setProperty("name", "Student");
                    else
                        chatMessage.setProperty("name", createPlayerFromConnectedResponse.getFirstname()
                                + " " + createPlayerFromConnectedResponse.getLastname());
                    chatMessage.setProperty("save_to_history", "1");

                    try {
                        chat.sendMessage(chatMessage);
                    } catch (XMPPException e) {
                        Log.e(TAG, "failed to send a message", e);
                    } catch (SmackException sme) {
                        Log.e(TAG, "failed to send a message", sme);
                    }catch(Exception e){
                        Log.e(TAG , "Exception " + e.toString());
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(List list) {
                    MathFriendzyHelper.dismissDialog();
                    AlertDialog.Builder dialog = new AlertDialog.Builder(ActHelpAStudent.this);
                    dialog.setMessage("error when join group chat: " + list.toString()).create().show();
                }
            });
        }else{
            Integer opponentID = QuickBloxSingleton.getInstance().getOpponentIDForPrivateDialog(dialog);
            chat = new PrivateChatManagerImpl(this, this , opponentID);
            //loadChatHistory(dialog);
        }
    }


    private void goForTutorScreen(GetPlayerNeedHelpForTutorResponse connectedTutor){
        Intent intent = new Intent(ActHelpAStudent.this , ActTutorSession.class);
        intent.putExtra("selectedTutor" , connectedTutor);
        startActivityForResult(intent, STAR_ACT_TUTOR_SEESION);
    }

    /**
     * Create the json for data to connect with student
     * @param disConnectedTutor
     * @return
     */
    private String getConnectWithStudentJson(GetPlayerNeedHelpForTutorResponse disConnectedTutor){
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("requestId" , disConnectedTutor.getRequestId());
            jsonObj.put("reqUserId" , disConnectedTutor.getUserId());
            jsonObj.put("reqPlayerId" , disConnectedTutor.getPlayerId());
            jsonObj.put("requestDate" , disConnectedTutor.getReqDate());
            jsonObj.put("message" , disConnectedTutor.getMessage());
            jsonObj.put("isConnected" , disConnectedTutor.getIsConnected());
            jsonObj.put("tutorUserId" ,   selectedPlayer.getParentUserId());
            jsonObj.put("tutorPlayerId" , selectedPlayer.getPlayerid());
            jsonObj.put("chatDialogId" , disConnectedTutor.getChatDialogId());
            return jsonObj.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }



    /**
     * Return the new index on which the new student add into the helping list from the need help list
     * @param disConnectedTutor
     * @return
     */
    private int getNewIndex(GetPlayerNeedHelpForTutorResponse disConnectedTutor){
        Date newConnetctedDate = MathFriendzyHelper.getValidateDate(disConnectedTutor.getReqDate()
                , SERVER_DATE_FORMAT);
        /*for(int i = 0 ; i < connectedTutorList.size() ; i ++ ){
            Date connectedDate = MathFriendzyHelper.getValidateDate(connectedTutorList.get(i).getReqDate()
                    , SERVER_DATE_FORMAT);
            int compare = connectedDate.compareTo(newConnetctedDate);
            if(compare < 0){
                return i;
            }
        }*/
        for(int i = 0 ; i < connetedAdapter.connectedTutors.size() ; i ++ ){
            Date connectedDate = MathFriendzyHelper.
                    getValidateDate(connetedAdapter.connectedTutors.get(i).getReqDate()
                            , SERVER_DATE_FORMAT);
            int compare = connectedDate.compareTo(newConnetctedDate);
            if(compare < 0){
                return i;
            }
        }
        return 0;
    }

    /**
     * Add the tutor to the connected list which is from the disconnected list
     * @param disConnectedTutor
     */
    private void addConnectedView(GetPlayerNeedHelpForTutorResponse disConnectedTutor , int index){
        ViewContainer viewContainer = this.getConnectedView(disConnectedTutor);
        connectedStudentsLayout.addView(viewContainer.view , index);
        connectedViewHolderList.add(index, viewContainer.viewHolder);
        connectedStudentsLayout.invalidate();
    }

    /**
     * Add tutor to the disconnected list which is from the connected list
     * @param connectedTutor
     */
    private void addDisconnectedView(GetPlayerNeedHelpForTutorResponse connectedTutor){
        ViewContainer viewContainer = this.getDisconnectedView(connectedTutor);
        disconnectedStudentsLayout.addView(viewContainer.view);
        disconnectedViewHolderList.add(viewContainer.viewHolder);
        disconnectedStudentsLayout.invalidate();
    }

    /**
     * Show connected tutor
     * @param connectedTutors
     */
    private void showConnectedTutor(final ArrayList<GetPlayerNeedHelpForTutorResponse> connectedTutors
            , final ArrayList<GetPlayerNeedHelpForTutorResponse> disConnectedTutorList){

        if(connectedTutors != null) {
            //this.connectedTutorList = connectedTutors;
            this.connectedTutorList.addAll(connectedTutors);
            //connectedStudentsLayout.removeAllViews();
            new AsyncTask<Void, Void, Void>() {

                @Override
                protected void onPreExecute() {
                    //MathFriendzyHelper.showDialog();
                    super.onPreExecute();
                }

                @Override
                protected Void doInBackground(Void... params) {
                    for (int i = 0; i < connectedTutors.size(); i++) {
                        final GetPlayerNeedHelpForTutorResponse connectedTutor
                                = connectedTutors.get(i);
                        final int index = i;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                addConnectedView(connectedTutor, index);
                            }
                        });
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    //MathFriendzyHelper.dismissDialog();
                    //showDisconnectedTutor(disConnectedTutorList);
                    super.onPostExecute(aVoid);
                }
            }.execute();
        }

    }

    /**
     * Show disconnected tutor
     * @param disconnectedTutors
     */
    private void showDisconnectedTutor(final ArrayList<GetPlayerNeedHelpForTutorResponse> disconnectedTutors){

        if(disconnectedTutors != null) {
            //this.disConnectedTutorList = disconnectedTutors;
            this.disConnectedTutorList.addAll(disconnectedTutors);
            //disconnectedStudentsLayout.removeAllViews();

            new AsyncTask<Void, Void, Void>() {

                @Override
                protected void onPreExecute() {
                    //MathFriendzyHelper.showDialog();
                    super.onPreExecute();
                }

                @Override
                protected Void doInBackground(Void... params) {
                    for (int i = 0; i < disconnectedTutors.size(); i++) {
                        final GetPlayerNeedHelpForTutorResponse disConnetcedTutor =
                                disconnectedTutors.get(i);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                addDisconnectedView(disConnetcedTutor);
                            }
                        });
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    //MathFriendzyHelper.dismissDialog();
                    super.onPostExecute(aVoid);
                }
            }.execute();
        }

    }


    /**
     * Set connected list adapter
     * @param connectedTutors
     */
    private void setConnectedAdapter(ArrayList<GetPlayerNeedHelpForTutorResponse> connectedTutors){
        if(connetedAdapter == null){
            ConnectListener listener = new ConnectListener() {
                @Override
                public void clickOnView(GetPlayerNeedHelpForTutorResponse connectedTutor) {
                    //ActHelpAStudent.this.connectedTutor = connectedTutor;
                    clickToViewHelpingStudent(connectedTutor , true);
                }
            };
            connetedAdapter = new ConnectedListAdapter(this , connectedTutors , listener);
            lstConnectedStudents.setAdapter(connetedAdapter);
        }else{
            connetedAdapter.addRecords(connectedTutors);
            connetedAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Set disconnected list adapter
     * @param disconnectedTutors
     */
    private void setDisconnectedAdapter(ArrayList<GetPlayerNeedHelpForTutorResponse> disconnectedTutors){
        if(disConnectedAdapter == null){
            DisconnectListener listener = new DisconnectListener() {

                @Override
                public void onConnect(final GetPlayerNeedHelpForTutorResponse disConnectedTutor, int position) {
                    if(CommonUtils.isInternetConnectionAvailable(ActHelpAStudent.this)){
                        final int index = position;
                        ConnectWithStudentParam param = new ConnectWithStudentParam();
                        param.setAction("connectWithStudent");
                        param.setData(getConnectWithStudentJson(disConnectedTutor));

                        new MyAsyckTask(ServerOperation
                                .createPostRequestForConnectWithStudent(param)
                                , null, ServerOperationUtil.CONNECT_WITH_STUDENT_REQUEST,
                                ActHelpAStudent.this,
                                new HttpResponseInterface() {
                                    @Override
                                    public void serverResponse(HttpResponseBase httpResponseBase,
                                                               int requestCode) {
                                        if(requestCode ==
                                                ServerOperationUtil.CONNECT_WITH_STUDENT_REQUEST){
                                            ConnectWithTutorResponse response =
                                                    (ConnectWithTutorResponse) httpResponseBase;
                                            if(response.getResult().equalsIgnoreCase("success")){
                                                int newIndex = getNewIndex(disConnectedTutor);
                                                /*connectedTutorList.add(newIndex , disConnectedTutor);
                                                disConnectedTutorList.remove(index);*/
                                                connetedAdapter.connectedTutors.add(newIndex ,disConnectedTutor);
                                                disConnectedAdapter.disconnectedTutors.remove(index);
                                                connetedAdapter.notifyDataSetChanged();
                                                disConnectedAdapter.notifyDataSetChanged();
                                                clickToViewHelpingStudent(disConnectedTutor , false);
                                            }
                                        }
                                    }
                                }, ServerOperationUtil.SIMPLE_DIALOG , true ,
                                getString(R.string.please_wait_dialog_msg))
                                .execute();
                    }else{
                        CommonUtils.showInternetDialog(ActHelpAStudent.this);
                    }
                }
            };
            disConnectedAdapter = new DisconnectedListAdapter(this , disconnectedTutors , listener);
            lstDisconnectedStudents.setAdapter(disConnectedAdapter);
        }else{
            disConnectedAdapter.addRecords(disconnectedTutors);
            disConnectedAdapter.notifyDataSetChanged();
        }
    }

    private void showIAmHereLayoutVisibility(boolean isVisible){
        if(isVisible){
            needHelpMainLayout.setVisibility(RelativeLayout.INVISIBLE);
            iAmHereLayout.setVisibility(RelativeLayout.VISIBLE);
            if(!selectedPlayer.getIsTutor().
                    equalsIgnoreCase(MathFriendzyHelper.YES + "")){
                iAmHereLayout.setVisibility(RelativeLayout.INVISIBLE);
            }
        }else{
            needHelpMainLayout.setVisibility(RelativeLayout.VISIBLE);
            iAmHereLayout.setVisibility(RelativeLayout.INVISIBLE);
        }
    }

    /*private void showIAmHereLayoutVisibility(boolean isVisible , boolean isActiveInput){
        if(isVisible){
            needHelpMainLayout.setVisibility(RelativeLayout.INVISIBLE);
            iAmHereLayout.setVisibility(RelativeLayout.VISIBLE);
            if(isActiveInput){
                iAmHereLayout.setVisibility(RelativeLayout.INVISIBLE);
            }
        }else{
            needHelpMainLayout.setVisibility(RelativeLayout.VISIBLE);
            iAmHereLayout.setVisibility(RelativeLayout.INVISIBLE);
        }
    }*/

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
        if(requestCode == ServerOperationUtil.GET_PLAYER_NEED_HELP_FOR_TUTOR_REQUEST){
            GetPlayerNeedHelpForTutorResponse response = (GetPlayerNeedHelpForTutorResponse) httpResponseBase;
            if(response.getResponse().equalsIgnoreCase("success")){
                try {
                    if (this.isActiveInputForIAmHelping
                            (response.getConnectedTutorList())) {
                        txtTimeAmHere.setText(MathFriendzyHelper
                                .getTimeSpentText(COUNT_DOWN_TIME));
                        this.setActiveInactiveStatusOfIamHareButton(false);
                        this.removeIamHareHandlerCallback();
                        this.stopTimerwhenClickOnConnectedList();
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }

                if(this.param.isFirstTime()){
                    connetedAdapter = null;
                    disConnectedAdapter = null;
                    if(response.getConnectedTutorList().size() < MAX_OFFSET_LIMIT){
                        setButtonVisibility(btnShowMoreHelpingStudent , false);
                    }

                    if(response.getNotConnectedTutorList().size() < MAX_OFFSET_LIMIT){
                        setButtonVisibility(btnShowMoreNeedHelpStudent , false);
                    }

                    if(response.getNotConnectedTutorList().size() == 0){
                        showIAmHereLayoutVisibility(true);
                        if(!this.isActiveInputForIAmHelping
                                (response.getConnectedTutorList())) {
                            checkForIAmHareActiveInactiveStatus();
                        }
                        checkForStartTimer();
                    }else{
                        showIAmHereLayoutVisibility(false);
                        removeIamHareHandlerCallback();
                        this.stopTimerwhenClickOnConnectedList();
                        timer.stop();
                        timer.releaseTimer();
                    }
                }else{
                    if(this.param.getIsConnected() == 1) {//for connected list
                        if(response.getConnectedTutorList().size() < MAX_OFFSET_LIMIT){
                            setButtonVisibility(btnShowMoreHelpingStudent , false);
                        }
                    }else{
                        if(response.getNotConnectedTutorList().size() < MAX_OFFSET_LIMIT){
                            setButtonVisibility(btnShowMoreNeedHelpStudent , false);
                        }
                    }
                }
                this.setConnectedAdapter(response.getConnectedTutorList());
                this.setDisconnectedAdapter(response.getNotConnectedTutorList());
                if((response.getConnectedTutorList().size()
                        + response.getNotConnectedTutorList().size()) == 0){
                    showMessageWhenNoRequestFound();
                }
                this.createGlobalRoomObjectOrGetUserStatus();
            }
        }else if(requestCode == ServerOperationUtil.UPDATE_CHAT_REQUEST_WITH_DIALOG_REQUEST){

        }else if(requestCode == ServerOperationUtil.GET_SERVICE_TIME_FOR_TUTOR_REQUEST) {
            LoadServiceTimeResponse response = (LoadServiceTimeResponse) httpResponseBase;
            if(response.getResult().equalsIgnoreCase("success")){
                this.serviceTimeFromServer(response);
            }
        }
    }

    private void showMessageWhenNoRequestFound(){
        if(loginUser.getIsParent().equals(TEACHER)){
            MathFriendzyHelper.showWarningDialog(this, lblNoStudentRequireHelp);
        }else{
            if(selectedPlayer.getIsTutor().equalsIgnoreCase(MathFriendzyHelper.YES + "")) {
                MathFriendzyHelper.showWarningDialog(this, lblNoStudentRequireHelp);
            }else{
                MathFriendzyHelper.showWarningDialog(this, studentNotATutorAlertTitle);
            }
        }
    }

    private void setButtonVisibility(Button btnButton , boolean isVisible){
        if(isVisible){
            btnButton.setVisibility(Button.VISIBLE);
        }else{
            btnButton.setVisibility(Button.GONE);
        }
    }


    /**
     * From Date
     */
    private void clickOnFromLayout(){
        final Calendar calender = MathFriendzyHelper.getCurrentCalender();
        MathFriendzyHelper.showDatePickerDialogPrevDateSelected(this, calender.get(Calendar.YEAR),
                calender.get(Calendar.MONTH), calender.get(Calendar.DAY_OF_MONTH),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);

                        //if (newDate.after(calender)) {
                        setDateText(txtFromDate , year, monthOfYear, dayOfMonth , true);
                        //}
                    }
                }, calender);
    }

    /**
     * To Date
     */
    private void clickOnToLayout(){
        final Calendar calender = MathFriendzyHelper.getCurrentCalender();
        MathFriendzyHelper.showDatePickerDialogPrevDateSelected(this, calender.get(Calendar.YEAR),
                calender.get(Calendar.MONTH), calender.get(Calendar.DAY_OF_MONTH),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        //if (newDate.after(calender)) {
                        setDateText(txtToDate , year, monthOfYear, dayOfMonth , false);
                        //}
                    }
                }, calender);
    }

    /**
     * Set date text
     * @param txtView
     * @param year
     * @param month
     * @param dayOfMonth
     * @param isStartDate
     */
    private void setDateText(TextView txtView , int year , int month , int dayOfMonth ,
                             boolean isStartDate){

        String tempStartDate = startDate;
        String tempEndDate   = endDate;

        month ++;
        String formateString = MathFriendzyHelper.getFourDigitFormatNumber(year) + "-" +
                MathFriendzyHelper.getDoubleDigitFormatNumber(month) + "-" +
                MathFriendzyHelper.getDoubleDigitFormatNumber(dayOfMonth);
        String formatedDate = MathFriendzyHelper.formatDataInGivenFormat(formateString ,
                SERVICE_TIME_MY_FORMATE, SERVICE_TIME_CONVERTED_FORMATE);
        if(isStartDate){
            tempStartDate = formatedDate;
        }else{
            tempEndDate = formatedDate;
        }

        Date fromDate = MathFriendzyHelper.getValidateDate(tempStartDate , SERVICE_TIME_CONVERTED_FORMATE);
        Date toDate   = MathFriendzyHelper.getValidateDate(tempEndDate , SERVICE_TIME_CONVERTED_FORMATE);
        if(fromDate.compareTo(toDate) <= 0){
            startDate = tempStartDate;
            endDate   = tempEndDate;
            txtView.setText(formatedDate);
            this.loadServiceTime();
        }else{
            MathFriendzyHelper.showWarningDialog(this , lblStartDateCantLater);
        }
    }

    /**
     * Set service time from server
     * @param response
     */
    private void serviceTimeFromServer(LoadServiceTimeResponse response){
        startDate = response.getStartDate();
        endDate   = response.getEndData();
        txtFromDate.setText(response.getStartDate());
        txtToDate.setText(response.getEndData());
        txtTime.setText(this.getServiceTimeString(response));
        //set average rating start
        fiveStartRatingView.setRatingStart(response.getRating());
    }

    private String getServiceTimeString(LoadServiceTimeResponse response){
        long totalTime = Long.parseLong(response.getTutoringTime()) +
                Long.parseLong(response.getIdleTime());
        this.totalServiceTime = totalTime;
        return MathFriendzyHelper.getTutorTimeSpentText(totalTime);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.imgInfo :
                DialogGenerator dialogGenerator = new DialogGenerator(this);
                dialogGenerator.helpStudentIconDefinationDialog();
                break;
            case R.id.btnShowMoreHelpingStudent:
                if(selectedPlayer != null)
                    this.getPlayersNeedHelpTutor(false , 1);//1 for connected student
                break;
            case R.id.btnShowMoreNeedHelpStudent:
                if(selectedPlayer != null)
                    this.getPlayersNeedHelpTutor(false , 0);//0 for disconneted student
                break;
            case R.id.fromLayout:
                this.clickOnFromLayout();
                break;
            case R.id.toLayout:
                this.clickOnToLayout();
                break;
            case R.id.btnAmHere:
                if(selectedPlayer != null) {
                    if (isIamHareActive) {
                        this.clickOnIAmHere();
                    }
                }
                break;

        }
    }

    @Override
    public void showMessage(QBChatMessage chatMessage) {
        MathFriendzyHelper.dismissDialog();
        try {
            if (chat != null) {
                chat.release();
            }
        } catch (XMPPException e) {
            e.printStackTrace();
        }
        QBCreateSession.getInstance(this).logoutFromChat();
        this.goForTutorScreen(connectedTutor);
    }

    @Override
    public void joinSuccessFull(QBEntityCallback callback) {

    }

    @Override
    public void onError(List list) {

    }

    @Override
    public void stillJoiningGroupChat() {

    }

    @Override
    public void joinUnsuccessFull() {

    }

    @Override
    public void showProcessImage(QBChatMessage message) {

    }

    @Override
    public void onlineUsers(ArrayList<Integer> dialogOccupents, Collection<Integer> onlineGroupUsers) {

    }

    class ViewHolder{
        private TextView txtName;
        private TextView txtGrade;
        private TextView txtLastModified;
        private TextView txtTimeSpent;
        private TextView txtOnline;
        private TextView txtView;
        private TextView txtDisconnect;
        private TextView txtConnect;
    }

    class ViewContainer{
        private View view;
        private ViewHolder viewHolder;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode ==  RESULT_OK){
            switch(requestCode){
                case STAR_ACT_TUTOR_SEESION:
                    if(data.getBooleanExtra("isNeedToRefereshList" , false)){
                        connetedAdapter = null;
                        disConnectedAdapter = null;
                        this.getPlayersNeedHelpTutor(true , 0);
                    }else {
                        this.connectedTutor.setTimeSpent(this.connectedTutor.getTimeSpent()
                                + data.getIntExtra("timeSpent", 0));
                        if (connetedAdapter != null)
                            connetedAdapter.notifyDataSetChanged();
                    }
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    /**
     * String load service time
     */
    private void loadServiceTime() {
        if(CommonUtils.isInternetConnectionAvailable(this)){
            LoadServiceTimeParam param = new LoadServiceTimeParam();
            param.setAction("getServiceTimeForTutor");
            param.setUserId(selectedPlayer.getParentUserId());
            param.setPlayerId(selectedPlayer.getPlayerid());
            param.setStartDate(startDate);
            param.setEndData(endDate);
            param.setTimeZone(MathFriendzyHelper.getTimeForTutorServiceTime(this));
            new MyAsyckTask(ServerOperation.createPostRequestForLoadServiceTime(param)
                    , null, ServerOperationUtil.GET_SERVICE_TIME_FOR_TUTOR_REQUEST, this,
                    this, ServerOperationUtil.SIMPLE_DIALOG , true ,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        }else{
            CommonUtils.showInternetDialog(this);
        }
    }

    /**
     * Start count down timer
     */
    private void clickOnIAmHere(){
        if(timer.isTimerRunning()){
            this.addServiceTimeForTutor(timer.getProgress() , true);
            timer.stop();
            timer.setProgress(0);
        }else {
            timer.setProgress(0);
        }
        timer.start();
    }

    @Override
    public void onProgress(final int progress) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                txtTimeAmHere.setText(MathFriendzyHelper.getTimeSpentText(
                        COUNT_DOWN_TIME - progress));
                if((COUNT_DOWN_TIME - progress) == 0) {
                    timer.stop();
                }
            }
        });
    }

    /**
     * Check for already running timer
     */
    private void checkForStartTimer() {
        if(timer.isTimerRunning()){
            timer.stop();
            timer.initializeCallback(this);
            timer.start();
        }
    }

    private void addServiceTimeForTutor(int seconds , boolean isShowDialog){
        this.totalServiceTime = this.totalServiceTime + seconds;
        txtTime.setText(MathFriendzyHelper.getTutorTimeSpentText(this.totalServiceTime));

        if(CommonUtils.isInternetConnectionAvailable(this)){
            AddServiceTimeForTutorParam param = new AddServiceTimeForTutorParam();
            param.setAction("addServiceTimeForTutor");
            param.setUserId(selectedPlayer.getParentUserId());
            param.setPlayerId(selectedPlayer.getPlayerid());
            param.setTimeInSeconds(seconds);
            new MyAsyckTask(ServerOperation.createPostRequestForAddServieTimeForTutor(param)
                    , null, ServerOperationUtil.ADD_SERVICE_TIME_FOR_TUTOR_REQUEST, this,
                    this, ServerOperationUtil.SIMPLE_DIALOG , true ,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        }
    }

    private void initializeCurrentObj(){
        currentObj = this;
    }

    public static ActHelpAStudent getCurrentObj(){
        return currentObj;
    }



    private void checkForIAmHareActiveInactiveStatus(){
        this.setActiveInactiveStatusOfIamHareButton();
        this.removeIamHareHandlerCallback();
        checkActiveInactiveIamHareStatus.postDelayed(new Runnable() {
            @Override
            public void run() {
                setActiveInactiveStatusOfIamHareButton();
                checkActiveInactiveIamHareStatus.postDelayed(this ,
                        ACTIVE_INACTIVE_TIMER_DIFF_FOR_I_AM_HARE);
            }
        }, ACTIVE_INACTIVE_TIMER_DIFF_FOR_I_AM_HARE);
    }

    private void removeIamHareHandlerCallback(){
        if(checkActiveInactiveIamHareStatus != null)
            checkActiveInactiveIamHareStatus.removeCallbacksAndMessages(null);
    }

    private void setActiveInactiveStatusOfIamHareButton(){
        if(MathFriendzyHelper.isTimeInBetween
                (MathFriendzyHelper.getCurrentDateInGiveGformateDate(timeFormat)
                        , startTime , endTime)){
            isIamHareActive = true;
            btnAmHere.setBackgroundResource(R.drawable.gjs_play_again);
        }else{
            isIamHareActive = false;
            btnAmHere.setBackgroundResource(R.drawable.light_gray);
        }
    }

    private void setActiveInactiveStatusOfIamHareButton(boolean isActive){
        if(isActive){
            isIamHareActive = true;
            btnAmHere.setBackgroundResource(R.drawable.gjs_play_again);
        }else{
            isIamHareActive = false;
            btnAmHere.setBackgroundResource(R.drawable.light_gray);
        }
    }
    @Override
    public void onBackPressed() {
        this.removeIamHareHandlerCallback();
        this.removeActiveInActiveHandler();
        super.onBackPressed();
    }

    private void stopTimerwhenClickOnConnectedList() {
        if(timer != null && timer.isTimerRunning()){
            this.addServiceTimeForTutor(timer.getProgress() , false);
            timer.stop();
            timer.setProgress(0);
            txtTimeAmHere.setText(MathFriendzyHelper
                    .getTimeSpentText(COUNT_DOWN_TIME));
        }
    }

    private boolean isActiveInputForIAmHelping
            (ArrayList<GetPlayerNeedHelpForTutorResponse> connectedTutors){
        if(connectedTutors != null
                && connectedTutors.size() > 0){
            for(int i = 0 ; i < connectedTutors.size() ; i ++){
                if(connectedTutors.get(i).getOpponentInput() == YES){
                    return true;
                }
            }
        }
        return false;
    }

    private String getUniqueStudentIdJsonStringForTheList(ArrayList<String> studentList){
        try{
            JSONArray jsonArray = new JSONArray();
            for(int i = 0 ; i < studentList.size() ; i ++ ) {
                String key = studentList.get(i);
                String userId = key.split(USER_PLAYER_IS_SEPARATOR)[0];
                String playerId = key.split(USER_PLAYER_IS_SEPARATOR)[1];
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("userId" , userId);
                jsonObject.put("playerId" , playerId);
                jsonArray.put(jsonObject);
            }
            return jsonArray.toString();
        }catch (Exception e){
            e.printStackTrace();
            return "[]";
        }
    }

    private ArrayList<String> getUniqueStudentDetail(){
        ArrayList<String> uniqueList = new ArrayList<String>();
        if(connetedAdapter != null && connetedAdapter.getConnectedStudentList().size() > 0){
            ArrayList<GetPlayerNeedHelpForTutorResponse> connectedStudentList =
                    connetedAdapter.getConnectedStudentList();
            for(int i = 0 ; i < connectedStudentList.size() ; i ++ ) {
                GetPlayerNeedHelpForTutorResponse object = connectedStudentList.get(i);
                String uniqueKey = object.getUserId() + USER_PLAYER_IS_SEPARATOR + object.getPlayerId();
                if(!uniqueList.contains(uniqueKey)){
                    uniqueList.add(uniqueKey);
                }
            }
        }

        if(disConnectedAdapter != null && disConnectedAdapter.getDisconnectedStudentList().size() > 0){
            ArrayList<GetPlayerNeedHelpForTutorResponse> disconnectedStudentList =
                    disConnectedAdapter.getDisconnectedStudentList();
            for(int i = 0 ; i < disconnectedStudentList.size() ; i ++ ) {
                GetPlayerNeedHelpForTutorResponse object = disconnectedStudentList.get(i);
                String uniqueKey = object.getUserId() + USER_PLAYER_IS_SEPARATOR + object.getPlayerId();
                if(!uniqueList.contains(uniqueKey)){
                    uniqueList.add(uniqueKey);
                }
            }
        }
        return uniqueList;
    }

    private void initializeHandlerAndStart(){
        getStudentActiveInActiveStatus = new Handler();
        runnableThread = new Runnable() {
            @Override
            public void run() {
                getInactiveStatusOfPlayersForTutor();
                getStudentActiveInActiveStatus.postDelayed(runnableThread,
                        GET_ACTIVE_INACTIVE_STATUS_REFRESH_TIME);
            }
        };
        getStudentActiveInActiveStatus.postDelayed(runnableThread , GET_ACTIVE_INACTIVE_STATUS_REFRESH_TIME);
    }

    private void removeActiveInActiveHandler(){
        if(getStudentActiveInActiveStatus != null){
            getStudentActiveInActiveStatus.removeCallbacksAndMessages(null);
        }
    }


    private void getInactiveStatusOfPlayersForTutor(){
        this.createGlobalRoomObjectOrGetUserStatus();
        /*GetActiveInActiveStatusForTutorParam param = new GetActiveInActiveStatusForTutorParam();
        param.setAction("getInactiveStatusOfPlayersForTutor");
        param.setData(this.getUniqueStudentIdJsonStringForTheList(this.getUniqueStudentDetail()));
        if(CommonUtils.isInternetConnectionAvailable(this)){
            new MyAsyckTask(ServerOperation.createPostRequestForGetInactiveStatusOfPlayersForTutor(param)
                    , null, ServerOperationUtil.GET_INACTIVE_STATUS_OF_PLAYER_FOR_TUTOR, this,
                    new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                            GetActiveInActiveStatusOfStudentForTutorResponse response =
                                    (GetActiveInActiveStatusOfStudentForTutorResponse) httpResponseBase;
                            if(response != null) {
                                if (connetedAdapter != null) {
                                    connetedAdapter.updatedStatusAndNotify(response.getList());
                                }

                                if (disConnectedAdapter != null) {
                                    disConnectedAdapter.updatedStatusAndNotify(response.getList());
                                }
                            }
                            getStudentActiveInActiveStatus.postDelayed(runnableThread,
                                    GET_ACTIVE_INACTIVE_STATUS_REFRESH_TIME);
                        }
                    }, ServerOperationUtil.SIMPLE_DIALOG , false ,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        }*/
    }

    //Online Status via socket
    private void createGlobalRoomObjectOrGetUserStatus() {
        if(MyGlobalWebSocket.getInstance() != null
                && MyGlobalWebSocket.getInstance().isSocketConnected()){
            MyGlobalWebSocket.getInstance().initializeCallback(this);
            MyGlobalWebSocket.getInstance().getOnlineStatus(MathFriendzyHelper.SERVER_PREFIX ,
                    this.getUniqueStudentIdJsonForGlobalRoom(this.getUniqueStudentDetail()));
        }else{
            UserPlayerDto selectedPlayerData = MathFriendzyHelper.getSelectedPlayer(this);
            if(selectedPlayerData != null){
                MyGlobalWebSocket.getInstance(this , this , 0 , selectedPlayerData.getPlayerid());
            }
        }
    }

    private JSONArray getUniqueStudentIdJsonForGlobalRoom(ArrayList<String> studentList){
        JSONArray jsonArray = new JSONArray();
        try{
            for(int i = 0 ; i < studentList.size() ; i ++ ) {
                String key = studentList.get(i);
                String playerId = key.split(USER_PLAYER_IS_SEPARATOR)[1];
                jsonArray.put(playerId);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return jsonArray;
    }

    @Override
    public void onConnected() {
        MyGlobalWebSocket.getInstance().getOnlineStatus(MathFriendzyHelper.SERVER_PREFIX ,
                this.getUniqueStudentIdJsonForGlobalRoom(this.getUniqueStudentDetail()));
    }

    @Override
    public void onConnectionFail() {

    }

    @Override
    public void onMessageSent(boolean isSuccess) {

    }

    @Override
    public void onMessageRecieved(String message) {
        try {
            CommonUtils.printLog(TAG, "Message Recieve from socket " + message);
            GetActiveInActiveStatusOfStudentForTutorResponse response =
                    MathFriendzyHelper.getActiveInActiveResponse(message);
            if (response != null) {
                if (connetedAdapter != null) {
                    connetedAdapter.updatedStatusAndNotify(response.getList());
                }

                if (disConnectedAdapter != null) {
                    disConnectedAdapter.updatedStatusAndNotify(response.getList());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
