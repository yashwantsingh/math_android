package com.mathfriendzy.controller.helpastudent;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.helpastudent.GetPlayerNeedHelpForTutorResponse;
import com.mathfriendzy.serveroperation.serveroperationparamresponseclasses.GetActiveInActiveStatusOfStudentForTutorResponse;

import java.util.ArrayList;

/**
 * Created by root on 24/4/15.
 */
public class DisconnectedListAdapter extends BaseAdapter{

    public ArrayList<GetPlayerNeedHelpForTutorResponse> disconnectedTutors  = null;
    private LayoutInflater mInflater = null;
    private ViewHolder viewHolder = null;
    private final int YES = 1;
    private final int NO = 0;
    private String anonymousText = "Anonymous";
    private final String SERVER_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private final String CONVERTED_DATE_FORMAT = "MMM. dd, yyyy hh:mm aa";
    private DisconnectListener listener = null;

    public DisconnectedListAdapter(Context context , ArrayList<GetPlayerNeedHelpForTutorResponse> disconnectedTutors ,
                                   DisconnectListener listener){
        this.listener = listener;
        this.disconnectedTutors = disconnectedTutors;
        mInflater = LayoutInflater.from(context);
    }

    public void addRecords
            (ArrayList<GetPlayerNeedHelpForTutorResponse> connectedTutors){
        this.disconnectedTutors.addAll(connectedTutors);
    }

    @Override
    public int getCount() {
        return disconnectedTutors.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if(view == null){
            viewHolder = new ViewHolder();
            view = mInflater.inflate(R.layout.need_help_student_layout , null);
            viewHolder.txtName = (TextView) view.findViewById(R.id.txtNeedHelperName);
            viewHolder.txtGrade = (TextView) view.findViewById(R.id.txtNeedHelperGrade);
            viewHolder.txtLastModified = (TextView) view.findViewById(R.id.txtNeedHelperLastModified);
            viewHolder.txtOnline = (TextView) view.findViewById(R.id.txtNeedHelperOnlineOrNot);
            viewHolder.txtConnect = (TextView) view.findViewById(R.id.txtNeedHelperConnect);
            view.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) view.getTag();
        }
        this.setWidgetesValue(viewHolder ,disconnectedTutors.get(position) , position);
        return view;
    }

    /**
     * Set widgets value
     * @param viewHolder
     * @param disConnectedTutor
     */
    private void setWidgetesValue(ViewHolder viewHolder ,final GetPlayerNeedHelpForTutorResponse disConnectedTutor
            , final int position){
        if(disConnectedTutor.getIsAnonymous() == YES){
            viewHolder.txtName.setText(anonymousText);
        }else {
            viewHolder.txtName.setText(disConnectedTutor.getPlayerFName() + " "
                    + disConnectedTutor.getPlayerLName());
        }

        viewHolder.txtGrade.setText(disConnectedTutor.getGrade() + "");
        viewHolder.txtLastModified.setText(MathFriendzyHelper.formatDataInGivenFormatWithDefaultTimeZone
                (disConnectedTutor.getReqDate(), SERVER_DATE_FORMAT, CONVERTED_DATE_FORMAT));

        this.setOnLineStatus(viewHolder ,disConnectedTutor);
        viewHolder.txtConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onConnect(disConnectedTutor , position);
            }
        });
    }

    private void setOnLineStatus(ViewHolder viewHolder ,
                                 GetPlayerNeedHelpForTutorResponse disConnectedTutor){
        if(disConnectedTutor.getStudentOnline() == MathFriendzyHelper.YES){
            viewHolder.txtOnline.setBackgroundResource(R.drawable.online_icon);
        }else {
            if (disConnectedTutor.getInActive() == YES) {
                viewHolder.txtOnline.setBackgroundResource(R.drawable.red_circle_icon);
            } else {
                viewHolder.txtOnline.setBackgroundResource(R.drawable.offline_icon);
            }
        }
    }

    class ViewHolder{
        private TextView txtName;
        private TextView txtGrade;
        private TextView txtLastModified;
        private TextView txtTimeSpent;
        private TextView txtOnline;
        private TextView txtView;
        private TextView txtDisconnect;
        private TextView txtConnect;
    }

    public ArrayList<GetPlayerNeedHelpForTutorResponse> getDisconnectedStudentList(){
        return this.disconnectedTutors;
    }

    public void updatedStatusAndNotify(ArrayList<GetActiveInActiveStatusOfStudentForTutorResponse>
                                               updatedActiveInActiveList){
        if(this.disconnectedTutors != null && this.disconnectedTutors.size() > 0) {
            for (int i = 0; i < this.disconnectedTutors.size() ; i ++ ){
                GetPlayerNeedHelpForTutorResponse student =  this.getActiveInActiveStatus(disconnectedTutors.get(i)
                        , updatedActiveInActiveList);
                if(student != null){
                    disconnectedTutors.get(i).setInActive(student.getInActive());
                    disconnectedTutors.get(i).setStudentOnline(student.getStudentOnline());
                }else{
                    disconnectedTutors.get(i).setInActive(1);
                    disconnectedTutors.get(i).setStudentOnline(MathFriendzyHelper.NO);
                }
            }
            this.notifyDataSetChanged();
        }
    }

    private GetPlayerNeedHelpForTutorResponse getActiveInActiveStatus(GetPlayerNeedHelpForTutorResponse student ,
                                                                      ArrayList<GetActiveInActiveStatusOfStudentForTutorResponse>
                                                                              updatedActiveInActiveList){
        for(int i = 0 ; i < updatedActiveInActiveList.size() ; i ++ ){
            GetActiveInActiveStatusOfStudentForTutorResponse status = updatedActiveInActiveList.get(i);
            if(/*status.getUserId().equalsIgnoreCase(student.getUserId()) &&*/
                    status.getPlayerId().equalsIgnoreCase(student.getPlayerId())){
                student.setInActive(0);
                /*if(status.getOnlineRequestId() == student.getRequestId()){
                    student.setStudentOnline(MathFriendzyHelper.YES);
                }else{
                    student.setStudentOnline(MathFriendzyHelper.NO);
                }*/
                return student;
            }
        }
        return null;
    }
}
