package com.mathfriendzy.controller.helpastudent;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.helpastudent.GetPlayerNeedHelpForTutorResponse;
import com.mathfriendzy.serveroperation.serveroperationparamresponseclasses.GetActiveInActiveStatusOfStudentForTutorResponse;

import java.util.ArrayList;

/**
 * Created by root on 24/4/15.
 */
public class ConnectedListAdapter extends BaseAdapter{

    public ArrayList<GetPlayerNeedHelpForTutorResponse> connectedTutors  = null;
    private LayoutInflater mInflater = null;
    private ViewHolder viewHolder = null;
    private final int YES = 1;
    private final int NO = 0;
    private String anonymousText = "Anonymous";
    private final String SERVER_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private final String CONVERTED_DATE_FORMAT = "MMM. dd, yyyy hh:mm aa";
    private ConnectListener listener = null;

    public ConnectedListAdapter(Context context ,
                                ArrayList<GetPlayerNeedHelpForTutorResponse> connectedTutors
            , ConnectListener listener){
        this.listener = listener;
        this.connectedTutors = connectedTutors;
        mInflater = LayoutInflater.from(context);
    }

    public void addRecords
            (ArrayList<GetPlayerNeedHelpForTutorResponse> connectedTutors){
        this.connectedTutors.addAll(connectedTutors);
    }

    @Override
    public int getCount() {
        return connectedTutors.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if(view == null){
            viewHolder = new ViewHolder();
            view = mInflater.inflate(R.layout.helping_student_layout , null);
            viewHolder.txtName = (TextView) view.findViewById(R.id.txtHelperName);
            viewHolder.txtGrade = (TextView) view.findViewById(R.id.txtHelperGrade);
            viewHolder.txtLastModified = (TextView) view.findViewById(R.id.txtHelperLastModified);
            viewHolder.txtTimeSpent = (TextView) view.findViewById(R.id.txtHelperTimeSpent);
            viewHolder.txtOnline = (TextView) view.findViewById(R.id.txtHelperOnlineOrNot);
            viewHolder.txtView = (TextView) view.findViewById(R.id.txtHelperView);
            viewHolder.txtDisconnect = (TextView) view.findViewById(R.id.txtHelperDisconnect);
            view.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) view.getTag();
        }
        this.setWidgetesValue(viewHolder ,connectedTutors.get(position));
        return view;
    }

    /**
     * Set widgets value
     * @param viewHolder
     * @param connectedTutor
     */
    private void setWidgetesValue(ViewHolder viewHolder ,final GetPlayerNeedHelpForTutorResponse connectedTutor){
        if(connectedTutor.getIsAnonymous() == YES){
            viewHolder.txtName.setText(anonymousText);
        }else {
            viewHolder.txtName.setText(connectedTutor.getPlayerFName() + " "
                    + connectedTutor.getPlayerLName());
        }
        viewHolder.txtGrade.setText(connectedTutor.getGrade() + "");
        viewHolder.txtLastModified.setText(MathFriendzyHelper.formatDataInGivenFormatWithDefaultTimeZone
                (connectedTutor.getReqDate(), SERVER_DATE_FORMAT, CONVERTED_DATE_FORMAT));
        viewHolder.txtTimeSpent.setText(MathFriendzyHelper.getTimeSpentText(connectedTutor.getTimeSpent()));

        viewHolder.txtView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.clickOnView(connectedTutor);
            }
        });
        this.setOnLineStatus(viewHolder ,connectedTutor);
        this.setOpponentInputStatus(viewHolder ,connectedTutor);
        viewHolder.txtDisconnect.setText(this.getStartText(connectedTutor.getStars()));
    }

    private void setOnLineStatus(ViewHolder viewHolder ,
                                 GetPlayerNeedHelpForTutorResponse connectedTutor){
        if(connectedTutor.getStudentOnline() == MathFriendzyHelper.YES){
            viewHolder.txtOnline.setBackgroundResource(R.drawable.online_icon);
        }else {
            if (connectedTutor.getInActive() == YES) {
                viewHolder.txtOnline.setBackgroundResource(R.drawable.red_circle_icon);
            } else {
                viewHolder.txtOnline.setBackgroundResource(R.drawable.offline_icon);
            }
        }
    }

    /**
     * Return the star text
     * @param numberOfStar
     * @return
     */
    private String getStartText(int numberOfStar){
        try{
            if(numberOfStar == 0)
                return "";
            return numberOfStar + "";
        }catch(Exception e){
            e.printStackTrace();
            return "";
        }
    }
    /**
     * Set opponent status
     * @param viewHolder
     * @param connectedTutor
     */
    private void setOpponentInputStatus(ViewHolder viewHolder ,
                                        GetPlayerNeedHelpForTutorResponse connectedTutor){
        if(connectedTutor.getOpponentInput() == YES){
            viewHolder.txtView.setBackgroundResource(R.drawable.small_pencil_with_green_background);
        }else{
            viewHolder.txtView.setBackgroundResource(R.drawable.small_pencil);
        }
    }

    class ViewHolder{
        private TextView txtName;
        private TextView txtGrade;
        private TextView txtLastModified;
        private TextView txtTimeSpent;
        private TextView txtOnline;
        private TextView txtView;
        private TextView txtDisconnect;
        private TextView txtConnect;
    }


    public ArrayList<GetPlayerNeedHelpForTutorResponse> getConnectedStudentList(){
        return this.connectedTutors;
    }

    public void updatedStatusAndNotify(ArrayList<GetActiveInActiveStatusOfStudentForTutorResponse>
                                               updatedActiveInActiveList){
        if(this.connectedTutors != null && this.connectedTutors.size() > 0) {
            for (int i = 0; i < this.connectedTutors.size() ; i ++ ){
                GetPlayerNeedHelpForTutorResponse student =  this.getActiveInActiveStatus(connectedTutors.get(i)
                        , updatedActiveInActiveList);
                if(student != null){
                    connectedTutors.get(i).setInActive(student.getInActive());
                    connectedTutors.get(i).setStudentOnline(student.getStudentOnline());
                }else{
                    connectedTutors.get(i).setInActive(1);
                    connectedTutors.get(i).setStudentOnline(MathFriendzyHelper.NO);
                }
            }
            this.notifyDataSetChanged();
        }
    }

    private GetPlayerNeedHelpForTutorResponse getActiveInActiveStatus(GetPlayerNeedHelpForTutorResponse student ,
                                        ArrayList<GetActiveInActiveStatusOfStudentForTutorResponse>
                                                updatedActiveInActiveList){
        for(int i = 0 ; i < updatedActiveInActiveList.size() ; i ++ ){
            GetActiveInActiveStatusOfStudentForTutorResponse status = updatedActiveInActiveList.get(i);
            if(/*status.getUserId().equalsIgnoreCase(student.getUserId()) &&*/
                    status.getPlayerId().equalsIgnoreCase(student.getPlayerId())){
                student.setInActive(0);
                if(status.getOnlineRequestId() == student.getRequestId()){
                    student.setStudentOnline(MathFriendzyHelper.YES);
                }else{
                    student.setStudentOnline(MathFriendzyHelper.NO);
                }

                return student;
            }
        }
        return null;
    }
}
