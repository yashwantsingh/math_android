package com.mathfriendzy.controller.multifriendzy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.controller.multifriendzy.contacts.ContactNameActivity;
import com.mathfriendzy.controller.multifriendzy.facebookfriends.FacebookFriendActivity;
import com.mathfriendzy.controller.multifriendzy.findbyuser.SearchByUserActivity;
import com.mathfriendzy.controller.top100.Top100Activity;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;

import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;
import static com.mathfriendzy.utils.ITextIds.ALERT_LOGIN_REGISTER;
import static com.mathfriendzy.utils.ITextIds.EMAIL_SUBJECT;
import static com.mathfriendzy.utils.ITextIds.LBL_EMAIL;
import static com.mathfriendzy.utils.ITextIds.LBL_FB;
import static com.mathfriendzy.utils.ITextIds.LBL_FIND_FRIEND;
import static com.mathfriendzy.utils.ITextIds.LBL_FIND_INVITE;
import static com.mathfriendzy.utils.ITextIds.LBL_FRIEND;
import static com.mathfriendzy.utils.ITextIds.LBL_INVITE;
import static com.mathfriendzy.utils.ITextIds.LBL_MESSAGE;
import static com.mathfriendzy.utils.ITextIds.LBL_TOP100;
import static com.mathfriendzy.utils.ITextIds.LBL_USER;
import static com.mathfriendzy.utils.ITextIds.LIKE_URL;
import static com.mathfriendzy.utils.ITextIds.MF_HOMESCREEN;
import static com.mathfriendzy.utils.ITextIds.MSG_CLICK;
import static com.mathfriendzy.utils.ITextIds.MSG_INVITE_1;
import static com.mathfriendzy.utils.ITextIds.MSG_INVITE_2;
import static com.mathfriendzy.utils.ITextIds.MSG_INVITE_2_FOR_EMIAL;
import static com.mathfriendzy.utils.ITextIds.MSG_INVITE_3;
import static com.mathfriendzy.utils.ITextIds.MSG_INVITE_4;
import static com.mathfriendzy.utils.ITextIds.MSG_INVITE_5;
import static com.mathfriendzy.utils.ITextIds.MSG_LIKE;


public class StartNewFriendzyActivity extends ActBaseClass implements OnClickListener
{
	private TextView txtTobBar						= null;
	private TextView txtFindfriend					= null;
	private TextView txtInviteFriends				= null;
	private TextView txtFindOrInvite				= null;
	private TextView txtFb							= null;
	private TextView txtUser						= null;
	private TextView txtMessage						= null;
	private TextView txtMail						= null;
	
	private RelativeLayout findFbLayout				= null;
	private RelativeLayout findUserLayout			= null;
	private RelativeLayout inviteViaMsgLayout		= null;
	private RelativeLayout inviteViaEmailLayout		= null;
	
	private Button btnTop100						= null;
	
	private String subject							= null;
	private String inviteMsg						= null;
	private String msg;
	private String likeMsg;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start_new_friendzy);
		
		getWidgetId();
		setTextOnWidget();
		
	}//END onCreate method
	
	

	private void getWidgetId() 
	{
		txtFb				= (TextView) findViewById(R.id.txtFb);
		txtFindfriend		= (TextView) findViewById(R.id.txtFindFriends);
		txtInviteFriends	= (TextView) findViewById(R.id.txtInviteFriends);
		txtFindOrInvite		= (TextView) findViewById(R.id.txtFindOrInvite);
		txtMail				= (TextView) findViewById(R.id.txtMail);
		txtMessage			= (TextView) findViewById(R.id.txtMessage);
		txtTobBar			= (TextView) findViewById(R.id.labelTop);
		txtUser				= (TextView) findViewById(R.id.txtUser);
		
		btnTop100			= (Button) findViewById(R.id.btnTop100);
		
		findFbLayout			= (RelativeLayout) findViewById(R.id.findFbLayout);
		findUserLayout			= (RelativeLayout) findViewById(R.id.findUserLayout);
		inviteViaEmailLayout	= (RelativeLayout) findViewById(R.id.inviteViaEmailLayout);
		inviteViaMsgLayout		= (RelativeLayout) findViewById(R.id.inviteViaMsgLayout);
		
		findFbLayout.setOnClickListener(this);
		findUserLayout.setOnClickListener(this);
		inviteViaEmailLayout.setOnClickListener(this);
		inviteViaMsgLayout.setOnClickListener(this);
		btnTop100.setOnClickListener(this);
		
	}//END getWidgetId method

	
	
	private void setTextOnWidget()
	{
		Translation translate = new Translation(this);
		translate.openConnection();
		String text;
		text = translate.getTranselationTextByTextIdentifier(MF_HOMESCREEN);
		txtTobBar.setText(text);
		subject = translate.getTranselationTextByTextIdentifier(EMAIL_SUBJECT);
		
		text = translate.getTranselationTextByTextIdentifier(LBL_TOP100);
		btnTop100.setText(text);
		
		text = translate.getTranselationTextByTextIdentifier(LBL_EMAIL);
		txtMail.setText(text);
		
		text = translate.getTranselationTextByTextIdentifier(LBL_MESSAGE);
		txtMessage.setText(text);
		
		text = translate.getTranselationTextByTextIdentifier(LBL_FB);
		txtFb.setText(text);
		
		text = translate.getTranselationTextByTextIdentifier(LBL_FIND_FRIEND);
		txtFindfriend.setText(text);
		
		text = translate.getTranselationTextByTextIdentifier(LBL_FIND_INVITE);
		txtFindOrInvite.setText(text);
		
		text = translate.getTranselationTextByTextIdentifier(LBL_INVITE) + " " 
						+ translate.getTranselationTextByTextIdentifier(LBL_FRIEND);
		txtInviteFriends.setText(text);
		
		text = translate.getTranselationTextByTextIdentifier(LBL_USER);
		txtUser.setText(text);
		
		
		
		translate.closeConnection();
		
	}//END setTextOnWIdget method



	@Override
	public void onClick(View v)
	{
		switch(v.getId())
		{
		case R.id.btnTop100:
			SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
			if(!sheredPreference.getBoolean(IS_LOGIN, false))
			{
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				DialogGenerator dg = new DialogGenerator(this);
				dg.generateRegisterOrLogInDialog(transeletion.getTranselationTextByTextIdentifier(ALERT_LOGIN_REGISTER));
				transeletion.closeConnection();	
			}
			else
			{
				startActivity(new Intent(this,Top100Activity.class));
			}
			break;
			
		case R.id.inviteViaEmailLayout:
			Intent emailIntent = new Intent(Intent.ACTION_SEND);
			
			inviteMsg = getInviteMessage();

            emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
			emailIntent.putExtra(Intent.EXTRA_TEXT, msg);
			emailIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(inviteMsg));
			emailIntent.setType("text/html");
			startActivity(Intent.createChooser(emailIntent, "Send email using"));
			break;
			
		case R.id.findUserLayout:
				startActivity(new Intent(this, SearchByUserActivity.class));
			break;
		case R.id.inviteViaMsgLayout:
			Intent sms_intent = new Intent(this, ContactNameActivity.class);
			sms_intent.putExtra("msg", getInviteSms());
			startActivity(sms_intent);
			break;
			
		case R.id.findFbLayout:
			startActivity(new Intent(this, FacebookFriendActivity.class));			
			break;
		}//End switch case		
		
	}//END onClick Method



	private String getInviteMessage() 
	{
		SharedPreferences sharedPrefPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		String playerName = sharedPrefPlayerInfo.getString("playerName", "");
		
		Translation translate = new Translation(this);
		translate.openConnection();
		String text = null;		
		msg = translate.getTranselationTextByTextIdentifier(MSG_INVITE_1);
		
		text =  translate.getTranselationTextByTextIdentifier(MSG_INVITE_1)+
					" <a href=\""+ MathFriendzyHelper.getAppRateUrl(this)+"\">"+translate.getTranselationTextByTextIdentifier(MSG_CLICK)+"</a>"+" "
					+ translate.getTranselationTextByTextIdentifier(MSG_INVITE_2_FOR_EMIAL)
					+ " '" + translate.getTranselationTextByTextIdentifier(MF_HOMESCREEN)+ "'."
					+ " " + translate.getTranselationTextByTextIdentifier(MSG_INVITE_3)
					+ " '" + playerName + "'"
					+ " " + translate.getTranselationTextByTextIdentifier(MSG_INVITE_4)
					+"<p><p><a href=\""+LIKE_URL+"\">" + translate.getTranselationTextByTextIdentifier(MSG_LIKE)+"</a>"
					+ " "+ translate.getTranselationTextByTextIdentifier(MSG_INVITE_5);
		
		translate.closeConnection();
		return text;
	}

	private String getInviteSms() 
	{
		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		String playerUserName = sharedPreffPlayerInfo.getString("userName", "");
		Translation translate = new Translation(this);
		translate.openConnection();
		String text = null;		
		msg = translate.getTranselationTextByTextIdentifier(MSG_INVITE_1);
		
		text =  translate.getTranselationTextByTextIdentifier(MSG_INVITE_1)+" "
					+ translate.getTranselationTextByTextIdentifier("btnTitleGo") + " "
					+ translate.getTranselationTextByTextIdentifier(MSG_INVITE_2)
					+ " '" + translate.getTranselationTextByTextIdentifier(MF_HOMESCREEN)+ "'"
					+ " " + translate.getTranselationTextByTextIdentifier(MSG_INVITE_3)
					+ " '" + playerUserName + "'."
					+ " " + translate.getTranselationTextByTextIdentifier(MSG_INVITE_4);
		
		translate.closeConnection();
		return text;
	}
	
	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}
}
