package com.mathfriendzy.controller.multifriendzy.schoolcurriculum;

import java.util.ArrayList;

import com.mathfriendzy.model.schoolcurriculum.singlefriendzy.SigleFriendzyQuestionFromServer;

public class WordQuestionFromServerObj {

		int grade;
		ArrayList<SigleFriendzyQuestionFromServer> questionList;
		
		public int getGrade() {
			return grade;
		}
		public void setGrade(int grade) {
			this.grade = grade;
		}
		public ArrayList<SigleFriendzyQuestionFromServer> getQuestionList() {
			return questionList;
		}
		public void setQuestionList(
				ArrayList<SigleFriendzyQuestionFromServer> questionList) {
			this.questionList = questionList;
		}
		
}
