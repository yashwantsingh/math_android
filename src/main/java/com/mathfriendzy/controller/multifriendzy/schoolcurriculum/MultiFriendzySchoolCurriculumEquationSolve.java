package com.mathfriendzy.controller.multifriendzy.schoolcurriculum;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.learningcenter.schoolcurriculum.OptionSelectDataObj;
import com.mathfriendzy.controller.learningcenter.schoolcurriculum.SchoolCurriculumEquationSolveBase;
import com.mathfriendzy.controller.multifriendzy.MultiFriendzyMain;
import com.mathfriendzy.controller.multifriendzy.MultiFriendzyRound;
import com.mathfriendzy.controller.multifriendzy.MultiFriendzyWinnerScreen;
import com.mathfriendzy.controller.multifriendzy.findbyuser.SelectedPlayerActivity;
import com.mathfriendzy.dawnloadimagesfromserver.DawnloadImagesFromserver;
import com.mathfriendzy.gcm.ProcessNotification;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.MathResultTransferObj;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.multifriendzy.CreateMultiFriendzyDto;
import com.mathfriendzy.model.multifriendzy.MathFriendzysRoundDTO;
import com.mathfriendzy.model.multifriendzy.MultiFriendzyServerOperation;
import com.mathfriendzy.model.multifriendzy.schoolcurriculum.MultiFriendzySchoolCurriculumServerOperation;
import com.mathfriendzy.model.player.base.Player;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.LearnignCenterSchoolCurriculumServerOperation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.MathEquationTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.MathScoreForSchoolCurriculumTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.QuestionLoadedTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SchoolCurriculumLearnignCenterimpl;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.UpdatedInfoTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.WordProblemQuestionTransferObj;
import com.mathfriendzy.model.schoolcurriculum.singlefriendzy.SigleFriendzyQuestionFromServer;
import com.mathfriendzy.model.schoolcurriculum.singlefriendzy.SingleFriendzySchoolCurriculumImpl;
import com.mathfriendzy.model.schoolcurriculum.singlefriendzy.SingleFriendzyServerOperationForWordProblem;
import com.mathfriendzy.model.spanishchanges.SpanishChangesImpl;
import com.mathfriendzy.notification.GetAndroidDevicePidForUser;
import com.mathfriendzy.notification.SendNotificationTransferObj;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ICommonUtils;
import com.mathfriendzy.utils.PlaySound;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Random;

import static com.mathfriendzy.utils.ICommonUtils.DEVICE_ID;
import static com.mathfriendzy.utils.ICommonUtils.DEVICE_ID_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.MULTI_FRIENDZY_SCHOOL_EQUATION_SOLVE;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;


/**
 * This class open when user play with multifriendzy
 * @author Yashwant Singh
 *
 */
public class MultiFriendzySchoolCurriculumEquationSolve extends SchoolCurriculumEquationSolveBase {

	//for second player
	private TextView txtPlayerName1     	= null;
	private TextView txtPoints1         	= null;
	private ImageView imgFlag1          	= null;
	private LinearLayout progressBarLayout1 = null;
	private ImageView getReady              = null;
	private RelativeLayout progressLayout1  = null;

	//for second player
	private Player  opponentData	= null;
	private String friendzyId 	    = "-1";
	private boolean isNotificationSend = false;

	private final String TAG = this.getClass().getSimpleName();

	// for questions
	private final int MAX_QUESTION = 10;//maximum question to be displayed
	ArrayList<SigleFriendzyQuestionFromServer> questionListFinal = 
			new ArrayList<SigleFriendzyQuestionFromServer>();
	//for get ready timer
	//get ready timer
	private CountDownTimer getReadyTimer = null;
	private long START_TIME_FOR_GET_READY = 7 * 1000;

	private final int SLEEP_TIME = 1000;
	private final int SLEEP_TIME_FOR_WRONG_ANS_THIRD_ATTEMP = 500;
	private String answerByUser 	= "";

	//for sound
	//private PlaySound playsound  = null;

	private int numberOfCoins      = 0;
	private int isCorrectAnsByUser = 0;

	//after completion
	ArrayList<MathFriendzysRoundDTO> updatedRoundList = null;
	private int turn       		= 0;
	private String winnerId		= "-1";
	private int roundScore 		= 0;
	private int shouldSaveEquation = 0;
	private String problems 	= "";
	private String playFriendzyDate = null;
	private final String THEIR_FRENDZY_TEXT = "their friendzy";
	private final String YOUR_FRIENDZY_TEXT = "your friendzy";
	private final String HISTORY_TEXT       = "";
	private int grade = 1;
	private final String MESSAGE_TEXT_FOR_NOTIFICATION = " has sent a Math Friendzy to ";

	//for second player points
	private int secondPlayerpoints = 0;
	private boolean isStopSecondPlayerProgress = false;

	private boolean isEquationFromserver    = false;
	private CountDownTimer finishRoundTimer = null;

	//for setting second player progress
	private long sleepTimeForSecondPlayer = 0l;
	private Handler handlerSetResultForSecondPlayer = null;
	private Date satrtTimeForSettingProgress;//Contain the starting time for each progress for second player
	//which is used in download images from server at the time of playing
	private Runnable runnableThreadForSeconPlayer  = null;

	//for sound
	private boolean isFromOnCreate = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_multi_friendzy_school_curriculum_equation_solve);

		if(MULTI_FRIENDZY_SCHOOL_EQUATION_SOLVE)
			Log.e(TAG, "inside onCreate()");

		//for sound
		isFromOnCreate = true;

		//for tts
		tts = new TextToSpeech(this, this);

		random  = new Random();
		playSound = new PlaySound(this);
		playDataList = new ArrayList<MathEquationTransferObj>();

		isTab = getResources().getBoolean(R.bool.isTablet);

		this.setWidgetsReferences();
		this.setRemainingWidgetsReferencesFromBase();
		this.setTextFromTranslation();
		//this.getIntentValue();
		this.setListenerOnWidgets();
		this.setPlayerDetail();

		this.showTimeAtStarting();

		if(ProcessNotification.isFromNotification)
		{
			ProcessNotification.isFromNotification = false;

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder
			.setTitle("Math Friendzy")
			.setMessage(ProcessNotification.sentNotificationData.getShareMessage())
			.setIcon(R.drawable.ic_launcher)
			.setPositiveButton("Ok", new DialogInterface.OnClickListener() 
			{
				public void onClick(DialogInterface dialog, int which) 
				{	
					getIntentValue();
				}
			}).show();
		}
		else
		{
			this.getIntentValue();
		}

		if(MULTI_FRIENDZY_SCHOOL_EQUATION_SOLVE)
			Log.e(TAG, "outside onCreate()");
	}

	/**
	 * This method show timer 
	 * Changes when required mail on 15 oct
	 */
	private void showTimeAtStarting(){
		int mins = (int) (startTimeForTimerAtFirstTime / 60);
		int secs = (int) (startTimeForTimerAtFirstTime % 60);
		txtTimer.setText(mins + ":"+ String.format("%02d", secs));
	}

	@SuppressWarnings("deprecation")
	private void setPlayerDetail() 
	{
		if(MULTI_FRIENDZY_SCHOOL_EQUATION_SOLVE)
			Log.e(TAG, "inside setPlayerDetail()");

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);

		String fullName = sharedPreffPlayerInfo.getString("playerName", "");
		String playerName = fullName.substring(0,(fullName.indexOf(" ") + 2));
		txtPlayerName.setText(playerName + ".");

		Country country = new Country();
		String coutryIso = country.getCountryIsoByCountryName(sharedPreffPlayerInfo.getString("countryName", ""), this);

		try 
		{
			if(!(coutryIso.equals("-")))
				imgFlag.setBackgroundDrawable(Drawable.createFromStream(getAssets().open(getResources().
						getString(R.string.countryImageFolder) +"/" 
						+ coutryIso + ".png"), null));
		} 
		catch (IOException e) 
		{
			Log.e(TAG, "Inside set player detail Error : No Image Found  " + e.toString());
		}

		//set second user detail
		if(MultiFriendzyRound.multiFriendzyServerDto != null)
		{			
			txtPlayerName1.setText(MultiFriendzyRound.oppenentPlayerName + ".");
			try 
			{
				if(!(coutryIso.equals("-")))
					imgFlag1.setBackgroundDrawable(Drawable.createFromStream(getAssets().open(getResources().
							getString(R.string.countryImageFolder) +"/" 
							+ MultiFriendzyRound.multiFriendzyServerDto.getCountryCode() + ".png"), null));
			} 
			catch (IOException e) 
			{	
				Log.e(TAG, "Inside set player detail Error : No Image Found " + e.toString());
			}
		}

		if(MULTI_FRIENDZY_SCHOOL_EQUATION_SOLVE)
			Log.e(TAG, "outside setPlayerDetail()");
	}

	/**
	 * This method change the image on each seconds for get ready
	 */
	private void getReadyTimer()
	{
        imgTimerImage.setVisibility(ImageView.GONE);
		getReadyTimer = new CountDownTimer(START_TIME_FOR_GET_READY,1) 
		{
			@Override
			public void onTick(long millisUntilFinished){
				int value = (int) ((millisUntilFinished/1000) % 60) ;
				setBacKGroundForGetReadyImage(value);
			}

			@Override
			public void onFinish(){
                imgTimerImage.setVisibility(ImageView.VISIBLE);
				questionAnswerLayout.setVisibility(RelativeLayout.VISIBLE);
				getReady.setVisibility(ImageView.INVISIBLE);

				playSound.playSoundForSingleFriendzyEquationSolve
				(MultiFriendzySchoolCurriculumEquationSolve.this);

				if(isEquationFromserver)
					setSecondPlayerProgressData();

				if(questionListFinal.size() > 0){
					getQuestionFromDatabase();//get question from database by question id
					// question id is either from server or from local database
				}else{
					notSufficientQuestionDailog();
				}
			}
		};
		getReadyTimer.start();
	}

	/**
	 * This method get question with its answer from database
	 * using quation id
	 * @param
	 */
	private void getQuestionFromDatabase(){

		if(numberOfQuestion < MAX_QUESTION){
			int questionId = questionListFinal.get(numberOfQuestion).getQuestionId();
			//int questionId = 5679;
			/*SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
			schoolImpl.openConnection();
			//get question from database into questionObj object
			questionObj  = schoolImpl.getQuestionByQuestionId(questionId);
			schoolImpl.closeConnection();*/

			//changes for Spanish
			if(languageCode == SPANISH){
				SpanishChangesImpl schoolImpl = new SpanishChangesImpl(this);
				schoolImpl.openConn();
				//get question from database into questionObj object
				questionObj  = schoolImpl.getQuestionByQuestionId(questionId);
				schoolImpl.closeConn();
			}else if(languageCode == ENGLISH){
				SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
				schoolImpl.openConnection();
				//get question from database into questionObj object
				questionObj  = schoolImpl.getQuestionByQuestionId(questionId);
				schoolImpl.closeConnection();
			}
			this.setIsSpanishBooleanValue(); // changes after
			//end changes

			//insert played question into WordQuestionsPlayed table
			SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
			String userId   = sharedPreffPlayerInfo.getString("userId", "0");
			String playerId = sharedPreffPlayerInfo.getString("playerId", "0");
			SingleFriendzySchoolCurriculumImpl dataObj = new SingleFriendzySchoolCurriculumImpl(this);
			dataObj.openConnection();
			dataObj.insertIntoWordQuestionsPlayed(userId, playerId, questionId);
			dataObj.closeConnection();

			this.showQuestion();
		}else{

			this.callWhenQuestionFinish();
		}
	}

	/**
	 * This method call when questions are finished 
	 * or second player gives the answer of all questions
	 */
	private void callWhenQuestionFinish(){

		isStopSecondPlayerProgress = true;
		/*if(finishRoundTimer != null)
			finishRoundTimer.cancel();*/

		playSound.playSoundForSchoolSurriculumForResultScreen(this);

		if(playSound != null)
			playSound.stopPlayer();

		this.callAfterCompletion();
	}

	//changes for update player points in local when Internet is not connected
	//updated on 26 11 2014 
	private void savePlayerDataWhenNoInternetConnected(){
		try{		
			SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
			String userId;
			String playerId;
			if(sharedPreffPlayerInfo.getString("userId", "").equals("0"))
				userId = "0";
			else
				userId = sharedPreffPlayerInfo.getString("userId", "");

			if(sharedPreffPlayerInfo.getString("playerId", "").equals("0"))
				playerId = "0";
			else
				playerId = sharedPreffPlayerInfo.getString("playerId", "");

			MathResultTransferObj mathResultObj = new MathResultTransferObj();
			mathResultObj.setUserId(userId);
			mathResultObj.setPlayerId(playerId);
			mathResultObj.setTotalScore(points);
			mathResultObj.setCoins((int) (points * ICommonUtils.COINS_PER_POINT));
			mathResultObj.setLevel(MathFriendzyHelper.getPlayerCompleteLavel(playerId , this));
			MathFriendzyHelper.insertPlayerPlayedDataIntoLocalEarnedScore(mathResultObj, this);
		}catch(Exception e){
			e.printStackTrace();
		}
	}//end updation on 26 11 2014

	/**
	 * This method is call when user complete to play equations
	 */
	private void callAfterCompletion(){

		updatedRoundList = new ArrayList<MathFriendzysRoundDTO>();
		String opponentPlayerId = null;
		String opponentUserId   = null;

		if(opponentData != null)//data from searched user
		{
			opponentPlayerId = opponentData.getPlayerId() + "";
			opponentUserId   = opponentData.getParentUserId() + "";
		}
		else //  data from click on your turn
		{
			opponentPlayerId = MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getPlayerId();
			opponentUserId   = MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getParentUserId();
		}

		//if mathfriendzy start by my player
		if(MultiFriendzyMain.isNewFriendzyStart)
		{
			MathFriendzysRoundDTO roundDto = new MathFriendzysRoundDTO();
			roundDto.setPlayerScore(points + "");
			roundDto.setOppScore("");
			roundDto.setRoundNumber("1");

			updatedRoundList.add(roundDto);

			turn = 1 ; // for their turn
		}
		else
		{
			MathFriendzysRoundDTO roundDto = MultiFriendzyRound.roundList.get
					(MultiFriendzyRound.roundList.size() - 1);//get last round record

			if(roundDto.getPlayerScore().equals(""))
			{
				roundDto.setPlayerScore(points + "");

				for(int i = 0 ; i < MultiFriendzyRound.roundList.size() - 1 ; i ++ )
				{
					updatedRoundList.add(MultiFriendzyRound.roundList.get(i));
				}

				updatedRoundList.add(roundDto);
			}
			else
			{
				MathFriendzysRoundDTO roundDto1 = new MathFriendzysRoundDTO();
				roundDto1.setPlayerScore(points + "");
				roundDto1.setOppScore("");
				roundDto1.setRoundNumber((MultiFriendzyRound.roundList.size() + 1) + "");

				for(int i = 0 ; i < MultiFriendzyRound.roundList.size(); i ++ )
				{
					updatedRoundList.add(MultiFriendzyRound.roundList.get(i));
				}

				updatedRoundList.add(roundDto1);
			}
		}

		if(updatedRoundList.size() > 1)
		{
			MathFriendzysRoundDTO roundDto = updatedRoundList.get(updatedRoundList.size() - 1);

			if((!roundDto.getPlayerScore().equals("")) && (!roundDto.getOppScore().equals("")))
			{
				turn = 0 ;// for your turn
			} 
			else if((!roundDto.getPlayerScore().equals("")) && (roundDto.getOppScore().equals("")))
			{
				turn = 1 ;// for their turn
			}
		}

		if(MultiFriendzyMain.isClickOnYourTurn && isEquationFromserver)
		{
			//Log.e(TAG, "inside click on your turn for notification");

			if(updatedRoundList.size() == 3)
			{
				int playerTotalPoints   = 0;
				int opponentTotalPoints = 0;

				for ( int i = 0 ; i < updatedRoundList.size() ; i ++ )
				{
					if(!updatedRoundList.get(i).getPlayerScore().equals(""))
						playerTotalPoints = playerTotalPoints + Integer.parseInt(
								updatedRoundList.get(i).getPlayerScore());

					if(!updatedRoundList.get(i).getOppScore().equals(""))
						opponentTotalPoints = opponentTotalPoints + Integer.parseInt
						(updatedRoundList.get(i).getOppScore());
				}

				if(playerTotalPoints > opponentTotalPoints)
				{
					SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
					String playerId = sharedPreffPlayerInfo.getString("playerId", "");

					winnerId = playerId;
					isNotificationSend = true;
					turn = 2;//for history
				}	
				else if(playerTotalPoints == opponentTotalPoints)
				{
					winnerId = opponentPlayerId;
					isNotificationSend = true;
					turn = 2;//for history
				}
				else
				{
					points = points - 10;
					winnerId = opponentPlayerId;
					isNotificationSend = true;
					turn = 2;//for history
				}
			}

			if(playDataList.size() > 0)
			{
				roundScore = points;
				shouldSaveEquation = 0;
				problems = getEquationSolveXml(playDataList);
			}
			else
			{
				roundScore = 0;
				shouldSaveEquation = 0;
			}
		}
		else
		{
			if(isEquationFromserver == false)
			{
				roundScore = points;
				problems = getEquationSolveXml(playDataList);
				shouldSaveEquation = 1;
			}
		}


		//playFriendzyDate = CommonUtils.formateDateIn24Hours(new Date());

		//set time zone changes
		playFriendzyDate = CommonUtils.formateDateIn24Hours(CommonUtils.setTimeZoneWithDate(new Date()));
		//end time zone changes

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		String userId = sharedPreffPlayerInfo.getString("userId", "");
		String playerId = sharedPreffPlayerInfo.getString("playerId", "");

		updatePlayerTotalPointsTable(playerId , userId);

		LearningCenterimpl learningCenterimpl = new LearningCenterimpl(this);
		learningCenterimpl.openConn();
		PlayerTotalPointsObj playerPoints =  learningCenterimpl.getDataFromPlayerTotalPoints(playerId);
		learningCenterimpl.closeConn();

		CreateMultiFriendzyDto createMultiFrinedzyDto = new CreateMultiFriendzyDto();

		createMultiFrinedzyDto.setProblems(problems);
		createMultiFrinedzyDto.setFriendzyId(friendzyId);
		createMultiFrinedzyDto.setPlayerUserId(userId);
		createMultiFrinedzyDto.setPlayerId(playerId);

		createMultiFrinedzyDto.setOpponentPlayerId(opponentPlayerId);
		createMultiFrinedzyDto.setOpponentUserId(opponentUserId);

		createMultiFrinedzyDto.setRoundScore(roundScore);
		createMultiFrinedzyDto.setPoints(playerPoints.getTotalPoints());
		createMultiFrinedzyDto.setCoins(playerPoints.getCoins());
		createMultiFrinedzyDto.setDate(playFriendzyDate);

		boolean isAddParameter = false;//this is for add below parameter to the url or not for hit api

		if(!winnerId.equals("-1"))
		{
			isAddParameter = true;
			createMultiFrinedzyDto.setModifying(1);
			createMultiFrinedzyDto.setIsCompleted(1);
			createMultiFrinedzyDto.setWinner(winnerId);
		}
		else if(!friendzyId.equals("-1"))
		{
			isAddParameter = true;
			createMultiFrinedzyDto.setModifying(1);
			createMultiFrinedzyDto.setIsCompleted(0);
			createMultiFrinedzyDto.setWinner("-1");
		}

		createMultiFrinedzyDto.setShouldSaveEquations(shouldSaveEquation);

		MultiFriendzyMain.isNewFriendzyStart = false;

		savePlayDataOnServer(createMultiFrinedzyDto , isAddParameter);
		this.savePlayerDataWhenNoInternetConnected();
	}

	/**
	 * This method save play multifriendzy data on server
	 */
	private void savePlayDataOnServer(CreateMultiFriendzyDto createMultiFrinedzyDto , boolean isAddParameter)
	{
		if(CommonUtils.isInternetConnectionAvailable(this))
		{
			new CreateMultiFriendzy(createMultiFrinedzyDto , isAddParameter).execute(null,null,null);
		}
		else
		{
			DialogGenerator dg = new DialogGenerator(this);
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
			transeletion.closeConnection();
		}
	}

	/**
	 * Update player total points table
	 * @param playerId
	 */
	private void updatePlayerTotalPointsTable(String playerId , String userId)
	{
		numberOfCoins = (int) (points * ICommonUtils.COINS_PER_POINT);

		LearningCenterimpl learningCenterimpl = new LearningCenterimpl(this);
		learningCenterimpl.openConn();
		PlayerTotalPointsObj playerPoints =  learningCenterimpl.getDataFromPlayerTotalPoints(playerId);

		playerPoints.setTotalPoints(playerPoints.getTotalPoints() + points);
		playerPoints.setCoins(playerPoints.getCoins() + numberOfCoins);
		playerPoints.setCompleteLevel(playerPoints.getCompleteLevel());
		playerPoints.setUserId(userId);
		playerPoints.setPlayerId(playerId);

		learningCenterimpl.deleteFromPlayerTotalPoints(playerId);
		learningCenterimpl.insertIntoPlayerTotalPoints(playerPoints);

		learningCenterimpl.closeConn();
	}

	/**
	 * This method show question 
	 */
	private void showQuestion(){

		try{
			numberOfQuestion ++ ;

			if(numberOfQuestion > 1 && numberOfQuestion <= MAX_QUESTION){
				animation = AnimationUtils.loadAnimation(this, R.anim.splash_animation_right);
				questionAnswerLayout.setAnimation(animation);
			}

			this.setKeyBoardVisivility(questionObj.getFillIn());
			this.setLayoutForShowQuetionAndAnswer(questionObj);
		}
		catch (Exception e) {
			Log.e(TAG, "No Question Found " + e.toString());
		}
	}

	/**
	 * This method inflate the layout for show question and answer
	 * @param questionObj
	 */
	private void setLayoutForShowQuetionAndAnswer(WordProblemQuestionTransferObj questionObj){
		ArrayList<String> imageNameList        = this.getImageArrayList(questionObj);
		ArrayList<String> unAvailableImageList = this.checkImageAvalability(imageNameList);

		if(unAvailableImageList.size() > 0){
			if(CommonUtils.isInternetConnectionAvailable(this)){
				//changes for timer when dawanload images from server
				if(isCountDawnTimer){
					if(timer != null){
						timer.cancel();
						timer = new MyTimer(startTimerForRoughArea, 1 * 1000);//second argument is the interval
					}
				}else{
					if(customHandler != null)
						customHandler.removeCallbacks(updateTimerThread);
				}
				//end changes

				if(isEquationFromserver)
				{
					if(handlerSetResultForSecondPlayer != null){
						handlerSetResultForSecondPlayer.removeCallbacks(runnableThreadForSeconPlayer);
						Date endTimeForSecondPlayer = new Date();
						/*Log.e(TAG, "time before " + sleepTimeForSecondPlayer);
						Log.e(TAG, "start time " + satrtTimeForSettingProgress.getTime());
						Log.e(TAG, "end time " + endTimeForSecondPlayer.getTime());
						Log.e(TAG, " diff " + (endTimeForSecondPlayer.getTime() - satrtTimeForSettingProgress.getTime()));*/

						sleepTimeForSecondPlayer = sleepTimeForSecondPlayer 
								- (endTimeForSecondPlayer.getTime() - satrtTimeForSettingProgress.getTime());
						//Log.e(TAG, "time after " + sleepTimeForSecondPlayer);
					}
				}

				new DawnloadImageForSchooCurriculumForMultiFriendzy(unAvailableImageList, this, questionObj)
				.execute(null,null,null);

			}else{
				this.setQuestionAnswerLayout(questionObj);
			}
		}
		else{
			this.setQuestionAnswerLayout(questionObj);
		}
	}

	/**
	 * This method set the Get Ready Image back ground
	 * @param value
	 */
	private void setBacKGroundForGetReadyImage(int value)
	{
		switch(value)
		{
		case 6 : 
			getReady.setBackgroundResource(R.drawable.mcg_get_ready);
			break;
		case 5 : 
			getReady.setBackgroundResource(R.drawable.mcg_5);
			break;
		case 4 : 
			getReady.setBackgroundResource(R.drawable.mcg_4);
			break;
		case 3 : 
			getReady.setBackgroundResource(R.drawable.mcg_3);
			break;
		case 2 : 
			getReady.setBackgroundResource(R.drawable.mcg_2);
			break;
		case 1 : 
			getReady.setBackgroundResource(R.drawable.mcg_1);
			break;
		case 0 : 
			getReady.setBackgroundResource(R.drawable.mcg_go);
			break;
		}
	}

	/**
	 * This method get intent value
	 */
	private void getIntentValue(){

		if(MULTI_FRIENDZY_SCHOOL_EQUATION_SOLVE)
			Log.e(TAG, "inside getIntentValue()");

		grade = this.getIntent().getIntExtra("grade", 1);

		//changes for Spanish
		selectedPlayGrade = this.getIntent().getIntExtra("grade", 1);

		if(MultiFriendzyMain.isNewFriendzyStart)
		{			
			opponentData = SelectedPlayerActivity.player;

			friendzyId = "-1";
			isNotificationSend = true;
			this.getEquationFromLocalDatabase();
			this.setVisibleOrInvisibleContent();
		}
		else
		{		
			friendzyId = MultiFriendzyRound.friendzyId;

			int index = MultiFriendzyRound.roundList.size() - 1 ;
			if(MultiFriendzyRound.roundList.get(index).getPlayerScore().length() > 0
					&& MultiFriendzyRound.roundList.get(index).getOppScore().length() > 0)
			{
				isNotificationSend = true;
				this.getEquationFromLocalDatabase();
				this.setVisibleOrInvisibleContent();
			}
			else
			{
				if(MultiFriendzyRound.roundList.get(index).getPlayerScore().length() == 0)
				{
					isNotificationSend = false;
					isEquationFromserver = true;
					this.getEquationFromServer();
				}
			}
		}

		if(MULTI_FRIENDZY_SCHOOL_EQUATION_SOLVE)
			Log.e(TAG, "outside getIntentValue()");
	}

	/**
	 * This method set the widgets references which are newly added for this screen
	 */
	private void setRemainingWidgetsReferencesFromBase() {

		if(MULTI_FRIENDZY_SCHOOL_EQUATION_SOLVE)
			Log.e(TAG, "inside setRemainingWidgetsReferencesFromBase()");

		txtPlayerName1 			= (TextView) 	findViewById(R.id.txtPlayerName1);
		txtPoints1     			= (TextView) 	findViewById(R.id.txtPoints1);
		imgFlag1       			= (ImageView) 	findViewById(R.id.imgFlag1);
		progressBarLayout1 		= (LinearLayout)findViewById(R.id.progressBarLayout1);
		getReady                = (ImageView) 	findViewById(R.id.getReady);

		//added for multifriendzy
		progressLayout1         = (RelativeLayout) findViewById(R.id.progressLayout1);

		if(MULTI_FRIENDZY_SCHOOL_EQUATION_SOLVE)
			Log.e(TAG, "outside setRemainingWidgetsReferencesFromBase()");
	}

	/**
	 * This method hide the content which is not to be display
	 */
	private void setVisibleOrInvisibleContent() {
		if(MULTI_FRIENDZY_SCHOOL_EQUATION_SOLVE)
			Log.e(TAG, "inside setVisibleOrInvisibleContent()");

		progressLayout1.setVisibility(ProgressBar.GONE);

		if(MULTI_FRIENDZY_SCHOOL_EQUATION_SOLVE)
			Log.e(TAG, "outside setVisibleOrInvisibleContent()");
	}

	/**
	 * This method get equation from local database
	 */
	private void getEquationFromLocalDatabase(){

		if(MULTI_FRIENDZY_SCHOOL_EQUATION_SOLVE)
			Log.e(TAG, "inside getEquationFromLocalDatabase()");

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		String userId   = sharedPreffPlayerInfo.getString("userId", "0");
		String playerId = sharedPreffPlayerInfo.getString("playerId", "0");
		//int grade       =  sharedPreffPlayerInfo.getInt("grade", 0);

		/*SingleFriendzySchoolCurriculumImpl schoolImpl = new SingleFriendzySchoolCurriculumImpl(this);
		schoolImpl.openConnection();
		ArrayList<Integer> questionIds = schoolImpl.getQuestionIdsFromDatabase(userId, playerId, grade);
		schoolImpl.closeConnection();*/

		//changes for Spanish
		ArrayList<Integer> questionIds = new ArrayList<Integer>();
		if(CommonUtils.getUserLanguageCode(this) == CommonUtils.ENGLISH){
			SingleFriendzySchoolCurriculumImpl schoolImpl = new SingleFriendzySchoolCurriculumImpl(this);
			schoolImpl.openConnection();
			questionIds = schoolImpl.getQuestionIdsFromDatabase(userId, playerId, grade);
			schoolImpl.closeConnection();
		}else{
			SpanishChangesImpl schoolImpl = new SpanishChangesImpl(this);
			schoolImpl.openConn();
			questionIds = schoolImpl.getQuestionIdsFromDatabase(userId, playerId, grade);
			schoolImpl.closeConn();
		}

		//create final question list
		if(questionIds.size() > 0){
			this.createFinalQuaetionListFromDatabase(questionIds);
		}else{
			this.notSufficientQuestionDailog();
		}

		if(MULTI_FRIENDZY_SCHOOL_EQUATION_SOLVE)
			Log.e(TAG, "outside getEquationFromLocalDatabase()");
	}

	/**
	 * Create final question list which is to be display for player
	 * @param questionIds
	 */
	private void createFinalQuaetionListFromDatabase(ArrayList<Integer> questionIds){
		if(questionIds.size() >= MAX_QUESTION){
			int index = 0;
			for(int i = 0 ; i < MAX_QUESTION ; i ++ ){
				index = random.nextInt(questionIds.size());
				SigleFriendzyQuestionFromServer questionObj = new SigleFriendzyQuestionFromServer();
				questionObj.setTimeTaken(0);
				questionObj.setIsCorrect(0);
				questionObj.setQuestionId(questionIds.get(index));
				questionObj.setPoints(0);
				questionObj.setPlayerId("");//for challenger
				questionObj.setUserId("");//for challenger
				questionListFinal.add(questionObj);
				questionIds.remove(index);
			}
			this.getReadyTimer();

		}else{
			SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
			String userId   = sharedPreffPlayerInfo.getString("userId", "0");
			String playerId = sharedPreffPlayerInfo.getString("playerId", "0");
			SingleFriendzySchoolCurriculumImpl dataObj = new SingleFriendzySchoolCurriculumImpl(this);
			dataObj.openConnection();
			dataObj.deleteFromWordQuestionsPlayed(userId, playerId);
			dataObj.closeConnection();
			this.getEquationFromLocalDatabase();
		}
	}

	/**
	 * This method open dialog when user does not have sufficient question to be played
	 */
	private void notSufficientQuestionDailog(){
		DialogGenerator dg = new DialogGenerator(this);
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		dg.generateWarningDialogForNoSufficientQuestionForSingleFriendzy(transeletion.
				getTranselationTextByTextIdentifier("lblSorryYouDoNotHaveQuestions") , this , false);
		transeletion.closeConnection();
	}

	/**
	 * This method get equation from server
	 */
	private void getEquationFromServer(){
		if(MULTI_FRIENDZY_SCHOOL_EQUATION_SOLVE)
			Log.e(TAG, "inside getEquationFromServer()");

		//Log.e(TAG, "inside getEquationFromServer");
		/*this.getEquationFromLocalDatabase();*/

		String userId 		= MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getParentUserId();
		String playerId 	= MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getPlayerId();
		String friendzyId 	= MultiFriendzyRound.friendzyId;
		if(CommonUtils.isInternetConnectionAvailable(this))
		{
			new GetEquationsFromServer(userId, playerId, friendzyId).execute(null,null,null);
		}


		if(MULTI_FRIENDZY_SCHOOL_EQUATION_SOLVE)
			Log.e(TAG, "outside getEquationFromServer()");
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnGoForWithoutFillIn:
			endTime = new Date();
			isClickOnGo = true;
			setClickableGoButton(false);
			this.clickOnbtnGoForWithoutFillIn();
			break;
		case R.id.btnGoForFillIn:
			endTime = new Date();
			isClickOnGo = true;
			setClickableGoButton(false);
			this.clickOnbtnGoForFillIn();
			break;
		case R.id.btn1:
			this.clickOnButton("1");
			break;
		case R.id.btn2:
			this.clickOnButton("2");
			break;
		case R.id.btn3:
			this.clickOnButton("3");
			break;
		case R.id.btn4:
			this.clickOnButton("4");
			break;
		case R.id.btn5:
			this.clickOnButton("5");
			break;
		case R.id.btn6:
			this.clickOnButton("6");
			break;
		case R.id.btn7:
			this.clickOnButton("7");
			break;
		case R.id.btn8:
			this.clickOnButton("8");
			break;
		case R.id.btn9:
			this.clickOnButton("9");
			break;
		case R.id.btn0:
			this.clickOnButton("0");
			break;	
		case R.id.btnSpace:
			this.clickOnButton(" ");
			break;	
		case R.id.btnSynchronized :
			this.clearAnsBox();
			break;
		case R.id.btnsecondryKeyBoard :
			primaryKeyboard.setVisibility(LinearLayout.GONE);
			secondryKeyboard.setVisibility(LinearLayout.VISIBLE);
            btnSecandryDelete.setVisibility(Button.VISIBLE);
			break;
		case R.id.btnNumber :
			primaryKeyboard.setVisibility(LinearLayout.VISIBLE);
			secondryKeyboard.setVisibility(LinearLayout.GONE);
            btnSecandryDelete.setVisibility(Button.GONE);
			break;
		case R.id.btnDot:
			this.clickOnButton(".");
			break;
		case R.id.btnMinus:
			this.clickOnButton("-");
			break;
		case R.id.btnComma:
			this.clickOnButton(",");
			break;
		case R.id.btnPlus:
			this.clickOnButton("+");
			break;
		case R.id.btnDevide:
			this.clickOnButton("/");
			break;
		case R.id.btnMultiply:
			this.clickOnButton("*");
			break;
		case R.id.btnCollon:
			this.clickOnButton(":");
			break;
		case R.id.btnLessThan:
			this.clickOnButton("<");
			break;
		case R.id.btnGreaterThan:
			this.clickOnButton(">");
			break;
		case R.id.btnEqual:
			this.clickOnButton("=");
			break;
		case R.id.btnRoot:
			this.clickOnButton("√");
			break;
		case R.id.btnDevider:
			this.clickOnButton("|");
			break;
		case R.id.btnOpeningBracket:
			this.clickOnButton("(");
			break;
		case R.id.btnClosingBracket:
			this.clickOnButton(")");
			break;
		case R.id.btnRoughWork:
			this.clickOnWorkArea(false);
			break;
		case R.id.btnRoughWorkWithourFillIn:
			this.clickOnWorkArea(false);
			break;
		case R.id.imgTimerImage:
			if(timer != null){
				timer.cancel();	
				timer = new MyTimer(startTimerForRoughArea, 1 * 1000);//second argument is the interval
			}
			DialogGenerator dg = new DialogGenerator(this);
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			String text = transeletion.getTranselationTextByTextIdentifier("lblThisIsTimeYourFriendTake");
			transeletion.closeConnection();
			dg.generateDialogForSchoolCurriculumTimer(text, timer);
			break;
		case R.id.btnEmailSendForWrongQuestion1:
			this.sendEmailFroWrongQuestion(questionObj.getQuestionId() , this , this);
			break;
		case R.id.btnEmailSendForWrongQuestion2:
			this.sendEmailFroWrongQuestion(questionObj.getQuestionId() , this , this);
			break;
            case R.id.btnSecandryDelete:
                this.clearAnsBox();
                break;
		}
	}

	/*@Override
	public void cancelCurrentTimerAndSetTheResumeTime() {
		if(timer != null){
			timer.cancel();
			timer = new MyTimer(startTimerForRoughArea, 1 * 1000);//second argument is the interval
		}
	}

	@Override
	public void resumeTimerFromTimeSetAtCancelTime() {
		if(timer != null)
			timer.start();
	}*/

	/**
	 * This method call when a user click on number button
	 * @param text
	 */
	private void clickOnButton(String text){
		//answerValue = answerValue + text;
		answerValue.append(text);
		this.setAnswertext(answerValue.toString());
	}

	/**
	 * This method set the string to ans box
	 * @param ansStringValue
	 */
	private void setAnswertext(String ansStringValue){
		if(isAnswerConatin == 0)
			ansBox.setText(answerValue);
		else if(isAnswerConatin == 1){
			ansBox.setText(ansPrefixValue + " " +  answerValue);
		}
		else if(isAnswerConatin == 2){
			ansBox.setText(answerValue + " " + ansPostFixValue); 
		}
		else if(isAnswerConatin == 3){
			ansBox.setText(ansPrefixValue + " " + answerValue + " " + ansPostFixValue);
		}
	}

	/**
	 * This method clear the ans box
	 */
	private void clearAnsBox() {
		if(answerValue.length() > 0){
			answerValue.deleteCharAt(answerValue.length() - 1);
			this.setAnswertext(answerValue.toString());
		}
	}

	/**
	 * This method call when click on btnGoForWithoutFillIn button
	 */
	private void clickOnbtnGoForWithoutFillIn(){

		ArrayList<Integer> selectedIntegerOptionList = new ArrayList<Integer>();

		String userAnswer = "";
		for( int i = 0 ; i < selectedOptionList.size() ; i ++ ){
			for(int j = 0 ; j < questionObj.getOptionList().size() ; j ++ ){
				if(selectedOptionList.get(i).getSelectedOptionTextValue()
						.equals(questionObj.getOptionList().get(j)))
				{
					selectedIntegerOptionList.add(j + 1);
				}
			}
		}

		Collections.sort(selectedIntegerOptionList);
		for(int i = 0 ; i < selectedIntegerOptionList.size() ; i ++ ){
			userAnswer = userAnswer + selectedIntegerOptionList.get(i) + ",";
		}

		if(userAnswer.length() > 0){
			answerByUser = userAnswer.substring(0, userAnswer.length() - 1);
		}

		if(userAnswer.equals(questionObj.getAns().replaceAll(" ", "") + ",")){
			for(int i = 0 ; i < selectedOptionList.size() ; i ++ ){
				if(isTab){
					selectedOptionList.get(i).getOpationSeletedLayout()
					.setBackgroundResource(R.drawable.right_ipad);
				}
				else{
					selectedOptionList.get(i).getOpationSeletedLayout()
					.setBackgroundResource(R.drawable.right);
				}
			}
			playSound.playSoundForSchoolSurriculumEquationSolveForRightAnswer(this);
			isCorrectAnsByUser = 1;
			setProgress(true);
			//this.setSecondPlayerProgress();
			this.setPoints();
			this.setHandlerForRightAnswer();
		}
		else{
			playSound.playSoundForSchoolSurriculumEquationSolveForWrongAnswer(this);
			isCorrectAnsByUser = 0;
			setProgress(false);
			//this.setSecondPlayerProgress();
			imgForGroundImage.setVisibility(ImageView.VISIBLE);
			this.setHandlerForWrongAnswerForWithoutFillIn();
		}
	}

	/**
	 * This method wait for 1 second after giving right answer and show the next question
	 */
	private void setHandlerForRightAnswer()
	{
		//this.setplayDataToArrayList();
        this.pauseTimer();
        disableKeyboard();

		Handler handler = new Handler();
		handler.postDelayed(new Runnable() 
		{
			@Override
			public void run() 
			{
                resumeTimer();
                enableKeyboard();
				//changes for time on 11 Mar
				setplayDataToArrayList();
				//end changes

				playSound.playSoundForSchoolSurriculumPagePeel
				(MultiFriendzySchoolCurriculumEquationSolve.this);
				setClickableGoButton(true);
				getQuestionFromDatabase();
			}
		}, SLEEP_TIME);
	}

	/**
	 * This method set the play equation data to the list
	 * for each equation which is to be save after completion of the 
	 * play all equations
	 */
	@SuppressWarnings("deprecation")
	private void setplayDataToArrayList(){

		//changes for timer on 11 Mar
		endTime = new Date();
		//end changes

		MathEquationTransferObj mathObjData = new MathEquationTransferObj();
		mathObjData.setEqautionId(questionObj.getQuestionId());
		mathObjData.setStartDate(CommonUtils.formateDate(startTime));
		mathObjData.setEndData(CommonUtils.formateDate(endTime));

		int startTimeValue = startTime.getSeconds() ;
		int endTimeVlaue   = endTime.getSeconds();

		if(endTimeVlaue < startTimeValue)
			endTimeVlaue = endTimeVlaue + 60;

		mathObjData.setTimeTakenToAnswer(endTimeVlaue - startTimeValue);
		mathObjData.setCatId(questionObj.getCatId());//changes when deepak will come
		mathObjData.setSubCatId(questionObj.getSubCatId());//changes when deepak will come

		if(isCorrectAnsByUser == 0)
			mathObjData.setPoints(0);
		else
			mathObjData.setPoints(this.getPointsForSolvingEachEquation());

		mathObjData.setIsAnswerCorrect(isCorrectAnsByUser);

		if(answerByUser.length() > 0)
			mathObjData.setUserAnswer(answerByUser);
		else
			mathObjData.setUserAnswer("1");

		mathObjData.setQuestionObj(questionObj);

		playDataList.add(mathObjData);
	}

	/**
	 * This method call when user give the wrong answer
	 */
	private void setHandlerForWrongAnswerForWithoutFillIn(){

        this.pauseTimer();
        disableKeyboard();

		Handler handler = new Handler();
		handler.postDelayed(new Runnable() 
		{
			@Override
			public void run() 
			{
				//setplayDataToArrayList();

                resumeTimer();
                enableKeyboard();
				imgForGroundImage.setVisibility(ImageView.INVISIBLE);
				selectedOptionList = new ArrayList<OptionSelectDataObj>();// for refreshing the old object 
				//from list
				for(int i = 0 ; i < optionLayoutList.size() ; i ++ ){
					optionNumberList.get(i).setText(alphabet.charAt(i) + "");//set the option text like 
					//A,B,c etc
					if(isTab)
						optionLayoutList.get(i).setBackgroundResource(getResources().getIdentifier
								("option" + (i+1) + "_ipad", "drawable",getPackageName()));
					else
						optionLayoutList.get(i).setBackgroundResource(getResources().getIdentifier
								("option" + (i + 1) + "", "drawable",getPackageName()));
				}

				ArrayList<Integer> listWithoutComma = getCommaSepratedValueFromString(questionObj.getAns());
				for(int i = 0 ; i < listWithoutComma.size() ; i ++ ){
					for(int j = 0 ; j < selectedOptionValue.size() ; j ++ ){
						if(questionObj.getOptionList().get(listWithoutComma.get(i) - 1)//index in array  
								// start from 0 to 7
								.equals(selectedOptionValue.get(j))){
							optionNumberList.get(j).setText("");						
							if(isTab)
								optionLayoutList.get(j).setBackgroundResource(R.drawable.right_ipad);
							else
								optionLayoutList.get(j).setBackgroundResource(R.drawable.right);
						}
					}
				}

                pauseTimer();
                disableKeyboard();
                Handler handler1 = new Handler();
				handler1.postDelayed(new Runnable() 
				{
					@Override
					public void run() 
					{
                        resumeTimer();
                        enableKeyboard();
						//changes for timer on 11 Mar
						setplayDataToArrayList();
						//end changes

						playSound.playSoundForSchoolSurriculumPagePeel
						(MultiFriendzySchoolCurriculumEquationSolve.this);
						setClickableGoButton(true);
						getQuestionFromDatabase();
					}
				}, SLEEP_TIME_FOR_WRONG_ANS_THIRD_ATTEMP);
			}
		}, SLEEP_TIME);
	}

	/**
	 * This method set points
	 */
	private void setPoints(){
		points = points + this.getPointsForSolvingEachEquation();
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		txtPoints.setText(transeletion.getTranselationTextByTextIdentifier("mfLblPts") + ": " + points);
		transeletion.closeConnection();
	}

	/**
	 * This method call when user btnGoForFillIn
	 */
	private void clickOnbtnGoForFillIn() {

		if(!questionObj.getOptionList().get(0).contains(" ") && answerValue.toString().contains(" ")){
			answerValue = new StringBuilder(answerValue.toString().replaceAll(" ", ""));
		}

		answerByUser = answerValue.toString();

		String userAns = answerValue.toString().replaceAll(" ", "").replace(",", "");
		String dbAns = questionObj.getOptionList().get(0).replaceAll(" ", "").replace(",", "");

		//if(!questionObj.getOptionList().get(0).contains(".")){
		if(this.getIsFloatValue(questionObj.getOptionList().get(0))){
			if(userAns.equals(dbAns)){//changes for space and ask to deepak about this
				playSound.playSoundForSchoolSurriculumEquationSolveForRightAnswer(this);
				isCorrectAnsByUser = 1;
				answerValue = new StringBuilder("");
				ansBox.setBackgroundResource(R.drawable.right_bar_ipad);
				setProgress(true);
				//this.setSecondPlayerProgress();
				this.setPoints();
				this.setHandlerForRightAnswer();
			}
			else{
				playSound.playSoundForSchoolSurriculumEquationSolveForWrongAnswer(this);
				isCorrectAnsByUser = 0;
				answerValue = new StringBuilder("");
				setProgress(false);
				//this.setSecondPlayerProgress();
				ansBox.setBackgroundResource(R.drawable.incorrect_bar_ipad);
				imgForGroundImage.setVisibility(ImageView.VISIBLE);
				this.setHandlerForWrongAnswerForFillIn();
			}
		}else{//changes for float ans
			if(this.compareFloatAns(answerByUser, questionObj.getOptionList().get(0))){
				playSound.playSoundForSchoolSurriculumEquationSolveForRightAnswer(this);
				isCorrectAnsByUser = 1;
				answerValue = new StringBuilder("");
				ansBox.setBackgroundResource(R.drawable.right_bar_ipad);
				setProgress(true);
				//this.setSecondPlayerProgress();
				this.setPoints();
				this.setHandlerForRightAnswer();
			}else{
				playSound.playSoundForSchoolSurriculumEquationSolveForWrongAnswer(this);
				isCorrectAnsByUser = 0;
				answerValue = new StringBuilder("");
				setProgress(false);
				//this.setSecondPlayerProgress();
				ansBox.setBackgroundResource(R.drawable.incorrect_bar_ipad);
				imgForGroundImage.setVisibility(ImageView.VISIBLE);
				this.setHandlerForWrongAnswerForFillIn();
			}
		}
	}

	/**
	 * This method call when user give wrong answer for fill in
	 */
	private void setHandlerForWrongAnswerForFillIn(){
        this.pauseTimer();
        disableKeyboard();
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() 
		{
			@Override
			public void run() 
			{
				//setplayDataToArrayList();
                resumeTimer();
                enableKeyboard();
				imgForGroundImage.setVisibility(ImageView.INVISIBLE);
				if(!questionObj.getLeftAns().equals("") && !questionObj.getRightAns().equals(""))
					ansBox.setText(questionObj.getLeftAns() + " " + questionObj.getRightAns());
				else if(!questionObj.getLeftAns().equals(""))
					ansBox.setText(questionObj.getLeftAns());
				else if(!questionObj.getRightAns().equals(""))
					ansBox.setText(questionObj.getRightAns());
				else
					ansBox.setText("");
				ansBox.setBackgroundResource(+ R.layout.edittext);

				ansBox.setText(questionObj.getLeftAns() + " " + questionObj.getOptionList().get(0)
						+ " " + questionObj.getRightAns());
                pauseTimer();
                disableKeyboard();
                Handler handler1 = new Handler();
				handler1.postDelayed(new Runnable() 
				{
					@Override
					public void run() 
					{
                        resumeTimer();
                        enableKeyboard();
						//changes for timer on 11 Mar
						setplayDataToArrayList();
						//end changes

						playSound.playSoundForSchoolSurriculumPagePeel
						(MultiFriendzySchoolCurriculumEquationSolve.this);
						setClickableGoButton(true);
						getQuestionFromDatabase();
					}
				}, SLEEP_TIME_FOR_WRONG_ANS_THIRD_ATTEMP);
			}
		}, SLEEP_TIME);
	}

	@Override
	public void onBackPressed() {

		isStopSecondPlayerProgress = true;//for stop the challenger player progresss

		if(playSound != null)
			playSound.stopPlayer();

		if(getReadyTimer != null)
			getReadyTimer.cancel();

		this.clickOnBackPress();

		super.onBackPressed();
	}

	/**
	 * This method call when user click on back press at the time of playing
	 * school curriculum multifriendzy
	 */
	private void clickOnBackPress(){
		if(points > 0){
			this.savePlayerDataWhenNoInternetConnected();
			this.insertIntoPlayerTotalPoints();
			this.saveScoreOnserverAndPlayDataIntoDatabase();
		}
	}

	/**
	 * This method insert the data into total player points table
	 * and also save data on server
	 * @param
	 */
	private void insertIntoPlayerTotalPoints()
	{
		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		String userId;
		String playerId;

		if(sharedPreffPlayerInfo.getString("userId", "").equals("0"))
			userId = "0";
		else
			userId = sharedPreffPlayerInfo.getString("userId", "");

		if(sharedPreffPlayerInfo.getString("playerId", "").equals("0"))
			playerId = "0";
		else
			playerId = sharedPreffPlayerInfo.getString("playerId", "");

		PlayerTotalPointsObj playerPoints = new PlayerTotalPointsObj();
		playerPoints.setUserId(userId);
		playerPoints.setPlayerId(playerId);
		playerPoints.setTotalPoints(points);

		numberOfCoins = (int) (points * ICommonUtils.COINS_PER_POINT);

		playerPoints.setCoins(numberOfCoins);
		playerPoints.setCompleteLevel(1);
		playerPoints.setPurchaseCoins(0);

		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();

		if(learningCenterObj.isPlayerTotalPointsExist(playerPoints.getPlayerId()))
		{
			int points = playerPoints.getTotalPoints();
			int coins  = playerPoints.getCoins();
			playerPoints.setTotalPoints(learningCenterObj.getDataFromPlayerTotalPoints
					(playerPoints.getPlayerId()).getTotalPoints() + points);

			playerPoints.setCoins(learningCenterObj.getDataFromPlayerTotalPoints(playerPoints.getPlayerId()).getCoins()
					+ coins);

			learningCenterObj.deleteFromPlayerTotalPoints(playerPoints.getPlayerId());
		}

		learningCenterObj.insertIntoPlayerTotalPoints(playerPoints);

		learningCenterObj.closeConn();
	}

	/**
	 * This method save total points and coins on server
	 * and play equation into local database
	 */
	private void saveScoreOnserverAndPlayDataIntoDatabase(){
		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		String userId;
		String playerId;
		if(sharedPreffPlayerInfo.getString("userId", "").equals("0"))
			userId = "0";
		else
			userId = sharedPreffPlayerInfo.getString("userId", "");

		if(sharedPreffPlayerInfo.getString("playerId", "").equals("0"))
			playerId = "0";
		else
			playerId = sharedPreffPlayerInfo.getString("playerId", "");

		MathScoreForSchoolCurriculumTransferObj mathScoreObj = new MathScoreForSchoolCurriculumTransferObj();
		mathScoreObj.setUserId(userId);
		mathScoreObj.setPlayerId(playerId);
		mathScoreObj.setProblems(this.getEquationSolveXmlForInterNetNotConnected(playDataList));

		SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
		schoolImpl.openConnection();
		schoolImpl.insertIntoWordProblemResults(mathScoreObj);
		schoolImpl.closeConnection();

		if(CommonUtils.isInternetConnectionAvailable(this)){
			PlayerTotalPointsObj playerObj = null;
			LearningCenterimpl learningCenterimpl = new LearningCenterimpl(this);
			learningCenterimpl.openConn();
			playerObj = learningCenterimpl.getDataFromPlayerTotalPoints(playerId);
			learningCenterimpl.closeConn();

			playerObj.setPlayerId(playerId);
			playerObj.setUserId(userId);
			new AddCoinAndPointsForLoginUser(playerObj , this)
			.execute(null,null,null);
		}
	}


	//for setting second player progress
	/**
	 * This method set the second player progress
	 */
	private void setSecondPlayerProgressData()
	{	
		if(questionListFinal.get(0).getTimeTaken() > 0)
		{
			setsecondPlayerDataProgressData(0, questionListFinal.get(0).getTimeTaken(),
					questionListFinal.get(0).getIsCorrect());
		}
	}

	/**
	 * This method set the second player points data
	 * @param index
	 * @param timeTakenToAnswer
	 */
	private void setsecondPlayerDataProgressData(final int index , double timeTakenToAnswer
			,final int isAnswerCorrect)
	{
		SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
		schoolImpl.openConnection();
		if(isAnswerCorrect == 1){
			timeTakenToAnswer += 1.2;
		}else{
			if(schoolImpl.getFillIn(questionListFinal.get(index).getQuestionId()) == 1){
				timeTakenToAnswer += 1.7;
			}else{
				timeTakenToAnswer += 2.7;
			}
		}
		schoolImpl.closeConnection();

		//changes for second player progress
		sleepTimeForSecondPlayer        = (long) (timeTakenToAnswer * 1000);
		satrtTimeForSettingProgress     = new Date();
		handlerSetResultForSecondPlayer = new Handler();

		runnableThreadForSeconPlayer = new Runnable() 
		{
			@Override
			public void run() 
			{	
				if(isAnswerCorrect == 1)
					setSecondPlayerProgressForMultiFriendzy(true);
				else
					setSecondPlayerProgressForMultiFriendzy(false);

				secondPlayerpoints = secondPlayerpoints + questionListFinal.get(index).getPoints();

				Translation transeletion = new Translation(MultiFriendzySchoolCurriculumEquationSolve.this);
				transeletion.openConnection();
				txtPoints1.setText(transeletion.getTranselationTextByTextIdentifier("mfLblPts") + ": " + secondPlayerpoints);
				transeletion.closeConnection();

				/*playsound.playSoundForRightForSingleFriendzyForChallenger
				(MultiFriendzySchoolCurriculumEquationSolve.this);*/

				equationIndex(index);
			}
		};

		handlerSetResultForSecondPlayer.postDelayed(runnableThreadForSeconPlayer , sleepTimeForSecondPlayer);
	}

	/**
	 * Get the data from the equation list for setting progress according to i index 
	 * @param index
	 */
	private void equationIndex(int index)
	{	
		if(!isStopSecondPlayerProgress)
		{
			if(index + 1 < questionListFinal.size())
			{
				if(questionListFinal.get(index + 1).getTimeTaken() > 0)
				{
					setsecondPlayerDataProgressData(index + 1, questionListFinal.get(index + 1).getTimeTaken()
							, questionListFinal.get(index + 1).getIsCorrect());
				}
				else
				{
					if(index < questionListFinal.size() - 2)
						equationIndex(index + 1);
				}
			}else{//changes for second player finish round
				if(startTimerForRoughArea  > 0){
					finishRoundTimer = new FinishRoundTimer(startTimerForRoughArea, 1 * 1000);
					finishRoundTimer.start();
				}else{
					this.callWhenQuestionFinish();
				}
			}
		}
	}

	/**
	 * This class show the timer for player
	 * @author Yashwant Singh
	 *
	 */
	class FinishRoundTimer extends CountDownTimer
	{
		public FinishRoundTimer(long millisInFuture, long countDownInterval){			
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onTick(long millisUntilFinished){
		}

		@Override
		public void onFinish(){
			callWhenQuestionFinish();
		}
	}

	/**
	 * This method set the progress
	 */
	private void setSecondPlayerProgressForMultiFriendzy(boolean isRightAnswer){
		ImageView imgProgressBar = new ImageView(this);
		if(isRightAnswer){
			if(isTab){
				imgProgressBar.setBackgroundResource(R.drawable.green_ipad);
			}
			else {
				imgProgressBar.setBackgroundResource(R.drawable.green);
			} 
		}
		else{
			if(isTab){
				imgProgressBar.setBackgroundResource(R.drawable.red_ipad);
			}
			else{
				imgProgressBar.setBackgroundResource(R.drawable.red);
			} 
		}
		progressBarLayout1.addView(imgProgressBar);
	}

	/**
	 * This method send notification to the devices 
	 * This method call after the data save on server aftre completion of 2 minuts
	 */
	private void sendNotification(CreateMultiFriendzyDto createMultiFrinedzyDto)
	{
		String notificationRecoiverDeviceId = "" ;//this variable contain the device ids to which notification is to be send
		String notificationIponDevices = "";

		String opponentName = null;

		if(opponentData != null)//data from searched user
		{
			opponentName = opponentData.getFirstName() + " " + opponentData.getLastName().charAt(0) + ".";
			notificationRecoiverDeviceId = opponentData.getAndroidPids();
			notificationIponDevices      = opponentData.getIponPids();
		}
		else //  data from click on your turn
		{
			String lastName = "";
			if(MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getlName().length() > 0 )
				lastName = MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getlName().charAt(0) + "";

			opponentName = MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getfName()
					+ " " + lastName + ".";

			notificationRecoiverDeviceId = MultiFriendzyRound.multiFriendzyServerDto.getAndroidPids();
			notificationIponDevices      = MultiFriendzyRound.multiFriendzyServerDto.getNotificationDevices();
		}

		SharedPreferences sharedPreff = getSharedPreferences(DEVICE_ID_PREFF, 0);
		String deviceId = sharedPreff.getString(DEVICE_ID, "");

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		Country country = new Country();
		String coutryIso = country.getCountryIsoByCountryName(sharedPreffPlayerInfo.getString("countryName", ""), this);
		String imageName = sharedPreffPlayerInfo.getString("imageName",null);
		//String playerName= sharedPreffPlayerInfo.getString("playerName", "");//changes for first character of last name
		String fullName = sharedPreffPlayerInfo.getString("playerName", "");
		String playerName = fullName.substring(0,(fullName.indexOf(" ") + 2)) + ".";

		String message   = playerName + MESSAGE_TEXT_FOR_NOTIFICATION + opponentName + "!";

		SendNotificationTransferObj sendNotificationObj = new SendNotificationTransferObj();
		sendNotificationObj.setDevicePid(notificationRecoiverDeviceId);//Receiver device ids
		sendNotificationObj.setIphoneDeviceIds(notificationIponDevices);//reciever devices
		sendNotificationObj.setMessage(message);//change
		sendNotificationObj.setUserId(createMultiFrinedzyDto.getPlayerUserId());
		sendNotificationObj.setPlayerId(createMultiFrinedzyDto.getPlayerId());
		sendNotificationObj.setCountry(coutryIso);
		sendNotificationObj.setOppUserId(createMultiFrinedzyDto.getOpponentUserId());
		sendNotificationObj.setOppPlayerId(createMultiFrinedzyDto.getOpponentPlayerId());
		sendNotificationObj.setFriendzyId(createMultiFrinedzyDto.getFriendzyId());
		sendNotificationObj.setProfileImageId(imageName);
		sendNotificationObj.setShareMessage(MultiFriendzyRound.notificationMessage);//changes
		sendNotificationObj.setSenderDeviceId(deviceId);//sender device ids (udid set after get from server)
		//changes for word problem
		sendNotificationObj.setForWord(1);

		//get android pid from server after that sent notification
		new GetAndroidDevicePidForUser(createMultiFrinedzyDto.getPlayerUserId(), deviceId , sendNotificationObj)
		.execute(null,null,null);

		// the asynckTask send notification to the devices
		//new SendNotification(sendNotificationObj).execute(null,null,null);		
	}

	/**
	 * This class create Multifriedzy on server
	 * @author Yashwant Singh
	 *
	 */
	class CreateMultiFriendzy extends AsyncTask<Void, Void, Void>
	{
		private CreateMultiFriendzyDto createMultiFrinedzyDto = null;
		private boolean isAddParameter;
		private String frendzyId = null;
		private ProgressDialog pd = null;	

		public CreateMultiFriendzy(CreateMultiFriendzyDto createMultiFrinedzyDto, boolean isAddParameter) 
		{
			this.createMultiFrinedzyDto = createMultiFrinedzyDto;
			this.isAddParameter         = isAddParameter;
		}

		@Override
		protected void onPreExecute() 
		{
			pd = CommonUtils.getProgressDialog(MultiFriendzySchoolCurriculumEquationSolve.this);
			pd.show();

			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			MultiFriendzyServerOperation multiFriendzyServerObj = new MultiFriendzyServerOperation();
			frendzyId = multiFriendzyServerObj.createMultiFriendzyForSchoolCurriculum
					(createMultiFrinedzyDto, isAddParameter , grade);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{			
			pd.cancel();

			//Log.e(TAG, "friendzy id " + frendzyId);

			if(frendzyId != null)
				createMultiFrinedzyDto.setFriendzyId(frendzyId);

			if(isNotificationSend)
				sendNotification(createMultiFrinedzyDto);//send object for getting some data from it

			MultiFriendzyRound.roundList = updatedRoundList;

			Translation transeletion = new Translation(MultiFriendzySchoolCurriculumEquationSolve.this);
			transeletion.openConnection();

			if(turn != 2)//not for history
			{
				if(turn == 1)//their turn
				{
					MultiFriendzyRound.turn = transeletion.getTranselationTextByTextIdentifier("lblTheirTurn"); 
					MultiFriendzyRound.type = THEIR_FRENDZY_TEXT;
				}
				else if(turn == 0)//your turn
				{
					MultiFriendzyRound.turn = transeletion.getTranselationTextByTextIdentifier("lblYourTurn");
					MultiFriendzyRound.type = YOUR_FRIENDZY_TEXT;
				}
				transeletion.closeConnection();

				if(frendzyId != null)
					MultiFriendzyRound.friendzyId = frendzyId;

				startActivity(new Intent(MultiFriendzySchoolCurriculumEquationSolve.this , MultiFriendzyRound.class));
			}
			else
			{	
				String opponentName 			= null;
				String opponentProfileImageId   = null;

				if(opponentData != null)//data from searched user
				{
					String lastName = "";
					if(opponentData.getLastName().length() > 0)
						lastName = opponentData.getLastName().charAt(0) + "";

					opponentName = opponentData.getFirstName() + " " + lastName + ".";
					opponentProfileImageId   = opponentData.getProfileImageName();
				}
				else //  data from click on your turn
				{
					String lastName = "";
					if(MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getlName().length() > 0)
						lastName = MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getlName().charAt(0) + "";

					opponentName = MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getfName() + " " + lastName + ".";
					opponentProfileImageId   = MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getProfileImageNameId();
				}

				MultiFriendzyWinnerScreen.oppenentPlayerName = opponentName;
				MultiFriendzyWinnerScreen.opponentImageId    = opponentProfileImageId;
				MultiFriendzyWinnerScreen.roundList          = updatedRoundList;

				int playerTotal 	= 0 ; 
				int opponentTotal 	= 0 ;

				for(int i = 0 ; i < MultiFriendzyWinnerScreen.roundList.size() ; i ++)
				{
					if(!MultiFriendzyWinnerScreen.roundList.get(i).getPlayerScore().equals(""))
					{
						playerTotal = playerTotal + Integer.parseInt(MultiFriendzyWinnerScreen.roundList.get(i).getPlayerScore());
					}

					if(!MultiFriendzyWinnerScreen.roundList.get(i).getOppScore().equals(""))
					{
						opponentTotal = opponentTotal + Integer.parseInt(MultiFriendzyWinnerScreen.roundList.get(i).getOppScore());
					}
				}

				if(playerTotal > opponentTotal)
					MultiFriendzyWinnerScreen.isWinner = true;
				else
					MultiFriendzyWinnerScreen.isWinner = false;

				startActivity(new Intent(MultiFriendzySchoolCurriculumEquationSolve.this , MultiFriendzyWinnerScreen.class));
			}

			super.onPostExecute(result);
		}
	}

	/**
	 * Get Count Dawn time
	 * @param questionListFinal
	 * @return
	 */
	private int getTimeForTimer(ArrayList<SigleFriendzyQuestionFromServer> questionListFinal){
		double time = 0;//long time = 0l;
		SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
		schoolImpl.openConnection();
		for( int i = 0 ; i  < questionListFinal.size() ; i ++ ){
			if(questionListFinal.get(i).getIsCorrect() == 1){
				time += 1.2;//time += 1;
			}else{
				if(schoolImpl.getFillIn(questionListFinal.get(i).getQuestionId()) == 1){
					time += 1.7;//time += 1;	
				}else{
					time += 2.7;//time += 2;
				}
			}
			time += questionListFinal.get(i).getTimeTaken();
		}
		schoolImpl.closeConnection();
		time += 2;//time += 1;
		return (int)time;
	}

	/**
	 * This method get equations from server
	 * which are played by challenger
	 * @author Yashwant Singh
	 *
	 */
	class GetEquationsFromServer extends AsyncTask<Void, Void, WordQuestionFromServerObj>{

		private String userId;
		private String playerId;
		private String friendzyId;
		ProgressDialog pd;

		GetEquationsFromServer(String userId , String playerId , String friendzyId){
			this.userId = userId;
			this.playerId = playerId;
			this.friendzyId = friendzyId;
		}

		@Override
		protected void onPreExecute() {
			pd = CommonUtils.getProgressDialog(MultiFriendzySchoolCurriculumEquationSolve.this);
			pd.show();
			super.onPreExecute();
		}

		@Override
		protected WordQuestionFromServerObj doInBackground(Void... params) {
			MultiFriendzySchoolCurriculumServerOperation serverObj = 
					new MultiFriendzySchoolCurriculumServerOperation();
			/*ArrayList<SigleFriendzyQuestionFromServer> questionList	= 
					serverObj.getWordQuestionForPlayer(userId, playerId, friendzyId);*/
			WordQuestionFromServerObj wordQuestion = serverObj.
					getWordQuestionForPlayer(userId, playerId, friendzyId);
			return wordQuestion;
		}

		@Override
		protected void onPostExecute(WordQuestionFromServerObj wordQuestion) {
			pd.cancel();

			if(wordQuestion != null && wordQuestion.getQuestionList().size() > 0){
				questionListFinal = wordQuestion.getQuestionList();
				startTimeForTimerAtFirstTime = getTimeForTimer(questionListFinal);
				showTimeAtStarting();//changes for timer
				loadQuestionFromServer(wordQuestion.getGrade());
				//getReadyTimer();
			}else{
				getEquationFromLocalDatabase();				
			}

			super.onPostExecute(wordQuestion);
		}
	}

	//changes for word problem
	/**
	 * This method call when user click on grade 
	 */
	private void loadQuestionFromServer(int grade){

		if(CommonUtils.getUserLanguageCode(this) == CommonUtils.ENGLISH){
			SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl = 
					new SchoolCurriculumLearnignCenterimpl(this);
			schooloCurriculumImpl.openConnection();
			boolean isQuestionLoaded = schooloCurriculumImpl.isQuestionLeaded(grade);
			schooloCurriculumImpl.closeConnection();

			if(isQuestionLoaded){
				if(CommonUtils.isInternetConnectionAvailable(this)){
					new GetWordProblemQuestionUpdatedDate(grade , false).execute(null,null,null);
				}else{
					getReadyTimer();
				}
			}
			else{
				if(CommonUtils.isInternetConnectionAvailable(this)){
					new GetWordProblemQuestion(grade ,this)
					.execute(null,null,null);
					new GetWordProblemQuestionUpdatedDate(grade , true).execute(null,null,null);
				}else{
					DialogGenerator dg = new DialogGenerator(this);
					Translation transeletion = new Translation(this);
					transeletion.openConnection();
					dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
					transeletion.closeConnection();	
				}
			}
		}else{
			this.callWhenUserLanguageIsSpanish(this, CommonUtils.SPANISH, grade);
		}
	}
	//end changes

	/**
	 * This class get word problem updated dates from server
	 * @author Yashwant Singh
	 *
	 */
	private class GetWordProblemQuestionUpdatedDate extends AsyncTask<Void, Void, ArrayList<UpdatedInfoTransferObj>>{
		private int grade;
		private ProgressDialog pd;
		private boolean isFisrtTimeCallForGrade;

		GetWordProblemQuestionUpdatedDate(int grade , boolean isFisrtTimeCallForGrade){
			this.grade = grade;
			this.isFisrtTimeCallForGrade = isFisrtTimeCallForGrade;
		}

		@Override
		protected void onPreExecute() {
			if(isFisrtTimeCallForGrade == false){
				pd = CommonUtils.getProgressDialog(MultiFriendzySchoolCurriculumEquationSolve.this);
				pd.show();
			}
			super.onPreExecute();
		}

		@Override
		protected ArrayList<UpdatedInfoTransferObj>  doInBackground(Void... params) {
			SingleFriendzyServerOperationForWordProblem serverObj = 
					new SingleFriendzyServerOperationForWordProblem();
			//changes for Spanish
			ArrayList<UpdatedInfoTransferObj>  updatedList = new ArrayList<UpdatedInfoTransferObj>();
			if(CommonUtils.getUserLanguageCode(MultiFriendzySchoolCurriculumEquationSolve.this) == 
					CommonUtils.ENGLISH)
				updatedList = serverObj.getWordProbleQuestionUpdatedData();
			else//for Spanish
				updatedList = serverObj.getWordProbleQuestionUpdatedDateForSpanish(CommonUtils.SPANISH);
			return updatedList;
		}

		@Override
		protected void onPostExecute(ArrayList<UpdatedInfoTransferObj>  updatedList) {
			if(isFisrtTimeCallForGrade == false){
				pd.cancel();
			}

			if(updatedList != null){
				SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl = 
						new SchoolCurriculumLearnignCenterimpl(MultiFriendzySchoolCurriculumEquationSolve.this);
				schooloCurriculumImpl.openConnection();
				//changes for Spanish
				SpanishChangesImpl implObj = new SpanishChangesImpl
						(MultiFriendzySchoolCurriculumEquationSolve.this);
				implObj.openConn();

				if(isFisrtTimeCallForGrade){
					//changes for Spanish
					if(CommonUtils.getUserLanguageCode(MultiFriendzySchoolCurriculumEquationSolve.this) 
							== CommonUtils.ENGLISH){
						if(!schooloCurriculumImpl.isWordProblemUpdateDetailsTableDataExist()){
							schooloCurriculumImpl.insertIntoWordProblemUpdateDetails(updatedList);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
						}else{
							String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
							schooloCurriculumImpl.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
						}
					}else{//for Spanish
						if(!implObj.isWordProblemUpdateDetailsTableDataExist()){
							implObj.insertIntoWordProblemUpdateDetails(updatedList);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
						}else{
							String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
							implObj.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
						}
					}
				}
				else{
					if(CommonUtils.getUserLanguageCode(MultiFriendzySchoolCurriculumEquationSolve.this) 
							== CommonUtils.ENGLISH){
						String updatedDateFromDatabase = schooloCurriculumImpl.getUpdatedDate(grade);
						if(CommonUtils.isUpdateDateChange(updatedDateFromDatabase , grade , updatedList))
						{
							String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
							schooloCurriculumImpl.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();

							if(CommonUtils.isInternetConnectionAvailable
									(MultiFriendzySchoolCurriculumEquationSolve.this)){
								new GetWordProblemQuestion(grade , MultiFriendzySchoolCurriculumEquationSolve.this)
								.execute(null,null,null);
							}else{
								getReadyTimer();
							}
						}
						else{
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
							getReadyTimer();
						}

					}else{//for Spanish
						String updatedDateFromDatabase = implObj.getUpdatedDate(grade);
						if(CommonUtils.isUpdateDateChange(updatedDateFromDatabase , grade , updatedList))
						{
							String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
							implObj.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();

							if(CommonUtils.isInternetConnectionAvailable
									(MultiFriendzySchoolCurriculumEquationSolve.this)){
								new GetWordProblemQuestion(grade , 
										MultiFriendzySchoolCurriculumEquationSolve.this)
								.execute(null,null,null);
							}else{
								getReadyTimer();
							}
						}
						else{
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
							getReadyTimer();
						}
					}
				}
			}else{
				if(isFisrtTimeCallForGrade == false){
					CommonUtils.showInternetDialog(MultiFriendzySchoolCurriculumEquationSolve.this);
				}
			}
			super.onPostExecute(updatedList);
		}
	}

	//changes for word problem
	/**
	 * This asynctask get questions from server according to grade if not loaded in database
	 * @author Yashwant Singh
	 *
	 */
	public class GetWordProblemQuestion extends AsyncTask<Void, Void, QuestionLoadedTransferObj>{

		private int grade;
		private ProgressDialog pd;
		private Context context;
		ArrayList<String> imageNameList = new ArrayList<String>();

		public GetWordProblemQuestion(int grade , Context context){
			this.grade 		= grade;
			this.context 	= context;
		}

		@Override
		protected void onPreExecute() {

			Translation translation = new Translation(context);
			translation.openConnection();
			pd = CommonUtils.getProgressDialog(context, "Please Wait, This could take several minutes." , false);
			translation.closeConnection();
			pd.show();

			super.onPreExecute();
		}

		@Override
		protected QuestionLoadedTransferObj doInBackground(Void... params) {
			LearnignCenterSchoolCurriculumServerOperation serverObj = new LearnignCenterSchoolCurriculumServerOperation();
			//QuestionLoadedTransferObj questionData = serverObj.getWordProblemQuestions(grade);
			//changes for Spanish Changes
			QuestionLoadedTransferObj questionData = new QuestionLoadedTransferObj();
			if(CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH)
				questionData = serverObj.getWordProblemQuestions(grade);
			else
				questionData= serverObj.getWordProblemQuestionsForSpanish(grade, CommonUtils.SPANISH);

			//changes for dialog loading wheel
			/*SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl = new SchoolCurriculumLearnignCenterimpl(context);
			schooloCurriculumImpl.openConnection();
			schooloCurriculumImpl.deleteFromWordProblemCategoriesbyGrade(grade);
			schooloCurriculumImpl.deleteFromWordProblemsSubCategoriesByCategoryId(questionData.getCategoryList());
			schooloCurriculumImpl.deleteFromWordProblemsQuestionsCategoryId(questionData.getCategoryList());

			schooloCurriculumImpl.insertIntoWordProblemCategories(questionData.getCategoryList(), grade);
			schooloCurriculumImpl.insertIntoWordProblemsSubCategories(questionData.getCategoryList());
			schooloCurriculumImpl.insertIntoWordProblemsQuestions(questionData.getQuestionList());*/
			if(questionData != null){
				SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl = new SchoolCurriculumLearnignCenterimpl(context);
				schooloCurriculumImpl.openConnection();
				if(CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH){
					//changes for dialog loading wheel
					schooloCurriculumImpl.deleteFromWordProblemCategoriesbyGrade(grade);
					schooloCurriculumImpl.deleteFromWordProblemsSubCategoriesByCategoryId(questionData.getCategoryList());
					schooloCurriculumImpl.deleteFromWordProblemsQuestionsCategoryId(questionData.getCategoryList());

					schooloCurriculumImpl.insertIntoWordProblemCategories(questionData.getCategoryList(), grade);
					schooloCurriculumImpl.insertIntoWordProblemsSubCategories(questionData.getCategoryList());
					schooloCurriculumImpl.insertIntoWordProblemsQuestions(questionData.getQuestionList());
				}else{
					SpanishChangesImpl implObj = new SpanishChangesImpl(context);
					implObj.openConn();

					implObj.deleteFromWordProblemCategoriesbyGrade(grade);
					implObj.deleteFromWordProblemsSubCategoriesByCategoryId(questionData.getCategoryList());
					implObj.deleteFromWordProblemsQuestionsCategoryId(questionData.getCategoryList());

					implObj.insertIntoWordProblemCategories(questionData.getCategoryList(), grade);
					implObj.insertIntoWordProblemsSubCategories(questionData.getCategoryList());
					implObj.insertIntoWordProblemsQuestions(questionData.getQuestionList());
					implObj.closeConn();
				}

				for(int i = 0 ; i < questionData.getQuestionList().size() ; i ++ ){

					WordProblemQuestionTransferObj questionObj = questionData.getQuestionList().get(i);

					if(questionObj.getQuestion().contains(".png"))
						imageNameList.add(questionObj.getQuestion());
					if(questionObj.getOpt1().contains(".png"))
						imageNameList.add(questionObj.getOpt1());
					if(questionObj.getOpt2().contains(".png"))
						imageNameList.add(questionObj.getOpt2());
					if(questionObj.getOpt3().contains(".png"))
						imageNameList.add(questionObj.getOpt3());
					if(questionObj.getOpt4().contains(".png"))
						imageNameList.add(questionObj.getOpt4());
					if(questionObj.getOpt5().contains(".png"))
						imageNameList.add(questionObj.getOpt5());
					if(questionObj.getOpt6().contains(".png"))
						imageNameList.add(questionObj.getOpt6());
					if(questionObj.getOpt7().contains(".png"))
						imageNameList.add(questionObj.getOpt7());
					if(questionObj.getOpt8().contains(".png"))
						imageNameList.add(questionObj.getOpt8());
					if(!questionObj.getImage().equals(""))
						imageNameList.add(questionObj.getImage());
				}

				if(!schooloCurriculumImpl.isImageTableExist()){
					schooloCurriculumImpl.createSchoolCurriculumImageTable();
				}
				schooloCurriculumImpl.closeConnection();
				//end changes
			}
			return questionData;
		}

		@Override
		protected void onPostExecute(QuestionLoadedTransferObj questionData) {

			pd.cancel();

			if(questionData != null){
				DawnloadImagesFromserver serverObj = new DawnloadImagesFromserver(imageNameList, context);
				Thread imageDawnLoadThrad = new Thread(serverObj);
				imageDawnLoadThrad.start();

				getReadyTimer();
			}else{
				CommonUtils.showInternetDialog(context);
			}
			super.onPostExecute(questionData);
		}
	}

	/**
	 * This class dawnload image from server for schoolcurriculum
	 * @author Yashwant Singh
	 *
	 */
	private class DawnloadImageForSchooCurriculumForMultiFriendzy extends AsyncTask<Void, Void, Void>{

		private ArrayList<String> imageNamelist;
		private byte [] imageFromServer;
		private Context context;
		SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl;
		LearnignCenterSchoolCurriculumServerOperation learningCenterServerobj;
		private WordProblemQuestionTransferObj questionObj;

		private ProgressDialog pd = null;

		private Date dawnloadStartTime = null;

		public DawnloadImageForSchooCurriculumForMultiFriendzy(ArrayList<String> imageNamelist , Context context,
				WordProblemQuestionTransferObj questionObj){
			this.imageNamelist 	= imageNamelist;
			this.context   		= context;
			this.questionObj    = questionObj;
			dawnloadStartTime   = new Date();
		}

		@Override
		protected void onPreExecute() {
			pd = CommonUtils.getProgressDialog(context);
			pd.show();
			schooloCurriculumImpl   = new SchoolCurriculumLearnignCenterimpl(context);
			learningCenterServerobj = new LearnignCenterSchoolCurriculumServerOperation();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			for(int i = 0 ; i < imageNamelist.size() ; i ++ ){
				schooloCurriculumImpl.openConnection();
				if(!schooloCurriculumImpl.isImageAlreadyDawnloaded(imageNamelist.get(i))){
					imageFromServer = learningCenterServerobj.dawabloadImageFromServer(imageNamelist.
							get(i));
					schooloCurriculumImpl.insertIntoSchoolCurriculumImageTable(imageNamelist.get(i), 
							imageFromServer);
				}
				schooloCurriculumImpl.closeConnection();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			pd.cancel();

			if(isCountDawnTimer){
				if(timer != null)
					timer.start();
			}else{
				if(customHandler != null){
					Date dawnloadEndTime = new Date();
					startTimeForTimer = startTimeForTimer 
							+ (dawnloadEndTime.getTime() - dawnloadStartTime.getTime());
					customHandler.postDelayed(updateTimerThread, 0);
				}
			}

			if(isEquationFromserver){
				if(handlerSetResultForSecondPlayer != null){
					handlerSetResultForSecondPlayer.postDelayed
					(runnableThreadForSeconPlayer, sleepTimeForSecondPlayer);
				}
			}

			setQuestionAnswerLayout(questionObj);
			super.onPostExecute(result);
		}
	}

	//changes for Spanish
	/**
	 * This method call when user language is Spanish
	 * This method for downloading question from server for Spanish Language
	 */
	private void callWhenUserLanguageIsSpanish(Context context , int lang , int grade){
		try {
			SpanishChangesImpl spanishImplObj = new SpanishChangesImpl(context);
			spanishImplObj.openConn();
			boolean isSpanishQuestionLoaded = spanishImplObj.isQuestionLeaded(grade);
			if(isSpanishQuestionLoaded){
				if(CommonUtils.isInternetConnectionAvailable(context)){
					new GetWordProblemQuestionUpdatedDate(grade , false).execute(null,null,null);
				}else{
					getReadyTimer();
				}
			}
			else{
				if(CommonUtils.isInternetConnectionAvailable(context)){
					new GetWordProblemQuestion(grade ,context)
					.execute(null,null,null);
					new GetWordProblemQuestionUpdatedDate(grade , true).execute(null,null,null);
				}else{
					DialogGenerator dg = new DialogGenerator(context);
					Translation transeletion = new Translation(context);
					transeletion.openConnection();
					dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
					transeletion.closeConnection();	
				}
			}
			spanishImplObj.closeConn();

		} catch (Exception e) {
			Log.e(TAG, "inside callWhenUserLanguageIsSpanish " + e.toString());
		} 
	}

	@Override
	public void onInit(int status) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onResume() {
		if(!isClickOnWorkArea){
			if(!isFromOnCreate){
				if(playSound != null)
					playSound.playSoundForSingleFriendzyEquationSolve
					(MultiFriendzySchoolCurriculumEquationSolve.this);
			}else{
				isFromOnCreate = false;
			}
		}else
			isClickOnWorkArea = false;
		super.onResume();
	}

	@Override
	protected void onPause() {
		if(!isClickOnWorkArea){
			if(playSound != null)
				playSound.stopPlayer();
		}
		super.onPause();
	}
}
