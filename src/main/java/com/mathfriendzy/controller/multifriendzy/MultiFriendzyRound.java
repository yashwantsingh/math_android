package com.mathfriendzy.controller.multifriendzy;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.controller.inapp.GetMoreCoins;
import com.mathfriendzy.controller.multifriendzy.findbyuser.SelectedPlayerActivity;
import com.mathfriendzy.controller.multifriendzy.schoolcurriculum.MultiFriendzySchoolCurriculumEquationSolve;
import com.mathfriendzy.dawnloadimagesfromserver.DawnloadImagesFromserver;
import com.mathfriendzy.gcm.ProcessNotification;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.OnPurchaseDone;
import com.mathfriendzy.model.appunlock.GetAppUnlockStatusResponse;
import com.mathfriendzy.model.chooseAvtar.ChooseAvtarOpration;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.grade.Grade;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.CoinsFromServerObj;
import com.mathfriendzy.model.learningcenter.LearningCenteServerOperation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.multifriendzy.MathFriendzysRoundDTO;
import com.mathfriendzy.model.multifriendzy.MultiFriendzyServerOperation;
import com.mathfriendzy.model.multifriendzy.MultiFriendzysFromServerDTO;
import com.mathfriendzy.model.multifriendzy.OppnentDataDTO;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.LearnignCenterSchoolCurriculumServerOperation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.QuestionLoadedTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SchoolCurriculumLearnignCenterimpl;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.UpdatedInfoTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.WordProblemQuestionTransferObj;
import com.mathfriendzy.model.schoolcurriculum.singlefriendzy.SingleFriendzyServerOperationForWordProblem;
import com.mathfriendzy.model.spanishchanges.SpanishChangesImpl;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.MathVersions;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import static com.mathfriendzy.utils.ICommonUtils.FACEBOOK_HOST_NAME;
import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.MULTIFRIENDZY_ROUND_FLAG;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_ID;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;


/**
 * Round for multifriendzy is start from this activity
 * @author Yashwant Singh
 *
 */
public class MultiFriendzyRound extends ActBaseClass implements OnClickListener
{
	private TextView labelTop 				= null;
	private Button   btnResign 				= null;
	private ImageView imgPlayerImg 			= null;
	private ImageView imgOpponentPlayerImg 	= null;
	private TextView txtVS 					= null;

	private TextView txtPlayerName 			= null;
	private TextView txtOpponentPlayerName 	= null;

	private TextView txtPlayerPoints1 		= null;
	private TextView txtPlayerPoints2 		= null;
	private TextView txtPlayerPoints3		= null;
	private TextView txtPlayerTotalPoints 	= null;

	private TextView txtRound1 				= null;
	private TextView txtRound2 				= null;
	private TextView txtRound3 				= null;
	private TextView txtRoundtotalRound 	= null;

	private TextView txtOpponentPoints1 	= null;
	private TextView txtOpponentPoints2 	= null;
	private TextView txtOpponentPoints3 	= null;
	private TextView txtOpponentTotalPoints = null;

	private RelativeLayout round1layout 	= null;
	private RelativeLayout round2layout 	= null;
	private RelativeLayout round3layout 	= null;
	private RelativeLayout totallayout 		= null;

	private TextView txtItsTheirTurn = null;
	private Button   btngoToFriendzy = null;


	private RelativeLayout saySomethingLayout = null;
	private ImageView saySomethingPlayerImage = null;
	private EditText  edtSaySomething         = null;

	private ImageView imgMedel1Round1 = null;
	private ImageView imgMedel2Round1 = null;
	private ImageView imgMedel1Round2 = null;
	private ImageView imgMedel2Round2 = null;
	private ImageView imgMedel1Round3 = null;
	private ImageView imgMedel2Round3 = null;

	private Bitmap profileImageBitmap = null;
	private final String TAG = this.getClass().getSimpleName();

	public static MultiFriendzysFromServerDTO multiFriendzyServerDto = null;

	public static String oppenentPlayerName = null;
	public static String opponentImageId 	= null;
	public static String turn = null;
	public static String type = null;
	public static String friendzyId = null;

	public static String notificationMessage = "";//this message used in the equation solve screen for send notifiation

	public static ArrayList<MathFriendzysRoundDTO> roundList = null;

	private final String THEIR_FRENDZY_TEXT = "their friendzy";
	private final String YOUR_FRIENDZY_TEXT = "your friendzy";
	private final String HISTORY_TEXT       = "";

	private boolean isDirectGoToPlayScrenn = false;//if your and round start by opponent the direct go to play screen

	private boolean appUnlockStatus = false;

	private final int MULTI_FRIENDZY_ITEM_ID = 12;//multi friendzy unlock item id


	//changes for word problem
	private RelativeLayout layoutCheckBox   = null;
	private ImageView imgSolveEquation 		= null;
	private ImageView imgSchoolCurriculum 	= null;
	private TextView  lblSolveEquation      = null;
	private TextView  lblSchoolCurriculum   = null;
	private RelativeLayout gradeLayout      = null;
	private Spinner spinnerGrade            = null;
	private TextView txtGrade               = null;

	private boolean isForWordProblem = false;
	//changes for show popup
	private boolean isShowPopUp = false;

    //NewInApp changes
    private ProgressDialog progressDialogForNewInApp = null;
    private boolean isCkeckStatusFromOnCreate = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_maulti_friendzy_round);

		if(MULTIFRIENDZY_ROUND_FLAG)
			Log.e(TAG, "inside onCreate()");

        this.init();
		if(ProcessNotification.isFromNotification)
		{		
			//ProcessNotification.isFromNotification = false;//send to the mulitfriendzy equatoin solve

			multiFriendzyServerDto = new MultiFriendzysFromServerDTO();

			String opponentNameFromnotificationMsg = "";
			int counter = 1;
			for(int i = 0 ; i < ProcessNotification.sentNotificationData.getMessage().length() ; i ++ )
			{
				if(ProcessNotification.sentNotificationData.getMessage().charAt(i) != ' ' && counter < 2) 
				{
					opponentNameFromnotificationMsg = opponentNameFromnotificationMsg + ProcessNotification.sentNotificationData.getMessage().charAt(i);
				}
				else
				{
					opponentNameFromnotificationMsg = opponentNameFromnotificationMsg + ProcessNotification.sentNotificationData.getMessage().charAt(i)
							+ ProcessNotification.sentNotificationData.getMessage().charAt(i + 1);
					counter ++ ;
				}

				if(counter == 2)
					break;
			}

			friendzyId         = ProcessNotification.sentNotificationData.getFriendzyId();
			oppenentPlayerName = opponentNameFromnotificationMsg;
			opponentImageId    = ProcessNotification.sentNotificationData.getProfileImageId();
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			turn			   = transeletion.getTranselationTextByTextIdentifier("lblYourTurn");
			transeletion.closeConnection();

			type               = YOUR_FRIENDZY_TEXT;

			multiFriendzyServerDto.setCountryCode(ProcessNotification.sentNotificationData.getCountry());
			if(ProcessNotification.sentNotificationData.getIsNotoficationFromIOS() == 1){
				multiFriendzyServerDto.setNotificationDevices(ProcessNotification.sentNotificationData.getSenderDeviceId());
				multiFriendzyServerDto.setAndroidPids("");
			}
			else{
				multiFriendzyServerDto.setAndroidPids(ProcessNotification.sentNotificationData.getSenderDeviceId());
				multiFriendzyServerDto.setNotificationDevices("");
			}
			
			multiFriendzyServerDto.setFriendzysId(ProcessNotification.sentNotificationData.getFriendzyId());
			OppnentDataDTO opponentData = new OppnentDataDTO();
			opponentData.setParentUserId(ProcessNotification.sentNotificationData.getUserId());
			opponentData.setPlayerId(ProcessNotification.sentNotificationData.getPlayerId());
			opponentData.setfName(opponentNameFromnotificationMsg);
			opponentData.setlName("");
			multiFriendzyServerDto.setOpponentData(opponentData);

			//changes for word problem
			multiFriendzyServerDto.setForWordProblem(ProcessNotification.sentNotificationData.getForWord());
			//end changes

			SharedPreferences sharedPrefPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
			SharedPreferences.Editor editor = sharedPrefPlayerInfo.edit();

			UserPlayerOperation userPlayerOpr = new UserPlayerOperation(this);
			UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(ProcessNotification.sentNotificationData.getOppPlayerId());
			//UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById("1221");

			if(userPlayerData != null )
			{
				UserRegistrationOperation userObj = new UserRegistrationOperation(this);
				RegistereUserDto regUserObj = userObj.getUserData();

				editor.clear();
				editor.putString("playerName", userPlayerData.getFirstname() + " " + userPlayerData.getLastname());
				editor.putString("city", regUserObj.getCity());
				editor.putString("state", regUserObj.getState());
				editor.putString("imageName",  userPlayerData.getImageName());
				editor.putString("coins",  userPlayerData.getCoin());
				editor.putInt("grade", Integer.parseInt(userPlayerData.getGrade()));
				editor.putString("userId",  userPlayerData.getParentUserId());
				editor.putString("playerId",  userPlayerData.getPlayerid());
				Country country = new Country();
				editor.putString("countryName", country.getCountryNameByCountryId(regUserObj.getCountryId(), this));
				editor.commit();
			}

			SharedPreferences sheredPreference = getSharedPreferences(IS_CHECKED_PREFF, 0);
			SharedPreferences.Editor editor1 = sheredPreference.edit();
			editor1.clear();
			//editor1.putString(PLAYER_ID,"1221");
			editor1.putString(PLAYER_ID,ProcessNotification.sentNotificationData.getOppPlayerId());
			editor1.commit();
		}
		/*else
		{
			this.getValueFromIntent();
		}*/

		this.setWidgetsReferences();
		this.setTextFromTranselation();
		this.setListenerOnWidgets();
		this.setPlayerData();

		/*if(ProcessNotification.isFromNotification)
		{			
			ProcessNotification.isFromNotification = false;

			new FindMultiFriendzyRound(ProcessNotification.sentNotificationData.getOppUserId(), 
						ProcessNotification.sentNotificationData.getOppPlayerId(), 
						ProcessNotification.sentNotificationData.getFriendzyId()).execute(null,null,null);
		}
		else
		{
			this.setRoundData();
			this.setVisibilityOfSySomethingLayout();
		}*/



		//changes for word problem
		this.setChecked();//set checked
		this.getGrade();
		//end changes

		this.setVisibilityOfResignButton();

		//this.setRoundData();
		this.setRoundData();
		this.setVisibilityOfSySomethingLayout();
		this.checkApplicationSatus(true);

        this.setVisibilityOfLayoutBasedOnAppVersion();

		if(MULTIFRIENDZY_ROUND_FLAG)
			Log.e(TAG, "outside onCreate()");
	}

    //New InAppp Changes
    private void init(){
        try{
            progressDialogForNewInApp = MathFriendzyHelper.getProgressDialog(this , "");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

	/**
	 * This method invisible the resign button 
	 */
	private void setVisibilityOfResignButton(){
		try{
			if(roundList != null && roundList.size() == 0){
				btnResign.setVisibility(Button.INVISIBLE);
			}else if(roundList == null){
				btnResign.setVisibility(Button.INVISIBLE);
			}
		}catch(Exception e){
			
		}
	}

	//changes for word problem
	/**
	 * This method set the checked image for solve equation or school curriculum
	 */
	private void setChecked(){
		if(CommonUtils.isClickedSolveEquation){
			imgSolveEquation.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
			imgSchoolCurriculum.setBackgroundResource(R.drawable.mf_check_box_ipad);
			gradeLayout.setVisibility(RelativeLayout.GONE);
		}else{
			imgSchoolCurriculum.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
			imgSolveEquation.setBackgroundResource(R.drawable.mf_check_box_ipad);
			gradeLayout.setVisibility(RelativeLayout.VISIBLE);
		}
	}
	//end changes

	//changes for word problem
	/** 
	 * @Descritpion getGradeData from database 
	 * and set it to the grade adapter
	 * @param
	 * @param
	 */
	private void getGrade(){			
		Grade gradeObj = new Grade();
		ArrayList<String> gradeList = gradeObj.getGradeList(this);
		this.setGradeAdapter(gradeList);
	}

	/**
	 * @Description set Grade data to adapter
	 * @param
	 * @param
	 * @param gradeList 
	 */
	private void setGradeAdapter(ArrayList<String> gradeList)
	{			
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);  
		final boolean isTab = getResources().getBoolean(R.bool.isTablet);

		gradeList.subList(12, 13).clear();//New change on 02 feb 2016

		ArrayAdapter<String> gradeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,gradeList);
		gradeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerGrade.setAdapter(gradeAdapter);
		spinnerGrade.setSelection(gradeAdapter.getPosition("1"));

		spinnerGrade.setOnItemSelectedListener(new OnItemSelectedListener() 
		{	@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
			((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
			if(!isTab)
				((TextView) parent.getChildAt(0)).setTextSize(10);
			((TextView) parent.getChildAt(0)).setTypeface(null, Typeface.BOLD);

			//changes for Spanish
			CommonUtils.isOneTimeClick = true;
			//end changes
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {

		}
		});
	}
	//end changes


	/**
	 * This method check for application is unlock or not
	 * @param itemId
	 * @param userId
	 * @return
	 */
	protected int getApplicationUnLockStatus(int itemId,String userId)
	{	
		int appStatus = 0;
		LearningCenterimpl learnignCenterImpl = new LearningCenterimpl(this);
		learnignCenterImpl.openConn();
		appStatus = learnignCenterImpl.getAppUnlockStatus(itemId,userId);
		learnignCenterImpl.closeConn();
		return appStatus;
	}

	/**
	 * This method check the application status its purchase or not 	
	 */
	private void checkApplicationSatus(boolean isFromOnCreate){
        this.isCkeckStatusFromOnCreate = isFromOnCreate;
        if(MathFriendzyHelper.isAppUnlockCategoryPurchased(this)){
            appUnlockStatus = true;
            return ;
        }

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		if(this.getApplicationUnLockStatus(100 , sharedPreffPlayerInfo.getString("userId", "")) == 1)
			appUnlockStatus = true;
		else if(this.getApplicationUnLockStatus(MULTI_FRIENDZY_ITEM_ID ,
                sharedPreffPlayerInfo.getString("userId", "")) == 1)
			appUnlockStatus = true ;
		else{
			if(MultiFriendzyMain.isNewFriendzyStart){
				new CheckFriendzyExistsForPlayers(sharedPreffPlayerInfo.getString("userId", ""),
						sharedPreffPlayerInfo.getString("playerId", ""), SelectedPlayerActivity.player.getParentUserId() + ""
						, SelectedPlayerActivity.player.getPlayerId() + "").execute(null,null,null);
			}else{
				appUnlockStatus = true;
			}
		}
	}

	/**
	 * This method set the player annd oppenent data
	 */
	private void setPlayerData() 
	{
		if(MULTIFRIENDZY_ROUND_FLAG)
			Log.e(TAG, "inside setPlayerData()");

		//set player data
		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		String fullName = sharedPreffPlayerInfo.getString("playerName", "");
		String playerName = fullName.substring(0,(fullName.indexOf(" ") + 2));
		//txtPlayerName.setText(sharedPreffPlayerInfo.getString("playerName", ""));
		txtPlayerName.setText(playerName + ".");
		String imageName = sharedPreffPlayerInfo.getString("imageName",null);
		this.setImage(imgPlayerImg, imageName);
		this.setImage(saySomethingPlayerImage, imageName);

		//set opponent data
		txtOpponentPlayerName.setText(oppenentPlayerName + ".");
		this.setImage(imgOpponentPlayerImg, opponentImageId);

		if(MULTIFRIENDZY_ROUND_FLAG)
			Log.e(TAG, "outside setPlayerData()");

	}


	/**
	 * This method set the opponent image
	 * @param
	 * @param imageName
	 */
	private void setImage(ImageView imgPlayer , String imageName)
	{
		try
		{
			Long.parseLong(imageName);
			//changes for Internet Connection
			if(CommonUtils.isInternetConnectionAvailable(this))
			{
				String strUrl = FACEBOOK_HOST_NAME + imageName + "/picture?type=large";
				new FacebookImageLoaderTask(strUrl , imgPlayer).execute(null,null,null);
			}
			else
			{
				imgPlayer.setBackgroundResource(R.drawable.smiley);

			}
		}
		catch(NumberFormatException ee)
		{
			ChooseAvtarOpration chooseAvtarObj = new ChooseAvtarOpration();
			chooseAvtarObj.openConn(this);
			if(chooseAvtarObj.getAvtarImageByName(imageName) != null)
			{
				//profileImageBitmap = CommonUtils.getBitmapFromByte(chooseAvtarObj.getAvtarImageByName(imageName) , this);
				profileImageBitmap = CommonUtils.getBitmapFromByte(chooseAvtarObj.getAvtarImageByName(imageName));
				imgPlayer.setImageBitmap(profileImageBitmap);
			}
			chooseAvtarObj.closeConn();
		}
	}

	/**
	 * This method set the round data
	 */
	private void setRoundData() 
	{	
		if(MULTIFRIENDZY_ROUND_FLAG)
			Log.e(TAG, "inside setRoundData()");

		if(roundList.size() > 0)
		{
			this.setRoundLayoutBackGround(roundList.size());

			for( int i = 0 ; i < roundList.size() ; i ++ )
			{
				this.setRoundValue(i + 1);
			}

			this.setRoundTotalData();
		}
		else
		{
			this.setRoundLayoutBackGround(1);
			txtPlayerPoints1.setVisibility(TextView.VISIBLE);
			this.setPoints("", txtPlayerPoints1);
		}

		//changes
		if(roundList.size() > 0)
		{
			if(roundList.size() < 3)
			{
				if(roundList.get(roundList.size() - 1).getOppScore().length() > 0 
						&& roundList.get(roundList.size() - 1).getPlayerScore().length() > 0)
				{
					this.setRoundLayoutBackGroundWhite(roundList.size());
					this.setRoundLayoutBackGround(roundList.size() + 1);

					if(type.equals(YOUR_FRIENDZY_TEXT))
					{
						if(roundList.size() == 1)
						{
							txtPlayerPoints2.setVisibility(TextView.VISIBLE);
							this.setPoints("", txtPlayerPoints2);
						}
						else
						{
							txtPlayerPoints3.setVisibility(TextView.VISIBLE);
							this.setPoints("", txtPlayerPoints3);
						}
					}
					else if(type.equals(THEIR_FRENDZY_TEXT))
					{
						if(roundList.size() == 1)
						{
							txtOpponentPoints2.setVisibility(TextView.VISIBLE);
							this.setPoints("", txtOpponentPoints2);
						}
						else
						{
							txtOpponentPoints3.setVisibility(TextView.VISIBLE);
							this.setPoints("", txtOpponentPoints3);
						}
					}
				}
			}
		}
		//end changes


		for( int i = 0 ; i < roundList.size() ; i ++ )
		{
			if((roundList.get(i).getOppScore().length() > 0 
					&& roundList.get(i).getPlayerScore().length() > 0))
			{
				if((Integer.parseInt(roundList.get(i).getOppScore())) > (Integer.parseInt(roundList.get(i).getPlayerScore())))
				{
					setMedel(i + 1 , false);
				}
				else
				{
					setMedel(i + 1 , true);
				}
			}
		}

		if(MULTIFRIENDZY_ROUND_FLAG)
			Log.e(TAG, "outside setRoundData()");
	}

	/**
	 * This method set the player medel
	 * @param index
	 * @param isPlayerMedel
	 */
	private void setMedel(int index , boolean isPlayerMedel)
	{
		switch(index)
		{
		case 1:
			if(isPlayerMedel)
				imgMedel1Round1.setVisibility(ImageView.VISIBLE);
			else
				imgMedel2Round1.setVisibility(ImageView.VISIBLE);
			break;
		case 2:
			if(isPlayerMedel)
				imgMedel1Round2.setVisibility(ImageView.VISIBLE);
			else
				imgMedel2Round2.setVisibility(ImageView.VISIBLE);
			break;
		case 3:
			if(isPlayerMedel)
				imgMedel1Round3.setVisibility(ImageView.VISIBLE);
			else
				imgMedel2Round3.setVisibility(ImageView.VISIBLE);
			break;
		}
	}


	/**
	 * This method set the total value of all round
	 */
	private void setRoundTotalData() 
	{
		if(MULTIFRIENDZY_ROUND_FLAG)
			Log.e(TAG, "inside setRoundTotalData()");

		int playerTotal 	= 0 ; 
		int opponentTotal 	= 0 ;

		for(int i = 0 ; i < roundList.size() ; i ++)
		{
			if(!roundList.get(i).getPlayerScore().equals(""))
			{
				playerTotal = playerTotal + Integer.parseInt(roundList.get(i).getPlayerScore());
			}

			if(!roundList.get(i).getOppScore().equals(""))
			{
				opponentTotal = opponentTotal + Integer.parseInt(roundList.get(i).getOppScore());
			}
		}

		txtPlayerTotalPoints.setText(CommonUtils.setNumberString(playerTotal + ""));
		txtOpponentTotalPoints.setText(CommonUtils.setNumberString(opponentTotal + ""));

		if(MULTIFRIENDZY_ROUND_FLAG)
			Log.e(TAG, "outside setRoundTotalData()");
	}

	/**
	 * This method set the round value
	 */
	private void setRoundValue(int index) 
	{
		switch(index)
		{
		case 1 : 
			txtPlayerPoints1.setVisibility(TextView.VISIBLE);
			txtOpponentPoints1.setVisibility(TextView.VISIBLE);
			this.setPoints(roundList.get(0).getPlayerScore(), txtPlayerPoints1);
			this.setPoints(roundList.get(0).getOppScore(), txtOpponentPoints1);
			break;

		case 2 : 
			txtPlayerPoints2.setVisibility(TextView.VISIBLE);
			txtOpponentPoints2.setVisibility(TextView.VISIBLE);
			this.setPoints(roundList.get(1).getPlayerScore(), txtPlayerPoints2);
			this.setPoints(roundList.get(1).getOppScore(), txtOpponentPoints2);
			break;

		case 3 : 
			txtPlayerPoints3.setVisibility(TextView.VISIBLE);
			txtOpponentPoints3.setVisibility(TextView.VISIBLE);
			this.setPoints(roundList.get(2).getPlayerScore(), txtPlayerPoints3);
			this.setPoints(roundList.get(2).getOppScore(), txtOpponentPoints3);
			break;
		}
	}

	/**
	 * This method set the points
	 */
	private void setPoints(String points , TextView txtpoints)
	{
		if(points.equals(""))
		{
			txtpoints.setText(turn);
		}
		else
		{
			txtpoints.setText(CommonUtils.setNumberString(points));
		}
	}

	/**
	 * This metod set the ruond layot background
	 */
	private void setRoundLayoutBackGround(int index)
	{
		switch(index)
		{
		case 1:
			round1layout.setBackgroundResource(+ R.layout.layout_for_round_1_for_multifriendzy);
			break;
		case 2:
			round2layout.setBackgroundColor(Color.parseColor("#F0E79A"));
			break;
		case 3:
			round3layout.setBackgroundColor(Color.parseColor("#F0E79A"));
			break;
		}
	}


	/**
	 * This metod set the ruond layot background as white
	 */
	private void setRoundLayoutBackGroundWhite(int index)
	{
		switch(index)
		{
		case 1:
			round1layout.setBackgroundResource(0);
			break;
		case 2:
			round2layout.setBackgroundResource(0);
			break;
		case 3:
			round3layout.setBackgroundResource(0);
			break;
		}
	}
	/**
	 * This method set the widgets references
	 */
	private void setWidgetsReferences() 
	{
		if(MULTIFRIENDZY_ROUND_FLAG)
			Log.e(TAG, "inside setWidgetsReferences()");

		labelTop 				= (TextView) findViewById(R.id.labelTop);
		btnResign 				= (Button) findViewById(R.id.btnResign);
		imgPlayerImg 			= (ImageView) findViewById(R.id.imgPlayerImg);
		imgOpponentPlayerImg 	= (ImageView) findViewById(R.id.imgOpponentPlayerImg);
		txtVS 					= (TextView) findViewById(R.id.txtVS);

		txtPlayerName 			= (TextView) findViewById(R.id.txtPlayerName);
		txtOpponentPlayerName 	= (TextView) findViewById(R.id.txtOpponentPlayerName);

		txtPlayerPoints1 		= (TextView) findViewById(R.id.txtPlayerPoints1);
		txtPlayerPoints2 		= (TextView) findViewById(R.id.txtPlayerPoints2);
		txtPlayerPoints3 		= (TextView) findViewById(R.id.txtPlayerPoints3);
		txtPlayerTotalPoints 	= (TextView) findViewById(R.id.txtPlayerTotalPoints);

		txtRound1 				= (TextView) findViewById(R.id.txtRound1);
		txtRound2 				= (TextView) findViewById(R.id.txtRound2);
		txtRound3 				= (TextView) findViewById(R.id.txtRound3);
		txtRoundtotalRound 		= (TextView) findViewById(R.id.txtRoundtotalRound);


		txtOpponentPoints1 		= (TextView) findViewById(R.id.txtOpponentPoints1);
		txtOpponentPoints2 		= (TextView) findViewById(R.id.txtOpponentPoints2);
		txtOpponentPoints3 		= (TextView) findViewById(R.id.txtOpponentPoints3);
		txtOpponentTotalPoints 	= (TextView) findViewById(R.id.txtOpponentTotalPoints);


		round1layout 			= (RelativeLayout) findViewById(R.id.round1layout);
		round2layout 			= (RelativeLayout) findViewById(R.id.round2layout);
		round3layout 			= (RelativeLayout) findViewById(R.id.round3layout);
		totallayout				= (RelativeLayout) findViewById(R.id.totalLayout);

		txtItsTheirTurn 		= (TextView) findViewById(R.id.txtItsTheirTurn);

		btngoToFriendzy 		= (Button) findViewById(R.id.btngoToFriendzy);

		saySomethingLayout 		= (RelativeLayout) findViewById(R.id.saySomethingLayout);
		saySomethingPlayerImage = (ImageView) findViewById(R.id.saySomethingPlayerImage);
		edtSaySomething         = (EditText) findViewById(R.id.edtSaySomething);

		imgMedel1Round1         = (ImageView) findViewById(R.id.imgMedel1Round1);
		imgMedel2Round1         = (ImageView) findViewById(R.id.imgMedel2Round1);
		imgMedel1Round2         = (ImageView) findViewById(R.id.imgMedel1Round2);
		imgMedel2Round2         = (ImageView) findViewById(R.id.imgMedel2Round2);
		imgMedel1Round3         = (ImageView) findViewById(R.id.imgMedel1Round3);
		imgMedel2Round3         = (ImageView) findViewById(R.id.imgMedel2Round3);


		//changes for word problem
		layoutCheckBox 		= (RelativeLayout) findViewById(R.id.layoutCheckBox);
		imgSolveEquation 	= (ImageView)  findViewById(R.id.imgSolveEquation);
		imgSchoolCurriculum = (ImageView)  findViewById(R.id.imgSchoolCurriculum);
		lblSolveEquation    = (TextView)   findViewById(R.id.lblSolveEquation);
		lblSchoolCurriculum = (TextView)   findViewById(R.id.lblSchoolCurriculum);
		gradeLayout         = (RelativeLayout) findViewById(R.id.gradeLayout);
		spinnerGrade        = (Spinner)    findViewById(R.id.spinnerGrade);
		txtGrade            = (TextView)   findViewById(R.id.txtGrade);

		if(MULTIFRIENDZY_ROUND_FLAG)
			Log.e(TAG, "outside setWidgetsReferences()");
	}

	/**
	 * This method set the text from transelation table
	 */
	private void setTextFromTranselation() 
	{
		if(MULTIFRIENDZY_ROUND_FLAG)
			Log.e(TAG, "inside setTextFromTranselation()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		labelTop.setText(transeletion.getTranselationTextByTextIdentifier("lblBestOf5Friendzy"));
		btnResign.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleResign"));

		txtRound1.setText(transeletion.getTranselationTextByTextIdentifier("lblRound") + " 1");
		txtRound2.setText(transeletion.getTranselationTextByTextIdentifier("lblRound") + " 2");
		txtRound3.setText(transeletion.getTranselationTextByTextIdentifier("lblRound") + " 3");
		txtRoundtotalRound.setText(transeletion.getTranselationTextByTextIdentifier("lblTotal"));

		txtItsTheirTurn.setText(transeletion.getTranselationTextByTextIdentifier("lblIts")
				+ " " + turn + "!");

		if(type.equals(YOUR_FRIENDZY_TEXT))
			btngoToFriendzy.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGo") + "!");
		else if(type.equals(THEIR_FRENDZY_TEXT))	
			btngoToFriendzy.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGoToFriendzys"));
		else
			btngoToFriendzy.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGoToFriendzys"));

		//changes for word problem
		lblSolveEquation.setText(transeletion.getTranselationTextByTextIdentifier("lblSolveEquations"));
		lblSchoolCurriculum.setText(transeletion.getTranselationTextByTextIdentifier("lblSchoolsCurriculum"));
		txtGrade.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade") + ":");
		//end changes

		transeletion.closeConnection();

		//this.setVisibilityOfSySomethingLayout();

		if(MULTIFRIENDZY_ROUND_FLAG)
			Log.e(TAG, "outside setTextFromTranselation()");
	}


	/**
	 * This method visible or invisible the say something layout
	 */
	private void setVisibilityOfSySomethingLayout()
	{	
		if(!type.equals(YOUR_FRIENDZY_TEXT)){
			saySomethingLayout.setVisibility(RelativeLayout.GONE);
			//changes for word problem
			layoutCheckBox.setVisibility(RelativeLayout.GONE);
			gradeLayout.setVisibility(RelativeLayout.GONE);
			//end changes
		}
		else
		{
			if(MultiFriendzyRound.roundList.size() > 0)
			{
				int index = MultiFriendzyRound.roundList.size() - 1 ;

				if(MultiFriendzyRound.roundList.get(index).getPlayerScore().length() == 0)
				{
					isDirectGoToPlayScrenn = true;
					saySomethingLayout.setVisibility(RelativeLayout.GONE);

					//changes for word problem
					layoutCheckBox.setVisibility(RelativeLayout.GONE);
					gradeLayout.setVisibility(RelativeLayout.GONE);
					//end changes	
				}else{
					isShowPopUp = true;
					//changes for word problem
					layoutCheckBox.setVisibility(RelativeLayout.VISIBLE);
					gradeLayout.setVisibility(RelativeLayout.VISIBLE);
					if(multiFriendzyServerDto.getForWordProblem() == 0){
						CommonUtils.isClickedSolveEquation = true;
					}
					else{
						CommonUtils.isClickedSolveEquation = false;
					}
					this.setChecked();
					//end changes
				}
			}
		}
	}

	/**
	 * This method set listener on widgets
	 */
	private void setListenerOnWidgets() 
	{
		if(MULTIFRIENDZY_ROUND_FLAG)
			Log.e(TAG, "inside setListenerOnWidgets()");

		btngoToFriendzy.setOnClickListener(this);
		btnResign.setOnClickListener(this);

		//changes for word problem
		imgSolveEquation.setOnClickListener(this);
		imgSchoolCurriculum.setOnClickListener(this);
		//end changes

		if(MULTIFRIENDZY_ROUND_FLAG)
			Log.e(TAG, "outside setListenerOnWidgets()");
	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.btngoToFriendzy : 
			this.setListenerOnbtngoToFriendzy();
			break;
		case R.id.btnResign :
			this.clickOnBtnResign();
			break;
		case R.id.imgSolveEquation :
			if(isShowPopUp){
				if(CommonUtils.isClickedSolveEquation == false)
					//this.showPopForClickOnChecKBox(true);
					this.showPopForClickOnChecKBox(1);
			}else{
				CommonUtils.isClickedSolveEquation = true;
				this.setChecked();
			}
			break;
		case R.id.imgSchoolCurriculum :
			if(isShowPopUp){
				if(CommonUtils.isClickedSolveEquation == true)
					//this.showPopForClickOnChecKBox(false);
					this.showPopForClickOnChecKBox(2);
			}else{
				CommonUtils.isClickedSolveEquation = false;
				this.setChecked();
			}
			break;	
		}
	}

	/**
	 * This method call when user click on the Practice Skill IF friendly start with School Curriculum in the
	 * middle of the game and viceversa
	 * @param
	 */
	private void showPopForClickOnChecKBox(int popUpCode){
		DialogGenerator dg = new DialogGenerator(this);
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		if(popUpCode == 1){
			dg.generateWarningDialog(transeletion.
					getTranselationTextByTextIdentifier("lblYouCanOnlyChoose")
					.replace("___", transeletion.
							getTranselationTextByTextIdentifier("lblSchoolsCurriculum")));
		}else if(popUpCode == 2){
			dg.generateWarningDialog(transeletion.
					getTranselationTextByTextIdentifier("lblYouCanOnlyChoose")
					.replace("___", transeletion.
							getTranselationTextByTextIdentifier("lblSolveEquations")));
		}else if(popUpCode == 3){
			dg.generateWarningDialog(transeletion.
					getTranselationTextByTextIdentifier("lblYouCanOnlyResignWhenYourTurn"));
		}
		transeletion.closeConnection();
	}

	/**
	 * This method call when click on resign button
	 */
	private void clickOnBtnResign()
	{
		if(type.equals(YOUR_FRIENDZY_TEXT) && roundList.size() > 0){
			DialogGenerator dg = new DialogGenerator(this);
			dg.generateResignDialogForMultifriendzy(friendzyId ,multiFriendzyServerDto.getOpponentData().getPlayerId());
		}else if(roundList.size() > 0){
			this.showPopForClickOnChecKBox(3);
		}
	}


	/**
	 * This method call when click on btngoToFriendzy
	 */
	private void setListenerOnbtngoToFriendzy()
	{
		if(!type.equals(YOUR_FRIENDZY_TEXT))
			startActivity(new Intent(this,MultiFriendzyMain.class));
		else
		{
			if(appUnlockStatus)
			{
				if(isDirectGoToPlayScrenn)
				{
					notificationMessage = "";
					//for test
					if(multiFriendzyServerDto.getForWordProblem() == 0){//for testing
						//need chenges for word problem for notification
						ArrayList<Integer> list = new ArrayList<Integer>();
						list.add(1);
						list.add(2);
						Intent intent = new Intent(this , MultiFriendzyEquationSolve.class);
						intent.putIntegerArrayListExtra("selectedCategories", list);
						startActivity(intent);
					}else{//for testing
						this.clickOnGoForSchollCurriculum();
					}
				}
				else
				{
					if(MultiFriendzyRound.roundList.size() > 0){
						notificationMessage = edtSaySomething.getText().toString();
						if(multiFriendzyServerDto.getForWordProblem() == 0){
							//changes for friendzy challenge
							if(CommonUtils.isActivePlayer){
								Intent intent = new Intent(this , MultiFriendzyEquationSolve.class);
								startActivity(intent);
							}
							else
								startActivity(new Intent(this,MultiFriendzyProblemType.class));
							//end changes
						}else{
							this.clickOnGoForSchollCurriculum();
						}
					}else{
						notificationMessage = edtSaySomething.getText().toString();
						//changes for word problem
						if(CommonUtils.isClickedSolveEquation){
							//changes for friendzy challenge
							if(CommonUtils.isActivePlayer){
								Intent intent = new Intent(this , MultiFriendzyEquationSolve.class);
								startActivity(intent);
							}
							else
								startActivity(new Intent(this,MultiFriendzyProblemType.class));
							//end changes
						}else{
							this.clickOnGoForSchollCurriculum();
						}
						//end changes
					}
				}
			}
			else
			{
				this.checkApplicationSatus(false);
			}
		}
	}

	//Changes for word problem
	/**
	 * This method call when user select school curriculum
	 * and click on go
	 */
	private void clickOnGoForSchollCurriculum(){
		loadQuestionFromServer(Integer.parseInt(spinnerGrade.getSelectedItem().toString()));
	}
	//end changes

	/**
	 * This method start the play screen for school curriculum
	 */
	private void goOnPlayScreenForSchoolCurriculum(){
		Intent intent = new Intent(new Intent(this,MultiFriendzySchoolCurriculumEquationSolve.class));
		intent.putExtra("grade", Integer.parseInt(spinnerGrade.getSelectedItem().toString()));
		startActivity(intent);

	}

	//changes for word problem
	/**
	 * This method call when user click on grade 
	 */
	private void loadQuestionFromServer(int grade){
		//changes for Spanish
		if(CommonUtils.getUserLanguageCode(this) == CommonUtils.ENGLISH){
			SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl = 
					new SchoolCurriculumLearnignCenterimpl(this);
			schooloCurriculumImpl.openConnection();
			boolean isQuestionLoaded = schooloCurriculumImpl.isQuestionLeaded(grade);
			schooloCurriculumImpl.closeConnection();

			if(isQuestionLoaded){
				if(CommonUtils.isInternetConnectionAvailable(this)){
					new GetWordProblemQuestionUpdatedDate(grade , false).execute(null,null,null);
				}else{
					this.goOnPlayScreenForSchoolCurriculum();
				}
			}
			else{
				if(CommonUtils.isInternetConnectionAvailable(this)){
					new GetWordProblemQuestion(grade ,this)
					.execute(null,null,null);
					new GetWordProblemQuestionUpdatedDate(grade , true).execute(null,null,null);
				}else{
					DialogGenerator dg = new DialogGenerator(this);
					Translation transeletion = new Translation(this);
					transeletion.openConnection();
					dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
					transeletion.closeConnection();	
				}
			}
		}else{
			this.callWhenUserLanguageIsSpanish(this, CommonUtils.SPANISH, grade);
		}
	}
	//end changes

	@Override
	public void onBackPressed() 
	{		
		startActivity(new Intent(this,MultiFriendzyMain.class));
		super.onBackPressed();
	}


	/**
	 * This asyncTask set image from facebook url to the Button 
	 * @author Yashwant Singh
	 *
	 */
	class FacebookImageLoaderTask extends AsyncTask<Void, Void, Void>
	{
		private String strUrl = null;
		private ImageView imgPlayer = null;
		public FacebookImageLoaderTask(String strUrl , ImageView imgPlayer)
		{
			this.strUrl = strUrl;
			this.imgPlayer = imgPlayer;
		}

		@Override
		protected Void doInBackground(Void... params) 
		{				
			URL img_value;
			try 
			{
				img_value = new URL(strUrl);
				profileImageBitmap = BitmapFactory.decodeStream(img_value.openConnection().getInputStream());
			} 
			catch (MalformedURLException e) 
			{			
				e.printStackTrace();
				Log.e("EditActivity", "Problem in setting image" + e);
			}
			catch(Exception ee)
			{
				Log.e("EditActivity", "Problem in setting image" + ee);
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			imgPlayer.setImageBitmap(profileImageBitmap);
			imgPlayer.invalidate();
			super.onPostExecute(result);
		}
	}

	/**
	 * This clas scheck for play with already exist player or not
	 * @author Yashwant Singh
	 *
	 */
	class CheckFriendzyExistsForPlayers extends AsyncTask<Void, Void, Void>
	{
		private String userId;
		private String playerId;
		private String opponentUserId;
		private String opponentPlayerId;
		private int alreadyExistPlayer = 0;
		private ProgressDialog pg = null;

		CheckFriendzyExistsForPlayers(String userId , String playerId , String opponentUserId , String opponentPlayerId)
		{
			this.userId 		= userId;
			this.playerId		= playerId;
			this.opponentPlayerId = opponentPlayerId;
			this.opponentUserId = opponentUserId;

			pg = CommonUtils.getProgressDialog(MultiFriendzyRound.this);
			pg.show();
		}

		@Override
		protected void onPreExecute() 
		{
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			MultiFriendzyServerOperation serverObj = new MultiFriendzyServerOperation();
			alreadyExistPlayer = serverObj.checkFriendzyExistsForPlayers(userId, playerId, opponentUserId, opponentPlayerId);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			pg.cancel();

			if(alreadyExistPlayer == 0)
				appUnlockStatus = true;
			else
			{
				/*SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
				String api = "itemId=" + MULTI_FRIENDZY_ITEM_ID + "&userId="+sharedPreffPlayerInfo.getString("userId", "")
						+"&playerId="+sharedPreffPlayerInfo.getString("playerId", "");
				new GetRequiredCoinsForPurchaseItem(api).execute(null,null,null);*/
                openToPurchaseLockCategory();
			}

			super.onPostExecute(result);
		}
	}

	/**
	 * This class get coins from server
	 * @author Yashwant Singh
	 *
	 */
	class GetRequiredCoinsForPurchaseItem extends AsyncTask<Void, Void, Void>
	{
		private String apiString = null;
		private CoinsFromServerObj coindFromServer;
		private ProgressDialog pg = null;

		GetRequiredCoinsForPurchaseItem(String apiValue)
		{
			this.apiString = apiValue;
		}

		@Override
		protected void onPreExecute() 
		{
			pg = CommonUtils.getProgressDialog(MultiFriendzyRound.this);
			pg.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			LearningCenteServerOperation learnignCenterOpr = new LearningCenteServerOperation();
			coindFromServer = learnignCenterOpr.getRequiredCoisForPurchase(apiString);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{			
			pg.cancel();

			if(coindFromServer != null){
				/*if(coindFromServer.getCoinsEarned() == -1 || coindFromServer.getCoinsEarned() >= coindFromServer.getCoinsRequired() 
					|| coindFromServer.getCoinsPurchase() == 0)
			{*/
				if(coindFromServer.getCoinsEarned() == -1)
				{
					LearningCenterimpl learnignCenterObj = new LearningCenterimpl(MultiFriendzyRound.this);
					learnignCenterObj.openConn();
					DialogGenerator dg = new DialogGenerator(MultiFriendzyRound.this);
					dg.generateLevelWarningDialogForLearnignCenter(learnignCenterObj.getDataFromPlayerTotalPoints("0").getCoins() ,
							coindFromServer , MULTI_FRIENDZY_ITEM_ID , null);
					learnignCenterObj.closeConn();
				}
				else
				{

					DialogGenerator dg = new DialogGenerator(MultiFriendzyRound.this);
					dg.generateLevelWarningDialogForLearnignCenter(coindFromServer.getCoinsEarned() ,
							coindFromServer , MULTI_FRIENDZY_ITEM_ID , null);
				}
				/*}
			else
			{
				DialogGenerator dg = new DialogGenerator(MultiFriendzyRound.this);
				dg.generateDialogForNeedCoinsForUlock(coindFromServer.getCoinsEarned() ,
						coindFromServer.getCoinsRequired());
			}*/
			}else{
				CommonUtils.showInternetDialog(MultiFriendzyRound.this);
			}
			super.onPostExecute(result);
		}
	}

	/**
	 * This class find multi friendzy rounds when user from notification
	 * @author Yashwant Singh
	 *
	 */
	class FindMultiFriendzyRound extends AsyncTask<Void, Void, Void>
	{
		private String userId ;
		private String playerId;
		private String friendzyId;
		private MultiFriendzysFromServerDTO multifriendzyDataFromSerevr = null;
		private ProgressDialog pd = null;

		FindMultiFriendzyRound(String userId ,String playerId , String friendzyId)
		{
			roundList = new ArrayList<MathFriendzysRoundDTO>();
			multifriendzyDataFromSerevr = new MultiFriendzysFromServerDTO();

			this.userId 	= userId;
			this.playerId 	= playerId;
			this.friendzyId = friendzyId;
		}

		@Override
		protected void onPreExecute() 
		{
			pd = CommonUtils.getProgressDialog(MultiFriendzyRound.this);
			pd.show();

			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			MultiFriendzyServerOperation serverObj = new MultiFriendzyServerOperation();
			multifriendzyDataFromSerevr = serverObj.getRoundList(userId, playerId, friendzyId);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			pd.cancel();
			if(multifriendzyDataFromSerevr != null){
				roundList = multifriendzyDataFromSerevr.getRoundList();
				setRoundData();
				setVisibilityOfSySomethingLayout();
			}else{
				CommonUtils.showInternetDialog(MultiFriendzyRound.this);
			}
			super.onPostExecute(result);
		}
	}

	//changes for word problem
	/**
	 * This asynctask get questions from server according to grade if not loaded in database
	 * @author Yashwant Singh
	 *
	 */
	public class GetWordProblemQuestion extends AsyncTask<Void, Void, QuestionLoadedTransferObj>{

		private int grade;
		private ProgressDialog pd;
		private Context context;
		ArrayList<String> imageNameList = new ArrayList<String>();

		public GetWordProblemQuestion(int grade , Context context){
			this.grade 		= grade;
			this.context 	= context;
		}

		@Override
		protected void onPreExecute() {

			Translation translation = new Translation(context);
			translation.openConnection();
			pd = CommonUtils.getProgressDialog(context, translation.getTranselationTextByTextIdentifier
					("alertPleaseWaitWeAreDownloading") , true);
			translation.closeConnection();
			pd.show();

			super.onPreExecute();
		}

		@Override
		protected QuestionLoadedTransferObj doInBackground(Void... params) {
			LearnignCenterSchoolCurriculumServerOperation serverObj = new LearnignCenterSchoolCurriculumServerOperation();
			//QuestionLoadedTransferObj questionData = serverObj.getWordProblemQuestions(grade);
			//changes for Spanish Changes
			QuestionLoadedTransferObj questionData = new QuestionLoadedTransferObj();
			if(CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH)
				questionData = serverObj.getWordProblemQuestions(grade);
			else
				questionData= serverObj.getWordProblemQuestionsForSpanish(grade, CommonUtils.SPANISH);

			if(questionData != null){
				//changes for dialog loading wheel
				/*SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl = new SchoolCurriculumLearnignCenterimpl(context);
			schooloCurriculumImpl.openConnection();
			schooloCurriculumImpl.deleteFromWordProblemCategoriesbyGrade(grade);
			schooloCurriculumImpl.deleteFromWordProblemsSubCategoriesByCategoryId(questionData.getCategoryList());
			schooloCurriculumImpl.deleteFromWordProblemsQuestionsCategoryId(questionData.getCategoryList());

			schooloCurriculumImpl.insertIntoWordProblemCategories(questionData.getCategoryList(), grade);
			schooloCurriculumImpl.insertIntoWordProblemsSubCategories(questionData.getCategoryList());
			schooloCurriculumImpl.insertIntoWordProblemsQuestions(questionData.getQuestionList());*/

				SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl = new SchoolCurriculumLearnignCenterimpl(context);
				schooloCurriculumImpl.openConnection();
				if(CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH){
					//changes for dialog loading wheel
					schooloCurriculumImpl.deleteFromWordProblemCategoriesbyGrade(grade);
					schooloCurriculumImpl.deleteFromWordProblemsSubCategoriesByCategoryId(questionData.getCategoryList());
					schooloCurriculumImpl.deleteFromWordProblemsQuestionsCategoryId(questionData.getCategoryList());

					schooloCurriculumImpl.insertIntoWordProblemCategories(questionData.getCategoryList(), grade);
					schooloCurriculumImpl.insertIntoWordProblemsSubCategories(questionData.getCategoryList());
					schooloCurriculumImpl.insertIntoWordProblemsQuestions(questionData.getQuestionList());
				}else{
					SpanishChangesImpl implObj = new SpanishChangesImpl(context);
					implObj.openConn();

					implObj.deleteFromWordProblemCategoriesbyGrade(grade);
					implObj.deleteFromWordProblemsSubCategoriesByCategoryId(questionData.getCategoryList());
					implObj.deleteFromWordProblemsQuestionsCategoryId(questionData.getCategoryList());

					implObj.insertIntoWordProblemCategories(questionData.getCategoryList(), grade);
					implObj.insertIntoWordProblemsSubCategories(questionData.getCategoryList());
					implObj.insertIntoWordProblemsQuestions(questionData.getQuestionList());
					implObj.closeConn();
				}

				for(int i = 0 ; i < questionData.getQuestionList().size() ; i ++ ){

					WordProblemQuestionTransferObj questionObj = questionData.getQuestionList().get(i);

					if(questionObj.getQuestion().contains(".png"))
						imageNameList.add(questionObj.getQuestion());
					if(questionObj.getOpt1().contains(".png"))
						imageNameList.add(questionObj.getOpt1());
					if(questionObj.getOpt2().contains(".png"))
						imageNameList.add(questionObj.getOpt2());
					if(questionObj.getOpt3().contains(".png"))
						imageNameList.add(questionObj.getOpt3());
					if(questionObj.getOpt4().contains(".png"))
						imageNameList.add(questionObj.getOpt4());
					if(questionObj.getOpt5().contains(".png"))
						imageNameList.add(questionObj.getOpt5());
					if(questionObj.getOpt6().contains(".png"))
						imageNameList.add(questionObj.getOpt6());
					if(questionObj.getOpt7().contains(".png"))
						imageNameList.add(questionObj.getOpt7());
					if(questionObj.getOpt8().contains(".png"))
						imageNameList.add(questionObj.getOpt8());
					if(!questionObj.getImage().equals(""))
						imageNameList.add(questionObj.getImage());
				}

				if(!schooloCurriculumImpl.isImageTableExist()){
					schooloCurriculumImpl.createSchoolCurriculumImageTable();
				}
				schooloCurriculumImpl.closeConnection();
				//end changes
			}
			return questionData;
		}

		@Override
		protected void onPostExecute(QuestionLoadedTransferObj questionData) {

			pd.cancel();

			if(questionData != null){
				DawnloadImagesFromserver serverObj = new DawnloadImagesFromserver(imageNameList, context);
				Thread imageDawnLoadThrad = new Thread(serverObj);
				imageDawnLoadThrad.start();

				goOnPlayScreenForSchoolCurriculum();
			}else{
				CommonUtils.showInternetDialog(context);
			}

			super.onPostExecute(questionData);
		}
	}

	/**
	 * This class get word problem updated dates from server
	 * @author Yashwant Singh
	 *
	 */
	private class GetWordProblemQuestionUpdatedDate extends AsyncTask<Void, Void, ArrayList<UpdatedInfoTransferObj>>{
		private int grade;
		private ProgressDialog pd;
		private boolean isFisrtTimeCallForGrade;

		GetWordProblemQuestionUpdatedDate(int grade , boolean isFisrtTimeCallForGrade){
			this.grade = grade;
			this.isFisrtTimeCallForGrade = isFisrtTimeCallForGrade;
		}

		@Override
		protected void onPreExecute() {
			if(isFisrtTimeCallForGrade == false){
				pd = CommonUtils.getProgressDialog(MultiFriendzyRound.this);
				pd.show();
			}
			super.onPreExecute();
		}

		@Override
		protected ArrayList<UpdatedInfoTransferObj>  doInBackground(Void... params) {
			SingleFriendzyServerOperationForWordProblem serverObj = 
					new SingleFriendzyServerOperationForWordProblem();
			//changes for Spanish
			ArrayList<UpdatedInfoTransferObj>  updatedList = new ArrayList<UpdatedInfoTransferObj>();
			if(CommonUtils.getUserLanguageCode(MultiFriendzyRound.this) == CommonUtils.ENGLISH)
				updatedList = serverObj.getWordProbleQuestionUpdatedData();
			else//for Spanish
				updatedList = serverObj.getWordProbleQuestionUpdatedDateForSpanish(CommonUtils.SPANISH);
			return updatedList;
		}

		@Override
		protected void onPostExecute(ArrayList<UpdatedInfoTransferObj>  updatedList) {
			if(isFisrtTimeCallForGrade == false){
				pd.cancel();
			}

			if(updatedList != null){
				SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl = 
						new SchoolCurriculumLearnignCenterimpl(MultiFriendzyRound.this);
				schooloCurriculumImpl.openConnection();

				//changes for Spanish
				SpanishChangesImpl implObj = new SpanishChangesImpl(MultiFriendzyRound.this);
				implObj.openConn();

				if(isFisrtTimeCallForGrade){
					//changes for Spanish
					if(CommonUtils.getUserLanguageCode(MultiFriendzyRound.this) == CommonUtils.ENGLISH){
						if(!schooloCurriculumImpl.isWordProblemUpdateDetailsTableDataExist()){
							schooloCurriculumImpl.insertIntoWordProblemUpdateDetails(updatedList);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
						}else{
							String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
							schooloCurriculumImpl.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
						}
					}else{//for Spanish
						if(!implObj.isWordProblemUpdateDetailsTableDataExist()){
							implObj.insertIntoWordProblemUpdateDetails(updatedList);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
						}else{
							String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
							implObj.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
						}
					}
				}
				else{
					/*String updatedDateFromDatabase = schooloCurriculumImpl.getUpdatedDate(grade);
				if(CommonUtils.isUpdateDateChange(updatedDateFromDatabase , grade , updatedList))
				{
					String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
					schooloCurriculumImpl.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
					schooloCurriculumImpl.closeConnection();

					if(CommonUtils.isInternetConnectionAvailable(MultiFriendzyRound.this)){
						new GetWordProblemQuestion(grade , MultiFriendzyRound.this)
						.execute(null,null,null);
					}else{
						goOnPlayScreenForSchoolCurriculum();
					}
				}
				else{
					schooloCurriculumImpl.closeConnection();
					goOnPlayScreenForSchoolCurriculum();
				}*/

					if(CommonUtils.getUserLanguageCode(MultiFriendzyRound.this) == CommonUtils.ENGLISH){
						String updatedDateFromDatabase = schooloCurriculumImpl.getUpdatedDate(grade);
						if(CommonUtils.isUpdateDateChange(updatedDateFromDatabase , grade , updatedList))
						{
							String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
							schooloCurriculumImpl.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();

							if(CommonUtils.isInternetConnectionAvailable(MultiFriendzyRound.this)){
								new GetWordProblemQuestion(grade , MultiFriendzyRound.this)
								.execute(null,null,null);
							}else{
								goOnPlayScreenForSchoolCurriculum();
							}
						}
						else{
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
							goOnPlayScreenForSchoolCurriculum();
						}

					}else{//for Spanish
						String updatedDateFromDatabase = implObj.getUpdatedDate(grade);
						if(CommonUtils.isUpdateDateChange(updatedDateFromDatabase , grade , updatedList))
						{
							String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
							implObj.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();

							if(CommonUtils.isInternetConnectionAvailable(MultiFriendzyRound.this)){
								new GetWordProblemQuestion(grade , MultiFriendzyRound.this)
								.execute(null,null,null);
							}else{
								goOnPlayScreenForSchoolCurriculum();
							}
						}
						else{
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
							goOnPlayScreenForSchoolCurriculum();
						}
					}
				}
			}else{
				if(isFisrtTimeCallForGrade == false){
					CommonUtils.showInternetDialog(MultiFriendzyRound.this);
				}
			}
			super.onPostExecute(updatedList);
		}
	}


	//changes for Spanish
	/**
	 * This method call when user language is Spanish
	 * This method for downloading question from server for Spanish Language
	 */
	private void callWhenUserLanguageIsSpanish(Context context , int lang , int grade){
		try {
			SpanishChangesImpl spanishImplObj = new SpanishChangesImpl(context);
			spanishImplObj.openConn();
			boolean isSpanishQuestionLoaded = spanishImplObj.isQuestionLeaded(grade);
			if(isSpanishQuestionLoaded){
				if(CommonUtils.isInternetConnectionAvailable(context)){
					new GetWordProblemQuestionUpdatedDate(grade , false).execute(null,null,null);
				}else{
					this.goOnPlayScreenForSchoolCurriculum();
				}
			}
			else{
				if(CommonUtils.isInternetConnectionAvailable(context)){
					new GetWordProblemQuestion(grade ,context)
					.execute(null,null,null);
					new GetWordProblemQuestionUpdatedDate(grade , true).execute(null,null,null);
				}else{
					DialogGenerator dg = new DialogGenerator(context);
					Translation transeletion = new Translation(context);
					transeletion.openConnection();
					dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
					transeletion.closeConnection();	
				}
			}
			spanishImplObj.closeConn();

		} catch (Exception e) {
			Log.e(TAG, "inside callWhenUserLanguageIsSpanish " + e.toString());
		} 
	}
	
	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}

    private void setVisibilityOfLayoutBasedOnAppVersion() {
        if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS){
            layoutCheckBox.setVisibility(RelativeLayout.VISIBLE);
        }
    }

    //New InApp changes

    private Activity getCurrentActivityObj(){
        return this;
    }

    private void openToPurchaseLockCategory(){
        if(!CommonUtils.isInternetConnectionAvailable(this)){
            CommonUtils.showInternetDialog(this);
            return ;
        }

        MathFriendzyHelper.showProgressDialog(progressDialogForNewInApp);
        MathFriendzyHelper.getAppUnlockStatusFromServerAndSaveIntoLocal(this ,
                CommonUtils.getUserId(this) , false , new HttpResponseInterface() {
                    @Override
                    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                        if(requestCode == MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE){
                            MathFriendzyHelper.hideProgressDialog(progressDialogForNewInApp);
                            return ;
                        }
                        GetAppUnlockStatusResponse response =
                                (GetAppUnlockStatusResponse) httpResponseBase;
                        if(response.getResult()
                                .equalsIgnoreCase(MathFriendzyHelper.SUCCESS)
                                && response.getAppUnlockStatus() == MathFriendzyHelper.APP_UNLOCK){
                            MathFriendzyHelper.hideProgressDialog(progressDialogForNewInApp);
                            updateUIAfterPurchaseSuccess(true);
                            return ;
                        }

                        MathFriendzyHelper.getInAppStatusAndShowDialog(getCurrentActivityObj(),
                                MathFriendzyHelper.SHOW_UNLOCK_FOR_MULTI_FRIENDZY ,
                                new HttpResponseInterface() {//interface call when save true from the server otherwise open dialog
                                    @Override
                                    public void serverResponse(HttpResponseBase httpResponseBase,
                                                               int requestCode) {
                                        MathFriendzyHelper.hideProgressDialog(progressDialogForNewInApp);
                                        if(requestCode ==
                                                MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE){
                                            return ;
                                        }
                                        updateUIAfterPurchaseSuccess(true);
                                    }
                                } , false , response , new OnPurchaseDone() {
                                    @Override
                                    public void onPurchaseDone(boolean isDone) {
                                        if(isDone) {
                                            updateUIAfterPurchaseSuccess(true);
                                        }
                                    }
                                });
                    }
                });
    }

    private void updateUIAfterPurchaseSuccess(boolean isPerformClick){
        appUnlockStatus = true;
        if(!this.isCkeckStatusFromOnCreate){
            if(isPerformClick) {
                this.setListenerOnbtngoToFriendzy();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            switch (requestCode){
                case GetMoreCoins.START_GET_MORE_COIN_ACTIVITY_REQUEST:
                    this.updateUIAfterPurchaseSuccess(false);
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
