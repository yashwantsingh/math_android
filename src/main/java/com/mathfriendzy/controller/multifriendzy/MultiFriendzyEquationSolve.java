package com.mathfriendzy.controller.multifriendzy;

import static com.mathfriendzy.controller.learningcenter.IOperationId.ADDITION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.DECIMAL_ADDITION_SUBTRACTION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.DECIMAL_MULTIPLICATION_DIVISION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.DIVISION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.FRACTION_ADDITION_SUBTRACTION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.FRACTION_MULTIPLICATION_DIVISION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.MULTIPLICATION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.NEGATIVE_ADDITION_SUBTRACTION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.NEGATIVE_MULTIPLICATION_DIVISION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.SUBTRACTION;
import static com.mathfriendzy.utils.ICommonUtils.DEVICE_ID;
import static com.mathfriendzy.utils.ICommonUtils.DEVICE_ID_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.LEARNING_CENTER_BG_INFO;
import static com.mathfriendzy.utils.ICommonUtils.LEARNING_CENTER_EQUATION_SOLVE_WITH_TIMER;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;
import static com.mathfriendzy.utils.ICommonUtils.SINGLE_FRIENDZY_EQUATION_SOLVE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.learningcenter.LearningCenterEquationSolve;
import com.mathfriendzy.controller.learningcenter.OnAnswerGivenComplete;
import com.mathfriendzy.controller.learningcenter.OnReverseDialogClose;
import com.mathfriendzy.controller.learningcenter.workarea.WorkAreaActivity;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.multifriendzy.findbyuser.SelectedPlayerActivity;
import com.mathfriendzy.gcm.ProcessNotification;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.friendzy.SaveTimePointsForFriendzyChallenge;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.MathEquationOperationCategorytransferObj;
import com.mathfriendzy.model.learningcenter.MathResultTransferObj;
import com.mathfriendzy.model.learningcenter.MathScoreTransferObj;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.learningcenter.SeeAnswerTransferObj;
import com.mathfriendzy.model.multifriendzy.CreateMultiFriendzyDto;
import com.mathfriendzy.model.multifriendzy.MathFriendzysRoundDTO;
import com.mathfriendzy.model.multifriendzy.MultiFriendzyImpl;
import com.mathfriendzy.model.multifriendzy.MultiFriendzyServerOperation;
import com.mathfriendzy.model.player.base.Player;
import com.mathfriendzy.model.registration.Register;
import com.mathfriendzy.model.singleFriendzy.EquationFromServerTransferObj;
import com.mathfriendzy.model.singleFriendzy.SingleFriendzyEquationObj;
import com.mathfriendzy.model.singleFriendzy.SingleFriendzyImpl;
import com.mathfriendzy.notification.GetAndroidDevicePidForUser;
import com.mathfriendzy.notification.SendNotificationTransferObj;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ICommonUtils;
import com.mathfriendzy.utils.PlaySound;

/**
 * This Activity open when user want to play
 * @author Yashwant Singh
 *
 */
public class MultiFriendzyEquationSolve extends LearningCenterEquationSolve 
{
	private TextView txtUserName1 		= null;
	private TextView txtLap1        	= null;
	private TextView txtPoints1     	= null;
	private ImageView imgFlag1      	= null;
	private ProgressBar layoutProgress1 = null;

	private TextView txtUserName2 		= null;
	private TextView txtLap2        	= null;
	private TextView txtPoints2     	= null;
	private ImageView imgFlag2     		= null;
	private ProgressBar layoutProgress2	= null;
	private LinearLayout layoutName1    = null;

	private TextView txtTimer 			= null;

	private ImageView getReady = null;

	private long START_TIME_FOR_GET_READY = 7 * 1000;

	private final String TAG = this.getClass().getSimpleName();

	private long startTimerForRoughArea = 0;
	private CountDownTimer timer 		= null;
	private long  START_TIME 			= 180 * 1000;//start time for timer
	//private long  START_TIME 			= 30 * 1000;//start time for timer
	private final long END_TIME   		= 1 * 1000;//end time for timer

	private int progress   = 0;
	private int progress1  = 0;
	private Date startTime = null;
	private Date endTime   = null;

	private int startTimeValue = 0;//this variable contains the number of seconds of starttime
	private int endTimeVlaue   = 0;//this variable contain the number of seconds of endtime

	private LearningCenterTransferObj learnignCenterobj		= null;
	private Player  opponentData		= null;

	private ArrayList<MathEquationOperationCategorytransferObj> mathOperationList  		= null;
	private ArrayList<MathEquationOperationCategorytransferObj> newMathOperationList  	= null;

	private String resultFromDatabase 			= "";
	private ArrayList<String> resultDigitsList  = null;

	private ArrayList<Integer> onebyoneDigitList 	= null;
	private ArrayList<Integer> twobyoneDigitList 	= null;
	private ArrayList<Integer> twobytwoDigitList 	= null;
	private ArrayList<Integer> threebytwoDigitList 	= null;
	private ArrayList<Integer> threebythreeDigitList= null;
	private ArrayList<Integer> onebytwoDigitList    = null;

	//variable which hold the data to transfer to the answer screen
	private ArrayList<LearningCenterTransferObj> playEquationList 	= null;
	private ArrayList<String> userAnswerList 						= null;

	private boolean isClickOnGo 	= false;
	private String userAnswer 		= "";
	//private PlaySound playsound  = null;
	private int lap     = 1;
	private int lap1    = 1;
	private int rightAnsCounter   = 0;
	private int isCorrectAnsByUser= 0;
	private int showEquationIndex = 0;//this index hold the sequence for shoe equation
	private final int SLEEP_TIME        = 2000; //for delay time for displaying result after 3 attempts wrong
	private int points = 0;
	private int pointsForEachQuestion = 0;
	private int secondPlayerpoints = 0;
	//private boolean isStopSecondPlayerProgress = false;

	private int numberOfCoins     = 0;
	//private float coinsPerPoint   = 0.05f;
	private boolean isClickOnBackPressed = false;
	private ArrayList<MathScoreTransferObj> playDatalist 	= null;//this list hold the equation time for solving equation
	public static int completeLevel = 0;//level user play at current which is set in Single FriendzyMain
	private int equationId = 0;
	private int isWin = 0;
	private ArrayList<EquationFromServerTransferObj> equationList = null;//hold the equation data from server
	private ArrayList<LearningCenterTransferObj> equationListFromDataBase = null;//hold the equation data from server

	//private final int EQUAITON_TYPE = 11;
	private String gameType = "Play";

	public static SeeAnswerTransferObj seeAnswerDataObj = null;

	//set data save on serevr
	private String friendzyId 	= "-1";
	private int turn       		= 0;
	private String winnerId		= "-1";
	private int roundScore 		= 0;
	private int shouldSaveEquation = 0;
	private boolean isEquationFromserver = false;
	private String problems 	= "";
	private String playFriendzyDate = null;
	ArrayList<MathFriendzysRoundDTO> updatedRoundList = null;

	private final String THEIR_FRENDZY_TEXT = "their friendzy";
	private final String YOUR_FRIENDZY_TEXT = "your friendzy";
	private final String HISTORY_TEXT       = "";
	private final String MESSAGE_TEXT_FOR_NOTIFICATION = " has sent a Math Friendzy to ";
	CountDownTimer getReadyTimer = null;
	private boolean isNotificationSend = false;

	private boolean vesrionGreaterThan = true;

	//added by shilpi for friendzy 
	private long totalTimeTaken = 0l;

	//changes for wrong timer , on 21 Mar
	private int totalTime  = 0 ;
	//end changes

	//for sound
	private boolean isFromOnCreate = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_multi_friendzy_equation_solve);

		//for sound
		isFromOnCreate = true;

		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "inside onCreate()");

		//changes for version
		if (android.os.Build.VERSION.RELEASE.startsWith("1.") ||
				android.os.Build.VERSION.RELEASE.startsWith("2.0") ||
				android.os.Build.VERSION.RELEASE.startsWith("2.1") ||
				android.os.Build.VERSION.RELEASE.startsWith("2.2")){
			vesrionGreaterThan = false;
		}
		//end changes

		/*DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);  

		boolean tabletSize = getResources().getBoolean(R.bool.isTablet);

		if(tabletSize)
		{
			//Log.e(TAG, "Tab");

			isTab = true;
			if(metrics.heightPixels <= TAB_HEIGHT_1024)
			{
				//Log.e(TAG, "Tab1");
				maxWidth = 80;
				MAX_WIDTH_FOR_FRACTION_TEXT = 35;
			  	widthForFractionLayout = 80;
				max_width_for_layout_maultiplication = 80;
			}
			else
			{
				if (metrics.densityDpi > 160){
					//Log.e(TAG, "Tab2");
					maxWidth = 110;
					MAX_WIDTH_FOR_FRACTION_TEXT = 50;
				  	widthForFractionLayout = 110;
				  	max_width_for_layout_maultiplication = 110;//changes for tab
				}
				else{
					//Log.e(TAG, "Tab3");
					maxWidth = 80;
					MAX_WIDTH_FOR_FRACTION_TEXT = 35;
				  	widthForFractionLayout = 80;
					max_width_for_layout_maultiplication = 80;
				}
			}
			//changes for tab
			//Log.e(TAG, " max width " + maxWidth);
		}
		else{
			if ((getResources().getConfiguration().screenLayout &
					Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
					metrics.densityDpi > SCREEN_DENISITY)
			{
				maxWidth = 100;
				MAX_WIDTH_FOR_FRACTION_TEXT = 50;
				widthForFractionLayout = 90;
				max_width_for_layout_maultiplication = 80;
			}
			else
			{		
				if ((getResources().getConfiguration().screenLayout &
					    Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
					  metrics.densityDpi > 160
					  &&
					  (getResources().getConfiguration().screenLayout &
							    Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
							  metrics.densityDpi <= SCREEN_DENISITY)
				{
					maxWidth = 75;
					MAX_WIDTH_FOR_FRACTION_TEXT = 38;
					widthForFractionLayout = 67;
					max_width_for_layout_maultiplication = 60;
				  }
				else
				{
					maxWidth = 50;
				  	MAX_WIDTH_FOR_FRACTION_TEXT = 25;
				  	widthForFractionLayout = 45;
				  	max_width_for_layout_maultiplication = 40;
				}
			}
		}		
		 */

		playsound 		   = new PlaySound(this);
		playDatalist       = new ArrayList<MathScoreTransferObj>();
		resultTextViewList = new ArrayList<TextView>();
		//playDatalist       = new ArrayList<MathScoreTransferObj>();
		learnignCenterobj  = new LearningCenterTransferObj();

		//Array List Which contain the category id for 1*1,1*2 or etc digits
		onebyoneDigitList 		= new ArrayList<Integer>();
		twobyoneDigitList 		= new ArrayList<Integer>();
		twobytwoDigitList 		= new ArrayList<Integer>();
		threebytwoDigitList 	= new ArrayList<Integer>();
		threebythreeDigitList 	= new ArrayList<Integer>();
		onebytwoDigitList       = new ArrayList<Integer>();

		//for transfer info to the answer screen

		playEquationList = new ArrayList<LearningCenterTransferObj>();
		//equationList     = new ArrayList<EquationFromServerTransferObj>();
		userAnswerList   = new ArrayList<String>();

		seeAnswerDataObj = new SeeAnswerTransferObj();

		//for question UI , Issues list 5 march 2014 , issues no. 1
		this.initilizeHeightAndWidthForDynamicLayoutCreation();
		//end changes

		this.setWidgetsReferences();
		this.setWidgetsReferencesForMultifriendzy();
		this.setWidgetsValuesFromtranselation();
		this.setTextFromeTranslationForMultiFriendzy();
		this.getIntentValue();
		this.setPlayerDetail();
		this.setlistenerOnWidgets();
		/*this.getEquations();*/

		this.keyBoardDisable();//disable kayboard at the starting 
		btnRoughWork.setClickable(false);

		if(ProcessNotification.isFromNotification)
		{
			ProcessNotification.isFromNotification = false;

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder
			.setTitle("Math Friendzy")
			.setMessage(ProcessNotification.sentNotificationData.getShareMessage())
			.setIcon(R.drawable.ic_launcher)
			.setPositiveButton("Ok", new DialogInterface.OnClickListener() 
			{
				public void onClick(DialogInterface dialog, int which) 
				{	
					getReadyTimer();
				}
			}).show();
		}
		else
		{
			this.getReadyTimer();
		}

		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "outside onCreate()");
	}



	/*
	 * This method set translation text from translation table
	 */
	private void setTextFromeTranslationForMultiFriendzy() 
	{	
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		txtPoints1.setText(transeletion.getTranselationTextByTextIdentifier("mfLblPts") + ": " + points);
		txtLap1.setText(transeletion.getTranselationTextByTextIdentifier("mfLblLap") + ": " + lap);
		txtPoints2.setText(transeletion.getTranselationTextByTextIdentifier("mfLblPts") + ": " + secondPlayerpoints);
		txtLap2.setText(transeletion.getTranselationTextByTextIdentifier("mfLblLap") + ": " + lap1);
		transeletion.closeConnection();
	}

	/**
	 * This method hide the content which is not to be display
	 */
	private void setVisibleOrInvisibleContent() 
	{			
		layoutName1.setVisibility(LinearLayout.GONE);
		imgFlag2.setVisibility(ImageView.GONE);
		layoutProgress2.setVisibility(ProgressBar.GONE);
	}

	/**
	 * This method set the widgets references from the layout to the reference object
	 */
	private void setWidgetsReferencesForMultifriendzy() 
	{
		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "inside setWidgetsReferences()");


		txtUserName1    = (TextView) findViewById(R.id.txtUserName);
		txtLap1			= (TextView) findViewById(R.id.txtLap);
		txtPoints1 		= (TextView) findViewById(R.id.txtPoints);
		imgFlag1		= (ImageView) findViewById(R.id.imgFlag);
		layoutProgress1	= (ProgressBar) findViewById(R.id.layoutProgress);

		txtUserName2    = (TextView) findViewById(R.id.txtUserName1);
		txtLap2 		= (TextView) findViewById(R.id.txtLap1);
		txtPoints2 		= (TextView) findViewById(R.id.txtPoints1);
		imgFlag2		= (ImageView) findViewById(R.id.imgFlag1);
		layoutProgress2	= (ProgressBar) findViewById(R.id.layoutProgress1);

		txtTimer 		= (TextView) findViewById(R.id.txtTimer);
		getReady = (ImageView) findViewById(R.id.getReady);

		layoutName1    = (LinearLayout)   findViewById(R.id.layoutName1);

		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "outside setWidgetsReferences()");

	}

	/**
	 * This method change the image on each seconds for get ready
	 */
	private void getReadyTimer()
	{
		getReadyTimer = new CountDownTimer(START_TIME_FOR_GET_READY,1) 
		{
			@Override
			public void onTick(long millisUntilFinished) 
			{
				int value = (int) ((millisUntilFinished/1000) % 60) ;
				setBacKGroundForGetReadyImage(value);
			}

			@Override
			public void onFinish() 
			{
				getReady.setVisibility(ImageView.INVISIBLE);
				txtTimer.setVisibility(TextView.VISIBLE);

				timer = new MyTimer(START_TIME, END_TIME);
				timer.start();

				playsound.playSoundForSingleFriendzyEquationSolve(MultiFriendzyEquationSolve.this);

				keyBoardEnable();
                btnRoughWork.setClickable(true);

				if(equationList != null)
				{
					showFirstEquation();
				}
				else
				{
					getEquationFromLocalDatabase();
					showFirstEquation();
				}
				//showFirstEquation();
			}
		};
		getReadyTimer.start();
	}


	/**
	 * This method show the first Equation and start to set second player progress
	 */
	private void showFirstEquation()
	{
		//Log.e(TAG, "inside showFirstEquation ");

		if(equationList != null)
			setSecondPlayerProgress();

		LearningCenterimpl learnignCenterImpl = new LearningCenterimpl(MultiFriendzyEquationSolve.this);
		learnignCenterImpl.openConn();
		showEqautionData(learnignCenterImpl.getEquation(equationsIdList.get(showEquationIndex)));
		learnignCenterImpl.closeConn();
	}


	/**
	 * This class show the timer for player
	 * @author Yashwant Singh
	 *
	 */
	private class MyTimer extends CountDownTimer
	{
		public MyTimer(long millisInFuture, long countDownInterval) 
		{
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onTick(long millisUntilFinished) 
		{		
			startTimerForRoughArea = millisUntilFinished;

			if(((millisUntilFinished/1000) % 60) < 10) // Calculate the second if it less then 10 then add 0 prefix
				txtTimer.setText(((millisUntilFinished/1000) / 60) + ":0" + ((millisUntilFinished/1000) % 60));
			else
				txtTimer.setText(((millisUntilFinished/1000) / 60) + ":" + ((millisUntilFinished/1000) % 60));
		}

		@Override
		public void onFinish() 
		{
			//Log.e("", "Stop");
			playsound.stopPlayer();
			isStopSecondPlayerProgress = true;

			updatedRoundList = new ArrayList<MathFriendzysRoundDTO>();


			String opponentPlayerId = null;
			String opponentUserId   = null;

			if(opponentData != null)//data from searched user
			{
				opponentPlayerId = opponentData.getPlayerId() + "";
				opponentUserId   = opponentData.getParentUserId() + "";
			}
			else //  data from click on your turn
			{
				opponentPlayerId = MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getPlayerId();
				opponentUserId   = MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getParentUserId();
			}

			//if mathfriendzy start by my player
			if(MultiFriendzyMain.isNewFriendzyStart)
			{
				MathFriendzysRoundDTO roundDto = new MathFriendzysRoundDTO();
				roundDto.setPlayerScore(points + "");
				roundDto.setOppScore("");
				roundDto.setRoundNumber("1");

				updatedRoundList.add(roundDto);

				turn = 1 ; // for their turn
			}
			else
			{
				MathFriendzysRoundDTO roundDto = MultiFriendzyRound.roundList.get
						(MultiFriendzyRound.roundList.size() - 1);//get last round record

				if(roundDto.getPlayerScore().equals(""))
				{
					roundDto.setPlayerScore(points + "");

					for(int i = 0 ; i < MultiFriendzyRound.roundList.size() - 1 ; i ++ )
					{
						updatedRoundList.add(MultiFriendzyRound.roundList.get(i));
					}

					updatedRoundList.add(roundDto);
				}
				else
				{
					MathFriendzysRoundDTO roundDto1 = new MathFriendzysRoundDTO();
					roundDto1.setPlayerScore(points + "");
					roundDto1.setOppScore("");
					roundDto1.setRoundNumber((MultiFriendzyRound.roundList.size() + 1) + "");

					for(int i = 0 ; i < MultiFriendzyRound.roundList.size(); i ++ )
					{
						updatedRoundList.add(MultiFriendzyRound.roundList.get(i));
					}

					updatedRoundList.add(roundDto1);
				}

				/*for(int i = 0 ; i < MultiFriendzyRound.roundList.size() - 1 ; i ++ )
				{
					updatedRoundList.add(MultiFriendzyRound.roundList.get(i));
				}

				updatedRoundList.add(roundDto);*/
			}

			if(updatedRoundList.size() > 1)
			{
				MathFriendzysRoundDTO roundDto = updatedRoundList.get(updatedRoundList.size() - 1);

				if((!roundDto.getPlayerScore().equals("")) && (!roundDto.getOppScore().equals("")))
				{
					turn = 0 ;// for your turn
				} 
				else if((!roundDto.getPlayerScore().equals("")) && (roundDto.getOppScore().equals("")))
				{
					turn = 1 ;// for their turn
				}
			}

			if(MultiFriendzyMain.isClickOnYourTurn && isEquationFromserver)
			{
				//Log.e(TAG, "inside click on your turn for notification");

				if(updatedRoundList.size() == 3)
				{
					int playerTotalPoints   = 0;
					int opponentTotalPoints = 0;

					for ( int i = 0 ; i < updatedRoundList.size() ; i ++ )
					{
						if(!updatedRoundList.get(i).getPlayerScore().equals(""))
							playerTotalPoints = playerTotalPoints + Integer.parseInt(
									updatedRoundList.get(i).getPlayerScore());

						if(!updatedRoundList.get(i).getOppScore().equals(""))
							opponentTotalPoints = opponentTotalPoints + Integer.parseInt
							(updatedRoundList.get(i).getOppScore());
					}

					if(playerTotalPoints > opponentTotalPoints)
					{
						SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
						String playerId = sharedPreffPlayerInfo.getString("playerId", "");

						winnerId = playerId;
						isNotificationSend = true;
						turn = 2;//for history
					}	
					else if(playerTotalPoints == opponentTotalPoints)
					{
						winnerId = opponentPlayerId;
						isNotificationSend = true;
						turn = 2;//for history
					}
					else
					{
						points = points - 10;
						winnerId = opponentPlayerId;
						isNotificationSend = true;
						turn = 2;//for history
					}
				}

				if(playDatalist.size() > 0)
				{
					roundScore = points;
					shouldSaveEquation = 0;

					//changs for correct time on result screen , on 07 Mar 2014
					totalTime = getNewMathPlayedData(playDatalist);
					//end changes

					problems = getEquationSolveXml(playDatalist);
				}
				else
				{
					roundScore = 0;
					shouldSaveEquation = 0;
				}
			}
			else
			{
				if(isEquationFromserver == false)
				{
					roundScore = points;

					//changs for correct time on result screen , on 07 Mar 2014
					totalTime = getNewMathPlayedData(playDatalist);
					//end changes

					problems = getEquationSolveXml(playDatalist) + getEquationUnSolveXml();
					shouldSaveEquation = 1;
				}
			}

			//playFriendzyDate = CommonUtils.formateDateIn24Hours(new Date());

			//set time zone changes
			playFriendzyDate = CommonUtils.formateDateIn24Hours(CommonUtils.setTimeZoneWithDate(new Date()));
			//end time zone changes

			SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
			String userId = sharedPreffPlayerInfo.getString("userId", "");
			String playerId = sharedPreffPlayerInfo.getString("playerId", "");

			updatePlayerTotalPointsTable(playerId , userId);

			LearningCenterimpl learningCenterimpl = new LearningCenterimpl(MultiFriendzyEquationSolve.this);
			learningCenterimpl.openConn();
			PlayerTotalPointsObj playerPoints =  learningCenterimpl.getDataFromPlayerTotalPoints(playerId);
			learningCenterimpl.closeConn();

			CreateMultiFriendzyDto createMultiFrinedzyDto = new CreateMultiFriendzyDto();

			createMultiFrinedzyDto.setProblems(problems);
			createMultiFrinedzyDto.setFriendzyId(friendzyId);
			createMultiFrinedzyDto.setPlayerUserId(userId);
			createMultiFrinedzyDto.setPlayerId(playerId);

			createMultiFrinedzyDto.setOpponentPlayerId(opponentPlayerId);
			createMultiFrinedzyDto.setOpponentUserId(opponentUserId);

			createMultiFrinedzyDto.setRoundScore(roundScore);
			createMultiFrinedzyDto.setPoints(playerPoints.getTotalPoints());
			createMultiFrinedzyDto.setCoins(playerPoints.getCoins());
			createMultiFrinedzyDto.setDate(playFriendzyDate);

			boolean isAddParameter = false;//this is for add below parameter to the url or not for hit api

			if(!winnerId.equals("-1"))
			{
				isAddParameter = true;
				createMultiFrinedzyDto.setModifying(1);
				createMultiFrinedzyDto.setIsCompleted(1);
				createMultiFrinedzyDto.setWinner(winnerId);
			}
			else if(!friendzyId.equals("-1"))
			{
				isAddParameter = true;
				createMultiFrinedzyDto.setModifying(1);
				createMultiFrinedzyDto.setIsCompleted(0);
				createMultiFrinedzyDto.setWinner("-1");
			}

			createMultiFrinedzyDto.setShouldSaveEquations(shouldSaveEquation);

			MultiFriendzyMain.isNewFriendzyStart = false;

			savePlayDataOnServer(createMultiFrinedzyDto , isAddParameter);
			savePlayerDataWhenNoInternetConnected();
		}
	}

	//changes for update player points in local when Internet is not connected
	//updated on 26 11 2014 
	private void savePlayerDataWhenNoInternetConnected(){
		try{		
			SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
			String userId;
			String playerId;
			if(sharedPreffPlayerInfo.getString("userId", "").equals("0"))
				userId = "0";
			else
				userId = sharedPreffPlayerInfo.getString("userId", "");

			if(sharedPreffPlayerInfo.getString("playerId", "").equals("0"))
				playerId = "0";
			else
				playerId = sharedPreffPlayerInfo.getString("playerId", "");

			MathResultTransferObj mathResultObj = new MathResultTransferObj();
			mathResultObj.setUserId(userId);
			mathResultObj.setPlayerId(playerId);
			mathResultObj.setTotalScore(points);
			mathResultObj.setCoins((int) (points * ICommonUtils.COINS_PER_POINT));
			mathResultObj.setLevel(MathFriendzyHelper.getPlayerCompleteLavel(playerId , this));
			MathFriendzyHelper.insertPlayerPlayedDataIntoLocalEarnedScore(mathResultObj, this);
		}catch(Exception e){
			e.printStackTrace();
		}
	}//end updation on 26 11 2014

	/**
	 * This method save play multifriendzy data on server
	 */
	private void savePlayDataOnServer(CreateMultiFriendzyDto createMultiFrinedzyDto , boolean isAddParameter)
	{
		if(CommonUtils.isInternetConnectionAvailable(this))
		{
			new CreateMultiFriendzy(createMultiFrinedzyDto , isAddParameter).execute(null,null,null);

			//added by shilpi for friendzy
			if(CommonUtils.isActivePlayer){
				/*new SaveTimePointsForFriendzyChallenge(totalTimeTaken, points, this)
				.execute(null, null, null);*/
				new SaveTimePointsForFriendzyChallenge(totalTime, points, this)
				.execute(null, null, null);
			}
		}
		else
		{
			DialogGenerator dg = new DialogGenerator(this);
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
			transeletion.closeConnection();
		}
	}


	/**
	 * This method send notification to the devices 
	 * This method call after the data save on server aftre completion of 2 minuts
	 */
	private void sendNotification(CreateMultiFriendzyDto createMultiFrinedzyDto)
	{
		String notificationRecoiverDeviceId = "" ;//this variable contain the device ids to which notification is to be send
		String notificationIponDevices = "";

		String opponentName = null;

		if(opponentData != null)//data from searched user
		{
			opponentName = opponentData.getFirstName() + " " + opponentData.getLastName().charAt(0) + ".";
			notificationRecoiverDeviceId = opponentData.getAndroidPids();
			notificationIponDevices      = opponentData.getIponPids();
		}
		else //  data from click on your turn
		{
			String lastName = "";
			if(MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getlName().length() > 0 )
				lastName = MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getlName().charAt(0) + "";

			opponentName = MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getfName()
					+ " " + lastName + ".";

			notificationRecoiverDeviceId = MultiFriendzyRound.multiFriendzyServerDto.getAndroidPids();
			notificationIponDevices      = MultiFriendzyRound.multiFriendzyServerDto.getNotificationDevices();
		}

		SharedPreferences sharedPreff = getSharedPreferences(DEVICE_ID_PREFF, 0);
		String deviceId = sharedPreff.getString(DEVICE_ID, "");

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		Country country = new Country();
		String coutryIso = country.getCountryIsoByCountryName(sharedPreffPlayerInfo.getString("countryName", ""), this);
		String imageName = sharedPreffPlayerInfo.getString("imageName",null);
		//String playerName= sharedPreffPlayerInfo.getString("playerName", "");//changes for first character of last name
		String fullName = sharedPreffPlayerInfo.getString("playerName", "");
		String playerName = fullName.substring(0,(fullName.indexOf(" ") + 2)) + ".";

		String message   = playerName + MESSAGE_TEXT_FOR_NOTIFICATION + opponentName + "!";

		SendNotificationTransferObj sendNotificationObj = new SendNotificationTransferObj();
		sendNotificationObj.setDevicePid(notificationRecoiverDeviceId);//Receiver device ids
		sendNotificationObj.setIphoneDeviceIds(notificationIponDevices);//reciever devices
		sendNotificationObj.setMessage(message);//change
		sendNotificationObj.setUserId(createMultiFrinedzyDto.getPlayerUserId());
		sendNotificationObj.setPlayerId(createMultiFrinedzyDto.getPlayerId());
		sendNotificationObj.setCountry(coutryIso);
		sendNotificationObj.setOppUserId(createMultiFrinedzyDto.getOpponentUserId());
		sendNotificationObj.setOppPlayerId(createMultiFrinedzyDto.getOpponentPlayerId());
		sendNotificationObj.setFriendzyId(createMultiFrinedzyDto.getFriendzyId());
		sendNotificationObj.setProfileImageId(imageName);
		sendNotificationObj.setShareMessage(MultiFriendzyRound.notificationMessage);//changes
		sendNotificationObj.setSenderDeviceId(deviceId);//sender device ids (udid set after get from server)
		//changes for word problem
		sendNotificationObj.setForWord(0);

		//get android pid from server after that sent notification
		new GetAndroidDevicePidForUser(createMultiFrinedzyDto.getPlayerUserId(), deviceId , sendNotificationObj)
		.execute(null,null,null);

		// the asynckTask send notification to the devices
		//new SendNotification(sendNotificationObj).execute(null,null,null);		
	}

	/**
	 * Update player total points table
	 * @param playerId
	 */
	private void updatePlayerTotalPointsTable(String playerId , String userId)
	{
		numberOfCoins = (int) (points * ICommonUtils.COINS_PER_POINT);

		LearningCenterimpl learningCenterimpl = new LearningCenterimpl(MultiFriendzyEquationSolve.this);
		learningCenterimpl.openConn();
		PlayerTotalPointsObj playerPoints =  learningCenterimpl.getDataFromPlayerTotalPoints(playerId);

		playerPoints.setTotalPoints(playerPoints.getTotalPoints() + points);
		playerPoints.setCoins(playerPoints.getCoins() + numberOfCoins);
		playerPoints.setCompleteLevel(playerPoints.getCompleteLevel());
		playerPoints.setUserId(userId);
		playerPoints.setPlayerId(playerId);

		learningCenterimpl.deleteFromPlayerTotalPoints(playerId);
		learningCenterimpl.insertIntoPlayerTotalPoints(playerPoints);

		learningCenterimpl.closeConn();
	}

	/**
	 * This method set the Get Ready Image back ground
	 * @param value
	 */
	private void setBacKGroundForGetReadyImage(int value)
	{
		switch(value)
		{
		case 6 : 
			getReady.setBackgroundResource(R.drawable.mcg_get_ready);
			break;
		case 5 : 
			getReady.setBackgroundResource(R.drawable.mcg_5);
			break;
		case 4 : 
			getReady.setBackgroundResource(R.drawable.mcg_4);
			break;
		case 3 : 
			getReady.setBackgroundResource(R.drawable.mcg_3);
			break;
		case 2 : 
			getReady.setBackgroundResource(R.drawable.mcg_2);
			break;
		case 1 : 
			getReady.setBackgroundResource(R.drawable.mcg_1);
			break;
		case 0 : 
			getReady.setBackgroundResource(R.drawable.mcg_go);
			break;
		}
	}

	@SuppressWarnings("deprecation")
	private void setPlayerDetail() 
	{
		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "inside setPlayerDetail()");

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);

		//completeLevel = sharedPreffPlayerInfo.getInt("completeLevel", 0);//get played complete level

		//txtUserName1.setText(sharedPreffPlayerInfo.getString("playerName", ""));

		String fullName = sharedPreffPlayerInfo.getString("playerName", "");
		String playerName = fullName.substring(0,(fullName.indexOf(" ") + 2));
		//txtPlayerName.setText(sharedPreffPlayerInfo.getString("playerName", ""));//change for last name first character
		txtUserName1.setText(playerName + ".");

		Country country = new Country();
		String coutryIso = country.getCountryIsoByCountryName(sharedPreffPlayerInfo.getString("countryName", ""), this);

		try 
		{
			if(!(coutryIso.equals("-")))
				imgFlag1.setBackgroundDrawable(Drawable.createFromStream(getAssets().open(getResources().
						getString(R.string.countryImageFolder) +"/" 
						+ coutryIso + ".png"), null));
		} 
		catch (IOException e) 
		{
			Log.e(TAG, "Inside set player detail Error : No Image Found");
			//e.printStackTrace();
		}

		//set second user detail
		if(MultiFriendzyRound.multiFriendzyServerDto != null)
		{
			/*txtUserName2.setText(MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getfName() + " " 
					+ MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getlName() + ".");*/

			txtUserName2.setText(MultiFriendzyRound.oppenentPlayerName + ".");

			try 
			{
				if(!(coutryIso.equals("-")))
					imgFlag2.setBackgroundDrawable(Drawable.createFromStream(getAssets().open(getResources().
							getString(R.string.countryImageFolder) +"/" 
							+ MultiFriendzyRound.multiFriendzyServerDto.getCountryCode() + ".png"), null));
			} 
			catch (IOException e) 
			{	
				Log.e(TAG, "Inside set player detail Error : No Image Found");
				//e.printStackTrace();
			}
		}

		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "outside setPlayerDetail()");
	}


	/**
	 * This method set the listener on widgets
	 */
	private void setlistenerOnWidgets() 
	{
		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "inside setlistenerOnWidgets()");

		if(txtCarry != null)
			txtCarry.setOnClickListener(this);

		btnDot.setOnClickListener(this);
		btnMinus.setOnClickListener(this);
		btn1.setOnClickListener(this);
		btn2.setOnClickListener(this);
		btn3.setOnClickListener(this);
		btn4.setOnClickListener(this);
		btn5.setOnClickListener(this);
		btn6.setOnClickListener(this);
		btn7.setOnClickListener(this);
		btn8.setOnClickListener(this);
		btn9.setOnClickListener(this);
		btn0.setOnClickListener(this);
		btnArrowBack.setOnClickListener(this);

		btnGo.setOnClickListener(this);
		btnRoughWork.setOnClickListener(this);

		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "outside setlistenerOnWidgets()");

	}


	/**
	 * This method get Category Id List From Database and set to the arrayList
	 * like 1*1 digit,2*1 and etc
	 */
	private void getCategoriyIdListByDigits()
	{
		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();
		onebyoneDigitList 	= learningCenterObj.getDigitType("1 digit%1 digit");
		twobyoneDigitList 	= learningCenterObj.getDigitType("2 digits%1 digit");
		twobytwoDigitList 	= learningCenterObj.getDigitType("2 digits%2 digits");
		threebytwoDigitList = learningCenterObj.getDigitType("3 digits%2 digits");
		threebythreeDigitList= learningCenterObj.getDigitType("3 digits%3 digits");
		onebytwoDigitList   = learningCenterObj.getDigitType("1 digit%2 digits");

		for( int i = 0 ; i < onebytwoDigitList.size() ; i ++ )
		{
			twobyoneDigitList.add(onebytwoDigitList.get(i));
		}

		learningCenterObj.closeConn();

		this.setSequaence();
	}


	/**
	 * This method set the sequence for displaying equations
	 */
	private void setSequaence()
	{
		ArrayList<MathEquationOperationCategorytransferObj> oneDByOneDlist = new ArrayList<MathEquationOperationCategorytransferObj>();
		ArrayList<MathEquationOperationCategorytransferObj> twoDByOneDlist = new ArrayList<MathEquationOperationCategorytransferObj>();
		ArrayList<MathEquationOperationCategorytransferObj> twoDByTwoDlist = new ArrayList<MathEquationOperationCategorytransferObj>();
		ArrayList<MathEquationOperationCategorytransferObj> threeDByTwoDlist = new ArrayList<MathEquationOperationCategorytransferObj>();
		ArrayList<MathEquationOperationCategorytransferObj> threeDByThreeDlist = new ArrayList<MathEquationOperationCategorytransferObj>();

		newMathOperationList = new ArrayList<MathEquationOperationCategorytransferObj>();
		randomGenerator = new Random();
		int randomNumber = 0;

		int maxEquation = mathOperationList.size() > 100 ? 100:mathOperationList.size();

		for( int i = 0 ; i < maxEquation ; i++)
		{
			randomNumber = randomGenerator.nextInt(mathOperationList.size());

			if(onebyoneDigitList.contains(mathOperationList.get(randomNumber).getCategoryId()))
			{
				oneDByOneDlist.add(mathOperationList.get(randomNumber));
			}
			else if(twobyoneDigitList.contains(mathOperationList.get(randomNumber).getCategoryId()))
			{
				twoDByOneDlist.add(mathOperationList.get(randomNumber));
			}
			else if(twobytwoDigitList.contains(mathOperationList.get(randomNumber).getCategoryId()))
			{
				twoDByTwoDlist.add(mathOperationList.get(randomNumber));
			}
			else if(threebytwoDigitList.contains(mathOperationList.get(randomNumber).getCategoryId()))
			{
				threeDByTwoDlist.add(mathOperationList.get(randomNumber));
			}
			else if(threebythreeDigitList.contains(mathOperationList.get(randomNumber).getCategoryId()))
			{
				threeDByThreeDlist.add(mathOperationList.get(randomNumber));
			}

			mathOperationList.remove(randomNumber);
		}

		for(int i = 0 ; i < 3 ; i++)
		{
			if(oneDByOneDlist.size() > i)
			{
				randomNumber = randomGenerator.nextInt(oneDByOneDlist.size());
				newMathOperationList.add(oneDByOneDlist.get(randomNumber));
				oneDByOneDlist.remove(randomNumber);
			}
		}

		for(int i = 0 ; i < 3 ; i++)
		{
			if(twoDByOneDlist.size() > i)
			{
				randomNumber = randomGenerator.nextInt(twoDByOneDlist.size());
				newMathOperationList.add(twoDByOneDlist.get(randomNumber));
				twoDByOneDlist.remove(randomNumber);
			}
		}

		for(int i = 0 ; i < 2 ; i++)
		{
			if(twoDByTwoDlist.size() > i)
			{
				randomNumber = randomGenerator.nextInt(twoDByTwoDlist.size());
				newMathOperationList.add(twoDByTwoDlist.get(randomNumber));
				twoDByTwoDlist.remove(randomNumber);
			}
		}

		for(int i = 0 ; i < 1 ; i++)
		{
			if(threeDByTwoDlist.size() > i)
			{
				randomNumber = randomGenerator.nextInt(threeDByTwoDlist.size());
				newMathOperationList.add(threeDByTwoDlist.get(randomNumber));
				threeDByTwoDlist.remove(randomNumber);
			}
		}

		for(int i = 0 ; i < 1 ; i++)
		{
			if(threeDByThreeDlist.size() > i)
			{
				randomNumber = randomGenerator.nextInt(threeDByThreeDlist.size());
				newMathOperationList.add(threeDByThreeDlist.get(randomNumber));
				threeDByThreeDlist.remove(randomNumber);
			}
		}

		int newListSize = newMathOperationList.size();
		for(int i = 0 ; i < maxEquation - newListSize ; i ++ )
		{
			if(oneDByOneDlist.size() > 0)
			{
				randomNumber = randomGenerator.nextInt(oneDByOneDlist.size());
				newMathOperationList.add(oneDByOneDlist.get(randomNumber));
				oneDByOneDlist.remove(randomNumber);
			}

			if(twoDByOneDlist.size() > 0)
			{
				randomNumber = randomGenerator.nextInt(twoDByOneDlist.size());
				newMathOperationList.add(twoDByOneDlist.get(randomNumber));
				twoDByOneDlist.remove(randomNumber);
			}

			if(twoDByTwoDlist.size() > 0)
			{
				randomNumber = randomGenerator.nextInt(twoDByTwoDlist.size());
				newMathOperationList.add(twoDByTwoDlist.get(randomNumber));
				twoDByTwoDlist.remove(randomNumber);
			}

			if(threeDByTwoDlist.size() > 0)
			{
				randomNumber = randomGenerator.nextInt(threeDByTwoDlist.size());
				newMathOperationList.add(threeDByTwoDlist.get(randomNumber));
				threeDByTwoDlist.remove(randomNumber);
			}

			if(threeDByThreeDlist.size() > 0)
			{
				randomNumber = randomGenerator.nextInt(threeDByThreeDlist.size());
				newMathOperationList.add(threeDByThreeDlist.get(randomNumber));
				threeDByThreeDlist.remove(randomNumber);
			}
		}
	}

	@Override
	public void onBackPressed() 
	{		
		if(timer != null)
			timer.cancel();

		if(getReadyTimer != null)
			getReadyTimer.cancel();

		if(playsound != null)
			playsound.stopPlayer();

		isStopSecondPlayerProgress = true;//for stop the challenger player progresss

		isClickOnBackPressed = false;

		this.clickOnBackPressed();

		super.onBackPressed();
	}


	/**
	 * This method call when user click on back button
	 */
	private void clickOnBackPressed()
	{			
		//changs for correct time on result screen , on 07 Mar 2014
		if(!isClickOnBackPressed)
			totalTime = this.getNewMathPlayedData((TOTAL_TIME_TO_PLAY - (int)(startTimerForRoughArea/1000)) , playDatalist);
		//end changes

		this.savePlayerDataWhenNoInternetConnected();

		if(CommonUtils.isInternetConnectionAvailable(this))
		{
			if(!isClickOnBackPressed)
			{

				this.inserPlayDataIntodatabase(savePlayDataToDataBase());
				new AddCoinAndPointsForLoginUser(getPlayerPoints(savePlayDataToDataBase())).execute(null,null,null);

				//added by shilpi for friendzy
				if(CommonUtils.isActivePlayer){
					/*new SaveTimePointsForFriendzyChallenge(totalTimeTaken, points, this)
					.execute(null, null, null);*/
					new SaveTimePointsForFriendzyChallenge(totalTime, points, this)
					.execute(null, null, null);
				}
			}
			else
			{
				this.inserPlayDataIntodatabase(savePlayDataToDataBase());
			}
		}
		else
		{
			this.inserPlayDataIntodatabase(savePlayDataToDataBase());

			DialogGenerator dg = new DialogGenerator(this);
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
			transeletion.closeConnection();
		}
	}

	/**
	 * This method save play data to the database(Math_Result table)
	 */
	private MathResultTransferObj savePlayDataToDataBase()
	{
		MathResultTransferObj mathResultObj = new MathResultTransferObj();
		mathResultObj.setRoundId(0);
		mathResultObj.setIsFirstRound("0");
		mathResultObj.setGameType(gameType);
		mathResultObj.setIsWin(isWin);

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		if(sharedPreffPlayerInfo.getString("userId", "").equals("0"))
			mathResultObj.setUserId("0");
		else
			mathResultObj.setUserId(sharedPreffPlayerInfo.getString("userId", ""));

		if(sharedPreffPlayerInfo.getString("playerId", "").equals("0"))
			mathResultObj.setPlayerId("0");
		else
			mathResultObj.setPlayerId(sharedPreffPlayerInfo.getString("playerId", ""));

		//mathResultObj.setChallengerId(challengerDataObj.getPlayerId());

		mathResultObj.setIsFakePlayer(0);
		mathResultObj.setProblems(this.getEquationSolveXml(playDatalist));

		mathResultObj.setTotalScore(points);

		mathResultObj.setCoins(numberOfCoins);

		return mathResultObj;
	}

	/**
	 * This method convert the data for solving the equation into xml format
	 */
	private String getEquationSolveXml(ArrayList<MathScoreTransferObj> playDatalist)
	{		
		StringBuilder xml = new StringBuilder("");

		for( int i = 0 ;  i < playDatalist.size() ; i++)
		{
			xml.append("<equation>" +
					"<equationId>"+playDatalist.get(i).getEquationId()+"</equationId>"+
					"<start_date_time>"+playDatalist.get(i).getStartDateTimeStr()+"</start_date_time>"+
					"<end_date_time>"+playDatalist.get(i).getEndDateTimeStr()+"</end_date_time>"+
					"<lap>"+playDatalist.get(i).getLap()+"</lap>"+
					"<time_taken_to_answer>"+playDatalist.get(i).getTimeTakenToAnswer()+"</time_taken_to_answer>"+
					"<math_operation_id>"+playDatalist.get(i).getMathOperationID()+"</math_operation_id>"+
					"<points>"+playDatalist.get(i).getPoints()+"</points>"+
					"<isAnswerCorrect>" + playDatalist.get(i).getAnswerCorrect() + "</isAnswerCorrect>"+
					"<user_answer>"+playDatalist.get(i).getAnswer()+"</user_answer>"+
					"</equation>");
		}

		return xml.toString();
	}

	/**
	 * This methdod make xml for unsove eqaurions
	 * @return
	 */
	private String getEquationUnSolveXml()
	{
		StringBuilder xml = new StringBuilder("");

		SingleFriendzyImpl singleFriendzy = new SingleFriendzyImpl(this);
		singleFriendzy.openConn();

		for( int i = playDatalist.size() ;  i < equationListFromDataBase.size() - playDatalist.size() ; i++)
		{						
			SingleFriendzyEquationObj singleFriendzyEquation  = singleFriendzy.getEquationData(equationListFromDataBase.get(i).getEquationsId());

			xml.append("<equation>" +
					"<equationId>"+equationListFromDataBase.get(i).getEquationsId()+"</equationId>"+
					"<start_date_time>"+CommonUtils.formateDate(endTime)+"</start_date_time>"+
					"<end_date_time>"+CommonUtils.formateDate(endTime)+"</end_date_time>"+
					"<lap>"+lap1+"</lap>"+
					"<time_taken_to_answer>"+ 0 +"</time_taken_to_answer>"+
					"<math_operation_id>"+ singleFriendzyEquation.getMathOperationId() +"</math_operation_id>"+
					"<points>"+ this.getPointForUnsolveEquations(singleFriendzyEquation.getMathOperationId()) +"</points>"+
					"<isAnswerCorrect>" + 0 + "</isAnswerCorrect>"+
					"<user_answer>"+ 1 +"</user_answer>"+
					"</equation>");
		}

		singleFriendzy.closeConn();
		return xml.toString();
	}

	/**
	 * This method set the points
	 */
	private int getPointForUnsolveEquations(int operationId)
	{
		if(LEARNING_CENTER_EQUATION_SOLVE_WITH_TIMER)
			Log.e(TAG, "inside setPoints()");
		int calculatedpoints = 0;
		if(operationId == ADDITION || operationId == SUBTRACTION)
		{					
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = 50;
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = 100;
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = 125;
			}
			else if(threebythreeDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = 150;
			}

		}
		else if(operationId == MULTIPLICATION)
		{
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = 50;
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = 150;
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = 250;
			}
			else if(threebytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = 300;
			}

		}
		else if(operationId == DIVISION)
		{
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = 50;
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = 150;
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = 250;
			}
			else if(threebytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = 400;
			}

		}
		else if(operationId == FRACTION_ADDITION_SUBTRACTION)
		{
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = 150;
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = 200;
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = 300;
			}

		}
		else if(operationId == FRACTION_MULTIPLICATION_DIVISION)
		{
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = 150;
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = 250;
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = 400;
			}

		}
		else if(operationId == DECIMAL_ADDITION_SUBTRACTION)
		{
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = 75;
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = 100;
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = 150;
			}
			else if(threebythreeDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = 200;
			}

		}
		else if(operationId == DECIMAL_MULTIPLICATION_DIVISION)
		{
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = 75;
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = 125;
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = 200;
			}
			else if(threebytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = 250;
			}

		}
		else if(operationId == NEGATIVE_ADDITION_SUBTRACTION)
		{
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = 100;
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = 150;
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = 200;
			}
			else if(threebythreeDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = 250;
			}
		}
		else if(operationId == NEGATIVE_MULTIPLICATION_DIVISION)
		{
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = 100;
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = 200;
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = 300;
			}
			else if(threebytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = 500;
			}
		}
		if(LEARNING_CENTER_EQUATION_SOLVE_WITH_TIMER)
			Log.e(TAG, "outside setPoints()");

		return calculatedpoints;
	}
	/**
	 * This method insert the data into total player points table
	 * @param mathResultObj
	 */
	private void insertIntoPlayerTotalPoints(MathResultTransferObj mathResultObj)
	{
		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();
		PlayerTotalPointsObj playerPoints = this.getPlayerPoints(mathResultObj);

		if(learningCenterObj.isPlayerTotalPointsExist(playerPoints.getPlayerId()))
		{
			int points = playerPoints.getTotalPoints();
			int coins  = playerPoints.getCoins();
			playerPoints.setTotalPoints(learningCenterObj.getDataFromPlayerTotalPoints(playerPoints.getPlayerId()).getTotalPoints()
					+ points);

			playerPoints.setCoins(learningCenterObj.getDataFromPlayerTotalPoints(playerPoints.getPlayerId()).getCoins()
					+ coins);

			learningCenterObj.deleteFromPlayerTotalPoints(playerPoints.getPlayerId());
		}

		learningCenterObj.insertIntoPlayerTotalPoints(playerPoints);

		learningCenterObj.closeConn();
	}

	/**
	 * This method return the player points with its user id and player id and also coins
	 * @return
	 */
	private PlayerTotalPointsObj getPlayerPoints(MathResultTransferObj mathResultObj)
	{
		PlayerTotalPointsObj playerPoints = new PlayerTotalPointsObj();
		playerPoints.setUserId(mathResultObj.getUserId());
		playerPoints.setPlayerId(mathResultObj.getPlayerId());

		playerPoints.setTotalPoints(points);

		//numberOfCoins = (int) (points * 0.05);

		playerPoints.setCoins(numberOfCoins);

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		playerPoints.setCompleteLevel(sharedPreffPlayerInfo.getInt("completeLevel", 0));//set complete level

		playerPoints.setPurchaseCoins(0);

		return playerPoints;
	}

	/**
	 * This method add the start and end time to solve equation and all the information related to the equation
	 * to the arrayList
	 */
	private void savePlayData()
	{		
		//changes for timer on 11 Mar
		endTime = new Date();
		//end chages

		MathScoreTransferObj mathObj = new MathScoreTransferObj();
		mathObj.setMathOperationID(operationId);
		mathObj.setEquationId(equationId);
		mathObj.setLap(lap);
		mathObj.setStartDateTimeStr(CommonUtils.formateDate(startTime));
		mathObj.setEndDateTimeStr(CommonUtils.formateDate(endTime));
		mathObj.setPoints(pointsForEachQuestion);

		if(userAnswer.length() > 0)
			mathObj.setAnswer(userAnswer);
		else
			mathObj.setAnswer("1");

		mathObj.setAnswerCorrect(isCorrectAnsByUser);


		startTimeValue = startTime.getSeconds() ;
		endTimeVlaue   = endTime.getSeconds();

		if(endTimeVlaue < startTimeValue)
			endTimeVlaue = endTimeVlaue + 60;


		mathObj.setTimeTakenToAnswer(endTimeVlaue - startTimeValue);

		mathObj.setEquation(learnignCenterobj);

		pointsForEachQuestion = 0;
		playDatalist.add(mathObj);

		userAnswerList.add(userAnswer);

		//added by shilpi for friendzy
		totalTimeTaken = totalTimeTaken + (endTimeVlaue - startTimeValue);
	}

	/**
	 * This method store data into database for Math_result
	 * @param mathResultObj
	 */
	private void inserPlayDataIntodatabase(MathResultTransferObj mathResultObj )
	{
		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();
		learningCenterObj.insertIntoMathResult(mathResultObj);
		learningCenterObj.closeConn();
		this.insertIntoPlayerTotalPoints(mathResultObj);
	}

	@Override
	protected void getIntentValue() 
	{

		if(MultiFriendzyMain.isNewFriendzyStart)
		{
			opponentData = SelectedPlayerActivity.player;

			/*Log.e(TAG, "notification from userv seacrhing ipod" + opponentData.getIponPids() 
						+ " android pids " + opponentData.getAndroidPids());*/

			friendzyId = "-1";

			isNotificationSend = true;
			this.getEquationFromLocalDatabase();
			this.setVisibleOrInvisibleContent();
		}
		else
		{			
			friendzyId = MultiFriendzyRound.friendzyId;

			/*Log.e(TAG, "notification from userv seacrhing ipod" + MultiFriendzyRound.multiFriendzyServerDto.getNotificationDevices() 
					+ " android pids " + MultiFriendzyRound.multiFriendzyServerDto.getAndroidPids());*/

			int index = MultiFriendzyRound.roundList.size() - 1 ;
			if(MultiFriendzyRound.roundList.get(index).getPlayerScore().length() > 0
					&& MultiFriendzyRound.roundList.get(index).getOppScore().length() > 0)
			{
				isNotificationSend = true;
				this.getEquationFromLocalDatabase();
				this.setVisibleOrInvisibleContent();
			}
			else
			{
				if(MultiFriendzyRound.roundList.get(index).getPlayerScore().length() == 0)
				{
					isNotificationSend = false;
					isEquationFromserver = true;
					this.getEquationFromServer();
				}
			}
		}
	}

	/**
	 * This method get equations from server
	 */
	private void getEquationFromServer()
	{		
		String userId 		= MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getParentUserId();
		String playerId 	= MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getPlayerId();
		String friendzyId 	= MultiFriendzyRound.friendzyId;

		/*userId 		= "670";
		playerId 	= "1226";
		friendzyId 	= "121";
		 */

		if(CommonUtils.isInternetConnectionAvailable(this))
		{
			new FindMultiFriendzyEquationsForPlayer(userId, playerId, friendzyId).execute(null,null,null);
		}
	}

	/**
	 * This method getEquaion deon local database
	 */
	private void getEquationFromLocalDatabase()
	{
		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();

		//changes for friendzy challenge
		if(CommonUtils.isActivePlayer){
			String equationString = MainActivity.equationsObj.getEquationIds();
			MultiFriendzyImpl multifrinedzyImpl = new MultiFriendzyImpl(this);
			multifrinedzyImpl.openConn();
			ArrayList<Integer> mathOperationIdList = multifrinedzyImpl.getEquationCategoryId
					(equationString.substring(equationString.indexOf('&') + 1 , equationString.length()), 
							equationString.substring(0 , equationString.indexOf('&')));

			mathOperationList = learningCenterObj.getMathEquationDataByCategories(mathOperationIdList);
			multifrinedzyImpl.closeConn();

		}else{
			mathOperationList = learningCenterObj.getMathEquationDataByCategories(
					this.getIntent().getIntegerArrayListExtra("selectedCategories"));
		}
		//end changes

		this.getCategoriyIdListByDigits();

		equationsIdList = new ArrayList<Integer>();
		for( int i = 0 ; i < newMathOperationList.size() ; i ++ )
		{
			equationsIdList.add(newMathOperationList.get(i).getEquationId());
		}

		learningCenterObj.closeConn();

		LearningCenterimpl learnignCenterImpl = new LearningCenterimpl(MultiFriendzyEquationSolve.this);
		learnignCenterImpl.openConn();

		equationListFromDataBase = new ArrayList<LearningCenterTransferObj>();

		for(int i = 0 ; i < equationsIdList.size() ; i ++ )
		{			
			equationListFromDataBase.add(learnignCenterImpl.getEquation(equationsIdList.get(i)));
		}

		learnignCenterImpl.closeConn();
	}


	@Override
	protected void showEqautionData(LearningCenterTransferObj learningObj) 
	{
		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "inside showEqautionData()");

		/*Log.e(TAG, "equationId" + learningObj.getEquationsId() + " number1 " + learningObj.getNumber1Str()
		+ " number2 " + learningObj.getNumber2Str() + " operator " + learningObj.getOperator() 
		+ " " + learningObj.getProductStr());*/


		SingleFriendzyImpl singleFriendzy = new SingleFriendzyImpl(this);
		singleFriendzy.openConn();
		SingleFriendzyEquationObj singleFriendzyEquation  = singleFriendzy.getEquationData(learningObj.getEquationsId());
		singleFriendzy.closeConn();

		playEquationList.add(learningObj);

		equationId = learningObj.getEquationsId();

		startTime = new Date();

		operationId =  singleFriendzyEquation.getMathOperationId();

		//send equation for workArea

		SharedPreferences sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putInt("operationId", operationId);
		editor.commit();

		WorkAreaActivity.learningObj = learningObj;

		//set default progress at the starting
		layoutProgress1.setProgress(progress);

		//equationId = learningObj.getEquationsId();
		learnignCenterobj = learningObj;

		number1 = learningObj.getNumber1Str();
		number2 = learningObj.getNumber2Str();
		result  = learningObj.getProductStr();

		resultFromDatabase = learningObj.getProductStr();
		resultDigitsList   = this.getDigit(learningObj.getProductStr());
		operator           = learningObj.getOperator();

		noOfDigitsInFisrtNumber  = this.getDigit(learningObj.getNumber1Str()).size();
		noOfDigitsInSecondNumber = this.getDigit(learningObj.getNumber2Str()).size();
		noOfDigitsInResult       = this.getDigit(learningObj.getProductStr()).size();

		/**
		 * Changing the layout 
		 */

		if(operationId == ADDITION || operationId == SUBTRACTION
				|| operationId == DECIMAL_ADDITION_SUBTRACTION || 
				operationId == NEGATIVE_ADDITION_SUBTRACTION)//1,2,7,9 are the constants which show the operation id's
		{
			this.operationPerformForAdditionSubtraction(learningObj);
		}
		else if(operationId == FRACTION_ADDITION_SUBTRACTION 
				|| operationId == FRACTION_MULTIPLICATION_DIVISION)// 5,6 for operation on fraction number
		{		
			this.operatioPerformForFraction();
		}
		else if(operationId == MULTIPLICATION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
		{
			numberOfBoxesForMultiplication = noOfDigitsInResult;
			this.operationPerformForMultiplication();
		}
		else if(operationId == DECIMAL_MULTIPLICATION_DIVISION)
		{
			if(operator.equals("x"))
			{
				this.setnumberOfBoxesForMultiplicationForDecimal();
				this.operationPerformForMultiplication();
			}
			else if(operator.equals("/"))		
			{
				this.setNumberOfRowsAndboxForDivisionForDecimal();
				this.operationPerfomrmForDivision();
			}

		}
		else if(operationId ==  DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/")))
		{
			if(noOfDigitsInFisrtNumber == 1 && noOfDigitsInSecondNumber == 1)
				numberOfBoxesForDivision = 1;
			else if((noOfDigitsInFisrtNumber == 2 && noOfDigitsInSecondNumber == 1) || (noOfDigitsInFisrtNumber == 2 && noOfDigitsInSecondNumber == 2))
				numberOfBoxesForDivision = 2;
			else if((noOfDigitsInFisrtNumber == 3 && noOfDigitsInSecondNumber == 2))
				numberOfBoxesForDivision = 3;

			numberOfRowsforDivision = 2 * noOfDigitsInResult;
			this.setNumberOfRowsAndboxForDivisionForBoth();
			this.operationPerfomrmForDivision();
		}

		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "outside showEqautionData()");

	}

	@Override
	protected void setSign() 
	{
		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "inside setSign()");

		if(isTab){
			if(operator.equals("+"))
				imgSign.setImageResource(R.drawable.ml_plus_tab);
			else if(operator.equals("-"))
				imgSign.setImageResource(R.drawable.ml_minus_tab);
			else if(operator.equals("x"))
				imgSign.setImageResource(R.drawable.ml_multiplication_tab);
			else if(operator.equals("/"))
				imgSign.setImageResource(R.drawable.ml_div_tab);
		}
		else{
			if(operator.equals("+"))
				imgSign.setImageResource(R.drawable.ml_plus_sign);
			else if(operator.equals("-"))
				imgSign.setImageResource(R.drawable.ml_minus_sign);
			else if(operator.equals("x"))
				imgSign.setImageResource(R.drawable.ml_mul_sign);
			else if(operator.equals("/"))
				imgSign.setImageResource(R.drawable.ml_div_sign);
		}
		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "outside setSign()");
	}

	@Override
	protected void createResultLayout(ArrayList<String> listOfDigits, LinearLayout layout) 
	{		
		/**
		 * Set the width of linear layout
		 */
		int width = ((noOfDigitsInFisrtNumber + 1) * maxWidth) + 10 ;
		RelativeLayout.LayoutParams lp1 = new RelativeLayout.LayoutParams(width,LayoutParams.WRAP_CONTENT);
		lp1.addRule(RelativeLayout.BELOW,R.id.imgResultLine);
		layout.setLayoutParams(lp1);

		for(i = 0 ; i < listOfDigits.size() ; i ++ )
		{		
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);

			lp.setMargins(2, 0, 0, 0);
			TextView txtView = new TextView(this);
			if(i == listOfDigits.size() - 1)
			{
				//changes for tab
				if(isTab)
					txtView.setBackgroundResource(R.drawable.tab_yellow_ipad);
				else
					txtView.setBackgroundResource(R.drawable.yellow);
			}
			else
			{
				//changes for tab
				if(isTab)
					txtView.setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
				else
					txtView.setBackgroundResource(R.drawable.ml_no1box);
			}

			txtView.setGravity(Gravity.CENTER);
			//changes for tab
			if(isTab)
				txtView.setTextSize(45);
			else
				txtView.setTextSize(32);
			txtView.setTypeface(null, Typeface.BOLD);
			txtView.setTextColor(Color.parseColor("#3CB3FF"));
			resultTextViewList.add(txtView);//add text view to arrayList
			layout.addView(txtView,lp);

			txtView.setOnClickListener(new OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					for( int  i = 0 ; i < resultTextViewList.size() ; i ++ )
					{
						if(v == resultTextViewList.get(i))
						{
							selectedIndex = i;
							index = i;
							//changes for tab
							if(isTab)
								resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
							else
								resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow);
						}
						else
						{
							//changes for tab
							if(isTab)
								resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
							else
								resultTextViewList.get(i).setBackgroundResource(R.drawable.ml_no1box);
						}
					}
				}
			});
		}
	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.btnGo:
			endTime = new Date();
			isClickOnGo = true;		
			if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION)//fraction
			{
				this.clickonGoForFraction();
			}
			else if(operationId == MULTIPLICATION || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x")) 
					|| (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
			{
				this.clickOnGoForMultiplication();
			}
			else if(operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
					||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))
			{
				this.clickOnGoForDivision();
			}
			else
			{
				this.clickOnGo();
			}
			break;
		case R.id.btn1 :
			if(isCarrySelected)
				this.setCarryText("1");
			else
				this.setTextResultValue("1");
			break;
		case R.id.btn2 :
			if(isCarrySelected)
				this.setCarryText("2");
			else
				this.setTextResultValue("2");
			break;
		case R.id.btn3 :
			if(isCarrySelected)
				this.setCarryText("3");
			else
				this.setTextResultValue("3");
			break;
		case R.id.btn4 :
			if(isCarrySelected)
				this.setCarryText("4");
			else
				this.setTextResultValue("4");
			break;
		case R.id.btn5 :
			if(isCarrySelected)
				this.setCarryText("5");
			else
				this.setTextResultValue("5");
			break;
		case R.id.btn6 :
			if(isCarrySelected)
				this.setCarryText("6");
			else
				this.setTextResultValue("6");
			break;
		case R.id.btn7 :
			if(isCarrySelected)
				this.setCarryText("7");
			else
				this.setTextResultValue("7");
			break;
		case R.id.btn8 :
			if(isCarrySelected)
				this.setCarryText("8");
			else
				this.setTextResultValue("8");
			break;
		case R.id.btn9 :
			if(isCarrySelected)
				this.setCarryText("9");
			else
				this.setTextResultValue("9");
			break;
		case R.id.btn0 :
			if(isCarrySelected)
				this.setCarryText("0");
			else
				this.setTextResultValue("0");
			break;
		case R.id.btnDot :
			this.clickonDot();
			break;
		case R.id.btnMinus :
			this.clickOnMinus();
			break;
		case R.id.btnArrowBack :
			this.clickOnBackArrow();
			break;
		case R.id.txtCarry :
			this.clickOnCarry();
			break;
		case R.id.btnRoughWork :
			isClickOnWorkArea = true;
			Intent intent = new Intent(this,WorkAreaActivity.class);
			intent.putExtra("callingActivity", TAG);
			intent.putExtra("startTime", startTimerForRoughArea);
			startActivityForResult(intent, 1);
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == 1)
		{
			if(resultCode == RESULT_OK)
			{
				START_TIME = data.getLongExtra("timerStartTime", 0);
				//this.countDawnTimerStart();
			}
		}
	}

	/**
	 * This method is called when the click on Go Button and operation perform on fraction number
	 */
	private void clickonGoForFraction()
	{

		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "inside clickonGoForFraction()");

		index = 0;

		StringBuilder resultValue = new StringBuilder("");
		for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
		{
			resultValue = resultValue.append(resultTextViewList.get(i).getText().toString());
		}

		userAnswer = resultValue.toString();

		if(userAnswer.endsWith("."))
			resultValue.deleteCharAt(userAnswer.length() - 1);

		if((result.replace("/", "").replace(" ", "")).equals(resultValue.toString()))
		{
			playsound.playSoundForRightForSingleFriendzy(this);

			this.setProgress();

			rightAnsCounter ++ ;
			isCorrectAnsByUser = 1;
			this.savePlayData();
			/*isYellowboxForFraction = false;
			resultTextViewList.clear();
			linearLayoutQuotient.removeAllViews();
			linearLayoutNumerator.removeAllViews();
			linearLayoutDenominator.removeAllViews();
			this.setIndexForShowEquation();*/
			
			
			//change
			this.keyBoardDisable();
            pauseTimer();
			this.showCorrectAns(new OnAnswerGivenComplete() {
				@Override
				public void onComplete() {
					isYellowboxForFraction = false;
					resultTextViewList.clear();
					linearLayoutQuotient.removeAllViews();
					linearLayoutNumerator.removeAllViews();
					linearLayoutDenominator.removeAllViews();
					setIndexForShowEquation();
					keyBoardEnable();
                    startTimer();
				}
			} , resultTextViewList , FRACTION_DESIGN);
		}
		else
		{
			isCorrectAnsByUser = 0;
			playsound.playSoundForWrongForSingleFriendzy(this);
			this.keyBoardDisable();
            pauseTimer();
			this.visibleWrongImageForFraction();
			this.setHandler();
		}

		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "outside clickonGoForFraction()");
	}

	/**
	 * This method call when click on go for multiplication
	 */
	private void clickOnGoForMultiplication() 
	{
		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "inside clickOnGoForMultiplication()");


		StringBuilder resultValue = new StringBuilder("");
		if(this.getDigit(number2).size() > 1)
			startIndex = this.getDigit(number2).size() * numberOfBoxesForMultiplication;

		for( int i = startIndex ; i < resultTextViewList.size() ; i ++ )
		{
			resultValue = resultValue.append(resultTextViewList.get(i).getText().toString());
		}

		userAnswer = resultValue.toString();

		if(userAnswer.endsWith("."))
			resultValue.deleteCharAt(userAnswer.length() - 1);

		if(resultValue.toString().equals(resultFromDatabase))
		{
			playsound.playSoundForRightForSingleFriendzy(this);

			this.setProgress();

			rightAnsCounter ++ ;
			isCorrectAnsByUser = 1;
			this.savePlayData();
			
			/*isYellowboxForFraction = false;
			resultTextViewList.clear();
			linearLayoutMultiple.removeAllViews();
			linearLayoutResult.removeAllViews();
			this.setIndexForShowEquation();*/
			
			
			//change
			this.keyBoardDisable();
            pauseTimer();
			this.showCorrectAns(new OnAnswerGivenComplete() {
				@Override
				public void onComplete() {
					isYellowboxForFraction = false;
					resultTextViewList.clear();
					linearLayoutMultiple.removeAllViews();
					linearLayoutResult.removeAllViews();
					setIndexForShowEquation();
					keyBoardEnable();
                    startTimer();
				}
			} , resultTextViewList , MULTIPLICATION_DESIGN);
		}
		else
		{	
			/**
			 * check for reverse result
			 */
			if(resultValue.reverse().toString().equals(resultFromDatabase))
			{
				txtCarry.setText("");
				txtCarry.setBackgroundResource(R.drawable.whitebox_small);

				if(getDigit(number2).size() > 1)
					startIndex = getDigit(number2).size() * numberOfBoxesForMultiplication;

				for( int i = startIndex ; i < resultTextViewList.size() ; i ++ )
				{
					resultTextViewList.get(i).setText("");

					if( i == resultTextViewList.size() - 1)
					{
						//changes for tab
						if(isTab)
							resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
						else
							resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow_small);
					}
					else
					{
						//changes for tab
						if(isTab)
							resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
						else
							resultTextViewList.get(i).setBackgroundResource(R.drawable.whitebox_small);
					}
				}
				index = resultTextViewList.size() - 1;

				this.setTimerForReverseResult(MULTIPLICATION_DESIGN);

			}
			else
			{
				playsound.playSoundForWrongForSingleFriendzy(this);

				isCorrectAnsByUser = 0;
				this.visibleWrongImageForMultiplication();
				this.keyBoardDisable();
                pauseTimer();
				this.setHandler();
			}
		}

		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "outside clickOnGoForMultiplication()");

	}


	/**
	 * This method method call when click on go for division
	 */
	private void clickOnGoForDivision()
	{
		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "inside clickOnGoForDivision()");

		StringBuilder resultValue = new StringBuilder("");

		boolean isStartFromZero = false;
		for( int i = 0 ; i < numberOfBoxesForDivision ; i ++ )
		{
			if(!(resultTextViewList.get(i).getText().toString().equals("0") && isStartFromZero == false))
			{	
				resultValue = resultValue.append(resultTextViewList.get(i).getText().toString());
				isStartFromZero = true;
			}
		}

		userAnswer = resultValue.toString();

		if(userAnswer.endsWith("."))
			resultValue.deleteCharAt(userAnswer.length() - 1);

		if(resultValue.toString().equals(resultFromDatabase))
		{
			playsound.playSoundForRightForSingleFriendzy(this);

			this.setProgress();

			rightAnsCounter ++ ;
			isCorrectAnsByUser = 1;
			this.savePlayData();
			/*isYellowboxForFraction = false;
			resultTextViewList.clear();
			linearLayoutResult.removeAllViews();
			linearLayoutRowsForDivision.removeAllViews();
			this.setIndexForShowEquation();*/
			
			
			//change
			this.keyBoardDisable();
            pauseTimer();
			this.showCorrectAns(new OnAnswerGivenComplete() {
				@Override
				public void onComplete() {
					isYellowboxForFraction = false;
					resultTextViewList.clear();
					linearLayoutResult.removeAllViews();
					linearLayoutRowsForDivision.removeAllViews();
					setIndexForShowEquation();
					keyBoardEnable();
                    startTimer();
				}
			} , resultTextViewList , DEVISION_DESIGN);
		}
		else
		{			
			playsound.playSoundForWrongForSingleFriendzy(this);
			isCorrectAnsByUser = 0;
			this.visibleWrongImageForDivision();
			this.keyBoardDisable();
            pauseTimer();
			this.setHandler();
		}

		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "outsideside clickOnGoForDivision()");
	}

	/**
	 * This method is called when the click on Go Button
	 */
	private void clickOnGo()
	{

		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "inside clickOnGo()");

		index         = noOfDigitsInResult - 1;
		selectedIndex = noOfDigitsInResult - 1;

		StringBuilder resultValue = new StringBuilder("");
		for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
		{
			resultValue = resultValue.append(resultTextViewList.get(i).getText().toString());
		}

		userAnswer = resultValue.toString();

		if(userAnswer.endsWith("."))
			resultValue.deleteCharAt(userAnswer.length() - 1);

		if(resultValue.toString().equals(resultFromDatabase))
		{
			playsound.playSoundForRightForSingleFriendzy(this);

			this.setProgress();
			rightAnsCounter ++ ;
			isCorrectAnsByUser = 1;
			this.savePlayData();

			//change 22 jan 2015
			this.keyBoardDisable();
            pauseTimer();
			this.showCorrectAns(new OnAnswerGivenComplete() {

				@Override
				public void onComplete() {
					resultTextViewList.clear();
					linearLayoutResultRow.removeViews(0, noOfDigitsInResult);
					setIndexForShowEquation();
					keyBoardEnable();
                    startTimer();
				}
			} , resultTextViewList);
			//end changes

			/*resultTextViewList.clear();
			linearLayoutResultRow.removeViews(0, noOfDigitsInResult);
			this.setIndexForShowEquation();*/

		}
		else
		{	
			playsound.playSoundForWrongForSingleFriendzy(this);

			/**
			 * check for reverse result
			 */

			if((resultValue.reverse().toString()).equals(resultFromDatabase))
			{
				txtCarry.setText("");
				txtCarry.setBackgroundResource(R.drawable.ml_no1box);

				for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
				{
					resultTextViewList.get(i).setText("");

					if(i == index)
					{
						//changes for tab
						if(isTab)
							resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
						else
							resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow);
					}
					else
					{
						//changes for tab
						if(isTab)
							resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
						else
							resultTextViewList.get(i).setBackgroundResource(R.drawable.ml_no1box);
					}
				}

				this.setTimerForReverseResult();
			}
			else
			{
				isCorrectAnsByUser = 0;
				this.keyBoardDisable();
                pauseTimer();
				visibleWrongImage();
				this.setHandler();
			}
		}

		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "outside clickOnGo()");
	}


	/**
	 * This method set the progress and also set points and lap
	 */
	private void setProgress()
	{
		int timeTaken = endTime.getSeconds() - startTime.getSeconds();

		//equation from server
		if(equationList != null)
		{
			int pointForChallenger 		= equationList.get(showEquationIndex).getPoints();
			int timeTakenForChallenger 	= (int) equationList.get(showEquationIndex).getTimeTakenToAnswer();

			pointsForEachQuestion = (pointForChallenger + timeTakenForChallenger - timeTaken);
			points = points + (pointForChallenger + timeTakenForChallenger - timeTaken);
		}
		else
		{
			this.setPoints();
		}

		if(progress == 100) // if progress is 100 then increment lap and set progress 25
		{
			lap = lap + 1; // Increment lap when user progress is 100
			progress = 25; // set progress for progress bar 25

		}
		else
		{
			progress = progress + 25;//update progress bar progress
		}

		layoutProgress1.setProgress(progress);
		setProgressColor(points , secondPlayerpoints);

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		txtLap1.setText(transeletion.getTranselationTextByTextIdentifier("mfLblLap") + ": " + lap);
		txtPoints1.setText(transeletion.getTranselationTextByTextIdentifier("mfLblPts") + ": " + points);
		transeletion.closeConnection();
	}


	/**
	 * This method set the second player progress
	 */
	private void setSecondPlayerProgress()
	{	
		//Log.e(TAG, "inside setSecondPlayerProgress ");

		if(equationList.get(0).getTimeTakenToAnswer() > 0 && equationList.get(0).getIsAnswerCorrect() == 1)
		{
			setsecondPlayerDataProgress(0, equationList.get(0).getTimeTakenToAnswer());
		}
	}


	/**
	 * This method set the second player points data
	 * @param index
	 * @param timeTakenToAnswer
	 */
	private void setsecondPlayerDataProgress(final int index , double timeTakenToAnswer)
	{
		int sleepTime = (int) (timeTakenToAnswer * 1000);

		Handler handlerSetResult = new Handler();
		handlerSetResult.postDelayed(new Runnable() 
		{
			@Override
			public void run() 
			{		
				if(progress1 == 100) // if progress is 100 then increment lap and set progress 25
				{
					progress1 = 25; // set progress for progress bar 25
				}
				else
				{
					if(equationList.get(index).getIsAnswerCorrect() == 1)
						progress1 = progress1 + 25;//update progress bar progress
				}

				layoutProgress2.setProgress(progress1);

				secondPlayerpoints = secondPlayerpoints + equationList.get(index).getPoints();

				setProgressColor(points , secondPlayerpoints);

				Translation transeletion = new Translation(MultiFriendzyEquationSolve.this);
				transeletion.openConnection();
				txtLap2.setText(transeletion.getTranselationTextByTextIdentifier("mfLblLap") + ": " + 
						equationList.get(index).getLap());
				txtPoints2.setText(transeletion.getTranselationTextByTextIdentifier("mfLblPts") + ": " + secondPlayerpoints);
				transeletion.closeConnection();

				playsound.playSoundForRightForSingleFriendzyForChallenger(MultiFriendzyEquationSolve.this);

				equationIndex(index);
			}
		}, sleepTime);
	}

	/**
	 * This method set the progress color
	 * @param playerpoints
	 * @param opponentPlayerpoints
	 */
	private void setProgressColor(int playerpoints , int opponentPlayerpoints){
		Drawable greenProgress = getResources().getDrawable(R.drawable.greenprogress);
		Drawable redProgress = getResources().getDrawable(R.drawable.redprogress);
		if(vesrionGreaterThan){
			if(playerpoints > opponentPlayerpoints){
				layoutProgress1.setProgressDrawable(greenProgress);
				layoutProgress2.setProgressDrawable(redProgress);
			}
			else{
				layoutProgress2.setProgressDrawable(greenProgress);
				layoutProgress1.setProgressDrawable(redProgress);
			}
		}
	}

	/**
	 * Get the data from the equation list for setting progress according to i index 
	 * @param index
	 */
	private void equationIndex(int index)
	{

		if(!isStopSecondPlayerProgress)
		{
			if(index + 1 < equationList.size())
			{
				if(equationList.get(index + 1).getTimeTakenToAnswer() > 0 && equationList.get(index + 1).getIsAnswerCorrect() == 1)
				{
					setsecondPlayerDataProgress(index + 1, equationList.get(index + 1).getTimeTakenToAnswer());
				}
				else
				{
					if(index < equationList.size() - 2)
						equationIndex(index + 1);
				}
			}
		}
	}


	/**
	 * This method set the points
	 */
	private int setPoints()
	{
		if(LEARNING_CENTER_EQUATION_SOLVE_WITH_TIMER)
			Log.e(TAG, "inside setPoints()");

		int calculatedpoints = 0;//initially 0
		if(operationId == ADDITION || operationId == SUBTRACTION)
		{					
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(50);
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(100);
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(125);
			}
			else if(threebythreeDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(150);
			}

		}
		else if(operationId == MULTIPLICATION)
		{
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(50);
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(150);
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(250);
			}
			else if(threebytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(300);
			}

		}
		else if(operationId == DIVISION)
		{
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(50);
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(150);
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(250);
			}
			else if(threebytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(400);
			}

		}
		else if(operationId == FRACTION_ADDITION_SUBTRACTION)
		{
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(150);
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(200);
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(300);
			}

		}
		else if(operationId == FRACTION_MULTIPLICATION_DIVISION)
		{
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(150);
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(250);
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(400);
			}

		}
		else if(operationId == DECIMAL_ADDITION_SUBTRACTION)
		{
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(75);
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(100);
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(150);
			}
			else if(threebythreeDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(200);
			}

		}
		else if(operationId == DECIMAL_MULTIPLICATION_DIVISION)
		{
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(75);
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(125);
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(200);
			}
			else if(threebytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(250);
			}

		}
		else if(operationId == NEGATIVE_ADDITION_SUBTRACTION)
		{
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(100);
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(150);
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(200);
			}
			else if(threebythreeDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(250);
			}
		}
		else if(operationId == NEGATIVE_MULTIPLICATION_DIVISION)
		{
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(100);
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(200);
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(300);
			}
			else if(threebytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(500);
			}
		}
		if(LEARNING_CENTER_EQUATION_SOLVE_WITH_TIMER)
			Log.e(TAG, "outside setPoints()");

		return calculatedpoints;
	}


	/**
	 * This method calculate points and return it;
	 * @param maxPoints
	 */
	@SuppressWarnings("deprecation")
	private int calculatePoints(int maxPoints)
	{		
		if((maxPoints - (endTime.getSeconds() - startTime.getSeconds())) > 3 && (endTime.getSeconds() - startTime.getSeconds()) > 0)
		{
			pointsForEachQuestion = (maxPoints - (endTime.getSeconds() - startTime.getSeconds()));
			points = points + (maxPoints - (endTime.getSeconds() - startTime.getSeconds()));
		}
		else
		{
			pointsForEachQuestion = 3; // add max 3 points when user give answer greater then max points time in seconds
			points = points + 3;
		}

		return points;
	}

	/**
	 * This method for displaying cross image for wrong question and invisible it
	 */
	private void setHandler()
	{
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() 
		{
			@Override
			public void run() 
			{
				imgWrong.setVisibility(ImageView.INVISIBLE);

				if(operationId == MULTIPLICATION || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
						||(operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
				{
					if(getDigit(number2).size() > 1)
						startIndex = getDigit(number2).size() * numberOfBoxesForMultiplication;

					for( int i = startIndex ; i < resultTextViewList.size() ; i ++ )
					{
						resultTextViewList.get(i).setText("");
						resultTextViewList.get(i).setTextColor(Color.parseColor("#3CB3FF"));
					}
				}
				else if(operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
						||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))
				{
					for( int i = 0; i < numberOfBoxesForDivision ; i ++ )
					{
						resultTextViewList.get(i).setText("");
						resultTextViewList.get(i).setTextColor(Color.parseColor("#3CB3FF"));
					}
				}
				else
				{
					for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
					{		
						resultTextViewList.get(i).setText("");
						resultTextViewList.get(i).setTextColor(Color.parseColor("#3CB3FF"));
					}
				}

				keyBoardEnable();
                startTimer();
				//savePlayData();

				if(operationId == MULTIPLICATION || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
						||(operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
				{
					if(getDigit(number2).size() > 1)
						startIndex = getDigit(number2).size() * numberOfBoxesForMultiplication;

					for( int i = startIndex ; i < resultTextViewList.size() ; i ++ )
					{
						try{
							//changes on 22 Jan 2015
							setGreenBackGroundToTextViewForDivision(resultTextViewList.get(i));
							//end changes
							resultTextViewList.get(i).setText(resultDigitsList.get(i - startIndex));
						}catch(IndexOutOfBoundsException ee)
						{
							break;
						}
					}
				}
				else if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION)//fraction
				{
					String compareResult = result.replace("/", "").replace(" ", "");
					for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
					{
						//changes on 22 Jan 2015
						setGreenBackGroundToTextViewForDivision(resultTextViewList.get(i));
						//end changes
						resultTextViewList.get(i).setText(compareResult.charAt(i) + "");
					}
				}
				else if(operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
						||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))
				{
					for( int i = 0 ; i < numberOfBoxesForDivision ; i ++ )
					{
						try{
							
							//changes on 22 Jan 2015
							setGreenBackGroundToTextViewForDivision(resultTextViewList.get(i));
							//end changes
							
							resultTextViewList.get(i).setText(resultDigitsList.get(i));
						}catch(IndexOutOfBoundsException ee)
						{
							break;
						}
					}
				}
				else
				{
					for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
					{
						//changes on 22 Jan 2015
						setGreenBackGroundToTextView(resultTextViewList.get(i));
						//end changes
						resultTextViewList.get(i).setText(resultDigitsList.get(i));
					}
				}

				keyBoardDisable();
                pauseTimer();
				Handler handlerSetResult = new Handler();
				handlerSetResult.postDelayed(new Runnable() 
				{
					@Override
					public void run() 
					{		
						//changes for timer on 11 Mar 2014
						savePlayData();
						//end chages

						if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION)//fraction
						{
							isYellowboxForFraction = false;
							resultTextViewList.clear();
							linearLayoutQuotient.removeAllViews();
							linearLayoutNumerator.removeAllViews();
							linearLayoutDenominator.removeAllViews();
							setIndexForShowEquation();
							keyBoardEnable();
                            startTimer();
						}
						else if(operationId == MULTIPLICATION 
								|| (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
								|| (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
						{
							isYellowboxForFraction = false;
							resultTextViewList.clear();
							linearLayoutMultiple.removeAllViews();
							linearLayoutResult.removeAllViews();
							txtCarry.setText("");
							txtCarry.setBackgroundResource(R.drawable.ml_no1box);
							setIndexForShowEquation();
							keyBoardEnable();
                            startTimer();
						}
						else if(operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
								||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))
						{
							isYellowboxForFraction = false;
							resultTextViewList.clear();
							linearLayoutResult.removeAllViews();
							linearLayoutRowsForDivision.removeAllViews();
							txtCarry.setText("");
							txtCarry.setBackgroundResource(R.drawable.ml_no1box);
							setIndexForShowEquation();
							keyBoardEnable();
                            startTimer();
						}
						else
						{
							resultTextViewList.clear();
							linearLayoutResultRow.removeViews(0, noOfDigitsInResult);
							txtCarry.setText("");
							txtCarry.setBackgroundResource(R.drawable.ml_no1box);
							setIndexForShowEquation();
							keyBoardEnable();
                            startTimer();
						}
					}
				}, SLEEP_TIME);
			}
		//}, SLEEP_TIME / 4);
		}, this.getWrongAnswerShowTime(3 , SLEEP_TIME));
	}

	/**
	 * This method call when user give the reverse answer
	 */
	private void setTimerForReverseResult(int DESIGN_FOR){
		START_TIME = startTimerForRoughArea;
		timer.cancel();
		timer = new MyTimer(START_TIME, END_TIME);
		
		if(DESIGN_FOR == MULTIPLICATION_DESIGN){
			try{
                //for the change on the 25 jun 2015  ,treat reverse answer as right, according to ryan
                this.setProgress();
                rightAnsCounter ++ ;
                isCorrectAnsByUser = 1;
                //end change , comment the following line isCorrectAnsByUser = 0;
                //isCorrectAnsByUser = 0;
                this.savePlayData();

				this.keyBoardDisable();
				this.showReverseCorrectAnsDialog(this, timer, new OnReverseDialogClose() {
					@Override
					public void onClose() {
						isYellowboxForFraction = false;
						resultTextViewList.clear();
						linearLayoutMultiple.removeAllViews();
						linearLayoutResult.removeAllViews();
						setIndexForShowEquation();
						keyBoardEnable();
                        startTimer();
					}
				},resultTextViewList , resultDigitsList , MULTIPLICATION_DESIGN);
			}catch(Exception e){
				e.printStackTrace();
			}
			//end changes
		}
	}

    /**
     * pause timer
     */
    private void pauseTimer(){
        try {
            START_TIME = startTimerForRoughArea;
            timer.cancel();
            timer = new MyTimer(START_TIME, END_TIME);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * start timer
     */
    private void startTimer(){
        try {
            timer.start();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

	/**
	 * This method call when user give the reverse answer
	 */
	private void setTimerForReverseResult()
	{
		START_TIME = startTimerForRoughArea;
		timer.cancel();
		timer = new MyTimer(START_TIME, END_TIME);

		/*DialogGenerator dg = new DialogGenerator(this);
		dg.generateDialogForSolvingProblem(timer);*/
		
		//changes on 22 Jan 2015
		try{
            //for the change on the 25 jun 2015  ,treat reverse answer as right, according to ryan
            this.setProgress();
            rightAnsCounter ++ ;
            isCorrectAnsByUser = 1;
            //end change , comment the following line isCorrectAnsByUser = 0;
            //isCorrectAnsByUser = 0;
            this.savePlayData();

			this.keyBoardDisable();
			this.showReverseCorrectAnsDialog(this, timer, new OnReverseDialogClose() {
				@Override
				public void onClose() {
					resultTextViewList.clear();
					linearLayoutResultRow.removeViews(0, noOfDigitsInResult);
					setIndexForShowEquation();
					keyBoardEnable();
                    startTimer();
				}
			},resultTextViewList , resultDigitsList);
		}catch(Exception e){
			e.printStackTrace();
		}
		//end changes		
	}


	/**
	 * This methos set the index for show equation and also calles showEqiuation method which
	 * show the equation on this selected index
	 */
	private void setIndexForShowEquation()
	{
		if(showEquationIndex < equationsIdList.size() - 1)
		{
			this.showEqautionData(this.getEquation(++showEquationIndex));
		}
		else
		{
			showEquationIndex = 0 ;
			this.showEqautionData(this.getEquation(++showEquationIndex));
		}
	}

	/**
	 * This class getEquation from server
	 * @author Yashwant Singh
	 *
	 */
	class FindMultiFriendzyEquationsForPlayer extends AsyncTask<Void, Void, ArrayList<EquationFromServerTransferObj>>
	{
		private String userId 	= null;
		private String playerId = null;
		private String friendzyId = null;

		FindMultiFriendzyEquationsForPlayer(String userId , String playerId , String friendzyId)
		{
			this.userId 	= userId;
			this.playerId 	= playerId;
			this.friendzyId = friendzyId;
		}

		@Override
		protected void onPreExecute() 
		{
			super.onPreExecute();
		}

		@Override
		protected ArrayList<EquationFromServerTransferObj> doInBackground(Void... params) 
		{
			ArrayList<EquationFromServerTransferObj> equatiolListFromServer = new ArrayList<EquationFromServerTransferObj>();
			MultiFriendzyServerOperation serverObj = new MultiFriendzyServerOperation();
			equatiolListFromServer = serverObj.findMultiFriendzyEquatiosForPlayer(userId, playerId, friendzyId);

			return equatiolListFromServer;
		}

		@Override
		protected void onPostExecute(ArrayList<EquationFromServerTransferObj> result) 
		{
			if(equationList == null  && result != null)
			{
				equationList = result;

				equationsIdList = new ArrayList<Integer>();

				for( int i = 0 ; i < equationList.size() ; i ++ )
				{	
					equationsIdList.add(equationList.get(i).getMathEquationId());
				}
			}

			super.onPostExecute(result);
		}
	}

	/**
	 * This asyncTask Add points and coins on server
	 * @author Yashwant Singh
	 *
	 */
	class AddCoinAndPointsForLoginUser extends AsyncTask<Void, Void, Void> 
	{		
		private PlayerTotalPointsObj playerObj = null;

		AddCoinAndPointsForLoginUser(PlayerTotalPointsObj playerObj)
		{
			this.playerObj = playerObj;

			LearningCenterimpl learningCenterimpl = new LearningCenterimpl(MultiFriendzyEquationSolve.this);
			learningCenterimpl.openConn();
			playerObj = learningCenterimpl.getDataFromPlayerTotalPoints(playerObj.getPlayerId());
			learningCenterimpl.closeConn();	

			this.playerObj.setTotalPoints(playerObj.getTotalPoints());
			this.playerObj.setCoins(playerObj.getCoins());
		}

		@Override
		protected void onPreExecute() 
		{			
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			Register register = new Register(MultiFriendzyEquationSolve.this);
			register.addPointsAndCoinsOnServerForloginUser(playerObj);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{			
			super.onPostExecute(result);
		}
	}

	/**
	 * This class create Multifriedzy on server
	 * @author Yashwant Singh
	 *
	 */
	class CreateMultiFriendzy extends AsyncTask<Void, Void, Void>
	{
		private CreateMultiFriendzyDto createMultiFrinedzyDto = null;
		private boolean isAddParameter;
		private String frendzyId = null;
		private ProgressDialog pd = null;	

		public CreateMultiFriendzy(CreateMultiFriendzyDto createMultiFrinedzyDto, boolean isAddParameter) 
		{
			this.createMultiFrinedzyDto = createMultiFrinedzyDto;
			this.isAddParameter         = isAddParameter;
		}

		@Override
		protected void onPreExecute() 
		{
			pd = CommonUtils.getProgressDialog(MultiFriendzyEquationSolve.this);
			pd.show();

			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			MultiFriendzyServerOperation multiFriendzyServerObj = new MultiFriendzyServerOperation();
			frendzyId = multiFriendzyServerObj.createMultiFriendzy(createMultiFrinedzyDto, isAddParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{			
			pd.cancel();

			//Log.e(TAG, "turn " + turn);

			if(frendzyId != null)
				createMultiFrinedzyDto.setFriendzyId(frendzyId);

			if(isNotificationSend)
				sendNotification(createMultiFrinedzyDto);//send object for getting some data from it

			MultiFriendzyRound.roundList = updatedRoundList;

			Translation transeletion = new Translation(MultiFriendzyEquationSolve.this);
			transeletion.openConnection();

			if(turn != 2)//not for history
			{
				if(turn == 1)//their turn
				{
					MultiFriendzyRound.turn = transeletion.getTranselationTextByTextIdentifier("lblTheirTurn"); 
					MultiFriendzyRound.type = THEIR_FRENDZY_TEXT;
				}
				else if(turn == 0)//your turn
				{
					MultiFriendzyRound.turn = transeletion.getTranselationTextByTextIdentifier("lblYourTurn");
					MultiFriendzyRound.type = YOUR_FRIENDZY_TEXT;
				}
				transeletion.closeConnection();

				if(frendzyId != null)
					MultiFriendzyRound.friendzyId = frendzyId;

				startActivity(new Intent(MultiFriendzyEquationSolve.this , MultiFriendzyRound.class));
			}
			else
			{	
				String opponentName 			= null;
				String opponentProfileImageId   = null;

				if(opponentData != null)//data from searched user
				{
					String lastName = "";
					if(opponentData.getLastName().length() > 0)
						lastName = opponentData.getLastName().charAt(0) + "";

					opponentName = opponentData.getFirstName() + " " + lastName + ".";
					opponentProfileImageId   = opponentData.getProfileImageName();
				}
				else //  data from click on your turn
				{
					String lastName = "";
					if(MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getlName().length() > 0)
						lastName = MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getlName().charAt(0) + "";

					opponentName = MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getfName() + " " + lastName + ".";
					opponentProfileImageId   = MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getProfileImageNameId();
				}

				MultiFriendzyWinnerScreen.oppenentPlayerName = opponentName;
				MultiFriendzyWinnerScreen.opponentImageId    = opponentProfileImageId;
				MultiFriendzyWinnerScreen.roundList          = updatedRoundList;

				int playerTotal 	= 0 ; 
				int opponentTotal 	= 0 ;

				for(int i = 0 ; i < MultiFriendzyWinnerScreen.roundList.size() ; i ++)
				{
					if(!MultiFriendzyWinnerScreen.roundList.get(i).getPlayerScore().equals(""))
					{
						playerTotal = playerTotal + Integer.parseInt(MultiFriendzyWinnerScreen.roundList.get(i).getPlayerScore());
					}

					if(!MultiFriendzyWinnerScreen.roundList.get(i).getOppScore().equals(""))
					{
						opponentTotal = opponentTotal + Integer.parseInt(MultiFriendzyWinnerScreen.roundList.get(i).getOppScore());
					}
				}

				if(playerTotal > opponentTotal)
					MultiFriendzyWinnerScreen.isWinner = true;
				else
					MultiFriendzyWinnerScreen.isWinner = false;

				startActivity(new Intent(MultiFriendzyEquationSolve.this , MultiFriendzyWinnerScreen.class));
			}

			super.onPostExecute(result);
		}
	}

	@Override
	protected void onResume() {
		if(!isClickOnWorkArea){
			isStopSecondPlayerProgress = false;//for stop the challenger player progresss
			if(!isFromOnCreate){
				if(playsound != null)
					playsound.playSoundForSingleFriendzyEquationSolve(MultiFriendzyEquationSolve.this);
			}else{
				isFromOnCreate = false;
			}
		}else
			isClickOnWorkArea = false;
		super.onResume();
	}

	@Override
	protected void onPause() {
		if(!isClickOnWorkArea){
			isStopSecondPlayerProgress = true;//for stop the challenger player progresss
			if(playsound != null)
				playsound.stopPlayer();
		}
		super.onPause();
	}
}
