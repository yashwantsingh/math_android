package com.mathfriendzy.controller.multifriendzy.contacts;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ContactOperation;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ICommonUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static com.mathfriendzy.utils.ITextIds.LBL_INVITE;
import static com.mathfriendzy.utils.ITextIds.LBL_MEMBER;

public class ContactNameActivity extends ActBaseClass implements OnClickListener
{
	private ArrayList<ContactBeans> contactRecord		= null;

	private String alertNoPhone							= null;
	private TextView txtTitleTopbar						= null;
	private EditText searchBar							= null;
	private Button btnInviteContact						= null;
	private ListView listContact						= null;
	private ArrayList<String> numberList				= null;
	private String txtInvite							= null;
	private String message;
	private DisplayMetrics metrics = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contact_nane);

		metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);  

		message				= getIntent().getStringExtra("msg");
		contactRecord		= new ArrayList<ContactBeans>();
		numberList			= new ArrayList<String>();

		ContactOperation contact = new ContactOperation(getContentResolver());

		contactRecord		= contact.addContactToList();	
		getWidgetId();
		setWidgetText();
		searchListener();

		Collections.sort(contactRecord, new ContactSortByName());

		ContactListAdapter adapter = new ContactListAdapter(this, 0,contactRecord);
		listContact.setAdapter(adapter);
	}//END onCreate method



	private void searchListener()
	{
		searchBar.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s)
			{
				ArrayList<ContactBeans> newContact = new ArrayList<ContactBeans>();

				for(ContactBeans obj : contactRecord)
				{
					if(obj.getName().toLowerCase().contains(searchBar.getText().toString().toLowerCase()))
					{
						newContact.add(obj);
					}
				}

				ContactListAdapter adapter = new ContactListAdapter(ContactNameActivity.this,0, newContact);
				listContact.setAdapter(adapter);
			}
		});

	}



	private void getWidgetId() 
	{
		txtTitleTopbar		= (TextView) findViewById(R.id.txtTitleTopbar);
		btnInviteContact 	= (Button) findViewById(R.id.btnInviteContact);
		listContact			= (ListView) findViewById(R.id.contactList);
		searchBar			= (EditText) findViewById(R.id.searchBar);

		btnInviteContact.setOnClickListener(this);
	}


	private void setWidgetText()
	{
		Translation translate = new Translation(this);
		translate.openConnection();
		String text;
		txtInvite = translate.getTranselationTextByTextIdentifier(LBL_INVITE);
		btnInviteContact.setText(txtInvite);

		text = txtInvite + " " +translate.getTranselationTextByTextIdentifier(LBL_MEMBER);
		txtTitleTopbar.setText(text);
		alertNoPhone = translate.getTranselationTextByTextIdentifier("alertMsgNoPhoneNumberAssociated");

		translate.closeConnection();

	}//END setWidgetText()



	class ContactSortByName implements Comparator<ContactBeans>
	{

		@Override
		public int compare(ContactBeans obj1, ContactBeans obj2)
		{			
			return obj1.getName().compareTo(obj2.getName());
		}

	}



	/**
	 * use to show list of contacts
	 * @author Shilpi Mangal
	 *
	 */
	public class ContactListAdapter extends ArrayAdapter
	{	

		int counter = 0;
		Context context;
		ArrayList<ContactBeans> contactList;	

		private boolean[] check	= null;

		@SuppressWarnings("unchecked")
		public ContactListAdapter(Context context,int id, ArrayList<ContactBeans> contactList) 
		{
			super(context, id,  contactList);
			this.contactList = contactList;
			this.context	 = context;	

			check = new boolean[contactList.size()];
		}

		@Override
		public int getCount() {
			return contactList.size();
		}

		@Override
		public View getView(final int position, View row, ViewGroup parent)
		{
			final ViewHolder holder;
			if(row == null)
			{		
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = inflater.inflate(R.layout.contact_list, parent, false);
				holder 		= new ViewHolder();
				holder.txtContactName = (TextView) row.findViewById(R.id.txtContactName);
				holder.imgContact	  = (ImageView) row.findViewById(R.id.imgContact);	
				holder.btnCheck		  = (Button) row.findViewById(R.id.btnCheck);
				holder.btnInvite	  = (Button) row.findViewById(R.id.btnInvite);

				row.setTag(holder);
			}
			else
			{
				holder = (ViewHolder) row.getTag();
			}

			holder.txtContactName.setText(contactList.get(position).getName());
			if(contactList.get(position).getPhoto() == null)
			{
				holder.imgContact.setImageBitmap(contactList.get(position).getPhoto());
			}
			else 
			{
				holder.imgContact.setImageBitmap(contactList.get(position).getPhoto());
			}

			if(check[position] == true){
				holder.btnInvite.setVisibility(View.INVISIBLE);
				holder.btnCheck.setVisibility(View.VISIBLE);
			}else{
				holder.btnInvite.setVisibility(View.VISIBLE);
				holder.btnCheck.setVisibility(View.INVISIBLE);
			}

			holder.btnInvite.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if(!MainActivity.isTab && 
							!(metrics.heightPixels == ICommonUtils.TAB_HEIGHT && metrics.widthPixels == ICommonUtils.TAB_WIDTH &&
							metrics.densityDpi == ICommonUtils.TAB_DENISITY)
							&& context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY)){
						if(contactList.get(position).getPhone() == null)
						{
							DialogGenerator dialog = new DialogGenerator(context);
							dialog.generateWarningDialog(alertNoPhone);
						}
						else
						{	
							if(check[position] == false){
								check[position] = true;
								holder.btnInvite.setVisibility(View.INVISIBLE);
								holder.btnCheck.setVisibility(View.VISIBLE);
								counter++;
								btnInviteContact.setText(txtInvite+" ("+counter+")");
								numberList.add(contactList.get(position).getPhone());
							}
							else{
								check[position] = false;
							}
						}
					}else{
						DialogGenerator dg = new DialogGenerator(context);
						Translation transeletion = new Translation(context);
						transeletion.openConnection();
						dg.generateWarningDialog(transeletion.
								getTranselationTextByTextIdentifier("lblFunctionAvaialableForSmartPhone"));
						transeletion.closeConnection();
					}
				}
			});

			holder.btnCheck.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {

					if(check[position] == true){
						check[position] = false;
						holder.btnInvite.setVisibility(View.VISIBLE);
						holder.btnCheck.setVisibility(View.INVISIBLE);
						counter--;
						btnInviteContact.setText(txtInvite+" ("+counter+")");
						numberList.remove(contactList.get(position).getPhone());
					}
				}
			});

			return row;
		}

		class ViewHolder
		{
			TextView txtContactName;
			ImageView imgContact;
			Button	btnInvite;
			Button btnCheck;
		}

	}//END ContactListAdapter class

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.btnInviteContact:
			if(!MainActivity.isTab && 
					!(metrics.heightPixels == ICommonUtils.TAB_HEIGHT && metrics.widthPixels == ICommonUtils.TAB_WIDTH &&
					metrics.densityDpi == ICommonUtils.TAB_DENISITY)
					&& this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY)){
				Intent smsIntent = new Intent(Intent.ACTION_VIEW);
				if(numberList.size() != 0)
					smsIntent.putExtra("address", ""+numberList);
				smsIntent.putExtra("sms_body", message);
				smsIntent.setType("vnd.android-dir/mms-sms");
				startActivity(smsIntent);
			} else {
				DialogGenerator dg = new DialogGenerator(this);
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				dg.generateWarningDialog(transeletion.
						getTranselationTextByTextIdentifier("lblFunctionAvaialableForSmartPhone"));
				transeletion.closeConnection();
			}
			break;
		}
	}	
	
	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}
}
