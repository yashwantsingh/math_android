package com.mathfriendzy.controller.multifriendzy.contacts;

import android.graphics.Bitmap;

public class ContactBeans
{
	String id;
	String name;
	String phone;
	Bitmap photo;
	
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Bitmap getPhoto() {
		return photo;
	}
	public void setPhoto(Bitmap photo) {
		this.photo = photo;
	}
	
	
}
