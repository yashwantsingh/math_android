package com.mathfriendzy.controller.multifriendzy;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.multifriendzy.MultiFriendzyImpl;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;
import static com.mathfriendzy.utils.ICommonUtils.MULTIFRIENDZY_PROBLEM_TYPE;

/**
 * This class set problem type with its difficulty
 * @author Yashwant Singh
 *
 */
public class MultiFriendzyProblemType extends ActBaseClass implements OnClickListener
{
	private TextView labelTop 				= null;
	private TextView txtChooseDifficulty 	= null;
	private TextView txtChooseProblemType 	= null;
	private Button   btnGo 					= null;
	private TextView txtNumber 				= null;
	private TextView txtFractions 			= null;
	private TextView txtDecimals 			= null;
	private TextView txtNegatives 			= null;

	private Spinner spinnerDifficutlty 		= null;

	private ImageView imgCheck1Number		= null;
	private ImageView imgCheck2Number 		= null;
	private ImageView imgCheck1Fractions 	= null;
	private ImageView imgCheck2Fractions 	= null;
	private ImageView imgCheck1Decimals 	= null;
	private ImageView imgCheck2Decimals 	= null;
	private ImageView imgCheck1Negatives 	= null;
	private ImageView imgCheck2Negatives 	= null;

	private final String TAG = this.getClass().getSimpleName();

	private final int MAX_CHECK_IMAGE = 8;// maximum number of check images 
	private ArrayList<String> difficultyList = null;
	private ArrayList<Boolean> checkList     = null;
	private ArrayList<Integer> categoryIdList = null;

	//added for friendzy challenge
	private boolean isCallFromFriendzyChallenge = false;
	private String selectedDifficulty = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_multi_friendzy_problem_type);

		if(MULTIFRIENDZY_PROBLEM_TYPE)
			Log.e(TAG, "inside onCreate()");

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);  

		if(metrics.heightPixels == ICommonUtils.TAB_HEIGHT && metrics.widthPixels == ICommonUtils.TAB_WIDTH && metrics.densityDpi == ICommonUtils.TAB_DENISITY)
		{
			setContentView(R.layout.activity_multi_friendzy_problem_type_tab_low_denisity);
		}

		difficultyList = new ArrayList<String>();
		checkList      = new ArrayList<Boolean>();

		for(int i = 0 ; i < MAX_CHECK_IMAGE ; i ++ )
		{
			checkList.add(false);
		}

		//for identify the calling
		//added when friendzy challenge required
		isCallFromFriendzyChallenge = this.getIntent().getBooleanExtra("isCallFromFriendzyChallenge", false);
		//end changes

		this.setWidgetsReferences();
		this.setTextFromTranslation();
		//for friendzy challenge
		this.setCheckedProblemTypeForFriendzyChallenge();
		//end changes
		this.setSpinnerValue();

		this.setListenerOnWidgets();

		if(MULTIFRIENDZY_PROBLEM_TYPE)
			Log.e(TAG, "outside onCreate()");

	}

	/**
	 * This method getIntent Values which are set in previous screen
	 */
	private void setCheckedProblemTypeForFriendzyChallenge(){
		try{
			if(isCallFromFriendzyChallenge){
				String selectedEquation = this.getIntent().getStringExtra("selectedEquations");
                if(!MathFriendzyHelper.isEmpty(selectedEquation)){
					selectedDifficulty = this.getSelectedDifficulty(selectedEquation.substring
                            (0 , selectedEquation.indexOf("&")));
					String equation = selectedEquation.substring
                            (selectedEquation.indexOf("&") + 1, selectedEquation.length());
					this.setChecked(equation);
				}
			}else{
				this.setChecked("1,2");
			}
		}
		catch(Exception e){
            e.printStackTrace();
			Log.e(TAG, "Error while setting in case of create challenge");
		}
	}

	/**
	 * This method set checked value
	 * @param equations
	 */
	private void setChecked(String equations){
		Log.e(TAG , "equation " + equations);
        if(equations.contains("1,2"))
			this.setCheckImage(0, imgCheck1Number);
		if(equations.contains("3,4"))
			this.setCheckImage(1, imgCheck2Number);
		if(equations.contains("5"))
			this.setCheckImage(2, imgCheck1Fractions);
		if(equations.contains("6"))
			this.setCheckImage(3, imgCheck2Fractions);
		if(equations.contains("7"))
			this.setCheckImage(4, imgCheck1Decimals);
		if(equations.contains("8"))
			this.setCheckImage(5, imgCheck2Decimals);
		if(equations.contains("9"))
			this.setCheckImage(6, imgCheck1Negatives);
		if(equations.contains("10"))
			this.setCheckImage(7, imgCheck2Negatives);
	}

	private String getSelectedDifficulty(String str){
		if(str.equals("1"))
			return difficultyList.get(0);
		else if(str.equals("2"))
			return difficultyList.get(1);
		else if(str.equals("3"))
			return difficultyList.get(2);
		else if(str.equals("4"))
			return difficultyList.get(3);
		else
			return difficultyList.get(0);
	}

	/**
	 * This  method set the widgets references from the layout
	 */
	private void setWidgetsReferences() 
	{
		if(MULTIFRIENDZY_PROBLEM_TYPE)
			Log.e(TAG, "inside setWidgetsReferences()");

		labelTop = (TextView) findViewById(R.id.labelTop);

		txtChooseDifficulty 	= (TextView) findViewById(R.id.txtChooseDifficulty);
		txtChooseProblemType 	= (TextView) findViewById(R.id.txtChooseProblemType);
		txtNumber 				= (TextView) findViewById(R.id.txtNumber);
		txtFractions 			= (TextView) findViewById(R.id.txtFractions);
		txtDecimals 			= (TextView) findViewById(R.id.txtDecimals);
		txtNegatives 			= (TextView) findViewById(R.id.txtNegatives);

		imgCheck1Number 		= (ImageView) findViewById(R.id.imgCheck1Number);
		imgCheck2Number 		= (ImageView) findViewById(R.id.imgCheck2Number);
		imgCheck1Fractions 		= (ImageView) findViewById(R.id.imgCheck1Fractions);
		imgCheck2Fractions 		= (ImageView) findViewById(R.id.imgCheck2Fractions);
		imgCheck1Decimals 		= (ImageView) findViewById(R.id.imgCheck1Decimals);
		imgCheck2Decimals 		= (ImageView) findViewById(R.id.imgCheck2Decimals);
		imgCheck1Negatives 		= (ImageView) findViewById(R.id.imgCheck1Negatives);
		imgCheck2Negatives 		= (ImageView) findViewById(R.id.imgCheck2Negatives);

		btnGo 				    = (Button)    findViewById(R.id.btnGo);

		spinnerDifficutlty = (Spinner) findViewById(R.id.spinnerDifficutlty);

		if(MULTIFRIENDZY_PROBLEM_TYPE)
			Log.e(TAG, "outside setWidgetsReferences()");
	}


	/**
	 * This method set the text from translation table
	 */
	private void setTextFromTranslation() 
	{
		if(MULTIFRIENDZY_PROBLEM_TYPE)
			Log.e(TAG, "inside setTextFromTranslation()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		labelTop.setText(transeletion.getTranselationTextByTextIdentifier("lblChooseProblemType"));
		txtChooseDifficulty.setText(transeletion.getTranselationTextByTextIdentifier("lblChooseDifficulty") + ":");
		txtChooseProblemType.setText(transeletion.getTranselationTextByTextIdentifier("lblChoose") + " "
				+ transeletion.getTranselationTextByTextIdentifier("lblChooseProblemType") + ":");
		txtNumber.setText(transeletion.getTranselationTextByTextIdentifier("lblNumbers"));
		txtFractions.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleFractions"));
		txtDecimals.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleDecimals"));
		txtNegatives.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNegatives"));
		btnGo.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGo"));

		difficultyList.add(transeletion.getTranselationTextByTextIdentifier("difficultyTypeEasy"));
		difficultyList.add(transeletion.getTranselationTextByTextIdentifier("difficultyTypeMedium"));
		difficultyList.add(transeletion.getTranselationTextByTextIdentifier("difficultyTypeHard"));
		difficultyList.add(transeletion.getTranselationTextByTextIdentifier("difficultyTypeExpert"));

		//for friendzy changes
		selectedDifficulty = transeletion.getTranselationTextByTextIdentifier("difficultyTypeEasy");
		transeletion.closeConnection();
		if(MULTIFRIENDZY_PROBLEM_TYPE)
			Log.e(TAG, "outside setTextFromTranslation()");
	}

	/**
	 * This method set the data to the choose difficulty spinner
	 */
	private void setSpinnerValue() 
	{
		if(MULTIFRIENDZY_PROBLEM_TYPE)
			Log.e(TAG, "inside setSpinnerValue()");

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this , 
				R.layout.spinner_text_layout , difficultyList);
		spinnerDifficutlty.setAdapter(adapter);
		
		if(isCallFromFriendzyChallenge){
			spinnerDifficutlty.setSelection(adapter.getPosition(selectedDifficulty));
		}

		if(MULTIFRIENDZY_PROBLEM_TYPE)
			Log.e(TAG, "outside setSpinnerValue()");
	}

	/**
	 * This method set the widgets references from layout
	 */
	private void setListenerOnWidgets() 
	{
		if(MULTIFRIENDZY_PROBLEM_TYPE)
			Log.e(TAG, "inside setListenerOnWidgets()");

		btnGo.setOnClickListener(this);
		imgCheck1Number.setOnClickListener(this);
		imgCheck2Number.setOnClickListener(this);
		imgCheck1Fractions.setOnClickListener(this);
		imgCheck2Fractions.setOnClickListener(this);
		imgCheck1Decimals.setOnClickListener(this);
		imgCheck2Decimals.setOnClickListener(this);
		imgCheck1Negatives.setOnClickListener(this);
		imgCheck2Negatives.setOnClickListener(this);

		if(MULTIFRIENDZY_PROBLEM_TYPE)
			Log.e(TAG, "outside setListenerOnWidgets()");
	}


	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.btnGo :
			this.clickOnGo();
			break;
		case R.id.imgCheck1Number :
			this.setCheckImage(0, imgCheck1Number);
			break;
		case R.id.imgCheck2Number :
			this.setCheckImage(1, imgCheck2Number);
			break;
		case R.id.imgCheck1Fractions :
			this.setCheckImage(2, imgCheck1Fractions);
			break;
		case R.id.imgCheck2Fractions :
			this.setCheckImage(3, imgCheck2Fractions);
			break;
		case R.id.imgCheck1Decimals :
			this.setCheckImage(4, imgCheck1Decimals);
			break;
		case R.id.imgCheck2Decimals :
			this.setCheckImage(5, imgCheck2Decimals);
			break;
		case R.id.imgCheck1Negatives :
			this.setCheckImage(6, imgCheck1Negatives);
			break;
		case R.id.imgCheck2Negatives :
			this.setCheckImage(7, imgCheck2Negatives);
			break;
		}
	}

	/**
	 * This method call when click on check images
	 * @param index
	 * @param imgImage
	 */
	private void setCheckImage(int index , ImageView imgImage)
	{
		if(MULTIFRIENDZY_PROBLEM_TYPE)
			Log.e(TAG, "inside setCheckImage()");

		if(checkList.get(index).booleanValue() == false)
		{
			imgImage.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
			checkList.set(index, true);
		}
		else
		{
			imgImage.setBackgroundResource(R.drawable.mf_check_box_ipad);
			checkList.set(index, false);
		}

		if(MULTIFRIENDZY_PROBLEM_TYPE)
			Log.e(TAG, "outside setCheckImage()");
	}


	/**
	 * This method call when player click on go button to play round
	 */
	private void clickOnGo(){
		categoryIdList = new ArrayList<Integer>();
		for(int i = 0 ; i < checkList.size() ; i ++ )
		{
			if(checkList.get(i).booleanValue())
				this.setCategoryIdList(i);
		}

		if(categoryIdList.size() == 0)
		{
			categoryIdList.add(1);
			categoryIdList.add(2);
		}

		MultiFriendzyImpl multifrinedzyImpl = new MultiFriendzyImpl(this);
		multifrinedzyImpl.openConn();
		ArrayList<Integer> mathOperationIdList = multifrinedzyImpl.getEquationCategoryId(categoryIdList, this.getDifficultyId());
		multifrinedzyImpl.closeConn();

		//changes for friendzy challenge
		if(isCallFromFriendzyChallenge){
			Intent intent = new Intent();
			intent.putIntegerArrayListExtra("selectedCategories", categoryIdList);
			intent.putExtra("dificullty",  this.getDifficultyId());
			this.setResult(4, intent);
			finish();
		}
		else{
			Intent intent = new Intent(this , MultiFriendzyEquationSolve.class);
			intent.putIntegerArrayListExtra("selectedCategories", mathOperationIdList);
			startActivity(intent);
		}
	}

    @Override
    public void onBackPressed() {
        if(isCallFromFriendzyChallenge){
            this.clickOnGo();
        }
        super.onBackPressed();
    }

    /**
	 * This method return the difficulty id
	 * @return
	 */
	private int getDifficultyId()
	{
		if(spinnerDifficutlty.getSelectedItem().toString().equals(difficultyList.get(0)))
			return 1;
		else if(spinnerDifficutlty.getSelectedItem().toString().equals(difficultyList.get(1)))
			return 2;
		else if(spinnerDifficutlty.getSelectedItem().toString().equals(difficultyList.get(2)))
			return 3;
		else 
			return 4;
	}

	/**
	 * This methid set and return the category id list
	 * @return
	 */
	private void setCategoryIdList(int index)
	{
		switch(index)
		{
		case 0:
			categoryIdList.add(1);
			categoryIdList.add(2);
			break;
		case 1:
			categoryIdList.add(3);
			categoryIdList.add(4);
			break;
		case 2:
			categoryIdList.add(5);
			break;
		case 3:
			categoryIdList.add(6);
			break;
		case 4:
			categoryIdList.add(7);
			break;
		case 5:
			categoryIdList.add(8);
			break;
		case 6:
			categoryIdList.add(9);
			break;
		case 7:
			categoryIdList.add(10);
			break;

		}
	}
	
	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}
}
