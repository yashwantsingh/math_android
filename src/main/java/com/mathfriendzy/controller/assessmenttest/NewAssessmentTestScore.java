package com.mathfriendzy.controller.assessmenttest;

/**
 * This class hold the time and score for each sub standards id
 * @author Yashwant Singh
 *
 */
public class NewAssessmentTestScore {
	private long totalTime = 0;
	private int totalScore = 0;
	
	public long getTotalTime() {
		return totalTime;
	}
	public void setTotalTime(long totalTime) {
		this.totalTime = totalTime;
	}
	public int getTotalScoce() {
		return totalScore;
	}
	public void setTotalScoce(int totalScore) {
		this.totalScore = totalScore;
	}
}
