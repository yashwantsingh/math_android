package com.mathfriendzy.controller.assessmenttest;

import static com.mathfriendzy.utils.ICommonUtils.ASSESSMENT_PLAY_SCREEN;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mathfriendzy.R;

import com.mathfriendzy.controller.learningcenter.schoolcurriculum.SchoolCurriculumEquationSolveBase;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.assessmenttest.AssessmentTestImpl;
import com.mathfriendzy.model.assessmenttest.CatagoriesDto;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.MathResultTransferObj;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.MathEquationTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SchoolCurriculumLearnignCenterimpl;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.WordProblemQuestionTransferObj;
import com.mathfriendzy.model.schoolcurriculum.singlefriendzy.SingleFriendzySchoolCurriculumImpl;
import com.mathfriendzy.model.spanishchanges.SpanishChangesImpl;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ICommonUtils;
import com.mathfriendzy.utils.PlaySound;

/**
 * This class for play assessment test
 * @author Yashwant Singh
 *
 */
public class AssessmentPlayActivity extends SchoolCurriculumEquationSolveBase {

    private Button btnGoRight 					= null;
    private Button btnGoLeft  					= null;
    private TextView txtNumberOfQuestionsLeft 	= null;

    private final String TAG = this.getClass().getSimpleName();
    private int MAX_QUESTION = 10;

    //grade set in AssessmentTestActivity and send using intent
    private int selectedGrade = 1;
    //userId and playerId set in setPlayerDetail from shared preff
    private String userId;
    private String playerId;

    private ArrayList<WordProblemQuestionTransferObj>
            questionAnswerList = null;//set from database
    private int index = 0 ;
    ArrayList<Integer> questionIds = null;

    //for sound
    //PlaySound playSound = null;

    // for saving play data
    private boolean isFillIn = false;//for fill in
    private String answerByUser = "";
    private int isCorrectAnsByUser = 0;

    //for set animation left to right or right to left
    private boolean isClickOnLeft = false;
    private boolean isClickOnGo   = false;

    //for animation on first question
    private boolean isDirectFromMainScreen = false;

    //added for direct play from main screen
    private MathAssessmentTestResultObj mathAssessmentTestResult    = null;
    private ArrayList<MathEquationTransferObj> mathEquationDataList = null;

    //changes for NewAssessment
    private ArrayList<CatagoriesDto> categoryInfoList = null;
    private int selectedStandardId = 1;
    //for holding the substandardList with question ids
    private HashMap<Integer, ArrayList<Integer>> subStandardListWithQuestionIds = null;
    private int numberOfTimesQuestionGet = 0;
    private final int MAX_QUESTION_FROM_DATABASE_SECOND_TIME = 20;
    private boolean isMaxQuestionCompleted = false;//this is true when the number of question is 20 in second time
    @SuppressLint("UseSparseArrays")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assessment_play);

        if(ASSESSMENT_PLAY_SCREEN)
            Log.e(TAG, "inside onCreate()");

        //for tts
        tts = new TextToSpeech(this, this);

        isTab = getResources().getBoolean(R.bool.isTablet);

        playSound = new PlaySound(this);
        //playSound.playSoundForSchoolSurriculumEquationSolve(this);

        questionIds = new ArrayList<Integer>();
        random      = new Random();

        playDataList = new ArrayList<MathEquationTransferObj>();

        //changes for new Assessment
        subStandardListWithQuestionIds = new HashMap<Integer, ArrayList<Integer>>();

        this.setWidgetsreferences();
        this.setListenerOnWidgetsForAssessment();
        this.setTextFromTranslationForAssessment();
        this.setPlayerDetail();
        //this function call after setPlayerDetail function because userId , and playerId are set in that function
        //for getting question from database
        this.getIntentValueForAssessment();

        //changes for direct play
        //this.getQuestionFromIdsDatabase();


        if(ASSESSMENT_PLAY_SCREEN)
            Log.e(TAG, "outside onCreate()");
    }

    /**
     * This method get the intent value which are set in previous screen
     */
    @SuppressWarnings("unchecked")
    private void getIntentValueForAssessment(){

        if(ASSESSMENT_PLAY_SCREEN)
            Log.e(TAG, "inside getIntentValueForAssessment()");

        if(this.getIntent().getBooleanExtra("isDirectFromMain", false)){

            isDirectFromMainScreen = true;

            mathAssessmentTestResult = (MathAssessmentTestResultObj) this.getIntent()
                    .getSerializableExtra("mathAssessmentTestResultTableData");
            mathEquationDataList     = (ArrayList<MathEquationTransferObj>)
                    this.getIntent().getSerializableExtra("alreadyPlayDataList");

            for(int i = 0 ; i < mathEquationDataList.size() ; i ++ ){
                MathEquationTransferObj mathObjData = new MathEquationTransferObj();
                mathObjData.setEqautionId(mathEquationDataList.get(i).getEqautionId());
                mathObjData.setStartDate(mathEquationDataList.get(i).getStartDate());
                mathObjData.setEndData(mathEquationDataList.get(i).getEndData());
                mathObjData.setCatId(mathEquationDataList.get(i).getCatId());
                mathObjData.setSubCatId(mathEquationDataList.get(i).getSubCatId());
                mathObjData.setPoints(mathEquationDataList.get(i).getPoints());
                mathObjData.setIsAnswerCorrect(mathEquationDataList.get(i).getIsAnswerCorrect());
                mathObjData.setUserAnswer(mathEquationDataList.get(i).getUserAnswer());
                //for assessment option selected
                if(mathEquationDataList.get(i).getIsAnswerCorrect() == 0
                        && mathEquationDataList.get(i).getUserAnswer().equals("1"))
                    mathObjData.setUserAnserForAssessment("");
                else
                    mathObjData.setUserAnserForAssessment(mathEquationDataList.get(i).getUserAnswer());

                mathObjData.setQuestionAttempt(true);

                SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
                schoolImpl.openConnection();
                //get question from database into questionObj object
                questionObj  = schoolImpl.getQuestionByQuestionId(mathEquationDataList.get(i).getEqautionId());
                schoolImpl.closeConnection();
                mathObjData.setQuestionObj(questionObj);
                //end

                playDataList.add(mathObjData);
            }

            String id = "";
            //set remaining question ids
            for(int i = 0 ; i < mathAssessmentTestResult.getQuestionRemaining().length() ; i ++ ){
                if(mathAssessmentTestResult.getQuestionRemaining().charAt(i) == ','){
                    MathEquationTransferObj mathObjData = new MathEquationTransferObj();
                    mathObjData.setEqautionId(Integer.parseInt(id));
                    mathObjData.setQuestionAttempt(false);
                    playDataList.add(mathObjData);
                    id = "";
                }else{
                    id = id + mathAssessmentTestResult.getQuestionRemaining().charAt(i);
                }
            }

            selectedGrade = mathAssessmentTestResult.getGrade();
            //changes for Spanish
            selectedPlayGrade = mathAssessmentTestResult.getGrade();
            //end changes

            MAX_QUESTION     = playDataList.size();
            //MAX_QUESTION = 5;//for testing
            numberOfQuestion = mathEquationDataList.size() ;

            //changes for NewAssessment
            selectedStandardId = mathAssessmentTestResult.getStandardId();
            subStandardListWithQuestionIds = this.getSubStandardsQuestionIds
                    (mathAssessmentTestResult.getSetSubStandardsQuestionJson());
            //end changes
            //this.printsSubStandardData(subStandardListWithQuestionIds);

            this.setRemainingQuestions();
            this.getQuestionFromDatabase();//get question from database and show it

        }else{
            selectedGrade 		= this.getIntent().getIntExtra("selectedGrade", 1);
            //changes for Spanish
            selectedPlayGrade 	= this.getIntent().getIntExtra("selectedGrade", 1);

            //changes for New Assessment
            categoryInfoList = (ArrayList<CatagoriesDto>) this.getIntent()
                    .getSerializableExtra("categoriesInfoList");
            selectedStandardId = this.getIntent().getIntExtra("selectedStandardId", 1);
            this.getQuestionFromIdsDatabase(categoryInfoList);
        }

        if(ASSESSMENT_PLAY_SCREEN)
            Log.e(TAG, "outside getIntentValueForAssessment()");
    }

    /**
     * This method set the player detail and also set the userId and playerId
     */
    @SuppressWarnings("deprecation")
    private void setPlayerDetail(){

        if(ASSESSMENT_PLAY_SCREEN)
            Log.e(TAG, "inside setPlayerDetail()");

        SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);

        userId   = sharedPreffPlayerInfo.getString("userId", "");
        playerId = sharedPreffPlayerInfo.getString("playerId", "");

        String fullName = sharedPreffPlayerInfo.getString("playerName", "");
        String playerName = fullName.substring(0,(fullName.indexOf(" ") + 2));
        txtPlayerName.setText(playerName + ".");

        Country country = new Country();
        String coutryIso = country.getCountryIsoByCountryName
                (sharedPreffPlayerInfo.getString("countryName", ""), this);

        try{
            if(!(coutryIso.equals("-")))
                imgFlag.setBackgroundDrawable(Drawable.createFromStream(getAssets().open(getResources().
                        getString(R.string.countryImageFolder) +"/"
                        + coutryIso + ".png"), null));
        }
        catch (IOException e){
            Log.e(TAG, "Inside set player detail Error No Image Found " + e.toString());
        }

        if(ASSESSMENT_PLAY_SCREEN)
            Log.e(TAG, "outside setPlayerDetail()");

    }

    /**
     * This method get questions from local database
     * and set it to list
     * @param categoryInfoList
     */
    private void getQuestionFromIdsDatabase(ArrayList<CatagoriesDto> categoryInfoList){

        if(ASSESSMENT_PLAY_SCREEN)
            Log.e(TAG, "inside getQuestionFromDatabase()");

        do{
            this.getQuestionIdsFromDatabese(categoryInfoList , questionIds);
        }while(questionIds.size() < 20 && questionIds.size() != 0);

        //create final question list
        if(questionIds.size() > 0){
            this.createFinalQuaetionListFromDatabase(questionIds);
        }else{
            this.notSufficientQuestionDailog();
        }

        //Log.e(TAG, "finalListSize " + questionIds.size());
        //this.printsSubStandardData(subStandardListWithQuestionIds);

        if(ASSESSMENT_PLAY_SCREEN)
            Log.e(TAG, "outside getQuestionFromDatabase()");
    }

    /**
     * This method getQuestionIds From Database
     * @return
     */
    @SuppressLint("UseSparseArrays")
    private void getQuestionIdsFromDatabese(ArrayList<CatagoriesDto> categoryInfoList
            ,ArrayList<Integer> questionIdList){
        //gradeList.subList(8, 13).clear();//changes for ticket #57

        //changes for max 20 questions
        numberOfTimesQuestionGet ++ ;
        int startIndex = 0;

        //changes for Spanish
        if(CommonUtils.getUserLanguageCode(this) == CommonUtils.ENGLISH){
            SingleFriendzySchoolCurriculumImpl schoolImpl = new SingleFriendzySchoolCurriculumImpl(this);
            schoolImpl.openConnection();
            for(int i = 0 ; i < categoryInfoList.size() ; i ++ ){
                ArrayList<Integer> questionIdsList = schoolImpl.getQuestionId(categoryInfoList.get(i).getCatId(),
                        categoryInfoList.get(i).getSubCatId(), questionIdList);
                questionIdList.addAll(questionIdsList);

                if(questionIdList.size() > MAX_QUESTION_FROM_DATABASE_SECOND_TIME
                        && numberOfTimesQuestionGet > 1){
                    isMaxQuestionCompleted = true;
                    startIndex = questionIdList.size() - MAX_QUESTION_FROM_DATABASE_SECOND_TIME;
                    questionIdList.removeAll(questionIdsList);
                    questionIdList.addAll(questionIdsList.subList(startIndex , questionIdsList.size()));
                }

                //for holding the question id for each sub standards
                if(subStandardListWithQuestionIds.containsKey(categoryInfoList.get(i).getSubStdId())){
                    //changes for max 20 question
                    if(isMaxQuestionCompleted){
                        //Log.e(TAG, "startIndex " + startIndex + " size " + questionIdsList.size());
                        subStandardListWithQuestionIds.get(categoryInfoList.get(i).getSubStdId())
                                .addAll(questionIdsList.subList(startIndex , questionIdsList.size()));
                    }else{
                        subStandardListWithQuestionIds.get(categoryInfoList.get(i).getSubStdId())
                                .addAll(questionIdsList);
                    }
                }else{
                    subStandardListWithQuestionIds.put(categoryInfoList.get(i).getSubStdId(),
                            questionIdsList);
                }
            }
            schoolImpl.closeConnection();

        }else{
            SpanishChangesImpl schoolImpl = new SpanishChangesImpl(this);
            schoolImpl.openConn();
            for(int i = 0 ; i < categoryInfoList.size() ; i ++ ){
                ArrayList<Integer> questionIdsList = schoolImpl.getQuestionId(categoryInfoList.get(i).getCatId(),
                        categoryInfoList.get(i).getSubCatId(), questionIdList);
                questionIdList.addAll(questionIdsList);

                if(questionIdList.size() > MAX_QUESTION_FROM_DATABASE_SECOND_TIME
                        && numberOfTimesQuestionGet > 1){
                    isMaxQuestionCompleted = true;
                    startIndex = questionIdList.size() - MAX_QUESTION_FROM_DATABASE_SECOND_TIME;
                    questionIdList.removeAll(questionIdsList);
                    questionIdList.addAll(questionIdsList.subList(startIndex , questionIdsList.size()));
                }

                //for holding the question id for each sub standards
                if(subStandardListWithQuestionIds.containsKey(categoryInfoList.get(i).getSubStdId())){
                    //changes for max 20 question
                    if(isMaxQuestionCompleted){
                        subStandardListWithQuestionIds.get(categoryInfoList.get(i).getSubStdId())
                                .addAll(questionIdsList.subList(startIndex , questionIdsList.size()));
                    }else{
                        subStandardListWithQuestionIds.get(categoryInfoList.get(i).getSubStdId())
                                .addAll(questionIdsList);
                    }
                }else{
                    subStandardListWithQuestionIds.put(categoryInfoList.get(i).getSubStdId(),
                            questionIdsList);
                }
            }
            schoolImpl.closeConn();
        }
    }

    /**
     * Create final question list which is to be display for player
     * @param questionIds
     */
    private void createFinalQuaetionListFromDatabase(ArrayList<Integer> questionIds){

		/*AssessmentTestImpl assessmentImpl = new AssessmentTestImpl(this);
		assessmentImpl.openConnection();
		MAX_QUESTION = assessmentImpl.getRequiredNumberOfQuestionForAssessmentTest(selectedGrade);
		assessmentImpl.closeConnection();*/

        //changes for max 20 questions
        if(numberOfTimesQuestionGet > 1)
            MAX_QUESTION = 20;
        else
            MAX_QUESTION = questionIds.size();

        //for(int i = 0 ; i < questionIds.size() ; i ++ ){
        for(int i = 0 ; i < MAX_QUESTION ; i ++ ){//changes for max 20 questions
            MathEquationTransferObj mathObjData = new MathEquationTransferObj();
            mathObjData.setEqautionId(questionIds.get(i));
            mathObjData.setQuestionAttempt(false);
            playDataList.add(mathObjData);
        }
        this.setRemainingQuestions();
        this.getQuestionFromDatabase();//get question from database and show it
    }

    /**
     * This method open dialog when user does not have sufficient question to be played
     */
    private void notSufficientQuestionDailog(){
        DialogGenerator dg = new DialogGenerator(this);
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        dg.generateWarningDialogForNoSufficientQuestionForSingleFriendzy(transeletion.
                getTranselationTextByTextIdentifier("lblSorryYouDoNotHaveQuestions") , this , 1);
        transeletion.closeConnection();
    }

    /**
     * This method set the widgets references from layout to object
     */
    private void setWidgetsreferences(){

        if(ASSESSMENT_PLAY_SCREEN)
            Log.e(TAG, "inside setWidgetsreferences()");

        //changes for Spanish
        this.setLanguageCode();
        //end changes

        mfTitleHomeScreen = (TextView) findViewById(R.id.mfTitleHomeScreen);
        txtPlayerName = (TextView) findViewById(R.id.txtPlayerName);
        imgFlag   = (ImageView) findViewById(R.id.imgFlag);

        questionLayout   = (RelativeLayout) findViewById(R.id.questionLayout);

        relativeBottomBarWithoutFillIn = (RelativeLayout) findViewById(R.id.relativeBottomBarWithoutFillIn);
        relativeBottomBarFillIn = (RelativeLayout) findViewById(R.id.relativeBottomBarFillIn);

        btnRoughWork = (Button) findViewById(R.id.btnRoughWork);

        primaryKeyboard  = (LinearLayout) findViewById(R.id.primaryKeyboard);
        secondryKeyboard = (LinearLayout) findViewById(R.id.secondryKeyboard);

        ansBox = (TextView) findViewById(R.id.ansBox);
        btnSpace = (Button) findViewById(R.id.btnSpace);
        btnsecondryKeyBoard = (Button) findViewById(R.id.btnsecondryKeyBoard);
        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);
        btn4 = (Button) findViewById(R.id.btn4);
        btn5 = (Button) findViewById(R.id.btn5);
        btn6 = (Button) findViewById(R.id.btn6);
        btn7 = (Button) findViewById(R.id.btn7);
        btn8 = (Button) findViewById(R.id.btn8);
        btn9 = (Button) findViewById(R.id.btn9);
        btn0 = (Button) findViewById(R.id.btn0);
        btnSynchronized = (Button) findViewById(R.id.btnSynchronized);
        btnBackArrow = (Button) findViewById(R.id.btnBackArrow);

        btnNumber = (Button) findViewById(R.id.btnNumber);
        btnDot = (Button) findViewById(R.id.btnDot);
        btnComma = (Button) findViewById(R.id.btnComma);
        btnMinus = (Button) findViewById(R.id.btnMinus);
        btnPlus = (Button) findViewById(R.id.btnPlus);
        btnDevide = (Button) findViewById(R.id.btnDevide);
        btnMultiply = (Button) findViewById(R.id.btnMultiply);
        btnCollon = (Button) findViewById(R.id.btnCollon);
        btnLessThan = (Button) findViewById(R.id.btnLessThan);
        btnGreaterThan = (Button) findViewById(R.id.btnGreaterThan);
        btnEqual = (Button) findViewById(R.id.btnEqual);
        btnRoot = (Button) findViewById(R.id.btnRoot);
        btnDevider = (Button) findViewById(R.id.btnDevider);
        btnOpeningBracket = (Button) findViewById(R.id.btnOpeningBracket);
        btnClosingBracket = (Button) findViewById(R.id.btnClosingBracket);
        btnSecandryDelete = (Button) findViewById(R.id.btnSecandryDelete);

        questionAnswerLayout = (RelativeLayout) findViewById(R.id.questionAnswerLayout);

        scrollView        = (ScrollView) findViewById(R.id.scrollView);

        btnGoRight        			= (Button) findViewById(R.id.btnGoRight);
        btnGoLeft         			= (Button) findViewById(R.id.btnGoLeft);
        txtNumberOfQuestionsLeft    = (TextView) findViewById(R.id.txtNumberOfQuestionsLeft);

        //added for emailSend for wrong question
        btnEmailSendForWrongQuestion1 = (Button) findViewById(R.id.btnEmailSendForWrongQuestion1);

        if(ASSESSMENT_PLAY_SCREEN)
            Log.e(TAG, "outside setWidgetsreferences()");
    }

    /**
     * This method set listener on widgets
     */
    private void setListenerOnWidgetsForAssessment(){

        if(ASSESSMENT_PLAY_SCREEN)
            Log.e(TAG, "inside setListenerOnWidgetsForAssessment()");

        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btn9.setOnClickListener(this);
        btn0.setOnClickListener(this);

        btnSynchronized.setOnClickListener(this);
        btnSpace.setOnClickListener(this);
        btnsecondryKeyBoard.setOnClickListener(this);

        //for secondry keyboard
        btnNumber.setOnClickListener(this);
        btnDot.setOnClickListener(this);
        btnComma.setOnClickListener(this);
        btnMinus.setOnClickListener(this);
        btnPlus.setOnClickListener(this);
        btnDevide.setOnClickListener(this);
        btnMultiply.setOnClickListener(this);
        btnCollon.setOnClickListener(this);
        btnLessThan.setOnClickListener(this);
        btnGreaterThan.setOnClickListener(this);
        btnEqual.setOnClickListener(this);
        btnRoot.setOnClickListener(this);
        btnDevider.setOnClickListener(this);
        btnOpeningBracket.setOnClickListener(this);
        btnClosingBracket.setOnClickListener(this);
        btnSecandryDelete.setOnClickListener(this);
        btnRoughWork.setOnClickListener(this);

        btnGoRight.setOnClickListener(this);
        btnGoLeft.setOnClickListener(this);

        //added for emailSend for wrong question
        btnEmailSendForWrongQuestion1.setOnClickListener(this);

        if(ASSESSMENT_PLAY_SCREEN)
            Log.e(TAG, "outside setListenerOnWidgetsForAssessment()");
    }

    /**
     * This method set the test from translation
     */
    private void setTextFromTranslationForAssessment(){

        if(ASSESSMENT_PLAY_SCREEN)
            Log.e(TAG, "inside setTextFromTranslationForAssessment()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        mfTitleHomeScreen.setText(transeletion.getTranselationTextByTextIdentifier("lblAssessmentTest"));
        transeletion.closeConnection();

        if(ASSESSMENT_PLAY_SCREEN)
            Log.e(TAG, "outside setTextFromTranslationForAssessment()");
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){

            case R.id.btnGoRight:
                isClickOnLeft = false;
                //for first question animation , when user come directly from main screen
                isDirectFromMainScreen = false;
                //isClickOnGo   = true;
                this.clickOnGo();
                break;
            case R.id.btnGoLeft:
                //for first question animation , when user come directly from main screen
                isDirectFromMainScreen = false;
                if(numberOfQuestion > 0){
                    numberOfQuestion -- ;
                    isClickOnLeft = true;
                    this.setRemainingQuestions();

                    if(playSound != null)
                        playSound.playSoundForSchoolSurriculumPagePeel(this);

                    this.getQuestionFromDatabase();
                }
                break;
            case R.id.btn1:
                this.clickOnButton("1");
                break;
            case R.id.btn2:
                this.clickOnButton("2");
                break;
            case R.id.btn3:
                this.clickOnButton("3");
                break;
            case R.id.btn4:
                this.clickOnButton("4");
                break;
            case R.id.btn5:
                this.clickOnButton("5");
                break;
            case R.id.btn6:
                this.clickOnButton("6");
                break;
            case R.id.btn7:
                this.clickOnButton("7");
                break;
            case R.id.btn8:
                this.clickOnButton("8");
                break;
            case R.id.btn9:
                this.clickOnButton("9");
                break;
            case R.id.btn0:
                this.clickOnButton("0");
                break;
            case R.id.btnSpace:
                this.clickOnButton(" ");
                break;
            case R.id.btnSynchronized :
                this.clearAnsBox();
                break;
            case R.id.btnsecondryKeyBoard :
                primaryKeyboard.setVisibility(LinearLayout.GONE);
                secondryKeyboard.setVisibility(LinearLayout.VISIBLE);
                btnSecandryDelete.setVisibility(Button.VISIBLE);
                break;
            case R.id.btnNumber :
                primaryKeyboard.setVisibility(LinearLayout.VISIBLE);
                secondryKeyboard.setVisibility(LinearLayout.GONE);
                btnSecandryDelete.setVisibility(Button.GONE);
                break;
            case R.id.btnDot:
                this.clickOnButton(".");
                break;
            case R.id.btnMinus:
                this.clickOnButton("-");
                break;
            case R.id.btnComma:
                this.clickOnButton(",");
                break;
            case R.id.btnPlus:
                this.clickOnButton("+");
                break;
            case R.id.btnDevide:
                this.clickOnButton("/");
                break;
            case R.id.btnMultiply:
                this.clickOnButton("*");
                break;
            case R.id.btnCollon:
                this.clickOnButton(":");
                break;
            case R.id.btnLessThan:
                this.clickOnButton("<");
                break;
            case R.id.btnGreaterThan:
                this.clickOnButton(">");
                break;
            case R.id.btnEqual:
                this.clickOnButton("=");
                break;
            case R.id.btnRoot:
                this.clickOnButton("√");
                break;
            case R.id.btnDevider:
                this.clickOnButton("|");
                break;
            case R.id.btnOpeningBracket:
                this.clickOnButton("(");
                break;
            case R.id.btnClosingBracket:
                this.clickOnButton(")");
                break;
            case R.id.btnRoughWork:
                this.clickOnWorkAreaForAssessmentTest();
                break;
            case R.id.btnEmailSendForWrongQuestion1:
                this.sendEmailFroWrongQuestion(questionObj.getQuestionId() , this , null);
                break;
            case R.id.btnSecandryDelete:
                this.clearAnsBox();
                break;
        }
    }

	/*@Override
	public void cancelCurrentTimerAndSetTheResumeTime() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resumeTimerFromTimeSetAtCancelTime() {
		// TODO Auto-generated method stub

	}*/

    /**
     * This method call when user click on go
     */
    private void clickOnGo(){

        if(isFillIn){
            this.clickOnbtnGoForFillIn();
        }else{
            this.clickOnbtnGoForWithoutFillIn();
        }
    }

    /**
     * This method show next question
     */
    private void showNextQuestion(){
        isClickOnGo   = true;
        endTime = new Date();

        this.setplayDataToArrayList();

        //clear answer value entered by user
        answerValue = new StringBuilder("");

        if(numberOfQuestion < MAX_QUESTION - 1){
            numberOfQuestion ++ ;
            this.setRemainingQuestions();

            if(playSound != null)
                playSound.playSoundForSchoolSurriculumPagePeel(this);

            this.getQuestionFromDatabase();
        }else{

            //set in to CongratulationActivityForAssessmentTest bt Alex Sheet.
			/*playSound.playSoundForSchoolSurriculumForResultScreen(this);*/

            if(playSound != null)
                playSound.stopPlayer();

            this.callAfterCompletion();
        }
    }

    //changes for update player points in local when Internet is not connected
    //updated on 26 11 2014
    private void savePlayerDataWhenNoInternetConnected(){
        try{
            SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
            String userId;
            String playerId;
            if(sharedPreffPlayerInfo.getString("userId", "").equals("0"))
                userId = "0";
            else
                userId = sharedPreffPlayerInfo.getString("userId", "");

            if(sharedPreffPlayerInfo.getString("playerId", "").equals("0"))
                playerId = "0";
            else
                playerId = sharedPreffPlayerInfo.getString("playerId", "");

            MathResultTransferObj mathResultObj = new MathResultTransferObj();
            mathResultObj.setUserId(userId);
            mathResultObj.setPlayerId(playerId);
            mathResultObj.setTotalScore(points);
            mathResultObj.setCoins((int) (points * ICommonUtils.COINS_PER_POINT));
            mathResultObj.setLevel(MathFriendzyHelper.getPlayerCompleteLavel(playerId , this));
            MathFriendzyHelper.insertPlayerPlayedDataIntoLocalEarnedScore(mathResultObj, this);
        }catch(Exception e){
            e.printStackTrace();
        }
    }//end updation on 26 11 2014

    /**
     * This method call when user play all questions
     */
    @SuppressLint("UseSparseArrays")
    private void callAfterCompletion(){

        //changes for New Assessment
        //this map hold the score and time for each sub standards id
        HashMap<Integer, NewAssessmentTestScore> scoreAndTimeForSubStandsrs =
                new HashMap<Integer, NewAssessmentTestScore>();

        long totalTimeForSubStandard = 0;
        int  totalScore              = 0;

		/*
		 * calculate the sub standards time and score
		 */
        for(int subStandardId : subStandardListWithQuestionIds.keySet()){
            totalScore 				= 0;
            totalTimeForSubStandard = 0;

            for(int i = 0 ; i < subStandardListWithQuestionIds.get(subStandardId).size() ; i ++ ){
                for(int j = 0 ; j < playDataList.size() ; j ++ ){
                    if(subStandardListWithQuestionIds.get(subStandardId).get(i) ==
                            playDataList.get(j).getEqautionId()){
                        totalTimeForSubStandard = totalTimeForSubStandard + playDataList.get(j)
                                .getTimeTakenToAnswer();
                        if(playDataList.get(j).getIsAnswerCorrect() == 1)
                            totalScore ++ ;
                    }
                }
            }

            //calculate percentage for right answer
            if(totalScore > 0)
                totalScore = (100 * totalScore) / subStandardListWithQuestionIds.get(subStandardId).size();
            NewAssessmentTestScore subStandardsObj = new NewAssessmentTestScore();
            subStandardsObj.setTotalScoce(totalScore);
            subStandardsObj.setTotalTime(totalTimeForSubStandard);
            scoreAndTimeForSubStandsrs.put(subStandardId, subStandardsObj);
        }
		/*
		 * end calculation
		 */
        //end changes

        int correctScore = this.calCulateRightQuestionPercent();

        MathAssessmentTestResultObj assessmentTestResultObj = new MathAssessmentTestResultObj();
        assessmentTestResultObj.setUserId(userId);
        assessmentTestResultObj.setPlayerId(playerId);
        assessmentTestResultObj.setGrade(selectedGrade);
        assessmentTestResultObj.setDate(CommonUtils.formateDateIn24Hours(new Date()));
        assessmentTestResultObj.setCorrectScore(correctScore);
        assessmentTestResultObj.setProblems(("<equations>"
                + this.getEquationSolveXmlForInterNetNotConnectedForAssessment(playDataList) +
                "</equations>"));
        assessmentTestResultObj.setStandardId(selectedStandardId);
        assessmentTestResultObj.setSubStandarsTimeAndScore
                (("<testScore>" + this.getXmlForSubStandardsTimeAndScore(scoreAndTimeForSubStandsrs) + "</testScore>"));

        //if net not connected , Changes for net not connected , used when net connected at the mainScreen,
        //update on server
        assessmentTestResultObj.setSetSubStandardsQuestionJson
                (("<testScore>" + this.getXmlForSubStandardsTimeAndScore(scoreAndTimeForSubStandsrs) + "</testScore>"));
        //end changes

        AssessmentTestImpl implObj = new AssessmentTestImpl(this);
        implObj.openConnection();

        if(CommonUtils.isInternetConnectionAvailable(this)){
            //implObj.deleteFromWordAssessmentTestResult(assessmentTestResultObj);
            //changes for New Assessment Test
            implObj.deleteFromNewWordAssessmentTestResult(assessmentTestResultObj);

            new AddAssessmentScoreOnServer(assessmentTestResultObj ,
                    this).execute(null,null,null);
            this.savePlayerScoreOnserverAndInLocalDB();
        }else{
            assessmentTestResultObj.setIsCompleted(1);
            assessmentTestResultObj.setQuestionRemaining("");

			/*implObj.deleteFromWordAssessmentTestResult(assessmentTestResultObj);
			implObj.insertIntoWordAssessmentTestResult(assessmentTestResultObj);*/
            //changes for New Assessment Test
            implObj.deleteFromNewWordAssessmentTestResult(assessmentTestResultObj);
            implObj.insertIntoNewWordAssessmentTestResult(assessmentTestResultObj);
        }

        implObj.deleteFromWordAssessmentLastInfo(userId, playerId);
        implObj.insertIntoWordAssessmentLastInfo(userId, playerId,
                CommonUtils.formateDate(new Date()));
        implObj.closeConnection();

        this.savePlayerDataWhenNoInternetConnected();

        Intent intent = new Intent(this , CongratulationActivityForAssessmentTest.class);
        intent.putExtra("rightAnswerPercentage", correctScore);
        intent.putExtra("playDatalist", playDataList);
        intent.putExtra("selectedGrade", selectedPlayGrade);
        startActivity(intent);
    }

    /**
     * This method return the player points with its user id and player id and also coins
     * @return
     */
    private PlayerTotalPointsObj getPlayerPoints(){
        PlayerTotalPointsObj playerPoints = new PlayerTotalPointsObj();
        playerPoints.setUserId(userId);
        playerPoints.setPlayerId(playerId);
        playerPoints.setTotalPoints(points);
        playerPoints.setCoins((int) (points * ICommonUtils.COINS_PER_POINT));
        playerPoints.setCompleteLevel(MathFriendzyHelper
                .getPlayerCompleteLavel(playerId, this));
        playerPoints.setPurchaseCoins(0);
        return playerPoints;
    }

    /**
     * Update into local DB
     */
    private void inserIntoLocalDB(){
        LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
        learningCenterObj.openConn();
        PlayerTotalPointsObj playerPoints = this.getPlayerPoints();
        if(learningCenterObj.isPlayerTotalPointsExist(playerPoints.getPlayerId())){
            int points = playerPoints.getTotalPoints();
            int coins  = playerPoints.getCoins();
            playerPoints.setTotalPoints(learningCenterObj.getDataFromPlayerTotalPoints(playerPoints.getPlayerId()).getTotalPoints()
                    + points);

            playerPoints.setCoins(learningCenterObj.getDataFromPlayerTotalPoints(playerPoints.getPlayerId()).getCoins()
                    + coins);

            learningCenterObj.deleteFromPlayerTotalPoints(playerPoints.getPlayerId());
        }
        learningCenterObj.insertIntoPlayerTotalPoints(playerPoints);
        learningCenterObj.closeConn();
    }

    /**
     * Save the player points on server
     */
    private void savePlayerScoreOnserverAndInLocalDB(){
        try{
            this.inserIntoLocalDB();
            LearningCenterimpl learningCenterimpl = new LearningCenterimpl(this);
            learningCenterimpl.openConn();
            PlayerTotalPointsObj playerObj = learningCenterimpl
                    .getDataFromPlayerTotalPoints(playerId);
            learningCenterimpl.closeConn();
            playerObj.setUserId(userId);
            playerObj.setPlayerId(playerId);
            playerObj.setTotalPoints(playerObj.getTotalPoints());
            playerObj.setCoins(playerObj.getCoins());
            new AddCoinAndPointsForLoginUser(playerObj , this).execute();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * This method calculate the percentage of right answer question
     * @return
     */
    private int calCulateRightQuestionPercent(){

        int rightAnswerConuter = 0;
        for(int i = 0 ; i < MAX_QUESTION ; i ++ ){
            if(playDataList.get(i).getIsAnswerCorrect() == 1){
                rightAnswerConuter ++ ;
            }
        }

        return (100 * rightAnswerConuter) / MAX_QUESTION;
    }

    /**
     * This method call when user btnGoForFillIn
     */
    private void clickOnbtnGoForFillIn() {

        if(answerValue.length() == 0){
            this.showDialogWhenUserClickOnGoWithoutGivingAnswer();
        }else{
            if(!questionObj.getOptionList().get(0).contains(" ") && answerValue.toString().contains(" ")){
                answerValue = new StringBuilder(answerValue.toString().replaceAll(" ", ""));
            }

            answerByUser = answerValue.toString();

            String userAns = answerValue.toString().replaceAll(" ", "").replace(",", "");
            String dbAns = questionObj.getOptionList().get(0).replaceAll(" ", "").replace(",", "");

            //if(!questionObj.getOptionList().get(0).contains(".")){
            if(this.getIsFloatValue(questionObj.getOptionList().get(0))){
                if(userAns.equals(dbAns)){//changes for space and ask to deepak about this
                    isCorrectAnsByUser = 1;
                }
                else{
                    isCorrectAnsByUser = 0;
                }
            }else{//changes for float ans
                if(this.compareFloatAns(answerByUser, questionObj.getOptionList().get(0))){
                    isCorrectAnsByUser = 1;
                }else{
                    isCorrectAnsByUser = 0;

                }
            }
            this.showNextQuestion();
        }
    }


    /**
     * This method call when click on btnGoForWithoutFillIn button
     */
    private void clickOnbtnGoForWithoutFillIn(){

        if(selectedOptionList.size() > 0){
            ArrayList<Integer> selectedIntegerOptionList = new ArrayList<Integer>();

            String userAnswer = "";
            answerByUser      = "";
            for( int i = 0 ; i < selectedOptionList.size() ; i ++ ){
                for(int j = 0 ; j < questionObj.getOptionList().size() ; j ++ ){
                    if(selectedOptionList.get(i).getSelectedOptionTextValue()
                            .equals(questionObj.getOptionList().get(j)))
                    {
                        selectedIntegerOptionList.add(j + 1);
                        //userAnswer = userAnswer + (j + 1) + ",";
                    }
                }
            }

            Collections.sort(selectedIntegerOptionList);
            for(int i = 0 ; i < selectedIntegerOptionList.size() ; i ++ ){
                userAnswer = userAnswer + selectedIntegerOptionList.get(i) + ",";
            }

            if(userAnswer.length() > 0){
                answerByUser = userAnswer.substring(0, userAnswer.length() - 1);
            }

            if(userAnswer.equals(questionObj.getAns().replaceAll(" ", "") + ",")){
                isCorrectAnsByUser = 1;
                //noOfAttempts = 0;
                for(int i = 0 ; i < selectedOptionList.size() ; i ++ ){
                    if(isTab){
                        selectedOptionList.get(i).getOpationSeletedLayout()
                                .setBackgroundResource(R.drawable.right_ipad);
                    }
                    else{
                        selectedOptionList.get(i).getOpationSeletedLayout()
                                .setBackgroundResource(R.drawable.right);
                    }
                }
            }
            else{
                isCorrectAnsByUser = 0;
            }
            this.showNextQuestion();
        }else{
            this.showDialogWhenUserClickOnGoWithoutGivingAnswer();
        }
    }

    /**
     * This method call when user proceed without giving answer
     */
    private void showDialogWhenUserClickOnGoWithoutGivingAnswer(){
        DialogGenerator dg = new DialogGenerator(this);
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        dg.generateWarningDialog(transeletion.
                getTranselationTextByTextIdentifier("lblPleaseAnswerQuestion"));
        transeletion.closeConnection();
    }

    /**
     * This method set the play equation data to the list
     * for each equation which is to be save after completion of the
     * play all equations
     */
    private void setplayDataToArrayList(){

        int startTimeValue = startTime.getSeconds() ;
        int endTimeVlaue   = endTime.getSeconds();

        if(endTimeVlaue < startTimeValue)
            endTimeVlaue = endTimeVlaue + 60;

        if(numberOfQuestion >= 0 && numberOfQuestion < playDataList.size()){
            playDataList.get(numberOfQuestion ).setStartDate(CommonUtils.formateDate(startTime));
            playDataList.get(numberOfQuestion ).setEndData(CommonUtils.formateDate(endTime));
            playDataList.get(numberOfQuestion ).setTimeTakenToAnswer(endTimeVlaue - startTimeValue);
            playDataList.get(numberOfQuestion ).setCatId(questionObj.getCatId());
            playDataList.get(numberOfQuestion ).setSubCatId(questionObj.getSubCatId());

            if(isCorrectAnsByUser == 0){
                playDataList.get(numberOfQuestion ).setPoints(0);
                points = points  + 0;
            }else{
                points = points  + this.getPointsForSolvingEachEquation();
                playDataList.get(numberOfQuestion ).setPoints
                        (this.getPointsForSolvingEachEquation());
            }

            playDataList.get(numberOfQuestion ).setIsAnswerCorrect(isCorrectAnsByUser);

            if(answerByUser.length() > 0){
                playDataList.get(numberOfQuestion ).setUserAnswer(answerByUser);
                //only for assessment test option selected
                playDataList.get(numberOfQuestion ).setUserAnserForAssessment(answerByUser);
            }
            else{
                playDataList.get(numberOfQuestion ).setUserAnswer("1");
                //only for assessment test option selected
                playDataList.get(numberOfQuestion ).setUserAnserForAssessment("");
            }
            playDataList.get(numberOfQuestion ).setQuestionObj(questionObj);
            playDataList.get(numberOfQuestion ).setQuestionAttempt(true);
        }
    }

    /**
     * This method set how many question user give answer
     */
    private void setRemainingQuestions(){
        txtNumberOfQuestionsLeft.setText((numberOfQuestion  + 1 ) + " of " + MAX_QUESTION);
    }

    /**
     * This method call when a user click on number button
     * @param text
     */
    private void clickOnButton(String text){
        answerValue.append(text);
        this.setAnswertext(answerValue.toString());
    }

    /**
     * This method set the string to ans box
     * @param ansStringValue
     */
    private void setAnswertext(String ansStringValue){

        if(isAnswerConatin == 0)
            ansBox.setText(answerValue);
        else if(isAnswerConatin == 1){
            ansBox.setText(ansPrefixValue + " " +  answerValue);
        }
        else if(isAnswerConatin == 2){
            ansBox.setText(answerValue + " " + ansPostFixValue);
        }
        else if(isAnswerConatin == 3){
            ansBox.setText(ansPrefixValue + " " + answerValue + " " + ansPostFixValue);
        }
    }

    /**
     * This method clear the ans box
     */
    private void clearAnsBox() {
        if(answerValue.length() > 0){
            answerValue.deleteCharAt(answerValue.length() - 1);
            this.setAnswertext(answerValue.toString());
        }
    }

    /**
     * This method get question with its answer from database
     * using quation id
     *
     */
    private void getQuestionFromDatabase(){

        if(numberOfQuestion < MAX_QUESTION){

            int questionId = playDataList.get(numberOfQuestion).getEqautionId();

            //changes for Spanish
            if(languageCode == SPANISH){
                SpanishChangesImpl schoolImpl = new SpanishChangesImpl(this);
                schoolImpl.openConn();
                //get question from database into questionObj object
                questionObj  = schoolImpl.getQuestionByQuestionId(questionId);
                schoolImpl.closeConn();
            }else if(languageCode == ENGLISH){
                SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
                schoolImpl.openConnection();
                //get question from database into questionObj object
                questionObj  = schoolImpl.getQuestionByQuestionId(questionId);
                schoolImpl.closeConnection();
            }
            this.setIsSpanishBooleanValue(); // changes after
            //end changes


            //insert played question into WordQuestionsPlayed table
            SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
            String userId   = sharedPreffPlayerInfo.getString("userId", "0");
            String playerId = sharedPreffPlayerInfo.getString("playerId", "0");
            SingleFriendzySchoolCurriculumImpl dataObj = new SingleFriendzySchoolCurriculumImpl(this);
            dataObj.openConnection();
            dataObj.insertIntoWordQuestionsPlayed(userId, playerId, questionId);
            dataObj.closeConnection();

            this.showQuestion();
        }
    }

    /**
     * This method show question according to the index
     * index is a local variable which is increased after each question to show
     * initially set to 0
     */
    private void showQuestion(){
        //numberOfQuestion ++ ;
        try{
            if(isClickOnLeft){
                if(numberOfQuestion >= 0){
                    animation = AnimationUtils.loadAnimation(this, R.anim.splash_animation_left);
                    questionAnswerLayout.setAnimation(animation);
                }
            }else{
                //isDirectFromMainScreen for direct from main and stop animation on first question
                if(numberOfQuestion > 0 && numberOfQuestion < MAX_QUESTION && isDirectFromMainScreen == false){
                    animation = AnimationUtils.loadAnimation(this, R.anim.splash_animation_right);
                    questionAnswerLayout.setAnimation(animation);
                }
            }

            this.setKeyBoardVisibilityForAssessment(questionObj.getFillIn());
            this.setLayoutForShowQuetionAndAnswer(questionObj);
        }catch(Exception e){
            Log.e(TAG, "No question found " + e.toString());
        }
    }

    /**
     * This method set the visibility of keyboard for fill or not
     */
    private void setKeyBoardVisibilityForAssessment(int fill_in_blank){
        if(fill_in_blank == 1){
            isFillIn = true;
            relativeBottomBarFillIn.setVisibility(RelativeLayout.VISIBLE);
        }
        else{
            isFillIn = false;
            relativeBottomBarFillIn.setVisibility(RelativeLayout.GONE);
        }
    }

    @Override
    public void onBackPressed() {

        if(playSound != null)
            playSound.stopPlayer();

        //this.printSavePlaydatainXmlFormate();
        this.clickOnBackPress();

        if(isClickOnGo)
            startActivity(new Intent(this , MainActivity.class));

        super.onBackPressed();
    }

    /**
     * This method inflate the layout for show question and answer
     * @param questionObj
     */
    private void setLayoutForShowQuetionAndAnswer(WordProblemQuestionTransferObj questionObj){
        ArrayList<String> imageNameList        = this.getImageArrayList(questionObj);
        ArrayList<String> unAvailableImageList = this.checkImageAvalability(imageNameList);

        if(unAvailableImageList.size() > 0){
            if(CommonUtils.isInternetConnectionAvailable(this)){

                new DawnloadImageForSchooCurriculumForAssessment(unAvailableImageList, this, questionObj)
                        .execute(null,null,null);
            }else{
                //this.showQuetion();
                this.setQuestionAnswerLayoutForAssessment(questionObj);
            }
        }
        else{
            this.setQuestionAnswerLayoutForAssessment(questionObj);
        }
    }

    /**
     * This method call when user click on back
     */
    private void clickOnBackPress(){

        if(isClickOnGo){

            MathAssessmentTestResultObj mathAssessmentTestResult = new MathAssessmentTestResultObj();
            mathAssessmentTestResult.setUserId(userId);
            mathAssessmentTestResult.setPlayerId(playerId);
            mathAssessmentTestResult.setGrade(selectedGrade);
            mathAssessmentTestResult.setIsCompleted(0);
            mathAssessmentTestResult.setDate("");
            mathAssessmentTestResult.setProblems
                    ("<equations>"
                            + this.getEquationSolveXmlForInterNetNotConnectedForAssessment(playDataList) +
                            "</equations>");

            mathAssessmentTestResult.setQuestionRemaining(this.getRemainingQuestionToPlay());
            mathAssessmentTestResult.setStandardId(selectedStandardId);
            mathAssessmentTestResult.setSetSubStandardsQuestionJson
                    (this.getJsonStringFromMap(subStandardListWithQuestionIds));
            mathAssessmentTestResult.setCorrectScore(0);
            //this.printsSubStandardData(subStandardListWithQuestionIds);

            AssessmentTestImpl implObj = new AssessmentTestImpl(this);
            implObj.openConnection();
			/*implObj.deleteFromWordAssessmentTestResult(mathAssessmentTestResult);
			implObj.insertIntoWordAssessmentTestResult(mathAssessmentTestResult);*/
            //changes for New Assessment Test
            implObj.deleteFromNewWordAssessmentTestResult(mathAssessmentTestResult);
            implObj.insertIntoNewWordAssessmentTestResult(mathAssessmentTestResult);
            implObj.closeConnection();
        }
    }

	/*//for testing 
	private void printsSubStandardData(HashMap<Integer, ArrayList<Integer>> subStandardListWithQuestionIds){
		for(int id : subStandardListWithQuestionIds.keySet()){
			Log.e(TAG, "id " + id + " list " + subStandardListWithQuestionIds.get(id));
		}
	}	
	//end testing
	 */

    /**
     * This method return the question to play
     * @return
     */
    private String getRemainingQuestionToPlay(){
        String remainingQuestion = "";
        for(int i = 0 ; i < playDataList.size() ; i ++ ){
            if(playDataList.get(i).isQuestionAttempt() == false){
                remainingQuestion = remainingQuestion + playDataList.get(i).getEqautionId() + ",";
            }
        }
        return remainingQuestion;
    }

    @Override
    public void onInit(int status) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void onResume() {
        if(!isClickOnWorkArea){
            if(playSound != null)
                playSound.playSoundForSchoolSurriculumEquationSolve(this);
        }else
            isClickOnWorkArea = false;
        super.onResume();
    }

    @Override
    protected void onPause() {
        if(!isClickOnWorkArea){
            if(playSound != null)
                playSound.stopPlayer();
        }
        super.onPause();
    }
}
