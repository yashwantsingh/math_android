package com.mathfriendzy.controller.assessmenttest.xmlparsing;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import com.mathfriendzy.model.schoolcurriculum.learnigncenter.MathEquationTransferObj;

import android.content.Context;
import android.util.Log;

/**
 * This class is for xml parsing
 * @author Yashwant Singh
 *
 */
public class XmlParser {
	
	public static ArrayList<MathEquationTransferObj> mathEquationDataList = null;
	
	private final String TAG = this.getClass().getSimpleName();
	
	public XmlParser(String xmlString , Context context){
		
		mathEquationDataList = new ArrayList<MathEquationTransferObj>();
		
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser parser;
		XMLReader xmlReader;
		
		try {
			
			parser = factory.newSAXParser();
			xmlReader = parser.getXMLReader();
			xmlReader.setContentHandler(new XmlHandler(context));
			xmlReader.parse(new InputSource(new StringReader(xmlString)));
			
		} catch (ParserConfigurationException e) {
			Log.e(TAG, "inside XmlParser constructr Error while parsing xml " + e.toString());
		} catch (SAXException e) {
			Log.e(TAG, "inside XmlParser constructr Error while parsing xml " + e.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, "inside XmlParser constructr Error while parsing xml " + e.toString());
		}
	}
	
	/**
	 * This method return the play Data
	 * @return
	 */
	public static ArrayList<MathEquationTransferObj> getPlayDataList(){
		return mathEquationDataList;
	}
}
