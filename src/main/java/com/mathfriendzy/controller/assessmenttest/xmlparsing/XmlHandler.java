package com.mathfriendzy.controller.assessmenttest.xmlparsing;


import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import android.content.Context;

import com.mathfriendzy.model.schoolcurriculum.learnigncenter.MathEquationTransferObj;

/**
 * This handler parse the xml
 * @author Yashwant Singh
 *
 */
public class XmlHandler extends DefaultHandler{

	//flags for xml
	private boolean equationId 		= false;
	private boolean start_date_time = false;
	private boolean end_date_time 	= false;
	private boolean category_id 	= false;
	private boolean subCategory_id 	= false;
	private boolean points 			= false;
	private boolean isAnswerCorrect = false;
	private boolean user_answer 	= false;
	private boolean equation 		= false;
	private Context context;
	
	private MathEquationTransferObj mathEquationData = null;
	/*private ArrayList<MathEquationTransferObj> mathEquationDataList = 
			new ArrayList<MathEquationTransferObj>();*/
	
	public XmlHandler(Context context){
		this.context = context;
	}
	
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		
		if(equationId)
			mathEquationData.setEqautionId(Integer.parseInt(new String(ch, start , length)));
		else if(start_date_time)
			mathEquationData.setStartDate(new String(ch, start , length));
		else if(end_date_time)
			mathEquationData.setEndData(new String(ch, start , length));
		else if(category_id)
			mathEquationData.setCatId(Integer.parseInt(new String(ch, start , length)));
		else if(subCategory_id)
			mathEquationData.setSubCatId(Integer.parseInt(new String(ch, start , length)));
		else if(points)
			mathEquationData.setPoints(Integer.parseInt(new String(ch, start , length)));
		else if(isAnswerCorrect)
			mathEquationData.setIsAnswerCorrect(Integer.parseInt(new String(ch, start , length)));
		else if(user_answer)
			mathEquationData.setUserAnswer(new String(ch, start , length));
				
		super.characters(ch, start, length);
	}

	@Override
	public void endDocument() throws SAXException {
		//printXmlParseData();
		super.endDocument();
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		
		if(localName.equals("equation")){
			this.setDataToArrayList(mathEquationData);
		}
		else if(localName.equals("equationId"))
			equationId = false;
		else if(localName.equals("start_date_time"))
			start_date_time = false;
		else if(localName.equals("end_date_time"))
			end_date_time = false;
		else if(localName.equals("category_id"))
			category_id = false;
		else if(localName.equals("subCategory_id"))
			subCategory_id = false;
		else if(localName.equals("points"))
			points = false;
		else if(localName.equals("isAnswerCorrect"))
			isAnswerCorrect = false;
		else if(localName.equals("user_answer"))
			user_answer = false;
		
		super.endElement(uri, localName, qName);
	}

	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
	
		if(localName.equals("equation")){
			mathEquationData = new MathEquationTransferObj();
		}
		else if(localName.equals("equationId"))
			equationId = true;
		else if(localName.equals("start_date_time"))
			start_date_time = true;
		else if(localName.equals("end_date_time"))
			end_date_time = true;
		else if(localName.equals("category_id"))
			category_id = true;
		else if(localName.equals("subCategory_id"))
			subCategory_id = true;
		else if(localName.equals("points"))
			points = true;
		else if(localName.equals("isAnswerCorrect"))
			isAnswerCorrect = true;
		else if(localName.equals("user_answer"))
			user_answer = true;
		
		super.startElement(uri, localName, qName, attributes);
	} 
	
	/**
	 * This method add the math assessment result data to list
	 * @param mathAssessmentresultObj
	 */
	private void setDataToArrayList(MathEquationTransferObj mathEquationData){
		XmlParser.mathEquationDataList.add(mathEquationData);
	}
	
	/**
	 * This method print the xml Data
	 *//*
	private void printXmlParseData(){
		for(int i = 0 ; i < mathEquationDataList.size() ; i ++ ){
			printLog(mathEquationDataList.get(i).getEqautionId() + "");
			printLog(mathEquationDataList.get(i).getStartDate());
			printLog(mathEquationDataList.get(i).getEndData());
			printLog(mathEquationDataList.get(i).getCatId() + "");
			printLog(mathEquationDataList.get(i).getSubCatId() + "");
			printLog(mathEquationDataList.get(i).getPoints() + "");
			printLog(mathEquationDataList.get(i).getIsAnswerCorrect() + "");
			printLog(mathEquationDataList.get(i).getUserAnswer());
			
		}
	}
	
	private void printLog(String msg){
		Log.e("", msg + " \n ");
	}*/
	
	/**
	 * This method return the play data list
	 * @return
	 *//*
	public ArrayList<MathEquationTransferObj> getPlayDataList(){
		return mathEquationDataList;
	}*/
}
