package com.mathfriendzy.controller.assessmenttest.report;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.model.assessmenttest.AssessmentTestImpl;
import com.mathfriendzy.model.assessmenttest.CCSSScore;
import com.mathfriendzy.model.assessmenttest.DetailedScoreDto;
import com.mathfriendzy.model.assessmenttest.StandardsDto;
import com.mathfriendzy.model.assessmenttest.StudentsScore;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DateTimeOperation;
import com.mathfriendzy.utils.DialogGenerator;

public class AssessmentTestRepostByStudentAndByCCSS extends ActBaseClass implements OnClickListener{

	private TextView txtTop = null;
	private TextView txtScore = null;
	private TextView txtSelectedStandard = null;
	private TextView txtByStudent = null;
	private ImageButton imgByStudent = null;
	private TextView txtByCCSS    = null;
	private ImageButton imgByCCSS = null;
	private RelativeLayout layoutScoreDetailByStudent = null;
	private TextView txtStudentName = null;
	private TextView txtPlayTime = null;
	private TextView txtAverageScore = null;
	private TextView txtViewAnswer = null;
	private LinearLayout selectGradeAndstandardlayout = null;

	private final String TAG = this.getClass().getSimpleName();

	//get value from previous screen
	StandardsDto stdDto = null;
	DetailedScoreDto detailedScore = null;

	//for set checked 
	private boolean isByStudent = true;
	//for displaying list
	private RelativeLayout childLayout  = null;
	private ArrayList<RelativeLayout> layoutList = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_assessment_test_repost_by_student_and_by_ccss);

		//for holding the display list relative layout obj
		layoutList = new ArrayList<RelativeLayout>();

		this.getIntentValues();
		this.setWidgetsReferences();
		this.setTextFromTransalation();
		this.setListenerOnWidgets();
		this.setTextValuesFromLastScreen();

		//at the starting 
		this.setScoreDetail(detailedScore);
	}

	/**
	 * This method get Intent values which are set in the previous screen
	 */
	private void getIntentValues(){
		stdDto = (StandardsDto) this.getIntent().getSerializableExtra("standardsDtoInfo");
		detailedScore =  (DetailedScoreDto) this.getIntent().getSerializableExtra("detailedScoreObj");
	}

	/**
	 * This method set the values to the current which are selected in last screen
	 */
	private void setTextValuesFromLastScreen(){
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		txtSelectedStandard.setText(stdDto.getName());
		/*txtScore.setText(transeletion.getTranselationTextByTextIdentifier("mfBtnTitleScore")
				+ "\n" + stdDto.getScore() + "%");*/
		txtScore.setText(transeletion.getTranselationTextByTextIdentifier("mfBtnTitleStudents")
				+ "'" + transeletion.getTranselationTextByTextIdentifier("lblAverage")
				+ "\n" + stdDto.getScore() + "%");
		transeletion.closeConnection();
	}

	/**
	 * This method set the widgets references from layout
	 */
	private void setWidgetsReferences(){
		txtTop   = (TextView) findViewById(R.id.txtTop);
		txtScore = (TextView) findViewById(R.id.txtScore);
		txtSelectedStandard = (TextView) findViewById(R.id.txtSelectedStandard);
		txtByStudent = (TextView) findViewById(R.id.txtByStudent);
		txtByCCSS = (TextView) findViewById(R.id.txtByCCSS);
		txtStudentName = (TextView) findViewById(R.id.txtStudentName);
		txtPlayTime = (TextView) findViewById(R.id.txtPlayTime);
		txtAverageScore = (TextView) findViewById(R.id.txtAverageScore);
		txtViewAnswer = (TextView) findViewById(R.id.txtViewAnswer);
		imgByStudent = (ImageButton) findViewById(R.id.imgByStudent);
		imgByCCSS = (ImageButton) findViewById(R.id.imgByCCSS);
		selectGradeAndstandardlayout = (LinearLayout) findViewById(R.id.selectGradeAndstandardlayout);
		layoutScoreDetailByStudent   = (RelativeLayout) findViewById(R.id.layoutScoreDetailByStudent);
	}

	/**
	 * This method set the translation text
	 */
	private void setTextFromTransalation(){
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		txtTop.setText(transeletion.getTranselationTextByTextIdentifier("lblAssessments"));
		txtByStudent.setText(transeletion.getTranselationTextByTextIdentifier("lblByStudents"));
		txtByCCSS.setText(transeletion.getTranselationTextByTextIdentifier("lblByCcss"));
		txtStudentName.setText(transeletion.getTranselationTextByTextIdentifier("mfLblStudentsName"));
		txtPlayTime.setText(transeletion.getTranselationTextByTextIdentifier("playTitle")
				+ " " + transeletion.getTranselationTextByTextIdentifier("lblTime"));
		txtAverageScore.setText(transeletion.getTranselationTextByTextIdentifier("lblAverage")
				+ " " + transeletion.getTranselationTextByTextIdentifier("mfBtnTitleScore"));

		//txtViewAnswer.setText("View Answers");
		txtViewAnswer.setVisibility(TextView.GONE);
		transeletion.closeConnection();
	}

	/**
	 * This method set the checked for byStudent or ByCCSS
	 */
	private void setCheckedImageButton(){
		if(isByStudent){
			imgByStudent.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
			imgByCCSS.setBackgroundResource(R.drawable.mf_check_box_ipad);
		}else{
			imgByCCSS.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
			imgByStudent.setBackgroundResource(R.drawable.mf_check_box_ipad);
		}
	}

	/**
	 * This method set the visibility of the by Student layout
	 */
	private void setVisibilityOflayoutScoreDetailByStudent(){
		if(isByStudent)
			layoutScoreDetailByStudent.setVisibility(RelativeLayout.VISIBLE);
		else
			layoutScoreDetailByStudent.setVisibility(RelativeLayout.GONE);
	}

	/**
	 * This method set listener on widgets
	 */
	private void setListenerOnWidgets(){
		imgByStudent.setOnClickListener(this);
		imgByCCSS.setOnClickListener(this);
	}

	/**
	 * This method set the score List by Student or by ccss
	 * @param detailedScore
	 */
	private void setScoreDetail(DetailedScoreDto detailedScore){
		this.setCheckedImageButton();
		this.setVisibilityOflayoutScoreDetailByStudent();

		if(detailedScore != null){
			if(isByStudent){
				if(detailedScore.getStudentScoreList().size() > 0){
					this.setScoreDetailForStudent(detailedScore.getStudentScoreList());
				}else{
					DialogGenerator dg = new DialogGenerator(this);
					Translation transeletion = new Translation(this);
					transeletion.openConnection();
					dg.generateWarningDialogForthumbImages(transeletion.
							getTranselationTextByTextIdentifier("lblNoneOfYourStudentTakenTest"));
					transeletion.closeConnection();
				}
			}else{
				if(detailedScore.getCcssScoreList().size() > 0){
					this.setScoreDetailForCCSS(detailedScore.getCcssScoreList());
				}else{
					DialogGenerator dg = new DialogGenerator(this);
					Translation transeletion = new Translation(this);
					transeletion.openConnection();
					dg.generateWarningDialogForthumbImages(transeletion.
							getTranselationTextByTextIdentifier("lblNoneOfYourStudentTakenTest"));
					transeletion.closeConnection();
				}
			}
		}
	}

	/**
	 * This method set the student score List
	 * @param arrayList
	 */
	@SuppressWarnings("static-access")
	private void setScoreDetailForStudent(final ArrayList<StudentsScore> arrayList){
		selectGradeAndstandardlayout.removeAllViews();
		layoutList.clear();


		DateTimeOperation date = new DateTimeOperation();
		for(int i = 0 ; i < arrayList.size() ; i ++ ){
			LayoutInflater inflater1 = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
			childLayout = (RelativeLayout) inflater1.inflate(R.layout.assessment_result_by_student_layout, null);
			TextView txtPlayerName = (TextView) childLayout.findViewById(R.id.txtStudentName);
			TextView txtPlayTime   = (TextView) childLayout.findViewById(R.id.txtPlayTime);
			TextView txtScore	   = (TextView) childLayout.findViewById(R.id.txtAverageScore);

			txtPlayerName.setText(arrayList.get(i).getName());
			txtPlayTime.setText(date.setTimeFormat(arrayList.get(i).getTime()));
			txtScore.setText(arrayList.get(i).getScore() + "%");
			childLayout.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					for(int i = 0 ; i < layoutList.size() ; i ++ ){
						if(v == layoutList.get(i)){
							clickOnDisplayScoreDetaiForUser(arrayList.get(i));
						}
					}
				}
			});
			layoutList.add(childLayout);
			selectGradeAndstandardlayout.addView(childLayout);
		}

	}

	/**
	 * This method set the ccss score list
	 * @param arrayList
	 */
	@SuppressWarnings("static-access")
	private void setScoreDetailForCCSS(ArrayList<CCSSScore> arrayList){
		selectGradeAndstandardlayout.removeAllViews();
		layoutList.clear();

		AssessmentTestImpl implObj = new AssessmentTestImpl(this);
		implObj.openConnection();

		for(int i = 0 ; i < arrayList.size() ; i ++ ){
			LayoutInflater inflater1 = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
			childLayout = (RelativeLayout) inflater1.inflate(R.layout.assessment_result_by_ccss_layout, null);
			TextView txtSubCategory = (TextView) childLayout.findViewById(R.id.txtStudentName);
			TextView txtScore		= (TextView) childLayout.findViewById(R.id.txtScore);

			txtSubCategory.setText(implObj.getSubStandardName(stdDto.getStdId(), arrayList.get(i).getId()));
			txtScore.setText(arrayList.get(i).getScore() + "%");
			selectGradeAndstandardlayout.addView(childLayout);
		}
		implObj.closeConnection();
	}


	/**
	 * This method call when user want to see the score detail for selected player
	 * @param studentsScore
	 */
	private void clickOnDisplayScoreDetaiForUser(StudentsScore studentsScore){
		Intent intent = new Intent(this , AssessmentScoreDetail.class);
		intent.putExtra("standardsDtoInfo", stdDto);
		intent.putExtra("studentScore", studentsScore);
		startActivity(intent);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.imgByStudent :
			isByStudent = true;
			this.setScoreDetail(detailedScore);
			break;
		case R.id.imgByCCSS :
			isByStudent = false;
			this.setScoreDetail(detailedScore);
			break;
		}
	}
	
	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}
}
