package com.mathfriendzy.controller.assessmenttest.report;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.model.assessmenttest.AssessmentReportForTeacherDto;
import com.mathfriendzy.model.assessmenttest.AssessmentTestImpl;
import com.mathfriendzy.model.assessmenttest.AssessmentTestServerOperation;
import com.mathfriendzy.model.assessmenttest.DetailedScoreDto;
import com.mathfriendzy.model.assessmenttest.StandardsDto;
import com.mathfriendzy.model.grade.Grade;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.utils.CommonUtils;

public class AssessmentScoreSelectGradeActivity extends ActBaseClass {

	private TextView txtTop 				= null;
	private TextView txtCommonCoreStandards = null;
	private TextView txtSelectAGrade        = null;
	private Spinner  spinnerGrade           = null;
	private LinearLayout selectGradeAndstandardlayout = null;

	private String userId 		= null;
	private final String TAG 	= this.getClass().getSimpleName();

	//for displaying list
	private RelativeLayout childLayout    			= null;
	private ArrayList<RelativeLayout> layoutList 	= null;
	private ArrayList<StandardsDto> displayedStandardsList = null;
	private int scoreForEachStandard				= 0;

	private final int MAX_ROUNDS = 4;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_assessment_score_select_grade);

		userId = this.getUserId();
		//userId = "31";//for testing

		//for perform action on child layout  
		layoutList = new ArrayList<RelativeLayout>();
		displayedStandardsList = new ArrayList<StandardsDto>();

		this.setWidgetsReferences();
		this.setTextFromTranslation();

		this.getGrade(1 + "");
	}

	/**
	 * This method getUserId From database
	 * @return
	 */
	private String getUserId(){
		UserRegistrationOperation userOprObj = new UserRegistrationOperation(this);
		String userId = userOprObj.getUserId();
		return userId;
	}

	/** 
	 * @Descritpion getGradeData from database 
	 * @param
	 * @param gradeValue
	 */
	private void getGrade(String gradeValue)
	{			
		Grade gradeObj = new Grade();
		ArrayList<String> gradeList = gradeObj.getGradeList(this);
		this.setGradeAdapter(gradeValue , gradeList);
	}

	/**
	 * @Description set Grade data to adapter
	 * @param
	 * @param gradeValue
	 * @param gradeList 
	 */
	private void setGradeAdapter(final String gradeValue, final ArrayList<String> gradeList)
	{

		//changes for 1-8
		gradeList.subList(8, 13).clear();
		//end changes

		ArrayAdapter<String> gradeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,gradeList);
		gradeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerGrade.setAdapter(gradeAdapter);

		spinnerGrade.setSelection(gradeAdapter.getPosition(gradeValue));

		spinnerGrade.setOnItemSelectedListener(new OnItemSelectedListener() 
		{	@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
			((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);

			if(!MainActivity.isTab)
				((TextView) parent.getChildAt(0)).setTextSize(10);
			((TextView) parent.getChildAt(0)).setTypeface(null, Typeface.BOLD);

			getAssessmentReportForteacher(Integer.parseInt(spinnerGrade.getSelectedItem().toString()));
			//Log.e(TAG, "selected grade " + Integer.parseInt(spinnerGrade.getSelectedItem().toString()));
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {

		}
		});
	}
	/**
	 * This method set the widgets references from layout
	 */
	private void setWidgetsReferences(){
		txtTop = (TextView) findViewById(R.id.txtTop);
		txtCommonCoreStandards = (TextView) findViewById(R.id.txtCommonCoreStandards);
		txtSelectAGrade = (TextView) findViewById(R.id.txtSelectAGrade);
		spinnerGrade = (Spinner) findViewById(R.id.spinnerGrade);
		selectGradeAndstandardlayout = (LinearLayout) findViewById(R.id.selectGradeAndstandardlayout);
	}

	/**
	 * This method set the text from translation
	 */
	private void setTextFromTranslation(){
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		txtTop.setText(transeletion.getTranselationTextByTextIdentifier("lblAssessments"));
		txtCommonCoreStandards.setText(transeletion.getTranselationTextByTextIdentifier("lblCommomCoreStandards"));
		txtSelectAGrade.setText(transeletion.getTranselationTextByTextIdentifier("lblSelectAGrade") + ":");
		transeletion.closeConnection();
	}

	/**
	 * This method update UI after downloading teacher records from server
	 */
	private void updateUIAfterServerOperation(AssessmentReportForTeacherDto assessmentReportForTeacher 
			, int grade){
		ArrayList<StandardsDto> standardsList = this.getStandardsFromDatabase(grade);
		//int maxRounds = this.getMaxRounds(assessmentReportForTeacher);//default round 1
		int maxRounds = MAX_ROUNDS;

		selectGradeAndstandardlayout.removeAllViews();
		layoutList.clear();
		displayedStandardsList.clear();

		Translation translation = new Translation(this);
		translation.openConnection();

		//if(maxRounds > 0){ //play at least one round
		for(int i = 1 ; i <= maxRounds ; i ++ ){
			LayoutInflater inflater = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
			childLayout = (RelativeLayout) inflater.inflate(R.layout.assessment_test_standard_round_layout, null);
			TextView txtRound = (TextView) childLayout.findViewById(R.id.txtRound);
			TextView txtScore = (TextView) childLayout.findViewById(R.id.txtScore);
			TextView txtView  = (TextView) childLayout.findViewById(R.id.txtView);

			txtRound.setText(translation.getTranselationTextByTextIdentifier("lblRound") + " " + i);
			txtScore.setText(translation.getTranselationTextByTextIdentifier("mfBtnTitleScore"));
			txtView.setText("View");
			txtView.setVisibility(TextView.INVISIBLE);

			selectGradeAndstandardlayout.addView(childLayout);

			for(int j = 0 ; j < standardsList.size() ; j ++ ){
				LayoutInflater inflater1 = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
				childLayout = (RelativeLayout) inflater1.inflate(R.layout.layout_for_new_assessment_result_for_standard, null);
				TextView txtStandard    = (TextView) childLayout.findViewById(R.id.txtStudentName);
				TextView txtScoreResult = (TextView) childLayout.findViewById(R.id.txtScore);

				scoreForEachStandard = this.getScore(standardsList.get(j).getStdId(), i, assessmentReportForTeacher);
				txtStandard.setText(standardsList.get(j).getName());
				txtScoreResult.setText(scoreForEachStandard + "%");

				//create new object and add it to list
				StandardsDto stdDto = new StandardsDto();
				stdDto.setName(standardsList.get(j).getName());
				stdDto.setGrade(standardsList.get(j).getGrade());
				stdDto.setStdId(standardsList.get(j).getStdId());
				stdDto.setScore(scoreForEachStandard);
				stdDto.setRound(i);
				displayedStandardsList.add(stdDto);

				childLayout.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						for(int i = 0 ; i < layoutList.size() ; i ++ ){
							if(v == layoutList.get(i)){
								clickOnStandard(displayedStandardsList.get(i));
							}
						}
					}
				});
				selectGradeAndstandardlayout.addView(childLayout);
				layoutList.add(childLayout);
			}
		}
		//}
		translation.closeConnection();
	}

	/**
	 * This method call when user select the standard from the displayed list
	 * @param stdDto
	 */
	private void clickOnStandard(StandardsDto stdDto){
		if(CommonUtils.isInternetConnectionAvailable(this)){
			new GetDetailedScoreForAssessment(stdDto, userId).execute(null,null,null);
		}else{
			this.goOnNextScreen(stdDto , null);
		}
	}

	/**
	 * This method go on next screen.
	 * @param detailedScore 
	 */
	private void goOnNextScreen(StandardsDto stdDto, DetailedScoreDto detailedScore){
		Intent intent = new Intent(AssessmentScoreSelectGradeActivity.this , 
				AssessmentTestRepostByStudentAndByCCSS.class);
		intent.putExtra("standardsDtoInfo", stdDto);
		intent.putExtra("detailedScoreObj", detailedScore);
		startActivity(intent);
	}

	/**
	 * This method return the score according to the stdId and round
	 * @param stdId
	 * @param round
	 * @param assessmentReportForTeacher
	 * @return
	 */
	private int getScore(int stdId , int round , AssessmentReportForTeacherDto assessmentReportForTeacher){
		int score = 0;
		for(int i = 0 ; i < assessmentReportForTeacher.getStandardRoundList().size() ; i ++ ){
			if(stdId == assessmentReportForTeacher.getStandardRoundList().get(i).getStdId()
					&& round == assessmentReportForTeacher.getStandardRoundList().get(i).getRounds()){
				score = assessmentReportForTeacher.getStandardRoundList().get(i).getScore();
				break;
			}
		}
		return score;
	}

	/**
	 * This method return the max number of round to be display
	 * @param assessmentReportForTeacher
	 * @return
	 */
	private int getMaxRounds(AssessmentReportForTeacherDto assessmentReportForTeacher){
		int maxRound = 1;
		for(int i = 0 ; i < assessmentReportForTeacher.getStandardRoundList().size() ; i ++ ){
			if(assessmentReportForTeacher.getStandardRoundList().get(i).getRounds() > maxRound)
				maxRound = assessmentReportForTeacher.getStandardRoundList().get(i).getRounds();
		}
		return maxRound;
	}

	/**
	 * This method getStandards from database
	 * @param grade
	 */
	private ArrayList<StandardsDto> getStandardsFromDatabase(int grade){
		AssessmentTestImpl implObj = new AssessmentTestImpl(this);
		implObj.openConnection();
		ArrayList<StandardsDto> standardsList = implObj.getStandardsByGrade(grade);
		implObj.closeConnection();
		return standardsList;
	}

	/**
	 * This method get the assessment score from server for teacher
	 * @param selectedGrade
	 */
	private void getAssessmentReportForteacher(int selectedGrade){
		new GetAssessmentReportForTeacher(selectedGrade , userId).execute(null,null,null);
	}

	/**
	 * This class get the assessment report from server
	 * @author Yashwant Singh
	 *
	 */
	class GetAssessmentReportForTeacher extends AsyncTask<Void, Void, AssessmentReportForTeacherDto>{

		private int grade = 1;
		private String userId = "";
		private ProgressDialog pd = null;

		GetAssessmentReportForTeacher(int grade , String userId){
			this.grade  = grade;
			this.userId = userId;
		}

		@Override
		protected void onPreExecute() {
			pd = CommonUtils.getProgressDialog(AssessmentScoreSelectGradeActivity.this);
			pd.show();
			super.onPreExecute();
		}

		@Override
		protected AssessmentReportForTeacherDto doInBackground(Void... params) {
			AssessmentTestServerOperation serverObj = new AssessmentTestServerOperation();
			AssessmentReportForTeacherDto assessmentReportForTeacher = 
					serverObj.getAssessmentReportForTeacher(grade, userId);
			return assessmentReportForTeacher;
		}

		@Override
		protected void onPostExecute(AssessmentReportForTeacherDto assessmentReportForTeacher) {
			pd.cancel();
			if(assessmentReportForTeacher != null)
				updateUIAfterServerOperation(assessmentReportForTeacher , grade);
			else{
				CommonUtils.showInternetDialog(AssessmentScoreSelectGradeActivity.this);
			}
			super.onPostExecute(assessmentReportForTeacher);
		}
	}

	/**
	 * This class get the Detailed score for assessment from server
	 * @author Yashwant Singh
	 *
	 */
	class GetDetailedScoreForAssessment extends AsyncTask<Void, Void, DetailedScoreDto>{

		private StandardsDto stdDto = null;
		private String  userId 		= null;
		private ProgressDialog pd	= null;

		GetDetailedScoreForAssessment(StandardsDto stdDto , String userId){
			this.stdDto = stdDto;
			this.userId = userId; 
		}

		@Override
		protected void onPreExecute() {
			pd = CommonUtils.getProgressDialog(AssessmentScoreSelectGradeActivity.this);
			pd.show();
			super.onPreExecute();
		}

		@Override
		protected DetailedScoreDto doInBackground(Void... params) {
			AssessmentTestServerOperation serverObj = new AssessmentTestServerOperation();
			DetailedScoreDto detailedScore = serverObj.getDetailedScoreForAssessment(stdDto, userId);
			return detailedScore;
		}

		@Override
		protected void onPostExecute(DetailedScoreDto detailedScore) {
			pd.cancel();
			if(detailedScore != null){
				goOnNextScreen(stdDto , detailedScore);
			}else{
				CommonUtils.showInternetDialog(AssessmentScoreSelectGradeActivity.this);
			}
			super.onPostExecute(detailedScore);
		}
	}
	
	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}
}
