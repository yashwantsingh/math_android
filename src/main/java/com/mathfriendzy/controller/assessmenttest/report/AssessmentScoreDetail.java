package com.mathfriendzy.controller.assessmenttest.report;

import java.util.ArrayList;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.model.assessmenttest.AssessmentTestImpl;
import com.mathfriendzy.model.assessmenttest.StandardsDto;
import com.mathfriendzy.model.assessmenttest.StudentsScore;
import com.mathfriendzy.model.assessmenttest.SubStandardsDto;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.CommonUtils;

import android.os.Bundle;
import android.app.Activity;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class AssessmentScoreDetail extends ActBaseClass {

	private TextView txtTop = null;
	private TextView txtPlayerName = null;
	private TextView txtSelectedStandard = null;
	private TextView txtScore = null;
	private LinearLayout selectGradeAndstandardlayout = null;

	//intent values
	private StudentsScore studentsScore = null;
	private StandardsDto stdDto = null;
	//for displaying list
	private RelativeLayout childLayout  = null;
	private ArrayList<RelativeLayout> layoutList = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_assessment_score_detail);

		layoutList = new ArrayList<RelativeLayout>();
		
		this.getIntentValues();
		this.setWidgetsReferences();
		this.setTextFromTranslation();
		this.setTextValuesFromIntentValue();
		this.setScoreDetailForCCSS(studentsScore.getSubStandardScoreList());
	}

	/**
	 * This method get the intent values which are set in the last screen
	 */
	private void getIntentValues(){
		stdDto = (StandardsDto) this.getIntent().getSerializableExtra("standardsDtoInfo");
		studentsScore = (StudentsScore) this.getIntent().getSerializableExtra("studentScore");
	}

	/**
	 * This method set the text values which are set in the last screen
	 */
	private void setTextValuesFromIntentValue(){
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		txtPlayerName.setText(studentsScore.getName());
		txtSelectedStandard.setText(stdDto.getName());
		txtScore.setText(transeletion.getTranselationTextByTextIdentifier("mfBtnTitleScore")
				+ "\n" + studentsScore.getScore() + "%");
		transeletion.closeConnection();
	}

	/**
	 * This method set widgets references from layout
	 */
	private void setWidgetsReferences(){
		txtTop = (TextView) findViewById(R.id.txtTop);
		txtPlayerName = (TextView) findViewById(R.id.txtPlayerName);
		txtSelectedStandard = (TextView) findViewById(R.id.txtSelectedStandard);
		txtScore = (TextView) findViewById(R.id.txtScore);
		selectGradeAndstandardlayout = (LinearLayout) findViewById(R.id.selectGradeAndstandardlayout);
	}

	/**
	 * This method set the text from translation
	 */
	private void setTextFromTranslation(){
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		txtTop.setText("Assessments");
		transeletion.closeConnection();
	}

	/**
	 * This method set the ccss score list
	 * @param arrayList
	 */
	@SuppressWarnings("static-access")
	private void setScoreDetailForCCSS(ArrayList<SubStandardsDto> arrayList){
		selectGradeAndstandardlayout.removeAllViews();
		layoutList.clear();

		AssessmentTestImpl implObj = new AssessmentTestImpl(this);
		implObj.openConnection();

		for(int i = 0 ; i < arrayList.size() ; i ++ ){
			LayoutInflater inflater1 = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
			childLayout = (RelativeLayout) inflater1.inflate(R.layout.assessment_result_by_ccss_layout, null);
			TextView txtSubCategory = (TextView) childLayout.findViewById(R.id.txtStudentName);
			TextView txtScore		= (TextView) childLayout.findViewById(R.id.txtScore);

			txtSubCategory.setText(implObj.getSubStandardName(stdDto.getStdId(), arrayList.get(i).getSubStdId()));
			txtScore.setText(arrayList.get(i).getScore() + "%");
			selectGradeAndstandardlayout.addView(childLayout);
		}
		implObj.closeConnection();
	}
	
	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}
}
