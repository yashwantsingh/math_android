package com.mathfriendzy.controller.assessmenttest.report;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.controller.friendzy.StudentChallengeActivity;
import com.mathfriendzy.controller.homework.assignhomeworkquiz.ActAssignHomework;
import com.mathfriendzy.controller.homework.checkhomeworkquiz.ActCheckHomework;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.managetutor.mystudent.ActManageTutorMyStudents;
import com.mathfriendzy.controller.studentaccount.StudentAccount;
import com.mathfriendzy.controller.teacherStudents.TeacherStudents;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.TeacherFunctionDialogListener;
import com.mathfriendzy.model.assessmenttest.AssessmentStandardDto;
import com.mathfriendzy.model.assessmenttest.AssessmentTestImpl;
import com.mathfriendzy.model.assessmenttest.AssessmentTestServerOperation;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.CoinsFromServerObj;
import com.mathfriendzy.model.learningcenter.LearningCenteServerOperation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.PurchaseItemObj;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ICommonUtils;
import com.mathfriendzy.utils.MathVersions;

import java.util.ArrayList;

import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_ID;

/**
 * This Activity show for asking want to see report for Practice And Assessment
 * @author Yashwant Singh
 *
 */
public class ReportForPracticeAndAssessment extends ActBaseClass implements OnClickListener{

    private TextView mfTitleHomeScreen = null;
    private TextView txtReportFor      = null;
    private Button   btnPractice       = null;
    private Button   btnAssessments    = null;
    private Button   btnCreateChallenge	   		= null;
    private ImageButton btnInfoPractice 		= null;
    private ImageButton btnInfoAssessment 		= null;
    private ImageButton btnInfoCreateChallenge	= null;

    //for new feature , students account , on 28 Apr 2014
    private Button   btnStudentsAccount   = null;
    private ImageButton btnInfoStudentsAccount   = null;

    //private final String TAG = this.getClass().getSimpleName();

    //for new teacher function start from 03/11/2014
    private Button btnAssessmentsReportByClass = null;
    private Button btnAssignHomeWorkQuizzess = null;
    private Button btnCheckHomeWorkQuizzess = null;
    private ImageButton btnInfoAssessmentReportByClass 	= null;
    private ImageButton btnInfoAssignHomeWorkQuizzess 	= null;
    private ImageButton btnInfoCheckHomeWorkQuizzess 	= null;

    private String lblYouMustRegisterLoginToAccess = null;

    //for new registration
    private Button      btnHowToRegisterStudent = null;
    private ImageButton btnInfoHowToRegisterStudent = null;
    private String aleratRegisterStudentMsg = null;

    //for manage tutor
    private Button btnManageTutor = null;
    private ImageView btnInfoManageTutor = null;

    private TextView txtAssignHomeworkPatentPending = null;
    private TextView txtCheckHomeworkPatentPending = null;
    private TextView txtManageTutorPatentPending = null;

    private RelativeLayout layoutStudents = null;
    private RelativeLayout layoutPlayers = null;
    private RelativeLayout layoutAssessmentReportByClass = null;
    private RelativeLayout layoutAssignHomeWorkQuizzess = null;
    private RelativeLayout layoutCheckHomeworkQuizzess = null;
    private RelativeLayout layoutCreateChallenge = null;
    private RelativeLayout layoutStudentAccount = null;
    private RelativeLayout layoutHowToRegisterStudent = null;
    private RelativeLayout layoutManageTutor = null;
    private RelativeLayout teacherFunctionsLayoutPlus = null;
    private TextView txtTeacherFunction = null;
    private TextView txtToRegister = null;
    private String lblRegisterYouStudent = "To Register your students , go to the www.mathfriendzy.com" +
            " , log in using your account and click on the \"Register Your students\" tab.";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_for_practice_and_assessment);

        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.setVisibilityOfLayoutsForMathPlus();
    }

    private void setVisibilityOfLayoutsForMathPlus(){
        if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS){
            layoutStudents.setVisibility(RelativeLayout.GONE);
            layoutPlayers.setVisibility(RelativeLayout.GONE);
            layoutAssessmentReportByClass.setVisibility(RelativeLayout.GONE);
            layoutCreateChallenge.setVisibility(RelativeLayout.GONE);
            layoutHowToRegisterStudent.setVisibility(RelativeLayout.GONE);
            teacherFunctionsLayoutPlus.setVisibility(RelativeLayout.VISIBLE);
            txtReportFor.setVisibility(TextView.GONE);

            btnAssignHomeWorkQuizzess.setBackgroundResource(R.drawable.view_detail);
            btnCheckHomeWorkQuizzess.setBackgroundResource(R.drawable.new_friendzy_blue_challenge_ipad);
            btnStudentsAccount.setBackgroundResource(R.drawable.new_friendzy_orange_challenge_ipad);
            btnManageTutor.setBackgroundResource(R.drawable.new_friendzy_green_challenge_ipad);
        }
    }


    /**
     * This method set the widgets references from layout
     */
    private void setWidgetsReferences(){
        mfTitleHomeScreen 	= (TextView) findViewById(R.id.mfTitleHomeScreen);
        txtReportFor 		= (TextView) findViewById(R.id.txtReportFor);
        btnPractice         = (Button)   findViewById(R.id.btnPractice);
        btnAssessments      = (Button)   findViewById(R.id.btnAssessments);
        btnInfoPractice     = (ImageButton) findViewById(R.id.btnInfoPractice);
        btnInfoAssessment   = (ImageButton) findViewById(R.id.btnInfoAssessment);

        //changes for new friendzy challenge
        btnCreateChallenge  = (Button)	findViewById(R.id.btnCreateChallenge);
        btnInfoCreateChallenge   = (ImageButton) findViewById(R.id.btnInfoCreateChallenge);
        //end changes

        //for new feature , students account , on 28 Apr 2014
        btnStudentsAccount 		= (Button) findViewById(R.id.btnStudentsAccount);
        btnInfoStudentsAccount 	= (ImageButton) findViewById(R.id.btnInfoStudentsAccount);

        //for new teacher function start from 03/11/2014
        btnAssessmentsReportByClass = (Button) findViewById(R.id.btnAssessmentsReportByClass);
        btnAssignHomeWorkQuizzess 	= (Button) findViewById(R.id.btnAssignHomeWorkQuizzess);
        btnCheckHomeWorkQuizzess 	= (Button) findViewById(R.id.btnCheckHomeWorkQuizzess);
        btnInfoAssessmentReportByClass = (ImageButton) findViewById(R.id.btnInfoAssessmentReportByClass);
        btnInfoAssignHomeWorkQuizzess = (ImageButton) findViewById(R.id.btnInfoAssignHomeWorkQuizzess);
        btnInfoCheckHomeWorkQuizzess = (ImageButton) findViewById(R.id.btnInfoCheckHomeWorkQuizzess);

        //for new registration
        btnHowToRegisterStudent = (Button) findViewById(R.id.btnHowToRegisterStudent);
        btnInfoHowToRegisterStudent = (ImageButton) findViewById(R.id.btnInfoHowToRegisterStudent);

        //for manage tutor
        btnManageTutor = (Button) findViewById(R.id.btnManageTutor);
        btnInfoManageTutor = (ImageView) findViewById(R.id.btnInfoManageTutor);

        /**
         * Add check popup and remove i button
         */
        btnInfoPractice.setVisibility(ImageButton.GONE);
        btnInfoAssessment.setVisibility(ImageButton.GONE);
        btnInfoAssessmentReportByClass.setVisibility(ImageButton.GONE);
        btnInfoAssignHomeWorkQuizzess.setVisibility(ImageButton.GONE);
        btnInfoCheckHomeWorkQuizzess.setVisibility(ImageButton.GONE);
        btnInfoCreateChallenge.setVisibility(ImageButton.GONE);
        btnInfoStudentsAccount.setVisibility(ImageButton.GONE);
        btnInfoHowToRegisterStudent.setVisibility(ImageButton.GONE);
        btnInfoManageTutor.setVisibility(ImageButton.GONE);


        txtAssignHomeworkPatentPending = (TextView) findViewById(R.id.txtAssignHomeworkPatentPending);
        txtCheckHomeworkPatentPending = (TextView) findViewById(R.id.txtCheckHomeworkPatentPending);
        txtManageTutorPatentPending = (TextView) findViewById(R.id.txtManageTutorPatentPending);


        //Math Plus changes
        layoutStudents = (RelativeLayout) findViewById(R.id.layoutStudents);
        layoutPlayers = (RelativeLayout) findViewById(R.id.layoutPlayers);
        layoutAssessmentReportByClass = (RelativeLayout) findViewById(R.id.layoutAssessmentReportByClass);
        layoutAssignHomeWorkQuizzess = (RelativeLayout) findViewById(R.id.layoutAssignHomeWorkQuizzess);
        layoutCheckHomeworkQuizzess = (RelativeLayout) findViewById(R.id.layoutCheckHomeworkQuizzess);
        layoutCreateChallenge = (RelativeLayout) findViewById(R.id.layoutCreateChallenge);
        layoutStudentAccount = (RelativeLayout) findViewById(R.id.layoutStudentAccount);
        layoutHowToRegisterStudent = (RelativeLayout) findViewById(R.id.layoutHowToRegisterStudent);
        layoutManageTutor = (RelativeLayout) findViewById(R.id.layoutManageTutor);
        teacherFunctionsLayoutPlus = (RelativeLayout) findViewById(R.id.teacherFunctionsLayoutPlus);
        txtTeacherFunction = (TextView) findViewById(R.id.txtTeacherFunction);
        txtToRegister = (TextView) findViewById(R.id.txtToRegister);
    }

    /**
     * This method set the translation text
     */
    private void setTextFromTranslation(){
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        mfTitleHomeScreen.setText(transeletion.getTranselationTextByTextIdentifier("lblMyClass"));
        txtReportFor.setText(transeletion.getTranselationTextByTextIdentifier("lblReportsFor") + ":");
        btnPractice.setText(transeletion.getTranselationTextByTextIdentifier("lblPractice"));
        btnAssessments.setText(transeletion.getTranselationTextByTextIdentifier("lblAssessmentsReports"));
        btnCreateChallenge.setText(transeletion.getTranselationTextByTextIdentifier("lblFriendzyChallenge"));
        //for new feature , students account , on 28 Apr 2014
        btnStudentsAccount.setText(transeletion.getTranselationTextByTextIdentifier("mfBtnTitleStudents")
                + "' " + transeletion.getTranselationTextByTextIdentifier("lblAccounts"));
        btnAssessmentsReportByClass.setText(transeletion.getTranselationTextByTextIdentifier("lblAssessmentsReports")
                + " " + transeletion.getTranselationTextByTextIdentifier("lblByCcss"));
        btnAssignHomeWorkQuizzess.setText(transeletion.getTranselationTextByTextIdentifier("lblAssignHomework")
                + "/" + transeletion.getTranselationTextByTextIdentifier("lblQuizzes"));
        btnCheckHomeWorkQuizzess.setText(transeletion.getTranselationTextByTextIdentifier("lblCheckHomework")
                + "/" + transeletion.getTranselationTextByTextIdentifier("lblQuizzes"));
        lblYouMustRegisterLoginToAccess = transeletion
                .getTranselationTextByTextIdentifier("lblYouMustRegisterLoginToAccess");
        //for manage tutor
        btnManageTutor.setText(transeletion.getTranselationTextByTextIdentifier("btnManageTutorsTitle"));

        //for new registration
        btnHowToRegisterStudent.setText(transeletion
                .getTranselationTextByTextIdentifier("lblRegisterYourStudents"));

        aleratRegisterStudentMsg = this.getUpdatedMessageWithUpdatedUrlBasedOnSelectedApp(transeletion
                .getTranselationTextByTextIdentifier("lblToRegisterStudentsWithFreeTrial"));
        txtAssignHomeworkPatentPending.setText(transeletion
                .getTranselationTextByTextIdentifier("lblPatentPending"));
        txtCheckHomeworkPatentPending.setText(transeletion
                .getTranselationTextByTextIdentifier("lblPatentPending"));
        txtManageTutorPatentPending.setText(transeletion
                .getTranselationTextByTextIdentifier("lblPatentPending"));
        txtTeacherFunction.setText(transeletion.getTranselationTextByTextIdentifier("lblReportsFor"));
        txtToRegister.setText(aleratRegisterStudentMsg);
        transeletion.closeConnection();
    }

    private String getUpdatedMessageWithUpdatedUrlBasedOnSelectedApp(String string){
        if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS){
            string = string.replace("www.mathfriendzy.com" , "www.SchoolFriendzy.com");
        }
        return string;
    }

    /**
     * This method set listener on widgets
     */
    private void setListenerOnWidgets(){
        btnPractice.setOnClickListener(this);
        btnAssessments.setOnClickListener(this);
        btnInfoPractice.setOnClickListener(this);
        btnInfoAssessment.setOnClickListener(this);
        btnCreateChallenge.setOnClickListener(this);
        btnInfoCreateChallenge.setOnClickListener(this);
        //for new feature , students account , on 28 Apr 2014
        btnStudentsAccount.setOnClickListener(this);
        btnInfoStudentsAccount.setOnClickListener(this);

        btnAssessmentsReportByClass.setOnClickListener(this);
        btnAssignHomeWorkQuizzess.setOnClickListener(this);
        btnCheckHomeWorkQuizzess.setOnClickListener(this);
        btnInfoAssessmentReportByClass.setOnClickListener(this);
        btnInfoAssignHomeWorkQuizzess.setOnClickListener(this);
        btnInfoCheckHomeWorkQuizzess.setOnClickListener(this);

        //for new registration
        btnHowToRegisterStudent.setOnClickListener(this);
        btnInfoHowToRegisterStudent.setOnClickListener(this);

        //for manage tutor
        btnManageTutor.setOnClickListener(this);
    }


    /**
     * Show the teacher function popup
     * @param popUpFor
     * @return
     */
    private boolean showTeacherFunctionPopUp(final String popUpFor , String txtTitle
            , boolean isShowWatchButton){
        if(MathFriendzyHelper.showTeacherFunctionPopup(this ,
                popUpFor , 	new TeacherFunctionDialogListener() {

                    @Override
                    public void clickOnWatchVedio(boolean isShow) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void clickOnDontShow(boolean isShow) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void clickOnClose(boolean isShow) {
                        clickOn(popUpFor);
                    }
                } , txtTitle , isShowWatchButton))
            return true;
        return false;
    }

    /**
     * Go after click on close button on Teacher function show vedio
     * @param dialogFor
     */
    private void clickOn(String dialogFor){
        if(MathFriendzyHelper.DIALOG_ASSESSMENT_REPORT_BY_CLASS.equals(dialogFor)){
            this.goForAssessmentReportByClass();
        }else if(MathFriendzyHelper.DIALOG_ASSIGN_HOMEWORK.equals(dialogFor)){
            this.goForAssignHomeworkQuizz();
        }else if(MathFriendzyHelper.DIALOG_CHECK_HOMEWORK.equals(dialogFor)){
            this.goForCheckHomeworkQuizz();
        }else if(MathFriendzyHelper.DIALOG_PRACTICE_REPORT.equals(dialogFor)){
            this.goForPracticeReport();
        }else if(MathFriendzyHelper.DIALOG_ASSESSMENT_REPORT.equals(dialogFor)){
            this.goForAssessmentReport();
        }else if(MathFriendzyHelper.DIALOG_STUDENT_ACCOUNT.equals(dialogFor)){
            this.goForStudentAccount();
        }else if(MathFriendzyHelper.DIALOG_CREATE_CHALLENGE.equals(dialogFor)){
            this.goForCreateChallenge();
        }else if(MathFriendzyHelper.DIALOG_MANAGE_TUTOR.equals(dialogFor)){
            this.clickOnManageTutor();
        }
    }

    /**
     * Click on Assessment report by Class
     */
    private void clickOnAssessmentReportByClass(){
        //Log.e(TAG, "inside clickOnAssessmentReportByClass");

        if(this.showTeacherFunctionPopUp
                (MathFriendzyHelper.DIALOG_ASSESSMENT_REPORT_BY_CLASS,
                        MathFriendzyHelper.getTextFromButton(btnAssessmentsReportByClass) , true))
            return ;

        this.goForAssessmentReportByClass();
    }

    private void goForAssessmentReportByClass(){
        if(this.isLogin()){
            CommonUtils.comingSoonPopUp(this);
        }else{
            this.showRegistratinoDialog();
        }
    }

    /**
     * Click to Assign Homework Quiz
     */
    private void clickOnAssignHomeworkQuizz(){
        //Log.e(TAG, "inside clickOnAssignHomeworkQuizz");

        if(this.showTeacherFunctionPopUp
                (MathFriendzyHelper.DIALOG_ASSIGN_HOMEWORK
                        , MathFriendzyHelper.getTextFromButton(btnAssignHomeWorkQuizzess) , true))
            return ;

        this.goForAssignHomeworkQuizz();
    }

    private void goForAssignHomeworkQuizz(){
        if(this.isLogin()){
            MathFriendzyHelper.getSchoolClasses(this , new HttpResponseInterface() {
                @Override
                public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                    startActivity(new Intent(getActivityObj(), ActAssignHomework.class));
                }
            } , true);
            //startActivity(new Intent(this, ActAssignHomework.class));
        }else{
            this.showRegistratinoDialog();
        }
    }

    /**
     * Click to check Homework Quizzes
     */
    private void clickOnCheckHomeworkQuizzes(){
        //Log.e(TAG, "inside clickOnCheckHomeworkQuizzes");
        //CommonUtils.comingSoonPopUp(this);

        if(this.showTeacherFunctionPopUp
                (MathFriendzyHelper.DIALOG_CHECK_HOMEWORK
                        , MathFriendzyHelper.getTextFromButton(btnCheckHomeWorkQuizzess) , true))
            return ;

        this.goForCheckHomeworkQuizz();
    }

    private void goForCheckHomeworkQuizz(){
        if(this.isLogin()){
            MathFriendzyHelper.getSchoolClasses(this , new HttpResponseInterface() {
                @Override
                public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                    startActivity(new Intent(getActivityObj() , ActCheckHomework.class));
                }
            } , true);
            //startActivity(new Intent(this , ActCheckHomework.class));
        }else{
            this.showRegistratinoDialog();
        }
    }

    /**
     * Click to open manage tutor screen
     */
    private void clickOnManageTutor(){
        if(this.isLogin()) {
            //Intent intent = new Intent(this, ActManageTutor.class);
            MathFriendzyHelper.getSchoolClasses(this , new HttpResponseInterface() {
                @Override
                public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                    Intent intent = new Intent(getActivityObj(), ActManageTutorMyStudents.class);
                    startActivity(intent);
                }
            } , true);
            /*Intent intent = new Intent(this, ActManageTutorMyStudents.class);
            startActivity(intent);*/
        }else{
            this.showRegistratinoDialog();
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnPractice :
                //clickOnAnyButtonForSubscription(1);
                this.clickOnPractice();
                break;
            case R.id.btnAssessments :
                //clickOnAnyButtonForSubscription(2);
                this.clickOnAssessment();
                break;
            case R.id.btnInfoPractice :
                this.generateDialogForAssessmentInfoAndPracticeInfo(2);
                break;
            case R.id.btnInfoAssessment :
                this.generateDialogForAssessmentInfoAndPracticeInfo(1);
                break;
            case R.id.btnCreateChallenge :
                //clickOnAnyButtonForSubscription(3);
                this.goOnCreateChallengerActivity();
                break;
            case R.id.btnInfoCreateChallenge :
                this.generateDialogForAssessmentInfoAndPracticeInfo(3);
                break;
            //for new feature , students account , on 28 Apr 2014
            case R.id.btnStudentsAccount :
                this.clickOnStudentsAccount();
                break;
            case R.id.btnInfoStudentsAccount :
                this.generateDialogForAssessmentInfoAndPracticeInfo(4);
                break;
            case R.id.btnAssessmentsReportByClass:
                this.clickOnAssessmentReportByClass();
                break;
            case R.id.btnAssignHomeWorkQuizzess:
                this.clickOnAssignHomeworkQuizz();
                break;
            case R.id.btnCheckHomeWorkQuizzess:
                this.clickOnCheckHomeworkQuizzes();
                break;
            case R.id.btnInfoAssessmentReportByClass:
                this.generateDialogForAssessmentInfoAndPracticeInfo(5);
                break;
            case R.id.btnInfoAssignHomeWorkQuizzess:
                this.generateDialogForAssessmentInfoAndPracticeInfo(6);
                break;
            case R.id.btnInfoCheckHomeWorkQuizzess:
                this.generateDialogForAssessmentInfoAndPracticeInfo(7);
                break;
            case R.id.btnHowToRegisterStudent:
                MathFriendzyHelper.warningDialog(this, aleratRegisterStudentMsg);
                break;
            case R.id.btnManageTutor:
                if(this.showTeacherFunctionPopUp
                        (MathFriendzyHelper.DIALOG_MANAGE_TUTOR
                                , MathFriendzyHelper.getTextFromButton(btnManageTutor) , true))
                    return ;

                this.clickOnManageTutor();
                break;
        }
    }

    /**
     * This method call when user click on Create challeger button
     */
    private void goOnCreateChallengerActivity(){

        if(this.showTeacherFunctionPopUp
                (MathFriendzyHelper.DIALOG_CREATE_CHALLENGE
                        , MathFriendzyHelper.getTextFromButton(btnCreateChallenge) , true))
            return ;

        this.goForCreateChallenge();
    }

    private void goForCreateChallenge(){
        if(this.isLogin()){
            UserRegistrationOperation userObj1 = new UserRegistrationOperation(this);
            RegistereUserDto regUserObj = userObj1.getUserData();
            String userId = regUserObj.getUserId();

            SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0);
            if(sheredPreferencePlayer.getString("userId", "").equals("")){
                Editor editor = sheredPreferencePlayer.edit();
                editor.putString("userId", userId);
                editor.commit();
            }

            Intent intent = new Intent(this,StudentChallengeActivity.class);
            intent.putExtra("isTeacher", true);
            startActivity(intent);
        }else{
            this.showRegistratinoDialog();
        }
    }

    /**
     * This method call when user click on btnInfoPractice and btnInfoAssessment
     * @param forAssessmt
     */
    private void generateDialogForAssessmentInfoAndPracticeInfo(int forAssessmt){//1 for assessment and 2 for practice
        DialogGenerator dg = new DialogGenerator(this);
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        if(forAssessmt == 2){
            dg.generateWarningDialogForthumbImages(transeletion.
                    getTranselationTextByTextIdentifier("lblThisButtonWillGenerateReport"));
        }
        else if(forAssessmt == 1){
            dg.generateWarningDialogForthumbImages(transeletion.
                    getTranselationTextByTextIdentifier("lblThisViewAllowToSee"));

        }else if(forAssessmt == 3){
            dg.generateWarningDialogForthumbImages(transeletion.
                    getTranselationTextByTextIdentifier("lblThisButtonAllowToCreateFriendzyChallenge"));

        }else if(forAssessmt == 4){
            dg.generateWarningDialogForthumbImages(transeletion.
                    getTranselationTextByTextIdentifier("lblThisButtonWillAllowYou"));

        }else if(forAssessmt == 5){
            dg.generateWarningDialogForthumbImages(transeletion.
                    getTranselationTextByTextIdentifier("lblThisButtonWillGenerateSeveralReports"));

        }else if(forAssessmt == 6){
            dg.generateWarningDialogForthumbImages(transeletion.
                    getTranselationTextByTextIdentifier("lblHereYouCanSelect"));

        }else if(forAssessmt == 7){
            dg.generateWarningDialogForthumbImages(transeletion.
                    getTranselationTextByTextIdentifier("lblOnceYouHaveAssigned"));

        }
        transeletion.closeConnection();
    }

    /**
     * Go On Next Screen
     * @param userId
     * @param playerId
     */
    private void goOnNextScreen(String userId , String playerId){
        startActivity(new Intent(this , AssessmentScoreSelectGradeActivity.class));
    }

    /**
     * This method call when user click on assessment
     */
    private void clickOnAssessment(){

        if(this.showTeacherFunctionPopUp
                (MathFriendzyHelper.DIALOG_ASSESSMENT_REPORT
                        , MathFriendzyHelper.getTextFromButton(btnAssessments) , true))
            return ;

        this.goForAssessmentReport();
    }

    private void goForAssessmentReport(){
        if(this.isLogin()){
            if(CommonUtils.isInternetConnectionAvailable(this)){
                new GetUpdatedDateAssessment("","").execute(null,null,null);
            }else{
                CommonUtils.showInternetDialog(this);
            }
        }else{
            this.showRegistratinoDialog();
        }
    }

    private void showRegistratinoDialog(){
        MathFriendzyHelper.showLoginRegistrationDialog(this, lblYouMustRegisterLoginToAccess);
    }

    private boolean isLogin(){
        return MathFriendzyHelper.isUserLogin(this);
    }

    /**
     * This method call when user click on practice button
     */
    private void clickOnPractice(){

        if(this.showTeacherFunctionPopUp
                (MathFriendzyHelper.DIALOG_PRACTICE_REPORT
                        , MathFriendzyHelper.getTextFromButton(btnPractice) , true))
            return ;

        this.goForPracticeReport();
    }

    private void goForPracticeReport(){
        if(this.isLogin()){
            Intent intentStudents = new Intent(this,TeacherStudents.class);
            intentStudents.putExtra("callingActivity", "ReportForPracticeAndAssessment");
            startActivity(intentStudents);
        }else{
            this.showRegistratinoDialog();
        }
    }

    /**
     * This method call when user click on students account
     */
    private void clickOnStudentsAccount(){

        if(this.showTeacherFunctionPopUp
                (MathFriendzyHelper.DIALOG_STUDENT_ACCOUNT ,
                        MathFriendzyHelper.getTextFromButton(btnStudentsAccount) , true))
            return ;

        this.goForStudentAccount();
    }

    private Activity getActivityObj(){
        return this;
    }

    private void goForStudentAccount(){
        if(this.isLogin()){
            MathFriendzyHelper.getSchoolClasses(this , new HttpResponseInterface() {
                @Override
                public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                    Intent intentStudents = new Intent(getActivityObj(),StudentAccount.class);
                    startActivity(intentStudents);
                }
            } , true);
            /*Intent intentStudents = new Intent(this,StudentAccount.class);
            startActivity(intentStudents);*/
        }else{
            this.showRegistratinoDialog();
        }
    }

    /**
     * Get Assessment date from server for standards
     * @author Yashwant Singh
     *
     */
    class GetUpdatedDateAssessment extends AsyncTask<Void, Void, String>{

        private String userId;
        private String playerId;
        private ProgressDialog pd;
        private int lang = 1;

        GetUpdatedDateAssessment(String userId , String playerId){
            this.userId = userId;
            this.playerId = playerId;
            if(CommonUtils.getUserLanguageCode(ReportForPracticeAndAssessment.this) == CommonUtils.ENGLISH)
                lang = CommonUtils.ENGLISH;
            else
                lang = CommonUtils.SPANISH;
        }

        @Override
        protected void onPreExecute() {
            pd = CommonUtils.getProgressDialog(ReportForPracticeAndAssessment.this);
            pd.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            AssessmentTestServerOperation serverObj = new AssessmentTestServerOperation();
            String date = serverObj.getUpdatedDateAssessment(lang);
            return date;
        }

        @Override
        protected void onPostExecute(String date) {
            pd.cancel();

            if(date != null){
                SharedPreferences sharedPrefForData = getSharedPreferences
                        (ICommonUtils.DATE_ASSESSMENT_STANDARD_DOWNLOAD, 0);

                int grade = 1;
                AssessmentTestImpl implObj = new AssessmentTestImpl(ReportForPracticeAndAssessment.this);
                implObj.openConnection();
                boolean isStandardLoaded = implObj.isStandard(grade);

                if(isStandardLoaded){
                    if(!sharedPrefForData.getString("assessmentStandardDate", "").equals(date)){
                        downLoadStandars(sharedPrefForData, date , lang , userId , playerId);
                    }else{
                        goOnNextScreen(userId , playerId);
                    }
                }else{
                    downLoadStandars(sharedPrefForData, date , lang , userId , playerId);
                }
                implObj.closeConnection();
            }else{
                CommonUtils.showInternetDialog(ReportForPracticeAndAssessment.this);
            }
            super.onPostExecute(date);
        }
    }

    /**
     * This method call when ready to download standards
     * @param sharedPrefForData
     * @param date
     * @param lang
     */
    private void downLoadStandars(SharedPreferences sharedPrefForData , String date , int lang
            ,String userId , String playerId){
        SharedPreferences.Editor editor = sharedPrefForData.edit();
        editor.putString("assessmentStandardDate", date);
        editor.commit();

        new GetAssessmentStandards(userId, playerId, lang).execute(null,null,null);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
        super.onBackPressed();
    }
    /**
     * Get Assessment Standards
     * @author Yashwant Singh
     *
     */
    class GetAssessmentStandards extends AsyncTask<Void, Void, AssessmentStandardDto>{

        private ProgressDialog pd;
        private int lang;
        private String userId;
        private String playerId;

        GetAssessmentStandards(String userId , String playerId , int lang){
            this.lang = lang;
            this.userId = userId;
            this.playerId = playerId;
        }

        @Override
        protected void onPreExecute() {
            pd = CommonUtils.getProgressDialog(ReportForPracticeAndAssessment.this);
            pd.show();
            super.onPreExecute();
        }

        @Override
        protected AssessmentStandardDto doInBackground(Void... params) {

            AssessmentTestServerOperation serverObj = new AssessmentTestServerOperation();
            AssessmentStandardDto assessmentStandard = serverObj.getAssessmentStandards(lang);
            if(assessmentStandard != null){
                try{
                    AssessmentTestImpl implObj = new AssessmentTestImpl(ReportForPracticeAndAssessment.this);
                    implObj.openConnection();
                    implObj.deleteFromWordAssessmentStandards();
                    implObj.deleteFromWordAssessmentSubStandards();
                    implObj.deleteFromWordAssessmentSubCategoriesInfo();
                    implObj.insertIntoWordAssessmentStandards(assessmentStandard.getStandardDataList());
                    implObj.insertIntoWordAssessmentSubStandards(assessmentStandard.getStandardDataList());
                    implObj.insertIntoWordAssessmentSubCategoriesInfo(assessmentStandard.getCategoriesList());
                    implObj.closeConnection();
                }catch(Exception e){
                    Log.e("AssessmentClass", "No Data on Server For Stabdards " + e.toString());
                }
            }
            return assessmentStandard;
        }

        @Override
        protected void onPostExecute(AssessmentStandardDto assessmentStandard) {
            pd.cancel();

            if(assessmentStandard != null)
                goOnNextScreen(userId , playerId);
            else
                CommonUtils.showInternetDialog(ReportForPracticeAndAssessment.this);

            super.onPostExecute(assessmentStandard);
        }
    }

    /**
     * This method call when user click on any of the button either on
     * Practice , Assessment reports and Create challenge
     */
    private void clickOnAnyButtonForSubscription(int clickFor){
        if(CommonUtils.isInternetConnectionAvailable(this))	{
            new GetRequiredCoinsForPurchaseItem(clickFor).execute(null,null,null);
        }
        else{
            CommonUtils.showInternetDialog(this);
        }
    }

    /**
     * This class get coins from server
     * @author Shilpi
     *
     */
    class GetRequiredCoinsForPurchaseItem extends AsyncTask<Void, Void, Void>
    {
        private String apiString = null;
        private CoinsFromServerObj coindFromServer;
        private ProgressDialog pg = null;
        String userId;
        String playerId;
        private int clickFor = 1;

        GetRequiredCoinsForPurchaseItem(int clickFor){

            SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0);
            if(!sheredPreferencePlayer.getString(PLAYER_ID, "").equals("")){
                userId   = sheredPreferencePlayer.getString("userId", "");
                playerId = sheredPreferencePlayer.getString("playerId", "");
            }else{
                UserRegistrationOperation userObj1 = new UserRegistrationOperation(ReportForPracticeAndAssessment.this);
                RegistereUserDto regUserObj = userObj1.getUserData();
                String userId = regUserObj.getUserId();
            }

            apiString = "userId="+userId+"&playerId="+playerId;

            this.clickFor = clickFor;
        }

        @Override
        protected void onPreExecute()
        {
            pg = CommonUtils.getProgressDialog(ReportForPracticeAndAssessment.this);
            pg.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            LearningCenteServerOperation learnignCenterOpr = new LearningCenteServerOperation();
            coindFromServer = learnignCenterOpr.getSubscriptionInfoForUser(apiString);
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            pg.cancel();
            if(coindFromServer != null){
                //update app status in local database
                if(coindFromServer.getAppUnlock() != -1){

                    ArrayList<PurchaseItemObj> purchaseItem = new ArrayList<PurchaseItemObj>();
                    PurchaseItemObj purchseObj = new PurchaseItemObj();
                    purchseObj.setUserId(userId);
                    purchseObj.setItemId(100);
                    purchseObj.setStatus(coindFromServer.getAppUnlock());
                    purchaseItem.add(purchseObj);

                    LearningCenterimpl learningCenter = new LearningCenterimpl(ReportForPracticeAndAssessment.this);
                    learningCenter.openConn();

                    learningCenter.deleteFromPurchaseItem(userId , 100);
                    learningCenter.insertIntoPurchaseItem(purchaseItem);
                    learningCenter.closeConn();
                }

                if(coindFromServer.getAppUnlock() == 1){
                    if(clickFor == 1)
                        clickOnPractice();
                    else if(clickFor == 2)
                        clickOnAssessment();
                    else if(clickFor == 3)
                        goOnCreateChallengerActivity();
                }
                else{
                    DialogGenerator dg = new DialogGenerator(ReportForPracticeAndAssessment.this);
                    dg.generateDailogForSchoolCurriculumUnlock(coindFromServer , userId , playerId,
                            coindFromServer.getAppUnlock());
                }
            }else{
                CommonUtils.showInternetDialog(ReportForPracticeAndAssessment.this);
            }
            super.onPostExecute(result);
        }
    }

    @Override
    protected void onResume() {
        //for show ad dialog
        CommonUtils.showAdDialog(this);
        //end ad dialog
        super.onResume();
    }
}
