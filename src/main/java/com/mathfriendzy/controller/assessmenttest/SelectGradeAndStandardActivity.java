package com.mathfriendzy.controller.assessmenttest;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.model.assessmenttest.AssessmentTestImpl;
import com.mathfriendzy.model.assessmenttest.StandardsDto;
import com.mathfriendzy.model.grade.Grade;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;

import java.util.ArrayList;

import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;


public class SelectGradeAndStandardActivity extends ActBaseClass {

	private TextView txtTop 			= null;
	private TextView txtSelectAGrade 	= null;
	private TextView txtSelectAStandard = null;
	private Spinner  spinnerGrade 		= null;
	private LinearLayout selectGradeAndstandardlayout = null;

	private int playerSelectedGrade = 1;
	private String userId 	= null;
	private String playerId = null;

	private final String TAG = this.getClass().getSimpleName();

	//for displaying list
	private RelativeLayout childLayout    = null;
	private TextView     txtStandardName  = null;
	private ArrayList<RelativeLayout> layoutList = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_grade_and_standard);

		this.setWidgetsReferences();
		this.setTranslationText();

		SharedPreferences sharedPrefPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		playerSelectedGrade = sharedPrefPlayerInfo.getInt("grade", 1);
		userId              = sharedPrefPlayerInfo.getString("userId", "");
		playerId            = sharedPrefPlayerInfo.getString("playerId", "");

		//changes for 1-8
		if(playerSelectedGrade > 8)
			playerSelectedGrade = 8;
		//end changes

		this.getGrade(playerSelectedGrade + "");
		this.getStandardsFromDatabase(playerSelectedGrade);
	}

	/**
	 * This method set the widgets references from layout
	 */
	private void setWidgetsReferences(){
		txtTop 				= (TextView) findViewById(R.id.txtTop);
		txtSelectAGrade 	= (TextView) findViewById(R.id.txtSelectAGrade);
		txtSelectAStandard 	= (TextView) findViewById(R.id.txtSelectAStandard);
		spinnerGrade        = (Spinner)  findViewById(R.id.spinnerGrade);
		selectGradeAndstandardlayout = (LinearLayout) findViewById(R.id.selectGradeAndstandardlayout);
	}

	/**
	 * This method set the translation text
	 */
	private void setTranslationText(){
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		txtTop.setText(transeletion.getTranselationTextByTextIdentifier("lblAssessmentTest"));
		txtSelectAGrade.setText(transeletion.getTranselationTextByTextIdentifier("lblSelectAGrade") + ":");
		txtSelectAStandard.setText(transeletion.getTranselationTextByTextIdentifier("lblSelectAStandard") + ":");
		transeletion.closeConnection();
	}

	/** 
	 * @Descritpion getGradeData from database 
	 * @param
	 * @param gradeValue
	 */
	private void getGrade(String gradeValue)
	{			
		Grade gradeObj = new Grade();
		ArrayList<String> gradeList = gradeObj.getGradeList(this);
		this.setGradeAdapter(gradeValue , gradeList);
	}

	/**
	 * @Description set Grade data to adapter
	 * @param
	 * @param gradeValue
	 * @param gradeList 
	 */
	private void setGradeAdapter(final String gradeValue, final ArrayList<String> gradeList)
	{		
		//changes for 1-8
		gradeList.subList(8, 13).clear();
		//end changes

		ArrayAdapter<String> gradeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,gradeList);
		gradeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerGrade.setAdapter(gradeAdapter);
		spinnerGrade.setSelection(gradeAdapter.getPosition(gradeValue));

		spinnerGrade.setOnItemSelectedListener(new OnItemSelectedListener() 
		{	@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
			((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);

			if(!MainActivity.isTab)
				((TextView) parent.getChildAt(0)).setTextSize(10);
			((TextView) parent.getChildAt(0)).setTypeface(null, Typeface.BOLD);

			int currentSelectedGrade = Integer.parseInt(spinnerGrade.getSelectedItem().toString());
			if(playerSelectedGrade != currentSelectedGrade){
				//changes for Spanish
				CommonUtils.isOneTimeClick = true;
				//end changes
				playerSelectedGrade = currentSelectedGrade;
				getStandardsFromDatabase(playerSelectedGrade);
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {

		}
		});
	}

	/**
	 * This method getStandards from database
	 * @param grade
	 */
	private void getStandardsFromDatabase(int grade){
		AssessmentTestImpl implObj = new AssessmentTestImpl(this);
		implObj.openConnection();
		ArrayList<StandardsDto> standardsList = implObj.getStandardsByGrade(grade);
		implObj.closeConnection();
		this.createStandardLayoutList(standardsList);
	}

	/**
	 * This method create the standards layout
	 * @param standardList
	 */
	private void createStandardLayoutList(final ArrayList<StandardsDto> standardList){

		selectGradeAndstandardlayout.removeAllViews();
		layoutList = new ArrayList<RelativeLayout>();

		if(standardList.size() > 0 ){
			for(int i = 0 ; i < standardList.size() ; i ++ ){
				LayoutInflater inflater = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
				childLayout = (RelativeLayout) inflater.inflate(R.layout.studentslistlayout, null);
				txtStandardName = (TextView) childLayout.findViewById(R.id.txtStudentName);
				txtStandardName.setText(standardList.get(i).getName());

				childLayout.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						for(int i = 0 ; i < layoutList.size() ; i ++ ){
							if(layoutList.get(i) == v){
								clickOnStandardToPlayAssessment(standardList.get(i));
							}
						}
					}
				});

				layoutList.add(childLayout);
				selectGradeAndstandardlayout.addView(childLayout);
			}
		}else{
			Log.e(TAG, "Not Sufficient Standards");
			CommonUtils.showInternetDialog(SelectGradeAndStandardActivity.this);
		}
	}

	/**
	 * This method call when user click on standards to play assessment
	 * @param standardsDto
	 */
	private void clickOnStandardToPlayAssessment(StandardsDto standardsDto){

		if(this.getRounds(userId , playerId , standardsDto.getStdId()) 
				< CommonUtils.MAX_ATTEMPT_FOR_PLAY_SAME_STANDARD){

			if(CommonUtils.isValidplayDate(this.getLastPlayDate(userId, playerId, standardsDto.getStdId()),
			//if(CommonUtils.isValidplayDate(this.getLastPlayDate("31", "450", standardsDto.getStdId()),
					this)){
				Intent intent = new Intent(SelectGradeAndStandardActivity.this , 
						NewAssessmentTestActivitity.class);
				intent.putExtra("selectedGrade", playerSelectedGrade);
				intent.putExtra("selectedStandardId", standardsDto.getStdId());
				intent.putExtra("selectedStandsrd", standardsDto.getName());
				startActivity(intent);
			}
		}else{
			DialogGenerator dg = new DialogGenerator(SelectGradeAndStandardActivity.this);
			Translation transeletion = new Translation(SelectGradeAndStandardActivity.this);
			transeletion.openConnection();
			/*dg.generateWarningDialog("You can take an assessment test for this standard a maximum of "
			+ MAX_ATTEMPT_FOR_PLAY_SAME_STANDARD + " times.", transeletion.getTranselationTextByTextIdentifier("pickerClose"));*/
			dg.generateWarningDialog
			(transeletion.getTranselationTextByTextIdentifier
					("lblYouCanTakeAssessmentMaxTimeOnly").replace("___", CommonUtils.MAX_ATTEMPT_FOR_PLAY_SAME_STANDARD +"")
					, transeletion.getTranselationTextByTextIdentifier("pickerClose"));
			transeletion.closeConnection();
		}
	}

	/**
	 * This method return the number of rounds which are played by player
	 * if rounds is less then 4 then able to play
	 * @param userId
	 * @param playerId
	 * @param stdId
	 * @return
	 */
	private int getRounds(String userId , String playerId , int stdId){
		int rounds = 1;
		AssessmentTestImpl implObj = new AssessmentTestImpl(this);
		implObj.openConnection();
		rounds = implObj.getRounds(userId, playerId, stdId);
		implObj.closeConnection();
		return rounds;
	}

	/**
	 * This method return the last play date
	 * @param userId
	 * @param playerId
	 * @param stdId
	 * @return
	 */
	private String getLastPlayDate(String userId , String playerId , int stdId){
		String lastPlayDate = "";
		AssessmentTestImpl implObj = new AssessmentTestImpl(this);
		implObj.openConnection();
		lastPlayDate = implObj.getLastPlayDate(userId, playerId, stdId);
		implObj.closeConnection();
		return lastPlayDate;
	}
	
	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}
}
