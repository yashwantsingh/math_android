package com.mathfriendzy.controller.assessmenttest;

import android.content.Context;
import android.os.AsyncTask;

import com.mathfriendzy.model.assessmenttest.AssessmentTestServerOperation;

/**
 * This asyncktask add assement test result on server
 * @author Yashwant Singh
 *
 */
public class AddAssessmentScoreOnServer  extends AsyncTask<Void, Void, Void>{

	private MathAssessmentTestResultObj assessmentTestResultObj = null;
	private Context context = null;
	
	public AddAssessmentScoreOnServer(MathAssessmentTestResultObj assessmentTestResultObj , Context context){
		this.assessmentTestResultObj = assessmentTestResultObj;
		this.context = context;
	}
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		AssessmentTestServerOperation serverObj = new AssessmentTestServerOperation();
		serverObj.addTestAssessmentScore(assessmentTestResultObj);
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
	}
}
