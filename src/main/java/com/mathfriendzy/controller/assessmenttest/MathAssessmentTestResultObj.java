package com.mathfriendzy.controller.assessmenttest;

import java.io.Serializable;
import java.util.HashMap;

public class MathAssessmentTestResultObj implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String userId;
	private String playerId;
	private int grade;
	private String problems;
	private int isCompleted;
	private String date;
	private String questionRemaining;

	//added for save assessment test score on server
	private int correctScore;

	//changes for New Assessment
	private HashMap<Integer, NewAssessmentTestScore> scoreAndTimeForSubStandsrs;
	private int standardId;
	private String setSubStandardsQuestionJson;
	private String subStandarsTimeAndScore;
	
	public String getSubStandarsTimeAndScore() {
		return subStandarsTimeAndScore;
	}

	public void setSubStandarsTimeAndScore(String subStandarsTimeAndScore) {
		this.subStandarsTimeAndScore = subStandarsTimeAndScore;
	}

	public String getSetSubStandardsQuestionJson() {
		return setSubStandardsQuestionJson;
	}
	
	public void setSetSubStandardsQuestionJson(String setSubStandardsQuestionJson) {
		this.setSubStandardsQuestionJson = setSubStandardsQuestionJson;
	}
	public int getStandardId() {
		return standardId;
	}
	public void setStandardId(int standardId) {
		this.standardId = standardId;
	}
	public HashMap<Integer, NewAssessmentTestScore> getScoreAndTimeForSubStandsrs() {
		return scoreAndTimeForSubStandsrs;
	}
	public void setScoreAndTimeForSubStandsrs(
			HashMap<Integer, NewAssessmentTestScore> scoreAndTimeForSubStandsrs) {
		this.scoreAndTimeForSubStandsrs = scoreAndTimeForSubStandsrs;
	}

	public int getCorrectScore() {
		return correctScore;
	}
	public void setCorrectScore(int correctScore) {
		this.correctScore = correctScore;
	}

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public int getGrade() {
		return grade;
	}
	public void setGrade(int grade) {
		this.grade = grade;
	}
	public String getProblems() {
		return problems;
	}
	public void setProblems(String problems) {
		this.problems = problems;
	}
	public int getIsCompleted() {
		return isCompleted;
	}
	public void setIsCompleted(int isCompleted) {
		this.isCompleted = isCompleted;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getQuestionRemaining() {
		return questionRemaining;
	}
	public void setQuestionRemaining(String questionRemaining) {
		this.questionRemaining = questionRemaining;
	}


}
