package com.mathfriendzy.controller.assessmenttest;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.dawnloadimagesfromserver.DawnloadImagesFromserver;
import com.mathfriendzy.model.assessmenttest.AssessmentTestImpl;
import com.mathfriendzy.model.assessmenttest.CatagoriesDto;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.LearnignCenterSchoolCurriculumServerOperation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.QuestionLoadedTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SchoolCurriculumLearnignCenterimpl;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.UpdatedInfoTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.WordProblemQuestionTransferObj;
import com.mathfriendzy.model.schoolcurriculum.singlefriendzy.SingleFriendzyServerOperationForWordProblem;
import com.mathfriendzy.model.spanishchanges.SpanishChangesImpl;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;

public class NewAssessmentTestActivitity extends ActBaseClass implements OnClickListener{

	private TextView txtTop 			= null;
	private TextView txtSelectedGrade   = null;
	private TextView txtSelectedStandard= null;
	private TextView txtYouAreAboutToStart = null;
	private Button   btnStartTest       = null;

	//values from previous screen or Activity
	private int playerSelectedGrade = 1;
	private String selectedStandardName = null;
	private int selectedStandardId	= 1;
	private ArrayList<CatagoriesDto> categoryInfoList = null;

	private final String TAG = this.getClass().getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_assessment_test_activitity);

		this.getIntentValues();
		this.setWidgetsReferences();
		this.setTextFromTranslation();
		this.getSubCategoriesInfoByStandardId(selectedStandardId);
		this.setListenerOnWidgets();
	}

	/**
	 * This method get the intent values which are set in previous screen
	 */
	private void getIntentValues(){
		playerSelectedGrade 	= this.getIntent().getIntExtra("selectedGrade", 1);
		selectedStandardName 	= this.getIntent().getStringExtra("selectedStandsrd");
		selectedStandardId      = this.getIntent().getIntExtra("selectedStandardId", 1);
	}

	/**
	 * This method set the widgets references from layout
	 */
	private void setWidgetsReferences(){
		txtTop 					= (TextView) findViewById(R.id.txtTop);
		txtSelectedGrade 		= (TextView) findViewById(R.id.txtSelectedGrade);
		txtSelectedStandard 	= (TextView) findViewById(R.id.txtSelectedStandard);
		txtYouAreAboutToStart 	= (TextView) findViewById(R.id.txtYouAreAboutToStart);
		btnStartTest            = (Button)   findViewById(R.id.btnStartTest);
	}


	/**
	 * This method set test from translation
	 */
	private void setTextFromTranslation(){
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		txtTop.setText(transeletion.getTranselationTextByTextIdentifier("lblAssessmentTest"));
		txtYouAreAboutToStart.setText(transeletion.getTranselationTextByTextIdentifier("lblYouAreAboutToStartTest")
				+ " " + CommonUtils.MAX_ATTEMPT_FOR_PLAY_SAME_STANDARD + " "
				+ transeletion.getTranselationTextByTextIdentifier("lblTimesAndOnlyOnce")
				+ " 7 " + transeletion.getTranselationTextByTextIdentifier("lblDays"));

		btnStartTest.setText(transeletion.getTranselationTextByTextIdentifier("lblStartTest"));

		//set the player selected data from last Activity or Screen
		txtSelectedGrade.setText(this.setGrade(playerSelectedGrade) + " " + 
				transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade"));
		txtSelectedStandard.setText(selectedStandardName);
		transeletion.closeConnection();
	}

	/**
	 * This method getSubCategoriesInfo by standard id
	 * @param standardId
	 */
	private void getSubCategoriesInfoByStandardId(int standardId){
		AssessmentTestImpl implObj = new AssessmentTestImpl(this);
		implObj.openConnection();
		categoryInfoList = implObj.getSubCategoriesInfoByStandardId(standardId);
		implObj.closeConnection();
	}

	/**
	 * use to set standard with grade
	 * @param grade
	 */
	private String setGrade(int grade)
	{
		String alias;
		switch(grade)
		{
		case 1:
			alias = "1st";
			break;
		case 2:
			alias = "2nd";
			break;
		case 3:
			alias = "3rd";
			break;
		case 13:
			alias = "adult";
		default :
			alias = grade+"th";
		}	
		return alias;

	}//END setGrade method

	private void setListenerOnWidgets(){
		btnStartTest.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnStartTest :
			this.clickOnStartTest();
			break;
		}

	}

	/**
	 * This method call when user click on starttest
	 */
	private void clickOnStartTest(){
		this.loadQuestionFromServer(playerSelectedGrade);
	}

	/**
	 * This method call when user move on next play assessment test screen
	 */
	private void goForNextScreen(){
		Intent intent = new Intent(this , AssessmentPlayActivity.class);
		intent.putExtra("categoriesInfoList", categoryInfoList);
		intent.putExtra("selectedGrade", playerSelectedGrade);
		intent.putExtra("selectedStandardId", selectedStandardId);
		startActivity(intent);
	}

	/**
	 * This method call when user click on grade 
	 */
	private void loadQuestionFromServer(int grade){

		if(CommonUtils.getUserLanguageCode(this) == CommonUtils.ENGLISH){
			SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl = 
					new SchoolCurriculumLearnignCenterimpl(this);
			schooloCurriculumImpl.openConnection();
			boolean isQuestionLoaded = schooloCurriculumImpl.isQuestionLeaded(grade);
			schooloCurriculumImpl.closeConnection();

			if(isQuestionLoaded){
				if(CommonUtils.isInternetConnectionAvailable(this)){
					new GetWordProblemQuestionUpdatedDate(grade , false).execute(null,null,null);
				}else{
					this.goForNextScreen();
				}
			}
			else{
				if(CommonUtils.isInternetConnectionAvailable(this)){
					new GetWordProblemQuestion(grade ,this)
					.execute(null,null,null);

					new GetWordProblemQuestionUpdatedDate(grade , true).execute(null,null,null);
				}else{
					DialogGenerator dg = new DialogGenerator(this);
					Translation transeletion = new Translation(this);
					transeletion.openConnection();
					dg.generateWarningDialog(transeletion.
							getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
					transeletion.closeConnection();	
				}
			}
		}else{
			this.callWhenUserLanguageIsSpanish(this, CommonUtils.SPANISH, grade);
		}
	}
	//end changes

	//changes for Spanish
	/**
	 * This method call when user language is Spanish
	 * This method for downloading question from server for Spanish Language
	 */
	private void callWhenUserLanguageIsSpanish(Context context , int lang , int grade){
		try {
			SpanishChangesImpl spanishImplObj = new SpanishChangesImpl(context);
			spanishImplObj.openConn();
			boolean isSpanishQuestionLoaded = spanishImplObj.isQuestionLeaded(grade);
			if(isSpanishQuestionLoaded){
				if(CommonUtils.isInternetConnectionAvailable(context)){
					new GetWordProblemQuestionUpdatedDate(grade , false).execute(null,null,null);
				}else{
					this.goForNextScreen();
				}
			}
			else{
				if(CommonUtils.isInternetConnectionAvailable(context)){
					new GetWordProblemQuestionUpdatedDate(grade , true).execute(null,null,null);
					new GetWordProblemQuestion(grade ,context)
					.execute(null,null,null);
				}else{
					DialogGenerator dg = new DialogGenerator(context);
					Translation transeletion = new Translation(context);
					transeletion.openConnection();
					dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
					transeletion.closeConnection();	
				}
			}
			spanishImplObj.closeConn();

		} catch (Exception e) {
			Log.e(TAG, "inside callWhenUserLanguageIsSpanish " + e.toString());
		} 
	}

	//for downloading questions
	/**
	 * This class get word problem updated dates from server
	 * @author Yashwant Singh
	 *
	 */
	private class GetWordProblemQuestionUpdatedDate extends AsyncTask<Void, Void, ArrayList<UpdatedInfoTransferObj>>{
		private int grade;
		private ProgressDialog pd;
		private boolean isFisrtTimeCallForGrade;

		GetWordProblemQuestionUpdatedDate(int grade , boolean isFisrtTimeCallForGrade){
			this.grade = grade;
			this.isFisrtTimeCallForGrade = isFisrtTimeCallForGrade;
		}

		@Override
		protected void onPreExecute() {
			if(isFisrtTimeCallForGrade == false){
				pd = CommonUtils.getProgressDialog(NewAssessmentTestActivitity.this);
				pd.show();
			}
			super.onPreExecute();
		}

		@Override
		protected ArrayList<UpdatedInfoTransferObj>  doInBackground(Void... params) {
			SingleFriendzyServerOperationForWordProblem serverObj = 
					new SingleFriendzyServerOperationForWordProblem();
			//changes for Spanish
			ArrayList<UpdatedInfoTransferObj>  updatedList = new ArrayList<UpdatedInfoTransferObj>();
			if(CommonUtils.getUserLanguageCode(NewAssessmentTestActivitity.this) == CommonUtils.ENGLISH)
				updatedList = serverObj.getWordProbleQuestionUpdatedData();
			else//for Spanish
				updatedList = serverObj.getWordProbleQuestionUpdatedDateForSpanish(CommonUtils.SPANISH);
			return updatedList;
		}

		@Override
		protected void onPostExecute(ArrayList<UpdatedInfoTransferObj>  updatedList) {
			if(isFisrtTimeCallForGrade == false){
				pd.cancel();
			}

			if(updatedList != null){
				SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl = 
						new SchoolCurriculumLearnignCenterimpl(NewAssessmentTestActivitity.this);
				schooloCurriculumImpl.openConnection();

				//changes for Spanish
				SpanishChangesImpl implObj = new SpanishChangesImpl(NewAssessmentTestActivitity.this);
				implObj.openConn();

				if(isFisrtTimeCallForGrade){
					//changes for Spanish
					if(CommonUtils.getUserLanguageCode(NewAssessmentTestActivitity.this) == CommonUtils.ENGLISH){
						if(!schooloCurriculumImpl.isWordProblemUpdateDetailsTableDataExist()){
							schooloCurriculumImpl.insertIntoWordProblemUpdateDetails(updatedList);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
						}else{
							String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
							schooloCurriculumImpl.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
						}
					}else{//for Spanish
						if(!implObj.isWordProblemUpdateDetailsTableDataExist()){
							implObj.insertIntoWordProblemUpdateDetails(updatedList);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
						}else{
							String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
							implObj.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
						}
					}
				}
				else{

					if(CommonUtils.getUserLanguageCode(NewAssessmentTestActivitity.this) == CommonUtils.ENGLISH){
						String updatedDateFromDatabase = schooloCurriculumImpl.getUpdatedDate(grade);
						if(CommonUtils.isUpdateDateChange(updatedDateFromDatabase , grade , updatedList))
						{
							String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
							schooloCurriculumImpl.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();

							if(CommonUtils.isInternetConnectionAvailable
									(NewAssessmentTestActivitity.this)){
								new GetWordProblemQuestion(grade , NewAssessmentTestActivitity.this)
								.execute(null,null,null);
							}else{
								goForNextScreen();
							}
						}
						else{
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
							goForNextScreen();
						}

					}else{//for Spanish
						String updatedDateFromDatabase = implObj.getUpdatedDate(grade);
						if(CommonUtils.isUpdateDateChange(updatedDateFromDatabase , grade , updatedList))
						{
							String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
							implObj.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();

							if(CommonUtils.isInternetConnectionAvailable(NewAssessmentTestActivitity.this)){
								new GetWordProblemQuestion(grade , NewAssessmentTestActivitity.this)
								.execute(null,null,null);
							}else{
								goForNextScreen();
							}
						}
						else{
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
							goForNextScreen();
						}
					}
				}
			}else{
				if(isFisrtTimeCallForGrade == false){
					CommonUtils.showInternetDialog(NewAssessmentTestActivitity.this);
				}
			}
			super.onPostExecute(updatedList);
		}
	}

	//changes for word problem
	/**
	 * This asynctask get questions from server according to grade if not loaded in database
	 * @author Yashwant Singh
	 *
	 */
	public class GetWordProblemQuestion extends AsyncTask<Void, Void, QuestionLoadedTransferObj>{

		private int grade;
		private ProgressDialog pd;
		private Context context;
		ArrayList<String> imageNameList = new ArrayList<String>();

		public GetWordProblemQuestion(int grade , Context context){
			this.grade 		= grade;
			this.context 	= context;
		}

		@Override
		protected void onPreExecute() {

			Translation translation = new Translation(context);
			translation.openConnection();
			pd = CommonUtils.getProgressDialog(context, translation.getTranselationTextByTextIdentifier
					("alertPleaseWaitWeAreDownloading") , true);
			translation.closeConnection();
			pd.show();

			super.onPreExecute();
		}

		@Override
		protected QuestionLoadedTransferObj doInBackground(Void... params) {
			LearnignCenterSchoolCurriculumServerOperation serverObj = new LearnignCenterSchoolCurriculumServerOperation();
			//QuestionLoadedTransferObj questionData = serverObj.getWordProblemQuestions(grade);
			//changes for Spanish Changes
			QuestionLoadedTransferObj questionData = new QuestionLoadedTransferObj();
			if(CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH)
				questionData = serverObj.getWordProblemQuestions(grade);
			else
				questionData = serverObj.getWordProblemQuestionsForSpanish(grade, CommonUtils.SPANISH);

			if(questionData != null){
				SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl = new SchoolCurriculumLearnignCenterimpl(context);
				schooloCurriculumImpl.openConnection();
				if(CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH){
					//changes for dialog loading wheel
					schooloCurriculumImpl.deleteFromWordProblemCategoriesbyGrade(grade);
					schooloCurriculumImpl.deleteFromWordProblemsSubCategoriesByCategoryId(questionData.getCategoryList());
					schooloCurriculumImpl.deleteFromWordProblemsQuestionsCategoryId(questionData.getCategoryList());

					schooloCurriculumImpl.insertIntoWordProblemCategories(questionData.getCategoryList(), grade);
					schooloCurriculumImpl.insertIntoWordProblemsSubCategories(questionData.getCategoryList());
					schooloCurriculumImpl.insertIntoWordProblemsQuestions(questionData.getQuestionList());
				}else{
					SpanishChangesImpl implObj = new SpanishChangesImpl(context);
					implObj.openConn();

					implObj.deleteFromWordProblemCategoriesbyGrade(grade);
					implObj.deleteFromWordProblemsSubCategoriesByCategoryId(questionData.getCategoryList());
					implObj.deleteFromWordProblemsQuestionsCategoryId(questionData.getCategoryList());

					implObj.insertIntoWordProblemCategories(questionData.getCategoryList(), grade);
					implObj.insertIntoWordProblemsSubCategories(questionData.getCategoryList());
					implObj.insertIntoWordProblemsQuestions(questionData.getQuestionList());
					implObj.closeConn();
				}

				for(int i = 0 ; i < questionData.getQuestionList().size() ; i ++ ){

					WordProblemQuestionTransferObj questionObj = questionData.getQuestionList().get(i);

					if(questionObj.getQuestion().contains(".png"))
						imageNameList.add(questionObj.getQuestion());
					if(questionObj.getOpt1().contains(".png"))
						imageNameList.add(questionObj.getOpt1());
					if(questionObj.getOpt2().contains(".png"))
						imageNameList.add(questionObj.getOpt2());
					if(questionObj.getOpt3().contains(".png"))
						imageNameList.add(questionObj.getOpt3());
					if(questionObj.getOpt4().contains(".png"))
						imageNameList.add(questionObj.getOpt4());
					if(questionObj.getOpt5().contains(".png"))
						imageNameList.add(questionObj.getOpt5());
					if(questionObj.getOpt6().contains(".png"))
						imageNameList.add(questionObj.getOpt6());
					if(questionObj.getOpt7().contains(".png"))
						imageNameList.add(questionObj.getOpt7());
					if(questionObj.getOpt8().contains(".png"))
						imageNameList.add(questionObj.getOpt8());
					if(!questionObj.getImage().equals(""))
						imageNameList.add(questionObj.getImage());
				}

				if(!schooloCurriculumImpl.isImageTableExist()){
					schooloCurriculumImpl.createSchoolCurriculumImageTable();
				}
				schooloCurriculumImpl.closeConnection();
				//end changes
			}
			return questionData;
		}

		@Override
		protected void onPostExecute(QuestionLoadedTransferObj questionData) {

			pd.cancel();

			if(questionData != null){
				DawnloadImagesFromserver serverObj = new DawnloadImagesFromserver(imageNameList, context);
				Thread imageDawnLoadThrad = new Thread(serverObj);
				imageDawnLoadThrad.start();

				goForNextScreen();
			}else{
				CommonUtils.showInternetDialog(NewAssessmentTestActivitity.this);
			}

			super.onPostExecute(questionData);
		}
	}
	
	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}
}
