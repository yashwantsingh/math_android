package com.mathfriendzy.controller.assessmenttest;


import java.util.ArrayList;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.controller.learningcenter.schoolcurriculum.SeeAnswerActivityForSchoolCurriculum;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.MathEquationTransferObj;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;
import com.mathfriendzy.utils.PlaySound;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class CongratulationActivityForAssessmentTest extends ActBaseClass implements OnClickListener{

	private TextView txtTileScreen				= null;
	private TextView txtGoodEffort				= null;
	private TextView txtEarned					= null;
	private TextView txtEarnedPoints			= null;
	private TextView txtPlayAgain				= null;
	private Button	 btnAnswer					= null;
	private Button	 btnPlay					= null;
	private TextView txtAssessmentText          = null;

	//for percentage
	private int percentage = 0;

	private int selectedPlayGrade = 1;

	PlaySound playsound = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_congratulation_screen_for_asessment);

		playsound = new PlaySound(this);

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);  

		if(metrics.heightPixels == ICommonUtils.TAB_HEIGHT && metrics.widthPixels == ICommonUtils.TAB_WIDTH && metrics.densityDpi == ICommonUtils.TAB_DENISITY)
		{
			setContentView(R.layout.activity_congratulation_screen_for_assessment_tab_low_denisity);
		}

		this.getIntentValue();
		this.getWidgetId();
		this.setWidgetText();
	}

	/**
	 * This method get Intent value 
	 * which are set in previous screen
	 */
	private void getIntentValue(){
		percentage = this.getIntent().getIntExtra("rightAnswerPercentage", 0);
		selectedPlayGrade = this.getIntent().getIntExtra("selectedGrade", 1);
		
		playsound.playSoundForSchoolSurriculumForResultScreen(this);
	}

	private void getWidgetId() 
	{
		txtTileScreen		= (TextView) findViewById(R.id.txtTitleScreen);		
		txtGoodEffort		= (TextView) findViewById(R.id.txtGoodEffort);
		txtEarned			= (TextView) findViewById(R.id.txtEarned);
		txtEarnedPoints		= (TextView) findViewById(R.id.txtEarnedPoints);
		txtPlayAgain        = (TextView) findViewById(R.id.txtPlayAgain);

		btnAnswer			= (Button) findViewById(R.id.btnAnswer);
		btnPlay				= (Button) findViewById(R.id.btnPlay);

		txtAssessmentText   = (TextView) findViewById(R.id.txtAssessmentText);

		btnAnswer.setOnClickListener(this);
		btnPlay.setOnClickListener(this);
	}//END getWidgetId method

	private void setWidgetText() 
	{
		String text;
		Translation translate = new Translation(this);
		translate.openConnection();

		text = translate.getTranselationTextByTextIdentifier("lblAssessmentTest");
		txtTileScreen.setText(text);

		text = translate.getTranselationTextByTextIdentifier("titleGreatJob");
		txtGoodEffort.setText(text + "!");

		text = translate.getTranselationTextByTextIdentifier("lblYouScored");
		txtEarned.setText(text);

		txtEarnedPoints.setText(percentage + "%");

		text = translate.getTranselationTextByTextIdentifier("lblOnYourAssessment");
		txtAssessmentText.setText(text + ".");

		text = translate.getTranselationTextByTextIdentifier("lblNowYouCanContinuePlayingOrViewYourAnswers");
		txtPlayAgain.setText(text);

		text = translate.getTranselationTextByTextIdentifier("mfLblAnswers");
		btnAnswer.setText(text);

		text = translate.getTranselationTextByTextIdentifier("playTitle");
		btnPlay.setText(text);

		translate.closeConnection();

	}//END setWidgetText method

	@SuppressWarnings("unchecked")
	@Override
	public void onClick(View v) 
	{
		switch (v.getId())
		{
		case R.id.btnAnswer:	
			ArrayList<MathEquationTransferObj> playDataList = (ArrayList<MathEquationTransferObj>) this.getIntent()
			.getSerializableExtra("playDatalist");
			Intent intent1 = new Intent(this , SeeAnswerActivityForSchoolCurriculum.class);
			intent1.putExtra("playDatalist", playDataList);
			intent1.putExtra("isCallForAssessment", true);
			intent1.putExtra("selectedGrade", selectedPlayGrade);
			startActivity(intent1);
			break;

		case R.id.btnPlay:
			startActivity(new Intent(this , MainActivity.class));
			break;
		}

	}//END onClick Method

	@Override
	public void onBackPressed() {
		startActivity(new Intent(this , MainActivity.class));
		super.onBackPressed();
	}

	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}
}
