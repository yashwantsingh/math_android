package com.mathfriendzy.controller.assessmenttest;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.mathfriendzy.controller.assessmenttest.xmlparsing.XmlParser;
import com.mathfriendzy.controller.player.CreateTeacherPlayerActivity;
import com.mathfriendzy.controller.player.LoginUserCreatePlayer;
import com.mathfriendzy.controller.player.LoginUserPlayerActivity;
import com.mathfriendzy.controller.player.TeacherPlayer;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.assessmenttest.AssessmentStandardDto;
import com.mathfriendzy.model.assessmenttest.AssessmentStatusDto;
import com.mathfriendzy.model.assessmenttest.AssessmentTestImpl;
import com.mathfriendzy.model.assessmenttest.AssessmentTestServerOperation;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.learningcenter.CoinsFromServerObj;
import com.mathfriendzy.model.learningcenter.LearningCenteServerOperation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.PurchaseItemObj;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.MathEquationTransferObj;
import com.mathfriendzy.utils.BuildApp;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;

import java.util.ArrayList;

import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_ID;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;

/**
 * This class when user click on assessment test
 * @author Yashwant Singh
 *
 */
public class AssessmentClass {

	private Context context = null;

	public AssessmentClass(Context context) {
		this.context = context;
	}

	/**
	 * This method start the assessment test screen
	 * @param
	 */
	private void goOnAssessmentScreen(String userId , String playerId){

		//changes for NewAssessentTest
		this.goOnAssessmentScreenAfterNewAssessmentChanges(userId,playerId);
	}

	/**
	 * This method created when new changes Assessment Test
	 * @param userId
	 * @param playerId
	 */
	private void goOnAssessmentScreenAfterNewAssessmentChanges(String userId , String playerId){
		if(CommonUtils.isInternetConnectionAvailable(context)){
			new GetUpdatedDateAssessment(userId, playerId).execute(null,null,null);
		}else{
			this.goOnNextScreen(userId, playerId);
		}
	}

	/**
	 * This method call when user is ready to go on next Screen
	 * @param playerId 
	 * @param userId 
	 */
	private void goOnNextScreen(String userId, String playerId){
		//context.startActivity(new Intent(context , SelectGradeAndStandardActivity.class));
		AssessmentTestImpl implObj = new AssessmentTestImpl(context);
		implObj.openConnection();
		MathAssessmentTestResultObj mathAssessmentTestResult = implObj.
				getNewAssessmentTestPlayData(userId, playerId);
		implObj.closeConnection();

		if(mathAssessmentTestResult != null){
			new XmlParser(mathAssessmentTestResult.getProblems() , context);

			ArrayList<MathEquationTransferObj> mathEquationDataList = XmlParser.getPlayDataList();

			//Direct open the play Screen when user already play assessment test or 
			//have data in mathAssessmentTestResult table
			Intent intent = new Intent(context , AssessmentPlayActivity.class);
			intent.putExtra("isDirectFromMain", true);
			intent.putExtra("alreadyPlayDataList", mathEquationDataList);
			intent.putExtra("mathAssessmentTestResultTableData", mathAssessmentTestResult);
			context.startActivity(intent);
		}else{
			//context.startActivity(new Intent(context,AssessmentTestActivity.class));
			context.startActivity(new Intent(context , SelectGradeAndStandardActivity.class));
		}
	}

	/**
	 * This method start the assessment test
	 */
	public void startAssessmentActivity(){
		this.setPlayerData();
	}

	/**
	 * This method set the data into sharedPreff
	 */
	private void setPlayerData(){

		//sharedpreference for holding player info
		SharedPreferences sharedPrefPlayerInfo = context.getSharedPreferences(PLAYER_INFO, 0);
		SharedPreferences.Editor editor = sharedPrefPlayerInfo.edit();

		UserPlayerOperation userPlayer = new UserPlayerOperation(context);
		if(!userPlayer.isUserPlayersExist())
		{
			SharedPreferences sheredPreferencePlayer = context.getSharedPreferences(IS_CHECKED_PREFF, 0); 
			if(sheredPreferencePlayer.getString(PLAYER_ID, "").equals(""))
			{	
				UserRegistrationOperation userObj = new UserRegistrationOperation(context);

				if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
				{
					Intent intent = new Intent(context,TeacherPlayer.class);
					context.startActivity(intent);
				}
				else
				{								
					Intent intent = new Intent(context,LoginUserPlayerActivity.class);
					context.startActivity(intent);
				}
			}
			else
			{
				UserPlayerOperation userPlayerOpr = new UserPlayerOperation(context);
				UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById
						(sheredPreferencePlayer.getString(PLAYER_ID, ""));

				if(userPlayerData != null )
				{
					UserRegistrationOperation userObj = new UserRegistrationOperation(context);
					RegistereUserDto regUserObj = userObj.getUserData();

					editor.clear();
					editor.putString("playerName", userPlayerData.getFirstname() 
							+ " " + userPlayerData.getLastname());
					editor.putString("userName", userPlayerData.getUsername());
					editor.putString("city", regUserObj.getCity());
					editor.putString("state", regUserObj.getState());
					editor.putString("imageName",  userPlayerData.getImageName());
					editor.putString("coins",  userPlayerData.getCoin());
					editor.putInt("grade", Integer.parseInt(userPlayerData.getGrade()));
					editor.putString("userId",  userPlayerData.getParentUserId());
					editor.putString("playerId",  userPlayerData.getPlayerid());
					Country country = new Country();
					editor.putString("countryName", country.getCountryNameByCountryId
							(regUserObj.getCountryId(), context));
					editor.commit();

					/*//check for application unlock status
					this.checkAppUnlockStatus(userPlayerData.getParentUserId(), 
							userPlayerData.getPlayerid());*/
					this.callOnAssessmentTest(userPlayerData.getParentUserId(),
							userPlayerData.getPlayerid());
				}
				else
				{
					UserRegistrationOperation userObj = new UserRegistrationOperation(context);
					if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
					{
						Intent intent = new Intent(context,TeacherPlayer.class);
						context.startActivity(intent);
					}
					else
					{									
						Intent intent = new Intent(context,LoginUserPlayerActivity.class);
						context.startActivity(intent);
					}
				}
			}
		}
		else
		{
			UserRegistrationOperation userObj = new UserRegistrationOperation(context);
			if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
			{
				Intent intent = new Intent(context,CreateTeacherPlayerActivity.class);
				context.startActivity(intent);
			}
			else
			{
				Intent intent = new Intent(context,LoginUserCreatePlayer.class);
				context.startActivity(intent);
			}
		}
	}

	/**
	 * This method call when user tab on assessment test
	 * @param userId
	 * @param playerId
	 */
	private void callOnAssessmentTest(String userId , String playerId){

		if(CommonUtils.isInternetConnectionAvailable(context)){
			AssessmentTestImpl implObj = new AssessmentTestImpl(context);
			implObj.openConnection();
			MathAssessmentTestResultObj mathAssessmentTestResult = implObj.
					getNewAssessmentTestPlayData(userId, playerId);

			if(mathAssessmentTestResult != null){
				if(mathAssessmentTestResult.getIsCompleted() == 1){

					//set question json which is the Score xml into setSubStandarsTimeAndScore
					mathAssessmentTestResult.setSubStandarsTimeAndScore
					(mathAssessmentTestResult.getSetSubStandardsQuestionJson());
					//end changes

					implObj.deleteFromNewWordAssessmentTestResult(mathAssessmentTestResult);

					new AddAssessmentScoreOnServerForCurrentScreen(mathAssessmentTestResult , context)
					.execute(null,null,null);
				}else{
					//check for application unlock status
					this.checkAppUnlockStatus(userId,playerId);
				}
			}else{
				//check for application unlock status
				this.checkAppUnlockStatus(userId,playerId);
			}

			implObj.closeConnection();
		}else{
			//check for application unlock status
			this.checkAppUnlockStatus(userId,playerId);
		}

	}

	/**
	 * This method check application unlock status
	 */
	private void checkAppUnlockStatus(String userId , String playerId){

		if(CommonUtils.isInternetConnectionAvailable(context)){
			if(BuildApp.BUILD_APP_TYPE == BuildApp.AMAZON){
                this.checkLastAssessmentPlayedDate("", userId , playerId);
            }else {
                new GetRequiredCoinsForPurchaseItem().execute(null, null, null);
            }
		}
		else{
			if(this.getApplicationUnLockStatus(100, userId) == 1){
				/*this.checkLastAssessmentPlayedDate(this.getAssessmentLastPlayDateFromLocal(userId, playerId)
						, userId , playerId);*/
				//changes for play assessment test multiple time for different standards
				this.checkLastAssessmentPlayedDate("", userId , playerId);
			}else{
				//will changes according to deepak
				CommonUtils.showInternetDialog(context);
			}
		}
	}

	/**
	 * This method check assessment play date
	 * on the basis of this identify the player is able to play assessment test or not
	 * @param playDate
	 */
	private void checkLastAssessmentPlayedDate(String playDate , String userId , String playerId){
		goOnAssessmentScreen(userId , playerId);

	}



	/**
	 * This method check for application is unlock or not
	 * @param itemId
	 * @param userId
	 * @return
	 */
	private int getApplicationUnLockStatus(int itemId ,String userId)
	{	
		int appStatus = 0;
		LearningCenterimpl learnignCenterImpl = new LearningCenterimpl(context);
		learnignCenterImpl.openConn();
		appStatus = learnignCenterImpl.getAppUnlockStatus(itemId,userId);
		learnignCenterImpl.closeConn();
		return appStatus;
	}

	/**
	 * This class get coins from server
	 * @author Shilpi
	 *
	 */
	class GetRequiredCoinsForPurchaseItem extends AsyncTask<Void, Void, Void>
	{
		private String apiString = null;
		private CoinsFromServerObj coindFromServer;
		private ProgressDialog pg = null;
		String userId;
		String playerId;

		GetRequiredCoinsForPurchaseItem()
		{   
			SharedPreferences sharedPreffPlayerInfo = context.getSharedPreferences(IS_CHECKED_PREFF, 0);
			userId   = sharedPreffPlayerInfo.getString("userId", "");
			playerId = sharedPreffPlayerInfo.getString("playerId", "");
			apiString = "userId="+userId+"&playerId="+playerId;
		}

		@Override
		protected void onPreExecute() 
		{
			pg = CommonUtils.getProgressDialog(context);
			pg.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			LearningCenteServerOperation learnignCenterOpr = new LearningCenteServerOperation();
			coindFromServer = learnignCenterOpr.getSubscriptionInfoForUser(apiString);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{   
			pg.cancel();

			if(coindFromServer != null){
				//update app status in local database
				if(coindFromServer.getAppUnlock() != -1){
					ArrayList<PurchaseItemObj> purchaseItem = new ArrayList<PurchaseItemObj>();
					PurchaseItemObj purchseObj = new PurchaseItemObj();
					purchseObj.setUserId(userId);
					purchseObj.setItemId(100);
					purchseObj.setStatus(coindFromServer.getAppUnlock());
					purchaseItem.add(purchseObj);

					LearningCenterimpl learningCenter = new LearningCenterimpl(context);
					learningCenter.openConn();

					learningCenter.deleteFromPurchaseItem(userId , 100);
					learningCenter.insertIntoPurchaseItem(purchaseItem);		
					learningCenter.closeConn();
				}

				if(coindFromServer.getAppUnlock() == 1)
				{					
					new GetAssessmentDateAndStatus(userId , playerId).execute(null,null,null);
				}
				else
				{				
					/*DialogGenerator dg = new DialogGenerator(context);
					dg.generateDailogForSchoolCurriculumUnlock(coindFromServer , userId , playerId
							, coindFromServer.getAppUnlock());*/
                    MathFriendzyHelper.openSubscriptionDialog(context, MathFriendzyHelper.DIALOG_SCHOOL_ASSESSMENT_TEST);
				}     
			}else{
				CommonUtils.showInternetDialog(context);
			}
			super.onPostExecute(result);
		}
	}

	/**
	 * This asyncktask get assessment date status from server
	 * @author Yashwant Singh
	 *
	 */
	class GetAssessmentDateAndStatus extends AsyncTask<Void, Void, AssessmentStatusDto>{

		private String userId   = null;
		private String playerId = null;
		private ProgressDialog pg = null;

		GetAssessmentDateAndStatus(String userId , String playerId){
			this.userId 	= userId;
			this.playerId 	= playerId;
		}

		@Override
		protected void onPreExecute() {
			pg = CommonUtils.getProgressDialog(context);
			pg.show();
			super.onPreExecute();
		}

		@Override
		protected AssessmentStatusDto doInBackground(Void... params) {
			AssessmentTestServerOperation serverObj = new AssessmentTestServerOperation();
			AssessmentStatusDto assessmentStatusObj = serverObj.getAssessmentDateAndStatus(userId, playerId);
			return assessmentStatusObj;
		}

		@Override
		protected void onPostExecute(AssessmentStatusDto assessmentStatusObj) {
			pg.cancel();

			if(assessmentStatusObj != null){
				AssessmentTestImpl implObj = new AssessmentTestImpl(context);
				implObj.openConnection();
				implObj.deleteFromWordAssessmentStandardsRoundInfo(userId, playerId);
				implObj.insertIntoWordAssessmentStandardsRoundInfo(assessmentStatusObj.getStandardsRoundList(), 
						userId, playerId);
				implObj.closeConnection();

				//changes for playing assessment test multiple time for different standards
				checkLastAssessmentPlayedDate("" , userId , playerId);
			}else{
				CommonUtils.showInternetDialog(context);
			}

			super.onPostExecute(assessmentStatusObj);
		}
	}

	//added , when update assessment score on server at the assessment click on main screen
	class AddAssessmentScoreOnServerForCurrentScreen  extends AsyncTask<Void, Void, Void>{

		private MathAssessmentTestResultObj assessmentTestResultObj = null;
		private Context context = null;
		private ProgressDialog pd = null;

		AddAssessmentScoreOnServerForCurrentScreen(MathAssessmentTestResultObj assessmentTestResultObj , 
				Context context){
			this.assessmentTestResultObj = assessmentTestResultObj;
			this.context = context;
		}

		@Override
		protected void onPreExecute() {
			pd = CommonUtils.getProgressDialog(context);
			pd.show();

			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			AssessmentTestServerOperation serverObj = new AssessmentTestServerOperation();
			serverObj.addTestAssessmentScore(assessmentTestResultObj);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			pd.cancel();

			checkAppUnlockStatus(assessmentTestResultObj.getUserId(), 
					assessmentTestResultObj.getPlayerId());

			super.onPostExecute(result);
		}
	}

	//changes for NewAssessment
	/**
	 * Get Assessment date from server for standards
	 * @author Yashwant Singh
	 *
	 */
	class GetUpdatedDateAssessment extends AsyncTask<Void, Void, String>{

		private String userId;
		private String playerId;
		private ProgressDialog pd;
		private int lang = 1;

		GetUpdatedDateAssessment(String userId , String playerId){
			this.userId = userId;
			this.playerId = playerId;
			if(CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH)
				lang = CommonUtils.ENGLISH;
			else 
				lang = CommonUtils.SPANISH;
		}

		@Override
		protected void onPreExecute() {
			pd = CommonUtils.getProgressDialog(context);
			pd.show();
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(Void... params) {
			AssessmentTestServerOperation serverObj = new AssessmentTestServerOperation();
			String date = serverObj.getUpdatedDateAssessment(lang);
			return date;
		}

		@Override
		protected void onPostExecute(String date) {
			pd.cancel();

			if(date != null){
				SharedPreferences sharedPrefForData = context.getSharedPreferences
						(ICommonUtils.DATE_ASSESSMENT_STANDARD_DOWNLOAD, 0);

				SharedPreferences sharedPrefPlayerInfo = context.getSharedPreferences(PLAYER_INFO, 0);
				int grade = sharedPrefPlayerInfo.getInt("grade",1);
				AssessmentTestImpl implObj = new AssessmentTestImpl(context);
				implObj.openConnection();
				boolean isStandardLoaded = implObj.isStandard(grade);

				if(isStandardLoaded){
					if(!sharedPrefForData.getString("assessmentStandardDate", "").equals(date)){
						downLoadStandars(sharedPrefForData, date , lang , userId , playerId);
					}else{
						goOnNextScreen(userId , playerId);
					}
				}else{
					downLoadStandars(sharedPrefForData, date , lang , userId , playerId);
				}
				implObj.closeConnection();
			}else{
				CommonUtils.showInternetDialog(context);
			}
			//goOnNextScreen(userId , playerId);
			super.onPostExecute(date);
		}
	}

	/**
	 * This method call when ready to download standards
	 * @param sharedPrefForData
	 * @param date
	 * @param lang
	 */
	private void downLoadStandars(SharedPreferences sharedPrefForData , String date , int lang
			,String userId , String playerId){
		SharedPreferences.Editor editor = sharedPrefForData.edit();
		editor.putString("assessmentStandardDate", date);
		editor.commit();

		new GetAssessmentStandards(userId, playerId, lang).execute(null,null,null);
	}

	/**
	 * Get Assessment Standards
	 * @author Yashwant Singh
	 *
	 */
	class GetAssessmentStandards extends AsyncTask<Void, Void, AssessmentStandardDto>{

		private ProgressDialog pd;
		private int lang;
		private String userId;
		private String playerId;

		GetAssessmentStandards(String userId , String playerId , int lang){
			this.lang = lang;
			this.userId = userId;
			this.playerId = playerId;
		}

		@Override
		protected void onPreExecute() {
			pd = CommonUtils.getProgressDialog(context);
			pd.show();
			super.onPreExecute();
		}

		@Override
		protected AssessmentStandardDto doInBackground(Void... params) {
			AssessmentTestServerOperation serverObj = new AssessmentTestServerOperation();
			AssessmentStandardDto assessmentStandard = serverObj.getAssessmentStandards(lang);

			if(assessmentStandard != null){
				try{
					AssessmentTestImpl implObj = new AssessmentTestImpl(context);
					implObj.openConnection();
					implObj.deleteFromWordAssessmentStandards();
					implObj.deleteFromWordAssessmentSubStandards();
					implObj.deleteFromWordAssessmentSubCategoriesInfo();
					implObj.insertIntoWordAssessmentStandards(assessmentStandard.getStandardDataList());
					implObj.insertIntoWordAssessmentSubStandards(assessmentStandard.getStandardDataList());
					implObj.insertIntoWordAssessmentSubCategoriesInfo(assessmentStandard.getCategoriesList());
					implObj.closeConnection();
				}catch(Exception e){
					Log.e("AssessmentClass", "No Data on Server For Standards " + e.toString());
				}
			}
			return assessmentStandard;
		}

		@Override
		protected void onPostExecute(AssessmentStandardDto assessmentStandard) {
			pd.cancel();
			if(assessmentStandard != null)
				goOnNextScreen(userId , playerId);
			else
				CommonUtils.showInternetDialog(context);
			super.onPostExecute(assessmentStandard);
		}
	}

}