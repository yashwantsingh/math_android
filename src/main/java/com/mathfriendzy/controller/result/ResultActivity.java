package com.mathfriendzy.controller.result;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore.Images;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.controller.learningcenter.NewLearnignCenter;
import com.mathfriendzy.controller.result.assessmenttest.AssessmentTestResultListActivity;
import com.mathfriendzy.controller.top100.Top100Activity;
import com.mathfriendzy.facebookconnect.ShareActivity;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.result.JsonAsynTaskForDate;
import com.mathfriendzy.model.result.WordProblemMathScoreObj;
import com.mathfriendzy.model.result.WordProblemMathScoreResultTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.CategoryListTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.LearnignCenterSchoolCurriculumServerOperation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.QuestionLoadedTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SchoolCurriculumLearnignCenterimpl;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SubCatergoryTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.WordProblemLevelTransferObj;
import com.mathfriendzy.model.spanishchanges.SpanishChangesImpl;
import com.mathfriendzy.model.top100.JsonFileParser;
import com.mathfriendzy.model.top100.Score;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DateTimeOperation;
import com.mathfriendzy.utils.DialogGenerator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;

import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ITextIds.LBL_ADDITION;
import static com.mathfriendzy.utils.ITextIds.LBL_DECIMAL;
import static com.mathfriendzy.utils.ITextIds.LBL_DIVISION;
import static com.mathfriendzy.utils.ITextIds.LBL_FRACTION;
import static com.mathfriendzy.utils.ITextIds.LBL_LIFETIME;
import static com.mathfriendzy.utils.ITextIds.LBL_MULTI;
import static com.mathfriendzy.utils.ITextIds.LBL_NEGATIVE;
import static com.mathfriendzy.utils.ITextIds.LBL_POINTS;
import static com.mathfriendzy.utils.ITextIds.LBL_SCORE;
import static com.mathfriendzy.utils.ITextIds.LBL_SELECTDATE;
import static com.mathfriendzy.utils.ITextIds.LBL_SUBTRACT;
import static com.mathfriendzy.utils.ITextIds.LBL_TIME;
import static com.mathfriendzy.utils.ITextIds.MF_HOMESCREEN;
import static com.mathfriendzy.utils.ITextIds.MF_RESULT;


public class ResultActivity extends ActBaseClass implements OnClickListener
{
    private TextView txtTitle			= null;
    private TextView txtPlayerName		= null;
    private TextView lifetTime			= null;
    private TextView time				= null;
    private TextView txtTime			= null;
    private TextView points				= null;
    private TextView txtPoints			= null;
    private TextView txtMathFriendzy	= null;
    private TextView txtTimeList		= null;
    private TextView txtScore			= null;
    private TextView txtDate			= null;

    private TextView txtMathTotal		= null;
    private TextView txtTotalTime		= null;
    private TextView txtAvgScore		= null;

    private TextView[] txtOperation		= null;
    private TextView[] txtTimes			= null;
    private TextView[] txtScores		= null;

    private Button	 btnDate			= null;
    private RelativeLayout layoutList	= null;

    private String playerName			= null;
    private int userId					= 0;
    private int playerId				= 0;
    private String playDate				= null;
    private JsonFileParser file			= null;
    private ArrayList<Score> scoreList	= null;
    private String jsonFile;

    private boolean SELECT_FLAG			= false;

    //added by me
    private Button btnAssessmentButton = null;
    private ImageView imgSolveEquation = null;
    private TextView  lblSolveEquation = null;
    private ImageView imgSchoolCurriculum = null;
    private TextView  lblSchoolCurriculum = null;

    private ArrayList<WordProblemLevelTransferObj> wordProblemLevelList = null;
    private ArrayList<RelativeLayout> chaildlayoutList = null;
    private ArrayList<RelativeLayout> subchaildlayoutList = null;
    private ArrayList<ImageView>      expendedImageList= null;
    private ImageView expendedImage = null; // this will hold the expend current image object

    //parent view
    private ImageView imgExpandImage;
    private ImageView imgMedalImage;
    private ImageView imgCategorySign;
    private TextView txtCategoryName;
    private TextView txtProgressDetail;

    private RelativeLayout childLayout    = null;
    private RelativeLayout subchildLayout = null;//
    private RelativeLayout clickedLayout  = null; // this will hold the clicked layout
    //for school curriculum layout
    private LinearLayout linearLayout = null;// layout where child and sub child added
    private ScrollView scrollSchool   = null;

    private int clickedIndex = -1;
    private boolean isTab = false;

    private boolean isDataLoadForSchoolCurriculum = false;
    private int playerGrade = 0;
    private int playerpoints = 0;
    //for word problem
    WordProblemMathScoreResultTransferObj wordProblemScoreObj = null;

    //for assessment
    private String imageName;

    //added for new assessment report
    public static boolean isCallFromPractice = false;
    private boolean isCallfromParcticeLocale = false;

    //added for categories name text overlapping changs it as ...
    private boolean isTablet = false;

    //for adding for share bar Jan 16 sheet , 7 no.
    private Button btnCloseShareTool		= null;
    private RelativeLayout layoutShare		= null;
    private Button btnScreenShot			= null;
    private Button btnEmail					= null;
    private Button btnTwitter				= null;
    private Button btnFbShare				= null;
    private Button btnShare					= null;
    private String subject					= null;
    private String body						= null;
    private String screenText				= null;
    private Bitmap b;


    //for show result by grade , school curriculum , on 21 May 2014
    private RelativeLayout gradeLayout = null;
    private TextView       txtGrade    = null;
    private Spinner        spinnerGrade = null;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        isTablet = getResources().getBoolean(R.bool.isTablet);

        //added for new assessment report
        if(isCallFromPractice){
            isCallFromPractice = false;
            isCallfromParcticeLocale = true;
        }

        wordProblemLevelList = new ArrayList<WordProblemLevelTransferObj>();

        playerName  = getIntent().getStringExtra("playerName");
        userId		= getIntent().getIntExtra("userId", 0);
        playerId	= getIntent().getIntExtra("playerId", 0);
        playDate 	= getIntent().getStringExtra("date");
        SELECT_FLAG	= getIntent().getBooleanExtra("flag", false);
        playerGrade = getIntent().getIntExtra("grade", 0);
        playerpoints= getIntent().getIntExtra("points", 0);

        //for assessment
        imageName  = getIntent().getStringExtra("imageName");
        //end changes

        String wordProblemDataString = this.getIntent().getStringExtra("wordProblemJson");

        file = new JsonFileParser(this);
        jsonFile = getIntent().getStringExtra("jsonFile");
        scoreList		= new ArrayList<Score>();
        scoreList 		= file.getMathScore(jsonFile);

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        RelativeLayout b = (RelativeLayout)inflater.inflate(R.layout.math_result_list, null);
        getWidgetId();
        layoutList.addView(b);
        //for word problem
        wordProblemScoreObj = new WordProblemMathScoreResultTransferObj();
        wordProblemScoreObj = this.getwordProblemResultData(wordProblemDataString);

        setWigdetIdToArray();
        setWidgetText();

        //set visibility of assessment button
        if(isCallfromParcticeLocale)
            btnAssessmentButton.setVisibility(Button.INVISIBLE);

        //for show result by grade , school curriculum , on 21 May 2014
        this.setVisibilityOfGradeLayout(true);

    }//END onClick method


    private void setWigdetIdToArray()
    {
        txtOperation = new TextView[10];
        txtTimes	 = new TextView[10];
        txtScores	 = new TextView[10];

        txtOperation[0] = (TextView) findViewById(R.id.txtAddition);
        txtOperation[1] = (TextView) findViewById(R.id.txtSubtraction);
        txtOperation[2] = (TextView) findViewById(R.id.txtMultiplication);
        txtOperation[3] = (TextView) findViewById(R.id.txtDivision);
        txtOperation[4] = (TextView) findViewById(R.id.txtAddFraction);
        txtOperation[5] = (TextView) findViewById(R.id.txtDivFraction);
        txtOperation[6] = (TextView) findViewById(R.id.txtAddDecimal);
        txtOperation[7] = (TextView) findViewById(R.id.txtDivDecimal);
        txtOperation[8] = (TextView) findViewById(R.id.txtAddNaegative);
        txtOperation[9] = (TextView) findViewById(R.id.txtDivNegatives);

        txtTimes[0]		= (TextView) findViewById(R.id.txtTime1);
        txtTimes[1]		= (TextView) findViewById(R.id.txtTime2);
        txtTimes[2]		= (TextView) findViewById(R.id.txtTime3);
        txtTimes[3]		= (TextView) findViewById(R.id.txtTime4);
        txtTimes[4]		= (TextView) findViewById(R.id.txtTime5);
        txtTimes[5]		= (TextView) findViewById(R.id.txtTime6);
        txtTimes[6]		= (TextView) findViewById(R.id.txtTime7);
        txtTimes[7]		= (TextView) findViewById(R.id.txtTime8);
        txtTimes[8]		= (TextView) findViewById(R.id.txtTime9);
        txtTimes[9]		= (TextView) findViewById(R.id.txtTime10);

        txtScores[0]	= (TextView) findViewById(R.id.txtScore1);
        txtScores[1]	= (TextView) findViewById(R.id.txtScore2);
        txtScores[2]	= (TextView) findViewById(R.id.txtScore3);
        txtScores[3]	= (TextView) findViewById(R.id.txtScore4);
        txtScores[4]	= (TextView) findViewById(R.id.txtScore5);
        txtScores[5]	= (TextView) findViewById(R.id.txtScore6);
        txtScores[6]	= (TextView) findViewById(R.id.txtScore7);
        txtScores[7]	= (TextView) findViewById(R.id.txtScore8);
        txtScores[8]	= (TextView) findViewById(R.id.txtScore9);
        txtScores[9]	= (TextView) findViewById(R.id.txtScore10);

        txtMathTotal	= (TextView) findViewById(R.id.txtMathTotal);
        txtTotalTime	= (TextView) findViewById(R.id.txtTotalTime);
        txtAvgScore		= (TextView) findViewById(R.id.txtAvgScore);
    }

    private void getWidgetId()
    {
        txtTime			= (TextView) findViewById(R.id.txtTime);
        txtDate			= (TextView) findViewById(R.id.txtDate);
        txtMathFriendzy = (TextView) findViewById(R.id.txtMathFriendzy);
        txtPlayerName 	= (TextView) findViewById(R.id.txtPlayerName);
        txtPoints		= (TextView) findViewById(R.id.txtPoints);
        txtScore 		= (TextView) findViewById(R.id.txtScore);
        txtTimeList 	= (TextView) findViewById(R.id.txtTimeList);
        txtTitle 		= (TextView) findViewById(R.id.txtTitleScreen);
        time			= (TextView) findViewById(R.id.time);
        points			= (TextView) findViewById(R.id.points);
        btnDate			= (Button) findViewById(R.id.btnDate);
        lifetTime		= (TextView) findViewById(R.id.lifeTime);
        layoutList		= (RelativeLayout) findViewById(R.id.lay);

        btnAssessmentButton = (Button) findViewById(R.id.btnAssessmentButton);
        imgSolveEquation    = (ImageView) findViewById(R.id.imgSolveEquation);
        lblSolveEquation    = (TextView)  findViewById(R.id.lblSolveEquation);
        imgSchoolCurriculum = (ImageView) findViewById(R.id.imgSchoolCurriculum);
        lblSchoolCurriculum = (TextView)  findViewById(R.id.lblSchoolCurriculum);

        linearLayout        = (LinearLayout) findViewById(R.id.learnignCenterLayout);
        scrollSchool        = (ScrollView)   findViewById(R.id.scrollSchool);

        //for share bar Jan 16 sheet , 7 no.
        layoutShare			= (RelativeLayout) findViewById(R.id.layoutShare);
        btnEmail			= (Button) findViewById(R.id.btnMail);
        btnFbShare			= (Button) findViewById(R.id.btnFbSahre);
        btnScreenShot		= (Button) findViewById(R.id.btnScreenShot);
        btnTwitter			= (Button) findViewById(R.id.btnTwitterShare);
        btnShare			= (Button) findViewById(R.id.btnShare);
        btnCloseShareTool	= (Button) findViewById(R.id.btnCloseShareToolbar);
        //end changes

        btnDate.setOnClickListener(this);
        btnAssessmentButton.setOnClickListener(this);
        imgSolveEquation.setOnClickListener(this);
        imgSchoolCurriculum.setOnClickListener(this);

        //for share bar Jan 16 sheet , 7 no.
        btnShare.setOnClickListener(this);
        btnCloseShareTool.setOnClickListener(this);
        btnEmail.setOnClickListener(this);
        btnFbShare.setOnClickListener(this);
        btnScreenShot.setOnClickListener(this);
        btnTwitter.setOnClickListener(this);
        //end changes

        //for show result by grade , school curriculum , on 21 May 2014
        gradeLayout = (RelativeLayout) findViewById(R.id.gradeLayout);
        txtGrade    = (TextView) findViewById(R.id.txtGrade);
        spinnerGrade = (Spinner) findViewById(R.id.spinnerGrade);
    }


    /**
     * This method set the grade layout visibility , show for the school curriculum and hide for the practice skill
     * @param isForPracticeSkill
     */
    private void setVisibilityOfGradeLayout(boolean isForPracticeSkill){
        if(isForPracticeSkill){
            gradeLayout.setVisibility(RelativeLayout.GONE);
        }else{
            gradeLayout.setVisibility(RelativeLayout.VISIBLE);
        }
    }

    private void setWidgetText()
    {
        DateTimeOperation date = new DateTimeOperation();
        String text;
        int totalTime = 0, totalScore = 0;

        Translation translate = new Translation(this);
        translate.openConnection();

        //for show result by grade , school curriculum , on 21 May 2014
        text = translate.getTranselationTextByTextIdentifier("lblAddPlayerGrade");
        txtGrade.setText(text + ":");
        //end 21

        text = translate.getTranselationTextByTextIdentifier(MF_HOMESCREEN);
        txtMathFriendzy.setText(text+":");
        txtMathTotal.setText(text+" " +translate.getTranselationTextByTextIdentifier("lblTotal"));

        text = translate.getTranselationTextByTextIdentifier(MF_RESULT);
        txtTitle.setText(text);

        text = translate.getTranselationTextByTextIdentifier(LBL_LIFETIME);
        lifetTime.setText(text+":");

        text = translate.getTranselationTextByTextIdentifier(LBL_POINTS);
        points.setText(text+":");

        text = translate.getTranselationTextByTextIdentifier(LBL_SCORE);
        txtScore.setText(text+":");

        text = translate.getTranselationTextByTextIdentifier(LBL_TIME);
        time.setText(text+":");
        txtTimeList.setText(text+":");

        text = translate.getTranselationTextByTextIdentifier(LBL_SELECTDATE);
        btnDate.setText(text);

        text = translate.getTranselationTextByTextIdentifier("lblAssessmentResults");
        btnAssessmentButton.setText(text);

        text = translate.getTranselationTextByTextIdentifier("lblSchoolsCurriculum");
        lblSchoolCurriculum.setText(text);

        text = translate.getTranselationTextByTextIdentifier("lblSolveEquations");
        lblSolveEquation.setText(text);

        //for adding for share bar Jan 16 sheet , 7 no.
        text = translate.getTranselationTextByTextIdentifier("btnTitleShare");
        btnShare.setText(text);
        screenText 	= translate.getTranselationTextByTextIdentifier("alertMsgAScreenShotHasBeenSavedToTheCameraRoll");
        subject		= translate.getTranselationTextByTextIdentifier("infoEmailSubject");
        body		= translate.getTranselationTextByTextIdentifier("shareFBEmailMessage");
        // end changes

        text = translate.getTranselationTextByTextIdentifier(LBL_ADDITION);
        txtOperation[0].setText(text);
        text = translate.getTranselationTextByTextIdentifier(LBL_SUBTRACT);
        txtOperation[1].setText(text);
        text = translate.getTranselationTextByTextIdentifier(LBL_MULTI);
        txtOperation[2].setText(text);
        text = translate.getTranselationTextByTextIdentifier(LBL_DIVISION);
        txtOperation[3].setText(text);

        text = translate.getTranselationTextByTextIdentifier(LBL_FRACTION);
        txtOperation[4].setText(text);
        txtOperation[5].setText(text);

        text = translate.getTranselationTextByTextIdentifier(LBL_DECIMAL);
        txtOperation[6].setText(text);
        txtOperation[7].setText(text);

        text = translate.getTranselationTextByTextIdentifier(LBL_NEGATIVE);
        txtOperation[8].setText(text);
        txtOperation[9].setText(text);

        translate.closeConnection();

        txtPlayerName.setText(playerName);

        int time = 0;

        try{
            time = file.getTotalTime(jsonFile) + Integer.parseInt(wordProblemScoreObj.getTotalTime());
        }
        catch(Exception e){
            time = file.getTotalTime(jsonFile) + 0;
        }

        //txtTime.setText(date.setTimeFormat(file.getTotalTime(jsonFile)));
        //changes for word problem score
        txtTime.setText(date.setTimeFormat(time));

        PlayerTotalPointsObj playerObj = null;
        LearningCenterimpl learningCenterimpl = new LearningCenterimpl(this);
        learningCenterimpl.openConn();
        playerObj = learningCenterimpl.getDataFromPlayerTotalPoints(playerId + "");
        learningCenterimpl.closeConn();

        //changes
		/*if(playerObj.getTotalPoints() > 0)
			txtPoints.setText(date.setNumberString(playerObj.getTotalPoints() + ""));
		else{*/
        try{
            txtPoints.setText(date.setNumberString((Integer.parseInt
                    (file.getTotalPoints(jsonFile))
                    + Integer.parseInt(wordProblemScoreObj.getAllPoints())) + ""));
        }catch(Exception e){
            txtPoints.setText("0");
        }
        //}

        try
        {
            for(int i = 0; i < scoreList.size(); i++)
            {
                Score obj = new Score();
                obj = scoreList.get(i);
                int id = Integer.parseInt(obj.getId()) - 1;

                totalScore = totalScore + (Integer.parseInt(obj.getScore()));
                totalTime  = totalTime + Integer.parseInt(obj.getTime());

                if(id >= 0){
                    txtTimes[id].setText(date.setTimeFormat(Integer.parseInt(obj.getTime())));
                    txtScores[id].setText(obj.getScore()+"%");
                }
            }
            txtTotalTime.setText(""+date.setTimeFormat(totalTime));
        }catch(Exception e){
            Log.e("ResultActivity", "Error : " + e.toString());
        }

        if(scoreList.size() > 0)
        {
            txtAvgScore.setText(""+totalScore/scoreList.size()+"%");
        }

        txtDate.setText(date.setDate(playDate));
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }

    /**
     * This method for schoolo curriculum category
     */
    private void schoolCurriculum(int grade , String stringGrade) {
        this.getWordProblemLevelData(userId + "", playerId + "");
        ArrayList<CategoryListTransferObj> categoryList = this.getCategories(grade);

        if(categoryList.size() > 0 )
            this.setLayoutValueForSchoolCurriculum(categoryList , stringGrade);
        else
        if(CommonUtils.isInternetConnectionAvailable(this))
            this.downloadCategory(grade , stringGrade);
        else
            CommonUtils.showInternetDialog(this);
    }

    /**
     * This method download the categories
     * @param grade
     */
    private void downloadCategory(int grade , String stringGrade){
        new DownlLoadCategoriesForSchoolCurriculum(grade, CommonUtils.getUserLanguageCode(this) ,stringGrade)
                .execute(null,null,null);
    }

    /**
     * This method get categories detail from database according to the user grade
     * @param grade
     */
    private ArrayList<CategoryListTransferObj> getCategories(int grade){
		/*ArrayList<CategoryListTransferObj> categoryList = new ArrayList<CategoryListTransferObj>();
		SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
		schoolImpl.openConnection();
		categoryList = schoolImpl.getCategoriesForSchoolCurriculum(grade);
		schoolImpl.closeConnection();*/
        ArrayList<CategoryListTransferObj> categoryList = new ArrayList<CategoryListTransferObj>();
        //changes for Spanish
        if(CommonUtils.getUserLanguageCode(this) == CommonUtils.ENGLISH){
            SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
            schoolImpl.openConnection();
            categoryList = schoolImpl.getCategoriesForSchoolCurriculum(grade);
            schoolImpl.closeConnection();
        }else {
            SpanishChangesImpl schoolImpl = new SpanishChangesImpl(this);
            schoolImpl.openConn();
            categoryList = schoolImpl.getCategoriesForSchoolCurriculum(grade);
            schoolImpl.closeConn();
        }
        return categoryList;
    }

    /**
     * This method get the word problem data from database for school curriculum
     * @param suerId
     * @param playerId
     */
    private void getWordProblemLevelData(String userId , String playerId){
        SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
        schoolImpl.openConnection();
        wordProblemLevelList = schoolImpl.getWordProblemLevelData(userId, playerId);
        schoolImpl.closeConnection();
    }
    /**
     * This method set the layout value for school curriculum
     * @param categoryList
     */
    @SuppressWarnings("static-access")
    private void setLayoutValueForSchoolCurriculum(final ArrayList<CategoryListTransferObj> categoryList ,
                                                   final String grade)
    {
        linearLayout.removeAllViews();// for restarting when click on back press in learning center equation solve with timer
        chaildlayoutList    = new ArrayList<RelativeLayout>();
        expendedImageList   = new ArrayList<ImageView>();
        clickedLayout = null;
        clickedIndex = -1;
        expendedImage = null;

        for (int i = 0 ; i < categoryList.size() ; i++) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
            childLayout = (RelativeLayout) inflater.inflate(R.layout.school_curriculum_result_layout, null);

            imgExpandImage 		= (ImageView) childLayout.findViewById(R.id.imgExpand);
            imgCategorySign 	= (ImageView) childLayout.findViewById(R.id.imgSign);
            txtCategoryName 	= (TextView)  childLayout.findViewById(R.id.txtCategoryName);
            txtProgressDetail 	= (TextView)  childLayout.findViewById(R.id.txtProgress);
            imgMedalImage       = (ImageView) childLayout.findViewById(R.id.imgMedal);
            //changes for ticket #56
            TextView txtTime    = (TextView) childLayout.findViewById(R.id.txtTimeList);

            //for category name override
            if(!isTablet){
                txtCategoryName.setWidth(getResources().getInteger(R.integer.widthForResultlayout));
            }

            imgCategorySign.setVisibility(ImageView.GONE);
            //changes for for ticket #56 from gone to visible
            txtProgressDetail.setVisibility(TextView.VISIBLE);
            //changes for for ticket #56
            txtTime.setVisibility(TextView.VISIBLE);

            txtCategoryName.setText(categoryList.get(i).getName());

            //changes for for ticket #56
            this.setTimeAndScoreForSchoolCurriculum(categoryList.get(i).getCid() , txtProgressDetail , txtTime
                    , grade);
            //end changes

            linearLayout.addView(childLayout);
            expendedImageList.add(imgExpandImage);
            chaildlayoutList.add(childLayout);

            childLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
                    if(!sheredPreference.getBoolean(IS_LOGIN, false))
                    {
                        Translation transeletion = new Translation(ResultActivity.this);
                        transeletion.openConnection();
                        DialogGenerator dg = new DialogGenerator(ResultActivity.this);
                        dg.generateRegisterOrLogInDialog
                                (transeletion.getTranselationTextByTextIdentifier
                                        ("alertMsgYouMustRegisterLoginToAccessThisFeature"));
                        transeletion.closeConnection();
                    }
                    else
                    {
                        LinearLayout linearChildLayout = new LinearLayout(ResultActivity.this);
                        linearChildLayout.setOrientation(LinearLayout.VERTICAL);

                        for (int i = 0; i < chaildlayoutList.size(); i++) {
                            if (v == chaildlayoutList.get(i)) {

                                if (clickedLayout != chaildlayoutList.get(i)) {

                                    if(clickedIndex != -1)
                                        linearLayout.removeViewAt(clickedIndex + 1);

                                    if(expendedImage != null){
                                        if(isTab)
                                            expendedImage.setBackgroundResource(R.drawable.tablet_expand_button);// set expended button image when click on other
                                        else
                                            expendedImage.setBackgroundResource(R.drawable.expand_button);// set expended button image when click on other
                                    }

                                    linearChildLayout = addChildLayoutForSchooCurriculum(categoryList.get(i).getCid()
                                            ,userId + "" , playerId + "" , grade);

                                    linearLayout.addView(linearChildLayout, i + 1);
                                    if(isTab)
                                        expendedImageList.get(i).setBackgroundResource(R.drawable.tablet_tab_expand_button);//set tab expend background image
                                    else
                                        expendedImageList.get(i).setBackgroundResource(R.drawable.tab_expand_button);//set tab expend background image

                                    clickedLayout = chaildlayoutList.get(i);
                                    clickedIndex = i;
                                    expendedImage = expendedImageList.get(i);
                                } else {
                                    linearLayout.removeViewAt(clickedIndex + 1);
                                    clickedLayout = null;
                                    clickedIndex = -1;
                                    if(isTab)
                                        expendedImage.setBackgroundResource(R.drawable.tablet_expand_button);// set expended button image when click on other
                                    else
                                        expendedImage.setBackgroundResource(R.drawable.expand_button);// set expended button image when click on other
                                    expendedImage = null;
                                }
                            }
                        }
                    }
                }
            });
        }
    }

    /**
     * This method set the avg total time and avg total score
     * @param txtTime
     * @param txtScore
     * @param catId
     */
    private void setTimeAndScoreForSchoolCurriculum(int catId, TextView txtScore, TextView txtTime
            , String grade){

        int totalTime 	= 0;
        int totalScore 	= 0;

        DateTimeOperation date = new DateTimeOperation();

		/*for (int i = 0; i < wordProblemScoreObj.getMathScoreList().size(); i++) {
			if(wordProblemScoreObj.getMathScoreList().get(i).getCatId() == catId){
				totalTime = totalTime + Integer.parseInt(wordProblemScoreObj.getMathScoreList().get(i).getTime());
				totalScore = totalScore + Integer.parseInt(wordProblemScoreObj.getMathScoreList().get(i).getScore());
			}
		}

		//changes for score
		int counter = 0;
		for(int i = 0 ; i < wordProblemScoreObj.getMathScoreList().size() ; i ++ ){
			WordProblemMathScoreObj wordProblemObj = wordProblemScoreObj.getMathScoreList().get(i);
			if(wordProblemObj.getCatId() == catId){
				counter ++ ;
			}
		}
		//end changes
		 */

        try{
            //for show result by grade , school curriculum , on 21 May 2014
            ArrayList<WordProblemMathScoreObj> wordScoreList = this.getMathScoreListByGrade(grade);
            for (int i = 0; i < wordScoreList.size(); i++) {
                if(wordScoreList.get(i).getCatId() == catId){
                    totalTime = totalTime + Integer.parseInt(wordScoreList.get(i).getTime());
                    totalScore = totalScore + Integer.parseInt(wordScoreList.get(i).getScore());
                }
            }

            //changes for score
            int counter = 0;
            for(int i = 0 ; i < wordScoreList.size() ; i ++ ){
                WordProblemMathScoreObj wordProblemObj = wordScoreList.get(i);
                if(wordProblemObj.getCatId() == catId){
                    counter ++ ;
                }
            }

            if(counter > 0)
                totalScore 	= totalScore / counter;
        }catch(Exception e){
            Log.e("ResultActivity", "inside setTimeAndScoreForSchoolCurriculum " +
                    "Error while setting result data for school curriculum " + e.toString());

        }
        //end changes , 21 May

		/*if(wordProblemScoreObj.getMathScoreList().size() > 0){*/

        //}

        txtTime.setText(date.setTimeFormat((totalTime)));
        txtScore.setText(totalScore+"%");
    }

    //for show result by grade , school curriculum , on 21 May 2014
    /**
     * Return the mathScore list for school curriculum
     * @param gradeKey
     * @return
     */
    private ArrayList<WordProblemMathScoreObj> getMathScoreListByGrade(String gradeKey){
        ArrayList<WordProblemMathScoreObj> wordList = new ArrayList<WordProblemMathScoreObj>();
        if(gradeKey == null){
            return wordList;//no record exist
        }else{
            wordList =  wordProblemScoreObj.getFinalMathListWithGradeKey().get(Integer.parseInt(gradeKey));
        }
        return wordList;
    }

    /**
     * This method set the adpater data
     */
    private void setGradeAdapterForSchooleCurriculum(){
        final ArrayList<Integer> gradeList = getGradeList();
        ArrayAdapter<Integer> gradeAdapter = new ArrayAdapter<Integer>(this, R.layout.spinner_textview_layout,gradeList);
        gradeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGrade.setAdapter(gradeAdapter);
        //spinnerGrade.setSelection(gradeAdapter.getPosition(gradeAdapter.getItem(0)));
        try{
            spinnerGrade.setSelection(gradeAdapter.getPosition(playerGrade));
        }catch(Exception e){//rare case , will not execute in any situation , but add for exceptional case
            spinnerGrade.setSelection(gradeAdapter.getPosition(gradeAdapter.getItem(0)));
        }
        spinnerGrade.setOnItemSelectedListener(new OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {
                schoolCurriculum(gradeList.get(pos), gradeList.get(pos) + "");
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });
    }

    /**
     * This method return the grade list
     * @return
     */
    private ArrayList<Integer> getGradeList(){
        ArrayList<Integer> gradeList = new ArrayList<Integer>();
        for(int key : wordProblemScoreObj.getFinalMathListWithGradeKey().keySet()){
            gradeList.add(key);
        }
        Collections.sort(gradeList);
        return gradeList;
    }

    /**
     * This method add the child layout for school curriculum
     * This method add child at the clicked + 1 position. on the basis of find the operation id and do work
     * @param playerId
     * @param userId
     * @return
     */
    private LinearLayout addChildLayoutForSchooCurriculum(final int categoryId, String userId, String playerId
            , String grade)
    {
        final ArrayList<SubCatergoryTransferObj> subCategoryList = this.getSubCategoryList(categoryId);
        DateTimeOperation date = new DateTimeOperation();

        subchaildlayoutList = new ArrayList<RelativeLayout>();

        LinearLayout linearChildLayout = new LinearLayout(this);
        linearChildLayout.setOrientation(LinearLayout.VERTICAL);

        for (int j = 0; j < subCategoryList.size(); j++) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(NewLearnignCenter.LAYOUT_INFLATER_SERVICE);
            subchildLayout = (RelativeLayout) inflater.inflate(R.layout.school_curriclumum_result_child_layout,null);

            TextView txtSubCategoryName = (TextView) subchildLayout.findViewById(R.id.txtSubCategoryName);
            TextView txtTime            = (TextView) subchildLayout.findViewById(R.id.txtTime);
            TextView txtScore           = (TextView) subchildLayout.findViewById(R.id.txtScore);

            txtSubCategoryName.setText(subCategoryList.get(j).getName());

            //for show result by grade , school curriculum , on 21 May 2014
            ArrayList<WordProblemMathScoreObj> wordScoreList = this.getMathScoreListByGrade(grade);
            for(int i = 0 ; i < wordScoreList.size() ; i ++ ){
                WordProblemMathScoreObj wordProblemObj = wordScoreList.get(i);
                if(wordProblemObj.getCatId() == categoryId &&
                        wordProblemObj.getSubCatId() == subCategoryList.get(j).getId()){

                    txtTime.setText(date.setTimeFormat(Integer.parseInt(wordProblemObj.getTime())));
                    txtScore.setText(wordProblemObj.getScore()+"%");
                }
            }
            subchaildlayoutList.add(subchildLayout);
            linearChildLayout.addView(subchildLayout);
        }
        return linearChildLayout;
    }

    /**
     * This method get number of sub category according oto category id
     * @param categoryId
     * @return
     */
    private ArrayList<SubCatergoryTransferObj> getSubCategoryList(int categoryId){
		/*SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
		schoolImpl.openConnection();
		ArrayList<SubCatergoryTransferObj> subCategoryList = schoolImpl.getSubChildCategories(categoryId);
		schoolImpl.closeConnection();*/
        //changes for Spanish
        ArrayList<SubCatergoryTransferObj> subCategoryList = new ArrayList<SubCatergoryTransferObj>();
        if(CommonUtils.getUserLanguageCode(this) == CommonUtils.ENGLISH){
            SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
            schoolImpl.openConnection();
            subCategoryList = schoolImpl.getSubChildCategories(categoryId);
            schoolImpl.closeConnection();
        }else{
            SpanishChangesImpl schoolImpl = new SpanishChangesImpl(this);
            schoolImpl.openConn();
            subCategoryList = schoolImpl.getSubChildCategories(categoryId);
            schoolImpl.closeConn();
        }
        return subCategoryList;
    }

    /**
     * This method take screen shot
     * @return
     */
    private Bitmap getScreenShot(){
        View view = getWindow().getDecorView().getRootView();
        view.setDrawingCacheEnabled(true);
        layoutShare.setVisibility(View.GONE);
        btnCloseShareTool.setVisibility(View.GONE);
        Bitmap b = view.getDrawingCache();
        return b;
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId()){
            case R.id.btnDate :
                if(!SELECT_FLAG)
                {
                    if(isCallfromParcticeLocale)
                        isCallFromPractice = true;

                    new JsonAsynTaskForDate(this, userId, playerId,playerName,0 , playerpoints
                            ,playerGrade , imageName).execute(null,null,null);
                }
                else
                {
                    onBackPressed();
                }
                break;
            case R.id.btnAssessmentButton :
                this.clickOnAssessmentTest();
                break;
            case R.id.imgSchoolCurriculum:
                imgSchoolCurriculum.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
                imgSolveEquation.setBackgroundResource(R.drawable.mf_check_box_ipad);

                scrollSchool.setVisibility(ScrollView.VISIBLE);
                layoutList.setVisibility(RelativeLayout.GONE);
                if(!isDataLoadForSchoolCurriculum){
                    //this.schoolCurriculum(playerGrade , null);
                    //for show result by grade , school curriculum , on 21 May 2014
                    ArrayList<Integer> gradeKeyList = getGradeList();
                    if(gradeKeyList.size() == 0){
                        schoolCurriculum(playerGrade, null);
                        this.setVisibilityOfGradeLayout(true);
                    }else if(gradeKeyList.size() == 1){
                        this.setVisibilityOfGradeLayout(true);
                        schoolCurriculum(gradeKeyList.get(0), gradeKeyList.get(0) + "");
                    }else{
                        this.setVisibilityOfGradeLayout(false);
                        this.setGradeAdapterForSchooleCurriculum();
                    }
                    isDataLoadForSchoolCurriculum = true;
                    //for show result by grade , school curriculum , on 21 May 2014
                }else{
                    ArrayList<Integer> gradeKeyList = getGradeList();
                    if(gradeKeyList.size() == 0){
                        this.setVisibilityOfGradeLayout(true);
                    }else if(gradeKeyList.size() == 1){
                        this.setVisibilityOfGradeLayout(true);
                    }else{
                        this.setVisibilityOfGradeLayout(false);
                    }
                }
                break;
            case R.id.imgSolveEquation:
                imgSolveEquation.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
                imgSchoolCurriculum.setBackgroundResource(R.drawable.mf_check_box_ipad);
                scrollSchool.setVisibility(ScrollView.GONE);
                layoutList.setVisibility(RelativeLayout.VISIBLE);

                //for show result by grade , school curriculum , on 21 May 2014
                this.setVisibilityOfGradeLayout(true);
                break;
            case R.id.btnShare :
                layoutShare.setVisibility(View.VISIBLE);
                btnCloseShareTool.setVisibility(View.VISIBLE);
                break;
            case R.id.btnCloseShareToolbar:
                layoutShare.setVisibility(View.GONE);
                btnCloseShareTool.setVisibility(View.GONE);
                break;
            case R.id.btnScreenShot:
                CommonUtils.saveBitmap(this.getScreenShot(), "DCIM/Camera", "screen");
                DialogGenerator generator = new DialogGenerator(this);
                Translation transeletion = new Translation(this);
                transeletion.openConnection();
                generator.generateWarningDialog(screenText);
                transeletion.closeConnection();
                break;

            case R.id.btnMail:
                //*******For Sharing ScreenShot ******************//
                String path = Images.Media.insertImage(getContentResolver(), this.getScreenShot(),"ScreenShot.jpg", null);
                Uri screenshotUri = Uri.parse(path);
                Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                emailIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, body+" "+MathFriendzyHelper.getAppRateUrl(this));
                emailIntent.setType("image/png");
                startActivity(Intent.createChooser(emailIntent, "Send email using"));
                break;

            case R.id.btnFbSahre:
                Top100Activity.b = this.getScreenShot();
                Intent intent = new Intent(this, ShareActivity.class);
                intent.putExtra("message", body+" "+ MathFriendzyHelper.getAppRateUrl(this));
                intent.putExtra("flag", false);
                intent.putExtra("id", R.id.btnFbSahre);
                startActivity(intent);
                break;

            case R.id.btnTwitterShare:
                Top100Activity.b = this.getScreenShot();
                Intent intent1 = new Intent(this, ShareActivity.class);
                intent1.putExtra("message", body);
                intent1.putExtra("flag", false);
                intent1.putExtra("id", R.id.btnTwitterShare);
                startActivity(intent1);
                break;
        }
    }

    /**
     * This method parse json string for word problem score
     * @param wordProblemDataString
     * @return
     */
    private WordProblemMathScoreResultTransferObj getwordProblemResultData(
            String wordProblemDataString) {

        WordProblemMathScoreResultTransferObj wordProblemMathScore = new WordProblemMathScoreResultTransferObj();
        LinkedHashMap<Integer, ArrayList<WordProblemMathScoreObj>> finalMathListWithGradeKey =
                new LinkedHashMap<Integer, ArrayList<WordProblemMathScoreObj>>();

		/*try {
			JSONObject jsonObj = new JSONObject(wordProblemDataString);
			JSONObject jsonDataObj = jsonObj.getJSONObject("data");
			wordProblemMathScore.setTotalTime(jsonDataObj.getString("totalTime"));
			wordProblemMathScore.setAllPoints(jsonDataObj.getString("allPoints"));
			JSONArray jsonMathScoreArray = jsonDataObj.getJSONArray("mathsScore");

			for(int i = 0 ; i < jsonMathScoreArray.length() ; i ++ ){
				JSONObject jsonMathScoreOObj = jsonMathScoreArray.getJSONObject(i);
				WordProblemMathScoreObj mathScoreObj = new WordProblemMathScoreObj();
				mathScoreObj.setTime(jsonMathScoreOObj.getString("time"));
				mathScoreObj.setPoints(jsonMathScoreOObj.getString("points"));
				mathScoreObj.setScore(jsonMathScoreOObj.getString("score"));
				mathScoreObj.setCatId(jsonMathScoreOObj.getInt("cId"));
				mathScoreObj.setSubCatId(jsonMathScoreOObj.getInt("scId"));
				mathScoreList.add(mathScoreObj);
			}
			wordProblemMathScore.setMathScoreList(mathScoreList);

		} catch (JSONException e) {

			e.printStackTrace();
		}*/

        //for show result by grade , school curriculum , on 21 May 2014
        try {
            JSONObject jsonObj = new JSONObject(wordProblemDataString);
            JSONObject jsonDataObj = jsonObj.getJSONObject("data");
            wordProblemMathScore.setTotalTime(jsonDataObj.getString("totalTime"));
            wordProblemMathScore.setAllPoints(jsonDataObj.getString("allPoints"));
            JSONObject jsonMathScoreObj = jsonDataObj.getJSONObject("mathsScore");
            Iterator<?> iterator = jsonMathScoreObj.keys();
            while(iterator.hasNext()){
                String gradeKey = (String) iterator.next();
                JSONArray jsonMathScoreArray = jsonMathScoreObj.getJSONArray(gradeKey + "");

                ArrayList<WordProblemMathScoreObj> mathScoreList = new ArrayList<WordProblemMathScoreObj>();
                for(int i = 0 ; i < jsonMathScoreArray.length() ; i ++ ){
                    JSONObject jsonMathScoreOObj = jsonMathScoreArray.getJSONObject(i);
                    WordProblemMathScoreObj mathScoreObj = new WordProblemMathScoreObj();
                    mathScoreObj.setTime(jsonMathScoreOObj.getString("time"));
                    mathScoreObj.setPoints(jsonMathScoreOObj.getString("points"));
                    mathScoreObj.setScore(jsonMathScoreOObj.getString("score"));
                    mathScoreObj.setCatId(jsonMathScoreOObj.getInt("cId"));
                    mathScoreObj.setSubCatId(jsonMathScoreOObj.getInt("scId"));
                    mathScoreList.add(mathScoreObj);
                }
                finalMathListWithGradeKey.put(Integer.parseInt(gradeKey), mathScoreList);
            }
            wordProblemMathScore.setFinalMathListWithGradeKey(finalMathListWithGradeKey);

        } catch (JSONException e) {

            e.printStackTrace();
        }
        return wordProblemMathScore;
    }


    //updated for assessment test
    /**
     * This method call when user want to see the assessment result
     */
    private void clickOnAssessmentTest(){
		/*DialogGenerator dg = new DialogGenerator(this);
		dg.generateWarningDialog("Coming Soon!!!");*/

        Intent intent = new Intent(this , AssessmentTestResultListActivity.class);
        intent.putExtra("userId", userId);
        intent.putExtra("playerId", playerId);
        intent.putExtra("imageName", imageName);
        intent.putExtra("playerName", playerName);
		/*intent.putExtra("playerName" , playerName);	
		intent.putExtra("date", playDate);
		intent.putExtra("jsonFile", jsonFile);
		intent.putExtra("points", playerpoints);
		intent.putExtra("grade", playerGrade);*/
        startActivity(intent);
    }

    /**
     * Thia asynck task download the categories
     * @author Yashwant Singh
     *
     */
    class DownlLoadCategoriesForSchoolCurriculum extends AsyncTask<Void, Void, QuestionLoadedTransferObj>{
        private int grade;
        private int lang;
        private ProgressDialog pd;
        private String stringGrade;

        DownlLoadCategoriesForSchoolCurriculum(int grade , int lang , String stringGrade){
            this.lang = lang;
            this.grade = grade;
            this.stringGrade = stringGrade;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = CommonUtils.getProgressDialog(ResultActivity.this);
            pd.show();
        }

        @Override
        protected QuestionLoadedTransferObj doInBackground(Void... params) {
            LearnignCenterSchoolCurriculumServerOperation serverObj =
                    new LearnignCenterSchoolCurriculumServerOperation();
            QuestionLoadedTransferObj questionData  = serverObj.getCategoriesList(grade, lang);

            if(questionData != null){
                SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl =
                        new SchoolCurriculumLearnignCenterimpl(ResultActivity.this);
                schooloCurriculumImpl.openConnection();

                if(lang == CommonUtils.ENGLISH){
                    //changes for dialog loading wheel
                    schooloCurriculumImpl.deleteFromWordProblemCategoriesbyGrade(grade);
                    schooloCurriculumImpl.deleteFromWordProblemsSubCategoriesByCategoryId(questionData.getCategoryList());
                    schooloCurriculumImpl.insertIntoWordProblemCategories(questionData.getCategoryList(), grade);
                    schooloCurriculumImpl.insertIntoWordProblemsSubCategories(questionData.getCategoryList());
                }else{
                    SpanishChangesImpl implObj = new SpanishChangesImpl(ResultActivity.this);
                    implObj.openConn();
                    implObj.deleteFromWordProblemCategoriesbyGrade(grade);
                    implObj.deleteFromWordProblemsSubCategoriesByCategoryId(questionData.getCategoryList());
                    implObj.insertIntoWordProblemCategories(questionData.getCategoryList(), grade);
                    implObj.insertIntoWordProblemsSubCategories(questionData.getCategoryList());
                    implObj.closeConn();
                }
                schooloCurriculumImpl.closeConnection();
            }
            return questionData;
        }

        @Override
        protected void onPostExecute(QuestionLoadedTransferObj questionData) {
            pd.cancel();

            if(questionData != null)
                schoolCurriculum(grade , stringGrade);
            else
                CommonUtils.showInternetDialog(ResultActivity.this);
            super.onPostExecute(questionData);
        }
    }

    @Override
    protected void onResume() {
        //for show ad dialog
        CommonUtils.showAdDialog(this);
        //end ad dialog
        super.onResume();
    }
}
