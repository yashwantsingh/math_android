package com.mathfriendzy.controller.result.assessmenttest;

import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mathfriendzy.utils.CommonUtils;

import android.util.Log;

public class AssessmentTestResultServeroperation {
	
	private final String TAG = this.getClass().getSimpleName();
	
	/**
	 * This method get assessment test dates
	 * @param userId
	 * @param playerId
	 * @param offset
	 */
	public AssessmentResult getAssessmentDates(String userId , String playerId , int offset){
		//String action = "getTestDates";
		//changes for new assessment record
		String action = "getNewTestDates";
		//end changes
		String strURL = COMPLETE_URL  + "action=" 	+ 	action 	 + "&"
									  + "userId="   +   userId   + "&"
									  + "playerId=" +   playerId + "&"
									  + "offset="   + offset;
		//Log.e(TAG, "url " + strURL);
		return this.parseAssessmentTestDates(CommonUtils.readFromURL(strURL));
	}
	
	/**
	 * This method get the assessment test dates
	 */
	private AssessmentResult parseAssessmentTestDates(String jsonString){
		
		//Log.e(TAG, "jsonString " + jsonString);
				
		AssessmentResult assessmentResult = new  AssessmentResult();
		
		ArrayList<AssessmentResultDataObj> assessmenetResultDateList = new ArrayList<AssessmentResultDataObj>();
		try 
		{
			JSONObject jObject = new JSONObject(jsonString);
			JSONArray assementResultArray = jObject.getJSONArray("data");
			
			for(int i = 0 ; i < assementResultArray.length(); i ++ ){
				JSONObject jsonDataObj = assementResultArray.getJSONObject(i);
				
				AssessmentResultDataObj assementresultObj = new AssessmentResultDataObj();
				assementresultObj.setTid(jsonDataObj.getInt("tid"));
				assementresultObj.setDate(jsonDataObj.getString("date"));
				assementresultObj.setGrade(jsonDataObj.getInt("grade"));
				assementresultObj.setScore(jsonDataObj.getInt("score"));
				assementresultObj.setStdId(jsonDataObj.getInt("stdId"));
				assementresultObj.setRound(jsonDataObj.getInt("round"));
				assessmenetResultDateList.add(assementresultObj);
			}
			
			assessmentResult.setAssessmentDataList(assessmenetResultDateList);
			//assessmentResult.setTime(jObject.getInt("time"));
			//changes for new assessment record
			assessmentResult.setTime(0);
			//end changes
		}catch (JSONException e) 
		{
			Log.e(TAG, " inside parseAssessmentTestDates Error while parsing " + e.toString());
			return null;
		}
		
		return assessmentResult;
	}
	
	/**
	 * This method return the question played list from server for assessment result
	 * @param userId
	 * @param playerId
	 * @param testId
	 */
	public String getQuestionsPlayedInTest(String userId , String playerId , int testId){
		//String action = "getQuestionsPlayedInTest";
		//added for new results
		String action = "getQuestionsPlayedInNewTest";
		String strURL = COMPLETE_URL  + "action=" 	+ 	action 	 + "&"
									  + "userId="   +   userId   + "&"
									  + "playerId=" +   playerId + "&"
									  + "testId="   + testId;
		//Log.e(TAG, "url " + strURL);
		return this.parseGetQuestionsPlayedInTestJsonString(CommonUtils.readFromURL(strURL));
	}
	
	/**
	 * This method parse jsonString for get played question detail json string
	 * @param jsonString
	 */
	private String parseGetQuestionsPlayedInTestJsonString(String jsonString){
		//Log.e(TAG, "jsonString " + jsonString);
		String response = null;
		
		try 
		{
			JSONObject jObject = new JSONObject(jsonString);
			response = jObject.getString("data");
		}catch (JSONException e) 
		{
			Log.e(TAG, " inside parseAssessmentTestDates Error whilem parsing " + e.toString());
		}
		return response;	
	}
}
