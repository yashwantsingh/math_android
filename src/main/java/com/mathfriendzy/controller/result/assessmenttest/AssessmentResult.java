package com.mathfriendzy.controller.result.assessmenttest;

import java.io.Serializable;
import java.util.ArrayList;

public class AssessmentResult implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<AssessmentResultDataObj> assessmentDataList;
	private int time;
	
	public ArrayList<AssessmentResultDataObj> getAssessmentDataList() {
		return assessmentDataList;
	}
	public void setAssessmentDataList(
			ArrayList<AssessmentResultDataObj> assessmentDataList) {
		this.assessmentDataList = assessmentDataList;
	}
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	
	
}
