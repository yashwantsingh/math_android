package com.mathfriendzy.controller.result.assessmenttest;


import static com.mathfriendzy.utils.ICommonUtils.FACEBOOK_HOST_NAME;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.model.assessmenttest.AssessmentStandardDto;
import com.mathfriendzy.model.assessmenttest.AssessmentTestImpl;
import com.mathfriendzy.model.assessmenttest.AssessmentTestServerOperation;
import com.mathfriendzy.model.chooseAvtar.ChooseAvtarOpration;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DateTimeOperation;
import com.mathfriendzy.utils.DialogGenerator;


public class AssessmentTestResultListActivity extends ActBaseClass {

	private final String TAG = this.getClass().getSimpleName();

	private TextView txtPlayerName       = null;
	private TextView resultAssessmentTop = null;
	private ImageView imgPlayerImage     = null;
	private TextView txtDate             = null;
	private TextView txtScore            = null;
	private LinearLayout dateAndScoreListLayout = null;

	//player detail
	private String playerName			= null;
	private int userId					= 0;
	private int playerId				= 0;
	private String imageName ;

	//for image
	private Bitmap profileImageBitmap = null;

	//for showMore Button
	private Button btnShowMore = null;
	private int offset = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_assessment_test_result_list);

		this.getIntentValue();
		this.setWidgetsReferences();
		this.setTextFromTranslation();
		this.setPlayerDetail();
		this.getTestDates();
	}

	/**
	 * This method get intent value that are set into previous activity
	 */
	private void getIntentValue(){

		playerName  = getIntent().getStringExtra("playerName");
		userId		= getIntent().getIntExtra("userId", 0);
		playerId	= getIntent().getIntExtra("playerId", 0);		
		imageName   = getIntent().getStringExtra("imageName");
	}


	/**
	 * This method set widgets references from layout
	 */
	private void setWidgetsReferences(){
		txtPlayerName = (TextView) findViewById(R.id.txtPlayerName);
		resultAssessmentTop = (TextView) findViewById(R.id.resultAssessmentTop);
		imgPlayerImage   = (ImageView) findViewById(R.id.imgPlayerImage);
		txtDate = (TextView) findViewById(R.id.txtDate);
		txtScore = (TextView) findViewById(R.id.txtScore);
		dateAndScoreListLayout = (LinearLayout) findViewById(R.id.dateAndScoreListLayout);

		btnShowMore = (Button) findViewById(R.id.btnShowMore);
	}


	/**
	 * This method set the translation text
	 */
	private void setTextFromTranslation(){
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		resultAssessmentTop.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleResults"));
		txtDate.setText(transeletion.getTranselationTextByTextIdentifier("mfLblDate") + ":");
		txtScore.setText(transeletion.getTranselationTextByTextIdentifier("mfBtnTitleScore") + ":");
		btnShowMore.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleShowMore"));
		transeletion.closeConnection();
	}

	/**
	 * This method set playerDetail
	 */
	private void setPlayerDetail(){

		txtPlayerName.setText(playerName);

		if(imageName.length() > 0){
			try
			{
				Long.parseLong(imageName);
				//changes for Internet Connection
				if(CommonUtils.isInternetConnectionAvailable(this))
				{
					String strUrl = FACEBOOK_HOST_NAME + imageName + "/picture?type=large";
					new FacebookImageLoaderTask(strUrl).execute(null,null,null);
				}
			}
			catch(NumberFormatException ee)
			{
				ChooseAvtarOpration chooseAvtarObj = new ChooseAvtarOpration();
				chooseAvtarObj.openConn(this);
				if(chooseAvtarObj.getAvtarImageByName(imageName) != null)
				{
					/*profileImageBitmap = CommonUtils.getBitmapFromByte
							(chooseAvtarObj.getAvtarImageByName(imageName) , AssessmentTestResultListActivity.this);*/
					profileImageBitmap = CommonUtils.getBitmapFromByte
							(chooseAvtarObj.getAvtarImageByName(imageName));
					imgPlayerImage.setImageBitmap(profileImageBitmap);
				}
				chooseAvtarObj.closeConn();
			}
		}else{
			ChooseAvtarOpration chooseAvtarObj = new ChooseAvtarOpration();
			chooseAvtarObj.openConn(this);
			/*profileImageBitmap = CommonUtils.getBitmapFromByte
					(chooseAvtarObj.getAvtarImageByName("smiley") , AssessmentTestResultListActivity.this);*/
			profileImageBitmap = CommonUtils.getBitmapFromByte
					(chooseAvtarObj.getAvtarImageByName("smiley"));
			imgPlayerImage.setImageBitmap(profileImageBitmap);
			chooseAvtarObj.closeConn();
		}
	}

	/**
	 * This method get the assessment dates from server
	 */
	private void getTestDates(){

		if(CommonUtils.isInternetConnectionAvailable(this)){
			new GetTestDated(userId + "" , playerId + "", offset).execute(null,null,null);
		}else{

			DialogGenerator dg = new DialogGenerator(this);
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			dg.generateWarningDialog(transeletion.
					getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
			transeletion.closeConnection();
		}
	}

	/**
	 * This asyncTask set image from facebook url to the Button 
	 * @author Yashwant Singh
	 *
	 */
	class FacebookImageLoaderTask extends AsyncTask<Void, Void, Void>
	{
		private String strUrl = null;
		public FacebookImageLoaderTask(String strUrl)
		{
			this.strUrl = strUrl;
		}

		@Override
		protected Void doInBackground(Void... params) 
		{				
			URL img_value;
			try 
			{
				img_value = new URL(strUrl);
				profileImageBitmap = BitmapFactory.decodeStream(img_value.openConnection().getInputStream());
			} 
			catch (MalformedURLException e) 
			{			
				e.printStackTrace();
				Log.e("EditActivity", "Problem in setting image" + e);
			}
			catch(Exception ee)
			{
				Log.e("EditActivity", "Problem in setting image" + ee);
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			imgPlayerImage.setImageBitmap(profileImageBitmap);
			imgPlayerImage.invalidate();
			super.onPostExecute(result);
		}
	}

	/**
	 * This get test dates from server
	 * @author Yashwant Singh
	 *
	 */
	class GetTestDated extends AsyncTask<Void, Void, AssessmentResult>{

		private String userId;
		private String playerId;
		private int offset;
		private ProgressDialog pd;

		GetTestDated(String userId , String playerId , int offset){
			this.userId 	= userId;
			this.playerId 	= playerId;
			this.offset 	= offset;
		}

		@Override
		protected void onPreExecute() {
			pd = CommonUtils.getProgressDialog(AssessmentTestResultListActivity.this);
			pd.show();

			super.onPreExecute();
		}

		@Override
		protected AssessmentResult doInBackground(Void... params) {
			AssessmentTestResultServeroperation serverObj = new AssessmentTestResultServeroperation();
			AssessmentResult assessmentResult = serverObj.getAssessmentDates(userId, playerId, offset);
			return assessmentResult;
		}

		@Override
		protected void onPostExecute(AssessmentResult assessmentResult) {
			pd.cancel();

			if(assessmentResult != null){
				createDateScoreResultList(assessmentResult);
			}
			super.onPostExecute(assessmentResult);
		}
	}

	/**
	 * This method call when user want to see the result of the seleced date
	 * @param time
	 * @param assessmentResultDataObj
	 */
	private void clickOnSelectedDate(int time, AssessmentResultDataObj assessmentResultDataObj){
		if(CommonUtils.isInternetConnectionAvailable(this)){
			AssessmentTestImpl implObj = new AssessmentTestImpl(this);
			implObj.openConnection();
			boolean isStandardLoaded = implObj.isStandard(assessmentResultDataObj.getGrade());
			implObj.closeConnection();
			if(isStandardLoaded){
				this.goOnNextScreen(time, assessmentResultDataObj);
			}else{
				int lang = CommonUtils.ENGLISH; 
				if(CommonUtils.getUserLanguageCode(this) == CommonUtils.ENGLISH)
					lang = CommonUtils.ENGLISH;
				else 
					lang = CommonUtils.SPANISH;
				new GetAssessmentStandards(time , assessmentResultDataObj ,lang).execute(null,null,null);
			}
		}else{
			this.goOnNextScreen(time, assessmentResultDataObj);
		}
	}

	/**
	 * This method is for going to the next screen
	 * @param time
	 * @param assessmentResultDataObj
	 */
	private void goOnNextScreen(int time, AssessmentResultDataObj assessmentResultDataObj){
		Intent intent = new Intent(AssessmentTestResultListActivity.this 
				, AssessmentResultActivity.class);
		intent.putExtra("time", time);
		intent.putExtra("dateScoreData", assessmentResultDataObj);
		intent.putExtra("imageName", imageName);
		intent.putExtra("playerName", playerName);
		intent.putExtra("userId", userId);
		intent.putExtra("playerId", playerId);
		startActivity(intent);
	}

	/**
	 * This method creates the date score result
	 */
	private void createDateScoreResultList(final AssessmentResult assessmentResult){

		final ArrayList<RelativeLayout> layoutList = new ArrayList<RelativeLayout>();

		dateAndScoreListLayout.removeAllViews();

		DateTimeOperation date = new DateTimeOperation();

		for(int i = 0 ; i < assessmentResult.getAssessmentDataList().size() ; i ++ ){

			LayoutInflater inflater = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
			RelativeLayout child = (RelativeLayout) inflater.inflate(R.layout.result_list_layout, null);
			TextView txtDate = (TextView) child.findViewById(R.id.txtDate);
			TextView txtScore = (TextView) child.findViewById(R.id.txtScore);

			txtDate.setText(date.setDate(assessmentResult.getAssessmentDataList().get(i).getDate().subSequence(0, 10) + ""));
			txtScore.setText(assessmentResult.getAssessmentDataList().get(i).getScore() + "%");

			child.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					for(int i = 0 ; i < layoutList.size() ; i ++ ){
						if(layoutList.get(i) == v){
							/*Intent intent = new Intent(AssessmentTestResultListActivity.this 
									, AssessmentResultActivity.class);
							intent.putExtra("time", assessmentResult.getTime());
							intent.putExtra("dateScoreData", assessmentResult.getAssessmentDataList().get(i));
							intent.putExtra("imageName", imageName);
							intent.putExtra("playerName", playerName);
							intent.putExtra("userId", userId);
							intent.putExtra("playerId", playerId);
							startActivity(intent);*/
							clickOnSelectedDate(assessmentResult.getTime() , assessmentResult
									.getAssessmentDataList().get(i));
						}
					}

				}
			});

			layoutList.add(child);
			dateAndScoreListLayout.addView(child);

			if(assessmentResult.getAssessmentDataList().size() >= 10){
				btnShowMore.setVisibility(Button.VISIBLE);

				btnShowMore.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						offset = offset + 10;		
						new GetTestDated(userId + "" , playerId + "", offset).execute(null,null,null);
					}
				});
			}
		}
	}

	/**
	 * Get Assessment Standards
	 * @author Yashwant Singh
	 *
	 */
	class GetAssessmentStandards extends AsyncTask<Void, Void, AssessmentStandardDto>{

		private ProgressDialog pd;
		private int lang;
		private int time;
		private AssessmentResultDataObj assessmentResultDataObj; 

		GetAssessmentStandards(int time, AssessmentResultDataObj assessmentResultDataObj,int lang){
			this.lang = lang;
			this.time = time;
			this.assessmentResultDataObj = assessmentResultDataObj;
		}

		@Override
		protected void onPreExecute() {
			pd = CommonUtils.getProgressDialog(AssessmentTestResultListActivity.this);
			pd.show();
			super.onPreExecute();
		}

		@Override
		protected AssessmentStandardDto doInBackground(Void... params) {
			AssessmentTestServerOperation serverObj = new AssessmentTestServerOperation();
			AssessmentStandardDto assessmentStandard = serverObj.getAssessmentStandards(lang);

			if(assessmentStandard != null){
				try{
					AssessmentTestImpl implObj = new AssessmentTestImpl(AssessmentTestResultListActivity.this);
					implObj.openConnection();
					implObj.deleteFromWordAssessmentStandards();
					implObj.deleteFromWordAssessmentSubStandards();
					implObj.deleteFromWordAssessmentSubCategoriesInfo();
					implObj.insertIntoWordAssessmentStandards(assessmentStandard.getStandardDataList());
					implObj.insertIntoWordAssessmentSubStandards(assessmentStandard.getStandardDataList());
					implObj.insertIntoWordAssessmentSubCategoriesInfo(assessmentStandard.getCategoriesList());
					implObj.closeConnection();
				}catch(Exception e){
					Log.e("AssessmentClass", "No Data on Server For Stabdards " + e.toString());
				}
			}
			return assessmentStandard;
		}

		@Override
		protected void onPostExecute(AssessmentStandardDto assessmentStandard) {
			pd.cancel();
			if(assessmentStandard != null){
			goOnNextScreen(time, assessmentResultDataObj);
			}else{
				CommonUtils.showInternetDialog(AssessmentTestResultListActivity.this);
			}
			super.onPostExecute(assessmentStandard);
		}
	}
	
	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}
}
