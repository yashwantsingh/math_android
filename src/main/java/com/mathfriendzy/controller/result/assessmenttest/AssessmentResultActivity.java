package com.mathfriendzy.controller.result.assessmenttest;

import static com.mathfriendzy.utils.ICommonUtils.FACEBOOK_HOST_NAME;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.assessmenttest.xmlparsing.XmlParser;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.controller.learningcenter.schoolcurriculum.SeeAnswerActivityForSchoolCurriculum;
import com.mathfriendzy.dawnloadimagesfromserver.DawnloadImagesFromserver;
import com.mathfriendzy.model.assessmenttest.AssessmentTestImpl;
import com.mathfriendzy.model.chooseAvtar.ChooseAvtarOpration;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.LearnignCenterSchoolCurriculumServerOperation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.MathEquationTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.QuestionLoadedTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SchoolCurriculumLearnignCenterimpl;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.UpdatedInfoTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.WordProblemQuestionTransferObj;
import com.mathfriendzy.model.schoolcurriculum.singlefriendzy.SingleFriendzyServerOperationForWordProblem;
import com.mathfriendzy.model.spanishchanges.SpanishChangesImpl;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DateTimeOperation;
import com.mathfriendzy.utils.DialogGenerator;

public class AssessmentResultActivity extends ActBaseClass implements OnClickListener{

	private TextView labelTop = null;
	private ImageView imgSmiley = null;
	private TextView txtPlayerName = null;
	private TextView txtAssessmentGradeText = null;
	private TextView txtScoreText = null;
	private TextView txtTimeSpentPlaying = null;
	private Button  btnSeeAnswer = null;

	//added for new result
	private TextView txtStandard = null;
	private TextView txtRound    = null;


	//for intent value
	private int time = 0 ;
	AssessmentResultDataObj assessmentData;

	//for player detail
	private String playerName	= null;
	private int userId			= 0;
	private int playerId		= 0;
	private String imageName ;
	//for image
	private Bitmap profileImageBitmap = null;

	private final String TAG = this.getClass().getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_assessment_result);

		this.getIntentValue();
		this.setWidgetsReferences();
		this.setTextFromTranslation();
		this.setPlayerDetail();
		this.setListenerOnWidgets();
	}

	/**
	 * This method getIntent value which are set in previous activity
	 */
	private void getIntentValue(){
		assessmentData 	= (AssessmentResultDataObj) this.getIntent().getSerializableExtra("dateScoreData");
		time 			= this.getIntent().getIntExtra("time", 0);
		playerName  = getIntent().getStringExtra("playerName");
		userId		= getIntent().getIntExtra("userId", 0);
		playerId	= getIntent().getIntExtra("playerId", 0);		
		imageName   = getIntent().getStringExtra("imageName");
	}

	/**
	 * This method set widgets references from layout
	 */
	private void setWidgetsReferences(){
		labelTop 		= (TextView) findViewById(R.id.labelTop);
		imgSmiley 		= (ImageView) findViewById(R.id.imgSmiley);
		txtPlayerName 	= (TextView) findViewById(R.id.txtPlayerName);
		txtAssessmentGradeText = (TextView) findViewById(R.id.txtAssessmentGradeText); 
		txtScoreText	 = (TextView) findViewById(R.id.txtScoreText);
		txtTimeSpentPlaying =(TextView) findViewById(R.id.txtTimeSpentPlaying);
		btnSeeAnswer 	= (Button) findViewById(R.id.btnSeeAnswer);

		//added for new result
		txtStandard     = (TextView) findViewById(R.id.txtStandard);
		txtRound     	= (TextView) findViewById(R.id.txtRound);
	}

	/**
	 * This method set text from translation
	 */
	private void setTextFromTranslation(){

		DateTimeOperation date = new DateTimeOperation();

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		labelTop.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleResults"));
		btnSeeAnswer.setText(transeletion.getTranselationTextByTextIdentifier("mfBtnTitleSeeAnswers"));
		txtAssessmentGradeText.setText(transeletion.getTranselationTextByTextIdentifier("lblAssessment")
				+ ": " + this.setGrade(assessmentData.getGrade()) + " "
				+ transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade"));

		txtScoreText.setText(transeletion.getTranselationTextByTextIdentifier("mfBtnTitleScore") + ": " 
				+ assessmentData.getScore() + "%");
		txtTimeSpentPlaying.setText(transeletion.getTranselationTextByTextIdentifier("lblTimeSpentPlayedSinceLastTest")
				+": " + date.setTimeFormat((time)));

		//added for new results
		txtRound.setText(transeletion.getTranselationTextByTextIdentifier
				("lblRound") + ": " + assessmentData.getRound());
		txtStandard.setText(this.getStandardNameByStdId(assessmentData.getStdId()));
		//end changes
		transeletion.closeConnection();
	}

	/**
	 * This method get the standard name by id
	 * @param stdId
	 * @return
	 */
	private String getStandardNameByStdId(int stdId){
		String stdName = "";
		AssessmentTestImpl implObj = new AssessmentTestImpl(this);
		implObj.openConnection();
		stdName = implObj.getStandardNameById(stdId);
		implObj.closeConnection();
		return stdName;
	}

	/**
	 * use to set standard with grade
	 * @param grade
	 */
	private String setGrade(int grade)
	{
		String alias;
		switch(grade)
		{
		case 1:
			alias = "1st";
			break;
		case 2:
			alias = "2nd";
			break;
		case 3:
			alias = "3rd";
			break;
		case 13:
			alias = "adult";
		default :
			alias = grade+"th";
		}	

		return alias;

	}//END setGrade method

	/**
	 * This method set player detail
	 */
	private void setPlayerDetail(){

		txtPlayerName.setText(playerName);

		if(imageName.length() > 0){
			try
			{
				Long.parseLong(imageName);
				//changes for Internet Connection
				if(CommonUtils.isInternetConnectionAvailable(this))
				{
					String strUrl = FACEBOOK_HOST_NAME + imageName + "/picture?type=large";
					new FacebookImageLoaderTask(strUrl).execute(null,null,null);
				}
			}
			catch(NumberFormatException ee)
			{
				ChooseAvtarOpration chooseAvtarObj = new ChooseAvtarOpration();
				chooseAvtarObj.openConn(this);
				if(chooseAvtarObj.getAvtarImageByName(imageName) != null)
				{
					/*profileImageBitmap = CommonUtils.getBitmapFromByte
							(chooseAvtarObj.getAvtarImageByName(imageName) , AssessmentResultActivity.this);*/
					profileImageBitmap = CommonUtils.getBitmapFromByte
							(chooseAvtarObj.getAvtarImageByName(imageName));
					imgSmiley.setImageBitmap(profileImageBitmap);
				}
				chooseAvtarObj.closeConn();
			}
		}else{
			ChooseAvtarOpration chooseAvtarObj = new ChooseAvtarOpration();
			chooseAvtarObj.openConn(this);
			/*profileImageBitmap = CommonUtils.getBitmapFromByte
					(chooseAvtarObj.getAvtarImageByName("smiley") , AssessmentResultActivity.this);*/
			profileImageBitmap = CommonUtils.getBitmapFromByte
					(chooseAvtarObj.getAvtarImageByName("smiley"));
			imgSmiley.setImageBitmap(profileImageBitmap);
			chooseAvtarObj.closeConn();
		}
	}

	/**
	 * This method set listener on widgets
	 */
	private void setListenerOnWidgets(){
		btnSeeAnswer.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.btnSeeAnswer){
			this.clickOnSeeAnswer();
		}

	}

	/**
	 * This method call when user click on see answer
	 */
	private void clickOnSeeAnswer(){
		if(CommonUtils.isInternetConnectionAvailable(this))
			new GetQuestionsPlayedInTest(userId + "" , playerId + "", assessmentData.getTid()).execute(null,null,null);
	}

	/**
	 * This asyncTask set image from facebook url to the Button 
	 * @author Yashwant Singh
	 *
	 */
	class FacebookImageLoaderTask extends AsyncTask<Void, Void, Void>
	{
		private String strUrl = null;
		public FacebookImageLoaderTask(String strUrl)
		{
			this.strUrl = strUrl;
		}

		@Override
		protected Void doInBackground(Void... params) 
		{				
			URL img_value;
			try 
			{
				img_value = new URL(strUrl);
				profileImageBitmap = BitmapFactory.decodeStream(img_value.openConnection().getInputStream());
			} 
			catch (MalformedURLException e) 
			{			
				e.printStackTrace();
				Log.e("EditActivity", "Problem in setting image" + e);
			}
			catch(Exception ee)
			{
				Log.e("EditActivity", "Problem in setting image" + ee);
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			imgSmiley.setImageBitmap(profileImageBitmap);
			imgSmiley.invalidate();
			super.onPostExecute(result);
		}
	}

	/**
	 * This method is for go on next Screen
	 * @param mathEquationDataList 
	 */
	private void goOnNextScreen(ArrayList<MathEquationTransferObj> mathEquationDataList){
		ArrayList<MathEquationTransferObj> playDataList = new ArrayList<MathEquationTransferObj>();

		//changes for Spanish
		if(CommonUtils.getUserLanguageCode(AssessmentResultActivity.this) == CommonUtils.ENGLISH){
			for(int i = 0 ; i < mathEquationDataList.size() ; i ++ ){
				MathEquationTransferObj mathObjData = mathEquationDataList.get(i);

				SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl
						(AssessmentResultActivity.this);
				schoolImpl.openConnection();
				//get question from database into questionObj object
				WordProblemQuestionTransferObj questionObj  = schoolImpl.
						getQuestionByQuestionId(mathEquationDataList.get(i).getEqautionId());
				schoolImpl.closeConnection();
				mathObjData.setQuestionObj(questionObj);
				//end

				playDataList.add(mathObjData);
			}
		}else{//for Spanish
			for(int i = 0 ; i < mathEquationDataList.size() ; i ++ ){
				MathEquationTransferObj mathObjData = mathEquationDataList.get(i);

				SpanishChangesImpl schoolImpl = new SpanishChangesImpl(AssessmentResultActivity.this);
				schoolImpl.openConn();
				//get question from database into questionObj object
				WordProblemQuestionTransferObj questionObj  = schoolImpl.
						getQuestionByQuestionId(mathEquationDataList.get(i).getEqautionId());
				schoolImpl.closeConn();
				mathObjData.setQuestionObj(questionObj);
				//end

				playDataList.add(mathObjData);
			}
		}

		Intent intent = new Intent(AssessmentResultActivity.this , 
				SeeAnswerActivityForSchoolCurriculum.class);
		intent.putExtra("playDatalist", playDataList);
		intent.putExtra("isCallForAssessment", true);
		intent.putExtra("selectedGrade", assessmentData.getGrade());
		intent.putExtra("isCallForresult", true);
		startActivity(intent);
	}

	/**
	 * This class getQuestion played in assessment test
	 * @author Yashwant Singh
	 *
	 */
	class GetQuestionsPlayedInTest extends AsyncTask<Void, Void, String>{

		private String userId;
		private String playerId;
		private int testId;

		private ProgressDialog pd;

		GetQuestionsPlayedInTest(String userId , String playerId , int testId){
			this.userId 	= userId;
			this.playerId 	= playerId;
			this.testId 	= testId;
		}

		@Override
		protected void onPreExecute() {
			pd = CommonUtils.getProgressDialog(AssessmentResultActivity.this);
			pd.show();
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(Void... params) {
			AssessmentTestResultServeroperation serverObj = new AssessmentTestResultServeroperation();
			String questionResult  = serverObj.getQuestionsPlayedInTest(userId, playerId, testId);
			return questionResult;
		}

		@Override
		protected void onPostExecute(String questionResult) {
			pd.cancel();

			if(questionResult != null){
				new XmlParser(questionResult , AssessmentResultActivity.this);
				ArrayList<MathEquationTransferObj> mathEquationDataList = XmlParser.getPlayDataList();
				clickForSchoolCurriculum(assessmentData.getGrade() , mathEquationDataList);

				//goOnNextScreen(mathEquationDataList);

				/*ArrayList<MathEquationTransferObj> playDataList = new ArrayList<MathEquationTransferObj>();

				//changes for Spanish
				if(CommonUtils.getUserLanguageCode(AssessmentResultActivity.this) == CommonUtils.ENGLISH){
					for(int i = 0 ; i < mathEquationDataList.size() ; i ++ ){
						MathEquationTransferObj mathObjData = mathEquationDataList.get(i);

						SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl
								(AssessmentResultActivity.this);
						schoolImpl.openConnection();
						//get question from database into questionObj object
						WordProblemQuestionTransferObj questionObj  = schoolImpl.getQuestionByQuestionId(mathEquationDataList.get(i).getEqautionId());
						schoolImpl.closeConnection();
						mathObjData.setQuestionObj(questionObj);
						//end

						playDataList.add(mathObjData);
					}
				}else{//for Spanish
					for(int i = 0 ; i < mathEquationDataList.size() ; i ++ ){
						MathEquationTransferObj mathObjData = mathEquationDataList.get(i);

						SpanishChangesImpl schoolImpl = new SpanishChangesImpl(AssessmentResultActivity.this);
						schoolImpl.openConn();
						//get question from database into questionObj object
						WordProblemQuestionTransferObj questionObj  = schoolImpl.getQuestionByQuestionId(mathEquationDataList.get(i).getEqautionId());
						schoolImpl.closeConn();
						mathObjData.setQuestionObj(questionObj);
						//end

						playDataList.add(mathObjData);
					}
				}

				Intent intent = new Intent(AssessmentResultActivity.this , 
						SeeAnswerActivityForSchoolCurriculum.class);
				intent.putExtra("playDatalist", playDataList);
				intent.putExtra("isCallForAssessment", true);
				intent.putExtra("selectedGrade", assessmentData.getGrade());
				startActivity(intent);*/
			}

			super.onPostExecute(questionResult);
		}
	}

	//added for downloading questions
	/**
	 * This method call when user play single school curriculum
	 * @param mathEquationDataList 
	 */
	private void clickForSchoolCurriculum(int grade, ArrayList<MathEquationTransferObj> mathEquationDataList){

		if(CommonUtils.getUserLanguageCode(this) == CommonUtils.ENGLISH){
			SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl = 
					new SchoolCurriculumLearnignCenterimpl(this);
			schooloCurriculumImpl.openConnection();
			boolean isQuestionLoaded = schooloCurriculumImpl.isQuestionLeaded(grade);
			schooloCurriculumImpl.closeConnection();
			if(isQuestionLoaded){
				if(CommonUtils.isInternetConnectionAvailable(this)){
					new GetWordProblemQuestionUpdatedDate(grade , false , mathEquationDataList).execute(null,null,null);
				}else{
					this.goOnNextScreen(mathEquationDataList);
				}
			}
			else{
				if(CommonUtils.isInternetConnectionAvailable(this)){
					new GetWordProblemQuestion(grade ,this , mathEquationDataList)
					.execute(null,null,null);
					new GetWordProblemQuestionUpdatedDate(grade , true , mathEquationDataList).execute(null,null,null);
				}else{
					DialogGenerator dg = new DialogGenerator(this);
					Translation transeletion = new Translation(this);
					transeletion.openConnection();
					dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
					transeletion.closeConnection();	
				}
			}
		}else{
			callWhenUserLanguageIsSpanish(this , CommonUtils.SPANISH , grade , mathEquationDataList);
		}
	}

	//changes for Spanish
	/**
	 * This method call when user language is Spanish
	 * This method for downloading question from server for Spanish Language
	 * @param mathEquationDataList 
	 */
	private void callWhenUserLanguageIsSpanish(Context context , int lang , int grade, 
			ArrayList<MathEquationTransferObj> mathEquationDataList){
		try {
			SpanishChangesImpl spanishImplObj = new SpanishChangesImpl(context);
			spanishImplObj.openConn();
			//CommonUtils.CheckWordProblemSpanishTable(context);
			boolean isSpanishQuestionLoaded = spanishImplObj.isQuestionLeaded(grade);

			if(isSpanishQuestionLoaded){
				if(CommonUtils.isInternetConnectionAvailable(context)){
					new GetWordProblemQuestionUpdatedDate(grade , false , mathEquationDataList)
					.execute(null,null,null);
				}else{
					this.goOnNextScreen(mathEquationDataList);
				}
			}
			else{
				if(CommonUtils.isInternetConnectionAvailable(context)){
					new GetWordProblemQuestion(grade ,context , mathEquationDataList)
					.execute(null,null,null);
					new GetWordProblemQuestionUpdatedDate(grade , true , mathEquationDataList)
					.execute(null,null,null);
				}else{
					DialogGenerator dg = new DialogGenerator(context);
					Translation transeletion = new Translation(context);
					transeletion.openConnection();
					dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
					transeletion.closeConnection();	
				}
			}
			spanishImplObj.closeConn();

		} catch (Exception e) {
			Log.e(TAG, "inside callWhenUserLanguageIsSpanish " + e.toString());
		} 
	}

	/**
	 * This class get word problem updated dates from server
	 * @author Yashwant Singh
	 *
	 */
	private class GetWordProblemQuestionUpdatedDate extends AsyncTask<Void, Void, ArrayList<UpdatedInfoTransferObj>>{
		private int grade;
		private ProgressDialog pd;
		private boolean isFisrtTimeCallForGrade;
		private ArrayList<MathEquationTransferObj> mathEquationDataList;

		GetWordProblemQuestionUpdatedDate(int grade , boolean isFisrtTimeCallForGrade,
				ArrayList<MathEquationTransferObj> mathEquationDataList){
			this.grade = grade;
			this.isFisrtTimeCallForGrade = isFisrtTimeCallForGrade;
			this.mathEquationDataList    = mathEquationDataList;
		}

		@Override
		protected void onPreExecute() {
			if(isFisrtTimeCallForGrade == false){
				pd = CommonUtils.getProgressDialog(AssessmentResultActivity.this);
				pd.show();
			}
			super.onPreExecute();
		}

		@Override
		protected ArrayList<UpdatedInfoTransferObj>  doInBackground(Void... params) {
			SingleFriendzyServerOperationForWordProblem serverObj = 
					new SingleFriendzyServerOperationForWordProblem();
			//changes for Spanish
			ArrayList<UpdatedInfoTransferObj>  updatedList = new ArrayList<UpdatedInfoTransferObj>();
			if(CommonUtils.getUserLanguageCode(AssessmentResultActivity.this) == CommonUtils.ENGLISH)
				updatedList = serverObj.getWordProbleQuestionUpdatedData();
			else//for Spanish
				updatedList = serverObj.getWordProbleQuestionUpdatedDateForSpanish(CommonUtils.SPANISH);
			return updatedList;
		}

		@Override
		protected void onPostExecute(ArrayList<UpdatedInfoTransferObj>  updatedList) {
			if(isFisrtTimeCallForGrade == false){
				pd.cancel();
			}

			if(updatedList != null){
				SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl = 
						new SchoolCurriculumLearnignCenterimpl(AssessmentResultActivity.this);
				schooloCurriculumImpl.openConnection();

				//changes for Spanish
				SpanishChangesImpl implObj = new SpanishChangesImpl(AssessmentResultActivity.this);
				implObj.openConn();

				if(isFisrtTimeCallForGrade){
					//changes for Spanish
					if(CommonUtils.getUserLanguageCode(AssessmentResultActivity.this) == CommonUtils.ENGLISH){
						if(!schooloCurriculumImpl.isWordProblemUpdateDetailsTableDataExist()){
							schooloCurriculumImpl.insertIntoWordProblemUpdateDetails(updatedList);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
						}else{
							String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
							schooloCurriculumImpl.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
						}
					}else{//for Spanish
						if(!implObj.isWordProblemUpdateDetailsTableDataExist()){
							implObj.insertIntoWordProblemUpdateDetails(updatedList);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
						}else{
							String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
							implObj.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
						}
					}
				}
				else{
					if(CommonUtils.getUserLanguageCode(AssessmentResultActivity.this) == CommonUtils.ENGLISH){
						String updatedDateFromDatabase = schooloCurriculumImpl.getUpdatedDate(grade);
						if(CommonUtils.isUpdateDateChange(updatedDateFromDatabase , grade , updatedList))
						{
							String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
							schooloCurriculumImpl.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();

							if(CommonUtils.isInternetConnectionAvailable(AssessmentResultActivity.this)){
								new GetWordProblemQuestion(grade , AssessmentResultActivity.this
										, mathEquationDataList).execute(null,null,null);
							}else{
								goOnNextScreen(mathEquationDataList);
							}
						}
						else{
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
							goOnNextScreen(mathEquationDataList);
						}

					}else{//for Spanish
						String updatedDateFromDatabase = implObj.getUpdatedDate(grade);
						if(CommonUtils.isUpdateDateChange(updatedDateFromDatabase , grade , updatedList))
						{
							String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
							implObj.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();

							if(CommonUtils.isInternetConnectionAvailable(AssessmentResultActivity.this)){
								new GetWordProblemQuestion(grade , AssessmentResultActivity.this
										, mathEquationDataList).execute(null,null,null);
							}else{
								goOnNextScreen(mathEquationDataList);
							}
						}
						else{
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
							goOnNextScreen(mathEquationDataList);
						}
					}
				}
			}else{
				if(isFisrtTimeCallForGrade == false){
					CommonUtils.showInternetDialog(AssessmentResultActivity.this);
				}
			}
			super.onPostExecute(updatedList);
		}
	}

	/**
	 * This asynctask get questions from server according to grade if not loaded in database
	 * @author Yashwant Singh
	 *
	 */
	public class GetWordProblemQuestion extends AsyncTask<Void, Void, QuestionLoadedTransferObj>{

		private int grade;
		private ProgressDialog pd;
		private Context context;
		ArrayList<String> imageNameList = new ArrayList<String>();
		private ArrayList<MathEquationTransferObj> mathEquationDataList;

		public GetWordProblemQuestion(int grade , Context context, 
				ArrayList<MathEquationTransferObj> mathEquationDataList){
			this.grade 		= grade;
			this.context 	= context;
			this.mathEquationDataList = mathEquationDataList;
		}

		@Override
		protected void onPreExecute() {

			Translation translation = new Translation(context);
			translation.openConnection();
			pd = CommonUtils.getProgressDialog(context, translation.getTranselationTextByTextIdentifier
					("alertPleaseWaitWeAreDownloading") , true);
			translation.closeConnection();
			pd.show();

			super.onPreExecute();
		}

		@Override
		protected QuestionLoadedTransferObj doInBackground(Void... params) {
			LearnignCenterSchoolCurriculumServerOperation serverObj = new LearnignCenterSchoolCurriculumServerOperation();
			//QuestionLoadedTransferObj questionData = serverObj.getWordProblemQuestions(grade);
			//changes for Spanish Changes
			QuestionLoadedTransferObj questionData = new QuestionLoadedTransferObj();
			if(CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH)
				questionData = serverObj.getWordProblemQuestions(grade);
			else
				questionData= serverObj.getWordProblemQuestionsForSpanish(grade, CommonUtils.SPANISH);

			if(questionData != null){
				SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl = new SchoolCurriculumLearnignCenterimpl(context);
				schooloCurriculumImpl.openConnection();
				if(CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH){
					//changes for dialog loading wheel
					schooloCurriculumImpl.deleteFromWordProblemCategoriesbyGrade(grade);
					schooloCurriculumImpl.deleteFromWordProblemsSubCategoriesByCategoryId(questionData.getCategoryList());
					schooloCurriculumImpl.deleteFromWordProblemsQuestionsCategoryId(questionData.getCategoryList());

					schooloCurriculumImpl.insertIntoWordProblemCategories(questionData.getCategoryList(), grade);
					schooloCurriculumImpl.insertIntoWordProblemsSubCategories(questionData.getCategoryList());
					schooloCurriculumImpl.insertIntoWordProblemsQuestions(questionData.getQuestionList());
				}else{
					SpanishChangesImpl implObj = new SpanishChangesImpl(context);
					implObj.openConn();

					implObj.deleteFromWordProblemCategoriesbyGrade(grade);
					implObj.deleteFromWordProblemsSubCategoriesByCategoryId(questionData.getCategoryList());
					implObj.deleteFromWordProblemsQuestionsCategoryId(questionData.getCategoryList());

					implObj.insertIntoWordProblemCategories(questionData.getCategoryList(), grade);
					implObj.insertIntoWordProblemsSubCategories(questionData.getCategoryList());
					implObj.insertIntoWordProblemsQuestions(questionData.getQuestionList());
					implObj.closeConn();
				}

				for(int i = 0 ; i < questionData.getQuestionList().size() ; i ++ ){

					WordProblemQuestionTransferObj questionObj = questionData.getQuestionList().get(i);

					if(questionObj.getQuestion().contains(".png"))
						imageNameList.add(questionObj.getQuestion());
					if(questionObj.getOpt1().contains(".png"))
						imageNameList.add(questionObj.getOpt1());
					if(questionObj.getOpt2().contains(".png"))
						imageNameList.add(questionObj.getOpt2());
					if(questionObj.getOpt3().contains(".png"))
						imageNameList.add(questionObj.getOpt3());
					if(questionObj.getOpt4().contains(".png"))
						imageNameList.add(questionObj.getOpt4());
					if(questionObj.getOpt5().contains(".png"))
						imageNameList.add(questionObj.getOpt5());
					if(questionObj.getOpt6().contains(".png"))
						imageNameList.add(questionObj.getOpt6());
					if(questionObj.getOpt7().contains(".png"))
						imageNameList.add(questionObj.getOpt7());
					if(questionObj.getOpt8().contains(".png"))
						imageNameList.add(questionObj.getOpt8());
					if(!questionObj.getImage().equals(""))
						imageNameList.add(questionObj.getImage());
				}

				if(!schooloCurriculumImpl.isImageTableExist()){
					schooloCurriculumImpl.createSchoolCurriculumImageTable();
				}
				schooloCurriculumImpl.closeConnection();
				//end changes
			}
			return questionData;
		}

		@Override
		protected void onPostExecute(QuestionLoadedTransferObj questionData) {

			pd.cancel();

			if(questionData != null){
				DawnloadImagesFromserver serverObj = new DawnloadImagesFromserver(imageNameList, context);
				Thread imageDawnLoadThrad = new Thread(serverObj);
				imageDawnLoadThrad.start();
				goOnNextScreen(mathEquationDataList);
			}
			else{
				CommonUtils.showInternetDialog(context);
			}

			super.onPostExecute(questionData);
		}
	}
	//end changes
	
	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}
}
