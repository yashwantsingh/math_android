package com.mathfriendzy.controller.timer;

import java.io.Serializable;

/**
 * This timer used for cancel and resume the timer from the current state
 * @author Yashwant Singh
 *
 */
public interface MyTimerInrerface extends Serializable{
	
	 void cancelCurrentTimerAndSetTheResumeTime();
	 void resumeTimerFromTimeSetAtCancelTime();
}
