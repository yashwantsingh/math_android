package com.mathfriendzy.controller.resources;

import com.mathfriendzy.model.resource.ResourceResponse;

/**
 * Created by root on 19/8/15.
 */
public interface ResourceAdapterCallback {
    void onAddToHomework(ResourceResponse resources);
    void onResourceLock(int index);
}
