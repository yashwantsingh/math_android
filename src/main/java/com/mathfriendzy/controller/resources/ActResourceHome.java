package com.mathfriendzy.controller.resources;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.resource.ResourceParam;
import com.mathfriendzy.model.resource.SearchResourceResponse;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;

import java.util.ArrayList;

public class ActResourceHome extends ActBase {

    private final String TAG = this.getClass().getSimpleName();

    private TextView txtBrowseOver = null;
    private TextView txtmadeByour = null;
    private TextView txtsearchtopic = null;
    private TextView txtsubject = null;
    private TextView txtresourceformat = null;

    private EditText edtSearchTopic 	= null;
    private Spinner spSubject			= null;
    private CheckBox chk1				= null;
    private CheckBox chk2				= null;
    private CheckBox chk3				= null;
    private CheckBox chk4				= null;
    private CheckBox chk5				= null;
    private CheckBox chk6				= null;
    private CheckBox chk7				= null;
    private CheckBox chk8				= null;
    private CheckBox chk9				= null;
    private CheckBox chk10				= null;
    private CheckBox chk11				= null;
    private CheckBox chk12				= null;

    private CheckBox chkVideo			= null;
    private CheckBox chkWebpage			= null;
    private CheckBox chkImages			= null;
    private CheckBox chkText			= null;
    private Button   btnSearch 			= null;
    private Button btnGrade             = null;

    //private ArrayList<String> subjectArray = null;
    private String alerMessageSearchTopicRequired = null;
    private String alerMessageGradeRequired = null;

    private final int VIDEO = 1;
    private final int WEBPAGE = 2;
    private final int IMAGES = 3;
    private final int TEXT = 4;
    private int selectedType = 1;//default video

    private String alerMessageNoResultReceived = null;
    private final int PAGE_LIMIT = 20;


    //for Added to open from assign homework
    private boolean isAssignHomework = false;
    private String hwTitle = "";
    private LinearLayout imagesLayout = null;
    private TextView txtHomeworkTitle = null;
    private static ActResourceHome currentObj = null;
    //private String searchTerm = null;
    private ResourceSearchTermParam searchParam = null;

    private RelativeLayout mainLayout = null;
    private boolean isDirectSearchFromCategoryScreen = false;

    //new resources change for khan academy
    private CheckBox chkEnglish = null;
    private CheckBox chkSpanish = null;
    private final int ENGLISH = 1;
    private final int SPANISH = 2;
    private int selectedLanguage = ENGLISH;//default selected language

    //new resource home changes
    private String mathSelectedSubject = "Math";
    private ListView lstLessonCategories = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_resource_home);

        CommonUtils.printLog(TAG , "inside onCreate()");

        this.initializeCurrentObj();
        this.getIntentValues();
        this.init();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.setLayoutVisibility();
        //this.setSubjectAdapter();
        this.setSearhTermAndDisableAllThingsForLessionApps();
        this.getCategories();

        CommonUtils.printLog(TAG , "outside onCreate()");
    }

    private void initializeCurrentObj() {
        currentObj = this;
    }

    public static ActResourceHome getCurrentObj(){
        return currentObj;
    }

    private void getIntentValues() {
        isAssignHomework = this.getIntent().getBooleanExtra("isAssignHomework" , false);
        hwTitle = this.getIntent().getStringExtra("homeworkTitle");
        searchParam = (ResourceSearchTermParam) this.getIntent().getSerializableExtra("searchParam");
    }

    private void init() {
        //subjectArray = new ArrayList<String>();
        if(isAssignHomework) {//if assign homework then only save the default selected language, not to save directly
        // because we are saving it on he ActLesson Category
            MathFriendzyHelper.saveResorceSelectedLanguage(this, selectedLanguage);
        }
    }

    private void setSearhTermAndDisableAllThingsForLessionApps(){
        if(!isAssignHomework) {
            selectedLanguage = searchParam.getSelectedLang();
            edtSearchTopic.setText(searchParam.getSearchTerm());
            if(searchParam.isDirectSearchFromCategoryScreen()){
                isDirectSearchFromCategoryScreen = true;
                this.setResourcesTypeChecked(searchParam.getSelectedResourceFormat());
            }
            this.setAllGradeCheckedUnchecked(true);
            this.clickOnSearch();
        }else{
            mainLayout.setVisibility(RelativeLayout.VISIBLE);
        }
    }

    private void setLayoutVisibility() {
        if(isAssignHomework){
            if(MathFriendzyHelper.isEmpty(hwTitle)) {
                txtHomeworkTitle.setVisibility(TextView.GONE);
            }else {
                txtHomeworkTitle.setVisibility(TextView.VISIBLE);
            }
            imagesLayout.setVisibility(LinearLayout.GONE);
        }else{
            txtHomeworkTitle.setVisibility(TextView.GONE);
            imagesLayout.setVisibility(LinearLayout.GONE);
        }
    }

    @Override
    protected void setWidgetsReferences() {

        CommonUtils.printLog(TAG , "inside setWidgetsReferences()");

        mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);
        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        edtSearchTopic = ( EditText ) findViewById(R.id.edtsearchtopic);
        spSubject = ( Spinner ) findViewById(R.id.spinnerSubject);
        txtBrowseOver = (TextView) findViewById(R.id.txtBrowseOver);
        txtmadeByour = (TextView) findViewById(R.id.txtmadeByour);
        txtsearchtopic = (TextView) findViewById(R.id.txtsearchtopic);
        txtsubject = (TextView) findViewById(R.id.txtsubject);
        txtresourceformat = (TextView) findViewById(R.id.txtresourceformat);

        chk1 = (CheckBox) findViewById(R.id.chk1);
        chk2 = (CheckBox) findViewById(R.id.chk2);
        chk3 = (CheckBox) findViewById(R.id.chk3);
        chk4 = (CheckBox) findViewById(R.id.chk4);
        chk5 = (CheckBox) findViewById(R.id.chk5);
        chk6 = (CheckBox) findViewById(R.id.chk6);
        chk7 = (CheckBox) findViewById(R.id.chk7);
        chk8 = (CheckBox) findViewById(R.id.chk8);
        chk9 = (CheckBox) findViewById(R.id.chk9);
        chk10 = (CheckBox) findViewById(R.id.chk10);
        chk11 = (CheckBox) findViewById(R.id.chk11);
        chk12 = (CheckBox) findViewById(R.id.chk12);
        chkVideo = (CheckBox) findViewById(R.id.chkvideo);
        chkWebpage = (CheckBox) findViewById(R.id.chkwebpage);
        chkImages = (CheckBox) findViewById(R.id.chkimages);
        chkText = (CheckBox) findViewById(R.id.chktext);
        btnSearch = (Button) findViewById(R.id.btnsearch);
        btnGrade = (Button) findViewById(R.id.btnGrade);

        imagesLayout = (LinearLayout) findViewById(R.id.imagesLayout);
        txtHomeworkTitle = (TextView) findViewById(R.id.txtHomeworkTitle);

        //new resources change for khan academy
        chkEnglish = (CheckBox) findViewById(R.id.chkEnglish);
        chkSpanish = (CheckBox) findViewById(R.id.chkSpanish);

        lstLessonCategories = (ListView) findViewById(R.id.lstLessonCategories);
        CommonUtils.printLog(TAG , "outside setWidgetsReferences()");
    }



    @Override
    protected void setTextFromTranslation() {
        CommonUtils.printLog(TAG , "inside setTextFromTranslation()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("navigationTitleResources"));
        txtBrowseOver.setText(transeletion
                .getTranselationTextByTextIdentifier("lblHeaderBrowseAndAddResources"));
        txtsearchtopic.setText(transeletion.getTranselationTextByTextIdentifier("lblSearchForTopic"));
        txtsubject.setText(transeletion.getTranselationTextByTextIdentifier("lblSubjectHeader"));
		/* subjectArray = MathFriendzyHelper.getCommaSepratedOptionInArrayList
                (transeletion.getTranselationTextByTextIdentifier("dataSourceSubjectArray") , ",");*/
        btnGrade.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade"));
        txtresourceformat.setText(transeletion.getTranselationTextByTextIdentifier("lblResourceFormat"));
        btnSearch.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGo"));
        chkVideo.setText(transeletion.getTranselationTextByTextIdentifier("lblResourceFormatVideos"));
        chkWebpage.setText(transeletion.getTranselationTextByTextIdentifier("lblResourceFormatWebPage"));
        chkImages.setText(transeletion.getTranselationTextByTextIdentifier("lblResourceFormatImages"));
        chkText.setText(transeletion.getTranselationTextByTextIdentifier("lblText"));

        alerMessageSearchTopicRequired = transeletion.getTranselationTextByTextIdentifier
                ("alerMessageSearchTopicRequired");
        alerMessageGradeRequired = transeletion.getTranselationTextByTextIdentifier
                ("alerMessageGradeRequired");
        alerMessageNoResultReceived = transeletion.getTranselationTextByTextIdentifier
                ("alerMessageNoResultReceived");

        if(isAssignHomework){
            txtmadeByour.setText(transeletion.getTranselationTextByTextIdentifier
                    ("lblselctedAndAddResourcesToHomework"));
            txtHomeworkTitle.setText(hwTitle);
        }else{
            txtmadeByour.setText(transeletion.getTranselationTextByTextIdentifier("lblSubHeadeCommunities"));
        }
        transeletion.closeConnection();
        CommonUtils.printLog(TAG , "outside setTextFromTranslation()");
    }


    @Override
    protected void setListenerOnWidgets() {

        CommonUtils.printLog(TAG , "inside setListenerOnWidgets()");

        btnSearch.setOnClickListener(this);
        btnGrade.setOnClickListener(this);
        chkVideo.setOnClickListener(this);
        chkImages.setOnClickListener(this);
        chkText.setOnClickListener(this);
        chkWebpage.setOnClickListener(this);

        chkEnglish.setOnClickListener(this);
        chkSpanish.setOnClickListener(this);

        this.setSubjectListener();
        CommonUtils.printLog(TAG , "outside setListenerOnWidgets()");
    }

    private void setSubjectAdapter() {
		/* ArrayAdapter<String> adapter = new ArrayAdapter<String>(this ,
                R.layout.spinner_text_layout , subjectArray);
        spSubject.setAdapter(adapter);*/
    }

    private ResourceParam getResourceParam(){
        ResourceParam resourceParam = new ResourceParam();
        resourceParam.setPageNumber("1");
        resourceParam.setPageSize(PAGE_LIMIT + "");
        resourceParam.setQuery(edtSearchTopic.getText().toString());
        resourceParam.setResourceFormat(this.getResourceFormate());
        if(isDirectSearchFromCategoryScreen){
            resourceParam.setGrade(searchParam.getSelectedGrades());
            resourceParam.setSubjectName(searchParam.getSelectedSubject());
        }else{
            resourceParam.setGrade(this.getGrade());
            resourceParam.setSubjectName(spSubject.getSelectedItem().toString());
        }
        resourceParam.setResourceFormateType(selectedType);
        resourceParam.setSelectedLang(selectedLanguage);
        return resourceParam;
    }

    /**
     * This method check which resources are marked.
     * @return Resource name ( video , text , image , webpage )  or null string.
     */
    private String getResourceFormate() {
        if(chkVideo.isChecked())
            return "video";
        else if(chkText.isChecked())
            return "text";
        else if(chkImages.isChecked())
            return "image";
        else if(chkWebpage.isChecked())
            return "webpage";
        return null;
    }

    /**
     * This method Create a grade .
     * @return all grades .
     */
    private String getGrade() {
        StringBuffer grade = new StringBuffer();
        if(chk1.isChecked()) grade.append("1,");
        if(chk2.isChecked()) grade.append("2,");
        if(chk3.isChecked()) grade.append("3,");
        if(chk4.isChecked()) grade.append("4,");
        if(chk5.isChecked()) grade.append("5,");
        if(chk6.isChecked()) grade.append("6,");
        if(chk7.isChecked()) grade.append("7,");
        if(chk8.isChecked()) grade.append("8,");
        if(chk9.isChecked()) grade.append("9,");
        if(chk10.isChecked()) grade.append("10,");
        if(chk11.isChecked()) grade.append("11,");
        if(chk12.isChecked()) grade.append("12,");

        if(grade.length() != 0)
            // This line delete last char ( , ) from complete grade ."
            grade.delete(grade.length() - 1, grade.length());
        return grade.toString();
    }

    /**
     * Check Grade .
     * @return Any one have grade mark then return true other wise false.
     */
    private boolean isAnyGradeHave() {
        if(chk1.isChecked() )
            return true;
        else if(chk2.isChecked() )
            return true;
        else if(chk3.isChecked() )
            return true;
        else if(chk4.isChecked() )
            return true;
        else if(chk5.isChecked() )
            return true;
        else if(chk6.isChecked() )
            return true;
        else if(chk7.isChecked() )
            return true;
        else if(chk8.isChecked() )
            return true;
        else if(chk9.isChecked() )
            return true;
        else if(chk10.isChecked() )
            return true;
        else if(chk11.isChecked() )
            return true;
        else if(chk12.isChecked() )
            return true;
        return false;
    }

    private boolean checkForSearchValidate(){
        if(MathFriendzyHelper.
                isEmpty(edtSearchTopic.getText().toString())){
            MathFriendzyHelper.showWarningDialog(this , alerMessageSearchTopicRequired);
            return false;
        }

        /*if(!isAnyGradeHave()){
            MathFriendzyHelper.showWarningDialog(this , alerMessageGradeRequired);
            return false;
        }*/
        return true;
    }

    private void clickOnSearch(){

        if(!this.checkForSearchValidate()){
            return;
        }

        if(CommonUtils.isInternetConnectionAvailable(this)) {
            new MyAsyckTask(ServerOperation.CreatePostRequestForGetResources(this.getResourceParam())
                    , null, ServerOperationUtil.GET_SEARCH_RESOURCES_REQUEST, this,
                    new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase,
                                                   int requestCode) {
                            if(requestCode == ServerOperationUtil.GET_SEARCH_RESOURCES_REQUEST) {
                                SearchResourceResponse response = (SearchResourceResponse) httpResponseBase;
                                goFoResourceResultActivity(response);
                            }
                        }
                    }, ServerOperationUtil.SIMPLE_DIALOG, true,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        }else{
            CommonUtils.showInternetDialog(this);
        }
    }

    private void goFoResourceResultActivity(SearchResourceResponse initialResponse){
        if(initialResponse.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)) {
            Intent intent = new Intent(this, ActResourceSearchResult.class);
            intent.putExtra("isAssignHomework", isAssignHomework);
            intent.putExtra("hwTitle" , hwTitle);
            intent.putExtra("searchParam", this.getResourceParam());
            intent.putExtra("initialResponse", initialResponse);
            startActivity(intent);
            finish();
        }else{
            MathFriendzyHelper.showWarningDialog(this , alerMessageNoResultReceived ,
                    new HttpServerRequest() {
                        @Override
                        public void onRequestComplete() {
                            finish();
                        }
                    });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnsearch:
                this.clickOnSearch();
                break;
            case R.id.chkvideo:
                this.setResourcesTypeChecked(VIDEO);
                break;
            case R.id.chkimages:
                this.setResourcesTypeChecked(IMAGES);
                break;
            case R.id.chkwebpage:
                this.setResourcesTypeChecked(WEBPAGE);
                break;
            case R.id.chktext:
                this.setResourcesTypeChecked(TEXT);
                break;
            case R.id.chkEnglish:
                this.selectedLanguage(ENGLISH);
                break;
            case R.id.chkSpanish:
                this.selectedLanguage(SPANISH);
                break;
        }
    }


    /**
     * Set the selected type
     * @param type
     */
    private void setResourcesTypeChecked(int type){
        selectedType = type;
        switch (type) {
            case VIDEO:
                chkVideo.setChecked(true);
                chkImages.setChecked(false);
                chkWebpage.setChecked(false);
                chkText.setChecked(false);
                break;
            case IMAGES:
                chkImages.setChecked(true);
                chkVideo.setChecked(false);
                chkWebpage.setChecked(false);
                chkText.setChecked(false);
                break;
            case WEBPAGE:
                chkWebpage.setChecked(true);
                chkVideo.setChecked(false);
                chkImages.setChecked(false);
                chkText.setChecked(false);
                break;
            case TEXT:
                chkText.setChecked(true);
                chkVideo.setChecked(false);
                chkImages.setChecked(false);
                chkWebpage.setChecked(false);
        }
    }

    public void finishActivity(){
        finish();
    }

    private void setAllGradeCheckedUnchecked(boolean isChecked){
        chk1.setChecked(isChecked);
        chk2.setChecked(isChecked);
        chk3.setChecked(isChecked);
        chk4.setChecked(isChecked);
        chk5.setChecked(isChecked);
        chk6.setChecked(isChecked);
        chk7.setChecked(isChecked);
        chk8.setChecked(isChecked);
        chk9.setChecked(isChecked);
        chk10.setChecked(isChecked);
        chk11.setChecked(isChecked);
        chk12.setChecked(isChecked);
    }

    //new resources change for khan academy
    private void selectedLanguage(int selectedLanguage){
        this.selectedLanguage = selectedLanguage;
        if(selectedLanguage == ENGLISH){
            chkEnglish.setChecked(true);
            chkSpanish.setChecked(false);
        }else if(selectedLanguage == SPANISH){
            chkEnglish.setChecked(false);
            chkSpanish.setChecked(true);
        }
        if(isAssignHomework) {//if assign homework then only save the default selected language, not to save directly
            // because we are saving it on he ActLesson Category
            MathFriendzyHelper.saveResorceSelectedLanguage(this, selectedLanguage);
        }
    }

    private void getCategories(){
        MathFriendzyHelper.getResourceCategories(this , this);
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
        if(requestCode == ServerOperationUtil.GET_RESOURCE_CATEGORIES_REQUEST){
            ResourceCategory resourceCategory = (ResourceCategory) httpResponseBase;
            this.setCategoryAdapter(resourceCategory.getCategoriesList());
            this.setSubjects(resourceCategory.getSubjectList());
        }
    }

    private void setCategoryAdapter(final ArrayList<ResourceCategory> catList) {
        ResourceCategoryAdapter adapter = new ResourceCategoryAdapter
                (this , catList);
        lstLessonCategories.setAdapter(adapter);
        lstLessonCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openResourceSubCatScreen(catList.get(position));
            }
        });
    }

    private void setSubjects(String[] subjectArray) {
        if(subjectArray != null && subjectArray.length > 0){
            ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>
                    (this, R.layout.spinner_country_item, subjectArray);
            countryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spSubject.setAdapter(countryAdapter);
            spSubject.setSelection(countryAdapter.getPosition(mathSelectedSubject));
        }
    }

    private void setSubjectListener(){
        spSubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                String currectSelectedSubject =
                        parent.getItemAtPosition(position).toString();
                if(mathSelectedSubject.equalsIgnoreCase(currectSelectedSubject)){
                    setVisibilityOfLayoutForSelectedSubject(true);
                }else{
                    setVisibilityOfLayoutForSelectedSubject(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setVisibilityOfLayoutForSelectedSubject(boolean isShowList){
        edtSearchTopic.setText("");
        if(isShowList){
            lstLessonCategories.setVisibility(ListView.VISIBLE);
        }else{
            lstLessonCategories.setVisibility(ListView.GONE);
        }
    }

    private void openResourceSubCatScreen(ResourceCategory selectedCat){
        Intent intent = new Intent(this , ActLessonSubCategory.class);
        intent.putExtra("selectedCat" , selectedCat);
        intent.putExtra("isAssignHomework", true);
        intent.putExtra("hwTitle" , hwTitle);
        intent.putExtra("searchParam", this.getResourceParam());
        startActivity(intent);
    }
}
