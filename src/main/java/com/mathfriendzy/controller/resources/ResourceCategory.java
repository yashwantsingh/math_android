package com.mathfriendzy.controller.resources;

import com.mathfriendzy.serveroperation.HttpResponseBase;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by root on 12/1/16.
 */
public class ResourceCategory extends HttpResponseBase implements Serializable{
    private String date;
    private int catId;
    private String catName;
    private String result;
    private ArrayList<ResourceSubCat> subCatList = null;
    private ArrayList<ResourceCategory> categoriesList = null;
    private String[] subjectList = null;

    public String getCatName() {
        return catName;
    }
    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public ArrayList<ResourceSubCat> getSubCatList() {
        return subCatList;
    }

    public void setSubCatList(ArrayList<ResourceSubCat> subCatList) {
        this.subCatList = subCatList;
    }

    public ArrayList<ResourceCategory> getCategoriesList() {
        return categoriesList;
    }

    public void setCategoriesList(ArrayList<ResourceCategory> categoriesList) {
        this.categoriesList = categoriesList;
    }
    public String[] getSubjectList() {
        return subjectList;
    }
    public void setSubjectList(String[] subjectList) {
        this.subjectList = subjectList;
    }
}
