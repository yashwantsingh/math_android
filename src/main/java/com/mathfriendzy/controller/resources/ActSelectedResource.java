package com.mathfriendzy.controller.resources;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.homework.assignhomeworkquiz.ActAssignCustomAns;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.helper.YesNoListenerInterfaceWithCross;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.resource.ResourceParam;
import com.mathfriendzy.model.resource.ResourceResponse;
import com.mathfriendzy.model.resource.SearchResourceResponse;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.utils.CommonUtils;

import java.util.ArrayList;
import java.util.HashMap;

public class ActSelectedResource extends ActBase {

    private final String TAG = this.getClass().getSimpleName();
    private TextView txtAddThemToYourHomework = null;
    private Button btnAgain = null;
    private Button btnAddMore = null;
    private Button btnAddResource = null;
    private ListView lstResourceList = null;

    private ArrayList<ResourceResponse> selectedResourceList = null;
    private ResourceAdapter adapter = null;

    private String hwTitle = "";
    private TextView txtHomeworkTitle = null;
    private String alertWouldYouLikeToAddTheseResources =
            "Would you like to add these resources to your homework?";
    private String yesText = null;
    private String noText = null;
    private boolean isAlreadyResourceSelected = false;

    private ResourceParam searchParam = null;
    private HashMap<String , SearchResourceResponse> dataList = null;
    private HashMap<String , ArrayList<ResourceResponse>> selectedListResources = null;
    private boolean isDirectOpenWithoutSearch = false;
    private boolean isNotShowResourceButton = false;

    //for student side
    private boolean isOpenForViewResources = false;
    private RelativeLayout titleLayout = null;
    private RelativeLayout titleLayoutForViewResources = null;
    private TextView txtViewResourcesTitle = null;

    private boolean isAnyThingDeleted = false;


    private final String VIDEO_TEXT = "video";
    private final String WEBPAGE_TEXT = "webpage";
    private final String IMAGE_TEXT = "image";
    private final String TEXT_TEXT = "text";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_selected_resource);

        CommonUtils.printLog(TAG , "inside onCreate()");

        this.getIntentValues();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.setListAdapter();
        this.setResourceButtonVisibility();
        this.setVisibilityTitleLayout();

        CommonUtils.printLog(TAG , "outside onCreate()");
    }

    private void setVisibilityTitleLayout() {
        if(isOpenForViewResources){
            titleLayout.setVisibility(RelativeLayout.GONE);
            titleLayoutForViewResources.setVisibility(RelativeLayout.VISIBLE);
        }else{
            titleLayout.setVisibility(RelativeLayout.VISIBLE);
            titleLayoutForViewResources.setVisibility(RelativeLayout.GONE);
        }
    }

    private void getIntentValues() {
        selectedResourceList = (ArrayList<ResourceResponse>) this.getIntent()
                .getSerializableExtra("selectedResourceList");
        hwTitle = this.getIntent().getStringExtra("hwTitle");
        searchParam = (ResourceParam) this.getIntent().getSerializableExtra("searchParam");
        dataList = (HashMap<String, SearchResourceResponse>)
                this.getIntent().getSerializableExtra("dataList");
        selectedListResources = (HashMap<String, ArrayList<ResourceResponse>>)
                this.getIntent().getSerializableExtra("selectedListResources");
        isDirectOpenWithoutSearch = this.getIntent().getBooleanExtra("isDirectOpenWithoutSearch" ,
                false);
        isNotShowResourceButton = this.getIntent().getBooleanExtra("isNotShowResourceButton" , false);
        isOpenForViewResources = this.getIntent().getBooleanExtra("isOpenForViewResources" , false);

        try {
            //this is added because teacher assign hw with resources and after that logout and login
            // with the student and go to view resources then this will create the problem
            if(isOpenForViewResources){
                return ;
            }

            if ((!isNotShowResourceButton) && ActAssignCustomAns.getCurrentObj().getSelectedResourceList() != null) {
                ArrayList<ResourceResponse> list = ActAssignCustomAns.getCurrentObj().getSelectedResourceList();
                ArrayList<ResourceResponse> tempSelectedList = new ArrayList<ResourceResponse>(selectedResourceList);
                for(ResourceResponse resourceResponse : list){
                    if(!this.isAlreadySelectedResources(resourceResponse , tempSelectedList)){
                        selectedResourceList.add(resourceResponse);
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private boolean isAlreadySelectedResources(ResourceResponse resourceResponse ,
                                               ArrayList<ResourceResponse> tempSelectedList){
        for(ResourceResponse resourceResponse1 : tempSelectedList){
            if(resourceResponse.getTitle().equalsIgnoreCase(resourceResponse1.getTitle())
                    && (resourceResponse.getResourceUrl()
                    .equalsIgnoreCase(resourceResponse1.getResourceUrl()))){
                return true;
            }
        }
        return false;
    }

    @Override
    protected void setWidgetsReferences() {

        CommonUtils.printLog(TAG , "inside setWidgetsReferences()");

        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        btnAgain = (Button) findViewById(R.id.btnAgain);
        btnAddMore = (Button) findViewById(R.id.btnAddMore);
        btnAddResource = (Button) findViewById(R.id.btnAddResource);
        lstResourceList = (ListView) findViewById(R.id.lstResourceList);
        txtAddThemToYourHomework = (TextView) findViewById(R.id.txtAddThemToYourHomework);
        txtHomeworkTitle = (TextView) findViewById(R.id.txtHomeworkTitle);
        titleLayout = (RelativeLayout) findViewById(R.id.titleLayout);
        titleLayoutForViewResources = (RelativeLayout) findViewById(R.id.titleLayoutForViewResources);
        txtViewResourcesTitle = (TextView) findViewById(R.id.txtViewResourcesTitle);

        CommonUtils.printLog(TAG , "outside setWidgetsReferences()");
    }

    @Override
    protected void setListenerOnWidgets() {
        CommonUtils.printLog(TAG , "inside setListenerOnWidgets()");

        btnAgain.setOnClickListener(this);
        btnAddMore.setOnClickListener(this);
        btnAddResource.setOnClickListener(this);

        CommonUtils.printLog(TAG, "outside setListenerOnWidgets()");
    }

    @Override
    protected void setTextFromTranslation() {
        CommonUtils.printLog(TAG , "inside setTextFromTranslation()");
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        if(isOpenForViewResources){
            txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("btnViewResources"));
        }else {
            txtTopbar.setText(transeletion.
                    getTranselationTextByTextIdentifier("navigationTitleResources"));
        }
        txtAddThemToYourHomework.setText(transeletion
                .getTranselationTextByTextIdentifier("lblselctedAndAddResourcesToHomework"));
        btnAgain.setText(transeletion.getTranselationTextByTextIdentifier("btnSearchAgain"));
        btnAddMore.setText(transeletion.getTranselationTextByTextIdentifier("lblAddMore"));
        btnAddResource.setText(transeletion.getTranselationTextByTextIdentifier("btnAddResources"));
        txtHomeworkTitle.setText(hwTitle);
        txtViewResourcesTitle.setText(transeletion.getTranselationTextByTextIdentifier("lblHomework")
        + ": " + hwTitle);
        alertWouldYouLikeToAddTheseResources = transeletion.
                getTranselationTextByTextIdentifier("alertWouldYouLikeToAddTheseResources");
        yesText = transeletion.getTranselationTextByTextIdentifier("btnTitleYes");
        noText = transeletion.getTranselationTextByTextIdentifier("lblNo");
        transeletion.closeConnection();
        CommonUtils.printLog(TAG , "outside setTextFromTranslation()");
    }

    private void setResourceButtonVisibility(){
        if(isNotShowResourceButton){
            btnAddResource.setVisibility(Button.INVISIBLE);
        }else{
            btnAddResource.setVisibility(Button.VISIBLE);
        }
    }

    private void setListAdapter() {
        if(adapter == null && selectedResourceList != null && selectedResourceList.size() > 0){
            adapter = new ResourceAdapter(this , selectedResourceList , false ,
                    new ResourceAdapterCallback() {
                        @Override
                        public void onAddToHomework(ResourceResponse resources) {

                        }

                        @Override
                        public void onResourceLock(int index) {//work as delete when user click on delete then this method call from the adapter only for this screen
                            isAnyThingDeleted = true;
                            ResourceResponse resources = selectedResourceList.get(index);
                            deleteFromHashMapAndUpdateMainDataListMap(resources);
                            //delete from the current list from the adapter to see adapter deleteAndNotify() method
                        }
                    } , 0 , true);
            if(isOpenForViewResources){
                adapter.setDeleteVisibility(false);
            }else {
                adapter.setDeleteVisibility(true);
            }
            lstResourceList.setAdapter(adapter);
            //adapter.notifyDataSetChanged();
        }
    }

    /**
     * Delete the selected resource from the HashMap to do not show the selected add to homework when click on add more
     * @param resources
     */
    private void deleteFromHashMapAndUpdateMainDataListMap(ResourceResponse resources){
        try {
            if (selectedListResources != null && selectedListResources.size() > 0) {
                ArrayList<ResourceResponse> list = selectedListResources.get(resources.getValue());
                if(list != null && list.size() > 0){
                    for(int i = 0 ; i < list.size() ; i ++ ){
                        ResourceResponse resourceResponse = list.get(i);
                        if(resourceResponse.getTitle().equalsIgnoreCase(resources.getTitle())
                                && resourceResponse.getResourceUrl().equalsIgnoreCase(resources.getResourceUrl())){
                            list.remove(i);
                            break;
                        }
                    }
                }
                selectedListResources.put(resources.getValue() , list);

                String key = resources.getValue() + "_" + (resources.getPageNumber() + 1);
                SearchResourceResponse searchResponse = dataList.get(key);
                ArrayList<ResourceResponse> resourceList = searchResponse.getListOfresource();
                for(int i = 0 ; i < resourceList.size() ; i ++ ){
                    ResourceResponse resourceResponse = resourceList.get(i);
                    if(resourceResponse.getTitle().equalsIgnoreCase(resources.getTitle())
                            && resourceResponse.getResourceUrl().equalsIgnoreCase(resources.getResourceUrl())){
                        resourceList.get(i).setAddedToHomework(false);
                        break;
                    }
                }
                searchResponse.setListOfresource(resourceList);
                dataList.put(key , searchResponse);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {

    }

    private void clickOnAddMore(){
        if(searchParam != null && dataList != null && selectedListResources != null) {
            Intent intent = new Intent(this, ActResourceSearchResult.class);
            intent.putExtra("isAssignHomework", true);
            intent.putExtra("homeworkTitle", hwTitle);
            intent.putExtra("isAddMoreResource", true);
            intent.putExtra("searchParam", searchParam);
            intent.putExtra("dataList", dataList);
            intent.putExtra("selectedListResources", selectedListResources);
            startActivity(intent);
            finish();
        }else{
            this.clickOnSearchAgain();
        }
    }

    private void clickOnAddResources(){
        openActAssignCustomAnsScreen(selectedResourceList);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnAgain:
                this.clickOnSearchAgain();
                break;
            case R.id.btnAddMore:
                this.clickOnAddMore();
                break;
            case R.id.btnAddResource:
                this.clickOnAddResources();
                break;
        }
    }

    private void showAddResourcesChoiceDialog(){
        if(isAnyThingDeleted){
            if(selectedResourceList != null && selectedResourceList.size() > 0){
                openActAssignCustomAnsScreen(selectedResourceList);
            }else{
                openActAssignCustomAnsScreen(null);
            }
            return ;
        }

        if(isNotShowResourceButton || isOpenForViewResources){
            finish();
            return ;
        }

        MathFriendzyHelper.yesNoConfirmationDialogWithCancelButton
                (this, alertWouldYouLikeToAddTheseResources,
                        yesText, noText,
                        new YesNoListenerInterfaceWithCross() {
                            @Override
                            public void onYes() {
                                openActAssignCustomAnsScreen(selectedResourceList);
                            }

                            @Override
                            public void onNo() {
                                openActAssignCustomAnsScreen(null);
                            }

                            @Override
                            public void onCross() {

                            }
                        });
    }

    private void clickOnSearchAgain(){
        /*ActAssignCustomAns.getCurrentObj()
                .setSelectedResources(null , null , null , null);*/
        try {
            ActAssignCustomAns.getCurrentObj()
                    .setSelectedResources(selectedResourceList , searchParam , dataList , selectedListResources);
            ActResourceHome.getCurrentObj().finishActivity();
        }catch(Exception e){
            e.printStackTrace();
        }

        Intent intent = new Intent(this, ActResourceHome.class);
        intent.putExtra("isAssignHomework", true);
        intent.putExtra("homeworkTitle", hwTitle);
        startActivity(intent);
        finish();
    }

    private void openActAssignCustomAnsScreen
            (ArrayList<ResourceResponse> selectedResourceList){
        ActAssignCustomAns.getCurrentObj()
                .setSelectedResources(selectedResourceList , searchParam , dataList , selectedListResources);
        if(!isAlreadyResourceSelected){
            ActResourceHome.getCurrentObj().finishActivity();
        }
        finish();
    }

    @Override
    public void onBackPressed() {
        this.showAddResourcesChoiceDialog();
    }
}
