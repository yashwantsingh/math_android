package com.mathfriendzy.controller.coinsdistribution;

import static com.mathfriendzy.utils.ICommonUtils.COINS_DISTRIBUTION_FLAG;

import java.util.ArrayList;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.controller.inapp.GetMoreCoins;
import com.mathfriendzy.model.coinsdistribution.CoinsDistributionServerOperation;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
/**
 * This screen is open when user distribute the coins to your players
 * @author Yashwant Singh
 *
 */
public class CoinsDistributionActivity extends ActBaseClass
{
	private TextView labelTop 	  = null;
	private TextView txtYourCoins = null;

	private TextView lblEnterAnAmountToBeTransferd = null;
	private TextView lblYouhavePurchasedOrRedeemed = null;

	private EditText edtEnterCoins 	= null;
	private ListView lstPlayer 		= null;
	private Button   btnNoThanks 	= null;

	private ArrayList<UserPlayerDto> userPlayerList = null;
	private int userCoins = 0;
	private RegistereUserDto regUserObj = null;

	private final String TAG = this.getClass().getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_coins_distribution);

		if(COINS_DISTRIBUTION_FLAG)
			Log.e(TAG, "inside onCreate()");

		this.getUserData();
		this.setWidgetsReferences();
		this.setTextFromTranslation();
		this.setUserPlayerDataToAdapter();
		this.setListenerOnWidgets();

		if(COINS_DISTRIBUTION_FLAG)
			Log.e(TAG, "outside onCreate()");
	}


	/**
	 * This method get userDataFrom the database
	 */
	private void getUserData() 
	{
		UserRegistrationOperation userObj = new UserRegistrationOperation(this);
		regUserObj = userObj.getUserData();

		if(regUserObj.getCoins().length() > 0)
			userCoins = Integer.parseInt(regUserObj.getCoins());
	}


	/**
	 * This method set the widgets references from layout 
	 */
	private void setWidgetsReferences()
	{
		if(COINS_DISTRIBUTION_FLAG)
			Log.e(TAG, "inside setWidgetsReferences()");

		labelTop 						= (TextView) findViewById(R.id.labelTop);
		txtYourCoins					= (TextView) findViewById(R.id.txtYourCoins);
		lblEnterAnAmountToBeTransferd 	= (TextView) findViewById(R.id.lblEnterAnAmountToBeTransferd);
		lblYouhavePurchasedOrRedeemed 	= (TextView) findViewById(R.id.lblYouhavePurchasedOrRedeemed);

		edtEnterCoins 					= (EditText) findViewById(R.id.edtEnterCoins);
		lstPlayer 						= (ListView) findViewById(R.id.lstPlayer);
		btnNoThanks 					= (Button) findViewById(R.id.btnNoThanks);

		if(COINS_DISTRIBUTION_FLAG)
			Log.e(TAG, "outside setWidgetsReferences()");
	}

	/**
	 * This method set the translation text from translation table
	 */
	private void setTextFromTranslation()
	{
		if(COINS_DISTRIBUTION_FLAG)
			Log.e(TAG, "inside setTextFromTranslation()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		labelTop.setText(transeletion.getTranselationTextByTextIdentifier("mfTitleHomeScreen"));
		txtYourCoins.setText(CommonUtils.setNumberString(userCoins + "") + " " + transeletion.getTranselationTextByTextIdentifier("lblCoins"));
		lblEnterAnAmountToBeTransferd.setText(transeletion.getTranselationTextByTextIdentifier("lblEnterAnAmountToBeTransferd"));
		lblYouhavePurchasedOrRedeemed.setText(transeletion.getTranselationTextByTextIdentifier("lblYouhavePurchasedOrRedeemed"));
		//btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks") + "!");
		btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleProceed"));

		transeletion.closeConnection();

		if(COINS_DISTRIBUTION_FLAG)
			Log.e(TAG, "outside setTextFromTranslation()");
	}

	/**
	 * This method set the userPlayer data to the list
	 */
	private void setUserPlayerDataToAdapter() 
	{
		if(COINS_DISTRIBUTION_FLAG)
			Log.e(TAG, "inside setUserPlayerDataToAdapter()");

		UserPlayerOperation userPlayerOpr = new UserPlayerOperation(this);
		userPlayerList = userPlayerOpr.getUserPlayerData();

		CoinsDistributionUserPlayerAdapter adapter = new CoinsDistributionUserPlayerAdapter(this,R.layout.coins_distribution_player_list_layout,userPlayerList);
		lstPlayer.setAdapter(adapter);

		if(COINS_DISTRIBUTION_FLAG)
			Log.e(TAG, "outside setUserPlayerDataToAdapter()");
	}

	/**
	 * This method set the updated coins
	 */
	private void setUpdatedCoins()
	{
		if(COINS_DISTRIBUTION_FLAG)
			Log.e(TAG, "inside setUpdatedCoins()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		txtYourCoins.setText(CommonUtils.setNumberString(userCoins + "") + " " + transeletion.getTranselationTextByTextIdentifier("lblCoins"));
		transeletion.closeConnection();

		if(COINS_DISTRIBUTION_FLAG)
			Log.e(TAG, "outside setUpdatedCoins()");
	}


	/**
	 * This method setListener on widgets
	 */
	private void setListenerOnWidgets()
	{
		if(COINS_DISTRIBUTION_FLAG)
			Log.e(TAG, "inside setListenerOnWidgets()");

		lstPlayer.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position,long id) 
			{
				DialogGenerator dg = new DialogGenerator(CoinsDistributionActivity.this);
				Translation transeletion = new Translation(CoinsDistributionActivity.this);
				transeletion.openConnection();

				if(edtEnterCoins.getText().toString().length() == 0)
				{
					edtEnterCoins.setText("0");
					dg.generateWarningDialogForEmptyCoisTransfer(transeletion.getTranselationTextByTextIdentifier("alertMsgPleaseEnterAnAmountOfCoinsYouWouldLikeToTransfer"));
				}
				else if(edtEnterCoins.getText().toString().equals("0"))
				{
					dg.generateWarningDialogForEmptyCoisTransfer(transeletion.getTranselationTextByTextIdentifier("alertMsgPleaseEnterAnAmountOfCoinsYouWouldLikeToTransfer"));
				}
				else if(Integer.parseInt(edtEnterCoins.getText().toString()) > userCoins)
				{
					int transferCoins = Integer.parseInt(edtEnterCoins.getText().toString());
					edtEnterCoins.setText(transferCoins + "");

					dg.generateDialogUserEnterGraterCoinsThenAvalableCoins(transeletion.getTranselationTextByTextIdentifier("alertMsgSorryYouDoNotHaveThatManyCoins") , userCoins);
				}
				else
				{					
					int transferCoins = Integer.parseInt(edtEnterCoins.getText().toString());

					edtEnterCoins.setText(transferCoins + "");
					
					if(transferCoins != 0)
					{
						userCoins = userCoins - transferCoins;
						userPlayerList.get(position).setCoin((Integer.parseInt(userPlayerList.get(position).getCoin())
								+ transferCoins) + "");

						setUpdatedCoins();

						//set updated data to adapter
						CoinsDistributionUserPlayerAdapter adapter = new CoinsDistributionUserPlayerAdapter(CoinsDistributionActivity.this,R.layout.coins_distribution_player_list_layout,userPlayerList);
						lstPlayer.setAdapter(adapter);

						if(userCoins == 0)
						{
							dg.generateDialogUserEnterGraterCoinsThenAvalableCoins(transeletion.getTranselationTextByTextIdentifier("lblCongratulations") + " \n"
									+ transeletion.getTranselationTextByTextIdentifier("alertMsgYouHaveSuccessfullyTransferdAllOfYourCoins") , userCoins);
						}
						else
						{
							dg.generateWarningDialogForEmptyCoisTransfer(transeletion.getTranselationTextByTextIdentifier("lblCongratulations") + "\n" 
									+ CommonUtils.setNumberString(transferCoins + "") + " "+ transeletion.getTranselationTextByTextIdentifier("alertMsgCoinsWereTransferedto") + "\n" 
									+ userPlayerList.get(position).getFirstname() + " " + userPlayerList.get(position).getLastname() + "!");
						}
												
						//changes the No Thanks text into Proceed , changes according to sheet Dec30
						edtEnterCoins.setText("");
						btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleProceed"));
						//end changes
						updateCoinsInLocalDatabaseAndServer(transferCoins , userCoins , Integer.parseInt(userPlayerList.get(position).getCoin()) , userPlayerList.get(position).getPlayerid());
					}
					else
					{
						dg.generateWarningDialogForEmptyCoisTransfer(transeletion.getTranselationTextByTextIdentifier("alertMsgPleaseEnterAnAmountOfCoinsYouWouldLikeToTransfer"));
					}
				}

				transeletion.closeConnection();
			}
		});


		btnNoThanks.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try{
					if(edtEnterCoins.getText().toString().length() > 0 &&
							Integer.parseInt(edtEnterCoins.getText().toString()) != 0){
						DialogGenerator dg = new DialogGenerator(CoinsDistributionActivity.this);
						Translation transeletion = new Translation(CoinsDistributionActivity.this);
						transeletion.openConnection();
						dg.generateWarningDialog(transeletion.
								getTranselationTextByTextIdentifier("lblPleaseSelectAPlayerToAssignCoins"));
						transeletion.closeConnection();
					}else{
						finish();
					}
				}catch(Exception e){
					Log.e(TAG, "Error while converting String to Integer " + e.toString());
					finish();
				}
			}
		});

		if(COINS_DISTRIBUTION_FLAG)
			Log.e(TAG, "outside setListenerOnWidgets()");
	}

	/**
	 * This method update coins in local database and server
	 */
	private void updateCoinsInLocalDatabaseAndServer(int transferCoins , int userCoins , int playerCoins , String playerId)
	{		
		if(COINS_DISTRIBUTION_FLAG)
			Log.e(TAG, "inside updateCoinsInLocalDatabaseAndServer()");

		UserRegistrationOperation userRegOpr = new UserRegistrationOperation(this);
		userRegOpr.updaUserCoins(userCoins, regUserObj.getUserId());
		userRegOpr.closeConn();

		UserPlayerOperation userPlayerOpr = new UserPlayerOperation(this);
		userPlayerOpr.updataPlayerCoins(playerCoins, playerId);
		userPlayerOpr.closeConn();

		LearningCenterimpl learningCenterImpl = new LearningCenterimpl(this);
		learningCenterImpl.openConn();
		learningCenterImpl.updateCoinsForPlayer(playerCoins, regUserObj.getUserId(), playerId);
		learningCenterImpl.closeConn();

		if(CommonUtils.isInternetConnectionAvailable(this))
		{
			new UpdateCoinsOnServer(regUserObj.getUserId(), userCoins, playerId, playerCoins).execute(null,null,null);
		}

		if(COINS_DISTRIBUTION_FLAG)
			Log.e(TAG, "outside updateCoinsInLocalDatabaseAndServer()");
	}


	@Override
	public void onBackPressed() {
		GetMoreCoins.isFromEditRegisteredUserPlayer = false;
		super.onBackPressed();
	}


	/**
	 * Update coins on server
	 * @author Yashwant Singh
	 *
	 */
	class UpdateCoinsOnServer extends AsyncTask<Void, Void, Void>
	{
		private String userId = null;
		private String playerId = null;
		private int userCoins = 0;
		private int playerCoins = 0;

		UpdateCoinsOnServer(String userId , int userCoins , String playerId , int playerCoins)
		{
			this.userId = userId;
			this.playerId = playerId;
			this.playerCoins = playerCoins;
			this.userCoins = userCoins;
		}
		@Override
		protected void onPreExecute() 
		{
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			if(COINS_DISTRIBUTION_FLAG)
				Log.e(TAG, "inside UpdateCoinsOnServer doInBackground()");

			CoinsDistributionServerOperation coinsUpdate = new CoinsDistributionServerOperation();
			coinsUpdate.updateCoinsOnServerForUserAndPlayer(userId, playerId, userCoins, playerCoins);

			if(COINS_DISTRIBUTION_FLAG)
				Log.e(TAG, "outside UpdateCoinsOnServer doInBackground()");

			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			super.onPostExecute(result);
		}
	}
	
	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}
}
