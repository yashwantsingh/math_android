package com.mathfriendzy.controller.coinsdistribution;

import static com.mathfriendzy.utils.ICommonUtils.FACEBOOK_HOST_NAME;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import com.mathfriendzy.R;
import com.mathfriendzy.model.chooseAvtar.ChooseAvtarOpration;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.utils.CommonUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CoinsDistributionUserPlayerAdapter extends BaseAdapter
{

	private Context context 				   = null;
	private int resource 					   = 0;
	private ViewHolder vholder 				   = null;
	private ArrayList<UserPlayerDto>  userPlayerList	= null; 
	private LayoutInflater mInflater 		   = null;
	private Bitmap profileImageBitmap 		   = null;
	
	
	CoinsDistributionUserPlayerAdapter(Context context,  int resourc , ArrayList<UserPlayerDto> userPlayerList)
	{
		mInflater 		= LayoutInflater.from(context);
		
		this.context = context;
		this.resource = resourc;
		this.userPlayerList = userPlayerList;
	}
	
	@Override
	public int getCount()
	{
		return userPlayerList.size();
	}

	@Override
	public Object getItem(int position) 
	{
		return null;
	}

	@Override
	public long getItemId(int position) 
	{
		return 0;
	}

	@Override
	public View getView(int index, View view, ViewGroup parent) 
	{		
		if(view == null)
		{
			vholder = new ViewHolder();
			view = mInflater.inflate(resource, null);
			vholder.imgPlayer = (ImageView) view.findViewById(R.id.imgSmiley);		
			vholder.txtAddress = (TextView) view.findViewById(R.id.txtPlayerAddress);
			vholder.txtCoins  = (TextView) view.findViewById(R.id.txtPlayerCoins);
			vholder.txtPlayerName = (TextView) view.findViewById(R.id.txtPlayerName);			
			view.setTag(vholder);
		}
		else
		{
			vholder = (ViewHolder) view.getTag();
		}
			
		
		UserRegistrationOperation userObj = new UserRegistrationOperation(context);
		RegistereUserDto regUserObj = userObj.getUserData();
		
		setImage(vholder.imgPlayer, userPlayerList.get(index).getImageName());
		
		vholder.txtAddress.setText(regUserObj.getCity() + " " + regUserObj.getState());
		vholder.txtPlayerName.setText(userPlayerList.get(index).getFirstname() + " " + userPlayerList.get(index).getLastname());
		
		Translation transeletion = new Translation(context);
		transeletion.openConnection();
		vholder.txtCoins.setText(CommonUtils.setNumberString(userPlayerList.get(index).getCoin()) + " " + transeletion.getTranselationTextByTextIdentifier("lblCoins"));
		transeletion.closeConnection();
		return view;
	}
	
	/**
	 * static class for view which hole the static objects of views
	 * @author Yashwant Singh
	 *
	 */
	static class ViewHolder
	{
		ImageView imgPlayer;
		TextView  txtAddress;
		TextView  txtCoins;
		TextView  txtPlayerName;
		RelativeLayout problemLayout;
	}
	
	/**
	 * This method set player image
	 */
	private void setImage(ImageView imgPlayer , String imageName)
	{
		try
		{
			Long.parseLong(imageName);
			//changes for Internet Connection
			if(CommonUtils.isInternetConnectionAvailable(context))
			{
				String strUrl = FACEBOOK_HOST_NAME + imageName + "/picture?type=large";
					new FacebookImageLoaderTask(strUrl , imgPlayer).execute(null,null,null);
			}
			else
			{
				imgPlayer.setBackgroundResource(R.drawable.smiley);
			}
		}
		catch(NumberFormatException ee)
		{
			ChooseAvtarOpration chooseAvtarObj = new ChooseAvtarOpration();
			chooseAvtarObj.openConn(context);
			if(chooseAvtarObj.getAvtarImageByName(imageName) != null)
			{
				/*profileImageBitmap = CommonUtils.getBitmapFromByte(chooseAvtarObj.getAvtarImageByName(imageName) , context);*/
				profileImageBitmap = CommonUtils.getBitmapFromByte(chooseAvtarObj.getAvtarImageByName(imageName));
				imgPlayer.setImageBitmap(profileImageBitmap);
			}
			chooseAvtarObj.closeConn();
	    }
	}
	
	/**
	 * This asyncTask set image from facebook url to the Button 
	 * @author Yashwant Singh
	 *
	 */
	class FacebookImageLoaderTask extends AsyncTask<Void, Void, Void>
	{
		private String strUrl = null;
		private ImageView imgPlayer = null;
		public FacebookImageLoaderTask(String strUrl , ImageView imgPlayer)
		{
			this.strUrl = strUrl;
			this.imgPlayer = imgPlayer;
		}

		@Override
		protected Void doInBackground(Void... params) 
		{				
			URL img_value;
			try 
			{
				img_value = new URL(strUrl);
				profileImageBitmap = BitmapFactory.decodeStream(img_value.openConnection().getInputStream());
			} 
			catch (MalformedURLException e) 
			{			
				e.printStackTrace();
				Log.e("EditActivity", "Problem in setting image" + e);
			}
			catch(Exception ee)
			{
				Log.e("EditActivity", "Problem in setting image" + ee);
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) 
		{
			imgPlayer.setImageBitmap(profileImageBitmap);
			imgPlayer.invalidate();
			super.onPostExecute(result);
		}
	}
}
