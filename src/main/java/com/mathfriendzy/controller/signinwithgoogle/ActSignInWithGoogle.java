package com.mathfriendzy.controller.signinwithgoogle;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.registration.RegistrationStep1;
import com.mathfriendzy.helper.ChoiceSelectionListener;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.OnRequestCompleteWithStringResult;
import com.mathfriendzy.model.login.Login;
import com.mathfriendzy.model.player.temp.TempPlayer;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.social.google.GoogleConnect;
import com.mathfriendzy.social.google.GoogleProfile;
import com.mathfriendzy.social.google.MyGoogleCallBack;
import com.mathfriendzy.social.google.UserEmailFetcher;
import com.mathfriendzy.utils.CommonUtils;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class ActSignInWithGoogle extends ActBase implements MyGoogleCallBack{

    private final String TAG = this.getClass().getSimpleName();
    private GoogleConnect googleConnect = null;
    private final int HALF_SECONDS = 500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_sign_in_with_google);

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside onCreate()");


		/*googleConnect = new GoogleConnect(this);
		googleConnect.initializeGoogleClient();*/
		MathFriendzyHelper.initializeProcessDialog(this);
        this.getAllEmail();

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside onCreate()");
    }

    @Override
    protected void onStart() {
        //googleConnect.onStart();
        //this.signInWithGoogle();
        super.onStart();
    }

    @Override
    protected void onStop() {
        //googleConnect.signOut();
        super.onStop();
    }

    /**
     * Sign in with google
     */
    private void signInWithGoogle(){
		/*final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				googleConnect.signIn();
			}
		}, HALF_SECONDS);*/
    }

    @Override
    protected void onResume() {
        this.signInWithGoogle();
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * Retrieve All the login email
     */
    private void getAllEmail(){
        try {
            final CharSequence[] allEmail = UserEmailFetcher.getAllLoginEmail(this);
            if (allEmail.length == 0) {
                //if(true){
                //no email account addedd
                Toast.makeText(this, "No Account Added , Please add an account.", Toast.LENGTH_SHORT).show();
                finish();
            } else if (allEmail.length == 1) {
                CharSequence email = allEmail[0];
                this.setProfileData(email.toString());
            } else {
                MathFriendzyHelper.showEmailChooserDialog(this, "Ok", "Cancel", "Choose an account",
                        "", allEmail, new ChoiceSelectionListener() {
                            @Override
                            public void onCancel(int selectionIndex) {
                                finish();
                            }

                            @Override
                            public void onOk(int selectionIndex) {
                                setProfileData(allEmail[selectionIndex].toString());
                            }
                        });
            }
        }catch(Exception e){
            Toast.makeText(this, "Error to " +
                    "", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Set profile data
     * @param email
     */
    private void setProfileData(String email){
        GoogleProfile profile = new GoogleProfile();
        profile.setEmail(email.toString());
        profile.setfName("First Name");
        profile.setlName("Last Name");
        try{
            //create user name from email
            profile.setUserName(email.split("@")[0]);
        }catch(Exception e){
            e.printStackTrace();
        }
        this.onProfileData(profile);
    }

    @Override
    public void onProfileData(final GoogleProfile profile) {
        MathFriendzyHelper.showDialog();
        MathFriendzyHelper.loginWithGoogle(this, profile.getEmail(),
                new OnRequestCompleteWithStringResult() {
                    @Override
                    public void onComplete(String result) {
                        int resultValue = Integer.parseInt(result);
                        if (resultValue == Login.SUCCESS) {
                            callAfterResultSuccess();
                        } else {
                            openRegistrationScreen(profile);
                        }
                    }
                });
    }

    /**
     * Open the registration screen when user is not registered before
     * @param profile
     */
    private void openRegistrationScreen(GoogleProfile profile){
        Intent intent = new Intent(this , RegistrationStep1.class);
        intent.putExtra("isRegisterFromGoogle", true);
        intent.putExtra("googleProfileData", profile);
        startActivity(intent);
    }

    /**
     * Call after login success
     */
    private void callAfterResultSuccess(){
        MathFriendzyHelper.saveUserLogin(this, true);
        MathFriendzyHelper.addUserOnServerWithAndroidDevice(this);

        if(CommonUtils.isInternetConnectionAvailable(this)){
            String userId = CommonUtils.getUserId(this);
            if(userId != null && userId.length() > 0){
                MathFriendzyHelper.getPurchasedItemDetail(this , userId);
                MathFriendzyHelper.getPurchasedAvater(userId, this);
            }
        }

        if(MathFriendzyHelper.isTempraroryPlayerCreated(this)){
            TempPlayer tempPlayer = MathFriendzyHelper.getTempPlayer(this);
            if(tempPlayer != null){
                MathFriendzyHelper.deleteTempPlayerRecord(this);
            }//changes require , may be need to do later on according to the login
        }else{
            MathFriendzyHelper.createBlankTempTable(this);
        }

        MathFriendzyHelper.setCheckPlayer(this);
        MathFriendzyHelper.dismissDialog();
        this.openMainActivity();
    }

    /**
     * Open main screen after login success
     */
    private void openMainActivity(){
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void setWidgetsReferences() {
        // TODO Auto-generated method stub

    }

    @Override
    protected void setListenerOnWidgets() {
        // TODO Auto-generated method stub

    }

    @Override
    protected void setTextFromTranslation() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase,
                               int requestCode) {
        // TODO Auto-generated method stub
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        try{
            googleConnect.onActivityResultCall(requestCode, resultCode, intent);
        }catch(Exception e){
            e.printStackTrace();
        }
        super.onActivityResult(requestCode, resultCode, intent);
    }
}
