package com.mathfriendzy.controller.main;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.ads.GetAdsFrequencies;
import com.mathfriendzy.controller.assessmenttest.AssessmentClass;
import com.mathfriendzy.controller.assessmenttest.report.ReportForPracticeAndAssessment;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.controller.friendzy.StudentChallengeActivity;
import com.mathfriendzy.controller.funwithmath.ActFunWithMath;
import com.mathfriendzy.controller.help.ActHelp;
import com.mathfriendzy.controller.helpastudent.ActHelpAStudent;
import com.mathfriendzy.controller.homework.ActHomeWork;
import com.mathfriendzy.controller.learningcenter.NewLearnignCenter;
import com.mathfriendzy.controller.multifriendzy.MultiFriendzyMain;
import com.mathfriendzy.controller.player.CreateTeacherPlayerActivity;
import com.mathfriendzy.controller.player.LoginUserCreatePlayer;
import com.mathfriendzy.controller.player.LoginUserPlayerActivity;
import com.mathfriendzy.controller.player.PlayersActivity;
import com.mathfriendzy.controller.player.TeacherPlayer;
import com.mathfriendzy.controller.professionaltutoring.ActTryItFreeNow;
import com.mathfriendzy.controller.registration.RegistrationStep1;
import com.mathfriendzy.controller.resources.ActLessonCategories;
import com.mathfriendzy.controller.schoolfunctions.ActSchoolFunctions;
import com.mathfriendzy.controller.singlefriendzy.SingleFriendzyMain;
import com.mathfriendzy.controller.top100.Top100Activity;
import com.mathfriendzy.controller.top100.Top100ListActivity;
import com.mathfriendzy.controller.tutorial.ActTutorial;
import com.mathfriendzy.controller.tutoringsession.ActTutoringSession;
import com.mathfriendzy.dawnloadimagesfromserver.DawnLoadAvtarFromServerThread;
import com.mathfriendzy.dawnloadimagesfromserver.DawnloadImagesFromserver;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.helper.YesNoListenerInterface;
import com.mathfriendzy.listener.CheckUnlockStatusCallback;
import com.mathfriendzy.listener.TeacherFunctionDialogListener;
import com.mathfriendzy.model.chooseAvtar.AvtarDTO;
import com.mathfriendzy.model.chooseAvtar.ChooseAvtarOpration;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.friendzy.FriendzyServerOperation;
import com.mathfriendzy.model.friendzy.GetEquationIdFromServerForFeiendzy;
import com.mathfriendzy.model.helpastudent.TutorAreaResponse;
import com.mathfriendzy.model.helpastudent.TutorAreaResponsesParam;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.CoinsFromServerObj;
import com.mathfriendzy.model.learningcenter.LearningCenteServerOperation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.PlayerDataFromServerObj;
import com.mathfriendzy.model.learningcenter.PlayerEquationLevelObj;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.learningcenter.PurchaseItemObj;
import com.mathfriendzy.model.multifriendzy.MultiFriendzyServerOperation;
import com.mathfriendzy.model.player.temp.TempPlayer;
import com.mathfriendzy.model.player.temp.TempPlayerOperation;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.LearnignCenterSchoolCurriculumServerOperation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.QuestionLoadedTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SchoolCurriculumLearnignCenterimpl;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.TestDetails;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.UpdatedInfoTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.WordProblemLevelTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.WordProblemQuestionTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.wordProblemLevelDetailTransferObj;
import com.mathfriendzy.model.schoolcurriculum.singlefriendzy.SingleFriendzyServerOperationForWordProblem;
import com.mathfriendzy.model.singleFriendzy.SingleFriendzyImpl;
import com.mathfriendzy.model.singleFriendzy.SingleFriendzyServerOperation;
import com.mathfriendzy.model.spanishchanges.GetWordQuestionsUpdateDatesLang;
import com.mathfriendzy.model.spanishchanges.SpanishChangesImpl;
import com.mathfriendzy.model.top100.JsonAsynTask;
import com.mathfriendzy.model.top100.TopListDatabase;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerDialogs;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.socketprograming.ClientCallback;
import com.mathfriendzy.socketprograming.MyGlobalWebSocket;
import com.mathfriendzy.utils.BuildApp;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ICommonUtils;
import com.mathfriendzy.utils.MathVersions;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.MAIN_ACTIVITY_LOG;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_ID;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;

/**
 * This is the main activity where the application home is display to play it
 * @author Yashwant Singh
 *
 */
public class MainActivity extends ActBaseClass implements OnClickListener , ClientCallback
{
    private RelativeLayout learningCenterLayout = null;
    private RelativeLayout singleFriendzyLayout = null;
    private RelativeLayout multiFriendzyLayout  = null;
    private Button btnRateUs 			= null;//work as tour button
    private Button btnHelp 				= null;
    private TextView lblLearningCenter 			= null;
    private TextView lblVideosFlashCardsPlay 	= null;
    private TextView lblSingleFriendzy 			= null;
    private TextView lblPlayAroundTheWorld 		= null;
    private TextView lblBestOf5Friendzy 		= null;
    private TextView lblPlayWithYourFriends 	= null;
    private Button   btnTitlePlayers 			= null;
    private TextView lblTheGameOfMath			= null;
    private Button   btnTitleTop100         	= null;

    private ArrayList<AvtarDTO> avtarDataList   = null;
    public static boolean isAvtarLoaded 		= false;
    private final String TAG					= this.getClass().getSimpleName();

    public static int IS_CALL_FROM_LEARNING_CENTER = 0;
    public static int IS_CALL_FROM_SINGLE_FRIENDZY = 0;
    public static int IS_CALL_FROM_MULTIFRIENDZY   = 0;

    private final int MAX_ITEM_ID = 11;// if item id is 11 the unlock the level
    //private final int MAX_ITEM_ID_FOR_MULTIFRIENDZY = 12;// if item id is 12 the unlock the level
    private final int MAX_ITEM_ID_FOR_MULTIFRIENDZY = 100;// if item id is 100 the unlock the level
    private final int MAX_ITEM_ID_FOR_ALL_APP = 100;

    private int isClickOn = 0;
    private boolean isCompleteLevelLoadFromSerevr = false;//single friendzy
    private int SCREEN_DENISITY   = 240;

    public static boolean isTab						= false;
    //private SharedPreferences sharedPreference 	= null;

    public static ArrayList<UpdatedInfoTransferObj> worlProblemLevelDetail
            = new ArrayList<UpdatedInfoTransferObj>();//transfer to new learning center

    //for friendzy challenge
    public static GetEquationIdFromServerForFeiendzy equationsObj = null;

    //changes for assesmentTest
    Button btnAssessmentTest = null;

    //for testDetail For assessment from server , set it into list for sending on newLearing center
    public static ArrayList<TestDetails> testDeatilList = new ArrayList<TestDetails>();

    //for student homework
    private Button btnHomeWork 			= null;
    private TextView txtNotification 	= null;

    //for school funstions
    private RelativeLayout schoolFunctions = null;
    private TextView lblSchoolFunction = null;
    private TextView lblLearnWithFriends = null;

    private final int START_TUTORIAL_ACT_CODE = 1001;

    private Button btnTour = null;//work as rate us
    private RelativeLayout freeSubscriptiontextLayout = null;
    private TextView txtFreeSubscriptionText = null;
    private String lblRegisterLoginToUseSchoolFree =
            "Register/Login to use our school function FREE for 30 days";
    //private String freeSubscriptionExpire = "Free Student/Teacher functions expires in 30 days";
    private String lblFreeStudentTeacherFunctionExpire = "Free Student/Teacher functions expires in";
    private String lblDays = "Days";

    //for top 100
    private boolean student_flag;
    private boolean school_flag;
    private boolean teacher_flag;

    private String lblYourSchoolSubscriptionExpire = "Your school subscription will expired on";
    private final String CURRENT_EXPIRE_DATE_FORMAT = "yyyy-MM-dd";
    private final String REQUIRED_EXPIRE_DATE_FORMAT = "MMMM dd, yyyy";
    private final String CURRENT_SUBSCRIPTION_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    //for testing socket
    //private MyWebSocket mySocket = null;

    //for professional tutoring
    private RelativeLayout professionalTutorLayout = null;
    private TextView lblRequestATutor = null;
    private TextView lblProfessionalAndFreeTutor = null;
    //private TextView txtProfessionalTutorNotification = null;
    private String lblYouMustRegisterLoginToAccess = null;

    //math plus change
    private RelativeLayout mathPlusLayout = null;
    private LinearLayout lytLinearMainMiddle = null;
    private RelativeLayout homeworkAndQuizzesLayoutPlus = null;
    private RelativeLayout learningResourcesLayoutPlus = null;
    private RelativeLayout liveTutorLayoutPlus = null;
    private RelativeLayout funWithMathLayoutPlus = null;
    private RelativeLayout teacherFunctionsLayoutPlus = null;
    private RelativeLayout tutoringSessionLayoutMathPlus = null;

    private TextView lblTheGameOfMathPlus = null;
    private TextView lblHomeworkAndQuizzessPlus = null;
    private TextView lblLearningResourcesPlus = null;
    private TextView lblLiveTutorPlus = null;
    private TextView lblFunWithMathPlus = null;
    private TextView lblTeacherFunctionPlus = null;
    private TextView txtLiveTutorPatentPendingPlus = null;
    private TextView txtHomeworkQuizActiveNotificationPlus = null;
    private TextView lblTTutoringSessionMathPlus = null;

    private RelativeLayout mainActivityFullLayout = null;
    private RelativeLayout multiFriendzyLayoutMathPlus = null;
    private TextView lblMultiFriendzyMathPlus = null;


    //math tutor plus
    private RelativeLayout mathTutorPlusLayout = null;
    private LinearLayout lytLinearMainMiddleTutorPlus = null;
    private RelativeLayout requestATutorLayoutTutorPlus = null;
    private RelativeLayout instructionVedionLayoutTutorPlus = null;
    private RelativeLayout mathExcersizeLayoutTutorPlus = null;
    private RelativeLayout worldCompletetionLayoutTutorPlus = null;
    private RelativeLayout tutoringSessionLayoutTutorPlus = null;


    private TextView lblrequestATutorsTutorPlus = null;
    private TextView lblInstructionVedioTutorPlus = null;
    private TextView lblMathExcersizePlusPlus = null;
    private TextView lblWorldCompletetinTutorPlus = null;
    private TextView lblTTutoringSessionTutorPlus = null;


    private RelativeLayout topBarHelpButtonLayout = null;
    private RelativeLayout topBarRateUsButtonLayout = null;
    private RelativeLayout topBarTourButtonLayout = null;
    private RelativeLayout topBarTop100ButtonLayout = null;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        if(MAIN_ACTIVITY_LOG)
            Log.e(TAG, " inside onCreate()");

        //only for Spanish Changes
        CommonUtils.isOneTimeClick = true;
        //end changes
        this.initializeLayouts();
        //added for new registration
        this.getIntentValue();
        //end changes

        this.getWidgetsReferences();
        this.setWidgetsText();
        this.setListenerOnWidgets();

        if(!isAvtarLoaded){//check for avtar is loaded or not, if loaded then it will not load again
            if(CommonUtils.isInternetConnectionAvailable(this)){
                isAvtarLoaded = true;//changes
                new GetAvtarFromServer().execute(null,null,null);
            }
        }
        this.showFreeSubscriptionDialog();
        //for math plus changes
        this.setVisibilityOfVersionLayout();
        //end math plus changes

        //New In App Changes
        this.getInAppPurchaseStatus();

        this.createGlobalRoomObject();
        if(MAIN_ACTIVITY_LOG)
            Log.e(TAG, " outside onCreate()");
    }

    private void initializeLayouts(){
        setContentView(R.layout.activity_main);
        isTab = getResources().getBoolean(R.bool.isTablet);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
        if(tabletSize){
            if (metrics.densityDpi > 160){
                setContentView(R.layout.activity_main);
            }else{
                setContentView(R.layout.tab_ten_inch_activity_main);
            }
        }else{
            if(metrics.heightPixels == ICommonUtils.TAB_HEIGHT &&
                    metrics.widthPixels == ICommonUtils.TAB_WIDTH
                    && metrics.densityDpi == ICommonUtils.TAB_DENISITY)
            {
                setContentView(R.layout.activity_main_tab_loe_denisity);
            }
            else if ((getResources().getConfiguration().screenLayout &
                    Configuration.SCREENLAYOUT_SIZE_MASK) ==
                    Configuration.SCREENLAYOUT_SIZE_NORMAL &&
                    metrics.densityDpi > SCREEN_DENISITY)
            {
                setContentView(R.layout.activity_main_activity_large);
            }
        }
    }

    private void setVisibilityOfVersionLayout() {
        if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS){
            mathPlusLayout.setVisibility(RelativeLayout.VISIBLE);
            lytLinearMainMiddle.setVisibility(LinearLayout.GONE);
            mathTutorPlusLayout.setVisibility(RelativeLayout.GONE);
            mainActivityFullLayout.setBackgroundResource(R.color.WHITE);

            //for topbar change
            topBarHelpButtonLayout.setVisibility(RelativeLayout.GONE);
            topBarRateUsButtonLayout.setVisibility(RelativeLayout.GONE);
            btnRateUs.setText("Send Log");

            //top 100 work as help button for homework friendzy
            btnTitleTop100.setBackgroundResource(R.drawable.blue_button_ipad);
            btnTitleTop100.setText(MathFriendzyHelper.getTreanslationTextById(this , "lblHelp"));
            //end change
        }else if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_SIMPLE){
            mathPlusLayout.setVisibility(RelativeLayout.GONE);
            mathTutorPlusLayout.setVisibility(RelativeLayout.GONE);
            lytLinearMainMiddle.setVisibility(LinearLayout.VISIBLE);
        }else if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_TUTOR_PLUS){
            mathPlusLayout.setVisibility(RelativeLayout.GONE);
            lytLinearMainMiddle.setVisibility(LinearLayout.GONE);
            mathTutorPlusLayout.setVisibility(RelativeLayout.VISIBLE);
            mainActivityFullLayout.setBackgroundResource(R.color.WHITE);

            //for topbar change
            topBarHelpButtonLayout.setVisibility(RelativeLayout.GONE);
            topBarRateUsButtonLayout.setVisibility(RelativeLayout.GONE);
        }else {
            mathPlusLayout.setVisibility(RelativeLayout.GONE);
            mathTutorPlusLayout.setVisibility(RelativeLayout.GONE);
            lytLinearMainMiddle.setVisibility(LinearLayout.VISIBLE);
        }

        this.setVisibilityOfLayoutButtonVisibility();
    }

    /**
     * Show the subscription dialog
     */
    private void showFreeSubscriptionDialog(){
        if(!MathFriendzyHelper.isFreeSubscriptionDialogShown(this)){//true for already shown
            // , false for not shown
            MathFriendzyHelper.saveFreeSubscriptionStatus(this, true);

            if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS) {
                MathFriendzyHelper.showFreeSubscriptionRegisterDialog
                        (this, new YesNoListenerInterface() {

                            @Override
                            public void onYes() {//For register
                                startActivity(new Intent(MainActivity.this,
                                        RegistrationStep1.class));
                            }

                            @Override
                            public void onNo() {//For No Thanks

                            }
                        });
            }else if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_TUTOR_PLUS) {
                MathFriendzyHelper.showFreeSubscriptionRegisterDialog
                        (this, new YesNoListenerInterface() {

                            @Override
                            public void onYes() {//For register
                                startActivity(new Intent(MainActivity.this,
                                        RegistrationStep1.class));
                            }

                            @Override
                            public void onNo() {//For No Thanks

                            }
                        });
            } else {
                Intent intent = new Intent(this , ActTutorial.class);
                startActivityForResult(intent, START_TUTORIAL_ACT_CODE);
            }
        }
    }

    /**
     * This method getIntent value which is set in registration step 2
     */
    private void getIntentValue(){
        if(this.getIntent().getBooleanExtra("isCallFromRegistration", false)){
            CommonUtils.showDialogAfterRegistrationSuccess(this);
        }
    }

    /**
     * This method get references from layout and set it to the widgets
     */
    private void getWidgetsReferences()
    {
        if(MAIN_ACTIVITY_LOG)
            Log.e(TAG, " inside getWidgetsReferences()");

        learningCenterLayout    = (RelativeLayout) findViewById(R.id.learningCenterLayout);
        singleFriendzyLayout    = (RelativeLayout) findViewById(R.id.singleFriendzyLayout);
        multiFriendzyLayout     = (RelativeLayout) findViewById(R.id.multiFriendzyLayout);
        btnHelp 				= (Button) findViewById(R.id.btnHelp);
        btnRateUs 				= (Button) findViewById(R.id.btnRateUs);
        btnTour                 = (Button) findViewById(R.id.btnTour);
        lblLearningCenter 		= (TextView) findViewById(R.id.lblLearningCenter);
        lblVideosFlashCardsPlay = (TextView) findViewById(R.id.lblVideosFlashCardsPlay);
        lblSingleFriendzy 		= (TextView) findViewById(R.id.lblSingleFriendzy);
        lblPlayAroundTheWorld 	= (TextView) findViewById(R.id.lblPlayAroundTheWorld);
        lblBestOf5Friendzy 		= (TextView) findViewById(R.id.lblBestOf5Friendzy);
        lblPlayWithYourFriends 	= (TextView) findViewById(R.id.lblPlayWithYourFriends);
        btnTitlePlayers 		= (Button) findViewById(R.id.btnTitlePlayers);
        lblTheGameOfMath		= (TextView) findViewById(R.id.lblTheGameOfMath);
        btnTitleTop100			= (Button) findViewById(R.id.btnTitleTop100);

        //changes for assesmentTest
        btnAssessmentTest       = (Button) findViewById(R.id.btnAssessmentTest);

        //for student homework
        btnHomeWork             = (Button) findViewById(R.id.btnHomeWork);
        txtNotification         = (TextView) findViewById(R.id.txtNotification);

        //for school funcitons
        schoolFunctions 	= (RelativeLayout) findViewById(R.id.schoolFunctions);
        lblSchoolFunction 	= (TextView) findViewById(R.id.lblSchoolFunction);
        lblLearnWithFriends = (TextView) findViewById(R.id.lblLearnWithFriends);

        freeSubscriptiontextLayout = (RelativeLayout) findViewById(R.id.freeSubscriptiontextLayout);
        txtFreeSubscriptionText    = (TextView) findViewById(R.id.txtFreeSubscriptionText);


        //for professional tutoring
        professionalTutorLayout = (RelativeLayout) findViewById(R.id.professionalTutorLayout);
        lblRequestATutor = (TextView) findViewById(R.id.lblRequestATutor);
        lblProfessionalAndFreeTutor = (TextView) findViewById(R.id.lblProfessionalAndFreeTutor);

        this.setWidgetsReferenceForMathPlus();
        this.setWidgetsReferenceForMathTutorPlus();

        topBarHelpButtonLayout = (RelativeLayout) findViewById(R.id.topBarHelpButtonLayout);
        topBarRateUsButtonLayout = (RelativeLayout) findViewById(R.id.topBarRateUsButtonLayout);
        topBarTourButtonLayout = (RelativeLayout) findViewById(R.id.topBarTourButtonLayout);
        topBarTop100ButtonLayout = (RelativeLayout) findViewById(R.id.topBarTop100ButtonLayout);

        if(MAIN_ACTIVITY_LOG)
            Log.e(TAG, " outside getWidgetsReferences()");
    }




    /**
     * This method set the widgets text values from the translation
     */
    private void setWidgetsText()
    {
        if(MAIN_ACTIVITY_LOG)
            Log.e(TAG, " inside setWidgetsText()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        btnTour.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleRateUs"));
        btnRateUs.setText(transeletion.getTranselationTextByTextIdentifier("lblTour"));
        btnHelp.setText(transeletion.getTranselationTextByTextIdentifier("lblHelp"));
        lblLearningCenter.setText(transeletion.getTranselationTextByTextIdentifier("lblLearningCenter"));
        lblVideosFlashCardsPlay.setText(transeletion.getTranselationTextByTextIdentifier("lblVideosFlashCardsPlay"));
        lblSingleFriendzy.setText(transeletion.getTranselationTextByTextIdentifier("lblSingleFriendzy"));
        lblPlayAroundTheWorld.setText(transeletion.getTranselationTextByTextIdentifier("lblPlayAroundTheWorld"));
        lblBestOf5Friendzy.setText(transeletion.getTranselationTextByTextIdentifier("lblBestOf5Friendzy"));
        lblPlayWithYourFriends.setText(transeletion.getTranselationTextByTextIdentifier("lblPlayWithYourFriends"));
        //btnTitlePlayers.setText(transeletion.getTranselationTextByTextIdentifier("btnTitlePlayers"));
        lblTheGameOfMath.setText(transeletion.getTranselationTextByTextIdentifier("lblTheGameOfMath") + "!");
        btnTitleTop100.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleTop100"));
        //changes for friendzy
        btnTitlePlayers.setText(transeletion.getTranselationTextByTextIdentifier("lblFriendzyChallenge"));
        //changes for assesmentTest
        btnAssessmentTest.setText(transeletion.getTranselationTextByTextIdentifier("lblAssessmentTest"));
        btnHomeWork.setText(transeletion.getTranselationTextByTextIdentifier("lblHomework")
                + "/" + transeletion.getTranselationTextByTextIdentifier("lblQuizzes"));
        //for school function
        lblSchoolFunction.setText(transeletion.getTranselationTextByTextIdentifier("pickerTitleSchool")
                + " " + transeletion.getTranselationTextByTextIdentifier("lblFunctions"));
        lblLearnWithFriends.setText(transeletion.getTranselationTextByTextIdentifier("lblLearnWithFriends"));
        lblFreeStudentTeacherFunctionExpire = transeletion
                .getTranselationTextByTextIdentifier("lblFreeStudentTeacherFunctionExpire");
        lblDays = transeletion.getTranselationTextByTextIdentifier("lblDays");
        lblRegisterLoginToUseSchoolFree = transeletion
                .getTranselationTextByTextIdentifier("lblRegisterLoginToUse")
                + " " + transeletion
                .getTranselationTextByTextIdentifier("btnTitleFree")
                + " " + transeletion.getTranselationTextByTextIdentifier("lblFor30days");
        lblYourSchoolSubscriptionExpire = transeletion
                .getTranselationTextByTextIdentifier("lblYourSchoolSubscriptionExpire");
        lblRequestATutor.setText(transeletion.getTranselationTextByTextIdentifier("lblRequestATutor"));
        lblProfessionalAndFreeTutor.setText(transeletion
                .getTranselationTextByTextIdentifier("lblMfProfessional")
                + " & " + transeletion
                .getTranselationTextByTextIdentifier("btnTitleTryFree"));
        lblYouMustRegisterLoginToAccess = transeletion
                .getTranselationTextByTextIdentifier("lblYouMustRegisterLoginToAccess");
        this.setMathPlusWidgetsText(transeletion);
        this.setMathTutorPlusText(transeletion);
        transeletion.closeConnection();

        if(MAIN_ACTIVITY_LOG)
            Log.e(TAG, " outside setWidgetsText()");
    }



    /**
     * This method set listener on widgets
     */
    private void setListenerOnWidgets()
    {
        if(MAIN_ACTIVITY_LOG)
            Log.e(TAG, " inside setListenerOnWidgets()");

        learningCenterLayout.setOnClickListener(this);
        singleFriendzyLayout.setOnClickListener(this);
        multiFriendzyLayout.setOnClickListener(this);
        btnTitlePlayers.setOnClickListener(this);
        btnTitleTop100.setOnClickListener(this);

        //changes for assesmentTest
        btnAssessmentTest.setOnClickListener(this);

        btnHomeWork.setOnClickListener(this);

        btnRateUs.setOnClickListener(this);
        btnHelp.setOnClickListener(this);

        //for school funstions
        schoolFunctions.setOnClickListener(this);
        btnTour.setOnClickListener(this);

        professionalTutorLayout.setOnClickListener(this);

        this.setListenetOnMathPlusLayout();
        this.setListenetOnMathTutorPlusLayout();

        if(MAIN_ACTIVITY_LOG)
            Log.e(TAG, " outside setListenerOnWidgets()");
    }



    /**
     * Show the teacher function popup
     * @param popUpFor
     * @return
     */
    private boolean showTeacherFunctionPopUp(final String popUpFor ,
                                             String txtTitle , boolean isShowWatchButton){
        if(MathFriendzyHelper.showTeacherFunctionPopup(this ,
                popUpFor , new TeacherFunctionDialogListener() {

                    @Override
                    public void clickOnWatchVedio(boolean isShow) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void clickOnDontShow(boolean isShow) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void clickOnClose(boolean isShow) {
                        clickOn(popUpFor);
                    }
                } , txtTitle , isShowWatchButton))
            return true;
        return false;
    }


    /**
     * Go after click on close button on Teacher function show vedio
     * @param dialogFor
     */
    private void clickOn(String dialogFor){
        if(MathFriendzyHelper.DIALOG_LEARNING_CENTER.equals(dialogFor)){
            this.goForLearningCenter();
        }else if(MathFriendzyHelper.DIALOG_SINGLE_FRIENDZY.equals(dialogFor)){
            this.goForSingleFriendzy();
        }else if(MathFriendzyHelper.DIALOG_MULTI_FRIENDZY.equals(dialogFor)){
            this.goForMultiFriendzy();
        }else if(MathFriendzyHelper.DIALOG_SCHOOL_HOMEWORK_QUIZZ.equals(dialogFor)){
            this.clickOnHomeWork();
        }else if(MathFriendzyHelper.DIALOG_TUTORING_SESSION.equals(dialogFor)){
            this.clickOnPreviousSession();
        }
    }

    /**
     * Start Learning center
     */
    private void goForLearningCenter(){
        TempPlayerOperation tempObj = new TempPlayerOperation(this);
        if(tempObj.isTemparyPlayerExist())
        {
            tempObj.closeConn();
            this.clickOnLearningCenter();
        }
        else
        {
            tempObj.createTempPlayer("LearningCenterMain");

        }
    }

    /**
     * Start Single Friendzy
     */
    private void goForSingleFriendzy(){
        TempPlayerOperation tempObj1 = new TempPlayerOperation(this);
        if(tempObj1.isTemparyPlayerExist())
        {
            tempObj1.closeConn();
            this.clickOnSingleFriendzy();

        }
        else
        {
            tempObj1.createTempPlayer("SingleFriendzyMain");
        }
    }

    /**
     * Start multifriendzy
     */
    private void goForMultiFriendzy(){
        if(!CommonUtils.isInternetConnectionAvailable(this)){
            DialogGenerator dg = new DialogGenerator(this);
            Translation transeletion = new Translation(this);
            transeletion.openConnection();
            dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
            transeletion.closeConnection();
        }
        else{
            TempPlayerOperation tempObj2 = new TempPlayerOperation(this);
            if(tempObj2.isTemparyPlayerExist())
            {
                tempObj2.closeConn();
                this.clickOnMultiFriendzy();
            }
            else
            {
                tempObj2.createTempPlayer("MultiFriendzyMain");
            }
        }
    }


    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.learningCenterLayout :
                if(this.showTeacherFunctionPopUp
                        (MathFriendzyHelper.DIALOG_LEARNING_CENTER
                                , MathFriendzyHelper.getTextFromTextView(lblLearningCenter) , false))
                    return ;
                this.goForLearningCenter();
                break;
            case R.id.singleFriendzyLayout :
                if(this.showTeacherFunctionPopUp
                        (MathFriendzyHelper.DIALOG_SINGLE_FRIENDZY,
                                MathFriendzyHelper.getTextFromTextView(lblSingleFriendzy) , false))
                    return ;
                this.goForSingleFriendzy();
                break;
            case R.id.multiFriendzyLayout :
                if(this.showTeacherFunctionPopUp
                        (MathFriendzyHelper.DIALOG_MULTI_FRIENDZY ,
                                MathFriendzyHelper.getTextFromTextView(lblBestOf5Friendzy) , false))
                    return ;
                this.goForMultiFriendzy();
                break;
            case R.id.btnTitlePlayers :
                //this.checkForTempPlayer();
                //added by shilpi for friendzy challenge
                this.checkPlayer();
                break;
            case R.id.btnTitleTop100 :
                //top 100 button will work as help button for the homework friendzy
                if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS){
                    this.clickOnHelp(ActHelp.Introduction_Id);
                    return ;
                }//end change

                SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
                if(!sheredPreference.getBoolean(IS_LOGIN, false))
                {
                    Translation transeletion = new Translation(this);
                    transeletion.openConnection();
                    DialogGenerator dg = new DialogGenerator(this);
                    dg.generateRegisterOrLogInDialog(transeletion.
                            getTranselationTextByTextIdentifier
                                    ("alertMsgYouMustLoginOrRegisterToViewAndParticipate"));
                    transeletion.closeConnection();
                }
                else
                {
                    //this.showTop100ForStudent();
                    startActivity(new Intent(this,Top100Activity.class));
                }
                break;

            case R.id.btnAssessmentTest :
                SharedPreferences sheredPreferenceLogin = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
                if(sheredPreferenceLogin.getBoolean(IS_LOGIN, false)){
                    AssessmentClass assessmentTest = new AssessmentClass(this);
                    assessmentTest.startAssessmentActivity();
                }else{
                    DialogGenerator dg = new DialogGenerator(this);
                    Translation translate = new Translation(this);
                    translate.openConnection();
                    dg.generateWarningDialog(translate.getTranselationTextByTextIdentifier
                            ("alertMsgYouMustRegisterLoginToAccessThisFeature")+"");
                    translate.closeConnection();
                }
                break;
            case R.id.btnHomeWork:
                if(this.showTeacherFunctionPopUp
                        (MathFriendzyHelper.DIALOG_SCHOOL_HOMEWORK_QUIZZ ,
                                MathFriendzyHelper.getTextFromButton(btnHomeWork) , true))
                    return ;

                this.clickOnHomeWork();
                break;
            case R.id.btnRateUs:
                if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS){
                    MathFriendzyHelper
                            .sendEmailWithLogFile(this , MathFriendzyHelper
                                    .getFullLogFilePath(MathFriendzyHelper.LOG_FILE_NAME));
                    return ;
                }
                this.clickOnTour();
                break;
            case R.id.btnHelp:
                this.clickOnHelp(ActHelp.Introduction_Id);
                break;
            case R.id.schoolFunctions:
               /* if(this.showTeacherFunctionPopUp
                        (MathFriendzyHelper.DIALOG_SCHOOL_FUNCTION))
                    return ;*/
                this.clickOnSchoolFunctions();
                break;
            case R.id.btnTour:
                this.clickOnRateUs();
                break;
            case R.id.professionalTutorLayout:
                this.clickOnProfessionalTutor();
                break;
            case R.id.teacherFunctionsLayoutPlus:
                this.clickOnTeacherFunctionPlus();
                break;
            case R.id.homeworkAndQuizzesLayoutPlus:
                if(this.showTeacherFunctionPopUp
                        (MathFriendzyHelper.DIALOG_SCHOOL_HOMEWORK_QUIZZ ,
                                MathFriendzyHelper.getTextFromTextView(lblHomeworkAndQuizzessPlus) , true))
                    return ;
                this.clickOnHomeWork();
                break;
            case R.id.liveTutorLayoutPlus:
                this.clickOnProfessionalTutor();
                break;
            case R.id.funWithMathLayoutPlus:
                this.clickOnFunWithMath();
                /*if(this.showTeacherFunctionPopUp
                        (MathFriendzyHelper.DIALOG_LEARNING_CENTER
                                , MathFriendzyHelper.getTextFromTextView(lblFunWithMathPlus) , false))
                    return ;
                this.goForLearningCenter();*/
                break;
            case R.id.learningResourcesLayoutPlus:
                this.clickOnResources();
                break;
            case R.id.requestATutorLayoutTutorPlus:
                this.clickOnProfessionalTutor();
                break;
            case R.id.instructionVedionLayoutTutorPlus:
                this.clickOnResources();
                break;
            case R.id.mathExcersizeLayoutTutorPlus:
                if(this.showTeacherFunctionPopUp
                        (MathFriendzyHelper.DIALOG_LEARNING_CENTER
                                , MathFriendzyHelper.getTextFromTextView(lblMathExcersizePlusPlus) , false))
                    return ;
                this.goForLearningCenter();
                break;
            case R.id.worldCompletetionLayoutTutorPlus:
                if(this.showTeacherFunctionPopUp
                        (MathFriendzyHelper.DIALOG_SINGLE_FRIENDZY,
                                MathFriendzyHelper.getTextFromTextView(lblWorldCompletetinTutorPlus) , false))
                    return ;
                this.goForSingleFriendzy();
                break;
            case R.id.tutoringSessionLayoutTutorPlus:
                if(this.showTeacherFunctionPopUp
                        (MathFriendzyHelper.DIALOG_TUTORING_SESSION,
                                MathFriendzyHelper.getTextFromTextView(lblTTutoringSessionTutorPlus) , false))
                    return ;
                this.clickOnPreviousSession();
                break;
            case R.id.tutoringSessionLayoutMathPlus:
                if(this.showTeacherFunctionPopUp
                        (MathFriendzyHelper.DIALOG_TUTORING_SESSION,
                                MathFriendzyHelper.getTextFromTextView(lblTTutoringSessionMathPlus) , false))
                    return ;
                this.clickOnPreviousSession();
                /*if(this.showTeacherFunctionPopUp
                        (MathFriendzyHelper.DIALOG_SINGLE_FRIENDZY,
                                MathFriendzyHelper.getTextFromTextView(lblTTutoringSessionMathPlus) , false))
                    return ;
                this.goForSingleFriendzy();*/
                break;
            case R.id.multiFriendzyLayoutMathPlus:
                if(this.showTeacherFunctionPopUp
                        (MathFriendzyHelper.DIALOG_MULTI_FRIENDZY ,
                                MathFriendzyHelper.getTextFromTextView(lblMultiFriendzyMathPlus) , false))
                    return ;
                this.goForMultiFriendzy();
                break;
        }
    }

    /**
     * Click for rate us , work as a tour function
     */
    private void clickOnRateUs(){
        if(CommonUtils.isInternetConnectionAvailable(this)){
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(MathFriendzyHelper.getAppRateUrl(this))));
        }else{
            CommonUtils.showInternetDialog(this);
        }
    }

    /**
     * Click for tour
     */
    private void clickOnTour(){
        Intent intent = new Intent(this , ActTutorial.class);
        startActivityForResult(intent, START_TUTORIAL_ACT_CODE);
    }

    /**
     * Click on Help
     */
    private void clickOnHelp(int defaultOpen){
        Intent intent = new Intent(this , ActHelp.class);
        intent.putExtra("visibleId", defaultOpen);
        startActivity(intent);
        //startActivity(new Intent(this , ActAddHomeworkSheet.class));
    }

    private void clickOnSchoolFunctions(){
        startActivity(new Intent(this , ActSchoolFunctions.class));
    }

    /**
     * This method call when user click on multifriendzy
     */
    private void clickOnMultiFriendzy()
    {
        if(MAIN_ACTIVITY_LOG)
            Log.e(TAG, " inside clickOnMultiFriendzy()");

        //check for user login or not in not then check for temp player existence

        //shared preff for holding chacked player for playing
        SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);

        //sharedpreference for holding player info
        SharedPreferences sharedPrefPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
        SharedPreferences.Editor editor = sharedPrefPlayerInfo.edit();

        if(!sheredPreference.getBoolean(IS_LOGIN, false))
        {
            TempPlayerOperation tempPlayer = new TempPlayerOperation(this);
            if(!tempPlayer.isTempPlayerDeleted())
            {
                TempPlayerOperation tempPlayer1 = new TempPlayerOperation(this);
                ArrayList<TempPlayer> tempPlayerData = tempPlayer1.getTempPlayerData();

                Intent intent = new Intent(this,MultiFriendzyMain.class);

                editor.clear();
                editor.putString("playerName", tempPlayerData.get(0).getFirstName() + " " + tempPlayerData.get(0).getLastName());
                editor.putString("city", tempPlayerData.get(0).getCity());
                editor.putString("state", tempPlayerData.get(0).getState());
                editor.putString("imageName",  tempPlayerData.get(0).getProfileImageName());
                editor.putString("coins",  String.valueOf(tempPlayerData.get(0).getCoins()));
                editor.putInt("grade", tempPlayerData.get(0).getGrade());

                //for score update
                editor.putString("userId",  "0");
                editor.putString("playerId",  "0");

                editor.putString("countryName",tempPlayerData.get(0).getCountry());
                editor.commit();
                startActivity(intent);
            }
            else
            {
                /*Intent intent = new Intent(this,CreateTempPlayerActivity.class);
                startActivity(intent);*/
                MathFriendzyHelper.openCreateTempPlayerActivity(this);
            }
        }
        else
        {
            UserPlayerOperation userPlayer = new UserPlayerOperation(this);
            if(!userPlayer.isUserPlayersExist())
            {
                SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0);
                if(sheredPreferencePlayer.getString(PLAYER_ID, "").equals(""))
                {
                    UserRegistrationOperation userObj = new UserRegistrationOperation(this);
                    if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
                    {
                        Intent intent = new Intent(this,TeacherPlayer.class);
                        startActivity(intent);
                    }
                    else
                    {
                        IS_CALL_FROM_MULTIFRIENDZY = 1;
                        Intent intent = new Intent(this,LoginUserPlayerActivity.class);
                        startActivity(intent);
                    }
                }
                else
                {
                    UserPlayerOperation userPlayerOpr = new UserPlayerOperation(this);
                    UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(sheredPreferencePlayer.getString(PLAYER_ID, ""));

                    if(userPlayerData != null )
                    {
						/*//changes for friendzy challenge
						if(CommonUtils.isActivePlayer(userPlayerData.getParentUserId()
								,userPlayerData.getPlayerid())) {

							//get challenger id
							String challengerId = CommonUtils.getActivePlayerChallengerId
									(userPlayerData.getParentUserId(), userPlayerData.getPlayerid());

							if(CommonUtils.isInternetConnectionAvailable(MainActivity.this)){
								new GetUpdatedEquationsForChallenge(userPlayerData.getParentUserId()
										, userPlayerData.getPlayerid() , challengerId).execute(null,null,null);
							}else{
								equationsObj.setEquationIds(CommonUtils.getActivePlayerChallengerId
										(userPlayerData.getPlayerid() , challengerId));
							}
						}
						//end changes
						 */

                        //changes for friendzy challenge
                        this.setActivePlayerDataForFriendzyChallenge(userPlayerData.getParentUserId() ,
                                userPlayerData.getPlayerid());
                        //end changes

                        //changes
                        SingleFriendzyImpl singleimpl = new SingleFriendzyImpl(this);
                        singleimpl.openConn();
                        int itemId = singleimpl.getItemIdForMultiFriendzy(userPlayerData.getParentUserId());
                        singleimpl.closeConn();

                        //if(itemId != MAX_ITEM_ID_FOR_MULTIFRIENDZY)
                        if(itemId != MAX_ITEM_ID_FOR_ALL_APP)
                        {
                            if(CommonUtils.isInternetConnectionAvailable(this))
                            {
                                new GetPurchsedItemDetailByUserId(userPlayerData.getParentUserId(),this)
                                        .execute(null,null,null);
                            }
                        }
                        //changes end
                        else
                        {
                            this.setUserDetailForMultiFriendzy();
                        }
                    }
                    else
                    {
                        UserRegistrationOperation userObj = new UserRegistrationOperation(this);
                        if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
                        {
                            Intent intent = new Intent(this,TeacherPlayer.class);
                            startActivity(intent);
                        }
                        else
                        {
                            IS_CALL_FROM_MULTIFRIENDZY = 1;
                            Intent intent = new Intent(this,LoginUserPlayerActivity.class);
                            startActivity(intent);
                        }
                    }
                }
            }
            else
            {
                UserRegistrationOperation userObj = new UserRegistrationOperation(this);
                if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
                {
                    Intent intent = new Intent(this,CreateTeacherPlayerActivity.class);
                    startActivity(intent);
                }
                else
                {
                    Intent intent = new Intent(this,LoginUserCreatePlayer.class);
                    startActivity(intent);
                }
            }
        }

        if(MAIN_ACTIVITY_LOG)
            Log.e(TAG, " outside clickOnMultiFriendzy()");
    }


    /**
     * This method set the user detail for multifriendzy
     */
    private void setUserDetailForMultiFriendzy()
    {
        SharedPreferences sharedPrefPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
        SharedPreferences.Editor editor = sharedPrefPlayerInfo.edit();

        SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0);
        UserPlayerOperation userPlayerOpr = new UserPlayerOperation(this);
        UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(sheredPreferencePlayer.getString(PLAYER_ID, ""));

        UserRegistrationOperation userObj = new UserRegistrationOperation(this);
        RegistereUserDto regUserObj = userObj.getUserData();

        Intent intent = new Intent(this,MultiFriendzyMain.class);

        editor.clear();
        editor.putString("playerName", userPlayerData.getFirstname() + " " + userPlayerData.getLastname());
        editor.putString("userName", userPlayerData.getUsername());
        editor.putString("city", regUserObj.getCity());
        editor.putString("state", regUserObj.getState());
        editor.putString("imageName",  userPlayerData.getImageName());
        editor.putString("coins",  userPlayerData.getCoin());
        editor.putInt("grade", Integer.parseInt(userPlayerData.getGrade()));
        editor.putString("userId",  userPlayerData.getParentUserId());
        editor.putString("playerId",  userPlayerData.getPlayerid());
        Country country = new Country();
        editor.putString("countryName", country.getCountryNameByCountryId(regUserObj.getCountryId(), this));
        editor.commit();

        startActivity(intent);
    }

    /**
     * This method call when the click perform on learninf center
     * it will check for already login player or not
     * if already login then get the detail from the user player data other wise get detail form temp player
     */
    private void clickOnLearningCenter()
    {
        if(MAIN_ACTIVITY_LOG)
            Log.e(TAG, " inside clickOnLearningCenter()");

        //check for user login or not in not then check for temp player existence

        //shared preff for holding chacked player for playing
        SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);

        //sharedpreference for holding player info
        SharedPreferences sharedPrefPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
        SharedPreferences.Editor editor = sharedPrefPlayerInfo.edit();

        if(!sheredPreference.getBoolean(IS_LOGIN, false))
        {
            TempPlayerOperation tempPlayer = new TempPlayerOperation(this);
            if(!tempPlayer.isTempPlayerDeleted())
            {
                TempPlayerOperation tempPlayer1 = new TempPlayerOperation(this);
                ArrayList<TempPlayer> tempPlayerData = tempPlayer1.getTempPlayerData();

                //Intent intent = new Intent(this,LearningCenterMain.class);

                editor.clear();
                editor.putString("playerName", tempPlayerData.get(0).getFirstName() + " " + tempPlayerData.get(0).getLastName());
                editor.putString("city", tempPlayerData.get(0).getCity());
                editor.putString("state", tempPlayerData.get(0).getState());
                editor.putString("imageName",  tempPlayerData.get(0).getProfileImageName());
                editor.putString("coins",  String.valueOf(tempPlayerData.get(0).getCoins()));
                editor.putInt("grade", tempPlayerData.get(0).getGrade());

                //for score update
                editor.putString("userId",  "0");
                editor.putString("playerId",  "0");

                editor.putString("countryName",tempPlayerData.get(0).getCountry());
                editor.commit();

                clickOnNewLearnignCenter();
                //startActivity(intent);
            }
            else
            {
                /*Intent intent = new Intent(this,CreateTempPlayerActivity.class);
                startActivity(intent);*/
                MathFriendzyHelper.openCreateTempPlayerActivity(this);
            }
        }
        else
        {
            UserPlayerOperation userPlayer = new UserPlayerOperation(this);
            if(!userPlayer.isUserPlayersExist())
            {
                SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0);
                if(sheredPreferencePlayer.getString(PLAYER_ID, "").equals(""))
                {

                    UserRegistrationOperation userObj = new UserRegistrationOperation(this);
                    if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
                    {
                        Intent intent = new Intent(this,TeacherPlayer.class);
                        startActivity(intent);
                    }
                    else
                    {
                        IS_CALL_FROM_LEARNING_CENTER = 1;
                        Intent intent = new Intent(this,LoginUserPlayerActivity.class);
                        startActivity(intent);
                    }
                }
                else
                {
                    UserPlayerOperation userPlayerOpr = new UserPlayerOperation(this);
                    UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(sheredPreferencePlayer.getString(PLAYER_ID, ""));

                    if(userPlayerData != null )
                    {
                        UserRegistrationOperation userObj = new UserRegistrationOperation(this);
                        RegistereUserDto regUserObj = userObj.getUserData();

                        //Intent intent = new Intent(this,LearningCenterMain.class);

                        editor.clear();
                        editor.putString("playerName", userPlayerData.getFirstname() + " " + userPlayerData.getLastname());
                        editor.putString("userName", userPlayerData.getUsername());
                        editor.putString("city", regUserObj.getCity());
                        editor.putString("state", regUserObj.getState());
                        editor.putString("imageName",  userPlayerData.getImageName());
                        editor.putString("coins",  userPlayerData.getCoin());
                        editor.putInt("grade", Integer.parseInt(userPlayerData.getGrade()));
                        editor.putString("userId",  userPlayerData.getParentUserId());
                        editor.putString("playerId",  userPlayerData.getPlayerid());

                        Country country = new Country();
                        editor.putString("countryName", country.getCountryNameByCountryId(regUserObj.getCountryId(), this));
                        editor.commit();

                        clickOnNewLearnignCenter();
                        //startActivity(intent);

                    }
                    else
                    {
                        UserRegistrationOperation userObj = new UserRegistrationOperation(this);
                        if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
                        {
                            Intent intent = new Intent(this,TeacherPlayer.class);
                            startActivity(intent);
                        }
                        else
                        {
                            IS_CALL_FROM_LEARNING_CENTER = 1;
                            NewLearnignCenter.isCallFromMainActivity = true;
                            Intent intent = new Intent(this,LoginUserPlayerActivity.class);
                            startActivity(intent);
                        }
                    }
                }
            }
            else
            {
                UserRegistrationOperation userObj = new UserRegistrationOperation(this);
                if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
                {
                    Intent intent = new Intent(this,CreateTeacherPlayerActivity.class);
                    startActivity(intent);
                }
                else
                {
                    Intent intent = new Intent(this,LoginUserCreatePlayer.class);
                    startActivity(intent);
                }
            }
        }

        if(MAIN_ACTIVITY_LOG)
            Log.e(TAG, " outside clickOnLearningCenter()");

    }

    /**
     * This method check if data exist in this table or not
     * Corresponding to the user and playerId
     * @return
     */
    private boolean isWordProblemDataExistForLearnignCenter(String userId , String playerId){
        boolean isDataExist = false;
        SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
        schoolImpl.openConnection();
        isDataExist = schoolImpl.isDataExistInLocalWordProblemLevels(userId, playerId);
        schoolImpl.closeConnection();
        return isDataExist;
    }

    /**
     * This method call when click on learnign center and created when new learning center is added
     */
    private void clickOnNewLearnignCenter()
    {
        NewLearnignCenter.isCallFromMainActivity = true;

        TempPlayerOperation tempPlayer = new TempPlayerOperation(MainActivity.this);

        if(tempPlayer.isTempPlayerDeleted())
        {
            SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
			/*//changes for friendzy challenge
			if(CommonUtils.isActivePlayer(sharedPreffPlayerInfo.getString("userId", "")
					,sharedPreffPlayerInfo.getString("playerId", ""))
					&& CommonUtils.isInternetConnectionAvailable(MainActivity.this)){
				new GetUpdatedEquationsForChallenge(sharedPreffPlayerInfo.getString("userId", "")
						, sharedPreffPlayerInfo.getString("playerId", "")).execute(null,null,null);
			}
			//end changes
			 */
            //changes for friendzy challenge
            this.setActivePlayerDataForFriendzyChallenge(sharedPreffPlayerInfo.getString("userId", ""),
                    sharedPreffPlayerInfo.getString("playerId", ""));
            //end changes

            if(CommonUtils.isInternetConnectionAvailable(MainActivity.this))
            {
                //changes according to shilpi
                LearningCenterimpl impl = new LearningCenterimpl(this);
                impl.openConn();
                boolean isLoacalPlayerDataExist = impl.isPlayerDataExistInLocalPlayerLevels(
                        sharedPreffPlayerInfo.getString("userId", ""),
                        sharedPreffPlayerInfo.getString("playerId", ""));
                impl.closeConn();
                //end changes

                if(isLoacalPlayerDataExist){
                    LearningCenterimpl learnignCenter = new LearningCenterimpl(this);
                    learnignCenter.openConn();
                    ArrayList<PlayerEquationLevelObj> playerquationData = learnignCenter.
                            getPlayerEquationLevelDataByPlayerId(sharedPreffPlayerInfo.getString("playerId", ""));
                    learnignCenter.closeConn();
                    new  AddLevelAsynTask(playerquationData).execute(null,null,null);

                }else{
                    //changes for word problem
                    if(isWordProblemDataExistForLearnignCenter(sharedPreffPlayerInfo.getString("userId", "")
                            , sharedPreffPlayerInfo.getString("playerId", "")))	{
                        new AddAllWordProblemLevel(sharedPreffPlayerInfo.getString("userId", "")
                                , sharedPreffPlayerInfo.getString("playerId", "")).execute(null,null,null);
                    }else{
                        new PlayerDataFromServer(sharedPreffPlayerInfo.getString("userId", ""),
                                sharedPreffPlayerInfo.getString("playerId", "")).execute(null,null,null);
                    }
                }
            }
            else
            {
                startActivity(new Intent(MainActivity.this , NewLearnignCenter.class));
            }
        }
        else//for temp player else block executed
        {
            //comment code for not downloading question on MainScreen for temp playe , On 23 May 2014
			/*SharedPreferences sharedPrefPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
			SchoolCurriculumLearnignCenterimpl scoolImpl = new SchoolCurriculumLearnignCenterimpl(MainActivity.this);
			scoolImpl.openConnection();
			boolean isQuestionLoaded = scoolImpl.isQuestionLeaded(sharedPrefPlayerInfo.getInt("grade" , 1));
			scoolImpl.closeConnection();

			if(CommonUtils.isInternetConnectionAvailable(this)){
				if(isQuestionLoaded)
					startActivity(new Intent(MainActivity.this , NewLearnignCenter.class));
				else
				{
					new GetWordProblemQuestionUpdatedDate(sharedPrefPlayerInfo.getInt("grade" , 1) , true)
					.execute(null,null,null);
					new GetWordProblemQuestion(sharedPrefPlayerInfo.getInt("grade" , 1) , this)
					.execute(null,null,null);
				}
			}else{*/
            startActivity(new Intent(MainActivity.this , NewLearnignCenter.class));
            //}
        }
    }


    /**
     * This method call when user click on single friendzy
     */
    private void clickOnSingleFriendzy()
    {
        if(MAIN_ACTIVITY_LOG)
            Log.e(TAG, " inside clickOnSingleFriendzy()");

        //check for user login or not in not then check for temp player existence

        //shared preff for holding chacked player for playing
        SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);

        //sharedpreference for holding player info
        SharedPreferences sharedPrefPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
        SharedPreferences.Editor editor = sharedPrefPlayerInfo.edit();

        if(!sheredPreference.getBoolean(IS_LOGIN, false))
        {
            TempPlayerOperation tempPlayer = new TempPlayerOperation(this);
            if(!tempPlayer.isTempPlayerDeleted())
            {
                TempPlayerOperation tempPlayer1 = new TempPlayerOperation(this);
                ArrayList<TempPlayer> tempPlayerData = tempPlayer1.getTempPlayerData();

                Intent intent = new Intent(this,SingleFriendzyMain.class);

                editor.clear();
                editor.putString("playerName", tempPlayerData.get(0).getFirstName() + " " + tempPlayerData.get(0).getLastName());
                editor.putString("city", tempPlayerData.get(0).getCity());
                editor.putString("state", tempPlayerData.get(0).getState());
                editor.putString("imageName",  tempPlayerData.get(0).getProfileImageName());
                editor.putString("coins",  String.valueOf(tempPlayerData.get(0).getCoins()));
                editor.putInt("grade", tempPlayerData.get(0).getGrade());

                LearningCenterimpl learningCenter = new LearningCenterimpl(MainActivity.this);
                learningCenter.openConn();
                if(learningCenter.getDataFromPlayerTotalPoints("0")
                        .getCompleteLevel() != 0)
                    editor.putInt("completeLevel", MathFriendzyHelper
                            .getNonZeroValue(learningCenter.getDataFromPlayerTotalPoints("0")
                                    .getCompleteLevel()));
                else
                    editor.putInt("completeLevel", MathFriendzyHelper
                            .getNonZeroValue(tempPlayerData.get(0).getCompeteLevel()));
                learningCenter.closeConn();

                //editor.putInt("completeLevel", tempPlayerData.get(0).getCompeteLevel());
                //for score update
                editor.putString("userId",  "0");
                editor.putString("playerId",  "0");

                editor.putString("countryName",tempPlayerData.get(0).getCountry());
                editor.commit();

                startActivity(intent);
            }
            else
            {
                /*Intent intent = new Intent(this,CreateTempPlayerActivity.class);
                startActivity(intent);*/
                MathFriendzyHelper.openCreateTempPlayerActivity(this);
            }
        }
        else
        {
            UserPlayerOperation userPlayer = new UserPlayerOperation(this);
            if(!userPlayer.isUserPlayersExist())
            {
                SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0);
                if(sheredPreferencePlayer.getString(PLAYER_ID, "").equals(""))
                {

                    UserRegistrationOperation userObj = new UserRegistrationOperation(this);
                    if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
                    {
                        Intent intent = new Intent(this,TeacherPlayer.class);
                        startActivity(intent);
                    }
                    else
                    {
                        IS_CALL_FROM_SINGLE_FRIENDZY = 1;
                        Intent intent = new Intent(this,LoginUserPlayerActivity.class);
                        startActivity(intent);
                    }
                }
                else
                {
                    UserPlayerOperation userPlayerOpr = new UserPlayerOperation(this);
                    UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(sheredPreferencePlayer.getString(PLAYER_ID, ""));

                    if(userPlayerData != null )
                    {
						/*//changes for friendzy challenge
						if(CommonUtils.isActivePlayer(userPlayerData.getParentUserId()
								,userPlayerData.getPlayerid())) {

							//get challenger id
							String challengerId = CommonUtils.getActivePlayerChallengerId
									(userPlayerData.getParentUserId(), userPlayerData.getPlayerid());

							if(CommonUtils.isInternetConnectionAvailable(MainActivity.this)){
								new GetUpdatedEquationsForChallenge(userPlayerData.getParentUserId()
										, userPlayerData.getPlayerid() , challengerId).execute(null,null,null);
							}else{
								equationsObj.setEquationIds(CommonUtils.getActivePlayerChallengerId
										(userPlayerData.getPlayerid() , challengerId));
							}
						}
						//end changes
						 */

                        //changes for friendzy challenge
                        this.setActivePlayerDataForFriendzyChallenge(userPlayerData.getParentUserId() ,
                                userPlayerData.getPlayerid());
                        //end changes

                        SingleFriendzyImpl singleimpl = new SingleFriendzyImpl(this);
                        singleimpl.openConn();
                        int itemId = singleimpl.getItemId(userPlayerData.getParentUserId());
                        singleimpl.closeConn();

                        if(itemId != MAX_ITEM_ID)
                        {
                            if(CommonUtils.isInternetConnectionAvailable(this))
                            {
                                new GetSingleFriendzyDetail(userPlayerData.getParentUserId(),
                                        userPlayerData.getPlayerid() , this).execute(null,null,null);
                            }
                            else
                            {
                                setUserPlayerDataForSingleFriendzyAndGotoSingleFriendzyMainActivity(this);
                            }

                        }
                        else
                        {
                            if(CommonUtils.isInternetConnectionAvailable(this))
                            {
                                new GetpointsLevelDetail(userPlayerData.getParentUserId(),
                                        userPlayerData.getPlayerid() , this).execute(null,null,null);
                            }
                            else
                            {
                                setUserPlayerDataForSingleFriendzyAndGotoSingleFriendzyMainActivity(this);
                            }
                        }
                    }
                    else
                    {
                        UserRegistrationOperation userObj = new UserRegistrationOperation(this);
                        if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
                        {
                            Intent intent = new Intent(this,TeacherPlayer.class);
                            startActivity(intent);
                        }
                        else
                        {
                            IS_CALL_FROM_SINGLE_FRIENDZY = 1;
                            Intent intent = new Intent(this,LoginUserPlayerActivity.class);
                            startActivity(intent);
                        }

                    }
                }
            }
            else
            {
                UserRegistrationOperation userObj = new UserRegistrationOperation(this);
                if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
                {
                    Intent intent = new Intent(this,CreateTeacherPlayerActivity.class);
                    startActivity(intent);
                }
                else
                {
                    Intent intent = new Intent(this,LoginUserCreatePlayer.class);
                    startActivity(intent);
                }
            }
        }

        if(MAIN_ACTIVITY_LOG)
            Log.e(TAG, " outside clickOnSingleFriendzy()");
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(keyCode == KeyEvent.KEYCODE_BACK)
        {
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * This method check for temp player is exist or not
     */
    private void checkForTempPlayer()
    {
        if(MAIN_ACTIVITY_LOG)
            Log.e(TAG, " inside checkForTempPlayer()");

        TempPlayerOperation tempObj = new TempPlayerOperation(this);
        if(tempObj.isTemparyPlayerExist())
        {
            SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
            if(sheredPreference.getBoolean(IS_LOGIN, false))
            {
                UserPlayerOperation userPlayer = new UserPlayerOperation(this);
                if(userPlayer.isUserPlayersExist())
                {
                    UserRegistrationOperation userObj = new UserRegistrationOperation(this);
                    if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
                    {
                        Intent intent = new Intent(this,CreateTeacherPlayerActivity.class);
                        startActivity(intent);
                    }
                    else
                    {
                        Intent intent = new Intent(this,LoginUserCreatePlayer.class);
                        startActivity(intent);
                    }
                }
                else
                {
                    UserRegistrationOperation userObj = new UserRegistrationOperation(this);
                    if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
                    {
                        Intent intent = new Intent(this,TeacherPlayer.class);
                        startActivity(intent);
                    }
                    else
                    {
                        Intent intent = new Intent(this,LoginUserPlayerActivity.class);
                        startActivity(intent);
                    }
                }
            }
            else
            {
                if(tempObj.isTempPlayerDeleted())//if temp player exist then check for temp player is deleted or not from the table
                {
                    /*Intent intentCreate = new Intent(this,CreateTempPlayerActivity.class);
                    startActivity(intentCreate);*/
                    MathFriendzyHelper.openCreateTempPlayerActivity(this);
                }
                else
                {
                    Intent intent  = new Intent(this,PlayersActivity.class);
                    this.startActivity(intent);
                }
            }
            tempObj.closeConn();
        }
        else
        {
            tempObj.createTempPlayer("");//create temp player if not exists
        }

        //tempObj.closeConn();

        if(MAIN_ACTIVITY_LOG)
            Log.e(TAG, " outside checkForTempPlayer()");
    }


    /**
     * This method call after the data from server is loaded for single friendzy
     */
    private void setUserPlayerDataForSingleFriendzyAndGotoSingleFriendzyMainActivity(Context context)
    {
        SharedPreferences sheredPreferencePlayer = context.getSharedPreferences(IS_CHECKED_PREFF, 0);
        UserPlayerOperation userPlayerOpr = new UserPlayerOperation(context);
        UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(sheredPreferencePlayer.getString(PLAYER_ID, ""));

        SharedPreferences sharedPrefPlayerInfo = context.getSharedPreferences(PLAYER_INFO, 0);
        SharedPreferences.Editor editor = sharedPrefPlayerInfo.edit();

        UserRegistrationOperation userObj = new UserRegistrationOperation(context);
        RegistereUserDto regUserObj = userObj.getUserData();

        Intent intent = new Intent(context,SingleFriendzyMain.class);

        editor.clear();
        editor.putString("playerName", userPlayerData.getFirstname() + " " + userPlayerData.getLastname());
        editor.putString("userName", userPlayerData.getUsername());
        editor.putString("city", regUserObj.getCity());
        editor.putString("state", regUserObj.getState());
        editor.putString("imageName",  userPlayerData.getImageName());
        editor.putString("coins",  userPlayerData.getCoin());
        editor.putInt("grade", Integer.parseInt(userPlayerData.getGrade()));

        editor.putString("userId",  userPlayerData.getParentUserId());
        editor.putString("playerId",  userPlayerData.getPlayerid());

        Country country = new Country();
        editor.putString("countryName", country.getCountryNameByCountryId(regUserObj.getCountryId(), context));

        LearningCenterimpl learningCenter = new LearningCenterimpl(context);
        learningCenter.openConn();

        if(learningCenter.getDataFromPlayerTotalPoints(userPlayerData.getPlayerid())
                .getCompleteLevel() != 0)
            editor.putInt("completeLevel", MathFriendzyHelper
                    .getNonZeroValue(learningCenter
                            .getDataFromPlayerTotalPoints(userPlayerData.getPlayerid())
                            .getCompleteLevel()));
        else
            editor.putInt("completeLevel", MathFriendzyHelper
                    .getNonZeroValue(Integer.parseInt(userPlayerData.getCompletelavel())));

        learningCenter.closeConn();
        //for score update
        editor.commit();

        context.startActivity(intent);
    }

    /**
     * This asynckTask get Avtar from server and save it
     * @author Yashwant Singh
     *
     */
    class GetAvtarFromServer extends AsyncTask<Void, Void, Void>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            if(MAIN_ACTIVITY_LOG)
                Log.e(TAG, " inside GetAvtarFromServer doInBackground()");

            ChooseAvtarOpration chooseAvtarObj = new ChooseAvtarOpration();
            avtarDataList = chooseAvtarObj.getAvtar();

            if(MAIN_ACTIVITY_LOG)
                Log.e(TAG, " outside GetAvtarFromServer doInBackground()");

            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            if(MAIN_ACTIVITY_LOG)
                Log.e(TAG, " inside GetAvtarFromServer onPostExecute()");

            //new GetImageBitmap(avtarDataList).execute(null,null,null);

            DawnLoadAvtarFromServerThread dawnlLoadAvter =
                    new DawnLoadAvtarFromServerThread(avtarDataList, MainActivity.this);
            Thread avtardDawnLoadThread = new Thread(dawnlLoadAvter);
            avtardDawnLoadThread.start();

            if(MAIN_ACTIVITY_LOG)
                Log.e(TAG, " outside GetAvtarFromServer onPostExecute()");

            super.onPostExecute(result);
        }
    }


    /**
     * This asyncktask get single friendzy detail from server
     * @author Yashwant Singh
     *
     */
    public class GetSingleFriendzyDetail extends AsyncTask<Void, Void, PlayerDataFromServerObj>
    {
        private String userId 	= null;
        private String playerId = null;
        private Context context = null;
        private ProgressDialog pd;

        public GetSingleFriendzyDetail(String userId, String playerId , Context context)
        {
            this.userId 	= userId;
            this.playerId 	= playerId;
            this.context    = context;
        }

        @Override
        protected void onPreExecute()
        {
            pd = CommonUtils.getProgressDialog(context);
            pd.show();
            super.onPreExecute();
        }

        @Override
        protected PlayerDataFromServerObj doInBackground(Void... params)
        {
            SingleFriendzyServerOperation singleServerObj = new SingleFriendzyServerOperation();
            PlayerDataFromServerObj playerDetaill = singleServerObj.getSingleFriendzyDetail(userId, playerId);

            return playerDetaill;
        }

        @Override
        protected void onPostExecute(PlayerDataFromServerObj result)
        {
            pd.cancel();

            if(result != null){
                String itemIds = "";
                ArrayList<String> itemList = new ArrayList<String>();

                if(result != null){//start if
                    if(result.getItems().length() > 0)
                    {
                        //itemIds = result.getItems().replace(",", "");
                        //get item value which is comma separated , add (,) at the last for finish
                        String newItemList = result.getItems() + ",";

                        for(int i = 0 ; i < newItemList.length() ; i ++ )
                        {
                            if(newItemList.charAt(i) == ',' )
                            {
                                itemList.add(itemIds);
                                itemIds = "";
                            }
                            else
                            {
                                itemIds = itemIds +  newItemList.charAt(i);
                            }
                        }
                    }

                    ArrayList<PurchaseItemObj> purchaseItem = new ArrayList<PurchaseItemObj>();

                    for( int i = 0 ; i < itemList.size() ; i ++ )
                    {
                        PurchaseItemObj purchseObj = new PurchaseItemObj();
                        purchseObj.setUserId(userId);
                        purchseObj.setItemId(Integer.parseInt(itemList.get(i)));
                        purchseObj.setStatus(1);
                        purchaseItem.add(purchseObj);
                    }

                    if(result.getLockStatus() != -1)
                    {
                        PurchaseItemObj purchseObj = new PurchaseItemObj();
                        purchseObj.setUserId(userId);
                        purchseObj.setItemId(100);
                        purchseObj.setStatus(result.getLockStatus());
                        purchaseItem.add(purchseObj);
                    }

                    LearningCenterimpl learningCenter = new LearningCenterimpl(context);
                    learningCenter.openConn();

                    if(learningCenter.isPlayerRecordExits(playerId))
                    {
                        learningCenter.updatePlayerPointsAndCmpleteLevel(result.getPoints(), result.getCompleteLevel(), playerId);
                    }
                    else
                    {
                        PlayerTotalPointsObj playerTotalPoints = new PlayerTotalPointsObj();
                        playerTotalPoints.setUserId(userId);
                        playerTotalPoints.setPlayerId(playerId);
                        playerTotalPoints.setCoins(0);
                        playerTotalPoints.setCompleteLevel(result.getCompleteLevel());
                        playerTotalPoints.setPurchaseCoins(0);
                        playerTotalPoints.setTotalPoints(result.getPoints());
                        learningCenter.insertIntoPlayerTotalPoints(playerTotalPoints);
                    }

                    learningCenter.deleteFromPurchaseItem();
                    learningCenter.insertIntoPurchaseItem(purchaseItem);
                    learningCenter.closeConn();

                }//end if
                setUserPlayerDataForSingleFriendzyAndGotoSingleFriendzyMainActivity(context);
            }else{
                CommonUtils.showInternetDialog(context);
            }
            super.onPostExecute(result);
        }
    }

    /**
     * this class get the player point detail from server for single friendzy
     * @author Yashwant Singh
     *
     */
    public class GetpointsLevelDetail extends AsyncTask<Void, Void, PlayerDataFromServerObj>
    {
        private String userId 	= null;
        private String playerId = null;
        private Context context = null;
        private ProgressDialog pd;

        public GetpointsLevelDetail(String userId, String playerId , Context context)
        {
            this.userId 	= userId;
            this.playerId 	= playerId;
            this.context    = context;
        }

        @Override
        protected void onPreExecute()
        {
            pd = CommonUtils.getProgressDialog(context);
            pd.show();
            super.onPreExecute();
        }

        @Override
        protected PlayerDataFromServerObj doInBackground(Void... params)
        {
            SingleFriendzyServerOperation singleServerObj = new SingleFriendzyServerOperation();
            PlayerDataFromServerObj playerDetaill = singleServerObj.getPointsLevelDetail(userId, playerId);
            return playerDetaill;
        }

        @Override
        protected void onPostExecute(PlayerDataFromServerObj result)
        {
            pd.cancel();

            if(result != null){//start if
                LearningCenterimpl learningCenter = new LearningCenterimpl(context);
                learningCenter.openConn();

                ArrayList<PurchaseItemObj> purchaseItem = new ArrayList<PurchaseItemObj>();
                if(result.getLockStatus() != -1)
                {
                    PurchaseItemObj purchseObj = new PurchaseItemObj();
                    purchseObj.setUserId(userId);
                    purchseObj.setItemId(100);
                    purchseObj.setStatus(result.getLockStatus());
                    purchaseItem.add(purchseObj);
                    learningCenter.deleteFromPurchaseItem(100);
                    learningCenter.insertIntoPurchaseItem(purchaseItem);
                }

                if(learningCenter.isPlayerRecordExits(playerId))
                {
                    learningCenter.updatePlayerPointsAndCmpleteLevel(result.getPoints(), result.getCompleteLevel(), playerId);
                }
                else
                {
                    PlayerTotalPointsObj playerTotalPoints = new PlayerTotalPointsObj();
                    playerTotalPoints.setUserId(userId);
                    playerTotalPoints.setPlayerId(playerId);
                    playerTotalPoints.setCoins(0);
                    playerTotalPoints.setCompleteLevel(result.getCompleteLevel());
                    playerTotalPoints.setPurchaseCoins(0);
                    playerTotalPoints.setTotalPoints(result.getPoints());
                    learningCenter.insertIntoPlayerTotalPoints(playerTotalPoints);
                }

                learningCenter.closeConn();
                setUserPlayerDataForSingleFriendzyAndGotoSingleFriendzyMainActivity(context);
            }//end if
            else{
                CommonUtils.showInternetDialog(context);
            }
            super.onPostExecute(result);
        }
    }


    /**
     * This asyncktask get single friendzy detail from server
     * @author Yashwant Singh
     *
     */
    public class GetPurchsedItemDetailByUserId extends AsyncTask<Void, Void, Void>
    {
        private String userId 	= null;
        private Context context = null;
        private String resultString = "";
        private ProgressDialog pd;

        public GetPurchsedItemDetailByUserId(String userId, Context context )
        {
            this.userId 	= userId;
            this.context    = context;
        }

        @Override
        protected void onPreExecute()
        {
            setUserDetailForMultiFriendzy();
            //pd = CommonUtils.getProgressDialog(context);
            //pd.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            MultiFriendzyServerOperation serverObj = new MultiFriendzyServerOperation();
            resultString = serverObj.getPurchasedItemDetailByUserId(userId);
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            //pd.cancel();

            try{
                if(resultString != null){
                    String itemIds = "";
                    ArrayList<String> itemList = new ArrayList<String>();


                    if(resultString != null){//start if

                        //got app unlock status , changes on 5 May 2014
                        String[] resultValue = resultString.split("&");
                        resultString = resultValue[0].replace("&", "");
                        String appUnlockStatus = resultValue[1].replace("&", "");

                        if(resultString.length() > 0)
                        {
                            //itemIds = result.getItems().replace(",", "");
                            //get item value which is comma separated , add (,) at the last for finish
                            String newItemList = resultString + ",";

                            for(int i = 0 ; i < newItemList.length() ; i ++ )
                            {
                                if(newItemList.charAt(i) == ',' )
                                {
                                    itemList.add(itemIds);
                                    itemIds = "";
                                }
                                else
                                {
                                    itemIds = itemIds +  newItemList.charAt(i);
                                }
                            }
                        }

                        ArrayList<PurchaseItemObj> purchaseItem = new ArrayList<PurchaseItemObj>();


                        for( int i = 0 ; i < itemList.size() ; i ++ )
                        {
                            PurchaseItemObj purchseObj = new PurchaseItemObj();
                            purchseObj.setUserId(userId);
                            purchseObj.setItemId(Integer.parseInt(itemList.get(i)));
                            purchseObj.setStatus(1);
                            purchaseItem.add(purchseObj);
                        }

                        //add app unlick statuc , changes on 5 May 2014
                        try{
                            if(!appUnlockStatus.equals("-1")){
                                PurchaseItemObj purchseObj = new PurchaseItemObj();
                                purchseObj.setUserId(userId);
                                purchseObj.setItemId(100);
                                purchseObj.setStatus(Integer.parseInt(appUnlockStatus));
                                purchaseItem.add(purchseObj);
                            }
                        }catch(Exception e){
                            Log.e(TAG, "Error while inserting app unlock status " + e.toString());
                        }

                        LearningCenterimpl learningCenter = new LearningCenterimpl(context);
                        learningCenter.openConn();
                        learningCenter.deleteFromPurchaseItem();
                        learningCenter.insertIntoPurchaseItem(purchaseItem);
                        learningCenter.closeConn();
                    }//end if
                }
            }catch(Exception e){
                Log.e(TAG, "Error while getting purchase response " + e.toString());
            }
            super.onPostExecute(result);
        }
    }

    /**
     * This class get player data from server for learning center
     * @author Yashwant Singh
     *
     */
    class PlayerDataFromServer extends AsyncTask<Void, Void, PlayerDataFromServerObj>
    {
        private ProgressDialog pd;
        private String userId;
        private String playerId;

        PlayerDataFromServer(String userId , String playeId){
            this.playerId = playeId;
            this.userId   = userId;
        }

        @Override
        protected void onPreExecute()
        {
            pd = CommonUtils.getProgressDialog(MainActivity.this);
            pd.show();
            super.onPreExecute();
        }

        @Override
        protected PlayerDataFromServerObj doInBackground(Void... params)
        {
            LearningCenteServerOperation learnignCenter = new LearningCenteServerOperation();
            PlayerDataFromServerObj playerDataFromServer = learnignCenter.getPlayerDetailFromServer(userId, playerId);
            return playerDataFromServer;
        }

        @Override
        protected void onPostExecute(PlayerDataFromServerObj result)
        {
            //pd.cancel();
            if(result != null){
                try{
                    String itemIds = "";

                    ArrayList<String> itemList = new ArrayList<String>();
                    if(result != null){//start if
                        if(result.getItems().length() > 0)
                        {
                            //itemIds = result.getItems().replace(",", "");
                            //get item value which is comma separated , add (,) at the last for finish
                            String newItemList = result.getItems() + ",";

                            for(int i = 0 ; i < newItemList.length() ; i ++ )
                            {
                                if(newItemList.charAt(i) == ',' )
                                {
                                    itemList.add(itemIds);
                                    itemIds = "";
                                }
                                else
                                {
                                    itemIds = itemIds +  newItemList.charAt(i);
                                }
                            }
                        }

                        ArrayList<PurchaseItemObj> purchaseItem = new ArrayList<PurchaseItemObj>();


                        for( int i = 0 ; i < itemList.size() ; i ++ )
                        {
                            PurchaseItemObj purchseObj = new PurchaseItemObj();
                            purchseObj.setUserId(userId);
                            purchseObj.setItemId(Integer.parseInt(itemList.get(i)));
                            purchseObj.setStatus(1);
                            purchaseItem.add(purchseObj);
                        }

                        if(result.getLockStatus() != -1)
                        {
                            PurchaseItemObj purchseObj = new PurchaseItemObj();
                            purchseObj.setUserId(userId);
                            purchseObj.setItemId(100);
                            purchseObj.setStatus(result.getLockStatus());
                            purchaseItem.add(purchseObj);
                        }

                        LearningCenterimpl learningCenter = new LearningCenterimpl(MainActivity.this);
                        learningCenter.openConn();
                        learningCenter.deleteFromPlayerEruationLevel();
                        for( int i = 0 ; i < result.getPlayerLevelList().size() ; i ++ )
                            learningCenter.insertIntoPlayerEquationLevel(result.getPlayerLevelList().get(i));

                        if(learningCenter.isPlayerRecordExits(playerId))
                        {
                            learningCenter.updatePlayerPoints(result.getPoints(), playerId);
                        }
                        else
                        {
                            PlayerTotalPointsObj playerTotalPoints = new PlayerTotalPointsObj();
                            playerTotalPoints.setUserId(userId);
                            playerTotalPoints.setPlayerId(playerId);
                            playerTotalPoints.setCoins(0);
                            playerTotalPoints.setCompleteLevel(0);
                            playerTotalPoints.setPurchaseCoins(0);
                            playerTotalPoints.setTotalPoints(result.getPoints());
                            learningCenter.insertIntoPlayerTotalPoints(playerTotalPoints);
                        }

                        learningCenter.deleteFromPurchaseItem();
                        learningCenter.insertIntoPurchaseItem(purchaseItem);
                        learningCenter.closeConn();
                    }//end if
                }catch (Exception e) {
                    Log.e(TAG, "inside PlayerDataFromServer onPost()" +
                            "Error while loading unlock categorieds from Internet " + e.toString());

                }
            }
            new GetWordProblemLevelDetail(userId, playerId , pd , MainActivity.this).execute(null,null,null);

            //startActivity(new Intent(MainActivity.this , NewLearnignCenter.class));

            super.onPostExecute(result);
        }
    }

    /**
     * This get word problem level data for learning center
     * @author Yashwant Singh
     *
     */
    public class GetWordProblemLevelDetail extends AsyncTask<Void, Void, wordProblemLevelDetailTransferObj>{

        private String userId;
        private String playerId;
        private ProgressDialog pd;
        private Context context;

        public GetWordProblemLevelDetail(String userId , String playerId, ProgressDialog pd , Context context){
            this.userId 	= userId;
            this.playerId 	= playerId;
            this.pd         = pd;
            this.context    = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected wordProblemLevelDetailTransferObj doInBackground(Void... params) {
            LearnignCenterSchoolCurriculumServerOperation learnignCenterServerOperation
                    = new LearnignCenterSchoolCurriculumServerOperation();
            wordProblemLevelDetailTransferObj wordProblemLevelDetail = learnignCenterServerOperation
                    .getWordProblemDetail(userId, playerId);
            //changes for UI hang when we do it onPostExecute()
            if(wordProblemLevelDetail != null){
                SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl =
                        new SchoolCurriculumLearnignCenterimpl(context);
                schooloCurriculumImpl.openConnection();
                schooloCurriculumImpl.deleteFromWordProblemLevelStar();
                schooloCurriculumImpl.insertIntoWordProblemLevelStar
                        (wordProblemLevelDetail.getLevelData());
                schooloCurriculumImpl.closeConnection();
            }
            return wordProblemLevelDetail;
        }

        @Override
        protected void onPostExecute(wordProblemLevelDetailTransferObj wordProblemLevelDetail) {

            if(wordProblemLevelDetail != null){
                worlProblemLevelDetail = wordProblemLevelDetail.getUpdatedData();

                //added for assessment test details
                testDeatilList = wordProblemLevelDetail.getTestDeatilList();
                //end changes

                SharedPreferences sharedPrefPlayerInfo = context.getSharedPreferences(PLAYER_INFO, 0);
                int grade = sharedPrefPlayerInfo.getInt("grade" , 1);
                String updatedDateFromDatabase = null;

                SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl = new SchoolCurriculumLearnignCenterimpl(context);
                schooloCurriculumImpl.openConnection();
                /*schooloCurriculumImpl.deleteFromWordProblemLevelStar();
                schooloCurriculumImpl.insertIntoWordProblemLevelStar(wordProblemLevelDetail.getLevelData());*/

                //add if for changes for Spanish
                if(CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH){

                    if(!schooloCurriculumImpl.isWordProblemUpdateDetailsTableDataExist())
                        schooloCurriculumImpl.insertIntoWordProblemUpdateDetails(wordProblemLevelDetail.getUpdatedData());
                    boolean isQuestionLoaded = schooloCurriculumImpl.isQuestionLeaded(grade);
                    schooloCurriculumImpl.closeConnection();

                    if(isQuestionLoaded){
                        schooloCurriculumImpl = new SchoolCurriculumLearnignCenterimpl(context);
                        schooloCurriculumImpl.openConnection();
                        updatedDateFromDatabase = schooloCurriculumImpl.getUpdatedDate(grade);

                        if(CommonUtils.isUpdateDateChange(updatedDateFromDatabase,grade
                                ,wordProblemLevelDetail.getUpdatedData())){
                            String serverUpdatedDate = CommonUtils.getServerDate(grade ,
                                    wordProblemLevelDetail.getUpdatedData());
                            schooloCurriculumImpl.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
                            schooloCurriculumImpl.closeConnection();

                            new GetWordProblemQuestion(grade , context)
                                    .execute(null,null,null);
                        }
                        else{
                            schooloCurriculumImpl.closeConnection();
                            context.startActivity(new Intent(context , NewLearnignCenter.class));
                        }
                    }
                    else
                    {
                        //update date when question is updated
                        //changes
                        schooloCurriculumImpl = new SchoolCurriculumLearnignCenterimpl(context);
                        schooloCurriculumImpl.openConnection();
                        String serverUpdatedDate = CommonUtils.getServerDate(grade ,
                                wordProblemLevelDetail.getUpdatedData());
                        schooloCurriculumImpl.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
                        schooloCurriculumImpl.closeConnection();
                        //end changes

                        //Log.e(TAG, "Question not loaded");
                        new GetWordProblemQuestion(grade , context)
                                .execute(null,null,null);
                    }
                }else{
                    schooloCurriculumImpl.closeConnection();
                    callWhenUserLanguageIsSpanish(context , CommonUtils.SPANISH , grade);
                }
            }else{
                CommonUtils.showInternetDialog(context);
            }
            super.onPostExecute(wordProblemLevelDetail);
            pd.cancel();
        }
    }


    /**
     * This asynctask get questions from server according to grade if not loaded in database
     * @author Yashwant Singh
     *
     */
    public class GetWordProblemQuestion extends AsyncTask<Void, Void, QuestionLoadedTransferObj>{

        private int grade;
        private ProgressDialog pd;
        private Context context;
        ArrayList<String> imageNameList = new ArrayList<String>();

        public GetWordProblemQuestion(int grade , Context context){
            this.grade 		= grade;
            this.context 	= context;
        }

        @Override
        protected void onPreExecute() {

            Translation translation = new Translation(context);
            translation.openConnection();
            pd = CommonUtils.getProgressDialog(context, translation.getTranselationTextByTextIdentifier
                    ("alertPleaseWaitWeAreDownloading") , true);
            translation.closeConnection();
            pd.show();

            super.onPreExecute();
        }

        @Override
        protected QuestionLoadedTransferObj doInBackground(Void... params) {
            LearnignCenterSchoolCurriculumServerOperation serverObj =
                    new LearnignCenterSchoolCurriculumServerOperation();

            //changes for Spanish Changes
            QuestionLoadedTransferObj questionData = new QuestionLoadedTransferObj();
            if(CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH)
                questionData = serverObj.getWordProblemQuestions(grade);
            else
                questionData= serverObj.getWordProblemQuestionsForSpanish(grade, CommonUtils.SPANISH);

            if(questionData != null){
                //changes for Spanish Changes
                SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl = new SchoolCurriculumLearnignCenterimpl(context);
                schooloCurriculumImpl.openConnection();

                if(CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH){
                    //changes for dialog loading wheel
                    schooloCurriculumImpl.deleteFromWordProblemCategoriesbyGrade(grade);
                    schooloCurriculumImpl.deleteFromWordProblemsSubCategoriesByCategoryId(questionData.getCategoryList());
                    schooloCurriculumImpl.deleteFromWordProblemsQuestionsCategoryId(questionData.getCategoryList());

                    schooloCurriculumImpl.insertIntoWordProblemCategories(questionData.getCategoryList(), grade);
                    schooloCurriculumImpl.insertIntoWordProblemsSubCategories(questionData.getCategoryList());
                    schooloCurriculumImpl.insertIntoWordProblemsQuestions(questionData.getQuestionList());
                }else{
                    SpanishChangesImpl implObj = new SpanishChangesImpl(context);
                    implObj.openConn();

                    implObj.deleteFromWordProblemCategoriesbyGrade(grade);
                    implObj.deleteFromWordProblemsSubCategoriesByCategoryId(questionData.getCategoryList());
                    implObj.deleteFromWordProblemsQuestionsCategoryId(questionData.getCategoryList());

                    implObj.insertIntoWordProblemCategories(questionData.getCategoryList(), grade);
                    implObj.insertIntoWordProblemsSubCategories(questionData.getCategoryList());
                    implObj.insertIntoWordProblemsQuestions(questionData.getQuestionList());
                    implObj.closeConn();
                }

                for(int i = 0 ; i < questionData.getQuestionList().size() ; i ++ ){

                    WordProblemQuestionTransferObj questionObj = questionData.getQuestionList().get(i);

                    if(questionObj.getQuestion().contains(".png"))
                        imageNameList.add(questionObj.getQuestion());
                    if(questionObj.getOpt1().contains(".png"))
                        imageNameList.add(questionObj.getOpt1());
                    if(questionObj.getOpt2().contains(".png"))
                        imageNameList.add(questionObj.getOpt2());
                    if(questionObj.getOpt3().contains(".png"))
                        imageNameList.add(questionObj.getOpt3());
                    if(questionObj.getOpt4().contains(".png"))
                        imageNameList.add(questionObj.getOpt4());
                    if(questionObj.getOpt5().contains(".png"))
                        imageNameList.add(questionObj.getOpt5());
                    if(questionObj.getOpt6().contains(".png"))
                        imageNameList.add(questionObj.getOpt6());
                    if(questionObj.getOpt7().contains(".png"))
                        imageNameList.add(questionObj.getOpt7());
                    if(questionObj.getOpt8().contains(".png"))
                        imageNameList.add(questionObj.getOpt8());
                    if(!questionObj.getImage().equals(""))
                        imageNameList.add(questionObj.getImage());
                }

                if(!schooloCurriculumImpl.isImageTableExist()){
                    schooloCurriculumImpl.createSchoolCurriculumImageTable();
                }
                schooloCurriculumImpl.closeConnection();
                //end changes
            }
            return questionData;
        }

        @Override
        protected void onPostExecute(QuestionLoadedTransferObj questionData) {

            pd.cancel();

            if(questionData != null){
                DawnloadImagesFromserver serverObj = new DawnloadImagesFromserver(imageNameList, context);
                Thread imageDawnLoadThrad = new Thread(serverObj);
                imageDawnLoadThrad.start();

                context.startActivity(new Intent(context , NewLearnignCenter.class));
            }else{
                CommonUtils.showInternetDialog(context);
            }

            super.onPostExecute(questionData);
        }
    }


    /**
     * Update Player level data
     * @author Yashwant Singh
     *
     */
    private class AddLevelAsynTask extends AsyncTask<Void, Void, Void>
    {
        private ArrayList<PlayerEquationLevelObj> playerquationData = null;
        private ProgressDialog pd;

        AddLevelAsynTask(ArrayList<PlayerEquationLevelObj> playerquationData){
            this.playerquationData = playerquationData;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pd = CommonUtils.getProgressDialog(MainActivity.this);
            pd.show();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            LearningCenteServerOperation serverOpr = new LearningCenteServerOperation();
            serverOpr.addAllLevel("<levels>" + getLevelXml(playerquationData) + "</levels>");
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            pd.cancel();
            SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
            LearningCenterimpl impl = new LearningCenterimpl(MainActivity.this);
            impl.openConn();
            impl.deleteFromLocalPlayerLevels(sharedPreffPlayerInfo.getString("userId", "")
                    , sharedPreffPlayerInfo.getString("playerId", ""));
            impl.closeConn();

            //changes for word problem
            if(isWordProblemDataExistForLearnignCenter(sharedPreffPlayerInfo.getString("userId", "")
                    , sharedPreffPlayerInfo.getString("playerId", "")))	{
                new AddAllWordProblemLevel(sharedPreffPlayerInfo.getString("userId", "")
                        , sharedPreffPlayerInfo.getString("playerId", "")).execute(null,null,null);
            }else{
                new PlayerDataFromServer(sharedPreffPlayerInfo.getString("userId", ""),
                        sharedPreffPlayerInfo.getString("playerId", "")).execute(null,null,null);
            }

            super.onPostExecute(result);
        }
    }

    private String getLevelXml(ArrayList<PlayerEquationLevelObj> playerquationData)
    {
        StringBuilder xml = new StringBuilder("");

        for( int i = 0 ; i < playerquationData.size() ; i ++ )
        {
            xml.append("<userLevel>" +
                    "<uid>"+playerquationData.get(i).getUserId()+"</uid>"+
                    "<pid>"+playerquationData.get(i).getPlayerId()+"</pid>"+
                    "<eqnId>"+playerquationData.get(i).getEquationType()+"</eqnId>"+
                    "<level>"+playerquationData.get(i).getLevel()+"</level>"+
                    "<stars>"+playerquationData.get(i).getStars()+"</stars>"+
                    "</userLevel>");
        }

        return xml.toString();
    }

    /**
     * Add word problem level stars on server
     * @author Yashwant Singh
     *
     */
    private class AddAllWordProblemLevel extends AsyncTask<Void, Void, Void>{

        private String levelXml;
        private ProgressDialog pd;

        AddAllWordProblemLevel(String userId , String playerId){
            SchoolCurriculumLearnignCenterimpl schoolImpl =
                    new SchoolCurriculumLearnignCenterimpl(MainActivity.this);
            schoolImpl.openConnection();
            ArrayList<WordProblemLevelTransferObj> wordProblemLevelList
                    = schoolImpl.getWordProblemLevelData(userId, playerId);
            schoolImpl.closeConnection();
            ArrayList<PlayerEquationLevelObj> playerquationData = new ArrayList<PlayerEquationLevelObj>();

            for(int i = 0 ; i < wordProblemLevelList.size() ; i ++ ){
                PlayerEquationLevelObj playerData = new PlayerEquationLevelObj();
                playerData.setEquationType(wordProblemLevelList.get(i).getCategoryId());
                playerData.setLevel(wordProblemLevelList.get(i).getSubCategoryId());
                playerData.setPlayerId(playerId);
                playerData.setUserId(userId);
                playerData.setStars(wordProblemLevelList.get(i).getStars());
                playerquationData.add(playerData);
            }
            levelXml = getLevelXml(playerquationData);
        }

        @Override
        protected void onPreExecute() {
            pd = CommonUtils.getProgressDialog(MainActivity.this);
            pd.show();

            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            LearnignCenterSchoolCurriculumServerOperation serverObj
                    = new LearnignCenterSchoolCurriculumServerOperation();
            serverObj.addAllWordProblemLevels("<levels>" + levelXml + "</levels>");
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            pd.cancel();

            SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
            String userId = sharedPreffPlayerInfo.getString("userId", "");
            String playerId = sharedPreffPlayerInfo.getString("playerId", "");
            SchoolCurriculumLearnignCenterimpl schoolImpl =
                    new SchoolCurriculumLearnignCenterimpl(MainActivity.this);
            schoolImpl.openConnection();
            schoolImpl.deleteFromLocalWordProblemLevels(userId, playerId);
            schoolImpl.closeConnection();

            new PlayerDataFromServer(userId , playerId).execute(null,null,null);
        }
    }

    /**
     * use to check player for challenge
     */
    private void checkPlayer()
    {
        SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
        if(sheredPreference.getBoolean(IS_LOGIN, false))
        {
            UserPlayerOperation userPlayer = new UserPlayerOperation(this);
            if(!userPlayer.isUserPlayersExist())
            {
                SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0);
                if(sheredPreferencePlayer.getString(PLAYER_ID, "").equals("")){
                    UserRegistrationOperation userObj = new UserRegistrationOperation(this);
                    if(userObj.getUserData().getIsParent().equals("0")){//0 for teacher
                        Intent intent = new Intent(this,TeacherPlayer.class);
                        startActivity(intent);
                    }
                    else{
                        Intent intent = new Intent(this,LoginUserPlayerActivity.class);
                        startActivity(intent);
                    }
                }
                else{

                    if(BuildApp.BUILD_APP_TYPE == BuildApp.AMAZON){
                        SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(IS_CHECKED_PREFF, 0);
                        String userId = sharedPreffPlayerInfo.getString("userId", "");
                        if(this.getApplicationUnLockStatus(MAX_ITEM_ID_FOR_ALL_APP, userId) == 1){
                            Intent intent = new Intent(this,StudentChallengeActivity.class);
                            startActivity(intent);
                        }
                    }else{
                        if(CommonUtils.isInternetConnectionAvailable(this)){
                            new GetRequiredCoinsForPurchaseItem().execute(null,null,null);
                        }else{
                            CommonUtils.showInternetDialog(this);
                        }
                    }

                    /*if(CommonUtils.isInternetConnectionAvailable(this)){
                        new GetRequiredCoinsForPurchaseItem().execute(null,null,null);
                    }
                    else{
                        CommonUtils.showInternetDialog(this);
                    }*/
                }
            }
            else{
                Intent intent = new Intent(this,LoginUserCreatePlayer.class);
                startActivity(intent);
            }
        }
        else{
            DialogGenerator dg = new DialogGenerator(this);
            Translation translate = new Translation(this);
            translate.openConnection();
            dg.generateWarningDialog(translate.getTranselationTextByTextIdentifier
                    ("alertMsgYouMustRegisterLoginToAccessThisFeature")+"");
            translate.closeConnection();
        }

    }

    /**
     * This method check for application is unlock or not
     * @param itemId
     * @param userId
     * @return
     */
    private int getApplicationUnLockStatus(int itemId,String userId){
        int appStatus;
        LearningCenterimpl learnignCenterImpl = new LearningCenterimpl(this);
        learnignCenterImpl.openConn();
        appStatus = learnignCenterImpl.getAppUnlockStatus(itemId,userId);
        learnignCenterImpl.closeConn();
        return appStatus;
    }

    /**
     * This class get coins from server
     * @author Shilpi
     *
     */
    class GetRequiredCoinsForPurchaseItem extends AsyncTask<Void, Void, Void>
    {
        private String apiString = null;
        private CoinsFromServerObj coindFromServer;
        private ProgressDialog pg = null;
        String userId;
        String playerId;

        GetRequiredCoinsForPurchaseItem()
        {
			/*UserRegistrationOperation userObj1 = new UserRegistrationOperation(MainActivity.this);
			RegistereUserDto regUserObj = userObj1.getUserData();
			String userId = regUserObj.getUserId();*/

            SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(IS_CHECKED_PREFF, 0);
            userId   = sharedPreffPlayerInfo.getString("userId", "");
            playerId = sharedPreffPlayerInfo.getString("playerId", "");
            apiString = "userId="+userId+"&playerId="+playerId;
        }

        @Override
        protected void onPreExecute()
        {
            pg = CommonUtils.getProgressDialog(MainActivity.this);
            pg.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            LearningCenteServerOperation learnignCenterOpr = new LearningCenteServerOperation();
            coindFromServer = learnignCenterOpr.getSubscriptionInfoForUser(apiString);
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            pg.cancel();

            if(coindFromServer != null){
                //update app status in local database
                if(coindFromServer.getAppUnlock() != -1){

                    ArrayList<PurchaseItemObj> purchaseItem = new ArrayList<PurchaseItemObj>();
                    PurchaseItemObj purchseObj = new PurchaseItemObj();
                    purchseObj.setUserId(userId);
                    purchseObj.setItemId(100);
                    purchseObj.setStatus(coindFromServer.getAppUnlock());
                    purchaseItem.add(purchseObj);

                    LearningCenterimpl learningCenter = new LearningCenterimpl(MainActivity.this);
                    learningCenter.openConn();

                    learningCenter.deleteFromPurchaseItem(userId , 100);
                    learningCenter.insertIntoPurchaseItem(purchaseItem);
                    learningCenter.closeConn();
                }

                if(coindFromServer.getAppUnlock() == 1)
                {
                    UserRegistrationOperation userObj = new UserRegistrationOperation(MainActivity.this);

                    if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
                    {
						/*Intent intent = new Intent(MainActivity.this,TeacherChallengeActivity.class);
					startActivity(intent);*/

                        //changes for remove screen
						/*UserRegistrationOperation userObj1 = new UserRegistrationOperation(MainActivity.this);
					RegistereUserDto regUserObj = userObj1.getUserData();
					String userId = regUserObj.getUserId();*/

                        SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0);
						/*Editor editor = sheredPreferencePlayer.edit();
					editor.putString("userId", userId);
					editor.commit();*/

                        if(sheredPreferencePlayer.getString(PLAYER_ID, "").equals(""))
                        {
                            Intent intent = new Intent(MainActivity.this,StudentChallengeActivity.class);
                            intent.putExtra("isTeacher", true);
                            startActivity(intent);
							/*if(regUserObj.getIsParent().equals("0"))//0 for teacher
						{
							Intent intent = new Intent(MainActivity.this,TeacherPlayer.class);
							intent.putExtra("calling", "MainActivity");
							startActivity(intent);
						}
						else
						{
							Intent intent = new Intent(MainActivity.this,LoginUserPlayerActivity.class);
							intent.putExtra("calling", "MainActivity");
							startActivity(intent);
						}*/
                        }
                        else
                        {
                            Intent intent = new Intent(MainActivity.this,StudentChallengeActivity.class);
                            //intent.putExtra("isTeacher", false);
                            startActivity(intent);
                        }
                        //end changes
                    }
                    else
                    {
                        Intent intent = new Intent(MainActivity.this,StudentChallengeActivity.class);
                        startActivity(intent);
                    }
                }
                else
                {
                    DialogGenerator dg = new DialogGenerator(MainActivity.this);
                    dg.generateDailogForSchoolCurriculumUnlock(coindFromServer , userId , playerId
                            , coindFromServer.getAppUnlock());
                }
            }else{
                CommonUtils.showInternetDialog(MainActivity.this);
            }
            super.onPostExecute(result);
        }
    }

    /**
     * This AsyncTask get updated equations details for friendzy
     * @author Yashwant Singh
     *
     */
    class GetUpdatedEquationsForChallenge extends AsyncTask<Void, Void, Void>{

        private String challengerId;
        private String playerId;
        GetUpdatedEquationsForChallenge(String userId , String playerId , String challengerId){
			/*challengerId = CommonUtils.getActivePlayerChallengerId(userId, playerId);*/
            this.challengerId = challengerId;
            this.playerId = playerId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            FriendzyServerOperation friendzyServerObj = new FriendzyServerOperation();
            equationsObj = friendzyServerObj.getUpdatedEquationsForChallenge(challengerId);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            try{
                CommonUtils.updateEndDateForFriendzyChallenge(playerId, equationsObj.getEndDate(), challengerId);
            }catch (Exception e) {
                Log.e(TAG, "Error while server response " + e.toString());
            }
            super.onPostExecute(result);
        }
    }

    //added for friendzy challenge
    /**
     * This method set the friendzy challenge equation data
     * @param userId
     * @param playerId
     */
    private void setActivePlayerDataForFriendzyChallenge(String userId , String playerId){
        //changes for Friendzy challenge
        if(CommonUtils.isActivePlayer(userId,playerId)) {
            //get challenger id
            String challengerId = CommonUtils.getActivePlayerChallengerId
                    (userId, playerId);

            if(CommonUtils.isInternetConnectionAvailable(MainActivity.this)){
                new GetUpdatedEquationsForChallenge(userId
                        , playerId , challengerId).execute(null,null,null);
            }else{
                try{
                    equationsObj.setEquationIds(CommonUtils.getActivePlayerChallengerId
                            (playerId , challengerId));
                }catch(Exception e){

                }
            }
        }
        //end changes
    }

    //changes for Spanish
    /**
     * This method call when user language is Spanish
     * This method for downloading question from server for Spanish Language
     */
    private void callWhenUserLanguageIsSpanish(Context context , int lang , int grade){
        try {
            SpanishChangesImpl spanishImplObj = new SpanishChangesImpl(context);
            spanishImplObj.openConn();
            //CommonUtils.CheckWordProblemSpanishTable(context);
            boolean isSpanishQuestionLoaded = spanishImplObj.isQuestionLeaded(grade);
            if(isSpanishQuestionLoaded){
                if(CommonUtils.isInternetConnectionAvailable(this)){
                    new GetWordQuestionsUpdateDatesLang(lang , context , false , grade , 0).execute(null,null,null);
                }
            }else{
                ArrayList<UpdatedInfoTransferObj> updatedList =
                        new GetWordQuestionsUpdateDatesLang(lang , context , true , grade , 0).execute(null,null,null).get();
                spanishImplObj.insertIntoWordProblemUpdateDetails(updatedList);
                //Log.e(TAG, "Question not loaded");
                new GetWordProblemQuestion(grade , context)
                        .execute(null,null,null);
            }
            spanishImplObj.closeConn();

        } catch (InterruptedException e) {
            Log.e(TAG, "inside callWhenUserLanguageIsSpanish " + e.toString());
        } catch (ExecutionException e) {
            Log.e(TAG, "inside callWhenUserLanguageIsSpanish " + e.toString());
        }
    }
    //end changes


    //chages for temp player download question
    /**
     * This class get word problem updated dates from server
     * @author Yashwant Singh
     *
     */
    @SuppressWarnings("unused")
    private class GetWordProblemQuestionUpdatedDate extends AsyncTask<Void, Void, ArrayList<UpdatedInfoTransferObj>>{
        private int grade;
        private ProgressDialog pd;
        private boolean isFisrtTimeCallForGrade;

        GetWordProblemQuestionUpdatedDate(int grade , boolean isFisrtTimeCallForGrade){
            this.grade = grade;
            this.isFisrtTimeCallForGrade = isFisrtTimeCallForGrade;
        }

        @Override
        protected void onPreExecute() {
            if(isFisrtTimeCallForGrade == false){
                pd = CommonUtils.getProgressDialog(MainActivity.this);
                pd.show();
            }
            super.onPreExecute();
        }

        @Override
        protected ArrayList<UpdatedInfoTransferObj>  doInBackground(Void... params) {
            SingleFriendzyServerOperationForWordProblem serverObj =
                    new SingleFriendzyServerOperationForWordProblem();
            //changes for Spanish
            ArrayList<UpdatedInfoTransferObj>  updatedList = new ArrayList<UpdatedInfoTransferObj>();
            if(CommonUtils.getUserLanguageCode(MainActivity.this) == CommonUtils.ENGLISH)
                updatedList = serverObj.getWordProbleQuestionUpdatedData();
            else//for Spanish
                updatedList = serverObj.getWordProbleQuestionUpdatedDateForSpanish(CommonUtils.SPANISH);
            return updatedList;
        }

        @Override
        protected void onPostExecute(ArrayList<UpdatedInfoTransferObj>  updatedList) {
            if(isFisrtTimeCallForGrade == false){
                pd.cancel();
            }

            if(updatedList != null){
                SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl =
                        new SchoolCurriculumLearnignCenterimpl(MainActivity.this);
                schooloCurriculumImpl.openConnection();

                //changes for Spanish
                SpanishChangesImpl implObj = new SpanishChangesImpl(MainActivity.this);
                implObj.openConn();

                if(isFisrtTimeCallForGrade){
                    //changes for Spanish
                    if(CommonUtils.getUserLanguageCode(MainActivity.this) == CommonUtils.ENGLISH){
                        if(!schooloCurriculumImpl.isWordProblemUpdateDetailsTableDataExist()){
                            schooloCurriculumImpl.insertIntoWordProblemUpdateDetails(updatedList);
                            schooloCurriculumImpl.closeConnection();
                            implObj.closeConn();
                        }else{
                            String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
                            schooloCurriculumImpl.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
                            schooloCurriculumImpl.closeConnection();
                            implObj.closeConn();
                        }
                    }else{//for Spanish
                        if(!implObj.isWordProblemUpdateDetailsTableDataExist()){
                            implObj.insertIntoWordProblemUpdateDetails(updatedList);
                            schooloCurriculumImpl.closeConnection();
                            implObj.closeConn();
                        }else{
                            String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
                            implObj.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
                            schooloCurriculumImpl.closeConnection();
                            implObj.closeConn();
                        }
                    }
                }
            }else{
                if(isFisrtTimeCallForGrade == false){
                    CommonUtils.showInternetDialog(MainActivity.this);
                }
            }
            super.onPostExecute(updatedList);
        }
    }

    @Override
    protected void onResume() {

        if(CommonUtils.isReadyToGetNewFrequesncyFromSerevr(this)
                && CommonUtils.isInternetConnectionAvailable(this)){
            new GetAdsFrequencies(this).execute(null,null,null);
        }
        //for show ad dialog
        CommonUtils.showAdDialog(this);
        //end ad dialog

        //this.setNumerOfActiveHomeWorkNotification();
        this.setSubscriptionRemainingDays();

        //for top 100
        this.setDataForTop100();
        this.setNumberOfActiveOpponent();
        //Only for Homework friendzy
        this.setVisibilityOfRequestATutorButton();

        MathFriendzyHelper.getLiveAppVersionAndCheckForUpdate(this , false);
        super.onResume();
    }

    /**
     * This method call when user click on HomeWork
     */
    private void clickOnHomeWork(){
        SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
        if(sheredPreference.getBoolean(IS_LOGIN, false))
        {
            UserPlayerOperation userPlayer = new UserPlayerOperation(this);
            if(!userPlayer.isUserPlayersExist())
            {
                SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0);
                if(sheredPreferencePlayer.getString(PLAYER_ID, "").equals("")){
                    UserRegistrationOperation userObj = new UserRegistrationOperation(this);
                    if(userObj.getUserData().getIsParent().equals("0")){//0 for teacher
                        Intent intent = new Intent(this,TeacherPlayer.class);
                        startActivity(intent);
                    }
                    else{
                        Intent intent = new Intent(this,LoginUserPlayerActivity.class);
                        startActivity(intent);
                    }
                }
                else{
                    MathFriendzyHelper.saveLoginPlayerDataIntoSharedPreff(this);
                    //startActivity(new Intent(this , ActHomeWork.class));
                    this.checkForUnlock(MathFriendzyHelper.DIALOG_SCHOOL_HOMEWORK_QUIZZ);
                }
            }
            else{
                Intent intent = new Intent(this,LoginUserCreatePlayer.class);
                startActivity(intent);
            }
        }
        else{
            /*DialogGenerator dg = new DialogGenerator(this);
            Translation translate = new Translation(this);
            translate.openConnection();
            dg.generateWarningDialog(translate.getTranselationTextByTextIdentifier
                    ("alertMsgYouMustRegisterLoginToAccessThisFeature")+"");
            translate.closeConnection();*/
            MathFriendzyHelper.showLoginRegistrationDialog(this , lblYouMustRegisterLoginToAccess);
        }
    }


    /**
     * This method set the number of active home work notification
     * added in the second phase , Student Phase
     */
    private void setNumerOfActiveHomeWorkNotification(){
        txtNotification.setVisibility(TextView.GONE);
        /*try{
            if(MathFriendzyHelper.isUserLogin(this)){
                UserPlayerDto userPlayer = MathFriendzyHelper.getSelectedPlayerDataById
                        (this, MathFriendzyHelper.getSelectedPlayerID(this));
                int numberOfActiveWork = MathFriendzyHelper.getNumberOfActiveHomeWork
                        (this, userPlayer.getParentUserId(), userPlayer.getPlayerid());
                int numberOfTutorResponses = MathFriendzyHelper.getTutorAreaResponses(this ,
                        userPlayer.getParentUserId(), userPlayer.getPlayerid());
                numberOfActiveWork = numberOfActiveWork + numberOfTutorResponses;
                if(numberOfActiveWork != 0){
                    txtNotification.setVisibility(TextView.VISIBLE);
                    txtNotification.setText(numberOfActiveWork + "");
                }else{
                    txtNotification.setVisibility(TextView.GONE);
                }
            }else{
                txtNotification.setVisibility(TextView.GONE);
            }
        }catch(Exception e){
            e.printStackTrace();
            txtNotification.setVisibility(TextView.GONE);
        }*/
    }

    private void setNumberOfActiveSchoolInout(int input){
        this.setHomeworkInoutForMathPlus();
        if(input > 0){
            txtNotification.setVisibility(TextView.VISIBLE);
            txtNotification.setText(input + "");
        }else{
            txtNotification.setVisibility(TextView.GONE);
        }
    }

    /**
     * This method return the user player data
     * @return
     */
    private UserPlayerDto getPlayerData(){
        try{
            return MathFriendzyHelper.getSelectedPlayerDataById
                    (this, MathFriendzyHelper.getSelectedPlayerID(this));
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    private void setNumberOfActiveOpponent() {
        try{
            txtNotification.setVisibility(TextView.GONE);
            txtHomeworkQuizActiveNotificationPlus.setVisibility(TextView.GONE);
            final UserPlayerDto selectedPlayerData = this.getPlayerData();
            if(MathFriendzyHelper.isUserLogin(this) &&
                    CommonUtils.isInternetConnectionAvailable(this) && selectedPlayerData != null
                    && !CommonUtils.isActiveHomeSchoolNotificationDownloaded){
                CommonUtils.isActiveHomeSchoolNotificationDownloaded = true;
                TutorAreaResponsesParam param = new TutorAreaResponsesParam();
                param.setAction("tutorAreaResponses");
                param.setUserId(selectedPlayerData.getParentUserId());
                param.setPlayerId(selectedPlayerData.getPlayerid());
                new MyAsyckTask(ServerOperation
                        .createPostRequestForTutorAreaResponses(param)
                        , null, ServerOperationUtil.GET_TUTOR_AREA_RESPONSES, this,
                        new HttpResponseInterface() {
                            @Override
                            public void serverResponse(HttpResponseBase httpResponseBase,
                                                       int requestCode) {
                                if(requestCode == ServerOperationUtil.GET_TUTOR_AREA_RESPONSES) {
                                    TutorAreaResponse response = (TutorAreaResponse) httpResponseBase;
                                    if (response.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)) {
                                        MathFriendzyHelper.saveTutorAreaResponse(MainActivity.this,
                                                selectedPlayerData.getParentUserId()
                                                , selectedPlayerData.getPlayerid(),
                                                response.getTutorResponses());
                                        MathFriendzyHelper.saveNumberOfActiveHomeWorkFromServer
                                                (MainActivity.this, selectedPlayerData.getParentUserId(),
                                                        selectedPlayerData.getPlayerid(),
                                                        response.getHwInput());
                                        MathFriendzyHelper.saveTutorSessionResponse(MainActivity.this,
                                                selectedPlayerData.getParentUserId()
                                                , selectedPlayerData.getPlayerid(),
                                                response.getTutorSessionResponses());
                                        setNumberOfActiveSchoolInout(getTotalSchoolInput(selectedPlayerData));
                                    }
                                }
                            }
                        }, ServerOperationUtil.SIMPLE_DIALOG , true ,
                        getString(R.string.please_wait_dialog_msg))
                        .execute();
            }else{
                try {
                    if (MathFriendzyHelper.isUserLogin(this)
                            && selectedPlayerData != null) {
                        this.setNumberOfActiveSchoolInout
                                (this.getTotalSchoolInput(selectedPlayerData));
                    }
                }catch(Exception e){
                    txtNotification.setVisibility(TextView.GONE);
                    e.printStackTrace();
                }
            }
        }catch(Exception e){
            txtNotification.setVisibility(TextView.GONE);
        }
    }

    private int getTotalSchoolInput(UserPlayerDto selectedPlayerData){
        int totalInput = 0;
        try{
            totalInput = totalInput + MathFriendzyHelper.getTutorAreaResponses(this,
                    selectedPlayerData.getParentUserId()
                    , selectedPlayerData.getPlayerid());
            totalInput = totalInput + MathFriendzyHelper.
                    getTutorSessionResponses(this,
                            selectedPlayerData.getParentUserId()
                            , selectedPlayerData.getPlayerid());
            totalInput = totalInput + MathFriendzyHelper.
                    getNumberOfActiveHomeWorkFromServer(this,
                            selectedPlayerData.getParentUserId()
                            , selectedPlayerData.getPlayerid());
        }catch(Exception e){
            e.printStackTrace();
        }
        return totalInput;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){
            switch(requestCode){
                case START_TUTORIAL_ACT_CODE:
                    if(data.getBooleanExtra("isShowDialog", false)){
                        MathFriendzyHelper.showFreeSubscriptionRegisterDialog
                                (this, new YesNoListenerInterface() {

                                    @Override
                                    public void onYes() {//For register
                                        startActivity(new Intent(MainActivity.this,
                                                RegistrationStep1.class));
                                    }

                                    @Override
                                    public void onNo() {//For No Thanks

                                    }
                                });
                    }
                    break;
            }
        }
    }

    /**
     * This method create the html text
     * @return
     */
    private String getRemainingDaysText(String firstText , int days , String secondtext){
        String htmlText = "<html>" + firstText
                + " " + days + " "
                + secondtext + "</html>";
        return htmlText;
    }

    /**
     * Set the subscription remaining days
     */
    private void setSubscriptionRemainingDays(){
        try{
            if(MathFriendzyHelper.isUserLogin(this)){
                boolean isPurchase = false;
                String subscriptionDateString = MathFriendzyHelper.getPurchaseSubscriptionDate(this);
                String expireDateString = MathFriendzyHelper.getSubscriptionExpireDate(this);
                Date subscriptionDate = MathFriendzyHelper.getValidateDate
                        (subscriptionDateString , CURRENT_SUBSCRIPTION_DATE_FORMAT);
                if(subscriptionDate != null) {
                    subscriptionDate.setHours(0);
                    subscriptionDate.setMinutes(0);
                    subscriptionDate.setSeconds(0);
                    Date expireDate = MathFriendzyHelper.getValidateDate
                            (expireDateString, CURRENT_EXPIRE_DATE_FORMAT);
                    if (!MathFriendzyHelper.isEmpty(subscriptionDateString)) {
                        if (subscriptionDate.compareTo(expireDate) > 0) {
                            isPurchase = true;
                        }
                    }
                }

                if(CommonUtils.LOG_ON)
                    Log.e(TAG , "date subscriptionDateString" + subscriptionDateString
                            + " expireDateString " + expireDateString + " " + isPurchase);

                if(isPurchase){
                    MathFriendzyHelper.initializeIsSuscriptionPurchase(true);
                    int days = MathFriendzyHelper.getFreeSubscriptionRemainingDays(this ,
                            subscriptionDateString , CURRENT_SUBSCRIPTION_DATE_FORMAT);
                    if (MathFriendzyHelper.getUserAccountType(this) == MathFriendzyHelper.TEACHER) {
                        if(days > 0){
                            txtFreeSubscriptionText.setText(lblYourSchoolSubscriptionExpire + " "
                                    + MathFriendzyHelper.formatDataInGivenFormat(subscriptionDateString,
                                    CURRENT_SUBSCRIPTION_DATE_FORMAT, REQUIRED_EXPIRE_DATE_FORMAT));
                            this.setVisibilityOfSubscriptionTextLayout(true);
                        }else{
                            this.setVisibilityOfSubscriptionTextLayout(false);
                        }
                    }else{
                        this.setVisibilityOfSubscriptionTextLayout(false);
                    }
                }else {
                    MathFriendzyHelper.initializeIsSuscriptionPurchase(false);
                    int days = MathFriendzyHelper.getFreeSubscriptionRemainingDays(this);
                    if (days > 0) {
                        /*String remainingDaysText = getRemainingDaysText
                                (lblFreeStudentTeacherFunctionExpire , days , lblDays);
                        txtFreeSubscriptionText.setText(Html.fromHtml(remainingDaysText));*/
                        txtFreeSubscriptionText.setText(lblFreeStudentTeacherFunctionExpire + " "
                                + MathFriendzyHelper.formatDataInGivenFormat(expireDateString,
                                CURRENT_EXPIRE_DATE_FORMAT, REQUIRED_EXPIRE_DATE_FORMAT));
                        this.setVisibilityOfSubscriptionTextLayout(true);
                    } else {
                        this.setVisibilityOfSubscriptionTextLayout(false);
                    }
                }
            }else{
                txtFreeSubscriptionText.setText(lblRegisterLoginToUseSchoolFree);
                this.setVisibilityOfSubscriptionTextLayout(false);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    /**
     * Set the subscription text layout visibility
     * @param isShow
     */
    private void setVisibilityOfSubscriptionTextLayout(boolean isShow){
        if(isShow){
            freeSubscriptiontextLayout.setVisibility(RelativeLayout.VISIBLE);
        }else{
            freeSubscriptiontextLayout.setVisibility(RelativeLayout.GONE);
        }
    }


    /**
     * Set data for top 100
     */
    private void setDataForTop100(){
        try{
            SharedPreferences prefs = getSharedPreferences("name",Context.MODE_PRIVATE);

            long date1 					= new Date().getTime();
            long date2 					= prefs.getLong("date",date1);
            long startTime				= prefs.getLong("start",0);
            student_flag				= prefs.getBoolean("student_flag", false);
            school_flag					= prefs.getBoolean("school_flag", false);
            teacher_flag				= prefs.getBoolean("teacher_flag", false);
            long diff 					= Math.abs(date1 - date2);
            startTime 					= startTime - diff;

            if(startTime/1000 <= 0)
            {
                student_flag = false;
                school_flag = false;
                teacher_flag = false;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Show the student top 100
     */
    private void showTop100ForStudent(){
        try{
            //create table top100 if not exist
            TopListDatabase db 	= new TopListDatabase();
            db.createTableTop100(this);

            Intent intent = new Intent(this, Top100ListActivity.class);
            intent.putExtra("id", 1);
            if(!CommonUtils.isInternetConnectionAvailable(this) && !student_flag)
                CommonUtils.showInternetDialog(this);
            else if(student_flag || !CommonUtils.isInternetConnectionAvailable(this))
                startActivity(intent);
            else if(!student_flag && CommonUtils.isInternetConnectionAvailable(this))
                new JsonAsynTask(this, 1).execute(null,null,null);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * This method call when user click on HomeWork
     */
    private void checkForStudentSelection(String checkFor){
        SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
        if(sheredPreference.getBoolean(IS_LOGIN, false)){
            UserPlayerOperation userPlayer = new UserPlayerOperation(this);
            if(!userPlayer.isUserPlayersExist())
            {
                SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0);
                if(sheredPreferencePlayer.getString(PLAYER_ID, "").equals("")){
                    UserRegistrationOperation userObj = new UserRegistrationOperation(this);
                    if(userObj.getUserData().getIsParent().equals("0")){//0 for teacher
                        Intent intent = new Intent(this,TeacherPlayer.class);
                        startActivity(intent);
                    }else{
                        Intent intent = new Intent(this,LoginUserPlayerActivity.class);
                        startActivity(intent);
                    }
                }else{
                    if(checkFor.equalsIgnoreCase(MathFriendzyHelper.DIALOG_PROFESSIONAL_TUTORING)){
                        this.goForProfessionalTutoring();
                    }else if(checkFor.equalsIgnoreCase(MathFriendzyHelper.DIALOG_RESOURCES)){
                        this.goForResourcesScreen();
                    }else if(checkFor.equalsIgnoreCase(MathFriendzyHelper.DIALOG_TUTORING_SESSION)){
                        this.goOnTutoringSession();
                    }
                }
            }else{
                Intent intent = new Intent(this,LoginUserCreatePlayer.class);
                startActivity(intent);
            }
        }else{
            if(checkFor.equalsIgnoreCase(MathFriendzyHelper.DIALOG_PROFESSIONAL_TUTORING)){
                this.goForProfessionalTutoring();
            }else {
                MathFriendzyHelper.showLoginRegistrationDialog(this, lblYouMustRegisterLoginToAccess);
            }
        }
    }

    private void clickOnProfessionalTutor() {
        this.checkForStudentSelection(MathFriendzyHelper.DIALOG_PROFESSIONAL_TUTORING);
    }

    private void goForProfessionalTutoring(){
        //startActivity(new Intent(this , ActProfessionalTutors.class));
        startActivity(new Intent(this , ActTryItFreeNow.class));
    }



    //math plus changes
    private void setWidgetsReferenceForMathPlus(){
        mathPlusLayout = (RelativeLayout) findViewById(R.id.mathPlusLayout);
        lytLinearMainMiddle = (LinearLayout) findViewById(R.id.lytLinearMainMiddle);
        homeworkAndQuizzesLayoutPlus = (RelativeLayout) findViewById(R.id.homeworkAndQuizzesLayoutPlus);
        learningResourcesLayoutPlus = (RelativeLayout) findViewById(R.id.learningResourcesLayoutPlus);
        liveTutorLayoutPlus = (RelativeLayout) findViewById(R.id.liveTutorLayoutPlus);
        funWithMathLayoutPlus = (RelativeLayout) findViewById(R.id.funWithMathLayoutPlus);
        teacherFunctionsLayoutPlus = (RelativeLayout) findViewById(R.id.teacherFunctionsLayoutPlus);
        tutoringSessionLayoutMathPlus = (RelativeLayout) findViewById(R.id.tutoringSessionLayoutMathPlus);

        lblTheGameOfMathPlus = (TextView) findViewById(R.id.lblTheGameOfMathPlus);
        lblHomeworkAndQuizzessPlus = (TextView) findViewById(R.id.lblHomeworkAndQuizzessPlus);
        lblLearningResourcesPlus = (TextView) findViewById(R.id.lblLearningResourcesPlus);
        lblLiveTutorPlus = (TextView) findViewById(R.id.lblLiveTutorPlus);
        lblFunWithMathPlus = (TextView) findViewById(R.id.lblFunWithMathPlus);
        lblTeacherFunctionPlus = (TextView) findViewById(R.id.lblTeacherFunctionPlus);
        lblTTutoringSessionMathPlus = (TextView) findViewById(R.id.lblTTutoringSessionMathPlus);

        txtLiveTutorPatentPendingPlus = (TextView) findViewById(R.id.txtLiveTutorPatentPendingPlus);
        txtHomeworkQuizActiveNotificationPlus = (TextView)
                findViewById(R.id.txtHomeworkQuizActiveNotificationPlus);
        mainActivityFullLayout = (RelativeLayout) findViewById(R.id.mainActivityFullLayout);
        multiFriendzyLayoutMathPlus = (RelativeLayout) findViewById(R.id.multiFriendzyLayoutMathPlus);
        lblMultiFriendzyMathPlus = (TextView) findViewById(R.id.lblMultiFriendzyMathPlus);
        //this.setVisibilityfTeacherFunctionButtonPlus();
    }

    private void setListenetOnMathPlusLayout(){
        teacherFunctionsLayoutPlus.setOnClickListener(this);
        homeworkAndQuizzesLayoutPlus.setOnClickListener(this);
        liveTutorLayoutPlus.setOnClickListener(this);
        funWithMathLayoutPlus.setOnClickListener(this);
        learningResourcesLayoutPlus.setOnClickListener(this);
        tutoringSessionLayoutMathPlus.setOnClickListener(this);
        multiFriendzyLayoutMathPlus.setOnClickListener(this);
    }

    private void setMathPlusWidgetsText(Translation transeletion){
        try{
            lblTheGameOfMathPlus.setText(transeletion
                    .getTranselationTextByTextIdentifier("lblTheGameOfMath") + "!");
            lblHomeworkAndQuizzessPlus.setText(transeletion
                    .getTranselationTextByTextIdentifier("lblAssignments")
                    + "/" + transeletion.getTranselationTextByTextIdentifier("lblQuizzes"));
            lblLearningResourcesPlus.setText(transeletion
                    .getTranselationTextByTextIdentifier("navigationTitleResources"));
            lblLiveTutorPlus.setText(transeletion.getTranselationTextByTextIdentifier("lblRequestATutor"));
            lblFunWithMathPlus.setText(transeletion.getTranselationTextByTextIdentifier("lblFunWithMath"));
            //lblFunWithMathPlus.setText(transeletion.getTranselationTextByTextIdentifier("lblLearningCenter"));
            lblTeacherFunctionPlus.setText(transeletion
                    .getTranselationTextByTextIdentifier("lblReportsFor"));
            txtLiveTutorPatentPendingPlus.setText(transeletion
                    .getTranselationTextByTextIdentifier("lblPatentPending"));
            lblTTutoringSessionMathPlus.setText(transeletion
                    .getTranselationTextByTextIdentifier("btnTutoringSession"));
            /*lblTTutoringSessionMathPlus.setText(transeletion
                    .getTranselationTextByTextIdentifier("lblSingleFriendzy"));*/
            lblMultiFriendzyMathPlus.setText(transeletion
                    .getTranselationTextByTextIdentifier("lblBestOf5Friendzy"));
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /*private void setVisibilityfTeacherFunctionButtonPlus(){
        if(!MathFriendzyHelper.isUserLogin(this)){
            teacherFunctionsLayoutPlus.setVisibility(RelativeLayout.VISIBLE);
            return ;
        }

        if(MathFriendzyHelper.getUserAccountType(this) ==
                MathFriendzyHelper.TEACHER){
            teacherFunctionsLayoutPlus.setVisibility(RelativeLayout.VISIBLE);
        }else{
            teacherFunctionsLayoutPlus.setVisibility(RelativeLayout.GONE);
        }
    }*/

    private void clickOnTeacherFunctionPlus() {
        Intent intent = new Intent(this , ReportForPracticeAndAssessment.class);
        startActivity(intent);
    }

    private void clickOnFunWithMath(){
        Intent intent = new Intent(this , ActFunWithMath.class);
        startActivity(intent);
    }

    private void setHomeworkInoutForMathPlus() {
        if(MathFriendzyHelper.isUserLogin(this)) {
            UserPlayerDto selectedPlayerData = this.getPlayerData();
            int homeworkInout = MathFriendzyHelper.getNumberOfActiveHomeWorkFromServer(this,
                    selectedPlayerData.getParentUserId()
                    , selectedPlayerData.getPlayerid());
            if(homeworkInout > 0) {
                txtHomeworkQuizActiveNotificationPlus.setVisibility(TextView.VISIBLE);
                txtHomeworkQuizActiveNotificationPlus.setText(homeworkInout + "");
            }else{
                txtHomeworkQuizActiveNotificationPlus.setVisibility(TextView.GONE);
            }
        }else{
            txtHomeworkQuizActiveNotificationPlus.setVisibility(TextView.GONE);
        }
    }

    private void clickOnResources(){
        //this.checkForStudentSelection(MathFriendzyHelper.DIALOG_RESOURCES);
        this.goForResourcesScreen();
    }

    private void goForResourcesScreen(){
        //startActivity(new Intent(this , ActResourceHome.class));
        startActivity(new Intent(this , ActLessonCategories.class));
    }

    private void setWidgetsReferenceForMathTutorPlus() {
        lytLinearMainMiddleTutorPlus = (LinearLayout) findViewById(R.id.lytLinearMainMiddleTutorPlus);
        mathTutorPlusLayout = (RelativeLayout) findViewById(R.id.mathTutorPlusLayout);
        requestATutorLayoutTutorPlus = (RelativeLayout) findViewById(R.id.requestATutorLayoutTutorPlus);
        instructionVedionLayoutTutorPlus = (RelativeLayout) findViewById(R.id.instructionVedionLayoutTutorPlus);
        mathExcersizeLayoutTutorPlus = (RelativeLayout) findViewById(R.id.mathExcersizeLayoutTutorPlus);
        worldCompletetionLayoutTutorPlus = (RelativeLayout) findViewById(R.id.worldCompletetionLayoutTutorPlus);
        tutoringSessionLayoutTutorPlus = (RelativeLayout) findViewById(R.id.tutoringSessionLayoutTutorPlus);

        lblrequestATutorsTutorPlus = (TextView) findViewById(R.id.lblrequestATutorsTutorPlus);
        lblInstructionVedioTutorPlus = (TextView) findViewById(R.id.lblInstructionVedioTutorPlus);
        lblMathExcersizePlusPlus = (TextView) findViewById(R.id.lblMathExcersizePlusPlus);
        lblWorldCompletetinTutorPlus = (TextView) findViewById(R.id.lblWorldCompletetinTutorPlus);
        lblTTutoringSessionTutorPlus = (TextView) findViewById(R.id.lblTTutoringSessionTutorPlus);
    }

    private void setListenetOnMathTutorPlusLayout(){
        requestATutorLayoutTutorPlus.setOnClickListener(this);
        instructionVedionLayoutTutorPlus.setOnClickListener(this);
        mathExcersizeLayoutTutorPlus.setOnClickListener(this);
        worldCompletetionLayoutTutorPlus.setOnClickListener(this);
        tutoringSessionLayoutTutorPlus.setOnClickListener(this);
    }

    private void setMathTutorPlusText(Translation transeletion) {
        try{
            lblrequestATutorsTutorPlus.setText(transeletion
                    .getTranselationTextByTextIdentifier("lblRequestATutor"));
            lblInstructionVedioTutorPlus.setText(transeletion
                    .getTranselationTextByTextIdentifier("lblInstructionalVideo"));
            lblMathExcersizePlusPlus.setText(transeletion
                    .getTranselationTextByTextIdentifier("lblMathExcercise"));
            lblWorldCompletetinTutorPlus.setText(transeletion
                    .getTranselationTextByTextIdentifier("lblWorldCompetitions"));
            lblTTutoringSessionTutorPlus.setText(transeletion
                    .getTranselationTextByTextIdentifier("btnTutoringSession"));
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void clickOnPreviousSession() {
        this.checkForStudentSelection(MathFriendzyHelper.DIALOG_TUTORING_SESSION);
    }

    private void goOnTutoringSession(){
        startActivity(new Intent(this , ActTutoringSession.class));
    }


    private void setVisibilityOfLayoutButtonVisibility() {
        if(MathVersions.CURRENT_MATH_VERSION  == MathVersions.MATH_PLUS){
            if(MathFriendzyHelper.isUserLogin(this)){
                if(MathFriendzyHelper.getUserAccountType(this)
                        == MathFriendzyHelper.TEACHER){
                    multiFriendzyLayoutMathPlus.setVisibility(RelativeLayout.GONE);
                    teacherFunctionsLayoutPlus.setVisibility(RelativeLayout.VISIBLE);
                    tutoringSessionLayoutMathPlus.setVisibility(RelativeLayout.GONE);
                }else{
                    teacherFunctionsLayoutPlus.setVisibility(RelativeLayout.GONE);
                    multiFriendzyLayoutMathPlus.setVisibility(RelativeLayout.GONE);
                    tutoringSessionLayoutMathPlus.setVisibility(RelativeLayout.VISIBLE);
                }
            }else{
                multiFriendzyLayoutMathPlus.setVisibility(RelativeLayout.GONE);
                teacherFunctionsLayoutPlus.setVisibility(RelativeLayout.VISIBLE);
                tutoringSessionLayoutMathPlus.setVisibility(RelativeLayout.GONE);
            }
        }
    }

    private void getInAppPurchaseStatus() {
        if(CommonUtils.isInternetConnectionAvailable(this)
                && MathFriendzyHelper.isUserLogin(this)
                && MathFriendzyHelper.isNeedToGetPurchaseStatus(this)){
            MathFriendzyHelper.getVideoInAppStatusSaveIntoSharedPreff(this ,
                    CommonUtils.getUserId(this) , new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase,
                                                   int requestCode) {

                        }
                    });
        }
    }


    /**
     * Go for homework quizz
     */
    private void checkForUnlock(final String checkFor){
        MathFriendzyHelper.saveLoginPlayerDataIntoSharedPreff(this);
        UserPlayerDto selectedPlayerData = this.getPlayerData();

        if(selectedPlayerData != null){
            final String userId = selectedPlayerData.getParentUserId();
            final String playerId = selectedPlayerData.getPlayerid();

            if(MathFriendzyHelper.getAppUnlockStatus
                    (MathFriendzyHelper.APP_UNLOCK_ID, userId, this) ==
                    MathFriendzyHelper.APP_UNLOCK){
                if(!MathFriendzyHelper.isNeedToDownloadAppUnlockStatus(this)) {
                    startActivity(checkFor);
                    return;
                }
            }

            if(CommonUtils.isInternetConnectionAvailable(this)){
                final ProgressDialog pd = ServerDialogs.getProgressDialog(this, "Please wait...", 1);
                pd.show();
                MathFriendzyHelper.checkAppUnlockStatus(userId, playerId, this ,
                        new CheckUnlockStatusCallback() {
                            @Override
                            public void onRequestComplete(CoinsFromServerObj coinsFromServer) {
                                //MathFriendzyHelper.dismissDialog();
                                if(pd != null && pd.isShowing())
                                    pd.dismiss();
                                if(coinsFromServer != null){
                                    if(coinsFromServer.getAppUnlock() == MathFriendzyHelper.APP_UNLOCK){
                                        startActivity(checkFor);
                                    }else{
                                        /*DialogGenerator dg = new DialogGenerator(MainActivity.this);
                                        dg.generateDailogForSchoolCurriculumUnlock
                                                (coinsFromServer , userId , playerId ,
                                                        coinsFromServer.getAppUnlock());*/
                                        MathFriendzyHelper.openSubscriptionDialog(getActivityObj() , checkFor);
                                    }
                                }else{
                                    CommonUtils.showInternetDialog(MainActivity.this);
                                }
                            }
                        });
            }else{
                if(MathFriendzyHelper.getAppUnlockStatus
                        (MathFriendzyHelper.APP_UNLOCK_ID, userId, this) ==
                        MathFriendzyHelper.APP_UNLOCK){
                    startActivity(checkFor);
                }else{
                    CommonUtils.showInternetDialog(this);
                }
            }
        }
    }

    /**
     * Start activity after checking application status
     * @param checkFor
     */
    private void startActivity(String checkFor){
        if(checkFor.equalsIgnoreCase(MathFriendzyHelper.DIALOG_SCHOOL_HOMEWORK_QUIZZ))
            startActivity(new Intent(this , ActHomeWork.class));
        else if(checkFor.equalsIgnoreCase(MathFriendzyHelper.DIALOG_SCHOOL_HELP_STUDENT))
            startActivity(new Intent(this , ActHelpAStudent.class));
    }

    private Activity getActivityObj(){
        return this;
    }


    /**
     * Set the request a tutor button on the home screen
     */
    private void setVisibilityOfRequestATutorButton() {
        if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS) {
            if(MathFriendzyHelper.isUserLogin(this)) {
                if(MathFriendzyHelper.getUserAccountType(this) == MathFriendzyHelper.TEACHER){
                    liveTutorLayoutPlus.setVisibility(RelativeLayout.GONE);
                    return ;
                }
                UserPlayerDto selectedPlayerData = this.getPlayerData();
                if(selectedPlayerData != null && (MathFriendzyHelper
                        .parseInt(selectedPlayerData.getAllowPaidTutor())
                        == MathFriendzyHelper.NO) &&
                        (MathFriendzyHelper.parseInt(selectedPlayerData.getSchoolId())
                                > MathFriendzyHelper.MAX_ASSUME_SCHOOL_ID)) {
                    liveTutorLayoutPlus.setVisibility(RelativeLayout.GONE);
                }else{
                    liveTutorLayoutPlus.setVisibility(RelativeLayout.VISIBLE);
                }
            }else{
                liveTutorLayoutPlus.setVisibility(RelativeLayout.VISIBLE);
            }
        }
    }


    //Global Room change
    private void createGlobalRoomObject() {
        UserPlayerDto selectedPlayerData = MathFriendzyHelper.getSelectedPlayer(this);
        if(selectedPlayerData != null){
            MyGlobalWebSocket.getInstance(this , this , 0 , selectedPlayerData.getPlayerid());
        }
    }

    @Override
    public void onConnected() {

    }

    @Override
    public void onConnectionFail() {

    }

    @Override
    public void onMessageSent(boolean isSuccess) {

    }

    @Override
    public void onMessageRecieved(String message) {

    }
}
