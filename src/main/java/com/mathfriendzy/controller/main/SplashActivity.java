package com.mathfriendzy.controller.main;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.amazon.device.messaging.ADM;
import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.gcm.GCMRegistration;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DatabaseAdapter;
import com.mathfriendzy.utils.StartUpAsyncTask;
import com.newrelic.agent.android.NewRelic;

import static com.mathfriendzy.utils.ICommonUtils.DEVICE_ID;
import static com.mathfriendzy.utils.ICommonUtils.DEVICE_ID_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.SPLASH_ACTIVITY_LOG;

/**
 * This is main activity where the execution of the project will start
 * @author Yashwant Singh
 *
 */
public class SplashActivity extends ActBaseClass
{
    //public static StartUpAsyncTask asynCkTaskObj = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if(SPLASH_ACTIVITY_LOG)
            Log.e("SplashActivity", "inside onCreate()");

        this.initializeNewRelic();

        //Use to Save Database file into database folder
        DatabaseAdapter adapter = new DatabaseAdapter(this , MathFriendzyHelper.getDBPath(this));

        //set the device id in shared preferences
        SharedPreferences sharedPreff = getSharedPreferences(DEVICE_ID_PREFF, 0);
        SharedPreferences.Editor editor = sharedPreff.edit();
        editor.putString(DEVICE_ID, MathFriendzyHelper.getDeviceId(this));
        editor.commit();

        SharedPreferences sheredPreference = getSharedPreferences("SHARED_FLAG", 0);
        CommonUtils.oldversion    = sheredPreference.getString("oldversion",
                CommonUtils.oldversion);

        //Use to check new version of apk
        if(!CommonUtils.oldversion.equals(CommonUtils.newversion)){
            MathFriendzyHelper.saveUserLogin(this , false);
            adapter.deleteDataBaseIfExist();
            adapter.createDatabase();
            CommonUtils.setSharedPreferences(this);
        }

        if(CommonUtils.isInternetConnectionAvailable(this)) {
            //new GCMRegistration(this , false);
            this.registerDeviceWithGCMAndADM();
            new StartUpAsyncTask(this).execute(null,null,null);
        }else{
            //changes for Spanish
            CommonUtils.CheckWordProblemSpanishTable(this);
            //end changes
            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);
            finish();
        }

        MathFriendzyHelper.updateAppActiveInActiveStatus(this);

        /*CommonUtils.printLog("SplashActivity" , "MD5 "
                + MathFriendzyHelper.getMD5(this , MathFriendzyHelper.getAppPackageName(this)));*/

        if(SPLASH_ACTIVITY_LOG)
            Log.e("SplashActivity", "outside onCreate()");
    }

    private void initializeNewRelic(){
        NewRelic.withApplicationToken(
                "AA0926c4dc886493e0fd4e472217297f93f56e867a"
        ).start(this.getApplication());
    }

    private void registerDeviceWithGCMAndADM(){
        if(MathFriendzyHelper.isADMSupportedForAmazonDevice()) {
            final ADM adm = new ADM(this);
            if (adm.getRegistrationId() == null) {
                //startRegister() is asynchronous; your app is notified via the
                //onRegistered() callback when the registration ID is available.
                adm.startRegister();
            } else {
                //MathFriendzyHelper.showToast(this, "AMD Registration ID " + adm.getRegistrationId());
                MathFriendzyHelper.registerDeviceOnServer(this, adm.getRegistrationId() ,
                        MathFriendzyHelper.AMAZON_DEVICE);
            }
        }else{
            new GCMRegistration(this , false);
        }
    }
}
