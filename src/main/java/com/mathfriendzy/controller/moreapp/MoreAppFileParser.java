package com.mathfriendzy.controller.moreapp;

import android.util.Log;

import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.utils.CommonUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MoreAppFileParser {
	public static ArrayList<AppDetail> parseAppData(String respone , String appId){
		try{
			if(respone != null){
				if(CommonUtils.LOG_ON)
					Log.e("MoreAppFileParser", "Response " + respone);
				ArrayList<AppDetail> appDetailList = new ArrayList<AppDetail>();
				JSONObject jsonObj = new JSONObject(respone);
				JSONArray jsonArray = 	jsonObj.getJSONArray("data");
				for(int i = 0 ; i < jsonArray.length() ; i ++ ){
					JSONObject jsonDataObj = jsonArray.getJSONObject(i);
					AppDetail appData = new AppDetail();
					appData.setAppId(jsonDataObj.getString("appId"));
					appData.setAppName(jsonDataObj.getString("name"));
					appData.setAppUrl(jsonDataObj.getString("appStoreUrl"));
					appData.setIconImage(jsonDataObj.getString("iconImage"));
					appData.setPrice(jsonDataObj.getString("price"));
					appData.setStatus(jsonDataObj.getString("status"));
					appData.setUrlScheme(jsonDataObj.getString("urlScheme"));					
					if(!MathFriendzyHelper.isEmpty(appData.getAppUrl()) && 
							!appData.getAppId().equalsIgnoreCase(appId)){
						appData.setPackageName(getPachageName(jsonDataObj.getString("appStoreUrl")));
						appDetailList.add(appData);
					}					
				}
				return appDetailList;
			}else{
				return null;
			}
		}catch(Exception e){
			return null;
		}
	}
	
	private static String getPachageName(String url){
		try{
			return url.split("=")[1];
		}catch(Exception e){
			return null;
		}
	}
}
