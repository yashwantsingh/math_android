package com.mathfriendzy.controller.moreapp;

import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ITextIds;

import java.util.ArrayList;

public class ActMoreApp extends ActBase{

	private TextView txtTitleScreen = null;
	private TextView txtLoveThisApp = null;
	private GridView gridView = null;
	private ArrayList<String> installedPackageList = null;
	private String btnTitlePlayAgain = null;
	private String btnTitleTryFree = null;

    private boolean isOpenAfterRateUsPopUp = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_act_more_app);

        this.getIntentValue();
		this.init();
		this.setWidgetsReferences();
		this.setTextFromTranslation();
		this.setListenerOnWidgets();
		this.getAppDetailList();
	}

	private void init() {
		installedPackageList = MathFriendzyHelper.getInstalledAppInAndroid(this);
	}

    private void getIntentValue() {
        isOpenAfterRateUsPopUp = this.getIntent().getBooleanExtra("isOpenAfterRateUsPopUp", false);
    }

	protected void setWidgetsReferences() {
		txtTitleScreen = (TextView) findViewById(R.id.txtTitleScreen);
		txtLoveThisApp = (TextView) findViewById(R.id.txtLoveThisApp);
		gridView = (GridView) findViewById(R.id.gridView);
	}

	protected void setListenerOnWidgets() {

	}

	protected void setTextFromTranslation() {
		Translation translate = new Translation(this);
		translate.openConnection();
		txtTitleScreen.setText(translate.getTranselationTextByTextIdentifier(ITextIds.MF_HOMESCREEN));
		txtLoveThisApp.setText(translate.getTranselationTextByTextIdentifier("lblIfYouLoveApp"));
		btnTitlePlayAgain = translate.getTranselationTextByTextIdentifier("btnTitlePlayAgain");
		btnTitleTryFree = translate.getTranselationTextByTextIdentifier("btnTitleTryFree");
		translate.closeConnection();
	}

	private void getAppDetailList(){
		if(!CommonUtils.isInternetConnectionAvailable(this)){
			CommonUtils.showInternetDialog(this);
			return ;
		}

		MathFriendzyHelper.downloadMoreApps(this, new MoreAppListener() {

			@Override
			public void onSuccess(ArrayList<AppDetail> appList) {
				if(appList != null && appList.size() > 0){
					setAppAdapter(appList);
				}
			}
		} , CommonUtils.APP_ID);
	}

	private void setAppAdapter(ArrayList<AppDetail> appDetailList){
		MoreAppAdapter adapter = new MoreAppAdapter(this , appDetailList,
				installedPackageList , new AppClickListener() {

			@Override
			public void OnClickListener(AppDetail appDetail) {
				if(MathFriendzyHelper.isEmpty(appDetail.getPackageName())){
					MathFriendzyHelper.openUrl(ActMoreApp.this, appDetail.getAppUrl());
				}else{
					if(installedPackageList.contains(appDetail.getPackageName())){
						MathFriendzyHelper.openApp(ActMoreApp.this, appDetail.getPackageName());
					}else{
						MathFriendzyHelper.openUrl(ActMoreApp.this, appDetail.getAppUrl());
					}
				}
			}
		} , btnTitlePlayAgain , btnTitleTryFree);
		gridView.setAdapter(adapter);
	}

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onBackPressed() {
        /*if(isOpenAfterRateUsPopUp){
            HouseAds.getInstance().showCustomeAds(this);
        }*/
        super.onBackPressed();
    }
}
