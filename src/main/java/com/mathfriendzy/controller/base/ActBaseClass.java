package com.mathfriendzy.controller.base;

import android.app.Activity;
import android.os.Bundle;

import com.mathfriendzy.controller.ads.BannerAds;
import com.mathfriendzy.controller.ads.RateUsPops;

/**
 * Created by root on 2/9/15.
 */
public class ActBaseClass extends Activity {

    private final String TAG = this.getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        BannerAds.getInstance(this).showAds();
        RateUsPops.getInstance(this).showRateUsPopUp();
        //HouseAds.getInstance().showAds(this);
        //HouseAds.getInstance().showAds(MyApplication.getAppContext());
        //DontLikeAds.getInstance(MyApplication.getAppContext()).showDontLikeAds();
        //RateUsPops.getInstance(MyApplication.getAppContext()).showRateUsPopUp();
        super.onResume();
    }
}
