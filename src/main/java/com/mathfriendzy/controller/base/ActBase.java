package com.mathfriendzy.controller.base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mathfriendzy.R;
import com.mathfriendzy.controller.ads.BannerAds;
import com.mathfriendzy.controller.ads.RateUsPops;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.assessmenttest.AssessmentTestImpl;
import com.mathfriendzy.model.assessmenttest.CatagoriesDto;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.player.temp.TempPlayer;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.QuestionLoadedTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SubCatergoryTransferObj;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.utils.CommonUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public abstract class ActBase extends Activity implements HttpResponseInterface
        , OnClickListener {

    private final String TAG = this.getClass().getSimpleName();

    protected TextView txtTopbar = null;

    //for Act bottom layout
    protected TextView txtTotalProblems = null;
    protected TextView txtZeroCredit = null;
    protected TextView txtHaltCredit = null;
    protected TextView txtFullCredit = null;
    protected TextView txtAvgScore = null;


    /**
     * This method get the widgets references from the xml and set it to the Objects
     */
    protected abstract void setWidgetsReferences();

    /**
     * This method set the listener on widgets
     */
    protected abstract void setListenerOnWidgets();

    /**
     * This method set the text from the transaltino
     */
    protected abstract void setTextFromTranslation();


    protected Button btnsecondryKeyBoard = null;
    protected Button btn1 = null;
    protected Button btn2 = null;
    protected Button btn3 = null;
    protected Button btn4 = null;
    protected Button btn5 = null;
    protected Button btn6 = null;
    protected Button btn7 = null;
    protected Button btn8 = null;
    protected Button btn9 = null;
    protected Button btn0 = null;
    protected Button btnSynchronized = null;// for clear ans box
    //protected Button btnBackArrow       = null;
    protected Button btnGoForFillIn = null;

    //for secondary key board
    protected Button btnNumber = null;
    protected Button btnDot = null;
    protected Button btnComma = null;
    protected Button btnMinus = null;
    protected Button btnPlus = null;
    protected Button btnDevide = null;
    protected Button btnMultiply = null;
    protected Button btnCollon = null;
    protected Button btnLessThan = null;
    protected Button btnGreaterThan = null;
    protected Button btnEqual = null;
    protected Button btnRoot = null;
    protected Button btnDevider = null;
    protected Button btnOpeningBracket = null;
    protected Button btnClosingBracket = null;

    //siddhiinfosoft

    protected boolean kwyboard_case_value = false;

    protected Button kb_btn_abc_case = null;

    protected Button kb_btn_abc_q = null;
    protected Button kb_btn_abc_w = null;
    protected Button kb_btn_abc_e = null;
    protected Button kb_btn_abc_r = null;
    protected Button kb_btn_abc_t = null;
    protected Button kb_btn_abc_y = null;
    protected Button kb_btn_abc_u = null;
    protected Button kb_btn_abc_i = null;
    protected Button kb_btn_abc_o = null;
    protected Button kb_btn_abc_p = null;
    protected Button kb_btn_abc_a = null;
    protected Button kb_btn_abc_s = null;
    protected Button kb_btn_abc_d = null;
    protected Button kb_btn_abc_f = null;
    protected Button kb_btn_abc_g = null;
    protected Button kb_btn_abc_h = null;
    protected Button kb_btn_abc_j = null;
    protected Button kb_btn_abc_k = null;
    protected Button kb_btn_abc_l = null;
    protected Button kb_btn_abc_z = null;
    protected Button kb_btn_abc_x = null;
    protected Button kb_btn_abc_c = null;
    protected Button kb_btn_abc_v = null;
    protected Button kb_btn_abc_b = null;
    protected Button kb_btn_abc_n = null;
    protected Button kb_btn_abc_m = null;

    protected LinearLayout kb_ll_123, kb_ll_abc;

    protected TextView kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_algebra, kb_tv_eq_name_geometry, kb_tv_eq_name_trignometry,
            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath;

    protected LinearLayout ll_kb_sub_sumbol_basicmath_sign1 = null, ll_kb_sub_sumbol_basicmath_sign2 = null, ll_kb_sub_sumbol_basicmath_sign5 = null;

    protected LinearLayout ll_kb_sub_sumbol_prealgebra_sign1 = null, ll_kb_sub_sumbol_prealgebra_sign2 = null,
            ll_kb_sub_sumbol_prealgebra_sign4 = null, ll_kb_sub_sumbol_prealgebra_sign5 = null;

    protected LinearLayout ll_kb_sub_sumbol_algebra_sign1 = null, ll_kb_sub_sumbol_algebra_sign2 = null,
            ll_kb_sub_sumbol_algebra_sign4 = null, ll_kb_sub_sumbol_algebra_sign5 = null, ll_kb_sub_sumbol_algebra_sign8 = null;

    protected LinearLayout ll_kb_sub_sumbol_trignometry_sign1 = null, ll_kb_sub_sumbol_trignometry_sign2 = null, ll_kb_sub_sumbol_trignometry_sign4 = null,
            ll_kb_sub_sumbol_trignometry_sign5 = null, ll_kb_sub_sumbol_trignometry_sign7 = null, ll_kb_sub_sumbol_trignometry_sign8 = null,
            ll_kb_sub_sumbol_trignometry_sign9 = null, ll_kb_sub_sumbol_trignometry_sign10 = null, ll_kb_sub_sumbol_trignometry_sign11 = null,
            ll_kb_sub_sumbol_trignometry_sign12 = null, ll_kb_sub_sumbol_trignometry_sign13 = null;

    protected LinearLayout ll_kb_sub_sumbol_precalculus_sign1 = null, ll_kb_sub_sumbol_precalculus_sign2 = null, ll_kb_sub_sumbol_precalculus_sign4 = null,
            ll_kb_sub_sumbol_precalculus_sign5 = null, ll_kb_sub_sumbol_precalculus_sign9 = null;

    protected LinearLayout ll_kb_sub_sumbol_calculus_sign1 = null, ll_kb_sub_sumbol_calculus_sign2 = null, ll_kb_sub_sumbol_calculus_sign4 = null,
            ll_kb_sub_sumbol_calculus_sign5 = null, ll_kb_sub_sumbol_calculus_sign10 = null;

    protected LinearLayout ll_kb_sub_sumbol_statistics_sign1 = null, ll_kb_sub_sumbol_statistics_sign3 = null,
            ll_kb_sub_sumbol_statistics_sign4 = null, ll_kb_sub_sumbol_statistics_sign6 = null,
            ll_kb_sub_sumbol_statistics_sign7 = null;

    protected LinearLayout ll_kb_sub_sumbol_finitemath_sign1 = null, ll_kb_sub_sumbol_finitemath_sign2 = null,
            ll_kb_sub_sumbol_finitemath_sign4 = null, ll_kb_sub_sumbol_finitemath_sign5 = null,
            ll_kb_sub_sumbol_finitemath_sign8 = null;

    protected LinearLayout ll_kb_sub_symbol_geometry_sign2, ll_kb_sub_symbol_geometry_sign5, ll_kb_sub_symbol_geometry_sign6,
            ll_kb_sub_symbol_geometry_sign7;

    protected LinearLayout ll_main_eq_sub_symbol = null;

    protected LinearLayout kb_ll_eq_symbol_basicmath = null, kb_ll_eq_symbol_prealgebra = null, kb_ll_eq_symbol_geometry = null, kb_ll_eq_symbol_algebra = null, kb_ll_eq_symbol_trignometry = null,
            kb_ll_eq_symbol_precalculus = null, kb_ll_eq_symbol_calculus = null, kb_ll_eq_symbol_statistics = null, kb_ll_eq_symbol_finitemath = null;

    protected HorizontalScrollView hsv_keyboard_eq_name = null, hsv_keyboard_eq_symbol = null;

    protected Button btn_eq_symbol_pre_scroll, btn_eq_symbol_next_scroll;

    protected Button kb_btn_123_delete, kb_btn_abc_delete, kb_btn_123_previous, kb_btn_123_next;
    protected boolean is_touch_kb = false, is_touch_kb_pre = false, is_touch_kb_next = false;

    /**
     * This method set the widgets references for the bottom layout widgets used in HomeWork
     */
    protected void setBottomLayoutWidgetsReferences() {
        txtTotalProblems = (TextView) findViewById(R.id.txtTotalProblems);
        txtZeroCredit = (TextView) findViewById(R.id.txtZeroCredit);
        txtHaltCredit = (TextView) findViewById(R.id.txtHaltCredit);
        txtFullCredit = (TextView) findViewById(R.id.txtFullCredit);
        txtAvgScore = (TextView) findViewById(R.id.txtAvgScore);
    }

    /**
     * This method return the user player data
     *
     * @return
     */
    protected UserPlayerDto getPlayerData() {
        try {
            return MathFriendzyHelper.getSelectedPlayerDataById
                    (this, MathFriendzyHelper.getSelectedPlayerID(this));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    //siddhiinfosoft

    protected void setcustomkeyboardwidgetsreference() {

        btn_eq_symbol_pre_scroll = (Button) findViewById(R.id.btn_eq_symbol_pre_scroll);
        btn_eq_symbol_next_scroll = (Button) findViewById(R.id.btn_eq_symbol_next_scroll);

        kb_ll_123 = (LinearLayout) findViewById(R.id.kb_ll_123);
        kb_ll_abc = (LinearLayout) findViewById(R.id.kb_ll_abc);

        kb_btn_abc_case = (Button) findViewById(R.id.kb_btn_abc_case);

        hsv_keyboard_eq_name = (HorizontalScrollView) findViewById(R.id.hsv_keyboard_eq_name);
        hsv_keyboard_eq_symbol = (HorizontalScrollView) findViewById(R.id.hsv_keyboard_eq_symbol);

        kb_ll_eq_symbol_basicmath = (LinearLayout) findViewById(R.id.kb_ll_eq_symbol_basicmath);
        kb_ll_eq_symbol_prealgebra = (LinearLayout) findViewById(R.id.kb_ll_eq_symbol_prealgebra);
        kb_ll_eq_symbol_algebra = (LinearLayout) findViewById(R.id.kb_ll_eq_symbol_algebra);
        kb_ll_eq_symbol_geometry = (LinearLayout) findViewById(R.id.kb_ll_eq_symbol_geometry);
        kb_ll_eq_symbol_trignometry = (LinearLayout) findViewById(R.id.kb_ll_eq_symbol_trignometry);
        kb_ll_eq_symbol_precalculus = (LinearLayout) findViewById(R.id.kb_ll_eq_symbol_precalculus);
        kb_ll_eq_symbol_calculus = (LinearLayout) findViewById(R.id.kb_ll_eq_symbol_calculus);
        kb_ll_eq_symbol_statistics = (LinearLayout) findViewById(R.id.kb_ll_eq_symbol_statistics);
        kb_ll_eq_symbol_finitemath = (LinearLayout) findViewById(R.id.kb_ll_eq_symbol_finitemath);

        ll_kb_sub_sumbol_basicmath_sign1 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_basicmath_sign1);
        ll_kb_sub_sumbol_basicmath_sign2 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_basicmath_sign2);
        ll_kb_sub_sumbol_basicmath_sign5 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_basicmath_sign5);

        ll_kb_sub_sumbol_prealgebra_sign1 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_prealgebra_sign1);
        ll_kb_sub_sumbol_prealgebra_sign2 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_prealgebra_sign2);
        ll_kb_sub_sumbol_prealgebra_sign4 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_prealgebra_sign4);
        ll_kb_sub_sumbol_prealgebra_sign5 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_prealgebra_sign5);

        ll_kb_sub_sumbol_algebra_sign1 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_algebra_sign1);
        ll_kb_sub_sumbol_algebra_sign2 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_algebra_sign2);
        ll_kb_sub_sumbol_algebra_sign4 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_algebra_sign4);
        ll_kb_sub_sumbol_algebra_sign5 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_algebra_sign5);
        ll_kb_sub_sumbol_algebra_sign8 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_algebra_sign8);

        ll_kb_sub_sumbol_trignometry_sign1 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_trignometry_sign1);
        ll_kb_sub_sumbol_trignometry_sign2 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_trignometry_sign2);
        ll_kb_sub_sumbol_trignometry_sign4 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_trignometry_sign4);
        ll_kb_sub_sumbol_trignometry_sign5 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_trignometry_sign5);
        ll_kb_sub_sumbol_trignometry_sign7 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_trignometry_sign7);
        ll_kb_sub_sumbol_trignometry_sign8 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_trignometry_sign8);
        ll_kb_sub_sumbol_trignometry_sign9 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_trignometry_sign9);
        ll_kb_sub_sumbol_trignometry_sign10 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_trignometry_sign10);
        ll_kb_sub_sumbol_trignometry_sign11 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_trignometry_sign11);
        ll_kb_sub_sumbol_trignometry_sign12 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_trignometry_sign12);
        ll_kb_sub_sumbol_trignometry_sign13 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_trignometry_sign13);

        ll_kb_sub_sumbol_precalculus_sign1 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_precalculus_sign1);
        ll_kb_sub_sumbol_precalculus_sign2 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_precalculus_sign2);
        ll_kb_sub_sumbol_precalculus_sign4 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_precalculus_sign4);
        ll_kb_sub_sumbol_precalculus_sign5 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_precalculus_sign5);
        ll_kb_sub_sumbol_precalculus_sign9 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_precalculus_sign9);

        ll_kb_sub_sumbol_calculus_sign1 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_calculus_sign1);
        ll_kb_sub_sumbol_calculus_sign2 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_calculus_sign2);
        ll_kb_sub_sumbol_calculus_sign4 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_calculus_sign4);
        ll_kb_sub_sumbol_calculus_sign5 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_calculus_sign5);
        ll_kb_sub_sumbol_calculus_sign10 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_calculus_sign10);

        ll_kb_sub_sumbol_statistics_sign1 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_statistics_sign1);
        ll_kb_sub_sumbol_statistics_sign3 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_statistics_sign3);
        ll_kb_sub_sumbol_statistics_sign4 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_statistics_sign4);
        ll_kb_sub_sumbol_statistics_sign6 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_statistics_sign6);
        ll_kb_sub_sumbol_statistics_sign7 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_statistics_sign7);

        ll_kb_sub_sumbol_finitemath_sign1 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_finitemath_sign1);
        ll_kb_sub_sumbol_finitemath_sign2 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_finitemath_sign2);
        ll_kb_sub_sumbol_finitemath_sign4 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_finitemath_sign4);
        ll_kb_sub_sumbol_finitemath_sign5 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_finitemath_sign5);
        ll_kb_sub_sumbol_finitemath_sign8 = (LinearLayout) findViewById(R.id.ll_kb_sub_sumbol_finitemath_sign8);

        ll_kb_sub_symbol_geometry_sign2 = (LinearLayout) findViewById(R.id.ll_kb_sub_symbol_geometry_sign2);
        ll_kb_sub_symbol_geometry_sign5 = (LinearLayout) findViewById(R.id.ll_kb_sub_symbol_geometry_sign5);
        ll_kb_sub_symbol_geometry_sign6 = (LinearLayout) findViewById(R.id.ll_kb_sub_symbol_geometry_sign6);
        ll_kb_sub_symbol_geometry_sign7 = (LinearLayout) findViewById(R.id.ll_kb_sub_symbol_geometry_sign7);

        ll_main_eq_sub_symbol = (LinearLayout) findViewById(R.id.ll_main_eq_sub_symbol);

        kb_btn_abc_q = (Button) findViewById(R.id.kb_btn_abc_q);
        kb_btn_abc_w = (Button) findViewById(R.id.kb_btn_abc_w);
        kb_btn_abc_e = (Button) findViewById(R.id.kb_btn_abc_e);
        kb_btn_abc_r = (Button) findViewById(R.id.kb_btn_abc_r);
        kb_btn_abc_t = (Button) findViewById(R.id.kb_btn_abc_t);
        kb_btn_abc_y = (Button) findViewById(R.id.kb_btn_abc_y);
        kb_btn_abc_u = (Button) findViewById(R.id.kb_btn_abc_u);
        kb_btn_abc_i = (Button) findViewById(R.id.kb_btn_abc_i);
        kb_btn_abc_o = (Button) findViewById(R.id.kb_btn_abc_o);
        kb_btn_abc_p = (Button) findViewById(R.id.kb_btn_abc_p);
        kb_btn_abc_a = (Button) findViewById(R.id.kb_btn_abc_a);
        kb_btn_abc_s = (Button) findViewById(R.id.kb_btn_abc_s);
        kb_btn_abc_d = (Button) findViewById(R.id.kb_btn_abc_d);
        kb_btn_abc_f = (Button) findViewById(R.id.kb_btn_abc_f);
        kb_btn_abc_g = (Button) findViewById(R.id.kb_btn_abc_g);
        kb_btn_abc_h = (Button) findViewById(R.id.kb_btn_abc_h);
        kb_btn_abc_j = (Button) findViewById(R.id.kb_btn_abc_j);
        kb_btn_abc_k = (Button) findViewById(R.id.kb_btn_abc_k);
        kb_btn_abc_l = (Button) findViewById(R.id.kb_btn_abc_l);
        kb_btn_abc_z = (Button) findViewById(R.id.kb_btn_abc_z);
        kb_btn_abc_x = (Button) findViewById(R.id.kb_btn_abc_x);
        kb_btn_abc_c = (Button) findViewById(R.id.kb_btn_abc_c);
        kb_btn_abc_v = (Button) findViewById(R.id.kb_btn_abc_v);
        kb_btn_abc_b = (Button) findViewById(R.id.kb_btn_abc_b);
        kb_btn_abc_n = (Button) findViewById(R.id.kb_btn_abc_n);
        kb_btn_abc_m = (Button) findViewById(R.id.kb_btn_abc_m);

        kb_tv_eq_name_basicmath = (TextView) findViewById(R.id.kb_tv_eq_name_basicmath);
        kb_tv_eq_name_prealgebra = (TextView) findViewById(R.id.kb_tv_eq_name_prealgebra);
        kb_tv_eq_name_algebra = (TextView) findViewById(R.id.kb_tv_eq_name_algebra);
        kb_tv_eq_name_geometry = (TextView) findViewById(R.id.kb_tv_eq_name_geometry);
        kb_tv_eq_name_trignometry = (TextView) findViewById(R.id.kb_tv_eq_name_trignometry);
        kb_tv_eq_name_precalculus = (TextView) findViewById(R.id.kb_tv_eq_name_precalculus);
        kb_tv_eq_name_calculus = (TextView) findViewById(R.id.kb_tv_eq_name_calculus);
        kb_tv_eq_name_statistics = (TextView) findViewById(R.id.kb_tv_eq_name_statistics);
        kb_tv_eq_name_finitemath = (TextView) findViewById(R.id.kb_tv_eq_name_finitemath);

        kb_btn_123_delete = (Button) findViewById(R.id.kb_btn_123_delete);
        kb_btn_abc_delete = (Button) findViewById(R.id.kb_btn_abc_delete);

        kb_btn_123_previous = (Button) findViewById(R.id.kb_btn_123_previous);
        kb_btn_123_next = (Button) findViewById(R.id.kb_btn_123_next);

    }

    protected void setvisibility_scroll_btn(boolean is_visible) {

        if (is_visible) {

            btn_eq_symbol_pre_scroll.setVisibility(View.VISIBLE);
            btn_eq_symbol_next_scroll.setVisibility(View.VISIBLE);

        } else {

            btn_eq_symbol_pre_scroll.setVisibility(View.GONE);
            btn_eq_symbol_next_scroll.setVisibility(View.GONE);

        }

    }

    protected void changecasevalue() {

        if (kwyboard_case_value) {
            kb_btn_abc_case.setBackgroundResource(R.drawable.kb_abc_text_trf);
            kb_btn_abc_q.setText("q");
            kb_btn_abc_w.setText("w");
            kb_btn_abc_e.setText("e");
            kb_btn_abc_r.setText("r");
            kb_btn_abc_t.setText("t");
            kb_btn_abc_y.setText("y");
            kb_btn_abc_u.setText("u");
            kb_btn_abc_i.setText("i");
            kb_btn_abc_o.setText("o");
            kb_btn_abc_p.setText("p");
            kb_btn_abc_a.setText("a");
            kb_btn_abc_s.setText("s");
            kb_btn_abc_d.setText("d");
            kb_btn_abc_f.setText("f");
            kb_btn_abc_g.setText("g");
            kb_btn_abc_h.setText("h");
            kb_btn_abc_j.setText("j");
            kb_btn_abc_k.setText("k");
            kb_btn_abc_l.setText("l");
            kb_btn_abc_z.setText("z");
            kb_btn_abc_x.setText("x");
            kb_btn_abc_c.setText("c");
            kb_btn_abc_v.setText("v");
            kb_btn_abc_b.setText("b");
            kb_btn_abc_n.setText("n");
            kb_btn_abc_m.setText("m");
            kb_btn_abc_q.setTag("q");
            kb_btn_abc_w.setTag("w");
            kb_btn_abc_e.setTag("e");
            kb_btn_abc_r.setTag("r");
            kb_btn_abc_t.setTag("t");
            kb_btn_abc_y.setTag("y");
            kb_btn_abc_u.setTag("u");
            kb_btn_abc_i.setTag("i");
            kb_btn_abc_o.setTag("o");
            kb_btn_abc_p.setTag("p");
            kb_btn_abc_a.setTag("a");
            kb_btn_abc_s.setTag("s");
            kb_btn_abc_d.setTag("d");
            kb_btn_abc_f.setTag("f");
            kb_btn_abc_g.setTag("g");
            kb_btn_abc_h.setTag("h");
            kb_btn_abc_j.setTag("j");
            kb_btn_abc_k.setTag("k");
            kb_btn_abc_l.setTag("l");
            kb_btn_abc_z.setTag("z");
            kb_btn_abc_x.setTag("x");
            kb_btn_abc_c.setTag("c");
            kb_btn_abc_v.setTag("v");
            kb_btn_abc_b.setTag("b");
            kb_btn_abc_n.setTag("n");
            kb_btn_abc_m.setTag("m");
            kwyboard_case_value = false;
        } else {
            kb_btn_abc_case.setBackgroundResource(R.drawable.kb_abc_text_trf_sel);
            kb_btn_abc_q.setText("Q");
            kb_btn_abc_w.setText("W");
            kb_btn_abc_e.setText("E");
            kb_btn_abc_r.setText("R");
            kb_btn_abc_t.setText("T");
            kb_btn_abc_y.setText("Y");
            kb_btn_abc_u.setText("U");
            kb_btn_abc_i.setText("I");
            kb_btn_abc_o.setText("O");
            kb_btn_abc_p.setText("P");
            kb_btn_abc_a.setText("A");
            kb_btn_abc_s.setText("S");
            kb_btn_abc_d.setText("D");
            kb_btn_abc_f.setText("F");
            kb_btn_abc_g.setText("G");
            kb_btn_abc_h.setText("H");
            kb_btn_abc_j.setText("J");
            kb_btn_abc_k.setText("K");
            kb_btn_abc_l.setText("L");
            kb_btn_abc_z.setText("Z");
            kb_btn_abc_x.setText("X");
            kb_btn_abc_c.setText("C");
            kb_btn_abc_v.setText("V");
            kb_btn_abc_b.setText("B");
            kb_btn_abc_n.setText("N");
            kb_btn_abc_m.setText("M");
            kb_btn_abc_q.setTag("Q");
            kb_btn_abc_w.setTag("W");
            kb_btn_abc_e.setTag("E");
            kb_btn_abc_r.setTag("R");
            kb_btn_abc_t.setTag("T");
            kb_btn_abc_y.setTag("Y");
            kb_btn_abc_u.setTag("U");
            kb_btn_abc_i.setTag("I");
            kb_btn_abc_o.setTag("O");
            kb_btn_abc_p.setTag("P");
            kb_btn_abc_a.setTag("A");
            kb_btn_abc_s.setTag("S");
            kb_btn_abc_d.setTag("D");
            kb_btn_abc_f.setTag("F");
            kb_btn_abc_g.setTag("G");
            kb_btn_abc_h.setTag("H");
            kb_btn_abc_j.setTag("J");
            kb_btn_abc_k.setTag("K");
            kb_btn_abc_l.setTag("L");
            kb_btn_abc_z.setTag("Z");
            kb_btn_abc_x.setTag("X");
            kb_btn_abc_c.setTag("C");
            kb_btn_abc_v.setTag("V");
            kb_btn_abc_b.setTag("B");
            kb_btn_abc_n.setTag("N");
            kb_btn_abc_m.setTag("M");
            kwyboard_case_value = true;
        }

    }

    protected void callscrollmethod(int int_type) {

        switch (int_type) {

            case 1:

                hsv_keyboard_eq_name.post(new Runnable() {
                    @Override
                    public void run() {
                        hsv_keyboard_eq_name.scrollTo((hsv_keyboard_eq_name.getScrollX() - 60), 0);
                    }
                });

                break;

            case 2:

                hsv_keyboard_eq_name.post(new Runnable() {
                    @Override
                    public void run() {
                        hsv_keyboard_eq_name.scrollTo((hsv_keyboard_eq_name.getScrollX() + 60), 0);
                    }
                });

                break;

            case 3:

                hsv_keyboard_eq_symbol.post(new Runnable() {
                    @Override
                    public void run() {
                        hsv_keyboard_eq_symbol.scrollTo((hsv_keyboard_eq_symbol.getScrollX() - 60), 0);
                    }
                });

                break;

            case 4:

                hsv_keyboard_eq_symbol.post(new Runnable() {
                    @Override
                    public void run() {
                        hsv_keyboard_eq_symbol.scrollTo((hsv_keyboard_eq_symbol.getScrollX() + 60), 0);
                    }
                });

                break;

        }

    }

    /**
     * Set the keyboard widgets references for the numeric and secendory
     */
    protected void setWidgetsReferencesForKeyboard() {

        btnsecondryKeyBoard = (Button) findViewById(R.id.btnsecondryKeyBoard);
        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);
        btn4 = (Button) findViewById(R.id.btn4);
        btn5 = (Button) findViewById(R.id.btn5);
        btn6 = (Button) findViewById(R.id.btn6);
        btn7 = (Button) findViewById(R.id.btn7);
        btn8 = (Button) findViewById(R.id.btn8);
        btn9 = (Button) findViewById(R.id.btn9);
        btn0 = (Button) findViewById(R.id.btn0);
        btnSynchronized = (Button) findViewById(R.id.btnSynchronized);
        //btnBackArrow = (Button) findViewById(R.id.btnBackArrow);
        btnGoForFillIn = (Button) findViewById(R.id.btnGoForFillIn);

        btnNumber = (Button) findViewById(R.id.btnNumber);
        btnDot = (Button) findViewById(R.id.btnDot);
        btnComma = (Button) findViewById(R.id.btnComma);
        btnMinus = (Button) findViewById(R.id.btnMinus);
        btnPlus = (Button) findViewById(R.id.btnPlus);
        btnDevide = (Button) findViewById(R.id.btnDevide);
        btnMultiply = (Button) findViewById(R.id.btnMultiply);
        btnCollon = (Button) findViewById(R.id.btnCollon);
        btnLessThan = (Button) findViewById(R.id.btnLessThan);
        btnGreaterThan = (Button) findViewById(R.id.btnGreaterThan);
        btnEqual = (Button) findViewById(R.id.btnEqual);
        btnRoot = (Button) findViewById(R.id.btnRoot);
        btnDevider = (Button) findViewById(R.id.btnDevider);
        btnOpeningBracket = (Button) findViewById(R.id.btnOpeningBracket);
        btnClosingBracket = (Button) findViewById(R.id.btnClosingBracket);
    }

    /**
     * Set the listener on the keyboard layout
     */
    protected void setListenerOnKeyBoardLayoutButton() {

        btnGoForFillIn.setOnClickListener(this);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btn9.setOnClickListener(this);
        btn0.setOnClickListener(this);

        btnSynchronized.setOnClickListener(this);
        btnsecondryKeyBoard.setOnClickListener(this);

        //for secondry keyboard
        btnNumber.setOnClickListener(this);
        btnDot.setOnClickListener(this);
        btnComma.setOnClickListener(this);
        btnMinus.setOnClickListener(this);
        btnPlus.setOnClickListener(this);
        btnDevide.setOnClickListener(this);
        btnMultiply.setOnClickListener(this);
        btnCollon.setOnClickListener(this);
        btnLessThan.setOnClickListener(this);
        btnGreaterThan.setOnClickListener(this);
        btnEqual.setOnClickListener(this);
        btnRoot.setOnClickListener(this);
        btnDevider.setOnClickListener(this);
        btnOpeningBracket.setOnClickListener(this);
        btnClosingBracket.setOnClickListener(this);
    }

    /**
     * Put Linked Hash map into Gson Json String to pass it through intent
     *
     * @param mapChildList
     * @return
     */
    protected String getGsonStringForLinkedHaskMap(LinkedHashMap<String,
            ArrayList<LearningCenterTransferObj>> mapChildList) {
        if (mapChildList != null) {
            Gson gson = new Gson();
            return gson.toJson(mapChildList);
        } else {
            return null;
        }
    }

    /**
     * Return the Linked HashMap from json
     *
     * @param jsonString
     * @return
     */
    protected LinkedHashMap<String, ArrayList<LearningCenterTransferObj>>
    getLinkedHashMapFromGsonJsonString(String jsonString) {
        if (jsonString != null) {
            Gson gson = new Gson();
            Type entityType = new TypeToken<LinkedHashMap<String,
                    ArrayList<LearningCenterTransferObj>>>() {
            }.getType();
            return gson.fromJson(jsonString, entityType);
        } else {
            return null;
        }
    }

    /**
     * Check For Is need to download categories or not
     *
     * @param grade
     * @return
     */
    protected boolean isSchoolCategoriesNeedToBeLoad(String grade) {
        try {
            if (MathFriendzyHelper.getCategories(Integer.parseInt(grade), this).size() > 0) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Thia asynck task download the categories
     *
     * @author Yashwant Singh
     */
    protected class DownlLoadCategoriesForSchoolCurriculum extends
            AsyncTask<Void, Void, QuestionLoadedTransferObj> {
        private int grade;
        private int lang;
        private ProgressDialog pd;
        private HttpServerRequest requestListener = null;

        public DownlLoadCategoriesForSchoolCurriculum(int grade, int lang,
                                                      HttpServerRequest requestListener) {
            this.lang = lang;
            this.grade = grade;
            this.requestListener = requestListener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = CommonUtils.getProgressDialog(ActBase.this);
            pd.show();
        }

        @Override
        protected QuestionLoadedTransferObj doInBackground(Void... params) {
            return MathFriendzyHelper.downLoadSchoolCategories(grade, lang, ActBase.this);
        }

        @Override
        protected void onPostExecute(QuestionLoadedTransferObj questionData) {
            pd.cancel();

            if (questionData != null) {
                requestListener.onRequestComplete();
            } else {
                CommonUtils.showInternetDialog(ActBase.this);
            }
            super.onPostExecute(questionData);
        }
    }

    /**
     * Put Linked Hash map into Gson Json String to pass it through intent
     *
     * @param mapChildList
     * @return
     */
    protected String getGsonStringForLinkedHaskMapForWordProblem
    (LinkedHashMap<String, ArrayList<SubCatergoryTransferObj>> mapChildList) {
        if (mapChildList != null) {
            Gson gson = new Gson();
            return gson.toJson(mapChildList);
        } else {
            return null;
        }
    }

    /**
     * Return the Linked HashMap from json
     *
     * @param jsonString
     * @return
     */
    protected LinkedHashMap<String, ArrayList<SubCatergoryTransferObj>>
    getLinkedHashMapFromGsonJsonStringForWorkProblem(String jsonString) {
        if (jsonString != null) {
            Gson gson = new Gson();
            Type entityType = new TypeToken<LinkedHashMap<String,
                    ArrayList<SubCatergoryTransferObj>>>() {
            }.getType();
            return gson.fromJson(jsonString, entityType);
        } else {
            return null;
        }
    }

    /**
     * This method get  word Assessment sub Categories info
     */
    protected ArrayList<CatagoriesDto> getWordAssessmentSubCategoriesInfo(int stdId, int subStdId) {
        AssessmentTestImpl implObj = new AssessmentTestImpl(this);
        implObj.openConnection();
        ArrayList<CatagoriesDto> categoryInfoList = implObj.getSubCategoriesInfoByStandardIdAndSubStdId(stdId, subStdId);
        implObj.closeConnection();
        return categoryInfoList;
    }

    /**
     * Clear the focus on the Adapter when user input value in the Edittext
     */
    public void cleasFocusFromEditText() {
        try {
            getCurrentFocus().clearFocus();
        } catch (Exception e) {

        }
    }

    /**
     * Hide the device kayboard
     */
    public void hideDeviceKeyboard() {
        try {
            InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*public void showHideKeyboard(){
        try {
            InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            //to show soft keyboard
            inputManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            //to hide it, call the method again
            inputManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    public void hideKeyboardAfterSomeTime(){
        InputMethodManager imm = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isAcceptingText()) {
            this.hideDeviceKeyboard();
        }
    }
    /**
     * Enable disable listener
     *
     * @param enableDisable
     */
    protected void enableDisableListenerOntextView(View view, boolean enableDisable) {
        if (enableDisable) {
            view.setOnClickListener(this);
        } else {
            view.setOnClickListener(null);
        }
    }

    /**
     * Set hint to text
     *
     * @param text
     * @param txtView
     */
    protected void setHintToText(String text, TextView txtView) {
        txtView.setText(text);
    }

    protected void setHintToEditText(String text, EditText edtText) {
        edtText.setHint(text);
    }

    /**
     * Set hint to edit text
     *
     * @param edt
     * @param hint
     */
    protected void setHintToEditText(EditText edt, String hint) {
        edt.setHint(hint);
    }

    /**
     * This method return the xml format of the temp player data
     *
     * @param tempPlayerObj
     * @return
     */
    protected String getXmlPlayer(ArrayList<TempPlayer> tempPlayerObj) {
        StringBuilder xml = new StringBuilder("");
        for (int i = 0; i < tempPlayerObj.size(); i++) {

            PlayerTotalPointsObj playerObj = null;
            LearningCenterimpl learningCenterimpl = new LearningCenterimpl(this);
            learningCenterimpl.openConn();
            playerObj = learningCenterimpl.getDataFromPlayerTotalPoints
                    (tempPlayerObj.get(i).getPlayerId() + "");
            learningCenterimpl.closeConn();
            xml.append("<player>" +
                    "<playerId>" + tempPlayerObj.get(i).getPlayerId() + "</playerId>" +
                    "<fName>" + tempPlayerObj.get(i).getFirstName() + "</fName>" +
                    "<lName>" + tempPlayerObj.get(i).getLastName() + "</lName>" +
                    "<grade>" + tempPlayerObj.get(i).getGrade() + "</grade>" +
                    "<schoolId>" + tempPlayerObj.get(i).getSchoolId() + "</schoolId>" +
                    "<teacherId>" + tempPlayerObj.get(i).getTeacherUserId() + "</teacherId>" +
                    "<indexOfAppearance>-1</indexOfAppearance>" +
                    "<profileImageId>" + tempPlayerObj.get(i).getProfileImageName() + "</profileImageId>" +
                    "<coins>" + playerObj.getCoins() + "</coins>" +
                    "<points>" + playerObj.getTotalPoints() + "</points>" +
                    "<userName>" + tempPlayerObj.get(i).getUserName() + "</userName>" +
                    "<competeLevel>" + playerObj.getCompleteLevel() + "</competeLevel>" +
                    "</player>");
        }
        return xml.toString();
    }


    protected void setVisibilityOfStateAndZipCode(EditText edtZip,
                                                  Spinner spinnerState, RelativeLayout stateSpinnerLayout,
                                                  boolean isVisible) {
        if (isVisible) {
            edtZip.setVisibility(EditText.VISIBLE);
            spinnerState.setVisibility(Spinner.VISIBLE);
            stateSpinnerLayout.setVisibility(RelativeLayout.VISIBLE);
        } else {
            edtZip.setVisibility(EditText.GONE);
            spinnerState.setVisibility(Spinner.GONE);
            stateSpinnerLayout.setVisibility(RelativeLayout.GONE);
        }
    }

    @Override
    protected void onResume() {
        //for show ad dialog
        CommonUtils.showAdDialog(this);
        MathFriendzyHelper.TrackingScreen(this, TAG);
        BannerAds.getInstance(this).showAds();
        RateUsPops.getInstance(this).showRateUsPopUp();
        //HouseAds.getInstance().showAds(this);
        //HouseAds.getInstance().showAds(MyApplication.getAppContext());
        //DontLikeAds.getInstance(MyApplication.getAppContext()).showDontLikeAds();
        //RateUsPops.getInstance(MyApplication.getAppContext()).showRateUsPopUp();
        super.onResume();
    }

    //keyboar changes by Yashwant
    protected void changeCase(String tag){
        if(kwyboard_case_value) {
            if (!(tag.equalsIgnoreCase(" ") || tag.equalsIgnoreCase("\"")
                    || tag.equalsIgnoreCase("?") || tag.equalsIgnoreCase(","))){
                this.changecasevalue();
            }
        }
    }
}
