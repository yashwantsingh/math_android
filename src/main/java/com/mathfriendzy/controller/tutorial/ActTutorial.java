package com.mathfriendzy.controller.tutorial;



import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.customview.CustomeViewPager;
import com.mathfriendzy.customview.CustomeViewPager.OnSwipeOutListener;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.utils.CommonUtils;

public class ActTutorial extends ActBase {

	private final String TAG = this.getClass().getSimpleName();
	private CustomeViewPager tutorialPager = null;
	private ArrayList<Integer> tutorialImageIdList = null;
	private TutorialPagerAdapter adapter = null;
	private Button btnCross = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_tutorial);

		if(CommonUtils.LOG_ON)
			Log.d(TAG, "inside onCreate()");

		this.init();
		this.setWidgetsReferences();
		this.setListenerOnWidgets();
		this.setAdapter();

		if(CommonUtils.LOG_ON)
			Log.d(TAG, "outside onCreate()");
	}

	/**
	 * initialize the variables
	 */
	private void init(){
		tutorialImageIdList = CommonUtils.getEnglistTutorialIdList();
	}

	@Override
	protected void setWidgetsReferences() {

		if(CommonUtils.LOG_ON)
			Log.d(TAG, "inside setWidgetsReferences()");

		tutorialPager = (CustomeViewPager) findViewById(R.id.tutorialPager);
		btnCross = (Button) findViewById(R.id.btnCross);
		
		if(CommonUtils.LOG_ON)
			Log.d(TAG, "outside setWidgetsReferences()");
	}



	@Override
	protected void setTextFromTranslation() {

		if(CommonUtils.LOG_ON)
			Log.d(TAG, "inside setTextFromTranslation()");

		if(CommonUtils.LOG_ON)
			Log.d(TAG, "outside setTextFromTranslation()");
	}


	/**
	 * Set the pager adapter
	 */
	private void setAdapter() {
		adapter = new TutorialPagerAdapter(this, tutorialImageIdList);
		tutorialPager.setAdapter(adapter);
		
		tutorialPager.setOnSwipeOutListener(new OnSwipeOutListener() {

			@Override
			public void onSwipeOutAtStart() {
				//Log.e(TAG, "start out");			
			}

			@Override
			public void onSwipeOutAtEnd() {
				onSwipeOutAtEndAndFinish();
			}
		});
	}

	@Override
	protected void setListenerOnWidgets() {
		btnCross.setOnClickListener(this);
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnCross:
			this.onSwipeOutAtEndAndFinish();
			break;
		}
	}

	@Override
	public void serverResponse(HttpResponseBase httpResponseBase,
			int requestCode) {

	}

	@Override
	public void onBackPressed() {
		this.onSwipeOutAtEndAndFinish();
		super.onBackPressed();
	}

	public void onSwipeOutAtEndAndFinish(){
		Intent intent = new Intent();
		intent.putExtra("isShowDialog", !MathFriendzyHelper.isUserLogin(this));
		//intent.putExtra("isShowDialog", true);
		setResult(RESULT_OK, intent);
		finish();
	}
}
