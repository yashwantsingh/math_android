package com.mathfriendzy.controller.tutorial;

import com.mathfriendzy.R;
import com.mathfriendzy.model.language.translation.Translation;

import android.app.Dialog;
import android.content.Context;

public class TutorialDialog {

	private Context context;
	private Dialog dialog;
	private Translation transeletion = null;
	
	public TutorialDialog(Context context){
		this.context = context;
		transeletion = new Translation(context);
	}
	
	public void showTutorialDialog(){
		dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.activity_act_tutorial);
		dialog.show();
	}
	
}
