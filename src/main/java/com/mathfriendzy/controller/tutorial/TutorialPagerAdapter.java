package com.mathfriendzy.controller.tutorial;

import java.util.ArrayList;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mathfriendzy.R;

public class TutorialPagerAdapter extends PagerAdapter{

	private ArrayList<Integer> tutorialImageList = null;//contains the id of images
	private LayoutInflater mInflater;
	//private ActTutorial actTutorial = null;
	//private final String DRAWABLE_PREFF = "drawable://";
		
	/*private ImageLoader loader;//images loader
	private DisplayImageOptions options; // contains the display option for image in grid
	 */	
	TutorialPagerAdapter(Context context , ArrayList<Integer> tutorialImageList){
		//actTutorial = (ActTutorial) context;
		this.tutorialImageList = tutorialImageList;
		mInflater = LayoutInflater.from(context);
		
		/*loader = ImageLoader.getInstance();

		options = new DisplayImageOptions.Builder()
		.showImageForEmptyUri(R.drawable.ic_empty)
		.showImageOnFail(R.drawable.ic_error)
		.resetViewBeforeLoading(true)
		 .cacheOnDisc(true)
		.imageScaleType(ImageScaleType.EXACTLY)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.considerExifParams(true)
		.displayer(new FadeInBitmapDisplayer(300))
		.build();*/
	}
	
	@Override
	public int getCount() {
		return tutorialImageList.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return  view.equals(object);
	}

	@Override
	public void restoreState(Parcelable state, ClassLoader loader) {
	}

	@Override
	public Parcelable saveState() {
		return null;
	}
	
	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}
	
	@Override
	public Object instantiateItem(ViewGroup view, int position) {
		View imageLayout = mInflater.inflate(R.layout.tutorial_item_pager_layout, view, false);
		ImageView imageView = (ImageView) imageLayout.findViewById(R.id.image);
		//Button btnCross = (Button) imageLayout.findViewById(R.id.btnCross);
		//final ProgressBar spinner = (ProgressBar) imageLayout.findViewById(R.id.loading);
		/*loader.displayImage(DRAWABLE_PREFF + tutorialImageList.get(position) , 
				imageView, options , null);*/
		imageView.setImageResource(tutorialImageList.get(position));
		
		/*btnCross.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try{
					actTutorial.onSwipeOutAtEndAndFinish();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		});*/
		view.addView(imageLayout, 0);
		return imageLayout;
	}
}
