package com.mathfriendzy.controller.schoolfunctions;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.assessmenttest.AssessmentClass;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.friendzy.StudentChallengeActivity;
import com.mathfriendzy.controller.helpastudent.ActHelpAStudent;
import com.mathfriendzy.controller.homework.ActHomeWork;
import com.mathfriendzy.controller.player.LoginUserCreatePlayer;
import com.mathfriendzy.controller.player.LoginUserPlayerActivity;
import com.mathfriendzy.controller.player.TeacherPlayer;
import com.mathfriendzy.controller.resources.ActLessonCategories;
import com.mathfriendzy.controller.tutoringsession.ActTutoringSession;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.CheckUnlockStatusCallback;
import com.mathfriendzy.listener.TeacherFunctionDialogListener;
import com.mathfriendzy.model.helpastudent.CheckForTutorParam;
import com.mathfriendzy.model.helpastudent.CheckForTutorResponse;
import com.mathfriendzy.model.helpastudent.TutorAreaResponse;
import com.mathfriendzy.model.helpastudent.TutorAreaResponsesParam;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.CoinsFromServerObj;
import com.mathfriendzy.model.learningcenter.LearningCenteServerOperation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.PurchaseItemObj;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerDialogs;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.BuildApp;
import com.mathfriendzy.utils.CommonUtils;

import java.util.ArrayList;

import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_ID;

public class ActSchoolFunctions extends ActBase {

    private final String TAG = this.getClass().getSimpleName();

    private TextView txtReportFor = null;
    private Button btnAssessmentTest = null;
    private Button btnSchoolChallenges = null;
    private Button btnHomeworkQuizzess = null;
    private Button btnStudyGroup = null;
    private Button btnHelpAStudent = null;
    private Button btnTutoringSession = null;

    private TextView txtHWNotification 	= null;
    private String lblYouMustRegisterLoginToAccess = null;

    private TextView txtStudyPatentPending = null;
    private TextView txtHelpStudentPatentPending = null;

    //for account type
    private final String TEACHER = "0";
    private RegistereUserDto loginUser = null;
    private UserPlayerDto selectedPlayer = null;

    //for is not tutor message
    private String studentNotATutorAlertTitle = null;
    private TextView txtHelpAStudentCounter = null;
    private UserPlayerDto selectedPlayerData = null;

    //for getting first time when the screen is created
    private boolean getNumberOfActiveHomeworkLoadedFirstTime = false;
    private TextView txtHomeworkQuizzPatentPending = null;
    private TextView txtTutoringSessionActiveInput = null;

    //for Resources
    private Button btnResources = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_school_functions);

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside onCreate()");

        MathFriendzyHelper.initializeProcessDialog(this);

        selectedPlayerData = this.getPlayerData();

        this.init();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside onCreate()");
    }

    private void init() {
        loginUser = CommonUtils.getLoginUser(this);
        selectedPlayer = this.getPlayerData();
    }


    @Override
    protected void setWidgetsReferences() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside setWidgetsReferences()");

        txtTopbar 	= (TextView) findViewById(R.id.txtTopbar);
        txtReportFor = (TextView) findViewById(R.id.txtReportFor);
        btnAssessmentTest = (Button) findViewById(R.id.btnAssessmentTest);
        btnSchoolChallenges = (Button) findViewById(R.id.btnSchoolChallenges);
        btnHomeworkQuizzess = (Button) findViewById(R.id.btnHomeworkQuizzess);
        btnStudyGroup = (Button) findViewById(R.id.btnStudyGroup);
        btnHelpAStudent = (Button) findViewById(R.id.btnHelpAStudent);
        txtHWNotification = (TextView) findViewById(R.id.txtHWNotification);
        btnTutoringSession = (Button) findViewById(R.id.btnTutoringSession);

        txtStudyPatentPending = (TextView) findViewById(R.id.txtStudyPatentPending);
        txtHelpStudentPatentPending = (TextView) findViewById(R.id.txtHelpStudentPatentPending);
        txtHelpAStudentCounter = (TextView) findViewById(R.id.txtHelpAStudentCounter);
        txtHomeworkQuizzPatentPending = (TextView) findViewById(R.id.txtHomeworkQuizzPatentPending);
        txtTutoringSessionActiveInput = (TextView) findViewById(R.id.txtTutoringSessionActiveInput);

        btnResources = (Button) findViewById(R.id.btnResources);

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside setWidgetsReferences()");
    }

    @Override
    protected void setTextFromTranslation() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside setTextFromTranslation()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("mfTitleHomeScreen"));
        txtReportFor.setText(transeletion.getTranselationTextByTextIdentifier("pickerTitleSchool")
                + " " + transeletion.getTranselationTextByTextIdentifier("lblFunctions"));
        btnAssessmentTest.setText(transeletion.getTranselationTextByTextIdentifier("lblAssessmentTest"));
        btnSchoolChallenges.setText(transeletion.getTranselationTextByTextIdentifier("lblSchoolInfoSchoolText")
                + " " + transeletion.getTranselationTextByTextIdentifier("btnTitleChallenges"));
        btnHomeworkQuizzess.setText(transeletion.getTranselationTextByTextIdentifier("lblHomework")
                + "/" + transeletion.getTranselationTextByTextIdentifier("lblQuizzes"));
        btnStudyGroup.setText(transeletion.getTranselationTextByTextIdentifier("lblStudyGroup"));
        btnHelpAStudent.setText(transeletion.getTranselationTextByTextIdentifier("lblHelpAStudent"));
        txtStudyPatentPending.setText(transeletion.getTranselationTextByTextIdentifier("lblPatentPending"));
        txtHelpStudentPatentPending.setText(transeletion.getTranselationTextByTextIdentifier("lblPatentPending"));
        btnTutoringSession.setText(transeletion.getTranselationTextByTextIdentifier("btnTutoringSession"));
        lblYouMustRegisterLoginToAccess = transeletion
                .getTranselationTextByTextIdentifier("lblYouMustRegisterLoginToAccess");
        studentNotATutorAlertTitle = transeletion
                .getTranselationTextByTextIdentifier("studentNotATutorAlertTitle");
        txtHomeworkQuizzPatentPending.setText(transeletion.
                getTranselationTextByTextIdentifier("lblPatentPending"));
        btnResources.setText(transeletion.getTranselationTextByTextIdentifier("navigationTitleResources"));
        transeletion.closeConnection();

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside setTextFromTranslation()");

    }

    @Override
    protected void setListenerOnWidgets() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside setListenerOnWidgets()");

        btnAssessmentTest.setOnClickListener(this);
        btnSchoolChallenges.setOnClickListener(this);
        btnHomeworkQuizzess.setOnClickListener(this);
        btnStudyGroup.setOnClickListener(this);
        btnHelpAStudent.setOnClickListener(this);
        btnTutoringSession.setOnClickListener(this);
        btnResources.setOnClickListener(this);

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside setListenerOnWidgets()");

    }


    /**
     * Click For Assessment test
     */
    private void clickOnAssessmentTest(){
        SharedPreferences sheredPreferenceLogin = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
        if(sheredPreferenceLogin.getBoolean(IS_LOGIN, false)){
            AssessmentClass assessmentTest = new AssessmentClass(this);
            assessmentTest.startAssessmentActivity();
        }else{
            MathFriendzyHelper.showLoginRegistrationDialog(this , lblYouMustRegisterLoginToAccess);
        }
    }

    /**
     * Click for school challenges
     */
    private void clickOnSchoolChallenge(){
        this.checkPlayer();
    }

    /**
     * Click For Homework quizzes
     */
    private void clickOnHomeworkQuizzes(){
        this.checkForStudentSelection(MathFriendzyHelper.DIALOG_SCHOOL_HOMEWORK_QUIZZ);
    }

    private void goForHomeworkScreen(){
        MathFriendzyHelper.saveLoginPlayerDataIntoSharedPreff(this);
        UserPlayerDto selectedPlayerData = this.getPlayerData();
        String userId = selectedPlayerData.getParentUserId();
        if(MathFriendzyHelper.getAppUnlockStatus
                (MathFriendzyHelper.APP_UNLOCK_ID, userId, this) ==
                MathFriendzyHelper.APP_UNLOCK){
            startActivity(new Intent(ActSchoolFunctions.this , ActHomeWork.class));
        }else{
            this.checkForUnlock(MathFriendzyHelper.DIALOG_SCHOOL_HOMEWORK_QUIZZ);
        }
    }

    /**
     * Go for homework quizz
     */
    private void checkForUnlock(final String checkFor){
        MathFriendzyHelper.saveLoginPlayerDataIntoSharedPreff(this);
        UserPlayerDto selectedPlayerData = this.getPlayerData();

        if(selectedPlayerData != null){
            final String userId = selectedPlayerData.getParentUserId();
            final String playerId = selectedPlayerData.getPlayerid();

            if(MathFriendzyHelper.getAppUnlockStatus
                    (MathFriendzyHelper.APP_UNLOCK_ID, userId, this) ==
                    MathFriendzyHelper.APP_UNLOCK){
                if(!MathFriendzyHelper.isNeedToDownloadAppUnlockStatus(this)) {
                    startActivity(checkFor);
                    return;
                }
            }

            if(CommonUtils.isInternetConnectionAvailable(this)){
                //MathFriendzyHelper.showDialog();
                final ProgressDialog pd = ServerDialogs.getProgressDialog(this , "Please wait..." , 1);
                pd.show();
                MathFriendzyHelper.checkAppUnlockStatus(userId, playerId, this ,
                        new CheckUnlockStatusCallback() {
                            @Override
                            public void onRequestComplete(CoinsFromServerObj coinsFromServer) {
                                //MathFriendzyHelper.dismissDialog();
                                if(pd != null && pd.isShowing())
                                    pd.dismiss();
                                if(coinsFromServer != null){
                                    if(coinsFromServer.getAppUnlock() == MathFriendzyHelper.APP_UNLOCK){
                                        startActivity(checkFor);
                                    }else{
                                        /*DialogGenerator dg = new DialogGenerator(ActSchoolFunctions.this);
                                        dg.generateDailogForSchoolCurriculumUnlock
                                                (coinsFromServer , userId , playerId ,
                                                        coinsFromServer.getAppUnlock());*/
                                        MathFriendzyHelper.openSubscriptionDialog(getActivityObj() , checkFor);
                                    }
                                }else{
                                    CommonUtils.showInternetDialog(ActSchoolFunctions.this);
                                }
                            }
                        });
            }else{
                if(MathFriendzyHelper.getAppUnlockStatus
                        (MathFriendzyHelper.APP_UNLOCK_ID, userId, this) ==
                        MathFriendzyHelper.APP_UNLOCK){
                    startActivity(checkFor);
                }else{
                    CommonUtils.showInternetDialog(this);
                }
            }
        }
    }

    /**
     * Start activity after checking application status
     * @param checkFor
     */
    private void startActivity(String checkFor){
        if(checkFor.equalsIgnoreCase(MathFriendzyHelper.DIALOG_SCHOOL_HOMEWORK_QUIZZ))
            startActivity(new Intent(ActSchoolFunctions.this , ActHomeWork.class));
        else if(checkFor.equalsIgnoreCase(MathFriendzyHelper.DIALOG_SCHOOL_HELP_STUDENT))
            startActivity(new Intent(this , ActHelpAStudent.class));
    }

    /**
     * Check for tutor
     */
    private void checkForTutorForHelpAStudent(){
        if(loginUser != null){
            if(loginUser.getIsParent().equals(TEACHER)){
                goForHelpStudentScreen();
            }else{
                if(CommonUtils.isInternetConnectionAvailable(this)){
                    CheckForTutorParam param = new CheckForTutorParam();
                    param.setAction("checkIfStudnetATutor");
                    param.setUserId(selectedPlayer.getParentUserId());
                    param.setPlayerId(selectedPlayer.getPlayerid());
                    new MyAsyckTask(ServerOperation
                            .createPostRequestForCheckIsTutor(param)
                            , null, ServerOperationUtil.CHECK_FOR_TUTOR_REQUEST, this,
                            this, ServerOperationUtil.SIMPLE_DIALOG , true ,
                            getString(R.string.please_wait_dialog_msg))
                            .execute();
                }else{
                    CommonUtils.showInternetDialog(this);
                }
            }
        }else{
            goForHelpStudentScreen();//never execute but for the rare case
        }
    }

    private void goForHelpStudentScreen(){
        startActivity(new Intent(this , ActHelpAStudent.class));
    }

    /**
     * Click for study a group
     */
    private void clickOnStudyAGroup(){
        CommonUtils.comingSoonPopUp(this);
    }

    /**
     * click for help a student
     */
    private void clickOnHelpAStudent(){
        this.checkForStudentSelection(MathFriendzyHelper.DIALOG_SCHOOL_HELP_STUDENT);
    }

    private void clickOnTutoringSession() {
        /*startActivity(new Intent(this , ActTutoringSession.class));*/
        this.checkForStudentSelection(MathFriendzyHelper.DIALOG_TUTORING_SESSION);
    }

    private void goForTutoringSessionScreen(){
        startActivity(new Intent(this , ActTutoringSession.class));
    }

    private void clickOnResources(){
        //this.checkForStudentSelection(MathFriendzyHelper.DIALOG_RESOURCES);
        this.goForResourcesScreen();
    }

    private void goForResourcesScreen(){
        //startActivity(new Intent(this , ActResourceHome.class));
        startActivity(new Intent(this , ActLessonCategories.class));
    }

    /**
     * use to check player for challenge
     */
    private void checkPlayer()
    {
        SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
        if(sheredPreference.getBoolean(IS_LOGIN, false))
        {
            UserPlayerOperation userPlayer = new UserPlayerOperation(this);
            if(!userPlayer.isUserPlayersExist())
            {
                SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0);
                if(sheredPreferencePlayer.getString(PLAYER_ID, "").equals("")){
                    UserRegistrationOperation userObj = new UserRegistrationOperation(this);
                    if(userObj.getUserData().getIsParent().equals("0")){//0 for teacher
                        Intent intent = new Intent(this,TeacherPlayer.class);
                        startActivity(intent);
                    }
                    else{
                        Intent intent = new Intent(this,LoginUserPlayerActivity.class);
                        startActivity(intent);
                    }
                }
                else{
                    if(BuildApp.BUILD_APP_TYPE == BuildApp.AMAZON){
                        Intent intent = new Intent(ActSchoolFunctions.this,
                                StudentChallengeActivity.class);
                        startActivity(intent);
                    }else {
                        if (CommonUtils.isInternetConnectionAvailable(this)) {
                            new GetRequiredCoinsForPurchaseItem().execute(null, null, null);
                        } else {
                            CommonUtils.showInternetDialog(this);
                        }
                    }
                }
            }
            else{
                Intent intent = new Intent(this,LoginUserCreatePlayer.class);
                startActivity(intent);
            }
        }
        else{
            MathFriendzyHelper.showLoginRegistrationDialog(this , lblYouMustRegisterLoginToAccess);
        }
    }

    /**
     * This method call when user click on HomeWork
     */
    private void checkForStudentSelection(String checkFor){
        SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
        if(sheredPreference.getBoolean(IS_LOGIN, false))
        {
            UserPlayerOperation userPlayer = new UserPlayerOperation(this);
            if(!userPlayer.isUserPlayersExist())
            {
                SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0);
                if(sheredPreferencePlayer.getString(PLAYER_ID, "").equals("")){
                    UserRegistrationOperation userObj = new UserRegistrationOperation(this);
                    if(userObj.getUserData().getIsParent().equals("0")){//0 for teacher
                        Intent intent = new Intent(this,TeacherPlayer.class);
                        startActivity(intent);
                    }
                    else{
                        Intent intent = new Intent(this,LoginUserPlayerActivity.class);
                        startActivity(intent);
                    }
                }
                else{
                    if(checkFor.equalsIgnoreCase(MathFriendzyHelper.DIALOG_SCHOOL_HELP_STUDENT)){
                        this.checkForTutorForHelpAStudent();
                    }else if(checkFor.equalsIgnoreCase(MathFriendzyHelper.DIALOG_TUTORING_SESSION)){
                        this.goForTutoringSessionScreen();
                    }else if(checkFor.equalsIgnoreCase(MathFriendzyHelper.DIALOG_RESOURCES)){
                        this.goForResourcesScreen();
                    }else if(checkFor.equalsIgnoreCase(MathFriendzyHelper.DIALOG_SCHOOL_HOMEWORK_QUIZZ)) {
                        //this.goForHomeworkScreen();
                        this.checkForUnlock(checkFor);
                    }else{
                        this.checkForUnlock(checkFor);
                    }
                }
            }else{
                Intent intent = new Intent(this,LoginUserCreatePlayer.class);
                startActivity(intent);
            }
        }
        else{
            MathFriendzyHelper.showLoginRegistrationDialog(this , lblYouMustRegisterLoginToAccess);
        }
    }

    /**
     * Show the teacher function popup
     * @param popUpFor
     * @return
     */
    private boolean showTeacherFunctionPopUp(final String popUpFor
            , String txtTitle , boolean isShowWatchButton){
        if(MathFriendzyHelper.showTeacherFunctionPopup(this ,
                popUpFor , new TeacherFunctionDialogListener() {

                    @Override
                    public void clickOnWatchVedio(boolean isShow) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void clickOnDontShow(boolean isShow) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void clickOnClose(boolean isShow) {
                        clickOn(popUpFor);
                    }
                } , txtTitle , isShowWatchButton))
            return true;
        return false;
    }

    /**
     * Go after click on close button on Teacher function show vedio
     * @param dialogFor
     */
    private void clickOn(String dialogFor){
        if(MathFriendzyHelper.DIALOG_SCHOOL_ASSESSMENT_TEST.equals(dialogFor))
            this.clickOnAssessmentTest();
        else if(MathFriendzyHelper.DIALOG_SCHOOL_SCHOOL_CHALLENGE.equals(dialogFor)){
            this.clickOnSchoolChallenge();
        }else if(MathFriendzyHelper.DIALOG_SCHOOL_HOMEWORK_QUIZZ.equals(dialogFor)){
            this.clickOnHomeworkQuizzes();
        }else if(MathFriendzyHelper.DIALOG_SCHOOL_STUDY_GROUP.equals(dialogFor)){
            this.clickOnStudyAGroup();
        }else if(MathFriendzyHelper.DIALOG_SCHOOL_HELP_STUDENT.equals(dialogFor)){
            this.clickOnHelpAStudent();
        }else if(MathFriendzyHelper.DIALOG_TUTORING_SESSION.equals(dialogFor)){
            this.clickOnTutoringSession();
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnAssessmentTest:

                if(this.showTeacherFunctionPopUp
                        (MathFriendzyHelper.DIALOG_SCHOOL_ASSESSMENT_TEST ,
                                MathFriendzyHelper.getTextFromButton(btnAssessmentTest) , true))
                    return ;

                this.clickOnAssessmentTest();
                break;
            case R.id.btnSchoolChallenges:

                if(this.showTeacherFunctionPopUp
                        (MathFriendzyHelper.DIALOG_SCHOOL_SCHOOL_CHALLENGE ,
                                MathFriendzyHelper.getTextFromButton(btnSchoolChallenges) , true))
                    return ;

                this.clickOnSchoolChallenge();
                break;
            case R.id.btnHomeworkQuizzess:

                if(this.showTeacherFunctionPopUp
                        (MathFriendzyHelper.DIALOG_SCHOOL_HOMEWORK_QUIZZ ,
                                MathFriendzyHelper.getTextFromButton(btnHomeworkQuizzess) , true))
                    return ;

                this.clickOnHomeworkQuizzes();
                break;
            case R.id.btnStudyGroup:

                if(this.showTeacherFunctionPopUp
                        (MathFriendzyHelper.DIALOG_SCHOOL_STUDY_GROUP ,
                                MathFriendzyHelper.getTextFromButton(btnStudyGroup) , true))
                    return ;

                this.clickOnStudyAGroup();
                break;
            case R.id.btnHelpAStudent:

                if(this.showTeacherFunctionPopUp
                        (MathFriendzyHelper.DIALOG_SCHOOL_HELP_STUDENT,
                                MathFriendzyHelper.getTextFromButton(btnHelpAStudent) , false))
                    return ;

                this.clickOnHelpAStudent();
                break;
            case R.id.btnTutoringSession:
                if(this.showTeacherFunctionPopUp
                        (MathFriendzyHelper.DIALOG_TUTORING_SESSION,
                                MathFriendzyHelper.getTextFromButton(btnTutoringSession) , false))
                    return ;

                this.clickOnTutoringSession();
                break;
            case R.id.btnResources:
                this.clickOnResources();
                break;
        }
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase,
                               int requestCode) {
        if(requestCode == ServerOperationUtil.CHECK_FOR_TUTOR_REQUEST){
            CheckForTutorResponse response = (CheckForTutorResponse) httpResponseBase;
            if(response.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)){
                MathFriendzyHelper.updateIsTutor(this , selectedPlayer , response.getIsTutor());
                if(response.getIsTutor() == MathFriendzyHelper.YES){
                    goForHelpStudentScreen();
                }else{
                    //goForHelpStudentScreen();
                    MathFriendzyHelper.warningDialog(this , studentNotATutorAlertTitle);
                }
            }
        }else if(requestCode == ServerOperationUtil.GET_TUTOR_AREA_RESPONSES){
            TutorAreaResponse response = (TutorAreaResponse) httpResponseBase;
            if(response.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)) {
                MathFriendzyHelper.saveTutorAreaResponse(this ,
                        selectedPlayer.getParentUserId()
                        , selectedPlayer.getPlayerid() ,
                        response.getTutorResponses());
                MathFriendzyHelper.saveNumberOfActiveHomeWorkFromServer
                        (this, selectedPlayerData.getParentUserId(),
                                selectedPlayerData.getPlayerid(),
                                response.getHwInput());
                MathFriendzyHelper.saveTutorSessionResponse(this ,
                        selectedPlayer.getParentUserId()
                        , selectedPlayer.getPlayerid() ,
                        response.getTutorSessionResponses());

                this.showTutorAreaResponsesCounter(response.getTutorResponses());
                //this.setNumerOfActiveHomeWorkNotification();
                this.showTutorSessionResponsesCounter(response.getTutorSessionResponses());
                this.showHomeworkInput(response.getHwInput());
            }
        }
    }

    /**
     * Show tutor area response counter
     * @param responseCounter
     */
    private void showTutorAreaResponsesCounter(int responseCounter){
        if(responseCounter > 0){
            txtHelpAStudentCounter.setVisibility(TextView.VISIBLE);
            txtHelpAStudentCounter.setText(responseCounter + "");
        }else{
            txtHelpAStudentCounter.setVisibility(TextView.GONE);
        }
    }

    /**
     * Show tutor area response counter
     * @param responseCounter
     */
    private void showTutorSessionResponsesCounter(int responseCounter){
        if(responseCounter > 0){
            txtTutoringSessionActiveInput.setVisibility(TextView.VISIBLE);
            txtTutoringSessionActiveInput.setText(responseCounter + "");
        }else{
            txtTutoringSessionActiveInput.setVisibility(TextView.GONE);
        }
    }

    private void showHomeworkInput(int responseCounter){
        if(responseCounter > 0){
            txtHWNotification.setVisibility(TextView.VISIBLE);
            txtHWNotification.setText(responseCounter + "");
        }else{
            txtHWNotification.setVisibility(TextView.GONE);
        }
    }

    /**
     * This class get coins from server
     * @author Shilpi
     *
     */
    class GetRequiredCoinsForPurchaseItem extends AsyncTask<Void, Void, Void>
    {
        private String apiString = null;
        private CoinsFromServerObj coindFromServer;
        private ProgressDialog pg = null;
        String userId;
        String playerId;

        GetRequiredCoinsForPurchaseItem()
        {
            SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(IS_CHECKED_PREFF, 0);
            userId   = sharedPreffPlayerInfo.getString("userId", "");
            playerId = sharedPreffPlayerInfo.getString("playerId", "");
            apiString = "userId="+userId+"&playerId="+playerId;
        }

        @Override
        protected void onPreExecute()
        {
            pg = CommonUtils.getProgressDialog(ActSchoolFunctions.this);
            pg.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            LearningCenteServerOperation learnignCenterOpr = new LearningCenteServerOperation();
            coindFromServer = learnignCenterOpr.getSubscriptionInfoForUser(apiString);
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            pg.cancel();

            if(coindFromServer != null){
                //update app status in local database
                if(coindFromServer.getAppUnlock() != -1){

                    ArrayList<PurchaseItemObj> purchaseItem = new ArrayList<PurchaseItemObj>();
                    PurchaseItemObj purchseObj = new PurchaseItemObj();
                    purchseObj.setUserId(userId);
                    purchseObj.setItemId(100);
                    purchseObj.setStatus(coindFromServer.getAppUnlock());
                    purchaseItem.add(purchseObj);

                    LearningCenterimpl learningCenter = new LearningCenterimpl(ActSchoolFunctions.this);
                    learningCenter.openConn();

                    learningCenter.deleteFromPurchaseItem(userId , 100);
                    learningCenter.insertIntoPurchaseItem(purchaseItem);
                    learningCenter.closeConn();
                }

                if(coindFromServer.getAppUnlock() == 1)
                {
                    UserRegistrationOperation userObj = new UserRegistrationOperation(ActSchoolFunctions.this);

                    if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
                    {
                        SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0);


                        if(sheredPreferencePlayer.getString(PLAYER_ID, "").equals(""))
                        {
                            Intent intent = new Intent(ActSchoolFunctions.this,
                                    StudentChallengeActivity.class);
                            intent.putExtra("isTeacher", true);
                            startActivity(intent);

                        }
                        else
                        {
                            Intent intent = new Intent(ActSchoolFunctions.this,
                                    StudentChallengeActivity.class);
                            //intent.putExtra("isTeacher", false);
                            startActivity(intent);
                        }
                        //end changes
                    }
                    else
                    {
                        Intent intent = new Intent(ActSchoolFunctions.this,
                                StudentChallengeActivity.class);
                        startActivity(intent);
                    }
                }
                else
                {
                    /*DialogGenerator dg = new DialogGenerator(ActSchoolFunctions.this);
                    dg.generateDailogForSchoolCurriculumUnlock(coindFromServer , userId , playerId
                            , coindFromServer.getAppUnlock());*/
                    MathFriendzyHelper.openSubscriptionDialog(getActivityObj() ,
                            MathFriendzyHelper.DIALOG_SCHOOL_SCHOOL_CHALLENGE);
                }
            }else{
                CommonUtils.showInternetDialog(ActSchoolFunctions.this);
            }
            super.onPostExecute(result);
        }
    }

    @Override
    protected void onResume() {
        //this.setNumerOfActiveHomeWorkNotification();
        this.setNumberOfActiveHelpAStudentTutor();
        super.onResume();
    }

    /**
     * This method set the number of active home work notification
     * added in the second phase , Student Phase
     */
    private void setNumerOfActiveHomeWorkNotification(){
        try{
            if(MathFriendzyHelper.isUserLogin(this)){
                UserPlayerDto userPlayer = MathFriendzyHelper.getSelectedPlayerDataById
                        (this, MathFriendzyHelper.getSelectedPlayerID(this));
                int numberOfActiveWork = MathFriendzyHelper.getNumberOfActiveHomeWork
                        (this, userPlayer.getParentUserId(), userPlayer.getPlayerid());
                if(numberOfActiveWork != 0){
                    txtHWNotification.setVisibility(TextView.VISIBLE);
                    txtHWNotification.setText(numberOfActiveWork + "");
                }else{
                    txtHWNotification.setVisibility(TextView.GONE);
                }
            }else{
                txtHWNotification.setVisibility(TextView.GONE);
            }
        }catch(Exception e){
            e.printStackTrace();
            txtHWNotification.setVisibility(TextView.GONE);
        }
    }

    /**
     * Set Active Number of counter
     */
    private void setNumberOfActiveHelpAStudentTutor(){
        if(CommonUtils.isInternetConnectionAvailable(this) && selectedPlayerData != null
                && !getNumberOfActiveHomeworkLoadedFirstTime){
            getNumberOfActiveHomeworkLoadedFirstTime = true;
            TutorAreaResponsesParam param = new TutorAreaResponsesParam();
            param.setAction("tutorAreaResponses");
            param.setUserId(selectedPlayerData.getParentUserId());
            param.setPlayerId(selectedPlayerData.getPlayerid());
            new MyAsyckTask(ServerOperation
                    .createPostRequestForTutorAreaResponses(param)
                    , null, ServerOperationUtil.GET_TUTOR_AREA_RESPONSES, this,
                    this, ServerOperationUtil.SIMPLE_DIALOG , true ,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        }else{
            try {
                if (MathFriendzyHelper.isUserLogin(this) && selectedPlayerData != null) {
                    this.showTutorAreaResponsesCounter
                            (MathFriendzyHelper.getTutorAreaResponses(this,
                                    selectedPlayer.getParentUserId()
                                    , selectedPlayer.getPlayerid()));
                    this.showTutorSessionResponsesCounter(MathFriendzyHelper.
                            getTutorSessionResponses(this,
                                    selectedPlayer.getParentUserId()
                                    , selectedPlayer.getPlayerid()));
                    this.showHomeworkInput(MathFriendzyHelper.
                            getNumberOfActiveHomeWorkFromServer(this,
                                    selectedPlayer.getParentUserId()
                                    , selectedPlayer.getPlayerid()));
                }
            }catch(Exception e){
                txtHelpAStudentCounter.setVisibility(TextView.GONE);
                txtTutoringSessionActiveInput.setVisibility(TextView.GONE);
                txtHWNotification.setVisibility(TextView.GONE);
                e.printStackTrace();
            }
        }
    }

    private Activity getActivityObj(){
        return this;
    }
}
