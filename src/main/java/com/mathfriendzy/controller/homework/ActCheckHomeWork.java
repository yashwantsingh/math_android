package com.mathfriendzy.controller.homework;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.homework.addhomeworksheet.ActViewPdf;
import com.mathfriendzy.controller.homework.assignhomeworkquiz.ActAssignCustomAns;
import com.mathfriendzy.controller.homework.homwworkworkarea.AddUrlToQuestionDialog;
import com.mathfriendzy.controller.homework.homwworkworkarea.AddUrlToWorkArea;
import com.mathfriendzy.controller.homework.homwworkworkarea.HomeWorkWorkArea;
import com.mathfriendzy.controller.homework.homwworkworkarea.OnUrlSaveCallback;
import com.mathfriendzy.controller.resources.ActSelectedResource;
import com.mathfriendzy.customview.AutoResizeEditText;
import com.mathfriendzy.customview.EditTextBlink;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.helper.YesNoListenerInterface;
import com.mathfriendzy.listener.OnRequestComplete;
import com.mathfriendzy.listener.OnRequestSuccess;
import com.mathfriendzy.model.homework.AddCustomeAnswerByStudentParam;
import com.mathfriendzy.model.homework.CheckUserAnswer;
import com.mathfriendzy.model.homework.CustomeAns;
import com.mathfriendzy.model.homework.CustomePlayerAns;
import com.mathfriendzy.model.homework.CustomeResult;
import com.mathfriendzy.model.homework.GetHomeWorkForStudentWithCustomeResponse;
import com.mathfriendzy.model.homework.HomeWorkImpl;
import com.mathfriendzy.model.homework.MultipleChoiceAnswer;
import com.mathfriendzy.model.homework.SaveHomeWorkServerResponse;
import com.mathfriendzy.model.homework.StudentAnsBase;
import com.mathfriendzy.model.homework.UserFillAnswer;
import com.mathfriendzy.model.homework.checkhomework.GetDetailOfHomeworkWithCustomeResponse;
import com.mathfriendzy.model.homework.checkhomework.GetQuestionDetailForTeacherResponse;
import com.mathfriendzy.model.homework.checkhomework.GetQuestionDetailsForTeacherParam;
import com.mathfriendzy.model.homework.checkhomework.SaveCheckTeacherChangesParam;
import com.mathfriendzy.model.homework.secondworkarea.AddDuplicateWorkAreaParam;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.resource.GetGooruResourceForHWResponse;
import com.mathfriendzy.model.resource.GetGooruResourcesForHWParam;
import com.mathfriendzy.notification.HelpSetudentResponse;
import com.mathfriendzy.notification.TutorImageUpdateNotif;
import com.mathfriendzy.pdfoperation.PDFOperation;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerDialogs;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.soringclasses.SortByCorrectAnsForTeacher;
import com.mathfriendzy.soringclasses.SortByWrongAnswerForTeacher;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;

import twitter4j.internal.org.json.JSONObject;

public class ActCheckHomeWork extends ActBase {

    private final String TAG = this.getClass().getSimpleName();

    private TextView txtTeacherName = null;
    private TextView txtGradeAndDueData = null;
    private TextView txtWorkTitle = null;
    private TextView txtStudentAnswer = null;
    private Button btnCalculate = null;

    private LinearLayout layout = null;

    private String teacherName = null;
    private String grade = null;
    private String dueDate = null;

    private CustomeResult customeResult = null;
    private GetHomeWorkForStudentWithCustomeResponse homeWorkQuizzData = null;
    private RelativeLayout keyboardLayout = null;
    private RelativeLayout numericLayout = null;
    private RelativeLayout sumbolicLayout = null;
    //private Animation animation = null;

    //for fillIn
    /*private ArrayList<TextView> ansBoxlist = null;
    private ArrayList<CustomeAns> fillAnsList = null;*/
    private ArrayList<UserFillAnswer> userFillInAnsList = null;
    //   private TextView selectedTextView = null;

    //for true/false and yes/ no answer
    private ArrayList<CheckUserAnswer> checkUserAnswer = null;

    //for multiple choice ans
    private ArrayList<MultipleChoiceAnswer> multipleChoiceAnsList = null;
    private UserPlayerDto selectedPlayerData = null;
    private int userScore = 0;

    private String pleaseAnswerAllQuestionMsg = null;
    private String pleaseAnsAtleastOneQuestion = null;
    private String youWouldNotAbleToChangeAnsMsg = null;
    private String yesButtonText = null;
    private String noButtonText = null;

    private int zeroCredit = 0;
    private int halfCredit = 0;
    private int fullCredit = 0;

    private String zeroCrediteText = null;
    private String halFCredittext = null;
    private String fullCreditText = null;
    private String creditText = null;
    private String avgScoreText = null;
    private String answerText = null;

    private TextView txtZeroCreditShowColor = null;
    private TextView txtHaltCreditShowColor = null;
    private TextView txtFullCreditShowColor = null;

    private ArrayList<CustomePlayerAns> customePlayListByStudent = null;

    public boolean isAllowChanges = false;
    //private boolean isNeedToUpdateOnServer = true;

    //list of image which need to upload when user click on plus button
    private ArrayList<String> changesImageNameList = null;
    public boolean isExpireQuizz = false;
    private SaveHomeWorkServerResponse resultAfterPlayed = null;

    //message text
    /*private String getHelpText = null;
    private String workAreaText = null;
	private String correctAnsForThisProblemText = null;
	private String youMustNowShowHowText = null;*/

    //when user click on back pressed and save the user changes on server then its value will true
    private boolean isClickOnBackPressed = false;

    private final int START_WORK_AREA_REQUEST_CODE = 1001;
    private final int START_ACT_PLUS_REQUEST_CODE = 1002;
    private final int START_ACT_OPEN_PDF_REQUEST_CODE = 1003;

    //for work area text
    public StudentAnsBase clickedStudent = null;
    public CustomePlayerAns clickedPlayerAns = null;

    private final int HALF_CREDIT = 3;
    private final int NONE_CREDITT = 1;
    private final int FULL_CREDIT = 4;
    private final int ZERO_CREDIT = 2;

    //open act for teacher or student , changes for check homework
    private boolean isCalledFortecherCheckHW = false;
    //student detail with homework data
    private GetDetailOfHomeworkWithCustomeResponse studentDetail = null;

    private String lblYourTeacherWillNotAllowYouToChange = null;

    //for view sheet
    private Button btnViewSheet = null;
    private String cropImageUriFromPdf = "";

    //for showing popup on the view pdf Actvity (ActViewPdf)
    public static boolean isNeedToShowPopUp = false;
    private boolean isTab = false;
    //Contains the name list of the downloaded images on this screen
    private ArrayList<String> downloadImageList = null;
    private static ActCheckHomeWork currentObj = null;
    private int numberOfActiveOponentInput = 0;
    private Button clickedbtnRoughWork = null;
    private String alertYouCantChangeYourAnswer =
            "You can't add or change your answers after the past due date of your homework";

    private LinkedHashMap<String, View> putViewOnKeyQuestionNumber = null;
    private String alertNoHomeworkSheetAttached = null;
    private Button btnViewResources = null;


    private String lblAnswerOnWorkAreaCreditGiven = "Answer on Work Area Credit will be given when teacher views the answer.";
    private String lblOnWorkArea = "On Work Area";

    //Student Viewed Answer Change
    private RelativeLayout rlStudentViewedAnswer = null;
    private TextView txtStudentViewedAnswer = null;

    //siddhiinfosoft
    private LinearLayout ll_visible_view = null, ll_ans_box_selected = null;
    private boolean is_keyboard_show = false;
    private LinearLayout ll_selected_view = null;
    protected Animation animation = null;
    public int dpwidth = 600;
    private EditText selectedTextView = null, selected_ed_2 = null;
    //boolean is_on_calculate = false;
    private final int FILL_IN_TYPE = 1;

    //showing correct and wrong answer
    private ImageView imgInfo = null;
    private Button btnShortAns = null;
    private String lblGreenNumberAndRedNumText = null;
    private boolean isShortByCorrectAns = false;


    //Add Links
    private Button btnLinks = null;
    private ArrayList<AddUrlToWorkArea> urlList = null;
    private AddUrlToQuestionDialog addUrlLinkDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_check_home_work);

        try {
            DisplayMetrics displayMetrics = ActCheckHomeWork.this.getResources().getDisplayMetrics();
            // float dpWidth2 = displayMetrics.widthPixels / displayMetrics.density;
            float dpWidth2 = displayMetrics.widthPixels;

            dpwidth = Math.round(dpWidth2);

            if (dpwidth < 600) {
                dpwidth = 600;
            }

        } catch (Exception e) {
            dpwidth = 600;
        }


        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside onCreate()");

        isNeedToShowPopUp = false;

        selectedPlayerData = this.getPlayerData();

        isTab = getResources().getBoolean(R.bool.isTablet);

        //for downloaded images
        downloadImageList = new ArrayList<String>();
        this.initializeTheDownloadImageListFromLocalImageList();

        this.initializeCurrentObj();
        this.init();
        this.getIntentValues();
        this.initilizeAllowChangesVariable();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        //this.setVisibilityOfCalculateButton();
        this.setVisibilityOfViewSheetButton();
        this.setCustomeDataToListLayout();
        this.setCreditAndAvgScore();

        if (/*!isExpireQuizz && */(!customeResult.isAlreadyPlayed())) {
            this.calculateAns(false, true);
        }
        //MathFriendzyHelper.deleteAllImages();

        this.setViewResourcesButtonVisibility();
        this.setVisibilityOfStudentViewedAnswerLayout();
        this.setShortAnswerWidgetsForTeacheer();
        this.setVisibilityOfCalculateButton();//Not to move above this method its dependency on setCustomeDataToListLayout()
        this.setVisibilityOfBtnLinks();

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "outside onCreate()");
    }

    /**
     * Get all the images from local which need to be send to server when intetnet is connected
     */
    private void initializeTheDownloadImageListFromLocalImageList() {
        try {
            HomeWorkImpl implObj = new HomeWorkImpl(this);
            implObj.openConnection();
            ArrayList<String> list = implObj.fetchAllImageNameWithReplaced();
            implObj.closeConnection();
            downloadImageList.addAll(list);
            //Log.e(TAG, "list " + list + " downloadImageList " + downloadImageList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Initialize variables
     */
    private void init() {
        /*ansBoxlist = new ArrayList<TextView>();
        fillAnsList = new ArrayList<CustomeAns>();*/

        userFillInAnsList = new ArrayList<UserFillAnswer>();
        checkUserAnswer = new ArrayList<CheckUserAnswer>();
        multipleChoiceAnsList = new ArrayList<MultipleChoiceAnswer>();
        customePlayListByStudent = new ArrayList<CustomePlayerAns>();
        changesImageNameList = new ArrayList<String>();
        putViewOnKeyQuestionNumber = new LinkedHashMap<String, View>();
    }

    /**
     * Get set intent values
     */
    private void getIntentValues() {
        teacherName = this.getIntent().getStringExtra("teacherName");
        grade = this.getIntent().getStringExtra("grade");
        dueDate = this.getIntent().getStringExtra("dueDate");
        customeResult = (CustomeResult) this.getIntent().getSerializableExtra("customeDetail");
        homeWorkQuizzData = (GetHomeWorkForStudentWithCustomeResponse)
                this.getIntent().getSerializableExtra("homeWorkQuizzData");
        isExpireQuizz = this.getIntent().getBooleanExtra("isExpireQuizz", false);

        //for check homework
        isCalledFortecherCheckHW = this.getIntent()
                .getBooleanExtra("isCalledFortecherCheckHW", false);
        if (isCalledFortecherCheckHW) {
            studentDetail = (GetDetailOfHomeworkWithCustomeResponse)
                    this.getIntent().getSerializableExtra("studentDetail");
        }
        numberOfActiveOponentInput = customeResult.getNumberOfActiveInput();

        this.initializeAddUrlList();
    }

    /**
     * Initialize the allow change variable
     */
    private void initilizeAllowChangesVariable() {
        if (isExpireQuizz) {
            isAllowChanges = false;
        } else {
            if (customeResult.getAllowChanges() == 1 ||
                    !customeResult.isAlreadyPlayed()) {
                isAllowChanges = true;
            } else {
                if (this.isAllAnsGiven())
                    isAllowChanges = false;
                else//chage for to update the answer not given
                    isAllowChanges = true;
            }
        }
    }

    @Override
    protected void setWidgetsReferences() {

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside setWidgetsReferences()");

        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        txtTeacherName = (TextView) findViewById(R.id.txtTeacherName);
        txtGradeAndDueData = (TextView) findViewById(R.id.txtGradeAndDueData);
        txtWorkTitle = (TextView) findViewById(R.id.txtWorkTitle);
        txtStudentAnswer = (TextView) findViewById(R.id.txtStudentAnswer);
        btnCalculate = (Button) findViewById(R.id.btnCalculate);

        layout = (LinearLayout) findViewById(R.id.checkHomeWorkListLayout);

        this.setBottomLayoutWidgetsReferences();

        //for keyboard
        keyboardLayout = (RelativeLayout) findViewById(R.id.rl_math_keyboard);
        numericLayout = (RelativeLayout) findViewById(R.id.numericLayout);
        sumbolicLayout = (RelativeLayout) findViewById(R.id.sumbolicLayout);

        txtZeroCreditShowColor = (TextView) findViewById(R.id.txtZeroCreditShowColor);
        txtHaltCreditShowColor = (TextView) findViewById(R.id.txtHaltCreditShowColor);
        txtFullCreditShowColor = (TextView) findViewById(R.id.txtFullCreditShowColor);

        this.setWidgetsReferencesForKeyboard();

        this.setcustomkeyboardwidgetsreference();  // siddhiinfosoft

        //for view sheet
        btnViewSheet = (Button) findViewById(R.id.btnViewSheet);
        btnViewResources = (Button) findViewById(R.id.btnViewResources);


        //Student Viewed Answer Change
        rlStudentViewedAnswer = (RelativeLayout) findViewById(R.id.rlStudentViewedAnswer);
        txtStudentViewedAnswer = (TextView) findViewById(R.id.txtStudentViewedAnswer);

        kb_btn_123_delete.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        is_touch_kb = true;
                        Handler hnd = new Handler();
                        hnd.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ondeleteclick();
                            }
                        }, 200);
                        break;
                    case MotionEvent.ACTION_UP:
                        is_touch_kb = false;
                        break;
                }
                return true;
            }
        });

        kb_btn_abc_delete.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        is_touch_kb = true;
                        Handler hnd = new Handler();
                        hnd.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ondeleteclick();
                            }
                        }, 200);
                        break;
                    case MotionEvent.ACTION_UP:
                        is_touch_kb = false;
                        break;
                }
                return true;
            }
        });

        kb_btn_123_previous.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        is_touch_kb_pre = true;
                        Handler hnd = new Handler();
                        hnd.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                onpreviousclick();
                            }
                        }, 200);
                        break;
                    case MotionEvent.ACTION_UP:
                        is_touch_kb_pre = false;
                        break;
                }
                return true;
            }
        });

        kb_btn_123_next.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        is_touch_kb_next = true;
                        Handler hnd = new Handler();
                        hnd.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                onnextclick();
                            }
                        }, 200);
                        break;
                    case MotionEvent.ACTION_UP:
                        is_touch_kb_next = false;
                        break;
                }
                return true;
            }
        });


        //showing correct and wrong answer
        imgInfo = (ImageView) findViewById(R.id.imgInfo);
        btnShortAns = (Button) findViewById(R.id.btnShortAns);


        //Add Links
        btnLinks = (Button) findViewById(R.id.btnLinks);

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "outside setWidgetsReferences()");

    }

    /**
     * Set View resource button visibility
     */
    private void setViewResourcesButtonVisibility() {
        try {
            //if(isTab && !isCalledFortecherCheckHW){
            //if (!isCalledFortecherCheckHW) {
            if (customeResult.getResourceAdded()
                    .equalsIgnoreCase(MathFriendzyHelper.YES + "")) {
                btnViewResources.setVisibility(Button.VISIBLE);
            } else {
                btnViewResources.setVisibility(Button.GONE);
            }
        /*} else {
            btnViewResources.setVisibility(Button.GONE);
        }*/
            //btnViewResources.setVisibility(Button.GONE);
        } catch (Exception e) {
            e.printStackTrace();
            btnViewResources.setVisibility(Button.GONE);
        }
    }


    @Override
    protected void setListenerOnWidgets() {

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside setListenerOnWidgets()");

        btnCalculate.setOnClickListener(this);

        //for view sheet
        btnViewSheet.setOnClickListener(this);

        //for keyboard
        this.setListenerOnKeyBoardLayoutButton();

        layout.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard();
                return false;
            }
        });

        btnViewResources.setOnClickListener(this);

        imgInfo.setOnClickListener(this);
        btnShortAns.setOnClickListener(this);


        //Add Links
        btnLinks.setOnClickListener(this);

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "outside setListenerOnWidgets()");

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //  hideKeyboard();
        return super.onTouchEvent(event);
    }

    @Override
    protected void setTextFromTranslation() {

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside setListenerOnWidgets()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblCheckHomework") + "/" +
                transeletion.getTranselationTextByTextIdentifier("lblQuizzes"));

        txtStudentAnswer.setText(transeletion.getTranselationTextByTextIdentifier("lblStudentAnswer"));
        if (isCalledFortecherCheckHW) {//set student name
            txtTeacherName.setText(transeletion.getTranselationTextByTextIdentifier("mfLblStudentsName")
                    + ": " + teacherName);
        } else {
            txtTeacherName.setText(transeletion.getTranselationTextByTextIdentifier("mfLblTeacherName")
                    + ": " + teacherName);
        }

        txtGradeAndDueData.setText(/*transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade")
                + ": " + grade + "          "
                + */transeletion.getTranselationTextByTextIdentifier("lblDueDate") + ": " + dueDate);
        txtWorkTitle.setText(customeResult.getTitle());

        txtTotalProblems.setText(transeletion.getTranselationTextByTextIdentifier
                ("lblTotal") + " " + transeletion.getTranselationTextByTextIdentifier("lblNumOfProblems")
                + ": " + customeResult.getProblem());

        creditText = transeletion.getTranselationTextByTextIdentifier("lblCredit");
        zeroCrediteText = transeletion.getTranselationTextByTextIdentifier("lblZero");
        halFCredittext = transeletion.getTranselationTextByTextIdentifier("lblHalf");
        fullCreditText = transeletion.getTranselationTextByTextIdentifier("lblFull");
        avgScoreText = transeletion.getTranselationTextByTextIdentifier
                ("lblAverage") + " " + transeletion.getTranselationTextByTextIdentifier("mfBtnTitleScore");
        answerText = transeletion.getTranselationTextByTextIdentifier("mfLblAnswers");

        txtZeroCreditShowColor.setText(zeroCrediteText + " " + creditText);
        txtHaltCreditShowColor.setText(halFCredittext + " " + creditText);
        txtFullCreditShowColor.setText(fullCreditText + " " + creditText);

        pleaseAnswerAllQuestionMsg = transeletion.getTranselationTextByTextIdentifier("lblPleaseFillAllAnsCustomHomework");
        pleaseAnsAtleastOneQuestion = transeletion.getTranselationTextByTextIdentifier("lblPleaseAnswerAtLeastOneQuestion");

        youWouldNotAbleToChangeAnsMsg = transeletion.getTranselationTextByTextIdentifier("lblYouWontBeAbleToChangeAnswers");
        yesButtonText = transeletion.getTranselationTextByTextIdentifier("btnTitleYes");
        noButtonText = transeletion.getTranselationTextByTextIdentifier("lblNo");

		/*getHelpText = transeletion.getTranselationTextByTextIdentifier("lblGetHelp");
        workAreaText = transeletion.getTranselationTextByTextIdentifier("titleWorkArea");

		correctAnsForThisProblemText = transeletion.getTranselationTextByTextIdentifier("lblTheCorrectAnswerForProblem");
		youMustNowShowHowText        = transeletion.getTranselationTextByTextIdentifier("lblYouMustNowShow");*/

        lblYourTeacherWillNotAllowYouToChange = transeletion.getTranselationTextByTextIdentifier
                ("lblYourTeacherWillNotAllowYouToChange");

        //for view sheet(pdf) button text
        //if(isTab){
        btnViewSheet.setText(transeletion.getTranselationTextByTextIdentifier("lblAssignments"));
        btnViewResources.setText(transeletion.getTranselationTextByTextIdentifier("navigationTitleResources"));
        alertNoHomeworkSheetAttached = transeletion.getTranselationTextByTextIdentifier
                ("alertNoHomeworkSheetAttached");
        /*}else{
            btnViewSheet.setText(transeletion.getTranselationTextByTextIdentifier("lblView"));
		}*/
        alertYouCantChangeYourAnswer = transeletion.getTranselationTextByTextIdentifier
                ("lblSorryYouAreNotAllowedToPlay");
        lblAnswerOnWorkAreaCreditGiven = transeletion.getTranselationTextByTextIdentifier("lblAnsOnWorkArea");


        //student viewd answer change
        txtStudentViewedAnswer.setText(transeletion.getTranselationTextByTextIdentifier("lblStudent")
                + " " + transeletion.getTranselationTextByTextIdentifier("lblViewedAnswer"));
        lblGreenNumberAndRedNumText = transeletion.getTranselationTextByTextIdentifier("lblGreenNumberAndRedNumText");
        //btnLinks.setText(transeletion.getTranselationTextByTextIdentifier("lblStudent"));
        transeletion.closeConnection();

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "outside setListenerOnWidgets()");
    }


    /**
     * Set the visibility of calculte button
     */
    private void setVisibilityOfCalculateButton() {
        if (isExpireQuizz) {
            //isAllowChanges = false;
            btnCalculate.setVisibility(Button.GONE);
            /*if (isCalledFortecherCheckHW){
                btnCalculate.setVisibility(Button.GONE);
            } else{
                btnCalculate.setVisibility(Button.VISIBLE);
            }*/
        } else {
            if (customeResult.getAllowChanges() == 1 ||
                    !customeResult.isAlreadyPlayed()) {
                isAllowChanges = true;
                btnCalculate.setVisibility(Button.VISIBLE);
            } else {
                /*isAllowChanges = false;
                btnCalculate.setVisibility(Button.GONE);
                try {
                    if (this.getPlayerAnsByQuestionId
                            (customeResult.getCustomeAnsList()
                                    .get(0).getQueNo()).getAns().length() > 0) {
                        isAllowChanges = false;
                        btnCalculate.setVisibility(Button.GONE);
                    } else {
                        isAllowChanges = true;
                        btnCalculate.setVisibility(Button.VISIBLE);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }*/
                try {
                    if (this.isAllAnsGiven()) {
                        //isAllowChanges = false;
                        btnCalculate.setVisibility(Button.INVISIBLE);
                    } else {
                        //isAllowChanges = true;
                        btnCalculate.setVisibility(Button.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //isAllowChanges = true;
                    btnCalculate.setVisibility(Button.VISIBLE);
                }
            }
        }
    }

    /**
     * Set the visibility of view sheet button
     */
    private void setVisibilityOfViewSheetButton() {
        /*if (isCalledFortecherCheckHW) {
            btnViewSheet.setVisibility(Button.GONE);
        } else {*/
        if (customeResult.getSheetName() != null
                && customeResult.getSheetName().length() > 0) {
            btnViewSheet.setVisibility(Button.VISIBLE);
        } else {
            btnViewSheet.setVisibility(Button.GONE);
        }
        //}
    }


    /**
     * Show the keyboard
     */
//    private void showKeyboard(){
//        //animation = AnimationUtils.loadAnimation(this, R.anim.bottom_up_animation);
//        keyboardLayout.setVisibility(RelativeLayout.VISIBLE);
//        //keyboardLayout.setAnimation(animation);
//    }

    /**
     * Hide the keyboard
     */
//    private void hideKeyboard(){
//		/*if(keyboardLayout.getVisibility() == RelativeLayout.VISIBLE){
//			animation = AnimationUtils.loadAnimation(this, R.anim.bottom_down_animation);*/
//        keyboardLayout.setVisibility(RelativeLayout.GONE);
//		/*keyboardLayout.setAnimation(animation);
//		}*/
//    }

    /**
     * Set the custom result data to the layout
     */
    private void setCustomeDataToListLayout() {

        //Collections.sort(customeResult.getCustomeAnsList(), new CustomeAnsSortByQuestionNo());
        //Collections.sort(customeResult.getCustomePlayerAnsList(), new PlayerAnsSortByQuestionNo());
        layout.removeAllViews();
        putViewOnKeyQuestionNumber.clear();
        for (int i = 0; i < customeResult.getCustomeAnsList().size(); i++) {
            View view = null;
            if (customeResult.getCustomeAnsList().get(i).getFillInType() == 1) {
                view = this.getCheckFillInLayout(customeResult.getCustomeAnsList().get(i), i);
                //layout.addView(this.getCheckFillInLayout(customeResult.getCustomeAnsList().get(i) , i));
            } else if (customeResult.getCustomeAnsList().get(i).getAnswerType() == 1) {//for multiple choice
                view = this.getCheckMultiPleChoiceLayout(customeResult.getCustomeAnsList().get(i), i);
                //layout.addView(this.getCheckMultiPleChoiceLayout(customeResult.getCustomeAnsList().get(i) , i));
            } else if (customeResult.getCustomeAnsList().get(i).getAnswerType() == 2) {//for true/false
                view = this.getCheckTrueFalseLayout(customeResult.getCustomeAnsList().get(i), i);
                //layout.addView(this.getCheckTrueFalseLayout(customeResult.getCustomeAnsList().get(i) , i));
            } else if (customeResult.getCustomeAnsList().get(i).getAnswerType() == 3) {//for yes/no
                view = this.getCheckTrueFalseLayout(customeResult.getCustomeAnsList().get(i), i);
                //layout.addView(this.getCheckTrueFalseLayout(customeResult.getCustomeAnsList().get(i) , i));
            }
            if (view != null) {
                layout.addView(view);
                layout.addView(this.getLineView());
                putViewOnKeyQuestionNumber.put(customeResult.getCustomeAnsList().get(i).getQueNo()
                        , view);
            }
        }
    }

    //To add line between layout change

    /**
     * Return the line to add between layouts
     *
     * @return
     */
    private View getLineView() {
        return LayoutInflater.from(this).inflate(R.layout.black_line_view_layout, null);
    }
    //add line change end

    /**
     * return the custome player object by the question id
     *
     * @param queId
     * @return
     */
    private CustomePlayerAns getPlayerAnsByQuestionId(String queId) {
        try {
            if (customeResult.getCustomePlayerAnsList() != null &&
                    customeResult.getCustomePlayerAnsList().size() > 0) {
                ArrayList<CustomePlayerAns> playerAnsList = customeResult
                        .getCustomePlayerAnsList();
                for (int i = 0; i < playerAnsList.size(); i++) {
                    if (queId.equalsIgnoreCase(playerAnsList.get(i).getQueNo())) {
                        return playerAnsList.get(i);
                    }
                }
                return null;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Set the view background according to the teacher credit
     *
     * @param playerAns
     */
    private void setViewBackGroundByTeacherCredit(View view, CustomePlayerAns playerAns) {
        if (customeResult.isAlreadyPlayed()) {
            if (playerAns.getIsAnswerAvailable() == MathFriendzyHelper.NO) {
                if (playerAns.getTeacherCredit().equals("2")) {//for zero credit
                    view.setBackgroundColor(getResources().getColor(R.color.ZERO_CREDIT));
                    zeroCredit++;
                } else if (playerAns.getTeacherCredit().equals("3")) {//half credit
                    halfCredit++;
                    view.setBackgroundColor(getResources().getColor(R.color.HALF_CREDIT));
                } else if (playerAns.getTeacherCredit().equals("4")) {//full credit
                    fullCredit++;
                    view.setBackgroundColor(getResources().getColor(R.color.FULL_CREDIT));
                } else {
                    zeroCredit++;
                }
                return;
            }

            if (playerAns.getAns().length() > 0) {//changes on 8 May 2015
                if (playerAns.getTeacherCredit().equals("1")) {//for no credit
                    if (playerAns.getIsCorrect() == 0) {
                        view.setBackgroundColor(getResources().getColor(R.color.ZERO_CREDIT));
                        zeroCredit++;
                    } else {
                        if (isCalledFortecherCheckHW) {
                            if (playerAns.getFirstTimeWrong() == 1) {
                                view.setBackgroundColor(getResources()
                                        .getColor(R.color.STUDENT_VIEW_ANSWER_COLOR));
                            }
                        }
                        fullCredit++;
                    }
                } else if (playerAns.getTeacherCredit().equals("2")) {//for zero credit
                    view.setBackgroundColor(getResources().getColor(R.color.ZERO_CREDIT));
                    zeroCredit++;
                } else if (playerAns.getTeacherCredit().equals("3")) {//half credit
                    halfCredit++;
                    view.setBackgroundColor(getResources().getColor(R.color.HALF_CREDIT));
                } else if (playerAns.getTeacherCredit().equals("4")) {//full credit
                    fullCredit++;
                    if (isCalledFortecherCheckHW) {
                        if (playerAns.getFirstTimeWrong() == 1) {
                            view.setBackgroundColor(getResources()
                                    .getColor(R.color.STUDENT_VIEW_ANSWER_COLOR));
                        } else {
                            view.setBackgroundColor(getResources().getColor(R.color.FULL_CREDIT));
                        }
                    } else {
                        view.setBackgroundColor(getResources().getColor(R.color.FULL_CREDIT));
                    }
                }
            } else {//changes on 8 May 2015
                zeroCredit++;
                view.setBackgroundColor(getResources().getColor(R.color.FULL_CREDIT));
            }
        }
        /*}else{
            view.setBackgroundColor(getResources().getColor(R.color.FULL_CREDIT));
		}*/
    }

    /**
     * Set the credit and avg score after calculation
     */
    private void setCreditAndAvgScore() {
        txtZeroCredit.setText(zeroCrediteText + " " + creditText + ": " + zeroCredit);
        txtHaltCredit.setText(halFCredittext + " " + creditText + ": " + halfCredit);
        txtFullCredit.setText(fullCreditText + " " + creditText + ": " + fullCredit);

        int avgScore = Math.round((((fullCredit + ((float) halfCredit / 2)) * 100)
                / customeResult.getProblem()));
        customeResult.setScore(avgScore + "");

        txtAvgScore.setText(avgScoreText + ": " + customeResult.getScore() + "%");

        //reinitialize after setting
        zeroCredit = 0;
        halfCredit = 0;
        fullCredit = 0;

        //is_on_calculate = true;

    }

    /**
     * Method call when user click on plus button
     * To the dialog which show the correct answer ,
     * It may open the work area when ans is incorrect
     *
     * @param customeAns
     * @param playerAns
     * @param index
     * @param btnRoughWork
     */
    private void clickOnPlus(final CustomeAns customeAns, final CustomePlayerAns playerAns,
                             final int index, final Button btnRoughWork, final StudentAnsBase student) {

        try {
            /*if(playerAns.getIsCorrect() == 1 || customeResult.getAllowChanges() == 0
                    || isExpireQuizz){//for correct ans and 0 for incorrect ans
                MathFriendzyHelper.warningDialog(this, answerText + ": " + customeAns.getCorrectAns());
            }else{*/
            setPlayerAnsWhenUserClickOnWorkArea(customeAns, playerAns, index);
            clickedStudent = student;
            //for green background for rough button
            clickedPlayerAns = playerAns;
            //student.setHasSeenAns(true);
            //student.setFirstTimeWrong(1);
            Intent intent = new Intent(ActCheckHomeWork.this, ActPlusButton.class);
            intent.putExtra("teacherName", teacherName);
            intent.putExtra("grade", grade);
            intent.putExtra("dueDate", dueDate);
            intent.putExtra("isExpireQuizz", isExpireQuizz);
            intent.putExtra("customeDetail", customeResult);
            intent.putExtra("homeWorkQuizzData", homeWorkQuizzData);
            intent.putExtra("customeAns", customeAns);
            intent.putExtra("index", index);
            intent.putExtra("playerAns", playerAns);
            intent.putExtra("playerMsg", student.getStdmessage());
            intent.putExtra("workareapublic", student.isWorkAreaPublic());
            //for view question image
            intent.putExtra("playerAndQuestionImage", student.getQuestionImage());
            intent.putExtra("cropImageUriFromPdf", cropImageUriFromPdf);
            //intent.putExtra("isGetHelp", student.isGetHelpFromOther());
            intent.putExtra("chatRequestId", student.getChatRequestId());
            intent.putStringArrayListExtra("downloadImageList", downloadImageList);
            startActivityForResult(intent, START_ACT_PLUS_REQUEST_CODE);
            //}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Return the work image name
     *
     * @param customeAns
     * @param indexOfQuestion
     * @return
     */
    private String getImageName(CustomeAns customeAns, int indexOfQuestion) {
        try {
            if (isCalledFortecherCheckHW) {
                return customeResult.getCustomHwId() + "_" +
                        studentDetail.getPlayerId() + "_" + indexOfQuestion + ".png";
            } else {
                return customeResult.getCustomHwId() + "_" +
                        selectedPlayerData.getPlayerid() + "_" + indexOfQuestion + ".png";
            }
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * Question image name
     *
     * @param customeAns
     * @param indexOfQuestion
     * @return
     */
    public String getQuestionImageName(CustomeAns customeAns, int indexOfQuestion) {
        try {
            if (isCalledFortecherCheckHW) {
                return "q_" + customeResult.getCustomHwId() + "_" +
                        studentDetail.getPlayerId() + "_" + indexOfQuestion + ".png";
            } else {
                return "q_" + customeResult.getCustomHwId() + "_" +
                        selectedPlayerData.getPlayerid() + "_" + indexOfQuestion + ".png";
            }
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * Method call when user click on work area
     *
     * @param customeAns
     * @param indexOfQuestion
     */
    private void clickOnWorkArea(CustomeAns customeAns, CustomePlayerAns playerAns,
                                 int indexOfQuestion, final StudentAnsBase student) {

        try {
            setPlayerAnsWhenUserClickOnWorkArea(customeAns, playerAns, indexOfQuestion);
            clickedStudent = student;
            //for teacher check homework
            clickedPlayerAns = playerAns;

            final Intent intent = new Intent(this, HomeWorkWorkArea.class);
            intent.putExtra("playerMsg", student.getStdmessage());
            intent.putExtra("workareapublic", student.isWorkAreaPublic());
            if (isCalledFortecherCheckHW) {
                intent.putExtra("studentDetail", studentDetail);
            }
            intent.putExtra("isCalledFortecherCheckHW", isCalledFortecherCheckHW);

            //for start activity for teacher check homework
            intent.putExtra("customeAns", customeAns);
            intent.putExtra("playerAns", playerAns);
            intent.putExtra("chatRequestId", student.getChatRequestId());

            //changes for update text to server
            intent.putExtra("customeDetail", customeResult);
            intent.putExtra("homeWorkQuizzData", homeWorkQuizzData);

            //for view pdf
            intent.putExtra("playerAndQuestionImage", student.getQuestionImage());
            intent.putExtra("questionImageName", this.getQuestionImageName
                    (customeAns, indexOfQuestion));

            if (cropImageUriFromPdf != null && cropImageUriFromPdf.length() > 0) {
                intent.putExtra("isImageCrop", true);
                intent.putExtra("cropImageUriFromPdf", cropImageUriFromPdf);
                intent.putExtra("questionImageName", this.getQuestionImageName
                        (customeAns, indexOfQuestion));
            }

            if (!customeResult.isAlreadyPlayed()) {
                String fileName = this.getImageName(customeAns, indexOfQuestion);
                //this.addToDownloadImageList(fileName);
                intent.putExtra("fileName", fileName);
                if (MathFriendzyHelper.checkForExistenceOfFile(fileName)) {
                    intent.putExtra("isFileExist", true);
                } else {
                    intent.putExtra("isFileExist", false);
                }
                startWorkAreaAct(intent, student);
                //startActivityForResult(intent , START_WORK_AREA_REQUEST_CODE);
            } else {
                String fileName = playerAns.getWorkImage();
                if (fileName == null || fileName.length() == 0)
                    fileName = this.getImageName(customeAns, indexOfQuestion);
                if (fileName != null && fileName.length() > 0) {
                    this.deleWorkAreaImageFromSDCard(fileName);
                    intent.putExtra("fileName", fileName);
                    if (MathFriendzyHelper.checkForExistenceOfFile(fileName)) {
                        intent.putExtra("isFileExist", true);
                        startWorkAreaAct(intent, student);
                    } else {
                        if (CommonUtils.isInternetConnectionAvailable(this)) {
                            MathFriendzyHelper.downLoadImageFromUrl
                                    (this, ICommonUtils.DOWNLOD_WORK_IMAGE_URL, fileName,
                                            new HttpServerRequest() {
                                                @Override
                                                public void onRequestComplete() {
                                                    intent.putExtra("isFileExist", true);
                                                    startWorkAreaAct(intent, student);
                                                }
                                            });
                        } else {
                            intent.putExtra("isFileExist", false);
                            startWorkAreaAct(intent, student);
                        }
                    }
                } else {
                    fileName = this.getImageName(customeAns, indexOfQuestion);
                    //this.addToDownloadImageList(fileName);
                    if (MathFriendzyHelper.checkForExistenceOfFile(fileName)) {
                        intent.putExtra("isFileExist", true);
                    } else {
                        intent.putExtra("isFileExist", false);
                    }
                    intent.putExtra("fileName", fileName);
                    startWorkAreaAct(intent, student);
                }
                //student.setImageName(fileName);
                changesImageNameList.add(fileName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete the image from sd card
     *
     * @param imageName
     */
    private void deleteQuestionImageFromSDCard(String imageName) {
        try {
            if (CommonUtils.isInternetConnectionAvailable(this)
                    && customeResult.isAlreadyPlayed()) {
                String pngImageName = MathFriendzyHelper.getPNGFormateImageName(imageName);
                if (!downloadImageList.contains(pngImageName)) {
                    //downloadImageList.add(pngImageName);
                    MathFriendzyHelper.deleteImageFromGivenUri
                            (MathFriendzyHelper.getMathFilePath() + "/" +
                                    MathFriendzyHelper.getPNGFormateImageName(imageName));
                } else {
                    //Log.e(TAG, "inside else");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete the work area image name from SD card
     *
     * @param imageName
     */
    private void deleWorkAreaImageFromSDCard(String imageName) {
        try {
            if (CommonUtils.isInternetConnectionAvailable(this)
                    && customeResult.isAlreadyPlayed()) {
                String pngImageName = MathFriendzyHelper.getPNGFormateImageName(imageName);
                if (!downloadImageList.contains(pngImageName)) {
                    MathFriendzyHelper.deleteImageFromGivenUri
                            (MathFriendzyHelper.getMathFilePath() + "/" +
                                    MathFriendzyHelper.getPNGFormateImageName(imageName));
                } else {
                    //Log.e(TAG, "inside else");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Add to the download image list , Name of images which are downloaded
     *
     * @param imageName
     */
    private void addToDownloadImageList(String imageName) {
        if (imageName != null && imageName.length() > 0) {
            String pngImageName = MathFriendzyHelper.getPNGFormateImageName(imageName);
            if (!downloadImageList.contains(pngImageName)) {
                downloadImageList.add(pngImageName);
            }
        }
    }

    /**
     * start the work area activity
     *
     * @param intent
     * @param student
     */
    private void startWorkAreaAct(final Intent intent, StudentAnsBase student) {
        final String questionImageName = student.getQuestionImage();
        if (questionImageName != null &&
                questionImageName.length() > 0) {
            this.deleteQuestionImageFromSDCard(questionImageName);
            if (MathFriendzyHelper.checkForExistenceOfFile(questionImageName)) {
                startActivityForResult(intent, START_WORK_AREA_REQUEST_CODE);
            } else {
                if (CommonUtils.isInternetConnectionAvailable(this)) {
                    MathFriendzyHelper.downLoadImageFromUrl
                            (this, ICommonUtils.DOWNLOAD_QUESTION_IMAGE_URL, questionImageName,
                                    new HttpServerRequest() {
                                        @Override
                                        public void onRequestComplete() {
                                            startActivityForResult(intent, START_WORK_AREA_REQUEST_CODE);
                                        }
                                    });
                } else {
                    startActivityForResult(intent, START_WORK_AREA_REQUEST_CODE);
                }
            }
        } else {
            startActivityForResult(intent, START_WORK_AREA_REQUEST_CODE);
        }
    }

    /**
     * Convert the str teacher credit into int
     *
     * @param strTeacherCredit
     * @return
     */
    private int getIntTeacherCreditValue(String strTeacherCredit) {
        try {
            return Integer.parseInt(strTeacherCredit);
        } catch (Exception e) {
            return 1;
        }
    }

    /**
     * Set the visibility of rough button layout
     *
     * @param button
     */
    private void setVisibilityOfRoughButton(Button button) {
        if (isCalledFortecherCheckHW) {
            if (customeResult.isAlreadyPlayed()) {
                button.setVisibility(Button.VISIBLE);
            } else {
                button.setVisibility(Button.INVISIBLE);
            }
        }
    }

    /**
     * Set rougn button background
     *
     * @param playerAns
     * @param btnButton
     */
    private void setRoughAreaButtonBackGround(CustomePlayerAns playerAns
            , Button btnButton) {
        try {
            if (isCalledFortecherCheckHW) {
                if (MathFriendzyHelper.parseInt(playerAns.getWorkInput())
                        == MathFriendzyHelper.YES) {
                    setRoughButtonBackground(btnButton, true);
                } else {
                    setRoughButtonBackground(btnButton, false);
                }
            } else {
                if (MathFriendzyHelper.parseInt(playerAns.getWorkInput())
                        == MathFriendzyHelper.YES
                        || MathFriendzyHelper.parseInt(playerAns.getOpponentInput())
                        == MathFriendzyHelper.YES) {
                    setRoughButtonBackground(btnButton, true);
                } else {
                    setRoughButtonBackground(btnButton, false);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set rougn button background , After the UI change when user
     * back from work area or from the + button
     *
     * @param clickedStudent
     * @param btnButton
     */
    private void setRoughAreaButtonBackGround(StudentAnsBase clickedStudent
            , Button btnButton) {
        try {
            if (isCalledFortecherCheckHW) {
                if (MathFriendzyHelper.parseInt(clickedStudent.getWorkInput())
                        == MathFriendzyHelper.YES) {
                    setRoughButtonBackground(btnButton, true);
                } else {
                    setRoughButtonBackground(btnButton, false);
                }
            } else {
                if (MathFriendzyHelper.parseInt(clickedStudent.getWorkInput())
                        == MathFriendzyHelper.YES
                        || MathFriendzyHelper.parseInt(clickedStudent.getOpponentInput())
                        == MathFriendzyHelper.YES) {
                    setRoughButtonBackground(btnButton, true);
                } else {
                    setRoughButtonBackground(btnButton, false);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set rough button background
     *
     * @param btnButton
     * @param isGreenButtonBackground
     */
    private void setRoughButtonBackground(Button btnButton, boolean isGreenButtonBackground) {
        if (btnButton != null) {
            if (isGreenButtonBackground) {
                btnButton.setBackgroundResource(R.drawable.small_pencil_with_green_background);
            } else {
                btnButton.setBackgroundResource(R.drawable.small_pencil);
            }
        }
    }

    /**
     * Inflate the layout for the fill in type
     *
     * @param customeAns
     * @return
     */
    @SuppressLint("InflateParams")
    private View getCheckFillInLayout(final CustomeAns customeAns, final int index) {
        final CustomePlayerAns playerAns = this.getPlayerAnsByQuestionId(customeAns.getQueNo());

        View view = LayoutInflater.from(this).inflate(R.layout.kb_check_home_fill_in_blank_layout, null);
        RelativeLayout viewLayout = (RelativeLayout) view.findViewById(R.id.checkHomeLayout);

        /*viewLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });*/


        Button btnSeeAnswer = (Button) view.findViewById(R.id.btnSeeAnswer);
        TextView txtNumber = (AutoResizeEditText) view.findViewById(R.id.txtNumber);
        TextView txtLeftUnit = (TextView) view.findViewById(R.id.txtLeftUnit);
        // TextView ansBox = (TextView) view.findViewById(R.id.ansBox);  //siddhiinfosoft
        TextView txtRighttUnit = (TextView) view.findViewById(R.id.txtRighttUnit);
        final Button btnRoughWork = (Button) view.findViewById(R.id.btnRoughWork);
        final LinearLayout ll_ans_box = (LinearLayout) view.findViewById(R.id.ll_ans_box); //siddhiinfosoft

        txtNumber.setFocusable(false);
        txtNumber.setFocusableInTouchMode(false);

        /*ll_ans_box.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                ActCheckHomeWork.this.showKeyboard();
                return false;
            }
        });*/


        RelativeLayout rlCorrectInCorrectLayout = (RelativeLayout) view.findViewById(R.id.rlCorrectInCorrectLayout);
        TextView txtCorrectAnsCounter = (TextView) view.findViewById(R.id.txtCorrectAnsCounter);
        TextView txtWrongAnsCounter = (TextView) view.findViewById(R.id.txtWrongAnsCounter);

        //Add Question
        TextView edtQuestion = (TextView) view.findViewById(R.id.edtQuestion);
        LinearLayout ll_que_box = (LinearLayout) view.findViewById(R.id.ll_que_box);
        RelativeLayout rlQuestionLayout = (RelativeLayout) view.findViewById(R.id.rlQuestionLayout);

        /*rlQuestionLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });*/

        this.setQuestion(customeAns, rlQuestionLayout, ll_que_box);

        //for Teacher Check Homework
        this.setVisibilityOfRoughButton(btnRoughWork);
        this.setRoughAreaButtonBackGround(playerAns, btnRoughWork);

        txtNumber.setText(customeAns.getQueNo());
        /*txtNumber.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                ActCheckHomeWork.this.hideKeyboard();

                return false;
            }
        });*/

        if (customeAns.getIsLeftUnit() == 0) {
            txtLeftUnit.setText("");
            txtRighttUnit.setText(customeAns.getAnsSuffix());
        } else {
//            txtLeftUnit.setText(customeAns.getAnsSuffix());
//            txtRighttUnit.setText("");
            txtLeftUnit.setText("");
            txtRighttUnit.setText(customeAns.getAnsSuffix());
        }

        final UserFillAnswer fillInAns = new UserFillAnswer();
        fillInAns.setQueId(customeAns.getQueNo());
        fillInAns.setCustomeAns(customeAns);
        // fillInAns.setTxtView(ansBox);
        fillInAns.setTxtView(ll_ans_box);
        fillInAns.setFirstTimeWrong(0);
        fillInAns.setIsAnswerAvailable(customeAns.getIsAnswerAvailable());
        if (playerAns != null && customeResult.isAlreadyPlayed()) {
            fillInAns.setImageName(playerAns.getWorkImage());
            fillInAns.setQuestionImage(playerAns.getQuestionImage());
            fillInAns.setStdmessage(playerAns.getMessage());
            fillInAns.setWorkAreaPublic(playerAns.getIsWorkAreaPublic());
            fillInAns.setGetHelpFromOther(playerAns.getIsGetHelp());
            fillInAns.setTeacherCredit(this.getIntTeacherCreditValue(playerAns.getTeacherCredit()));
            fillInAns.setPrevAnsCorrect(playerAns.getIsCorrect());
            if (playerAns.getFirstTimeWrong() == 1) {
                fillInAns.setFirstTimeWrong(playerAns.getFirstTimeWrong());
                //btnRoughWork.setBackgroundResource(R.drawable.red_small_pencil);
            }
            fillInAns.setChatRequestId(playerAns.getChatRequestedId());
            fillInAns.setWorkInput(playerAns.getWorkInput());
            fillInAns.setOpponentInput(playerAns.getOpponentInput());
            fillInAns.setFixedWorkImage(playerAns.getFixedWorkImage());
            fillInAns.setFixedMessage(playerAns.getFixedMessage());
            fillInAns.setFixedQuestionImage(playerAns.getFixedQuestionImage());
            this.setUrlLinks(fillInAns, playerAns);
        } else {
            fillInAns.setImageName(this.getImageName(customeAns, index));
            fillInAns.setQuestionImage("");
            fillInAns.setStdmessage("");
            fillInAns.setWorkAreaPublic(1);
            fillInAns.setGetHelpFromOther(0);
            fillInAns.setTeacherCredit(1);
            fillInAns.setChatRequestId(0);
            fillInAns.setWorkInput("0");
            fillInAns.setOpponentInput("0");
            fillInAns.setFixedWorkImage("");
            fillInAns.setFixedMessage("");
            fillInAns.setFixedQuestionImage("");
        }

        ll_ans_box.setMinimumWidth(dpwidth - 170); // siddhiinfosoft
        ll_ans_box.setTag(R.id.first, "1");
        ll_ans_box.setTag(R.id.second, "main");
        this.setstudentansbox(customeAns, ll_ans_box, playerAns);  // siddhiinfosoft


        if (playerAns != null) {
            //  ansBox.setText(playerAns.getAns());
            playerAns.setIsAnswerAvailable(customeAns.getIsAnswerAvailable());
            this.setViewBackGroundByTeacherCredit(viewLayout, playerAns);
            if (customeResult.getShowAns() == 1
                    && customeResult.isAlreadyPlayed()) {
                if (!isCalledFortecherCheckHW)//For check teacher homework
                    btnSeeAnswer.setVisibility(Button.VISIBLE);
            }
            fillInAns.setStdmessage(playerAns.getMessage());
            fillInAns.setWorkAreaPublic(playerAns.getIsWorkAreaPublic());
            fillInAns.setGetHelpFromOther(playerAns.getIsGetHelp());
            fillInAns.setTeacherCredit(this.getIntTeacherCreditValue(playerAns.getTeacherCredit()));
            fillInAns.setChatRequestId(playerAns.getChatRequestedId());
            fillInAns.setWorkInput(playerAns.getWorkInput());
            fillInAns.setOpponentInput(playerAns.getOpponentInput());
            fillInAns.setFixedWorkImage(playerAns.getFixedWorkImage());
            fillInAns.setFixedMessage(playerAns.getFixedMessage());
            fillInAns.setFixedQuestionImage(playerAns.getFixedQuestionImage());
            this.setUrlLinks(fillInAns, playerAns);
        }

        userFillInAnsList.add(fillInAns);

        if (isAllowChanges && this.isPlayerAnswerEditable(playerAns)) {
//            ansBox.setOnClickListener(new OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if(customeAns.getIsAnswerAvailable() == MathFriendzyHelper.NO){
//                        MathFriendzyHelper.showWarningDialog(ActCheckHomeWork.this ,
//                                lblAnswerOnWorkAreaCreditGiven);
//                        return ;
//                    }
//                    showKeyboard();
//                    for(int i = 0 ; i < userFillInAnsList.size() ; i ++ ){
//                        userFillInAnsList.get(i).getTxtView().setText(
//                                userFillInAnsList.get(i).getTxtView()
//                                        .getText().toString().replace("|", ""));
//                        if(v == userFillInAnsList.get(i).getTxtView()){
//                            selectedTextView = userFillInAnsList.get(i).getTxtView();
//                            setTextToSelectedTextView(userFillInAnsList.get(i).getTxtView(), "");
//                        }
//                    }
//                }
//            });
        } else {
            ll_ans_box.setOnClickListener(new OnClickListener() { // add by siddhi info soft
                @Override
                public void onClick(View v) {
                    if (isQuizExpireForStudent()) {
                        showCantEditAnswerDialog();
                        return;
                    } else if (!isPlayerAnswerEditable(playerAns)) {
                        showNoLongerChangeThisQuestion();
                        return;
                    }
                }
            });
//            ansBox.setOnClickListener(new OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if(isQuizExpireForStudent()){
//                        showCantEditAnswerDialog();
//                        return ;
//                    }
//                }
//            });
        }

        btnSeeAnswer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    clickedbtnRoughWork = btnRoughWork;
                    clickOnPlus(customeAns, playerAns, index, btnRoughWork, fillInAns);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        btnRoughWork.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    clickedbtnRoughWork = btnRoughWork;
                    //clickOnWorkArea(customeAns , playerAns , index , fillInAns);
                    getQuestionDetailForTeacher(customeAns, playerAns, index, fillInAns);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        ll_ans_box.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (customeAns.getIsAnswerAvailable() == MathFriendzyHelper.NO) {
                    MathFriendzyHelper.showWarningDialog(ActCheckHomeWork.this,
                            lblAnswerOnWorkAreaCreditGiven);
                } else {
                    if (isQuizExpireForStudent()) {
                        showCantEditAnswerDialog();
                        return;
                    } else if (!isPlayerAnswerEditable(playerAns)) {
                        showNoLongerChangeThisQuestion();
                        return;
                    } else {
                        callclickmethod(customeAns, (LinearLayout) v, index);
                    }
                }
            }
        });

        if (isCalledFortecherCheckHW) {//for showing the correct and wrong anser counter by the students
            rlCorrectInCorrectLayout.setVisibility(RelativeLayout.VISIBLE);
            txtCorrectAnsCounter.setText(customeAns.getCorrectAnsCounter() + "");
            txtWrongAnsCounter.setText(customeAns.getWrongAnsCounter() + "");
        }

//        if(customeAns.getIsAnswerAvailable() == MathFriendzyHelper.NO){
//            ansBox.setText(lblAnswerOnWorkAreaCreditGiven);
//        }
        return view;
    }

    private boolean isQuizExpireForStudent() {
        if (isCalledFortecherCheckHW) {
            return false;
        } else {
            if (isExpireQuizz)
                return true;
        }
        return false;
    }

    private void showCantEditAnswerDialog() {
        MathFriendzyHelper.showWarningDialog(this, alertYouCantChangeYourAnswer);
    }

    private void showNoLongerChangeThisQuestion() {
        if (!isCalledFortecherCheckHW) {
            MathFriendzyHelper.showWarningDialog(this,
                    MathFriendzyHelper.getTreanslationTextById(this, "lblCantChangeAns"));
        }
    }

    private void getQuestionDetailForTeacher(final CustomeAns customeAns, final CustomePlayerAns playerAns,
                                             final int indexOfQuestion, final StudentAnsBase student) {
        if (isCalledFortecherCheckHW) {//for teacher for check homework
            GetQuestionDetailsForTeacherParam param = new GetQuestionDetailsForTeacherParam();
            param.setAction("getQuestionDetailsForTeacher");
            param.setHwId(studentDetail.getHomeWorkId());
            param.setCustomHWId(customeResult.getCustomHwId() + "");
            param.setPlayerId(studentDetail.getPlayerId());
            param.setUserId(studentDetail.getParentId());
            param.setQuesNo(customeAns.getQueNo());

            if (CommonUtils.isInternetConnectionAvailable(this)) {
                new MyAsyckTask(ServerOperation.createPostRequestForGetQuestionDetailsForTeacher(param)
                        , null, ServerOperationUtil.GET_QUESTION_DETAIL_FOR_TEACHER_REQUEST, this,
                        new HttpResponseInterface() {
                            @Override
                            public void serverResponse(HttpResponseBase httpResponseBase,
                                                       int requestCode) {
                                GetQuestionDetailForTeacherResponse response =
                                        (GetQuestionDetailForTeacherResponse) httpResponseBase;
                                if (response.getResult().equals(MathFriendzyHelper.SUCCESS)) {
                                    CustomePlayerAns playerAnsFromServer = response.getPlayerAns();
                                    playerAns.setQueNo(playerAnsFromServer.getQueNo());
                                    playerAns.setAns(playerAnsFromServer.getAns());
                                    playerAns.setIsCorrect(playerAnsFromServer.getIsCorrect());
                                    playerAns.setWorkImage(playerAnsFromServer.getWorkImage());
                                    playerAns.setFirstTimeWrong(playerAnsFromServer.getFirstTimeWrong());
                                    playerAns.setTeacherCredit(playerAnsFromServer.getTeacherCredit());
                                    playerAns.setMessage(playerAnsFromServer.getMessage());
                                    playerAns.setIsWorkAreaPublic(playerAnsFromServer.getIsWorkAreaPublic());
                                    playerAns.setIsGetHelp(playerAnsFromServer.getIsGetHelp());
                                    playerAns.setQuestionImage(playerAnsFromServer.getQuestionImage());
                                    playerAns.setChatRequestedId(playerAnsFromServer.getChatRequestedId());
                                    playerAns.setFixedWorkImage(playerAnsFromServer.getFixedWorkImage());
                                    playerAns.setFixedMessage(playerAnsFromServer.getFixedMessage());
                                    playerAns.setFixedQuestionImage(playerAnsFromServer.getFixedQuestionImage());
                                    playerAns.setUrlLinks(playerAnsFromServer.getUrlLinks());
                                    clickOnWorkArea(customeAns, playerAns, indexOfQuestion, student);
                                }
                            }
                        }, ServerOperationUtil.SIMPLE_DIALOG, true,
                        getString(R.string.please_wait_dialog_msg))
                        .execute();
            } else {
                clickOnWorkArea(customeAns, playerAns, indexOfQuestion, student);
            }
        } else {
            clickOnWorkArea(customeAns, playerAns, indexOfQuestion, student);
        }
    }

    /**
     * Inflate the layout for not fill in type but for multiple choice answer type = 1
     *
     * @param customeAns
     * @return
     */
    @SuppressLint("InflateParams")
    private View getCheckMultiPleChoiceLayout(final CustomeAns customeAns, final int index) {

        final CustomePlayerAns playerAns = this.getPlayerAnsByQuestionId(customeAns.getQueNo());

        View view = LayoutInflater.from(this).inflate(R.layout.check_home_multiple_choice_layout, null);
        RelativeLayout viewLayout = (RelativeLayout) view.findViewById(R.id.checkHomeLayout);
        Button btnSeeAnswer = (Button) view.findViewById(R.id.btnSeeAnswer);
        TextView txtNumber = (TextView) view.findViewById(R.id.txtNumber);
        final Button btnRoughWork = (Button) view.findViewById(R.id.btnRoughWork);
        LinearLayout optionLayout = (LinearLayout) view.findViewById(R.id.optionLayout);
        RelativeLayout rlCorrectInCorrectLayout = (RelativeLayout) view.findViewById(R.id.rlCorrectInCorrectLayout);
        TextView txtCorrectAnsCounter = (TextView) view.findViewById(R.id.txtCorrectAnsCounter);
        TextView txtWrongAnsCounter = (TextView) view.findViewById(R.id.txtWrongAnsCounter);

        //Add Question
        TextView edtQuestion = (TextView) view.findViewById(R.id.edtQuestion);
        LinearLayout ll_que_box = (LinearLayout) view.findViewById(R.id.ll_que_box);

       /* ll_que_box.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });*/

        RelativeLayout rlQuestionLayout = (RelativeLayout) view.findViewById(R.id.rlQuestionLayout);
        this.setQuestion(customeAns, rlQuestionLayout, ll_que_box);

        //for Teacher Check Homework
        this.setVisibilityOfRoughButton(btnRoughWork);
        this.setRoughAreaButtonBackGround(playerAns, btnRoughWork);

        final MultipleChoiceAnswer multipleChiceAns = new MultipleChoiceAnswer();
        multipleChiceAns.setQueId(customeAns.getQueNo());
        multipleChiceAns.setCustomeAns(customeAns);
        multipleChiceAns.setFirstTimeWrong(0);

        ArrayList<TextView> optionList = new ArrayList<TextView>();
        multipleChiceAns.setOptionList(optionList);

        if (playerAns != null && customeResult.isAlreadyPlayed()) {
            multipleChiceAns.setStdmessage(playerAns.getMessage());
            multipleChiceAns.setQuestionImage(playerAns.getQuestionImage());
            multipleChiceAns.setImageName(playerAns.getWorkImage());
            multipleChiceAns.setWorkAreaPublic(playerAns.getIsWorkAreaPublic());
            multipleChiceAns.setGetHelpFromOther(playerAns.getIsGetHelp());
            multipleChiceAns.setTeacherCredit(this.getIntTeacherCreditValue(playerAns.getTeacherCredit()));
            multipleChiceAns.setPrevAnsCorrect(playerAns.getIsCorrect());
            multipleChiceAns.setChatRequestId(playerAns.getChatRequestedId());
            multipleChiceAns.setWorkInput(playerAns.getWorkInput());
            multipleChiceAns.setOpponentInput(playerAns.getOpponentInput());
            multipleChiceAns.setFixedWorkImage(playerAns.getFixedWorkImage());
            multipleChiceAns.setFixedMessage(playerAns.getFixedMessage());
            multipleChiceAns.setFixedQuestionImage(playerAns.getFixedQuestionImage());
            this.setUrlLinks(multipleChiceAns, playerAns);
        } else {
            multipleChiceAns.setStdmessage("");
            multipleChiceAns.setImageName(this.getImageName(customeAns, index));
            multipleChiceAns.setQuestionImage("");
            multipleChiceAns.setWorkAreaPublic(1);
            multipleChiceAns.setGetHelpFromOther(0);
            multipleChiceAns.setTeacherCredit(1);
            multipleChiceAns.setChatRequestId(0);
            multipleChiceAns.setWorkInput("0");
            multipleChiceAns.setOpponentInput("0");
            multipleChiceAns.setFixedWorkImage("");
            multipleChiceAns.setFixedMessage("");
            multipleChiceAns.setFixedQuestionImage("");
        }
        multipleChoiceAnsList.add(multipleChiceAns);

        if (playerAns != null) {
            this.setViewBackGroundByTeacherCredit(viewLayout, playerAns);
            if (customeResult.getShowAns() == 1 &&
                    customeResult.isAlreadyPlayed()) {
                if (!isCalledFortecherCheckHW)//For check teacher homework
                    btnSeeAnswer.setVisibility(Button.VISIBLE);
            }

            if (playerAns.getFirstTimeWrong() == 1) {
                multipleChiceAns.setFirstTimeWrong(playerAns.getFirstTimeWrong());
                //btnRoughWork.setBackgroundResource(R.drawable.red_small_pencil);
            }

            multipleChiceAns.setStdmessage(playerAns.getMessage());
            multipleChiceAns.setWorkAreaPublic(playerAns.getIsWorkAreaPublic());
            multipleChiceAns.setGetHelpFromOther(playerAns.getIsGetHelp());
            multipleChiceAns.setTeacherCredit(this.getIntTeacherCreditValue(playerAns.getTeacherCredit()));
            multipleChiceAns.setChatRequestId(playerAns.getChatRequestedId());
            multipleChiceAns.setWorkInput(playerAns.getWorkInput());
            multipleChiceAns.setOpponentInput(playerAns.getOpponentInput());
            multipleChiceAns.setFixedWorkImage(playerAns.getFixedWorkImage());
            multipleChiceAns.setFixedMessage(playerAns.getFixedMessage());
            multipleChiceAns.setFixedQuestionImage(playerAns.getFixedQuestionImage());
            this.setUrlLinks(multipleChiceAns, playerAns);
        }


        String[] commaSepratedOption = MathFriendzyHelper.
                getCommaSepratedOption(customeAns.getOptions(), ",");

        for (int i = 0; i < commaSepratedOption.length; i++) {
            optionLayout.addView(this.getMultipleOptionLayout
                    (commaSepratedOption[i], customeAns, optionList,
                            multipleChiceAns, playerAns));
        }
        txtNumber.setText(customeAns.getQueNo());

        btnSeeAnswer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    clickedbtnRoughWork = btnRoughWork;
                    clickOnPlus(customeAns, playerAns, index, btnRoughWork, multipleChiceAns);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        btnRoughWork.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    clickedbtnRoughWork = btnRoughWork;
                    //clickOnWorkArea(customeAns , playerAns , index , multipleChiceAns);
                    getQuestionDetailForTeacher(customeAns, playerAns, index, multipleChiceAns);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (isCalledFortecherCheckHW) {//for showing the correct and wrong anser counter by the students
            rlCorrectInCorrectLayout.setVisibility(RelativeLayout.VISIBLE);
            txtCorrectAnsCounter.setText(customeAns.getCorrectAnsCounter() + "");
            txtWrongAnsCounter.setText(customeAns.getWrongAnsCounter() + "");
        }

        return view;
    }

	/*private void setIsNeedToSaveOnServer(boolean bValues){
        isNeedToUpdateOnServer = bValues;
	}*/

    /**
     * Inflate the layout for not fill in type but for multiple choice answer type = 2 or 3
     * for true false or yes no
     *
     * @param customeAns
     * @return
     */
    @SuppressLint("InflateParams")
    private View getCheckTrueFalseLayout(final CustomeAns customeAns, final int index) {

        final CustomePlayerAns playerAns = this.getPlayerAnsByQuestionId(customeAns.getQueNo());

        View view = LayoutInflater.from(this).inflate(R.layout.check_home_true_false_layout, null);
        RelativeLayout viewLayout = (RelativeLayout) view.findViewById(R.id.checkHomeLayout);
        Button btnSeeAnswer = (Button) view.findViewById(R.id.btnSeeAnswer);
        TextView txtNumber = (TextView) view.findViewById(R.id.txtNumber);
        final TextView ansTrue = (TextView) view.findViewById(R.id.ansTrue);
        final TextView ansFalse = (TextView) view.findViewById(R.id.ansFalse);
        final Button btnRoughWork = (Button) view.findViewById(R.id.btnRoughWork);
        RelativeLayout rlCorrectInCorrectLayout = (RelativeLayout) view.findViewById(R.id.rlCorrectInCorrectLayout);
        TextView txtCorrectAnsCounter = (TextView) view.findViewById(R.id.txtCorrectAnsCounter);
        TextView txtWrongAnsCounter = (TextView) view.findViewById(R.id.txtWrongAnsCounter);

        txtNumber.setText(customeAns.getQueNo());
        String[] ansArray = MathFriendzyHelper.getCommaSepratedOption(customeAns.getOptions(), ",");
        ansTrue.setText(ansArray[0]);
        ansFalse.setText(ansArray[1]);

        //Add Question
        TextView edtQuestion = (TextView) view.findViewById(R.id.edtQuestion);
        LinearLayout ll_que_box = (LinearLayout) view.findViewById(R.id.ll_que_box);
        RelativeLayout rlQuestionLayout = (RelativeLayout) view.findViewById(R.id.rlQuestionLayout);
        this.setQuestion(customeAns, rlQuestionLayout, ll_que_box);

        //for Teacher Check Homework
        this.setVisibilityOfRoughButton(btnRoughWork);
        this.setRoughAreaButtonBackGround(playerAns, btnRoughWork);

        final CheckUserAnswer checkUserAns = new CheckUserAnswer();
        checkUserAns.setQueId(customeAns.getQueNo());
        checkUserAns.setCustomeAns(customeAns);
        checkUserAns.setTxtYes(ansTrue);
        checkUserAns.setTxtNo(ansFalse);
        checkUserAns.setFirstTimeWrong(0);

        if (playerAns != null && customeResult.isAlreadyPlayed()) {
            checkUserAns.setImageName(playerAns.getWorkImage());
            checkUserAns.setQuestionImage(playerAns.getQuestionImage());
            checkUserAns.setStdmessage(playerAns.getMessage());
            checkUserAns.setWorkAreaPublic(playerAns.getIsWorkAreaPublic());
            checkUserAns.setGetHelpFromOther(playerAns.getIsGetHelp());
            checkUserAns.setTeacherCredit(this.getIntTeacherCreditValue(playerAns.getTeacherCredit()));
            checkUserAns.setPrevAnsCorrect(playerAns.getIsCorrect());
            checkUserAns.setChatRequestId(playerAns.getChatRequestedId());
            checkUserAns.setWorkInput(playerAns.getWorkInput());
            checkUserAns.setOpponentInput(playerAns.getOpponentInput());
            checkUserAns.setFixedWorkImage(playerAns.getFixedWorkImage());
            checkUserAns.setFixedMessage(playerAns.getFixedMessage());
            checkUserAns.setFixedQuestionImage(playerAns.getFixedQuestionImage());
            this.setUrlLinks(checkUserAns, playerAns);
        } else {
            checkUserAns.setImageName(this.getImageName(customeAns, index));
            checkUserAns.setStdmessage("");
            checkUserAns.setQuestionImage("");
            checkUserAns.setWorkAreaPublic(1);
            checkUserAns.setGetHelpFromOther(0);
            checkUserAns.setTeacherCredit(1);
            checkUserAns.setChatRequestId(0);
            checkUserAns.setWorkInput("0");
            checkUserAns.setOpponentInput("0");
            checkUserAns.setFixedWorkImage("");
            checkUserAns.setFixedMessage("");
            checkUserAns.setFixedQuestionImage("");
        }

        if (playerAns != null) {
            if (playerAns.getAns().equals("Yes")) {
                checkUserAns.setUserAns(checkUserAns.getTxtYes().getText().toString());
                checkUserAns.getTxtYes().setBackgroundResource(R.drawable.checked_box_home_work);
                checkUserAns.getTxtNo().setBackgroundResource(R.drawable.unchecked_box_home_work);
            } else if (playerAns.getAns().equals("No")) {
                checkUserAns.setUserAns(checkUserAns.getTxtNo().getText().toString());
                checkUserAns.getTxtYes().setBackgroundResource(R.drawable.unchecked_box_home_work);
                checkUserAns.getTxtNo().setBackgroundResource(R.drawable.checked_box_home_work);
            } else if (playerAns.getAns().equals("True")) {
                checkUserAns.setUserAns(checkUserAns.getTxtYes().getText().toString());
                checkUserAns.getTxtYes().setBackgroundResource(R.drawable.checked_box_home_work);
                checkUserAns.getTxtNo().setBackgroundResource(R.drawable.unchecked_box_home_work);
            } else if (playerAns.getAns().equals("False")) {
                checkUserAns.setUserAns(checkUserAns.getTxtNo().getText().toString());
                checkUserAns.getTxtYes().setBackgroundResource(R.drawable.unchecked_box_home_work);
                checkUserAns.getTxtNo().setBackgroundResource(R.drawable.checked_box_home_work);
            }
            this.setViewBackGroundByTeacherCredit(viewLayout, playerAns);

            if (customeResult.getShowAns() == 1
                    && customeResult.isAlreadyPlayed()) {
                if (!isCalledFortecherCheckHW)//For check teacher homework
                    btnSeeAnswer.setVisibility(Button.VISIBLE);
            }

            if (playerAns.getFirstTimeWrong() == 1) {
                checkUserAns.setFirstTimeWrong(playerAns.getFirstTimeWrong());
                //btnRoughWork.setBackgroundResource(R.drawable.red_small_pencil);
            }

            checkUserAns.setStdmessage(playerAns.getMessage());
            checkUserAns.setWorkAreaPublic(playerAns.getIsWorkAreaPublic());
            checkUserAns.setGetHelpFromOther(playerAns.getIsGetHelp());
            checkUserAns.setTeacherCredit(this.getIntTeacherCreditValue(playerAns.getTeacherCredit()));
            checkUserAns.setChatRequestId(playerAns.getChatRequestedId());
            checkUserAns.setWorkInput(playerAns.getWorkInput());
            checkUserAns.setOpponentInput(playerAns.getOpponentInput());
            checkUserAns.setFixedWorkImage(playerAns.getFixedWorkImage());
            checkUserAns.setFixedMessage(playerAns.getFixedMessage());
            checkUserAns.setFixedQuestionImage(playerAns.getFixedQuestionImage());
            this.setUrlLinks(checkUserAns, playerAns);
        }

        checkUserAnswer.add(checkUserAns);

        if (isAllowChanges && this.isPlayerAnswerEditable(playerAns)) {
            ansTrue.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int i = 0; i < checkUserAnswer.size(); i++) {
                        if (v == checkUserAnswer.get(i).getTxtYes()) {
                            checkUserAnswer.get(i).setUserAns(checkUserAnswer.get(i).getTxtYes().getText().toString());
                            checkUserAnswer.get(i).getTxtYes().setBackgroundResource(R.drawable.checked_box_home_work);
                            checkUserAnswer.get(i).getTxtNo().setBackgroundResource(R.drawable.unchecked_box_home_work);
                        }
                    }
                }
            });

            ansFalse.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int i = 0; i < checkUserAnswer.size(); i++) {
                        if (v == checkUserAnswer.get(i).getTxtNo()) {
                            checkUserAnswer.get(i).setUserAns(checkUserAnswer.get(i).getTxtNo().getText().toString());
                            checkUserAnswer.get(i).getTxtNo().setBackgroundResource(R.drawable.checked_box_home_work);
                            checkUserAnswer.get(i).getTxtYes().setBackgroundResource(R.drawable.unchecked_box_home_work);
                        }
                    }
                }
            });
        } else {
            ansTrue.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isQuizExpireForStudent()) {
                        showCantEditAnswerDialog();
                        return;
                    } else if (!isPlayerAnswerEditable(playerAns)) {
                        showNoLongerChangeThisQuestion();
                        return;
                    }
                }
            });

            ansFalse.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isQuizExpireForStudent()) {
                        showCantEditAnswerDialog();
                        return;
                    } else if (!isPlayerAnswerEditable(playerAns)) {
                        showNoLongerChangeThisQuestion();
                        return;
                    }
                }
            });
        }


        btnSeeAnswer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    clickedbtnRoughWork = btnRoughWork;
                    clickOnPlus(customeAns, playerAns, index, btnRoughWork, checkUserAns);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        btnRoughWork.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    clickedbtnRoughWork = btnRoughWork;
                    //clickOnWorkArea(customeAns , playerAns , index , checkUserAns);
                    getQuestionDetailForTeacher(customeAns, playerAns, index, checkUserAns);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (isCalledFortecherCheckHW) {//for showing the correct and wrong anser counter by the students
            rlCorrectInCorrectLayout.setVisibility(RelativeLayout.VISIBLE);
            txtCorrectAnsCounter.setText(customeAns.getCorrectAnsCounter() + "");
            txtWrongAnsCounter.setText(customeAns.getWrongAnsCounter() + "");
        }
        return view;
    }


    /**
     * Return the inflated view to multiple choice option
     *
     * @param optionText
     * @param customeAns
     * @param optionList
     * @param multipleChiceAns
     * @param playerAns
     * @return
     */
    @SuppressLint("InflateParams")
    private View getMultipleOptionLayout(String optionText,
                                         final CustomeAns customeAns, ArrayList<TextView> optionList,
                                         MultipleChoiceAnswer multipleChiceAns, final CustomePlayerAns playerAns) {
        View view = LayoutInflater.from(this).inflate(R.layout.check_home_work_option_layout, null);
        final TextView option = (TextView) view.findViewById(R.id.option);
        option.setText(optionText);
        optionList.add(option);

        if (playerAns != null) {
            if (playerAns.getAns().contains(optionText)) {
                if (multipleChiceAns.getUserAns() != null) {
                    multipleChiceAns.setUserAns(multipleChiceAns.getUserAns() + option.getText().toString());
                } else {
                    multipleChiceAns.setUserAns(option.getText().toString());
                }
                option.setBackgroundResource(R.drawable.checked_small_box_home_work);
            }
        }

        if (isAllowChanges && this.isPlayerAnswerEditable(playerAns)) {
            option.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int i = 0; i < multipleChoiceAnsList.size(); i++) {
                        for (int j = 0; j < multipleChoiceAnsList.get(i).getOptionList().size(); j++) {
                            if (v == multipleChoiceAnsList.get(i).getOptionList().get(j)) {
                                if (multipleChoiceAnsList.get(i).getUserAns() != null) {
                                    String strBuilder = multipleChoiceAnsList.get(i).getUserAns();

                                    if (multipleChoiceAnsList.get(i).getUserAns().contains
                                            (multipleChoiceAnsList.get(i)
                                                    .getOptionList().get(j).getText().toString())) {
                                        multipleChoiceAnsList.get(i).getOptionList().get(j)
                                                .setBackgroundResource
                                                        (R.drawable.unchecked_small_box_home_work);
                                        strBuilder = strBuilder.replace(multipleChoiceAnsList.get(i)
                                                .getOptionList().get(j).getText().toString(), "");
                                    } else {
                                        multipleChoiceAnsList.get(i).getOptionList().get(j)
                                                .setBackgroundResource
                                                        (R.drawable.checked_small_box_home_work);
                                        strBuilder = strBuilder + multipleChoiceAnsList.get(i)
                                                .getOptionList().get(j).getText().toString();

                                    }
                                    multipleChoiceAnsList.get(i).setUserAns(strBuilder);
                                } else {
                                    multipleChoiceAnsList.get(i).getOptionList().get(j)
                                            .setBackgroundResource
                                                    (R.drawable.checked_small_box_home_work);
                                    multipleChoiceAnsList.get(i).setUserAns(multipleChoiceAnsList.get(i)
                                            .getOptionList().get(j).getText().toString());
                                }
                            }
                        }
                    }
                }
            });
        } else {
            option.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isQuizExpireForStudent()) {
                        showCantEditAnswerDialog();
                        return;
                    } else if (!isPlayerAnswerEditable(playerAns)) {
                        showNoLongerChangeThisQuestion();
                        return;
                    }
                }
            });
        }

        return view;
    }

    /**
     * Set the text to the selected text view
     *
     * @param txtView
     * @param text
     */
    private void setTextToSelectedTextView(TextView txtView, String text) {
        txtView.setText(txtView.getText().toString().replace("|", "") + text + "|");
    }


    /**
     * Clear text
     *
     * @param txtView
     */
    private void cleartextFromSelectedTextView(TextView txtView) {
        StringBuilder text = new StringBuilder(txtView.getText().toString().replace("|", ""));
        if (text.length() > 0) {
            text.deleteCharAt(text.length() - 1);
            txtView.setText(text + "|");
        }
    }

    /**
     * Save Fill In answer at the time of calculate
     */
    private void saveFillInAns() {
        for (int i = 0; i < userFillInAnsList.size(); i++) {
            if (userFillInAnsList.get(i).getIsAnswerAvailable() == MathFriendzyHelper.NO) {
                userFillInAnsList.get(i)
                        .setUserAns("");

            } else {
//                userFillInAnsList.get(i)
//                        .setUserAns(userFillInAnsList.get(i)
//                                .getTxtView().getText().toString().replace("|", ""));
                try {
                    userFillInAnsList.get(i)                                     //added by siddhiinfosoft
                            .setUserAns(makeansfromview(userFillInAnsList.get(i)
                                    .getTxtView()));
                } catch (Exception e) {

                }
            }
        }
    }

    /**
     * Convert user ans into json string
     *
     * @param finalAnsList
     * @return
     */
    private String getJosnString(ArrayList<StudentAnsBase> finalAnsList) {
        StringBuilder strBuilder = new StringBuilder("");
        try {
            for (int i = 0; i < finalAnsList.size(); i++) {
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("questionNum", finalAnsList.get(i).getQueId());

                ArrayList<String> arr = new ArrayList<String>();
                if (finalAnsList.get(i).getCustomeAns().getAnswerType() == 1) {//for multiple choice ans
                    String[] userAnswers = MathFriendzyHelper.getCommaSepratedOption
                            (finalAnsList.get(i).getUserAns(), ",");
                    for (int j = 0; j < userAnswers.length; j++) {
                        arr.add(userAnswers[j]);
                    }
                } else {
                    arr.add(finalAnsList.get(i).getUserAns());
                }

                jsonObj.put("answersGiven", (Object) arr);

                if (finalAnsList.get(i).isUserAnsCorrect()) {
                    jsonObj.put("isAnswerGivenCorrect", "1");
                } else {
                    jsonObj.put("isAnswerGivenCorrect", "0");
                }

                jsonObj.put("wasAnswerWrongFirstTime", finalAnsList.get(i).getFirstTimeWrong() + "");

                if (customeResult.isAlreadyPlayed()) {
                    jsonObj.put("workImageName", finalAnsList.get(i).getImageName().replace(".png", ""));
                } else {
                    if (MathFriendzyHelper.checkForExistenceOfFile(finalAnsList.get(i).getImageName()))
                        jsonObj.put("workImageName", finalAnsList.get(i).getImageName().replace(".png", ""));
                    else {
                        jsonObj.put("workImageName", "");
                    }
                }

                //jsonObj.put("workAreaMessage", finalAnsList.get(i).getStdmessage());
                jsonObj.put("workAreaMessage", this.getWorkAreaMessage(finalAnsList.get(i)));
                jsonObj.put("questionImage", finalAnsList.get(i).getQuestionImage().replace(".png", ""));
                jsonObj.put("isWorkAreaPublic", finalAnsList.get(i).isWorkAreaPublic() + "");
                jsonObj.put("gotHelpFromOthers", finalAnsList.get(i).isGetHelpFromOther() + "");
                jsonObj.put("teacherCreditGiven", finalAnsList.get(i).getTeacherCredit() + "");
                jsonObj.put("chatRequestId", finalAnsList.get(i).getChatRequestId() + "");
                strBuilder.append(jsonObj.toString() + ",");
            }
            strBuilder.deleteCharAt(strBuilder.length() - 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "[" + strBuilder.toString() + "]";
    }

    /**
     * Based on has seen by student
     */
    public void setTeacherCredit(StudentAnsBase std) {
        if (std.isHasSeenAns() || std.isGetHelpFromOther() == 1/*|| std.getFirstTimeWrong() == 1*/) {
            std.setTeacherCredit(HALF_CREDIT);//for half credit
            std.setGetHelpFromOther(1);
        } else {
            std.setTeacherCredit(FULL_CREDIT);//for half credit
        }
    }

    /**
     * Set teacher credit for wrong ans
     *
     * @param std
     */
    public void setTeacherCreditForWrongAns(StudentAnsBase std) {
        //std.setTeacherCredit(NONE_CREDITT);
        if (customeResult.isAlreadyPlayed()) {
            std.setTeacherCredit(ZERO_CREDIT);
//            if(std.isPrevAnsCorrect() == 1){
//                std.setTeacherCredit(NONE_CREDITT);
//            }
        } else {
            std.setTeacherCredit(NONE_CREDITT);
        }
    }

    /**
     * Calculate answers given by user
     */
    private void calculateAns(boolean isClickOnBackPressed, boolean isFirstTimeCall) {
        try {
            boolean isGivenAllQuestionsAnswer = true;

            this.saveFillInAns();
            ArrayList<StudentAnsBase> finalAnsList = new ArrayList<StudentAnsBase>();

            for (int i = 0; i < userFillInAnsList.size(); i++) {
                finalAnsList.add(userFillInAnsList.get(i));
            }

            for (int i = 0; i < multipleChoiceAnsList.size(); i++) {
                multipleChoiceAnsList.get(i).setUserAns
                        (MathFriendzyHelper.
                                convertStringIntoSperatedStringWithDelimeter
                                        (multipleChoiceAnsList.get(i).getUserAns(), ","));
                finalAnsList.add(multipleChoiceAnsList.get(i));
            }

            for (int i = 0; i < checkUserAnswer.size(); i++) {
                finalAnsList.add(checkUserAnswer.get(i));
            }

            int totalProblems = customeResult.getProblem();
            int rightAnsCounter = 0;
            int halfCreditCounter = 0;
            boolean isGiveAtleastOneQuestionAns = false;

            for (int i = 0; i < finalAnsList.size(); i++) {
                if (finalAnsList.get(i).getUserAns() != null
                        && finalAnsList.get(i).getUserAns().length() > 0) {
                    isGiveAtleastOneQuestionAns = true;
                    if (finalAnsList.get(i).getCustomeAns().getFillInType() == 1) {
                        if (this.compareFillInAns(finalAnsList.get(i).getCustomeAns().getCorrectAns(),
                                finalAnsList.get(i).getUserAns())) {
                            finalAnsList.get(i).setUserAnsCorrect(true);
                            //if(finalAnsList.get(i).getTeacherCredit() == 1)
                            //rightAnsCounter ++ ;
                            this.setTeacherCredit(finalAnsList.get(i));
                        } else {
                            this.setTeacherCreditForWrongAns(finalAnsList.get(i));
                        }
                    } else if (finalAnsList.get(i).getCustomeAns().getAnswerType() == 1) {
                        if (this.compareMultiPleChoiceAns(finalAnsList.get(i).getCustomeAns()
                                .getCorrectAns(), finalAnsList.get(i).getUserAns())) {
                            finalAnsList.get(i).setUserAnsCorrect(true);
                            //if(finalAnsList.get(i).getTeacherCredit() == 1)
                            //rightAnsCounter ++ ;
                            this.setTeacherCredit(finalAnsList.get(i));
                        } else {
                            this.setTeacherCreditForWrongAns(finalAnsList.get(i));
                            //finalAnsList.get(i).setTeacherCredit(NONE_CREDITT);
                        }
                    } else if (finalAnsList.get(i).getCustomeAns().getAnswerType() == 2) {
                        if (this.compareTrueFalseOrYesNoAns
                                (finalAnsList.get(i).getCustomeAns().getCorrectAns(),
                                        finalAnsList.get(i).getUserAns())) {
                            finalAnsList.get(i).setUserAnsCorrect(true);
                            //if(finalAnsList.get(i).getTeacherCredit() == 1)
                            //rightAnsCounter ++ ;
                            this.setTeacherCredit(finalAnsList.get(i));
                        } else {
                            this.setTeacherCreditForWrongAns(finalAnsList.get(i));
                        }
                    } else if (finalAnsList.get(i).getCustomeAns().getAnswerType() == 3) {
                        if (this.compareTrueFalseOrYesNoAns
                                (finalAnsList.get(i).getCustomeAns().getCorrectAns(),
                                        finalAnsList.get(i).getUserAns())) {
                            finalAnsList.get(i).setUserAnsCorrect(true);
                            //if(finalAnsList.get(i).getTeacherCredit() == 1)
                            //rightAnsCounter ++ ;
                            this.setTeacherCredit(finalAnsList.get(i));
                        } else {
                            this.setTeacherCreditForWrongAns(finalAnsList.get(i));
                        }
                    }
                } else {
                    if (customeResult.getAllowChanges() == 0) {
                        //isGivenAllQuestionsAnswer = false;
                        //break;
                    } else {
                        finalAnsList.get(i).setUserAnsCorrect(false);
                        if (finalAnsList.get(i).getIsAnswerAvailable() == MathFriendzyHelper.YES) {
                            finalAnsList.get(i).setTeacherCredit(NONE_CREDITT);
                        }
                    }
                }
            }

            //update counter for score
            for (int i = 0; i < finalAnsList.size(); i++) {
                if ((finalAnsList.get(i).getTeacherCredit() == NONE_CREDITT &&
                        finalAnsList.get(i).isUserAnsCorrect()) ||
                        finalAnsList.get(i).getTeacherCredit() == FULL_CREDIT) {
                    rightAnsCounter++;
                } else if (finalAnsList.get(i).getTeacherCredit() == HALF_CREDIT) {
                    halfCreditCounter++;
                }
            }

            this.updatePlayerAnsList(finalAnsList, isClickOnBackPressed);

            if (isFirstTimeCall) {
                updateCalculeteAnsOnServer(rightAnsCounter,
                        totalProblems, finalAnsList, halfCreditCounter);
            } else {
                if (!isClickOnBackPressed) {
                    if (isGivenAllQuestionsAnswer) {
                        if (isGiveAtleastOneQuestionAns) {
                            if (customeResult.getAllowChanges() == 1) {
                                updateCalculeteAnsOnServer(rightAnsCounter,
                                        totalProblems, finalAnsList, halfCreditCounter);
                            } else {
                                //lblYouWontBeAbleToChangeAnswers click on calculate
                                //lblYourTeacherWillNotAllowYouToChange click on back
                                //lblYouWontBeAbleToChangeAnswers for old message
                                String message = MathFriendzyHelper.getTreanslationTextById(this,
                                        "lblYouWontBeAbleToChangeAnswers");
                                if (this.isClickOnBackPressed) {
                                    message = MathFriendzyHelper.getTreanslationTextById(this,
                                            "lblYourTeacherWillNotAllowYouToChange");
                                }

                                if (this.isAllAnsGiven()) {
                                    updateCalculeteAnsOnServer(rightAnsCounter,
                                            totalProblems, finalAnsList, halfCreditCounter);
                                    return;
                                }

                                /*if (this.isClickOnBackPressed) {
                                    updateCalculeteAnsOnServer(rightAnsCounter,
                                            totalProblems, finalAnsList, halfCreditCounter);
                                } else {*/
                                final int finalRightAnsCounter = rightAnsCounter;
                                final int finalTotalProblem = totalProblems;
                                final int finalHalfCreditCounter = halfCreditCounter;
                                final ArrayList<StudentAnsBase> finalFinalAnsList = finalAnsList;
                                MathFriendzyHelper.yesNoConfirmationDialog(this,
                                        message,
                                        yesButtonText, noButtonText, new YesNoListenerInterface() {
                                            @Override
                                            public void onYes() {
                                                updateCalculeteAnsOnServer(finalRightAnsCounter,
                                                        finalTotalProblem, finalFinalAnsList, finalHalfCreditCounter);
                                            }

                                            @Override
                                            public void onNo() {
                                                setCalculateButtonValue(false);
                                            }
                                        });
                                //}
                            }
                        } else {
                            MathFriendzyHelper.warningDialog(this, pleaseAnsAtleastOneQuestion);
                            this.setCalculateButtonValue(false);
                        }
                    } else {
                        MathFriendzyHelper.warningDialog(this, pleaseAnswerAllQuestionMsg);
                        this.setCalculateButtonValue(false);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update the calculated data on server
     *
     * @param rightAnsCounter
     * @param totalProblems
     * @param finalAnsList
     * @param halfCreditCounter
     */
    private void updateCalculeteAnsOnServer(int rightAnsCounter, int totalProblems,
                                            ArrayList<StudentAnsBase> finalAnsList, int halfCreditCounter) {

        //userScore = (rightAnsCounter * 100) / totalProblems;

        userScore = Math.round((((rightAnsCounter + ((float) halfCreditCounter / 2)) * 100) / totalProblems));

		/*Log.e(TAG, "user Score " + userScore + " right " +
        rightAnsCounter + " half credit " + halfCreditCounter / 2);*/

        final AddCustomeAnswerByStudentParam param = new AddCustomeAnswerByStudentParam();
        param.setAction("addCustomAnswersByStdent");
        param.setUserId(selectedPlayerData.getParentUserId());
        param.setPlayerId(selectedPlayerData.getPlayerid());
        param.setHWId(homeWorkQuizzData.getHomeWorkId());
        param.setCustomHWId(customeResult.getCustomHwId() + "");
        param.setScore(userScore);
        param.setNumberOfQuestions(totalProblems);

        param.setQuestionJson(this.getJosnString(finalAnsList));

        //this.startUploadingImages(finalAnsList);
        //Log.e(TAG, "question json " + this.getJosnString(finalAnsList));

        if (CommonUtils.isInternetConnectionAvailable(this)) {
            new MyAsyckTask(ServerOperation.createPostRequestForSaveCustomeAnsBuStudents(param)
                    , null, ServerOperationUtil.SAVE_HW_CUSTOME_ANS_BY_STUDENTS, this,
                    this, ServerOperationUtil.SIMPLE_DIALOG, true,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        } else {

            new AsyncTask<Void, Void, SaveHomeWorkServerResponse>() {
                private ProgressDialog pd = null;

                @Override
                protected void onPreExecute() {
                    pd = ServerDialogs.getProgressDialog
                            (ActCheckHomeWork.this,
                                    getString(R.string.please_wait_dialog_msg), 1);
                    pd.show();
                    super.onPreExecute();
                }

                @Override
                protected SaveHomeWorkServerResponse doInBackground(
                        Void... params) {

                    MathFriendzyHelper.saveCustomeForOfflineHW(ActCheckHomeWork.this, param);
                    SaveHomeWorkServerResponse response = MathFriendzyHelper
                            .getUpdatedResultForCustomeForOfflineHomework
                                    (customeResult.getCustomHwId(),
                                            userScore + "", customePlayListByStudent);

                    if (response != null) {
                        //changes for offline
                        resultAfterPlayed = response;
                        savePlayedDataIntoLocal();
                        //end offline changes
                    }

                    return response;
                }

                @Override
                protected void onPostExecute(SaveHomeWorkServerResponse response) {
                    if (pd != null && pd.isShowing()) {
                        pd.cancel();
                    }

                    if (response != null) {

						/*//changes for offline
                        resultAfterPlayed = response;
						savePlayedDataIntoLocal();
						//end offline changes
						 */

                        if (isClickOnBackPressed) {//If user click on back and save user answer on server
                            Intent intent = new Intent();
                            intent.putExtra("resultAfterPlayed", response);
                            intent.putExtra("customePlayData", customePlayListByStudent);
                            setResult(Activity.RESULT_OK, intent);
                            finish();
                        } else {
                            resultAfterPlayed = response;
                            updateScreenAfterCalculateResultSuccess(response);
                        }
                    }else{
                        //MathFriendzyHelper.showWarningDialog(ActCheckHomeWork.this , "Response is Null");
                    }
                    super.onPostExecute(response);
                }
            }.execute();
        }
    }


    /**
     * Update the customePlayListByStudent which need to update on the back screen
     *
     * @param finalAnsList
     * @param isClickOnBackPressed
     */
    private void updatePlayerAnsList(ArrayList<StudentAnsBase> finalAnsList,
                                     boolean isClickOnBackPressed) {

        //create new list to update
        customePlayListByStudent = new ArrayList<CustomePlayerAns>();

        for (int i = 0; i < finalAnsList.size(); i++) {
            CustomePlayerAns customePlayer = new CustomePlayerAns();
            customePlayer.setQueNo(finalAnsList.get(i).getQueId());
            if (finalAnsList.get(i).getUserAns() != null
                    && finalAnsList.get(i).getUserAns().length() > 0) {
                customePlayer.setAns(finalAnsList.get(i).getUserAns());
            } else {
                customePlayer.setAns("");
            }

            if (finalAnsList.get(i).isUserAnsCorrect()) {
                customePlayer.setIsCorrect(1);
            } else {
                customePlayer.setIsCorrect(0);
            }

            if (customeResult.isAlreadyPlayed()) {
                customePlayer.setWorkImage(finalAnsList.get(i).getImageName().replace(".png", ""));
            } else {
                if (MathFriendzyHelper.checkForExistenceOfFile(finalAnsList.get(i).getImageName()))
                    customePlayer.setWorkImage(finalAnsList.get(i).getImageName().replace
                            (".png", ""));
                else {
                    customePlayer.setWorkImage("");
                }
            }

            customePlayer.setFirstTimeWrong(finalAnsList.get(i).getFirstTimeWrong());
            customePlayer.setTeacherCredit(finalAnsList.get(i).getTeacherCredit() + "");
            customePlayer.setMessage(finalAnsList.get(i).getStdmessage());
            customePlayer.setQuestionImage(finalAnsList.get(i).getQuestionImage());
            customePlayer.setIsWorkAreaPublic(finalAnsList.get(i).isWorkAreaPublic());
            customePlayer.setChatRequestedId(finalAnsList.get(i).getChatRequestId());
            customePlayer.setWorkInput(finalAnsList.get(i).getWorkInput());
            customePlayer.setOpponentInput(finalAnsList.get(i).getOpponentInput());
            customePlayer.setFixedWorkImage(finalAnsList.get(i).getFixedWorkImage());
            customePlayer.setFixedMessage(finalAnsList.get(i).getFixedMessage());
            customePlayer.setFixedQuestionImage(finalAnsList.get(i).getFixedQuestionImage());
            customePlayer.setIsGetHelp(finalAnsList.get(i).isGetHelpFromOther()); // siddhiinfosoft
            try {
                customePlayer.setUrlLinks(finalAnsList.get(i).getUrlLinks());
            }catch (Exception e){
                e.printStackTrace();
            }
            /*Log.e(TAG, "work area  oponent input" + finalAnsList.get(i).getOpponentInput()
             + " work input" + finalAnsList.get(i).getWorkInput());*/
            customePlayListByStudent.add(customePlayer);
        }
    }

    /**
     * Update the customePlayListByStudent which need to update on the back screen
     *
     * @param finalAnsList
     * @param isClickOnBackPressed
     */
    private void updatePlayerAnsListForTeacher(ArrayList<StudentAnsBase> finalAnsList,
                                               boolean isClickOnBackPressed) {

        //create new list to update
        customePlayListByStudent = new ArrayList<CustomePlayerAns>();

        for (int i = 0; i < finalAnsList.size(); i++) {
            CustomePlayerAns customePlayer = new CustomePlayerAns();
            customePlayer.setQueNo(finalAnsList.get(i).getQueId());
            if (finalAnsList.get(i).getUserAns() != null
                    && finalAnsList.get(i).getUserAns().length() > 0) {
                customePlayer.setAns(finalAnsList.get(i).getUserAns());
            } else {
                customePlayer.setAns("");
            }

            customePlayer.setIsCorrect(finalAnsList.get(i).isPrevAnsCorrect());

            if (customeResult.isAlreadyPlayed()) {
                customePlayer.setWorkImage(finalAnsList.get(i).getImageName().replace(".png", ""));
            } else {
                if (MathFriendzyHelper.checkForExistenceOfFile(finalAnsList.get(i).getImageName()))
                    customePlayer.setWorkImage(finalAnsList.get(i).getImageName().replace
                            (".png", ""));
                else {
                    customePlayer.setWorkImage("");
                }
            }

            customePlayer.setFirstTimeWrong(finalAnsList.get(i).getFirstTimeWrong());
            customePlayer.setTeacherCredit(finalAnsList.get(i).getTeacherCredit() + "");
            customePlayer.setMessage(finalAnsList.get(i).getStdmessage());
            customePlayer.setQuestionImage(finalAnsList.get(i).getQuestionImage());
            customePlayer.setIsWorkAreaPublic(finalAnsList.get(i).isWorkAreaPublic());
            customePlayer.setChatRequestedId(finalAnsList.get(i).getChatRequestId());
            customePlayer.setWorkInput(finalAnsList.get(i).getWorkInput());
            customePlayer.setOpponentInput(finalAnsList.get(i).getOpponentInput());
            //Log.e(TAG, "work area " + finalAnsList.get(i).isWorkAreaPublic() + " " + finalAnsList.get(i).getQueId());

            customePlayListByStudent.add(customePlayer);
        }
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase,
                               int requestCode) {
        if (requestCode == ServerOperationUtil.SAVE_HW_CUSTOME_ANS_BY_STUDENTS) {
            final SaveHomeWorkServerResponse response =
                    (SaveHomeWorkServerResponse) httpResponseBase;
            if (response.getResult().equals("success")) {
                response.setCurrentPlaySubCatScore(userScore + "");

                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params) {
                        //changes for offline
                        resultAfterPlayed = response;
                        savePlayedDataIntoLocal();
                        //end offline changes
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void result) {

                        if (isClickOnBackPressed) {//If user click on back and save user answer on server
                            Intent intent = new Intent();
                            intent.putExtra("resultAfterPlayed", response);
                            intent.putExtra("customePlayData", customePlayListByStudent);
                            setResult(Activity.RESULT_OK, intent);
                            finish();
                        } else {
                            resultAfterPlayed = response;
                            updateScreenAfterCalculateResultSuccess(response);
                        }
                        super.onPostExecute(result);
                    }
                }.execute();

				/*//changes for offline
                resultAfterPlayed = response;
				this.savePlayedDataIntoLocal();
				//end offline changes

				if(isClickOnBackPressed){//If user click on back and save user answer on server
					Intent intent = new Intent();
					intent.putExtra("resultAfterPlayed", response);
					intent.putExtra("customePlayData", customePlayListByStudent);
					setResult(Activity.RESULT_OK, intent);
					finish();
				}else{
					resultAfterPlayed = response;
					updateScreenAfterCalculateResultSuccess(response);
				}*/
            }
        } else if (requestCode == ServerOperationUtil.SAVE_TEACHER_CHECK_HW_CHANGES_REQUEST) {
            SaveHomeWorkServerResponse response = (SaveHomeWorkServerResponse) httpResponseBase;

			/*try{
                //changes for offline
				response.setCurrentPlaySubCatScore
				(response.getCustomeScore());
				selectedPlayerData.setPlayerid(studentDetail.getPlayerId());
				selectedPlayerData.setParentUserId(studentDetail.getParentId());

				resultAfterPlayed = response;
				this.savePlayedDataIntoLocal();
				//end offline changes
			}catch(Exception e){
				e.printStackTrace();
			}*/

            Intent intent = new Intent();
            intent.putExtra("resultAfterPlayed", response);
            intent.putExtra("customePlayData", customePlayListByStudent);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    /**
     * Update the data and UI when result success after calculation
     *
     * @param response
     */
    private void updateScreenAfterCalculateResultSuccess(SaveHomeWorkServerResponse response) {

		/*if(CommonUtils.LOG_ON)
            Log.e(TAG, "updateScreenAfterCalculateResultSuccess " +
					"customePlayListByStudent size " + customePlayListByStudent.size());*/

        customeResult.setScore(response.getCurrentPlaySubCatScore());
        //customeResult.setScore(response.getCurrentPlaySubCatScore());
        customeResult.setAlreadyPlayed(true);
        customeResult.setCustomePlayerAnsList(customePlayListByStudent);

        this.init();
        this.initilizeAllowChangesVariable();
        this.setCustomeDataToListLayout();
        this.setCreditAndAvgScore();
        this.setVisibilityOfCalculateButton();//Not to move above this method its dependency on setCustomeDataToListLayout()

        this.setCalculateButtonValue(false);
    }

    /**
     * Compare fill in answer
     * @param teacherAns
     * @param studentAns
     * @return
     */
//    private boolean compareFillInAns(String teacherAns , String studentAns){
//        return teacherAns.replace("," , "").equalsIgnoreCase(studentAns.replace("," , ""));
//    }

    /**
     * compare true/false and yes/no ans
     *
     * @param teacherAns
     * @param studentAns
     * @return
     */
    private boolean compareTrueFalseOrYesNoAns(String teacherAns, String studentAns) {
        return teacherAns.equalsIgnoreCase(studentAns);
    }

    /**
     * compare multiple choice ans
     *
     * @param teacherAns
     * @param studentAns
     * @return
     */
    private boolean compareMultiPleChoiceAns(String teacherAns, String studentAns) {
        //Log.e(TAG, "teacher ans " + teacherAns + " student and " + studentAns);
        String[] commaSepratedString = MathFriendzyHelper.getCommaSepratedOption(studentAns, ",");
        String[] commaSepratedTeacherString = MathFriendzyHelper
                .getCommaSepratedOption(teacherAns, ",");

        if (commaSepratedString.length == commaSepratedTeacherString.length) {
            if (commaSepratedString.length > 0) {
                for (int i = 0; i < commaSepratedString.length; i++) {
                    if (!teacherAns.contains(commaSepratedString[i])) {
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    /**
     * Check forAtlease one answer given
     *
     * @return
     */
    private boolean isAtleaseOneAnsGiven() {
        ArrayList<StudentAnsBase> finalAnsList = this.getFinalStudenAnsList();
        for (int i = 0; i < finalAnsList.size(); i++) {
            if (finalAnsList.get(i).getUserAns() != null
                    && finalAnsList.get(i).getUserAns().length() > 0)
                return true;
        }
        return false;
    }

    /**
     * Check forAtlease one answer given
     *
     * @return
     */
    private boolean isAllAnsGiven() {
        /*ArrayList<StudentAnsBase> finalAnsList = this.getFinalStudenAnsList();
        for(int i = 0 ; i < finalAnsList.size() ; i ++ ){
            if(!(finalAnsList.get(i).getUserAns() != null
                    && finalAnsList.get(i).getUserAns().length() > 0))
                return false;
        }
        return true;*/
        try {
            ArrayList<CustomePlayerAns> playerAns = customeResult.getCustomePlayerAnsList();
            for (int i = 0; i < playerAns.size(); i++) {
                if (!(playerAns.get(i).getAns() != null
                        && playerAns.get(i).getAns().length() > 0))
                    return false;
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Finish only for oponent input
     */
    private void finishOnlyForOponentInput() {
        Intent intent = new Intent();
        intent.putExtra("numberOfActiveOponentInput"
                , numberOfActiveOponentInput);
        intent.putExtra("isOnlyForOponentInput", true);
        setResult(RESULT_OK, intent);
        finish();
    }

    /*@Override
    public void onBackPressed() {
        if (is_keyboard_show) {
            this.hideKeyboard();
        } else {
            if (isExpireQuizz) {
                if (isCalledFortecherCheckHW) {
                    if (customeResult.isAlreadyPlayed())
                        this.calculateTeacherChanges();
                    else
                        super.onBackPressed();
                } else {
                    //super.onBackPressed();
                    this.finishOnlyForOponentInput();
                }
            } else {

                if (this.isAtleaseOneAnsGiven()) {
                    if (customeResult.getAllowChanges() == 1) {//1 for allow changes
                        if (customeResult.isAlreadyPlayed()) {
                            isClickOnBackPressed = true;
                            this.calculateAns(false, false);
                        } else {
                            if (this.isAtleaseOneAnsGiven()) {
                                isClickOnBackPressed = true;
                                this.calculateAns(false, false);
                            } else {
                                super.onBackPressed();
                            }
                        }
                    } else {
                        if (customeResult.isAlreadyPlayed()) {
                            isClickOnBackPressed = true;
                            this.calculateAns(false, false);
                        } else {
                            if (this.isAtleaseOneAnsGiven()) {
                                MathFriendzyHelper.yesNoConfirmationDialog(this,
                                        lblYourTeacherWillNotAllowYouToChange,
                                        yesButtonText, noButtonText,
                                        new YesNoListenerInterface() {

                                            @Override
                                            public void onYes() {
                                                superBackPressedCall();
                                            }

                                            @Override
                                            public void onNo() {

                                            }
                                        });
                            } else {
                                super.onBackPressed();
                            }
                        }
                    }
                } else {
                    super.onBackPressed();
                }
            }
        }
    }*/

    /**
     * For super back call
     */
    private void superBackPressedCall() {
        super.onBackPressed();
    }

    private boolean isClickToCalculate = false;

    private void setCalculateButtonValue(boolean isClickToCalculate) {
        this.isClickToCalculate = isClickToCalculate;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCalculate:
                if (isClickToCalculate) {
                    return;
                }

                isClickToCalculate = true;
                if (isExpireQuizz) {
                    this.showCantEditAnswerDialog();
                    this.setCalculateButtonValue(false);
                } else {
                    //if (is_on_calculate) {
                    //is_on_calculate = false;
                    isClickOnBackPressed = false;
                    this.hideKeyboard();
                    this.calculateAns(false, false);
                    //}
                }
                break;
            case R.id.btnsecondryKeyBoard:
                numericLayout.setVisibility(RelativeLayout.GONE);
                sumbolicLayout.setVisibility(RelativeLayout.VISIBLE);
                break;
            case R.id.btnNumber:
                numericLayout.setVisibility(RelativeLayout.VISIBLE);
                sumbolicLayout.setVisibility(RelativeLayout.GONE);
                break;
            case R.id.btn1:
                this.setTextToSelectedTextView(selectedTextView, "1");
                break;
            case R.id.btn2:
                this.setTextToSelectedTextView(selectedTextView, "2");
                break;
            case R.id.btn3:
                this.setTextToSelectedTextView(selectedTextView, "3");
                break;
            case R.id.btn4:
                this.setTextToSelectedTextView(selectedTextView, "4");
                break;
            case R.id.btn5:
                this.setTextToSelectedTextView(selectedTextView, "5");
                break;
            case R.id.btn6:
                this.setTextToSelectedTextView(selectedTextView, "6");
                break;
            case R.id.btn7:
                this.setTextToSelectedTextView(selectedTextView, "7");
                break;
            case R.id.btn8:
                this.setTextToSelectedTextView(selectedTextView, "8");
                break;
            case R.id.btn9:
                this.setTextToSelectedTextView(selectedTextView, "9");
                break;
            case R.id.btn0:
                this.setTextToSelectedTextView(selectedTextView, "0");
                break;
            case R.id.btnSynchronized:
                this.cleartextFromSelectedTextView(selectedTextView);
                break;
            case R.id.btnDot:
                this.setTextToSelectedTextView(selectedTextView, ".");
                break;
            case R.id.btnMinus:
                this.setTextToSelectedTextView(selectedTextView, "-");
                break;
            case R.id.btnComma:
                this.setTextToSelectedTextView(selectedTextView, ",");
                break;
            case R.id.btnPlus:
                this.setTextToSelectedTextView(selectedTextView, "+");
                break;
            case R.id.btnDevide:
                this.setTextToSelectedTextView(selectedTextView, "/");
                break;
            case R.id.btnMultiply:
                this.setTextToSelectedTextView(selectedTextView, "*");
                break;
            case R.id.btnCollon:
                this.setTextToSelectedTextView(selectedTextView, ":");
                break;
            case R.id.btnLessThan:
                this.setTextToSelectedTextView(selectedTextView, "<");
                break;
            case R.id.btnGreaterThan:
                this.setTextToSelectedTextView(selectedTextView, ">");
                break;
            case R.id.btnEqual:
                this.setTextToSelectedTextView(selectedTextView, "=");
                break;
            case R.id.btnRoot:
                this.setTextToSelectedTextView(selectedTextView, "√");
                break;
            case R.id.btnDevider:
                this.setTextToSelectedTextView(selectedTextView, "|");
                break;
            case R.id.btnOpeningBracket:
                this.setTextToSelectedTextView(selectedTextView, "(");
                break;
            case R.id.btnClosingBracket:
                this.setTextToSelectedTextView(selectedTextView, ")");
                break;
            case R.id.btnViewSheet:
                this.clickOnViewSheet();
                break;
            case R.id.btnViewResources:
                this.clickOnViewResources();
                break;
            case R.id.imgInfo:
                this.showInfo();
                break;
            case R.id.btnShortAns:
                this.shortAnswer();
                break;
            case R.id.btnLinks:
                this.showAddLinksDialog();
                break;
        }
    }

    private void clickOnViewResources() {
        //MathFriendzyHelper.showWarningDialog(this , "Under Developement");
        if (CommonUtils.isInternetConnectionAvailable(this)) {
            GetGooruResourcesForHWParam param = new GetGooruResourcesForHWParam();
            param.setAction("getGooruResourcesForHw");
            if (isCalledFortecherCheckHW)
                param.setHomeworkId(studentDetail.getHomeWorkId());
            else
                param.setHomeworkId(homeWorkQuizzData.getHomeWorkId());
            param.setCustomHWId(customeResult.getCustomHwId() + "");
            new MyAsyckTask(ServerOperation.CreatePostRequestForGetGooruResourcesForHomework(param)
                    , null, ServerOperationUtil.GET_GOORU_RESOURCES_FOR_HW, this,
                    new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase,
                                                   int requestCode) {
                            GetGooruResourceForHWResponse response =
                                    (GetGooruResourceForHWResponse) httpResponseBase;
                            if (response.getResult().equalsIgnoreCase
                                    (MathFriendzyHelper.SUCCESS)) {
                                if (response.getSelectedResourceList() != null
                                        && response.getSelectedResourceList().size() > 0) {
                                    Intent intent = new Intent(ActCheckHomeWork.this,
                                            ActSelectedResource.class);
                                    intent.putExtra("isOpenForViewResources", true);
                                    intent.putExtra("selectedResourceList",
                                            response.getSelectedResourceList());
                                    intent.putExtra("hwTitle", customeResult.getTitle());
                                    startActivity(intent);
                                }
                            }
                        }
                    }, ServerOperationUtil.SIMPLE_DIALOG, true,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        } else {
            CommonUtils.showInternetDialog(this);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == START_WORK_AREA_REQUEST_CODE) {
                clickedStudent.setStdmessage(data.getStringExtra("studentMessage"));
                clickedStudent.setWorkAreaPublic(data.getIntExtra("workareapublic", 1));

                //if(data.getIntExtra("chatRequestId" , 0) > 0)
                clickedStudent.setChatRequestId(data.getIntExtra("chatRequestId", 0));

                if (data.getBooleanExtra("isImageSaved", false)) {
                    clickedStudent.setImageName(data.getStringExtra("imageName"));
                    //for downloaded image
                    //this.addToDownloadImageList(data.getStringExtra("imageName"));
                }

                //for question image
                if (data.getBooleanExtra("isDeleteImage", false)) {
                    clickedStudent.setQuestionImage
                            (data.getStringExtra("questionImageName"));
                    //for downloaded image
                    this.addToDownloadImageList(data.getStringExtra("questionImageName"));
                } else {
                    if (data.getBooleanExtra("isClickOnPaste", false)) {
                        clickedStudent.setQuestionImage
                                (data.getStringExtra("questionImageName"));
                        //for downloaded image
                        this.addToDownloadImageList(data.getStringExtra("questionImageName"));
                        cropImageUriFromPdf = "";
                    }
                }

                //Add Gdoc and web links
                try {
                    ArrayList<AddUrlToWorkArea> urlToWorkAreaArrayList =
                            (ArrayList<AddUrlToWorkArea>) data.getSerializableExtra("urlLinks");
                    clickedPlayerAns.setUrlLinks(urlToWorkAreaArrayList);
                    clickedStudent.setUrlLinks(urlToWorkAreaArrayList);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //end changes

                //end question image
                if (isCalledFortecherCheckHW) {
                    int teacherCredit = data.getIntExtra("teacherCredit", 0);
                    //if(clickedStudent.getTeacherCredit() != teacherCredit){
                    clickedStudent.setTeacherCredit(teacherCredit);
                    //update the player data
                    clickedPlayerAns.setTeacherCredit(teacherCredit + "");
                    clickedPlayerAns.setMessage(data.getStringExtra("studentMessage"));
                    clickedPlayerAns.setIsWorkAreaPublic(data.getIntExtra("workareapublic", 1));
                    if (data.getBooleanExtra("isImageSaved", false)) {
                        clickedPlayerAns.setWorkImage(data.getStringExtra("imageName"));
                    }
                    clickedPlayerAns.setQuestionImage(clickedStudent.getQuestionImage());

                    if (data.getBooleanExtra("isWorkInputSeen", false)) {
                        if (clickedStudent.getWorkInput().
                                equalsIgnoreCase(MathFriendzyHelper.YES + "")) {
                            numberOfActiveOponentInput--;
                        }
                        clickedPlayerAns.setWorkInput(MathFriendzyHelper.NO + "");
                        clickedStudent.setWorkInput(MathFriendzyHelper.NO + "");
                    }

                    this.init();
                    this.initilizeAllowChangesVariable();
                    this.setCustomeDataToListLayout();
                    this.setCreditAndAvgScore();
                    this.setVisibilityOfCalculateButton();
                } else {
                    try {
                        if (data.getBooleanExtra("isWorkInputSeen", false)) {
                            if (clickedStudent.getWorkInput().
                                    equalsIgnoreCase(MathFriendzyHelper.YES + "")) {
                                decreaseActiveInput();
                                numberOfActiveOponentInput--;
                            }
                            clickedStudent.setWorkInput(MathFriendzyHelper.NO + "");
                            this.setRoughAreaButtonBackGround(clickedStudent, clickedbtnRoughWork);
                        }

                        if (data.getBooleanExtra("isOpponentInputSeen", false)) {
                            if (clickedStudent.getOpponentInput().
                                    equalsIgnoreCase(MathFriendzyHelper.YES + "")) {
                                decreaseActiveInput();
                                numberOfActiveOponentInput--;
                            }
                            clickedStudent.setOpponentInput(MathFriendzyHelper.NO + "");
                            this.setRoughAreaButtonBackGround(clickedStudent, clickedbtnRoughWork);

                        }

                        if (data.getBooleanExtra("isAnsUpdated", false)) {
                            this.setUpdatedDataAndRefreshView(data);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if (requestCode == START_ACT_PLUS_REQUEST_CODE) {
                clickedStudent.setStdmessage(data.getStringExtra("studentMessage"));
                clickedStudent.setWorkAreaPublic(data.getIntExtra("workareapublic", 1));

                //Add Gdoc and web links
                try {
                    ArrayList<AddUrlToWorkArea> urlToWorkAreaArrayList =
                            (ArrayList<AddUrlToWorkArea>) data.getSerializableExtra("urlLinks");
                    clickedPlayerAns.setUrlLinks(urlToWorkAreaArrayList);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //end changes

                if (data.getIntExtra("isGetHelp", 0) > 0)
                    clickedStudent.setGetHelpFromOther(data.getIntExtra("isGetHelp", 0));

                //if(data.getIntExtra("chatRequestId" , 0) > 0)
                clickedStudent.setChatRequestId(data.getIntExtra("chatRequestId", 0));

                if (data.getBooleanExtra("hasSeenAnswer", false)) {
                    clickedStudent.setHasSeenAns(data.getBooleanExtra("hasSeenAnswer", false));
                }

                if (data.getIntExtra("isFirstTimeWrong", 0) > 0)
                    clickedStudent.setFirstTimeWrong(1);

                if (data.getBooleanExtra("isImageSaved", false)) {
                    clickedStudent.setImageName(data.getStringExtra("imageName"));
                }

                //for question image
                if (data.getBooleanExtra("isDeleteImage", false)) {
                    clickedStudent.setQuestionImage
                            (data.getStringExtra("questionImageName"));
                } else {
                    if (data.getBooleanExtra("isClickOnPaste", false)) {
                        clickedStudent.setQuestionImage
                                (data.getStringExtra("questionImageName"));
                        cropImageUriFromPdf = "";
                    }
                }
                downloadImageList = data.getStringArrayListExtra("downloadImageList");

                if (data.getBooleanExtra("isWorkInputSeen", false)) {
                    if (clickedStudent.getWorkInput().
                            equalsIgnoreCase(MathFriendzyHelper.YES + "")) {
                        decreaseActiveInput();
                    }
                    clickedStudent.setWorkInput(MathFriendzyHelper.NO + "");
                    this.setRoughAreaButtonBackGround(clickedStudent, clickedbtnRoughWork);
                }

                if (data.getBooleanExtra("isOpponentInputSeen", false)) {
                    if (clickedStudent.getOpponentInput().
                            equalsIgnoreCase(MathFriendzyHelper.YES + "")) {
                        decreaseActiveInput();
                    }
                    clickedStudent.setOpponentInput(MathFriendzyHelper.NO + "");
                    this.setRoughAreaButtonBackGround(clickedStudent, clickedbtnRoughWork);
                }

                if (data.getBooleanExtra("isAnsUpdated", false)) {
                    this.setUpdatedDataAndRefreshView(data);
                }
            } else if (requestCode == START_ACT_OPEN_PDF_REQUEST_CODE) {
                cropImageUriFromPdf = data.getStringExtra("cropImageUri");
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Calculate answer for teacher check homework
     */
    private ArrayList<StudentAnsBase> getFinalStudenAnsList() {
        ArrayList<StudentAnsBase> finalAnsList = new ArrayList<StudentAnsBase>();
        this.saveFillInAns();

        for (int i = 0; i < userFillInAnsList.size(); i++) {
            finalAnsList.add(userFillInAnsList.get(i));
        }

        for (int i = 0; i < multipleChoiceAnsList.size(); i++) {
            multipleChoiceAnsList.get(i).setUserAns
                    (MathFriendzyHelper.
                            convertStringIntoSperatedStringWithDelimeter
                                    (multipleChoiceAnsList.get(i).getUserAns(), ","));
            finalAnsList.add(multipleChoiceAnsList.get(i));
        }

        for (int i = 0; i < checkUserAnswer.size(); i++) {
            finalAnsList.add(checkUserAnswer.get(i));
        }
        return finalAnsList;
    }

    /**
     * Calculate the score
     *
     * @param finalAnsList
     * @return
     */
    private String calculateScore(ArrayList<StudentAnsBase> finalAnsList) {
        return customeResult.getScore();
    }

    /**
     * Return the work rea message
     * Return blank id internet is connected
     *
     * @param studentAns
     * @return
     */
    private String getWorkAreaMessage(StudentAnsBase studentAns) {
        if (CommonUtils.isInternetConnectionAvailable(this))
            return "";
        return studentAns.getStdmessage();
    }

    /**
     * Create the data json String for the Teacher save response
     *
     * @return
     */
    private String getDataJsonForTeacher() {
        try {
            ArrayList<StudentAnsBase> finalAnsList = this.getFinalStudenAnsList();
            //update the player answer list , update customePlayListByStudent list which need to be sent
            //on the back screen after result save on server
            this.updatePlayerAnsListForTeacher(finalAnsList, isClickOnBackPressed);

            org.json.JSONObject josnObj = new org.json.JSONObject();
            josnObj.put("userId", studentDetail.getParentId());
            josnObj.put("playerId", studentDetail.getPlayerId());
            josnObj.put("homeworkId", studentDetail.getHomeWorkId());
            josnObj.put("customHwId", customeResult.getCustomHwId());
            josnObj.put("score", this.calculateScore(finalAnsList));

            JSONArray questionJsonArray = new JSONArray();
            for (int i = 0; i < finalAnsList.size(); i++) {
                StudentAnsBase studentAns = finalAnsList.get(i);
                org.json.JSONObject jsonQuestion = new org.json.JSONObject();
                jsonQuestion.put("question", studentAns.getQueId());
                jsonQuestion.put("credit", studentAns.getTeacherCredit());
                //jsonQuestion.put("workAreaMessage", studentAns.getStdmessage());
                jsonQuestion.put("workAreaMessage", this.getWorkAreaMessage(studentAns));
                jsonQuestion.put("workImage", studentAns.getImageName());
                questionJsonArray.put(jsonQuestion);
            }
            josnObj.put("questions", questionJsonArray);
            //return josnObj.toString().replaceAll("\\\\", "");
            return josnObj.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    //for teacher
    private void calculateTeacherChanges() {
        SaveCheckTeacherChangesParam param = new SaveCheckTeacherChangesParam();
        param.setAction("addNotesByTeacherForStudent");
        param.setData(this.getDataJsonForTeacher());

        //super.onBackPressed();
        new MyAsyckTask(ServerOperation.createPostRequestForAddNotesByTeacherForStudent(param)
                , null, ServerOperationUtil.SAVE_TEACHER_CHECK_HW_CHANGES_REQUEST, this,
                this, ServerOperationUtil.SIMPLE_DIALOG, true,
                getString(R.string.please_wait_dialog_msg))
                .execute();
    }

    /**
     * View answer sheet
     */
    private void clickOnViewSheet() {
        final String pdfFileName = customeResult.getSheetName();
        if (PDFOperation.checkForExistanceOfPdf(this, pdfFileName)
                /*&& (!PDFOperation.isFileCorrupted(pdfFileName , this))*/) {
            PDFOperation.isFileCorrupted(pdfFileName, this, new OnRequestSuccess() {
                @Override
                public void onRequestSuccess(boolean isSuccess) {
                    CommonUtils.printLog(TAG, "inside clickOnViewSheet "
                            + isSuccess);
                    if (isSuccess) {//true if file corrupted
                        downloadPdf(pdfFileName);
                    } else {
                        openPdfAct(pdfFileName);
                    }
                }
            });
            //openPdfAct(pdfFileName);
        } else {
            downloadPdf(pdfFileName);
            /*if(CommonUtils.isInternetConnectionAvailable(this)){
                PDFOperation.downloadPdf(this, ICommonUtils.DOWNLOAD_WORK_PDF_URL,
                        pdfFileName, new OnRequestSuccess() {

                            @Override
                            public void onRequestSuccess(boolean isSuccess) {
                                if(isSuccess) {
                                    openPdfAct(pdfFileName);
                                }else{
                                    MathFriendzyHelper.showWarningDialog(ActCheckHomeWork.this ,
                                            alertNoHomeworkSheetAttached);
                                }
                            }
                        });
            }else{
                CommonUtils.showInternetDialog(this);
            }*/
        }
    }

    private void downloadPdf(final String pdfFileName) {
        if (CommonUtils.isInternetConnectionAvailable(this)) {
            PDFOperation.downloadPdf(this, ICommonUtils.DOWNLOAD_WORK_PDF_URL,
                    pdfFileName, new OnRequestSuccess() {

                        @Override
                        public void onRequestSuccess(boolean isSuccess) {
                            if (isSuccess) {
                                openPdfAct(pdfFileName);
                            } else {
                                MathFriendzyHelper.showWarningDialog(ActCheckHomeWork.this,
                                        alertNoHomeworkSheetAttached);
                            }
                        }
                    });
        } else {
            CommonUtils.showInternetDialog(this);
        }
    }

    /**
     * View pdf file
     *
     * @param pdfFileName
     */
    private void openPdfAct(String pdfFileName) {
        Intent intent = new Intent(this, ActViewPdf.class);
        intent.putExtra("pdfFileName", pdfFileName);
        startActivityForResult(intent, START_ACT_OPEN_PDF_REQUEST_CODE);
    }

    /**
     * This method update the local data for creating json
     */
    private void updateDataToCreateJsonWhichIsToBeSaved() {

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "updateDataToCreateJsonWhichIsToBeSaved" +
                    " customePlayListByStudent size "
                    + customePlayListByStudent.size());

        //global update
        for (int i = 0; i < HomeWorkListAdapter.homeWorkresult
                .get(ActHomeWork.selectedPosition)
                .getCustomeresultList().size(); i++) {
            if (customeResult.getCustomHwId() ==
                    HomeWorkListAdapter.homeWorkresult.get
                            (ActHomeWork.selectedPosition).
                            getCustomeresultList().get(i)
                            .getCustomHwId()) {
                if (resultAfterPlayed != null) {
                    HomeWorkListAdapter.homeWorkresult.get
                            (ActHomeWork.selectedPosition).
                            getCustomeresultList().get(i)
                            .setScore(resultAfterPlayed.getCurrentPlaySubCatScore());
                    HomeWorkListAdapter.homeWorkresult.get
                            (ActHomeWork.selectedPosition).
                            getCustomeresultList().get(i)
                            .setAlreadyPlayed(true);
                }
                HomeWorkListAdapter.homeWorkresult.get
                        (ActHomeWork.selectedPosition).
                        getCustomeresultList().get(i)
                        .setCustomePlayerAnsList(customePlayListByStudent);
            }
        }
        MathFriendzyHelper.updateMainHomeworkList(resultAfterPlayed);
    }

    /**
     * Save the played data into local
     */
    private void savePlayedDataIntoLocal() {
        try {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    updateDataToCreateJsonWhichIsToBeSaved();
                    return null;
                }

                @Override
                protected void onPostExecute(Void result) {
                    MathFriendzyHelper.createJsonForHomeworkAndSaveIntoLocal
                            (ActCheckHomeWork.this, HomeWorkListAdapter.homeWorkresult,
                                    selectedPlayerData.getParentUserId()
                                    , selectedPlayerData.getPlayerid(),
                                    new HttpServerRequest() {
                                        @Override
                                        public void onRequestComplete() {

                                        }
                                    });
                    super.onPostExecute(result);
                }
            }.execute();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeCurrentObj() {
        currentObj = this;
    }

    public static ActCheckHomeWork getCurrentObj() {
        return currentObj;
    }

    /*public void calculateAnswerFromChatSession(){

    }*/

    /**
     * Check for adding work area content to duplicate area
     *
     * @return
     */
    public boolean isAddWorkAreaContentToDuplicateArea() {
        if (clickedStudent.isGetHelpFromOther() == MathFriendzyHelper.NO
                && clickedStudent.getFirstTimeWrong() == MathFriendzyHelper.NO)
            return true;
        return false;
    }

    /**
     * Return the add duplicate work area param
     *
     * @param paramFor
     * @return
     */
    public AddDuplicateWorkAreaParam getAddWorkAreaContentsToDuplicateArea(int paramFor) {
        if (this.isAddWorkAreaContentToDuplicateArea()) {
            AddDuplicateWorkAreaParam param = new AddDuplicateWorkAreaParam();
            param.setAction("addWorkAreaContentsToDuplicateArea");
            param.setHwId(homeWorkQuizzData.getHomeWorkId());
            param.setCustomHWId(customeResult.getCustomHwId() + "");
            param.setUserId(selectedPlayerData.getParentUserId());
            param.setPlayerId(selectedPlayerData.getPlayerid());
            param.setQuestion(clickedStudent.getQueId());

            if (!MathFriendzyHelper.isEmpty(clickedStudent.getImageName())) {
                param.setFixedWorkImageName("fwa_" + clickedStudent.getImageName().replace(".png", ""));
            } else {
                param.setFixedWorkImageName("");
            }

            if (!MathFriendzyHelper.isEmpty(clickedStudent.getQuestionImage())) {
                param.setFixedQuestionImage("fwa_" + clickedStudent.getQuestionImage().replace(".png", ""));
            } else {
                param.setFixedQuestionImage("");
            }
            param.setWorkImageName(clickedStudent.getImageName().replace(".png", ""));
            param.setQuestionImageName(clickedStudent.getQuestionImage().replace(".png", ""));
            param.setFixedMessage(clickedStudent.getStdmessage());

            if (AddDuplicateWorkAreaParam.GET_HELP_PARAM == paramFor) {
                clickedStudent.setGetHelpFromOther(MathFriendzyHelper.YES);
                param.setGothelp(MathFriendzyHelper.YES);
                param.setSawAnswer(clickedStudent.getFirstTimeWrong());
            } else if (AddDuplicateWorkAreaParam.SAW_ANSWER_PARAM == paramFor) {
                clickedStudent.setFirstTimeWrong(MathFriendzyHelper.YES);
                param.setGothelp(clickedStudent.isGetHelpFromOther());
                param.setSawAnswer(MathFriendzyHelper.YES);
            }
            return param;
        } else {
            return null;
        }
    }

    /**
     * Add duplicate area or student for showing on the second work area tab
     *
     * @param context
     * @param paramFor
     */
    public void addDuplicateWork(Context context, int paramFor) {
        AddDuplicateWorkAreaParam param = this.getAddWorkAreaContentsToDuplicateArea(paramFor);
        if (param != null) {
            new MyAsyckTask(ServerOperation.createPostRequestForAddDuplicateWorkForStudent(param)
                    , null, ServerOperationUtil.ADD_DUPLICATE_WORK_FOR_STUDENT, context,
                    new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase,
                                                   int requestCode) {

                        }
                    }, ServerOperationUtil.SIMPLE_DIALOG, true,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        }
    }

    /**
     * Add duplicate area or student for showing on the second work area tab
     *
     * @param context
     * @param paramFor
     */
    public void addDuplicateWork(Context context, int paramFor, final OnRequestComplete listener) {
        AddDuplicateWorkAreaParam param = this.getAddWorkAreaContentsToDuplicateArea(paramFor);
        if (param != null) {
            new MyAsyckTask(ServerOperation.createPostRequestForAddDuplicateWorkForStudent(param)
                    , null, ServerOperationUtil.ADD_DUPLICATE_WORK_FOR_STUDENT, context,
                    new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase,
                                                   int requestCode) {
                            listener.onComplete();
                        }
                    }, ServerOperationUtil.SIMPLE_DIALOG, false,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        }
    }

    public void updateWorkAreaContentForDuplicateWorkArea(Context context, String imageName,
                                                          String questionImageName,
                                                          String chatMessage, OnRequestComplete listener) {
        clickedStudent.setImageName(imageName);
        clickedStudent.setQuestionImage(questionImageName);
        clickedStudent.setStdmessage(chatMessage);
        this.addDuplicateWork(context, AddDuplicateWorkAreaParam.GET_HELP_PARAM, listener);
    }


    /**
     * Update the UI after the tutor responsed
     *
     * @param response
     */
    public void updateUIAfterTutorInput(final HelpSetudentResponse response) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int reqId = MathFriendzyHelper.parseInt(response.getChatReqId());
                    String getQuestionNumberByReqId = updateDataForNotificationChangesAndReturnQuesId(reqId);
                    if (!MathFriendzyHelper.isEmpty(getQuestionNumberByReqId)) {
                        View view = getViewByQuesId(getQuestionNumberByReqId);
                        if (view != null) {
                            Button btnRough = (Button) view.findViewById(R.id.btnRoughWork);
                            setRoughButtonBackground(btnRough, true);
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update the UI after the tutor responsed
     *
     * @param response
     */
    public void updateUIAfterTutorInputForImageUpdate(final TutorImageUpdateNotif response) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int reqId = MathFriendzyHelper.parseInt(response.getChatRequestId());
                    String getQuestionNumberByReqId = updateDataForNotificationChangesAndReturnQuesId(reqId);
                    if (!MathFriendzyHelper.isEmpty(getQuestionNumberByReqId)) {
                        View view = getViewByQuesId(getQuestionNumberByReqId);
                        if (view != null) {
                            Button btnRough = (Button) view.findViewById(R.id.btnRoughWork);
                            setRoughButtonBackground(btnRough, true);
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private View getViewByQuesId(String questionId) {
        try {
            return putViewOnKeyQuestionNumber.get(questionId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private String updateDataForNotificationChangesAndReturnQuesId(int reqId) {
        ArrayList<StudentAnsBase> finalAnsList = new ArrayList<StudentAnsBase>();
        for (int i = 0; i < userFillInAnsList.size(); i++) {
            finalAnsList.add(userFillInAnsList.get(i));
        }
        for (int i = 0; i < multipleChoiceAnsList.size(); i++) {
            multipleChoiceAnsList.get(i).setUserAns
                    (MathFriendzyHelper.
                            convertStringIntoSperatedStringWithDelimeter
                                    (multipleChoiceAnsList.get(i).getUserAns(), ","));
            finalAnsList.add(multipleChoiceAnsList.get(i));
        }
        for (int i = 0; i < checkUserAnswer.size(); i++) {
            finalAnsList.add(checkUserAnswer.get(i));
        }

        for (int i = 0; i < finalAnsList.size(); i++) {
            if (reqId == finalAnsList.get(i).getChatRequestId()) {
                finalAnsList.get(i).setOpponentInput(MathFriendzyHelper.YES + "");
                return finalAnsList.get(i).getQueId();
            }
        }
        return "";
    }

    /**
     * Decrease active oponent input
     */
    private void decreaseActiveInput() {
        try {
            if (selectedPlayerData != null) {
                int activeInput = MathFriendzyHelper.getNumberOfActiveHomeWorkFromServer
                        (this, selectedPlayerData.getParentUserId(),
                                selectedPlayerData.getPlayerid());
                activeInput--;
                MathFriendzyHelper.saveNumberOfActiveHomeWorkFromServer
                        (this, selectedPlayerData.getParentUserId(),
                                selectedPlayerData.getPlayerid(), activeInput);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setVisibilityOfStudentViewedAnswerLayout() {
        if (isCalledFortecherCheckHW) {
            rlStudentViewedAnswer.setVisibility(RelativeLayout.VISIBLE);
        } else {
            rlStudentViewedAnswer.setVisibility(RelativeLayout.GONE);
        }
    }

    private void setShortAnswerWidgetsForTeacheer() {
        if (isCalledFortecherCheckHW) {
            imgInfo.setVisibility(ImageView.VISIBLE);
            btnShortAns.setVisibility(Button.VISIBLE);
        } else {
            imgInfo.setVisibility(ImageView.GONE);
            btnShortAns.setVisibility(Button.GONE);
        }
    }

    private void shortAnswer() {
        isShortByCorrectAns = !isShortByCorrectAns;
        ArrayList<CustomeAns> customeAnsList = customeResult.getCustomeAnsList();
        if (isShortByCorrectAns) {
            Collections.sort(customeAnsList, new SortByCorrectAnsForTeacher());
        } else {
            Collections.sort(customeAnsList, new SortByWrongAnswerForTeacher());
        }
        customeResult.setCustomeAnsList(customeAnsList);
        this.setCustomeDataToListLayout();
    }

    private void showInfo() {
        MathFriendzyHelper.showWarningDialogForSomeLongMessage(this, lblGreenNumberAndRedNumText);
    }

    /*@Override
    protected void onResume() {
        MathFriendzyHelper.showWarningDialog(this, MathFriendzyHelper.getMemoryInfo());
        super.onResume();
    }*/

    //siddhiinfosoft BELOVE ALL METHOD ADDED BY SIDDHIINFOSOFT


    private boolean compareFillInAns(String teacherAns, String studentAns) { // siddhiinfosoft
        try {
            boolean brt = false;

            teacherAns = teacherAns.replace(" ", "");
            studentAns = studentAns.replace(" ", "");

            teacherAns = teacherAns.replace("-", "op_minus");
            studentAns = studentAns.replace("-", "op_minus");

            if (!(teacherAns.contains("#"))) { // for old keyboard

                String str = studentAns.replace("text#", "");

                brt = teacherAns.replace(",", "").equalsIgnoreCase(str.replace(",", ""));
                return brt;
            }

            try {
                String stsr = teacherAns.substring(teacherAns.length() - 4);

                if (stsr.equals("@op#")) {
                    teacherAns = teacherAns.substring(0, (teacherAns.length() - 4));
                }

            } catch (Exception e) {

            }

            brt = teacherAns.replace(",", "").equalsIgnoreCase(studentAns.replace(",", ""));

            if (brt) {
                return brt;
            } else {

                brt = this.comparereversestring(teacherAns, studentAns);

                return brt;
            }
        } catch (Exception e) {
            return false;
        }
    }

    private boolean comparereversestring(String correct_ans, String user_ans) {
        try {
            String[] str_1 = correct_ans.split("@");
            String[] str_2 = user_ans.split("@");

            Collections.reverse(Arrays.asList(str_1));

            boolean btr = false;

            for (int i = 0; i < str_1.length; i++) {

                String sstr = str_1[i];

                if (sstr.contains("op") || sstr.contains("text")) {

                    if (sstr.contains("op_equal") || sstr.contains("op_plus") || sstr.contains("op_multi") || sstr.contains("text")) {
                        btr = true;
                    } else {
                        btr = false;
                        break;
                    }

                } else {
                    btr = false;
                    break;

                }

            }

            if (btr) {

                if (Arrays.equals(str_1, str_2)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public void showKeyboard() {

        try {    // siddhiinfosoft
            this.hideDeviceKeyboard();
            if (!is_keyboard_show) {
                if (!isCalledFortecherCheckHW) {

                    if (this.isQuizExpireForStudent()) {
                        this.showCantEditAnswerDialog();
                        return;
                    }

                    is_keyboard_show = true;
                    keyboardLayout.setVisibility(RelativeLayout.VISIBLE);
                    animation = AnimationUtils.loadAnimation(this, R.anim.keyboard_show_animation);
                    keyboardLayout.setAnimation(animation);
                }

            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    hideKeyboardAfterSomeTime();
                }
            } , 10);
        } catch (Exception e) {

        }

    }

    public void hideKeyboard() {

        try {   // siddhiinfosoft
            if (is_keyboard_show) {
                is_keyboard_show = false;
                animation = AnimationUtils.loadAnimation(this, R.anim.keyboard_hide_animation);
                keyboardLayout.setAnimation(animation);
                keyboardLayout.setVisibility(RelativeLayout.GONE);
                //this.cleasFocusFromEditText();
                ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
                ll_main_eq_sub_symbol.setVisibility(View.GONE);
                CommonUtils.printLog("cursor_postion " + cursor_postion);
            }
        } catch (Exception e) {

        }

    }

    private String replacecharfromsign(final String str) {
        String str_final = str;

        try {

            String str_plus = "" + Html.fromHtml("&#43;");
            String str_minus = "" + Html.fromHtml("&#8722;");
            String str_equal = "" + Html.fromHtml("&#61;");
            String str_multi = "" + Html.fromHtml("&#215;");
            String str_divide = "" + Html.fromHtml("&#247;");
            String str_union = "" + Html.fromHtml("&#8746;");
            String str_intersection = "" + Html.fromHtml("&#8745;");
            String str_pi = "" + Html.fromHtml("&#960;");
            String str_factorial = "" + Html.fromHtml("&#33;");
            String str_percentage = "" + Html.fromHtml("&#37;");
            String str_goe = "" + Html.fromHtml("&#8805;");
            String str_loe = "" + Html.fromHtml("&#8804;");
            String str_grthan = "" + Html.fromHtml("&#62;");
            String str_lethan = "" + Html.fromHtml("&#60;");
            String str_infinity = "" + Html.fromHtml("&#8734;");
            String str_degree = "" + Html.fromHtml("&#176;");
            String str_xbar = "" + Html.fromHtml("x&#x0304;");

            String str_integral = "" + Html.fromHtml("&#8747;");
            String str_summation = "" + Html.fromHtml("&#8721;");
            String str_alpha = "" + Html.fromHtml("&#945;");
            String str_theta = "" + Html.fromHtml("ø");
            String str_mu = "" + Html.fromHtml("&#956;");
            String str_sigma = "" + Html.fromHtml("&#963;");

            String str_beta = "" + Html.fromHtml("&#946;");
            String str_angle = "" + Html.fromHtml("&#8736;");
            String str_mangle = "" + Html.fromHtml("&#8737;");
            String str_sangle = "" + Html.fromHtml("&#8738;");
            String str_rangle = "" + Html.fromHtml("&#8735;");
            String str_triangle = "" + Html.fromHtml("&#9651;");
            String str_rectangle = "" + Html.fromHtml("&#9645;");
            String str_parallelogram = "" + Html.fromHtml("&#9649;");
            String str_perpendicular = "" + Html.fromHtml("&#8869;");
            String str_congruent = "" + Html.fromHtml("&#8773;");
            String str_similarty = "" + Html.fromHtml("&#8764;");
            String str_parallel = "" + Html.fromHtml("&#8741;");

            String str_arcm = "" + Html.fromHtml("&#8242;");
            String str_arcs = "" + Html.fromHtml("&#8243;");

            str_final = str_final.replace(str_plus, "op_plus");
            str_final = str_final.replace(str_minus, "op_minus");
            str_final = str_final.replace(str_equal, "op_equal");
            str_final = str_final.replace(str_multi, "op_multi");
            str_final = str_final.replace(str_divide, "op_divide");
            str_final = str_final.replace(str_union, "op_union");
            str_final = str_final.replace(str_intersection, "op_intersection");
            str_final = str_final.replace(str_pi, "op_pi");
            str_final = str_final.replace(str_factorial, "op_factorial");
            str_final = str_final.replace(str_percentage, "op_percentage");
            str_final = str_final.replace(str_goe, "op_goe");
            str_final = str_final.replace(str_loe, "op_loe");
            str_final = str_final.replace(str_grthan, "op_grthan");
            str_final = str_final.replace(str_lethan, "op_lethan");
            str_final = str_final.replace(str_infinity, "op_infinity");
            str_final = str_final.replace(str_degree, "op_degree");
            str_final = str_final.replace(str_xbar, "op_xbar");

            str_final = str_final.replace(str_integral, "op_integral");
            str_final = str_final.replace(str_summation, "op_summation");
            str_final = str_final.replace(str_alpha, "op_alpha");
            str_final = str_final.replace(str_theta, "op_theta");
            str_final = str_final.replace(str_mu, "op_mu");
            str_final = str_final.replace(str_sigma, "op_sigma");

            str_final = str_final.replace(str_beta, "op_beta");
            str_final = str_final.replace(str_angle, "op_angle");
            str_final = str_final.replace(str_mangle, "op_mangle");
            str_final = str_final.replace(str_sangle, "op_sangle");
            str_final = str_final.replace(str_rangle, "op_rangle");
            str_final = str_final.replace(str_triangle, "op_triangle");
            str_final = str_final.replace(str_rectangle, "op_rectangle");
            str_final = str_final.replace(str_parallelogram, "op_parallelogram");
            str_final = str_final.replace(str_perpendicular, "op_perpendicular");
            str_final = str_final.replace(str_congruent, "op_congruent");
            str_final = str_final.replace(str_similarty, "op_similarty");
            str_final = str_final.replace(str_parallel, "op_parallel");

            str_final = str_final.replace(str_arcm, "op_arcm");
            str_final = str_final.replace(str_arcs, "op_arcs");


            //by a1
            str_final = str_final.replace("-", "op_minus");


        } catch (Exception e) {

        }

        return str_final;
    }

    private String replacesignfromchar(final String str, final EditTextBlink ed) {
        String str_final = str;

        try {

            String str_plus = "" + Html.fromHtml("&#43;");
            String str_minus = "" + Html.fromHtml("&#8722;");
            String str_equal = "" + Html.fromHtml("&#61;");
            String str_multi = "" + Html.fromHtml("&#215;");
            String str_divide = "" + Html.fromHtml("&#247;");
            String str_union = "" + Html.fromHtml("&#8746;");
            String str_intersection = "" + Html.fromHtml("&#8745;");
            String str_pi = "" + Html.fromHtml("&#960;");
            String str_factorial = "" + Html.fromHtml("&#33;");
            String str_percentage = "" + Html.fromHtml("&#37;");
            String str_goe = "" + Html.fromHtml("&#8805;");
            String str_loe = "" + Html.fromHtml("&#8804;");
            String str_grthan = "" + Html.fromHtml("&#62;");
            String str_lethan = "" + Html.fromHtml("&#60;");
            String str_infinity = "" + Html.fromHtml("&#8734;");
            String str_degree = "" + Html.fromHtml("&#176;");
            String str_xbar = "" + Html.fromHtml("x&#x0304;");

            String str_integral = "" + Html.fromHtml("&#8747;");
            String str_summation = "" + Html.fromHtml("&#8721;");
            String str_alpha = "" + Html.fromHtml("&#945;");
            String str_theta = "" + Html.fromHtml("ø");
            String str_mu = "" + Html.fromHtml("&#956;");
            String str_sigma = "" + Html.fromHtml("&#963;");

            String str_beta = "" + Html.fromHtml("&#946;");
            String str_angle = "" + Html.fromHtml("&#8736;");
            String str_mangle = "" + Html.fromHtml("&#8737;");
            String str_sangle = "" + Html.fromHtml("&#8738;");
            String str_rangle = "" + Html.fromHtml("&#8735;");
            String str_triangle = "" + Html.fromHtml("&#9651;");
            String str_rectangle = "" + Html.fromHtml("&#9645;");
            String str_parallelogram = "" + Html.fromHtml("&#9649;");
            String str_perpendicular = "" + Html.fromHtml("&#8869;");
            String str_congruent = "" + Html.fromHtml("&#8773;");
            String str_similarty = "" + Html.fromHtml("&#8764;");
            String str_parallel = "" + Html.fromHtml("&#8741;");

            String str_arcm = "" + Html.fromHtml("&#8242;");
            String str_arcs = "" + Html.fromHtml("&#8243;");

            str_final = str_final.replace("op_plus", str_plus);
            str_final = str_final.replace("op_minus", str_minus);
            str_final = str_final.replace("op_equal", str_equal);
            str_final = str_final.replace("op_multi", str_multi);
            str_final = str_final.replace("op_divide", str_divide);
            str_final = str_final.replace("op_union", str_union);
            str_final = str_final.replace("op_intersection", str_intersection);
            str_final = str_final.replace("op_pi", str_pi);
            str_final = str_final.replace("op_factorial", str_factorial);
            str_final = str_final.replace("op_percentage", str_percentage);
            str_final = str_final.replace("op_goe", str_goe);
            str_final = str_final.replace("op_loe", str_loe);
            str_final = str_final.replace("op_grthan", str_grthan);
            str_final = str_final.replace("op_lethan", str_lethan);
            str_final = str_final.replace("op_infinity", str_infinity);
            str_final = str_final.replace("op_degree", str_degree);
            str_final = str_final.replace("op_xbar", str_xbar);

            str_final = str_final.replace("op_alpha", str_alpha);
            str_final = str_final.replace("op_theta", str_theta);
            str_final = str_final.replace("op_mu", str_mu);
            str_final = str_final.replace("op_sigma", str_sigma);

            str_final = str_final.replace("op_beta", str_beta);
            str_final = str_final.replace("op_angle", str_angle);
            str_final = str_final.replace("op_mangle", str_mangle);
            str_final = str_final.replace("op_sangle", str_sangle);
            str_final = str_final.replace("op_rangle", str_rangle);
            str_final = str_final.replace("op_triangle", str_triangle);
            str_final = str_final.replace("op_rectangle", str_rectangle);
            str_final = str_final.replace("op_parallelogram", str_parallelogram);
            str_final = str_final.replace("op_perpendicular", str_perpendicular);
            str_final = str_final.replace("op_congruent", str_congruent);
            str_final = str_final.replace("op_similarty", str_similarty);
            str_final = str_final.replace("op_parallel", str_parallel);

            str_final = str_final.replace("op_arcm", str_arcm);
            str_final = str_final.replace("op_arcs", str_arcs);

            str_final = str_final.replace("op_integral", str_integral);
            str_final = str_final.replace("op_summation", str_summation);

            for (int i = 0; i < str_final.length(); i++) {

                String ss = String.valueOf(str_final.charAt(i));

                if (ss.equals(str_integral) || ss.equals(str_summation)) {
                    SpannableString ss1 = new SpannableString(ss);
                    ss1.setSpan(new RelativeSizeSpan(1.6f), 0, 1, 0);
                    ed.getText().insert(ed.getSelectionStart(), ss1);
                } else if (ss.equals(str_perpendicular) || ss.equals(str_parallel) ||
                        ss.equals(str_rectangle) || ss.equals(str_parallelogram)) {
                    SpannableString ss1 = new SpannableString(ss);
                    ss1.setSpan(new RelativeSizeSpan(1.2f), 0, 1, 0);
                    ed.getText().insert(ed.getSelectionStart(), ss1);
                } else if (ss.equals(str_angle) || ss.equals(str_mangle) ||
                        ss.equals(str_sangle) || ss.equals(str_rangle)) {
                    SpannableString ss1 = new SpannableString(ss);
                    ss1.setSpan(new RelativeSizeSpan(1.5f), 0, 1, 0);
                    ed.getText().insert(ed.getSelectionStart(), ss1);
                } else {
                    ed.getText().insert(ed.getSelectionStart(), ss);
                }

                if (ss.equals(str_integral)) {
                    ed.getText().insert(ed.getSelectionStart(), " ");
                }

            }

        } catch (Exception e) {

        }

        return str_final;
    }


    private void setvisibilityofabc123layout(int int_layout) { // siddhiinfosoft

        if (int_layout == 1) {
            kb_ll_123.setVisibility(View.VISIBLE);
            kb_ll_abc.setVisibility(View.GONE);
        } else {
            kb_ll_123.setVisibility(View.GONE);
            kb_ll_abc.setVisibility(View.VISIBLE);
        }

    }

    protected void setanimationtoeqsymbols1() {  // siddhiinfosoft
        try {
            animation = AnimationUtils.loadAnimation(this, R.anim.symbol_hide_animation);
            hsv_keyboard_eq_symbol.setAnimation(animation);
        } catch (Exception e) {

        }
    }

    protected void setanimationtoeqsymbols2() {
        try {
            animation = AnimationUtils.loadAnimation(this, R.anim.symbol_show_animation);
            hsv_keyboard_eq_symbol.setAnimation(animation);
        } catch (Exception e) {

        }

    }

    public void onclkeqsmbl(View v) {

        try {

            int id_view = v.getId();
            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            if (ll_visible_view != null) {
                ll_visible_view.setVisibility(View.GONE);
            }

            switch (id_view) {

                case R.id.kb_eq_symbol_basicmath_sign1:
                case R.id.kb_eq_symbol_basicmath_sign1_arrow:
                    ll_visible_view = ll_kb_sub_sumbol_basicmath_sign1;
                    Utils.setvisibility_basicmath_subeq(ll_kb_sub_sumbol_basicmath_sign1, ll_kb_sub_sumbol_basicmath_sign2, ll_kb_sub_sumbol_basicmath_sign5);

                    break;

                case R.id.kb_eq_symbol_basicmath_sign2:
                case R.id.kb_eq_symbol_basicmath_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_basicmath_sign2;
                    Utils.setvisibility_basicmath_subeq(ll_kb_sub_sumbol_basicmath_sign2, ll_kb_sub_sumbol_basicmath_sign1, ll_kb_sub_sumbol_basicmath_sign5);

                    break;

                case R.id.kb_eq_symbol_basicmath_sign5:
                case R.id.kb_eq_symbol_basicmath_sign5_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_basicmath_sign5;
                    Utils.setvisibility_basicmath_subeq(ll_kb_sub_sumbol_basicmath_sign5, ll_kb_sub_sumbol_basicmath_sign1, ll_kb_sub_sumbol_basicmath_sign2);

                    break;

                case R.id.kb_eq_symbol_prealgebra_sign1:
                case R.id.kb_eq_symbol_prealgebra_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_prealgebra_sign1;
                    Utils.setvisibility_prealgebra_subeq(ll_kb_sub_sumbol_prealgebra_sign1, ll_kb_sub_sumbol_prealgebra_sign2,
                            ll_kb_sub_sumbol_prealgebra_sign4, ll_kb_sub_sumbol_prealgebra_sign5);

                    break;

                case R.id.kb_eq_symbol_prealgebra_sign2:
                case R.id.kb_eq_symbol_prealgebra_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_prealgebra_sign2;
                    Utils.setvisibility_prealgebra_subeq(ll_kb_sub_sumbol_prealgebra_sign2, ll_kb_sub_sumbol_prealgebra_sign1,
                            ll_kb_sub_sumbol_prealgebra_sign4, ll_kb_sub_sumbol_prealgebra_sign5);

                    break;

                case R.id.kb_eq_symbol_prealgebra_sign4:
                case R.id.kb_eq_symbol_prealgebra_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_prealgebra_sign4;
                    Utils.setvisibility_prealgebra_subeq(ll_kb_sub_sumbol_prealgebra_sign4, ll_kb_sub_sumbol_prealgebra_sign2,
                            ll_kb_sub_sumbol_prealgebra_sign1, ll_kb_sub_sumbol_prealgebra_sign5);


                    break;

                case R.id.kb_eq_symbol_prealgebra_sign5:
                case R.id.kb_eq_symbol_prealgebra_sign5_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_prealgebra_sign5;
                    Utils.setvisibility_prealgebra_subeq(ll_kb_sub_sumbol_prealgebra_sign5, ll_kb_sub_sumbol_prealgebra_sign1, ll_kb_sub_sumbol_prealgebra_sign2,
                            ll_kb_sub_sumbol_prealgebra_sign4);


                    break;

                case R.id.kb_eq_symbol_algebra_sign1:
                case R.id.kb_eq_symbol_algebra_sign1_arrow:
                case R.id.kb_eq_symbol_geometry_sign1:
                case R.id.kb_eq_symbol_geometry_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_algebra_sign1;
                    Utils.setvisibility_algebra_subeq(ll_kb_sub_sumbol_algebra_sign1, ll_kb_sub_sumbol_algebra_sign2,
                            ll_kb_sub_sumbol_algebra_sign4, ll_kb_sub_sumbol_algebra_sign5,
                            ll_kb_sub_sumbol_algebra_sign8);


                    break;

                case R.id.kb_eq_symbol_algebra_sign2:
                case R.id.kb_eq_symbol_algebra_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_algebra_sign2;
                    Utils.setvisibility_algebra_subeq(ll_kb_sub_sumbol_algebra_sign2, ll_kb_sub_sumbol_algebra_sign1,
                            ll_kb_sub_sumbol_algebra_sign4, ll_kb_sub_sumbol_algebra_sign5,
                            ll_kb_sub_sumbol_algebra_sign8);

                    break;

                case R.id.kb_eq_symbol_algebra_sign4:
                case R.id.kb_eq_symbol_algebra_sign4_arrow:
                case R.id.kb_eq_symbol_geometry_sign3:
                case R.id.kb_eq_symbol_geometry_sign3_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_algebra_sign4;
                    Utils.setvisibility_algebra_subeq(ll_kb_sub_sumbol_algebra_sign4, ll_kb_sub_sumbol_algebra_sign2,
                            ll_kb_sub_sumbol_algebra_sign1,
                            ll_kb_sub_sumbol_algebra_sign5,
                            ll_kb_sub_sumbol_algebra_sign8);

                    break;

                case R.id.kb_eq_symbol_algebra_sign5:
                case R.id.kb_eq_symbol_algebra_sign5_arrow:
                case R.id.kb_eq_symbol_geometry_sign4:
                case R.id.kb_eq_symbol_geometry_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_algebra_sign5;
                    Utils.setvisibility_algebra_subeq(ll_kb_sub_sumbol_algebra_sign5, ll_kb_sub_sumbol_algebra_sign4,
                            ll_kb_sub_sumbol_algebra_sign2, ll_kb_sub_sumbol_algebra_sign1,
                            ll_kb_sub_sumbol_algebra_sign8);

                    break;


                case R.id.kb_eq_symbol_algebra_sign8:
                case R.id.kb_eq_symbol_algebra_sign8_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_algebra_sign8;
                    Utils.setvisibility_algebra_subeq(ll_kb_sub_sumbol_algebra_sign8, ll_kb_sub_sumbol_algebra_sign5, ll_kb_sub_sumbol_algebra_sign4,
                            ll_kb_sub_sumbol_algebra_sign2, ll_kb_sub_sumbol_algebra_sign1
                    );

                    break;

                case R.id.kb_eq_symbol_trignometry_sign1:
                case R.id.kb_eq_symbol_trignometry_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign1;
                    ll_kb_sub_sumbol_trignometry_sign1.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign2:
                case R.id.kb_eq_symbol_trignometry_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign2;
                    ll_kb_sub_sumbol_trignometry_sign2.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign3:

                    break;

                case R.id.kb_eq_symbol_trignometry_sign4:
                case R.id.kb_eq_symbol_trignometry_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign4;
                    ll_kb_sub_sumbol_trignometry_sign4.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign5:
                case R.id.kb_eq_symbol_trignometry_sign5_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign5;
                    ll_kb_sub_sumbol_trignometry_sign5.setVisibility(View.VISIBLE);

                    break;


                case R.id.kb_eq_symbol_trignometry_sign7:
                case R.id.kb_eq_symbol_trignometry_sign7_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign7;
                    ll_kb_sub_sumbol_trignometry_sign7.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign8:
                case R.id.kb_eq_symbol_trignometry_sign8_arrow:
                case R.id.kb_eq_symbol_geometry_sign8:
                case R.id.kb_eq_symbol_geometry_sign8_arrow:
                case R.id.kb_eq_symbol_calculus_sign11:
                case R.id.kb_eq_symbol_calculus_sign11_arrow:
                case R.id.kb_eq_symbol_precalculus_sign10:
                case R.id.kb_eq_symbol_precalculus_sign10_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign8;
                    ll_kb_sub_sumbol_trignometry_sign8.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign9:
                case R.id.kb_eq_symbol_trignometry_sign9_arrow:
                case R.id.kb_eq_symbol_geometry_sign9:
                case R.id.kb_eq_symbol_geometry_sign9_arrow:
                case R.id.kb_eq_symbol_calculus_sign12:
                case R.id.kb_eq_symbol_calculus_sign12_arrow:
                case R.id.kb_eq_symbol_precalculus_sign11:
                case R.id.kb_eq_symbol_precalculus_sign11_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign9;
                    ll_kb_sub_sumbol_trignometry_sign9.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign10:
                case R.id.kb_eq_symbol_trignometry_sign10_arrow:
                case R.id.kb_eq_symbol_geometry_sign10:
                case R.id.kb_eq_symbol_geometry_sign10_arrow:
                case R.id.kb_eq_symbol_calculus_sign13:
                case R.id.kb_eq_symbol_calculus_sign13_arrow:
                case R.id.kb_eq_symbol_precalculus_sign12:
                case R.id.kb_eq_symbol_precalculus_sign12_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign10;
                    ll_kb_sub_sumbol_trignometry_sign10.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign11:
                case R.id.kb_eq_symbol_trignometry_sign11_arrow:
                case R.id.kb_eq_symbol_geometry_sign11:
                case R.id.kb_eq_symbol_geometry_sign11_arrow:
                case R.id.kb_eq_symbol_calculus_sign14:
                case R.id.kb_eq_symbol_calculus_sign14_arrow:
                case R.id.kb_eq_symbol_precalculus_sign13:
                case R.id.kb_eq_symbol_precalculus_sign13_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign11;
                    ll_kb_sub_sumbol_trignometry_sign11.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign12:
                case R.id.kb_eq_symbol_trignometry_sign12_arrow:
                case R.id.kb_eq_symbol_geometry_sign12:
                case R.id.kb_eq_symbol_geometry_sign12_arrow:
                case R.id.kb_eq_symbol_calculus_sign15:
                case R.id.kb_eq_symbol_calculus_sign15_arrow:
                case R.id.kb_eq_symbol_precalculus_sign14:
                case R.id.kb_eq_symbol_precalculus_sign14_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign12;
                    ll_kb_sub_sumbol_trignometry_sign12.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign13:
                case R.id.kb_eq_symbol_trignometry_sign13_arrow:
                case R.id.kb_eq_symbol_geometry_sign13:
                case R.id.kb_eq_symbol_geometry_sign13_arrow:
                case R.id.kb_eq_symbol_calculus_sign16:
                case R.id.kb_eq_symbol_calculus_sign16_arrow:
                case R.id.kb_eq_symbol_precalculus_sign15:
                case R.id.kb_eq_symbol_precalculus_sign15_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign13;
                    ll_kb_sub_sumbol_trignometry_sign13.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_precalculus_sign1:
                case R.id.kb_eq_symbol_precalculus_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_precalculus_sign1;
                    ll_kb_sub_sumbol_precalculus_sign1.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_precalculus_sign2:
                case R.id.kb_eq_symbol_precalculus_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_precalculus_sign2;
                    ll_kb_sub_sumbol_precalculus_sign2.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_precalculus_sign3:

                    break;

                case R.id.kb_eq_symbol_precalculus_sign4:
                case R.id.kb_eq_symbol_precalculus_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_precalculus_sign4;
                    ll_kb_sub_sumbol_precalculus_sign4.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_precalculus_sign5:
                case R.id.kb_eq_symbol_precalculus_sign5_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_precalculus_sign5;
                    ll_kb_sub_sumbol_precalculus_sign5.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_precalculus_sign6:

                    break;

                case R.id.kb_eq_symbol_precalculus_sign7:

                    break;

                case R.id.kb_eq_symbol_precalculus_sign9:
                case R.id.kb_eq_symbol_precalculus_sign9_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_precalculus_sign9;
                    ll_kb_sub_sumbol_precalculus_sign9.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_geometry_sign2:
                case R.id.kb_eq_symbol_geometry_sign2_arrow:

                    ll_visible_view = ll_kb_sub_symbol_geometry_sign2;
                    ll_kb_sub_symbol_geometry_sign2.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_geometry_sign5:
                case R.id.kb_eq_symbol_geometry_sign5_arrow:

                    ll_visible_view = ll_kb_sub_symbol_geometry_sign5;
                    ll_kb_sub_symbol_geometry_sign5.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_geometry_sign6:
                case R.id.kb_eq_symbol_geometry_sign6_arrow:

                    ll_visible_view = ll_kb_sub_symbol_geometry_sign6;
                    ll_kb_sub_symbol_geometry_sign6.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_geometry_sign7:
                case R.id.kb_eq_symbol_geometry_sign7_arrow:

                    ll_visible_view = ll_kb_sub_symbol_geometry_sign7;
                    ll_kb_sub_symbol_geometry_sign7.setVisibility(View.VISIBLE);

                    break;


                case R.id.kb_eq_symbol_calculus_sign1:
                case R.id.kb_eq_symbol_calculus_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign1;
                    ll_kb_sub_sumbol_calculus_sign1.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_calculus_sign2:
                case R.id.kb_eq_symbol_calculus_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign2;
                    ll_kb_sub_sumbol_calculus_sign2.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_calculus_sign3:

                    break;

                case R.id.kb_eq_symbol_calculus_sign4:
                case R.id.kb_eq_symbol_calculus_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign4;
                    ll_kb_sub_sumbol_calculus_sign4.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_calculus_sign5:
                case R.id.kb_eq_symbol_calculus_sign5_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign5;
                    ll_kb_sub_sumbol_calculus_sign5.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_calculus_sign6:

                    break;

                case R.id.kb_eq_symbol_calculus_sign7:

                    break;

                case R.id.kb_eq_symbol_calculus_sign10:
                case R.id.kb_eq_symbol_calculus_sign10_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign10;
                    ll_kb_sub_sumbol_calculus_sign10.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_statistics_sign1:
                case R.id.kb_eq_symbol_statistics_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_statistics_sign1;
                    ll_kb_sub_sumbol_statistics_sign1.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_statistics_sign2:

                    break;

                case R.id.kb_eq_symbol_statistics_sign3:
                case R.id.kb_eq_symbol_statistics_sign3_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_statistics_sign3;
                    ll_kb_sub_sumbol_statistics_sign3.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_statistics_sign4:
                case R.id.kb_eq_symbol_statistics_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_statistics_sign4;
                    ll_kb_sub_sumbol_statistics_sign4.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_statistics_sign5:

                    break;

                case R.id.kb_eq_symbol_statistics_sign6:
                case R.id.kb_eq_symbol_statistics_sign6_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_statistics_sign6;
                    ll_kb_sub_sumbol_statistics_sign6.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_statistics_sign7:
                case R.id.kb_eq_symbol_statistics_sign7_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_statistics_sign7;
                    ll_kb_sub_sumbol_statistics_sign7.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_finitemath_sign1:
                case R.id.kb_eq_symbol_finitemath_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_finitemath_sign1;
                    ll_kb_sub_sumbol_finitemath_sign1.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_finitemath_sign2:
                case R.id.kb_eq_symbol_finitemath_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_finitemath_sign2;
                    ll_kb_sub_sumbol_finitemath_sign2.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_finitemath_sign3:

                    break;

                case R.id.kb_eq_symbol_finitemath_sign4:
                case R.id.kb_eq_symbol_finitemath_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_finitemath_sign4;
                    ll_kb_sub_sumbol_finitemath_sign4.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_finitemath_sign5:
                case R.id.kb_eq_symbol_finitemath_sign5_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_finitemath_sign5;
                    ll_kb_sub_sumbol_finitemath_sign5.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_finitemath_sign8:
                case R.id.kb_eq_symbol_finitemath_sign8_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_finitemath_sign8;
                    ll_kb_sub_sumbol_finitemath_sign8.setVisibility(View.VISIBLE);

                    break;

            }
        } catch (Exception e) {

        }
    }

    public void onkbclick(View v) {
        try {


            int id_view = v.getId();

            switch (id_view) {

                case R.id.btn_eq_name_pre_scroll:

                    this.callscrollmethod(1);

                    break;

                case R.id.btn_eq_name_next_scroll:

                    this.callscrollmethod(2);

                    break;

                case R.id.btn_eq_symbol_pre_scroll:

                    this.callscrollmethod(3);

                    break;

                case R.id.btn_eq_symbol_next_scroll:

                    this.callscrollmethod(4);

                    break;

                case R.id.kb_btn_abc_close:

                    hideKeyboard();

                    break;

                case R.id.kb_btn_123_close:

                    hideKeyboard();

                    break;

                case R.id.kb_btn_abc:

                    setvisibilityofabc123layout(2);

                    break;

                case R.id.kb_btn_123:

                    setvisibilityofabc123layout(1);

                    break;

                case R.id.kb_btn_abc_case:

                    this.changecasevalue();

                    break;

                case R.id.kb_tv_eq_name_basicmath:

                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_basicmath, kb_tv_eq_name_geometry, kb_tv_eq_name_prealgebra, kb_tv_eq_name_algebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {


                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_basicmath, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_trignometry,
                                    kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();

                        }
                    }, 500);

                    this.setvisibility_scroll_btn(false);

                    break;

                case R.id.kb_tv_eq_name_prealgebra:

                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_prealgebra, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_algebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler2 = new Handler();
                    handler2.postDelayed(new Runnable() {
                        @Override
                        public void run() {


                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_basicmath, kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_trignometry,
                                    kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();

                        }
                    }, 500);

                    this.setvisibility_scroll_btn(false);

                    break;

                case R.id.kb_tv_eq_name_algebra:

                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_algebra, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler3 = new Handler();
                    handler3.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath, kb_ll_eq_symbol_trignometry,
                                    kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;

                case R.id.kb_tv_eq_name_geometry:

                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_geometry, kb_tv_eq_name_algebra, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler33 = new Handler();
                    handler33.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath, kb_ll_eq_symbol_trignometry,
                                    kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;

                case R.id.kb_tv_eq_name_trignometry:
                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_trignometry, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_algebra,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler4 = new Handler();
                    handler4.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_trignometry, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath,
                                    kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;

                case R.id.kb_tv_eq_name_precalculus:
                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_precalculus, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_algebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_calculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler5 = new Handler();
                    handler5.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_trignometry, kb_ll_eq_symbol_algebra,
                                    kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath,
                                    kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;

                case R.id.kb_tv_eq_name_calculus:
                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_calculus, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_algebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler6 = new Handler();
                    handler6.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_trignometry,
                                    kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath,
                                    kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;

                case R.id.kb_tv_eq_name_statistics:
                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_statistics, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_algebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_finitemath);

                    Handler handler7 = new Handler();
                    handler7.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_precalculus,
                                    kb_ll_eq_symbol_trignometry, kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath,
                                    kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;

                case R.id.kb_tv_eq_name_finitemath:
                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_finitemath, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_algebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_statistics);

                    Handler handler8 = new Handler();
                    handler8.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_finitemath, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_calculus,
                                    kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_trignometry, kb_ll_eq_symbol_algebra,
                                    kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;


            }
            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);

        } catch (Exception e) {

        }
    }

    public void onkb_abc_click(View v) { // Added By Siddhi Info Soft

        try {

            String btn_tag = (String) v.getTag();
            this.changeCase(btn_tag);
            this.setTextToSelectedTextView(selectedTextView, btn_tag);
            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);

        } catch (Exception e) {

        }

    }

    public void onkb_123_click(View v) { // Added By Siddhi Info Soft

        try {

            String btn_tag = String.valueOf(v.getTag());
            this.setTextToSelectedTextView(selectedTextView, btn_tag);
            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);

        } catch (Exception e) {

        }

    }

    public void onkb_123_symbol_click(View v) { // Added By Siddhi Info Soft

        try {

            String btn_tag = String.valueOf(v.getTag());
            this.add_sign_into_box(btn_tag);
            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);

        } catch (Exception e) {

        }

    }

    /*public void onkb_sub_symbol_click(View v) { // Added By Siddhi Info Soft

        try {

            String btn_tag = String.valueOf(v.getTag());

            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);

            if (btn_tag.equals("fraction") ||
                    btn_tag.equals("pointview")
                    || btn_tag.equals("lim") || btn_tag.equals("parenthesis") || btn_tag.equals("absolute") | btn_tag.equals("squareroot") || btn_tag.equals("nthroot")
                    || btn_tag.equals("super_script") || btn_tag.equals("sub_script") || btn_tag.equals("super_sub_script")
                    || btn_tag.equals("line") || btn_tag.equals("lsegment") || btn_tag.equals("ray")
                    || btn_tag.equals("arc")) {
                this.addviewtoselectedview(ll_ans_box_selected, btn_tag, "view");
            } else if (btn_tag.equals("absolute")) {
                setTextToSelectedTextView(selectedTextView, "|  |");
            } else {
                this.add_sign_into_box(btn_tag);
            }

        } catch (Exception e) {

        }

    }*/

    private void add_sign_into_box(final String tag) {

        String str_text = "";
        boolean is_operator = false;

        if (tag.equals("op_plus")) {
            str_text = "" + Html.fromHtml("&#43;");
            is_operator = true;
        } else if (tag.equals("op_minus")) {
            str_text = "" + Html.fromHtml("&#8722;");
            is_operator = true;
        } else if (tag.equals("op_equal")) {
            str_text = "" + Html.fromHtml("&#61;");
            is_operator = true;
        } else if (tag.equals("op_multi")) {
            str_text = "" + Html.fromHtml("&#215;");
            is_operator = true;
        } else if (tag.equals("op_divide")) {
            str_text = "" + Html.fromHtml("&#247;");
            is_operator = true;
        } else if (tag.equals("op_union")) {
            str_text = "" + Html.fromHtml("&#8746;");
        } else if (tag.equals("op_intersection")) {
            str_text = "" + Html.fromHtml("&#8745;");
        } else if (tag.equals("op_pi")) {
            str_text = "" + Html.fromHtml("&#960;");
        } else if (tag.equals("op_factorial")) {
            str_text = "" + Html.fromHtml("&#33;");
        } else if (tag.equals("op_percentage")) {
            str_text = "" + Html.fromHtml("&#37;");
        } else if (tag.equals("op_rarrow")) {
            str_text = "" + Html.fromHtml("&#8594;");
        } else if (tag.equals("op_xbar")) {
            str_text = "" + Html.fromHtml("x&#x0304;");
        } else if (tag.equals("op_muxbar")) {
            str_text = "" + Html.fromHtml("&#956;") + "" + Html.fromHtml("x&#772;");
        } else if (tag.equals("op_sigmaxbar")) {
            str_text = "" + Html.fromHtml("&#963;") + "" + Html.fromHtml("x&#772;");
        } else if (tag.equals("op_goe")) {
            str_text = "" + Html.fromHtml("&#8805;");
            is_operator = true;
        } else if (tag.equals("op_loe")) {
            str_text = "" + Html.fromHtml("&#8804;");
            is_operator = true;
        } else if (tag.equals("op_grthan")) {
            str_text = "" + Html.fromHtml("&#62;");
            is_operator = true;
        } else if (tag.equals("op_lethan")) {
            str_text = "" + Html.fromHtml("&#60;");
            is_operator = true;
        } else if (tag.equals("op_infinity")) {
            str_text = "" + Html.fromHtml("&#8734;");
        } else if (tag.equals("op_degree")) {
            str_text = "" + Html.fromHtml("&#176;");
        } else if (tag.equals("op_integral")) {
            str_text = " " + Html.fromHtml("&#8747;");
        } else if (tag.equals("op_summation")) {
            str_text = " " + Html.fromHtml("&#8721;");
        } else if (tag.equals("op_alpha")) {
            str_text = " " + Html.fromHtml("&#945;");
            //  str_text = " "+Html.fromHtml("&#913;");
        } else if (tag.equals("op_theta")) {
            str_text = " ø";
        } else if (tag.equals("op_mu")) {
            str_text = " " + Html.fromHtml("&#956;");
        } else if (tag.equals("op_sigma")) {
            str_text = " " + Html.fromHtml("&#963;");
        } else if (tag.equals("op_beta")) {
            str_text = " " + Html.fromHtml("&#946;");
            //  str_text = " "+Html.fromHtml("&#7526;");
        } else if (tag.equals("op_angle")) {
            str_text = " " + Html.fromHtml("&#8736;");
        } else if (tag.equals("op_mangle")) {
            str_text = " " + Html.fromHtml("&#8737;");
        } else if (tag.equals("op_sangle")) {
            str_text = " " + Html.fromHtml("&#8738;");
        } else if (tag.equals("op_rangle")) {
            str_text = " " + Html.fromHtml("&#8735;");
        } else if (tag.equals("op_triangle")) {
            str_text = " " + Html.fromHtml("&#9651;");
        } else if (tag.equals("op_rectangle")) {
            str_text = " " + Html.fromHtml("&#9645;");
        } else if (tag.equals("op_parallelogram")) {
            str_text = " " + Html.fromHtml("&#9649;");
        } else if (tag.equals("op_line")) {
            str_text = " " + Html.fromHtml("&#963;");
        } else if (tag.equals("op_lsegment")) {
            str_text = " " + Html.fromHtml("&#963;");
        } else if (tag.equals("op_ray")) {
            str_text = " " + Html.fromHtml("&#963;");
        } else if (tag.equals("op_arc")) {
            str_text = " " + Html.fromHtml("&#963;");
        } else if (tag.equals("op_perpendicular")) {
            str_text = " " + Html.fromHtml("&#8869;");
        } else if (tag.equals("op_congruent")) {
            str_text = " " + Html.fromHtml("&#8773;");
        } else if (tag.equals("op_similarty")) {
            str_text = " " + Html.fromHtml("&#8764;");
        } else if (tag.equals("op_parallel")) {
            str_text = " " + Html.fromHtml("&#8741;");
        } else if (tag.equals("op_arcm")) {
            str_text = "" + Html.fromHtml("&#8242;");
        } else if (tag.equals("op_arcs")) {
            str_text = "" + Html.fromHtml("&#8243;");
        } else {
            str_text = tag;
        }

        if (is_operator) {
            setTextToSelectedTextView2(selectedTextView, " " + str_text + " ");
        } else {
            setTextToSelectedTextView2(selectedTextView, str_text);
        }

    }

    public void onkb_sum_symbol_click(View v) {

        try {

            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);

            String btn_tag = String.valueOf(v.getTag());
            String str_text = "";
            if (btn_tag.equals("op_integral")) {
                str_text = " " + Html.fromHtml("&#8747;");
            } else if (btn_tag.equals("op_summation")) {
                str_text = " " + Html.fromHtml("&#8721;");
            } else if (btn_tag.equals("op_perpendicular")) {
                str_text = " " + Html.fromHtml("&#8869;");
            } else if (btn_tag.equals("op_parallel")) {
                str_text = " " + Html.fromHtml("&#8741;");
            } else if (btn_tag.equals("op_rectangle")) {
                str_text = " " + Html.fromHtml("&#9645;");
            } else if (btn_tag.equals("op_parallelogram")) {
                str_text = " " + Html.fromHtml("&#9649;");
            } else if (btn_tag.equals("op_angle")) {
                str_text = " " + Html.fromHtml("&#8736;");
            } else if (btn_tag.equals("op_mangle")) {
                str_text = " " + Html.fromHtml("&#8737;");
            } else if (btn_tag.equals("op_sangle")) {
                str_text = " " + Html.fromHtml("&#8738;");
            } else if (btn_tag.equals("op_rangle")) {
                str_text = " " + Html.fromHtml("&#8735;");
            }

            if (btn_tag.equals("op_integral") || btn_tag.equals("op_summation")) {
                SpannableString ss1 = new SpannableString(str_text);
                ss1.setSpan(new RelativeSizeSpan(1.6f), 1, 2, 0);
                selectedTextView.getText().insert(selectedTextView.getSelectionStart(), ss1);
            } else if (btn_tag.equals("op_perpendicular") || btn_tag.equals("op_parallel") ||
                    btn_tag.equals("op_rectangle") || btn_tag.equals("op_parallelogram")) {
                SpannableString ss1 = new SpannableString(str_text);
                ss1.setSpan(new RelativeSizeSpan(1.2f), 1, 2, 0);
                selectedTextView.getText().insert(selectedTextView.getSelectionStart(), ss1);
            } else if (btn_tag.equals("op_angle") || btn_tag.equals("op_mangle") ||
                    btn_tag.equals("op_sangle") || btn_tag.equals("op_rangle")) {
                SpannableString ss1 = new SpannableString(str_text);
                ss1.setSpan(new RelativeSizeSpan(1.5f), 1, 2, 0);
                selectedTextView.getText().insert(selectedTextView.getSelectionStart(), ss1);
            }

            if (btn_tag.equals("op_integral")) {
                selectedTextView.getText().insert(selectedTextView.getSelectionStart(), " ");
            }


//            SpannableString ss1=  new SpannableString(str_text);
//            ss1.setSpan(new RelativeSizeSpan(2f), 1,2, 0);
//            selectedTextView.getText().insert(selectedTextView.getSelectionStart(), ss1);

        } catch (Exception e) {

        }

    }

    private void addnewed(final LinearLayout ll_main, final String str_cut, final int pos, final int text_size) {

//        String sss = String.valueOf(ll_main.getTag(R.id.first));
//        int tag1 = Integer.valueOf(sss);
//        String tag2 = String.valueOf(ll_main.getTag(R.id.second));
//        int text_size = 22;
//
//        try {
//
//            if (tag2.equals("super_top") || tag2.equals("sub_bottom") || tag2.equals("ss_top") ||
//                    tag2.equals("ss_bottom") || tag2.equals("nth_top") || tag2.equals("lim_left") || tag2.equals("lim_right")){
//                text_size = (text_size - 3)-(tag1+1);
//            } else if(tag2.equals("frac_top") || tag2.equals("frac_bottom") || tag2.equals("sqrt_c") || tag2.equals("nth_bottom")
//                    || tag2.equals("parenthesis_center")){
//                text_size = text_size-(tag1+1);
//            }
//
//        } catch (Exception e){
//
//        }

        final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
        ll_last.setTag("text");
        final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
        // ed_centre.setText(str_cut);
        this.replacesignfromchar(str_cut, ed_centre);
        ed_centre.setMinWidth(20);
        ed_centre.setTextSize(text_size);
        ll_last.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
            }
        });
        ed_centre.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
                return false;
            }
        });
        ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
                    selectedTextView = ed_centre;
                    ll_selected_view = ll_last;
                    selected_ed_2 = null;
                    ll_ans_box_selected = ll_main;
                }
            }
        });

        ll_main.addView(ll_last, (pos + 2));

        //   }

    }

    private void add_last_ed(final LinearLayout ll_main, final int textsize, final int pos) {

        String sss = String.valueOf(ll_main.getTag(R.id.first));
        int tag1 = Integer.valueOf(sss);
        String tag2 = String.valueOf(ll_main.getTag(R.id.second));
        int text_size = ActCheckHomeWork.this.getResources().getInteger(R.integer.kb_textsize_main_int);

        try {

            if (tag2.equals("super_top") || tag2.equals("sub_bottom") || tag2.equals("ss_top") ||
                    tag2.equals("ss_bottom") || tag2.equals("nth_top") || tag2.equals("lim_left") || tag2.equals("lim_right")) {
                text_size = (text_size - 1) - (tag1 + 1);
            } else if (tag2.equals("frac_top") || tag2.equals("frac_bottom") || tag2.equals("sqrt_c") || tag2.equals("nth_bottom")
                    || tag2.equals("parenthesis_center") || tag2.equals("absolute_center")
                    || tag2.equals("line_center") || tag2.equals("lsegment_center")
                    || tag2.equals("ray_center") || tag2.equals("arc_center")) {
                text_size = text_size - (tag1 + 1);
            }

        } catch (Exception e) {

        }
        if (text_size < 5) {
            text_size = 5;
        }

        final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
        ll_last.setTag("text");
        final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
        ed_centre.setMinWidth(3);
        ed_centre.setMinimumWidth(3);
        ed_centre.requestLayout();
        ed_centre.setTextSize(text_size);
        ll_last.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
            }
        });
        ed_centre.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
                return false;
            }
        });
        ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    selectedTextView = ed_centre;
                    ll_selected_view = ll_last;
                    selected_ed_2 = null;
                    ll_ans_box_selected = ll_main;
                }
            }
        });
        Log.e("delete6", "1234");
        ll_main.addView(ll_last, pos);
        Log.e("delete7", "1234");
        ed_centre.requestFocus();
    }

    private void add_ed_after_delete(final LinearLayout ll_main, final int textsize, final int pos) {

        String sss = String.valueOf(ll_main.getTag(R.id.first));
        int tag1 = Integer.valueOf(sss);
        String tag2 = String.valueOf(ll_main.getTag(R.id.second));
        int text_size = ActCheckHomeWork.this.getResources().getInteger(R.integer.kb_textsize_main_int);

        try {

            if (tag2.equals("super_top") || tag2.equals("sub_bottom") || tag2.equals("ss_top") ||
                    tag2.equals("ss_bottom") || tag2.equals("nth_top") || tag2.equals("lim_left") || tag2.equals("lim_right")) {
                text_size = (text_size - 1) - (tag1 + 1);
            } else if (tag2.equals("frac_top") || tag2.equals("frac_bottom") || tag2.equals("sqrt_c") || tag2.equals("nth_bottom")
                    || tag2.equals("parenthesis_center") || tag2.equals("absolute_center")
                    || tag2.equals("line_center") || tag2.equals("lsegment_center")
                    || tag2.equals("ray_center") || tag2.equals("arc_center")) {
                text_size = text_size - (tag1 + 1);
            }

        } catch (Exception e) {

        }
        if (text_size < 5) {
            text_size = 5;
        }
        final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
        ll_last.setTag("text");
        final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
        ed_centre.setMinWidth(20);
        ed_centre.setTextSize(text_size);
        ll_last.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
            }
        });
        ed_centre.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
                return false;
            }
        });
        ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                custom_ed_focus(ed_centre, ll_last, ll_main, hasFocus);
            }
        });
        //   Utils.change_edittext_bg(true, ed_centre);
        Log.e("edd2", "" + pos);
        ll_main.addView(ll_last, pos);
        ed_centre.requestFocus();

    }

    /*private void custom_ed_touch(final EditTextBlink ed, final LinearLayout ll_sub, final LinearLayout ll_main) {

        try {

            ed.setCursorVisible(true);
            selectedTextView = ed;
            ll_selected_view = ll_sub;
            ll_ans_box_selected = ll_main;
            selected_ed_2 = null;
            showKeyboard();

        } catch (Exception e) {
        }
    }*/

    private void add_inside_ed(final LinearLayout ll_main, final boolean req_focus, int text_size) {

        String tag2 = String.valueOf(ll_main.getTag(R.id.second));

        try {

            if (tag2.equals("super_top") || tag2.equals("sub_bottom") || tag2.equals("ss_top") ||
                    tag2.equals("ss_bottom") || tag2.equals("nth_top") || tag2.equals("lim_left") || tag2.equals("lim_right")) {
                // text_size = (text_size - 3)-(tag1+1);
                text_size = text_size - 3;
            } else if (tag2.equals("frac_top") || tag2.equals("frac_bottom") || tag2.equals("sqrt_c") || tag2.equals("nth_bottom")
                    || tag2.equals("parenthesis_center") || tag2.equals("absolute_center")
                    || tag2.equals("line_center") || tag2.equals("lsegment_center")
                    || tag2.equals("ray_center") || tag2.equals("arc_center")) {
                //  text_size = text_size-(tag1+1);
                text_size = text_size - 1;
            }

        } catch (Exception e) {
            text_size = 22;
        }

        Log.e("text_size", "" + text_size);
        final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
        ll_last.setTag("text");
        final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
        ed_centre.setMinWidth(25);
        ed_centre.setTextSize(text_size);
        ll_last.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
            }
        });
        ed_centre.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
                return false;
            }
        });
        ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                custom_ed_focus(ed_centre, ll_last, ll_main, hasFocus);
            }
        });
        Utils.change_edittext_bg(true, ed_centre);
        ll_main.addView(ll_last);

        Log.e("het", "" + ll_last.getHeight());
        ll_main.requestLayout();

        if (req_focus) {
            ed_centre.requestFocus();
        }

    }

    private void custom_ed_focus(final EditTextBlink ed, final LinearLayout ll_sub, final LinearLayout ll_main, final boolean hasFocus) {

        if (!(ll_main.getTag(R.id.second).equals("main"))) {
            Utils.change_edittext_bg(hasFocus, ed);
        }

        if (hasFocus) {
            selectedTextView = ed;
            ll_selected_view = ll_sub;
            selected_ed_2 = null;
            ll_ans_box_selected = ll_main;
        }

    }

    /*private void addviewtoselectedview(final LinearLayout ll_ans_box, String tag, String type) {  // Added By Siddhi Info Soft
        boolean is_devide = false;
        int start_pos = 0, ed_legnth = 0, ll_sub_selected_pos = 0;
        String cut_str = "";
        String sss = String.valueOf(ll_ans_box.getTag(R.id.first));
        int tag_main_ll = Integer.valueOf(sss);
        boolean need_delete = false;
        int text_size = ActCheckHomeWork.this.getResources().getInteger(R.integer.kb_textsize_main_int);

        try {
            DisplayMetrics metrics;
            metrics = getApplicationContext().getResources().getDisplayMetrics();
            float Textsize = selectedTextView.getTextSize() / metrics.density;
            text_size = Math.round(Textsize);
            Log.e("text_size", "" + text_size);
            if (text_size < 5) {
                text_size = 5;
            }

        } catch (Exception e) {
            text_size = 16;
        }

        try {

            start_pos = selectedTextView.getSelectionStart();
            ed_legnth = selectedTextView.getText().toString().length();

            try {
                ll_sub_selected_pos = ll_ans_box_selected.indexOfChild(ll_selected_view);
            } catch (Exception e) {
                ll_sub_selected_pos = 0;
            }

            try {
                if (ll_sub_selected_pos == -1) {
                    ll_sub_selected_pos = 0;
                }

                if (start_pos != 0 && start_pos < ed_legnth) {
                    cut_str = selectedTextView.getText().toString();
                    cut_str = cut_str.substring(start_pos, ed_legnth);
                    is_devide = true;
                    Log.e("math_ed_pos", cut_str + "" + ll_sub_selected_pos);
                    selectedTextView.getText().delete(start_pos, ed_legnth);
                }
            } catch (Exception e) {
            }

            try {
                if (selectedTextView.getText().toString().trim().length() == 0) {
                    need_delete = true;
                }
            } catch (Exception e) {
            }

            try {

                final LinearLayout ll_last = (LinearLayout) ll_ans_box.getChildAt((ll_ans_box.getChildCount() - 1));
                String str_tag = String.valueOf(ll_last.getTag());
                // Log.e("tagg",str_tag);
                if (str_tag.equals("text")) {
                    final EditTextBlink ed_last = (EditTextBlink) ll_last.getChildAt(0);
                    if (ed_last.getText().toString().trim().length() == 0) {

                        try {
                            if (ll_ans_box.getChildCount() > 1) {
                                LinearLayout ll_last_2 = (LinearLayout) ll_ans_box.getChildAt((ll_ans_box.getChildCount() - 2));
                                String str_tag_2 = String.valueOf(ll_last_2.getTag());
                                if (str_tag_2.equals("text")) {
                                    ll_ans_box.removeView(ll_last);
                                } else {
                                    ed_last.setMinWidth(4);
                                }
                            } else {
                                ed_last.setMinWidth(4);
                            }
                        } catch (Exception e) {
                        }
                    } else {
                        //  Log.e("tagg12", str_tag);
                        ed_last.setMinWidth(4);
                        ed_last.setMinimumWidth(4);
                        ed_last.requestLayout();
                        //  Log.e("tagg123", str_tag);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (type.equals("view")) {
                selected_ed_2 = null;
                if (tag.equals("super_script")) {

                    final LinearLayout ll_super_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_superscript, null);
                    ll_super_script.setTag("super_script");
                    final LinearLayout ll_top_superscript = (LinearLayout) ll_super_script.findViewById(R.id.ll_top_superscript);
                    ll_top_superscript.setTag(R.id.first, (tag_main_ll + 1));
                    ll_top_superscript.setTag(R.id.second, "super_top");

                    add_inside_ed(ll_top_superscript, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_super_script, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_super_script, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("sub_script")) {

                    final LinearLayout ll_sub_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_subscript, null);
                    ll_sub_script.setTag("sub_script");
                    final LinearLayout ll_bottom_subscript = (LinearLayout) ll_sub_script.findViewById(R.id.ll_bottom_subscript);
                    ll_bottom_subscript.setTag(R.id.first, (tag_main_ll + 1));
                    ll_bottom_subscript.setTag(R.id.second, "sub_bottom");
                    add_inside_ed(ll_bottom_subscript, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_sub_script, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_sub_script, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("super_sub_script")) {

                    final LinearLayout ll_super_sub_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_super_sub_script, null);
                    ll_super_sub_script.setTag("super_sub_script");

                    final LinearLayout ll_top_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_top_super_sub_script);
                    final LinearLayout ll_bottom_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_bottom_super_sub_script);
                    ll_top_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                    ll_bottom_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                    ll_top_super_sub_script.setTag(R.id.second, "ss_top");
                    ll_bottom_super_sub_script.setTag(R.id.second, "ss_bottom");
                    add_inside_ed(ll_top_super_sub_script, true, text_size);
                    add_inside_ed(ll_bottom_super_sub_script, false, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_super_sub_script, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_super_sub_script, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("fraction")) {

                    final LinearLayout ll_fraction = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_fraction, null);
                    final LinearLayout ll_fraction_top = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_top);
                    final LinearLayout ll_fraction_bottom = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_bottom);
                    ll_fraction_top.setTag(R.id.first, (tag_main_ll + 1));
                    ll_fraction_bottom.setTag(R.id.first, (tag_main_ll + 1));
                    ll_fraction_top.setTag(R.id.second, "frac_top");
                    ll_fraction_bottom.setTag(R.id.second, "frac_bottom");
                    add_inside_ed(ll_fraction_top, true, text_size);
                    add_inside_ed(ll_fraction_bottom, false, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_fraction, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_fraction, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }

                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("squareroot")) {

                    final LinearLayout ll_squareroot = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_square_root, null);
                    final LinearLayout ll_center_square_root = (LinearLayout) ll_squareroot.findViewById(R.id.ll_center_square_root);
                    ll_center_square_root.setTag(R.id.first, (tag_main_ll + 1));
                    ll_center_square_root.setTag(R.id.second, "sqrt_c");
                    add_inside_ed(ll_center_square_root, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_squareroot, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_squareroot, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("nthroot")) {

                    final LinearLayout ll_nthroot = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_nth_root, null);

                    final LinearLayout ll_top_nth_root = (LinearLayout) ll_nthroot.findViewById(R.id.ll_top_nth_root);
                    final LinearLayout ll_center_nth_root = (LinearLayout) ll_nthroot.findViewById(R.id.ll_center_nth_root);
                    final ImageView img = (ImageView) ll_nthroot.findViewById(R.id.img);
                    ll_top_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                    ll_center_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                    ll_top_nth_root.setTag(R.id.second, "nth_top");
                    ll_center_nth_root.setTag(R.id.second, "nth_bottom");
                    add_inside_ed(ll_top_nth_root, true, text_size);
                    add_inside_ed(ll_center_nth_root, false, text_size);
                    img.getLayoutParams().height = ll_center_nth_root.getHeight();
                    img.requestLayout();
                    ll_center_nth_root.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                        @Override
                        public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                            Log.e("het", "" + v.getHeight());
                            img.getLayoutParams().height = v.getHeight();
                            img.requestLayout();
                        }
                    });

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_nthroot, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_nthroot, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("lim")) {

                    final LinearLayout ll_lim = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_lim, null);
                    final TextView tv_text = (TextView) ll_lim.findViewById(R.id.tv_text);
                    tv_text.setText(Html.fromHtml("&#8594;"));
                    final LinearLayout ll_lim_left = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_left);
                    final LinearLayout ll_lim_right = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_right);
                    ll_lim_left.setTag(R.id.first, (tag_main_ll + 1));
                    ll_lim_right.setTag(R.id.first, (tag_main_ll + 1));
                    ll_lim_left.setTag(R.id.second, "lim_left");
                    ll_lim_right.setTag(R.id.second, "lim_right");
                    add_inside_ed(ll_lim_left, true, text_size);
                    add_inside_ed(ll_lim_right, false, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_lim, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_lim, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }

                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("parenthesis")) {

                    final LinearLayout ll_parenthesis = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_parenthesis, null);
                    ll_parenthesis.setTag("parenthesis");
                    final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_parenthesis_center);
                    ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                    ll_parenthesis_center.setTag(R.id.second, "parenthesis_center");
                    add_inside_ed(ll_parenthesis_center, true, text_size);
                    final ImageView img_left = (ImageView) ll_parenthesis.findViewById(R.id.img_left);
                    final ImageView img_rht = (ImageView) ll_parenthesis.findViewById(R.id.img_rht);

                    img_left.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final LinearLayout lll = (LinearLayout) ll_parenthesis_center.getChildAt(0);
                            if (lll.getTag().equals("text")) {
                                final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                edd.setSelection(0);
                                edd.requestFocus();
                            } else {
                                add_last_ed(ll_parenthesis_center, 22, 0);
                            }
                        }
                    });

                    img_rht.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final LinearLayout lll = (LinearLayout) ll_parenthesis_center.getChildAt((ll_parenthesis_center.getChildCount() - 1));
                            if (lll.getTag().equals("text")) {
                                final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                edd.setSelection(edd.getText().toString().length());
                                edd.requestFocus();
                            } else {
                                add_last_ed(ll_parenthesis_center, 22, (ll_parenthesis_center.getChildCount()));
                            }
                        }
                    });

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_parenthesis, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_parenthesis, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("absolute")) {

                    final LinearLayout ll_parenthesis = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_absolute, null);
                    ll_parenthesis.setTag("absolute");
                    final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_absolute_center);
                    ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                    ll_parenthesis_center.setTag(R.id.second, "absolute_center");
                    add_inside_ed(ll_parenthesis_center, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_parenthesis, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_parenthesis, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("line") || tag.equals("lsegment")
                        || tag.equals("ray") || tag.equals("arc")) {

                    final LinearLayout ll_parenthesis = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_lsegment, null);

                    final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_lsegment_center);
                    final ImageView img_top = (ImageView) ll_parenthesis.findViewById(R.id.img_top);
                    ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                    if (tag.equals("line")) {
                        img_top.setImageResource(R.drawable.line_icon);
                        ll_parenthesis.setTag("line");
                        ll_parenthesis_center.setTag(R.id.second, "line_center");
                    } else if (tag.equals("lsegment")) {
                        img_top.setImageResource(R.drawable.line_segment);
                        ll_parenthesis.setTag("lsegment");
                        ll_parenthesis_center.setTag(R.id.second, "lsegment_center");
                    } else if (tag.equals("ray")) {
                        img_top.setImageResource(R.drawable.ray_icon);
                        ll_parenthesis.setTag("ray");
                        ll_parenthesis_center.setTag(R.id.second, "ray_center");
                    } else if (tag.equals("arc")) {
                        img_top.setImageResource(R.drawable.arc_icon);
                        ll_parenthesis.setTag("arc");
                        ll_parenthesis_center.setTag(R.id.second, "arc_center");
                    }

                    add_inside_ed(ll_parenthesis_center, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_parenthesis, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_parenthesis, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                }
            }

            LinearLayout ll_last_2 = (LinearLayout) ll_ans_box.getChildAt((ll_ans_box.getChildCount() - 1));
            String str_tag = String.valueOf(ll_last_2.getTag());
            if (str_tag.equals("text")) {
                EditTextBlink ed_last = (EditTextBlink) ll_last_2.getChildAt(0);
                ed_last.setMinWidth(5);

            } else {

                final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                ll_last.setTag("text");
                final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                ed_centre.setTextSize(text_size);
                ed_centre.setMinWidth(4);
                ed_centre.setMinimumWidth(4);
                ed_centre.requestLayout();
                ll_last.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ed_centre.setCursorVisible(true);
                        selectedTextView = ed_centre;
                        ll_selected_view = ll_last;
                        ll_ans_box_selected = ll_ans_box;
                        selected_ed_2 = null;
                        showKeyboard();
                    }
                });
                ed_centre.setOnTouchListener(new OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        ed_centre.setCursorVisible(true);
                        selectedTextView = ed_centre;
                        ll_selected_view = ll_last;
                        ll_ans_box_selected = ll_ans_box;
                        selected_ed_2 = null;
                        showKeyboard();
                        return false;
                    }
                });
                ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            selectedTextView = ed_centre;
                            ll_selected_view = ll_last;
                            selected_ed_2 = null;
                            ll_ans_box_selected = ll_ans_box;
                        }
                    }
                });

                ll_ans_box.addView(ll_last);

            }

        } catch (Exception e) {

        }
    }*/

    public void setstudentansboxsub(final String str_ans, final LinearLayout ll_ans_box) {

        String[] str_1 = str_ans.split(ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first));

        String sss = String.valueOf(ll_ans_box.getTag(R.id.first));
        int tag_main_ll = Integer.valueOf(sss);

        for (int i = 0; i < str_1.length; i++) {

            String str_1_value = str_1[i];

            if (str_1_value.startsWith("text")) {

                str_1_value = str_1_value.substring(5);

                String sss1 = String.valueOf(ll_ans_box.getTag(R.id.first));
                int tag1 = Integer.valueOf(sss1);
                final String tag2 = String.valueOf(ll_ans_box.getTag(R.id.second));
                int text_size = ActCheckHomeWork.this.getResources().getInteger(R.integer.kb_textsize_main_int);

                try {

                    if (tag2.equals("super_top") || tag2.equals("sub_bottom") || tag2.equals("ss_top") ||
                            tag2.equals("ss_bottom") || tag2.equals("nth_top") || tag2.equals("lim_left") || tag2.equals("lim_right")) {
                        text_size = (text_size - 1) - (tag1 + 1);
                    } else if (tag2.equals("frac_top") || tag2.equals("frac_bottom") || tag2.equals("sqrt_c") || tag2.equals("nth_bottom")
                            || tag2.equals("parenthesis_center") || tag2.equals("absolute_center")
                            || tag2.equals("line_center") || tag2.equals("lsegment_center")
                            || tag2.equals("ray_center") || tag2.equals("arc_center")) {
                        text_size = text_size - (tag1 + 1);
                    }

                } catch (Exception e) {

                }

                final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                ll_last.setTag("text");
                final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                this.replacesignfromchar(str_1_value, ed_centre);
                ed_centre.setMinWidth(20);
                ed_centre.setTextSize(text_size);
                ll_last.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        custom_ed_touch(ed_centre, ll_last, ll_ans_box);
                    }
                });
                ed_centre.setOnTouchListener(new OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        custom_ed_touch(ed_centre, ll_last, ll_ans_box);
                        return false;
                    }
                });
                ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (!(tag2.equals("main"))) {
                            Utils.change_edittext_bg(hasFocus, ed_centre);
                        }
                        if (hasFocus) {
                            selectedTextView = ed_centre;
                            ll_selected_view = ll_last;
                            ll_ans_box_selected = ll_ans_box;
                        }
                    }
                });

                ll_ans_box.addView(ll_last);

            }
            if (str_1_value.startsWith("fraction")) {

                str_1_value = str_1_value.substring(8);

                final LinearLayout ll_fraction = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_fraction, null);
                final LinearLayout ll_fraction_top = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_top);
                final LinearLayout ll_fraction_bottom = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_bottom);
                ll_fraction_top.setTag(R.id.first, (tag_main_ll + 1));
                ll_fraction_bottom.setTag(R.id.first, (tag_main_ll + 1));
                ll_fraction_top.setTag(R.id.second, "frac_top");
                ll_fraction_bottom.setTag(R.id.second, "frac_bottom");

                final String[] str_2 = str_1_value.split("frac_btm_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_fraction);

                this.setstudentansboxsub(str_2[0], ll_fraction_top);
                this.setstudentansboxsub(str_2[1], ll_fraction_bottom);

            } else if (str_1_value.startsWith("super_script")) {
                str_1_value = str_1_value.substring(12);

                final LinearLayout ll_super_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_superscript, null);
                final LinearLayout ll_top_superscript = (LinearLayout) ll_super_script.findViewById(R.id.ll_top_superscript);
                ll_top_superscript.setTag(R.id.first, (tag_main_ll + 1));
                ll_top_superscript.setTag(R.id.second, "super_top");

                ll_ans_box.addView(ll_super_script);

                this.setstudentansboxsub(str_1_value, ll_top_superscript);

            } else if (str_1_value.startsWith("sub_script")) {
                str_1_value = str_1_value.substring(10);

                final LinearLayout ll_super_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_subscript, null);
                final LinearLayout ll_bottom_subscript = (LinearLayout) ll_super_script.findViewById(R.id.ll_bottom_subscript);
                ll_bottom_subscript.setTag(R.id.first, (tag_main_ll + 1));
                ll_bottom_subscript.setTag(R.id.second, "sub_bottom");

                ll_ans_box.addView(ll_super_script);

                this.setstudentansboxsub(str_1_value, ll_bottom_subscript);

            } else if (str_1_value.startsWith("super_sub_script")) {

                str_1_value = str_1_value.substring(16);

                final LinearLayout ll_super_sub_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_super_sub_script, null);
                final LinearLayout ll_top_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_top_super_sub_script);
                final LinearLayout ll_bottom_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_bottom_super_sub_script);
                ll_top_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                ll_bottom_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                ll_top_super_sub_script.setTag(R.id.second, "ss_top");
                ll_bottom_super_sub_script.setTag(R.id.second, "ss_bottom");

                final String[] str_2 = str_1_value.split("ss_btm_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_super_sub_script);

                this.setstudentansboxsub(str_2[0], ll_top_super_sub_script);
                this.setstudentansboxsub(str_2[1], ll_bottom_super_sub_script);

            } else if (str_1_value.startsWith("sqrt")) {

                str_1_value = str_1_value.substring(4);
                final LinearLayout ll_sqrt = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_square_root, null);
                final LinearLayout ll_center_square_root = (LinearLayout) ll_sqrt.findViewById(R.id.ll_center_square_root);
                ll_center_square_root.setTag(R.id.first, (tag_main_ll + 1));
                ll_center_square_root.setTag(R.id.second, "sqrt_c");

                ll_ans_box.addView(ll_sqrt);

                this.setstudentansboxsub(str_1_value, ll_center_square_root);

            } else if (str_1_value.startsWith("nsqrt")) {

                str_1_value = str_1_value.substring(5);

                final LinearLayout ll_nsqrt = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_nth_root, null);
                final LinearLayout ll_top_nth_root = (LinearLayout) ll_nsqrt.findViewById(R.id.ll_top_nth_root);
                final LinearLayout ll_center_nth_root = (LinearLayout) ll_nsqrt.findViewById(R.id.ll_center_nth_root);
                final ImageView img = (ImageView) ll_nsqrt.findViewById(R.id.img);
                ll_top_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                ll_center_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                ll_top_nth_root.setTag(R.id.second, "nth_top");
                ll_center_nth_root.setTag(R.id.second, "nth_bottom");

                img.getLayoutParams().height = ll_center_nth_root.getHeight();
                img.requestLayout();
                ll_center_nth_root.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                    @Override
                    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                        Log.e("het", "" + v.getHeight());
                        img.getLayoutParams().height = v.getHeight();
                        img.requestLayout();
                    }
                });

                final String[] str_2 = str_1_value.split("nsqrt_center_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_nsqrt);

                this.setstudentansboxsub(str_2[0], ll_top_nth_root);
                this.setstudentansboxsub(str_2[1], ll_center_nth_root);

            } else if (str_1_value.startsWith("lim")) {

                str_1_value = str_1_value.substring(3);

                final LinearLayout ll_lim = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_lim, null);
                final TextView tv_text = (TextView) ll_lim.findViewById(R.id.tv_text);
                tv_text.setText(Html.fromHtml("&#8594;"));
                final LinearLayout ll_lim_left = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_left);
                final LinearLayout ll_lim_right = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_right);
                ll_lim_left.setTag(R.id.first, (tag_main_ll + 1));
                ll_lim_right.setTag(R.id.first, (tag_main_ll + 1));
                ll_lim_left.setTag(R.id.second, "lim_left");
                ll_lim_right.setTag(R.id.second, "lim_right");

                final String[] str_2 = str_1_value.split("lim_rht_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_lim);

                this.setstudentansboxsub(str_2[0], ll_lim_left);
                this.setstudentansboxsub(str_2[1], ll_lim_right);

            } else if (str_1_value.startsWith("parenthesis")) {
                str_1_value = str_1_value.substring(11);

                final LinearLayout ll_parenthesis = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_parenthesis, null);
                final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_parenthesis_center);
                ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                ll_parenthesis_center.setTag(R.id.second, "parenthesis_center");

                final ImageView img_left = (ImageView) ll_parenthesis.findViewById(R.id.img_left);
                final ImageView img_rht = (ImageView) ll_parenthesis.findViewById(R.id.img_rht);

                img_left.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final LinearLayout lll = (LinearLayout) ll_parenthesis_center.getChildAt(0);
                        if (lll.getTag().equals("text")) {
                            final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                            edd.setSelection(0);
                            edd.requestFocus();
                        } else {
                            add_last_ed(ll_parenthesis_center, 22, 0);
                        }
                    }
                });

                img_rht.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final LinearLayout lll = (LinearLayout) ll_parenthesis_center.getChildAt((ll_parenthesis_center.getChildCount() - 1));
                        if (lll.getTag().equals("text")) {
                            final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                            edd.setSelection(edd.getText().toString().length());
                            edd.requestFocus();
                        } else {
                            add_last_ed(ll_parenthesis_center, 22, (ll_parenthesis_center.getChildCount()));
                        }
                    }
                });

                ll_ans_box.addView(ll_parenthesis);

                this.setstudentansboxsub(str_1_value, ll_parenthesis_center);

            } else if (str_1_value.startsWith("absolute")) {
                str_1_value = str_1_value.substring(8);

                final LinearLayout ll_super_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_absolute, null);
                final LinearLayout ll_parenthesis_center = (LinearLayout) ll_super_script.findViewById(R.id.ll_absolute_center);
                ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                ll_parenthesis_center.setTag(R.id.second, "absolute_center");

                ll_ans_box.addView(ll_super_script);

                this.setstudentansboxsub(str_1_value, ll_parenthesis_center);

            } else if (str_1_value.startsWith("line") || str_1_value.startsWith("lsegment") || str_1_value.startsWith("ray")
                    || str_1_value.startsWith("arc")) {

                final LinearLayout ll_parenthesis = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_lsegment, null);
                final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_lsegment_center);
                ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                final ImageView img_top = (ImageView) ll_parenthesis.findViewById(R.id.img_top);
                // ll_parenthesis_center.setTag(R.id.second,"absolute_center");

                if (str_1_value.startsWith("line")) {
                    img_top.setImageResource(R.drawable.line_icon);
                    str_1_value = str_1_value.substring(4);
                    ll_parenthesis.setTag("line");
                    ll_parenthesis_center.setTag(R.id.second, "line_center");
                } else if (str_1_value.startsWith("lsegment")) {
                    str_1_value = str_1_value.substring(8);
                    img_top.setImageResource(R.drawable.line_segment);
                    ll_parenthesis.setTag("lsegment");
                    ll_parenthesis_center.setTag(R.id.second, "lsegment_center");
                } else if (str_1_value.startsWith("ray")) {
                    str_1_value = str_1_value.substring(3);
                    img_top.setImageResource(R.drawable.ray_icon);
                    ll_parenthesis.setTag("ray");
                    ll_parenthesis_center.setTag(R.id.second, "ray_center");
                } else if (str_1_value.startsWith("arc")) {
                    str_1_value = str_1_value.substring(3);
                    img_top.setImageResource(R.drawable.arc_icon);
                    ll_parenthesis.setTag("arc");
                    ll_parenthesis_center.setTag(R.id.second, "arc_center");
                }

                ll_ans_box.addView(ll_parenthesis);

                this.setstudentansboxsub(str_1_value, ll_parenthesis_center);

            }

        }
    }

    public void setstudentansbox(final CustomeAns customeAns, final LinearLayout ll_ans_box, final
    CustomePlayerAns playerAns) {
        try {

            if (customeAns.getIsAnswerAvailable() == MathFriendzyHelper.NO) {

                final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_textview, null);
                ll_last.setTag("vtext");
                final TextView tv = (TextView) ll_last.findViewById(R.id.tv);
                tv.setText(lblAnswerOnWorkAreaCreditGiven);

                tv.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MathFriendzyHelper.showWarningDialog(ActCheckHomeWork.this,
                                lblAnswerOnWorkAreaCreditGiven);
                    }
                });

                ll_ans_box.addView(ll_last);

            } else {

                if (playerAns != null) {

                    String str_ans = playerAns.getAns();

                    Log.e("stdans", str_ans);

                    if (str_ans.trim().length() != 0) {
                        setstudentansboxsub(str_ans, ll_ans_box);
                    }
                }

                Log.e("stdansC", customeAns.getCorrectAns());

                boolean is_ed_last = false;

                if (ll_ans_box.getChildCount() > 0) {
                    LinearLayout lt = (LinearLayout) ll_ans_box.getChildAt(ll_ans_box.getChildCount() - 1);
                    if (lt.getTag().toString().equals("text")) {
                        is_ed_last = true;
                    } else {
                        is_ed_last = false;
                    }
                } else {
                    is_ed_last = false;
                }

                if (!is_ed_last) {

                    final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                    ll_last.setTag("text");
                    final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                    ed_centre.setMinWidth(20);
                    ll_last.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            custom_ed_touch(ed_centre, ll_last, ll_ans_box);
                        }
                    });
                    ed_centre.setOnTouchListener(new OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            custom_ed_touch(ed_centre, ll_last, ll_ans_box);
                            return false;
                        }
                    });
                    ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                selectedTextView = ed_centre;
                                ll_selected_view = ll_last;
                                ll_ans_box_selected = ll_ans_box;
                            }
                        }
                    });
                    ll_ans_box.addView(ll_last);
                }
            }

        } catch (Exception e) {

        }
    }


    public void onnextsubcall(final LinearLayout ll_sub) {

        if (ll_sub.getTag().toString().equals("text")) {

            EditTextBlink ed = (EditTextBlink) ll_sub.getChildAt(0);
            ed.requestFocus();
            ed.setSelection(0);

        } else if (ll_sub.getTag().toString().equals("fraction") || ll_sub.getTag().toString().equals("super_script") || ll_sub.getTag().toString().equals("super_sub_script")
                || ll_sub.getTag().toString().equals("nsqrt")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(0);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, 0);
            } else {
                this.onnextsubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("sub_script")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, 0);
            } else {
                this.onnextsubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("parenthesis") || ll_sub.getTag().toString().equals("absolute") ||
                ll_sub.getTag().toString().equals("line") || ll_sub.getTag().toString().equals("lsegment")
                || ll_sub.getTag().toString().equals("ray") || ll_sub.getTag().toString().equals("arc")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, 0);
            } else {
                this.onnextsubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("sqrt")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt(0);
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_2, 22, 0);
            } else {
                this.onnextsubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("lim")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(0);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt(0);
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_2, 22, 0);
            } else {
                this.onnextsubcall(ll_sub_1);
            }

        }

    }

    /*public void onnextclick() {

        try {
            int cur_pos = 0, ed_legnth = 0;

            try {
                cur_pos = selectedTextView.getSelectionStart();
                ed_legnth = selectedTextView.getText().toString().length();
            } catch (Exception e) {
            }

            if ((cur_pos != ed_legnth)) {

                String str_bar_1 = "" + Html.fromHtml("&#x0304;");
                String str_bar_2 = "" + Html.fromHtml("&#772;");

                if (selectedTextView.getText().toString().length() > 1) {
                    String ss = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos));
                    if (ss.equals("x")) {
                        if ((cur_pos + 1) < ed_legnth) {
                            String ss_1 = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos + 1));
                            if (ss_1.equals(str_bar_1) || ss_1.equals(str_bar_2)) {
                                selectedTextView.setSelection((cur_pos + 2));
                            } else {
                                selectedTextView.setSelection((cur_pos + 1));
                            }
                        } else {
                            selectedTextView.setSelection((cur_pos + 1));
                        }
                    } else {
                        selectedTextView.setSelection((cur_pos + 1));
                    }
                } else {
                    selectedTextView.setSelection((cur_pos + 1));
                }

                if (is_touch_kb_next) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onnextclick();
                        }
                    }, 150);
                }

            } else {
                final LinearLayout ll_main_ll = ll_ans_box_selected;
                final LinearLayout ll_sub_ll = ll_selected_view;

                int pos_sub_ll = ll_main_ll.indexOfChild(ll_sub_ll);

                if (ll_main_ll.getChildCount() > 1 && pos_sub_ll < (ll_main_ll.getChildCount() - 1)) {

                    LinearLayout ll_sub = (LinearLayout) ll_main_ll.getChildAt(pos_sub_ll + 1);
                    onnextsubcall(ll_sub);

                } else if (pos_sub_ll == (ll_main_ll.getChildCount() - 1)) {

                    String main_tag = (String) ll_main_ll.getTag(R.id.second);

                    if (main_tag.equals("frac_top")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("ss_top")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(1);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("nth_top")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("lim_left")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("sqrt_c") || main_tag.equals("nth_bottom") || main_tag.equals("lim_right")) {

                        sub_next_sqrt_c(ll_main_ll);

                    } else if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom") ||
                            main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                            || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                            || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {

                        sub_next_frac_bottom(ll_main_ll);

                    }
                }

                if (is_touch_kb_next) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onnextclick();
                        }
                    }, 150);
                }
            }

        } catch (Exception e) {
        }
    }*/

    public void sub_next_sqrt_c(final LinearLayout ll_main_ll) {
        try {

            try {

                final LinearLayout ll_mainm = (LinearLayout) ll_main_ll.getParent();
                final LinearLayout ll_main_1m = (LinearLayout) ll_mainm.getParent();
                final LinearLayout ll_main2 = (LinearLayout) ll_main_1m.getParent();
                int main_pos = ll_main2.indexOfChild(ll_main_1m);

                final String main_tag = String.valueOf(ll_main2.getTag(R.id.second));
                if (main_pos == (ll_main2.getChildCount() - 1)) {
                    add_last_ed(ll_main2, 22, main_pos + 1);
                } else if (ll_main2.getChildCount() > 1 && main_pos < (ll_main2.getChildCount() - 1)) {
                    final LinearLayout ll_sub = (LinearLayout) ll_main2.getChildAt(main_pos + 1);
                    if (!(ll_sub.getTag().equals("text"))) {
                        add_last_ed(ll_main2, 22, main_pos + 1);
                    } else {
                        onnextsubcall(ll_sub);
                    }
                } else {

                    if (main_tag.equals("sqrt_c") || main_tag.equals("nth_bottom") || main_tag.equals("lim_right")) {
                        sub_next_sqrt_c(ll_main2);
                    } else if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")
                            || main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                            || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                            || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {
                        sub_next_frac_bottom(ll_main2);
                    } else if (main_tag.equals("frac_top")) {
                        final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                        final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        final LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);
                    } else if (main_tag.equals("ss_top")) {
                        final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                        final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(1);
                        final LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);
                    } else if (main_tag.equals("nth_top")) {
                        final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                        final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        final LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
                        final LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt(0);
                        onnextsubcall(ll_sub_1);
                    } else if (main_tag.equals("lim_left")) {

                        LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("main")) {
                        add_last_ed(ll_main2, 18, ll_main2.getChildCount());
                    }
                }

            } catch (Exception e) {

            }

        } catch (Exception e) {
        }
    }

    public void sub_next_frac_bottom(final LinearLayout ll_main_ll) {
        try {
            final LinearLayout ll_mainm = (LinearLayout) ll_main_ll.getParent();
            final LinearLayout ll_main2 = (LinearLayout) ll_mainm.getParent();
            int main_pos = ll_main2.indexOfChild(ll_mainm);
            final String main_tag = String.valueOf(ll_main2.getTag(R.id.second));

            if (main_pos == (ll_main2.getChildCount() - 1)) {
                add_last_ed(ll_main2, 22, main_pos + 1);
            } else if (ll_main2.getChildCount() > 1 && main_pos < (ll_main2.getChildCount() - 1)) {
                final LinearLayout ll_sub = (LinearLayout) ll_main2.getChildAt(main_pos + 1);
                if (!(ll_sub.getTag().equals("text"))) {
                    add_last_ed(ll_main2, 22, main_pos + 1);
                } else {
                    onnextsubcall(ll_sub);
                }

            } else {
                if (main_tag.equals("sqrt_c") || main_tag.equals("nth_bottom") || main_tag.equals("lim_right")) {
                    sub_next_sqrt_c(ll_main2);
                } else if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")
                        || main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                        || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                        || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {
                    sub_next_frac_bottom(ll_main2);
                } else if (main_tag.equals("frac_top")) {
                    final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                    final LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                    onnextsubcall(ll_sub_1);
                } else if (main_tag.equals("ss_top")) {
                    final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(1);
                    final LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                    onnextsubcall(ll_sub_1);
                } else if (main_tag.equals("nth_top")) {
                    final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                    final LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
                    final LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt(0);
                    onnextsubcall(ll_sub_1);
                } else if (main_tag.equals("lim_left")) {

                    LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                    LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                    onnextsubcall(ll_sub_1);

                } else if (main_tag.equals("main")) {
                    add_last_ed(ll_main2, 22, ll_main2.getChildCount());
                }

            }
        } catch (Exception e) {
        }
    }

    public void onpresubcall(final LinearLayout ll_sub) {

        if (ll_sub.getTag().toString().equals("text")) {

            EditTextBlink ed = (EditTextBlink) ll_sub.getChildAt(0);
            ed.setSelection(ed.getText().toString().length());
            ed.requestFocus();

        } else if (ll_sub.getTag().toString().equals("sub_script")
                ) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, ll_main_1.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("super_script")
                ) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(0);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, ll_main_1.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("fraction")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(2);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, ll_main_1.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("parenthesis") || ll_sub.getTag().toString().equals("absolute")
                || ll_sub.getTag().toString().equals("line") || ll_sub.getTag().toString().equals("lsegment")
                || ll_sub.getTag().toString().equals("ray") || ll_sub.getTag().toString().equals("arc")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, ll_main_1.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("super_sub_script")) {
            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, ll_main_1.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("sqrt")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_2, 22, ll_main_2.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("nsqrt")) {
            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(2);
            LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_2, 22, ll_main_2.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("lim")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(2);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_2, 22, ll_main_2.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        }

    }

    /*public void ondeleteclick() {

        final LinearLayout ll_main_ll = ll_ans_box_selected;
        final LinearLayout ll_sub_ll = ll_selected_view;

        try {
            int ed_pos = 0, ed_legnth = 0;
            try {
                ed_pos = selectedTextView.getSelectionStart();
                ed_legnth = selectedTextView.getText().toString().trim().length();
            } catch (Exception e) {
            }
            if (ed_pos > 0 && ed_legnth > 0) {
                String str_bar_1 = "" + Html.fromHtml("&#x0304;");
                String str_bar_2 = "" + Html.fromHtml("&#772;");
                String str_integral = "" + Html.fromHtml("&#8747;");
                if (selectedTextView.getText().toString().length() > 1) {
                    String ss = String.valueOf(selectedTextView.getText().toString().charAt(ed_pos - 1));
                    if (ss.equals(str_bar_1) || ss.equals(str_bar_2)) {

                        selectedTextView.getText().delete(ed_pos - 2, ed_pos);

                    } else if (ss.equals("x")) {
                        if (ed_pos < ed_legnth) {
                            String ss_2 = String.valueOf(selectedTextView.getText().toString().charAt(ed_pos));
                            if (ss_2.equals(str_bar_1) || ss_2.equals(str_bar_2)) {
                                selectedTextView.getText().delete(ed_pos - 1, ed_pos + 1);

                            } else {
                                selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                            }
                        } else {
                            selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                        }
                    } else if (ss.equals(" ")) {
                        try {
                            String ss2 = String.valueOf(selectedTextView.getText().toString().charAt(ed_pos - 2));
                            if (ss2.equals(str_integral)) {
                                selectedTextView.getText().delete(ed_pos - 2, ed_pos);
                            } else {
                                selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                            }
                        } catch (Exception e) {
                            selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                        }
                    } else {
                        selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                    }
                } else {
                    selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                }
                try {
                    if (selectedTextView.getText().toString().trim().length() == 0) {
                        selectedTextView.setSelection(0);
                    }
                } catch (Exception e) {
                }

                if (is_touch_kb) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ondeleteclick();
                        }
                    }, 150);
                }

            } else if (ed_pos == 0 && ed_legnth == 0) {

                int pos_sub_ll = ll_main_ll.indexOfChild(ll_sub_ll);
                Log.e("delete4", "" + pos_sub_ll);

                if (pos_sub_ll != 0 && pos_sub_ll != -1) {

                    LinearLayout ll_sub = (LinearLayout) ll_main_ll.getChildAt(pos_sub_ll - 1);
                    onpresubcall(ll_sub);
                    ll_main_ll.removeViewAt(pos_sub_ll);

                } else if (pos_sub_ll == 0) {

                    String main_tag = (String) ll_main_ll.getTag(R.id.second);

                    Log.e("delete5", "" + main_tag);

                    if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                        if (ll_main_ll.getChildCount() > 1) {
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        }

                    } else if (main_tag.equals("nth_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                        if (ll_main_ll.getChildCount() > 1) {
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        }

                    } else if (main_tag.equals("lim_right")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                        if (ll_main_ll.getChildCount() > 1) {
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        }

                    } else if (main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                            || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                            || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {

                        try {
                            LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            LinearLayout ll_main2 = (LinearLayout) ll_main.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_frac_top(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {

                                if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                    sub_pre_frac_top(ll_main_ll, 2, false);
                                    ll_main2.removeViewAt(main_pos);
                                    this.add_ed_after_delete(ll_main2, 18, main_pos);
                                } else {
                                    if (main_pos != 0) {
                                        sub_pre_frac_top(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                    } else {
                                        sub_pre_frac_top(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                        try {
                                            final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                            if (lll.getTag().toString().equals("text")) {
                                                EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                edd.setMinWidth(20);
                                                edd.setMinimumWidth(20);
                                                edd.requestLayout();
                                                edd.requestFocus();
                                                if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                    Utils.change_edittext_bg(true, edd);
                                                }
                                            }
                                        } catch (Exception e) {
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("sqrt_c")) {

                        try {

                            final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            final LinearLayout ll_main1 = (LinearLayout) ll_main.getParent();
                            final LinearLayout ll_main2 = (LinearLayout) ll_main1.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main1);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_sqrt_c(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {
                                if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                    Log.e("edd", "" + main_pos);
                                    sub_pre_sqrt_c(ll_main_ll, 2, false);
                                    ll_main2.removeViewAt(main_pos);
                                    Log.e("edd1", "" + main_pos);
                                    this.add_ed_after_delete(ll_main2, 18, main_pos);
                                } else {
                                    if (main_pos != 0) {
                                        sub_pre_sqrt_c(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                    } else {
                                        sub_pre_sqrt_c(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                        try {
                                            final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                            if (lll.getTag().toString().equals("text")) {
                                                final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                edd.setMinWidth(20);
                                                edd.setMinimumWidth(20);
                                                edd.requestLayout();
                                                edd.requestFocus();
                                                if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                    Utils.change_edittext_bg(true, edd);
                                                }
                                            }
                                        } catch (Exception e) {
                                        }
                                    }
                                }
                            }

                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("nth_top")) {

                        try {
                            final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            final LinearLayout ll_main2 = (LinearLayout) ll_main.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_frac_top(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {
                                final LinearLayout ll_sqrt = (LinearLayout) ll_main.getChildAt(2);
                                final LinearLayout ll_sqrt2 = (LinearLayout) ll_sqrt.getChildAt(1);
                                if (this.makeansfromview(ll_sqrt2).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_frac_top(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            }

                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("lim_left")) {

                        try {
                            final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            final LinearLayout ll_main1 = (LinearLayout) ll_main.getParent();
                            final LinearLayout ll_main2 = (LinearLayout) ll_main1.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main1);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_sqrt_c(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {
                                final LinearLayout ll_lim_rht = (LinearLayout) ll_main.getChildAt(2);
                                if (this.makeansfromview(ll_lim_rht).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_sqrt_c(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_sqrt_c(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_sqrt_c(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            }

                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("frac_top") || main_tag.equals("ss_top")) {

                        final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        final LinearLayout ll_main2 = (LinearLayout) ll_main.getParent();
                        int main_pos = ll_main2.indexOfChild(ll_main);

                        if (ll_main_ll.getChildCount() > 1) {
                            sub_pre_frac_top(ll_main_ll, 0, false);
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        } else {
                            if (main_tag.equals("frac_top")) {
                                LinearLayout ll_frac = (LinearLayout) ll_main.getChildAt(2);
                                if (this.makeansfromview(ll_frac).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_frac_top(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            } else {
                                LinearLayout ll_frac = (LinearLayout) ll_main.getChildAt(1);

                                if (this.makeansfromview(ll_frac).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_frac_top(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            }
                        }
                    }
                }

                if (is_touch_kb) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ondeleteclick();
                        }
                    }, 150);
                }
            } else {
                this.onpreviousclick();
                if (is_touch_kb) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ondeleteclick();
                        }
                    }, 150);
                }
            }

        } catch (Exception e) {
        }
    }*/

    /*public void onpreviousclick() {

        try {
            int cur_pos = 0;

            try {
                cur_pos = selectedTextView.getSelectionStart();
            } catch (Exception e) {
            }

            if (cur_pos != 0) {

                String str_bar_1 = "" + Html.fromHtml("&#x0304;");
                String str_bar_2 = "" + Html.fromHtml("&#772;");

                if (selectedTextView.getText().toString().length() > 1) {
                    String ss = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos - 1));
                    if (ss.equals(str_bar_1) || ss.equals(str_bar_2)) {

                        selectedTextView.setSelection((cur_pos - 2));

                    } else {
                        selectedTextView.setSelection((cur_pos - 1));
                    }
                } else {
                    selectedTextView.setSelection((cur_pos - 1));
                }

                if (is_touch_kb_pre) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onpreviousclick();
                        }
                    }, 150);
                }

            } else {

                final LinearLayout ll_main_ll = ll_ans_box_selected;
                final LinearLayout ll_sub_ll = ll_selected_view;

                int pos_sub_ll = ll_main_ll.indexOfChild(ll_sub_ll);

                if (pos_sub_ll != 0 && pos_sub_ll != -1) {

                    LinearLayout ll_sub = (LinearLayout) ll_main_ll.getChildAt(pos_sub_ll - 1);
                    onpresubcall(ll_sub);

                } else if (pos_sub_ll == 0) {

                    String main_tag = (String) ll_main_ll.getTag(R.id.second);

                    if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                    } else if (main_tag.equals("nth_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                    } else if (main_tag.equals("lim_right")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                    } else if (main_tag.equals("sqrt_c") || main_tag.equals("lim_left")) {

                        try {
                            sub_pre_sqrt_c(ll_main_ll, 0, true);
                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("frac_top") || main_tag.equals("ss_top") ||
                            main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("nth_top") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                            || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                            || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {

                        try {
                            sub_pre_frac_top(ll_main_ll, 0, true);
                        } catch (Exception e) {
                        }

                    }
                }

                if (is_touch_kb_pre) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onpreviousclick();
                        }
                    }, 150);
                }

            }

        } catch (Exception e) {
        }

    }*/

    public void sub_pre_sqrt_c(final LinearLayout ll_main_ll, final int is_main, final boolean is_add_ed) {

        try {

            LinearLayout ll_mainm = (LinearLayout) ll_main_ll.getParent();
            LinearLayout ll_main1 = (LinearLayout) ll_mainm.getParent();
            LinearLayout ll_main2 = (LinearLayout) ll_main1.getParent();
            int main_pos = ll_main2.indexOfChild(ll_main1);
            final String main_tag = String.valueOf(ll_main2.getTag(R.id.second));
            if (main_pos == 0 && is_add_ed) {
                add_last_ed(ll_main2, 22, main_pos);
            } else if (ll_main2.getChildCount() > 1 && main_pos != 0) {
                LinearLayout ll_sub = (LinearLayout) ll_main2.getChildAt(main_pos - 1);
                if (!(ll_sub.getTag().equals("text")) && is_add_ed) {
                    add_last_ed(ll_main2, 22, main_pos);
                } else {
                    onpresubcall(ll_sub);
                }
            } else {
                if (main_tag.equals("sqrt_c") || main_tag.equals("lim_left")) {
                    this.sub_pre_sqrt_c(ll_main2, 0, is_add_ed);
                } else if (main_tag.equals("frac_top") || main_tag.equals("ss_top")
                        || main_tag.equals("super_top") || main_tag.equals("sub_bottom")
                        || main_tag.equals("nth_top") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                        || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                        || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {
                    this.sub_pre_frac_top(ll_main2, 0, is_add_ed);
                } else if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")) {
                    final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(0);
                    final LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
                    onpresubcall(ll_sub_1);
                } else if (main_tag.equals("nth_bottom")) {
                    LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    LinearLayout ll_main_1 = (LinearLayout) ll_main.getParent();
                    LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(0);
                    LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                    onpresubcall(ll_sub_1);
                } else if (main_tag.equals("lim_right")) {

                    LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    LinearLayout ll_main_2 = (LinearLayout) ll_main.getChildAt(0);
                    LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                    onpresubcall(ll_sub_1);

                } else if (main_tag.equals("main")) {
                    if (is_main == 0) {
                        add_last_ed(ll_main2, 18, 0);
                    } else if (is_main == 1) {
                        this.onnextsubcall((LinearLayout) ll_main2.getChildAt(1));
                    }
                }
            }

        } catch (Exception e) {
        }

    }

    public void sub_pre_frac_top(final LinearLayout ll_main_ll, final int is_main, final boolean is_add_ed) {

        try {
            final LinearLayout ll_mainm = (LinearLayout) ll_main_ll.getParent();
            final LinearLayout ll_main2 = (LinearLayout) ll_mainm.getParent();
            int main_pos = ll_main2.indexOfChild(ll_mainm);
            final String main_tag = String.valueOf(ll_main2.getTag(R.id.second));
            if (main_pos == 0 && is_add_ed) {
                add_last_ed(ll_main2, 22, main_pos);
            } else if (ll_main2.getChildCount() > 1 && main_pos != 0) {
                final LinearLayout ll_sub = (LinearLayout) ll_main2.getChildAt(main_pos - 1);
                if (!(ll_sub.getTag().equals("text")) && is_add_ed) {
                    add_last_ed(ll_main2, 22, main_pos);
                } else {
                    onpresubcall(ll_sub);
                }
            } else {
                Log.e("delete2", "" + ll_main2.getTag(R.id.second));
                if (main_tag.equals("sqrt_c") || main_tag.equals("lim_left")) {
                    this.sub_pre_sqrt_c(ll_main2, 0, is_add_ed);
                } else if (main_tag.equals("frac_top") || main_tag.equals("ss_top")
                        || main_tag.equals("super_top") || main_tag.equals("sub_bottom")
                        || main_tag.equals("nth_top") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                        || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                        || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {
                    this.sub_pre_frac_top(ll_main2, 0, is_add_ed);
                } else if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")) {
                    final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(0);
                    final LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
                    onpresubcall(ll_sub_1);
                } else if (main_tag.equals("nth_bottom")) {
                    LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    LinearLayout ll_main_1 = (LinearLayout) ll_main.getParent();
                    LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(0);
                    LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                    onpresubcall(ll_sub_1);
                } else if (main_tag.equals("lim_right")) {

                    LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    LinearLayout ll_main_2 = (LinearLayout) ll_main.getChildAt(0);
                    LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                    onpresubcall(ll_sub_1);

                } else if (main_tag.equals("main")) {
                    if (is_main == 0) {
                        add_last_ed(ll_main2, 18, 0);
                    } else if (is_main == 1) {
                        this.onnextsubcall((LinearLayout) ll_main2.getChildAt(1));
                    }
                }
            }
        } catch (Exception e) {
        }

    }

    private String makeansfromview(final LinearLayout ll_ans_box) {

        String str = "";

        for (int i = 0; i < ll_ans_box.getChildCount(); i++) {

            LinearLayout ll_view = (LinearLayout) ll_ans_box.getChildAt(i);

            String main_tag = String.valueOf(ll_view.getTag());

            if (main_tag.equals("text")) {

                EditTextBlink ed_1 = (EditTextBlink) ll_view.getChildAt(0);
                if (ed_1.getText().toString().trim().length() != 0) {
                    str = str + "text#" + this.replacecharfromsign(ed_1.getText().toString().trim()) + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }

            } else if (main_tag.equals("fraction")) {

                LinearLayout ll_fraction_top = (LinearLayout) ll_view.getChildAt(0);
                LinearLayout ll_fraction_bottom = (LinearLayout) ll_view.getChildAt(2);

                String ss = this.makeansfromview(ll_fraction_top);
                String ss1 = this.makeansfromview(ll_fraction_bottom);
                if (ss.trim().length() != 0 && ss1.trim().length() != 0) {
                    str = str + "fraction" + ss + "frac_btm_" + ll_ans_box.getTag(R.id.first)
                            + ss1 + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }
            } else if (main_tag.equals("super_script")) {

                LinearLayout ll_top_superscript = (LinearLayout) ll_view.getChildAt(0);

                String ss = this.makeansfromview(ll_top_superscript);

                if (ss.trim().length() != 0) {
                    str = str + "super_script" + ss + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }

            } else if (main_tag.equals("sub_script")) {

                LinearLayout ll_bottom_subscript = (LinearLayout) ll_view.getChildAt(1);

                String ss = this.makeansfromview(ll_bottom_subscript);

                if (ss.trim().length() != 0) {
                    str = str + "sub_script" + ss + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }


            } else if (main_tag.equals("super_sub_script")) {

                LinearLayout ll_top_super_sub_script = (LinearLayout) ll_view.getChildAt(0);
                LinearLayout ll_bottom_super_sub_script = (LinearLayout) ll_view.getChildAt(1);

                String ss = this.makeansfromview(ll_top_super_sub_script);
                String ss1 = this.makeansfromview(ll_bottom_super_sub_script);
                if (ss.trim().length() != 0 && ss1.trim().length() != 0) {
                    str = str + "super_sub_script" + ss + "ss_btm_" + ll_ans_box.getTag(R.id.first)
                            + ss1 + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }
            } else if (main_tag.equals("sqrt")) {

                LinearLayout ll_l = (LinearLayout) ll_view.getChildAt(1);
                LinearLayout ll_center_square_root = (LinearLayout) ll_l.getChildAt(1);

                String ss = this.makeansfromview(ll_center_square_root);

                if (ss.trim().length() != 0) {
                    str = str + "sqrt" + ss + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }

            } else if (main_tag.equals("nsqrt")) {

                LinearLayout ll_top_nth_root = (LinearLayout) ll_view.getChildAt(0);
                LinearLayout ll_l = (LinearLayout) ll_view.getChildAt(2);
                LinearLayout ll_center_nth_root = (LinearLayout) ll_l.getChildAt(1);

                String ss = this.makeansfromview(ll_top_nth_root);
                String ss1 = this.makeansfromview(ll_center_nth_root);
                if (ss.trim().length() != 0 && ss1.trim().length() != 0) {
                    str = str + "nsqrt" + ss + "nsqrt_center_" + ll_ans_box.getTag(R.id.first)
                            + ss1 + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }
            } else if (main_tag.equals("lim")) {

                LinearLayout ll_1 = (LinearLayout) ll_view.getChildAt(1);
                LinearLayout ll_lim_left = (LinearLayout) ll_1.getChildAt(0);
                LinearLayout ll_lim_right = (LinearLayout) ll_1.getChildAt(2);

                String ss = this.makeansfromview(ll_lim_left);
                String ss1 = this.makeansfromview(ll_lim_right);
                if (ss.trim().length() != 0 && ss1.trim().length() != 0) {
                    str = str + "lim" + ss + "lim_rht_" + ll_ans_box.getTag(R.id.first)
                            + ss1 + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }
            } else if (main_tag.equals("parenthesis")) {

                LinearLayout ll_top_superscript = (LinearLayout) ll_view.getChildAt(1);

                String ss = this.makeansfromview(ll_top_superscript);

                if (ss.trim().length() != 0) {
                    str = str + "parenthesis" + ss + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }

            } else if (main_tag.equals("absolute")) {

                LinearLayout ll_top_superscript = (LinearLayout) ll_view.getChildAt(1);

                String ss = this.makeansfromview(ll_top_superscript);

                if (ss.trim().length() != 0) {
                    str = str + "absolute" + ss + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }

            } else if (main_tag.equals("line") || main_tag.equals("lsegment")
                    || main_tag.equals("ray") || main_tag.equals("arc")) {

                LinearLayout ll_top_superscript = (LinearLayout) ll_view.getChildAt(1);

                String ss = this.makeansfromview(ll_top_superscript);

                if (ss.trim().length() != 0) {
                    str = str + main_tag + ss + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }

            }
        }
        try {
            str = str.substring(0, (str.length() - 3));
        } catch (Exception e) {

        }

        try {
            String cut_str = str.substring(str.length() - 8);
            if (cut_str.equals("fraction")) {
                str = str.substring(0, (str.length() - 11));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 12);
            if (cut_str.equals("super_script")) {
                str = str.substring(0, (str.length() - 15));
            }
        } catch (Exception e) {
        }
        try {
            String cut_str = str.substring(str.length() - 10);
            if (cut_str.equals("sub_script")) {
                str = str.substring(0, (str.length() - 13));
            }
        } catch (Exception e) {
        }
        try {
            String cut_str = str.substring(str.length() - 16);
            if (cut_str.equals("super_sub_script")) {
                str = str.substring(0, (str.length() - 19));
            }
        } catch (Exception e) {
        }
        try {
            String cut_str = str.substring(str.length() - 4);
            if (cut_str.equals("sqrt")) {
                str = str.substring(0, (str.length() - 7));
            }
        } catch (Exception e) {
        }
        try {
            String cut_str = str.substring(str.length() - 5);
            if (cut_str.equals("nsqrt")) {
                str = str.substring(0, (str.length() - 8));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 3);
            if (cut_str.equals("lim")) {
                str = str.substring(0, (str.length() - 6));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 11);
            if (cut_str.equals("parenthesis")) {
                str = str.substring(0, (str.length() - 14));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 8);
            if (cut_str.equals("absolute")) {
                str = str.substring(0, (str.length() - 11));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 4);
            if (cut_str.equals("line")) {
                str = str.substring(0, (str.length() - 7));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 8);
            if (cut_str.equals("lsegment")) {
                str = str.substring(0, (str.length() - 11));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 3);
            if (cut_str.equals("ray")) {
                str = str.substring(0, (str.length() - 6));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 3);
            if (cut_str.equals("arc")) {
                str = str.substring(0, (str.length() - 6));
            }
        } catch (Exception e) {
        }

        return str;
    }

    private boolean is_char_match(String ss) {

        String str_plus = "" + Html.fromHtml("&#43;");
        String str_minus = "" + Html.fromHtml("&#8722;");
        String str_equal = "" + Html.fromHtml("&#61;");
        String str_multi = "" + Html.fromHtml("&#215;");
        String str_divide = "" + Html.fromHtml("&#247;");
        String str_goe = "" + Html.fromHtml("&#8805;");
        String str_loe = "" + Html.fromHtml("&#8804;");
        String str_grthan = "" + Html.fromHtml("&#62;");
        String str_lethan = "" + Html.fromHtml("&#60;");

        if (ss.equals(str_plus) || ss.equals(str_minus) || ss.equals(str_equal) || ss.equals(str_multi) || ss.equals(str_divide)
                || ss.equals(str_goe) || ss.equals(str_loe) || ss.equals(str_grthan)
                || ss.equals(str_lethan)) {
            return true;
        } else {
            return false;
        }

    }

    public void onplusminusclick(View v) {
        try {
            if (selected_ed_2 == null) {

                int pos = selectedTextView.getSelectionStart();

                try {

                    if (pos == 0) {

                        if (selectedTextView.getText().toString().trim().length() > 0) {

                            String ss = String.valueOf(selectedTextView.getText().toString().charAt(0));

                            if (ss.equals("-")) {
                                selectedTextView.getText().delete(0, 1);
                            } else {
                                selectedTextView.getText().insert(0, "-");
                            }

                        } else {
                            selectedTextView.getText().insert(0, "-");
                        }

                    }

                } catch (Exception e) {

                }

                for (int i = (pos - 1); i >= 0; i--) {

                    String ss = String.valueOf(selectedTextView.getText().toString().charAt(i));

                    if (ss.equals("-")) {
                        selectedTextView.getText().delete(i, i + 1);
                        break;
                    } else if (ss.equals(" ")) {
                        selectedTextView.getText().insert(i + 1, "-");
                        break;
                    } else if (ss.equals("(") || ss.equals(")")) {
                        selectedTextView.getText().insert(i + 1, "-");
                        break;
                    } else if (is_char_match(ss)) {
                        selectedTextView.getText().insert(i + 1, "-");
                        break;
                    } else if (i == 0) {
                        selectedTextView.getText().insert(i, "-");
                        break;
                    }

                }

            } else {

                int cur_pos = ll_ans_box_selected.indexOfChild(ll_selected_view);

                if (cur_pos < ll_ans_box_selected.getChildCount()) {

                    LinearLayout ll_next = (LinearLayout) ll_ans_box_selected.getChildAt((cur_pos + 1));

                    if (String.valueOf(ll_next.getTag()).equals("text")) {
                        EditTextBlink ed_text = (EditTextBlink) ll_next.getChildAt(0);
                        if (ed_text.getText().toString().contains("-")) {
                            ed_text.getText().delete(0, 1);
                        } else {
                            ed_text.getText().insert(0, "-");
                        }
                        ed_text.setSelection(1);
                        ed_text.requestFocus();
                    } else {
                        final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                        ll_last.setTag("text");
                        final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                        ed_centre.setText("-");
                        ed_centre.requestFocus();
                        ll_last.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ed_centre.setCursorVisible(true);
                                selectedTextView = ed_centre;
                                ll_selected_view = ll_last;
                                selected_ed_2 = null;
                                showKeyboard();
                            }
                        });
                        ed_centre.setOnTouchListener(new OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                ed_centre.setCursorVisible(true);
                                selectedTextView = ed_centre;
                                ll_selected_view = ll_last;
                                selected_ed_2 = null;
                                showKeyboard();
                                return false;
                            }
                        });
                        ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                if (hasFocus) {
                                    selectedTextView = ed_centre;
                                    ll_selected_view = ll_last;
                                    selected_ed_2 = null;
                                }
                            }
                        });

                        ll_ans_box_selected.addView(ll_last, (cur_pos + 1));
                    }
                }
            }
        } catch (Exception e) {

        }
    }

    private void setTextToSelectedTextView(EditText txtView, String text) { // Changes by Siddhi info soft

        final LinearLayout ll_ans_box = ll_ans_box_selected;

        try {

            if (text.equals("op_pi")) {
                text = "" + Html.fromHtml("&#960;");
            }

            if (selected_ed_2 == null) {

                setTextToSelectedTextView2(txtView, text);

            } else {

                int cur_pos = ll_ans_box_selected.indexOfChild(ll_selected_view);

                if (cur_pos < ll_ans_box_selected.getChildCount()) {

                    final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                    ll_last.setTag("text");
                    final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                    ed_centre.setText(text);
                    ll_last.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ed_centre.setCursorVisible(true);
                            selectedTextView = ed_centre;
                            ll_selected_view = ll_last;
                            selected_ed_2 = null;
                            ll_ans_box_selected = ll_ans_box;
                            showKeyboard();
                        }
                    });
                    ed_centre.setOnTouchListener(new OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            ed_centre.setCursorVisible(true);
                            selectedTextView = ed_centre;
                            ll_selected_view = ll_last;
                            selected_ed_2 = null;
                            ll_ans_box_selected = ll_ans_box;
                            showKeyboard();
                            return false;
                        }
                    });
                    ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                selectedTextView = ed_centre;
                                ll_selected_view = ll_last;
                                selected_ed_2 = null;
                                ll_ans_box_selected = ll_ans_box;
                            }
                        }
                    });

                    ll_ans_box.addView(ll_last, (cur_pos + 1));

                    ed_centre.requestFocus();

                }
            }

        } catch (Exception e) {

        }

        selectedTextView.setCursorVisible(true);

    }

    public void setTextToSelectedTextView2(EditText txtView, String text) { //added by siddhiinfosoft
        try {
            if (selectedTextView != null) {

                int cur_pos = selectedTextView.getSelectionStart();
                int ed_length = selectedTextView.getText().toString().length();

                if (selectedTextView.getText().toString().length() > 1) {

                    String str_bar_1 = "" + Html.fromHtml("&#x0304;");
                    String str_bar_2 = "" + Html.fromHtml("&#772;");

                    if (cur_pos != 0 && cur_pos < ed_length) {
                        String ss = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos - 1));

                        if (ss.equals("x")) {

                            String ss_1 = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos));

                            if (ss_1.equals(str_bar_1) || ss_1.equals(str_bar_2)) {

                            } else {
                                selectedTextView.getText().insert(selectedTextView.getSelectionStart(), text);
                            }

                        } else {
                            selectedTextView.getText().insert(selectedTextView.getSelectionStart(), text);
                        }

                    } else {
                        selectedTextView.getText().insert(selectedTextView.getSelectionStart(), text);
                    }

                } else {

                    selectedTextView.getText().insert(selectedTextView.getSelectionStart(), text);

                }

                if (text.equals("|  |")) {
                    selectedTextView.setSelection((selectedTextView.getSelectionStart() - 3));
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        selectedTextView.setCursorVisible(true);
    }

    private void callclickmethod(final CustomeAns customeAns, final LinearLayout ll_ans_box
            , final int position) {

        try {
            if (customeAns.getFillInType() == FILL_IN_TYPE) {

                LinearLayout ll_last = (LinearLayout) ll_ans_box.getChildAt((ll_ans_box.getChildCount() - 1));
                if (ll_last.getTag().toString().equals("text")) {
                    final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                    ed_centre.setSelection(ed_centre.getText().toString().length());
                    if(ed_centre.hasFocus()) {
                        ed_centre.clearFocus();
                    }
                    ed_centre.requestFocus();
                } else {
                    add_last_ed(ll_ans_box, 22, ll_ans_box.getChildCount());
                }
                showKeyboard();
            }
        } catch (Exception e) {
        }
    }

    //change for the edit those answer for not allow change which are not given by student
    private boolean isPlayerAnswerEditable(CustomePlayerAns playerAns) {
        try {
            if (isExpireQuizz) {
                return false;
            } else {
                if (customeResult.getAllowChanges() == 1 ||
                        !customeResult.isAlreadyPlayed()) {
                    return true;
                } else {
                    if (this.isPlayerAnsGiven(playerAns))
                        return false;
                    else//chage for to update the answer not given
                        return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
    }

    private boolean isPlayerAnsGiven(CustomePlayerAns playerAns) {
        try {
            if (playerAns != null) {
                if (playerAns.getAns().length() > 0) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    //Add Question
    private void setQuestion(CustomeAns customeAns, RelativeLayout rlQuestionLayout,
                             LinearLayout ll_que_box) {
        try {
            if (MathFriendzyHelper.isEmpty(customeAns.getQuestionString())) {
                rlQuestionLayout.setVisibility(View.GONE);
            } else {
                ll_que_box.setMinimumWidth(ActAssignCustomAns.dpwidth - 200);
                ll_que_box.setTag(R.id.first, "1");
                ll_que_box.setTag(R.id.second, "main");
                this.addansboxview(ll_que_box,
                        this.getUpdatedQuestionString(customeAns.getQuestionString()), MathFriendzyHelper.YES);
                rlQuestionLayout.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            rlQuestionLayout.setVisibility(View.GONE);
        }
    }

    //update the question string to show the old question with the new keyboard
    private String getUpdatedQuestionString(String string) {
        if (string.contains("#")) {
            return string;
        }
        return "text#" + string;
    }

    //Add Links
    private void initializeAddUrlList() {
        try {
            urlList = customeResult.getLinks();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setVisibilityOfBtnLinks() {
        if (urlList != null && urlList.size() > 0) {
            this.setVisibilityOfBtnAddLinks(true);
        } else {
            this.setVisibilityOfBtnAddLinks(false);
        }
    }

    private void setVisibilityOfBtnAddLinks(boolean bValue) {
        if (bValue) {
            btnLinks.setVisibility(View.VISIBLE);
        } else {
            btnLinks.setVisibility(View.GONE);
        }
    }

    private void showAddLinksDialog() {
        if (addUrlLinkDialog == null) {
            addUrlLinkDialog = new AddUrlToQuestionDialog(this, false);
            addUrlLinkDialog.initializeCallback(new OnUrlSaveCallback() {
                @Override
                public void onSave(ArrayList<AddUrlToWorkArea> updatedUrlLinkList, int tag) {
                    ActCheckHomeWork.this.urlList = updatedUrlLinkList;
                    if (tag == OnUrlSaveCallback.ON_SAVE) {
                        //click to save
                    }
                }
            });
        }
        addUrlLinkDialog.showDialog(urlList);
    }


    private void setUpdatedDataAndRefreshView(Intent data) {
        try {
            CustomePlayerAns updatedPlayerAns = (CustomePlayerAns)
                    data.getSerializableExtra("updatedPlayerAns");
            clickedStudent.setUserAns(updatedPlayerAns.getAns());
            clickedStudent.setTeacherCredit(MathFriendzyHelper
                    .parseInt(updatedPlayerAns.getTeacherCredit()));
            if (updatedPlayerAns.getIsCorrect() == MathFriendzyHelper.YES) {
                clickedStudent.setUserAnsCorrect(true);
            } else {
                clickedStudent.setUserAnsCorrect(false);
            }
            clickedPlayerAns.setAns(updatedPlayerAns.getAns());
            clickedPlayerAns.setTeacherCredit(updatedPlayerAns.getTeacherCredit());
            clickedPlayerAns.setIsCorrect(updatedPlayerAns.getIsCorrect());

            /*this.init();
            this.initilizeAllowChangesVariable();
            this.setCustomeDataToListLayout();
            this.setCreditAndAvgScore();
            this.setVisibilityOfCalculateButton();*/
            View view = this.getViewByQuesId(clickedStudent.getCustomeAns().getQueNo());
            //RelativeLayout viewLayout = (RelativeLayout) view.findViewById(R.id.checkHomeLayout);
            //this.setViewBackGroundByTeacherCredit(viewLayout , clickedPlayerAns);
            if (clickedStudent.getCustomeAns().getFillInType() == 1) {
                final LinearLayout ll_ans_box = (LinearLayout) view.findViewById(R.id.ll_ans_box); //siddhiinfosoft
                ll_ans_box.removeAllViews();
                ll_ans_box.setMinimumWidth(dpwidth - 170); // siddhiinfosoft
                ll_ans_box.setTag(R.id.first, "1");
                ll_ans_box.setTag(R.id.second, "main");
                this.setstudentansbox(clickedStudent.getCustomeAns(), ll_ans_box, clickedPlayerAns);  // siddhiinfosoft
            } else if (clickedStudent.getCustomeAns().getAnswerType() == 1) {//for multiple choice
                LinearLayout optionLayout = (LinearLayout) view.findViewById(R.id.optionLayout);
                int childCount = optionLayout.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    View optionView = optionLayout.getChildAt(i);
                    final TextView option = (TextView) optionView.findViewById(R.id.option);
                    if (clickedPlayerAns.getAns().contains(option.getText().toString())) {
                        option.setBackgroundResource(R.drawable.checked_small_box_home_work);
                    } else {
                        option.setBackgroundResource(R.drawable.unchecked_small_box_home_work);
                    }
                }
            } else if (clickedStudent.getCustomeAns().getAnswerType() == 2 ||
                    clickedStudent.getCustomeAns().getAnswerType() == 3) {//for yes/no and true/false
                final TextView ansTrue = (TextView) view.findViewById(R.id.ansTrue);
                final TextView ansFalse = (TextView) view.findViewById(R.id.ansFalse);
                if (clickedPlayerAns.getAns().equals("Yes")) {
                    clickedStudent.setUserAns(ansTrue.getText().toString());
                    ansTrue.setBackgroundResource(R.drawable.checked_box_home_work);
                    ansFalse.setBackgroundResource(R.drawable.unchecked_box_home_work);
                } else if (clickedPlayerAns.getAns().equals("No")) {
                    clickedStudent.setUserAns(ansFalse.getText().toString());
                    ansTrue.setBackgroundResource(R.drawable.unchecked_box_home_work);
                    ansFalse.setBackgroundResource(R.drawable.checked_box_home_work);
                } else if (clickedPlayerAns.getAns().equals("True")) {
                    clickedStudent.setUserAns(ansTrue.getText().toString());
                    ansTrue.setBackgroundResource(R.drawable.checked_box_home_work);
                    ansFalse.setBackgroundResource(R.drawable.unchecked_box_home_work);
                } else if (clickedPlayerAns.getAns().equals("False")) {
                    clickedStudent.setUserAns(ansFalse.getText().toString());
                    ansTrue.setBackgroundResource(R.drawable.unchecked_box_home_work);
                    ansFalse.setBackgroundResource(R.drawable.checked_box_home_work);
                }
            }

            //for update the avg score and credit
            for (int i = 0; i < customeResult.getCustomeAnsList().size(); i++) {
                String questNo = customeResult.getCustomeAnsList().get(i).getQueNo();
                View viewBack = this.getViewByQuesId(questNo);
                CustomePlayerAns playerAns = this.getPlayerAnsByQuestionId(questNo);
                RelativeLayout viewLayoutBack = (RelativeLayout) viewBack.findViewById(R.id.checkHomeLayout);
                this.setViewBackGroundByTeacherCredit(viewLayoutBack, playerAns);
            }
            this.setCreditAndAvgScore();
            //end changes
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setPlayerAnsWhenUserClickOnWorkArea(CustomeAns customeAns,
                                                     CustomePlayerAns playerAns, int index) {
        playerAns.setAns(getUserAnsWhenClickOnWorkArea(index, customeAns));
    }

    private String getUserAnsWhenClickOnWorkArea(int index, CustomeAns customeAns) {
        try {
            if (customeAns.getFillInType() == 1) {
                return this.getFillInAndByQuestionId(customeAns.getQueNo());
            } else if (customeAns.getAnswerType() == 1) {
                return this.getMultiPleChoiceAndByQuestionId(customeAns.getQueNo());
            } else if (customeAns.getAnswerType() == 2 || customeAns.getAnswerType() == 3) {
                return this.getCheckFillInAnsByQuestionId(customeAns.getQueNo());
            }
            return "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private String getFillInAndByQuestionId(String questionID) {
        for (int i = 0; i < userFillInAnsList.size(); i++) {
            if (questionID.equalsIgnoreCase(userFillInAnsList.get(i).getQueId()))
                return makeansfromview(userFillInAnsList.get(i).getTxtView());
        }
        return "";
    }

    private String getMultiPleChoiceAndByQuestionId(String questionID) {
        for (int i = 0; i < multipleChoiceAnsList.size(); i++) {
            if (questionID.equalsIgnoreCase(multipleChoiceAnsList.get(i).getQueId())) {
                multipleChoiceAnsList.get(i).setUserAns
                        (MathFriendzyHelper.
                                convertStringIntoSperatedStringWithDelimeter
                                        (multipleChoiceAnsList.get(i).getUserAns(), ","));
                return multipleChoiceAnsList.get(i).getUserAns();
            }
        }
        return "";
    }

    private String getCheckFillInAnsByQuestionId(String questionID) {
        for (int i = 0; i < checkUserAnswer.size(); i++) {
            if (questionID.equalsIgnoreCase(checkUserAnswer.get(i).getQueId())) {
                return checkUserAnswer.get(i).getUserAns();
            }
        }
        return "";
    }


    //For adding question with keyboard
    public void addansboxview(final LinearLayout ll_ans_box, String correct_ans, final int is_ans_available) {  // siddhiinfosoft

        try {

            try {
                ll_ans_box.removeAllViews();
            } catch (Exception e) {

            }

            if (is_ans_available == MathFriendzyHelper.NO) {

                final LinearLayout ll_last = (LinearLayout) this.getLayoutInflater().inflate(R.layout.kb_view_textview, null);
                ll_last.setTag("vtext");
                final TextView tv = (TextView) ll_last.findViewById(R.id.tv);
                String sttr = lblAnswerOnWorkAreaCreditGiven;
                tv.setText(sttr);

                tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MathFriendzyHelper.showWarningDialog(getCurrentObj(),
                                lblAnswerOnWorkAreaCreditGiven);
                    }
                });

                ll_ans_box.addView(ll_last);

            } else {

                if (correct_ans.trim().length() != 0) {
                    setstudentansboxsub(correct_ans, ll_ans_box);
                    Log.e("1234", "" + correct_ans);
                }

                boolean is_ed_last = false;

                if (ll_ans_box.getChildCount() > 0) {
                    LinearLayout lt = (LinearLayout) ll_ans_box.getChildAt(ll_ans_box.getChildCount() - 1);
                    if (lt.getTag().toString().equals("text")) {
                        is_ed_last = true;
                    } else {
                        is_ed_last = false;
                    }
                } else {
                    is_ed_last = false;
                }

                if (!is_ed_last) {

                    final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                    ll_last.setTag("text");
                    final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                    ed_centre.setMinWidth(20);
                    ll_last.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            custom_ed_touch(ed_centre, ll_last, ll_ans_box);
                        }
                    });
                    ed_centre.setOnTouchListener(new OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            custom_ed_touch(ed_centre, ll_last, ll_ans_box);
                            return false;
                        }
                    });
                    ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                selectedTextView = ed_centre;
                                ll_selected_view = ll_last;
                                ll_ans_box_selected = ll_ans_box;
                            }
                        }
                    });

                    ll_ans_box.addView(ll_last);

                }

            }
            MathFriendzyHelper.enableDisableControls(false, ll_ans_box);
        } catch (Exception e) {

        }
    }


    //Keyboard issues resolve by Hardip
    public EditTextBlink selectionEdittext = null;
    String blank_string;
    int cursor_postion;
    boolean parenth = false;
    public static boolean isGreen_student = false;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            try {
                int cur_pos = selectedTextView.getSelectionStart();
                if (cur_pos != 0) {
                    ActAssignCustomAns.isGreen = false;
                }
            } catch (Exception e) {
            }

            if (isGreen_student) {
                try {
                    BlankFieldAlert();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }
        } else if (keyCode == KeyEvent.KEYCODE_ENTER) {

            ActCheckHomeWork.this.hideDeviceKeyboard();
            ActCheckHomeWork.this.showKeyboard();

        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    public void onBackPressed() {
        if (is_keyboard_show) {
            this.hideKeyboard();
        } else {
            if (isExpireQuizz) {
                if (isCalledFortecherCheckHW) {
                    if (customeResult.isAlreadyPlayed())
                        this.calculateTeacherChanges();
                    else
                        super.onBackPressed();
                } else {
                    //super.onBackPressed();
                    this.finishOnlyForOponentInput();
                }
            } else {

                if (this.isAtleaseOneAnsGiven()) {
                    if (customeResult.getAllowChanges() == 1) {//1 for allow changes
                        if (customeResult.isAlreadyPlayed()) {
                            isClickOnBackPressed = true;
                            this.calculateAns(false, false);
                        } else {
                            if (this.isAtleaseOneAnsGiven()) {
                                isClickOnBackPressed = true;
                                this.calculateAns(false, false);
                            } else {
                                super.onBackPressed();
                            }
                        }
                    } else {
                        if (customeResult.isAlreadyPlayed()) {
                            isClickOnBackPressed = true;
                            this.calculateAns(false, false);
                        } else {
                            if (this.isAtleaseOneAnsGiven()) {
                                MathFriendzyHelper.yesNoConfirmationDialog(this,
                                        lblYourTeacherWillNotAllowYouToChange,
                                        yesButtonText, noButtonText,
                                        new YesNoListenerInterface() {

                                            @Override
                                            public void onYes() {
                                                superBackPressedCall();
                                            }

                                            @Override
                                            public void onNo() {

                                            }
                                        });
                            } else {
                                super.onBackPressed();
                            }
                        }
                    }
                } else {
                    super.onBackPressed();
                }
            }
        }
    }


    public void onkb_sub_symbol_click(View v) { // Added By Siddhi Info Soft

        try {

            String btn_tag = String.valueOf(v.getTag());

            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);

            if (btn_tag.equals("parenthesis")) {

                parenth = true;

            }

            if (btn_tag.equals("fraction") ||
                    btn_tag.equals("pointview")
                    || btn_tag.equals("lim") || btn_tag.equals("parenthesis") || btn_tag.equals("absolute") | btn_tag.equals("squareroot") || btn_tag.equals("nthroot")
                    || btn_tag.equals("super_script") || btn_tag.equals("sub_script") || btn_tag.equals("super_sub_script")
                    || btn_tag.equals("line") || btn_tag.equals("lsegment") || btn_tag.equals("ray")
                    || btn_tag.equals("arc")) {
                this.addviewtoselectedview(ll_ans_box_selected, btn_tag, "view");
            } else if (btn_tag.equals("absolute")) {
                setTextToSelectedTextView(selectedTextView, "|  |");
            } else {
                this.add_sign_into_box(btn_tag);
            }

        } catch (Exception e) {

        }
    }

    private void custom_ed_touch(final EditTextBlink ed, final LinearLayout ll_sub, final LinearLayout ll_main) {

//        parenth = true;

        try {

            ed.setCursorVisible(true);
            selectedTextView = ed;
            ll_selected_view = ll_sub;
            ll_ans_box_selected = ll_main;
            selected_ed_2 = null;
            showKeyboard();

        } catch (Exception e) {
        }
    }

    private void addviewtoselectedview(final LinearLayout ll_ans_box, String tag, String type) {  // Added By Siddhi Info Soft
        boolean is_devide = false;
        int start_pos = 0, ed_legnth = 0, ll_sub_selected_pos = 0;
        String cut_str = "";
        String sss = String.valueOf(ll_ans_box.getTag(R.id.first));
        int tag_main_ll = Integer.valueOf(sss);
        boolean need_delete = false;
        int text_size = ActCheckHomeWork.this.getResources().getInteger(R.integer.kb_textsize_main_int);

        try {
            DisplayMetrics metrics;
            metrics = getApplicationContext().getResources().getDisplayMetrics();
            float Textsize = selectedTextView.getTextSize() / metrics.density;
            text_size = Math.round(Textsize);
            Log.e("text_size", "" + text_size);
            if (text_size < 5) {
                text_size = 5;
            }

        } catch (Exception e) {
            text_size = 16;
        }

        try {

            start_pos = selectedTextView.getSelectionStart();
            ed_legnth = selectedTextView.getText().toString().length();

            String str = selectedTextView.getText().toString().trim();

            boolean pluscheck = false;
            boolean minuscheck = false;
            boolean multicheck = false;
            boolean divicheck = false;
            boolean equalcheck = false;


            if (!TextUtils.isEmpty(str)) {


                // coding for 96 or 96 + 96 for add substring


                if (!str.contains("" + Html.fromHtml("&#43;"))) {
                    pluscheck = true;

                } else {
                    int s = 0;

                    if (str.endsWith("" + Html.fromHtml("&#43;"))) {

                    } else {
                        s = str.length() - 1;
                    }


                    String temstring = str.substring(0, s);


                    // coding for 96+ or 96 + 96 + for alert

                    if (temstring.contains("" + Html.fromHtml("&#43;"))) {
                        //str.contains("" + Html.fromHtml("&#215;")) || str.contains("" + Html.fromHtml("&#247;")) || str.contains("" + Html.fromHtml("&#61;")
                        pluscheck = true;
                    } else {

                        pluscheck = false;
                    }
                }


            /*    if (str.contains("" + Html.fromHtml("&#43;"))) {
                    pluscheck = false;
                } else {
                    pluscheck = true;
                }*/


                if (!str.contains("" + Html.fromHtml("&#8722;"))) {
                    minuscheck = true;

                } else {

                    int s = 0;

                    if (str.endsWith("" + Html.fromHtml("&#8722;"))) {

                    } else {
                        s = str.length() - 1;
                    }


                    String temstring = str.substring(0, s);

                    // coding for 96+ or 96 + 96 + for alert

                    if (temstring.contains("" + Html.fromHtml("&#8722;"))) {
                        //str.contains("" + Html.fromHtml("&#215;")) || str.contains("" + Html.fromHtml("&#247;")) || str.contains("" + Html.fromHtml("&#61;")
                        minuscheck = true;
                    } else {


                        minuscheck = false;
                    }
                }

              /*  if (str.contains("" + Html.fromHtml("&#8722;"))) {
                    minuscheck = false;
                } else {
                    minuscheck = true;
                }*/

                if (!str.contains("" + Html.fromHtml("&#215;"))) {
                    multicheck = true;

                } else {
                    int s = 0;

                    if (str.endsWith("" + Html.fromHtml("&#215;"))) {

                    } else {
                        s = str.length() - 1;
                    }

                    String temstring = str.substring(0, s);

                    // coding for 96+ or 96 + 96 + for alert

                    if (temstring.contains("" + Html.fromHtml("&#215;"))) {
                        //str.contains("" + Html.fromHtml("&#215;")) || str.contains("" + Html.fromHtml("&#247;")) || str.contains("" + Html.fromHtml("&#61;")
                        multicheck = true;
                    } else {


                        multicheck = false;
                    }
                }

/*
                if (str.contains("" + Html.fromHtml("&#215;"))) {
                    multicheck = false;
                } else {
                    multicheck = true;
                }*/


                if (!str.contains("" + Html.fromHtml("&#247;"))) {
                    divicheck = true;

                } else {
                    int s = 0;

                    if (str.endsWith("" + Html.fromHtml("&#247;"))) {

                    } else {
                        s = str.length() - 1;
                    }

                    String temstring = str.substring(0, s);

                    // coding for 96+ or 96 + 96 + for alert

                    if (temstring.contains("" + Html.fromHtml("&#247;"))) {
                        //str.contains("" + Html.fromHtml("&#215;")) || str.contains("" + Html.fromHtml("&#247;")) || str.contains("" + Html.fromHtml("&#61;")
                        divicheck = true;
                    } else {


                        divicheck = false;
                    }
                }

             /*   if (str.contains("" + Html.fromHtml("&#247;"))) {
                    divicheck = false;
                } else {
                    divicheck = true;
                }*/

                if (!str.contains("" + Html.fromHtml("&#61;"))) {
                    equalcheck = true;

                } else {
                    int s = 0;

                    if (str.endsWith("" + Html.fromHtml("&#61;"))) {

                    } else {
                        s = str.length() - 1;
                    }


                    String temstring = str.substring(0, s);

                    // coding for 96+ or 96 + 96 + for alert

                    if (temstring.contains("" + Html.fromHtml("&#61;"))) {
                        //str.contains("" + Html.fromHtml("&#215;")) || str.contains("" + Html.fromHtml("&#247;")) || str.contains("" + Html.fromHtml("&#61;")
                        equalcheck = true;
                    } else {


                        equalcheck = false;
                    }
                }

               /* if (str.contains("" + Html.fromHtml("&#61;"))) {
                    equalcheck = false;
                } else {
                    equalcheck = true;
                }*/


            }

            try {
                ll_sub_selected_pos = ll_ans_box_selected.indexOfChild(ll_selected_view);
            } catch (Exception e) {
                ll_sub_selected_pos = 0;
            }

            try {
                if (ll_sub_selected_pos == -1) {
                    ll_sub_selected_pos = 0;
                }

                if (start_pos != 0 && start_pos < ed_legnth) {
                    cut_str = selectedTextView.getText().toString();
                    cut_str = cut_str.substring(start_pos, ed_legnth);
                    is_devide = true;
                    Log.e("math_ed_pos", cut_str + "" + ll_sub_selected_pos);
                    selectedTextView.getText().delete(start_pos, ed_legnth);
                }
            } catch (Exception e) {
            }

            try {
                if (selectedTextView.getText().toString().trim().length() == 0) {
                    need_delete = true;
                }
            } catch (Exception e) {
            }

            try {

                final LinearLayout ll_last = (LinearLayout) ll_ans_box.getChildAt((ll_ans_box.getChildCount() - 1));
                String str_tag = String.valueOf(ll_last.getTag());
                // Log.e("tagg",str_tag);
                if (str_tag.equals("text")) {
                    final EditTextBlink ed_last = (EditTextBlink) ll_last.getChildAt(0);
                    if (ed_last.getText().toString().trim().length() == 0) {

                        try {
                            if (ll_ans_box.getChildCount() > 1) {
                                LinearLayout ll_last_2 = (LinearLayout) ll_ans_box.getChildAt((ll_ans_box.getChildCount() - 2));
                                String str_tag_2 = String.valueOf(ll_last_2.getTag());
                                if (str_tag_2.equals("text")) {
                                    ll_ans_box.removeView(ll_last);
                                } else {
                                    ed_last.setMinWidth(4);
                                }
                            } else {
                                ed_last.setMinWidth(4);
                            }
                        } catch (Exception e) {
                        }
                    } else {
                        //  Log.e("tagg12", str_tag);
                        ed_last.setMinWidth(4);
                        ed_last.setMinimumWidth(4);
                        ed_last.requestLayout();
                        //  Log.e("tagg123", str_tag);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (type.equals("view")) {
                selected_ed_2 = null;
                if (tag.equals("super_script")) {

                    final LinearLayout ll_super_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_superscript, null);


                    ll_super_script.setTag("super_script");
                    final LinearLayout ll_top_superscript = (LinearLayout) ll_super_script.findViewById(R.id.ll_top_superscript);
                    ll_top_superscript.setTag(R.id.first, (tag_main_ll + 1));
                    ll_top_superscript.setTag(R.id.second, "super_top");

                    add_inside_ed(ll_top_superscript, true, text_size);

                    /*if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_super_script, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_super_script, (ll_sub_selected_pos + 1));
                    }*/

                    if (start_pos == 0 && !parenth) {
                        isGreen_student = false;

                        AlertScript();
                        custom_ed_focus(selectionEdittext, ll_ans_box, ll_super_script, parenth);

                        //need for validation changes
                    } else if (sss.equals("2")) {

                        AlertScript();
                        custom_ed_focus(selectionEdittext, ll_ans_box, ll_super_script, parenth);

                    } /*else if (ll_sub_selected_pos != 0) {

                        if (!parenth) {

                            parenth = false;

                            AlertScript();
                        } else {
                            parenth = true;

                            ll_ans_box.addView(ll_super_script, (ll_sub_selected_pos + 1));
                        }

                    }*/ else if (start_pos != 0 && !pluscheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !minuscheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !multicheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !divicheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !equalcheck) {

                        AlertScript();


                    } else if (start_pos == 1 && !need_delete) {

                        parenth = false;
                        ll_ans_box.addView(ll_super_script, (ll_sub_selected_pos + 1));

                    } else {
                        parenth = false;
                        ll_ans_box.addView(ll_super_script, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("sub_script")) {

                    final LinearLayout ll_sub_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_subscript, null);
                    ll_sub_script.setTag("sub_script");
                    final LinearLayout ll_bottom_subscript = (LinearLayout) ll_sub_script.findViewById(R.id.ll_bottom_subscript);
                    ll_bottom_subscript.setTag(R.id.first, (tag_main_ll + 1));
                    ll_bottom_subscript.setTag(R.id.second, "sub_bottom");
                    add_inside_ed(ll_bottom_subscript, true, text_size);

                  /*  if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_sub_script, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_sub_script, (ll_sub_selected_pos + 1));
                    }*/

                    if (start_pos == 0 && !parenth) {
                        isGreen_student = false;

                        AlertScript();
                        custom_ed_focus(selectionEdittext, ll_ans_box, ll_sub_script, parenth);

                        //need for validation changes
                    } else if (sss.equals("2")) {

                        AlertScript();
                        custom_ed_focus(selectionEdittext, ll_ans_box, ll_sub_script, parenth);

                    }/* else if (ll_sub_selected_pos != 0) {

                        if (!parenth) {

                            parenth = false;

                            AlertScript();
                        } else {
                            parenth = true;

                            ll_ans_box.addView(ll_sub_script, (ll_sub_selected_pos + 1));
                        }

                    }*/ else if (start_pos != 0 && !pluscheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !minuscheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !multicheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !divicheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !equalcheck) {

                        AlertScript();


                    } else if (start_pos == 1 && !need_delete) {

                        parenth = false;
//                        ll_ans_box.addView(ll_sub_script, ll_sub_selected_pos);
                        ll_ans_box.addView(ll_sub_script, (ll_sub_selected_pos + 1));
                    } else {

                        parenth = false;
                        ll_ans_box.addView(ll_sub_script, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("super_sub_script")) {

                    final LinearLayout ll_super_sub_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_super_sub_script, null);
                    ll_super_sub_script.setTag("super_sub_script");

                    final LinearLayout ll_top_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_top_super_sub_script);
                    final LinearLayout ll_bottom_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_bottom_super_sub_script);
                    ll_top_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                    ll_bottom_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                    ll_top_super_sub_script.setTag(R.id.second, "ss_top");
                    ll_bottom_super_sub_script.setTag(R.id.second, "ss_bottom");
                    add_inside_ed(ll_top_super_sub_script, true, text_size);
                    add_inside_ed(ll_bottom_super_sub_script, false, text_size);

                  /*  if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_super_sub_script, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_super_sub_script, (ll_sub_selected_pos + 1));
                    }*/

                    if (start_pos == 0 && !parenth) {
                        isGreen_student = false;

                        AlertScript();
                        custom_ed_focus(selectionEdittext, ll_ans_box, ll_super_sub_script, parenth);

                        //need for validation changes
                    } else if (sss.equals("2")) {

                        AlertScript();
                        custom_ed_focus(selectionEdittext, ll_ans_box, ll_super_sub_script, parenth);

                    }/* else if (ll_sub_selected_pos != 0) {

                        if (!parenth) {

                            parenth = false;

                            AlertScript();
                        } else {
                            parenth = true;

                            ll_ans_box.addView(ll_super_sub_script, (ll_sub_selected_pos + 1));
                        }

                    }*/ else if (start_pos != 0 && !pluscheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !minuscheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !multicheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !divicheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !equalcheck) {

                        AlertScript();


                    } else if (start_pos == 1 && !need_delete) {
                        parenth = false;
//                        ll_ans_box.addView(ll_sub_script, ll_sub_selected_pos);
                        ll_ans_box.addView(ll_super_sub_script, (ll_sub_selected_pos + 1));
                    } else {

                        parenth = false;
                        ll_ans_box.addView(ll_super_sub_script, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("fraction")) {

                    final LinearLayout ll_fraction = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_fraction, null);
                    final LinearLayout ll_fraction_top = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_top);
                    final LinearLayout ll_fraction_bottom = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_bottom);
                    ll_fraction_top.setTag(R.id.first, (tag_main_ll + 1));
                    ll_fraction_bottom.setTag(R.id.first, (tag_main_ll + 1));
                    ll_fraction_top.setTag(R.id.second, "frac_top");
                    ll_fraction_bottom.setTag(R.id.second, "frac_bottom");
                    add_inside_ed(ll_fraction_top, true, text_size);
                    add_inside_ed(ll_fraction_bottom, false, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_fraction, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_fraction, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }

                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("squareroot")) {

                    final LinearLayout ll_squareroot = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_square_root, null);
                    final LinearLayout ll_center_square_root = (LinearLayout) ll_squareroot.findViewById(R.id.ll_center_square_root);
                    ll_center_square_root.setTag(R.id.first, (tag_main_ll + 1));
                    ll_center_square_root.setTag(R.id.second, "sqrt_c");
                    add_inside_ed(ll_center_square_root, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_squareroot, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_squareroot, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("nthroot")) {

                    final LinearLayout ll_nthroot = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_nth_root, null);

                    final LinearLayout ll_top_nth_root = (LinearLayout) ll_nthroot.findViewById(R.id.ll_top_nth_root);
                    final LinearLayout ll_center_nth_root = (LinearLayout) ll_nthroot.findViewById(R.id.ll_center_nth_root);
                    final ImageView img = (ImageView) ll_nthroot.findViewById(R.id.img);
                    ll_top_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                    ll_center_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                    ll_top_nth_root.setTag(R.id.second, "nth_top");
                    ll_center_nth_root.setTag(R.id.second, "nth_bottom");
                    add_inside_ed(ll_top_nth_root, true, text_size);
                    add_inside_ed(ll_center_nth_root, false, text_size);
                    img.getLayoutParams().height = ll_center_nth_root.getHeight();
                    img.requestLayout();
                    ll_center_nth_root.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                        @Override
                        public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                            Log.e("het", "" + v.getHeight());
                            img.getLayoutParams().height = v.getHeight();
                            img.requestLayout();
                        }
                    });

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_nthroot, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_nthroot, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("lim")) {

                    final LinearLayout ll_lim = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_lim, null);
                    final TextView tv_text = (TextView) ll_lim.findViewById(R.id.tv_text);
                    tv_text.setText(Html.fromHtml("&#8594;"));
                    final LinearLayout ll_lim_left = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_left);
                    final LinearLayout ll_lim_right = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_right);
                    ll_lim_left.setTag(R.id.first, (tag_main_ll + 1));
                    ll_lim_right.setTag(R.id.first, (tag_main_ll + 1));
                    ll_lim_left.setTag(R.id.second, "lim_left");
                    ll_lim_right.setTag(R.id.second, "lim_right");
                    add_inside_ed(ll_lim_left, true, text_size);
                    add_inside_ed(ll_lim_right, false, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_lim, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_lim, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }

                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("parenthesis")) {

                    final LinearLayout ll_parenthesis = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_parenthesis, null);
                    ll_parenthesis.setTag("parenthesis");
                    final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_parenthesis_center);
                    ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                    ll_parenthesis_center.setTag(R.id.second, "parenthesis_center");
                    add_inside_ed(ll_parenthesis_center, true, text_size);
                    final ImageView img_left = (ImageView) ll_parenthesis.findViewById(R.id.img_left);
                    final ImageView img_rht = (ImageView) ll_parenthesis.findViewById(R.id.img_rht);

                    img_left.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final LinearLayout lll = (LinearLayout) ll_parenthesis_center.getChildAt(0);
                            if (lll.getTag().equals("text")) {
                                final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                edd.setSelection(0);
                                edd.requestFocus();
                            } else {
                                add_last_ed(ll_parenthesis_center, 22, 0);
                            }
                        }
                    });

                    img_rht.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final LinearLayout lll = (LinearLayout) ll_parenthesis_center.getChildAt((ll_parenthesis_center.getChildCount() - 1));
                            if (lll.getTag().equals("text")) {
                                final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                edd.setSelection(edd.getText().toString().length());
                                edd.requestFocus();
                            } else {
                                add_last_ed(ll_parenthesis_center, 22, (ll_parenthesis_center.getChildCount()));
                            }
                        }
                    });

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_parenthesis, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_parenthesis, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("absolute")) {

                    final LinearLayout ll_parenthesis = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_absolute, null);
                    ll_parenthesis.setTag("absolute");
                    final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_absolute_center);
                    ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                    ll_parenthesis_center.setTag(R.id.second, "absolute_center");
                    add_inside_ed(ll_parenthesis_center, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_parenthesis, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_parenthesis, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("line") || tag.equals("lsegment")
                        || tag.equals("ray") || tag.equals("arc")) {

                    final LinearLayout ll_parenthesis = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_lsegment, null);

                    final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_lsegment_center);
                    final ImageView img_top = (ImageView) ll_parenthesis.findViewById(R.id.img_top);
                    ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                    if (tag.equals("line")) {
                        img_top.setImageResource(R.drawable.line_icon);
                        ll_parenthesis.setTag("line");
                        ll_parenthesis_center.setTag(R.id.second, "line_center");
                    } else if (tag.equals("lsegment")) {
                        img_top.setImageResource(R.drawable.line_segment);
                        ll_parenthesis.setTag("lsegment");
                        ll_parenthesis_center.setTag(R.id.second, "lsegment_center");
                    } else if (tag.equals("ray")) {
                        img_top.setImageResource(R.drawable.ray_icon);
                        ll_parenthesis.setTag("ray");
                        ll_parenthesis_center.setTag(R.id.second, "ray_center");
                    } else if (tag.equals("arc")) {
                        img_top.setImageResource(R.drawable.arc_icon);
                        ll_parenthesis.setTag("arc");
                        ll_parenthesis_center.setTag(R.id.second, "arc_center");
                    }

                    add_inside_ed(ll_parenthesis_center, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_parenthesis, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_parenthesis, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                }
            }

            LinearLayout ll_last_2 = (LinearLayout) ll_ans_box.getChildAt((ll_ans_box.getChildCount() - 1));
            String str_tag = String.valueOf(ll_last_2.getTag());
            if (str_tag.equals("text")) {
                EditTextBlink ed_last = (EditTextBlink) ll_last_2.getChildAt(0);
                ed_last.setMinWidth(5);

            } else {

                final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                ll_last.setTag("text");
                final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                ed_centre.setTextSize(text_size);
                ed_centre.setMinWidth(4);
                ed_centre.setMinimumWidth(4);
                ed_centre.requestLayout();
                ll_last.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ed_centre.setCursorVisible(true);
                        selectedTextView = ed_centre;
                        ll_selected_view = ll_last;
                        ll_ans_box_selected = ll_ans_box;
                        selected_ed_2 = null;
                        showKeyboard();
                    }
                });
                ed_centre.setOnTouchListener(new OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        ed_centre.setCursorVisible(true);
                        selectedTextView = ed_centre;
                        ll_selected_view = ll_last;
                        ll_ans_box_selected = ll_ans_box;
                        selected_ed_2 = null;
                        showKeyboard();
                        return false;
                    }
                });
                ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            selectedTextView = ed_centre;
                            ll_selected_view = ll_last;
                            selected_ed_2 = null;
                            ll_ans_box_selected = ll_ans_box;
                        }
                    }
                });

                ll_ans_box.addView(ll_last);

            }

        } catch (Exception e) {

        }
    }

    public void onnextclick() {


        try {
            int cur_pos = 0, ed_legnth = 0;

            try {
                cur_pos = selectedTextView.getSelectionStart();
                ed_legnth = selectedTextView.getText().toString().length();
            } catch (Exception e) {
            }


            if ((cur_pos != ed_legnth)) {

                String str_bar_1 = "" + Html.fromHtml("&#x0304;");
                String str_bar_2 = "" + Html.fromHtml("&#772;");

                if (selectedTextView.getText().toString().length() > 1) {
                    String ss = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos));
                    if (ss.equals("x")) {
                        if ((cur_pos + 1) < ed_legnth) {
                            String ss_1 = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos + 1));
                            if (ss_1.equals(str_bar_1) || ss_1.equals(str_bar_2)) {
                                selectedTextView.setSelection((cur_pos + 2));
                            } else {
                                selectedTextView.setSelection((cur_pos + 1));
                            }
                        } else {
                            selectedTextView.setSelection((cur_pos + 1));
                        }
                    } else {
                        selectedTextView.setSelection((cur_pos + 1));
                    }
                } else {
                    selectedTextView.setSelection((cur_pos + 1));
                }

                if (is_touch_kb_next) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onnextclick();
                        }
                    }, 150);
                }

            } else {
                final LinearLayout ll_main_ll = ll_ans_box_selected;
                final LinearLayout ll_sub_ll = ll_selected_view;

                int pos_sub_ll = ll_main_ll.indexOfChild(ll_sub_ll);

                if (ll_main_ll.getChildCount() > 1 && pos_sub_ll < (ll_main_ll.getChildCount() - 1)) {

                    LinearLayout ll_sub = (LinearLayout) ll_main_ll.getChildAt(pos_sub_ll + 1);
                    onnextsubcall(ll_sub);

                } else if (pos_sub_ll == (ll_main_ll.getChildCount() - 1)) {

                    String main_tag = (String) ll_main_ll.getTag(R.id.second);

                    if (main_tag.equals("frac_top")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("ss_top")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(1);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("nth_top")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("lim_left")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("sqrt_c") || main_tag.equals("nth_bottom") || main_tag.equals("lim_right")) {

                        sub_next_sqrt_c(ll_main_ll);

                    } else if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom") ||
                            main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                            || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                            || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {

                        if (main_tag.equals("parenthesis_center")) {

                            parenth = true;
                        }

                        sub_next_frac_bottom(ll_main_ll);

                    }
                }

                if (is_touch_kb_next) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onnextclick();
                        }
                    }, 150);
                }
            }

        } catch (Exception e) {
        }
    }

    public void ondeleteclick() {

        ActAssignCustomAns.isGreen = false;


//        parenth = false;
        final LinearLayout ll_main_ll = ll_ans_box_selected;
        final LinearLayout ll_sub_ll = ll_selected_view;

        try {
            int ed_pos = 0, ed_legnth = 0;
            try {
                ed_pos = selectedTextView.getSelectionStart();
                ed_legnth = selectedTextView.getText().toString().trim().length();
            } catch (Exception e) {
            }
            if (ed_pos > 0 && ed_legnth > 0) {
                String str_bar_1 = "" + Html.fromHtml("&#x0304;");
                String str_bar_2 = "" + Html.fromHtml("&#772;");
                String str_integral = "" + Html.fromHtml("&#8747;");
                if (selectedTextView.getText().toString().length() > 1) {
                    String ss = String.valueOf(selectedTextView.getText().toString().charAt(ed_pos - 1));
                    if (ss.equals(str_bar_1) || ss.equals(str_bar_2)) {

                        selectedTextView.getText().delete(ed_pos - 2, ed_pos);

                    } else if (ss.equals("x")) {
                        if (ed_pos < ed_legnth) {
                            String ss_2 = String.valueOf(selectedTextView.getText().toString().charAt(ed_pos));
                            if (ss_2.equals(str_bar_1) || ss_2.equals(str_bar_2)) {
                                selectedTextView.getText().delete(ed_pos - 1, ed_pos + 1);

                            } else {
                                selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                            }
                        } else {
                            selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                        }
                    } else if (ss.equals(" ")) {
                        try {
                            String ss2 = String.valueOf(selectedTextView.getText().toString().charAt(ed_pos - 2));
                            if (ss2.equals(str_integral)) {
                                selectedTextView.getText().delete(ed_pos - 2, ed_pos);
                            } else {
                                selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                            }
                        } catch (Exception e) {
                            selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                        }
                    } else {
                        selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                    }
                } else {
                    selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                }
                try {
                    if (selectedTextView.getText().toString().trim().length() == 0) {
                        selectedTextView.setSelection(0);
                    }
                } catch (Exception e) {
                }

                if (is_touch_kb) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ondeleteclick();
                        }
                    }, 150);
                }

            } else if (ed_pos == 0 && ed_legnth == 0) {

                int pos_sub_ll = ll_main_ll.indexOfChild(ll_sub_ll);
                Log.e("delete4", "" + pos_sub_ll);

                if (pos_sub_ll != 0 && pos_sub_ll != -1) {

                    LinearLayout ll_sub = (LinearLayout) ll_main_ll.getChildAt(pos_sub_ll - 1);
                    onpresubcall(ll_sub);
                    ll_main_ll.removeViewAt(pos_sub_ll);

                } else if (pos_sub_ll == 0) {

                    String main_tag = (String) ll_main_ll.getTag(R.id.second);

                    Log.e("delete5", "" + main_tag);

                    if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                        if (ll_main_ll.getChildCount() > 1) {
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        }

                    } else if (main_tag.equals("nth_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                        if (ll_main_ll.getChildCount() > 1) {
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        }

                    } else if (main_tag.equals("lim_right")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                        if (ll_main_ll.getChildCount() > 1) {
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        }

                    } else if (main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                            || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                            || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {

                        try {
                            LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            LinearLayout ll_main2 = (LinearLayout) ll_main.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_frac_top(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {

                                if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                    sub_pre_frac_top(ll_main_ll, 2, false);
                                    ll_main2.removeViewAt(main_pos);
                                    this.add_ed_after_delete(ll_main2, 18, main_pos);
                                } else {
                                    if (main_pos != 0) {
                                        sub_pre_frac_top(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                    } else {
                                        sub_pre_frac_top(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                        try {
                                            final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                            if (lll.getTag().toString().equals("text")) {
                                                EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                edd.setMinWidth(20);
                                                edd.setMinimumWidth(20);
                                                edd.requestLayout();
                                                edd.requestFocus();
                                                if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                    Utils.change_edittext_bg(true, edd);
                                                }
                                            }
                                        } catch (Exception e) {
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("sqrt_c")) {

                        try {

                            final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            final LinearLayout ll_main1 = (LinearLayout) ll_main.getParent();
                            final LinearLayout ll_main2 = (LinearLayout) ll_main1.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main1);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_sqrt_c(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {
                                if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                    Log.e("edd", "" + main_pos);
                                    sub_pre_sqrt_c(ll_main_ll, 2, false);
                                    ll_main2.removeViewAt(main_pos);
                                    Log.e("edd1", "" + main_pos);
                                    this.add_ed_after_delete(ll_main2, 18, main_pos);
                                } else {
                                    if (main_pos != 0) {
                                        sub_pre_sqrt_c(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                    } else {
                                        sub_pre_sqrt_c(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                        try {
                                            final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                            if (lll.getTag().toString().equals("text")) {
                                                final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                edd.setMinWidth(20);
                                                edd.setMinimumWidth(20);
                                                edd.requestLayout();
                                                edd.requestFocus();
                                                if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                    Utils.change_edittext_bg(true, edd);
                                                }
                                            }
                                        } catch (Exception e) {
                                        }
                                    }
                                }
                            }

                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("nth_top")) {

                        try {
                            final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            final LinearLayout ll_main2 = (LinearLayout) ll_main.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_frac_top(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {
                                final LinearLayout ll_sqrt = (LinearLayout) ll_main.getChildAt(2);
                                final LinearLayout ll_sqrt2 = (LinearLayout) ll_sqrt.getChildAt(1);
                                if (this.makeansfromview(ll_sqrt2).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_frac_top(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            }

                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("lim_left")) {

                        try {
                            final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            final LinearLayout ll_main1 = (LinearLayout) ll_main.getParent();
                            final LinearLayout ll_main2 = (LinearLayout) ll_main1.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main1);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_sqrt_c(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {
                                final LinearLayout ll_lim_rht = (LinearLayout) ll_main.getChildAt(2);
                                if (this.makeansfromview(ll_lim_rht).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_sqrt_c(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_sqrt_c(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_sqrt_c(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            }

                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("frac_top") || main_tag.equals("ss_top")) {

                        final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        final LinearLayout ll_main2 = (LinearLayout) ll_main.getParent();
                        int main_pos = ll_main2.indexOfChild(ll_main);

                        if (ll_main_ll.getChildCount() > 1) {
                            sub_pre_frac_top(ll_main_ll, 0, false);
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        } else {
                            if (main_tag.equals("frac_top")) {
                                LinearLayout ll_frac = (LinearLayout) ll_main.getChildAt(2);
                                if (this.makeansfromview(ll_frac).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_frac_top(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            } else {
                                LinearLayout ll_frac = (LinearLayout) ll_main.getChildAt(1);

                                if (this.makeansfromview(ll_frac).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_frac_top(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            }
                        }
                    }
                }

                if (is_touch_kb) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ondeleteclick();
                        }
                    }, 150);
                }
            } else {
                this.onpreviousclick();
                if (is_touch_kb) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ondeleteclick();
                        }
                    }, 150);
                }
            }

        } catch (Exception e) {
        }
    }

    public void onpreviousclick() {

        parenth = false;

        try {
            int cur_pos = 0;

            try {
                cur_pos = selectedTextView.getSelectionStart();
            } catch (Exception e) {
            }

            if (cur_pos != 0) {

                String str_bar_1 = "" + Html.fromHtml("&#x0304;");
                String str_bar_2 = "" + Html.fromHtml("&#772;");

                if (selectedTextView.getText().toString().length() > 1) {
                    String ss = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos - 1));
                    if (ss.equals(str_bar_1) || ss.equals(str_bar_2)) {

                        selectedTextView.setSelection((cur_pos - 2));

                    } else {
                        selectedTextView.setSelection((cur_pos - 1));
                    }
                } else {
                    selectedTextView.setSelection((cur_pos - 1));
                }

                if (is_touch_kb_pre) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onpreviousclick();
                        }
                    }, 150);
                }

            } else {

                final LinearLayout ll_main_ll = ll_ans_box_selected;
                final LinearLayout ll_sub_ll = ll_selected_view;

                int pos_sub_ll = ll_main_ll.indexOfChild(ll_sub_ll);

                if (pos_sub_ll != 0 && pos_sub_ll != -1) {

                    LinearLayout ll_sub = (LinearLayout) ll_main_ll.getChildAt(pos_sub_ll - 1);
                    onpresubcall(ll_sub);

                } else if (pos_sub_ll == 0) {

                    String main_tag = (String) ll_main_ll.getTag(R.id.second);

                    if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                    } else if (main_tag.equals("nth_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                    } else if (main_tag.equals("lim_right")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                    } else if (main_tag.equals("sqrt_c") || main_tag.equals("lim_left")) {

                        try {
                            sub_pre_sqrt_c(ll_main_ll, 0, true);
                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("frac_top") || main_tag.equals("ss_top") ||
                            main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("nth_top") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                            || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                            || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {

                        try {
                            sub_pre_frac_top(ll_main_ll, 0, true);
                        } catch (Exception e) {
                        }

                    }
                }

                if (is_touch_kb_pre) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onpreviousclick();
                        }
                    }, 150);
                }

            }

        } catch (Exception e) {
        }

    }

    public void hideDeviceKeyboardAndClearFocus() {
        try {
            //this.cleasFocusFromEditText();
            this.hideDeviceKeyboard();
            this.hideKeyboard();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void AlertScript() {
        /*final Dialog dialog = new Dialog(ActAssignCustomAns.this);
        dialog.setContentView(R.layout.script_alert_dialoug);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_title);
        TextView dialogButton = (TextView) dialog.findViewById(R.id.tv_ok);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                parenth = false;
                hideDeviceKeyboardAndClearFocus();
            }
        });
        dialog.show();*/

        MathFriendzyHelper.showWarningDialog(this,
                MathFriendzyHelper.getTreanslationTextById(this, "lblProbWithAns"),
                new HttpServerRequest() {
                    @Override
                    public void onRequestComplete() {
                        parenth = false;
                        hideDeviceKeyboardAndClearFocus();
//                        ActCheckHomeWork.this.cleasFocusFromEditText();
                    }
                });
    }

    private void BlankFieldAlert() {
        /*final Dialog dialog = new Dialog(ActAssignCustomAns.this);
        dialog.setContentView(R.layout.blank_field_alert_dialoug);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView text = (TextView) dialog.findViewById(R.id.tv_title_blank);

        TextView dialogButton = (TextView) dialog.findViewById(R.id.tv_ok_blank);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                parenth = false;
                hideDeviceKeyboardAndClearFocus();
            }
        });
        dialog.show();*/

        MathFriendzyHelper.showWarningDialog(this,
                MathFriendzyHelper.getTreanslationTextById(this, "lblLeavingFieldEmpty"),
                new HttpServerRequest() {
                    @Override
                    public void onRequestComplete() {
                        parenth = false;
                        //hideDeviceKeyboardAndClearFocus();
                    }
                });
    }

    //offline changes for links
    private void setUrlLinks(StudentAnsBase baseAns , CustomePlayerAns playerAns){
        try {
            baseAns.setUrlLinks(playerAns.getUrlLinks());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}