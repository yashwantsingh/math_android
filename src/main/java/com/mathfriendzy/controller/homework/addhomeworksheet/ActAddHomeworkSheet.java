package com.mathfriendzy.controller.homework.addhomeworksheet;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.homework.assignhomeworkquiz.ActAssignHomework;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.helper.QuizzSelectionInterface;
import com.mathfriendzy.imageoperation.ImageOperation;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.pdfoperation.PDFOperation;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.ServerDialogs;
import com.mathfriendzy.utils.CommonUtils;

import java.io.File;
import java.util.ArrayList;

public class ActAddHomeworkSheet extends ActBase {

	private final String TAG = this.getClass().getSimpleName();

	private ViewPager viewPager = null;
	private Button btnCrop 		= null;
	private Button btnDelete 	= null;
	private Button btnAddMore 	= null;
	private Button btnDone 		= null;
	private Button btnRotate 	= null;
	private TextView txtNoOfSelection = null;

	private ArrayList<String> imegeListUrls = null;

	private String lblCamera  = null;
	private String lblGallery = null;

	private final int PICK_IMAGE = 1001;
	private final int CAPTURE_IMAGE = 1002;
	private final int CROP_IMAGE = 1003;

	private ViewPagerAdapter adapter = null;
	private int displayIndex = 0;

	private ProgressBar loadingDialog = null;

	private boolean isRotateClick = false;

	private int clickedPosition = 0;
	private String questionSheetName = "";

	private ProgressDialog pd = null;

	private Uri uri = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_act_add_homework_sheet);

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside onCreate()");

		this.getIntentValues();
		this.init();
		this.setWidgetsReferences();
		this.setTextFromTranslation();
		this.setListenerOnWidgets();
		this.showDialogForSelectPicture();
		this.setAdapter();
		this.setNumberOfSelectedImages();

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside onCreate()");
	}

	/**
	 * Get the intent values which are set in previous screen
	 */
	@SuppressWarnings("unchecked")
	private void getIntentValues() {
		imegeListUrls = (ArrayList<String>) this.getIntent()
				.getSerializableExtra("workSheetImageList");	
		clickedPosition = this.getIntent().getIntExtra("clickedPosition", 0);
	}

	/**
	 * Initialize the variables
	 */
	private void init() {
		if(imegeListUrls == null || imegeListUrls.size() == 0){
			imegeListUrls = new ArrayList<String>();
		}
	}

	@Override
	protected void setWidgetsReferences() {

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside onCreate()");

		txtTopbar 	= (TextView) findViewById(R.id.txtTopbar);
		btnCrop     = (Button) findViewById(R.id.btnCrop);
		btnDelete   = (Button) findViewById(R.id.btnDelete);
		btnAddMore  = (Button) findViewById(R.id.btnAddMore);
		btnDone     = (Button) findViewById(R.id.btnDone);
		btnRotate   = (Button) findViewById(R.id.btnRotate);
		txtNoOfSelection = (TextView) findViewById(R.id.txtNoOfSelection);

		viewPager   = (ViewPager) findViewById(R.id.viewPager);

		loadingDialog = (ProgressBar) findViewById(R.id.loadingDialog);

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside onCreate()");

	}

	@Override
	protected void setTextFromTranslation() {

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside setTextFromTranslation()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		txtTopbar.setText("");
		btnCrop.setText(transeletion.getTranselationTextByTextIdentifier("lblCrop"));
		btnDelete.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleDelete"));
		btnAddMore.setText(transeletion.getTranselationTextByTextIdentifier("lblAddMore"));
		btnDone.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleDone"));

		lblCamera  = transeletion.getTranselationTextByTextIdentifier("lblCamera");
		lblGallery = transeletion.getTranselationTextByTextIdentifier("lblGallery");

		transeletion.closeConnection();

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside setTextFromTranslation()");
	}

	/**
	 * Selection Dialog
	 */
	private void selectionDialog(){
		MathFriendzyHelper.showHomeworkQuizzSelectionDialog
		(this, new QuizzSelectionInterface() {

			//on Gallery Selection
			@Override
			public void onWordProblemSelected() {
				onGallerySelected();
			}

			//on Camera selection
			@Override
			public void onPracticeSkillSelected() {
				onCameraSelected();
			}

			@Override
			public void onCustomeHomeworkSelected() {
				// TODO Auto-generated method stub
			}
		}, lblCamera, lblGallery, "");
	}

	/**
	 * Show the dialog for image selection at first time screen created
	 */
	private void showDialogForSelectPicture(){
		if(imegeListUrls.size() == 0){
			this.selectionDialog();
		}
	}

	/**
	 * Call when camera selected to Add image
	 */
	@SuppressLint({ "InlinedApi", "SdCardPath" })
	private void onCameraSelected(){
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		Uri fileUri = MathFriendzyHelper.getOutputMediaFileUri
				(MathFriendzyHelper.MEDIA_TYPE_IMAGE); // create a file to save the image
		uri = fileUri;
		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);// set the image file name
		startActivityForResult(intent, CAPTURE_IMAGE);
	}

	/**
	 * Call when Gallery Selected to Add image
	 */
	private void onGallerySelected(){
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(Intent.createChooser
				(intent, "Select Picture"), PICK_IMAGE);
	}

	@Override
	protected void setListenerOnWidgets() {

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside setListenerOnWidgets()");

		btnCrop.setOnClickListener(this);
		btnDelete.setOnClickListener(this);
		btnAddMore.setOnClickListener(this);
		btnDone.setOnClickListener(this);
		btnRotate.setOnClickListener(this);

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside setListenerOnWidgets()");

	}

	/**
	 * Initialize the display index
	 */
	private void initDisplayImageIndexAfterAddingImageToList(){
		displayIndex = imegeListUrls.size() - 1;
	}

	/**
	 * Initialize the display index
	 */
	private void initDisplayImageIndexAfterRemovingImageFromList(){
		if(imegeListUrls.size() == 1){
			displayIndex = 0;
		}else{
			if(displayIndex >= imegeListUrls.size()){
				displayIndex = imegeListUrls.size() - 1;
			}
		}
	}

	/**
	 * Set the current index from the total number of images
	 */
	private void setNumberOfSelectedImages(){
		if(imegeListUrls.size() > 0){
			txtNoOfSelection.setText((displayIndex  + 1 ) + " of " + imegeListUrls.size());
		}else{
			txtNoOfSelection.setText(0 + " of " + imegeListUrls.size());
		}
	}

	/**
	 * Set the adapter
	 */
	private void setAdapter(){
		try{
			//if(adapter == null){
			adapter = new ViewPagerAdapter(this, imegeListUrls);
			viewPager.setAdapter(adapter);
			/*}else{
				adapter.notifyDataSetChanged();
			}*/

			viewPager.setCurrentItem(displayIndex);
			viewPager.setOnPageChangeListener(new OnPageChangeListener() {
				@Override
				public void onPageSelected(int position) {
					displayIndex = viewPager.getCurrentItem();
					setNumberOfSelectedImages();
				}

				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {
					// TODO Auto-generated method stub
				}

				@Override
				public void onPageScrollStateChanged(int arg0) {
					// TODO Auto-generated method stub

				}
			});
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Add new image url at the existing list
	 * @param newImageUrl
	 */
	private void addUrlToImageListAtLast(String newImageUrl){
		try{
			if(newImageUrl.startsWith(MathFriendzyHelper.IMAGE_FILE_PREFF))
				imegeListUrls.add(newImageUrl);
			else
				imegeListUrls.add(MathFriendzyHelper.IMAGE_FILE_PREFF + newImageUrl);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Replace the old url with new one
	 * @param index
	 * @param newImageUrl
	 */
	private void replaceNewUriWithexisting(int index , String newImageUrl){
		try{
			if(newImageUrl.startsWith(MathFriendzyHelper.IMAGE_FILE_PREFF))
				imegeListUrls.set(index, newImageUrl);
			else
				imegeListUrls.set(index ,
						MathFriendzyHelper.IMAGE_FILE_PREFF + newImageUrl);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * helper to retrieve the path of an image URI
	 */
	public String getPath(Uri uri) {
		if( uri == null ) {
			return null;
		}
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
		if( cursor != null ){
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		}
		return uri.getPath();
	}

	/**
	 * Remove image
	 */
	private void removeImage(){
		try{
			if(imegeListUrls != null){ 
				if(imegeListUrls.size() > 0){
					imegeListUrls.remove(displayIndex);
					this.initDisplayImageIndexAfterRemovingImageFromList();
					this.setAdapter();
					this.setNumberOfSelectedImages();
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Rotate the image
	 * @param url
	 */
	private void rotateImage(final String url){

		new AsyncTask<Void, Void, Void>() {

			protected void onPreExecute() {
				loadingDialog.setVisibility(ProgressBar.VISIBLE);
			}

			@Override
			protected Void doInBackground(Void... params) {
				Bitmap rotateBitmap = ImageOperation.rotateImage
						(ImageOperation.getBitmapByFilePath
								(ActAddHomeworkSheet.this, url), 90);
				ImageOperation.saveImageInSdCard
				(ActAddHomeworkSheet.this, url , rotateBitmap , 
						new HttpServerRequest() {
					@Override
					public void onRequestComplete() {
						isRotateClick = false;
					}
				});
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				loadingDialog.setVisibility(ProgressBar.GONE);
				setAdapter();
				super.onPostExecute(result);
			}
		}.execute();
	}

	/**
	 * Return the replace file uri with File://
	 * @param url
	 * @return
	 */
	private String getFileUriAfterRepacingFilePreff(String url){
		return url.replaceAll(MathFriendzyHelper.IMAGE_FILE_PREFF , "");
	}

	/**
	 * Rotate the image
	 */
	private void doRotation(){
		try{
			if(imegeListUrls.size() > 0){
				if(!isRotateClick){
					isRotateClick = true;
					this.rotateImage(imegeListUrls.get(displayIndex).
							replaceAll(MathFriendzyHelper.IMAGE_FILE_PREFF , ""));
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Do the cropping
	 */
	private void doCroping(){
		try{
			if(imegeListUrls.size() > 0){
				ImageOperation.performCropImage(
						getFileUriAfterRepacingFilePreff(imegeListUrls.get(displayIndex)),
						this, CROP_IMAGE);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}


	/**
	 * Update UI after capture image from camera
	 * @param
	 */
	private void updateUIAfterCaptureImage(final String url){

		new AsyncTask<Void, Void, Void>() {

			protected void onPreExecute() {
				loadingDialog.setVisibility(ProgressBar.VISIBLE);
			}

			@Override
			protected Void doInBackground(Void... params) {
				Bitmap bitmap = ImageOperation.getBitmapByFilePath
						(ActAddHomeworkSheet.this, url);
				Bitmap rotateBitmap = ImageOperation
						.imageOreintationValidator(bitmap , url);

				ImageOperation.saveImageInSdCard
				(ActAddHomeworkSheet.this, url , rotateBitmap , 
						new HttpServerRequest() {
					@Override
					public void onRequestComplete() {

					}
				});
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				loadingDialog.setVisibility(ProgressBar.GONE);
				addUrlToImageListAtLast(url);
				initDisplayImageIndexAfterAddingImageToList();
				setAdapter();
				setNumberOfSelectedImages();
				super.onPostExecute(result);
			}
		}.execute();
	}

	/**
	 * Create PDF
	 */
	private int createPdf(){
		try{
			if(imegeListUrls != null 
					&& imegeListUrls.size() > 0){
				String pdfName = ActAssignHomework.getQuestionSheetName(clickedPosition);
				questionSheetName = pdfName;
				return PDFOperation.createPdfForImages
				(this, imegeListUrls, (pdfName + ".pdf"));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
        return 0;
	}

	/**
	 * Create pdf asyncktask
	 */
	private void createAsynckTaskPdf(){
		new AsyncTask<Void, Void, Integer>() {

			@Override
			protected void onPreExecute() {
				try{
					pd = ServerDialogs.getProgressDialog
							(ActAddHomeworkSheet.this, "", 1);
					pd.show();
				}catch(Exception e){
					e.printStackTrace();
				}
				super.onPreExecute();
			}

			@Override
			protected Integer doInBackground(Void... params) {
				return createPdf();
			}

			@Override
			protected void onPostExecute(Integer result) {
				try{
					if(pd != null && pd.isShowing()){
						pd.cancel();
						pd.dismiss();
					}

                    if(result.intValue() == PDFOperation.OUT_OF_MEMORY){
                        String message = MathFriendzyHelper.getTreanslationTextById(ActAddHomeworkSheet.this ,
                                "lblYourFileCantLarger");
                        MathFriendzyHelper.showWarningDialog(ActAddHomeworkSheet.this , message);
                        return ;
                    }

                    String pdfName = ActAssignHomework.getQuestionSheetName(clickedPosition);
                    if(MathFriendzyHelper.getPDFFileSizeInMB(ActAddHomeworkSheet.this , pdfName) >
                            MathFriendzyHelper.MAX_PDF_FILE_SIZE_IN_MB){
                        String message = MathFriendzyHelper.getTreanslationTextById(ActAddHomeworkSheet.this ,
                                "lblYourFileCantLarger");
                        MathFriendzyHelper.showWarningDialog(ActAddHomeworkSheet.this , message);
                        return ;
                    }
				}catch(Exception e){
					e.printStackTrace();
				}
				clickOnDone();
				super.onPostExecute(result);
			}
		}.execute();
	}

	/**
	 * onDone clicked
	 */
	private void clickOnDone(){
		Intent intent = new Intent();
		intent.putExtra("workSheetImageList", imegeListUrls);
		intent.putExtra("sheetName", questionSheetName);
		setResult(RESULT_OK, intent);
		finish();
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnCrop:
			this.doCroping();
			break;
		case R.id.btnDelete:
			this.removeImage();
			break;
		case R.id.btnAddMore:
			this.selectionDialog();
			break;
		case R.id.btnDone:
			//this.createPdf();
			//this.clickOnDone();
			this.createAsynckTaskPdf();
			break;
		case R.id.btnRotate:
			this.doRotation();			
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == RESULT_OK){
			switch(requestCode){
			case PICK_IMAGE:
				String pickImageUrl = MathFriendzyHelper.getPath(this, data.getData());
				if(pickImageUrl != null){
					this.addUrlToImageListAtLast(pickImageUrl);
					this.initDisplayImageIndexAfterAddingImageToList();
					this.setAdapter();
					this.setNumberOfSelectedImages();
				}
				break;
			case CAPTURE_IMAGE:
				String capturedImageUrl = MathFriendzyHelper.getImagePathUsingUri(this, uri);
				if(capturedImageUrl != null){
					this.updateUIAfterCaptureImage(capturedImageUrl.
							replace(MathFriendzyHelper.IMAGE_FILE_PREFF, ""));
				}
				break;
			case CROP_IMAGE:
				File file = ImageOperation.getNewCropFile();
				if(file != null){
					this.replaceNewUriWithexisting(displayIndex, file.getAbsolutePath());
					ImageOperation.setNewCropFileToNull();
				}
				this.setAdapter();
				break;
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void serverResponse(HttpResponseBase httpResponseBase,
			int requestCode) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onBackPressed() {
		//this.clickOnDone();
		this.createAsynckTaskPdf();
		//super.onBackPressed();
	}
}
