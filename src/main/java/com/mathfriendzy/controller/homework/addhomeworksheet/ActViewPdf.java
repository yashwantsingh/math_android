package com.mathfriendzy.controller.homework.addhomeworksheet;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.artifex.mupdf.MuPDFCore;
import com.artifex.mupdf.MuPDFPageAdapter;
import com.artifex.mupdf.view.DocumentReaderView;
import com.artifex.mupdf.view.ReaderView;
import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.homework.ActCheckHomeWork;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.imageoperation.ImageOperation;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.pdfoperation.PDFOperation;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.ServerDialogs;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.zoomview.ZoomView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.File;

public class ActViewPdf extends ActBase {

    private final String TAG = this.getClass().getSimpleName();

    //private ListView lstView = null;
    private RelativeLayout lstView = null;
    private Button btnSelect = null;
    private RelativeLayout bottombar = null;

    private MuPDFCore core;
    private MuPDFCore cores[];
    private MuPDFPageAdapter mDocViewAdapter;
    private ReaderView docView;

    //private String paths[] = { PDFOperation.getPDFSavePath(this) + "/375_questions_1.pdf" };
    private String paths[] 		= null;
    private String pdfFileName 	= null;

    private final int PERFORM_CROP = 1001;
    private String cropFileUri = "";
    private ProgressDialog pd;
    private String lblThisToolAllowsToCopyImage = null;

    //for image selection dialog
    private boolean isForImageSelection = false;
    //private ImageView imgPinchToZoomImage = null;
    private boolean isForImageView = false;
    private ImageLoader loader;//images loader
    private DisplayImageOptions options; // contains the display option for image in grid
    private ProgressBar loading = null;

    private TextView txtZoomWithYourFinger = null;
    private RelativeLayout topbar = null;

    private TextView txtNumberOfPagesShown = null;
    private Handler hideHanlder = null;
    private long timeToHide = 2 * 1000;
    private int totalPage = 0;
    private int currentPageIndex = 0;

    private Button btnRotate = null;

    private ZoomView zoomView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_view_pdf);

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside onCreate()");

        this.getIntentValues();
        this.init();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.setVisibilityOfBottomBar();

        if(isForImageView){
            this.setVisibilityOfRotateButton(true);
            this.setZoomView();
        }else {
            this.setVisibilityOfRotateButton(false);
            this.setPdfAdapter();
        }
        //this.setViewVisibility();

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside onCreate()");
    }


    private void setVisibilityOfBottomBar() {
        if(isForImageSelection){
            bottombar.setVisibility(RelativeLayout.VISIBLE);
            topbar.setVisibility(RelativeLayout.GONE);
        }else{
            bottombar.setVisibility(RelativeLayout.GONE);
            topbar.setVisibility(RelativeLayout.VISIBLE);
        }
    }

    /**
     * Get the intent values which are set in previous screen
     */
    private void getIntentValues() {
        //for selection image from dialog
        isForImageSelection = this.getIntent().getBooleanExtra("isForImageSelection" , false);

        isForImageView = this.getIntent().getBooleanExtra("isForImageView" , false);
        pdfFileName = this.getIntent().getStringExtra("pdfFileName");

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "pdf file name " + pdfFileName);
    }

    private void setViewVisibility(){
        if(isForImageView){
            //imgPinchToZoomImage.setVisibility(ImageView.VISIBLE);
            lstView.setVisibility(RelativeLayout.GONE);
        }else{
            //imgPinchToZoomImage.setVisibility(ImageView.GONE);
            lstView.setVisibility(RelativeLayout.VISIBLE);
        }
    }

    /**
     * Initialize the variable
     */
    private void init() {
        if(!isForImageView) {
            paths = new String[]{PDFOperation.getPDFSavePath(this) + "/" +
                    PDFOperation.getPdfFormattedFileName(pdfFileName)};
        }

        loader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.ic_empty)
                .showImageOnFail(R.drawable.ic_error)
                .resetViewBeforeLoading(true)
		        /*.cacheOnDisc(true)*/
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .considerExifParams(true)
                .displayer(new FadeInBitmapDisplayer(300))
                .build();

        this.initializeHandler();
    }

    @Override
    protected void setWidgetsReferences() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside setWidgetsReferences()");

        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        lstView = (RelativeLayout) findViewById(R.id.lstView);
        btnSelect = (Button) findViewById(R.id.btnSelect);
        loading = (ProgressBar) findViewById(R.id.loading);
        bottombar = (RelativeLayout) findViewById(R.id.bottombar);
        //imgPinchToZoomImage = (ImageView) findViewById(R.id.imgPinchToZoomImage);
        txtZoomWithYourFinger = (TextView) findViewById(R.id.txtZoomWithYourFinger);
        topbar = (RelativeLayout) findViewById(R.id.topbar);

        txtNumberOfPagesShown = (TextView) findViewById(R.id.txtNumberOfPagesShown);

        btnRotate = (Button) findViewById(R.id.btnRotate);

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside setWidgetsReferences()");

    }

    @Override
    protected void setListenerOnWidgets() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside setListenerOnWidgets()");

        btnSelect.setOnClickListener(this);
        btnRotate.setOnClickListener(this);
        //imgPinchToZoomImage.setOnTouchListener(this);

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside setListenerOnWidgets()");

    }

    @Override
    protected void setTextFromTranslation() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside setTextFromTranslation()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblView")
                + " " + transeletion.getTranselationTextByTextIdentifier("lblHomework")
                + " " + transeletion.getTranselationTextByTextIdentifier("lblSheets"));
        btnSelect.setText(transeletion.getTranselationTextByTextIdentifier("lblSelect"));
        lblThisToolAllowsToCopyImage = transeletion.getTranselationTextByTextIdentifier
                ("lblThisToolAllowsToCopyImage");
        txtZoomWithYourFinger.setText(transeletion.getTranselationTextByTextIdentifier
                ("lblSwipeImageToEnlarge"));
        transeletion.closeConnection();

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside setTextFromTranslation()");

    }

    /**
     * Set the pdf view adapter
     */
    private void setPdfAdapter() {
        try{
            cores = new MuPDFCore[paths.length];
            for(int i = 0 ; i < paths.length ; i++){
                cores[i] = openFile(paths[i]);
            }

            docView = new DocumentReaderView(this){
                @Override
                protected void onMoveToChild(View view, int i){
                    super.onMoveToChild(view, i);
                    try {
                        currentPageIndex = i;
                        setTextToNumberOfText(totalPage, i);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

                @Override
                public boolean onScroll(MotionEvent e1, MotionEvent e2
                        ,float distanceX, float distanceY){
                    try {
                        setTextToNumberOfText(totalPage, currentPageIndex);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    return super.onScroll(e1, e2, distanceX, distanceY);
                }

                @Override
                protected void onContextMenuClick(){

                }

                @Override
                protected void onBuy(String path){

                }
            };

            mDocViewAdapter = new MuPDFPageAdapter(this, cores);
            //lstView.setAdapter(mDocViewAdapter);
            docView.setAdapter(mDocViewAdapter);
            lstView.addView(docView);

            try{
                totalPage = mDocViewAdapter.getCount();
            }catch (Exception e){
                e.printStackTrace();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Set zoom view
     */
    private void setZoomView(){
        View v = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.image_zoom_view_layout, null, false);
        ImageView imgZoomImage = (ImageView) v.findViewById(R.id.imgZoomImage);
        this.setImageFromUri(pdfFileName , imgZoomImage);
        if(zoomView == null) {
            zoomView = new ZoomView(this);
        }
        zoomView.removeAllViews();
        zoomView.addView(v);
        lstView.removeAllViews();
        lstView.addView(zoomView);
    }

    /**
     * For loading image from uri
     * @param uri
     * @param imageView
     */
    private void setImageFromUri(String uri , ImageView imageView){
        if(!uri.startsWith(MathFriendzyHelper.IMAGE_FILE_PREFF))
            uri = MathFriendzyHelper.IMAGE_FILE_PREFF + uri;
        loader.displayImage(uri, imageView, options,
                new ImageLoadingListener() {

                    @Override
                    public void onLoadingStarted(String arg0, View arg1) {
                        loading.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
                        loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingCancelled(String arg0, View arg1) {
                        // TODO Auto-generated method stub
                    }
                });
    }


    /*private void setImageView(){
        Log.e(TAG , "Set image view");
    }*/

    /**
     * open the file on the given path
     * @param path
     * @return
     */
    private MuPDFCore openFile(String path){
        try{
            core = new MuPDFCore(path);
        } catch (Exception e) {
            return null;
        }
        return core;
    }

    /**
     * Click on Select
     * First take screen shot then save it into SD card , and the open the
     * cropping intent
     */
    private void clickOnSelect(){
        try{
            new AsyncTask<Void, Void, File>(){

                @Override
                protected void onPreExecute() {
                    pd = ServerDialogs.getProgressDialog
                            (ActViewPdf.this, "", 1);
                    pd.show();
                    super.onPreExecute();
                }

                @Override
                protected File doInBackground(Void... params) {
                    File file = MathFriendzyHelper.saveScreenShot(MathFriendzyHelper.
                                    takeScreenShot(ActViewPdf.this, lstView),
                            MathFriendzyHelper.getUniqueScreenShotName());
                    return file;
                }

                @Override
                protected void onPostExecute(final File file) {
                    if(pd != null && pd.isShowing()){
                        pd.cancel();
                    }

                    if(!ActCheckHomeWork.isNeedToShowPopUp){
                        ActCheckHomeWork.isNeedToShowPopUp = true;
                        MathFriendzyHelper.warningDialog(ActViewPdf.this,
                                lblThisToolAllowsToCopyImage , new HttpServerRequest() {
                                    @Override
                                    public void onRequestComplete() {
                                        //cropFileUri = file.getAbsolutePath();
                                        ImageOperation.performCropImage
                                                (file.getAbsolutePath(), ActViewPdf.this, PERFORM_CROP);
                                    }
                                });
                    }else{
                        //cropFileUri = file.getAbsolutePath();
                        ImageOperation.performCropImage
                                (file.getAbsolutePath(), ActViewPdf.this, PERFORM_CROP);
                    }
                    super.onPostExecute(file);
                }
            }.execute();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnSelect:
                try {
                    this.removeCallback();
                    this.setVisibilityOfTxtNumberOfPagesShown(false);
                }catch (Exception e){
                    e.printStackTrace();
                }

                if(isForImageView){
                    this.getScreenShotOfZoomImage(new ZoomImageScreenShot() {
                        @Override
                        public void onScreenShotComplete(File file) {
                            ImageOperation.performCropImage
                                    (file.getAbsolutePath(), ActViewPdf.this, PERFORM_CROP);
                        }
                    });
                }else {
                    this.clickOnSelect();
                }
                break;
            case R.id.btnRotate:
                this.doRotation();
                break;
        }
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase,
                               int requestCode) {

    }

    private void showProgressDialog(){
        pd = ServerDialogs.getProgressDialog
                (ActViewPdf.this, "", 1);
        pd.show();
        //loading.setVisibility(ProgressBar.VISIBLE);
    }

    private void hideProgressDialog(){
        if(pd != null && pd.isShowing()){
            pd.cancel();
        }
        //loading.setVisibility(ProgressBar.GONE);
    }

    private void getScreenShotOfZoomImage(final ZoomImageScreenShot callback){
        this.showProgressDialog();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        File file = MathFriendzyHelper.saveScreenShot(MathFriendzyHelper.
                                        takeScreenShot(ActViewPdf.this, lstView),
                                MathFriendzyHelper.getUniqueScreenShotName());
                        hideProgressDialog();
                        callback.onScreenShotComplete(file);
                    }
                });
            }
        } , 100);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            switch(requestCode){
                case PERFORM_CROP:
                    File file = ImageOperation.getNewCropFile();
                    if(file != null){
                        cropFileUri = file.getAbsolutePath();
                        ImageOperation.setNewCropFileToNull();
                    }
                    if(isForImageSelection){
                        this.onBackPressed();
                    }
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        this.removeCallback();
        Intent intent = new Intent();
        intent.putExtra("cropImageUri", cropFileUri);
        setResult(RESULT_OK, intent);
        finish();
        super.onBackPressed();
    }

    interface ZoomImageScreenShot{
        void onScreenShotComplete(File file);
    }

    private void initializeHandler(){
        try{
            hideHanlder = new Handler();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setVisibilityOfTxtNumberOfPagesShown(boolean isShow){
        if(isShow){
            txtNumberOfPagesShown.setVisibility(TextView.VISIBLE);
        }else{
            txtNumberOfPagesShown.setVisibility(TextView.GONE);
        }
    }

    private void hideNumberOfText(){
        try {
            this.removeCallback();
            hideHanlder.postDelayed(new Runnable() {
                @Override
                public void run() {
                    setVisibilityOfTxtNumberOfPagesShown(false);
                }
            }, timeToHide);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void removeCallback(){
        try {
            if (hideHanlder != null) {
                hideHanlder.removeCallbacksAndMessages(null);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setTextToNumberOfText(int total , int index){
        try{
            this.removeCallback();
            txtNumberOfPagesShown.setVisibility(TextView.VISIBLE);
            txtNumberOfPagesShown.setText((index + 1) + " of " + total);
            this.hideNumberOfText();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //change for rotate image
    private boolean isRotateClick = false;
    private void setVisibilityOfRotateButton(boolean isVisible){
        if(isVisible){
            btnRotate.setVisibility(Button.VISIBLE);
        }else{
            btnRotate.setVisibility(Button.GONE);
        }
    }

    private void doRotation(){
        try{
            if(!MathFriendzyHelper.isEmpty(pdfFileName)){
                if(!isRotateClick){
                    isRotateClick = true;
                    this.rotateImage(pdfFileName.
                            replaceAll(MathFriendzyHelper.IMAGE_FILE_PREFF , ""));
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Rotate the image
     * @param url
     */
    private void rotateImage(final String url){

        new AsyncTask<Void, Void, Void>() {

            private ProgressDialog pd = null;

            protected void onPreExecute() {
                //loadingDialog.setVisibility(ProgressBar.VISIBLE);
                pd = MathFriendzyHelper.getProgressDialog(ActViewPdf.this , "");
                MathFriendzyHelper.showProgressDialog(pd);
            }

            @Override
            protected Void doInBackground(Void... params) {
                Bitmap rotateBitmap = ImageOperation.rotateImage
                        (ImageOperation.getBitmapByFilePath
                                (ActViewPdf.this, url), 90);
                ImageOperation.saveImageInSdCard
                        (ActViewPdf.this, url , rotateBitmap ,
                                new HttpServerRequest() {
                                    @Override
                                    public void onRequestComplete() {
                                        isRotateClick = false;
                                    }
                                });
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                //loadingDialog.setVisibility(ProgressBar.GONE);
                MathFriendzyHelper.hideProgressDialog(pd);
                setZoomView();
                super.onPostExecute(result);
            }
        }.execute();
    }
}
