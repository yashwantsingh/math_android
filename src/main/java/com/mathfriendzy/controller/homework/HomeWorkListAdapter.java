package com.mathfriendzy.controller.homework;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.HomeworkClickListener;
import com.mathfriendzy.model.homework.CustomeResult;
import com.mathfriendzy.model.homework.GetHomeWorkForStudentWithCustomeResponse;
import com.mathfriendzy.model.homework.PracticeResult;
import com.mathfriendzy.model.homework.PracticeResultSubCat;
import com.mathfriendzy.model.homework.PracticeSkillSortByCategoryId;
import com.mathfriendzy.model.homework.WordResult;
import com.mathfriendzy.model.homework.WordSubCatResult;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class HomeWorkListAdapter extends BaseAdapter {

    public static ArrayList<GetHomeWorkForStudentWithCustomeResponse> homeWorkresult = null;
    private Context context = null;
    private LayoutInflater mInflator = null;
    private ViewHolder viewHolder = null;
    private Date currentDate = null;
    private final String CURRENT_DATE_FORMAT = "MM-dd-yyyy";
    private final String REQUIRED_DATE_FORMAT = "MMM. dd, yyyy";
    private String strDueDate = null;
    private String lblScore = null;

    public HomeWorkListAdapter(Context context,
                               ArrayList<GetHomeWorkForStudentWithCustomeResponse> homeWorkresult) {
        this.context = context;
        this.homeWorkresult = homeWorkresult;
        mInflator = LayoutInflater.from(context);
        currentDate = MathFriendzyHelper.getCurrentDateForHomwWork();

        //////////////////////////////////////////////
        String text[] = MathFriendzyHelper.getTreanslationTextById(context ,
                "lblSolveEquations" , "lblWordProblem" , "lblClass" , "lblWork" , "lblQuizzes" ,
                "lblDueDate" , "mfBtnTitleScore");
        practiceSkillText = text[0];
        wordProblemtext = text[1];
        customeProblemtext = text[2] + " " + text[3] + "/" + text[4];
        strDueDate = text[5];
        lblScore = text[6];
        laernignCenterFunctionsList = MathFriendzyHelper.getPracticeSkillCategories(this.context);
        updatedCatName = MathFriendzyHelper
                .getUpdatedPracticeSkillCatNameList(laernignCenterFunctionsList, this.context);
    }

    @Override
    public int getCount() {
        return homeWorkresult.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        /*if (view == null) {
            view = mInflator.inflate(R.layout.school_home_work_list_layout, null);
            viewHolder = new ViewHolder();
            viewHolder.txtDueDate = (TextView) view.findViewById(R.id.txtDueDate);
            viewHolder.txtPracticeSkill = (TextView) view.findViewById(R.id.txtPracticeSkill);
            viewHolder.txtSchoolCurriculum = (TextView) view.findViewById(R.id.txtSchoolCurriculum);
            viewHolder.txtCustome = (TextView) view.findViewById(R.id.txtCustome);
            viewHolder.txtAvgScore = (TextView) view.findViewById(R.id.txtAvgScore);
            viewHolder.layout = (RelativeLayout) view.findViewById(R.id.layout);
            viewHolder.txtNumberOfActiveInput = (TextView) view.findViewById(R.id.txtNumberOfActiveInput);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.txtDueDate.setText(this.getConvertedDateFormat
                (homeWorkresult.get(position).getDate()));
        this.setScore(homeWorkresult.get(position).getPracticeScore(), viewHolder.txtPracticeSkill);
        this.setScore(homeWorkresult.get(position).getWordScore(), viewHolder.txtSchoolCurriculum);
        this.setScore(homeWorkresult.get(position).getCustomeScore(), viewHolder.txtCustome);
        this.setScore(homeWorkresult.get(position).getAvgScore(), viewHolder.txtAvgScore);
        this.setNumberOfActiveOponent(homeWorkresult.get(position).getCustomeresultList(),
                viewHolder.txtNumberOfActiveInput);
        if (this.isExpireHomwWork(homeWorkresult.get(position).getDate())) {
            viewHolder.layout.setBackgroundColor(context.getResources().getColor(R.color.LIGHT_GRAY));
        } else {
            viewHolder.layout.setBackgroundColor(context.getResources().getColor(R.color.WHITE));
        }
        return view;*/

        if(view == null){
            view = mInflator.inflate(R.layout.homework_item_layout_doing_hw , null);
            viewHolder = new ViewHolder();
            viewHolder.txtDate = (TextView) view.findViewById(R.id.txtDate);
            viewHolder.btnSelect = (Button) view.findViewById(R.id.btnSelect);
            viewHolder.txtNo = (TextView) view.findViewById(R.id.txtNo);
            viewHolder.homeWorkQuizzLayout = (LinearLayout) view.findViewById(R.id.homeWorkQuizzLayout);
            viewHolder.rlClassLayout = (RelativeLayout) view.findViewById(R.id.rlClassLayout);
            viewHolder.txtClassDetail = (TextView) view.findViewById(R.id.txtClassDetail);
            viewHolder.txtScore = (TextView) view.findViewById(R.id.txtScore);
            view.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.txtScore.setText(lblScore);
        boolean isExpireQuiz = this.isExpireHomwWork(homeWorkresult.get(position).getDate());
        viewHolder.txtDate.setText(strDueDate + ": " + this.getConvertedDateFormat
                (homeWorkresult.get(position).getDate()));
        setAssignData(viewHolder.homeWorkQuizzLayout , homeWorkresult.get(position));
        if (isExpireQuiz) {
            viewHolder.homeWorkQuizzLayout.setBackgroundColor(context.getResources().getColor(R.color.LIGHT_GRAY));
        } else {
            viewHolder.homeWorkQuizzLayout.setBackgroundColor(context.getResources().getColor(R.color.WHITE));
        }

        this.setClassDetail(viewHolder , homeWorkresult.get(position));

        viewHolder.btnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(homeworkClickListener != null){
                    homeworkClickListener.onHomeworkClick(position);
                }
            }
        });
        return view;
    }

    /*private class ViewHolder {
        private TextView txtDueDate;
        private TextView txtPracticeSkill;
        private TextView txtSchoolCurriculum;
        private TextView txtCustome;
        private TextView txtAvgScore;
        private RelativeLayout layout;
        private TextView txtNumberOfActiveInput;
    }*/

    private void setScore(String score, TextView txtView) {
        if (score != null && score.length() > 0 /*&& !score.equals("0")*/) {
            txtView.setText(score + "%");
        } else {
            txtView.setText("-");
        }
    }

    /**
     * Check for work expire date
     *
     * @param dueDate
     * @return
     */
    private boolean isExpireHomwWork(String dueDate) {
        Date dueWorkDate = MathFriendzyHelper.getValidateDataForHomeWork(dueDate);
        if (dueWorkDate.compareTo(currentDate) >= 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Return the number of Active Player
     *
     * @return
     */
    public int getNumberOfActiveHomeWork() {
        int numberOfActiveHomeWork = 0;
        for (int i = 0; i < homeWorkresult.size(); i++) {
            if (!this.isExpireHomwWork(homeWorkresult.get(i).getDate())) {
                numberOfActiveHomeWork++;
            }
            for (int j = 0; j < homeWorkresult.get(i).getCustomeresultList().size(); j++) {
                numberOfActiveHomeWork = numberOfActiveHomeWork
                        + homeWorkresult.get(i).getCustomeresultList().get(j)
                        .getNumberOfActiveInput();
            }
        }
        return numberOfActiveHomeWork;
    }

    /**
     * Set Active oponent input
     *
     * @param customeResultList
     * @param txtText
     */
    private void setNumberOfActiveOponent(ArrayList<CustomeResult> customeResultList
            , TextView txtText) {
        try {
            int numberOfActiveOpponent = 0;
            for (int i = 0; i < customeResultList.size(); i++) {
                numberOfActiveOpponent = numberOfActiveOpponent + customeResultList.get(i)
                        .getNumberOfActiveInput();
            }
            if (numberOfActiveOpponent > 0) {
                txtText.setVisibility(TextView.VISIBLE);
                txtText.setText(numberOfActiveOpponent + "");
            } else {
                txtText.setVisibility(TextView.GONE);
            }
        } catch (Exception e) {
            txtText.setVisibility(TextView.GONE);
        }
    }

    /**
     * Get converted date format
     * @param date
     * @return
     */
    private String getConvertedDateFormat(String date) {
        try {
            return MathFriendzyHelper.formatDataInGivenFormat(date,
                    CURRENT_DATE_FORMAT, REQUIRED_DATE_FORMAT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    /////////////////////////////////////////////////
    private String customeProblemtext = null;
    private String practiceSkillText = null;
    private String wordProblemtext = null;
    private ArrayList<LearningCenterTransferObj> laernignCenterFunctionsList = null;
    private ArrayList<String> updatedCatName = null;
    private static class ViewHolder{
        private TextView txtDate;
        private Button btnSelect;
        private TextView txtNo;
        private LinearLayout homeWorkQuizzLayout;
        private RelativeLayout rlClassLayout;
        private TextView txtClassDetail;
        private TextView txtScore;
    }

    /**
     * Set the assign homework data into the list layout
     */
    private void setAssignData(LinearLayout homeWorkQuizzLayout ,
                               GetHomeWorkForStudentWithCustomeResponse homeworkData){
        try{
            homeWorkQuizzLayout.removeAllViews();
            ArrayList<CustomeResult> customeQuizList = homeworkData.getCustomeresultList();
            //LinkedHashMap<Integer, ArrayList<LearningCenterTransferObj>> practiceList = homeworkData.getPracticeList();
            //LinkedHashMap<String, ArrayList<SubCatergoryTransferObj>> workProblemList = homeworkData.getWorkProblemList();
            ArrayList<PracticeResult> practiceList = homeworkData.getPractiseResultList();
            ArrayList<WordResult> workProblemList = homeworkData.getWordResultList();
            if(customeQuizList != null && customeQuizList.size() > 0){
                homeWorkQuizzLayout.addView
                        (this.addCustomeProblemData(customeQuizList , homeworkData));
            }
            if(practiceList != null && practiceList.size() > 0){
                homeWorkQuizzLayout.addView
                        (this.addPracticeSkillData(practiceList , homeworkData));
            }
            if(workProblemList != null && workProblemList.size() > 0){
                homeWorkQuizzLayout.addView
                        (this.addWordProblemData(workProblemList , homeworkData));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * This method add the practice layout data into the main layout to display
     * @param
     */
    /*private LinearLayout addPracticeSkillData(LinkedHashMap<Integer, ArrayList<LearningCenterTransferObj>> practiceList){
        LinearLayout layout = this.getLinearLayout();
        layout.addView(this.getInflatedViewForQuizzQuestionType(1));
        layout.addView(this.addPracticeSkillLayout(practiceList));
        return layout;
    }*/

    /**
     * This method add the practice layout data into the main layout to display
     * @param
     */
    private LinearLayout addPracticeSkillData(ArrayList<PracticeResult> practiceList
            , GetHomeWorkForStudentWithCustomeResponse homeworkData){
        LinearLayout layout = this.getLinearLayout();
        layout.addView(this.getInflatedViewForQuizzQuestionType(1));
        layout.addView(this.addPracticeSkillLayout(practiceList , homeworkData));
        return layout;
    }


    /**
     * Add word problem layout
     * @return
     */
    /*private LinearLayout addWordProblemData(LinkedHashMap<String, ArrayList<SubCatergoryTransferObj>> workProblemList){
        LinearLayout layout = this.getLinearLayout();
        layout.addView(this.getInflatedViewForQuizzQuestionType(2));
        layout.addView(this.addWordProblemLayout(workProblemList));
        return layout;
    }*/

    private LinearLayout addWordProblemData(ArrayList<WordResult> workProblemList
            , GetHomeWorkForStudentWithCustomeResponse homeworkData){
        LinearLayout layout = this.getLinearLayout();
        layout.addView(this.getInflatedViewForQuizzQuestionType(2));
        layout.addView(this.addWordProblemLayout(workProblemList , homeworkData));
        return layout;
    }

    /**
     * Add custom problem layout
     * @return
     */
    private LinearLayout addCustomeProblemData(ArrayList<CustomeResult> customeQuizList
            , GetHomeWorkForStudentWithCustomeResponse homeworkData){
        LinearLayout layout = this.getLinearLayout();
        layout.addView(this.getInflatedViewForQuizzQuestionType(3));
        layout.addView(this.addCustomProblemLayout(customeQuizList , homeworkData));
        return layout;
    }

    /**
     * This method create and return a linear layout
     * @return
     */
    private LinearLayout getLinearLayout(){
        LinearLayout layout = new LinearLayout(this.context);
        layout.setOrientation(LinearLayout.VERTICAL);
        return layout;
    }

    /**
     * Return the view with the selection type text
     * @param inflateFor
     * @return
     */
    private View getInflatedViewForQuizzQuestionType(int inflateFor){

        View view = LayoutInflater.from(this.context)
                .inflate(R.layout.homework_quizz_question_type_new_doing_hw, null); // some changes by siddhiinfosoft
        TextView txtView = (TextView)view.findViewById(R.id.txtQuestionType);
        TextView txtNo   = (TextView)view.findViewById(R.id.txtNo);
        TextView txtScore   = (TextView)view.findViewById(R.id.txtScore);
        /*txtNo.setVisibility(TextView.VISIBLE);
        txtScore.setVisibility(TextView.VISIBLE);*/

        txtScore.setText("");
        txtScore.setBackgroundResource(R.drawable.arrow);

        if(inflateFor == 1){//for practice skill
            txtView.setText(practiceSkillText);

            txtScore.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    //onPracticeSkillCliked();
                }
            });
        }else if(inflateFor == 2){//for word problem
            txtView.setText(wordProblemtext);

            txtScore.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    //onWorkProblemClicked();
                }
            });
        }else if(inflateFor == 3){//for custom
            txtView.setText(customeProblemtext);

            txtScore.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    //onCustomeHomeworkClicked();
                }
            });
        }
        return view;
    }

    /**
     * This method add the practice skill layout with cat and subCat
     * @param arrayList
     * @return
     */
    private LinearLayout addPracticeSkillLayout(final ArrayList<PracticeResult> arrayList
    , final GetHomeWorkForStudentWithCustomeResponse homeworkData){

        Collections.sort(arrayList,
                new PracticeSkillSortByCategoryId());
        LinearLayout layout = this.getLinearLayout();

        for(int i = 0 ; i < arrayList.size() ; i ++ ){
            for(int j = 0 ; j < laernignCenterFunctionsList.size() ; j ++){
                if(arrayList.get(i).getCatId().equals
                        (laernignCenterFunctionsList.get(j)
                                .getLearningCenterMathOperationId() + "")){
                    View view = this.getCategoryInflatedLayout(1);
                    ImageView imgSign = (ImageView) view.findViewById(R.id.imgSign);
                    TextView  txtCatName = (TextView) view.findViewById(R.id.txtCategoryName);
                    imgSign.setBackgroundResource(this.context.getResources().getIdentifier
                            ("mf_" + laernignCenterFunctionsList.get(j)
                                            .getLearningCenterOperation()
                                            .toLowerCase().replace(" ","_")+"_sign", "drawable",
                                    this.context.getPackageName()));
                    txtCatName.setText(updatedCatName.get(j));
                    layout.addView(view);

                    final ArrayList<PracticeResultSubCat> practiceResultSubCatList =
                            arrayList.get(i).getPracticeResultSubCatList();

                    for(int k = 0 ; k < practiceResultSubCatList.size() ; k ++ ){
                        View subCatView = this.getSubCategoryInflatedLayout(1);
                        TextView txtSubCatName = (TextView) subCatView.findViewById(R.id.txtSubCategoryName);
                        TextView txtNoOfQuestions = (TextView) subCatView.findViewById(R.id.txtNo);
                        TextView txtScore = (TextView) subCatView.findViewById(R.id.txtScore);
                        RelativeLayout rlArrowLayout = (RelativeLayout) subCatView.findViewById(R.id.rlArrowLayout);
                        txtSubCatName.setText(this.getPracticeSkillSubCatName
                                (arrayList.get(i).getCatId(), practiceResultSubCatList.get(k).getSubCatId() + ""));
                        txtNoOfQuestions.setText(practiceResultSubCatList.get(k).getProblems() + "");
                        txtScore.setText(practiceResultSubCatList.get(k).getScore() + "%");

                        practiceResultSubCatList.get(k).setCatId(arrayList.get(i).getCatId());

                        final PracticeResultSubCat practiceResultSubCat = practiceResultSubCatList.get(k);
                        rlArrowLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(homeworkClickListener != null){
                                    homeworkClickListener.onPracticeClick(practiceResultSubCat , homeworkData , 0);
                                }
                            }
                        });
                        layout.addView(subCatView);
                    }
                    break;
                }
            }
        }
        return layout;
    }

    /**
     * Add work problem layout
     */
    /**
     * This method add the word problem layout with Cat and SubCat
     * @param arrayList
     * @return
     */
    private LinearLayout addWordProblemLayout(ArrayList<WordResult> arrayList
     , final GetHomeWorkForStudentWithCustomeResponse homeworkData){
        LinearLayout layout = this.getLinearLayout();

        for(int i = 0 ; i < arrayList.size() ; i ++ ){
            View view = this.getCategoryInflatedLayout(2);
            ImageView imgSign = (ImageView) view.findViewById(R.id.imgSign);
            TextView  txtCatName = (TextView) view.findViewById(R.id.txtCategoryName);
            txtCatName.setText(MathFriendzyHelper
                    .getSchoolCurriculumCategoryNameByCatId
                            (this.context, arrayList.get(i).getCatIg()));
            imgSign.setVisibility(ImageView.GONE);
            layout.addView(view);

            ArrayList<WordSubCatResult> subCatList = arrayList.get(i).getSubCatResultList();
            for(int j = 0 ; j < subCatList.size() ; j ++ ){
                View subCatView = this.getSubCategoryInflatedLayout(2);
                TextView txtSubCatName = (TextView) subCatView.findViewById(R.id.txtSubCategoryName);
                TextView txtNoOfQuestions = (TextView) subCatView.findViewById(R.id.txtNo);
                TextView txtScore = (TextView) subCatView.findViewById(R.id.txtScore);
                RelativeLayout rlArrowLayout = (RelativeLayout) subCatView.findViewById(R.id.rlArrowLayout);
                txtSubCatName.setText(MathFriendzyHelper.
                        getSchoolCurriculumSubCatNameByCatIdAndSubCatId
                                (this.context, arrayList.get(i).getCatIg(), subCatList.get(j).getSubCatId() + ""));
                txtNoOfQuestions.setText("10");
                txtScore.setText(subCatList.get(j).getScore() + "%");

                subCatList.get(j).setCatId(arrayList.get(i).getCatIg());

                final WordSubCatResult subCatResult = subCatList.get(j);
                rlArrowLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(homeworkClickListener != null){
                            homeworkClickListener.onWordClick(subCatResult, homeworkData, 0);
                        }
                    }
                });
                layout.addView(subCatView);

            }
        }
        return layout;
    }

    /**
     * Add Custom Problem layout
     * @return
     */
    private LinearLayout addCustomProblemLayout(ArrayList<CustomeResult> customeQuizList
     , final GetHomeWorkForStudentWithCustomeResponse homeworkData){
        LinearLayout layout = this.getLinearLayout();
        for(int i = 0 ; i < customeQuizList.size() ; i ++ ){
            View view = this.getSubCategoryInflatedLayout(1);
            TextView txtSubCatName = (TextView) view.findViewById(R.id.txtSubCategoryName);
            TextView txtNoOfQuestions = (TextView) view.findViewById(R.id.txtNo);
            TextView txtScore = (TextView) view.findViewById(R.id.txtScore);
            ImageView imgArrow = (ImageView) view.findViewById(R.id.imgArrow);
            RelativeLayout rlArrowLayout = (RelativeLayout) view.findViewById(R.id.rlArrowLayout);
            /*imgArrow.setVisibility(ImageView.INVISIBLE);
            txtScore.setVisibility(TextView.INVISIBLE);*/
            txtSubCatName.setText(customeQuizList.get(i).getTitle());
            txtNoOfQuestions.setText(customeQuizList.get(i).getCustomeAnsList().size() + "");
            txtScore.setText(customeQuizList.get(i).getScore() + "%");

            final CustomeResult customeResult = customeQuizList.get(i);
            rlArrowLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(homeworkClickListener != null){
                        homeworkClickListener.onCustomClick(customeResult, homeworkData, 0);
                    }
                }
            });
            layout.addView(view);
        }
        return layout;
    }

    @SuppressLint("InflateParams")
    private View getCategoryInflatedLayout(int inflateFor){
        View view = LayoutInflater.from(this.context)
                .inflate(R.layout.school_home_question_categories_layout_new, null);
        return view;
    }

    @SuppressLint("InflateParams")
    private View getSubCategoryInflatedLayout(int inflateFor){
        View view = LayoutInflater.from(this.context)
                .inflate(R.layout.home_work_quizz_sub_category_layout_new_doing_hw, null);
        return view;
    }

    /**
     * Get practice skill subCat Name
     * @param catId
     * @param subCatId
     * @return
     */
    private String getPracticeSkillSubCatName(String catId , String subCatId){

        String subCatName = null;
        LearningCenterimpl laerningCenter = new LearningCenterimpl(this.context);
        laerningCenter.openConn();
        subCatName = laerningCenter.
                getMathOperationCategoriesByCatIdAndSubCatId(catId,subCatId)
                .getMathOperationCategory();
        laerningCenter.closeConn();

        Translation transeletion = new Translation(this.context);
        transeletion.openConnection();
        subCatName = transeletion.getTranselationTextByTextIdentifier(subCatName);
        transeletion.closeConnection();

        return subCatName;
    }

    private HomeworkClickListener homeworkClickListener = null;
    public void initializeListener(HomeworkClickListener homeworkClickListener){
        this.homeworkClickListener = homeworkClickListener;
    }

    /**
     * Set the class detail
     * @param viewHolder
     * @param homeWorkData
     */
    private void setClassDetail(ViewHolder viewHolder ,
                                GetHomeWorkForStudentWithCustomeResponse homeWorkData){
        try {
            viewHolder.rlClassLayout.setVisibility(View.VISIBLE);
            viewHolder.txtClassDetail.setText(homeWorkData.getSubjectName()
                    + ", " + homeWorkData.getClassName());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}