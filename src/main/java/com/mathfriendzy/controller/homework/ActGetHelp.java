package com.mathfriendzy.controller.homework;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.homework.homwworkworkarea.HomeWorkWorkArea;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.homework.CustomeAns;
import com.mathfriendzy.model.homework.CustomeResult;
import com.mathfriendzy.model.homework.GetHelpStudentListResponse;
import com.mathfriendzy.model.homework.GetHomeWorkForStudentWithCustomeResponse;
import com.mathfriendzy.model.homework.GetStudentsForWorkAreaHelpParam;
import com.mathfriendzy.model.homework.secondworkarea.AddDuplicateWorkAreaParam;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;

public class ActGetHelp extends ActBase {

	private final String TAG = this.getClass().getSimpleName();
	private TextView txtGetHelpFromClassMates = null;
	private TextView txtName = null;
	private TextView txtRating = null;
	private ListView lstStudentHelpList = null;
		
	private CustomeResult customeResult = null;
	private GetHomeWorkForStudentWithCustomeResponse homeWorkQuizzData = null;
	private UserPlayerDto selectedPlayerData = null;
	private CustomeAns customeAns = null;
	
	//for start activity for result to open work and update the student rating if change on
	//the work area
	private final int GET_HELP_WORK_AREA_REQUEST_FOR_RATING_CHANGES = 1001;
	private GetHelpStudentAdapter adapter = null;
	//clicked index from the list to open the work area
	private int selectedIndex = 0;
	private int isGetHelp = 0;
	private String lblNoStudentHasAnswered = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_act_get_help);

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside onCreate()");
		
		selectedPlayerData = this.getPlayerData();
		
		this.getIntentValues();
		this.setWidgetsReferences();
		this.setTextFromTranslation();
		this.setListenerOnWidgets();
		this.getGetHelpStudentList();
		
		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside onCreate()");
	}

	/**
	 * Get the saved intent values in previous screen
	 */
	private void getIntentValues(){
		customeResult = (CustomeResult) this.getIntent().getSerializableExtra("customeDetail");
		homeWorkQuizzData = (GetHomeWorkForStudentWithCustomeResponse) 
				this.getIntent().getSerializableExtra("homeWorkQuizzData");
		customeAns = (CustomeAns) this.getIntent().getSerializableExtra("customeAns");
	}
	
	@Override
	protected void setWidgetsReferences() {
		
		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside setWidgetsReferences()");
		
		txtTopbar 		= (TextView) findViewById(R.id.txtTopbar);
		txtGetHelpFromClassMates = (TextView) findViewById(R.id.txtGetHelpFromClassMates);
		txtName = (TextView) findViewById(R.id.txtName);
		txtRating = (TextView) findViewById(R.id.txtRating);
				
		lstStudentHelpList = (ListView) findViewById(R.id.lstStudentHelpList);
		
		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside setWidgetsReferences()");

	}

	@Override
	protected void setListenerOnWidgets() {
		
		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside setListenerOnWidgets()");
		
		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside setListenerOnWidgets()");

	}

	@Override
	protected void setTextFromTranslation() {
		
		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside setTextFromTranslation()");
		
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblCheckHomework") + "/" + 	
				transeletion.getTranselationTextByTextIdentifier("lblQuizzes"));
		txtGetHelpFromClassMates.setText(transeletion.getTranselationTextByTextIdentifier("lblGetHelpFromClassmates"));
		txtName.setText(transeletion.getTranselationTextByTextIdentifier("lblRegName"));
		txtRating.setText(transeletion.getTranselationTextByTextIdentifier("lblRating"));
        lblNoStudentHasAnswered = transeletion.getTranselationTextByTextIdentifier("alertMessageNoneOfYourClassmatesAnswerCorrectly");
        transeletion.closeConnection();
		
		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside setTextFromTranslation()");
	}
	
	@Override
	protected void onResume() {
		//this.getGetHelpStudentList();
		super.onResume();
	}
	
	/**
	 * Set the student help list data
	 * @param studentList
	 */
	private void setAdapterData(ArrayList<GetHelpStudentListResponse> studentList){
		if(studentList.size() > 0) {
            adapter = new GetHelpStudentAdapter(this, studentList);
            lstStudentHelpList.setAdapter(adapter);

            lstStudentHelpList.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    selectedIndex = position;
                    isGetHelp = 1;
                    clickOnItem(adapter.studentList.get(position));
                    MathFriendzyHelper.addDuplicateWorkAreaContent(ActGetHelp.this ,
                            AddDuplicateWorkAreaParam.GET_HELP_PARAM);
                }
            });
        }else{
            MathFriendzyHelper.warningDialog
                    (this, lblNoStudentHasAnswered, new HttpServerRequest() {
                @Override
                public void onRequestComplete() {
                    finish();
                }
            });
        }
	}
	
	/**
	 * Click on item or cell of the student list
	 * @param student
	 */
	private void clickOnItem(GetHelpStudentListResponse student){
		String fileName = student.getWorkImageName();
		final Intent intent = new Intent(this , HomeWorkWorkArea.class);
		intent.putExtra("isCallForGetHelp", true);
		intent.putExtra("ratingStar", student.getYourRating());
		intent.putExtra("helperStudent", student);
		intent.putExtra("homeWorkQuizzData", homeWorkQuizzData);
		intent.putExtra("customeDetail", customeResult);
		intent.putExtra("customeAns", customeAns);
		intent.putExtra("playerMsg", student.getStudentTeacherMsg());
		//for view pdf
		final String playerAndQuestionImage = student.getQuestionImage();
		intent.putExtra("playerAndQuestionImage", playerAndQuestionImage);
		
		if(fileName != null && fileName.length() > 0){
			intent.putExtra("fileName", fileName);
			if(MathFriendzyHelper.checkForExistenceOfFile(fileName)){
				intent.putExtra("isFileExist", true);
				//startActivityForResult(intent , GET_HELP_WORK_AREA_REQUEST_FOR_RATING_CHANGES);
				startWorkAreaAct(intent , playerAndQuestionImage);
			}else{
				MathFriendzyHelper.downLoadImageFromUrl
				(this, ICommonUtils.DOWNLOD_WORK_IMAGE_URL, fileName, new HttpServerRequest() {
					@Override
					public void onRequestComplete() {
						intent.putExtra("isFileExist", true);
						//startActivityForResult(intent , GET_HELP_WORK_AREA_REQUEST_FOR_RATING_CHANGES);
						startWorkAreaAct(intent , playerAndQuestionImage);
					}
				});
			}
		}else{//Not reachable code but executed when the case of crash or other thing
			fileName = this.getImageName(customeAns, 0);
			if(MathFriendzyHelper.checkForExistenceOfFile(fileName)){
				intent.putExtra("isFileExist", true);
			}else{
				intent.putExtra("isFileExist", false);
			}
			intent.putExtra("fileName", fileName);
			//startActivityForResult(intent , GET_HELP_WORK_AREA_REQUEST_FOR_RATING_CHANGES);
			startWorkAreaAct(intent , playerAndQuestionImage);
		}
	}
	
	/**
	 * start the work area activity
	 * @param intent
	 */
	private void startWorkAreaAct(final Intent intent, final String questionImageName){
			if(questionImageName != null && 
				questionImageName.length() > 0){
			if(MathFriendzyHelper.checkForExistenceOfFile(questionImageName)){
				startActivityForResult(intent , GET_HELP_WORK_AREA_REQUEST_FOR_RATING_CHANGES);
			}else{
				MathFriendzyHelper.downLoadImageFromUrl
				(this, ICommonUtils.DOWNLOAD_QUESTION_IMAGE_URL, questionImageName, 
						new HttpServerRequest() {
					@Override
					public void onRequestComplete() {
						startActivityForResult(intent , GET_HELP_WORK_AREA_REQUEST_FOR_RATING_CHANGES);
					}
				});
			}
		}else{
			startActivityForResult(intent , GET_HELP_WORK_AREA_REQUEST_FOR_RATING_CHANGES);
		}
	}
	
	/**
	 * Return the work image name
	 * @param customeAns
	 * @param indexOfQuestion
	 * @return
	 */
	private String getImageName(CustomeAns customeAns , int indexOfQuestion){
		return customeResult.getCustomHwId() + "_" + 
				selectedPlayerData.getPlayerid() + "_" + indexOfQuestion + ".png";
	}
	
	/**
	 * Get help student list from server
	 */
	private void getGetHelpStudentList(){

        if(CommonUtils.isInternetConnectionAvailable(this)) {
            GetStudentsForWorkAreaHelpParam param = new GetStudentsForWorkAreaHelpParam();
            param.setAction("getStudentsForWorkAreaHelp");
            param.setUserId(selectedPlayerData.getParentUserId());
            param.setPlayerId(selectedPlayerData.getPlayerid());
            param.setHWId(homeWorkQuizzData.getHomeWorkId());
            param.setCutomeHWId(customeResult.getCustomHwId() + "");
            param.setQuestionId(customeAns.getQueNo());

            new MyAsyckTask(ServerOperation.createPostToGethelpStudentList(param)
                    , null, ServerOperationUtil.GET_HELP_STUDENT_REQUEST, this,
                    this, ServerOperationUtil.SIMPLE_DIALOG, true,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        }else{
            CommonUtils.showInternetDialog(this);
        }
	}
	
	@Override
	public void serverResponse(HttpResponseBase httpResponseBase,
			int requestCode) {
		if(requestCode == ServerOperationUtil.GET_HELP_STUDENT_REQUEST){
			GetHelpStudentListResponse response = (GetHelpStudentListResponse) httpResponseBase;
			setAdapterData(response.getStudentList());
		}
	}

	@Override
	public void onClick(View v) {
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == RESULT_OK){
			if(requestCode == GET_HELP_WORK_AREA_REQUEST_FOR_RATING_CHANGES){
				adapter.studentList.set(selectedIndex, 
						(GetHelpStudentListResponse)data.getSerializableExtra("helperStudent"));
				adapter.notifyDataSetChanged();
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	public void onBackPressed() {
		Intent intent = new Intent();
		intent.putExtra("isGetHelp", isGetHelp);
		setResult(RESULT_OK, intent);
		finish();
		super.onBackPressed();
	}
}
