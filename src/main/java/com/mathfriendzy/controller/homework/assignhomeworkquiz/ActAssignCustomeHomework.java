package com.mathfriendzy.controller.homework.assignhomeworkquiz;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.homework.CustomeResult;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.utils.CommonUtils;

import java.util.ArrayList;

public class ActAssignCustomeHomework extends ActBase {

	private final String TAG = this.getClass().getSimpleName();
	private TextView txtClassWorkQuiz = null;
	private TextView txtCreateAnswerSheetForHW = null;
	private ListView lstAssignHomeworkTitleList = null;
	private ArrayList<CustomeResult> customeQuizList = null;

	public final int START_ACT_ASSIGN_CUSTOME_ANS = 1001;

	private CutomeHomeworkTitleAdapter adapter = null;

	//for dialog msg
	private String lblPleaseFillAllAnsCustomHomework = null;
	private String yesText = null;
	private String notext  = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_act_assign_custome_homework);

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside onCreate()");

		this.getIntentValues();
		this.init();
		this.setWidgetsReferences();
		this.setTextFromTranslation();
		this.setListenerOnWidgets();
		this.setTitleListAdapter();

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside onCreate()");

	}

	/**
	 * Get the intent which are set in previous screen
	 */
	@SuppressWarnings("unchecked")
	private void getIntentValues() {
		customeQuizList = (ArrayList<CustomeResult>) 
				this.getIntent().getSerializableExtra("customeQuizList");
	}

	/**
	 * Clear the focus on the Adapter when user input value in the Edittext
	 *//*
	public void cleasFocusFromEditText(){
		getCurrentFocus().clearFocus();
	}*/

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		cleasFocusFromEditText();
		return super.onTouchEvent(event);
	}

	/**
	 * Initialize the variables initially
	 */
	private void init(){
		if(customeQuizList == null){		
			customeQuizList = new ArrayList<CustomeResult>();
			//for(int  i = 0 ; i < 30 ; i ++ )
			customeQuizList.add(this.getCustomeResultWithTitle(""));
			//customeQuizList.add("");//add blank at title name at the first index
		}
	}

	/**
	 * Create new custom result objects based on the title name
	 * @param title
	 * @return
	 */
	public CustomeResult getCustomeResultWithTitle(String title){
		CustomeResult customeResult = new CustomeResult();
        customeResult.setShowAns(1);//set default show answer
        customeResult.setAllowChanges(1);//set default allow changes
		customeResult.setTitle(title);
		return customeResult;
	}

	@Override
	protected void setWidgetsReferences() {

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside onCreate()");

		txtTopbar 	= (TextView) findViewById(R.id.txtTopbar);
		txtClassWorkQuiz = (TextView) findViewById(R.id.txtClassWorkQuiz);
		txtCreateAnswerSheetForHW = (TextView) findViewById(R.id.txtCreateAnswerSheetForHW);
		lstAssignHomeworkTitleList = (ListView) findViewById(R.id.lstAssignHomeworkTitleList);

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside onCreate()");
	}

	@Override
	protected void setTextFromTranslation() {

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside onCreate()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblAssignHomework"));
		txtClassWorkQuiz.setText(transeletion.getTranselationTextByTextIdentifier("lblClass")
				+ " " + transeletion.getTranselationTextByTextIdentifier("lblWork")
				+ "/" + transeletion.getTranselationTextByTextIdentifier("lblQuizzes"));
		txtCreateAnswerSheetForHW.setText(transeletion.getTranselationTextByTextIdentifier
				("lblCreateAnswerSheetForHomeworkAndQuiz"));

		lblPleaseFillAllAnsCustomHomework = transeletion.getTranselationTextByTextIdentifier
				("lblPleaseFillAllAnsCustomHomework");
		yesText = transeletion.getTranselationTextByTextIdentifier("btnTitleYes");
		notext = transeletion.getTranselationTextByTextIdentifier("lblNo");
		transeletion.closeConnection();

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside onCreate()");

	}

	@Override
	protected void setListenerOnWidgets() {

	}

	/**
	 * Set the title list adapter to the listview
	 */
	private void setTitleListAdapter(){
		adapter = new CutomeHomeworkTitleAdapter(this, customeQuizList);		
		lstAssignHomeworkTitleList.setAdapter(adapter);
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
	}

	@Override
	public void serverResponse(HttpResponseBase httpResponseBase,
			int requestCode) {
		// TODO Auto-generated method stub
	}

	/**
	 * Check For Custom Title without correct answer list
	 * @return
	 */
	private boolean isAnyTitleWithoutAnsList(){
		try{
			if(adapter.customeQuizList.size() == 1){//if list size is 1
				for(int i = 0 ; i < adapter.customeQuizList.size() ; i ++){
					if(!(adapter.customeQuizList.get(i).getCustomeAnsList() != null
							&& adapter.customeQuizList.get(i).getCustomeAnsList().size() > 0)){
						String title = adapter.customeQuizList.get(i).getTitle();
						if(title != null && title.length() > 0)
							return true;
					}
				}
				return false;
			}else{
				for(int i = 0 ; i < adapter.customeQuizList.size() ; i ++){
					if(!(adapter.customeQuizList.get(i).getCustomeAnsList() != null
							&& adapter.customeQuizList.get(i).getCustomeAnsList().size() > 0)){
						return true;
					}
				}
				return false;
			}
		}catch(Exception e){
			return false;
		}
	}

	@Override
	public void onBackPressed() {
		this.cleasFocusFromEditText();
		if(this.isAnyTitleWithoutAnsList()){
			MathFriendzyHelper.warningDialog(this, lblPleaseFillAllAnsCustomHomework);
		}else{
			onBack();
			super.onBackPressed();
		}
	}

	/**
	 * Called when click on back
	 */
	private void onBack(){
		Intent intent = new Intent();
		intent.putExtra("customeQuizList", adapter.customeQuizList);
		setResult(RESULT_OK, intent);
		finish();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == RESULT_OK){
			switch(requestCode){
			case START_ACT_ASSIGN_CUSTOME_ANS:
				CustomeResult customeResult = (CustomeResult)
				data.getSerializableExtra("customeResult");

				if(customeResult.getCustomeAnsList() != null){
					adapter.customeQuizList.set(adapter.clickedPosition, customeResult);
				}else{
					if(adapter.layoutList.get(adapter.clickedPosition) != null){
						adapter.customeQuizList.get(adapter.clickedPosition).setLayoutVisible(false);
						adapter.setVisibilityOfEditLayout(adapter.layoutList.get(adapter.clickedPosition) , false);
					}

					if(adapter.customeQuizList.size() > 1){
						adapter.customeQuizList.remove(adapter.clickedPosition);
						adapter.layoutList.remove(adapter.clickedPosition);
					}else{
						adapter.customeQuizList.get(adapter.clickedPosition).setTitle("");
						adapter.customeQuizList.get(adapter.clickedPosition)
						.setCustomeAnsList(null);
					}
				}
				adapter.notifyDataSetChanged();
				break;
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
}
