package com.mathfriendzy.controller.homework.assignhomeworkquiz;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.model.homework.CustomeAns;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.utils.CommonUtils;

import java.util.ArrayList;

public class ActMultipleChoiceAnsSheet extends ActBase {

	private final String TAG = this.getClass().getSimpleName();
	private TextView txtAnswerSheetFor = null;
	private TextView txtProblem = null;
	private TextView txtMultipleChoice = null;
	private TextView multiPleCorrectAns = null;
	private TextView multipleChoice = null;
	private TextView txtTrueFalse = null;
	private TextView trueFalseCorrectAns = null;
	private TextView trueFalseChoice = null;
	private TextView txtYesNo = null;
	private TextView yesNoCorrectAns = null;
	private TextView yesNoChoice = null;

	//for multiple choice selection options
	private ListView lstMultipleChoiceOptionList = null;
	private LinearLayout multipleChoiceChoiceSelectionLayout = null;
	private final int MULTIPLE_CHOICE_MAX_OPTION = 8;
	private ArrayList<String> characterlist = null;
	private final String REMOVE_TEXT = "Remove";
	private ArrayList<String> multipleChoiceOptionList = null;//by default 4 option A,B,C,D
	private ArrayList<String> multipleChoiceOptionListTempList = null;
	
	//for multiple choice 
	private ArrayList<TextView> txtViewList = null;
	private ArrayList<Boolean>  userAnswerSelectedList = null;

	//for true false selection
	private int isAnswerTrue = 2;
	private final int ANSWER_FALSE = 0;
	private final int ANSWER_TRUE = 1;
	private ImageView imgTrueCheck = null;
	private ImageView imgFalseCheck = null;

	//for yes no selection
	private ImageView imgYesCheck = null;
	private ImageView imgNoCheck = null;
	private int isAnswerYes = 2;
	private final int ANSWER_NO = 0;
	private final int ANSWER_YES = 1;

	private final int MULTIPLE_CHOICE_ANS_TYPE = 1;
	private final int TRUE_FALSE_ANS_TYPE = 2;
	private final int YES_NO_ANS_TYPE = 3;

	//by default MULTIPLE_CHOICE_ANS_TYPE selected
	private int answerType = MULTIPLE_CHOICE_ANS_TYPE;

	//for answer type selection
	private ImageView imgMultipleChoice = null;
	private ImageView imgTrueFalse      = null;
	private ImageView imgYesNo          = null;
	private RelativeLayout multipleChoiceSelectionLayout = null;
	private RelativeLayout trueFalseSelectionLayout 	 = null;
	private RelativeLayout yesNoSelectionLayout 		 = null;

	//for common
	private String userAnswer = null;

	private CustomeAns customeAnsObj = null;
	
	public static final String TRUE = "True";
	public static final String FALSE = "False";
	public static final String YES = "Yes";
	public static final String NO = "No";

    private boolean isNewLine = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_act_multiple_choice_ans_sheet);

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside onCreate()");

		this.getIntentValues();
		this.init();
		this.setWidgetsReferences();
		this.setTextFromTranslation();
		this.setListenerOnWidgets();
		this.setMultipleChoiceLayoutOption();
		this.setTrueFalseLayout();
		this.setYesNoLayout();
		
		this.setAnswerTypeSelected(answerType);

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside onCreate()");
	}

	/**
	 * Get the intent values which are set in previous screen
	 */
	private void getIntentValues(){
        isNewLine = this.getIntent().getBooleanExtra("isNewLine" , false);
		customeAnsObj = (CustomeAns) this.getIntent().getSerializableExtra("customeAnsObj");
        if(isNewLine){
            customeAnsObj.setQueNo("");
        }
		customeAnsObj.setFillInType(0);
		answerType = customeAnsObj.getAnswerType();
		userAnswer = customeAnsObj.getCorrectAns();
		
		if(answerType == MULTIPLE_CHOICE_ANS_TYPE){
			multipleChoiceOptionList = customeAnsObj.getMultipleChoiceOptionList();
		}
	}
	
	/**
	 * Initialize the initial input variables 
	 */
	private void init() {
		characterlist = CommonUtils.getCharacterList();
		characterlist.add(0 , REMOVE_TEXT);
		//characterlist.add("");

		//default option , when user not select any option before
		if(multipleChoiceOptionList == null){
			multipleChoiceOptionList = new ArrayList<String>();
			multipleChoiceOptionList.add("A");
			multipleChoiceOptionList.add("B");
			multipleChoiceOptionList.add("C");
			multipleChoiceOptionList.add("D");
		}

		txtViewList = new ArrayList<TextView>(MULTIPLE_CHOICE_MAX_OPTION);
		//if(userAnswerSelectedList == null){
		userAnswerSelectedList = new ArrayList<Boolean>(MULTIPLE_CHOICE_MAX_OPTION);
		for(int i = 0 ; i < MULTIPLE_CHOICE_MAX_OPTION ; i ++ ){
			userAnswerSelectedList.add(false);
		}
		//}
	}

	/**
	 * Set the answer type selection
	 */
	private void setAnswerTypeSelected(int answerType){
		this.answerType = answerType;
		if(answerType == MULTIPLE_CHOICE_ANS_TYPE){

			/*this.disableEnableControls(true , multipleChoiceSelectionLayout);
			this.disableEnableControls(false , trueFalseSelectionLayout);
			this.disableEnableControls(false , yesNoSelectionLayout);*/

			imgMultipleChoice.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
			imgTrueFalse.setBackgroundResource(R.drawable.mf_check_box_ipad);
			imgYesNo.setBackgroundResource(R.drawable.mf_check_box_ipad);

		}else if(answerType == TRUE_FALSE_ANS_TYPE){
			
			/*this.disableEnableControls(false , multipleChoiceSelectionLayout);
			this.disableEnableControls(true , trueFalseSelectionLayout);
			this.disableEnableControls(false , yesNoSelectionLayout);*/

			imgMultipleChoice.setBackgroundResource(R.drawable.mf_check_box_ipad);
			imgTrueFalse.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
			imgYesNo.setBackgroundResource(R.drawable.mf_check_box_ipad);

		}else if(answerType == YES_NO_ANS_TYPE){
			
			/*this.disableEnableControls(false , multipleChoiceSelectionLayout);
			this.disableEnableControls(false , trueFalseSelectionLayout);
			this.disableEnableControls(true , yesNoSelectionLayout);*/

			imgMultipleChoice.setBackgroundResource(R.drawable.mf_check_box_ipad);
			imgTrueFalse.setBackgroundResource(R.drawable.mf_check_box_ipad);
			imgYesNo.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
		}
	}

	/**
	 * Disable Enable Controls
	 * @param enable
	 * @param vg
	 */
	private void disableEnableControls(boolean enable, ViewGroup vg){
		for (int i = 0; i < vg.getChildCount(); i++){
			View child = vg.getChildAt(i);
			child.setEnabled(enable);
			if (child instanceof ViewGroup){ 
				disableEnableControls(enable, (ViewGroup)child);
			}
		}
	}

	/**
	 * get Selected option to show
	 * @param index
	 * @return
	 */
	private String getSelectedoption(int index){
		try{
			return  multipleChoiceOptionList.get(index);
		}catch(Exception e){
			return "";
		}
	}

	@Override
	protected void setWidgetsReferences() {

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside setWidgetsReferences()");

		txtTopbar 	= (TextView) findViewById(R.id.txtTopbar);
		txtAnswerSheetFor = (TextView) findViewById(R.id.txtAnswerSheetFor);
		txtProblem = (TextView) findViewById(R.id.txtProblem);
		txtMultipleChoice = (TextView) findViewById(R.id.txtMultipleChoice);
		multiPleCorrectAns = (TextView) findViewById(R.id.multiPleCorrectAns);
		multipleChoice = (TextView) findViewById(R.id.multipleChoice);
		txtTrueFalse = (TextView) findViewById(R.id.txtTrueFalse);
		trueFalseCorrectAns = (TextView) findViewById(R.id.trueFalseCorrectAns);
		trueFalseChoice = (TextView) findViewById(R.id.trueFalseChoice);
		txtYesNo = (TextView) findViewById(R.id.txtYesNo);
		yesNoCorrectAns = (TextView) findViewById(R.id.yesNoCorrectAns);
		yesNoChoice = (TextView) findViewById(R.id.yesNoChoice);

		//for multiple choice selection
		multipleChoiceChoiceSelectionLayout = (LinearLayout) findViewById
				(R.id.multipleChoiceChoiceSelectionLayout);
		lstMultipleChoiceOptionList = (ListView) findViewById(R.id.lstMultipleChoiceOptionList);

		//for true false selection
		imgTrueCheck = (ImageView) findViewById(R.id.imgTrueCheck);
		imgFalseCheck = (ImageView) findViewById(R.id.imgFalseCheck);

		//for yes no selection
		imgYesCheck = (ImageView) findViewById(R.id.imgYesCheck);
		imgNoCheck = (ImageView) findViewById(R.id.imgNoCheck);

		//for answer type selection
		imgMultipleChoice = (ImageView) findViewById(R.id.imgMultipleChoice);
		imgTrueFalse 	  = (ImageView) findViewById(R.id.imgTrueFalse);
		imgYesNo 		  = (ImageView) findViewById(R.id.imgYesNo);

		multipleChoiceSelectionLayout = (RelativeLayout) findViewById(R.id.multipleChoiceSelectionLayout);
		trueFalseSelectionLayout = (RelativeLayout) findViewById(R.id.trueFalseSelectionLayout);
		yesNoSelectionLayout = (RelativeLayout) findViewById(R.id.yesNoSelectionLayout);

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside setWidgetsReferences()");

	}

	@Override
	protected void setTextFromTranslation() {

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside setTextFromTranslation()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblAssignHomework"));
		txtAnswerSheetFor.setText(transeletion.getTranselationTextByTextIdentifier("lblAnswerSheetFor") + ":");
		txtProblem.setText(transeletion.getTranselationTextByTextIdentifier("mfLblProblem") + ": "
                + customeAnsObj.getQueNo());
		txtMultipleChoice.setText(transeletion.getTranselationTextByTextIdentifier("lblMultiple")
				+ " " + transeletion.getTranselationTextByTextIdentifier("lblChoice"));
		//multiPleCorrectAns.setText(transeletion.getTranselationTextByTextIdentifier("lblAssignHomework"));
		multipleChoice.setText(transeletion.getTranselationTextByTextIdentifier("lblChoice"));
		txtTrueFalse.setText(transeletion.getTranselationTextByTextIdentifier("lblTrue")
				+ " / " + transeletion.getTranselationTextByTextIdentifier("lblFalse"));
		//trueFalseCorrectAns.setText(transeletion.getTranselationTextByTextIdentifier("lblAssignHomework"));
		trueFalseChoice.setText(transeletion.getTranselationTextByTextIdentifier("lblChoice"));
		txtYesNo.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleYes")
				+ " / " + transeletion.getTranselationTextByTextIdentifier("lblNo"));
		//yesNoCorrectAns.setText(transeletion.getTranselationTextByTextIdentifier("lblAssignHomework"));
		yesNoChoice.setText(transeletion.getTranselationTextByTextIdentifier("lblChoice"));
		transeletion.closeConnection();
		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside setTextFromTranslation()");

	}

	/**
	 * Set Multiple choice options Layout
	 */
	@SuppressLint("InflateParams")
	private void setMultipleChoiceLayoutOption(){

		for(int i = 0 ; i < MULTIPLE_CHOICE_MAX_OPTION ; i ++ ){
			final int position = i;
			View view  = LayoutInflater.from(this).inflate
					(R.layout.answer_sheet_for_multiplechoice_selection_layout, null);
			LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.WRAP_CONTENT,
					LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
			view.setLayoutParams(param);
			//Spinner ansOption = (Spinner) view.findViewById(R.id.ansOption);
			TextView ansOption = (TextView) view.findViewById(R.id.ansOption);
			ImageView imgOpCheck = (ImageView) view.findViewById(R.id.imgOpCheck);
			ansOption.setText(this.getSelectedoption(i));
			this.setListAdapterAdapter(ansOption , this.getSelectedoption(i));
			multipleChoiceChoiceSelectionLayout.addView(view);

			if(answerType == MULTIPLE_CHOICE_ANS_TYPE){
				if(isUserAnswerSelected(this.getSelectedoption(i))){
					userAnswerSelectedList.set(position, true);
					imgOpCheck.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
				}else{
					userAnswerSelectedList.set(position, false);
					imgOpCheck.setBackgroundResource(R.drawable.mf_check_box_ipad);
				}
			}
			
			imgOpCheck.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
                    setAnswerTypeSelected(MULTIPLE_CHOICE_ANS_TYPE);
                    userAnswerSelectedList.set(position, !userAnswerSelectedList.get(position));
					if(userAnswerSelectedList.get(position).booleanValue()){
						v.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
					}else{
						v.setBackgroundResource(R.drawable.mf_check_box_ipad);
					}
				}
			});

			txtViewList.add(ansOption);
		}
	}

	/**
	 * Check User answer multiple choice question
	 * @param optionText
	 * @return
	 */
	private boolean isUserAnswerSelected(String optionText){
		if(userAnswer != null && userAnswer.length() > 0){		
			if(optionText.length() > 0){
				if(userAnswer.contains(optionText))
					return true;
				return false;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	/**
	 * Set adapter on the textview click
	 * @param txtView
	 * @param selection
	 */
	private void setListAdapterAdapter(final TextView txtView , String selection){
		txtView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				multipleChoiceOptionListTempList = new ArrayList<String>(characterlist);
				lstMultipleChoiceOptionList.setVisibility(ListView.VISIBLE);
				multipleChoiceOptionListTempList.removeAll(multipleChoiceOptionList);
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(ActMultipleChoiceAnsSheet.this, 
						R.layout.spinner_textview_layout_assign_homework_choice_option,multipleChoiceOptionListTempList);
				lstMultipleChoiceOptionList.setAdapter(adapter);
				lstMultipleChoiceOptionList.setSelection(0);
				lstMultipleChoiceOptionList.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						if(characterlist.get(position).endsWith(REMOVE_TEXT)){
							if(txtView.getText().toString().length() > 0){
								multipleChoiceOptionList.remove(txtView.getText().toString());
							}
							txtView.setText("");
						}else{
							multipleChoiceOptionList.remove(txtView.getText().toString());
							txtView.setText(multipleChoiceOptionListTempList.get(position));
							multipleChoiceOptionList.add(multipleChoiceOptionListTempList.get(position));
						}
						lstMultipleChoiceOptionList.setVisibility(ListView.GONE);
					}
				});
			}
		});
	}

	/**
	 * Set the true false layout values
	 */
	private void setTrueFalseLayout() {
		if(answerType == TRUE_FALSE_ANS_TYPE){
			if(userAnswer != null && userAnswer.length() > 0){
				if(userAnswer.equalsIgnoreCase("TRUE")){
					isAnswerTrue = ANSWER_TRUE;
					imgTrueCheck.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
					imgFalseCheck.setBackgroundResource(R.drawable.mf_check_box_ipad);
				}else if(userAnswer.equalsIgnoreCase("FALSE")){
					isAnswerTrue = ANSWER_FALSE;
					imgTrueCheck.setBackgroundResource(R.drawable.mf_check_box_ipad);
					imgFalseCheck.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
				}
			}
		}
	}
	
	/**
	 * Set the yes no layout values 
	 */
	private void setYesNoLayout() {
		if(answerType == YES_NO_ANS_TYPE){
			if(userAnswer != null && userAnswer.length() > 0){
				if(userAnswer.equalsIgnoreCase("YES")){
					isAnswerYes = ANSWER_YES;
					imgYesCheck.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
					imgNoCheck.setBackgroundResource(R.drawable.mf_check_box_ipad);
				}else if(userAnswer.equalsIgnoreCase("NO")){
					isAnswerYes = ANSWER_NO;
					imgYesCheck.setBackgroundResource(R.drawable.mf_check_box_ipad);
					imgNoCheck.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
				}
			}
		}
	}
	
	@Override
	protected void setListenerOnWidgets() {
		imgTrueCheck.setOnClickListener(this);
		imgFalseCheck.setOnClickListener(this);
		imgYesCheck.setOnClickListener(this);
		imgNoCheck.setOnClickListener(this);
		imgMultipleChoice.setOnClickListener(this);
		imgTrueFalse.setOnClickListener(this);
		imgYesNo.setOnClickListener(this);
	}

	/**
	 * Set the true false selection
	 * @param
	 */
	private void setTrueFalseSelection(int answerValue){
        this.setAnswerTypeSelected(TRUE_FALSE_ANS_TYPE);
        if(answerValue == ANSWER_TRUE){
			imgTrueCheck.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
			imgFalseCheck.setBackgroundResource(R.drawable.mf_check_box_ipad);
		}else{
			imgTrueCheck.setBackgroundResource(R.drawable.mf_check_box_ipad);
			imgFalseCheck.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
		}
	}

	/**
	 * Set the yes no selection
	 * @param
	 */
	private void setYesNoSelection(int answerValue){
        this.setAnswerTypeSelected(YES_NO_ANS_TYPE);
        if(answerValue == ANSWER_YES){
			imgYesCheck.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
			imgNoCheck.setBackgroundResource(R.drawable.mf_check_box_ipad);
		}else{
			imgYesCheck.setBackgroundResource(R.drawable.mf_check_box_ipad);
			imgNoCheck.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
		}
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.imgTrueCheck:
			isAnswerTrue = ANSWER_TRUE;
			this.setTrueFalseSelection(isAnswerTrue);
			break;
		case R.id.imgFalseCheck:
			isAnswerTrue = ANSWER_FALSE;
			this.setTrueFalseSelection(isAnswerTrue);
			break;
		case R.id.imgYesCheck:
			isAnswerYes = ANSWER_YES;
			this.setYesNoSelection(isAnswerYes);
			break;
		case R.id.imgNoCheck:
			isAnswerYes = ANSWER_NO;
			this.setYesNoSelection(isAnswerYes);
			break;
		case R.id.imgMultipleChoice:
			this.setAnswerTypeSelected(MULTIPLE_CHOICE_ANS_TYPE);
			break;
		case R.id.imgTrueFalse:
			this.setAnswerTypeSelected(TRUE_FALSE_ANS_TYPE);
			break;
		case R.id.imgYesNo:
			this.setAnswerTypeSelected(YES_NO_ANS_TYPE);
			break;
		}
	}

	@Override
	public void serverResponse(HttpResponseBase httpResponseBase,
			int requestCode) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void onBackPressed() {
		Intent intent = new Intent();
		intent.putExtra("customeAnsObj", this.getUpdatedCustomeObj(customeAnsObj));
		setResult(RESULT_OK, intent);
		super.onBackPressed();
	}
	
	/**
	 * Update the custome answer object based on answer type selection
	 * @param customeAns
	 * @return
	 */
	private CustomeAns getUpdatedCustomeObj(CustomeAns customeAns){
		customeAns.setAnswerType(answerType);
		customeAns.setFillInType(0);
		if(answerType == MULTIPLE_CHOICE_ANS_TYPE){
			customeAns.setMultipleChoiceOptionList(multipleChoiceOptionList);
			customeAns.setCorrectAns(this.getMultipleChoiceUserAns());
		}else if(answerType == TRUE_FALSE_ANS_TYPE){
			customeAns.setMultipleChoiceOptionList(null);
			if(isAnswerTrue == ANSWER_TRUE){
				customeAns.setCorrectAns(TRUE);
			}else if(isAnswerTrue == ANSWER_FALSE){
				customeAns.setCorrectAns(FALSE);
			}
		}else if(answerType == YES_NO_ANS_TYPE){
			customeAns.setMultipleChoiceOptionList(null);
			if(isAnswerYes == ANSWER_YES){
				customeAns.setCorrectAns(YES);
			}else if(isAnswerYes == ANSWER_NO){
				customeAns.setCorrectAns(NO);
			}
		}
		return customeAns;
	}

	/**
	 * Return the multiple choice correct ans
	 * @return
	 */
	private String getMultipleChoiceUserAns(){
		try{
			String correctAns = "";
			for(int i = 0 ; i < userAnswerSelectedList.size() ; i ++ ){
				if(userAnswerSelectedList.get(i).booleanValue()){
					correctAns = correctAns + txtViewList.get(i).getText().toString();
				}
			}
			return correctAns;
		}catch(Exception e){
			return "";
		}
	}
}
