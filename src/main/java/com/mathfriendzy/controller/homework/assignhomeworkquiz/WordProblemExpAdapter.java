package com.mathfriendzy.controller.homework.assignhomeworkquiz;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.model.assessmenttest.CatagoriesDto;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.CategoryListTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SubCatergoryTransferObj;
import com.mathfriendzy.utils.CommonUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class WordProblemExpAdapter extends BaseExpandableListAdapter{

	private ArrayList<CategoryListTransferObj> categoryList = null;
	public LinkedHashMap<String, ArrayList<SubCatergoryTransferObj>> mSubCatList = null;
	private LayoutInflater mInflator = null;
	private GroupViewHolder gVHolder;
	private ChiledViewHolder cVHolder;
	public ArrayList<CatagoriesDto> categoryInfoList = null;
    private final String TAG = this.getClass().getSimpleName();
    private WordProblemExxListener listener = null;

	public WordProblemExpAdapter(Context context , ArrayList<CategoryListTransferObj> categoryList
			, LinkedHashMap<String, ArrayList<SubCatergoryTransferObj>> mSubCatList,
                                 ArrayList<CatagoriesDto> categoryInfoList , WordProblemExxListener listener){
		this.categoryList = categoryList;
		this.mSubCatList = mSubCatList;
		this.categoryInfoList = categoryInfoList;
        this.listener = listener;
		mInflator = LayoutInflater.from(context);
	}

	@Override
	public int getGroupCount() {
		return categoryList.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return mSubCatList.get(categoryList.get(groupPosition).getName()).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return categoryList.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return mSubCatList.get(categoryList.get(groupPosition).getName()).get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		if(convertView == null){
			gVHolder = new GroupViewHolder();
			convertView = mInflator.inflate(R.layout.practice_skill_assign_homework_catlayout, null);
			convertView.setTag(gVHolder);
			gVHolder.imgCategorySign 	= (ImageView) convertView.findViewById(R.id.imgSign);
			gVHolder.txtCategoryName 	= (TextView)  convertView.findViewById(R.id.txtCategoryName);
		}else{
			gVHolder = (GroupViewHolder) convertView.getTag();
		}

		gVHolder.imgCategorySign.setVisibility(ImageView.INVISIBLE);
		gVHolder.txtCategoryName.setText(categoryList.get(groupPosition).getName());

		if(isCatIdExistForShowByCCSS(categoryList.get(groupPosition).getCid())){
			gVHolder.txtCategoryName.setTextColor(Color.GREEN);
		}else{
			gVHolder.txtCategoryName.setTextColor(Color.DKGRAY);
		}

		return convertView;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		final SubCatergoryTransferObj subCat = (SubCatergoryTransferObj) getChild(groupPosition, childPosition);

		if(convertView == null){
			cVHolder = new ChiledViewHolder();
			convertView = mInflator.inflate
					(R.layout.assign_practice_skill_homework_sub_cat_layout, null);

			cVHolder.txtChildLevelName = (TextView) convertView.findViewById(R.id.txtlevelName);
			cVHolder.imgCheckImage = (ImageView) convertView.findViewById(R.id.imgCheckImage);
			cVHolder.spinnerNumberOfProblem = (Spinner) 
					convertView.findViewById(R.id.spinnerNumberOfProblem);
			cVHolder.numerOfProblemLayout = (RelativeLayout) convertView.findViewById(R.id.numerOfProblemLayout);
            cVHolder.showQuestionArrowLayout = (RelativeLayout) convertView.findViewById(R.id.showQuestionArrowLayout);
            cVHolder.mainLayout = (RelativeLayout) convertView.findViewById(R.id.mainLayout);
            convertView.setTag(cVHolder);
		}else{
			cVHolder = (ChiledViewHolder) convertView.getTag();
		}

		cVHolder.numerOfProblemLayout.setVisibility(RelativeLayout.GONE);
        cVHolder.showQuestionArrowLayout.setVisibility(RelativeLayout.VISIBLE);
		cVHolder.txtChildLevelName.setText(subCat.getName());
		this.setCheckedImages(cVHolder.imgCheckImage, subCat);
		
		if(this.isSubCatExistForShowByCCSS(subCat.getId(), groupPosition)){
			cVHolder.txtChildLevelName.setTextColor(Color.GREEN);
		}else{
			cVHolder.txtChildLevelName.setTextColor(Color.DKGRAY);
		}

        cVHolder.showQuestionArrowLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onShowQuestionDetail(subCat);
            }
        });

        cVHolder.mainLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onShowQuestionDetail(subCat);
            }
        });
		return convertView;
	}

	private class GroupViewHolder{
		private ImageView imgCategorySign;
		private TextView txtCategoryName;
	}

	private class ChiledViewHolder{
		private TextView txtChildLevelName;
		private ImageView imgCheckImage;
		private Spinner spinnerNumberOfProblem;
		private RelativeLayout numerOfProblemLayout;
        private RelativeLayout showQuestionArrowLayout;
        private RelativeLayout mainLayout;
	}

	/**
	 * Set image check 
	 * @param imgCheck
	 * @param obj
	 */
	private void setCheckedImages(ImageView imgCheck , final SubCatergoryTransferObj obj){
		if(obj.isSelected()){
			imgCheck.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
		}else{
			imgCheck.setBackgroundResource(R.drawable.mf_check_box_ipad);
		}

		imgCheck.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(obj.isSelected()){
					obj.setSelected(false);
				}else{
					obj.setSelected(true);
				}
				WordProblemExpAdapter.this.notifyDataSetChanged();
			}
		});
	}

	/**
	 * Check for catgory exist for show by CCSS or not
	 * @param catId
	 * @return
	 */
	private boolean isCatIdExistForShowByCCSS(int catId){
		if(categoryInfoList != null && categoryInfoList.size() > 0){
			for(int i = 0 ; i < categoryInfoList.size() ; i ++ ){
				if(categoryInfoList.get(i).getCatId() == catId)
					return true;
			}
			return false;
		}else{
			return false;
		}
	}

	/**
	 * Check for isSubCatExistFor Show By CCSS
	 * @param subCat
	 * @param groupPosition
	 * @return
	 */
	private boolean isSubCatExistForShowByCCSS(int subCat , int groupPosition){
		return CommonUtils.getArrayListFromStringWithCommas
				(this.getSubCategoriesFromList
						(categoryList.get(groupPosition).getCid())).contains(subCat);
	}

	/**
	 * This method return the categories from the list
	 * @param catId
	 * @return
	 */
	private String getSubCategoriesFromList(int catId){
		if(categoryInfoList != null && categoryInfoList.size() > 0){
			for(int i = 0 ; i < categoryInfoList.size() ; i ++ ){
				if(categoryInfoList.get(i).getCatId() == catId)
					return categoryInfoList.get(i).getSubCatId();
			}
		}
		return "";
	}
}
