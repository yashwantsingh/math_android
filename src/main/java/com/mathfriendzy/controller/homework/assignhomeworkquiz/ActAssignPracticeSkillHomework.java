package com.mathfriendzy.controller.homework.assignhomeworkquiz;


import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.utils.CommonUtils;

public class ActAssignPracticeSkillHomework extends ActBase {

	private TextView txtPracticeSkill = null;
	private TextView txtSelectecOneOrMoreCategories = null;
	private TextView txtCategory = null;
	private TextView txtNumberOfProblems = null;
	
	private final String TAG = this.getClass().getSimpleName();

	private ArrayList<LearningCenterTransferObj> laernignCenterFunctionsList1 = null;
	private ArrayList<LearningCenterTransferObj> laernignCenterFunctionsList  = null;

	//changes for open selected categories
	private final int MAX_LENGTH   = 4;//this is for checking max length for string for multiplication

	private ExpandableListView lstCatgoryList = null;
	private LinkedHashMap<String ,ArrayList<LearningCenterTransferObj>> mapChildList = null;
	private PracticeSkillExpAdapter adapter = null;
	private ArrayList<Boolean> openSelectedCatList = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_act_assign_practice_skill_homework);

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside onCreate()");

		this.init();
		this.getIntentValues();
		this.setWidgetsReferences();
		this.setTextFromTranslation();
		this.setListenerOnWidgets();
		this.setCategoriesListLayout();

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside onCreate()");
	}

	/**
	 * Get the intent values which are set in previous screen
	 */
	@SuppressWarnings("unchecked")
	private void getIntentValues() {
		/*mapChildList = (LinkedHashMap<String, ArrayList<LearningCenterTransferObj>>)
				this.getIntent().getSerializableExtra("selectedPracticeSkill");*/

		mapChildList = this.getLinkedHashMapFromGsonJsonString
				(this.getIntent().getStringExtra("selectedPracticeSkill"));
		openSelectedCatList = (ArrayList<Boolean>) 
				this.getIntent().getSerializableExtra("openSelectedCatList");
	}

	@Override
	protected void setWidgetsReferences() {

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside setWidgetsReferences()");

		txtTopbar 		= (TextView) findViewById(R.id.txtTopbar);
		txtPracticeSkill = (TextView) findViewById(R.id.txtPracticeSkill);
		txtSelectecOneOrMoreCategories = (TextView) findViewById(R.id.txtSelectecOneOrMoreCategories);
		txtCategory = (TextView) findViewById(R.id.txtCategory);
		txtNumberOfProblems = (TextView) findViewById(R.id.txtNumberOfProblems);
		//categoriesList = (LinearLayout) findViewById(R.id.categoriesList);

		lstCatgoryList = (ExpandableListView) findViewById(R.id.lstCatgoryList);

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside setWidgetsReferences()");

	}

	/**
	 * Initialize the variables
	 */
	private void init(){
		//mapChildList = new LinkedHashMap<String, ArrayList<LearningCenterTransferObj>>();
		//openSelectedCatList = new ArrayList<Boolean>();
	}

	@Override
	protected void setListenerOnWidgets() {

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside setListenerOnWidgets()");

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside setListenerOnWidgets()");

	}

	@Override
	protected void setTextFromTranslation() {

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside setTextFromTranslation()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblAssignHomework"));
		txtPracticeSkill.setText(transeletion.getTranselationTextByTextIdentifier("lblSolveEquations"));
		txtSelectecOneOrMoreCategories.setText(transeletion.getTranselationTextByTextIdentifier("lblSelectOneOrMoreCategory"));
		txtCategory.setText(transeletion.getTranselationTextByTextIdentifier("mfLblCategorySettings"));
		txtNumberOfProblems.setText(transeletion.getTranselationTextByTextIdentifier("lblNumOfProblems"));
		transeletion.closeConnection();

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside setTextFromTranslation()");

	}

	/**
	 * Set the categories into the list layout
	 */
	private void setCategoriesListLayout(){
		this.setCategoriesList();
		if(mapChildList == null){
			mapChildList = new LinkedHashMap<String, ArrayList<LearningCenterTransferObj>>();
			for(int i = 0 ; i < laernignCenterFunctionsList.size() ; i ++ ){
				mapChildList.put(laernignCenterFunctionsList.get(i)
						.getLearningCenterOperation(), this.getSubCatList((i + 1) 
								, laernignCenterFunctionsList1.get(i)
								.getLearningCenterOperation()));
			}
		}
		this.setAdapter();
	}

	/**
	 * Set Expandable Adapter
	 */
	private void setAdapter(){
		adapter = new PracticeSkillExpAdapter
				(this, laernignCenterFunctionsList1, laernignCenterFunctionsList, mapChildList);
		lstCatgoryList.setAdapter(adapter);

		
		if(openSelectedCatList != null){
			for(int i = 0 ; i < openSelectedCatList.size() ; i ++ ){
				if(openSelectedCatList.get(i).booleanValue()){
					lstCatgoryList.expandGroup(i);	
				}
			}
		}else{
			openSelectedCatList = new ArrayList<Boolean>();
			for(int i = 0 ; i < laernignCenterFunctionsList.size() ; i ++ ){
				openSelectedCatList.add(false);
			}
		}
		
		lstCatgoryList.setOnGroupExpandListener(new OnGroupExpandListener() {
			@Override
			public void onGroupExpand(int groupPosition) {
				openSelectedCatList.set(groupPosition , true);
			}
		});
		
		lstCatgoryList.setOnGroupCollapseListener(new OnGroupCollapseListener() {
			@Override
			public void onGroupCollapse(int groupPosition) {
				openSelectedCatList.set(groupPosition , false);
			}
		});
	}

	/**
	 * This method set the categories list from the database
	 */
	private void setCategoriesList(){

		laernignCenterFunctionsList = new ArrayList<LearningCenterTransferObj>();
		laernignCenterFunctionsList1 = new ArrayList<LearningCenterTransferObj>();
		LearningCenterimpl laerningCenter = new LearningCenterimpl(this);
		laerningCenter.openConn();
		laernignCenterFunctionsList = laerningCenter.getLearningCenterFunctions();
		laerningCenter.closeConn();
		laernignCenterFunctionsList1 = MathFriendzyHelper
				.getLearningCenterCategoryListWithUpdatedCategoryName
				(laernignCenterFunctionsList, this);
		
		/*Translation transeletion = new Translation(this);
		transeletion.openConnection();

		for(int i = 0; i < laernignCenterFunctionsList.size() ; i ++ )
		{
			LearningCenterTransferObj lc = new LearningCenterTransferObj();

			if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Fractions"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier("btnTitleFractions"));
			else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Decimals"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier("btnTitleDecimals"));
			else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Negative"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier("btnTitleNegatives"));
			else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Addition"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier("btnTitleAddition"));
			else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Subtraction"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier("btnTitleSubtraction"));
			else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Multiplication"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier("btnTitleMutiplication"));
			else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Division"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier("btnTitleDivision"));
			laernignCenterFunctionsList1.add(lc);
		}
		transeletion.closeConnection();*/
	}

	/**
	 * Return the sub cat list based on operation id
	 * @param operationId
	 * @return
	 */
	private  ArrayList<LearningCenterTransferObj> getSubCatList(int operationId ,
			String operationName){
		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();
		ArrayList<LearningCenterTransferObj> childCatList = learningCenterObj
				.getMathOperationCategoriesById(operationId);


		//if(operationId == 3){//for multiplication
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		ArrayList<LearningCenterTransferObj> tempList = 
				new ArrayList<LearningCenterTransferObj>();
		for(int i = 0 ; i < childCatList.size() ; i ++ ){
			if(childCatList.get(i).getMathOperationCategory().length() > MAX_LENGTH){
				tempList.add(childCatList.get(i));
				childCatList.get(i).setTotalProblem
				(learningCenterObj.getTotalProblemInCat
						(childCatList.get(i).getMathOperationCategoryId()));
				childCatList.get(i).setLearningCenterMathOperationId(operationId);
				//childCatList.get(i).setLearningCenterOperation(operationName);//update main cat name
				childCatList.get(i).setMathOperationCategory
				(transeletion.getTranselationTextByTextIdentifier
						(childCatList.get(i).getMathOperationCategory()));//update sub cat name
				childCatList.get(i).setSelected(false);
				childCatList.get(i).setNumberOfProblemSelected(10);
			}
		}
		transeletion.closeConnection();
		learningCenterObj.closeConn();
		return tempList;
		/*}else{
			learningCenterObj.closeConn();
			return childCatList;
		}*/
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	@Override
	public void serverResponse(HttpResponseBase httpResponseBase,
			int requestCode) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent();
		intent.putExtra("selectedPracticeSkill", this.getGsonStringForLinkedHaskMap
				(adapter.mapChildList));
		intent.putExtra("openSelectedCatList", openSelectedCatList);
		setResult(RESULT_OK, intent);
		finish();
		super.onBackPressed();
	}
}
