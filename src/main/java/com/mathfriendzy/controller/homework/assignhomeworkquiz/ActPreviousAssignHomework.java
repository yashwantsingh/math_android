package com.mathfriendzy.controller.homework.assignhomeworkquiz;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.ClassSelectedListener;
import com.mathfriendzy.model.homework.HomeworkData;
import com.mathfriendzy.model.homework.SelectAssignHomeworkClassParam;
import com.mathfriendzy.model.homework.assignhomework.GetPreviousAssignHomeworkParam;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.classes.ClassWithName;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ActPreviousAssignHomework extends ActBase {

    private final String TAG = this.getClass().getSimpleName();

    private TextView txtSelectAPrevHW = null;
    private TextView txtAssignDate = null;
    private RelativeLayout rlFromDateLayout = null;
    private TextView edtDatePickerFrom = null;
    private RelativeLayout rlToDateLayout = null;
    private TextView edtDatePickerTo = null;
    private RelativeLayout selectClassLayout = null;
    private TextView txtSelectClassValue = null;
    private TextView txtSubject = null;
    private TextView txtSubjectValues = null;
    private TextView txtClass = null;
    private TextView txtClassValues = null;
    private LinearLayout lstPreviousHW = null;
    private Button btnShowMore = null;
    private TextView txtTo = null;

    private String startDate = "";
    private String endDate = "";
    private final String SERVICE_TIME_MY_FORMATE = "yyyy-MM-dd";
    private final String SERVICE_TIME_CONVERTED_FORMATE = "MMM. dd, yyyy";
    private String lblStartDateCantLater = null;

    //private ArrayList<HomeworkData> homeworkDataList = null;

    private final int RECORD_LIMIT = 10;
    private int offset = 0;
    private ProgressDialog pd = null;
    private CustomeAdapterForLinearLayout adapter = null;
    private String selectedClassIds = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_previous_assign_homework);

        CommonUtils.printLog("inside onCreate()");

        this.init();
        this.getIntentValues();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        //this.setListAdapter();
        this.setShowMoreButtonVisiblity(false);
        this.setInitialStartEndDate();
        this.getPreviousHomeworks(selectedClassIds);
        //this.showInitialPopUp();

        CommonUtils.printLog("outside onCreate()");
    }

    private void showInitialPopUp(){
        MathFriendzyHelper.showWarningDialog(this ,
                MathFriendzyHelper.getTreanslationTextById(this , "lblPleaseSelectClassToGetPrevHw"));
    }

    private void init() {
        pd = MathFriendzyHelper.getProgressDialog(this);
        registerUserFromPreff = MathFriendzyHelper.getUserFromPreff(this);
        startDate  = MathFriendzyHelper.getBackAndForthDateInGiveGformateDate
                (SERVICE_TIME_CONVERTED_FORMATE, -1 , -1);
        endDate = MathFriendzyHelper.getBackAndForthDateInGiveGformateDate
                (SERVICE_TIME_CONVERTED_FORMATE , 0 , -1);
        //this.initClasses();
    }

    private void getIntentValues() {
    }

    @Override
    protected void setWidgetsReferences() {
        txtTopbar 		= (TextView) findViewById(R.id.txtTopbar);
        txtSelectAPrevHW = (TextView) findViewById(R.id.txtSelectAPrevHW);
        txtAssignDate = (TextView) findViewById(R.id.txtAssignDate);
        rlFromDateLayout = (RelativeLayout) findViewById(R.id.rlFromDateLayout);
        txtTo = (TextView) findViewById(R.id.txtTo);
        edtDatePickerFrom = (TextView) findViewById(R.id.edtDatePickerFrom);
        rlToDateLayout = (RelativeLayout) findViewById(R.id.rlToDateLayout);
        edtDatePickerTo = (TextView) findViewById(R.id.edtDatePickerTo);
        selectClassLayout = (RelativeLayout) findViewById(R.id.selectClassLayout);
        txtSelectClassValue = (TextView) findViewById(R.id.txtSelectClassValue);
        txtSubject = (TextView) findViewById(R.id.txtSubject);
        txtSubjectValues = (TextView) findViewById(R.id.txtSubjectValues);
        txtClass = (TextView) findViewById(R.id.txtClass);
        txtClassValues = (TextView) findViewById(R.id.txtClassValues);

        lstPreviousHW = (LinearLayout) findViewById(R.id.lstPreviousHW);
        btnShowMore = (Button) findViewById(R.id.btnShowMore);
    }

    @Override
    protected void setListenerOnWidgets() {
        rlFromDateLayout.setOnClickListener(this);
        rlToDateLayout.setOnClickListener(this);
        selectClassLayout.setOnClickListener(this);
        btnShowMore.setOnClickListener(this);
    }

    @Override
    protected void setTextFromTranslation() {
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblAssignHomework"));
        txtSelectAPrevHW.setText(transeletion.getTranselationTextByTextIdentifier("lblSelectAPrevHW"));
        txtAssignDate.setText(transeletion.getTranselationTextByTextIdentifier("lblAssignedDate"));
        txtTo.setText(transeletion.getTranselationTextByTextIdentifier("lblTo"));
        txtSubject.setText(transeletion.getTranselationTextByTextIdentifier("lblSubjectHeader") + ":");
        txtClass.setText(transeletion.getTranselationTextByTextIdentifier("lblClass") + ":");
        edtDatePickerFrom.setHint(transeletion.getTranselationTextByTextIdentifier("lblSelect"));
        edtDatePickerTo.setHint(transeletion.getTranselationTextByTextIdentifier("lblSelect"));
        txtSelectClassValue.setHint(transeletion.getTranselationTextByTextIdentifier("lblSelect"));
        btnShowMore.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleShowMore"));

        lblStartDateCantLater = transeletion.getTranselationTextByTextIdentifier("lblStartDateCantLater");
        transeletion.closeConnection();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.rlFromDateLayout:
                this.selectFromDate();
                break;
            case R.id.rlToDateLayout:
                this.selectToDate();
                break;
            case R.id.selectClassLayout:
                this.clickToSelectClass();
                break;
            case R.id.btnShowMore:
                this.getPreviousHomeworks(selectedClassIds);
                break;
        }
    }

    /**
     * Set the start and end date initially
     */
    private void setInitialStartEndDate() {
        try {
            edtDatePickerFrom.setText(startDate);
            edtDatePickerTo.setText(endDate);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Show from date from date picker
     */
    private void selectFromDate(){
        final Calendar calender = MathFriendzyHelper.getCurrentCalender();
        MathFriendzyHelper.showDatePickerDialogPrevDateSelected(this, calender.get(Calendar.YEAR),
                calender.get(Calendar.MONTH), calender.get(Calendar.DAY_OF_MONTH),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);

                        //if (newDate.after(calender)) {
                        setDateText(edtDatePickerFrom, year, monthOfYear, dayOfMonth, true);
                        //}
                    }
                }, calender);
    }

    /**
     * Show to date from date picker
     */
    private void selectToDate(){
        final Calendar calender = MathFriendzyHelper.getCurrentCalender();
        MathFriendzyHelper.showDatePickerDialogPrevDateSelected(this, calender.get(Calendar.YEAR),
                calender.get(Calendar.MONTH), calender.get(Calendar.DAY_OF_MONTH),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);

                        //if (newDate.after(calender)) {
                        setDateText(edtDatePickerTo, year, monthOfYear, dayOfMonth, false);
                        //}
                    }
                }, calender);
    }


    /**
     * Set date text
     * @param txtView
     * @param year
     * @param month
     * @param dayOfMonth
     * @param isStartDate
     */
    private void setDateText(TextView txtView , int year , int month , int dayOfMonth ,
                             boolean isStartDate){

        String tempStartDate = startDate;
        String tempEndDate   = endDate;

        month ++;
        String formateString = MathFriendzyHelper.getFourDigitFormatNumber(year) + "-" +
                MathFriendzyHelper.getDoubleDigitFormatNumber(month) + "-" +
                MathFriendzyHelper.getDoubleDigitFormatNumber(dayOfMonth);
        String formatedDate = MathFriendzyHelper.formatDataInGivenFormat(formateString ,
                SERVICE_TIME_MY_FORMATE, SERVICE_TIME_CONVERTED_FORMATE);
        if(isStartDate){
            tempStartDate = formatedDate;
        }else{
            tempEndDate = formatedDate;
        }
        Date fromDate = MathFriendzyHelper.getValidateDate(tempStartDate , SERVICE_TIME_CONVERTED_FORMATE);
        Date toDate   = MathFriendzyHelper.getValidateDate(tempEndDate , SERVICE_TIME_CONVERTED_FORMATE);
        try {
            if(fromDate != null && toDate != null) {
                if (fromDate.compareTo(toDate) <= 0) {
                    startDate = tempStartDate;
                    endDate = tempEndDate;
                    //txtView.setText(formatedDate);
                    this.setDateTextToTextView(txtView , formatedDate);
                } else {
                    MathFriendzyHelper.showWarningDialog(this, lblStartDateCantLater);
                }
            }else{
                startDate = tempStartDate;
                endDate = tempEndDate;
                //txtView.setText(formatedDate);
                this.setDateTextToTextView(txtView , formatedDate);
            }
        }catch (Exception e){
            e.printStackTrace();
            startDate = tempStartDate;
            endDate = tempEndDate;
            txtView.setText(formatedDate);
        }
    }

    /**
     * Set the date text
     * @param txtView
     * @param dateText
     */
    private void setDateTextToTextView(TextView txtView , String dateText){
        if(CommonUtils.isInternetConnectionAvailable(this)) {
            this.resetValues();
            String selectedClass = "0";
            if(selectedClassList != null && selectedClassList.size() > 0)
                selectedClass = getCommaSeparatedClassIds(selectedClassList);
            this.getPreviousHomeworks(selectedClass);
        }else{
            CommonUtils.showInternetDialog(this);
        }
        txtView.setText(dateText);
    }

    private ArrayList<ClassWithName> classWithNames = null;
    private ArrayList<ClassWithName> selectedClassList = null;
    private RegistereUserDto registerUserFromPreff = null;

    private void initClasses(ArrayList<ClassWithName> classWithNames ,
                             int lastClassAssignedForHw){
        this.classWithNames = classWithNames;
        if(classWithNames != null && classWithNames.size() > 0){
            for(int i = 0 ; i < classWithNames.size() ; i ++ ){
                if(classWithNames.get(i).getClassId() == lastClassAssignedForHw){
                    //classWithNames.get(i).setSelected(true);
                    //initialize the selected class
                    selectedClassList = new ArrayList<ClassWithName>();
                    selectedClassList.add(classWithNames.get(i));
                    this.setTextToSelectedClassValues(classWithNames.get(i).getClassName());
                    this.setTextToSelectedSubjectValues(classWithNames.get(i).getSubject());
                    return ;
                }
            }
        }else{
            selectedClassList = null;
            this.setTextToSelectedClassValues("");
            this.setTextToSelectedSubjectValues("");
        }
    }

    private void clickToSelectClass() {
        SelectAssignHomeworkClassParam param = new SelectAssignHomeworkClassParam();
        param.setShowForPrevSaveHomework(true);
        MathFriendzyHelper.showClassSelectionDialog(this , classWithNames , new ClassSelectedListener() {
            @Override
            public void onClassSelectionDone(ArrayList<ClassWithName> selectedClass) {
                selectedClassList = selectedClass;
                if(selectedClass != null && selectedClass.size() > 0){
                    setTextToSelectedClassValues(getCommaSeparatedClassName(selectedClass));
                    setTextToSelectedSubjectValues(getCommaSeparatedSubjectName(selectedClass));
                }else{
                    setTextToSelectedClassValues("");
                    setTextToSelectedSubjectValues("");
                }

                resetValues();
                getPreviousHomeworks(getCommaSeparatedClassIds(selectedClass));
            }

            @Override
            public void onUpdatedClasses(ArrayList<ClassWithName> updatedList) {
                classWithNames = updatedList;
            }
        } , param);
    }

    private void resetValues(){
        try {
            offset = 0;
            adapter = null;
            if (lstPreviousHW != null) {
                lstPreviousHW.removeAllViews();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Get the saved homework from server
     */
    private void getPreviousHomeworks(String selectedClassIds) {
        this.selectedClassIds = selectedClassIds;
        if(CommonUtils.isInternetConnectionAvailable(this)){
            MathFriendzyHelper.showProgressDialog(pd);
            GetPreviousAssignHomeworkParam param = new GetPreviousAssignHomeworkParam();
            param.setAction("getPreviousHomework");
            param.setUserId(registerUserFromPreff.getUserId());
            param.setStartDate(MathFriendzyHelper.formatDataInGivenFormat(startDate,
                    SERVICE_TIME_CONVERTED_FORMATE, SERVICE_TIME_MY_FORMATE));
            param.setEndDate(MathFriendzyHelper.formatDataInGivenFormat(endDate ,
                    SERVICE_TIME_CONVERTED_FORMATE , SERVICE_TIME_MY_FORMATE));
            param.setOffset(offset);
            param.setLimit(RECORD_LIMIT);
            param.setClassIds(selectedClassIds);
            new MyAsyckTask(ServerOperation.createPostRequestForGetPreviousAssignedHw(param)
                    , null, ServerOperationUtil.GET_PREV_HOMEWORK_FROM_SERVER_REQUEST, this,
                    this, ServerOperationUtil.SIMPLE_DIALOG , false ,
                    this.getString(R.string.please_wait_dialog_msg))
                    .execute();
        }else{
            CommonUtils.showInternetDialog(this);
        }
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
        if(requestCode == ServerOperationUtil.GET_PREV_HOMEWORK_FROM_SERVER_REQUEST){
            //MathFriendzyHelper.hideDialog(pd);
            HomeworkData homeworkData = (HomeworkData) httpResponseBase;

            if(homeworkData != null) {
                if(homeworkData.isClassArrayExist()) {
                    ArrayList<ClassWithName> classWithNames = homeworkData.getClassWithNames();
                    if (classWithNames != null && classWithNames.size() > 0) {
                        this.initClasses(classWithNames, homeworkData.getLastClassAssignedForHw());
                    }else{
                        if(classWithNames == null)
                            classWithNames = new ArrayList<ClassWithName>();
                        this.initClasses(classWithNames, homeworkData.getLastClassAssignedForHw());
                        MathFriendzyHelper.showWarningDialog(this,
                                MathFriendzyHelper.getTreanslationTextById(this, "lblThereAreNoClass"),
                                new HttpServerRequest() {
                                    @Override
                                    public void onRequestComplete() {
                                        //finish();
                                    }
                                });
                        MathFriendzyHelper.hideDialog(pd);
                        return;
                    }
                }
            }

            final ArrayList<HomeworkData> homeworkDataList = homeworkData.getHomeworkDatas();
            if(homeworkDataList != null && homeworkDataList.size() > 0){
                if(homeworkDataList.size() >= RECORD_LIMIT){
                    offset = offset + RECORD_LIMIT;
                    this.setShowMoreButtonVisiblity(true);
                }else{
                    this.setShowMoreButtonVisiblity(false);
                }
            }else{
                this.setShowMoreButtonVisiblity(false);
            }

            new AsyncTask<Void, Void, Void>(){
                @Override
                protected Void doInBackground(Void... params) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setListAdapter(homeworkDataList);
                        }
                    });
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    MathFriendzyHelper.hideDialog(pd);
                    super.onPostExecute(aVoid);
                }
            }.execute();
            //this.setListAdapter(homeworkDataList);
        }
    }

    private String getCommaSeparatedClassIds(ArrayList<ClassWithName> selectedClassList) {
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0 ; i < selectedClassList.size() ; i ++ ){
            stringBuilder.append(stringBuilder.length() > 0 ? "," + selectedClassList.get(i).getClassId()
                    : selectedClassList.get(i).getClassId());
        }
        return stringBuilder.toString();
    }

    private void setTextToSelectedClassValues(String commaSeparatedClasses){
        txtSelectClassValue.setText(commaSeparatedClasses);
        txtClassValues.setText(commaSeparatedClasses);
    }

    private void setTextToSelectedSubjectValues(String commaSeparatedSubjects){
        txtSubjectValues.setText(commaSeparatedSubjects);
    }

    private String getCommaSeparatedClassName(ArrayList<ClassWithName> selectedClassList){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0 ; i < selectedClassList.size() ; i ++ ){
            stringBuilder.append(stringBuilder.length() > 0 ? "," + selectedClassList.get(i).getClassName()
                    : selectedClassList.get(i).getClassName());
        }
        return stringBuilder.toString();
    }

    private String getCommaSeparatedSubjectName(ArrayList<ClassWithName> selectedClassList){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0 ; i < selectedClassList.size() ; i ++ ){
            stringBuilder.append(stringBuilder.length() > 0 ? "," + selectedClassList.get(i).getSubject()
                    : selectedClassList.get(i).getSubject());
        }
        return stringBuilder.toString();
    }

    private void setListAdapter(ArrayList<HomeworkData> homeworkDataList) {
        if(homeworkDataList != null && homeworkDataList.size() > 0) {
            if(adapter != null){
                adapter.addMoreRecordAndUpdate(homeworkDataList);
                return ;
            }
            /*adapter = new HomeworkListAdapter(this, homeworkDataList, true ,
                    new SavedHomeworkSelectListener() {
                @Override
                public void onSelectListener(int index, HomeworkData selectedHomeworkData) {
                    setSelectedHomeworkDataAndFinish(selectedHomeworkData);
                }
            });
            lstPreviousHW.setAdapter(adapter);*/
            adapter = new CustomeAdapterForLinearLayout(this, homeworkDataList, true ,
                    new SavedHomeworkSelectListener() {
                        @Override
                        public void onSelectListener(int index, HomeworkData selectedHomeworkData) {
                            setSelectedHomeworkDataAndFinish(selectedHomeworkData);
                        }
                    } , lstPreviousHW);
            adapter.setInflatedViews();
        }else{
            if(offset == 0) {
                MathFriendzyHelper.showWarningDialog(this,
                        MathFriendzyHelper.getTreanslationTextById(this, "lblYouDontHavePrevHW"),
                        new HttpServerRequest() {
                            @Override
                            public void onRequestComplete() {
                                //finish();
                            }
                        });
            }
        }
    }

    private void setShowMoreButtonVisiblity(boolean isVisible){
        if(isVisible){
            btnShowMore.setVisibility(Button.VISIBLE);
        }else{
            btnShowMore.setVisibility(Button.INVISIBLE);
        }
    }

    private void setSelectedHomeworkDataAndFinish(HomeworkData selectedHomeworkData){
        selectedHomeworkData.setUserClasses(selectedClassList);
        Intent intent = new Intent();
        intent.putExtra("selectedHWData" , selectedHomeworkData);
        setResult(RESULT_OK, intent);
        finish();
    }
}
