package com.mathfriendzy.controller.homework.assignhomeworkquiz;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.ClassSelectedListener;
import com.mathfriendzy.model.homework.HomeworkData;
import com.mathfriendzy.model.homework.SelectAssignHomeworkClassParam;
import com.mathfriendzy.model.homework.savehomework.GetSavedHomeworkParam;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.classes.ClassWithName;
import com.mathfriendzy.model.registration.classes.UserClasses;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;

import java.util.ArrayList;

public class ActSavedHomework extends ActBase {

    private final String TAG = this.getClass().getSimpleName();

    private TextView txtSelectAPrevHW = null;
    private RelativeLayout selectClassLayout = null;
    private TextView txtSelectClassValue = null;
    private TextView txtSubject = null;
    private TextView txtSubjectValues = null;
    private TextView txtClass = null;
    private TextView txtClassValues = null;
    private LinearLayout lstPreviousHW = null;
    private Button btnShowMore = null;

    private final int RECORD_LIMIT = 10;
    private int offset = 0;
    private ProgressDialog pd = null;
    private CustomeAdapterForLinearLayout adapter = null;
    private String selectedClassIds = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_saved_homework);

        CommonUtils.printLog("inside onCreate()");

        this.init();
        this.getIntentValues();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        //this.getSavedHomework();
        this.setShowMoreButtonVisiblity(false);
        this.showInitialPopUp();

        CommonUtils.printLog("outside onCreate()");
    }

    private void showInitialPopUp(){
        MathFriendzyHelper.showWarningDialog(this ,
                MathFriendzyHelper.getTreanslationTextById(this , "lblPleaseSelectClassToGetSavedHw"));
    }

    private void init() {
        pd = MathFriendzyHelper.getProgressDialog(this);
        registerUserFromPreff = MathFriendzyHelper.getUserFromPreff(this);
        this.initClasses();
    }

    private void getIntentValues() {
    }

    @Override
    protected void setWidgetsReferences() {
        txtTopbar 		= (TextView) findViewById(R.id.txtTopbar);
        txtSelectAPrevHW = (TextView) findViewById(R.id.txtSelectAPrevHW);
        selectClassLayout = (RelativeLayout) findViewById(R.id.selectClassLayout);
        txtSelectClassValue = (TextView) findViewById(R.id.txtSelectClassValue);
        txtSubject = (TextView) findViewById(R.id.txtSubject);
        txtSubjectValues = (TextView) findViewById(R.id.txtSubjectValues);
        txtClass = (TextView) findViewById(R.id.txtClass);
        txtClassValues = (TextView) findViewById(R.id.txtClassValues);

        lstPreviousHW = (LinearLayout) findViewById(R.id.lstPreviousHW);
        btnShowMore = (Button) findViewById(R.id.btnShowMore);
    }

    @Override
    protected void setListenerOnWidgets() {
        selectClassLayout.setOnClickListener(this);
        btnShowMore.setOnClickListener(this);
    }

    @Override
    protected void setTextFromTranslation() {
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblAssignHomework"));
        txtSelectAPrevHW.setText(transeletion.getTranselationTextByTextIdentifier("lblSelectASavedHW"));
        txtSubject.setText(transeletion.getTranselationTextByTextIdentifier("lblSubjectHeader"));
        txtClass.setText(transeletion.getTranselationTextByTextIdentifier("lblClass"));
        txtSelectClassValue.setHint(transeletion.getTranselationTextByTextIdentifier("lblSelect")
                + " " + transeletion.getTranselationTextByTextIdentifier("lblClass"));
        btnShowMore.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleShowMore"));
        transeletion.closeConnection();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.selectClassLayout:
                this.clickToSelectClass();
                break;
            case R.id.btnShowMore:
                this.getSavedHomework(selectedClassIds);
                break;
        }
    }

    private ArrayList<ClassWithName> classWithNames = null;
    private ArrayList<ClassWithName> selectedClassList = null;
    private RegistereUserDto registerUserFromPreff = null;

    private void initClasses(){
        classWithNames = this.getUserClasses();
    }

    private ArrayList<ClassWithName> getUserClasses(){
        try {
            if (registerUserFromPreff != null) {
                ArrayList<UserClasses> userClasseses = registerUserFromPreff.getUserClasses();
                ArrayList<ClassWithName> allGradeClasses = new ArrayList<ClassWithName>();
                for (int i = 0; i < userClasseses.size(); i++) {
                    UserClasses userClass = userClasseses.get(i);
                    allGradeClasses.addAll(userClass.getClassList());
                }
                return allGradeClasses;
            } else {
                return null;
            }
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    private void clickToSelectClass() {
        if(!(classWithNames != null && classWithNames.size() > 0)){
            MathFriendzyHelper.showWarningDialog(this ,
                    MathFriendzyHelper.getTreanslationTextById(this , "lblNoClass"));
            return ;
        }
        SelectAssignHomeworkClassParam param = new SelectAssignHomeworkClassParam();
        param.setShowForPrevSaveHomework(true);
        MathFriendzyHelper.showClassSelectionDialog(this , classWithNames , new ClassSelectedListener() {
            @Override
            public void onClassSelectionDone(ArrayList<ClassWithName> selectedClass) {
                selectedClassList = selectedClass;
                if(selectedClass != null && selectedClass.size() > 0){
                    setTextToSelectedClassValues(getCommaSeparatedClassName(selectedClass));
                    setTextToSelectedSubjectValues(getCommaSeparatedSubjectName(selectedClass));
                }else{
                    setTextToSelectedClassValues("");
                    setTextToSelectedSubjectValues("");
                }

                resetValues();
                getSavedHomework(getCommaSeparatedClassIds(selectedClass));
            }

            @Override
            public void onUpdatedClasses(ArrayList<ClassWithName> updatedList) {
                classWithNames = updatedList;
            }
        } , param);
    }

    private void setTextToSelectedClassValues(String commaSeparatedClasses){
        txtSelectClassValue.setText(commaSeparatedClasses);
        txtClassValues.setText(commaSeparatedClasses);
    }

    private void setTextToSelectedSubjectValues(String commaSeparatedSubjects){
        txtSubjectValues.setText(commaSeparatedSubjects);
    }

    private String getCommaSeparatedClassName(ArrayList<ClassWithName> selectedClassList){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0 ; i < selectedClassList.size() ; i ++ ){
            stringBuilder.append(stringBuilder.length() > 0 ? "," + selectedClassList.get(i).getClassName()
                    : selectedClassList.get(i).getClassName());
        }
        return stringBuilder.toString();
    }

    private String getCommaSeparatedSubjectName(ArrayList<ClassWithName> selectedClassList){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0 ; i < selectedClassList.size() ; i ++ ){
            stringBuilder.append(stringBuilder.length() > 0 ? "," + selectedClassList.get(i).getSubject()
                    : selectedClassList.get(i).getSubject());
        }
        return stringBuilder.toString();
    }

    private String getCommaSeparatedClassIds(ArrayList<ClassWithName> selectedClassList) {
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0 ; i < selectedClassList.size() ; i ++ ){
            stringBuilder.append(stringBuilder.length() > 0 ? "," + selectedClassList.get(i).getClassId()
                    : selectedClassList.get(i).getClassId());
        }
        return stringBuilder.toString();
    }

    private void resetValues(){
        try {
            offset = 0;
            adapter = null;
            if (lstPreviousHW != null) {
                lstPreviousHW.removeAllViews();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Get the saved homework from server
     */
    private void getSavedHomework(String selectedClassIds) {
        this.selectedClassIds = selectedClassIds;
        if(CommonUtils.isInternetConnectionAvailable(this)){
            MathFriendzyHelper.showProgressDialog(pd);
            GetSavedHomeworkParam param = new GetSavedHomeworkParam();
            param.setAction("getSavedHomework");
            param.setUserId(registerUserFromPreff.getUserId());
            param.setOffset(offset);
            param.setLimit(RECORD_LIMIT);
            param.setClassIds(selectedClassIds);
            new MyAsyckTask(ServerOperation.createPostRequestForGetSavedHomework(param)
                    , null, ServerOperationUtil.GET_SAVED_HOMEWORK_FROM_SERVER_REQUEST, this,
                    this, ServerOperationUtil.SIMPLE_DIALOG , false ,
                    this.getString(R.string.please_wait_dialog_msg))
                    .execute();
        }else{
            CommonUtils.showInternetDialog(this);
        }
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
        if(requestCode == ServerOperationUtil.GET_SAVED_HOMEWORK_FROM_SERVER_REQUEST){
            //MathFriendzyHelper.hideDialog(pd);
            HomeworkData homeworkData = (HomeworkData) httpResponseBase;
            final ArrayList<HomeworkData> homeworkDataList = homeworkData.getHomeworkDatas();
            if(homeworkDataList != null && homeworkDataList.size() > 0){
                if(homeworkDataList.size() >= RECORD_LIMIT){
                    offset = offset + RECORD_LIMIT;
                    this.setShowMoreButtonVisiblity(true);
                }else{
                    this.setShowMoreButtonVisiblity(false);
                }
            }else{
                this.setShowMoreButtonVisiblity(false);
            }

            new AsyncTask<Void, Void, Void>(){
                @Override
                protected Void doInBackground(Void... params) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setListAdapter(homeworkDataList);
                        }
                    });
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    MathFriendzyHelper.hideDialog(pd);
                    super.onPostExecute(aVoid);
                }
            }.execute();
            //this.setListAdapter(homeworkDataList);
        }
    }


    private void setListAdapter(ArrayList<HomeworkData> homeworkDataList) {
        if(homeworkDataList != null && homeworkDataList.size() > 0) {
            if(adapter != null){
                adapter.addMoreRecordAndUpdate(homeworkDataList);
                return ;
            }
            /*adapter = new HomeworkListAdapter(this, homeworkDataList, true ,
                    new SavedHomeworkSelectListener() {
                @Override
                public void onSelectListener(int index, HomeworkData selectedHomeworkData) {
                    setSelectedHomeworkDataAndFinish(selectedHomeworkData);
                }
            });
            lstPreviousHW.setAdapter(adapter);*/
            adapter = new CustomeAdapterForLinearLayout(this, homeworkDataList, true ,
                    new SavedHomeworkSelectListener() {
                        @Override
                        public void onSelectListener(int index, HomeworkData selectedHomeworkData) {
                            setSelectedHomeworkDataAndFinish(selectedHomeworkData);
                        }
                    } , lstPreviousHW);
            adapter.setInflatedViews();
        }else{
            if(offset == 0) {
                MathFriendzyHelper.showWarningDialog(this,
                        MathFriendzyHelper.getTreanslationTextById(this, "lblYouDontHaveSaveHW"),
                        new HttpServerRequest() {
                            @Override
                            public void onRequestComplete() {
                                //finish();
                            }
                        });
            }
        }
    }

    private void setShowMoreButtonVisiblity(boolean isVisible){
        if(isVisible){
            btnShowMore.setVisibility(Button.VISIBLE);
        }else{
            btnShowMore.setVisibility(Button.INVISIBLE);
        }
    }
    private void setSelectedHomeworkDataAndFinish(HomeworkData selectedHomeworkData){
        selectedHomeworkData.setUserClasses(selectedClassList);
        Intent intent = new Intent();
        intent.putExtra("selectedHWData" , selectedHomeworkData);
        setResult(RESULT_OK , intent);
        finish();
    }
}
