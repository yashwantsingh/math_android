package com.mathfriendzy.controller.homework.assignhomeworkquiz;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.homework.CustomeAns;

import java.util.ArrayList;

public class AssignCustomeQuizzAdapter extends BaseAdapter {

    public ArrayList<CustomeAns> customeAnsList = null;
    private LayoutInflater mInfltor = null;
    private ViewHolder vHolder = null;
    public int clickedPosition = 0;
    private final int FILL_IN_TYPE = 1;
    private final int MULTIPLE_CHOICE_ANS_TYPE = 1;
    private final int TRUE_FALSE_ANS_TYPE = 2;
    private final int YES_NO_ANS_TYPE = 3;

	/*private ArrayList<RelativeLayout> editLayoutDialoglist = null;
    private ArrayList<RelativeLayout> fillLayoutDialogList = null;
	private ArrayList<RelativeLayout> nonFillInLayoutDialogList = null;*/

    private Context context = null;
    private ActAssignCustomAns actObj = null;
    private boolean isNeedAnimation = true;

    //for Student Correct And
//	public TextView selectedTextView = null;
    public EditText selectedTextView = null;

    private int selectedTextViewposition = -1;

    private String lblAnswerOnWorkAreaCreditGiven = "";
    private String lblOnWorkArea = "";

    public AssignCustomeQuizzAdapter(Context context, ArrayList<CustomeAns> customeAnsList) {
        this.customeAnsList = customeAnsList;
        mInfltor = LayoutInflater.from(context);

        this.context = context;

        actObj = (ActAssignCustomAns) context;

        //MathFriendzyHelper.getTreanslationTextById(context , "lblbOnWorkArea" , "lblAnswerOnWorkAreaCreditGiven");
        String[] textArray = MathFriendzyHelper
                .getTreanslationTextById(context, "lblAnsOnWorkArea", "lblOnWorkArea");
        lblAnswerOnWorkAreaCreditGiven = textArray[0];
        lblOnWorkArea = textArray[1];
    }

    @Override
    public int getCount() {
        return customeAnsList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            vHolder = new ViewHolder();
            convertView = mInfltor.inflate(R.layout.assign_homework_custome_quizz_layout, null);
            vHolder.fillInLayout = (RelativeLayout) convertView.findViewById(R.id.fillInLayout);
            vHolder.multipleChoiceLayout = (RelativeLayout) convertView.findViewById(R.id.multipleChoiceLayout);
            vHolder.trueFalseLayout = (RelativeLayout) convertView.findViewById(R.id.trueFalseLayout);
            vHolder.txtNumber = (EditText) convertView.findViewById(R.id.txtNumber);
            vHolder.editLayout = (RelativeLayout) convertView.findViewById(R.id.editLayout);
            vHolder.fillEditDialog = (RelativeLayout) convertView.findViewById(R.id.fillEditDialog);
            vHolder.nonfillEditDialog = (RelativeLayout) convertView.findViewById(R.id.nonfillEditDialog);
            vHolder.btnEdit = (Button) convertView.findViewById(R.id.btnEdit);

            vHolder.btnFillInAddLine = (Button) convertView.findViewById(R.id.btnFillInAddLine);
            vHolder.btnMultipleChoice = (Button) convertView.findViewById(R.id.btnMultipleChoice);
            vHolder.bntDeleteFillIn = (Button) convertView.findViewById(R.id.bntDeleteFillIn);

            vHolder.btnNofillInEdit = (Button) convertView.findViewById(R.id.btnNofillInEdit);
            vHolder.btnAddNonFillInLine = (Button) convertView.findViewById(R.id.btnAddNonFillInLine);
            vHolder.btnSingleAnswer = (Button) convertView.findViewById(R.id.btnSingleAnswer);
            vHolder.bntDeleteNonFillIn = (Button) convertView.findViewById(R.id.bntDeleteNonFillIn);

            //for fillIn layout
            vHolder.imgCheckLeftUnit = (ImageView) convertView.findViewById(R.id.imgCheckLeftUnit);
            vHolder.imgCheckRightUnit = (ImageView) convertView.findViewById(R.id.imgCheckRightUnit);
            vHolder.edtFillInStudentAnswer = (TextView) convertView.findViewById(R.id.edtFillInStudentAnswer);
            vHolder.edtUnitOfMeasurement = (EditText) convertView.findViewById(R.id.edtUnitOfMeasurement);

            //for multiple choice layout
            vHolder.optionLayout = (LinearLayout) convertView.findViewById(R.id.optionLayout);

            //for true false or yes no layout
            vHolder.ansTrue = (TextView) convertView.findViewById(R.id.ansTrue);
            vHolder.ansFalse = (TextView) convertView.findViewById(R.id.ansFalse);

            //for new change regarding the work area button in sliding
            vHolder.btnOnworkAreaFillIn = (Button) convertView.findViewById(R.id.btnOnworkAreaFillIn);
            vHolder.btnOnworkAreaNonFillIn = (Button) convertView.findViewById(R.id.btnOnworkAreaNonFillIn);

            convertView.setTag(vHolder);
        } else {
            vHolder = (ViewHolder) convertView.getTag();
        }

		/*editLayoutDialoglist.set(position, vHolder.editLayout);
		fillLayoutDialogList.set(position, vHolder.fillEditDialog);
		nonFillInLayoutDialogList.set(position, vHolder.nonfillEditDialog);*/

        vHolder.txtNumber.setText(customeAnsList.get(position).getQueNo());
        this.setVisibilityOfLayout(customeAnsList.get(position),
                vHolder.fillInLayout, vHolder.multipleChoiceLayout,
                vHolder.trueFalseLayout);

        if (customeAnsList.get(position).isVisibleLayout()) {
            this.setVisibilityOfLayout(vHolder.editLayout, true, true);
            if (customeAnsList.get(position).getFillInType() == FILL_IN_TYPE) {
                this.setVisibilityOfLayout(vHolder.fillEditDialog, true, false);
                this.setVisibilityOfLayout(vHolder.nonfillEditDialog, false, false);
            } else {
                this.setVisibilityOfLayout(vHolder.fillEditDialog, false, false);
                this.setVisibilityOfLayout(vHolder.nonfillEditDialog, true, false);
            }
        } else {
            this.setVisibilityOfLayout(vHolder.editLayout, false, true);
            this.setVisibilityOfLayout(vHolder.fillEditDialog, false, false);
            this.setVisibilityOfLayout(vHolder.nonfillEditDialog, false, false);
        }

        this.setFillInLayout(customeAnsList.get(position), vHolder, position);
        this.setMultiplechoiceLayout(customeAnsList.get(position), vHolder);
        this.setTrueFalseOrYesNoLayout(customeAnsList.get(position), vHolder);

        vHolder.btnEdit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setVisibilityOfEditLayout(position);
                actObj.hideDeviceKeyboardAndClearFocus();
            }
        });


        vHolder.btnFillInAddLine.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setVisibilityOfCustomeAnsLayout(position, false);
                customeAnsList.add(position + 1, actObj.getCustomeAnsObj("", 1, 1));
                AssignCustomeQuizzAdapter.this.notifyDataSetChanged();
                actObj.hideDeviceKeyboardAndClearFocus();
            }
        });

        vHolder.btnMultipleChoice.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                actObj.hideDeviceKeyboardAndClearFocus();
                clickedPosition = position;
                setVisibilityOfCustomeAnsLayout(position, false);
                Intent intent = new Intent(actObj, ActMultipleChoiceAnsSheet.class);
                intent.putExtra("customeAnsObj", customeAnsList.get(position));
                actObj.startActivityForResult(intent, actObj.START_MULTIPLE_CHOICE_ACT_REQUEST);
            }
        });

        vHolder.bntDeleteFillIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                actObj.hideDeviceKeyboardAndClearFocus();
                setVisibilityOfCustomeAnsLayout(position, false);
                if (customeAnsList != null && customeAnsList.size() > 1) {
                    customeAnsList.remove(position);
                } else {
                    customeAnsList.get(position).setQueNo("");
                    customeAnsList.get(position).setCorrectAns("");
                }
                AssignCustomeQuizzAdapter.this.notifyDataSetChanged();
            }
        });

        vHolder.btnNofillInEdit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                actObj.hideDeviceKeyboardAndClearFocus();
                clickedPosition = position;
                setVisibilityOfCustomeAnsLayout(position, false);
                Intent intent = new Intent(actObj, ActMultipleChoiceAnsSheet.class);
                intent.putExtra("customeAnsObj", customeAnsList.get(position));
                actObj.startActivityForResult(intent, actObj.START_MULTIPLE_CHOICE_ACT_REQUEST);
            }
        });


        vHolder.btnAddNonFillInLine.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                actObj.hideDeviceKeyboardAndClearFocus();
                setVisibilityOfCustomeAnsLayout(position, false);
                CustomeAns prevRowAns = customeAnsList.get(position);
                CustomeAns customeObj = actObj.getCustomeAnsObj("", 1, 1);
                customeObj.setFillInType(prevRowAns.getFillInType());
                customeObj.setAnswerType(prevRowAns.getAnswerType());
                if (prevRowAns.getAnswerType() == MULTIPLE_CHOICE_ANS_TYPE) {
                    customeObj.setMultipleChoiceOptionList
                            (actObj.getDefaultMultipleChoiceOptionList());
                }
                customeAnsList.add(position + 1, customeObj);
                AssignCustomeQuizzAdapter.this.notifyDataSetChanged();
            }
        });

        vHolder.btnSingleAnswer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                actObj.hideDeviceKeyboardAndClearFocus();
                setVisibilityOfCustomeAnsLayout(position, false);
                customeAnsList.get(position).setFillInType(FILL_IN_TYPE);
                AssignCustomeQuizzAdapter.this.notifyDataSetChanged();
            }
        });

        vHolder.bntDeleteNonFillIn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                actObj.hideDeviceKeyboardAndClearFocus();
                setVisibilityOfCustomeAnsLayout(position, false);
                if (customeAnsList != null && customeAnsList.size() > 1) {
                    customeAnsList.remove(position);
                } else {
                    customeAnsList.get(position).setQueNo("");
                    customeAnsList.get(position).setCorrectAns("");
                }
                AssignCustomeQuizzAdapter.this.notifyDataSetChanged();
            }
        });

        vHolder.txtNumber.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String questionNumber = ((EditText) v).getText().toString();
                    customeAnsList.get(position).setQueNo(questionNumber);
                    if (!MathFriendzyHelper.isEmpty(questionNumber)) {
                        if (isQuestionNumberAlreadyExist(questionNumber, position)) {
                            ((EditText) v).setText("");
                            customeAnsList.get(position).setQueNo("");
                        }
                    }
                }
            }
        });


        //for the new changes for adding on work area button in sliding bar
        vHolder.btnOnworkAreaFillIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                clickOnWorkArea(position, customeAnsList.get(position), vHolder);
            }
        });

        //not work right now
        vHolder.btnOnworkAreaNonFillIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                clickOnWorkArea(position, customeAnsList.get(position), vHolder);
            }
        });
        return convertView;
    }

    private class ViewHolder {
        private EditText txtNumber;
        private RelativeLayout fillInLayout;
        private RelativeLayout multipleChoiceLayout;
        private RelativeLayout trueFalseLayout;//for true false or yes no
        private RelativeLayout editLayout;
        private RelativeLayout fillEditDialog;
        private RelativeLayout nonfillEditDialog;
        private Button btnEdit;

        //for fillInLayout
        private ImageView imgCheckLeftUnit;
        private ImageView imgCheckRightUnit;
        private TextView edtFillInStudentAnswer;
        private EditText edtUnitOfMeasurement;

        //for fill dialog layout
        private Button btnFillInAddLine;
        private Button btnMultipleChoice;
        private Button bntDeleteFillIn;

        //for non fillIn
        private Button btnNofillInEdit;
        private Button btnAddNonFillInLine;
        private Button btnSingleAnswer;
        private Button bntDeleteNonFillIn;

        //for multiple choice layout
        private LinearLayout optionLayout;

        //for true false or yes no layout
        private TextView ansTrue;
        private TextView ansFalse;


        //for new change regarding the on work area button
        private Button btnOnworkAreaFillIn;
        private Button btnOnworkAreaNonFillIn;
    }

    /**
     * Set the visibility of layout
     *
     * @param custom
     * @param fillIntypeLayout
     * @param multipleChoiceLayout
     * @param trueFalseLayout
     */
    private void setVisibilityOfLayout(CustomeAns custom, RelativeLayout fillIntypeLayout
            , RelativeLayout multipleChoiceLayout, RelativeLayout trueFalseLayout) {
        if (custom.getFillInType() == FILL_IN_TYPE) {
            fillIntypeLayout.setVisibility(RelativeLayout.VISIBLE);
            multipleChoiceLayout.setVisibility(RelativeLayout.GONE);
            trueFalseLayout.setVisibility(RelativeLayout.GONE);
        } else {
            if (custom.getAnswerType() == MULTIPLE_CHOICE_ANS_TYPE) {
                fillIntypeLayout.setVisibility(RelativeLayout.GONE);
                multipleChoiceLayout.setVisibility(RelativeLayout.VISIBLE);
                trueFalseLayout.setVisibility(RelativeLayout.GONE);
            } else if (custom.getAnswerType() == TRUE_FALSE_ANS_TYPE
                    || custom.getAnswerType() == YES_NO_ANS_TYPE) {
                fillIntypeLayout.setVisibility(RelativeLayout.GONE);
                multipleChoiceLayout.setVisibility(RelativeLayout.GONE);
                trueFalseLayout.setVisibility(RelativeLayout.VISIBLE);
            }
        }
    }

    /**
     * Set Visibility of Edit layout
     *
     * @param position
     */
    private void setVisibilityOfEditLayout(int position) {
        for (int i = 0; i < customeAnsList.size(); i++) {
            if (i == position) {
                customeAnsList.get(i).setVisibleLayout
                        (!customeAnsList.get(i).isVisibleLayout());
            } else {
                customeAnsList.get(i).setVisibleLayout(false);
            }
        }
        AssignCustomeQuizzAdapter.this.notifyDataSetChanged();
    }

    /**
     * Set visibility of layout
     *
     * @param position
     * @param isVisible
     */
    private void setVisibilityOfCustomeAnsLayout(int position, boolean isVisible) {
        try {
            customeAnsList.get(position).setVisibleLayout(isVisible);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set the visibility of layout
     *
     * @param layout
     * @param bValue
     * @param isMainLayout
     */
    private void setVisibilityOfLayout(RelativeLayout layout, boolean bValue
            , boolean isMainLayout) {
        if (bValue) {
            layout.setVisibility(RelativeLayout.VISIBLE);
            if (!isMainLayout) {
                if (isNeedAnimation) {
                    MathFriendzyHelper.showViewFromRightToLeftAnimation(context, layout);
                } else {
                    isNeedAnimation = true;
                }
            }
        } else {
            layout.setVisibility(RelativeLayout.GONE);
        }
    }

    /**
     * Set the multiple choice layout
     *
     * @param customeAns
     */
    @SuppressLint("InflateParams")
    private void setMultiplechoiceLayout(final CustomeAns customeAns, ViewHolder vHolder) {
        if (customeAns.getFillInType() != FILL_IN_TYPE &&
                customeAns.getAnswerType() == MULTIPLE_CHOICE_ANS_TYPE) {
            vHolder.optionLayout.removeAllViews();
            for (int i = 0; i < customeAns.getMultipleChoiceOptionList().size(); i++) {
                String optionText = customeAns.getMultipleChoiceOptionList().get(i);
                View view = mInfltor.inflate(R.layout.check_home_work_option_layout, null);
                final TextView option = (TextView) view.findViewById(R.id.option);
                option.setText(optionText);

                if (customeAns.getCorrectAns().contains(optionText)) {
                    option.setBackgroundResource(R.drawable.checked_small_box_home_work);
                } else {
                    option.setBackgroundResource(R.drawable.unchecked_box_home_work);
                }

                option.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //setInvisibilityOfAllDialogLayout();
                        actObj.hideDeviceKeyboardAndClearFocus();
                        setIsNeedAnimationVariable();
                        String optionText = ((TextView) v).getText().toString();
                        String userCorrectAns = customeAns.getCorrectAns();
                        if (userCorrectAns.contains(optionText)) {
                            userCorrectAns = userCorrectAns.replace(optionText, "");
                        } else {
                            userCorrectAns = userCorrectAns + optionText;
                        }
                        customeAns.setCorrectAns(userCorrectAns);
                        AssignCustomeQuizzAdapter.this.notifyDataSetChanged();
                    }
                });
                vHolder.optionLayout.addView(view);
            }
        }
    }

    /**
     * Set the true false and yes no layout
     *
     * @param customeAns
     */
    private void setTrueFalseOrYesNoLayout(final CustomeAns customeAns, ViewHolder vHolder) {

        if (customeAns.getFillInType() != FILL_IN_TYPE) {
            if (customeAns.getAnswerType() == YES_NO_ANS_TYPE) {
                vHolder.ansTrue.setText(ActMultipleChoiceAnsSheet.YES);
                vHolder.ansFalse.setText(ActMultipleChoiceAnsSheet.NO);

                if (customeAns.getCorrectAns().equalsIgnoreCase(ActMultipleChoiceAnsSheet.YES)) {
                    vHolder.ansTrue.setBackgroundResource(R.drawable.checked_box_home_work);
                    vHolder.ansFalse.setBackgroundResource(R.drawable.unchecked_box_home_work);
                } else if (customeAns.getCorrectAns().equalsIgnoreCase(ActMultipleChoiceAnsSheet.NO)) {
                    vHolder.ansTrue.setBackgroundResource(R.drawable.unchecked_box_home_work);
                    vHolder.ansFalse.setBackgroundResource(R.drawable.checked_box_home_work);
                }
            } else if (customeAns.getAnswerType() == TRUE_FALSE_ANS_TYPE) {
                vHolder.ansTrue.setText(ActMultipleChoiceAnsSheet.TRUE);
                vHolder.ansFalse.setText(ActMultipleChoiceAnsSheet.FALSE);

                if (customeAns.getCorrectAns().equalsIgnoreCase(ActMultipleChoiceAnsSheet.TRUE)) {
                    vHolder.ansTrue.setBackgroundResource(R.drawable.checked_box_home_work);
                    vHolder.ansFalse.setBackgroundResource(R.drawable.unchecked_box_home_work);
                } else if (customeAns.getCorrectAns().equalsIgnoreCase
                        (ActMultipleChoiceAnsSheet.FALSE)) {
                    vHolder.ansTrue.setBackgroundResource(R.drawable.unchecked_box_home_work);
                    vHolder.ansFalse.setBackgroundResource(R.drawable.checked_box_home_work);
                }
            }

            vHolder.ansTrue.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    //setInvisibilityOfAllDialogLayout();
                    actObj.hideDeviceKeyboardAndClearFocus();
                    setIsNeedAnimationVariable();
                    if (customeAns.getAnswerType() == YES_NO_ANS_TYPE) {
                        customeAns.setCorrectAns(ActMultipleChoiceAnsSheet.YES);
                    } else if (customeAns.getAnswerType() == TRUE_FALSE_ANS_TYPE) {
                        customeAns.setCorrectAns(ActMultipleChoiceAnsSheet.TRUE);
                    }
                    AssignCustomeQuizzAdapter.this.notifyDataSetChanged();
                }
            });

            vHolder.ansFalse.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    actObj.hideDeviceKeyboardAndClearFocus();
                    //setInvisibilityOfAllDialogLayout();
                    setIsNeedAnimationVariable();
                    if (customeAns.getAnswerType() == YES_NO_ANS_TYPE) {
                        customeAns.setCorrectAns(ActMultipleChoiceAnsSheet.NO);
                    } else if (customeAns.getAnswerType() == TRUE_FALSE_ANS_TYPE) {
                        customeAns.setCorrectAns(ActMultipleChoiceAnsSheet.FALSE);
                    }
                    AssignCustomeQuizzAdapter.this.notifyDataSetChanged();
                }
            });
        }
    }

    /**
     * Set fillIn layout
     *
     * @param customeAns
     * @param vHolder
     */
    private void setFillInLayout(final CustomeAns customeAns, ViewHolder vHolder
            , final int position) {

        if (customeAns.getFillInType() == FILL_IN_TYPE) {
            //vHolder.edtFillInStudentAnswer.setMovementMethod(new ScrollingMovementMethod());
            if (customeAns.getIsLeftUnit() == 0) {
                vHolder.imgCheckRightUnit.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
                vHolder.imgCheckLeftUnit.setBackgroundResource(R.drawable.mf_check_box_ipad);
            } else {
                vHolder.imgCheckLeftUnit.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
                vHolder.imgCheckRightUnit.setBackgroundResource(R.drawable.mf_check_box_ipad);
            }

            vHolder.edtUnitOfMeasurement.setText(customeAns.getAnsSuffix());

            if (selectedTextViewposition == position) {
                vHolder.edtFillInStudentAnswer.setText(customeAns.getCorrectAns() + "|");
            } else {
                vHolder.edtFillInStudentAnswer.setText(customeAns.getCorrectAns());
            }

            vHolder.imgCheckLeftUnit.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    actObj.hideDeviceKeyboardAndClearFocus();
                    setIsNeedAnimationVariable();
                    //setInvisibilityOfAllDialogLayout();
                    customeAns.setIsLeftUnit(1);
                    AssignCustomeQuizzAdapter.this.notifyDataSetChanged();
                }
            });

            vHolder.imgCheckRightUnit.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    actObj.hideDeviceKeyboardAndClearFocus();
                    setIsNeedAnimationVariable();
                    //setInvisibilityOfAllDialogLayout();
                    customeAns.setIsLeftUnit(0);
                    AssignCustomeQuizzAdapter.this.notifyDataSetChanged();
                }
            });

            vHolder.edtUnitOfMeasurement.setOnFocusChangeListener(new OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        customeAns.setAnsSuffix(((EditText) v).getText().toString());
                    }
                }
            });

            vHolder.edtFillInStudentAnswer.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //isNeedAnimation = false;
                    if (customeAnsList.get(position).getIsAnswerAvailable() == MathFriendzyHelper.NO) {
                        MathFriendzyHelper.showWarningDialog(context, lblAnswerOnWorkAreaCreditGiven);
                        return;
                    }
                    selectedTextViewposition = position;
                    selectedTextView = ((EditText) v);
                    setTextToSelectedTextView(selectedTextView, "");
                    //actObj.hideDeviceKeyboardAndClearFocus();
                    actObj.cleasFocusFromEditText();
                    actObj.hideDeviceKeyboard();
                    actObj.showKeyboard();
                }
            });
        }
    }

    /**
     * Invisible all layout
     */
	/*private void setInvisibilityOfAllDialogLayout(){
		for(int i = 0 ; i < customeAnsList.size() ; i ++ ){
			setVisibilityOfCustomeAnsLayout(i , false);
		}
	}*/

    /**
     * Set Is Animation variable
     */
    private void setIsNeedAnimationVariable() {
        for (int i = 0; i < customeAnsList.size(); i++) {
            if (customeAnsList.get(i).isVisibleLayout()) {
                isNeedAnimation = false;
                break;
            }
        }
    }

    /**
     * Set the text to the selected text view
     *
     * @param txtView
     * @param text
     */
    public void setTextToSelectedTextView(TextView txtView, String text) {
        try {
            if (selectedTextView != null) {
                selectedTextView.setText
                        (selectedTextView.getText().toString()
                                .replace("|", "") + text + "|");
            }
            this.setIsNeedAnimationVariable();
            customeAnsList.get(selectedTextViewposition)
                    .setCorrectAns(selectedTextView.getText().toString()
                            .replace("|", ""));
            this.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Clear text
     *
     * @param txtView
     */
    public void cleartextFromSelectedTextView(TextView txtView) {
        try {
            StringBuilder text = new StringBuilder(selectedTextView
                    .getText().toString().replace("|", ""));
            if (text.length() > 0) {
                text.deleteCharAt(text.length() - 1);
                selectedTextView.setText(text + "|");
            }
            this.setIsNeedAnimationVariable();
            customeAnsList.get(selectedTextViewposition)
                    .setCorrectAns(selectedTextView.getText().toString()
                            .replace("|", ""));
            this.notifyDataSetChanged();
        } catch (Exception e) {

        }
    }

    /**
     * Call when the Math kayboard is closed
     */
    public void closeMathKeyboard() {
		/*selectedTextViewposition = -1;
		this.notifyDataSetChanged();*/
        try {
            if (selectedTextView != null) {
                selectedTextView.setText(selectedTextView.getText().toString().replace("|", ""));
                selectedTextViewposition = -1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Check for question already exist
     *
     * @param questionNumber
     * @return
     */
    private boolean isQuestionNumberAlreadyExist(String questionNumber, int position) {
        try {
            for (int i = 0; i < customeAnsList.size(); i++) {
                if (i != position && customeAnsList.get(i).getQueNo()
                        .equals(questionNumber))
                    return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    private void clickOnWorkArea(int position, CustomeAns customeAns, ViewHolder vHolder) {
        setVisibilityOfCustomeAnsLayout(position, false);
        customeAnsList.get(position).setCorrectAns(lblAnswerOnWorkAreaCreditGiven);
        customeAnsList.get(position).setIsAnswerAvailable(0);
        this.notifyDataSetChanged();
    }
}
