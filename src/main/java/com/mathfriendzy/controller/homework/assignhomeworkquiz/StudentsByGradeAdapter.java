package com.mathfriendzy.controller.homework.assignhomeworkquiz;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.model.homework.assignhomework.GetStudentByGradeResponse;

import java.util.ArrayList;

public class StudentsByGradeAdapter extends BaseAdapter{

	public ArrayList<GetStudentByGradeResponse> arrayList = null;
	private LayoutInflater mInflator = null;
	private ViewHolder vHolder = null;
    private ArrayList<GetStudentByGradeResponse> filterList;
	//public boolean selectionArray[] = null;

	public StudentsByGradeAdapter(Context context , 
			ArrayList<GetStudentByGradeResponse> arrayList,
            ArrayList<GetStudentByGradeResponse> filterList){
		this.arrayList = arrayList;

        //If filter list is null or not have any element or when user see the student list when no class selected
        if(filterList != null && filterList.size() > 0) {
            this.filterList = filterList;
        }else{
            this.filterList = arrayList;
        }

		mInflator = LayoutInflater.from(context);
		/*selectionArray = new boolean[arrayList.size()];

		for(int i = 0 ; i < selectionArray.length ; i ++ ){
			selectionArray[i] = true;
		}*/
	}

	@Override
	public int getCount() {
		return filterList.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		if(view == null){
			vHolder = new ViewHolder();
			view = mInflator.inflate(R.layout.homework_students_by_grade_list, null);
			vHolder.imgCheck = (ImageView) view.findViewById(R.id.imgCheck);
			vHolder.txtStudentName = (TextView) view.findViewById(R.id.txtStudentName);
			view.setTag(vHolder);
		}else{
			vHolder = (ViewHolder) view.getTag();
		}

		vHolder.txtStudentName.setText(filterList.get(position).getfName()
				+ " " + filterList.get(position).getlName());

		/*if(selectionArray[position]){
			vHolder.imgCheck.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
		}else{
			vHolder.imgCheck.setBackgroundResource(R.drawable.mf_check_box_ipad);
		}*/

		if(filterList.get(position).isSelected()){
			vHolder.imgCheck.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
		}else{
			vHolder.imgCheck.setBackgroundResource(R.drawable.mf_check_box_ipad);
		}
		
		vHolder.imgCheck.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(position == 0){
					if(filterList.get(position).isSelected()){
						for(int i = 0 ; i < filterList.size() ; i ++ ){
							setIndexValue(i , false);
						}
					}else{
						for(int i = 0 ; i < filterList.size() ; i ++ ){
							setIndexValue(i , true);
						}
					}
				}else{
					if(filterList.get(position).isSelected()){
						setIndexValue(position , false);
						setIndexValue(0 , false);
					}else{
						setIndexValue(position , true);
						if(isAllChecked()){
							setIndexValue(0 , true);
						}
					}
				}
				StudentsByGradeAdapter.this.notifyDataSetChanged();
			}
		});
		return view;
	}

	/**
	 * Check for all student selected or not
	 * @return
	 */
	private boolean isAllChecked(){
		for(int i = 1 ; i < filterList.size() ; i ++ ){
			if(!filterList.get(i).isSelected())
				return false;
		}
		return true;
	}
	
	/**
	 * Set the bValues on the selected index
	 * @param index
	 * @param bValue
	 */
	private void setIndexValue(int index , boolean bValue){
		//selectionArray[index] = bValue;
        filterList.get(index).setSelected(bValue);
	}

	private class ViewHolder{
		private ImageView imgCheck;
		private TextView txtStudentName;
	}
}
