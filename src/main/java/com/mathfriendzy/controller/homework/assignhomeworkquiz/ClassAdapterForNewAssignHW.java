package com.mathfriendzy.controller.homework.assignhomeworkquiz;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.homework.SelectAssignHomeworkClassParam;
import com.mathfriendzy.model.registration.classes.ClassWithName;

import java.util.ArrayList;

/**
 * Created by root on 31/5/16.
 */
public class ClassAdapterForNewAssignHW extends BaseAdapter {

    private ArrayList<ClassWithName> classes = null;
    private LayoutInflater mInflater = null;
    private ViewHolder vHolder = null;
    private Context context = null;
    private SelectAssignHomeworkClassParam param = null;
    private AdapterListenerForNewAssignHW listenerForNewAssignHW = null;

    private final String CURRENT_DATE_FORMAT = "yyyy-MM-dd";
    private final String REQUIRED_DATE_FORMAT = "MMM yyyy";
    public ClassAdapterForNewAssignHW(Context context , ArrayList<ClassWithName> classes ,
                          SelectAssignHomeworkClassParam param ,
                          AdapterListenerForNewAssignHW listenerForNewAssignHW){
        this.context = context;
        this.classes = classes;
        mInflater = LayoutInflater.from(context);
        this.param = param;
        this.listenerForNewAssignHW = listenerForNewAssignHW;
    }


    @Override
    public int getCount() {
        return this.classes.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        if(view == null){
            vHolder = new ViewHolder();
            view = mInflater.inflate(R.layout.class_title_assign_homework_item , null);
            vHolder.txtStartDate = (TextView) view.findViewById(R.id.txtStartDate);
            vHolder.txtSubject = (TextView) view.findViewById(R.id.txtSubject);
            vHolder.txtClassName = (TextView) view.findViewById(R.id.txtClassName);
            vHolder.rlClassTitleItemLayout = (LinearLayout) view.findViewById(R.id.rlClassTitleItemLayout);
            vHolder.imgCheck = (ImageView) view.findViewById(R.id.imgCheck);
            vHolder.rlStartDate = (RelativeLayout) view.findViewById(R.id.rlStartDate);
            view.setTag(vHolder);
        }else {
            vHolder = (ViewHolder) view.getTag();
        }
        vHolder.txtStartDate.setText(MathFriendzyHelper.
                formatDataInGivenFormat(classes.get(position).getStartDate() ,
                        CURRENT_DATE_FORMAT , REQUIRED_DATE_FORMAT));
        vHolder.txtSubject.setText(classes.get(position).getSubject());
        vHolder.txtClassName.setText(classes.get(position).getClassName());
        this.setSelectedItemBackGround(vHolder , classes.get(position));
        this.setVisibilityOfStartLayout(vHolder);
        vHolder.rlClassTitleItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(param.isShowForPrevSaveHomework()){
                    //classes.get(position).setSelected(true);
                    listenerForNewAssignHW.onSingleSelectionDone(classes.get(position));
                }else {
                    if(classes.get(position).isSelected()){
                        classes.get(position).setSelected(!classes.get(position).isSelected());
                        ClassAdapterForNewAssignHW.this.notifyDataSetChanged();
                        return ;
                    }

                    if(isClassSelectWithDifferent(classes.get(position))){
                        MathFriendzyHelper.showWarningDialog(context ,
                                MathFriendzyHelper.getTreanslationTextById(context , "lblClassWithDiffSub"));
                    }else {
                        classes.get(position).setSelected(!classes.get(position).isSelected());
                        ClassAdapterForNewAssignHW.this.notifyDataSetChanged();
                    }
                }
            }
        });
        return view;
    }

    /**
     * Check for the selected class which is same as previous class selected
     * That means user can select the multiple class with the same subject
     * @param classWithName
     * @return
     */
    private boolean isClassSelectWithDifferent(ClassWithName classWithName){
        try{
            for(int i = 0 ; i < classes.size() ; i ++ ){
                if(classes.get(i).isSelected()
                        && classes.get(i).getSubjectId()
                        != classWithName.getSubjectId()){
                      return true;
                }
            }
            return false;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
    ///
    private void setSelectedItemBackGround(ViewHolder viewHolder , ClassWithName classWithName){
        if(classWithName.isSelected()){
            vHolder.rlClassTitleItemLayout.setBackgroundColor(context.getResources().getColor(R.color.GREEN));
            //vHolder.imgCheck.setVisibility(ImageView.VISIBLE);
        }else{
            vHolder.rlClassTitleItemLayout.setBackgroundColor(context.getResources().getColor(R.color.WHITE));
            //vHolder.imgCheck.setVisibility(ImageView.GONE);
        }
    }

    private class ViewHolder{
        private TextView txtStartDate;
        private TextView txtSubject;
        private TextView txtClassName;
        private LinearLayout rlClassTitleItemLayout;
        private ImageView imgCheck;
        private RelativeLayout rlStartDate;
    }

    public ArrayList<ClassWithName> getSelectedList(){
        ArrayList<ClassWithName> selectedClassList = new ArrayList<ClassWithName>();
        for(int i = 0 ; i < this.classes.size() ; i ++ ){
            if(this.classes.get(i).isSelected()){
                selectedClassList.add(this.classes.get(i));
            }
        }
        return selectedClassList;
    }

    /**
     * To show the popup again and again then show the selected class again
     * @return
     */
    public ArrayList<ClassWithName> getUpdateClassesList(){
        return this.classes;
    }

    private void setVisibilityOfStartLayout(ViewHolder vHolder){
        try {
            if(this.param.isShowForPrevSaveHomework()){
                vHolder.rlStartDate.setVisibility(RelativeLayout.GONE);
            }else{
                vHolder.rlStartDate.setVisibility(RelativeLayout.VISIBLE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
