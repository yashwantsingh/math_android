package com.mathfriendzy.controller.homework.assignhomeworkquiz;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.mathfriendzy.R;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.homework.CustomeResult;

public class CutomeHomeworkTitleAdapter extends BaseAdapter{

	public ArrayList<CustomeResult> customeQuizList = null;
	public int clickedPosition = 0;
	private LayoutInflater mInflator = null;
	private ViewHolder vHolder = null;
	private Context context = null;
	public ArrayList<RelativeLayout> layoutList = null;
	private ActAssignCustomeHomework actObj = null;
	
	public CutomeHomeworkTitleAdapter(Context context , ArrayList<CustomeResult> customeQuizList){
		this.customeQuizList = customeQuizList;
		mInflator = LayoutInflater.from(context);
		this.context = context;
		actObj = ((ActAssignCustomeHomework)context);
		layoutList = new ArrayList<RelativeLayout>();
		for(int i = 0 ; i < customeQuizList.size() ; i ++){
			layoutList.add(null);
		}
	}

	@Override
	public int getCount() {
		return customeQuizList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if(convertView == null){
			vHolder = new ViewHolder();
			convertView = mInflator.inflate(R.layout.assign_cutome_home_work_title_layout, null);
			vHolder.edtTitle = (EditText) convertView.findViewById(R.id.edtTitle);
			vHolder.btnAnswer = (Button) convertView.findViewById(R.id.btnAnswer);
			vHolder.btnEdit = (Button) convertView.findViewById(R.id.btnEdit);
			vHolder.editLayout = (RelativeLayout) convertView.findViewById(R.id.editLayout);
			vHolder.bntAddAssignment = (Button) convertView.findViewById(R.id.bntAddAssignment);
			vHolder.bntDeleteAssignment = (Button) convertView.findViewById(R.id.bntDeleteAssignment);
			convertView.setTag(vHolder);
		}else{
			vHolder	= (ViewHolder) convertView.getTag();
		}

		layoutList.set(position, vHolder.editLayout);

		try{
			if(customeQuizList.get(position).getTitle() != null 
					&& customeQuizList.get(position).getTitle().length() > 0){
				vHolder.edtTitle.setText(customeQuizList.get(position).getTitle());
			}else{
				vHolder.edtTitle.setText("");
			}
		}catch(Exception e){
			e.printStackTrace();
			vHolder.edtTitle.setText("");
		}

		if(customeQuizList.get(position).isLayoutVisible()){
			this.setVisibilityOfEditLayout(vHolder.editLayout, true);
		}else{
			this.setVisibilityOfEditLayout(vHolder.editLayout, false);
		}
		
		vHolder.btnEdit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				actObj.cleasFocusFromEditText();

				for(int i = 0 ; i < layoutList.size() ; i ++ ){
					if(i == position){
						customeQuizList.get(i).setLayoutVisible
						(!customeQuizList.get(i).isLayoutVisible());
					}else{
						customeQuizList.get(i).setLayoutVisible(false);
					}
				}
				CutomeHomeworkTitleAdapter.this.notifyDataSetChanged();
			}
		});

		vHolder.bntAddAssignment.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(layoutList.get(position) != null){
					customeQuizList.get(position).setLayoutVisible(false);
					setVisibilityOfEditLayout(layoutList.get(position) , false);
				}
				customeQuizList.add(position + 1 , actObj.getCustomeResultWithTitle(""));
				layoutList.add(position + 1, null);
				CutomeHomeworkTitleAdapter.this.notifyDataSetChanged();
			}
		});

		vHolder.bntDeleteAssignment.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(layoutList.get(position) != null){
					customeQuizList.get(position).setLayoutVisible(false);
					setVisibilityOfEditLayout(layoutList.get(position) , false);
				}

				if(customeQuizList.size() > 1){
					customeQuizList.remove(position);
					layoutList.remove(position);
				}else{
					customeQuizList.get(position).setTitle("");
					customeQuizList.get(position).setCustomeAnsList(null);
				}
				CutomeHomeworkTitleAdapter.this.notifyDataSetChanged();
			}
		});

		vHolder.edtTitle.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(!hasFocus){
					customeQuizList.get(position).setTitle(((EditText)v).getText().toString());
				}
			}
		});

		vHolder.btnAnswer.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				actObj.cleasFocusFromEditText();
				clickedPosition = position;
				customeQuizList.get(position).setClickedPosition(position);
				Intent intent = new Intent(actObj , ActAssignCustomAns.class);
				intent.putExtra("customeResult", customeQuizList.get(position));
				actObj.startActivityForResult(intent, actObj.START_ACT_ASSIGN_CUSTOME_ANS);
			}
		});

		return convertView;
	}

	/**
	 * Set visibility of EditLayout
	 * @param layout
	 */
	public void setVisibilityOfEditLayout(RelativeLayout layout , boolean bValue){
		if(bValue){
			layout.setVisibility(RelativeLayout.VISIBLE);
			MathFriendzyHelper.showViewFromRightToLeftAnimation(context, layout);
		}else{
			layout.setVisibility(RelativeLayout.GONE);
		}
	}

	private class ViewHolder{
		private EditText edtTitle;
		private Button btnAnswer;
		private Button btnEdit;
		private RelativeLayout editLayout;
		private Button bntAddAssignment;
		private Button bntDeleteAssignment;
	}

	/*private void clearFocus(){
		((Activity)context).getCurrentFocus().clearFocus();
	}*/
}
