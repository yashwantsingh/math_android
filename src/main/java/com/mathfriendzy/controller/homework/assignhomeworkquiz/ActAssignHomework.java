package com.mathfriendzy.controller.homework.assignhomeworkquiz;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.helper.QuizzSelectionInterface;
import com.mathfriendzy.helper.YesNoListenerInterface;
import com.mathfriendzy.listener.ClassSelectedListener;
import com.mathfriendzy.model.assessmenttest.CatagoriesDto;
import com.mathfriendzy.model.homework.CustomeAns;
import com.mathfriendzy.model.homework.CustomeResult;
import com.mathfriendzy.model.homework.HomeworkData;
import com.mathfriendzy.model.homework.PracticeResult;
import com.mathfriendzy.model.homework.PracticeResultSubCat;
import com.mathfriendzy.model.homework.SelectAssignHomeworkClassParam;
import com.mathfriendzy.model.homework.WordResult;
import com.mathfriendzy.model.homework.WordSubCatResult;
import com.mathfriendzy.model.homework.assignhomework.AssignHomeworkParam;
import com.mathfriendzy.model.homework.assignhomework.AssignHomeworkResponse;
import com.mathfriendzy.model.homework.assignhomework.GetStudentByGradeResponse;
import com.mathfriendzy.model.homework.assignhomework.GetStudentForSchoolClassesParam;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.classes.ClassWithName;
import com.mathfriendzy.model.registration.classes.UserClasses;
import com.mathfriendzy.model.resource.ResourceResponse;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.CategoryListTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SubCatergoryTransferObj;
import com.mathfriendzy.pdfoperation.PDFOperation;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.uploadimages.UploadPdf;
import com.mathfriendzy.utils.CommonUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;

public class ActAssignHomework extends ActBase {

    private final String TAG = this.getClass().getSimpleName();

    private TextView txtTheFastestWay = null;
    private TextView txtGrade = null;
    private TextView txtDueDate = null;
    //private Spinner spinnerGrade = null;
    private RelativeLayout datePickerLayout = null;
    private TextView edtDatePicker = null;
    private Button btnSelectStudent = null;
    private EditText txtTeacherMessage = null;
    private Button btnAddHomeWorkQuizzes = null;
    private Button btnAssignHomework = null;
    private LinearLayout homeWorkQuizzLayout = null;

    private String teacherId = "";

    private final int SELECT_STUDENT_REQUEST = 1001;
    private final int SELECT_PRACTICE_SKILL_REQUEST = 1002;
    private final int SELECT_WORD_PROBLEM_REQUEST = 1003;
    private final int SELECT_CUSTOME_HOMEWORK_REQUEST = 1004;

    //for selected student
    private ArrayList<GetStudentByGradeResponse> selectedStudentList = null;

    //for add home work quizz
    private boolean isSeletionMakeSureDialogShow = false;
    private String lblMakeSureToSelectGrade = null;

    //for quiz type selection dialog
    private String practiceSkillTxt = null;
    private String wordProblemTxt   = null;
    private String customeHomeworkTxt = null;
    private String classWorkQuizz = null;
    private String lblNum = null;
    private boolean islblNumDisplayed = false;

    //for practice skill selection
    private LinkedHashMap<String , ArrayList<LearningCenterTransferObj>> mapChildList = null;
    private ArrayList<Boolean> openSelectedCatList = null;

    //for word problem
    private LinkedHashMap<String, ArrayList<SubCatergoryTransferObj>> mSubCatList = null;
    private ArrayList<Boolean> openSelectedCatListForWordProblem = null;
    //for show by CCSS
    private ArrayList<CatagoriesDto> categoryInfoList = null;

    //for custom home work
    private ArrayList<CustomeResult> customeQuizList = null;

    //for showing assign homework
    private LinkedHashMap<Integer, ArrayList<LearningCenterTransferObj>> practiceList = null;
    private LinkedHashMap<String, ArrayList<SubCatergoryTransferObj>> workProblemList = null;

    private String warningNoStudentMsg = null;

    private final int FILL_IN_TYPE = 1;
    private final int MULTIPLE_CHOICE_ANS_TYPE = 1;
    private final int TRUE_FALSE_ANS_TYPE = 2;
    private final int YES_NO_ANS_TYPE = 3;

    private String lblTheHomeworkAssignmentYouEntered = null;
    private String btnTitleYes = null;
    private String lblNo = null;

    private String mfPleaseSelectOneOrMoreEquationType = "Please select one or more type of homework.";

    //class title changes
    private RegistereUserDto registerUserFromPreff = null;
    private RelativeLayout selectClassLayout = null;
    private TextView txtSelectClass = null;
    private TextView txtSelectClassValue = null;
    private RelativeLayout titleListLayout = null;
    private Button btnClassListDone = null;
    private ListView lstClassesList = null;
    private ClassesAdapter adapter = null;


    //New View added for new homework assignment
    private TextView txtCreatesAssignment = null;
    private TextView txtTypeValues = null;
    private TextView txtStudent = null;
    private TextView txtStudentPicker = null;
    private TextView txtAssignHomework = null;
    private TextView txtOnceAssign = null;
    private TextView btnSave = null;
    private RelativeLayout gradeLayout = null;//work as type layout
    private RelativeLayout rlStudentLayout = null;

    private ArrayList<ClassWithName> classWithNames = null;
    private ArrayList<ClassWithName> selectedClassList = null;

    //saved homework changes
    public final int SELECT_SAVED_HOMEWORK_REQUEST = 1005;
    public final int SELECT_PREVIOUS_HOMEWORK_REQUEST = 1006;
    private HomeworkData selectedHWData = null;

    private final int INITIAL_GRADE = 1;
    private int selectedGrade = 1;

    //for teacher create homework from checkhomework
    private boolean isTeacherCreateAssignmentFromCheckHW = false;
    private int selectedSubjectId = MathFriendzyHelper.MATH_SUBJECT_ID;
    //end changes

    //edit homework changes
    private boolean isTeacherEditHomework = false;
    private HomeworkData editTeacherHWData = null;
    private LinearLayout pickerLayout = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_assign_homework);

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside onCreate()");

        this.init();
        this.getIntentValues();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.setInitialHomeworkData();
        this.setVisibilityOfSelectClassLayout();
        this.setDataAndVisibilityOfViewForEditHW();
        this.setVisibilityOfViewForTeacherCreateAssignmentFromCheckHW();

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside onCreate()");
    }

    private void getIntentValues() {
        isTeacherCreateAssignmentFromCheckHW = this.getIntent()
                .getBooleanExtra("isTeacherCreateAssignmentFromCheckHW", false);
        isTeacherEditHomework = this.getIntent().getBooleanExtra("isTeacherEditHomework" , false);
        if(isTeacherCreateAssignmentFromCheckHW){
            this.setDataForCreateHomeworkFromTeacherCheckHomework();
        }

        if(isTeacherEditHomework){
            this.setEditTeacherHomeWorkData();
        }
    }

    /**
     *Set the data from intent to create homework for the selected student from the
     * teacher check homework
     */
    private void setDataForCreateHomeworkFromTeacherCheckHomework(){
        try {
            selectedStudentList = (ArrayList<GetStudentByGradeResponse>)
                    this.getIntent().getSerializableExtra("selectedStudentList");
            selectedSubjectId = selectedStudentList.get(0).getSubjectId();
            this.addNewStudentForAllSelecteion(selectedStudentList);
            ClassWithName selectedClass = (ClassWithName) this.getIntent()
                    .getSerializableExtra("selectedClass");
            selectedClassList = new ArrayList<ClassWithName>();
            selectedClassList.add(selectedClass);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setEditTeacherHomeWorkData() {
        editTeacherHWData = (HomeworkData) this.getIntent().getSerializableExtra("TeacherEditHWData");
    }

    private void setVisibilityOfSelectClassLayout(){
        if(isTeacherCreateAssignmentFromCheckHW){
            selectClassLayout.setVisibility(View.INVISIBLE);
        }else{
            selectClassLayout.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Initialize the variables
     */
    private void init() {
        registerUserFromPreff = MathFriendzyHelper.getUserFromPreff(this);
        teacherId = CommonUtils.getUserId(this);
        //practiceList = new LinkedHashMap<Integer, ArrayList<LearningCenterTransferObj>>();
        this.initClasses();
    }

    /**
     * set the double digit format
     * @param number
     * @return
     */
    private String getDoubleDigitFormatNumber(int number){
        return new DecimalFormat("00").format(number);
    }

    /**
     * Set the initial value to the widgets
     */
    private void setInitialHomeworkData(){
            /*Calendar calender = MathFriendzyHelper.getCurrentCalender();
            this.setDatepickerValue(calender.get(Calendar.YEAR),
                    calender.get(Calendar.MONTH), calender.get(Calendar.DAY_OF_MONTH));*/
        //this.getGrade("1");
    }

    /**
     * @Descritpion getGradeData from database
     * @param
     * @param gradeValue
     */
        /*private void getGrade(String gradeValue){
            Grade gradeObj = new Grade();
            ArrayList<String> gradeList = gradeObj.getGradeList(this);
            this.setGradeAdapter(gradeValue , gradeList);
        }*/

    /**
     * @Description set Grade data to adapter
     * @param
     * @param
     * @param
     */
        /*private void setGradeAdapter(final String gradeValue, ArrayList<String> gradeList){
            ArrayAdapter<String> gradeAdapter = new ArrayAdapter<String>(this, R.layout.spinner_textview_layout,gradeList);
            gradeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerGrade.setAdapter(gradeAdapter);
            spinnerGrade.setSelection(gradeAdapter.getPosition(gradeValue));

            spinnerGrade.setOnItemSelectedListener(new OnItemSelectedListener(){
                @Override
                public void onItemSelected(AdapterView<?> parent, View view,
                                           int pos, long id) {

                    //Reinitialize variable when grade changes
                    mSubCatList = null;
                    openSelectedCatListForWordProblem = null;
                    workProblemList = null;

                    mapChildList =  null;
                    openSelectedCatList = null;
                    practiceList = null;

                    customeQuizList = null;
                    selectedStudentList = null;

                    setAssignData();

                    //class title changes
                    *//*try {
                        setClassTitle(MathFriendzyHelper.parseInt(spinnerGrade
                                .getSelectedItem().toString()));
                    }catch (Exception e){
                        e.printStackTrace();
                    }*//*
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {

                }
            });
        }*/

    @Override
    protected void setWidgetsReferences() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside setWidgetsReferences()");

        txtTopbar 		= (TextView) findViewById(R.id.txtTopbar);
        txtTheFastestWay = (TextView) findViewById(R.id.txtTheFastestWay);
        txtGrade = (TextView) findViewById(R.id.txtGrade);
        txtDueDate = (TextView) findViewById(R.id.txtDueDate);
        //spinnerGrade = (Spinner) findViewById(R.id.spinnerGrade);
        datePickerLayout = (RelativeLayout) findViewById(R.id.datePickerLayout);
        edtDatePicker = (TextView) findViewById(R.id.edtDatePicker);
        btnSelectStudent = (Button) findViewById(R.id.btnSelectStudent);
        btnAddHomeWorkQuizzes = (Button) findViewById(R.id.btnAddHomeWorkQuizzes);
        btnAssignHomework = (Button) findViewById(R.id.btnAssignHomework);
        homeWorkQuizzLayout = (LinearLayout) findViewById(R.id.homeWorkQuizzLayout);

        txtTeacherMessage = (EditText) findViewById(R.id.txtTeacherMessage);

        //class title changes
        selectClassLayout = (RelativeLayout) findViewById(R.id.selectClassLayout);
        txtSelectClass = (TextView) findViewById(R.id.txtSelectClass);
        txtSelectClassValue = (TextView) findViewById(R.id.txtSelectClassValue);
        titleListLayout = (RelativeLayout) findViewById(R.id.titleListLayout);
        btnClassListDone = (Button) findViewById(R.id.btnClassListDone);
        lstClassesList = (ListView) findViewById(R.id.lstClassesList);

        //new HW changes
        txtCreatesAssignment = (TextView) findViewById(R.id.txtCreatesAssignment);
        txtTypeValues = (TextView) findViewById(R.id.txtTypeValues);
        txtStudent = (TextView) findViewById(R.id.txtStudent);
        txtStudentPicker = (TextView) findViewById(R.id.txtStudentPicker);
        txtAssignHomework = (TextView) findViewById(R.id.txtAssignHomework);
        txtOnceAssign = (TextView) findViewById(R.id.txtOnceAssign);
        btnSave = (TextView) findViewById(R.id.btnSave);
        gradeLayout = (RelativeLayout) findViewById(R.id.gradeLayout);
        rlStudentLayout = (RelativeLayout) findViewById(R.id.rlStudentLayout);


        //edit homework changes
        pickerLayout = (LinearLayout) findViewById(R.id.pickerLayout);

        txtOnceAssign.setVisibility(View.GONE);
        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside setWidgetsReferences()");

    }

    @Override
    protected void setListenerOnWidgets() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside setListenerOnWidgets()");

        datePickerLayout.setOnClickListener(this);
        btnSelectStudent.setOnClickListener(this);
        btnAddHomeWorkQuizzes.setOnClickListener(this);
        btnAssignHomework.setOnClickListener(this);

        //class title changes
        selectClassLayout.setOnClickListener(this);
        btnClassListDone.setOnClickListener(this);

        //new HW changes
        gradeLayout.setOnClickListener(this);
        rlStudentLayout.setOnClickListener(this);
        btnSave.setOnClickListener(this);

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside setListenerOnWidgets()");

    }

    @Override
    protected void setTextFromTranslation() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside setTextFromTranslation()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblAssignHomework"));
        txtTheFastestWay.setText(transeletion.getTranselationTextByTextIdentifier("lblTheFastestWayToAssign"));
        txtGrade.setText(transeletion.getTranselationTextByTextIdentifier("lblType"));
        txtDueDate.setText(transeletion.getTranselationTextByTextIdentifier("lblDueDate"));
        btnSelectStudent.setText(transeletion.getTranselationTextByTextIdentifier("lblSelect")
                + " " + transeletion.getTranselationTextByTextIdentifier("mfBtnTitleStudents"));
        btnAddHomeWorkQuizzes.setText(transeletion.getTranselationTextByTextIdentifier("lblAddSection"));
        btnAssignHomework.setText(transeletion.getTranselationTextByTextIdentifier("lblAssign"));
        lblMakeSureToSelectGrade = transeletion.getTranselationTextByTextIdentifier("lblMakeSureToSelectGrade");

        practiceSkillTxt = transeletion.getTranselationTextByTextIdentifier("lblSolveEquations");
        wordProblemTxt = transeletion.getTranselationTextByTextIdentifier("lblWordProblem");
        customeHomeworkTxt = transeletion.getTranselationTextByTextIdentifier("lblCustomHomework");

        classWorkQuizz = transeletion.getTranselationTextByTextIdentifier("lblClass")
                + " " + transeletion.getTranselationTextByTextIdentifier("lblWork")
                + "/" + transeletion.getTranselationTextByTextIdentifier("lblQuizzes");
        lblNum = transeletion.getTranselationTextByTextIdentifier("lblNum");

        warningNoStudentMsg = transeletion.getTranselationTextByTextIdentifier("lblNoStudentsForGrade");

        btnTitleYes = transeletion.getTranselationTextByTextIdentifier("btnTitleYes");
        lblNo = transeletion.getTranselationTextByTextIdentifier("lblNo");
        lblTheHomeworkAssignmentYouEntered = transeletion.getTranselationTextByTextIdentifier("lblYouMustAssignOrSave");
        mfPleaseSelectOneOrMoreEquationType = transeletion.getTranselationTextByTextIdentifier("lblSelectOneOrMoreHomeworkType");

        //class title changes
        txtSelectClass.setText(transeletion.getTranselationTextByTextIdentifier("lblClass"));
        btnClassListDone.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleDone"));
        txtSelectClassValue.setHint(transeletion.getTranselationTextByTextIdentifier("lblSelect"));

        //new HW changes
        txtCreatesAssignment.setText(transeletion.getTranselationTextByTextIdentifier("lblCreate")
                + " " + transeletion.getTranselationTextByTextIdentifier("lblClass") + " "
                + transeletion.getTranselationTextByTextIdentifier("lblAssignment")
                + "/" + transeletion.getTranselationTextByTextIdentifier("lblQuizzes"));
        txtTypeValues.setHint(transeletion.getTranselationTextByTextIdentifier("lblNew"));
        txtStudent.setText(transeletion.getTranselationTextByTextIdentifier("mfBtnTitleStudents"));
        txtStudentPicker.setHint(transeletion.getTranselationTextByTextIdentifier("lblAll"));
        txtAssignHomework.setText(transeletion.getTranselationTextByTextIdentifier("lblAssignHomework"));
        txtOnceAssign.setText(transeletion.getTranselationTextByTextIdentifier("lblOnceAssigned"));
        btnSave.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleSave"));

        edtDatePicker.setHint(transeletion.getTranselationTextByTextIdentifier("lblSelect"));
        transeletion.closeConnection();

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside setTextFromTranslation()");

    }

    /**
     * Add Homework assignment quiz
     */
    private void clickOnAddHomwworkQuizzess(){
        if(getSelectedClassSubjectId()
                != MathFriendzyHelper.MATH_SUBJECT_ID) {
            onCustomeHomeworkClicked();//new HW change to change the order
            return ;
        }

        //if(isSeletionMakeSureDialogShow){
        MathFriendzyHelper.showHomeworkQuizzSelectionDialog(this, new QuizzSelectionInterface() {

            @Override
            public void onWordProblemSelected() {
                onPracticeSkillCliked();//new HW change to change the order
            }

            @Override
            public void onPracticeSkillSelected() {
                onCustomeHomeworkClicked();//new HW change to change the order
            }

            @Override
            public void onCustomeHomeworkSelected() {
                onWorkProblemClicked();//new HW change to change the order
            }
        } , customeHomeworkTxt , practiceSkillTxt  , wordProblemTxt);
            /*}else{
                isSeletionMakeSureDialogShow = true;
                MathFriendzyHelper.warningDialog(this, lblMakeSureToSelectGrade);
            }*/
    }

    /**
     * Select assignment for practice skill
     */
    private void onPracticeSkillCliked(){
        Intent intent = new Intent(ActAssignHomework.this ,
                ActAssignPracticeSkillHomework.class);
        intent.putExtra("selectedPracticeSkill",
                getGsonStringForLinkedHaskMap(mapChildList));
        intent.putExtra("openSelectedCatList",openSelectedCatList);
        startActivityForResult(intent, SELECT_PRACTICE_SKILL_REQUEST);
    }

    /**
     * Select word problem
     */
    private void onWorkProblemClicked(){
        final String grade = selectedGrade + "";
        //spinnerGrade.getSelectedItem().toString();
        MathFriendzyHelper.downLoadQuestionForWordProblem(ActAssignHomework.this,
                Integer.parseInt(grade), new HttpServerRequest() {
                    @Override
                    public void onRequestComplete() {
                        goToAssignWordProblem(grade);
                    }
                });
    }

    /**
     * Select Custom Homework
     */
    private void onCustomeHomeworkClicked(){
            /*Intent intent = new Intent(ActAssignHomework.this , ActAssignCustomeHomework.class);
            intent.putExtra("customeQuizList", customeQuizList);
            startActivityForResult(intent, SELECT_CUSTOME_HOMEWORK_REQUEST);*/
        if(customeQuizList == null)
            customeQuizList = new ArrayList<CustomeResult>();
        customeQuizList.add(this.getCustomeResultWithTitle(""));
        this.setAssignData();
    }

    /**
     * Create new custom result objects based on the title name
     * @param title
     * @return
     */
    public CustomeResult getCustomeResultWithTitle(String title){
        CustomeResult customeResult = new CustomeResult();
        customeResult.setShowAns(1);//set default show answer
        customeResult.setAllowChanges(1);//set default allow changes
        customeResult.setNewHomework(true);
        customeResult.setTitle(title);
        return customeResult;
    }

    /**
     * Go to Assign work problem screen
     * @param grade
     */
    private void goToAssignWordProblem(String grade){
        Intent intent = new Intent(ActAssignHomework.this , ActAssignSchoolCurriculumHomework.class);
        intent.putExtra("selectedStudentGrade", grade);
        intent.putExtra("mSubCatList", this.getGsonStringForLinkedHaskMapForWordProblem(mSubCatList));
        intent.putExtra("openSelectedCatListForWordProblem", openSelectedCatListForWordProblem);
        //for show by CCSS
        intent.putExtra("categoryInfoList", categoryInfoList);
        startActivityForResult(intent, SELECT_WORD_PROBLEM_REQUEST);
    }

    /**
     * Set the data picker value
     * @param year
     * @param month
     * @param dayOnmonth
     */
    private void setDatepickerValue(int year , int month , int dayOnmonth){
        month ++;
        edtDatePicker.setText(this.getDoubleDigitFormatNumber(month)
                + "-" + this.getDoubleDigitFormatNumber(dayOnmonth)
                + "-" + this.getDoubleDigitFormatNumber(year));
    }

    /**
     * For DatePicker
     */
    private void clickOnDueDateLayout(){
        final Calendar calender = MathFriendzyHelper.getCurrentCalender();
        MathFriendzyHelper.showDatePickerDialog(this, calender.get(Calendar.YEAR),
                calender.get(Calendar.MONTH), calender.get(Calendar.DAY_OF_MONTH),
                new OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);

                        if (newDate.after(calender)) {
                            setDatepickerValue(year, monthOfYear, dayOfMonth);
                        }
                    }
                } , calender);
    }

    /**
     * Selecte Student form the list
     */
    private void clickOnSelectStudent(){
        Intent intent = new Intent(this , ActSelectStudentForHomeworkAssignment.class);
        //intent.putExtra("selectedGrade", spinnerGrade.getSelectedItem().toString());
        intent.putExtra("selectedGrade", selectedGrade + "");
        intent.putExtra("teacherId", teacherId);
        intent.putExtra("selectedStudentList", selectedStudentList);
        ArrayList<ClassWithName> selectedClassList = this.getSelectedClasses();
        if(selectedClassList != null && selectedClassList.size() > 0){
            intent.putExtra("isClassSelected", true);
            intent.putExtra("selectedClassList" , selectedClassList);
            startActivityForResult(intent, SELECT_STUDENT_REQUEST);
        }else{
            MathFriendzyHelper.showWarningDialog(this , MathFriendzyHelper
                    .getTreanslationTextById(this , "lblPlsSelectClass"));
        }
    }

    /**
     * Return the selected student json String
     * @return
     */
    private String getSelectedStudentJsonString(){
        JSONArray studentJsonArray = new JSONArray();
        try {
            ArrayList<ClassWithName> selectedClassList = this.getSelectedClasses();
            ArrayList<GetStudentByGradeResponse> filterList = null;
            if(selectedClassList != null && selectedClassList.size() > 0){
                filterList = this.filterStudentListBasedOnSelectedClass(selectedStudentList);
            }else{
                filterList = selectedStudentList;
            }

                /*for(int i = 0 ; i < selectedStudentList.size() ; i ++ ){
                    if(selectedStudentList.get(i).isSelected()
                            && !selectedStudentList.get(i).getfName().equals("All")){
                        JSONObject json = new JSONObject();
                        json.put("parentId", selectedStudentList.get(i).getParentUserId());
                        json.put("playerId", selectedStudentList.get(i).getPlayerId());
                        studentJsonArray.put(json);
                    }
                }*/

            if(filterList != null && filterList.size() > 0){
                //this.printStudent(filterList);
                filterList = GetStudentByGradeResponse.removeDuplicateStudents(filterList);
                //CommonUtils.printLog(TAG , "After remove duplicate");
                //this.printStudent(filterList);
                for(int i = 0 ; i < filterList.size() ; i ++ ){
                    if(filterList.get(i).isSelected()
                            && !filterList.get(i).getfName().equals("All")){
                        JSONObject json = new JSONObject();
                        json.put("parentId", filterList.get(i).getParentUserId());
                        json.put("playerId", filterList.get(i).getPlayerId());
                        json.put("classId", filterList.get(i).getClassId());
                        studentJsonArray.put(json);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }
        return studentJsonArray.toString();
    }

        /*private void printStudent(ArrayList<GetStudentByGradeResponse> students){
            for (GetStudentByGradeResponse ays : students) {
                CommonUtils.printLog(TAG , "playerId " + ays.getPlayerId()
                        + " parent User Id " + ays.getParentUserId() + " classID " + ays.getClassId());
            }
        }*/

    /**
     * Create practice skill answer json
     * @return
     */
    private String getPracticeSkillJsonString(){
        JSONArray practiceJsonArray = new JSONArray();
        try{
            if(practiceList != null && practiceList.size() > 0){
                for(int key : practiceList.keySet()){
                    ArrayList<LearningCenterTransferObj> catlist = practiceList.get(key);
                    for(int i = 0 ; i < catlist.size() ; i ++ ){
                        JSONObject json = new JSONObject();
                        json.put("categoryId", key + "");
                        json.put("subCategId", catlist.get(i).getMathOperationCategoryId() + "");
                        json.put("problems", catlist.get(i).getNumberOfProblemSelected() + "");
                        practiceJsonArray.put(json);
                    }
                }
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }
        return practiceJsonArray.toString();
    }


    /**
     * Create word problem answer json
     * @return
     */
    private String getWordProblemJsonString(){
        JSONArray practiceJsonArray = new JSONArray();
        try{
            if(workProblemList != null && workProblemList.size() > 0){
                for(String key : workProblemList.keySet()){
                    ArrayList<SubCatergoryTransferObj> catlist = workProblemList.get(key);
                    for(int i = 0 ; i < catlist.size() ; i ++ ){
                        JSONObject json = new JSONObject();
                        json.put("categoryId", catlist.get(i).getMainCatId() + "");
                        json.put("subCategId", catlist.get(i).getId() + "");
                        practiceJsonArray.put(json);
                    }
                }
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }
        return practiceJsonArray.toString();
    }

    /**
     * Create json ans array String
     * @param ansList
     * @return
     */
    private JSONArray getAnsJsonArray(ArrayList<String> ansList){
        JSONArray ansArray = new JSONArray();
        try{
            for(int i = 0 ; i < ansList.size() ; i ++ ){
                ansArray.put(ansList.get(i));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return ansArray;
    }

    /**
     * Create ans list for the single correct ans
     * @param correctAns
     * @return
     */
    private ArrayList<String> getSingleAnsArrayList(String correctAns){
        ArrayList<String> list = new ArrayList<String>();
        list.add(correctAns);
        return list;
    }

    /**
     * Create yes no or true false list
     * @param answerType
     * @return
     */
    private ArrayList<String> getYESNOORTRUEFALSELIST(int answerType){
        ArrayList<String> list = new ArrayList<String>();
        if(answerType == YES_NO_ANS_TYPE){
            list.add(ActMultipleChoiceAnsSheet.YES);
            list.add(ActMultipleChoiceAnsSheet.NO);
        }else if(answerType == TRUE_FALSE_ANS_TYPE){
            list.add(ActMultipleChoiceAnsSheet.TRUE);
            list.add(ActMultipleChoiceAnsSheet.FALSE);
        }
        return list;
    }

    /**
     * return the answer sheet name
     * @param index
     * @return
     */
    public static String getQuestionSheetName(int index){
        return PDFOperation.ANS_SHEET_PREFF + index;
    }

    private JSONArray getGooruResourceString
            (ArrayList<ResourceResponse> selectedResourceList){
        JSONArray jsonArray = new JSONArray();
        try {
            if(selectedResourceList != null && selectedResourceList.size() > 0){
                for(int i = 0 ; i < selectedResourceList.size() ; i ++ ){
                    ResourceResponse resource = selectedResourceList.get(i);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("isMobileFriendliness" , resource.getMediaType());
                    jsonObject.put("resourceUrl" , resource.getResourceUrl());
                    JSONArray jsonCurriculumArray = new JSONArray();
                    for(int j = 0 ; j < resource.getCurriculumCode().size() ; j ++ ){
                        jsonCurriculumArray.put(resource.getCurriculumCode().get(j));
                    }
                    jsonObject.put("curriculumCode" , jsonCurriculumArray);
                    jsonObject.put("resourceFormat" , resource.getValue());
                    jsonObject.put("scollectionCount" , resource.getScollectionCount());
                    jsonObject.put("title" , resource.getTitle());
                    jsonObject.put("description" , resource.getDescription());
                    jsonObject.put("url" , resource.getUrl());
                    jsonArray.put(jsonObject);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }


    /**
     * Create custome answer json String
     * @return
     */
    private String getCustomeAnsJsonString(){
        /*if(true){
            return "[{\"answers\":[{\"questionNum\":\"1%@$\",\"correctAnswers\":[\"text#852\"],\"measurementUnit\":\"=%\\/&\",\"isFillInBlank\":1,\"isLeftMeasurement\":0,\"multipleChoiceQuestionType\":0,\"answers\":[],\"questionStr\":\"hf@$&*% €£₩(\",\"isAnswerAvailable\":1},{\"questionNum\":\"2€£\",\"correctAnswers\":[\"A\",\"B\"],\"measurementUnit\":\"\",\"isFillInBlank\":0,\"isLeftMeasurement\":0,\"multipleChoiceQuestionType\":1,\"answers\":[\"A\",\"B\",\"C\",\"D\"],\"questionStr\":\"jvcjgc#\\/%_€£\",\"isAnswerAvailable\":1},{\"questionNum\":\"3£¥£\",\"correctAnswers\":[\"On Work Area\\nUse this for questions that cannot be answered with our keyboard.\"],\"measurementUnit\":\"jgcjgc^*&&^\",\"isFillInBlank\":1,\"isLeftMeasurement\":0,\"multipleChoiceQuestionType\":0,\"answers\":[],\"questionStr\":\"ibfibf $\\/$^:\",\"isAnswerAvailable\":0},{\"questionNum\":\"4€$\\/€\\/$=_\",\"correctAnswers\":[\"True\"],\"measurementUnit\":\"\",\"isFillInBlank\":0,\"isLeftMeasurement\":0,\"multipleChoiceQuestionType\":2,\"answers\":[\"True\",\"False\"],\"questionStr\":\"igckbc £^\\/£^\\*//*^\\/₩\",\"isAnswerAvailable\":1},{\"questionNum\":\"5igdigf\",\"correctAnswers\":[\"Yes\"],\"measurementUnit\":\"\",\"isFillInBlank\":0,\"isLeftMeasurement\":0,\"multipleChoiceQuestionType\":3,\"answers\":[\"Yes\",\"No\"],\"questionStr\":\"kbckhvkh (\\/¥&^£^^\",\"isAnswerAvailable\":1},{\"questionNum\":\"6(;^^\",\"correctAnswers\":[\"text#3 op_minus 2 op_minus 2\"],\"measurementUnit\":\"fjh@$$\",\"isFillInBlank\":1,\"isLeftMeasurement\":0,\"multipleChoiceQuestionType\":0,\"answers\":[],\"questionStr\":\"igckgc\",\"isAnswerAvailable\":1}],\"showAnswers\":\"1\",\"links\":[{\"title\":\"Google\",\"url\":\"google.com\"}],\"allowChanges\":\"1\",\"title\":\"Alex Test link added with question\",\"questionSheet\":\"\"},{\"answers\":[{\"questionNum\":\"1(:&\",\"correctAnswers\":[\"text#85\"],\"measurementUnit\":\"ugcjg\",\"isFillInBlank\":1,\"isLeftMeasurement\":0,\"multipleChoiceQuestionType\":0,\"answers\":[],\"questionStr\":\"\",\"isAnswerAvailable\":1},{\"questionNum\":\"2£;\\\"\",\"correctAnswers\":[\"text#82\"],\"measurementUnit\":\"jbf £^\\*//*&\",\"isFillInBlank\":1,\"isLeftMeasurement\":0,\"multipleChoiceQuestionType\":0,\"answers\":[],\"questionStr\":\"\",\"isAnswerAvailable\":1},{\"questionNum\":\"3igc\",\"correctAnswers\":[\"A\",\"B\"],\"measurementUnit\":\"\",\"isFillInBlank\":0,\"isLeftMeasurement\":0,\"multipleChoiceQuestionType\":1,\"answers\":[\"A\",\"B\",\"C\",\"D\"],\"questionStr\":\"\",\"isAnswerAvailable\":1},{\"questionNum\":\"4¥&\\/\",\"correctAnswers\":[\"True\"],\"measurementUnit\":\"\",\"isFillInBlank\":0,\"isLeftMeasurement\":0,\"multipleChoiceQuestionType\":2,\"answers\":[\"True\",\"False\"],\"questionStr\":\"\",\"isAnswerAvailable\":1},{\"questionNum\":\"5£&\\\"\",\"correctAnswers\":[\"Yes\"],\"measurementUnit\":\"\",\"isFillInBlank\":0,\"isLeftMeasurement\":0,\"multipleChoiceQuestionType\":3,\"answers\":[\"Yes\",\"No\"],\"questionStr\":\"\",\"isAnswerAvailable\":1},{\"questionNum\":\"6uvc=^$\",\"correctAnswers\":[\"On Work Area\\nUse this for questions that cannot be answered with our keyboard.\"],\"measurementUnit\":\"jgc £&\\/\",\"isFillInBlank\":1,\"isLeftMeasurement\":0,\"multipleChoiceQuestionType\":0,\"answers\":[],\"questionStr\":\"\",\"isAnswerAvailable\":0}],\"showAnswers\":\"1\",\"links\":[],\"allowChanges\":\"1\",\"title\":\"Alex Test without link and question\",\"questionSheet\":\"\"}]";
        }*/

        JSONArray customeJsonArray = new JSONArray();
        try{
            if(customeQuizList != null && customeQuizList.size() > 0){
                for(int i = 0 ; i < customeQuizList.size() ; i ++ ) {
                    CustomeResult customeResult = customeQuizList.get(i);
                    JSONObject json = new JSONObject();
                    json.put("title", customeResult.getTitle());
                    json.put("allowChanges", customeResult.getAllowChanges() + "");
                    json.put("showAnswers", customeResult.getShowAns() + "");
                    json.put("questionSheet", customeResult.getSheetName());
                    if (customeResult.getSelectedResourceList() != null
                            && customeResult.getSelectedResourceList().size() > 0)
                        json.put("gooruResources", this.getGooruResourceString
                                (customeResult.getSelectedResourceList()));
                    json.put("links", new JSONArray(MathFriendzyHelper
                            .getLinkString(customeResult.getLinks())));

                    ArrayList<CustomeAns> customeAnslist = customeResult.getCustomeAnsList();
                    JSONArray customeAnswersArray = new JSONArray();
                    if (customeAnslist != null && customeAnslist.size() > 0){
                        for (int j = 0; j < customeAnslist.size(); j++) {
                            CustomeAns customeAns = customeAnslist.get(j);
                            JSONObject customeAnsJson = new JSONObject();
                            customeAnsJson.put("questionNum", customeAns.getQueNo() + "");
                            customeAnsJson.put("isLeftMeasurement", customeAns.getIsLeftUnit());
                            customeAnsJson.put("measurementUnit", customeAns.getAnsSuffix());

                            if (customeAns.getFillInType() == FILL_IN_TYPE) {
                                customeAnsJson.put("correctAnswers", this.getAnsJsonArray
                                        (this.getSingleAnsArrayList(customeAns.getCorrectAns())));
                                customeAnsJson.put("answers", this.getAnsJsonArray
                                        (new ArrayList<String>()));
                                customeAnsJson.put("multipleChoiceQuestionType", 0);
                                customeAnsJson.put("isAnswerAvailable", customeAns.getIsAnswerAvailable());
                            } else {
                                customeAnsJson.put("isAnswerAvailable", 1);
                                if (customeAns.getAnswerType() == MULTIPLE_CHOICE_ANS_TYPE) {
                                    customeAnsJson.put("correctAnswers", this.getAnsJsonArray
                                            (MathFriendzyHelper.getCommaSepratedOptionInArrayList
                                                    (MathFriendzyHelper.convertStringIntoSperatedStringWithDelimeter
                                                            (customeAns.getCorrectAns(), ","), ",")));
                                    customeAnsJson.put("answers", this.getAnsJsonArray
                                            (customeAns.getMultipleChoiceOptionList()));
                                } else if (customeAns.getAnswerType() == YES_NO_ANS_TYPE) {
                                    customeAnsJson.put("correctAnswers", this.getAnsJsonArray
                                            (this.getSingleAnsArrayList(customeAns.getCorrectAns())));
                                    customeAnsJson.put("answers", this.getAnsJsonArray
                                            (this.getYESNOORTRUEFALSELIST(YES_NO_ANS_TYPE)));
                                } else if (customeAns.getAnswerType() == TRUE_FALSE_ANS_TYPE) {
                                    customeAnsJson.put("correctAnswers", this.getAnsJsonArray
                                            (this.getSingleAnsArrayList(customeAns.getCorrectAns())));
                                    customeAnsJson.put("answers", this.getAnsJsonArray
                                            (this.getYESNOORTRUEFALSELIST(TRUE_FALSE_ANS_TYPE)));
                                }
                                customeAnsJson.put("multipleChoiceQuestionType", customeAns.getAnswerType());
                            }
                            customeAnsJson.put("isFillInBlank", customeAns.getFillInType());
                            customeAnsJson.put("questionStr", customeAns.getQuestionString());
                            customeAnswersArray.put(customeAnsJson);
                        }
                    }
                    json.put("answers", customeAnswersArray);

                    if(isTeacherEditHomework){
                        json.put("customHwId", customeResult.getCustomHwId());
                    }
                    customeJsonArray.put(json);
                }
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }
        return customeJsonArray.toString();
    }

    /**
     * Check for student selected
     * @return
     */
    private boolean isAtleasetOneStudentIsSelected(){
        if(selectedStudentList != null && selectedStudentList.size() > 0){
            for(int i = 0 ; i < selectedStudentList.size() ; i ++ ){
                if(selectedStudentList.get(i).isSelected()
                        && !selectedStudentList.get(i).getfName().equals("All")){
                    return true;
                }
            }
            return false;
        }else{
            return false;
        }
    }

    /**
     * Assign the Homework
     */
    private void clickOnAssignHomework(){
        if(!this.isDueDateSelected()){
            MathFriendzyHelper.showWarningDialog(this , MathFriendzyHelper
                    .getTreanslationTextById(this ,"lblPleaseSelectDueDate"));
            return ;
        }

        if(!isTeacherEditHomework) {
            ArrayList<ClassWithName> selectedClassList = this.getSelectedClasses();
            if (!(selectedClassList != null && selectedClassList.size() > 0)) {
                MathFriendzyHelper.showWarningDialog(this, MathFriendzyHelper
                        .getTreanslationTextById(this, "lblPlsSelectClass"));
                return;
            }
        }


        if(this.isAtleasetOneStudentIsSelected() || isTeacherEditHomework){//already selected student
            int code = this.isAnyCustomHwWithoutTitleOrAns();
            if(code == NO_TITLE_ENTER){
                MathFriendzyHelper.showWarningDialog(this ,
                        MathFriendzyHelper.getTreanslationTextById(this , "lblEnterTitleCustomHw"));
                return;
            }else if(code == NO_ANS_GIVEN){
                MathFriendzyHelper.showWarningDialog(this ,
                        MathFriendzyHelper.getTreanslationTextById(this , "lblPleaseFillAllAnsCustomHomework"));
                return ;
            }

            if(this.isNeedToShowDialog()){
                //if(true){
                AssignHomeworkParam param = new AssignHomeworkParam();
                param.setAction("addHomeworkForStudents");
                param.setUserId(teacherId);
                param.setDate(MathFriendzyHelper.formatDataInYYYYMMDD
                        (edtDatePicker.getText().toString()));
                param.setGrade(selectedGrade + "");
                //param.setGrade(spinnerGrade.getSelectedItem().toString());
                param.setStudentJson(this.getSelectedStudentJsonString());
                param.setPracticeCatJson(this.getPracticeSkillJsonString());
                param.setWordCatJson(this.getWordProblemJsonString());
                param.setCustomeDataJson(this.getCustomeAnsJsonString());
                param.setMessage(txtTeacherMessage.getText().toString());

                try {
                    if (selectedHWData != null && selectedHWData.isSavedHomeworkSelected()) {
                        param.setHwId(selectedHWData.getSavedHwId());//to delete the saved homework
                    } else {
                        param.setHwId(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

                if(isTeacherEditHomework){
                    param.setTeacherEditHomework(true);
                    param.setHwId(editTeacherHWData.getSavedHwId());
                }

                if(CommonUtils.isInternetConnectionAvailable(this)){
                    new MyAsyckTask(ServerOperation.createPostRequestForAssignHomework(param)
                            , null, ServerOperationUtil.ASSIGN_HOMEWORK_REQUEST, this,
                            this, ServerOperationUtil.SIMPLE_DIALOG , true ,
                            getString(R.string.please_wait_dialog_msg))
                            .execute();
                }else{
                    CommonUtils.showInternetDialog(this);
                }
            }else{
                MathFriendzyHelper.warningDialog(this, mfPleaseSelectOneOrMoreEquationType);
            }
        }else{
                /*GetStudentByGradeParam param = new GetStudentByGradeParam();
                param.setAction("getStudentsForGrade");
                param.setGrade(spinnerGrade.getSelectedItem().toString());
                param.setTeacherId(teacherId);
                if(registerUserFromPreff != null){
                    param.setSchoolYear(registerUserFromPreff.getSchoolYear());
                }
                if(CommonUtils.isInternetConnectionAvailable(this)){
                    new MyAsyckTask(ServerOperation.createPostRequestForGetStudentByGrade(param)
                            , null, ServerOperationUtil.GET_STUDENTS_BY_GRADE_REQUEST, this,
                            this, ServerOperationUtil.SIMPLE_DIALOG , true ,
                            getString(R.string.please_wait_dialog_msg))
                            .execute();
                }else{
                    CommonUtils.showInternetDialog(this);
                }*/
            MathFriendzyHelper.showWarningDialog(this , MathFriendzyHelper
                    .getTreanslationTextById(this , "lblPlsSelectStudentForHW"));
        }
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase,
                               int requestCode) {
        if(requestCode == ServerOperationUtil.ASSIGN_HOMEWORK_REQUEST){
            AssignHomeworkResponse response = (AssignHomeworkResponse) httpResponseBase;
            if(response.getResult().equals("success")){
                if(isTeacherEditHomework) {
                    return;
                }
                try{
                    PDFOperation.renamePdfFilesAfterGettingHWID(this,
                            response.getData());
                    UploadPdf.uploadPdf(PDFOperation.getAllPdfPathStartWithGivenHWID
                            (this, response.getData()));
                }catch(Exception e){
                    e.printStackTrace();
                }
                setResult(RESULT_OK);
                finish();
            }
        }if(requestCode == ServerOperationUtil.GET_STUDENTS_BY_GRADE_REQUEST){
            GetStudentByGradeResponse response = (GetStudentByGradeResponse) httpResponseBase;
            ArrayList<GetStudentByGradeResponse> arrayList = response.getStudentList();
            if(arrayList.size() > 0){
                selectedStudentList = arrayList;
                clickOnAssignHomework();
            }else{
                MathFriendzyHelper.showWarningDialog(this, warningNoStudentMsg);
            }
        }
    }

    /**
     * Set the assign homework data into the list layout
     */
    private void setAssignData(){
        try{

            islblNumDisplayed = false;
            homeWorkQuizzLayout.removeAllViews();

            if(customeQuizList != null && customeQuizList.size() > 0){
                homeWorkQuizzLayout.addView
                        (this.addCustomeProblemData());
                homeWorkQuizzLayout.addView(this.getLineView());
            }

            if(practiceList != null && practiceList.size() > 0){
                homeWorkQuizzLayout.addView
                        (this.addPracticeSkillData());
                homeWorkQuizzLayout.addView(this.getLineView());
            }

            if(workProblemList != null && workProblemList.size() > 0){
                homeWorkQuizzLayout.addView
                        (this.addWordProblemData());
                homeWorkQuizzLayout.addView(this.getLineView());
            }

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    //To add line between layout change
    /**
     * Return the line to add between layouts
     * @return
     */
    private View getLineView(){
        return  LayoutInflater.from(this).inflate(R.layout.line_view_layout, null);
    }
    //add line change end

    /**
     * This method create and return a linear layout
     * @return
     */
    private LinearLayout getLinearLayout(){
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);
        return layout;
    }

    /*private void setArrowHeightWidth(View view){
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view.getLayoutParams();
        lp.height = 50;
        lp.width = 50;
        view.setLayoutParams(lp);
    }*/

    /**
     * Return the view with the selection type text
     * @param inflateFor
     * @return
     */
    private View getInflatedViewForQuizzQuestionType(int inflateFor){

        View view = LayoutInflater.from(this)
                .inflate(R.layout.homework_quizz_question_type_new_with_green_button, null); // some changes by siddhiinfosoft
        TextView txtView = (TextView)view.findViewById(R.id.txtQuestionType);
        TextView txtNo   = (TextView)view.findViewById(R.id.txtNo);
        TextView txtScore   = (TextView)view.findViewById(R.id.txtScore);
        RelativeLayout arrowLayout = (RelativeLayout) view.findViewById(R.id.arrowLayout);

        txtNo.setVisibility(TextView.VISIBLE);
        txtScore.setVisibility(TextView.VISIBLE);

        if(!islblNumDisplayed){
            islblNumDisplayed = true;
            txtNo.setText(lblNum);
        }else{
            txtNo.setText("");
        }

        //this.setArrowHeightWidth(txtScore);
        txtScore.setText("");
        //txtScore.setBackgroundResource(R.drawable.arrow);

        if(inflateFor == 1){//for practice skill
            txtView.setText(practiceSkillTxt);

            arrowLayout.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    onPracticeSkillCliked();
                }
            });

            if(isTeacherEditHomework){
                arrowLayout.setVisibility(View.INVISIBLE);
            }
        }else if(inflateFor == 2){//for word problem
            txtView.setText(wordProblemTxt);

            arrowLayout.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    onWorkProblemClicked();
                }
            });

            if(isTeacherEditHomework){
                arrowLayout.setVisibility(View.INVISIBLE);
            }
        }else if(inflateFor == 3){//for custom
            txtView.setText(classWorkQuizz);
            txtScore.setVisibility(TextView.GONE);
            arrowLayout.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    onCustomeHomeworkClicked();
                }
            });
        }
        return view;
    }

    /**
     * This method add the practice layout data into the main layout to display
     * @param
     */
    private LinearLayout addPracticeSkillData(){
        LinearLayout layout = this.getLinearLayout();
        layout.addView(this.getInflatedViewForQuizzQuestionType(1));
        layout.addView(this.addPracticeSkillLayout());
        return layout;
    }

    /**
     * Add word problem layout
     * @return
     */
    private LinearLayout addWordProblemData(){
        LinearLayout layout = this.getLinearLayout();
        layout.addView(this.getInflatedViewForQuizzQuestionType(2));
        layout.addView(this.addWordProblemLayout());
        return layout;
    }

    /**
     * Add custom problem layout
     * @return
     */
    private LinearLayout addCustomeProblemData(){
        LinearLayout layout = this.getLinearLayout();
        layout.addView(this.getInflatedViewForQuizzQuestionType(3));
        layout.addView(this.addCustomProblemLayout());
        return layout;
    }

    @SuppressLint("InflateParams")
    private View getCategoryInflatedLayout(int inflateFor){
        View view = LayoutInflater.from(this)
                .inflate(R.layout.school_home_question_categories_layout_new, null);
        return view;
    }

    @SuppressLint("InflateParams")
    private View getSubCategoryInflatedLayout(int inflateFor){
        View view = LayoutInflater.from(this)
                .inflate(R.layout.home_work_quizz_sub_category_layout_new_with_green_button, null);
        return view;
    }

    /**
     * This method add the practice skill layout with cat and subCat
     * @param
     * @return
     */
    private LinearLayout addPracticeSkillLayout(){
        LinearLayout layout = this.getLinearLayout();

        for(int key : practiceList.keySet()){
            ArrayList<LearningCenterTransferObj> subCatList = practiceList.get(key);

            View view = this.getCategoryInflatedLayout(1);
            ImageView imgSign = (ImageView) view.findViewById(R.id.imgSign);
            TextView  txtCatName = (TextView) view.findViewById(R.id.txtCategoryName);
            imgSign.setBackgroundResource(getResources().getIdentifier
                    ("mf_" + subCatList.get(0)
                                    .getLearningCenterOperation()
                                    .toLowerCase().replace(" ","_")+"_sign", "drawable",
                            getPackageName()));
            txtCatName.setText(MathFriendzyHelper.getUpdatedCatNameByCatName
                    (this,subCatList.get(0).getLearningCenterOperation()));
            layout.addView(view);

            for(int j = 0 ; j < subCatList.size() ; j ++ ){
                LearningCenterTransferObj subCatObj = subCatList.get(j);
                View subCatView = this.getSubCategoryInflatedLayout(1);
                TextView txtSubCatName = (TextView) subCatView.findViewById(R.id.txtSubCategoryName);
                TextView txtNoOfQuestions = (TextView) subCatView.findViewById(R.id.txtNo);
                TextView txtScore = (TextView) subCatView.findViewById(R.id.txtScore);
                ImageView imgArrow = (ImageView) subCatView.findViewById(R.id.imgArrow);

                imgArrow.setVisibility(ImageView.INVISIBLE);
                txtSubCatName.setText(subCatObj.getMathOperationCategory());
                txtNoOfQuestions.setText(subCatObj.getNumberOfProblemSelected() + "");
                txtScore.setVisibility(TextView.INVISIBLE);
                layout.addView(subCatView);
            }
        }

        return layout;
    }

    /**
     * Add work problem layout
     */
    private LinearLayout addWordProblemLayout(){

        LinearLayout layout = this.getLinearLayout();

        for(String key : workProblemList.keySet()){

            View view = this.getCategoryInflatedLayout(1);
            ImageView imgSign = (ImageView) view.findViewById(R.id.imgSign);
            TextView  txtCatName = (TextView) view.findViewById(R.id.txtCategoryName);
            imgSign.setVisibility(ImageView.GONE);
            txtCatName.setText(key);
            layout.addView(view);

            ArrayList<SubCatergoryTransferObj> subCatList = workProblemList.get(key);

            for(int j = 0 ; j < subCatList.size() ; j ++ ){
                SubCatergoryTransferObj subCatObj = subCatList.get(j);
                View subCatView = this.getSubCategoryInflatedLayout(1);
                TextView txtSubCatName = (TextView) subCatView.findViewById(R.id.txtSubCategoryName);
                TextView txtNoOfQuestions = (TextView) subCatView.findViewById(R.id.txtNo);
                TextView txtScore = (TextView) subCatView.findViewById(R.id.txtScore);
                ImageView imgArrow = (ImageView) subCatView.findViewById(R.id.imgArrow);

                imgArrow.setVisibility(ImageView.INVISIBLE);
                txtSubCatName.setText(subCatObj.getName());
                txtNoOfQuestions.setText(subCatObj.getNumberOfQuestions() + "");
                txtScore.setVisibility(TextView.INVISIBLE);
                layout.addView(subCatView);
            }
        }

        return layout;
    }

    /**
     * Add Custom Problem layout
     * @return
     */
    private LinearLayout addCustomProblemLayout(){
        LinearLayout layout = this.getLinearLayout();
        for(int i = 0 ; i < customeQuizList.size() ; i ++ ){
            View view = this.getSubCategoryInflatedLayout(1);
            TextView txtSubCatName = (TextView) view.findViewById(R.id.txtSubCategoryName);
            TextView txtNoOfQuestions = (TextView) view.findViewById(R.id.txtNo);
            TextView txtScore = (TextView) view.findViewById(R.id.txtScore);
            ImageView imgArrow = (ImageView) view.findViewById(R.id.imgArrow);
            RelativeLayout rlArrowLayout = (RelativeLayout) view.findViewById(R.id.rlArrowLayout);
            final EditText edtCustomTitle = (EditText) view.findViewById(R.id.edtCustomTitle);
            edtCustomTitle.setHint(MathFriendzyHelper.getTreanslationTextById(this , "lblEnterTitleCustomHw"));
            edtCustomTitle.setVisibility(EditText.VISIBLE);
            //imgArrow.setVisibility(ImageView.INVISIBLE);
            txtScore.setVisibility(TextView.INVISIBLE);
            txtSubCatName.setText(customeQuizList.get(i).getTitle());
            edtCustomTitle.setText(customeQuizList.get(i).getTitle());
            txtSubCatName.setVisibility(TextView.INVISIBLE);

            //this.setArrowHeightWidth(imgArrow);
            //imgArrow.setBackgroundResource(R.drawable.assign_hw_arrow);

            if(customeQuizList.get(i).isNewHomework()){
                txtNoOfQuestions.setVisibility(TextView.GONE);
            }else {
                txtNoOfQuestions.setText(customeQuizList.get(i).getCustomeAnsList().size() + "");
            }
            layout.addView(view);

            final CustomeResult customeResult = customeQuizList.get(i);
            final int index = i;
            rlArrowLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    customeResult.setTitle(edtCustomTitle.getText().toString());
                    openAssignCustomeAnsScreen(index , customeResult);
                }
            });

            edtCustomTitle.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(!hasFocus){
                        customeQuizList.get(index).setTitle(((EditText)v).getText().toString());
                    }
                }
            });
        }
        return layout;
    }

    public final int START_ACT_ASSIGN_CUSTOME_ANS = 100001;
    private int customeHWClickedIndex = 0;
    private void openAssignCustomeAnsScreen(int index , CustomeResult customeResult){
        this.customeHWClickedIndex = index;
        Intent intent = new Intent(this , ActAssignCustomAns.class);
        intent.putExtra("customeResult", customeResult);
        intent.putExtra("isTeacherEditHomework", isTeacherEditHomework);
        startActivityForResult(intent, START_ACT_ASSIGN_CUSTOME_ANS);
    }

    @Override
    public void onClick(View v) {
        this.cleasFocusFromEditText();
        switch(v.getId()){
            case R.id.btnAddHomeWorkQuizzes:
                this.clickOnAddHomwworkQuizzess();
                break;
            case R.id.datePickerLayout:
                this.clickOnDueDateLayout();
                break;
            case R.id.btnSelectStudent:
                this.clickOnSelectStudent();
                break;
            case R.id.btnAssignHomework:
                this.clickOnAssignHomework();
                break;
            case R.id.selectClassLayout:
                this.clickToSelectClass();
                break;
            case R.id.btnClassListDone:
                this.clickToClassSelectionDone();
                break;
            case R.id.gradeLayout://work as type layout
                this.clickOnSelectType();
                break;
            case R.id.rlStudentLayout:
                this.clickOnSelectStudent();
                break;
            case R.id.btnSave:
                this.saveHomework();
                break;
        }
    }

    /**
     * For back pressed
     */
    private void superBackPressed(){
        super.onBackPressed();
    }

    /**
     * Need to warning dialog on back pressed
     * @return
     */
    private boolean isNeedToShowDialog(){

        if(practiceList != null && practiceList.size() > 0){
            return true;
        }

        if(workProblemList != null && workProblemList.size() > 0){
            return true;
        }

        if(customeQuizList != null && customeQuizList.size() > 0){
            for(int i = 0 ; i < customeQuizList.size() ; i ++){
                if(customeQuizList.get(i).getCustomeAnsList() != null
                        && customeQuizList.get(i).getCustomeAnsList().size() > 0){
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    /**
     * Delete the unused pdf files from SD Card
     */
    private void deleteUnUsedPDFFiles(){
        PDFOperation.deleteUnusedPdf(this);
    }

    @Override
    public void onBackPressed() {
        if(this.isNeedToShowDialog()){
            if(isTeacherEditHomework){
                lblTheHomeworkAssignmentYouEntered =
                        MathFriendzyHelper.getTreanslationTextById(this , "selectSaveEditHw");
            }
            MathFriendzyHelper.yesNoConfirmationDialog
                    (this, lblTheHomeworkAssignmentYouEntered, btnTitleYes, lblNo,
                            new YesNoListenerInterface() {

                                @Override
                                public void onYes() {
                                    if(isTeacherEditHomework){
                                        deleteUnUsedPDFFiles();
                                        superBackPressed();
                                    }
                                }

                                @Override
                                public void onNo() {
                                    if(isTeacherEditHomework){
                                        return;
                                    }
                                    deleteUnUsedPDFFiles();
                                    superBackPressed();
                                }
                            });
        }else{
            superBackPressed();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            switch(requestCode){
                case SELECT_STUDENT_REQUEST:
                    selectedStudentList = (ArrayList<GetStudentByGradeResponse>)
                            data.getSerializableExtra("studentList");
                    break;
                case SELECT_PRACTICE_SKILL_REQUEST :
                    mapChildList = this.getLinkedHashMapFromGsonJsonString
                            (data.getStringExtra("selectedPracticeSkill"));
                    openSelectedCatList = (ArrayList<Boolean>)
                            data.getSerializableExtra("openSelectedCatList");
                    practiceList = new LinkedHashMap<Integer, ArrayList<LearningCenterTransferObj>>();

                    for(String key : mapChildList.keySet()){
                        ArrayList<LearningCenterTransferObj> learnList = mapChildList.get(key);
                        for(int i = 0 ; i < learnList.size() ; i ++ ){
                            if(learnList.get(i).isSelected()){

                                ArrayList<LearningCenterTransferObj> list = practiceList
                                        .get(learnList.get(i).getLearningCenterMathOperationId());
                                if(list == null){
                                    list = new ArrayList<LearningCenterTransferObj>();
                                }
                                //can't delete set cat name line
                                learnList.get(i).setLearningCenterOperation(key);//set cat name
                                list.add(learnList.get(i));
                                practiceList.put(learnList.get(i)
                                        .getLearningCenterMathOperationId()
                                        , list);

                            }
                        }
                    }
                    this.setAssignData();
                    break;
                case SELECT_WORD_PROBLEM_REQUEST:
                    selectedGrade = data.getIntExtra("selectedGrade" , INITIAL_GRADE);
                    mSubCatList = this.getLinkedHashMapFromGsonJsonStringForWorkProblem
                            (data.getStringExtra("mSubCatList"));
                    openSelectedCatListForWordProblem = (ArrayList<Boolean>)
                            data.getSerializableExtra("openSelectedCatListForWordProblem");
                    //for show by CCSS
                    categoryInfoList = (ArrayList<CatagoriesDto>)
                            data.getSerializableExtra("categoryInfoList");

                    workProblemList = new LinkedHashMap<String, ArrayList<SubCatergoryTransferObj>>();
                    for(String key : mSubCatList.keySet()){
                        ArrayList<SubCatergoryTransferObj> subCat = mSubCatList.get(key);
                        for(int i = 0 ; i < subCat.size() ; i ++ ){
                            if(subCat.get(i).isSelected()){

                                ArrayList<SubCatergoryTransferObj> list = workProblemList.get(key);
                                if(list == null){
                                    list = new ArrayList<SubCatergoryTransferObj>();
                                }
                                list.add(subCat.get(i));
                                workProblemList.put(key, list);
                            }
                        }
                    }
                    this.setAssignData();
                    break;
                case SELECT_CUSTOME_HOMEWORK_REQUEST:
                    customeQuizList = (ArrayList<CustomeResult>)
                            data.getSerializableExtra("customeQuizList");
                    this.setAssignData();
                    break;
                case START_ACT_ASSIGN_CUSTOME_ANS:
                    CustomeResult customeResult = (CustomeResult)
                            data.getSerializableExtra("customeResult");
                    if(customeResult.getCustomeAnsList() != null){
                        customeResult.setNewHomework(false);
                        customeQuizList.set(this.customeHWClickedIndex, customeResult);
                    }else{
                        if(customeQuizList.size() > 0){
                            customeQuizList.remove(this.customeHWClickedIndex);
                        }
                    }
                    this.setAssignData();
                    break;
                case SELECT_SAVED_HOMEWORK_REQUEST:
                    selectedHWData = (HomeworkData) data.getSerializableExtra("selectedHWData");
                    if(selectedHWData != null){
                        selectedHWData.setSavedHomeworkSelected(true);
                        this.setSelectedType(MathFriendzyHelper.getTreanslationTextById(this , "lblSaved"));
                        new AsyncTask<Void,Void,Void>(){
                            private ProgressDialog pd = null;
                            @Override
                            protected void onPreExecute() {
                                pd = MathFriendzyHelper.getProgressDialog(ActAssignHomework.this , "");
                                MathFriendzyHelper.showProgressDialog(pd);
                                super.onPreExecute();
                            }

                            @Override
                            protected Void doInBackground(Void... params) {
                                setSelectedSavedHomework(selectedHWData);
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                MathFriendzyHelper.hideDialog(pd);
                                setWidgetsDataFromSelectedHW(selectedHWData);
                                setClassesAndDownloadStudentForSavedHW(selectedHWData.getUserClasses());
                                setAssignData();
                                super.onPostExecute(aVoid);
                            }
                        }.execute();
                            /*this.setWidgetsDataFromSelectedHW(selectedHWData);
                            this.setAssignData();*/
                    }
                    break;
                case SELECT_PREVIOUS_HOMEWORK_REQUEST:
                    selectedHWData = (HomeworkData) data.getSerializableExtra("selectedHWData");
                    if(selectedHWData != null){
                        this.setSelectedType(MathFriendzyHelper.getTreanslationTextById(this , "btnPrevious"));
                        new AsyncTask<Void,Void,Void>(){
                            private ProgressDialog pd = null;
                            @Override
                            protected void onPreExecute() {
                                pd = MathFriendzyHelper.getProgressDialog(ActAssignHomework.this , "");
                                MathFriendzyHelper.showProgressDialog(pd);
                                super.onPreExecute();
                            }

                            @Override
                            protected Void doInBackground(Void... params) {
                                setSelectedSavedHomework(selectedHWData);
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                MathFriendzyHelper.hideDialog(pd);
                                setWidgetsDataFromSelectedHW(selectedHWData);
                                setClassesAndDownloadStudentForSavedHW(selectedHWData.getUserClasses());
                                setAssignData();
                                super.onPostExecute(aVoid);
                            }
                        }.execute();
                            /*this.setWidgetsDataFromSelectedHW(selectedHWData);
                            this.setAssignData();*/
                    }
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    //new HW changes
    private void clickOnSelectType() {
        final String texts[] = MathFriendzyHelper.getTreanslationTextById(this ,
                "lblNew" , "lblSaved" , "btnPrevious");
        MathFriendzyHelper.showHomeworkQuizzSelectionDialog(this, new QuizzSelectionInterface() {

            @Override
            public void onPracticeSkillSelected() {
                //For New Homework
                //MathFriendzyHelper.showWarningDialog(ActAssignHomework.this , "New");
                setSelectedType(texts[0]);
                setNewDataWhenUserSetNewType();
            }

            @Override
            public void onWordProblemSelected() {
                //setSelectedType(texts[1]);
                //for saved Homework
                showSavedHWScreen();
            }

            @Override
            public void onCustomeHomeworkSelected() {
                //MathFriendzyHelper.showWarningDialog(ActAssignHomework.this , "Under Development!!!");
                //setSelectedType(texts[2]);
                //For previous Homework
                showPreviousHWScreen();
            }
        } , texts[0] , texts[1] , texts[2]);
    }

    /**
     * Check for already selected type
     * @param selectedType
     * @return
     */
    private boolean isTypeAlreadySelected(String selectedType){
        if(txtTypeValues.getText().toString().equalsIgnoreCase(selectedType))
            return true;
        return false;
    }

    private void setSelectedType(String selectedType){
        txtTypeValues.setHint(selectedType);
    }

    /**
     * Reset the data when user select the new homework type
     */
    private void setNewDataWhenUserSetNewType(){
        //Reinitialize variable when grade changes
        selectedHWData = null;//Set null for the selected saved homework
            /*selectedClassList = null;
            edtDatePicker.setText("");
            txtSelectClassValue.setText("");*/

        selectedGrade = INITIAL_GRADE;
        mSubCatList = null;
        openSelectedCatListForWordProblem = null;
        workProblemList = null;
        mapChildList =  null;
        openSelectedCatList = null;
        practiceList = null;
        customeQuizList = null;
        //selectedStudentList = null;
        setAssignData();
    }

    private void showPreviousHWScreen(){
        MathFriendzyHelper.downloadAllGradeWordCategories(this , new HttpResponseInterface() {
            @Override
            public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                goToShowPreviousHW();
            }
        } , true);
    }

    private void showSavedHWScreen(){
        MathFriendzyHelper.downloadAllGradeWordCategories(this , new HttpResponseInterface() {
            @Override
            public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                goToShowSavedHW();
            }
        } , true);
    }

    private void goToShowSavedHW(){
        Intent intent = new Intent(this , ActSavedHomework.class);
        startActivityForResult(intent, SELECT_SAVED_HOMEWORK_REQUEST);
    }

    private void goToShowPreviousHW(){
        Intent intent = new Intent(this , ActPreviousAssignHomework.class);
        startActivityForResult(intent, SELECT_PREVIOUS_HOMEWORK_REQUEST);
    }

    /**
     * Save homework in local to assign later
     */
    private void saveHomework() {
            /*if(!this.isDueDateSelected()){
                MathFriendzyHelper.showWarningDialog(this , "Please select a Due Date.");
                return ;
            }*/

        ArrayList<ClassWithName> selectedClassList = this.getSelectedClasses();
        if(!(selectedClassList != null && selectedClassList.size() > 0)){
            MathFriendzyHelper.showWarningDialog(this , MathFriendzyHelper
                    .getTreanslationTextById(this , "lblPlsSelectClass"));
            return ;
        }

            /*if(!this.isAtleasetOneStudentIsSelected()){
                MathFriendzyHelper.showWarningDialog(this , MathFriendzyHelper
                        .getTreanslationTextById(this , "lblPlsSelectStudentForHW"));
                return ;
            }*/

        int code = this.isAnyCustomHwWithoutTitleOrAns();
        if(code == NO_TITLE_ENTER){
            MathFriendzyHelper.showWarningDialog(this ,
                    MathFriendzyHelper.getTreanslationTextById(this , "lblEnterTitleCustomHw"));
            return;
        }else if(code == NO_ANS_GIVEN){
            MathFriendzyHelper.showWarningDialog(this ,
                    MathFriendzyHelper.getTreanslationTextById(this , "lblPleaseFillAllAnsCustomHomework"));
            return ;
        }

        if(this.isNeedToShowDialog()){
            final AssignHomeworkParam param = new AssignHomeworkParam();
            //param.setAction("addHomeworkForStudents");
            param.setUserId(teacherId);
            param.setDate(MathFriendzyHelper.formatDataInYYYYMMDD
                    (edtDatePicker.getText().toString()));
            param.setGrade(selectedGrade + "");
            //param.setGrade(spinnerGrade.getSelectedItem().toString());
            param.setStudentJson(this.getSelectedStudentJsonString());
            param.setPracticeCatJson(this.getPracticeSkillJsonString());
            param.setWordCatJson(this.getWordProblemJsonString());
            param.setCustomeDataJson(this.getCustomeAnsJsonString());
            param.setMessage(txtTeacherMessage.getText().toString());
            param.setSelectedClasses(this.getCommaSeparatedClassIds());
            param.setShowDialog(true);
            if(selectedHWData != null){
                param.setHwId(selectedHWData.getSavedHwId());
            }else {
                param.setHwId(0);
            }

            MathFriendzyHelper.saveMathHomework(this , param , new HttpResponseInterface() {
                @Override
                public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                    AssignHomeworkResponse response = (AssignHomeworkResponse) httpResponseBase;
                    if(response.getResult().equals("success")){
                        if(param.getHwId() == 0) {
                            try {
                                String dataString = "save_" + response.getData();
                                PDFOperation.renamePdfFilesAfterGettingHWID(ActAssignHomework.this,
                                        dataString);
                                UploadPdf.uploadPdf(PDFOperation.getAllPdfPathStartWithGivenHWID
                                        (ActAssignHomework.this, dataString));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            setResult(RESULT_OK);
                        }
                        finish();
                    }
                    //finish();
                }
            });
        }else{
            MathFriendzyHelper.showWarningDialog(this ,
                    MathFriendzyHelper.getTreanslationTextById(ActAssignHomework.this , "lblAddASectionToHW"));
        }
    }

    private ArrayList<PracticeResult> getPracticeResults() {
        ArrayList<PracticeResult> practiceResults = new ArrayList<PracticeResult>();
        try{
            if(practiceList != null && practiceList.size() > 0){
                for(int key : practiceList.keySet()){
                    PracticeResult practiceResult = new PracticeResult();
                    practiceResult.setCatId(key + "");
                    practiceResult.setIntCatId(key);
                    ArrayList<LearningCenterTransferObj> catlist = practiceList.get(key);
                    ArrayList<PracticeResultSubCat> subCats = new ArrayList<PracticeResultSubCat>();
                    for(int i = 0 ; i < catlist.size() ; i ++ ){
                        PracticeResultSubCat subCat = new PracticeResultSubCat();
                        subCat.setSubCatId(catlist.get(i).getMathOperationCategoryId());
                        subCat.setProblems(catlist.get(i).getNumberOfProblemSelected());
                        subCats.add(subCat);
                    }
                    practiceResult.setPracticeResultSubCatList(subCats);
                    practiceResults.add(practiceResult);
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return practiceResults;
    }

    private ArrayList<WordResult> getWordResults() {
        ArrayList<WordResult> wordResults = new ArrayList<WordResult>();
        try{
            if(workProblemList != null && workProblemList.size() > 0){
                for(String key : workProblemList.keySet()){
                    WordResult wordResult = new WordResult();
                    ArrayList<SubCatergoryTransferObj> catlist = workProblemList.get(key);
                    ArrayList<WordSubCatResult> subCatResultList = new ArrayList<WordSubCatResult>();
                    for(int i = 0 ; i < catlist.size() ; i ++ ){
                        WordSubCatResult subCatResult = new WordSubCatResult();
                        wordResult.setCatIg(catlist.get(i).getMainCatId() + "");
                        subCatResult.setSubCatId(catlist.get(i).getId());
                        subCatResultList.add(subCatResult);
                    }
                    wordResult.setSubCatResultList(subCatResultList);
                    wordResults.add(wordResult);
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return wordResults;
    }


    private void initClasses(){
        classWithNames = this.getUserClasses();
        if(classWithNames != null && classWithNames.size() > 0) {
            for (int i = 0; i < classWithNames.size(); i++) {
                classWithNames.get(i).setSelected(false);
            }
        }
    }

    private void initClassesWithSelection(ArrayList<ClassWithName> selectedClasses){
        classWithNames = this.getUserClasses();
        if(selectedClasses != null && selectedClasses.size() > 0) {
            ArrayList<Integer> selectedClassIds = new ArrayList<Integer>();
            for (int i = 0; i < selectedClasses.size(); i++) {
                selectedClassIds.add(selectedClasses.get(i).getClassId());
            }

            for (int i = 0; i < classWithNames.size(); i++) {
                if (selectedClassIds.contains(classWithNames.get(i).getClassId())) {
                    classWithNames.get(i).setSelected(true);
                }else{
                    classWithNames.get(i).setSelected(false);
                }
            }
        }
    }

    private ArrayList<ClassWithName> getUserClasses(){
        try {
            if (registerUserFromPreff != null) {
                ArrayList<UserClasses> userClasseses = registerUserFromPreff.getUserClasses();
                ArrayList<ClassWithName> allGradeClasses = new ArrayList<ClassWithName>();
                for (int i = 0; i < userClasseses.size(); i++) {
                    UserClasses userClass = userClasseses.get(i);
                    allGradeClasses.addAll(userClass.getClassList());
                }
                return allGradeClasses;
            } else {
                return null;
            }
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    private void setTextToSelectedClassValues(String commaSeparatedClasses){
        txtSelectClassValue.setText(commaSeparatedClasses);
    }

    private void clickToSelectClass() {
        if(!(classWithNames != null && classWithNames.size() > 0)){
            MathFriendzyHelper.showWarningDialog(this ,
                    MathFriendzyHelper.getTreanslationTextById(this , "lblNoClass"));
            return ;
        }

        SelectAssignHomeworkClassParam param = new SelectAssignHomeworkClassParam();
        param.setShowForPrevSaveHomework(false);
        MathFriendzyHelper.showClassSelectionDialog(this , classWithNames , new ClassSelectedListener() {
            @Override
            public void onClassSelectionDone(ArrayList<ClassWithName> selectedClass) {
                selectedClassList = selectedClass;
                if(selectedClass != null && selectedClass.size() > 0){
                    setTextToSelectedClassValues(getCommaSeparatedClassName(selectedClass));
                }else{
                    setTextToSelectedClassValues("");
                }
                downloadStudentByClassIds(selectedClass);

                if(getSelectedClassSubjectId()
                        != MathFriendzyHelper.MATH_SUBJECT_ID){
                    removeNonMathSubjects();
                }
            }

            @Override
            public void onUpdatedClasses(ArrayList<ClassWithName> updatedList) {
                classWithNames = updatedList;
            }
        } , param);
    }

    /**
     * Remove the NonMath Subjects and reload the data list
     */
    private void removeNonMathSubjects(){
        mSubCatList = null;
        openSelectedCatListForWordProblem = null;
        workProblemList = null;
        mapChildList =  null;
        openSelectedCatList = null;
        practiceList = null;
        this.setAssignData();
    }

    /**
     * Return the selected class Ids
     * @return
     */
    private int getSelectedClassSubjectId(){
        if(isTeacherCreateAssignmentFromCheckHW){
            return selectedSubjectId;
        }
        try {
            if (this.selectedClassList != null
                    && this.selectedClassList.size() > 0) {
                return this.selectedClassList.get(0)
                        .getSubjectId();
            }
            return MathFriendzyHelper.MATH_SUBJECT_ID;
        }catch (Exception e){
            e.printStackTrace();
            return MathFriendzyHelper.MATH_SUBJECT_ID;
        }
    }

    private ArrayList<Integer> classIdsForWhichStudentDownloaded = new ArrayList<Integer>();
    private void downloadStudentByClassIds(ArrayList<ClassWithName> selectedClass){
        final ArrayList<Integer> classIdForWhichStudentNotDownloaded = new ArrayList<Integer>();
        for(int i = 0 ; i < selectedClass.size() ; i ++ ){
            if(!classIdsForWhichStudentDownloaded.contains(selectedClass.get(i).getClassId())){
                classIdForWhichStudentNotDownloaded.add(selectedClass.get(i).getClassId());
            }
        }

        if(classIdForWhichStudentNotDownloaded.size() > 0) {
            GetStudentForSchoolClassesParam param = new GetStudentForSchoolClassesParam();
            param.setDialogShow(true);
            param.setAction("getStudentsForSchoolClasses");
            param.setUserId(teacherId);
            param.setClasses(MathFriendzyHelper
                    .getCommaSeparatedStringForIntegerList(classIdForWhichStudentNotDownloaded, ","));

            MathFriendzyHelper.getStudentForSchoolClasses(this , param , new HttpResponseInterface() {
                @Override
                public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                    GetStudentByGradeResponse response = (GetStudentByGradeResponse) httpResponseBase;
                    ArrayList<GetStudentByGradeResponse> arrayList = response.getStudentList();
                    classIdsForWhichStudentDownloaded.addAll(classIdForWhichStudentNotDownloaded);
                    if(arrayList.size() > 0){
                        if(selectedStudentList == null) {
                            selectedStudentList = new ArrayList<GetStudentByGradeResponse>();
                            addNewStudentForAllSelecteion(arrayList);
                        }
                        selectedStudentList.addAll(arrayList);
                    }else{
                        MathFriendzyHelper.showWarningDialog(ActAssignHomework.this, MathFriendzyHelper
                                .getTreanslationTextById(ActAssignHomework.this , "lblNoStudentsForClasses"));
                    }
                }
            });
        }
    }

    private boolean isDueDateSelected() {
        return !MathFriendzyHelper.isEmpty(edtDatePicker.getText().toString());
    }

    private ArrayList<ClassWithName> getSelectedClasses(){
        return selectedClassList;
    }

    private ArrayList<GetStudentByGradeResponse> filterStudentListBasedOnSelectedClass
            (ArrayList<GetStudentByGradeResponse> arrayList){
        ArrayList<ClassWithName> selectedClassList = this.getSelectedClasses();
        if(selectedClassList != null && selectedClassList.size() > 0){
            ArrayList<GetStudentByGradeResponse> newFilerStudentList
                    = new ArrayList<GetStudentByGradeResponse>();
            if (arrayList != null && arrayList.size() > 0) {
                for (int i = 0 ; i < arrayList.size() ; i ++ ) {
                    if (this.ifStudentExistForSelectedClass(selectedClassList ,
                            arrayList.get(i).getClassId())){
                        newFilerStudentList.add(arrayList.get(i));
                    }
                }
            }
            return newFilerStudentList;
        }
        return null;
    }

    private String getCommaSeparatedClassName(ArrayList<ClassWithName> selectedClassList){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0 ; i < selectedClassList.size() ; i ++ ){
            stringBuilder.append(stringBuilder.length() > 0 ? "," + selectedClassList.get(i).getClassName()
                    : selectedClassList.get(i).getClassName());
        }
        return stringBuilder.toString();
    }

    private String getCommaSeparatedClassIds() {
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0 ; i < selectedClassList.size() ; i ++ ){
            stringBuilder.append(stringBuilder.length() > 0 ? "," + selectedClassList.get(i).getClassId()
                    : selectedClassList.get(i).getClassId());
        }
        return stringBuilder.toString();
    }

    /**
     * Add new student with name All for selection for All
     * @param arrayList
     */
    private void addNewStudentForAllSelecteion(ArrayList<GetStudentByGradeResponse> arrayList){
        GetStudentByGradeResponse allObject = new GetStudentByGradeResponse();
        allObject.setfName("All");
        allObject.setlName("");
        allObject.setParentUserId("");
        allObject.setProfileImageId("");
        allObject.setSelected(true);
        allObject.setClassId(-1);
        arrayList.add(0, allObject);
    }

    private final int NO_ANS_GIVEN = 1001;
    private final int NO_TITLE_ENTER = 1002;

    private int isAnyCustomHwWithoutTitleOrAns() {
        if(customeQuizList != null && customeQuizList.size() > 0){
            for(int i = 0 ; i < customeQuizList.size() ; i ++ ){
                String title = customeQuizList.get(i).getTitle();
                ArrayList<CustomeAns> customeAnsList = customeQuizList.get(i).getCustomeAnsList();
                if(MathFriendzyHelper.isEmpty(title)){
                    return NO_TITLE_ENTER;
                }
                if(!(customeAnsList != null
                        && customeAnsList.size() > 0)){
                    return NO_ANS_GIVEN;
                }
            }
        }
        return 0;
    }

    private void setSelectedSavedHomework(HomeworkData selectedHWData){
        this.initializeNewValuesFromSelectedHW(selectedHWData);
    }

    /**
     * Set the selected classes and download student for the selected classes
     * @param selectedClass
     */
    private void setClassesAndDownloadStudentForSavedHW(ArrayList<ClassWithName> selectedClass){
        if(selectedClass != null && selectedClass.size() > 0) {
            this.selectedClassList = selectedClass;
            if (selectedClass != null && selectedClass.size() > 0) {
                setTextToSelectedClassValues(getCommaSeparatedClassName(selectedClass));
            } else {
                setTextToSelectedClassValues("");
            }
            downloadStudentByClassIds(selectedClass);
        }
    }

    private void initializeNewValuesFromSelectedHW(HomeworkData selectedHWData){
        try{
            this.selectedGrade = MathFriendzyHelper.parseInt(selectedHWData.getGrade());
            //this.selectedStudentList = null;
            this.initClassesWithSelection(selectedHWData.getUserClasses());
            this.customeQuizList = selectedHWData.getCustomeQuizList();
            this.initializePracticeSkillData(selectedHWData.getPracticeResults());
            this.initializeWordProblemData(selectedHWData.getWordResults() , this.selectedGrade);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initializePracticeSkillData(ArrayList<PracticeResult> practiceResults) {
        mapChildList = new LinkedHashMap<String, ArrayList<LearningCenterTransferObj>>();
        ArrayList<LearningCenterTransferObj> laernignCenterFunctionsList =
                MathFriendzyHelper.getPracticeSkillCategories(this);
        ArrayList<LearningCenterTransferObj> laernignCenterFunctionsList1 = MathFriendzyHelper
                .getLearningCenterCategoryListWithUpdatedCategoryName
                        (laernignCenterFunctionsList, this);
        openSelectedCatList = new ArrayList<Boolean>();
        for(int i = 0 ; i < laernignCenterFunctionsList.size() ; i ++ ){
            openSelectedCatList.add(false);
        }

        for(int i = 0 ; i < laernignCenterFunctionsList.size() ; i ++ ){
            mapChildList.put(laernignCenterFunctionsList.get(i)
                    .getLearningCenterOperation(), this.getSubCatList((i + 1)
                    , laernignCenterFunctionsList1.get(i)
                    .getLearningCenterOperation() , practiceResults , i));
        }

        practiceList = new LinkedHashMap<Integer, ArrayList<LearningCenterTransferObj>>();
        for(String key : mapChildList.keySet()){
            ArrayList<LearningCenterTransferObj> learnList = mapChildList.get(key);
            for(int i = 0 ; i < learnList.size() ; i ++ ){
                if(learnList.get(i).isSelected()){

                    ArrayList<LearningCenterTransferObj> list = practiceList
                            .get(learnList.get(i).getLearningCenterMathOperationId());
                    if(list == null){
                        list = new ArrayList<LearningCenterTransferObj>();
                    }
                    //can't delete set cat name line
                    learnList.get(i).setLearningCenterOperation(key);//set cat name
                    list.add(learnList.get(i));
                    practiceList.put(learnList.get(i)
                            .getLearningCenterMathOperationId()
                            , list);

                }
            }
        }
    }

    private final int MAX_LENGTH   = 4;//this is for checking max length for string for multiplication
    /**
     * Return the sub cat list based on operation id
     * @param operationId
     * @return
     */
    private  ArrayList<LearningCenterTransferObj> getSubCatList(int operationId , String operationName
            , ArrayList<PracticeResult> practiceResults , int index){
        LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
        learningCenterObj.openConn();
        ArrayList<LearningCenterTransferObj> childCatList = learningCenterObj
                .getMathOperationCategoriesById(operationId);

        //if(operationId == 3){//for multiplication
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        ArrayList<LearningCenterTransferObj> tempList =
                new ArrayList<LearningCenterTransferObj>();
        for(int i = 0 ; i < childCatList.size() ; i ++ ){
            if(childCatList.get(i).getMathOperationCategory().length() > MAX_LENGTH){
                tempList.add(childCatList.get(i));
                childCatList.get(i).setTotalProblem
                        (learningCenterObj.getTotalProblemInCat
                                (childCatList.get(i).getMathOperationCategoryId()));
                childCatList.get(i).setLearningCenterMathOperationId(operationId);
                childCatList.get(i).setMathOperationCategory
                        (transeletion.getTranselationTextByTextIdentifier
                                (childCatList.get(i).getMathOperationCategory()));//update sub cat name
                PracticeResultSubCat subSelectedCatData = this.getSelectedSubCategoryData(childCatList.get(i).getMathOperationCategoryId()
                        , operationId, practiceResults);
                if(subSelectedCatData != null ){
                    openSelectedCatList.set(index , true);
                    childCatList.get(i).setSelected(true);
                    childCatList.get(i).setNumberOfProblemSelected(subSelectedCatData.getProblems());
                }else {
                    childCatList.get(i).setSelected(false);
                    childCatList.get(i).setNumberOfProblemSelected(10);
                }
            }
        }
        transeletion.closeConnection();
        learningCenterObj.closeConn();
        return tempList;
    }

    private PracticeResultSubCat getSelectedSubCategoryData(int subCatId ,
                                                            int operationId, ArrayList<PracticeResult> practiceResults){
        for(int i = 0 ; i < practiceResults.size() ; i ++ ){
            if(operationId == practiceResults.get(i).getIntCatId()){
                ArrayList<PracticeResultSubCat> practiceResultSubCatList =
                        practiceResults.get(i).getPracticeResultSubCatList();
                for(int j = 0 ; j < practiceResultSubCatList.size() ; j ++ ){
                    if(subCatId == practiceResultSubCatList.get(j).getSubCatId()){
                        return practiceResultSubCatList.get(j);
                    }
                }
            }
        }
        return null;
    }

    private void initializeWordProblemData(ArrayList<WordResult> wordResults , int selectedGrade) {
        ArrayList<CategoryListTransferObj> categoryList = MathFriendzyHelper.getCategories
                (selectedGrade, this);
        mSubCatList = new LinkedHashMap<String, ArrayList<SubCatergoryTransferObj>>();
        openSelectedCatListForWordProblem = new ArrayList<Boolean>();
        for(int i = 0 ; i < categoryList.size() ; i ++ ){
            openSelectedCatListForWordProblem.add(false);
        }
        for(int i = 0 ; i < categoryList.size() ; i ++ ){
            ArrayList<SubCatergoryTransferObj> subCatList = MathFriendzyHelper.
                    getWorkSubCatListByCatId(this, categoryList.get(i).getCid());
            for(int j = 0 ; j < subCatList.size() ; j ++ ){
                WordSubCatResult subCatResult = this.getSelectedSubCategoryForWord(subCatList.get(j).getId() ,
                        categoryList.get(i).getCid() , wordResults);
                if(subCatResult != null){
                    openSelectedCatListForWordProblem.set(i , true);
                    subCatList.get(j).setSelected(true);
                    subCatList.get(j).setNumberOfQuestions(10);
                }else{
                    subCatList.get(j).setSelected(false);
                    subCatList.get(j).setNumberOfQuestions(10);
                }
            }
            mSubCatList.put(categoryList.get(i).getName(), subCatList);
        }

        workProblemList = new LinkedHashMap<String, ArrayList<SubCatergoryTransferObj>>();
        for(String key : mSubCatList.keySet()){
            ArrayList<SubCatergoryTransferObj> subCat = mSubCatList.get(key);
            for(int i = 0 ; i < subCat.size() ; i ++ ){
                if(subCat.get(i).isSelected()){

                    ArrayList<SubCatergoryTransferObj> list = workProblemList.get(key);
                    if(list == null){
                        list = new ArrayList<SubCatergoryTransferObj>();
                    }
                    list.add(subCat.get(i));
                    workProblemList.put(key, list);
                }
            }
        }
    }

    private WordSubCatResult getSelectedSubCategoryForWord(int subCatId ,
                                                           int catId, ArrayList<WordResult> wordResults){
        for(int i = 0 ; i < wordResults.size() ; i ++ ){
            if(catId == MathFriendzyHelper.parseInt(wordResults.get(i).getCatIg())){
                ArrayList<WordSubCatResult> subCatResultList =
                        wordResults.get(i).getSubCatResultList();
                for(int j = 0 ; j < subCatResultList.size() ; j ++ ){
                    if(subCatId == subCatResultList.get(j).getSubCatId()){
                        return subCatResultList.get(j);
                    }
                }
            }
        }
        return null;
    }

    private void setWidgetsDataFromSelectedHW(HomeworkData selectedHWData){
        try{
            edtDatePicker.setText(MathFriendzyHelper.formatDataInGivenFormat(selectedHWData.getDate() ,
                    "yyyy-MM-dd" , "MM-dd-yyyy"));
            txtTeacherMessage.setText(selectedHWData.getMessage());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    //new HW changes end

    //below class title changes
    private void setClassTitle(int selectedGrade){
        this.setVisibilityOfTitleListLayout(false);
        ArrayList<ClassWithName> classWithNames = this.getClassTitleList(selectedGrade);
        this.clearDataWhenUserSelectOtherGrade(classWithNames);
        if(classWithNames != null && classWithNames.size() > 0){
            this.setVisibilityOfClassesLayout(true);
            this.setClassListAdapter(classWithNames);
        }else{
            this.setVisibilityOfClassesLayout(false);
        }
    }

    private void clearDataWhenUserSelectOtherGrade
            (ArrayList<ClassWithName> classWithNames){
        adapter = null;
        txtSelectClassValue.setText("");
        if(classWithNames != null && classWithNames.size() > 0) {
            for (int i = 0; i < classWithNames.size(); i++) {
                classWithNames.get(i).setSelected(false);
            }
        }
    }

    private ArrayList<ClassWithName> getClassTitleList(int grade){
        if(registerUserFromPreff != null){
            ArrayList<UserClasses> userClasseses = registerUserFromPreff.getUserClasses();
            for (int i = 0 ; i < userClasseses.size() ; i ++ ){
                UserClasses userClass = userClasseses.get(i);
                if(userClass.getGrade() == grade){
                    return userClass.getClassList();
                }
            }
            return null;
        }else{
            return null;
        }
    }

    private void setVisibilityOfClassesLayout(boolean isVisible){
        if(isVisible){
            selectClassLayout.setVisibility(RelativeLayout.VISIBLE);
        }else{
            selectClassLayout.setVisibility(RelativeLayout.GONE);
        }
    }

    private void setVisibilityOfTitleListLayout(boolean isVisible){
        if(isVisible){
            titleListLayout.setVisibility(RelativeLayout.VISIBLE);
            lstClassesList.setVisibility(ListView.VISIBLE);
        }else{
            titleListLayout.setVisibility(RelativeLayout.GONE);
            lstClassesList.setVisibility(ListView.GONE);
        }
    }

    private void clickToClassSelectionDone() {
        txtSelectClassValue.setText("");
        this.setVisibilityOfTitleListLayout(false);
        ArrayList<ClassWithName> selectedClassList = this.getSelectedClasses();
        if(selectedClassList != null && selectedClassList.size() > 0) {
            txtSelectClassValue.setText(this.getCommaSeparatedClassName(selectedClassList));
        }
    }

       /* private ArrayList<ClassWithName> getSelectedClasses(){
            if(adapter != null && adapter.getSelectedList().size() > 0){
                return adapter.getSelectedList();
            }
            return null;
        }*/

    private void setClassListAdapter(ArrayList<ClassWithName> classWithNames){
        adapter = new ClassesAdapter(this , classWithNames);
        lstClassesList.setAdapter(adapter);
    }

    private boolean ifStudentExistForSelectedClass(ArrayList<ClassWithName> selectedClassList , int classId){
        if(selectedClassList != null && selectedClassList.size() > 0){
            for(int i = 0 ; i < selectedClassList.size() ; i ++ ){
                if(selectedClassList.get(i).getClassId() == classId){
                    return true;
                }
            }
        }
        return false;
    }
    //end class title changes


    //edit homework changes
    private void setDataAndVisibilityOfViewForEditHW() {
        if(isTeacherEditHomework){
            pickerLayout.setVisibility(View.GONE);
            btnAddHomeWorkQuizzes.setVisibility(View.GONE);
            btnSave.setVisibility(View.INVISIBLE);
            btnAssignHomework.setText(MathFriendzyHelper.getTreanslationTextById(this , "btnTitleSave"));
            this.setHomeworkDataToEditHomework(editTeacherHWData);
        }
    }

    /**
     * Set Homework data to edit hoemwork
     * @param selectedHWData
     */
    private void setHomeworkDataToEditHomework(final HomeworkData selectedHWData) {
        if (selectedHWData != null) {
            new AsyncTask<Void, Void, Void>() {
                private ProgressDialog pd = null;

                @Override
                protected void onPreExecute() {
                    pd = MathFriendzyHelper.getProgressDialog(ActAssignHomework.this, "");
                    MathFriendzyHelper.showProgressDialog(pd);
                    super.onPreExecute();
                }

                @Override
                protected Void doInBackground(Void... params) {
                    setSelectedSavedHomework(selectedHWData);
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    MathFriendzyHelper.hideDialog(pd);
                    setWidgetsDataFromSelectedHW(selectedHWData);
                    setClassesAndDownloadStudentForSavedHW(selectedHWData.getUserClasses());
                    setAssignData();
                    super.onPostExecute(aVoid);
                }
            }.execute();
        }
    }

    /**
     * Set visibility of view when teacher create assignment from chech homework
     */
    private void setVisibilityOfViewForTeacherCreateAssignmentFromCheckHW() {
        if(isTeacherCreateAssignmentFromCheckHW) {
            gradeLayout.setVisibility(View.INVISIBLE);
        }
    }
}
