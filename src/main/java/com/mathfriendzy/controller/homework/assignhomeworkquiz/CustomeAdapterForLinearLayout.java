package com.mathfriendzy.controller.homework.assignhomeworkquiz;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.homework.CustomeResult;
import com.mathfriendzy.model.homework.HomeworkData;
import com.mathfriendzy.model.homework.PracticeResult;
import com.mathfriendzy.model.homework.PracticeResultSubCat;
import com.mathfriendzy.model.homework.WordResult;
import com.mathfriendzy.model.homework.WordSubCatResult;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;

import java.util.ArrayList;

/**
 * Created by root on 10/6/16.
 */
public class CustomeAdapterForLinearLayout {

    private ArrayList<HomeworkData> homeworkList = null;
    private boolean isSavedHW;
    private static ViewHolder vHolder = null;
    private LayoutInflater mInflator = null;
    private Context context = null;
    private String customeProblemtext = null;
    private String practiceSkillText = null;
    private String wordProblemtext = null;
    private String txtDateCreates = null;
    private String CURRENT_DATE_FORMATE = "yyyy-MM-dd HH:mm:ss";
    private String REQUIRED_DATE_FORMATE =  "MMM dd, yyyy";
    private SavedHomeworkSelectListener listener = null;

    private ArrayList<LearningCenterTransferObj> laernignCenterFunctionsList = null;
    private ArrayList<String> updatedCatName = null;

    private LinearLayout layout = null;
    private int startingIndex = 0;

    public CustomeAdapterForLinearLayout(Context context , ArrayList<HomeworkData> homeworkList ,
                                         boolean isSavedHW
            , SavedHomeworkSelectListener listener , LinearLayout layout){
        this.context = context;
        this.homeworkList = homeworkList;
        this.isSavedHW = isSavedHW;
        mInflator = LayoutInflater.from(context);

        String text[] = MathFriendzyHelper.getTreanslationTextById(context,
                "lblSolveEquations", "lblWordProblem", "lblClass", "lblWork", "lblQuizzes");
        practiceSkillText = text[0];
        wordProblemtext = text[1];
        customeProblemtext = text[2] + " " + text[3] + "/" + text[4];
        if(isSavedHW) {
            txtDateCreates = "Date Created:";
        }else{
            txtDateCreates = "Assigned Date:";
        }
        this.listener = listener;


        laernignCenterFunctionsList = MathFriendzyHelper.getPracticeSkillCategories(this.context);
        updatedCatName = MathFriendzyHelper
                .getUpdatedPracticeSkillCatNameList(laernignCenterFunctionsList, this.context);

        this.layout = layout;
    }

    public void setInflatedViews(){
        for(int i = startingIndex ; i < this.homeworkList.size() ; i ++ ){
            final int selectedIndex = i;
            View view = mInflator.inflate(R.layout.homework_item_layout , null);
            vHolder = new ViewHolder();
            vHolder.txtDate = (TextView) view.findViewById(R.id.txtDate);
            vHolder.btnSelect = (Button) view.findViewById(R.id.btnSelect);
            vHolder.txtNo = (TextView) view.findViewById(R.id.txtNo);
            vHolder.homeWorkQuizzLayout = (LinearLayout) view.findViewById(R.id.homeWorkQuizzLayout);

            vHolder.txtDate.setText(txtDateCreates + " " +
                    MathFriendzyHelper.formatDataInGivenFormat(this.homeworkList.get(i).getCreationDate() ,
                            CURRENT_DATE_FORMATE , REQUIRED_DATE_FORMATE));
            setAssignData(vHolder.homeWorkQuizzLayout , homeworkList.get(i));

            vHolder.btnSelect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        listener.onSelectListener(selectedIndex , homeworkList.get(selectedIndex));
                    }
                }
            });
            this.layout.addView(view);
        }
    }

    private static class ViewHolder{
        private TextView txtDate;
        private Button btnSelect;
        private TextView txtNo;
        private LinearLayout homeWorkQuizzLayout;
    }


    /**
     * Set the assign homework data into the list layout
     */
    private void setAssignData(LinearLayout homeWorkQuizzLayout , HomeworkData homeworkData){
        try{
            homeWorkQuizzLayout.removeAllViews();
            ArrayList<CustomeResult> customeQuizList = homeworkData.getCustomeQuizList();
            //LinkedHashMap<Integer, ArrayList<LearningCenterTransferObj>> practiceList = homeworkData.getPracticeList();
            //LinkedHashMap<String, ArrayList<SubCatergoryTransferObj>> workProblemList = homeworkData.getWorkProblemList();
            ArrayList<PracticeResult> practiceList = homeworkData.getPracticeResults();
            ArrayList<WordResult> workProblemList = homeworkData.getWordResults();
            if(customeQuizList != null && customeQuizList.size() > 0){
                homeWorkQuizzLayout.addView
                        (this.addCustomeProblemData(customeQuizList));
            }
            if(practiceList != null && practiceList.size() > 0){
                homeWorkQuizzLayout.addView
                        (this.addPracticeSkillData(practiceList));
            }
            if(workProblemList != null && workProblemList.size() > 0){
                homeWorkQuizzLayout.addView
                        (this.addWordProblemData(workProblemList));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * This method add the practice layout data into the main layout to display
     * @param
     */
    /*private LinearLayout addPracticeSkillData(LinkedHashMap<Integer, ArrayList<LearningCenterTransferObj>> practiceList){
        LinearLayout layout = this.getLinearLayout();
        layout.addView(this.getInflatedViewForQuizzQuestionType(1));
        layout.addView(this.addPracticeSkillLayout(practiceList));
        return layout;
    }*/

    /**
     * This method add the practice layout data into the main layout to display
     * @param
     */
    private LinearLayout addPracticeSkillData(ArrayList<PracticeResult> practiceList){
        LinearLayout layout = this.getLinearLayout();
        layout.addView(this.getInflatedViewForQuizzQuestionType(1));
        layout.addView(this.addPracticeSkillLayout(practiceList));
        return layout;
    }


    /**
     * Add word problem layout
     * @return
     */
    /*private LinearLayout addWordProblemData(LinkedHashMap<String, ArrayList<SubCatergoryTransferObj>> workProblemList){
        LinearLayout layout = this.getLinearLayout();
        layout.addView(this.getInflatedViewForQuizzQuestionType(2));
        layout.addView(this.addWordProblemLayout(workProblemList));
        return layout;
    }*/

    private LinearLayout addWordProblemData(ArrayList<WordResult> workProblemList){
        LinearLayout layout = this.getLinearLayout();
        layout.addView(this.getInflatedViewForQuizzQuestionType(2));
        layout.addView(this.addWordProblemLayout(workProblemList));
        return layout;
    }

    /**
     * Add custom problem layout
     * @return
     */
    private LinearLayout addCustomeProblemData(ArrayList<CustomeResult> customeQuizList){
        LinearLayout layout = this.getLinearLayout();
        layout.addView(this.getInflatedViewForQuizzQuestionType(3));
        layout.addView(this.addCustomProblemLayout(customeQuizList));
        return layout;
    }

    /**
     * This method create and return a linear layout
     * @return
     */
    private LinearLayout getLinearLayout(){
        LinearLayout layout = new LinearLayout(this.context);
        layout.setOrientation(LinearLayout.VERTICAL);
        return layout;
    }

    /**
     * Return the view with the selection type text
     * @param inflateFor
     * @return
     */
    private View getInflatedViewForQuizzQuestionType(int inflateFor){

        View view = LayoutInflater.from(this.context)
                .inflate(R.layout.homework_quizz_question_type_new, null); // some changes by siddhiinfosoft
        TextView txtView = (TextView)view.findViewById(R.id.txtQuestionType);
        TextView txtNo   = (TextView)view.findViewById(R.id.txtNo);
        TextView txtScore   = (TextView)view.findViewById(R.id.txtScore);
        /*txtNo.setVisibility(TextView.VISIBLE);
        txtScore.setVisibility(TextView.VISIBLE);*/

        txtScore.setText("");
        txtScore.setBackgroundResource(R.drawable.arrow);

        if(inflateFor == 1){//for practice skill
            txtView.setText(practiceSkillText);

            txtScore.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    //onPracticeSkillCliked();
                }
            });
        }else if(inflateFor == 2){//for word problem
            txtView.setText(wordProblemtext);

            txtScore.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    //onWorkProblemClicked();
                }
            });
        }else if(inflateFor == 3){//for custom
            txtView.setText(customeProblemtext);

            txtScore.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    //onCustomeHomeworkClicked();
                }
            });
        }
        return view;
    }

    /**
     * This method add the practice skill layout with cat and subCat
     * @param arrayList
     * @return
     */
    private LinearLayout addPracticeSkillLayout(final ArrayList<PracticeResult> arrayList){

        LinearLayout layout = this.getLinearLayout();

        for(int i = 0 ; i < arrayList.size() ; i ++ ){
            for(int j = 0 ; j < laernignCenterFunctionsList.size() ; j ++){
                if(arrayList.get(i).getCatId().equals
                        (laernignCenterFunctionsList.get(j)
                                .getLearningCenterMathOperationId() + "")){
                    View view = this.getCategoryInflatedLayout(1);
                    ImageView imgSign = (ImageView) view.findViewById(R.id.imgSign);
                    TextView  txtCatName = (TextView) view.findViewById(R.id.txtCategoryName);
                    imgSign.setBackgroundResource(this.context.getResources().getIdentifier
                            ("mf_" + laernignCenterFunctionsList.get(j)
                                            .getLearningCenterOperation()
                                            .toLowerCase().replace(" ","_")+"_sign", "drawable",
                                    this.context.getPackageName()));
                    txtCatName.setText(updatedCatName.get(j));
                    layout.addView(view);

                    final ArrayList<PracticeResultSubCat> practiceResultSubCatList =
                            arrayList.get(i).getPracticeResultSubCatList();

                    for(int k = 0 ; k < practiceResultSubCatList.size() ; k ++ ){
                        View subCatView = this.getSubCategoryInflatedLayout(1);
                        TextView txtSubCatName = (TextView) subCatView.findViewById(R.id.txtSubCategoryName);
                        TextView txtNoOfQuestions = (TextView) subCatView.findViewById(R.id.txtNo);
                        TextView txtScore = (TextView) subCatView.findViewById(R.id.txtScore);
                        RelativeLayout rlArrowLayout = (RelativeLayout) subCatView.findViewById(R.id.rlArrowLayout);
                        txtSubCatName.setText(this.getPracticeSkillSubCatName
                                (arrayList.get(i).getCatId(), practiceResultSubCatList.get(k).getSubCatId() + ""));
                        txtNoOfQuestions.setText(practiceResultSubCatList.get(k).getProblems() + "");
                        //txtScore.setText(practiceResultSubCatList.get(k).getScore() + "%");
                        txtScore.setVisibility(TextView.INVISIBLE);
                        rlArrowLayout.setVisibility(RelativeLayout.INVISIBLE);
                        layout.addView(subCatView);
                        practiceResultSubCatList.get(k).setCatId(arrayList.get(i).getCatId());
                    }
                    break;
                }
            }
        }

        return layout;
    }

    /**
     * Add work problem layout
     */
    /**
     * This method add the word problem layout with Cat and SubCat
     * @param arrayList
     * @return
     */
    private LinearLayout addWordProblemLayout(ArrayList<WordResult> arrayList){
        LinearLayout layout = this.getLinearLayout();

        for(int i = 0 ; i < arrayList.size() ; i ++ ){
            View view = this.getCategoryInflatedLayout(2);
            ImageView imgSign = (ImageView) view.findViewById(R.id.imgSign);
            TextView  txtCatName = (TextView) view.findViewById(R.id.txtCategoryName);
            txtCatName.setText(MathFriendzyHelper
                    .getSchoolCurriculumCategoryNameByCatId
                            (this.context, arrayList.get(i).getCatIg()));
            imgSign.setVisibility(ImageView.GONE);
            layout.addView(view);

            ArrayList<WordSubCatResult> subCatList = arrayList.get(i).getSubCatResultList();
            for(int j = 0 ; j < subCatList.size() ; j ++ ){
                View subCatView = this.getSubCategoryInflatedLayout(2);
                TextView txtSubCatName = (TextView) subCatView.findViewById(R.id.txtSubCategoryName);
                TextView txtNoOfQuestions = (TextView) subCatView.findViewById(R.id.txtNo);
                TextView txtScore = (TextView) subCatView.findViewById(R.id.txtScore);
                RelativeLayout rlArrowLayout = (RelativeLayout) subCatView.findViewById(R.id.rlArrowLayout);
                txtSubCatName.setText(MathFriendzyHelper.
                        getSchoolCurriculumSubCatNameByCatIdAndSubCatId
                                (this.context, arrayList.get(i).getCatIg(), subCatList.get(j).getSubCatId() + ""));
                txtNoOfQuestions.setText("10");
                //txtScore.setText(subCatList.get(j).getScore() + "%");
                txtScore.setVisibility(TextView.INVISIBLE);
                rlArrowLayout.setVisibility(RelativeLayout.INVISIBLE);
                layout.addView(subCatView);

                subCatList.get(j).setCatId(arrayList.get(i).getCatIg());
            }
        }
        return layout;
    }

    /**
     * Add Custom Problem layout
     * @return
     */
    private LinearLayout addCustomProblemLayout(ArrayList<CustomeResult> customeQuizList){
        LinearLayout layout = this.getLinearLayout();
        for(int i = 0 ; i < customeQuizList.size() ; i ++ ){
            View view = this.getSubCategoryInflatedLayout(1);
            TextView txtSubCatName = (TextView) view.findViewById(R.id.txtSubCategoryName);
            TextView txtNoOfQuestions = (TextView) view.findViewById(R.id.txtNo);
            TextView txtScore = (TextView) view.findViewById(R.id.txtScore);
            ImageView imgArrow = (ImageView) view.findViewById(R.id.imgArrow);

            imgArrow.setVisibility(ImageView.INVISIBLE);
            txtScore.setVisibility(TextView.INVISIBLE);
            txtSubCatName.setText(customeQuizList.get(i).getTitle());
            txtNoOfQuestions.setText(customeQuizList.get(i).getCustomeAnsList().size() + "");
            layout.addView(view);
        }
        return layout;
    }

    @SuppressLint("InflateParams")
    private View getCategoryInflatedLayout(int inflateFor){
        View view = LayoutInflater.from(this.context)
                .inflate(R.layout.school_home_question_categories_layout_new, null);
        return view;
    }

    @SuppressLint("InflateParams")
    private View getSubCategoryInflatedLayout(int inflateFor){
        View view = LayoutInflater.from(this.context)
                .inflate(R.layout.home_work_quizz_sub_category_layout_new, null);
        return view;
    }

    /**
     * Get practice skill subCat Name
     * @param catId
     * @param subCatId
     * @return
     */
    private String getPracticeSkillSubCatName(String catId , String subCatId){

        String subCatName = null;
        LearningCenterimpl laerningCenter = new LearningCenterimpl(this.context);
        laerningCenter.openConn();
        subCatName = laerningCenter.
                getMathOperationCategoriesByCatIdAndSubCatId(catId,subCatId)
                .getMathOperationCategory();
        laerningCenter.closeConn();

        Translation transeletion = new Translation(this.context);
        transeletion.openConnection();
        subCatName = transeletion.getTranselationTextByTextIdentifier(subCatName);
        transeletion.closeConnection();

        return subCatName;
    }


    /**
     * Add more record when user tab on show more
     * @param homeworkDataList
     */
    public void addMoreRecordAndUpdate(ArrayList<HomeworkData> homeworkDataList){
        startingIndex = this.homeworkList.size();
        this.homeworkList.addAll(homeworkDataList);
        this.setInflatedViews();
    }
}
