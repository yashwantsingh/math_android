package com.mathfriendzy.controller.homework.assignhomeworkquiz;

import com.mathfriendzy.model.registration.classes.ClassWithName;

/**
 * Created by root on 9/6/16.
 */
public interface AdapterListenerForNewAssignHW {
    void onSingleSelectionDone(ClassWithName selectedClass);
}
