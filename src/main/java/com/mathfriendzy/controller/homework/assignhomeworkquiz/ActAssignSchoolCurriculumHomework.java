package com.mathfriendzy.controller.homework.assignhomeworkquiz;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.Spinner;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.learningcenter.schoolcurriculum.SeeAnswerActivityForSchoolCurriculum;
import com.mathfriendzy.controller.learningcenter.showbyccss.ShowByCCSSMainStandards;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.assessmenttest.CatagoriesDto;
import com.mathfriendzy.model.grade.Grade;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.CategoryListTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.MathEquationTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SubCatergoryTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.WordProblemQuestionTransferObj;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.utils.CommonUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class ActAssignSchoolCurriculumHomework extends ActBase {

    private final String TAG = this.getClass().getSimpleName();

    private TextView txtSchoolCurriculum = null;
    private TextView txtSelectecOneOrMoreCategories = null;
    private TextView txtCategory = null;
    private TextView txtShowByCCSS = null;
    private ExpandableListView lstCatgoryList = null;

    private String selectedStudentGrade = "1";
    private ArrayList<CategoryListTransferObj> categoryList = null;
    private LinkedHashMap<String, ArrayList<SubCatergoryTransferObj>> mSubCatList = null;
    private ArrayList<Boolean> openSelectedCatListForWordProblem = null;

    private WordProblemExpAdapter adapter = null;
    private final int SHOW_BY_CCSS_REQUEST = 1001;

    private int stdId = 0;
    private int subStdId = 0;

    //for show by CCSS
    ArrayList<CatagoriesDto> categoryInfoList = null;

    //for grade field added for new assign homework changes
    private Spinner spinnerGrade = null;
    private TextView txtGrade = null;
    private boolean isInitialDataSet = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_assign_school_curriculum_homework);

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside onCreate()");

        this.getIntentValues();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.setInitialData();
        //this.getWordCatAndSubCat(selectedStudentGrade);

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside onCreate()");
    }

    private void setInitialData(){
        this.getGrade(selectedStudentGrade + "");
    }


    /**
     * Get the previous selected intent values extras
     */
    @SuppressWarnings("unchecked")
    private void getIntentValues(){
        selectedStudentGrade = this.getIntent().getStringExtra("selectedStudentGrade");
        mSubCatList = this.getLinkedHashMapFromGsonJsonStringForWorkProblem
                (this.getIntent().getStringExtra("mSubCatList"));
        openSelectedCatListForWordProblem = (ArrayList<Boolean>) this.getIntent().
                getSerializableExtra("openSelectedCatListForWordProblem");
        categoryInfoList = (ArrayList<CatagoriesDto>) this.getIntent()
                .getSerializableExtra("categoryInfoList");
    }

    @Override
    protected void setWidgetsReferences() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside setWidgetsReferences()");

        txtTopbar 	= (TextView) findViewById(R.id.txtTopbar);
        txtSchoolCurriculum = (TextView) findViewById(R.id.txtSchoolCurriculum);
        txtSelectecOneOrMoreCategories = (TextView) findViewById(R.id.txtSelectecOneOrMoreCategories);
        txtCategory = (TextView) findViewById(R.id.txtCategory);
        txtShowByCCSS = (TextView) findViewById(R.id.txtShowByCCSS);
        lstCatgoryList = (ExpandableListView) findViewById(R.id.lstCatgoryList);

        //new assign homework changes
        spinnerGrade = (Spinner) findViewById(R.id.spinnerGrade);
        txtGrade = (TextView) findViewById(R.id.txtGrade);

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside setWidgetsReferences()");

    }

    @Override
    protected void setListenerOnWidgets() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside setListenerOnWidgets()");

        txtShowByCCSS.setOnClickListener(this);

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside setListenerOnWidgets()");

    }

    @Override
    protected void setTextFromTranslation() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside setTextFromTranslation()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblAssignHomework"));
        txtSchoolCurriculum.setText(transeletion.getTranselationTextByTextIdentifier("lblSchoolsCurriculum"));
        txtSelectecOneOrMoreCategories.setText(transeletion.getTranselationTextByTextIdentifier("lblSelectOneOrMoreWordCategory"));
        txtCategory.setText(transeletion.getTranselationTextByTextIdentifier("mfLblCategorySettings"));
        txtShowByCCSS.setText(transeletion.getTranselationTextByTextIdentifier("lblShowByCcss"));
        txtGrade.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade"));
        transeletion.closeConnection();

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside setTextFromTranslation()");

    }

    /**
     * Get cat and sub cat by grade
     * @param grade
     */
    private void getWordCatAndSubCat(String grade){
        if(categoryList == null){
            categoryList = MathFriendzyHelper.getCategories
                    (Integer.parseInt(grade), this);

        }
        if(mSubCatList == null){
            mSubCatList = new LinkedHashMap<String, ArrayList<SubCatergoryTransferObj>>();
            for(int i = 0 ; i < categoryList.size() ; i ++ ){
                mSubCatList.put(categoryList.get(i).getName(), MathFriendzyHelper.
                        getWorkSubCatListByCatId(this, categoryList.get(i).getCid()));
            }
        }
        this.setExpandableAdapter();
    }

    /**
     * Set the word problem expandable adapter to list
     */
    private void setExpandableAdapter(){
        adapter = new WordProblemExpAdapter(this, categoryList, mSubCatList , categoryInfoList ,
                new WordProblemExxListener() {
                    @Override
                    public void onShowQuestionDetail(SubCatergoryTransferObj subCat) {
                        clickToShowQuestionDetail(subCat);
                    }
                });
        lstCatgoryList.setAdapter(adapter);

        if(openSelectedCatListForWordProblem != null){
            for(int i = 0 ; i < categoryList.size() ; i ++ ){
                if(openSelectedCatListForWordProblem.get(i).booleanValue()){
                    lstCatgoryList.expandGroup(i);
                }
            }
        }else{
            openSelectedCatListForWordProblem = new ArrayList<Boolean>();
            for(int i = 0 ; i < categoryList.size() ; i ++ ){
                openSelectedCatListForWordProblem.add(false);
            }
        }

        lstCatgoryList.setOnGroupExpandListener(new OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                openSelectedCatListForWordProblem.set(groupPosition , true);
            }
        });

        lstCatgoryList.setOnGroupCollapseListener(new OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                openSelectedCatListForWordProblem.set(groupPosition , false);
            }
        });
    }

    private void clickToShowQuestionDetail(SubCatergoryTransferObj subCat){
        ArrayList<WordProblemQuestionTransferObj> questionAnswerList =
                MathFriendzyHelper.getWordProblemQuestions(this, subCat.getMainCatId(), subCat.getId());
        ArrayList<MathEquationTransferObj> playDataList = new ArrayList<MathEquationTransferObj>();
        for(int i = 0 ; i < questionAnswerList.size() ; i ++ ){
            MathEquationTransferObj equationObj = new MathEquationTransferObj();
            equationObj.setQuestionObj(questionAnswerList.get(i));
            equationObj.setIsAnswerCorrect(1);
            equationObj.setUserAnswer(questionAnswerList.get(i).getAns());
            playDataList.add(equationObj);
        }
        Intent intent = new Intent(this , SeeAnswerActivityForSchoolCurriculum.class);
        intent.putExtra("playDatalist", playDataList);
        intent.putExtra("isFromLearnignCenterSchoolCurriculum", true);
        intent.putExtra("isForAssignHomeworkShowQuestionDetail", true);
        intent.putExtra("selectedGrade", Integer.parseInt(selectedStudentGrade));
        startActivity(intent);
    }

    /**
     * Show by CCSS
     */
    private void clickOnShowBuCCSS(){
        if(MathFriendzyHelper.isShowByCCSSDownLoaded(this, Integer.parseInt(selectedStudentGrade))){
            this.goToCCSS();
        }else{
            MathFriendzyHelper.downloadCCSSStandards(this , new HttpServerRequest() {

                @Override
                public void onRequestComplete() {
                    goToCCSS();
                }
            });
        }
    }

    /**
     * Go to the show by CCSS
     */
    private void goToCCSS(){
        Intent intent = new Intent(this , ShowByCCSSMainStandards.class);
        intent.putExtra("selectedGrade", Integer.parseInt(selectedStudentGrade));
        intent.putExtra("callingAct", 1);
        startActivityForResult(intent , SHOW_BY_CCSS_REQUEST);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.txtShowByCCSS:
                this.clickOnShowBuCCSS();
                break;
        }

    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase,
                               int requestCode) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        try {
            intent.putExtra("openSelectedCatListForWordProblem", openSelectedCatListForWordProblem);
            intent.putExtra("categoryInfoList", categoryInfoList);
            intent.putExtra("selectedGrade", MathFriendzyHelper.parseInt(selectedStudentGrade));
            intent.putExtra("mSubCatList", this
                    .getGsonStringForLinkedHaskMapForWordProblem(adapter.mSubCatList));
        }catch (Exception e){
            e.printStackTrace();
        }
        setResult(RESULT_OK, intent);
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            switch(requestCode){
                case SHOW_BY_CCSS_REQUEST:
                    stdId = data.getIntExtra("stdId", 0);
                    subStdId = data.getIntExtra("subStdId", 0);
                    categoryInfoList = this.getWordAssessmentSubCategoriesInfo(stdId, subStdId);
                    if(adapter != null){
                        adapter.categoryInfoList = categoryInfoList;
                        adapter.notifyDataSetChanged();
                    }
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void getGrade(String gradeValue){
        Grade gradeObj = new Grade();
        ArrayList<String> gradeList = gradeObj.getGradeList(this);
        this.setGradeAdapter(gradeValue , gradeList);
    }


    private void setGradeAdapter(final String gradeValue, ArrayList<String> gradeList){
        ArrayAdapter<String> gradeAdapter = new ArrayAdapter<String>(this, R.layout.spinner_textview_layout,gradeList);
        gradeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGrade.setAdapter(gradeAdapter);
        spinnerGrade.setSelection(gradeAdapter.getPosition(gradeValue));

        spinnerGrade.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {
                downloadGradeDateAndSetValues(MathFriendzyHelper.parseInt(spinnerGrade
                        .getSelectedItem().toString()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });
    }

    private void resetValues(){
        if(isInitialDataSet) {//not null all the initial value at the starting , like to show the selected category
            categoryList = null;
            categoryInfoList = null;
            mSubCatList = null;
            openSelectedCatListForWordProblem = null;
            adapter = null;
        }else{
            isInitialDataSet = true;
        }
    }

    private void downloadGradeDateAndSetValues(final int grade){
        selectedStudentGrade = grade + "";
        this.resetValues();
        if(MathFriendzyHelper.checkForDownload(this , grade)){
            getWordCatAndSubCat(grade + "");
        }else {
            MathFriendzyHelper.downLoadQuestionForWordProblem(this,
                    grade, new HttpServerRequest() {
                        @Override
                        public void onRequestComplete() {
                            getWordCatAndSubCat(grade + "");
                        }
                    });
        }
    }
}
