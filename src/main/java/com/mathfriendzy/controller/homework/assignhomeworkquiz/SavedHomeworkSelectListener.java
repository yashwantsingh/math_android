package com.mathfriendzy.controller.homework.assignhomeworkquiz;

import com.mathfriendzy.model.homework.HomeworkData;

/**
 * Created by root on 6/6/16.
 */
public interface SavedHomeworkSelectListener {
    void onSelectListener(int index , HomeworkData selectedHomeworkData);
}
