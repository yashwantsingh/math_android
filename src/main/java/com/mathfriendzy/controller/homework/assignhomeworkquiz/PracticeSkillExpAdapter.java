package com.mathfriendzy.controller.homework.assignhomeworkquiz;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;

public class PracticeSkillExpAdapter extends BaseExpandableListAdapter{

	private ArrayList<LearningCenterTransferObj> laernignCenterFunctionsList1 = null;
	private ArrayList<LearningCenterTransferObj> laernignCenterFunctionsList  = null;
	public LinkedHashMap<String ,ArrayList<LearningCenterTransferObj>> mapChildList = null;

	private LayoutInflater mInflator = null;
	private GroupViewHolder gVHolder;
	private ChiledViewHolder cVHolder;
	private Context context;

	//private Map<String, PracticeSkillSelectionDTO> selectionMap = null;
	//private boolean isTab = false;

	public PracticeSkillExpAdapter(Context context , 
			ArrayList<LearningCenterTransferObj> laernignCenterFunctionsList1
			, ArrayList<LearningCenterTransferObj> laernignCenterFunctionsList
			, LinkedHashMap<String ,ArrayList<LearningCenterTransferObj>> mapChildList ){
		this.laernignCenterFunctionsList = laernignCenterFunctionsList;
		this.laernignCenterFunctionsList1 = laernignCenterFunctionsList1;
		this.mapChildList = mapChildList;
		mInflator = LayoutInflater.from(context);
		this.context = context;
	}

	@Override
	public int getGroupCount() {
		return laernignCenterFunctionsList.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return mapChildList.get(laernignCenterFunctionsList.get(groupPosition)
				.getLearningCenterOperation()).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return laernignCenterFunctionsList.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return mapChildList.get(laernignCenterFunctionsList.get(groupPosition)
				.getLearningCenterOperation()).get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {

		if(convertView == null){
			gVHolder = new GroupViewHolder();
			convertView = mInflator.inflate(R.layout.practice_skill_assign_homework_catlayout, null);
			convertView.setTag(gVHolder);
			gVHolder.imgCategorySign 	= (ImageView) convertView.findViewById(R.id.imgSign);
			gVHolder.txtCategoryName 	= (TextView)  convertView.findViewById(R.id.txtCategoryName);
		}else{
			gVHolder = (GroupViewHolder) convertView.getTag();
		}

		gVHolder.imgCategorySign.setBackgroundResource(context.getResources().getIdentifier
				("mf_" + laernignCenterFunctionsList.get(groupPosition)
						.getLearningCenterOperation().toLowerCase().replace(" ","_")+"_sign",
						"drawable", context.getPackageName()));

		gVHolder.txtCategoryName.setText(laernignCenterFunctionsList1
				.get(groupPosition).getLearningCenterOperation());
		
		/*ExpandableListView eLV = (ExpandableListView) parent;
	    eLV.expandGroup(groupPosition);*/
	    
		return convertView;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		LearningCenterTransferObj obj = (LearningCenterTransferObj)	getChild(groupPosition, childPosition);

		if(convertView == null){
			cVHolder = new ChiledViewHolder();
			convertView = mInflator.inflate
					(R.layout.assign_practice_skill_homework_sub_cat_layout, null);

			cVHolder.txtChildLevelName = (TextView) convertView.findViewById(R.id.txtlevelName);
			cVHolder.imgCheckImage = (ImageView) convertView.findViewById(R.id.imgCheckImage);
			cVHolder.spinnerNumberOfProblem = (Spinner) 
					convertView.findViewById(R.id.spinnerNumberOfProblem);
			convertView.setTag(cVHolder);
		}else{
			cVHolder = (ChiledViewHolder) convertView.getTag();
		}

		cVHolder.txtChildLevelName.setText(obj.getMathOperationCategory());
		this.setNoOfProblemAdapterToSpinner(cVHolder.spinnerNumberOfProblem , 
				obj.getTotalProblem() , obj.getNumberOfProblemSelected() , obj);
		this.setCheckedImages(cVHolder.imgCheckImage, obj);
		return convertView;
	}

	private class GroupViewHolder{
		private ImageView imgCategorySign;
		private TextView txtCategoryName;
	}

	private class ChiledViewHolder{
		private TextView txtChildLevelName;
		private ImageView imgCheckImage;
		private Spinner spinnerNumberOfProblem;
	}

	/**
	 * Set Number of problem in the spinner
	 * @param spinner
	 * @param totalProblem
	 * @param selectedProblems
	 * @param obj
	 */
	private void setNoOfProblemAdapterToSpinner(final Spinner spinner , int totalProblem , 
			int selectedProblems, final LearningCenterTransferObj obj){
		ArrayAdapter<String> numberOfProblemAdapter = new ArrayAdapter<String>
		(context, R.layout.spinner_textview_layout,this.getArrayList(totalProblem));
		numberOfProblemAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(numberOfProblemAdapter);
		spinner.setSelection(numberOfProblemAdapter.getPosition(selectedProblems + ""));

		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				obj.setNumberOfProblemSelected
				(Integer.parseInt(spinner.getSelectedItem().toString()));			
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
	}

	/**
	 * Create the array list based on the number of question in the cat
	 * @param totalNoOfProblems
	 * @return
	 */
	private ArrayList<String> getArrayList(int totalNoOfProblems){
		ArrayList<String> list = new ArrayList<String>();
		for(int i = 5 ; i <= totalNoOfProblems ; i += 5){
			list.add(i + "");
		}
		return list;
	}

	/**
	 * Set image check 
	 * @param imgCheck
	 * @param obj
	 */
	private void setCheckedImages(ImageView imgCheck , final LearningCenterTransferObj obj){
		if(obj.isSelected()){
			imgCheck.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
		}else{
			imgCheck.setBackgroundResource(R.drawable.mf_check_box_ipad);
		}

		imgCheck.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(obj.isSelected()){
					obj.setSelected(false);
				}else{
					obj.setSelected(true);
				}
				PracticeSkillExpAdapter.this.notifyDataSetChanged();
			}
		});
	}
}
