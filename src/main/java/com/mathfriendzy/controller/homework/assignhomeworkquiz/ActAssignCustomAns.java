package com.mathfriendzy.controller.homework.assignhomeworkquiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.homework.Utils;
import com.mathfriendzy.controller.homework.addhomeworksheet.ActAddHomeworkSheet;
import com.mathfriendzy.controller.homework.addhomeworksheet.ActViewPdf;
import com.mathfriendzy.controller.homework.homwworkworkarea.AddUrlToQuestionDialog;
import com.mathfriendzy.controller.homework.homwworkworkarea.AddUrlToWorkArea;
import com.mathfriendzy.controller.homework.homwworkworkarea.OnUrlSaveCallback;
import com.mathfriendzy.controller.resources.ActResourceHome;
import com.mathfriendzy.controller.resources.ActSelectedResource;
import com.mathfriendzy.customview.EditTextBlink;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.helper.YesNoListenerInterface;
import com.mathfriendzy.listener.OnRequestSuccess;
import com.mathfriendzy.model.homework.CustomeAns;
import com.mathfriendzy.model.homework.CustomeResult;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.resource.ResourceParam;
import com.mathfriendzy.model.resource.ResourceResponse;
import com.mathfriendzy.model.resource.SearchResourceResponse;
import com.mathfriendzy.pdfoperation.PDFOperation;
import com.mathfriendzy.regexp.MyRegularExpression;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;

import java.util.ArrayList;
import java.util.HashMap;

public class ActAssignCustomAns extends ActBase {

    private final String TAG = this.getClass().getSimpleName();
    private Button btnAddHomeworkSheet = null;
    private TextView txtAnswerSheetFor = null;
    private TextView txtStudentAns = null;
    private TextView txtUnitOfMeasurement = null;
    private TextView txtPositionOfUnitOfMeasurent = null;
    private TextView txtAllowChanges = null;
    private TextView txtShowAnswer = null;

    private ImageView imgStudentAnsInfo = null;
    private ImageView imgAllowChangesCheck = null;
    private ImageView imgShowAnswer = null;
    private ImageView imgShowAllowChangesAndAnswerInfo = null;

    private ListView lstAnswerList = null;

    private ArrayList<CustomeAns> customeAnsList = null;

    //for msg dialog
    private String lblStudentAnswerIsWhereYourStudent = null;
    private String lblIfYouUnselectChkBox = null;

    //for multiple answer type selection activity
    private AssignCustomeQuizzAdapter adapter = null;
    public final int START_MULTIPLE_CHOICE_ACT_REQUEST = 1001;

    private CustomeResult customeResult = null;

    //for custom keyboard
    private RelativeLayout keyboardLayout = null;
    private RelativeLayout numericLayout = null;
    private RelativeLayout sumbolicLayout = null;
    private Button btnCloseKeyboard = null;

    //on backprees dialog
    private String lblYouHaveOneOrMoreQuestionsIncomplete = null;
    private String btnTitleYes = null;
    private String lblNo = null;
    private String lblTheAnswerSheetIsBlank = null;

    private final int ADD_WORK_SHEET_REQUEST = 1002;
    private ArrayList<String> workSheetImageList = null;
    private static ActAssignCustomAns currentObj = null;

    //Add Resources
    private Button btnAddResources = null;
    private ArrayList<ResourceResponse> selectedResourceList = null;
    private ResourceParam searchParam = null;
    private HashMap<String, SearchResourceResponse> dataList = null;
    private HashMap<String, ArrayList<ResourceResponse>> selectedListResources = null;

    //siddhiinfosoft

    private boolean is_keyboard_show = false;
    protected Animation animation = null;
    private LinearLayout ll_visible_view = null;
    public static int dpwidth = 600;
    private boolean isNeedAnimation = true;
    private final int FILL_IN_TYPE = 1;
    private final int MULTIPLE_CHOICE_ANS_TYPE = 1;
    private final int TRUE_FALSE_ANS_TYPE = 2;
    private final int YES_NO_ANS_TYPE = 3;
    public int clickedPosition = 0;
    private String lblAnswerOnWorkAreaCreditGiven = "";
    private String lblOnWorkArea = "";
    public ArrayList<LinearLayout> list_ans_box = new ArrayList<LinearLayout>();
    public EditText selected_ed_2 = null;
    public EditText selectedTextView = null;
    public LinearLayout ll_selected_view = null;
    public LinearLayout ll_ans_box_selected = null;
    private int selectedTextViewposition = -1;
    private ArrayList<RelativeLayout> list_anim_rl;
    private LinearLayout assignHomeWorkListLayout = null;


    //For the new popups
    private String lblProblemsWithSameNum = "You have one or more answers labeled with the same problem number.  " +
            "This is not allowed. Please revise the \"Problem Numbers\" for your answer fields.";

    //new assign homework change to delete the homework
    private Button btnDeleteHomework = null;
    private String lblUseThisForQue = "Use this for questions that cannot be answered with our keyboard.";


    //Add question
    private Button btnQuestion = null;
    private boolean isShowQuestion = false;//hide question default
    private ArrayList<ViewHolder> viewHolders = new ArrayList<ViewHolder>();
    private String lblEnterQuestion = null;
    private String lblEnterAnswer = null;
    public ArrayList<LinearLayout> list_que_box = new ArrayList<LinearLayout>();

    //Add Links
    private Button btnAddLinks = null;
    private ArrayList<AddUrlToWorkArea> urlList = null;
    private AddUrlToQuestionDialog addUrlLinkDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_assign_custom_ans2);


        try {
            DisplayMetrics displayMetrics = ActAssignCustomAns.this.getResources().getDisplayMetrics();
            // float dpWidth2 = displayMetrics.widthPixels / displayMetrics.density;

            dpwidth = displayMetrics.widthPixels;
            float dpWidth2 = displayMetrics.widthPixels;

            dpwidth = Math.round(dpWidth2);

            if (dpwidth < 600) {
                dpwidth = 600;
            }

            Log.e("kb_ll_width", "" + dpwidth + "" + displayMetrics.widthPixels);
        } catch (Exception e) {
            dpwidth = 600;
        }

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside onCreate()");


        this.initializeCurrentObj();
        this.getIntentValues();
        this.init();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.setAdapter();
        this.setAllowChanges(customeResult.getAllowChanges());
        this.setSeeAnswer(customeResult.getShowAns());
        this.setAddResourceButtonVisibility();
        this.setTextOfAddHomeworkSheetButton();

        this.setList();

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "outside onCreate()");
    }

    private void setAddResourceButtonVisibility() {
        if (getResources().getBoolean(R.bool.isTablet)) {
            btnAddResources.setVisibility(Button.VISIBLE);
        } else {
            btnAddResources.setVisibility(Button.VISIBLE);
        }
    }

    /**
     * Get the intent values which are set in previous screen
     */
    private void getIntentValues() {
        customeResult = (CustomeResult) this.getIntent().getSerializableExtra("customeResult");
        customeAnsList = customeResult.getCustomeAnsList();
        workSheetImageList = customeResult.getPdfImagesList();
    }

    /**
     * initialize the variables
     */
    private void init() {
        try {
            if (customeAnsList == null) {
                customeAnsList = new ArrayList<CustomeAns>();
                for (int i = 0; i < 10; i++) {
                    customeAnsList.add(this.getCustomeAnsObj(((i + 1) + ""), 1, 1));//
                }
            } else {
                this.isShowQuestion = this.isAtleastOneQuestionAdded(customeAnsList);
            }

            this.initializeInitialResourceData();
        } catch (Exception e) {
            e.printStackTrace();
        }
        urlList = customeResult.getLinks();
        this.initializeAddUrlList();
    }

    private void initializeInitialResourceData() {
        try {
            selectedResourceList = customeResult.getSelectedResourceList();
            searchParam = customeResult.getSearchParam();
            dataList = customeResult.getDataList();
            selectedListResources = customeResult.getSelectedListResources();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // this.hideDeviceKeyboardAndClearFocus();
        return super.onTouchEvent(event);
    }

    public void hideDeviceKeyboardAndClearFocus() {
        try {
            //this.cleasFocusFromEditText();
            this.hideDeviceKeyboard();
            this.hideKeyboard();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Return the custome ans object
     *
     * @param questionNo
     * @return
     */
    public CustomeAns getCustomeAnsObj(String questionNo,
                                       int fillIntype, int answerType) {
        CustomeAns ans = new CustomeAns();
        ans.setQueNo(questionNo);
        ans.setFillInType(fillIntype);
        ans.setAnswerType(answerType);//1 default , for multiple choice
        ans.setIsLeftUnit(0);
        ans.setCorrectAns("");
        ans.setAnsSuffix("");
        ans.setVisibleLayout(false);
        return ans;
    }

    /**
     * Return default multiple choice option list
     *
     * @return
     */
    public ArrayList<String> getDefaultMultipleChoiceOptionList() {
        ArrayList<String> list = new ArrayList<String>();
        list.add("A");
        list.add("B");
        list.add("C");
        list.add("D");
        return list;
    }

    @Override
    protected void setWidgetsReferences() {

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside onCreate()");

        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        btnAddHomeworkSheet = (Button) findViewById(R.id.btnAddHomeworkSheet);
        txtAnswerSheetFor = (TextView) findViewById(R.id.txtAnswerSheetFor);
        txtStudentAns = (TextView) findViewById(R.id.txtStudentAns);
        txtUnitOfMeasurement = (TextView) findViewById(R.id.txtUnitOfMeasurement);
        txtPositionOfUnitOfMeasurent = (TextView) findViewById(R.id.txtPositionOfUnitOfMeasurent);
        txtAllowChanges = (TextView) findViewById(R.id.txtAllowChanges);
        txtShowAnswer = (TextView) findViewById(R.id.txtShowAnswer);

        assignHomeWorkListLayout = (LinearLayout) findViewById(R.id.assignHomeWorkListLayout); //siddhiinfosoft

        imgStudentAnsInfo = (ImageView) findViewById(R.id.imgStudentAnsInfo);
        imgAllowChangesCheck = (ImageView) findViewById(R.id.imgAllowChangesCheck);
        imgShowAnswer = (ImageView) findViewById(R.id.imgShowAnswer);
        imgShowAllowChangesAndAnswerInfo = (ImageView) findViewById(R.id.imgShowAllowChangesAndAnswerInfo);

        lstAnswerList = (ListView) findViewById(R.id.lstAnswerList);

        //for keyboard
        keyboardLayout = (RelativeLayout) findViewById(R.id.rl_math_keyboard); //siddhiinfosoft
        numericLayout = (RelativeLayout) findViewById(R.id.numericLayout);
        sumbolicLayout = (RelativeLayout) findViewById(R.id.sumbolicLayout);
        this.setWidgetsReferencesForKeyboard();

        btnCloseKeyboard = (Button) findViewById(R.id.btnCloseKeyboard);
        btnAddResources = (Button) findViewById(R.id.btnAddResources);


        //new assign homework change to delete the homework
        btnDeleteHomework = (Button) findViewById(R.id.btnDeleteHomework);
        //end changes


        this.setcustomkeyboardwidgetsreference();  // siddhiinfosoft

        kb_btn_123_delete.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        is_touch_kb = true;
                        Handler hnd = new Handler();
                        hnd.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ondeleteclick();
                            }
                        }, 200);
                        break;
                    case MotionEvent.ACTION_UP:
                        is_touch_kb = false;
                        break;
                }
                return true;
            }
        });

        kb_btn_abc_delete.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        is_touch_kb = true;
                        Handler hnd = new Handler();
                        hnd.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ondeleteclick();
                            }
                        }, 200);
                        break;
                    case MotionEvent.ACTION_UP:
                        is_touch_kb = false;
                        break;
                }
                return true;
            }
        });

        kb_btn_123_previous.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        is_touch_kb_pre = true;
                        Handler hnd = new Handler();
                        hnd.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                onpreviousclick();
                            }
                        }, 200);
                        break;
                    case MotionEvent.ACTION_UP:
                        is_touch_kb_pre = false;
                        break;
                }
                return true;
            }
        });

        kb_btn_123_next.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        is_touch_kb_next = true;
                        Handler hnd = new Handler();
                        hnd.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                onnextclick();
                            }
                        }, 200);
                        break;
                    case MotionEvent.ACTION_UP:
                        is_touch_kb_next = false;
                        break;
                }
                return true;
            }
        });

        //Add question
        btnQuestion = (Button) findViewById(R.id.btnQuestion);

        //Add Links
        btnAddLinks = (Button) findViewById(R.id.btnAddLinks);

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "outside onCreate()");

    }

    @Override
    protected void setListenerOnWidgets() {

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside onCreate()");

        imgStudentAnsInfo.setOnClickListener(this);
        imgShowAllowChangesAndAnswerInfo.setOnClickListener(this);
        imgAllowChangesCheck.setOnClickListener(this);
        imgShowAnswer.setOnClickListener(this);
        btnCloseKeyboard.setOnClickListener(this);
        btnAddHomeworkSheet.setOnClickListener(this);
        //for keyboard
        this.setListenerOnKeyBoardLayoutButton();

        btnAddResources.setOnClickListener(this);


        //new assign homework change to delete the homework
        btnDeleteHomework.setOnClickListener(this);
        //end changes

        //Add question
        btnQuestion.setOnClickListener(this);

        //Add Links
        btnAddLinks.setOnClickListener(this);

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "outside onCreate()");

    }

    @Override
    protected void setTextFromTranslation() {

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside onCreate()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();

        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblAssignHomework")
                + "/" + transeletion.getTranselationTextByTextIdentifier("lblQuizzes"));
        btnAddHomeworkSheet.setText(transeletion.getTranselationTextByTextIdentifier("lblAssignments"));
        txtAnswerSheetFor.setText(transeletion.getTranselationTextByTextIdentifier("lblAnswerSheetFor")
                + ": " + customeResult.getTitle());
        txtStudentAns.setText(transeletion.getTranselationTextByTextIdentifier("lblStudentAnswer"));
        txtUnitOfMeasurement.setText(transeletion.getTranselationTextByTextIdentifier("lblUnitOfMeasurement"));
        txtPositionOfUnitOfMeasurent.setText(transeletion.getTranselationTextByTextIdentifier("lblPositionOfUnit"));
        txtAllowChanges.setText(transeletion.getTranselationTextByTextIdentifier("lblAllowChanges"));
        txtShowAnswer.setText(transeletion.getTranselationTextByTextIdentifier("lblShowAnswers"));

        lblStudentAnswerIsWhereYourStudent = transeletion.getTranselationTextByTextIdentifier("lblStudentAnswerIsWhereYourStudent");
        lblIfYouUnselectChkBox = transeletion.getTranselationTextByTextIdentifier("lblIfYouUnselectChkBox");

        lblYouHaveOneOrMoreQuestionsIncomplete = transeletion.getTranselationTextByTextIdentifier("lblYouHaveOneOrMoreQuestionsIncomplete");
        btnTitleYes = transeletion.getTranselationTextByTextIdentifier("btnTitleYes");
        lblNo = transeletion.getTranselationTextByTextIdentifier("lblNo");
        lblTheAnswerSheetIsBlank = transeletion.getTranselationTextByTextIdentifier("lblTheAnswerSheetIsBlank");

        btnAddResources.setText(transeletion.getTranselationTextByTextIdentifier("navigationTitleResources"));
        //Add question
        btnQuestion.setText(transeletion.getTranselationTextByTextIdentifier("lblQuestion"));

        //Add Links
        //btnAddLinks.setText(transeletion.getTranselationTextByTextIdentifier("lblQuestion"));
        transeletion.closeConnection();

        String[] textArray = MathFriendzyHelper
                .getTreanslationTextById(ActAssignCustomAns.this, "lblAnsOnWorkArea",
                        "lblOnWorkArea", "lblUseThisQues", "lblEnterQuestion", "lblEnterAnswer");
        lblAnswerOnWorkAreaCreditGiven = textArray[0];
        lblOnWorkArea = textArray[1];
        lblUseThisForQue = textArray[2];
        lblAnswerOnWorkAreaCreditGiven = lblOnWorkArea + "\n" + lblUseThisForQue;

        lblEnterQuestion = textArray[3];
        lblEnterAnswer = textArray[4];

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "outside onCreate()");
    }

    /**
     * Set the custome list adapter
     */
//    private void setAdapter() {
//        adapter = new AssignCustomeQuizzAdapter(this, customeAnsList);
//        lstAnswerList.setAdapter(adapter);
//
//        lstAnswerList.setOnTouchListener(new OnTouchListener() {
//
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                hideDeviceKeyboardAndClearFocus();
//                return false;
//            }
//        });
//    }

    /**
     * Set Allow changes
     *
     * @param intValue
     */
    private void setAllowChanges(int intValue) {
        if (intValue == 1) {
            imgAllowChangesCheck.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
        } else {
            imgAllowChangesCheck.setBackgroundResource(R.drawable.mf_check_box_ipad);
        }
    }

    /**
     * Set See Answer
     *
     * @param intValue
     */
    private void setSeeAnswer(int intValue) {
        if (intValue == 1) {
            imgShowAnswer.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
        } else {
            imgShowAnswer.setBackgroundResource(R.drawable.mf_check_box_ipad);
        }
    }

    /**
     * Show the keyboard
     */
//    public void showKeyboard(){
//        //animation = AnimationUtils.loadAnimation(this, R.anim.bottom_up_animation);
//        btnCloseKeyboard.setVisibility(Button.VISIBLE);
//        keyboardLayout.setVisibility(RelativeLayout.VISIBLE);
//        //keyboardLayout.setAnimation(animation);
//    }

    /**
     * Hide the keyboard
     */
//    public void hideKeyboard(){
//		/*if(keyboardLayout.getVisibility() == RelativeLayout.VISIBLE){
//			animation = AnimationUtils.loadAnimation(this, R.anim.bottom_down_animation);*/
//        btnCloseKeyboard.setVisibility(Button.INVISIBLE);
//        keyboardLayout.setVisibility(RelativeLayout.INVISIBLE);
//        adapter.closeMathKeyboard();
//		/*keyboardLayout.setAnimation(animation);
//		}*/
//    }

    /**
     * Set the text to the selected text view
     *
     * @param txtView
     * @param text
     */
    public void setTextToSelectedTextView(TextView txtView, String text) {
        /*if(txtView != null){
            txtView.setText(txtView.getText().toString().replace("|", "") + text + "|");
		}*/

        adapter.setTextToSelectedTextView(txtView, text);
    }

    /**
     * Clear text
     *
     * @param txtView
     */
    public void cleartextFromSelectedTextView(TextView txtView) {
        /*StringBuilder text = new StringBuilder(txtView.getText().toString().replace("|", ""));
        if(text.length() > 0){
			text.deleteCharAt(text.length() - 1);
			txtView.setText(text + "|");
		}*/
        adapter.cleartextFromSelectedTextView(txtView);
    }

    /**
     * Open the add work sheet Activity
     */
    private void clickOnAddHomeworkSheet() {
        if (!MathFriendzyHelper.isEmpty(customeResult.getSheetName())
                && workSheetImageList == null) {
            this.clickOnViewSheet();
        } else {
            Intent intent = new Intent(this, ActAddHomeworkSheet.class);
            intent.putExtra("workSheetImageList", workSheetImageList);
            intent.putExtra("clickedPosition", customeResult.getClickedPosition());
            startActivityForResult(intent, ADD_WORK_SHEET_REQUEST);
        }
        //CommonUtils.comingSoonPopUp(this);
    }

    /**
     * Set show answer
     */
    private void setShowAnswer() {
        customeResult.setShowAns((customeResult.getShowAns() + 1) % 2);
        this.setSeeAnswer(customeResult.getShowAns());
    }

    /**
     * Set allow changes
     */
    private void setAllowChanges() {
        customeResult.setAllowChanges((customeResult.getAllowChanges() + 1) % 2);
        this.setAllowChanges(customeResult.getAllowChanges());
    }

    @Override
    public void onClick(View v) {
        hideDeviceKeyboardAndClearFocus();
        switch (v.getId()) {
            case R.id.imgStudentAnsInfo:
                MathFriendzyHelper.warningDialogForLongMsg(this, lblStudentAnswerIsWhereYourStudent);
                break;
            case R.id.imgShowAllowChangesAndAnswerInfo:
                MathFriendzyHelper.warningDialogForLongMsg(this, lblIfYouUnselectChkBox);
                break;
            case R.id.imgAllowChangesCheck:
                this.setAllowChanges();
                break;
            case R.id.imgShowAnswer:
                this.setShowAnswer();
                break;
            case R.id.btnsecondryKeyBoard:
                numericLayout.setVisibility(RelativeLayout.GONE);
                sumbolicLayout.setVisibility(RelativeLayout.VISIBLE);
                break;
            case R.id.btnNumber:
                numericLayout.setVisibility(RelativeLayout.VISIBLE);
                sumbolicLayout.setVisibility(RelativeLayout.GONE);
                break;
            case R.id.btn1:
                this.setTextToSelectedTextView(adapter.selectedTextView, "1");
                break;
            case R.id.btn2:
                this.setTextToSelectedTextView(adapter.selectedTextView, "2");
                break;
            case R.id.btn3:
                this.setTextToSelectedTextView(adapter.selectedTextView, "3");
                break;
            case R.id.btn4:
                this.setTextToSelectedTextView(adapter.selectedTextView, "4");
                break;
            case R.id.btn5:
                this.setTextToSelectedTextView(adapter.selectedTextView, "5");
                break;
            case R.id.btn6:
                this.setTextToSelectedTextView(adapter.selectedTextView, "6");
                break;
            case R.id.btn7:
                this.setTextToSelectedTextView(adapter.selectedTextView, "7");
                break;
            case R.id.btn8:
                this.setTextToSelectedTextView(adapter.selectedTextView, "8");
                break;
            case R.id.btn9:
                this.setTextToSelectedTextView(adapter.selectedTextView, "9");
                break;
            case R.id.btn0:
                this.setTextToSelectedTextView(adapter.selectedTextView, "0");
                break;
            case R.id.btnSynchronized:
                this.cleartextFromSelectedTextView(adapter.selectedTextView);
                break;
            case R.id.btnDot:
                this.setTextToSelectedTextView(adapter.selectedTextView, ".");
                break;
            case R.id.btnMinus:
                this.setTextToSelectedTextView(adapter.selectedTextView, "-");
                break;
            case R.id.btnComma:
                this.setTextToSelectedTextView(adapter.selectedTextView, ",");
                break;
            case R.id.btnPlus:
                this.setTextToSelectedTextView(adapter.selectedTextView, "+");
                break;
            case R.id.btnDevide:
                this.setTextToSelectedTextView(adapter.selectedTextView, "/");
                break;
            case R.id.btnMultiply:
                this.setTextToSelectedTextView(adapter.selectedTextView, "*");
                break;
            case R.id.btnCollon:
                this.setTextToSelectedTextView(adapter.selectedTextView, ":");
                break;
            case R.id.btnLessThan:
                this.setTextToSelectedTextView(adapter.selectedTextView, "<");
                break;
            case R.id.btnGreaterThan:
                this.setTextToSelectedTextView(adapter.selectedTextView, ">");
                break;
            case R.id.btnEqual:
                this.setTextToSelectedTextView(adapter.selectedTextView, "=");
                break;
            case R.id.btnRoot:
                this.setTextToSelectedTextView(adapter.selectedTextView, "√");
                break;
            case R.id.btnDevider:
                this.setTextToSelectedTextView(adapter.selectedTextView, "|");
                break;
            case R.id.btnOpeningBracket:
                this.setTextToSelectedTextView(adapter.selectedTextView, "(");
                break;
            case R.id.btnClosingBracket:
                this.setTextToSelectedTextView(adapter.selectedTextView, ")");
                break;
            case R.id.btnCloseKeyboard:
                this.hideDeviceKeyboardAndClearFocus();
                //this.hideKeyboard();
                break;
            case R.id.btnAddHomeworkSheet:
                this.clickOnAddHomeworkSheet();
                break;
            case R.id.btnAddResources:
                this.openAddResourceScreen();
                break;
            case R.id.btnDeleteHomework:
                this.deleteHomework();
                break;
            case R.id.btnQuestion:
                if (!this.isShowQuestion) {
                    this.showQuestionFields(isShowQuestion);
                    return;
                }
                String text[] = MathFriendzyHelper.getTreanslationTextById(this,
                        "lblWillDeleteAllQues", "btnTitleYes", "lblNo");
                MathFriendzyHelper.yesNoConfirmationDialog(this,
                        text[0], text[1], text[2], new YesNoListenerInterface() {
                            @Override
                            public void onYes() {
                                showQuestionFields(isShowQuestion);
                            }

                            @Override
                            public void onNo() {

                            }
                        });
                break;
            case R.id.btnAddLinks:
                this.showAddLinksDialog();
                break;
        }
    }

    private void openAddResourceScreen() {
        if (selectedResourceList != null && this.selectedResourceList.size() > 0) {
            Intent intent = new Intent(this, ActSelectedResource.class);
            intent.putExtra("isDirectOpenWithoutSearch", true);
            intent.putExtra("isNotShowResourceButton", true);
            intent.putExtra("selectedResourceList", selectedResourceList);
            intent.putExtra("hwTitle", customeResult.getTitle());
            intent.putExtra("searchParam", searchParam);
            intent.putExtra("dataList", dataList);
            intent.putExtra("selectedListResources", selectedListResources);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, ActResourceHome.class);
            intent.putExtra("isAssignHomework", true);
            intent.putExtra("homeworkTitle", customeResult.getTitle());
            startActivity(intent);
        }
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase,
                               int requestCode) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onBackPressed() {
        if (is_keyboard_show) {
            this.hideKeyboard();
        } else {
            this.hideDeviceKeyboardAndClearFocus();
            // this.getUpdatedCustomeResult();
            this.getUpdatedCustomeResultnew(); //siddhiinfosoft
            //super.onBackPressed();
        }

    }

    private CustomeResult getUpdatedCustomeResultnew() { //siddhiinfosoft

        try {

            int int_pos = 0;

            boolean isGivenAtleastOneQuestionAns = false;
            final ArrayList<CustomeAns> updatedList = new ArrayList<CustomeAns>();
            for (int i = 0; i < customeAnsList.size(); i++) {
                customeAnsList.get(i).setVisibleLayout(false);
                this.setQuestionToCustomAns(customeAnsList.get(i), i);

                if (customeAnsList.get(i).getFillInType() == 1) {

                    LinearLayout ll_anx_box = (LinearLayout) list_ans_box.get(int_pos);
                    String questionNumber = customeAnsList.get(i).getQueNo();
                    String ans_value = this.makeansfromview(ll_anx_box);

                    int_pos = int_pos + 1;

                    if (!MathFriendzyHelper.isEmpty(questionNumber)) {
                        if (customeAnsList.get(i).getIsAnswerAvailable()
                                == MathFriendzyHelper.NO) {
                            customeAnsList.get(i).setCorrectAns(lblAnswerOnWorkAreaCreditGiven);
                            isGivenAtleastOneQuestionAns = true;
                            if (this.isQuestionCompleteTeAdd(customeAnsList.get(i)))
                                updatedList.add(customeAnsList.get(i));
                        } else {
                            if (ans_value != null && !ans_value.equals("")) {
                                customeAnsList.get(i).setCorrectAns(ans_value);
                                isGivenAtleastOneQuestionAns = true;
                                if (this.isQuestionCompleteTeAdd(customeAnsList.get(i)))
                                    updatedList.add(customeAnsList.get(i));
                            }
                        }
                    }
                } else {

                    String correctAns = customeAnsList.get(i).getCorrectAns();
                    String questionNumber = customeAnsList.get(i).getQueNo();
                    if (correctAns != null && correctAns.length() > 0
                            && !MathFriendzyHelper.isEmpty(questionNumber)) {
                        isGivenAtleastOneQuestionAns = true;
                        if (this.isQuestionCompleteTeAdd(customeAnsList.get(i)))
                            updatedList.add(customeAnsList.get(i));
                    }
                }
            }

            if (isGivenAtleastOneQuestionAns) {
                if (customeAnsList.size() == updatedList.size()) {
                    customeResult.setCustomeAnsList(updatedList);
                    finishCurrectActOnBackPress(customeResult);
                } else {
                    MathFriendzyHelper.yesNoConfirmationDialog
                            (this, lblYouHaveOneOrMoreQuestionsIncomplete
                                    , btnTitleYes, lblNo, new YesNoListenerInterface() {

                                        @Override
                                        public void onYes() {
                                            if (updatedList.size() > 0)
                                                customeResult.setCustomeAnsList(updatedList);
                                            else
                                                customeResult.setCustomeAnsList(null);
                                            finishCurrectActOnBackPress(customeResult);
                                        }

                                        @Override
                                        public void onNo() {

                                        }
                                    });
                }
            } else {
                //customeResult.setCustomeAnsList(customeAnsList);
                MathFriendzyHelper.yesNoConfirmationDialog(this, lblTheAnswerSheetIsBlank,
                        btnTitleYes, lblNo, new YesNoListenerInterface() {

                            @Override
                            public void onYes() {
                                customeResult.setCustomeAnsList(null);
                                finishCurrectActOnBackPress(customeResult);
                            }

                            @Override
                            public void onNo() {

                            }
                        });

            }

        } catch (Exception e) {

        }

        return customeResult;
    }

    private void finishCurrectActOnBackPress(CustomeResult updatedCustomeObj) {
        try {
            customeResult.setSelectedResourceList(selectedResourceList);
            customeResult.setSearchParam(searchParam);
            customeResult.setDataList(dataList);
            customeResult.setSelectedListResources(selectedListResources);
            customeResult.setLinks(urlList);
        } catch (Exception e) {
            e.printStackTrace();
        }

        updatedCustomeObj.setQuestionAdded(this.isShowQuestion);
        Intent intent = new Intent();
        intent.putExtra("customeResult", updatedCustomeObj);
        setResult(RESULT_OK, intent);
        finish();
    }

    /**
     * Update the custom result with new answer
     *
     * @return
     */
    private CustomeResult getUpdatedCustomeResult() {
        boolean isGivenAtleastOneQuestionAns = false;

        final ArrayList<CustomeAns> updatedList = new ArrayList<CustomeAns>();
        for (int i = 0; i < adapter.customeAnsList.size(); i++) {
            adapter.customeAnsList.get(i).setVisibleLayout(false);
            String correctAns = adapter.customeAnsList.get(i).getCorrectAns();
            String questionNumber = adapter.customeAnsList.get(i).getQueNo();
            if (correctAns != null && correctAns.length() > 0
                    && !MathFriendzyHelper.isEmpty(questionNumber)) {
                isGivenAtleastOneQuestionAns = true;
                updatedList.add(adapter.customeAnsList.get(i));
            }
        }

        if (isGivenAtleastOneQuestionAns) {
            if (adapter.customeAnsList.size() == updatedList.size()) {
                customeResult.setCustomeAnsList(updatedList);
                finishCurrectActOnBackPress(customeResult);
            } else {
                MathFriendzyHelper.yesNoConfirmationDialog
                        (this, lblYouHaveOneOrMoreQuestionsIncomplete
                                , btnTitleYes, lblNo, new YesNoListenerInterface() {

                                    @Override
                                    public void onYes() {
                                        customeResult.setCustomeAnsList(updatedList);
                                        finishCurrectActOnBackPress(customeResult);
                                    }

                                    @Override
                                    public void onNo() {

                                    }
                                });
            }
        } else {
            //customeResult.setCustomeAnsList(adapter.customeAnsList);
            MathFriendzyHelper.yesNoConfirmationDialog(this, lblTheAnswerSheetIsBlank,
                    btnTitleYes, lblNo, new YesNoListenerInterface() {

                        @Override
                        public void onYes() {
                            customeResult.setCustomeAnsList(null);
                            finishCurrectActOnBackPress(customeResult);
                        }

                        @Override
                        public void onNo() {

                        }
                    });

        }
        return customeResult;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case START_MULTIPLE_CHOICE_ACT_REQUEST:
                    CustomeAns customeAns = (CustomeAns) data.getSerializableExtra("customeAnsObj");
                    /// adapter.customeAnsList.set(adapter.clickedPosition, customeAns);
                    customeAnsList.set(clickedPosition, customeAns);
                    //  adapter.notifyDataSetChanged();
                    setAdapter();
                    break;
                case ADD_WORK_SHEET_REQUEST:
                    workSheetImageList = (ArrayList<String>)
                            data.getSerializableExtra("workSheetImageList");
                    customeResult.setPdfImagesList(workSheetImageList);
                    customeResult.setSheetName(data.getStringExtra("sheetName"));
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initializeCurrentObj() {
        currentObj = this;
    }

    public static ActAssignCustomAns getCurrentObj() {
        return currentObj;
    }

    public void setSelectedResources(ArrayList<ResourceResponse> selectedResourceList,
                                     ResourceParam searchParam, HashMap<String,
            SearchResourceResponse> dataList,
                                     HashMap<String, ArrayList<ResourceResponse>> selectedListResources) {
        this.selectedResourceList = selectedResourceList;
        this.searchParam = searchParam;
        this.dataList = dataList;
        this.selectedListResources = selectedListResources;
    }

    //change to search again and not to remove all  the things to assign homework
    public ArrayList<ResourceResponse> getSelectedResourceList() {
        return this.selectedResourceList;
    }

    public void initializeSelectedResourcesToNull() {
        this.selectedResourceList = null;
    }
    //end changes

    //siddhiinfosoft   BELOWE ALL METHODS ADDED BY SIDDHIINFOSOFT

    private class ViewHolder {
        private EditText txtNumber;
        private RelativeLayout fillInLayout;
        private RelativeLayout multipleChoiceLayout;
        private RelativeLayout trueFalseLayout;//for true false or yes no
        private RelativeLayout editLayout;
        private RelativeLayout fillEditDialog;
        private RelativeLayout nonfillEditDialog;
        private Button btnEdit;
        private LinearLayout ll_ans_box;

        //for fillInLayout
        private ImageView imgCheckLeftUnit;
        private ImageView imgCheckRightUnit;
        private TextView edtFillInStudentAnswer;
        private EditText edtUnitOfMeasurement;

        //for fill dialog layout
        private Button btnFillInAddLine;
        private Button btnMultipleChoice;
        private Button bntDeleteFillIn;

        //for non fillIn
        private Button btnNofillInEdit;
        private Button btnAddNonFillInLine;
        private Button btnSingleAnswer;
        private Button bntDeleteNonFillIn;

        //for multiple choice layout
        private LinearLayout optionLayout;

        //for true false or yes no layout
        private TextView ansTrue;
        private TextView ansFalse;

        //for new change regarding the on work area button
        private Button btnOnworkAreaFillIn;
        private Button btnOnworkAreaNonFillIn;

        //Add question
        private EditText edtQuestion;
        private LinearLayout ll_que_box;
        private RelativeLayout rlQuestionLayout;
        private TextView txtEnterAnsHint;
        private TextView txtEnterQueHint;

    }


    private void setAdapter() {

        try {
            assignHomeWorkListLayout.removeAllViews();
            list_anim_rl = new ArrayList<RelativeLayout>();
            viewHolders = new ArrayList<ViewHolder>();
        } catch (Exception e) {

        }

        try {

            for (int i = 0; i < customeAnsList.size(); i++) {

                View view = null;
                view = this.addalltypelayout(customeAnsList.get(i), i);

                if (view != null) {
                    assignHomeWorkListLayout.addView(view);
                }

            }

        } catch (Exception e) {

        }

    }

    private View addalltypelayout(final CustomeAns customeAns, final int position) {

        final ViewHolder vHolder = new ViewHolder();

        View convertView = LayoutInflater.from(this).inflate(R.layout.assign_homework_custome_quizz_layout, null);

        vHolder.fillInLayout = (RelativeLayout) convertView.findViewById(R.id.fillInLayout);

        /*vHolder.fillInLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });*/


        vHolder.multipleChoiceLayout = (RelativeLayout) convertView.findViewById(R.id.multipleChoiceLayout);
        vHolder.trueFalseLayout = (RelativeLayout) convertView.findViewById(R.id.trueFalseLayout);
        vHolder.txtNumber = (EditText) convertView.findViewById(R.id.txtNumber);
        vHolder.editLayout = (RelativeLayout) convertView.findViewById(R.id.editLayout);
        //    final RelativeLayout fillEditDialog = (RelativeLayout) convertView.findViewById(R.id.fillEditDialog);
        vHolder.fillEditDialog = (RelativeLayout) convertView.findViewById(R.id.fillEditDialog);
        vHolder.nonfillEditDialog = (RelativeLayout) convertView.findViewById(R.id.nonfillEditDialog);

        vHolder.btnEdit = (Button) convertView.findViewById(R.id.btnEdit);
        vHolder.ll_ans_box = (LinearLayout) convertView.findViewById(R.id.ll_ans_box);

        vHolder.btnFillInAddLine = (Button) convertView.findViewById(R.id.btnFillInAddLine);
        vHolder.btnMultipleChoice = (Button) convertView.findViewById(R.id.btnMultipleChoice);
        vHolder.bntDeleteFillIn = (Button) convertView.findViewById(R.id.bntDeleteFillIn);

        vHolder.btnNofillInEdit = (Button) convertView.findViewById(R.id.btnNofillInEdit);
        vHolder.btnAddNonFillInLine = (Button) convertView.findViewById(R.id.btnAddNonFillInLine);
        vHolder.btnSingleAnswer = (Button) convertView.findViewById(R.id.btnSingleAnswer);
        vHolder.bntDeleteNonFillIn = (Button) convertView.findViewById(R.id.bntDeleteNonFillIn);

        //for fillIn layout
        vHolder.imgCheckLeftUnit = (ImageView) convertView.findViewById(R.id.imgCheckLeftUnit);
        vHolder.imgCheckRightUnit = (ImageView) convertView.findViewById(R.id.imgCheckRightUnit);
        vHolder.edtFillInStudentAnswer = (TextView) convertView.findViewById(R.id.edtFillInStudentAnswer);
        vHolder.edtUnitOfMeasurement = (EditText) convertView.findViewById(R.id.edtUnitOfMeasurement);

        //for multiple choice layout
        vHolder.optionLayout = (LinearLayout) convertView.findViewById(R.id.optionLayout);

        //for true false or yes no layout
        vHolder.ansTrue = (TextView) convertView.findViewById(R.id.ansTrue);
        vHolder.ansFalse = (TextView) convertView.findViewById(R.id.ansFalse);

        //for new change regarding the work area button in sliding
        vHolder.btnOnworkAreaFillIn = (Button) convertView.findViewById(R.id.btnOnworkAreaFillIn);
        vHolder.btnOnworkAreaNonFillIn = (Button) convertView.findViewById(R.id.btnOnworkAreaNonFillIn);

        customeAns.setVisibleLayout(false);
        vHolder.txtNumber.setText(customeAnsList.get(position).getQueNo());
        list_anim_rl.add(vHolder.editLayout);
        this.setVisibilityOfLayout(customeAnsList.get(position),
                vHolder.fillInLayout, vHolder.multipleChoiceLayout,
                vHolder.trueFalseLayout);

        if (customeAns.isVisibleLayout()) {
            this.setVisibilityOfLayout(vHolder.editLayout, true, true);
            if (customeAns.getFillInType() == FILL_IN_TYPE) {
                this.setVisibilityOfLayout(vHolder.fillEditDialog, true, false);
                this.setVisibilityOfLayout(vHolder.nonfillEditDialog, false, false);
            } else {
                this.setVisibilityOfLayout(vHolder.fillEditDialog, false, false);
                this.setVisibilityOfLayout(vHolder.nonfillEditDialog, true, false);
            }
        } else {
            this.setVisibilityOfLayout(vHolder.editLayout, false, true);
            this.setVisibilityOfLayout(vHolder.fillEditDialog, false, false);
            this.setVisibilityOfLayout(vHolder.nonfillEditDialog, false, false);
        }

        //Enter Ans Hint for fill question ans
        vHolder.txtEnterAnsHint = (TextView) convertView.findViewById(R.id.txtEnterAnsHint);
        vHolder.txtEnterQueHint = (TextView) convertView.findViewById(R.id.txtEnterQueHint);
        vHolder.txtEnterAnsHint.setHint(lblEnterAnswer);
        vHolder.txtEnterQueHint.setHint(lblEnterQuestion);
        //end enter answer hint for fill question ans

        this.setFillInLayout(customeAns, vHolder, position);
        this.setMultiplechoiceLayout(customeAns, vHolder);
        this.setTrueFalseOrYesNoLayout(customeAns, vHolder);

        vHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (customeAns.isVisibleLayout()) {
                    customeAns.setVisibleLayout(false);
                    vHolder.editLayout.setVisibility(View.GONE);
                } else {

                    for (int i = 0; i < list_anim_rl.size(); i++) {
                        RelativeLayout rl = (RelativeLayout) list_anim_rl.get(i);
                        if (i == position) {
                            rl.setVisibility(View.VISIBLE);
                            customeAnsList.get(i).setVisibleLayout(true);
                        } else {
                            rl.setVisibility(View.GONE);
                            customeAnsList.get(i).setVisibleLayout(false);
                        }
                    }
                    if (customeAns.getFillInType() == FILL_IN_TYPE) {
                        vHolder.fillEditDialog.setVisibility(View.VISIBLE);
                        vHolder.nonfillEditDialog.setVisibility(View.GONE);
                        MathFriendzyHelper.showViewFromRightToLeftAnimation(ActAssignCustomAns.this, vHolder.fillEditDialog);
                    } else {
                        vHolder.nonfillEditDialog.setVisibility(View.VISIBLE);
                        vHolder.fillEditDialog.setVisibility(View.GONE);
                        MathFriendzyHelper.showViewFromRightToLeftAnimation(ActAssignCustomAns.this, vHolder.nonfillEditDialog);
                    }
                }

            }
        });


        vHolder.btnFillInAddLine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setVisibilityOfCustomeAnsLayout(position, false);
                makeans();
                customeAnsList.add(position + 1, getCustomeAnsObj("", 1, 1));
                setAdapter();
                hideDeviceKeyboardAndClearFocus();
            }
        });

        vHolder.btnMultipleChoice.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                hideDeviceKeyboardAndClearFocus();
                clickedPosition = position;
                makeans();
                setVisibilityOfCustomeAnsLayout(position, false);
                Intent intent = new Intent(ActAssignCustomAns.this, ActMultipleChoiceAnsSheet.class);
                intent.putExtra("customeAnsObj", customeAns);
                startActivityForResult(intent, START_MULTIPLE_CHOICE_ACT_REQUEST);
            }
        });

        vHolder.bntDeleteFillIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideDeviceKeyboardAndClearFocus();
                setVisibilityOfCustomeAnsLayout(position, false);
                makeans();
                if (customeAnsList != null && customeAnsList.size() > 1) {
                    customeAnsList.remove(position);
                } else {
                    customeAns.setQueNo("");
                    customeAns.setCorrectAns("");
                }
                setAdapter();
            }
        });

        vHolder.btnNofillInEdit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                hideDeviceKeyboardAndClearFocus();
                clickedPosition = position;
                makeans();
                setVisibilityOfCustomeAnsLayout(position, false);
                Intent intent = new Intent(ActAssignCustomAns.this, ActMultipleChoiceAnsSheet.class);
                intent.putExtra("customeAnsObj", customeAns);
                startActivityForResult(intent, START_MULTIPLE_CHOICE_ACT_REQUEST);
            }
        });


        vHolder.btnAddNonFillInLine.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                hideDeviceKeyboardAndClearFocus();
                setVisibilityOfCustomeAnsLayout(position, false);
                makeans();
                CustomeAns prevRowAns = customeAns;
                CustomeAns customeObj = getCustomeAnsObj("", 1, 1);
                customeObj.setFillInType(prevRowAns.getFillInType());
                customeObj.setAnswerType(prevRowAns.getAnswerType());
                if (prevRowAns.getAnswerType() == MULTIPLE_CHOICE_ANS_TYPE) {
                    customeObj.setMultipleChoiceOptionList
                            (getDefaultMultipleChoiceOptionList());
                }

                customeAnsList.add(position + 1, customeObj);
                //   AssignCustomeQuizzthis.notifyDataSetChanged();
                setAdapter();
                makeans();
                clickedPosition = position + 1;
                Intent intent = new Intent(ActAssignCustomAns.this, ActMultipleChoiceAnsSheet.class);
                intent.putExtra("customeAnsObj", customeAns);
                intent.putExtra("isNewLine", true);
                startActivityForResult(intent, START_MULTIPLE_CHOICE_ACT_REQUEST);

            }
        });

        vHolder.btnSingleAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideDeviceKeyboardAndClearFocus();
                setVisibilityOfCustomeAnsLayout(position, false);
                makeans();
                customeAns.setFillInType(FILL_IN_TYPE);
                //   AssignCustomeQuizzthis.notifyDataSetChanged();
                setAdapter();
            }
        });

        vHolder.bntDeleteNonFillIn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                hideDeviceKeyboardAndClearFocus();
                setVisibilityOfCustomeAnsLayout(position, false);
                if (customeAnsList != null && customeAnsList.size() > 1) {
                    customeAnsList.remove(position);
                } else {
                    customeAns.setQueNo("");
                    customeAns.setCorrectAns("");
                }
                makeans();
                setAdapter();
            }
        });

        /*vHolder.txtNumber.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                ActAssignCustomAns.this.hideKeyboard();

                return false;
            }
        });*/


        vHolder.txtNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String questionNumber = ((EditText) v).getText().toString();
                    customeAns.setQueNo(questionNumber);
                    if (!MathFriendzyHelper.isEmpty(questionNumber)) {
                        if (isQuestionNumberAlreadyExist(questionNumber, position)) {
                            ((EditText) v).setText("");
                            customeAns.setQueNo("");

                            //changes to show popup
                            MathFriendzyHelper.showWarningDialog(ActAssignCustomAns.this,
                                    lblProblemsWithSameNum);
                        }
                    }
                }
            }
        });


        /*vHolder.ll_ans_box.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.setFocusable(true);
                v.setFocusableInTouchMode(true);


                return false;
            }
        });*/


        vHolder.ll_ans_box.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ActAssignCustomAns.this.showKeyboard();

                if (customeAns.getIsAnswerAvailable() == MathFriendzyHelper.NO) {
                    MathFriendzyHelper.showWarningDialog(ActAssignCustomAns.this, lblAnswerOnWorkAreaCreditGiven);
                } else {
                    callclickmethod(customeAns, (LinearLayout) v, position);
                }
            }
        });

        //for the new changes for adding on work area button in sliding bar
        vHolder.btnOnworkAreaFillIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeans();
                clickOnWorkArea(position, customeAns, vHolder);
            }
        });

        //not work right now
        vHolder.btnOnworkAreaNonFillIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeans();
                clickOnWorkArea(position, customeAns, vHolder);
            }
        });

        //Add question
        vHolder.ll_que_box = (LinearLayout) convertView.findViewById(R.id.ll_que_box);
        vHolder.rlQuestionLayout = (RelativeLayout) convertView.findViewById(R.id.rlQuestionLayout);

        /*vHolder.rlQuestionLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });*/


        vHolder.ll_que_box.setMinimumWidth(ActAssignCustomAns.dpwidth - 200);
        vHolder.ll_que_box.setTag(R.id.first, "1");
        vHolder.ll_que_box.setTag(R.id.second, "main");
        this.addansboxview(vHolder.ll_que_box, customeAns.getQuestionString(), MathFriendzyHelper.YES);
        list_que_box.add(vHolder.ll_que_box);
        this.showQuestionField(vHolder.rlQuestionLayout);


        vHolder.ll_que_box.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callclickmethod(customeAns, (LinearLayout) v, position);
            }
        });

        if (MathFriendzyHelper.isEmpty(customeAns.getQuestionString())) {
            this.setVisibilityOfEnterQueHintText(vHolder, true);
        } else {
            this.setVisibilityOfEnterQueHintText(vHolder, false);
        }

        vHolder.txtEnterQueHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setVisibilityOfEnterQueHintText(vHolder, false);
                try {
                    /*if (vHolder.ll_que_box.getChildCount() > 0) {
                        vHolder.ll_que_box.getChildAt(0).performClick();
                    }*/
                    callclickmethod(customeAns, vHolder.ll_que_box, position);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        /*vHolder.edtQuestion = (EditText) convertView.findViewById(R.id.edtQuestion);
        vHolder.edtQuestion.setHint(lblEnterQuestion);
        vHolder.edtQuestion.setText(customeAns.getQuestionString());
        this.showQuestionField(vHolder.edtQuestion);
        vHolder.edtQuestion.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    customeAns.setQuestionString(vHolder.edtQuestion.getText().toString());
                }
            }
        });*/

        viewHolders.add(vHolder);

        this.setTextWatcher(vHolder.txtNumber);
        this.setOnKeyListenerToEditText(vHolder.txtNumber);
        this.setOnKeyListenerToEditText(vHolder.edtUnitOfMeasurement);
        return convertView;
    }

    private boolean isQuestionNumberAlreadyExist(String questionNumber, int position) {
        try {
            for (int i = 0; i < customeAnsList.size(); i++) {
                if (i != position && customeAnsList.get(i).getQueNo()
                        .equals(questionNumber))
                    return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void setVisibilityOfCustomeAnsLayout(int position, boolean isVisible) {
        try {
            customeAnsList.get(position).setVisibleLayout(isVisible);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setIsNeedAnimationVariable() {
        for (int i = 0; i < customeAnsList.size(); i++) {
            if (customeAnsList.get(i).isVisibleLayout()) {
                isNeedAnimation = false;
                break;
            }
        }
    }

    private void clickOnWorkArea(int position, CustomeAns customeAns, ViewHolder vHolder) {
        setVisibilityOfCustomeAnsLayout(position, false);
        customeAnsList.get(position).setCorrectAns(lblAnswerOnWorkAreaCreditGiven);
        customeAnsList.get(position).setIsAnswerAvailable(0);
        //  this.notifyDataSetChanged();
        setAdapter();
    }

    public void makeans() {

        int int_pos = 0;

        for (int i = 0; i < customeAnsList.size(); i++) {

            try {

                if (customeAnsList.get(i).getFillInType() == 1) {
                    LinearLayout ll_anx_box = (LinearLayout) list_ans_box.get(int_pos);
                    int_pos = int_pos + 1;
                    String ans_value = makeansfromview(ll_anx_box);
                    customeAnsList.get(i).setCorrectAns(ans_value);
                    Log.e("anss", ans_value);
                }

            } catch (Exception e) {
            }

        }

        try {
            list_ans_box.clear();
        } catch (Exception e) {

        }
        list_ans_box = new ArrayList<LinearLayout>();


        //for making questions , Added By Yashwant
        if (this.isShowQuestion) {
            for (int i = 0; i < customeAnsList.size(); i++) {
                try {
                    LinearLayout ll_que_box = (LinearLayout) list_que_box.get(i);
                    String que_value = makeansfromview(ll_que_box);
                    customeAnsList.get(i).setQuestionString(que_value);
                } catch (Exception e) {
                }
            }

            try {
                list_que_box.clear();
            } catch (Exception e) {

            }
            list_que_box = new ArrayList<LinearLayout>();
        }
    }

    private void callclickmethod(final CustomeAns customeAns, final LinearLayout ll_ans_box
            , final int position) {

        try {
            //if (customeAns.getFillInType() == FILL_IN_TYPE) {
                LinearLayout ll_last = (LinearLayout) ll_ans_box.getChildAt((ll_ans_box.getChildCount() - 1));
                if (ll_last.getTag().toString().equals("text")) {
                    final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                    ed_centre.setSelection(ed_centre.getText().toString().length());
                    if(ed_centre.hasFocus()) {
                        ed_centre.clearFocus();
                    }
                    ed_centre.requestFocus();
                } else {
                    add_last_ed(ll_ans_box, 22, ll_ans_box.getChildCount());
                }
                showKeyboard();
            //}
        } catch (Exception e) {
        }
    }

    private void setVisibilityOfLayout(CustomeAns custom, RelativeLayout fillIntypeLayout
            , RelativeLayout multipleChoiceLayout, RelativeLayout trueFalseLayout) {
        if (custom.getFillInType() == FILL_IN_TYPE) {
            fillIntypeLayout.setVisibility(RelativeLayout.VISIBLE);
            multipleChoiceLayout.setVisibility(RelativeLayout.GONE);
            trueFalseLayout.setVisibility(RelativeLayout.GONE);
        } else {
            if (custom.getAnswerType() == MULTIPLE_CHOICE_ANS_TYPE) {
                fillIntypeLayout.setVisibility(RelativeLayout.GONE);
                multipleChoiceLayout.setVisibility(RelativeLayout.VISIBLE);
                trueFalseLayout.setVisibility(RelativeLayout.GONE);
            } else if (custom.getAnswerType() == TRUE_FALSE_ANS_TYPE
                    || custom.getAnswerType() == YES_NO_ANS_TYPE) {
                fillIntypeLayout.setVisibility(RelativeLayout.GONE);
                multipleChoiceLayout.setVisibility(RelativeLayout.GONE);
                trueFalseLayout.setVisibility(RelativeLayout.VISIBLE);
            }
        }
    }

    private void setVisibilityOfLayout(RelativeLayout layout, boolean bValue
            , boolean isMainLayout) {
        if (bValue) {
            layout.setVisibility(RelativeLayout.VISIBLE);
            if (!isMainLayout) {
                if (isNeedAnimation) {
                    MathFriendzyHelper.showViewFromRightToLeftAnimation(this, layout);
                } else {
                    isNeedAnimation = true;
                }
            }
        } else {
            layout.setVisibility(RelativeLayout.GONE);
        }
    }

    private void setMultiplechoiceLayout(final CustomeAns customeAns, ViewHolder vHolder) {
        if (customeAns.getFillInType() != FILL_IN_TYPE &&
                customeAns.getAnswerType() == MULTIPLE_CHOICE_ANS_TYPE) {
            vHolder.optionLayout.removeAllViews();
            for (int i = 0; i < customeAns.getMultipleChoiceOptionList().size(); i++) {
                String optionText = customeAns.getMultipleChoiceOptionList().get(i);
                View view = ActAssignCustomAns.this.getLayoutInflater().inflate(R.layout.check_home_work_option_layout, null);
                final TextView option = (TextView) view.findViewById(R.id.option);
                option.setText(optionText);

                if (customeAns.getCorrectAns().contains(optionText)) {
                    option.setBackgroundResource(R.drawable.checked_small_box_home_work);
                } else {
                    option.setBackgroundResource(R.drawable.unchecked_box_home_work);
                }

                option.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //setInvisibilityOfAllDialogLayout();
                        hideDeviceKeyboardAndClearFocus();
                        setIsNeedAnimationVariable();
                        String optionText = ((TextView) v).getText().toString();
                        String userCorrectAns = customeAns.getCorrectAns();
                        if (userCorrectAns.contains(optionText)) {
                            option.setBackgroundResource(R.drawable.unchecked_box_home_work);
                            userCorrectAns = userCorrectAns.replace(optionText, "");
                        } else {
                            option.setBackgroundResource(R.drawable.checked_small_box_home_work);
                            userCorrectAns = userCorrectAns + optionText;
                        }
                        customeAns.setCorrectAns(userCorrectAns);

                    }
                });
                vHolder.optionLayout.addView(view);
            }
        }
    }

    private void setTrueFalseOrYesNoLayout(final CustomeAns customeAns, final ViewHolder vHolder) {

        if (customeAns.getFillInType() != FILL_IN_TYPE) {
            if (customeAns.getAnswerType() == YES_NO_ANS_TYPE) {
                vHolder.ansTrue.setText(ActMultipleChoiceAnsSheet.YES);
                vHolder.ansFalse.setText(ActMultipleChoiceAnsSheet.NO);

                if (customeAns.getCorrectAns().equalsIgnoreCase(ActMultipleChoiceAnsSheet.YES)) {
                    vHolder.ansTrue.setBackgroundResource(R.drawable.checked_box_home_work);
                    vHolder.ansFalse.setBackgroundResource(R.drawable.unchecked_box_home_work);
                } else if (customeAns.getCorrectAns().equalsIgnoreCase(ActMultipleChoiceAnsSheet.NO)) {
                    vHolder.ansTrue.setBackgroundResource(R.drawable.unchecked_box_home_work);
                    vHolder.ansFalse.setBackgroundResource(R.drawable.checked_box_home_work);
                }
            } else if (customeAns.getAnswerType() == TRUE_FALSE_ANS_TYPE) {
                vHolder.ansTrue.setText(ActMultipleChoiceAnsSheet.TRUE);
                vHolder.ansFalse.setText(ActMultipleChoiceAnsSheet.FALSE);

                if (customeAns.getCorrectAns().equalsIgnoreCase(ActMultipleChoiceAnsSheet.TRUE)) {
                    vHolder.ansTrue.setBackgroundResource(R.drawable.checked_box_home_work);
                    vHolder.ansFalse.setBackgroundResource(R.drawable.unchecked_box_home_work);
                } else if (customeAns.getCorrectAns().equalsIgnoreCase
                        (ActMultipleChoiceAnsSheet.FALSE)) {
                    vHolder.ansTrue.setBackgroundResource(R.drawable.unchecked_box_home_work);
                    vHolder.ansFalse.setBackgroundResource(R.drawable.checked_box_home_work);
                }
            }

            vHolder.ansTrue.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (customeAns.getAnswerType() == YES_NO_ANS_TYPE) {
                        String userCorrectAns = customeAns.getCorrectAns();
                        customeAns.setCorrectAns(ActMultipleChoiceAnsSheet.YES);
                        vHolder.ansTrue.setBackgroundResource(R.drawable.checked_box_home_work);
                        vHolder.ansFalse.setBackgroundResource(R.drawable.unchecked_box_home_work);
                    } else if (customeAns.getAnswerType() == TRUE_FALSE_ANS_TYPE) {
                        customeAns.setCorrectAns(ActMultipleChoiceAnsSheet.TRUE);
                        vHolder.ansTrue.setBackgroundResource(R.drawable.checked_box_home_work);
                        vHolder.ansFalse.setBackgroundResource(R.drawable.unchecked_box_home_work);
                    }
                }
            });

            vHolder.ansFalse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setIsNeedAnimationVariable();
                    if (customeAns.getAnswerType() == YES_NO_ANS_TYPE) {
                        customeAns.setCorrectAns(ActMultipleChoiceAnsSheet.NO);
                        vHolder.ansTrue.setBackgroundResource(R.drawable.unchecked_box_home_work);
                        vHolder.ansFalse.setBackgroundResource(R.drawable.checked_box_home_work);
                    } else if (customeAns.getAnswerType() == TRUE_FALSE_ANS_TYPE) {
                        customeAns.setCorrectAns(ActMultipleChoiceAnsSheet.FALSE);
                        vHolder.ansTrue.setBackgroundResource(R.drawable.unchecked_box_home_work);
                        vHolder.ansFalse.setBackgroundResource(R.drawable.checked_box_home_work);
                    }
                }
            });
        }
    }

    private void setFillInLayout(final CustomeAns customeAns, final ViewHolder vHolder
            , final int position) {

        if (customeAns.getFillInType() == FILL_IN_TYPE) {
            if (customeAns.getIsLeftUnit() == 0) {
                vHolder.imgCheckRightUnit.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
                vHolder.imgCheckLeftUnit.setBackgroundResource(R.drawable.mf_check_box_ipad);
            } else {
                vHolder.imgCheckLeftUnit.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
                vHolder.imgCheckRightUnit.setBackgroundResource(R.drawable.mf_check_box_ipad);
            }

            vHolder.edtUnitOfMeasurement.setText(customeAns.getAnsSuffix());

            vHolder.ll_ans_box.setMinimumWidth(ActAssignCustomAns.dpwidth - 200);
            vHolder.ll_ans_box.setTag(R.id.first, "1");
            vHolder.ll_ans_box.setTag(R.id.second, "main");

            this.addansboxview(vHolder.ll_ans_box, customeAns.getCorrectAns(), customeAnsList.get(position).getIsAnswerAvailable());

            list_ans_box.add(vHolder.ll_ans_box);

            vHolder.imgCheckLeftUnit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideDeviceKeyboardAndClearFocus();
                    setIsNeedAnimationVariable();
                    customeAns.setIsLeftUnit(1);
                    setAdapter();
                }
            });

            vHolder.imgCheckRightUnit.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    hideDeviceKeyboardAndClearFocus();
                    setIsNeedAnimationVariable();
                    customeAns.setIsLeftUnit(0);
                    setAdapter();
                }
            });

            /*vHolder.edtUnitOfMeasurement.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    v.setFocusable(true);
                    v.setFocusableInTouchMode(true);
                    return false;
                }
            });*/

            vHolder.edtUnitOfMeasurement.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        customeAns.setAnsSuffix(((EditText) v).getText().toString());
                    }
                }
            });


            /*vHolder.edtFillInStudentAnswer.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    v.setFocusable(true);
                    v.setFocusableInTouchMode(true);
                    return false;
                }
            });
            */

            vHolder.edtFillInStudentAnswer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //isNeedAnimation = false;
                    if (customeAnsList.get(position).getIsAnswerAvailable() == MathFriendzyHelper.NO) {
                        MathFriendzyHelper.showWarningDialog(ActAssignCustomAns.this, lblAnswerOnWorkAreaCreditGiven);
                        return;
                    }
                    selectedTextView = ((EditText) v);
                    setTextToSelectedTextView(selectedTextView, "");
                    cleasFocusFromEditText();
                    hideDeviceKeyboard();
                    showKeyboard();
                }
            });

            if (MathFriendzyHelper.isEmpty(customeAns.getCorrectAns())
                    && (customeAnsList.get(position).getIsAnswerAvailable()
                    == MathFriendzyHelper.YES)) {
                this.setVisibilityOfEnterAnsHintText(vHolder, true);
            } else {
                this.setVisibilityOfEnterAnsHintText(vHolder, false);
            }


            /*vHolder.txtEnterAnsHint.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    v.setFocusable(true);
                    v.setFocusableInTouchMode(true);
                    return false;
                }
            });*/

            vHolder.txtEnterAnsHint.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setVisibilityOfEnterAnsHintText(vHolder, false);
                    try {
                       /* if (vHolder.ll_ans_box.getChildCount() > 0) {
                            vHolder.ll_ans_box.getChildAt(0).performClick();
                        }*/
                        callclickmethod(customeAns, vHolder.ll_ans_box, position);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public void addansboxview(final LinearLayout ll_ans_box, String correct_ans, final int is_ans_available) {  // siddhiinfosoft

        try {

            try {
                ll_ans_box.removeAllViews();
            } catch (Exception e) {

            }

            if (is_ans_available == MathFriendzyHelper.NO) {

                final LinearLayout ll_last = (LinearLayout) ActAssignCustomAns.this.getLayoutInflater().inflate(R.layout.kb_view_textview, null);
                ll_last.setTag("vtext");
                final TextView tv = (TextView) ll_last.findViewById(R.id.tv);
                String sttr = lblAnswerOnWorkAreaCreditGiven;
                tv.setText(sttr);

                tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MathFriendzyHelper.showWarningDialog(ActAssignCustomAns.this,
                                lblAnswerOnWorkAreaCreditGiven);
                    }
                });

                ll_ans_box.addView(ll_last);

            } else {

                if (correct_ans.trim().length() != 0) {
                    setstudentansboxsub(correct_ans, ll_ans_box);
                    Log.e("1234", "" + correct_ans);
                }

                boolean is_ed_last = false;

                if (ll_ans_box.getChildCount() > 0) {
                    LinearLayout lt = (LinearLayout) ll_ans_box.getChildAt(ll_ans_box.getChildCount() - 1);
                    if (lt.getTag().toString().equals("text")) {
                        is_ed_last = true;
                    } else {
                        is_ed_last = false;
                    }
                } else {
                    is_ed_last = false;
                }

                if (!is_ed_last) {

                    final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                    ll_last.setTag("text");
                    final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                    ed_centre.setMinWidth(20);
                    ll_last.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            custom_ed_touch(ed_centre, ll_last, ll_ans_box);
                        }
                    });
                    ed_centre.setOnTouchListener(new OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            custom_ed_touch(ed_centre, ll_last, ll_ans_box);
                            return false;
                        }
                    });
                    ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                selectedTextView = ed_centre;
                                ll_selected_view = ll_last;
                                ll_ans_box_selected = ll_ans_box;
                            }
                        }
                    });

                    ll_ans_box.addView(ll_last);

                }

            }

        } catch (Exception e) {

        }
    }


    public void showKeyboard() {

        try {    // siddhiinfosoft
            this.hideDeviceKeyboard();
            if (!is_keyboard_show) {
                is_keyboard_show = true;
                keyboardLayout.setVisibility(RelativeLayout.VISIBLE);
                animation = AnimationUtils.loadAnimation(this, R.anim.keyboard_show_animation);
                keyboardLayout.setAnimation(animation);

            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    hideKeyboardAfterSomeTime();
                }
            } , 10);
        } catch (Exception e) {

        }

    }

    public void hideKeyboard() {

        try {   // siddhiinfosoft
            if (is_keyboard_show) {
                is_keyboard_show = false;
                animation = AnimationUtils.loadAnimation(this, R.anim.keyboard_hide_animation);
                keyboardLayout.setAnimation(animation);
                keyboardLayout.setVisibility(RelativeLayout.GONE);
                //this.cleasFocusFromEditText();
                ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
                ll_main_eq_sub_symbol.setVisibility(View.GONE);
            }
        } catch (Exception e) {

        }

    }

    private String replacecharfromsign(final String str) {
        String str_final = str;

        try {

            String str_plus = "" + Html.fromHtml("&#43;");
            String str_minus = "" + Html.fromHtml("&#8722;");
            String str_equal = "" + Html.fromHtml("&#61;");
            String str_multi = "" + Html.fromHtml("&#215;");
            String str_divide = "" + Html.fromHtml("&#247;");
            String str_union = "" + Html.fromHtml("&#8746;");
            String str_intersection = "" + Html.fromHtml("&#8745;");
            String str_pi = "" + Html.fromHtml("&#960;");
            String str_factorial = "" + Html.fromHtml("&#33;");
            String str_percentage = "" + Html.fromHtml("&#37;");
            String str_goe = "" + Html.fromHtml("&#8805;");
            String str_loe = "" + Html.fromHtml("&#8804;");
            String str_grthan = "" + Html.fromHtml("&#62;");
            String str_lethan = "" + Html.fromHtml("&#60;");
            String str_infinity = "" + Html.fromHtml("&#8734;");
            String str_degree = "" + Html.fromHtml("&#176;");
            String str_xbar = "" + Html.fromHtml("x&#x0304;");
            //String str_muxbar = "" + Html.fromHtml("&#956;") + "" + Html.fromHtml("x&#772;");
            //String str_sigmaxbar = "" + Html.fromHtml("&#963;") + "" + Html.fromHtml("x&#772;");


            String str_integral = "" + Html.fromHtml("&#8747;");
            String str_summation = "" + Html.fromHtml("&#8721;");
            String str_alpha = "" + Html.fromHtml("&#945;");
            String str_theta = "" + Html.fromHtml("ø");
            String str_mu = "" + Html.fromHtml("&#956;");
            String str_sigma = "" + Html.fromHtml("&#963;");

            String str_beta = "" + Html.fromHtml("&#946;");
            String str_angle = "" + Html.fromHtml("&#8736;");
            String str_mangle = "" + Html.fromHtml("&#8737;");
            String str_sangle = "" + Html.fromHtml("&#8738;");
            String str_rangle = "" + Html.fromHtml("&#8735;");
            String str_triangle = "" + Html.fromHtml("&#9651;");
            String str_rectangle = "" + Html.fromHtml("&#9645;");
            String str_parallelogram = "" + Html.fromHtml("&#9649;");
            String str_perpendicular = "" + Html.fromHtml("&#8869;");
            String str_congruent = "" + Html.fromHtml("&#8773;");
            String str_similarty = "" + Html.fromHtml("&#8764;");
            String str_parallel = "" + Html.fromHtml("&#8741;");

            String str_arcm = "" + Html.fromHtml("&#8242;");
            String str_arcs = "" + Html.fromHtml("&#8243;");

            str_final = str_final.replace(str_plus, "op_plus");
            str_final = str_final.replace(str_minus, "op_minus");
            str_final = str_final.replace(str_equal, "op_equal");
            str_final = str_final.replace(str_multi, "op_multi");
            str_final = str_final.replace(str_divide, "op_divide");
            str_final = str_final.replace(str_union, "op_union");
            str_final = str_final.replace(str_intersection, "op_intersection");
            str_final = str_final.replace(str_pi, "op_pi");
            str_final = str_final.replace(str_factorial, "op_factorial");
            str_final = str_final.replace(str_percentage, "op_percentage");
            str_final = str_final.replace(str_goe, "op_goe");
            str_final = str_final.replace(str_loe, "op_loe");
            str_final = str_final.replace(str_grthan, "op_grthan");
            str_final = str_final.replace(str_lethan, "op_lethan");
            str_final = str_final.replace(str_infinity, "op_infinity");
            str_final = str_final.replace(str_degree, "op_degree");
            //str_final = str_final.replace(str_muxbar, "op_muxbar");
            //str_final = str_final.replace(str_sigmaxbar, "op_sigmaxbar");
            str_final = str_final.replace(str_xbar, "op_xbar");

            str_final = str_final.replace(str_integral, "op_integral");
            str_final = str_final.replace(str_summation, "op_summation");
            str_final = str_final.replace(str_alpha, "op_alpha");
            str_final = str_final.replace(str_theta, "op_theta");
            str_final = str_final.replace(str_mu, "op_mu");
            str_final = str_final.replace(str_sigma, "op_sigma");

            str_final = str_final.replace(str_beta, "op_beta");
            str_final = str_final.replace(str_angle, "op_angle");
            str_final = str_final.replace(str_mangle, "op_mangle");
            str_final = str_final.replace(str_sangle, "op_sangle");
            str_final = str_final.replace(str_rangle, "op_rangle");
            str_final = str_final.replace(str_triangle, "op_triangle");
            str_final = str_final.replace(str_rectangle, "op_rectangle");
            str_final = str_final.replace(str_parallelogram, "op_parallelogram");
            str_final = str_final.replace(str_perpendicular, "op_perpendicular");
            str_final = str_final.replace(str_congruent, "op_congruent");
            str_final = str_final.replace(str_similarty, "op_similarty");
            str_final = str_final.replace(str_parallel, "op_parallel");

            str_final = str_final.replace(str_arcm, "op_arcm");
            str_final = str_final.replace(str_arcs, "op_arcs");


            //by a1
            str_final = str_final.replace("-", "op_minus");

        } catch (Exception e) {

        }

        return str_final;
    }

    private String replacesignfromchar(final String str, final EditTextBlink ed) {
        String str_final = str;

        try {

            String str_plus = "" + Html.fromHtml("&#43;");
            String str_minus = "" + Html.fromHtml("&#8722;");
            String str_equal = "" + Html.fromHtml("&#61;");
            String str_multi = "" + Html.fromHtml("&#215;");
            String str_divide = "" + Html.fromHtml("&#247;");
            String str_union = "" + Html.fromHtml("&#8746;");
            String str_intersection = "" + Html.fromHtml("&#8745;");
            String str_pi = "" + Html.fromHtml("&#960;");
            String str_factorial = "" + Html.fromHtml("&#33;");
            String str_percentage = "" + Html.fromHtml("&#37;");
            String str_goe = "" + Html.fromHtml("&#8805;");
            String str_loe = "" + Html.fromHtml("&#8804;");
            String str_grthan = "" + Html.fromHtml("&#62;");
            String str_lethan = "" + Html.fromHtml("&#60;");
            String str_infinity = "" + Html.fromHtml("&#8734;");
            String str_degree = "" + Html.fromHtml("&#176;");
            String str_xbar = "" + Html.fromHtml("x&#x0304;");
            String str_muxbar = "" + Html.fromHtml("&#956;") + "" + Html.fromHtml("x&#772;");
            String str_sigmaxbar = "" + Html.fromHtml("&#963;") + "" + Html.fromHtml("x&#772;");


            String str_integral = "" + Html.fromHtml("&#8747;");
            String str_summation = "" + Html.fromHtml("&#8721;");
            String str_alpha = "" + Html.fromHtml("&#945;");
            String str_theta = "" + Html.fromHtml("ø");
            String str_mu = "" + Html.fromHtml("&#956;");
            String str_sigma = "" + Html.fromHtml("&#963;");

            String str_beta = "" + Html.fromHtml("&#946;");
            String str_angle = "" + Html.fromHtml("&#8736;");
            String str_mangle = "" + Html.fromHtml("&#8737;");
            String str_sangle = "" + Html.fromHtml("&#8738;");
            String str_rangle = "" + Html.fromHtml("&#8735;");
            String str_triangle = "" + Html.fromHtml("&#9651;");
            String str_rectangle = "" + Html.fromHtml("&#9645;");
            String str_parallelogram = "" + Html.fromHtml("&#9649;");
            String str_perpendicular = "" + Html.fromHtml("&#8869;");
            String str_congruent = "" + Html.fromHtml("&#8773;");
            String str_similarty = "" + Html.fromHtml("&#8764;");
            String str_parallel = "" + Html.fromHtml("&#8741;");

            String str_arcm = "" + Html.fromHtml("&#8242;");
            String str_arcs = "" + Html.fromHtml("&#8243;");

            str_final = str_final.replace("op_plus", str_plus);
            str_final = str_final.replace("op_minus", str_minus);
            str_final = str_final.replace("op_equal", str_equal);
            str_final = str_final.replace("op_multi", str_multi);
            str_final = str_final.replace("op_divide", str_divide);
            str_final = str_final.replace("op_union", str_union);
            str_final = str_final.replace("op_intersection", str_intersection);
            str_final = str_final.replace("op_pi", str_pi);
            str_final = str_final.replace("op_factorial", str_factorial);
            str_final = str_final.replace("op_percentage", str_percentage);
            str_final = str_final.replace("op_goe", str_goe);
            str_final = str_final.replace("op_loe", str_loe);
            str_final = str_final.replace("op_grthan", str_grthan);
            str_final = str_final.replace("op_lethan", str_lethan);
            str_final = str_final.replace("op_infinity", str_infinity);
            str_final = str_final.replace("op_degree", str_degree);
            str_final = str_final.replace("op_xbar", str_xbar);
            str_final = str_final.replace("op_muxbar", str_muxbar);
            str_final = str_final.replace("op_sigmaxbar", str_sigmaxbar);

            str_final = str_final.replace("op_alpha", str_alpha);
            str_final = str_final.replace("op_theta", str_theta);
            str_final = str_final.replace("op_mu", str_mu);
            str_final = str_final.replace("op_sigma", str_sigma);

            str_final = str_final.replace("op_beta", str_beta);
            str_final = str_final.replace("op_angle", str_angle);
            str_final = str_final.replace("op_mangle", str_mangle);
            str_final = str_final.replace("op_sangle", str_sangle);
            str_final = str_final.replace("op_rangle", str_rangle);
            str_final = str_final.replace("op_triangle", str_triangle);
            str_final = str_final.replace("op_rectangle", str_rectangle);
            str_final = str_final.replace("op_parallelogram", str_parallelogram);
            str_final = str_final.replace("op_perpendicular", str_perpendicular);
            str_final = str_final.replace("op_congruent", str_congruent);
            str_final = str_final.replace("op_similarty", str_similarty);
            str_final = str_final.replace("op_parallel", str_parallel);

            str_final = str_final.replace("op_arcm", str_arcm);
            str_final = str_final.replace("op_arcs", str_arcs);

            str_final = str_final.replace("op_integral", str_integral);
            str_final = str_final.replace("op_summation", str_summation);

            for (int i = 0; i < str_final.length(); i++) {

                String ss = String.valueOf(str_final.charAt(i));

                if (ss.equals(str_integral) || ss.equals(str_summation)) {
                    SpannableString ss1 = new SpannableString(ss);
                    ss1.setSpan(new RelativeSizeSpan(1.6f), 0, 1, 0);
                    ed.getText().insert(ed.getSelectionStart(), ss1);
                } else if (ss.equals(str_perpendicular) || ss.equals(str_parallel) ||
                        ss.equals(str_rectangle) || ss.equals(str_parallelogram)) {
                    SpannableString ss1 = new SpannableString(ss);
                    ss1.setSpan(new RelativeSizeSpan(1.2f), 0, 1, 0);
                    ed.getText().insert(ed.getSelectionStart(), ss1);
                } else if (ss.equals(str_angle) || ss.equals(str_mangle) ||
                        ss.equals(str_sangle) || ss.equals(str_rangle)) {
                    SpannableString ss1 = new SpannableString(ss);
                    ss1.setSpan(new RelativeSizeSpan(1.5f), 0, 1, 0);
                    ed.getText().insert(ed.getSelectionStart(), ss1);
                } else {
                    ed.getText().insert(ed.getSelectionStart(), ss);
                }

                if (ss.equals(str_integral)) {
                    ed.getText().insert(ed.getSelectionStart(), " ");
                }

            }

        } catch (Exception e) {

        }

        return str_final;
    }

    private void setvisibilityofabc123layout(int int_layout) { // siddhiinfosoft

        if (int_layout == 1) {
            kb_ll_123.setVisibility(View.VISIBLE);
            kb_ll_abc.setVisibility(View.GONE);
        } else {
            kb_ll_123.setVisibility(View.GONE);
            kb_ll_abc.setVisibility(View.VISIBLE);
        }

    }

    protected void setanimationtoeqsymbols1() {  // siddhiinfosoft
        try {
            animation = AnimationUtils.loadAnimation(this, R.anim.symbol_hide_animation);
            hsv_keyboard_eq_symbol.setAnimation(animation);
        } catch (Exception e) {

        }
    }

    protected void setanimationtoeqsymbols2() {
        try {
            animation = AnimationUtils.loadAnimation(this, R.anim.symbol_show_animation);
            hsv_keyboard_eq_symbol.setAnimation(animation);
        } catch (Exception e) {

        }

    }

    public void onclkeqsmbl(View v) {

        try {

            int id_view = v.getId();
            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            if (ll_visible_view != null) {
                ll_visible_view.setVisibility(View.GONE);
            }

            switch (id_view) {

                case R.id.kb_eq_symbol_basicmath_sign1:
                case R.id.kb_eq_symbol_basicmath_sign1_arrow:
                    ll_visible_view = ll_kb_sub_sumbol_basicmath_sign1;
                    Utils.setvisibility_basicmath_subeq(ll_kb_sub_sumbol_basicmath_sign1, ll_kb_sub_sumbol_basicmath_sign2, ll_kb_sub_sumbol_basicmath_sign5);

                    break;


                case R.id.kb_eq_symbol_basicmath_sign2:
                case R.id.kb_eq_symbol_basicmath_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_basicmath_sign2;
                    Utils.setvisibility_basicmath_subeq(ll_kb_sub_sumbol_basicmath_sign2, ll_kb_sub_sumbol_basicmath_sign1, ll_kb_sub_sumbol_basicmath_sign5);

                    break;

         /*       case R.id.kb_eq_symbol_basicmath_sign3:
              *//*  case R.id.kb_eq_symbol_basicmath_sign3_arrow:*//*

                    ll_visible_view = ll_kb_sub_sumbol_basicmath_sign3;
                    Utils.setvisibility_basicmath_subeq(ll_kb_sub_sumbol_basicmath_sign2, ll_kb_sub_sumbol_basicmath_sign1, ll_kb_sub_sumbol_basicmath_sign5);

                    break;
*/
              /*  case R.id.kb_eq_symbol_basicmath_sign4:


                    break;*/

                case R.id.kb_eq_symbol_basicmath_sign5:
                case R.id.kb_eq_symbol_basicmath_sign5_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_basicmath_sign5;
                    Utils.setvisibility_basicmath_subeq(ll_kb_sub_sumbol_basicmath_sign5, ll_kb_sub_sumbol_basicmath_sign1, ll_kb_sub_sumbol_basicmath_sign2);

                    break;

            /*    case R.id.kb_eq_symbol_basicmath_sign6:

                    break;

                case R.id.kb_eq_symbol_basicmath_sign7:

                    break;*/

                case R.id.kb_eq_symbol_prealgebra_sign1:
                case R.id.kb_eq_symbol_prealgebra_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_prealgebra_sign1;
                    Utils.setvisibility_prealgebra_subeq(ll_kb_sub_sumbol_prealgebra_sign1, ll_kb_sub_sumbol_prealgebra_sign2,
                            ll_kb_sub_sumbol_prealgebra_sign4, ll_kb_sub_sumbol_prealgebra_sign5);

                    break;

                case R.id.kb_eq_symbol_prealgebra_sign2:
                case R.id.kb_eq_symbol_prealgebra_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_prealgebra_sign2;
                    Utils.setvisibility_prealgebra_subeq(ll_kb_sub_sumbol_prealgebra_sign2, ll_kb_sub_sumbol_prealgebra_sign1,
                            ll_kb_sub_sumbol_prealgebra_sign4, ll_kb_sub_sumbol_prealgebra_sign5);

                    break;

   /*             case R.id.kb_eq_symbol_prealgebra_sign3:
//                case R.id.kb_eq_symbol_prealgebra_sign3_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_prealgebra_sign3;
                    Utils.setvisibility_prealgebra_subeq1(ll_kb_sub_sumbol_prealgebra_sign3, ll_kb_sub_sumbol_prealgebra_sign1, ll_kb_sub_sumbol_prealgebra_sign2,
                            ll_kb_sub_sumbol_prealgebra_sign4, ll_kb_sub_sumbol_prealgebra_sign5);


                    break;*/

                case R.id.kb_eq_symbol_prealgebra_sign4:
                case R.id.kb_eq_symbol_prealgebra_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_prealgebra_sign4;
                    Utils.setvisibility_prealgebra_subeq(ll_kb_sub_sumbol_prealgebra_sign4, ll_kb_sub_sumbol_prealgebra_sign2,
                            ll_kb_sub_sumbol_prealgebra_sign1, ll_kb_sub_sumbol_prealgebra_sign5);


                    break;

                case R.id.kb_eq_symbol_prealgebra_sign5:
                case R.id.kb_eq_symbol_prealgebra_sign5_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_prealgebra_sign5;
                    Utils.setvisibility_prealgebra_subeq(ll_kb_sub_sumbol_prealgebra_sign5, ll_kb_sub_sumbol_prealgebra_sign1, ll_kb_sub_sumbol_prealgebra_sign2,
                            ll_kb_sub_sumbol_prealgebra_sign4);


                    break;

            /*    case R.id.kb_eq_symbol_prealgebra_sign6:

                    break;

                case R.id.kb_eq_symbol_prealgebra_sign7:

                    break;*/

                case R.id.kb_eq_symbol_algebra_sign1:
                case R.id.kb_eq_symbol_algebra_sign1_arrow:
                case R.id.kb_eq_symbol_geometry_sign1:
                case R.id.kb_eq_symbol_geometry_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_algebra_sign1;
                    Utils.setvisibility_algebra_subeq(ll_kb_sub_sumbol_algebra_sign1, ll_kb_sub_sumbol_algebra_sign2,
                            ll_kb_sub_sumbol_algebra_sign4, ll_kb_sub_sumbol_algebra_sign5,
                            ll_kb_sub_sumbol_algebra_sign8);

                    break;

                case R.id.kb_eq_symbol_algebra_sign2:
                case R.id.kb_eq_symbol_algebra_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_algebra_sign2;
                    Utils.setvisibility_algebra_subeq(ll_kb_sub_sumbol_algebra_sign2, ll_kb_sub_sumbol_algebra_sign1,
                            ll_kb_sub_sumbol_algebra_sign4, ll_kb_sub_sumbol_algebra_sign5,
                            ll_kb_sub_sumbol_algebra_sign8);

                    break;

          /*      case R.id.kb_eq_symbol_algebra_sign3:
              *//*  case R.id.kb_eq_symbol_algebra_sign3_arrow:*//*

                    ll_visible_view = ll_kb_sub_sumbol_algebra_sign3;
                    Utils.setvisibility_algebra_subeq1(ll_kb_sub_sumbol_algebra_sign3, ll_kb_sub_sumbol_algebra_sign2, ll_kb_sub_sumbol_algebra_sign1,
                            ll_kb_sub_sumbol_algebra_sign4, ll_kb_sub_sumbol_algebra_sign5,
                            ll_kb_sub_sumbol_algebra_sign7, ll_kb_sub_sumbol_algebra_sign8);

                    break;*/

                case R.id.kb_eq_symbol_algebra_sign4:
                case R.id.kb_eq_symbol_algebra_sign4_arrow:
                case R.id.kb_eq_symbol_geometry_sign3:
                case R.id.kb_eq_symbol_geometry_sign3_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_algebra_sign4;
                    Utils.setvisibility_algebra_subeq(ll_kb_sub_sumbol_algebra_sign4, ll_kb_sub_sumbol_algebra_sign2,
                            ll_kb_sub_sumbol_algebra_sign1,
                            ll_kb_sub_sumbol_algebra_sign5,
                            ll_kb_sub_sumbol_algebra_sign8);

                    break;

                case R.id.kb_eq_symbol_algebra_sign5:
                case R.id.kb_eq_symbol_algebra_sign5_arrow:
                case R.id.kb_eq_symbol_geometry_sign4:
                case R.id.kb_eq_symbol_geometry_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_algebra_sign5;
                    Utils.setvisibility_algebra_subeq(ll_kb_sub_sumbol_algebra_sign5, ll_kb_sub_sumbol_algebra_sign4,
                            ll_kb_sub_sumbol_algebra_sign2, ll_kb_sub_sumbol_algebra_sign1,
                            ll_kb_sub_sumbol_algebra_sign8);

                    break;

             /*   case R.id.kb_eq_symbol_algebra_sign6:

                    break;

                case R.id.kb_eq_symbol_algebra_sign7:
                case R.id.kb_eq_symbol_algebra_sign7_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_algebra_sign7;
                    Utils.setvisibility_algebra_subeq1(ll_kb_sub_sumbol_algebra_sign7, ll_kb_sub_sumbol_algebra_sign5, ll_kb_sub_sumbol_algebra_sign4,
                            ll_kb_sub_sumbol_algebra_sign3,
                            ll_kb_sub_sumbol_algebra_sign2, ll_kb_sub_sumbol_algebra_sign1,
                            ll_kb_sub_sumbol_algebra_sign8);

                    break;*/

                case R.id.kb_eq_symbol_algebra_sign8:
                case R.id.kb_eq_symbol_algebra_sign8_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_algebra_sign8;
                    Utils.setvisibility_algebra_subeq(ll_kb_sub_sumbol_algebra_sign8, ll_kb_sub_sumbol_algebra_sign5, ll_kb_sub_sumbol_algebra_sign4,
                            ll_kb_sub_sumbol_algebra_sign2, ll_kb_sub_sumbol_algebra_sign1
                    );


                    break;

                case R.id.kb_eq_symbol_trignometry_sign1:
                case R.id.kb_eq_symbol_trignometry_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign1;
                    ll_kb_sub_sumbol_trignometry_sign1.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign2:
                case R.id.kb_eq_symbol_trignometry_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign2;
                    ll_kb_sub_sumbol_trignometry_sign2.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign3:

                    break;

                case R.id.kb_eq_symbol_trignometry_sign4:
                case R.id.kb_eq_symbol_trignometry_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign4;
                    ll_kb_sub_sumbol_trignometry_sign4.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign5:
                case R.id.kb_eq_symbol_trignometry_sign5_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign5;
                    ll_kb_sub_sumbol_trignometry_sign5.setVisibility(View.VISIBLE);

                    break;

              /*  case R.id.kb_eq_symbol_trignometry_sign6:

                    break;*/

                case R.id.kb_eq_symbol_trignometry_sign7:
                case R.id.kb_eq_symbol_trignometry_sign7_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign7;
                    ll_kb_sub_sumbol_trignometry_sign7.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign8:
                case R.id.kb_eq_symbol_trignometry_sign8_arrow:
                case R.id.kb_eq_symbol_geometry_sign8:
                case R.id.kb_eq_symbol_geometry_sign8_arrow:
                case R.id.kb_eq_symbol_calculus_sign11:
                case R.id.kb_eq_symbol_calculus_sign11_arrow:
                case R.id.kb_eq_symbol_precalculus_sign10:
                case R.id.kb_eq_symbol_precalculus_sign10_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign8;
                    ll_kb_sub_sumbol_trignometry_sign8.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign9:
                case R.id.kb_eq_symbol_trignometry_sign9_arrow:
                case R.id.kb_eq_symbol_geometry_sign9:
                case R.id.kb_eq_symbol_geometry_sign9_arrow:
                case R.id.kb_eq_symbol_calculus_sign12:
                case R.id.kb_eq_symbol_calculus_sign12_arrow:
                case R.id.kb_eq_symbol_precalculus_sign11:
                case R.id.kb_eq_symbol_precalculus_sign11_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign9;
                    ll_kb_sub_sumbol_trignometry_sign9.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign10:
                case R.id.kb_eq_symbol_trignometry_sign10_arrow:
                case R.id.kb_eq_symbol_geometry_sign10:
                case R.id.kb_eq_symbol_geometry_sign10_arrow:
                case R.id.kb_eq_symbol_calculus_sign13:
                case R.id.kb_eq_symbol_calculus_sign13_arrow:
                case R.id.kb_eq_symbol_precalculus_sign12:
                case R.id.kb_eq_symbol_precalculus_sign12_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign10;
                    ll_kb_sub_sumbol_trignometry_sign10.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign11:
                case R.id.kb_eq_symbol_trignometry_sign11_arrow:
                case R.id.kb_eq_symbol_geometry_sign11:
                case R.id.kb_eq_symbol_geometry_sign11_arrow:
                case R.id.kb_eq_symbol_calculus_sign14:
                case R.id.kb_eq_symbol_calculus_sign14_arrow:
                case R.id.kb_eq_symbol_precalculus_sign13:
                case R.id.kb_eq_symbol_precalculus_sign13_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign11;
                    ll_kb_sub_sumbol_trignometry_sign11.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign12:
                case R.id.kb_eq_symbol_trignometry_sign12_arrow:
                case R.id.kb_eq_symbol_geometry_sign12:
                case R.id.kb_eq_symbol_geometry_sign12_arrow:
                case R.id.kb_eq_symbol_calculus_sign15:
                case R.id.kb_eq_symbol_calculus_sign15_arrow:
                case R.id.kb_eq_symbol_precalculus_sign14:
                case R.id.kb_eq_symbol_precalculus_sign14_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign12;
                    ll_kb_sub_sumbol_trignometry_sign12.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign13:
                case R.id.kb_eq_symbol_trignometry_sign13_arrow:
                case R.id.kb_eq_symbol_geometry_sign13:
                case R.id.kb_eq_symbol_geometry_sign13_arrow:
                case R.id.kb_eq_symbol_calculus_sign16:
                case R.id.kb_eq_symbol_calculus_sign16_arrow:
                case R.id.kb_eq_symbol_precalculus_sign15:
                case R.id.kb_eq_symbol_precalculus_sign15_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign13;
                    ll_kb_sub_sumbol_trignometry_sign13.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_precalculus_sign1:
                case R.id.kb_eq_symbol_precalculus_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_precalculus_sign1;
                    ll_kb_sub_sumbol_precalculus_sign1.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_precalculus_sign2:
                case R.id.kb_eq_symbol_precalculus_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_precalculus_sign2;
                    ll_kb_sub_sumbol_precalculus_sign2.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_precalculus_sign3:

                    break;

                case R.id.kb_eq_symbol_precalculus_sign4:
                case R.id.kb_eq_symbol_precalculus_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_precalculus_sign4;
                    ll_kb_sub_sumbol_precalculus_sign4.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_precalculus_sign5:
                case R.id.kb_eq_symbol_precalculus_sign5_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_precalculus_sign5;
                    ll_kb_sub_sumbol_precalculus_sign5.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_precalculus_sign6:

                    break;

                case R.id.kb_eq_symbol_precalculus_sign7:

                    break;

              /*  case R.id.kb_eq_symbol_precalculus_sign8:

                    break;*/

                case R.id.kb_eq_symbol_precalculus_sign9:
                case R.id.kb_eq_symbol_precalculus_sign9_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_precalculus_sign9;
                    ll_kb_sub_sumbol_precalculus_sign9.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_geometry_sign2:
                case R.id.kb_eq_symbol_geometry_sign2_arrow:

                    ll_visible_view = ll_kb_sub_symbol_geometry_sign2;
                    ll_kb_sub_symbol_geometry_sign2.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_geometry_sign5:
                case R.id.kb_eq_symbol_geometry_sign5_arrow:

                    ll_visible_view = ll_kb_sub_symbol_geometry_sign5;
                    ll_kb_sub_symbol_geometry_sign5.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_geometry_sign6:
                case R.id.kb_eq_symbol_geometry_sign6_arrow:

                    ll_visible_view = ll_kb_sub_symbol_geometry_sign6;
                    ll_kb_sub_symbol_geometry_sign6.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_geometry_sign7:
                case R.id.kb_eq_symbol_geometry_sign7_arrow:

                    ll_visible_view = ll_kb_sub_symbol_geometry_sign7;
                    ll_kb_sub_symbol_geometry_sign7.setVisibility(View.VISIBLE);

                    break;

              /*  case R.id.kb_eq_symbol_precalculus_sign10:
                case R.id.kb_eq_symbol_precalculus_sign10_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_precalculus_sign10;
                    ll_kb_sub_sumbol_precalculus_sign10.setVisibility(View.VISIBLE);

                    break;*/

             /*   case R.id.kb_eq_symbol_precalculus_sign11:
                case R.id.kb_eq_symbol_precalculus_sign11_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_precalculus_sign11;
                    ll_kb_sub_sumbol_precalculus_sign11.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_precalculus_sign12:
                case R.id.kb_eq_symbol_precalculus_sign12_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_precalculus_sign12;
                    ll_kb_sub_sumbol_precalculus_sign12.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_precalculus_sign13:
                case R.id.kb_eq_symbol_precalculus_sign13_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_precalculus_sign13;
                    ll_kb_sub_sumbol_precalculus_sign13.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_precalculus_sign14:
                case R.id.kb_eq_symbol_precalculus_sign14_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_precalculus_sign14;
                    ll_kb_sub_sumbol_precalculus_sign14.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_precalculus_sign15:
                case R.id.kb_eq_symbol_precalculus_sign15_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_precalculus_sign15;
                    ll_kb_sub_sumbol_precalculus_sign15.setVisibility(View.VISIBLE);

                    break;*/

                case R.id.kb_eq_symbol_calculus_sign1:
                case R.id.kb_eq_symbol_calculus_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign1;
                    ll_kb_sub_sumbol_calculus_sign1.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_calculus_sign2:
                case R.id.kb_eq_symbol_calculus_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign2;
                    ll_kb_sub_sumbol_calculus_sign2.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_calculus_sign3:

                    break;

                case R.id.kb_eq_symbol_calculus_sign4:
                case R.id.kb_eq_symbol_calculus_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign4;
                    ll_kb_sub_sumbol_calculus_sign4.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_calculus_sign5:
                case R.id.kb_eq_symbol_calculus_sign5_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign5;
                    ll_kb_sub_sumbol_calculus_sign5.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_calculus_sign6:

                    break;

                case R.id.kb_eq_symbol_calculus_sign7:

                    break;

             /*   case R.id.kb_eq_symbol_calculus_sign8:
              *//*  case R.id.kb_eq_symbol_calculus_sign8_arrow:*//*

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign8;
                    ll_kb_sub_sumbol_calculus_sign8.setVisibility(View.VISIBLE);

                    break;*/

               /* case R.id.kb_eq_symbol_calculus_sign9:

                    break;*/

                case R.id.kb_eq_symbol_calculus_sign10:
                case R.id.kb_eq_symbol_calculus_sign10_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign10;
                    ll_kb_sub_sumbol_calculus_sign10.setVisibility(View.VISIBLE);

                    break;

              /*  case R.id.kb_eq_symbol_calculus_sign11:
                case R.id.kb_eq_symbol_calculus_sign11_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign11;
                    ll_kb_sub_sumbol_calculus_sign11.setVisibility(View.VISIBLE);

                    break;*/

            /*    case R.id.kb_eq_symbol_calculus_sign12:
                case R.id.kb_eq_symbol_calculus_sign12_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign12;
                    ll_kb_sub_sumbol_calculus_sign12.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_calculus_sign13:
                case R.id.kb_eq_symbol_calculus_sign13_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign13;
                    ll_kb_sub_sumbol_calculus_sign13.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_calculus_sign14:
                case R.id.kb_eq_symbol_calculus_sign14_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign14;
                    ll_kb_sub_sumbol_calculus_sign14.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_calculus_sign15:
                case R.id.kb_eq_symbol_calculus_sign15_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign15;
                    ll_kb_sub_sumbol_calculus_sign15.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_calculus_sign16:
                case R.id.kb_eq_symbol_calculus_sign16_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign16;
                    ll_kb_sub_sumbol_calculus_sign16.setVisibility(View.VISIBLE);

                    break;*/

                case R.id.kb_eq_symbol_statistics_sign1:
                case R.id.kb_eq_symbol_statistics_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_statistics_sign1;
                    ll_kb_sub_sumbol_statistics_sign1.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_statistics_sign2:

                    break;

                case R.id.kb_eq_symbol_statistics_sign3:
                case R.id.kb_eq_symbol_statistics_sign3_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_statistics_sign3;
                    ll_kb_sub_sumbol_statistics_sign3.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_statistics_sign4:
                case R.id.kb_eq_symbol_statistics_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_statistics_sign4;
                    ll_kb_sub_sumbol_statistics_sign4.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_statistics_sign5:

                    break;

                case R.id.kb_eq_symbol_statistics_sign6:
                case R.id.kb_eq_symbol_statistics_sign6_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_statistics_sign6;
                    ll_kb_sub_sumbol_statistics_sign6.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_statistics_sign7:
                case R.id.kb_eq_symbol_statistics_sign7_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_statistics_sign7;
                    ll_kb_sub_sumbol_statistics_sign7.setVisibility(View.VISIBLE);

                    break;

             /*   case R.id.kb_eq_symbol_statistics_sign8:
                case R.id.kb_eq_symbol_statistics_sign8_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_statistics_sign8;
                    ll_kb_sub_sumbol_statistics_sign8.setVisibility(View.VISIBLE);

                    break;*/

                case R.id.kb_eq_symbol_finitemath_sign1:
                case R.id.kb_eq_symbol_finitemath_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_finitemath_sign1;
                    ll_kb_sub_sumbol_finitemath_sign1.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_finitemath_sign2:
                case R.id.kb_eq_symbol_finitemath_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_finitemath_sign2;
                    ll_kb_sub_sumbol_finitemath_sign2.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_finitemath_sign3:

                    break;

                case R.id.kb_eq_symbol_finitemath_sign4:
                case R.id.kb_eq_symbol_finitemath_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_finitemath_sign4;
                    ll_kb_sub_sumbol_finitemath_sign4.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_finitemath_sign5:
                case R.id.kb_eq_symbol_finitemath_sign5_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_finitemath_sign5;
                    ll_kb_sub_sumbol_finitemath_sign5.setVisibility(View.VISIBLE);

                    break;

        /*        case R.id.kb_eq_symbol_finitemath_sign6:
                case R.id.kb_eq_symbol_finitemath_sign6_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_finitemath_sign6;
                    ll_kb_sub_sumbol_finitemath_sign6.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_finitemath_sign7:

                    break;*/

                case R.id.kb_eq_symbol_finitemath_sign8:
                case R.id.kb_eq_symbol_finitemath_sign8_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_finitemath_sign8;
                    ll_kb_sub_sumbol_finitemath_sign8.setVisibility(View.VISIBLE);

                    break;

          /*      case R.id.kb_eq_symbol_linearalgebra_sign1:
                case R.id.kb_eq_symbol_linearalgebra_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_linearalgebra_sign1;
                    ll_kb_sub_sumbol_linearalgebra_sign1.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_linearalgebra_sign2:

                    break;

                case R.id.kb_eq_symbol_linearalgebra_sign3:
                case R.id.kb_eq_symbol_linearalgebra_sign3_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_linearalgebra_sign3;
                    ll_kb_sub_sumbol_linearalgebra_sign3.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_linearalgebra_sign4:
                case R.id.kb_eq_symbol_linearalgebra_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_linearalgebra_sign4;
                    ll_kb_sub_sumbol_linearalgebra_sign4.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_linearalgebra_sign5:
                case R.id.kb_eq_symbol_linearalgebra_sign5_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_linearalgebra_sign5;
                    ll_kb_sub_sumbol_linearalgebra_sign5.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_chemistry_sign1:

                    break;

                case R.id.kb_eq_symbol_chemistry_sign2:

                    break;

                case R.id.kb_eq_symbol_chemistry_sign3:
                case R.id.kb_eq_symbol_chemistry_sign3_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_chemistry_sign3;
                    ll_kb_sub_sumbol_chemistry_sign3.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_chemistry_sign4:
                case R.id.kb_eq_symbol_chemistry_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_chemistry_sign4;
                    ll_kb_sub_sumbol_chemistry_sign4.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_chemistry_sign5:
                case R.id.kb_eq_symbol_chemistry_sign5_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_chemistry_sign5;
                    ll_kb_sub_sumbol_chemistry_sign5.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_chemistry_sign6:
                case R.id.kb_eq_symbol_chemistry_sign6_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_chemistry_sign6;
                    ll_kb_sub_sumbol_chemistry_sign6.setVisibility(View.VISIBLE);

                    break;
*/
            }
        } catch (Exception e) {

        }
    }

    public void onkbclick(View v) {
        try {


            int id_view = v.getId();

            switch (id_view) {

                case R.id.btn_eq_name_pre_scroll:

                    this.callscrollmethod(1);

                    break;

                case R.id.btn_eq_name_next_scroll:

                    this.callscrollmethod(2);

                    break;

                case R.id.btn_eq_symbol_pre_scroll:

                    this.callscrollmethod(3);

                    break;

                case R.id.btn_eq_symbol_next_scroll:

                    this.callscrollmethod(4);

                    break;

                case R.id.kb_btn_abc_close:

                    hideKeyboard();

                    break;

                case R.id.kb_btn_123_close:

                    hideKeyboard();

                    break;

                case R.id.kb_btn_abc:

                    setvisibilityofabc123layout(2);

                    break;

                case R.id.kb_btn_123:

                    setvisibilityofabc123layout(1);

                    break;

                case R.id.kb_btn_abc_case:

                    this.changecasevalue();

                    break;

                case R.id.kb_tv_eq_name_basicmath:

                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_basicmath, kb_tv_eq_name_geometry, kb_tv_eq_name_prealgebra, kb_tv_eq_name_algebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {


                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_basicmath, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_trignometry,
                                    kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();

                        }
                    }, 500);

                    this.setvisibility_scroll_btn(false);

                    break;

                case R.id.kb_tv_eq_name_prealgebra:

                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_prealgebra, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_algebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler2 = new Handler();
                    handler2.postDelayed(new Runnable() {
                        @Override
                        public void run() {


                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_basicmath, kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_trignometry,
                                    kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();

                        }
                    }, 500);

                    this.setvisibility_scroll_btn(false);

                    break;

                case R.id.kb_tv_eq_name_algebra:

                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_algebra, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler3 = new Handler();
                    handler3.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath, kb_ll_eq_symbol_trignometry,
                                    kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;

                case R.id.kb_tv_eq_name_geometry:

                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_geometry, kb_tv_eq_name_algebra, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler33 = new Handler();
                    handler33.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath, kb_ll_eq_symbol_trignometry,
                                    kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;

                case R.id.kb_tv_eq_name_trignometry:
                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_trignometry, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_algebra,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler4 = new Handler();
                    handler4.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_trignometry, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath,
                                    kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;

                case R.id.kb_tv_eq_name_precalculus:
                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_precalculus, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_algebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_calculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler5 = new Handler();
                    handler5.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_trignometry, kb_ll_eq_symbol_algebra,
                                    kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath,
                                    kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;

                case R.id.kb_tv_eq_name_calculus:
                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_calculus, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_algebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler6 = new Handler();
                    handler6.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_trignometry,
                                    kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath,
                                    kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;

                case R.id.kb_tv_eq_name_statistics:
                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_statistics, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_algebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_finitemath);

                    Handler handler7 = new Handler();
                    handler7.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_precalculus,
                                    kb_ll_eq_symbol_trignometry, kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath,
                                    kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;

                case R.id.kb_tv_eq_name_finitemath:
                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_finitemath, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_algebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_statistics);

                    Handler handler8 = new Handler();
                    handler8.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_finitemath, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_calculus,
                                    kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_trignometry, kb_ll_eq_symbol_algebra,
                                    kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;


            }
            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);

        } catch (Exception e) {

        }
    }

    public void onkb_abc_click(View v) { // Added By Siddhi Info Soft

        try {

            String btn_tag = (String) v.getTag();
            this.changeCase(btn_tag);
            this.setTextToSelectedTextView(selectedTextView, btn_tag);
            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);

        } catch (Exception e) {

        }

    }

    public void onkb_123_click(View v) { // Added By Siddhi Info Soft

        try {

            String btn_tag = String.valueOf(v.getTag());
            this.setTextToSelectedTextView(selectedTextView, btn_tag);
            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);

        } catch (Exception e) {

        }

    }

    public void onkb_123_symbol_click(View v) { // Added By Siddhi Info Soft

        try {

            String btn_tag = String.valueOf(v.getTag());
            this.add_sign_into_box(btn_tag);
            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);

        } catch (Exception e) {

        }

    }

    /*public void onkb_sub_symbol_click(View v) { // Added By Siddhi Info Soft

        try {

            String btn_tag = String.valueOf(v.getTag());

            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);

            if (btn_tag.equals("fraction") ||
                    btn_tag.equals("pointview")
                    || btn_tag.equals("lim") || btn_tag.equals("parenthesis") || btn_tag.equals("absolute") | btn_tag.equals("squareroot") || btn_tag.equals("nthroot")
                    || btn_tag.equals("super_script") || btn_tag.equals("sub_script") || btn_tag.equals("super_sub_script")
                    || btn_tag.equals("line") || btn_tag.equals("lsegment") || btn_tag.equals("ray")
                    || btn_tag.equals("arc")) {
                this.addviewtoselectedview(ll_ans_box_selected, btn_tag, "view");
            } else if (btn_tag.equals("absolute")) {
                setTextToSelectedTextView(selectedTextView, "|  |");
            } else {
                this.add_sign_into_box(btn_tag);
            }

        } catch (Exception e) {

        }

    }*/

    private void add_sign_into_box(final String tag) {

        String str_text = "";
        boolean is_operator = false;

        if (tag.equals("op_plus")) {
            str_text = "" + Html.fromHtml("&#43;");
            is_operator = true;
        } else if (tag.equals("op_minus")) {
            str_text = "" + Html.fromHtml("&#8722;");
            is_operator = true;
        } else if (tag.equals("op_equal")) {
            str_text = "" + Html.fromHtml("&#61;");
            is_operator = true;
        } else if (tag.equals("op_multi")) {
            str_text = "" + Html.fromHtml("&#215;");
            is_operator = true;
        } else if (tag.equals("op_divide")) {
            str_text = "" + Html.fromHtml("&#247;");
            is_operator = true;
        } else if (tag.equals("op_union")) {
            str_text = "" + Html.fromHtml("&#8746;");
        } else if (tag.equals("op_intersection")) {
            str_text = "" + Html.fromHtml("&#8745;");
        } else if (tag.equals("op_pi")) {
            str_text = "" + Html.fromHtml("&#960;");
        } else if (tag.equals("op_factorial")) {
            str_text = "" + Html.fromHtml("&#33;");
        } else if (tag.equals("op_percentage")) {
            str_text = "" + Html.fromHtml("&#37;");
        } else if (tag.equals("op_rarrow")) {
            str_text = "" + Html.fromHtml("&#8594;");
        } else if (tag.equals("op_xbar")) {
            str_text = "" + Html.fromHtml("x&#x0304;");
        } else if (tag.equals("op_muxbar")) {
            str_text = "" + Html.fromHtml("&#956;") + "" + Html.fromHtml("x&#772;");
        } else if (tag.equals("op_sigmaxbar")) {
            str_text = "" + Html.fromHtml("&#963;") + "" + Html.fromHtml("x&#772;");
        } else if (tag.equals("op_goe")) {
            str_text = "" + Html.fromHtml("&#8805;");
            is_operator = true;
        } else if (tag.equals("op_loe")) {
            str_text = "" + Html.fromHtml("&#8804;");
            is_operator = true;
        } else if (tag.equals("op_grthan")) {
            str_text = "" + Html.fromHtml("&#62;");
            is_operator = true;
        } else if (tag.equals("op_lethan")) {
            str_text = "" + Html.fromHtml("&#60;");
            is_operator = true;
        } else if (tag.equals("op_infinity")) {
            str_text = "" + Html.fromHtml("&#8734;");
        } else if (tag.equals("op_degree")) {
            str_text = "" + Html.fromHtml("&#176;");
        } else if (tag.equals("op_integral")) {
            str_text = " " + Html.fromHtml("&#8747;");
        } else if (tag.equals("op_summation")) {
            str_text = " " + Html.fromHtml("&#8721;");
        } else if (tag.equals("op_alpha")) {
            str_text = " " + Html.fromHtml("&#945;");
            //  str_text = " "+Html.fromHtml("&#913;");
        } else if (tag.equals("op_theta")) {
            str_text = " ø";
        } else if (tag.equals("op_mu")) {
            str_text = " " + Html.fromHtml("&#956;");
        } else if (tag.equals("op_sigma")) {
            str_text = " " + Html.fromHtml("&#963;");
        } else if (tag.equals("op_beta")) {
            str_text = " " + Html.fromHtml("&#946;");
            //  str_text = " "+Html.fromHtml("&#7526;");
        } else if (tag.equals("op_angle")) {
            str_text = " " + Html.fromHtml("&#8736;");
        } else if (tag.equals("op_mangle")) {
            str_text = " " + Html.fromHtml("&#8737;");
        } else if (tag.equals("op_sangle")) {
            str_text = " " + Html.fromHtml("&#8738;");
        } else if (tag.equals("op_rangle")) {
            str_text = " " + Html.fromHtml("&#8735;");
        } else if (tag.equals("op_triangle")) {
            str_text = " " + Html.fromHtml("&#9651;");
        } else if (tag.equals("op_rectangle")) {
            str_text = " " + Html.fromHtml("&#9645;");
        } else if (tag.equals("op_parallelogram")) {
            str_text = " " + Html.fromHtml("&#9649;");
        } else if (tag.equals("op_line")) {
            str_text = " " + Html.fromHtml("&#963;");
        } else if (tag.equals("op_lsegment")) {
            str_text = " " + Html.fromHtml("&#963;");
        } else if (tag.equals("op_ray")) {
            str_text = " " + Html.fromHtml("&#963;");
        } else if (tag.equals("op_arc")) {
            str_text = " " + Html.fromHtml("&#963;");
        } else if (tag.equals("op_perpendicular")) {
            str_text = " " + Html.fromHtml("&#8869;");
        } else if (tag.equals("op_congruent")) {
            str_text = " " + Html.fromHtml("&#8773;");
        } else if (tag.equals("op_similarty")) {
            str_text = " " + Html.fromHtml("&#8764;");
        } else if (tag.equals("op_parallel")) {
            str_text = " " + Html.fromHtml("&#8741;");
        } else if (tag.equals("op_arcm")) {
            str_text = "" + Html.fromHtml("&#8242;");
        } else if (tag.equals("op_arcs")) {
            str_text = "" + Html.fromHtml("&#8243;");
        } else {
            str_text = tag;
        }

        if (is_operator) {
            setTextToSelectedTextView2(selectedTextView, " " + str_text + " ");
        } else {
            setTextToSelectedTextView2(selectedTextView, str_text);
        }

    }

    public void onkb_sum_symbol_click(View v) {

        try {

            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);

            String btn_tag = String.valueOf(v.getTag());
            String str_text = "";
            if (btn_tag.equals("op_integral")) {
                str_text = " " + Html.fromHtml("&#8747;");
            } else if (btn_tag.equals("op_summation")) {
                str_text = " " + Html.fromHtml("&#8721;");
            } else if (btn_tag.equals("op_perpendicular")) {
                str_text = " " + Html.fromHtml("&#8869;");
            } else if (btn_tag.equals("op_parallel")) {
                str_text = " " + Html.fromHtml("&#8741;");
            } else if (btn_tag.equals("op_rectangle")) {
                str_text = " " + Html.fromHtml("&#9645;");
            } else if (btn_tag.equals("op_parallelogram")) {
                str_text = " " + Html.fromHtml("&#9649;");
            } else if (btn_tag.equals("op_angle")) {
                str_text = " " + Html.fromHtml("&#8736;");
            } else if (btn_tag.equals("op_mangle")) {
                str_text = " " + Html.fromHtml("&#8737;");
            } else if (btn_tag.equals("op_sangle")) {
                str_text = " " + Html.fromHtml("&#8738;");
            } else if (btn_tag.equals("op_rangle")) {
                str_text = " " + Html.fromHtml("&#8735;");
            }

            if (btn_tag.equals("op_integral") || btn_tag.equals("op_summation")) {
                SpannableString ss1 = new SpannableString(str_text);
                ss1.setSpan(new RelativeSizeSpan(1.6f), 1, 2, 0);
                selectedTextView.getText().insert(selectedTextView.getSelectionStart(), ss1);
            } else if (btn_tag.equals("op_perpendicular") || btn_tag.equals("op_parallel") ||
                    btn_tag.equals("op_rectangle") || btn_tag.equals("op_parallelogram")) {
                SpannableString ss1 = new SpannableString(str_text);
                ss1.setSpan(new RelativeSizeSpan(1.2f), 1, 2, 0);
                selectedTextView.getText().insert(selectedTextView.getSelectionStart(), ss1);
            } else if (btn_tag.equals("op_angle") || btn_tag.equals("op_mangle") ||
                    btn_tag.equals("op_sangle") || btn_tag.equals("op_rangle")) {
                SpannableString ss1 = new SpannableString(str_text);
                ss1.setSpan(new RelativeSizeSpan(1.5f), 1, 2, 0);
                selectedTextView.getText().insert(selectedTextView.getSelectionStart(), ss1);
            }

            if (btn_tag.equals("op_integral")) {
                selectedTextView.getText().insert(selectedTextView.getSelectionStart(), " ");
            }


//            SpannableString ss1=  new SpannableString(str_text);
//            ss1.setSpan(new RelativeSizeSpan(2f), 1,2, 0);
//            selectedTextView.getText().insert(selectedTextView.getSelectionStart(), ss1);

        } catch (Exception e) {

        }

    }

    private void addnewed(final LinearLayout ll_main, final String str_cut, final int pos, final int text_size) {

//        String sss = String.valueOf(ll_main.getTag(R.id.first));
//        int tag1 = Integer.valueOf(sss);
//        String tag2 = String.valueOf(ll_main.getTag(R.id.second));
//        int text_size = 22;
//
//        try {
//
//            if (tag2.equals("super_top") || tag2.equals("sub_bottom") || tag2.equals("ss_top") ||
//                    tag2.equals("ss_bottom") || tag2.equals("nth_top") || tag2.equals("lim_left") || tag2.equals("lim_right")){
//                text_size = (text_size - 3)-(tag1+1);
//            } else if(tag2.equals("frac_top") || tag2.equals("frac_bottom") || tag2.equals("sqrt_c") || tag2.equals("nth_bottom")
//                    || tag2.equals("parenthesis_center")){
//                text_size = text_size-(tag1+1);
//            }
//
//        } catch (Exception e){
//
//        }

        final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
        ll_last.setTag("text");
        final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
        // ed_centre.setText(str_cut);
        this.replacesignfromchar(str_cut, ed_centre);
        ed_centre.setMinWidth(20);
        ed_centre.setTextSize(text_size);
        ll_last.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
            }
        });
        ed_centre.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
                return false;
            }
        });
        ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
                    selectedTextView = ed_centre;
                    ll_selected_view = ll_last;
                    selected_ed_2 = null;
                    ll_ans_box_selected = ll_main;
                }
            }
        });

        ll_main.addView(ll_last, (pos + 2));

        //   }

    }

    private void add_last_ed(final LinearLayout ll_main, final int textsize, final int pos) {

        String sss = String.valueOf(ll_main.getTag(R.id.first));
        int tag1 = Integer.valueOf(sss);
        String tag2 = String.valueOf(ll_main.getTag(R.id.second));
        int text_size = ActAssignCustomAns.this.getResources().getInteger(R.integer.kb_textsize_main_int);

        try {

            if (tag2.equals("super_top") || tag2.equals("sub_bottom") || tag2.equals("ss_top") ||
                    tag2.equals("ss_bottom") || tag2.equals("nth_top") || tag2.equals("lim_left") || tag2.equals("lim_right")) {
                text_size = (text_size - 1) - (tag1 + 1);
            } else if (tag2.equals("frac_top") || tag2.equals("frac_bottom") || tag2.equals("sqrt_c") || tag2.equals("nth_bottom")
                    || tag2.equals("parenthesis_center") || tag2.equals("absolute_center")
                    || tag2.equals("line_center") || tag2.equals("lsegment_center")
                    || tag2.equals("ray_center") || tag2.equals("arc_center")) {
                text_size = text_size - (tag1 + 1);
            }

        } catch (Exception e) {

        }
        if (text_size < 5) {
            text_size = 5;
        }

        final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
        ll_last.setTag("text");
        final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
        ed_centre.setMinWidth(3);
        ed_centre.setMinimumWidth(3);
        ed_centre.requestLayout();
        ed_centre.setTextSize(text_size);
        ll_last.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
            }
        });
        ed_centre.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
                return false;
            }
        });
        ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    selectedTextView = ed_centre;
                    ll_selected_view = ll_last;
                    selected_ed_2 = null;
                    ll_ans_box_selected = ll_main;
                }
            }
        });
        Log.e("delete6", "1234");
        ll_main.addView(ll_last, pos);
        Log.e("delete7", "1234");
        ed_centre.requestFocus();
    }

    private void add_ed_after_delete(final LinearLayout ll_main, final int textsize, final int pos) {

        String sss = String.valueOf(ll_main.getTag(R.id.first));
        int tag1 = Integer.valueOf(sss);
        String tag2 = String.valueOf(ll_main.getTag(R.id.second));
        int text_size = ActAssignCustomAns.this.getResources().getInteger(R.integer.kb_textsize_main_int);

        try {

            if (tag2.equals("super_top") || tag2.equals("sub_bottom") || tag2.equals("ss_top") ||
                    tag2.equals("ss_bottom") || tag2.equals("nth_top") || tag2.equals("lim_left") || tag2.equals("lim_right")) {
                text_size = (text_size - 1) - (tag1 + 1);
            } else if (tag2.equals("frac_top") || tag2.equals("frac_bottom") || tag2.equals("sqrt_c") || tag2.equals("nth_bottom")
                    || tag2.equals("parenthesis_center") || tag2.equals("absolute_center")
                    || tag2.equals("line_center") || tag2.equals("lsegment_center")
                    || tag2.equals("ray_center") || tag2.equals("arc_center")) {
                text_size = text_size - (tag1 + 1);
            }

        } catch (Exception e) {

        }
        if (text_size < 5) {
            text_size = 5;
        }
        final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
        ll_last.setTag("text");
        final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
        ed_centre.setMinWidth(20);
        ed_centre.setTextSize(text_size);
        ll_last.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
            }
        });
        ed_centre.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
                return false;
            }
        });
        ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                custom_ed_focus(ed_centre, ll_last, ll_main, hasFocus);
            }
        });
        //   Utils.change_edittext_bg(true, ed_centre);
        Log.e("edd2", "" + pos);
        ll_main.addView(ll_last, pos);
        ed_centre.requestFocus();

    }

    /*private void custom_ed_touch(final EditTextBlink ed, final LinearLayout ll_sub, final LinearLayout ll_main) {

        try {

            ed.setCursorVisible(true);
            selectedTextView = ed;
            ll_selected_view = ll_sub;
            ll_ans_box_selected = ll_main;
            selected_ed_2 = null;
            showKeyboard();

        } catch (Exception e) {
        }
    }*/

    private void add_inside_ed(final LinearLayout ll_main, final boolean req_focus, int text_size) {

        String tag2 = String.valueOf(ll_main.getTag(R.id.second));

        try {

            if (tag2.equals("super_top") || tag2.equals("sub_bottom") || tag2.equals("ss_top") ||
                    tag2.equals("ss_bottom") || tag2.equals("nth_top") || tag2.equals("lim_left") || tag2.equals("lim_right")) {
                // text_size = (text_size - 3)-(tag1+1);
                text_size = text_size - 3;
            } else if (tag2.equals("frac_top") || tag2.equals("frac_bottom") || tag2.equals("sqrt_c") || tag2.equals("nth_bottom")
                    || tag2.equals("parenthesis_center") || tag2.equals("absolute_center")
                    || tag2.equals("line_center") || tag2.equals("lsegment_center")
                    || tag2.equals("ray_center") || tag2.equals("arc_center")) {
                //  text_size = text_size-(tag1+1);
                text_size = text_size - 1;
            }

        } catch (Exception e) {
            text_size = 22;
        }

        Log.e("text_size", "" + text_size);
        final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
        ll_last.setTag("text");
        final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
        ed_centre.setMinWidth(25);
        ed_centre.setTextSize(text_size);
        ll_last.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
            }
        });
        ed_centre.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
                return false;
            }
        });
        ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                custom_ed_focus(ed_centre, ll_last, ll_main, hasFocus);
            }
        });
        Utils.change_edittext_bg(true, ed_centre);
        ll_main.addView(ll_last);

        Log.e("het", "" + ll_last.getHeight());
        ll_main.requestLayout();

        if (req_focus) {
            ed_centre.requestFocus();
        }

    }

    private void custom_ed_focus(final EditTextBlink ed, final LinearLayout ll_sub, final LinearLayout ll_main, final boolean hasFocus) {

        if (!(ll_main.getTag(R.id.second).equals("main"))) {
            Utils.change_edittext_bg(hasFocus, ed);
        }

        if (hasFocus) {
            selectedTextView = ed;
            ll_selected_view = ll_sub;
            selected_ed_2 = null;
            ll_ans_box_selected = ll_main;
        }

    }

   /* private void addviewtoselectedview(final LinearLayout ll_ans_box, String tag, String type) {  // Added By Siddhi Info Soft
        boolean is_devide = false;
        int start_pos = 0, ed_legnth = 0, ll_sub_selected_pos = 0;
        String cut_str = "";
        String sss = String.valueOf(ll_ans_box.getTag(R.id.first));
        int tag_main_ll = Integer.valueOf(sss);
        boolean need_delete = false;
        int text_size = ActAssignCustomAns.this.getResources().getInteger(R.integer.kb_textsize_main_int);

        try {
            DisplayMetrics metrics;
            metrics = getApplicationContext().getResources().getDisplayMetrics();
            float Textsize = selectedTextView.getTextSize() / metrics.density;
            text_size = Math.round(Textsize);
            Log.e("text_size", "" + text_size);
            if (text_size < 5) {
                text_size = 5;
            }

        } catch (Exception e) {
            text_size = 16;
        }

        try {

            start_pos = selectedTextView.getSelectionStart();
            ed_legnth = selectedTextView.getText().toString().length();

            try {
                ll_sub_selected_pos = ll_ans_box_selected.indexOfChild(ll_selected_view);
            } catch (Exception e) {
                ll_sub_selected_pos = 0;
            }

            try {
                if (ll_sub_selected_pos == -1) {
                    ll_sub_selected_pos = 0;
                }

                if (start_pos != 0 && start_pos < ed_legnth) {
                    cut_str = selectedTextView.getText().toString();
                    cut_str = cut_str.substring(start_pos, ed_legnth);
                    is_devide = true;
                    Log.e("math_ed_pos", cut_str + "" + ll_sub_selected_pos);
                    selectedTextView.getText().delete(start_pos, ed_legnth);
                }
            } catch (Exception e) {
            }

            try {
                if (selectedTextView.getText().toString().trim().length() == 0) {
                    need_delete = true;
                }
            } catch (Exception e) {
            }

            try {

                final LinearLayout ll_last = (LinearLayout) ll_ans_box.getChildAt((ll_ans_box.getChildCount() - 1));
                String str_tag = String.valueOf(ll_last.getTag());
                // Log.e("tagg",str_tag);
                if (str_tag.equals("text")) {
                    final EditTextBlink ed_last = (EditTextBlink) ll_last.getChildAt(0);
                    if (ed_last.getText().toString().trim().length() == 0) {

                        try {
                            if (ll_ans_box.getChildCount() > 1) {
                                LinearLayout ll_last_2 = (LinearLayout) ll_ans_box.getChildAt((ll_ans_box.getChildCount() - 2));
                                String str_tag_2 = String.valueOf(ll_last_2.getTag());
                                if (str_tag_2.equals("text")) {
                                    ll_ans_box.removeView(ll_last);
                                } else {
                                    ed_last.setMinWidth(4);
                                }
                            } else {
                                ed_last.setMinWidth(4);
                            }
                        } catch (Exception e) {
                        }
                    } else {
                        //  Log.e("tagg12", str_tag);
                        ed_last.setMinWidth(4);
                        ed_last.setMinimumWidth(4);
                        ed_last.requestLayout();
                        //  Log.e("tagg123", str_tag);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (type.equals("view")) {
                selected_ed_2 = null;
                if (tag.equals("super_script")) {

                    final LinearLayout ll_super_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_superscript, null);
                    ll_super_script.setTag("super_script");
                    final LinearLayout ll_top_superscript = (LinearLayout) ll_super_script.findViewById(R.id.ll_top_superscript);
                    ll_top_superscript.setTag(R.id.first, (tag_main_ll + 1));
                    ll_top_superscript.setTag(R.id.second, "super_top");

                    add_inside_ed(ll_top_superscript, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_super_script, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_super_script, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("sub_script")) {

                    final LinearLayout ll_sub_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_subscript, null);
                    ll_sub_script.setTag("sub_script");
                    final LinearLayout ll_bottom_subscript = (LinearLayout) ll_sub_script.findViewById(R.id.ll_bottom_subscript);
                    ll_bottom_subscript.setTag(R.id.first, (tag_main_ll + 1));
                    ll_bottom_subscript.setTag(R.id.second, "sub_bottom");
                    add_inside_ed(ll_bottom_subscript, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_sub_script, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_sub_script, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("super_sub_script")) {

                    final LinearLayout ll_super_sub_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_super_sub_script, null);
                    ll_super_sub_script.setTag("super_sub_script");

                    final LinearLayout ll_top_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_top_super_sub_script);
                    final LinearLayout ll_bottom_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_bottom_super_sub_script);
                    ll_top_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                    ll_bottom_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                    ll_top_super_sub_script.setTag(R.id.second, "ss_top");
                    ll_bottom_super_sub_script.setTag(R.id.second, "ss_bottom");
                    add_inside_ed(ll_top_super_sub_script, true, text_size);
                    add_inside_ed(ll_bottom_super_sub_script, false, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_super_sub_script, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_super_sub_script, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("fraction")) {

                    final LinearLayout ll_fraction = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_fraction, null);
                    final LinearLayout ll_fraction_top = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_top);
                    final LinearLayout ll_fraction_bottom = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_bottom);
                    ll_fraction_top.setTag(R.id.first, (tag_main_ll + 1));
                    ll_fraction_bottom.setTag(R.id.first, (tag_main_ll + 1));
                    ll_fraction_top.setTag(R.id.second, "frac_top");
                    ll_fraction_bottom.setTag(R.id.second, "frac_bottom");
                    add_inside_ed(ll_fraction_top, true, text_size);
                    add_inside_ed(ll_fraction_bottom, false, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_fraction, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_fraction, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }

                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("squareroot")) {

                    final LinearLayout ll_squareroot = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_square_root, null);
                    final LinearLayout ll_center_square_root = (LinearLayout) ll_squareroot.findViewById(R.id.ll_center_square_root);
                    ll_center_square_root.setTag(R.id.first, (tag_main_ll + 1));
                    ll_center_square_root.setTag(R.id.second, "sqrt_c");
                    add_inside_ed(ll_center_square_root, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_squareroot, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_squareroot, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("nthroot")) {

                    final LinearLayout ll_nthroot = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_nth_root, null);

                    final LinearLayout ll_top_nth_root = (LinearLayout) ll_nthroot.findViewById(R.id.ll_top_nth_root);
                    final LinearLayout ll_center_nth_root = (LinearLayout) ll_nthroot.findViewById(R.id.ll_center_nth_root);
                    final ImageView img = (ImageView) ll_nthroot.findViewById(R.id.img);
                    ll_top_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                    ll_center_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                    ll_top_nth_root.setTag(R.id.second, "nth_top");
                    ll_center_nth_root.setTag(R.id.second, "nth_bottom");
                    add_inside_ed(ll_top_nth_root, true, text_size);
                    add_inside_ed(ll_center_nth_root, false, text_size);
                    img.getLayoutParams().height = ll_center_nth_root.getHeight();
                    img.requestLayout();
                    ll_center_nth_root.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                        @Override
                        public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                            Log.e("het", "" + v.getHeight());
                            img.getLayoutParams().height = v.getHeight();
                            img.requestLayout();
                        }
                    });

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_nthroot, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_nthroot, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("lim")) {

                    final LinearLayout ll_lim = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_lim, null);
                    final TextView tv_text = (TextView) ll_lim.findViewById(R.id.tv_text);
                    tv_text.setText(Html.fromHtml("&#8594;"));
                    final LinearLayout ll_lim_left = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_left);
                    final LinearLayout ll_lim_right = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_right);
                    ll_lim_left.setTag(R.id.first, (tag_main_ll + 1));
                    ll_lim_right.setTag(R.id.first, (tag_main_ll + 1));
                    ll_lim_left.setTag(R.id.second, "lim_left");
                    ll_lim_right.setTag(R.id.second, "lim_right");
                    add_inside_ed(ll_lim_left, true, text_size);
                    add_inside_ed(ll_lim_right, false, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_lim, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_lim, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }

                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("parenthesis")) {

                    final LinearLayout ll_parenthesis = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_parenthesis, null);
                    ll_parenthesis.setTag("parenthesis");
                    final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_parenthesis_center);
                    ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                    ll_parenthesis_center.setTag(R.id.second, "parenthesis_center");
                    add_inside_ed(ll_parenthesis_center, true, text_size);
                    final ImageView img_left = (ImageView) ll_parenthesis.findViewById(R.id.img_left);
                    final ImageView img_rht = (ImageView) ll_parenthesis.findViewById(R.id.img_rht);

                    img_left.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final LinearLayout lll = (LinearLayout) ll_parenthesis_center.getChildAt(0);
                            if (lll.getTag().equals("text")) {
                                final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                edd.setSelection(0);
                                edd.requestFocus();
                            } else {
                                add_last_ed(ll_parenthesis_center, 22, 0);
                            }
                        }
                    });

                    img_rht.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final LinearLayout lll = (LinearLayout) ll_parenthesis_center.getChildAt((ll_parenthesis_center.getChildCount() - 1));
                            if (lll.getTag().equals("text")) {
                                final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                edd.setSelection(edd.getText().toString().length());
                                edd.requestFocus();
                            } else {
                                add_last_ed(ll_parenthesis_center, 22, (ll_parenthesis_center.getChildCount()));
                            }
                        }
                    });

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_parenthesis, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_parenthesis, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("absolute")) {

                    final LinearLayout ll_parenthesis = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_absolute, null);
                    ll_parenthesis.setTag("absolute");
                    final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_absolute_center);
                    ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                    ll_parenthesis_center.setTag(R.id.second, "absolute_center");
                    add_inside_ed(ll_parenthesis_center, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_parenthesis, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_parenthesis, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("line") || tag.equals("lsegment")
                        || tag.equals("ray") || tag.equals("arc")) {

                    final LinearLayout ll_parenthesis = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_lsegment, null);

                    final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_lsegment_center);
                    final ImageView img_top = (ImageView) ll_parenthesis.findViewById(R.id.img_top);
                    ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                    if (tag.equals("line")) {
                        img_top.setImageResource(R.drawable.line_icon);
                        ll_parenthesis.setTag("line");
                        ll_parenthesis_center.setTag(R.id.second, "line_center");
                    } else if (tag.equals("lsegment")) {
                        img_top.setImageResource(R.drawable.line_segment);
                        ll_parenthesis.setTag("lsegment");
                        ll_parenthesis_center.setTag(R.id.second, "lsegment_center");
                    } else if (tag.equals("ray")) {
                        img_top.setImageResource(R.drawable.ray_icon);
                        ll_parenthesis.setTag("ray");
                        ll_parenthesis_center.setTag(R.id.second, "ray_center");
                    } else if (tag.equals("arc")) {
                        img_top.setImageResource(R.drawable.arc_icon);
                        ll_parenthesis.setTag("arc");
                        ll_parenthesis_center.setTag(R.id.second, "arc_center");
                    }

                    add_inside_ed(ll_parenthesis_center, true, text_size);


                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_parenthesis, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_parenthesis, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                }
            }

            LinearLayout ll_last_2 = (LinearLayout) ll_ans_box.getChildAt((ll_ans_box.getChildCount() - 1));
            String str_tag = String.valueOf(ll_last_2.getTag());
            if (str_tag.equals("text")) {
                EditTextBlink ed_last = (EditTextBlink) ll_last_2.getChildAt(0);
                ed_last.setMinWidth(5);

            } else {

                final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                ll_last.setTag("text");
                final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                ed_centre.setTextSize(text_size);
                ed_centre.setMinWidth(4);
                ed_centre.setMinimumWidth(4);
                ed_centre.requestLayout();
                ll_last.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ed_centre.setCursorVisible(true);
                        selectedTextView = ed_centre;
                        ll_selected_view = ll_last;
                        ll_ans_box_selected = ll_ans_box;
                        selected_ed_2 = null;
                        showKeyboard();
                    }
                });
                ed_centre.setOnTouchListener(new OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        ed_centre.setCursorVisible(true);
                        selectedTextView = ed_centre;
                        ll_selected_view = ll_last;
                        ll_ans_box_selected = ll_ans_box;
                        selected_ed_2 = null;
                        showKeyboard();
                        return false;
                    }
                });
                ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            selectedTextView = ed_centre;
                            ll_selected_view = ll_last;
                            selected_ed_2 = null;
                            ll_ans_box_selected = ll_ans_box;
                        }
                    }
                });

                ll_ans_box.addView(ll_last);

            }

        } catch (Exception e) {

        }
    }*/

    public void setstudentansboxsub(final String str_ans, final LinearLayout ll_ans_box) {

        String[] str_1 = str_ans.split(ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first));

        String sss = String.valueOf(ll_ans_box.getTag(R.id.first));
        int tag_main_ll = Integer.valueOf(sss);

        for (int i = 0; i < str_1.length; i++) {

            String str_1_value = str_1[i];

            if (str_1_value.startsWith("text")) {

                str_1_value = str_1_value.substring(5);

                String sss1 = String.valueOf(ll_ans_box.getTag(R.id.first));
                int tag1 = Integer.valueOf(sss1);
                final String tag2 = String.valueOf(ll_ans_box.getTag(R.id.second));
                int text_size = ActAssignCustomAns.this.getResources().getInteger(R.integer.kb_textsize_main_int);

                try {

                    if (tag2.equals("super_top") || tag2.equals("sub_bottom") || tag2.equals("ss_top") ||
                            tag2.equals("ss_bottom") || tag2.equals("nth_top") || tag2.equals("lim_left") || tag2.equals("lim_right")) {
                        text_size = (text_size - 1) - (tag1 + 1);
                    } else if (tag2.equals("frac_top") || tag2.equals("frac_bottom") || tag2.equals("sqrt_c") || tag2.equals("nth_bottom")
                            || tag2.equals("parenthesis_center") || tag2.equals("absolute_center")
                            || tag2.equals("line_center") || tag2.equals("lsegment_center")
                            || tag2.equals("ray_center") || tag2.equals("arc_center")) {
                        text_size = text_size - (tag1 + 1);
                    }

                } catch (Exception e) {

                }

                final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                ll_last.setTag("text");
                final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                this.replacesignfromchar(str_1_value, ed_centre);
                ed_centre.setMinWidth(20);
                ed_centre.setTextSize(text_size);
                ll_last.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        custom_ed_touch(ed_centre, ll_last, ll_ans_box);
                    }
                });
                ed_centre.setOnTouchListener(new OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        custom_ed_touch(ed_centre, ll_last, ll_ans_box);
                        return false;
                    }
                });
                ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (!(tag2.equals("main"))) {
                            Utils.change_edittext_bg(hasFocus, ed_centre);
                        }
                        if (hasFocus) {
                            selectedTextView = ed_centre;
                            ll_selected_view = ll_last;
                            ll_ans_box_selected = ll_ans_box;
                        }
                    }
                });

                ll_ans_box.addView(ll_last);

            }
            if (str_1_value.startsWith("fraction")) {

                str_1_value = str_1_value.substring(8);

                final LinearLayout ll_fraction = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_fraction, null);
                final LinearLayout ll_fraction_top = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_top);
                final LinearLayout ll_fraction_bottom = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_bottom);
                ll_fraction_top.setTag(R.id.first, (tag_main_ll + 1));
                ll_fraction_bottom.setTag(R.id.first, (tag_main_ll + 1));
                ll_fraction_top.setTag(R.id.second, "frac_top");
                ll_fraction_bottom.setTag(R.id.second, "frac_bottom");

                final String[] str_2 = str_1_value.split("frac_btm_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_fraction);

                this.setstudentansboxsub(str_2[0], ll_fraction_top);
                this.setstudentansboxsub(str_2[1], ll_fraction_bottom);

            } else if (str_1_value.startsWith("super_script")) {
                str_1_value = str_1_value.substring(12);

                final LinearLayout ll_super_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_superscript, null);
                final LinearLayout ll_top_superscript = (LinearLayout) ll_super_script.findViewById(R.id.ll_top_superscript);
                ll_top_superscript.setTag(R.id.first, (tag_main_ll + 1));
                ll_top_superscript.setTag(R.id.second, "super_top");

                ll_ans_box.addView(ll_super_script);

                this.setstudentansboxsub(str_1_value, ll_top_superscript);

            } else if (str_1_value.startsWith("sub_script")) {
                str_1_value = str_1_value.substring(10);

                final LinearLayout ll_super_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_subscript, null);
                final LinearLayout ll_bottom_subscript = (LinearLayout) ll_super_script.findViewById(R.id.ll_bottom_subscript);
                ll_bottom_subscript.setTag(R.id.first, (tag_main_ll + 1));
                ll_bottom_subscript.setTag(R.id.second, "sub_bottom");

                ll_ans_box.addView(ll_super_script);

                this.setstudentansboxsub(str_1_value, ll_bottom_subscript);

            } else if (str_1_value.startsWith("super_sub_script")) {

                str_1_value = str_1_value.substring(16);

                final LinearLayout ll_super_sub_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_super_sub_script, null);
                final LinearLayout ll_top_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_top_super_sub_script);
                final LinearLayout ll_bottom_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_bottom_super_sub_script);
                ll_top_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                ll_bottom_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                ll_top_super_sub_script.setTag(R.id.second, "ss_top");
                ll_bottom_super_sub_script.setTag(R.id.second, "ss_bottom");

                final String[] str_2 = str_1_value.split("ss_btm_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_super_sub_script);

                this.setstudentansboxsub(str_2[0], ll_top_super_sub_script);
                this.setstudentansboxsub(str_2[1], ll_bottom_super_sub_script);

            } else if (str_1_value.startsWith("sqrt")) {

                str_1_value = str_1_value.substring(4);
                final LinearLayout ll_sqrt = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_square_root, null);
                final LinearLayout ll_center_square_root = (LinearLayout) ll_sqrt.findViewById(R.id.ll_center_square_root);
                ll_center_square_root.setTag(R.id.first, (tag_main_ll + 1));
                ll_center_square_root.setTag(R.id.second, "sqrt_c");

                ll_ans_box.addView(ll_sqrt);

                this.setstudentansboxsub(str_1_value, ll_center_square_root);

            } else if (str_1_value.startsWith("nsqrt")) {

                str_1_value = str_1_value.substring(5);

                final LinearLayout ll_nsqrt = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_nth_root, null);
                final LinearLayout ll_top_nth_root = (LinearLayout) ll_nsqrt.findViewById(R.id.ll_top_nth_root);
                final LinearLayout ll_center_nth_root = (LinearLayout) ll_nsqrt.findViewById(R.id.ll_center_nth_root);
                final ImageView img = (ImageView) ll_nsqrt.findViewById(R.id.img);
                ll_top_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                ll_center_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                ll_top_nth_root.setTag(R.id.second, "nth_top");
                ll_center_nth_root.setTag(R.id.second, "nth_bottom");

                img.getLayoutParams().height = ll_center_nth_root.getHeight();
                img.requestLayout();
                ll_center_nth_root.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                    @Override
                    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                        Log.e("het", "" + v.getHeight());
                        img.getLayoutParams().height = v.getHeight();
                        img.requestLayout();
                    }
                });

                final String[] str_2 = str_1_value.split("nsqrt_center_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_nsqrt);

                this.setstudentansboxsub(str_2[0], ll_top_nth_root);
                this.setstudentansboxsub(str_2[1], ll_center_nth_root);

            } else if (str_1_value.startsWith("lim")) {

                str_1_value = str_1_value.substring(3);

                final LinearLayout ll_lim = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_lim, null);
                final TextView tv_text = (TextView) ll_lim.findViewById(R.id.tv_text);
                tv_text.setText(Html.fromHtml("&#8594;"));
                final LinearLayout ll_lim_left = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_left);
                final LinearLayout ll_lim_right = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_right);
                ll_lim_left.setTag(R.id.first, (tag_main_ll + 1));
                ll_lim_right.setTag(R.id.first, (tag_main_ll + 1));
                ll_lim_left.setTag(R.id.second, "lim_left");
                ll_lim_right.setTag(R.id.second, "lim_right");

                final String[] str_2 = str_1_value.split("lim_rht_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_lim);

                this.setstudentansboxsub(str_2[0], ll_lim_left);
                this.setstudentansboxsub(str_2[1], ll_lim_right);

            } else if (str_1_value.startsWith("parenthesis")) {
                str_1_value = str_1_value.substring(11);

                final LinearLayout ll_parenthesis = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_parenthesis, null);
                final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_parenthesis_center);
                ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                ll_parenthesis_center.setTag(R.id.second, "parenthesis_center");

                final ImageView img_left = (ImageView) ll_parenthesis.findViewById(R.id.img_left);
                final ImageView img_rht = (ImageView) ll_parenthesis.findViewById(R.id.img_rht);

                img_left.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final LinearLayout lll = (LinearLayout) ll_parenthesis_center.getChildAt(0);
                        if (lll.getTag().equals("text")) {
                            final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                            edd.setSelection(0);
                            edd.requestFocus();
                        } else {
                            add_last_ed(ll_parenthesis_center, 22, 0);
                        }
                    }
                });

                img_rht.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final LinearLayout lll = (LinearLayout) ll_parenthesis_center.getChildAt((ll_parenthesis_center.getChildCount() - 1));
                        if (lll.getTag().equals("text")) {
                            final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                            edd.setSelection(edd.getText().toString().length());
                            edd.requestFocus();
                        } else {
                            add_last_ed(ll_parenthesis_center, 22, (ll_parenthesis_center.getChildCount()));
                        }
                    }
                });

                ll_ans_box.addView(ll_parenthesis);

                this.setstudentansboxsub(str_1_value, ll_parenthesis_center);

            } else if (str_1_value.startsWith("absolute")) {
                str_1_value = str_1_value.substring(8);

                final LinearLayout ll_super_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_absolute, null);
                final LinearLayout ll_parenthesis_center = (LinearLayout) ll_super_script.findViewById(R.id.ll_absolute_center);
                ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                ll_parenthesis_center.setTag(R.id.second, "absolute_center");

                ll_ans_box.addView(ll_super_script);

                this.setstudentansboxsub(str_1_value, ll_parenthesis_center);

            } else if (str_1_value.startsWith("line") || str_1_value.startsWith("lsegment") || str_1_value.startsWith("ray")
                    || str_1_value.startsWith("arc")) {

                final LinearLayout ll_parenthesis = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_lsegment, null);
                final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_lsegment_center);
                ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                final ImageView img_top = (ImageView) ll_parenthesis.findViewById(R.id.img_top);
                // ll_parenthesis_center.setTag(R.id.second,"absolute_center");

                if (str_1_value.startsWith("line")) {
                    img_top.setImageResource(R.drawable.line_icon);
                    str_1_value = str_1_value.substring(4);
                    ll_parenthesis.setTag("line");
                    ll_parenthesis_center.setTag(R.id.second, "line_center");
                } else if (str_1_value.startsWith("lsegment")) {
                    str_1_value = str_1_value.substring(8);
                    img_top.setImageResource(R.drawable.line_segment);
                    ll_parenthesis.setTag("lsegment");
                    ll_parenthesis_center.setTag(R.id.second, "lsegment_center");
                } else if (str_1_value.startsWith("ray")) {
                    str_1_value = str_1_value.substring(3);
                    img_top.setImageResource(R.drawable.ray_icon);
                    ll_parenthesis.setTag("ray");
                    ll_parenthesis_center.setTag(R.id.second, "ray_center");
                } else if (str_1_value.startsWith("arc")) {
                    str_1_value = str_1_value.substring(3);
                    img_top.setImageResource(R.drawable.arc_icon);
                    ll_parenthesis.setTag("arc");
                    ll_parenthesis_center.setTag(R.id.second, "arc_center");
                }

                ll_ans_box.addView(ll_parenthesis);

                this.setstudentansboxsub(str_1_value, ll_parenthesis_center);

            }

        }
    }

    public void onnextsubcall(final LinearLayout ll_sub) {

        if (ll_sub.getTag().toString().equals("text")) {

            EditTextBlink ed = (EditTextBlink) ll_sub.getChildAt(0);
            ed.requestFocus();
            ed.setSelection(0);

        } else if (ll_sub.getTag().toString().equals("fraction") || ll_sub.getTag().toString().equals("super_script") || ll_sub.getTag().toString().equals("super_sub_script")
                || ll_sub.getTag().toString().equals("nsqrt")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(0);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, 0);
            } else {
                this.onnextsubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("sub_script")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, 0);
            } else {
                this.onnextsubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("parenthesis") || ll_sub.getTag().toString().equals("absolute") ||
                ll_sub.getTag().toString().equals("line") || ll_sub.getTag().toString().equals("lsegment")
                || ll_sub.getTag().toString().equals("ray") || ll_sub.getTag().toString().equals("arc")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, 0);
            } else {
                this.onnextsubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("sqrt")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt(0);
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_2, 22, 0);
            } else {
                this.onnextsubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("lim")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(0);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt(0);
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_2, 22, 0);
            } else {
                this.onnextsubcall(ll_sub_1);
            }

        }

    }

    /*public void onnextclick() {

        try {
            int cur_pos = 0, ed_legnth = 0;

            try {
                cur_pos = selectedTextView.getSelectionStart();
                ed_legnth = selectedTextView.getText().toString().length();
            } catch (Exception e) {
            }

            if ((cur_pos != ed_legnth)) {

                String str_bar_1 = "" + Html.fromHtml("&#x0304;");
                String str_bar_2 = "" + Html.fromHtml("&#772;");

                if (selectedTextView.getText().toString().length() > 1) {
                    String ss = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos));
                    if (ss.equals("x")) {
                        if ((cur_pos + 1) < ed_legnth) {
                            String ss_1 = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos + 1));
                            if (ss_1.equals(str_bar_1) || ss_1.equals(str_bar_2)) {
                                selectedTextView.setSelection((cur_pos + 2));
                            } else {
                                selectedTextView.setSelection((cur_pos + 1));
                            }
                        } else {
                            selectedTextView.setSelection((cur_pos + 1));
                        }
                    } else {
                        selectedTextView.setSelection((cur_pos + 1));
                    }
                } else {
                    selectedTextView.setSelection((cur_pos + 1));
                }

                if (is_touch_kb_next) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onnextclick();
                        }
                    }, 150);
                }

            } else {
                final LinearLayout ll_main_ll = ll_ans_box_selected;
                final LinearLayout ll_sub_ll = ll_selected_view;

                int pos_sub_ll = ll_main_ll.indexOfChild(ll_sub_ll);

                if (ll_main_ll.getChildCount() > 1 && pos_sub_ll < (ll_main_ll.getChildCount() - 1)) {

                    LinearLayout ll_sub = (LinearLayout) ll_main_ll.getChildAt(pos_sub_ll + 1);
                    onnextsubcall(ll_sub);

                } else if (pos_sub_ll == (ll_main_ll.getChildCount() - 1)) {

                    String main_tag = (String) ll_main_ll.getTag(R.id.second);

                    if (main_tag.equals("frac_top")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("ss_top")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(1);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("nth_top")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("lim_left")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("sqrt_c") || main_tag.equals("nth_bottom") || main_tag.equals("lim_right")) {

                        sub_next_sqrt_c(ll_main_ll);

                    } else if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom") ||
                            main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                            || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                            || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {

                        sub_next_frac_bottom(ll_main_ll);

                    }
                }

                if (is_touch_kb_next) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onnextclick();
                        }
                    }, 150);
                }
            }

        } catch (Exception e) {
        }
    }*/

    public void sub_next_sqrt_c(final LinearLayout ll_main_ll) {
        try {

            try {

                final LinearLayout ll_mainm = (LinearLayout) ll_main_ll.getParent();
                final LinearLayout ll_main_1m = (LinearLayout) ll_mainm.getParent();
                final LinearLayout ll_main2 = (LinearLayout) ll_main_1m.getParent();
                int main_pos = ll_main2.indexOfChild(ll_main_1m);

                final String main_tag = String.valueOf(ll_main2.getTag(R.id.second));
                if (main_pos == (ll_main2.getChildCount() - 1)) {
                    add_last_ed(ll_main2, 22, main_pos + 1);
                } else if (ll_main2.getChildCount() > 1 && main_pos < (ll_main2.getChildCount() - 1)) {
                    final LinearLayout ll_sub = (LinearLayout) ll_main2.getChildAt(main_pos + 1);
                    if (!(ll_sub.getTag().equals("text"))) {
                        add_last_ed(ll_main2, 22, main_pos + 1);
                    } else {
                        onnextsubcall(ll_sub);
                    }
                } else {

                    if (main_tag.equals("sqrt_c") || main_tag.equals("nth_bottom") || main_tag.equals("lim_right")) {
                        sub_next_sqrt_c(ll_main2);
                    } else if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")
                            || main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                            || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                            || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {
                        sub_next_frac_bottom(ll_main2);
                    } else if (main_tag.equals("frac_top")) {
                        final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                        final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        final LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);
                    } else if (main_tag.equals("ss_top")) {
                        final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                        final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(1);
                        final LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);
                    } else if (main_tag.equals("nth_top")) {
                        final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                        final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        final LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
                        final LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt(0);
                        onnextsubcall(ll_sub_1);
                    } else if (main_tag.equals("lim_left")) {

                        LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("main")) {
                        add_last_ed(ll_main2, 18, ll_main2.getChildCount());
                    }
                }

            } catch (Exception e) {

            }

        } catch (Exception e) {
        }
    }

    public void sub_next_frac_bottom(final LinearLayout ll_main_ll) {
        try {
            final LinearLayout ll_mainm = (LinearLayout) ll_main_ll.getParent();
            final LinearLayout ll_main2 = (LinearLayout) ll_mainm.getParent();
            int main_pos = ll_main2.indexOfChild(ll_mainm);
            final String main_tag = String.valueOf(ll_main2.getTag(R.id.second));

            if (main_pos == (ll_main2.getChildCount() - 1)) {
                add_last_ed(ll_main2, 22, main_pos + 1);
            } else if (ll_main2.getChildCount() > 1 && main_pos < (ll_main2.getChildCount() - 1)) {
                final LinearLayout ll_sub = (LinearLayout) ll_main2.getChildAt(main_pos + 1);
                if (!(ll_sub.getTag().equals("text"))) {
                    add_last_ed(ll_main2, 22, main_pos + 1);
                } else {
                    onnextsubcall(ll_sub);
                }

            } else {
                if (main_tag.equals("sqrt_c") || main_tag.equals("nth_bottom") || main_tag.equals("lim_right")) {
                    sub_next_sqrt_c(ll_main2);
                } else if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")
                        || main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                        || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                        || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {
                    sub_next_frac_bottom(ll_main2);
                } else if (main_tag.equals("frac_top")) {
                    final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                    final LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                    onnextsubcall(ll_sub_1);
                } else if (main_tag.equals("ss_top")) {
                    final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(1);
                    final LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                    onnextsubcall(ll_sub_1);
                } else if (main_tag.equals("nth_top")) {
                    final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                    final LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
                    final LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt(0);
                    onnextsubcall(ll_sub_1);
                } else if (main_tag.equals("lim_left")) {

                    LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                    LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                    onnextsubcall(ll_sub_1);

                } else if (main_tag.equals("main")) {
                    add_last_ed(ll_main2, 22, ll_main2.getChildCount());
                }

            }
        } catch (Exception e) {
        }
    }

    public void onpresubcall(final LinearLayout ll_sub) {

        if (ll_sub.getTag().toString().equals("text")) {

            EditTextBlink ed = (EditTextBlink) ll_sub.getChildAt(0);
            ed.setSelection(ed.getText().toString().length());
            ed.requestFocus();

        } else if (ll_sub.getTag().toString().equals("sub_script")
                ) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, ll_main_1.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("super_script")
                ) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(0);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, ll_main_1.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("fraction")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(2);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, ll_main_1.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("parenthesis") || ll_sub.getTag().toString().equals("absolute")
                || ll_sub.getTag().toString().equals("line") || ll_sub.getTag().toString().equals("lsegment")
                || ll_sub.getTag().toString().equals("ray") || ll_sub.getTag().toString().equals("arc")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, ll_main_1.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("super_sub_script")) {
            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, ll_main_1.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("sqrt")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_2, 22, ll_main_2.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("nsqrt")) {
            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(2);
            LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_2, 22, ll_main_2.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("lim")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(2);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_2, 22, ll_main_2.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        }

    }

    /*public void ondeleteclick() {

        final LinearLayout ll_main_ll = ll_ans_box_selected;
        final LinearLayout ll_sub_ll = ll_selected_view;

        try {
            int ed_pos = 0, ed_legnth = 0;
            try {
                ed_pos = selectedTextView.getSelectionStart();
                ed_legnth = selectedTextView.getText().toString().trim().length();
            } catch (Exception e) {
            }
            if (ed_pos > 0 && ed_legnth > 0) {
                String str_bar_1 = "" + Html.fromHtml("&#x0304;");
                String str_bar_2 = "" + Html.fromHtml("&#772;");
                String str_integral = "" + Html.fromHtml("&#8747;");
                if (selectedTextView.getText().toString().length() > 1) {
                    String ss = String.valueOf(selectedTextView.getText().toString().charAt(ed_pos - 1));
                    if (ss.equals(str_bar_1) || ss.equals(str_bar_2)) {

                        selectedTextView.getText().delete(ed_pos - 2, ed_pos);

                    } else if (ss.equals("x")) {
                        if (ed_pos < ed_legnth) {
                            String ss_2 = String.valueOf(selectedTextView.getText().toString().charAt(ed_pos));
                            if (ss_2.equals(str_bar_1) || ss_2.equals(str_bar_2)) {
                                selectedTextView.getText().delete(ed_pos - 1, ed_pos + 1);

                            } else {
                                selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                            }
                        } else {
                            selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                        }
                    } else if (ss.equals(" ")) {
                        try {
                            String ss2 = String.valueOf(selectedTextView.getText().toString().charAt(ed_pos - 2));
                            if (ss2.equals(str_integral)) {
                                selectedTextView.getText().delete(ed_pos - 2, ed_pos);
                            } else {
                                selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                            }
                        } catch (Exception e) {
                            selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                        }
                    } else {
                        selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                    }
                } else {
                    selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                }
                try {
                    if (selectedTextView.getText().toString().trim().length() == 0) {
                        selectedTextView.setSelection(0);
                    }
                } catch (Exception e) {
                }

                if (is_touch_kb) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ondeleteclick();
                        }
                    }, 150);
                }

            } else if (ed_pos == 0 && ed_legnth == 0) {

                int pos_sub_ll = ll_main_ll.indexOfChild(ll_sub_ll);
                Log.e("delete4", "" + pos_sub_ll);

                if (pos_sub_ll != 0 && pos_sub_ll != -1) {

                    LinearLayout ll_sub = (LinearLayout) ll_main_ll.getChildAt(pos_sub_ll - 1);
                    onpresubcall(ll_sub);
                    ll_main_ll.removeViewAt(pos_sub_ll);

                } else if (pos_sub_ll == 0) {

                    String main_tag = (String) ll_main_ll.getTag(R.id.second);

                    Log.e("delete5", "" + main_tag);

                    if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                        if (ll_main_ll.getChildCount() > 1) {
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        }

                    } else if (main_tag.equals("nth_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                        if (ll_main_ll.getChildCount() > 1) {
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        }

                    } else if (main_tag.equals("lim_right")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                        if (ll_main_ll.getChildCount() > 1) {
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        }

                    } else if (main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                            || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                            || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {

                        try {
                            LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            LinearLayout ll_main2 = (LinearLayout) ll_main.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_frac_top(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {

                                if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                    sub_pre_frac_top(ll_main_ll, 2, false);
                                    ll_main2.removeViewAt(main_pos);
                                    this.add_ed_after_delete(ll_main2, 18, main_pos);
                                } else {
                                    if (main_pos != 0) {
                                        sub_pre_frac_top(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                    } else {
                                        sub_pre_frac_top(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                        try {
                                            final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                            if (lll.getTag().toString().equals("text")) {
                                                EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                edd.setMinWidth(20);
                                                edd.setMinimumWidth(20);
                                                edd.requestLayout();
                                                edd.requestFocus();
                                                if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                    Utils.change_edittext_bg(true, edd);
                                                }
                                            }
                                        } catch (Exception e) {
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("sqrt_c")) {

                        try {

                            final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            final LinearLayout ll_main1 = (LinearLayout) ll_main.getParent();
                            final LinearLayout ll_main2 = (LinearLayout) ll_main1.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main1);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_sqrt_c(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {
                                if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                    Log.e("edd", "" + main_pos);
                                    sub_pre_sqrt_c(ll_main_ll, 2, false);
                                    ll_main2.removeViewAt(main_pos);
                                    Log.e("edd1", "" + main_pos);
                                    this.add_ed_after_delete(ll_main2, 18, main_pos);
                                } else {
                                    if (main_pos != 0) {
                                        sub_pre_sqrt_c(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                    } else {
                                        sub_pre_sqrt_c(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                        try {
                                            final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                            if (lll.getTag().toString().equals("text")) {
                                                final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                edd.setMinWidth(20);
                                                edd.setMinimumWidth(20);
                                                edd.requestLayout();
                                                edd.requestFocus();
                                                if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                    Utils.change_edittext_bg(true, edd);
                                                }
                                            }
                                        } catch (Exception e) {
                                        }
                                    }
                                }
                            }

                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("nth_top")) {

                        try {
                            final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            final LinearLayout ll_main2 = (LinearLayout) ll_main.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_frac_top(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {
                                final LinearLayout ll_sqrt = (LinearLayout) ll_main.getChildAt(2);
                                final LinearLayout ll_sqrt2 = (LinearLayout) ll_sqrt.getChildAt(1);
                                if (this.makeansfromview(ll_sqrt2).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_frac_top(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            }

                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("lim_left")) {

                        try {
                            final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            final LinearLayout ll_main1 = (LinearLayout) ll_main.getParent();
                            final LinearLayout ll_main2 = (LinearLayout) ll_main1.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main1);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_sqrt_c(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {
                                final LinearLayout ll_lim_rht = (LinearLayout) ll_main.getChildAt(2);
                                if (this.makeansfromview(ll_lim_rht).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_sqrt_c(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_sqrt_c(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_sqrt_c(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            }

                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("frac_top") || main_tag.equals("ss_top")) {

                        final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        final LinearLayout ll_main2 = (LinearLayout) ll_main.getParent();
                        int main_pos = ll_main2.indexOfChild(ll_main);

                        if (ll_main_ll.getChildCount() > 1) {
                            sub_pre_frac_top(ll_main_ll, 0, false);
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        } else {
                            if (main_tag.equals("frac_top")) {
                                LinearLayout ll_frac = (LinearLayout) ll_main.getChildAt(2);
                                if (this.makeansfromview(ll_frac).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_frac_top(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            } else {
                                LinearLayout ll_frac = (LinearLayout) ll_main.getChildAt(1);

                                if (this.makeansfromview(ll_frac).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_frac_top(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            }
                        }
                    }
                }

                if (is_touch_kb) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ondeleteclick();
                        }
                    }, 150);
                }
            } else {
                this.onpreviousclick();
                if (is_touch_kb) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ondeleteclick();
                        }
                    }, 150);
                }
            }

        } catch (Exception e) {
        }
    }*/

    /*public void onpreviousclick() {

        try {
            int cur_pos = 0;

            try {
                cur_pos = selectedTextView.getSelectionStart();
            } catch (Exception e) {
            }

            if (cur_pos != 0) {

                String str_bar_1 = "" + Html.fromHtml("&#x0304;");
                String str_bar_2 = "" + Html.fromHtml("&#772;");

                if (selectedTextView.getText().toString().length() > 1) {
                    String ss = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos - 1));
                    if (ss.equals(str_bar_1) || ss.equals(str_bar_2)) {

                        selectedTextView.setSelection((cur_pos - 2));

                    } else {
                        selectedTextView.setSelection((cur_pos - 1));
                    }
                } else {
                    selectedTextView.setSelection((cur_pos - 1));
                }

                if (is_touch_kb_pre) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onpreviousclick();
                        }
                    }, 150);
                }

            } else {

                final LinearLayout ll_main_ll = ll_ans_box_selected;
                final LinearLayout ll_sub_ll = ll_selected_view;

                int pos_sub_ll = ll_main_ll.indexOfChild(ll_sub_ll);

                if (pos_sub_ll != 0 && pos_sub_ll != -1) {

                    LinearLayout ll_sub = (LinearLayout) ll_main_ll.getChildAt(pos_sub_ll - 1);
                    onpresubcall(ll_sub);

                } else if (pos_sub_ll == 0) {

                    String main_tag = (String) ll_main_ll.getTag(R.id.second);

                    if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                    } else if (main_tag.equals("nth_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                    } else if (main_tag.equals("lim_right")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                    } else if (main_tag.equals("sqrt_c") || main_tag.equals("lim_left")) {

                        try {
                            sub_pre_sqrt_c(ll_main_ll, 0, true);
                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("frac_top") || main_tag.equals("ss_top") ||
                            main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("nth_top") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                            || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                            || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {

                        try {
                            sub_pre_frac_top(ll_main_ll, 0, true);
                        } catch (Exception e) {
                        }

                    }
                }

                if (is_touch_kb_pre) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onpreviousclick();
                        }
                    }, 150);
                }

            }

        } catch (Exception e) {
        }

    }*/

    public void sub_pre_sqrt_c(final LinearLayout ll_main_ll, final int is_main, final boolean is_add_ed) {

        try {

            LinearLayout ll_mainm = (LinearLayout) ll_main_ll.getParent();
            LinearLayout ll_main1 = (LinearLayout) ll_mainm.getParent();
            LinearLayout ll_main2 = (LinearLayout) ll_main1.getParent();
            int main_pos = ll_main2.indexOfChild(ll_main1);
            final String main_tag = String.valueOf(ll_main2.getTag(R.id.second));
            if (main_pos == 0 && is_add_ed) {
                add_last_ed(ll_main2, 22, main_pos);
            } else if (ll_main2.getChildCount() > 1 && main_pos != 0) {
                LinearLayout ll_sub = (LinearLayout) ll_main2.getChildAt(main_pos - 1);
                if (!(ll_sub.getTag().equals("text")) && is_add_ed) {
                    add_last_ed(ll_main2, 22, main_pos);
                } else {
                    onpresubcall(ll_sub);
                }
            } else {
                if (main_tag.equals("sqrt_c") || main_tag.equals("lim_left")) {
                    this.sub_pre_sqrt_c(ll_main2, 0, is_add_ed);
                } else if (main_tag.equals("frac_top") || main_tag.equals("ss_top")
                        || main_tag.equals("super_top") || main_tag.equals("sub_bottom")
                        || main_tag.equals("nth_top") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                        || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                        || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {
                    this.sub_pre_frac_top(ll_main2, 0, is_add_ed);
                } else if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")) {
                    final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(0);
                    final LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
                    onpresubcall(ll_sub_1);
                } else if (main_tag.equals("nth_bottom")) {
                    LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    LinearLayout ll_main_1 = (LinearLayout) ll_main.getParent();
                    LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(0);
                    LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                    onpresubcall(ll_sub_1);
                } else if (main_tag.equals("lim_right")) {

                    LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    LinearLayout ll_main_2 = (LinearLayout) ll_main.getChildAt(0);
                    LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                    onpresubcall(ll_sub_1);

                } else if (main_tag.equals("main")) {
                    if (is_main == 0) {
                        add_last_ed(ll_main2, 18, 0);
                    } else if (is_main == 1) {
                        this.onnextsubcall((LinearLayout) ll_main2.getChildAt(1));
                    }
                }
            }

        } catch (Exception e) {
        }

    }

    public void sub_pre_frac_top(final LinearLayout ll_main_ll, final int is_main, final boolean is_add_ed) {

        try {
            final LinearLayout ll_mainm = (LinearLayout) ll_main_ll.getParent();
            final LinearLayout ll_main2 = (LinearLayout) ll_mainm.getParent();
            int main_pos = ll_main2.indexOfChild(ll_mainm);
            final String main_tag = String.valueOf(ll_main2.getTag(R.id.second));
            if (main_pos == 0 && is_add_ed) {
                add_last_ed(ll_main2, 22, main_pos);
            } else if (ll_main2.getChildCount() > 1 && main_pos != 0) {
                final LinearLayout ll_sub = (LinearLayout) ll_main2.getChildAt(main_pos - 1);
                if (!(ll_sub.getTag().equals("text")) && is_add_ed) {
                    add_last_ed(ll_main2, 22, main_pos);
                } else {
                    onpresubcall(ll_sub);
                }
            } else {
                Log.e("delete2", "" + ll_main2.getTag(R.id.second));
                if (main_tag.equals("sqrt_c") || main_tag.equals("lim_left")) {
                    this.sub_pre_sqrt_c(ll_main2, 0, is_add_ed);
                } else if (main_tag.equals("frac_top") || main_tag.equals("ss_top")
                        || main_tag.equals("super_top") || main_tag.equals("sub_bottom")
                        || main_tag.equals("nth_top") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                        || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                        || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {
                    this.sub_pre_frac_top(ll_main2, 0, is_add_ed);
                } else if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")) {
                    final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(0);
                    final LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
                    onpresubcall(ll_sub_1);
                } else if (main_tag.equals("nth_bottom")) {
                    LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    LinearLayout ll_main_1 = (LinearLayout) ll_main.getParent();
                    LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(0);
                    LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                    onpresubcall(ll_sub_1);
                } else if (main_tag.equals("lim_right")) {

                    LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    LinearLayout ll_main_2 = (LinearLayout) ll_main.getChildAt(0);
                    LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                    onpresubcall(ll_sub_1);

                } else if (main_tag.equals("main")) {
                    if (is_main == 0) {
                        add_last_ed(ll_main2, 18, 0);
                    } else if (is_main == 1) {
                        this.onnextsubcall((LinearLayout) ll_main2.getChildAt(1));
                    }
                }
            }
        } catch (Exception e) {
        }

    }

    private String makeansfromview(final LinearLayout ll_ans_box) {

        String str = "";

        for (int i = 0; i < ll_ans_box.getChildCount(); i++) {

            LinearLayout ll_view = (LinearLayout) ll_ans_box.getChildAt(i);

            String main_tag = String.valueOf(ll_view.getTag());

            if (main_tag.equals("text")) {

                EditTextBlink ed_1 = (EditTextBlink) ll_view.getChildAt(0);
                if (ed_1.getText().toString().trim().length() != 0) {
                    str = str + "text#" + this.replacecharfromsign(ed_1.getText().toString().trim()) + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }

            } else if (main_tag.equals("fraction")) {

                LinearLayout ll_fraction_top = (LinearLayout) ll_view.getChildAt(0);
                LinearLayout ll_fraction_bottom = (LinearLayout) ll_view.getChildAt(2);

                String ss = this.makeansfromview(ll_fraction_top);
                String ss1 = this.makeansfromview(ll_fraction_bottom);
                if (ss.trim().length() != 0 && ss1.trim().length() != 0) {
                    str = str + "fraction" + ss + "frac_btm_" + ll_ans_box.getTag(R.id.first)
                            + ss1 + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }
            } else if (main_tag.equals("super_script")) {

                LinearLayout ll_top_superscript = (LinearLayout) ll_view.getChildAt(0);

                String ss = this.makeansfromview(ll_top_superscript);

                if (ss.trim().length() != 0) {
                    str = str + "super_script" + ss + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }

            } else if (main_tag.equals("sub_script")) {

                LinearLayout ll_bottom_subscript = (LinearLayout) ll_view.getChildAt(1);

                String ss = this.makeansfromview(ll_bottom_subscript);

                if (ss.trim().length() != 0) {
                    str = str + "sub_script" + ss + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }


            } else if (main_tag.equals("super_sub_script")) {

                LinearLayout ll_top_super_sub_script = (LinearLayout) ll_view.getChildAt(0);
                LinearLayout ll_bottom_super_sub_script = (LinearLayout) ll_view.getChildAt(1);

                String ss = this.makeansfromview(ll_top_super_sub_script);
                String ss1 = this.makeansfromview(ll_bottom_super_sub_script);
                if (ss.trim().length() != 0 && ss1.trim().length() != 0) {
                    str = str + "super_sub_script" + ss + "ss_btm_" + ll_ans_box.getTag(R.id.first)
                            + ss1 + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }
            } else if (main_tag.equals("sqrt")) {

                LinearLayout ll_l = (LinearLayout) ll_view.getChildAt(1);
                LinearLayout ll_center_square_root = (LinearLayout) ll_l.getChildAt(1);

                String ss = this.makeansfromview(ll_center_square_root);

                if (ss.trim().length() != 0) {
                    str = str + "sqrt" + ss + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }

            } else if (main_tag.equals("nsqrt")) {

                LinearLayout ll_top_nth_root = (LinearLayout) ll_view.getChildAt(0);
                LinearLayout ll_l = (LinearLayout) ll_view.getChildAt(2);
                LinearLayout ll_center_nth_root = (LinearLayout) ll_l.getChildAt(1);

                String ss = this.makeansfromview(ll_top_nth_root);
                String ss1 = this.makeansfromview(ll_center_nth_root);
                if (ss.trim().length() != 0 && ss1.trim().length() != 0) {
                    str = str + "nsqrt" + ss + "nsqrt_center_" + ll_ans_box.getTag(R.id.first)
                            + ss1 + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }
            } else if (main_tag.equals("lim")) {

                LinearLayout ll_1 = (LinearLayout) ll_view.getChildAt(1);
                LinearLayout ll_lim_left = (LinearLayout) ll_1.getChildAt(0);
                LinearLayout ll_lim_right = (LinearLayout) ll_1.getChildAt(2);

                String ss = this.makeansfromview(ll_lim_left);
                String ss1 = this.makeansfromview(ll_lim_right);
                if (ss.trim().length() != 0 && ss1.trim().length() != 0) {
                    str = str + "lim" + ss + "lim_rht_" + ll_ans_box.getTag(R.id.first)
                            + ss1 + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }
            } else if (main_tag.equals("parenthesis")) {

                LinearLayout ll_top_superscript = (LinearLayout) ll_view.getChildAt(1);

                String ss = this.makeansfromview(ll_top_superscript);

                if (ss.trim().length() != 0) {
                    str = str + "parenthesis" + ss + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }

            } else if (main_tag.equals("absolute")) {

                LinearLayout ll_top_superscript = (LinearLayout) ll_view.getChildAt(1);

                String ss = this.makeansfromview(ll_top_superscript);

                if (ss.trim().length() != 0) {
                    str = str + "absolute" + ss + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }

            } else if (main_tag.equals("line") || main_tag.equals("lsegment")
                    || main_tag.equals("ray") || main_tag.equals("arc")) {

                LinearLayout ll_top_superscript = (LinearLayout) ll_view.getChildAt(1);

                String ss = this.makeansfromview(ll_top_superscript);

                if (ss.trim().length() != 0) {
                    str = str + main_tag + ss + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }

            }
        }
        try {
            str = str.substring(0, (str.length() - 3));
        } catch (Exception e) {

        }

        try {
            String cut_str = str.substring(str.length() - 8);
            if (cut_str.equals("fraction")) {
                str = str.substring(0, (str.length() - 11));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 12);
            if (cut_str.equals("super_script")) {
                str = str.substring(0, (str.length() - 15));
            }
        } catch (Exception e) {
        }
        try {
            String cut_str = str.substring(str.length() - 10);
            if (cut_str.equals("sub_script")) {
                str = str.substring(0, (str.length() - 13));
            }
        } catch (Exception e) {
        }
        try {
            String cut_str = str.substring(str.length() - 16);
            if (cut_str.equals("super_sub_script")) {
                str = str.substring(0, (str.length() - 19));
            }
        } catch (Exception e) {
        }
        try {
            String cut_str = str.substring(str.length() - 4);
            if (cut_str.equals("sqrt")) {
                str = str.substring(0, (str.length() - 7));
            }
        } catch (Exception e) {
        }
        try {
            String cut_str = str.substring(str.length() - 5);
            if (cut_str.equals("nsqrt")) {
                str = str.substring(0, (str.length() - 8));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 3);
            if (cut_str.equals("lim")) {
                str = str.substring(0, (str.length() - 6));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 11);
            if (cut_str.equals("parenthesis")) {
                str = str.substring(0, (str.length() - 14));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 8);
            if (cut_str.equals("absolute")) {
                str = str.substring(0, (str.length() - 11));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 4);
            if (cut_str.equals("line")) {
                str = str.substring(0, (str.length() - 7));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 8);
            if (cut_str.equals("lsegment")) {
                str = str.substring(0, (str.length() - 11));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 3);
            if (cut_str.equals("ray")) {
                str = str.substring(0, (str.length() - 6));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 3);
            if (cut_str.equals("arc")) {
                str = str.substring(0, (str.length() - 6));
            }
        } catch (Exception e) {
        }

        return str;
    }

    private boolean is_char_match(String ss) {

        String str_plus = "" + Html.fromHtml("&#43;");
        String str_minus = "" + Html.fromHtml("&#8722;");
        String str_equal = "" + Html.fromHtml("&#61;");
        String str_multi = "" + Html.fromHtml("&#215;");
        String str_divide = "" + Html.fromHtml("&#247;");
        String str_goe = "" + Html.fromHtml("&#8805;");
        String str_loe = "" + Html.fromHtml("&#8804;");
        String str_grthan = "" + Html.fromHtml("&#62;");
        String str_lethan = "" + Html.fromHtml("&#60;");

        if (ss.equals(str_plus) || ss.equals(str_minus) || ss.equals(str_equal) || ss.equals(str_multi) || ss.equals(str_divide)
                || ss.equals(str_goe) || ss.equals(str_loe) || ss.equals(str_grthan)
                || ss.equals(str_lethan)) {
            return true;
        } else {
            return false;
        }

    }

    public void onplusminusclick(View v) {
        try {
            if (selected_ed_2 == null) {

                int pos = selectedTextView.getSelectionStart();

                try {

                    if (pos == 0) {

                        if (selectedTextView.getText().toString().trim().length() > 0) {

                            String ss = String.valueOf(selectedTextView.getText().toString().charAt(0));

                            if (ss.equals("-")) {
                                selectedTextView.getText().delete(0, 1);
                            } else {
                                selectedTextView.getText().insert(0, "-");
                            }

                        } else {
                            selectedTextView.getText().insert(0, "-");
                        }

                    }

                } catch (Exception e) {

                }

                for (int i = (pos - 1); i >= 0; i--) {

                    String ss = String.valueOf(selectedTextView.getText().toString().charAt(i));

                    if (ss.equals("-")) {
                        selectedTextView.getText().delete(i, i + 1);
                        break;
                    } else if (ss.equals(" ")) {
                        selectedTextView.getText().insert(i + 1, "-");
                        break;
                    } else if (ss.equals("(") || ss.equals(")")) {
                        selectedTextView.getText().insert(i + 1, "-");
                        break;
                    } else if (is_char_match(ss)) {
                        selectedTextView.getText().insert(i + 1, "-");
                        break;
                    } else if (i == 0) {
                        selectedTextView.getText().insert(i, "-");
                        break;
                    }

                }

            } else {

                int cur_pos = ll_ans_box_selected.indexOfChild(ll_selected_view);

                if (cur_pos < ll_ans_box_selected.getChildCount()) {

                    LinearLayout ll_next = (LinearLayout) ll_ans_box_selected.getChildAt((cur_pos + 1));

                    if (String.valueOf(ll_next.getTag()).equals("text")) {
                        EditTextBlink ed_text = (EditTextBlink) ll_next.getChildAt(0);
                        if (ed_text.getText().toString().contains("-")) {
                            ed_text.getText().delete(0, 1);
                        } else {
                            ed_text.getText().insert(0, "-");
                        }
                        ed_text.setSelection(1);
                        ed_text.requestFocus();
                    } else {
                        final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                        ll_last.setTag("text");
                        final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                        ed_centre.setText("-");
                        ed_centre.requestFocus();
                        ll_last.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ed_centre.setCursorVisible(true);
                                selectedTextView = ed_centre;
                                ll_selected_view = ll_last;
                                selected_ed_2 = null;
                                showKeyboard();
                            }
                        });
                        ed_centre.setOnTouchListener(new OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                ed_centre.setCursorVisible(true);
                                selectedTextView = ed_centre;
                                ll_selected_view = ll_last;
                                selected_ed_2 = null;
                                showKeyboard();
                                return false;
                            }
                        });
                        ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                if (hasFocus) {
                                    selectedTextView = ed_centre;
                                    ll_selected_view = ll_last;
                                    selected_ed_2 = null;
                                }
                            }
                        });

                        ll_ans_box_selected.addView(ll_last, (cur_pos + 1));
                    }
                }
            }
        } catch (Exception e) {

        }
    }

    private void setTextToSelectedTextView(EditText txtView, String text) { // Changes by Siddhi info soft

        final LinearLayout ll_ans_box = ll_ans_box_selected;

        try {

            if (text.equals("op_pi")) {
                text = "" + Html.fromHtml("&#960;");
            }

            if (selected_ed_2 == null) {

                setTextToSelectedTextView2(txtView, text);

            } else {

                int cur_pos = ll_ans_box_selected.indexOfChild(ll_selected_view);

                if (cur_pos < ll_ans_box_selected.getChildCount()) {

                    final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                    ll_last.setTag("text");
                    final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                    ed_centre.setText(text);
                    ll_last.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ed_centre.setCursorVisible(true);
                            selectedTextView = ed_centre;
                            ll_selected_view = ll_last;
                            selected_ed_2 = null;
                            ll_ans_box_selected = ll_ans_box;
                            showKeyboard();
                        }
                    });
                    ed_centre.setOnTouchListener(new OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            ed_centre.setCursorVisible(true);
                            selectedTextView = ed_centre;
                            ll_selected_view = ll_last;
                            selected_ed_2 = null;
                            ll_ans_box_selected = ll_ans_box;
                            showKeyboard();
                            return false;
                        }
                    });
                    ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                selectedTextView = ed_centre;
                                ll_selected_view = ll_last;
                                selected_ed_2 = null;
                                ll_ans_box_selected = ll_ans_box;
                            }
                        }
                    });

                    ll_ans_box.addView(ll_last, (cur_pos + 1));

                    ed_centre.requestFocus();

                }
            }

        } catch (Exception e) {

        }

        selectedTextView.setCursorVisible(true);

    }

    public void setTextToSelectedTextView2(EditText txtView, String text) { //added by siddhiinfosoft
        try {
            if (selectedTextView != null) {

                int cur_pos = selectedTextView.getSelectionStart();
                int ed_length = selectedTextView.getText().toString().length();

                if (selectedTextView.getText().toString().length() > 1) {

                    String str_bar_1 = "" + Html.fromHtml("&#x0304;");
                    String str_bar_2 = "" + Html.fromHtml("&#772;");

                    if (cur_pos != 0 && cur_pos < ed_length) {
                        String ss = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos - 1));

                        if (ss.equals("x")) {

                            String ss_1 = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos));

                            if (ss_1.equals(str_bar_1) || ss_1.equals(str_bar_2)) {

                            } else {
                                selectedTextView.getText().insert(selectedTextView.getSelectionStart(), text);
                            }

                        } else {
                            selectedTextView.getText().insert(selectedTextView.getSelectionStart(), text);
                        }

                    } else {
                        selectedTextView.getText().insert(selectedTextView.getSelectionStart(), text);
                    }

                } else {

                    selectedTextView.getText().insert(selectedTextView.getSelectionStart(), text);

                }

                if (text.equals("|  |")) {
                    selectedTextView.setSelection((selectedTextView.getSelectionStart() - 3));
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        selectedTextView.setCursorVisible(true);
    }


    //new assign homework change to delete the homework
    private void deleteHomework() {
        String[] texts = MathFriendzyHelper.getTreanslationTextById(this, "lblAreYouSureToDeleteHW",
                "btnTitleYes", "lblNo");
        MathFriendzyHelper.yesNoConfirmationDialog(this, texts[0], texts[1], texts[2],
                new YesNoListenerInterface() {
                    @Override
                    public void onYes() {
                        customeResult.setCustomeAnsList(null);
                        finishCurrectActOnBackPress(customeResult);
                    }

                    @Override
                    public void onNo() {

                    }
                });

    }
    //end changes

    //newly added

    /**
     * View answer sheet
     */
    private void clickOnViewSheet() {
        final String pdfFileName = customeResult.getSheetName();
        if (PDFOperation.checkForExistanceOfPdf(this, pdfFileName)
                /*&& (!PDFOperation.isFileCorrupted(pdfFileName , this))*/) {
            PDFOperation.isFileCorrupted(pdfFileName, this, new OnRequestSuccess() {
                @Override
                public void onRequestSuccess(boolean isSuccess) {
                    CommonUtils.printLog(TAG, "inside clickOnViewSheet "
                            + isSuccess);
                    if (isSuccess) {//true if file corrupted
                        downloadPdf(pdfFileName);
                    } else {
                        openPdfAct(pdfFileName);
                    }
                }
            });
        } else {
            downloadPdf(pdfFileName);
        }
    }

    private void downloadPdf(final String pdfFileName) {
        if (CommonUtils.isInternetConnectionAvailable(this)) {
            PDFOperation.downloadPdf(this, ICommonUtils.DOWNLOAD_WORK_PDF_URL,
                    pdfFileName, new OnRequestSuccess() {

                        @Override
                        public void onRequestSuccess(boolean isSuccess) {
                            if (isSuccess) {
                                openPdfAct(pdfFileName);
                            } else {
                                MathFriendzyHelper.showWarningDialog(ActAssignCustomAns.this,
                                        MathFriendzyHelper.getTreanslationTextById(ActAssignCustomAns.this
                                                , "alertNoHomeworkSheetAttached"));
                            }
                        }
                    });
        } else {
            CommonUtils.showInternetDialog(this);
        }
    }

    /**
     * View pdf file
     *
     * @param pdfFileName
     */
    private void openPdfAct(String pdfFileName) {
        Intent intent = new Intent(this, ActViewPdf.class);
        intent.putExtra("pdfFileName", pdfFileName);
        startActivity(intent);
    }

    /**
     * Remove the Add from the prefix if user need to see only the homework sheet
     */
    private void setTextOfAddHomeworkSheetButton() {
        try {
            if (!MathFriendzyHelper.isEmpty(customeResult.getSheetName())
                    && workSheetImageList == null) {
                String text[] = MathFriendzyHelper.getTreanslationTextById(this, "lblAssignment", "lblSheets");
                btnAddHomeworkSheet.setText(text[0] + " " + text[1]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Add question
    private void showQuestionFields(boolean isShowQuestion) {
        hideDeviceKeyboardAndClearFocus();
        this.isShowQuestion = !this.isShowQuestion;
        if (viewHolders != null && viewHolders.size() > 0) {
            for (int i = 0; i < viewHolders.size(); i++) {
                /*if(!this.isShowQuestion){//hide the question and set text to blank
                    viewHolders.get(i).ll_que_box.removeAllViews();
                }*/
                this.showQuestionField(viewHolders.get(i).rlQuestionLayout);
            }
        }
    }

    private void showQuestionField(RelativeLayout rlQuestionLayout) {
        if (this.isShowQuestion) {


            rlQuestionLayout.setVisibility(View.VISIBLE);

        } else {
            rlQuestionLayout.setVisibility(View.GONE);
        }
    }

    private void setQuestionToCustomAns(CustomeAns customAns, int position) {
        if (this.isShowQuestion) {
            try {
                //customAns.setQuestionString(viewHolders.get(position).edtQuestion.getText().toString());
                customAns.setQuestionString(this.makeansfromview(viewHolders.get(position).ll_que_box));
            } catch (Exception e) {
                customAns.setQuestionString("");
            }
        } else {
            customAns.setQuestionString("");
        }
    }

    private boolean isQuestionCompleteTeAdd(CustomeAns customAns) {
        if (this.isShowQuestion) {
            if (MathFriendzyHelper.isEmpty(customAns.getQuestionString())) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    private void setVisibilityOfEnterAnsHintText(ViewHolder vHolder, boolean isVisible) {
        if (isVisible) {
            vHolder.txtEnterAnsHint.setVisibility(View.VISIBLE);
        } else {
            vHolder.txtEnterAnsHint.setVisibility(View.GONE);
        }
    }

    private void setVisibilityOfEnterQueHintText(ViewHolder vHolder, boolean isVisible) {
        if (isVisible) {
            vHolder.txtEnterQueHint.setVisibility(View.VISIBLE);
        } else {
            vHolder.txtEnterQueHint.setVisibility(View.GONE);
        }
    }

    private boolean isAtleastOneQuestionAdded(ArrayList<CustomeAns> customeAnsList) {
        try {
            if (customeAnsList != null && customeAnsList.size() > 0) {
                for (int i = 0; i < customeAnsList.size(); i++) {
                    if (!MathFriendzyHelper.isEmpty(customeAnsList.get(i).getQuestionString())) {
                        return true;
                    }
                }
                return false;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    //Add links

    private void initializeAddUrlList() {
        try {
            if (!(urlList != null && urlList.size() > 0)) {
                urlList = new ArrayList<AddUrlToWorkArea>();
                AddUrlToWorkArea url = new AddUrlToWorkArea();
                url.setTitle("");
                url.setUrl("");
                urlList.add(url);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showAddLinksDialog() {
        if (addUrlLinkDialog == null) {
            addUrlLinkDialog = new AddUrlToQuestionDialog(this, true);
            addUrlLinkDialog.initializeCallback(new OnUrlSaveCallback() {
                @Override
                public void onSave(ArrayList<AddUrlToWorkArea> updatedUrlLinkList, int tag) {
                    ActAssignCustomAns.this.urlList = updatedUrlLinkList;
                    if (tag == OnUrlSaveCallback.ON_SAVE) {
                        //click to save
                    }
                }
            });
        }
        addUrlLinkDialog.showDialog(urlList);
    }

    //Keyboard issues resolve by Hardip
    public EditTextBlink selectionEdittext = null;
    private String blank_string;
    private int cursor_postion;
    public static boolean isGreen = false;


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            if (TextUtils.isEmpty(blank_string) || isGreen)
            int cur_pos = 0, ed_legnth = 0;
            try {
                cur_pos = selectedTextView.getSelectionStart();
                ed_legnth = selectedTextView.getText().toString().length();
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                // int   cur_pos = selectedTextView.getSelectionStart();
                if (cur_pos != 0) {
                    ActAssignCustomAns.isGreen = false;
                }

                if ((cur_pos != ed_legnth)) {
                    ActAssignCustomAns.isGreen = false;
                } else {
                    // ActAssignCustomAns.isGreen = true;
                }

            } catch (Exception e) {
            }


            if (isGreen) {
                try {
                    BlankFieldAlert();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }
        } else if (keyCode == KeyEvent.KEYCODE_ENTER) {

            ActAssignCustomAns.this.hideDeviceKeyboard();
            ActAssignCustomAns.this.showKeyboard();

        }


        return super.onKeyDown(keyCode, event);
    }

    private void custom_ed_touch(final EditTextBlink ed, final LinearLayout ll_sub, final LinearLayout ll_main) {

//        parenth = true;

        try {

            ed.setCursorVisible(true);
            selectedTextView = ed;
            ll_selected_view = ll_sub;
            ll_ans_box_selected = ll_main;
            selected_ed_2 = null;
            showKeyboard();

        } catch (Exception e) {
        }
    }

    public void onkb_sub_symbol_click(View v) { // Added By Siddhi Info Soft
        try {
            String btn_tag = String.valueOf(v.getTag());
            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);


            if (btn_tag.equals("parenthesis")) {
                parenth = true;
            }

            if (btn_tag.equals("fraction") ||
                    btn_tag.equals("pointview")
                    || btn_tag.equals("lim") || btn_tag.equals("parenthesis") || btn_tag.equals("absolute") | btn_tag.equals("squareroot") || btn_tag.equals("nthroot")
                    || btn_tag.equals("super_script") || btn_tag.equals("sub_script") || btn_tag.equals("super_sub_script")
                    || btn_tag.equals("line") || btn_tag.equals("lsegment") || btn_tag.equals("ray")
                    || btn_tag.equals("arc")) {
                this.addviewtoselectedview(ll_ans_box_selected, btn_tag, "view");
            } else if (btn_tag.equals("absolute")) {
                setTextToSelectedTextView(selectedTextView, "|  |");
            } else {
                this.add_sign_into_box(btn_tag);
            }

        } catch (Exception e) {

        }
    }

    public static boolean parenth = false;


    private void addviewtoselectedview(final LinearLayout ll_ans_box, String tag, String type) {  // Added By Siddhi Info Soft
        boolean is_devide = false;
        int start_pos = 0, ed_legnth = 0, ll_sub_selected_pos = 0;
        String cut_str = "";
        String sss = String.valueOf(ll_ans_box.getTag(R.id.first));
        int tag_main_ll = Integer.valueOf(sss);
        boolean need_delete = false;
        int text_size = ActAssignCustomAns.this.getResources().getInteger(R.integer.kb_textsize_main_int);


        try {
            DisplayMetrics metrics;
            metrics = getApplicationContext().getResources().getDisplayMetrics();
            float Textsize = selectedTextView.getTextSize() / metrics.density;
            text_size = Math.round(Textsize);
            Log.e("text_size", "" + text_size);
            if (text_size < 5) {
                text_size = 5;
            }

        } catch (Exception e) {
            text_size = 16;
        }

        try {

            start_pos = selectedTextView.getSelectionStart();
            ed_legnth = selectedTextView.getText().toString().length();

            String str = selectedTextView.getText().toString().trim();

            boolean pluscheck = false;
            boolean minuscheck = false;
            boolean multicheck = false;
            boolean divicheck = false;
            boolean equalcheck = false;


            if (!TextUtils.isEmpty(str)) {


                // coding for 96 or 96 + 96 for add substring


                if (!str.contains("" + Html.fromHtml("&#43;"))) {
                    pluscheck = true;

                } else {
                    int s = 0;

                    if (str.endsWith("" + Html.fromHtml("&#43;"))) {

                    } else {
                        s = str.length() - 1;
                    }


                    String temstring = str.substring(0, s);


                    // coding for 96+ or 96 + 96 + for alert

                    if (temstring.contains("" + Html.fromHtml("&#43;"))) {
                        //str.contains("" + Html.fromHtml("&#215;")) || str.contains("" + Html.fromHtml("&#247;")) || str.contains("" + Html.fromHtml("&#61;")
                        pluscheck = true;
                    } else {

                        pluscheck = false;
                    }
                }


            /*    if (str.contains("" + Html.fromHtml("&#43;"))) {
                    pluscheck = false;
                } else {
                    pluscheck = true;
                }*/


                if (!str.contains("" + Html.fromHtml("&#8722;"))) {
                    minuscheck = true;

                } else {

                    int s = 0;

                    if (str.endsWith("" + Html.fromHtml("&#8722;"))) {

                    } else {
                        s = str.length() - 1;
                    }


                    String temstring = str.substring(0, s);

                    // coding for 96+ or 96 + 96 + for alert

                    if (temstring.contains("" + Html.fromHtml("&#8722;"))) {
                        //str.contains("" + Html.fromHtml("&#215;")) || str.contains("" + Html.fromHtml("&#247;")) || str.contains("" + Html.fromHtml("&#61;")
                        minuscheck = true;
                    } else {


                        minuscheck = false;
                    }
                }

              /*  if (str.contains("" + Html.fromHtml("&#8722;"))) {
                    minuscheck = false;
                } else {
                    minuscheck = true;
                }*/

                if (!str.contains("" + Html.fromHtml("&#215;"))) {
                    multicheck = true;

                } else {
                    int s = 0;

                    if (str.endsWith("" + Html.fromHtml("&#215;"))) {

                    } else {
                        s = str.length() - 1;
                    }

                    String temstring = str.substring(0, s);

                    // coding for 96+ or 96 + 96 + for alert

                    if (temstring.contains("" + Html.fromHtml("&#215;"))) {
                        //str.contains("" + Html.fromHtml("&#215;")) || str.contains("" + Html.fromHtml("&#247;")) || str.contains("" + Html.fromHtml("&#61;")
                        multicheck = true;
                    } else {

                        multicheck = false;
                    }
                }

/*
                if (str.contains("" + Html.fromHtml("&#215;"))) {
                    multicheck = false;
                } else {
                    multicheck = true;
                }*/


                if (!str.contains("" + Html.fromHtml("&#247;"))) {
                    divicheck = true;

                } else {
                    int s = 0;

                    if (str.endsWith("" + Html.fromHtml("&#247;"))) {

                    } else {
                        s = str.length() - 1;
                    }

                    String temstring = str.substring(0, s);

                    // coding for 96+ or 96 + 96 + for alert

                    if (temstring.contains("" + Html.fromHtml("&#247;"))) {
                        //str.contains("" + Html.fromHtml("&#215;")) || str.contains("" + Html.fromHtml("&#247;")) || str.contains("" + Html.fromHtml("&#61;")
                        divicheck = true;
                    } else {


                        divicheck = false;
                    }
                }

             /*   if (str.contains("" + Html.fromHtml("&#247;"))) {
                    divicheck = false;
                } else {
                    divicheck = true;
                }*/

                if (!str.contains("" + Html.fromHtml("&#61;"))) {
                    equalcheck = true;

                } else {
                    int s = 0;

                    if (str.endsWith("" + Html.fromHtml("&#61;"))) {

                    } else {
                        s = str.length() - 1;
                    }


                    String temstring = str.substring(0, s);

                    // coding for 96+ or 96 + 96 + for alert

                    if (temstring.contains("" + Html.fromHtml("&#61;"))) {
                        //str.contains("" + Html.fromHtml("&#215;")) || str.contains("" + Html.fromHtml("&#247;")) || str.contains("" + Html.fromHtml("&#61;")
                        equalcheck = true;
                    } else {


                        equalcheck = false;
                    }
                }

               /* if (str.contains("" + Html.fromHtml("&#61;"))) {
                    equalcheck = false;
                } else {
                    equalcheck = true;
                }*/


            }


            try {
                ll_sub_selected_pos = ll_ans_box_selected.indexOfChild(ll_selected_view);
            } catch (Exception e) {
                ll_sub_selected_pos = 0;
            }

            try {
                if (ll_sub_selected_pos == -1) {
                    ll_sub_selected_pos = 0;
                }

                if (start_pos != 0 && start_pos < ed_legnth) {
                    cut_str = selectedTextView.getText().toString();
                    cut_str = cut_str.substring(start_pos, ed_legnth);
                    is_devide = true;
                    Log.e("math_ed_pos", cut_str + "" + ll_sub_selected_pos);
                    selectedTextView.getText().delete(start_pos, ed_legnth);
                }
            } catch (Exception e) {
            }

            try {
                if (selectedTextView.getText().toString().trim().length() == 0) {
                    need_delete = true;
                }
            } catch (Exception e) {
            }

            try {

                final LinearLayout ll_last = (LinearLayout) ll_ans_box.getChildAt((ll_ans_box.getChildCount() - 1));
                String str_tag = String.valueOf(ll_last.getTag());
                // Log.e("tagg",str_tag);
                if (str_tag.equals("text")) {
                    final EditTextBlink ed_last = (EditTextBlink) ll_last.getChildAt(0);
                    if (ed_last.getText().toString().trim().length() == 0) {

                        try {
                            if (ll_ans_box.getChildCount() > 1) {
                                LinearLayout ll_last_2 = (LinearLayout) ll_ans_box.getChildAt((ll_ans_box.getChildCount() - 2));
                                String str_tag_2 = String.valueOf(ll_last_2.getTag());
                                if (str_tag_2.equals("text")) {
                                    ll_ans_box.removeView(ll_last);
                                } else {
                                    ed_last.setMinWidth(4);
                                }
                            } else {
                                ed_last.setMinWidth(4);
                            }
                        } catch (Exception e) {
                        }
                    } else {
                        //  Log.e("tagg12", str_tag);
                        ed_last.setMinWidth(4);
                        ed_last.setMinimumWidth(4);
                        ed_last.requestLayout();
                        //  Log.e("tagg123", str_tag);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (type.equals("view")) {
                selected_ed_2 = null;
                if (tag.equals("super_script")) {

                    final LinearLayout ll_super_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_superscript, null);
                    ll_super_script.setTag("super_script");
                    final LinearLayout ll_top_superscript = (LinearLayout) ll_super_script.findViewById(R.id.ll_top_superscript);
                    ll_top_superscript.setTag(R.id.first, (tag_main_ll + 1));
                    ll_top_superscript.setTag(R.id.second, "super_top");

                    add_inside_ed(ll_top_superscript, true, text_size);

                 /*   if (start_pos == 0 && !need_delete) {

                        //need for validation changes
                        ll_ans_box.addView(ll_super_script, ll_sub_selected_pos);
                    } else {


                     || tag.equals("op_plus") || tag.equals("op_minus") || tag.equals("op_multi") || tag.equals("op_divide") || tag.equals("op_equal")
                        ll_ans_box.addView(ll_super_script, (ll_sub_selected_pos + 1));
                        !str.contains(Html.fromHtml("&#40;") + "" + Html.fromHtml("&#41;"))
                    }*/


                    if (start_pos == 0 && !parenth) {

                        isGreen = false;
                        AlertScript();
                        custom_ed_focus(selectionEdittext, ll_ans_box, ll_super_script, parenth);

                        //need for validation changes
                    } else if (sss.equals("2")) {

                        AlertScript();
                        custom_ed_focus(selectionEdittext, ll_ans_box, ll_super_script, parenth);

                    } /*else if (ll_sub_selected_pos != 0) {


                        if (!parenth) {

                            parenth = false;

                            AlertScript();

                        } else {
                            parenth = true;

                            ll_ans_box.addView(ll_super_script, (ll_sub_selected_pos + 1));
                        }

                    } */ else if (start_pos != 0 && !pluscheck) {

                        AlertScript();

                    } else if (start_pos != 0 && !minuscheck) {

                        AlertScript();

                    } else if (start_pos != 0 && !multicheck) {

                        AlertScript();

                    } else if (start_pos != 0 && !divicheck) {

                        AlertScript();

                    } else if (start_pos != 0 && !equalcheck) {

                        AlertScript();

                    } else if (start_pos == 1 && !need_delete) {

                        parenth = false;
                        ll_ans_box.addView(ll_super_script, (ll_sub_selected_pos + 1));

                    } else {
                        parenth = false;
                        ll_ans_box.addView(ll_super_script, (ll_sub_selected_pos + 1));
                    }
                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("sub_script")) {

                    final LinearLayout ll_sub_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_subscript, null);
                    ll_sub_script.setTag("sub_script");
                    final LinearLayout ll_bottom_subscript = (LinearLayout) ll_sub_script.findViewById(R.id.ll_bottom_subscript);
                    ll_bottom_subscript.setTag(R.id.first, (tag_main_ll + 1));
                    ll_bottom_subscript.setTag(R.id.second, "sub_bottom");
                    add_inside_ed(ll_bottom_subscript, true, text_size);

                   /* if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_sub_script, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_sub_script, (ll_sub_selected_pos + 1));
                    }*/

                    if (start_pos == 0 && !parenth) {

                        isGreen = false;
                        AlertScript();
                        custom_ed_focus(selectionEdittext, ll_ans_box, ll_sub_script, parenth);

                        //need for validation changes
                    } else if (sss.equals("2")) {

                        AlertScript();
                        custom_ed_focus(selectionEdittext, ll_ans_box, ll_sub_script, parenth);

                    }/* else if (ll_sub_selected_pos != 0) {

                        if (!parenth) {

                            parenth = false;

                            AlertScript();
                        } else {
                            parenth = true;

                            ll_ans_box.addView(ll_sub_script, (ll_sub_selected_pos + 1));
                        }

                    }*/ else if (start_pos != 0 && !pluscheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !minuscheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !multicheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !divicheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !equalcheck) {

                        AlertScript();


                    } else if (start_pos == 1 && !need_delete) {
                        parenth = false;
//                        ll_ans_box.addView(ll_sub_script, ll_sub_selected_pos);
                        ll_ans_box.addView(ll_sub_script, (ll_sub_selected_pos + 1));
                    } else {
                        parenth = false;
                        ll_ans_box.addView(ll_sub_script, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("super_sub_script")) {

                    final LinearLayout ll_super_sub_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_super_sub_script, null);
                    ll_super_sub_script.setTag("super_sub_script");

                    final LinearLayout ll_top_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_top_super_sub_script);
                    final LinearLayout ll_bottom_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_bottom_super_sub_script);
                    ll_top_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                    ll_bottom_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                    ll_top_super_sub_script.setTag(R.id.second, "ss_top");
                    ll_bottom_super_sub_script.setTag(R.id.second, "ss_bottom");
                    add_inside_ed(ll_top_super_sub_script, true, text_size);
                    add_inside_ed(ll_bottom_super_sub_script, false, text_size);

                  /*  if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_super_sub_script, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_super_sub_script, (ll_sub_selected_pos + 1));
                    }*/

                    if (start_pos == 0 && !parenth) {

                        isGreen = false;

                        AlertScript();
                        custom_ed_focus(selectionEdittext, ll_ans_box, ll_super_sub_script, parenth);

                        //need for validation changes
                    } else if (sss.equals("2")) {

                        AlertScript();
                        custom_ed_focus(selectionEdittext, ll_ans_box, ll_super_sub_script, parenth);

                    }/* else if (ll_sub_selected_pos != 0) {

                        if (!parenth) {

                            parenth = false;

                            AlertScript();
                        } else {
                            parenth = true;

                            ll_ans_box.addView(ll_super_sub_script, (ll_sub_selected_pos + 1));
                        }

                    }*/ else if (start_pos != 0 && !pluscheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !minuscheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !multicheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !divicheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !equalcheck) {

                        AlertScript();


                    } else if (start_pos == 1 && !need_delete) {

                        parenth = false;
//                        ll_ans_box.addView(ll_sub_script, ll_sub_selected_pos);
                        ll_ans_box.addView(ll_super_sub_script, (ll_sub_selected_pos + 1));
                    } else {
                        parenth = false;
                        ll_ans_box.addView(ll_super_sub_script, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("fraction")) {

                    final LinearLayout ll_fraction = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_fraction, null);
                    final LinearLayout ll_fraction_top = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_top);
                    final LinearLayout ll_fraction_bottom = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_bottom);
                    ll_fraction_top.setTag(R.id.first, (tag_main_ll + 1));
                    ll_fraction_bottom.setTag(R.id.first, (tag_main_ll + 1));
                    ll_fraction_top.setTag(R.id.second, "frac_top");
                    ll_fraction_bottom.setTag(R.id.second, "frac_bottom");
                    add_inside_ed(ll_fraction_top, true, text_size);
                    add_inside_ed(ll_fraction_bottom, false, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_fraction, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_fraction, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }

                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("squareroot")) {

                    final LinearLayout ll_squareroot = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_square_root, null);
                    final LinearLayout ll_center_square_root = (LinearLayout) ll_squareroot.findViewById(R.id.ll_center_square_root);
                    ll_center_square_root.setTag(R.id.first, (tag_main_ll + 1));
                    ll_center_square_root.setTag(R.id.second, "sqrt_c");
                    add_inside_ed(ll_center_square_root, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_squareroot, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_squareroot, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("nthroot")) {

                    final LinearLayout ll_nthroot = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_nth_root, null);

                    final LinearLayout ll_top_nth_root = (LinearLayout) ll_nthroot.findViewById(R.id.ll_top_nth_root);
                    final LinearLayout ll_center_nth_root = (LinearLayout) ll_nthroot.findViewById(R.id.ll_center_nth_root);
                    final ImageView img = (ImageView) ll_nthroot.findViewById(R.id.img);
                    ll_top_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                    ll_center_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                    ll_top_nth_root.setTag(R.id.second, "nth_top");
                    ll_center_nth_root.setTag(R.id.second, "nth_bottom");
                    add_inside_ed(ll_top_nth_root, true, text_size);
                    add_inside_ed(ll_center_nth_root, false, text_size);
                    img.getLayoutParams().height = ll_center_nth_root.getHeight();
                    img.requestLayout();
                    ll_center_nth_root.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                        @Override
                        public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                            Log.e("het", "" + v.getHeight());
                            img.getLayoutParams().height = v.getHeight();
                            img.requestLayout();
                        }
                    });

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_nthroot, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_nthroot, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("lim")) {

                    final LinearLayout ll_lim = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_lim, null);
                    final TextView tv_text = (TextView) ll_lim.findViewById(R.id.tv_text);
                    tv_text.setText(Html.fromHtml("&#8594;"));
                    final LinearLayout ll_lim_left = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_left);
                    final LinearLayout ll_lim_right = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_right);
                    ll_lim_left.setTag(R.id.first, (tag_main_ll + 1));
                    ll_lim_right.setTag(R.id.first, (tag_main_ll + 1));
                    ll_lim_left.setTag(R.id.second, "lim_left");
                    ll_lim_right.setTag(R.id.second, "lim_right");
                    add_inside_ed(ll_lim_left, true, text_size);
                    add_inside_ed(ll_lim_right, false, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_lim, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_lim, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }

                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("parenthesis")) {

                    final LinearLayout ll_parenthesis = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_parenthesis, null);
                    ll_parenthesis.setTag("parenthesis");
                    final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_parenthesis_center);
                    ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                    ll_parenthesis_center.setTag(R.id.second, "parenthesis_center");
                    add_inside_ed(ll_parenthesis_center, true, text_size);
                    final ImageView img_left = (ImageView) ll_parenthesis.findViewById(R.id.img_left);
                    final ImageView img_rht = (ImageView) ll_parenthesis.findViewById(R.id.img_rht);

                    img_left.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final LinearLayout lll = (LinearLayout) ll_parenthesis_center.getChildAt(0);
                            if (lll.getTag().equals("text")) {
                                final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                edd.setSelection(0);
                                edd.requestFocus();
                            } else {
                                add_last_ed(ll_parenthesis_center, 22, 0);
                            }
                        }
                    });

                    img_rht.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final LinearLayout lll = (LinearLayout) ll_parenthesis_center.getChildAt((ll_parenthesis_center.getChildCount() - 1));
                            if (lll.getTag().equals("text")) {
                                final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                edd.setSelection(edd.getText().toString().length());
                                edd.requestFocus();
                            } else {
                                add_last_ed(ll_parenthesis_center, 22, (ll_parenthesis_center.getChildCount()));
                            }
                        }
                    });

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_parenthesis, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_parenthesis, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("absolute")) {

                    final LinearLayout ll_parenthesis = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_absolute, null);
                    ll_parenthesis.setTag("absolute");
                    final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_absolute_center);
                    ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                    ll_parenthesis_center.setTag(R.id.second, "absolute_center");
                    add_inside_ed(ll_parenthesis_center, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_parenthesis, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_parenthesis, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("line") || tag.equals("lsegment")
                        || tag.equals("ray") || tag.equals("arc")) {

                    final LinearLayout ll_parenthesis = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_lsegment, null);

                    final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_lsegment_center);
                    final ImageView img_top = (ImageView) ll_parenthesis.findViewById(R.id.img_top);
                    ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                    if (tag.equals("line")) {
                        img_top.setImageResource(R.drawable.line_icon);
                        ll_parenthesis.setTag("line");
                        ll_parenthesis_center.setTag(R.id.second, "line_center");
                    } else if (tag.equals("lsegment")) {
                        img_top.setImageResource(R.drawable.line_segment);
                        ll_parenthesis.setTag("lsegment");
                        ll_parenthesis_center.setTag(R.id.second, "lsegment_center");
                    } else if (tag.equals("ray")) {
                        img_top.setImageResource(R.drawable.ray_icon);
                        ll_parenthesis.setTag("ray");
                        ll_parenthesis_center.setTag(R.id.second, "ray_center");
                    } else if (tag.equals("arc")) {
                        img_top.setImageResource(R.drawable.arc_icon);
                        ll_parenthesis.setTag("arc");
                        ll_parenthesis_center.setTag(R.id.second, "arc_center");
                    }

                    add_inside_ed(ll_parenthesis_center, true, text_size);


                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_parenthesis, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_parenthesis, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                }
            }

            LinearLayout ll_last_2 = (LinearLayout) ll_ans_box.getChildAt((ll_ans_box.getChildCount() - 1));
            String str_tag = String.valueOf(ll_last_2.getTag());
            if (str_tag.equals("text")) {
                EditTextBlink ed_last = (EditTextBlink) ll_last_2.getChildAt(0);
                ed_last.setMinWidth(5);

            } else {

                final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                ll_last.setTag("text");
                final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                ed_centre.setTextSize(text_size);
                ed_centre.setMinWidth(4);
                ed_centre.setMinimumWidth(4);
                ed_centre.requestLayout();
                ll_last.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ed_centre.setCursorVisible(true);
                        selectedTextView = ed_centre;
                        ll_selected_view = ll_last;
                        ll_ans_box_selected = ll_ans_box;
                        selected_ed_2 = null;
                        showKeyboard();
                    }
                });
                ed_centre.setOnTouchListener(new OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        ed_centre.setCursorVisible(true);
                        selectedTextView = ed_centre;
                        ll_selected_view = ll_last;
                        ll_ans_box_selected = ll_ans_box;
                        selected_ed_2 = null;
                        showKeyboard();
                        return false;
                    }
                });
                ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            selectedTextView = ed_centre;
                            ll_selected_view = ll_last;
                            selected_ed_2 = null;
                            ll_ans_box_selected = ll_ans_box;
                        }
                    }
                });

                ll_ans_box.addView(ll_last);

            }

        } catch (Exception e) {

        }
    }


    public void onnextclick() {


        try {
            int cur_pos = 0, ed_legnth = 0;


            try {
                cur_pos = selectedTextView.getSelectionStart();
                ed_legnth = selectedTextView.getText().toString().length();
            } catch (Exception e) {
                e.printStackTrace();
            }


//                cursor_postion = cur_pos;


       /*     try {

                if (TextUtils.isEmpty(s1)) {

                    AlertScript();
                    cur_pos = 0;

                }

            } catch (Exception e) {
                e.printStackTrace();

            }*/
            if ((cur_pos != ed_legnth)) {

                String str_bar_1 = "" + Html.fromHtml("&#x0304;");
                String str_bar_2 = "" + Html.fromHtml("&#772;");

                if (selectedTextView.getText().toString().length() > 1) {
                    String ss = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos));
                    if (ss.equals("x")) {
                        if ((cur_pos + 1) < ed_legnth) {
                            String ss_1 = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos + 1));
                            if (ss_1.equals(str_bar_1) || ss_1.equals(str_bar_2)) {
                                selectedTextView.setSelection((cur_pos + 2));
                            } else {
                                selectedTextView.setSelection((cur_pos + 1));
                            }
                        } else {
                            selectedTextView.setSelection((cur_pos + 1));
                        }
                    } else {
                        selectedTextView.setSelection((cur_pos + 1));
                    }
                } else {
                    selectedTextView.setSelection((cur_pos + 1));
                }

                if (is_touch_kb_next) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onnextclick();
                        }
                    }, 150);
                }

            } else {
                final LinearLayout ll_main_ll = ll_ans_box_selected;
                final LinearLayout ll_sub_ll = ll_selected_view;

                int pos_sub_ll = ll_main_ll.indexOfChild(ll_sub_ll);

                if (ll_main_ll.getChildCount() > 1 && pos_sub_ll < (ll_main_ll.getChildCount() - 1)) {

                    LinearLayout ll_sub = (LinearLayout) ll_main_ll.getChildAt(pos_sub_ll + 1);
                    onnextsubcall(ll_sub);

                } else if (pos_sub_ll == (ll_main_ll.getChildCount() - 1)) {

                    String main_tag = (String) ll_main_ll.getTag(R.id.second);

                    if (main_tag.equals("frac_top")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("ss_top")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(1);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("nth_top")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("lim_left")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("sqrt_c") || main_tag.equals("nth_bottom") || main_tag.equals("lim_right")) {

                        sub_next_sqrt_c(ll_main_ll);

                    } else if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom") ||
                            main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                            || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                            || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {


                        if (main_tag.equals("parenthesis_center")) {

                            parenth = true;

                        }


                        sub_next_frac_bottom(ll_main_ll);

                    }
                }

                if (is_touch_kb_next) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onnextclick();
                        }
                    }, 150);
                }
            }

        } catch (Exception e) {
        }
    }


    public void ondeleteclick() {


//        parenth = false;

        final LinearLayout ll_main_ll = ll_ans_box_selected;
        final LinearLayout ll_sub_ll = ll_selected_view;

        try {
            int ed_pos = 0, ed_legnth = 0;
            try {
                ed_pos = selectedTextView.getSelectionStart();
                ed_legnth = selectedTextView.getText().toString().trim().length();
            } catch (Exception e) {
            }
            if (ed_pos > 0 && ed_legnth > 0) {
                String str_bar_1 = "" + Html.fromHtml("&#x0304;");
                String str_bar_2 = "" + Html.fromHtml("&#772;");
                String str_integral = "" + Html.fromHtml("&#8747;");
                if (selectedTextView.getText().toString().length() > 1) {
                    String ss = String.valueOf(selectedTextView.getText().toString().charAt(ed_pos - 1));
                    if (ss.equals(str_bar_1) || ss.equals(str_bar_2)) {

                        selectedTextView.getText().delete(ed_pos - 2, ed_pos);

                    } else if (ss.equals("x")) {
                        if (ed_pos < ed_legnth) {
                            String ss_2 = String.valueOf(selectedTextView.getText().toString().charAt(ed_pos));
                            if (ss_2.equals(str_bar_1) || ss_2.equals(str_bar_2)) {
                                selectedTextView.getText().delete(ed_pos - 1, ed_pos + 1);

                            } else {
                                selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                            }
                        } else {
                            selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                        }
                    } else if (ss.equals(" ")) {
                        try {
                            String ss2 = String.valueOf(selectedTextView.getText().toString().charAt(ed_pos - 2));
                            if (ss2.equals(str_integral)) {
                                selectedTextView.getText().delete(ed_pos - 2, ed_pos);
                            } else {
                                selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                            }
                        } catch (Exception e) {
                            selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                        }
                    } else {
                        selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                    }
                } else {
                    selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                }
                try {
                    if (selectedTextView.getText().toString().trim().length() == 0) {
                        selectedTextView.setSelection(0);
                    }
                } catch (Exception e) {
                }

                if (is_touch_kb) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ondeleteclick();
                        }
                    }, 150);
                }

            } else if (ed_pos == 0 && ed_legnth == 0) {

                int pos_sub_ll = ll_main_ll.indexOfChild(ll_sub_ll);
                Log.e("delete4", "" + pos_sub_ll);

                if (pos_sub_ll != 0 && pos_sub_ll != -1) {

                    LinearLayout ll_sub = (LinearLayout) ll_main_ll.getChildAt(pos_sub_ll - 1);
                    onpresubcall(ll_sub);
                    ll_main_ll.removeViewAt(pos_sub_ll);

                } else if (pos_sub_ll == 0) {

                    String main_tag = (String) ll_main_ll.getTag(R.id.second);

                    Log.e("delete5", "" + main_tag);

                    if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                        if (ll_main_ll.getChildCount() > 1) {
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        }

                    } else if (main_tag.equals("nth_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                        if (ll_main_ll.getChildCount() > 1) {
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        }

                    } else if (main_tag.equals("lim_right")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                        if (ll_main_ll.getChildCount() > 1) {
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        }

                    } else if (main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                            || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                            || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {

                        try {
                            LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            LinearLayout ll_main2 = (LinearLayout) ll_main.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_frac_top(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {

                                if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                    sub_pre_frac_top(ll_main_ll, 2, false);
                                    ll_main2.removeViewAt(main_pos);
                                    this.add_ed_after_delete(ll_main2, 18, main_pos);
                                } else {
                                    if (main_pos != 0) {
                                        sub_pre_frac_top(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                    } else {
                                        sub_pre_frac_top(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                        try {
                                            final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                            if (lll.getTag().toString().equals("text")) {
                                                EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                edd.setMinWidth(20);
                                                edd.setMinimumWidth(20);
                                                edd.requestLayout();
                                                edd.requestFocus();
                                                if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                    Utils.change_edittext_bg(true, edd);
                                                }
                                            }
                                        } catch (Exception e) {
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("sqrt_c")) {

                        try {

                            final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            final LinearLayout ll_main1 = (LinearLayout) ll_main.getParent();
                            final LinearLayout ll_main2 = (LinearLayout) ll_main1.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main1);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_sqrt_c(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {
                                if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                    Log.e("edd", "" + main_pos);
                                    sub_pre_sqrt_c(ll_main_ll, 2, false);
                                    ll_main2.removeViewAt(main_pos);
                                    Log.e("edd1", "" + main_pos);
                                    this.add_ed_after_delete(ll_main2, 18, main_pos);
                                } else {
                                    if (main_pos != 0) {
                                        sub_pre_sqrt_c(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                    } else {
                                        sub_pre_sqrt_c(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                        try {
                                            final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                            if (lll.getTag().toString().equals("text")) {
                                                final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                edd.setMinWidth(20);
                                                edd.setMinimumWidth(20);
                                                edd.requestLayout();
                                                edd.requestFocus();
                                                if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                    Utils.change_edittext_bg(true, edd);
                                                }
                                            }
                                        } catch (Exception e) {
                                        }
                                    }
                                }
                            }

                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("nth_top")) {

                        try {
                            final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            final LinearLayout ll_main2 = (LinearLayout) ll_main.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_frac_top(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {
                                final LinearLayout ll_sqrt = (LinearLayout) ll_main.getChildAt(2);
                                final LinearLayout ll_sqrt2 = (LinearLayout) ll_sqrt.getChildAt(1);
                                if (this.makeansfromview(ll_sqrt2).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_frac_top(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            }

                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("lim_left")) {

                        try {
                            final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            final LinearLayout ll_main1 = (LinearLayout) ll_main.getParent();
                            final LinearLayout ll_main2 = (LinearLayout) ll_main1.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main1);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_sqrt_c(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {
                                final LinearLayout ll_lim_rht = (LinearLayout) ll_main.getChildAt(2);
                                if (this.makeansfromview(ll_lim_rht).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_sqrt_c(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_sqrt_c(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_sqrt_c(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            }

                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("frac_top") || main_tag.equals("ss_top")) {

                        final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        final LinearLayout ll_main2 = (LinearLayout) ll_main.getParent();
                        int main_pos = ll_main2.indexOfChild(ll_main);

                        if (ll_main_ll.getChildCount() > 1) {
                            sub_pre_frac_top(ll_main_ll, 0, false);
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        } else {
                            if (main_tag.equals("frac_top")) {
                                LinearLayout ll_frac = (LinearLayout) ll_main.getChildAt(2);
                                if (this.makeansfromview(ll_frac).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_frac_top(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            } else {
                                LinearLayout ll_frac = (LinearLayout) ll_main.getChildAt(1);

                                if (this.makeansfromview(ll_frac).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_frac_top(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            }
                        }
                    }
                }

                if (is_touch_kb) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ondeleteclick();
                        }
                    }, 150);
                }
            } else {
                this.onpreviousclick();
                if (is_touch_kb) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ondeleteclick();
                        }
                    }, 150);
                }
            }

        } catch (Exception e) {
        }
    }

    public void onpreviousclick() {

        parenth = false;
        try {
            int cur_pos = 0;

            try {
                cur_pos = selectedTextView.getSelectionStart();
            } catch (Exception e) {
            }

            if (cur_pos != 0) {

                String str_bar_1 = "" + Html.fromHtml("&#x0304;");
                String str_bar_2 = "" + Html.fromHtml("&#772;");

                if (selectedTextView.getText().toString().length() > 1) {
                    String ss = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos - 1));
                    if (ss.equals(str_bar_1) || ss.equals(str_bar_2)) {

                        selectedTextView.setSelection((cur_pos - 2));

                    } else {
                        selectedTextView.setSelection((cur_pos - 1));
                    }
                } else {
                    selectedTextView.setSelection((cur_pos - 1));
                }

                if (is_touch_kb_pre) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onpreviousclick();
                        }
                    }, 150);
                }

            } else {

                final LinearLayout ll_main_ll = ll_ans_box_selected;
                final LinearLayout ll_sub_ll = ll_selected_view;

                int pos_sub_ll = ll_main_ll.indexOfChild(ll_sub_ll);

                if (pos_sub_ll != 0 && pos_sub_ll != -1) {

                    LinearLayout ll_sub = (LinearLayout) ll_main_ll.getChildAt(pos_sub_ll - 1);
                    onpresubcall(ll_sub);

                } else if (pos_sub_ll == 0) {

                    String main_tag = (String) ll_main_ll.getTag(R.id.second);

                    if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                    } else if (main_tag.equals("nth_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                    } else if (main_tag.equals("lim_right")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                    } else if (main_tag.equals("sqrt_c") || main_tag.equals("lim_left")) {

                        try {
                            sub_pre_sqrt_c(ll_main_ll, 0, true);
                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("frac_top") || main_tag.equals("ss_top") ||
                            main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("nth_top") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                            || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                            || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {

                        try {
                            sub_pre_frac_top(ll_main_ll, 0, true);
                        } catch (Exception e) {
                        }

                    }
                }

                if (is_touch_kb_pre) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onpreviousclick();
                        }
                    }, 150);
                }

            }

        } catch (Exception e) {
        }

    }


    private void AlertScript() {
        /*final Dialog dialog = new Dialog(ActAssignCustomAns.this);
        dialog.setContentView(R.layout.script_alert_dialoug);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_title);
        TextView dialogButton = (TextView) dialog.findViewById(R.id.tv_ok);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                parenth = false;
                hideDeviceKeyboardAndClearFocus();
            }
        });
        dialog.show();*/

        MathFriendzyHelper.showWarningDialog(this,
                MathFriendzyHelper.getTreanslationTextById(this, "lblProbWithAns"),
                new HttpServerRequest() {
                    @Override
                    public void onRequestComplete() {
                        parenth = false;

                        //cleasFocusFromEditText();
                        hideDeviceKeyboardAndClearFocus();

//                        ActAssignCustomAns.this.cleasFocusFromEditText();

                    }
                });
    }


    private void BlankFieldAlert() {
        /*final Dialog dialog = new Dialog(ActAssignCustomAns.this);
        dialog.setContentView(R.layout.blank_field_alert_dialoug);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView text = (TextView) dialog.findViewById(R.id.tv_title_blank);

        TextView dialogButton = (TextView) dialog.findViewById(R.id.tv_ok_blank);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                parenth = false;
                hideDeviceKeyboardAndClearFocus();
            }
        });
        dialog.show();*/

        MathFriendzyHelper.showWarningDialog(this,
                MathFriendzyHelper.getTreanslationTextById(this, "lblLeavingFieldEmpty"),
                new HttpServerRequest() {
                    @Override
                    public void onRequestComplete() {
                        parenth = false;
                        //hideDeviceKeyboardAndClearFocus();
                    }
                });
    }

    private void setTextWatcher(final EditText editText) {
        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    if (MyRegularExpression.isSpecialCharacterExistInQuestion(source.charAt(i) + "")) {
                        MathFriendzyHelper.showWarningDialog(getCurrentObj(),
                                MathFriendzyHelper.getTreanslationTextById(getCurrentObj()
                                        , "lblSomeSpecialCharAreNotAllowed"));
                        return "";
                    }
                }
                return null;
            }
        };
        InputFilter lengthFilter = new InputFilter.LengthFilter(4);//max length limit
        editText.setFilters(new InputFilter[]{filter, lengthFilter});
    }

    private void setOnKeyListenerToEditText(EditText editText){

        editText.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard();
                return false;
            }
        });
    }


    public void setKeyboardVisibilityListener(Activity activity, final KeyboardVisibilityListener keyboardVisibilityListener) {
        final View contentView = activity.findViewById(android.R.id.content);
        contentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            private int mPreviousHeight;

            @Override
            public void onGlobalLayout() {
                int newHeight = contentView.getHeight();
                if (mPreviousHeight != 0) {
                    if (mPreviousHeight > newHeight) {
                        // Height decreased: keyboard was shown
                        keyboardVisibilityListener.onKeyboardVisibilityChanged(true);
                    } else if (mPreviousHeight < newHeight) {
                        // Height increased: keyboard was hidden
                        keyboardVisibilityListener.onKeyboardVisibilityChanged(false);
                    } else {
                        // No change
                    }
                }
                mPreviousHeight = newHeight;
            }
        });
    }

    private void setList(){
        setKeyboardVisibilityListener(this , new KeyboardVisibilityListener() {
            @Override
            public void onKeyboardVisibilityChanged(boolean keyboardVisible) {
                MathFriendzyHelper.showWarningDialog(ActAssignCustomAns.this , "Message " + keyboardVisible);
            }
        });
    }
    public interface KeyboardVisibilityListener {
        void onKeyboardVisibilityChanged(boolean keyboardVisible);
    }

}
