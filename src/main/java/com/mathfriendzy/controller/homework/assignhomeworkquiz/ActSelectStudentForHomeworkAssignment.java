package com.mathfriendzy.controller.homework.assignhomeworkquiz;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.homework.assignhomework.GetStudentByGradeResponse;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.classes.ClassWithName;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;

import java.util.ArrayList;

public class ActSelectStudentForHomeworkAssignment extends ActBase {

	private final String TAG = this.getClass().getSimpleName();
	private TextView txtSelectStudent = null;
	private ListView lstStudentList   = null;
	private String selectedGrade = "1";
	private String teacherId = null;

	private String warningNoStudentMsg = null;
	private StudentsByGradeAdapter adapter = null;

	private boolean noStudentFound = false;

	private ArrayList<GetStudentByGradeResponse> selectedStudentList = null;

    //class title changes
    private RegistereUserDto registerUserFromPreff = null;
    private boolean isClassSelected = false;
    private  ArrayList<ClassWithName> selectedClassList = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_act_select_student_for_homework_assignment);

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside onCreate()");

        this.init();
		this.getIntentValues();
		this.setWidgetsReferences();
		this.setTextFromTranslation();
		this.setListenerOnWidgets();
		this.getStudents();

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside onCreate()");
	}

    private void init() {
        try{
            registerUserFromPreff = MathFriendzyHelper.getUserFromPreff(this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
	 * Get the selected values which are set in previous screen
	 */
	@SuppressWarnings("unchecked")
	private void getIntentValues() {
		selectedGrade = this.getIntent().getStringExtra("selectedGrade");
		teacherId = this.getIntent().getStringExtra("teacherId");
		selectedStudentList = (ArrayList<GetStudentByGradeResponse>) 
				this.getIntent().getSerializableExtra("selectedStudentList");
        isClassSelected = this.getIntent().getBooleanExtra("isClassSelected" , false);
        if(isClassSelected){
            selectedClassList = (ArrayList<ClassWithName>) this.getIntent()
                    .getSerializableExtra("selectedClassList");
        }
	}

	@Override
	protected void setWidgetsReferences() {

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside setWidgetsReferences()");

		txtTopbar 		= (TextView) findViewById(R.id.txtTopbar);
		txtSelectStudent = (TextView) findViewById(R.id.txtSelectStudent);
		lstStudentList = (ListView) findViewById(R.id.lstStudentList);

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside setWidgetsReferences()");

	}

	@Override
	protected void setListenerOnWidgets() {

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside setListenerOnWidgets()");

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside setListenerOnWidgets()");

	}

	@Override
	protected void setTextFromTranslation() {

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside setTextFromTranslation()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblHomeWorkAssignment"));
		txtSelectStudent.setText(transeletion.getTranselationTextByTextIdentifier("lblSelect")
				+ " " + transeletion.getTranselationTextByTextIdentifier("mfBtnTitleStudents"));
		warningNoStudentMsg = transeletion.getTranselationTextByTextIdentifier("lblNoStudentsForGrade");
		transeletion.closeConnection();

		if(CommonUtils.LOG_ON)
			Log.e(TAG, "outside setTextFromTranslation()");

	}

	/**
	 * Get the student by grade
	 */
	private void getStudents(){
		if(selectedStudentList != null && selectedStudentList.size() > 0){
			this.setStudentAdpater(selectedStudentList);
		}else{
			/*GetStudentByGradeParam param = new GetStudentByGradeParam();
			param.setAction("getStudentsForGrade");
			param.setGrade(selectedGrade);
			param.setTeacherId(teacherId);
            if(registerUserFromPreff != null){
                param.setSchoolYear(registerUserFromPreff.getSchoolYear());
            }
			new MyAsyckTask(ServerOperation.createPostRequestForGetStudentByGrade(param)
					, null, ServerOperationUtil.GET_STUDENTS_BY_GRADE_REQUEST, this, 
					this, ServerOperationUtil.SIMPLE_DIALOG , true , 
					getString(R.string.please_wait_dialog_msg))
			.execute();*/
            selectedStudentList = new ArrayList<GetStudentByGradeResponse>();
            this.addNewStudentForAllSelecteion(selectedStudentList);
            this.setStudentAdpater(selectedStudentList);
		}
	}

	/**
	 * Set the student adapter
	 * @param arrayList
	 */
	private void setStudentAdpater(ArrayList<GetStudentByGradeResponse> arrayList){
        ArrayList<GetStudentByGradeResponse> filterList =
                this.filterStudentListBasedOnSelectedClass(arrayList);
        if(filterList != null && filterList.size() > 0){
            filterList = GetStudentByGradeResponse.removeDuplicateStudents(filterList);
            filterList.add(0, arrayList.get(0));//to show the All at the starting
        }else{//if no student in the filter list then show the All only , not to show the list in the list
            if(filterList == null)
                filterList = new ArrayList<GetStudentByGradeResponse>();
            filterList.add(0, arrayList.get(0));//to show the All at the starting
        }

        if(arrayList.size() > 0){
			adapter = new StudentsByGradeAdapter(this, arrayList , filterList);
			lstStudentList.setAdapter(adapter);
		}else{
			noStudentFound = true;
			MathFriendzyHelper.showWarningDialog(this, warningNoStudentMsg);
		}
	}

	/**
	 * Add new student with name All for selection for All
	 * @param arrayList
	 */
	private void addNewStudentForAllSelecteion(ArrayList<GetStudentByGradeResponse> arrayList){
		GetStudentByGradeResponse allObject = new GetStudentByGradeResponse();
		allObject.setfName("All");
		allObject.setlName("");
		allObject.setParentUserId("");
		allObject.setProfileImageId("");
		allObject.setSelected(true);
		arrayList.add(0, allObject);
	}

	@Override
	public void onClick(View v) {

	}

	@Override
	public void serverResponse(HttpResponseBase httpResponseBase,
			int requestCode) {
		if(requestCode == ServerOperationUtil.GET_STUDENTS_BY_GRADE_REQUEST){
			GetStudentByGradeResponse response = (GetStudentByGradeResponse) httpResponseBase;
            this.addNewStudentForAllSelecteion(response.getStudentList());
			this.setStudentAdpater(response.getStudentList());
		}
	}

	@Override
	public void onBackPressed() {
		if(!noStudentFound){
			//ArrayList<GetStudentByGradeResponse> selectedStudentList = this.getSelectedStudentList();
			Intent intent = new Intent();
			intent.putExtra("studentList", adapter.arrayList);
			setResult(RESULT_OK, intent);
			finish();
		}
		super.onBackPressed();
	}

    private ArrayList<GetStudentByGradeResponse> filterStudentListBasedOnSelectedClass
            (ArrayList<GetStudentByGradeResponse> arrayList){
        if(isClassSelected) {
            ArrayList<GetStudentByGradeResponse> newFilerStudentList
                    = new ArrayList<GetStudentByGradeResponse>();
            if (arrayList != null && arrayList.size() > 0) {
                for (int i = 0 ; i < arrayList.size() ; i ++ ) {
                    if (this.ifStudentExistForSelectedClass(arrayList.get(i).getClassId())){
                        newFilerStudentList.add(arrayList.get(i));
                    }
                }
            }
            return newFilerStudentList;
        }
        return null;
    }

    private boolean ifStudentExistForSelectedClass(int classId){
        if(selectedClassList != null && selectedClassList.size() > 0){
            for(int i = 0 ; i < selectedClassList.size() ; i ++ ){
                if(selectedClassList.get(i).getClassId() == classId){
                     return true;
                }
            }
        }
        return false;
    }
	/*private ArrayList<GetStudentByGradeResponse> getSelectedStudentList(){
		if(adapter.selectionArray[0]){
			adapter.arrayList.remove(0);
			return adapter.arrayList;
		}else{
			ArrayList<GetStudentByGradeResponse> selectedStudentList = new ArrayList<GetStudentByGradeResponse>();
			for(int i = 1 ; i < adapter.arrayList.size() ; i ++ ){
				if(adapter.selectionArray[i]){
					selectedStudentList.add(adapter.arrayList.get(i));
				}
			}
			return selectedStudentList;
		}
	}*/

}
