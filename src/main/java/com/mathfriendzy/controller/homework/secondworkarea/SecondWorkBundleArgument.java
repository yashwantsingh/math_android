package com.mathfriendzy.controller.homework.secondworkarea;

import com.mathfriendzy.model.homework.CustomeAns;
import com.mathfriendzy.model.homework.CustomePlayerAns;

import java.io.Serializable;

/**
 * Created by root on 15/6/15.
 */
public class SecondWorkBundleArgument implements Serializable {

    private CustomePlayerAns playerAns = null;
    private CustomeAns customeAns = null;

    public CustomePlayerAns getPlayerAns() {
        return playerAns;
    }
    public void setPlayerAns(CustomePlayerAns playerAns) {
        this.playerAns = playerAns;
    }

    public CustomeAns getCustomeAns() {
        return customeAns;
    }

    public void setCustomeAns(CustomeAns customeAns) {
        this.customeAns = customeAns;
    }
}
