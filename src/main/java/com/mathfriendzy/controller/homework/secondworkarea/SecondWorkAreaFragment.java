package com.mathfriendzy.controller.homework.secondworkarea;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.homework.assignhomeworkquiz.ActMultipleChoiceAnsSheet;
import com.mathfriendzy.controller.homework.homwworkworkarea.HomeWorkMsgAdapter;
import com.mathfriendzy.customview.EditTextBlink;
import com.mathfriendzy.dawnloadimagesfromserver.DownloadImageCallback;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.OnScrollChange;
import com.mathfriendzy.model.homework.CustomeAns;
import com.mathfriendzy.model.homework.CustomePlayerAns;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.ICommonUtils;

import java.io.File;
import java.util.ArrayList;

import static com.mathfriendzy.helper.MathFriendzyHelper.ON_SCROLLING;
import static com.mathfriendzy.helper.MathFriendzyHelper.SCROLL_DOWN_COMPLETE;
import static com.mathfriendzy.helper.MathFriendzyHelper.SCROLL_UP_COMPLETE;
import static com.mathfriendzy.helper.MathFriendzyHelper.scrollDown;
import static com.mathfriendzy.helper.MathFriendzyHelper.scrollUp;

/**
 * Created by root on 15/6/15.
 */
public class SecondWorkAreaFragment extends Fragment implements OnScrollChange {

    private RelativeLayout studentImageLayout = null;
    private ImageView imgStudentImage = null;
    private LinearLayout mySideDrawingLayout = null;
    private Button draw = null;
    private Button clearPaint = null;
    private Button btnScrollUp = null;
    private Button btnScrollDown = null;

    private ListView teacherStudentMsgList = null;
    private EditText edtTeacherStudentText = null;
    private Button btnSend = null;
    private ProgressBar loadingChatProgressDialog = null;

    private LinearLayout answerLayout = null;

    //for credit layout for teacher view
    private TextView txtGotHelp = null;
    private TextView txtViewedAnswer = null;
    private TextView txtCredit = null;
    private ImageView imgCheckGotHelp = null;
    private ImageView imgCheckViewdAnswer = null;
    private TextView txtCreditPercentage = null;
    private ScrollView scrollView = null;

    //private MyView mySideDrawingView = null;

    private SecondWorkBundleArgument arguments = null;
    private CustomePlayerAns playerAns = null;
    private CustomeAns customeAns = null;
    private ImageView imgDrawingImage = null;
    private HomeWorkMsgAdapter adapter = null;

    private final String TAG = this.getClass().getSimpleName();

    private String lblAnswerOnWorkAreaCreditGiven = "Answer on Work Area Credit will be given when teacher views the answer.";


    private final int HALF_CREDIT = 3;
    private final int NONE_CREDITT = 1;
    private final int FULL_CREDIT = 4;
    private final int ZERO_CREDIT = 2;

    private final String PERCENTAGE_0 = "0%";
    private final String PERCENTAGE_50 = "50%";
    private final String PERCENTAGE_100 = "100%";

    private final int FILL_IN_TYPE = 1;
    private final int MULTIPLE_CHOICE_ANS_TYPE = 1;
    private final int TRUE_FALSE_ANS_TYPE = 2;
    private final int YES_NO_ANS_TYPE = 3;

    //for teacher check answer
    private View selectedView = null;//player answer view , need to set background color on credit changes
    private String lblStudentAnswer = null;
    private String lblcorrectAns = null;
    private ProgressDialog pd = null;
    private boolean isFixedImageDone = false;
    private boolean isFixedQuestionImageDone = false;

    //to hide the answer layout from teacher check homework
    private Button imgCloseAnswerLayout = null;
    private String closeAnswersText = null;
    private String showAnswersText = null;
    private boolean isAnswerLayoutShown = true;
    private Button btnCloseViewAnswer = null;
    private Button btnDeleteAndSelectImageOption = null;
    private Button btnAddLinks = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.initArgument();
        View view = inflater.inflate(R.layout.second_tutor_area_fragment, container, false);
        this.setWidgetsReferences(view);
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.setUpViewHeightAndWidth();
        this.setWorkAreaImage(playerAns.getFixedWorkImage());
        this.setQuestionImage(playerAns.getFixedQuestionImage());
        this.setMessages(playerAns.getFixedMessage());
        this.setStudentAnswerLayout();

        this.setVisiblilityOfBtnCloseViewAnswer();
        this.closeStudentAnswerLayout();

        return view;
    }

    private void setUpViewHeightAndWidth() {
        DisplayMetrics metrics = getActivity().getBaseContext().getResources().getDisplayMetrics();
        int w = metrics.widthPixels;
        int h = metrics.heightPixels;
        h = h * 3;
        this.setViewHeightAndWidth(w, h, imgStudentImage);
        this.setViewHeightAndWidth(w, h, imgDrawingImage);
    }


    private void initArgument() {
        arguments = (SecondWorkBundleArgument) getArguments().getSerializable("arguments");
        playerAns = arguments.getPlayerAns();
        customeAns = arguments.getCustomeAns();
        this.showPopUp();
    }


    /**
     * Create popup
     */
    private void showPopUp() {
        if (pd == null)
            pd = MathFriendzyHelper.getProgressDialog(getActivity());
        if (!pd.isShowing())
            pd.show();
    }

    private void hidePopupWithCondition() {
        if (isFixedQuestionImageDone && isFixedImageDone) {
            hidePopWithoutCondition();
        }
    }

    private void hidePopWithoutCondition() {
        if (pd != null && pd.isShowing())
            pd.cancel();
    }


    /**
     * Set the widgets references
     *
     * @param view
     */
    private void setWidgetsReferences(View view) {
        studentImageLayout = (RelativeLayout) view.findViewById(R.id.studentImageLayout);
        imgStudentImage = (ImageView) view.findViewById(R.id.imgStudentImage);
        //mySideDrawingLayout = (LinearLayout) view.findViewById(R.id.mySideDrawingLayout);
        draw = (Button) view.findViewById(R.id.draw);
        clearPaint = (Button) view.findViewById(R.id.clearPaint);
        btnScrollUp = (Button) view.findViewById(R.id.btnScrollUp);
        btnScrollDown = (Button) view.findViewById(R.id.btnScrollDown);
        teacherStudentMsgList = (ListView) view.findViewById(R.id.teacherStudentMsgList);
        edtTeacherStudentText = (EditText) view.findViewById(R.id.edtTeacherStudentText);
        btnSend = (Button) view.findViewById(R.id.btnSend);
        loadingChatProgressDialog = (ProgressBar) view.findViewById(R.id.loadingChatProgressDialog);
        answerLayout = (LinearLayout) view.findViewById(R.id.answerLayout);
        txtGotHelp = (TextView) view.findViewById(R.id.txtGotHelp);
        txtViewedAnswer = (TextView) view.findViewById(R.id.txtViewedAnswer);
        txtCredit = (TextView) view.findViewById(R.id.txtCredit);
        imgCheckGotHelp = (ImageView) view.findViewById(R.id.imgCheckGotHelp);
        imgCheckViewdAnswer = (ImageView) view.findViewById(R.id.imgCheckViewdAnswer);
        txtCreditPercentage = (TextView) view.findViewById(R.id.txtCreditPercentage);
        imgDrawingImage = (ImageView) view.findViewById(R.id.imgDrawingImage);
        scrollView = (ScrollView) view.findViewById(R.id.scrollView);

        edtTeacherStudentText.setEnabled(false);
        btnSend.setBackgroundResource(R.drawable.light_gray);

        //to hide the answer layout from teacher check homework
        imgCloseAnswerLayout = (Button) view.findViewById(R.id.imgCloseAnswerLayout);
        btnCloseViewAnswer = (Button) view.findViewById(R.id.btnCloseViewAnswer);
        btnDeleteAndSelectImageOption = (Button) view.findViewById(R.id.btnDeleteAndSelectImageOption);
        btnAddLinks = (Button) view.findViewById(R.id.btnAddLinks);
    }

    /**
     * Set text from translation
     */
    private void setTextFromTranslation() {
        Translation translate = new Translation(getActivity());
        translate.openConnection();
        btnSend.setText(translate.getTranselationTextByTextIdentifier("lblSend"));
        txtGotHelp.setText(translate.getTranselationTextByTextIdentifier("lblGotHelp"));
        txtViewedAnswer.setText(translate.getTranselationTextByTextIdentifier("lblViewedAnswer"));
        txtCredit.setText(translate.getTranselationTextByTextIdentifier("lblCredit"));
        lblStudentAnswer = translate.getTranselationTextByTextIdentifier("lblStudentAnswer");
        lblcorrectAns = translate.getTranselationTextByTextIdentifier("mfLblCorrect")
                + " " + translate.getTranselationTextByTextIdentifier("mfLblAnswers");

        closeAnswersText = translate.getTranselationTextByTextIdentifier("pickerClose") + " "
                + translate.getTranselationTextByTextIdentifier("mfLblAnswers");
        showAnswersText = translate.getTranselationTextByTextIdentifier("lblView") + " "
                + translate.getTranselationTextByTextIdentifier("mfLblAnswers");
        imgCloseAnswerLayout.setText(closeAnswersText);
        translate.closeConnection();
    }

    private void setListenerOnWidgets() {
        scrollUp(btnScrollUp, scrollView, this);
        scrollDown(btnScrollDown, scrollView, this);

        //to hide the answer layout from teacher check homework
        imgCloseAnswerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeStudentAnswerLayout();
            }
        });

        btnCloseViewAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeStudentAnswerLayout();
            }
        });
    }

    /**
     * set the main view height and width
     *
     * @param w
     * @param h
     */
    private void setViewHeightAndWidth(int w, int h, View view) {
        RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(w, h);
        view.setLayoutParams(param);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    private void setWorkAreaImage(String imageName) {
        if (imageName != null && imageName.length() > 0) {
            this.downloadImageFromServer(imageName, new DownloadImageCallback() {
                @Override
                public void onDownloadComplete(File file, Bitmap bitmap) {
                    if (bitmap != null) {
                        imgDrawingImage.setImageBitmap(bitmap);
                    }
                    isFixedImageDone = true;
                    hidePopupWithCondition();
                }
            }, ICommonUtils.DOWNLOD_WORK_IMAGE_URL);
        } else {
            isFixedImageDone = true;
            hidePopupWithCondition();
        }
    }

    private void setQuestionImage(String imageName) {
        if (imageName != null && imageName.length() > 0) {
            this.downloadImageFromServer(imageName, new DownloadImageCallback() {
                @Override
                public void onDownloadComplete(File file, Bitmap bitmap) {
                    if (bitmap != null) {
                        imgStudentImage.setImageBitmap(bitmap);
                    }
                    isFixedQuestionImageDone = true;
                    hidePopupWithCondition();
                }
            }, ICommonUtils.DOWNLOAD_QUESTION_IMAGE_URL);
        } else {
            isFixedQuestionImageDone = true;
            hidePopupWithCondition();
        }
    }

    private void setMessages(String message) {
        this.setChatMessage(message);
    }

    /**
     * Set the teacher student msg
     */
    private void setChatMessage(String message) {
        if (message != null &&
                message.length() > 0) {
            ArrayList<String> studemtTeacherMsgList = MathFriendzyHelper
                    .getCommaSepratedOptionInArrayList(message,
                            MathFriendzyHelper.FULL_MESSAGE_DELIMETER);
            adapter = new HomeWorkMsgAdapter(getActivity(), studemtTeacherMsgList);
            teacherStudentMsgList.setAdapter(adapter);
            teacherStudentMsgList.setSelection
                    (teacherStudentMsgList.getAdapter().getCount() - 1);
        }
    }

    private void setStudentAnswerLayout() {
        try {
            answerLayout.removeAllViews();
            answerLayout.addView(this.getStudentAnswerLayout(1));
            answerLayout.addView(this.getStudentAnswerLayout(2));

            if (playerAns.getIsGetHelp() == 1) {
                imgCheckGotHelp.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
            }

            if (playerAns.getFirstTimeWrong() == 1) {
                imgCheckViewdAnswer.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
            }

            if (playerAns.getTeacherCredit().equalsIgnoreCase(HALF_CREDIT + "")) {
                txtCreditPercentage.setText(PERCENTAGE_50);
            } else if (playerAns.getTeacherCredit().equalsIgnoreCase(FULL_CREDIT + "")) {
                txtCreditPercentage.setText(PERCENTAGE_100);
            } else if (playerAns.getTeacherCredit().equalsIgnoreCase(ZERO_CREDIT + "")) {
                txtCreditPercentage.setText(PERCENTAGE_0);
            } else if (playerAns.getTeacherCredit().equalsIgnoreCase(NONE_CREDITT + "")) {
                if (playerAns.getIsCorrect() == 1) {
                    txtCreditPercentage.setText(PERCENTAGE_100);
                } else {
                    txtCreditPercentage.setText(PERCENTAGE_0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * For fillInType
     *
     * @param customeAns
     * @param playerAns
     * @param forStudentAns
     */
    private void setFillInLayout(CustomeAns customeAns, CustomePlayerAns playerAns,
                                 int forStudentAns, ViewHolder viewHolder) {

        if (customeAns.getFillInType() == FILL_IN_TYPE) {
            viewHolder.fillInLayout.setVisibility(RelativeLayout.VISIBLE);
            viewHolder.multipleChoiceLayout.setVisibility(RelativeLayout.GONE);
            viewHolder.trueFalseLayout.setVisibility(RelativeLayout.GONE);

            if (forStudentAns == 1) {//for player answer
                viewHolder.txtStudentAnswer.setText(customeAns.getQueNo() + "." +
                        lblStudentAnswer + ":");
                //viewHolder.ansBox.setText(playerAns.getAns());
                //Added siddhiinfosoft
                this.setstudentansbox(viewHolder.ll_ans_box, playerAns.getAns(), playerAns.getIsAnswerAvailable());
                this.setViewBackGroundByTeacherCredit(viewHolder.mainLayout, playerAns);
            } else if (forStudentAns == 2) {//for correct answer
                viewHolder.txtStudentAnswer.setText(customeAns.getQueNo() + "." +
                        lblcorrectAns + ":");
                //viewHolder.ansBox.setText(customeAns.getCorrectAns());
                //Added siddhiinfosoft
                this.setstudentansbox(viewHolder.ll_ans_box, customeAns.getCorrectAns(), customeAns.getIsAnswerAvailable());
            }

            if (customeAns.getIsLeftUnit() == 1) {
                viewHolder.txtLeftUnit.setText(customeAns.getAnsSuffix());
                viewHolder.txtRighttUnit.setText("");
            } else {
                viewHolder.txtLeftUnit.setText("");
                viewHolder.txtRighttUnit.setText(customeAns.getAnsSuffix());
            }
        }
    }

    /**
     * For True False or Yes No Type
     *
     * @param customeAns
     * @param playerAns
     * @param forStudentAns
     * @param viewHolder
     */
    private void setTrueFalseOrYesNoLayout(CustomeAns customeAns, CustomePlayerAns playerAns,
                                           int forStudentAns, ViewHolder viewHolder) {
        if (customeAns.getAnswerType() == TRUE_FALSE_ANS_TYPE
                || customeAns.getAnswerType() == YES_NO_ANS_TYPE) {
            viewHolder.fillInLayout.setVisibility(RelativeLayout.GONE);
            viewHolder.multipleChoiceLayout.setVisibility(RelativeLayout.GONE);
            viewHolder.trueFalseLayout.setVisibility(RelativeLayout.VISIBLE);

            if (customeAns.getAnswerType() == TRUE_FALSE_ANS_TYPE) {
                viewHolder.ansTrue.setText(ActMultipleChoiceAnsSheet.TRUE);
                viewHolder.ansFalse.setText(ActMultipleChoiceAnsSheet.FALSE);
            } else if (customeAns.getAnswerType() == YES_NO_ANS_TYPE) {
                viewHolder.ansTrue.setText(ActMultipleChoiceAnsSheet.YES);
                viewHolder.ansFalse.setText(ActMultipleChoiceAnsSheet.NO);
            }

            if (forStudentAns == 1) {//for player answer
                viewHolder.txtStudentAnswer.setText(customeAns.getQueNo() + "." +
                        lblStudentAnswer + ":");
                if (customeAns.getAnswerType() == TRUE_FALSE_ANS_TYPE) {
                    if (playerAns.getAns().equalsIgnoreCase
                            (ActMultipleChoiceAnsSheet.TRUE)) {
                        viewHolder.ansTrue.setBackgroundResource
                                (R.drawable.checked_box_home_work);
                        viewHolder.ansFalse.setBackgroundResource
                                (R.drawable.unchecked_box_home_work);
                    } else if (playerAns.getAns().equalsIgnoreCase
                            (ActMultipleChoiceAnsSheet.FALSE)) {
                        viewHolder.ansTrue.setBackgroundResource
                                (R.drawable.unchecked_box_home_work);
                        viewHolder.ansFalse.setBackgroundResource
                                (R.drawable.checked_box_home_work);
                    }
                } else if (customeAns.getAnswerType() == YES_NO_ANS_TYPE) {
                    if (playerAns.getAns().equalsIgnoreCase
                            (ActMultipleChoiceAnsSheet.YES)) {
                        viewHolder.ansTrue.setBackgroundResource
                                (R.drawable.checked_box_home_work);
                        viewHolder.ansFalse.setBackgroundResource
                                (R.drawable.unchecked_box_home_work);
                    } else if (playerAns.getAns().equalsIgnoreCase
                            (ActMultipleChoiceAnsSheet.NO)) {
                        viewHolder.ansTrue.setBackgroundResource
                                (R.drawable.unchecked_box_home_work);
                        viewHolder.ansFalse.setBackgroundResource
                                (R.drawable.checked_box_home_work);
                    }
                }
                this.setViewBackGroundByTeacherCredit(viewHolder.mainLayout, playerAns);
            } else if (forStudentAns == 2) {//for correct answer
                viewHolder.txtStudentAnswer.setText(customeAns.getQueNo() + "." +
                        lblcorrectAns + ":");
                if (customeAns.getAnswerType() == TRUE_FALSE_ANS_TYPE) {
                    if (customeAns.getCorrectAns().equalsIgnoreCase
                            (ActMultipleChoiceAnsSheet.TRUE)) {
                        viewHolder.ansTrue.setBackgroundResource
                                (R.drawable.checked_box_home_work);
                        viewHolder.ansFalse.setBackgroundResource
                                (R.drawable.unchecked_box_home_work);
                    } else if (customeAns.getCorrectAns().equalsIgnoreCase
                            (ActMultipleChoiceAnsSheet.FALSE)) {
                        viewHolder.ansTrue.setBackgroundResource
                                (R.drawable.unchecked_box_home_work);
                        viewHolder.ansFalse.setBackgroundResource
                                (R.drawable.checked_box_home_work);
                    }
                } else if (customeAns.getAnswerType() == YES_NO_ANS_TYPE) {
                    if (customeAns.getCorrectAns().equalsIgnoreCase
                            (ActMultipleChoiceAnsSheet.YES)) {
                        viewHolder.ansTrue.setBackgroundResource
                                (R.drawable.checked_box_home_work);
                        viewHolder.ansFalse.setBackgroundResource
                                (R.drawable.unchecked_box_home_work);
                    } else if (customeAns.getCorrectAns().equalsIgnoreCase
                            (ActMultipleChoiceAnsSheet.NO)) {
                        viewHolder.ansTrue.setBackgroundResource
                                (R.drawable.unchecked_box_home_work);
                        viewHolder.ansFalse.setBackgroundResource
                                (R.drawable.checked_box_home_work);
                    }
                }
            }
        }
    }


    /**
     * For Multiple choice layout
     *
     * @param customeAns
     * @param playerAns
     * @param forStudentAns
     * @param viewHolder
     */
    private void setMultipleChoiceLayout(CustomeAns customeAns, CustomePlayerAns playerAns,
                                         int forStudentAns, ViewHolder viewHolder) {
        if (customeAns.getAnswerType() == MULTIPLE_CHOICE_ANS_TYPE) {
            viewHolder.fillInLayout.setVisibility(RelativeLayout.GONE);
            viewHolder.multipleChoiceLayout.setVisibility(RelativeLayout.VISIBLE);
            viewHolder.trueFalseLayout.setVisibility(RelativeLayout.GONE);

            if (forStudentAns == 1) {//for student answer
                viewHolder.txtStudentAnswer.setText(customeAns.getQueNo() + "." +
                        lblStudentAnswer + ":");
                this.setViewBackGroundByTeacherCredit(viewHolder.mainLayout, playerAns);
            } else if (forStudentAns == 2) {//for correct answer
                viewHolder.txtStudentAnswer.setText(customeAns.getQueNo() + "." +
                        lblcorrectAns + ":");
            }

            String[] commaSepratedOption = MathFriendzyHelper.
                    getCommaSepratedOption(customeAns.getOptions(), ",");
            for (int i = 0; i < commaSepratedOption.length; i++) {
                viewHolder.optionLayout.addView(this.getMultipleOptionLayout
                        (commaSepratedOption[i], customeAns, playerAns, forStudentAns));
            }
        }
    }

    /**
     * Multiple choice option with selection
     *
     * @param optionText
     * @param forStudentAns
     * @return
     */
    @SuppressLint("InflateParams")
    private View getMultipleOptionLayout(String optionText, CustomeAns customeAns,
                                         CustomePlayerAns playerAns, int forStudentAns) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.check_home_work_option_layout
                , null);
        TextView option = (TextView) view.findViewById(R.id.option);
        option.setText(optionText);
        if (forStudentAns == 1) {//1 for student answer
            if (playerAns.getAns().contains(optionText)) {
                option.setBackgroundResource(R.drawable.checked_small_box_home_work);
            }
        } else if (forStudentAns == 2) {//2 for correct answer
            if (customeAns.getCorrectAns().contains(optionText)) {
                option.setBackgroundResource(R.drawable.checked_small_box_home_work);
            }
        }
        return view;
    }

    /**
     * Set the view background according to the teacher credit
     *
     * @param playerAns
     */
    private void setViewBackGroundByTeacherCredit(View view, CustomePlayerAns playerAns) {

        try {
            //for save view
            selectedView = view;

            if (playerAns.getTeacherCredit().equals("1")) {//for no credit
                if (playerAns.getIsCorrect() == 0) {
                    view.setBackgroundColor(getResources().getColor(R.color.ZERO_CREDIT));
                }
            } else if (playerAns.getTeacherCredit().equals("2")) {//for zero credit
                view.setBackgroundColor(getResources().getColor(R.color.ZERO_CREDIT));
            } else if (playerAns.getTeacherCredit().equals("3")) {//half credit
                view.setBackgroundColor(getResources().getColor(R.color.HALF_CREDIT));
            } else if (playerAns.getTeacherCredit().equals("4")) {//full credit
                view.setBackgroundColor(getResources().getColor(R.color.FULL_CREDIT));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Return the inflated answer layout , for the student answer and correct answer display
     *
     * @param forStudentAns
     * @return
     */
    @SuppressLint("InflateParams")
    private View getStudentAnswerLayout(int forStudentAns) {
        View view = LayoutInflater.from(getActivity())
                .inflate(R.layout.kb_check_hw_workarea_answer_layout, null);

        ViewHolder viewHolder = new ViewHolder();
        viewHolder.mainLayout = (RelativeLayout) view.findViewById(R.id.mainLayout);
        viewHolder.txtStudentAnswer = (TextView) view.findViewById(R.id.txtStudentAnswer);
        viewHolder.fillInLayout = (RelativeLayout) view.findViewById(R.id.fillInLayout);
        viewHolder.txtLeftUnit = (TextView) view.findViewById(R.id.txtLeftUnit);
        viewHolder.ansBox = (TextView) view.findViewById(R.id.ansBox);
        viewHolder.txtRighttUnit = (TextView) view.findViewById(R.id.txtRighttUnit);
        viewHolder.multipleChoiceLayout = (RelativeLayout) view.findViewById(R.id.multipleChoiceLayout);
        viewHolder.optionLayout = (LinearLayout) view.findViewById(R.id.optionLayout);
        viewHolder.trueFalseLayout = (RelativeLayout) view.findViewById(R.id.trueFalseLayout);
        viewHolder.ansTrue = (TextView) view.findViewById(R.id.ansTrue);
        viewHolder.ansFalse = (TextView) view.findViewById(R.id.ansFalse);
        viewHolder.ll_ans_box = (LinearLayout) view.findViewById(R.id.ll_ans_box);

        this.setFillInLayout(customeAns, playerAns, forStudentAns, viewHolder);
        this.setTrueFalseOrYesNoLayout(customeAns, playerAns, forStudentAns, viewHolder);
        this.setMultipleChoiceLayout(customeAns, playerAns, forStudentAns, viewHolder);
        return view;
    }

    /**
     * Download image from server
     *
     * @param imageName
     * @param callback
     */
    public void downloadImageFromServer(String imageName,
                                        final DownloadImageCallback callback, String url) {
        MathFriendzyHelper.downloadImageFromGivenUrl(url
                , imageName, getActivity(), new DownloadImageCallback() {
            @Override
            public void onDownloadComplete(File file,
                                           Bitmap bitmap) {
                callback.onDownloadComplete(file, bitmap);
            }
        });
    }

    @Override
    public void onScrolling(int state) {
        switch (state) {
            case ON_SCROLLING:
                btnScrollDown.setBackgroundResource(R.drawable.down);
                btnScrollUp.setBackgroundResource(R.drawable.up);
                break;
            case SCROLL_DOWN_COMPLETE:
                btnScrollDown.setBackgroundResource(R.drawable.unselected_down);
                btnScrollUp.setBackgroundResource(R.drawable.up);
                break;
            case SCROLL_UP_COMPLETE:
                btnScrollDown.setBackgroundResource(R.drawable.down);
                btnScrollUp.setBackgroundResource(R.drawable.unselected_up);
                break;
        }
    }

    //for Activity open for teacher
    class ViewHolder {
        private RelativeLayout mainLayout;
        private TextView txtStudentAnswer;

        //for fillIn layout
        private RelativeLayout fillInLayout;
        private TextView txtLeftUnit;
        private TextView ansBox;
        private TextView txtRighttUnit;

        //for multiple choice layout
        private RelativeLayout multipleChoiceLayout;
        private LinearLayout optionLayout, ll_ans_box;
        //for True False or Yes No layout
        private RelativeLayout trueFalseLayout;
        private TextView ansTrue;
        private TextView ansFalse;
    }

    //siddhiinfosoft BELOW TWO METHODS ADDED BY SIDDHIINFOSOFT

    public void setstudentansbox(final LinearLayout ll_ans_box, final
    String strAns, final int isansavail) {
        try {
            ll_ans_box.setTag(R.id.first, "1");
            ll_ans_box.setTag(R.id.second, "main");
            if (isansavail == MathFriendzyHelper.NO) {

                final LinearLayout ll_last = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.kb_view_textview, null);
                ll_last.setTag("vtext");
                final TextView tv = (TextView) ll_last.findViewById(R.id.tv);
                tv.setText(lblAnswerOnWorkAreaCreditGiven);

//                tv.setOnClickListener(new OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        MathFriendzyHelper.showWarningDialog(HomeWorkWorkArea.this,
//                                lblAnswerOnWorkAreaCreditGiven);
//                    }
//                });

                ll_ans_box.addView(ll_last);

            } else {

                if (strAns.trim().length() != 0) {
                    setstudentansboxsub(strAns, ll_ans_box);
                }

            }

        } catch (Exception e) {

        }
    }


    public void setstudentansboxsub(final String str_ans, final LinearLayout ll_ans_box) {

        String[] str_1 = str_ans.split(ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first));

        String sss = String.valueOf(ll_ans_box.getTag(R.id.first));
        int tag_main_ll = Integer.valueOf(sss);

        for (int i = 0; i < str_1.length; i++) {

            String str_1_value = str_1[i];

            if (str_1_value.startsWith("text")) {

                str_1_value = str_1_value.substring(5);

                String sss1 = String.valueOf(ll_ans_box.getTag(R.id.first));
                int tag1 = Integer.valueOf(sss1);
                String tag2 = String.valueOf(ll_ans_box.getTag(R.id.second));
                int text_size = SecondWorkAreaFragment.this.getResources().getInteger(R.integer.kb_textsize_main_int);

                try {

                    if (tag2.equals("super_top") || tag2.equals("sub_bottom") || tag2.equals("ss_top") ||
                            tag2.equals("ss_bottom") || tag2.equals("nth_top") || tag2.equals("lim_left") || tag2.equals("lim_right")) {
                        text_size = (text_size - 1) - (tag1 + 1);
                    } else if (tag2.equals("frac_top") || tag2.equals("frac_bottom") || tag2.equals("sqrt_c") || tag2.equals("nth_bottom")
                            || tag2.equals("parenthesis_center") || tag2.equals("absolute_center")
                            || tag2.equals("line_center") || tag2.equals("lsegment_center")
                            || tag2.equals("ray_center") || tag2.equals("arc_center")) {
                        text_size = text_size - (tag1 + 1);
                    }

                } catch (Exception e) {

                }

                final LinearLayout ll_last = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                ll_last.setTag("text");
                final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                this.replacesignfromchar(str_1_value, ed_centre);
                ed_centre.setMinWidth(20);
                ed_centre.setTextSize(text_size);

                ll_ans_box.addView(ll_last);

            }
            if (str_1_value.startsWith("fraction")) {

                str_1_value = str_1_value.substring(8);

                final LinearLayout ll_fraction = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.kb_view_fraction, null);
                final LinearLayout ll_fraction_top = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_top);
                final LinearLayout ll_fraction_bottom = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_bottom);
                ll_fraction_top.setTag(R.id.first, (tag_main_ll + 1));
                ll_fraction_bottom.setTag(R.id.first, (tag_main_ll + 1));
                ll_fraction_top.setTag(R.id.second, "frac_top");
                ll_fraction_bottom.setTag(R.id.second, "frac_bottom");

                final String[] str_2 = str_1_value.split("frac_btm_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_fraction);

                this.setstudentansboxsub(str_2[0], ll_fraction_top);
                this.setstudentansboxsub(str_2[1], ll_fraction_bottom);

            } else if (str_1_value.startsWith("super_script")) {
                str_1_value = str_1_value.substring(12);

                final LinearLayout ll_super_script = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.kb_new_view_superscript, null);
                final LinearLayout ll_top_superscript = (LinearLayout) ll_super_script.findViewById(R.id.ll_top_superscript);
                ll_top_superscript.setTag(R.id.first, (tag_main_ll + 1));
                ll_top_superscript.setTag(R.id.second, "super_top");

                ll_ans_box.addView(ll_super_script);

                this.setstudentansboxsub(str_1_value, ll_top_superscript);

            } else if (str_1_value.startsWith("sub_script")) {
                str_1_value = str_1_value.substring(10);

                final LinearLayout ll_super_script = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.kb_new_view_subscript, null);
                final LinearLayout ll_bottom_subscript = (LinearLayout) ll_super_script.findViewById(R.id.ll_bottom_subscript);
                ll_bottom_subscript.setTag(R.id.first, (tag_main_ll + 1));
                ll_bottom_subscript.setTag(R.id.second, "sub_bottom");

                ll_ans_box.addView(ll_super_script);

                this.setstudentansboxsub(str_1_value, ll_bottom_subscript);

            } else if (str_1_value.startsWith("super_sub_script")) {

                str_1_value = str_1_value.substring(16);

                final LinearLayout ll_super_sub_script = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.kb_new_view_super_sub_script, null);
                final LinearLayout ll_top_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_top_super_sub_script);
                final LinearLayout ll_bottom_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_bottom_super_sub_script);
                ll_top_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                ll_bottom_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                ll_top_super_sub_script.setTag(R.id.second, "ss_top");
                ll_bottom_super_sub_script.setTag(R.id.second, "ss_bottom");

                final String[] str_2 = str_1_value.split("ss_btm_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_super_sub_script);

                this.setstudentansboxsub(str_2[0], ll_top_super_sub_script);
                this.setstudentansboxsub(str_2[1], ll_bottom_super_sub_script);

            } else if (str_1_value.startsWith("sqrt")) {

                str_1_value = str_1_value.substring(4);
                final LinearLayout ll_sqrt = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.kb_view_square_root, null);
                final LinearLayout ll_center_square_root = (LinearLayout) ll_sqrt.findViewById(R.id.ll_center_square_root);
                ll_center_square_root.setTag(R.id.first, (tag_main_ll + 1));
                ll_center_square_root.setTag(R.id.second, "sqrt_c");

                ll_ans_box.addView(ll_sqrt);

                this.setstudentansboxsub(str_1_value, ll_center_square_root);

            } else if (str_1_value.startsWith("nsqrt")) {

                str_1_value = str_1_value.substring(5);

                final LinearLayout ll_nsqrt = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.kb_view_nth_root, null);
                final LinearLayout ll_top_nth_root = (LinearLayout) ll_nsqrt.findViewById(R.id.ll_top_nth_root);
                final LinearLayout ll_center_nth_root = (LinearLayout) ll_nsqrt.findViewById(R.id.ll_center_nth_root);
                final ImageView img = (ImageView) ll_nsqrt.findViewById(R.id.img);
                ll_top_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                ll_center_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                ll_top_nth_root.setTag(R.id.second, "nth_top");
                ll_center_nth_root.setTag(R.id.second, "nth_bottom");

                img.getLayoutParams().height = ll_center_nth_root.getHeight();
                img.requestLayout();
                ll_center_nth_root.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                    @Override
                    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                        Log.e("het", "" + v.getHeight());
                        img.getLayoutParams().height = v.getHeight();
                        img.requestLayout();
                    }
                });

                final String[] str_2 = str_1_value.split("nsqrt_center_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_nsqrt);

                this.setstudentansboxsub(str_2[0], ll_top_nth_root);
                this.setstudentansboxsub(str_2[1], ll_center_nth_root);

            } else if (str_1_value.startsWith("lim")) {

                str_1_value = str_1_value.substring(3);

                final LinearLayout ll_lim = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.kb_new_view_lim, null);
                final TextView tv_text = (TextView) ll_lim.findViewById(R.id.tv_text);
                tv_text.setText(Html.fromHtml("&#8594;"));
                final LinearLayout ll_lim_left = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_left);
                final LinearLayout ll_lim_right = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_right);
                ll_lim_left.setTag(R.id.first, (tag_main_ll + 1));
                ll_lim_right.setTag(R.id.first, (tag_main_ll + 1));
                ll_lim_left.setTag(R.id.second, "lim_left");
                ll_lim_right.setTag(R.id.second, "lim_right");

                final String[] str_2 = str_1_value.split("lim_rht_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_lim);

                this.setstudentansboxsub(str_2[0], ll_lim_left);
                this.setstudentansboxsub(str_2[1], ll_lim_right);

            } else if (str_1_value.startsWith("parenthesis")) {
                str_1_value = str_1_value.substring(11);

                final LinearLayout ll_super_script = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.kb_view_parenthesis, null);
                final LinearLayout ll_parenthesis_center = (LinearLayout) ll_super_script.findViewById(R.id.ll_parenthesis_center);
                ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                ll_parenthesis_center.setTag(R.id.second, "parenthesis_center");

                ll_ans_box.addView(ll_super_script);

                this.setstudentansboxsub(str_1_value, ll_parenthesis_center);

            } else if (str_1_value.startsWith("absolute")) {
                str_1_value = str_1_value.substring(8);

                final LinearLayout ll_super_script = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.kb_view_absolute, null);
                final LinearLayout ll_parenthesis_center = (LinearLayout) ll_super_script.findViewById(R.id.ll_absolute_center);
                ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                ll_parenthesis_center.setTag(R.id.second, "absolute_center");

                ll_ans_box.addView(ll_super_script);

                this.setstudentansboxsub(str_1_value, ll_parenthesis_center);

            } else if (str_1_value.startsWith("line") || str_1_value.startsWith("lsegment") || str_1_value.startsWith("ray")
                    || str_1_value.startsWith("arc")) {

                final LinearLayout ll_parenthesis = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.kb_view_lsegment, null);
                final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_lsegment_center);
                ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                final ImageView img_top = (ImageView) ll_parenthesis.findViewById(R.id.img_top);
                // ll_parenthesis_center.setTag(R.id.second,"absolute_center");

                if (str_1_value.startsWith("line")) {
                    img_top.setImageResource(R.drawable.line_icon);
                    str_1_value = str_1_value.substring(4);
                    ll_parenthesis.setTag("line");
                    ll_parenthesis_center.setTag(R.id.second, "line_center");
                } else if (str_1_value.startsWith("lsegment")) {
                    str_1_value = str_1_value.substring(8);
                    img_top.setImageResource(R.drawable.line_segment);
                    ll_parenthesis.setTag("lsegment");
                    ll_parenthesis_center.setTag(R.id.second, "lsegment_center");
                } else if (str_1_value.startsWith("ray")) {
                    str_1_value = str_1_value.substring(3);
                    img_top.setImageResource(R.drawable.ray_icon);
                    ll_parenthesis.setTag("ray");
                    ll_parenthesis_center.setTag(R.id.second, "ray_center");
                } else if (str_1_value.startsWith("arc")) {
                    str_1_value = str_1_value.substring(3);
                    img_top.setImageResource(R.drawable.arc_icon);
                    ll_parenthesis.setTag("arc");
                    ll_parenthesis_center.setTag(R.id.second, "arc_center");
                }

                ll_ans_box.addView(ll_parenthesis);

                this.setstudentansboxsub(str_1_value, ll_parenthesis_center);

            }

        }
    }

    private String replacesignfromchar(final String str, final EditTextBlink ed) {
        String str_final = str;

        try {

            String str_plus = "" + Html.fromHtml("&#43;");
            String str_minus = "" + Html.fromHtml("&#8722;");
            String str_equal = "" + Html.fromHtml("&#61;");
            String str_multi = "" + Html.fromHtml("&#215;");
            String str_divide = "" + Html.fromHtml("&#247;");
            String str_union = "" + Html.fromHtml("&#8746;");
            String str_intersection = "" + Html.fromHtml("&#8745;");
            String str_pi = "" + Html.fromHtml("&#960;");
            String str_factorial = "" + Html.fromHtml("&#33;");
            String str_percentage = "" + Html.fromHtml("&#37;");
            String str_goe = "" + Html.fromHtml("&#8805;");
            String str_loe = "" + Html.fromHtml("&#8804;");
            String str_grthan = "" + Html.fromHtml("&#62;");
            String str_lethan = "" + Html.fromHtml("&#60;");
            String str_infinity = "" + Html.fromHtml("&#8734;");
            String str_degree = "" + Html.fromHtml("&#176;");
            String str_xbar = "" + Html.fromHtml("x&#x0304;");
            String str_muxbar = "" + Html.fromHtml("&#956;") + "" + Html.fromHtml("x&#772;");
            String str_sigmaxbar = "" + Html.fromHtml("&#963;") + "" + Html.fromHtml("x&#772;");

            // Added merging siddhi infosoft
            String str_integral = "" + Html.fromHtml("&#8747;");
            String str_summation = "" + Html.fromHtml("&#8721;");
            String str_alpha = "" + Html.fromHtml("&#945;");
            String str_theta = "" + Html.fromHtml("ø");
            String str_mu = "" + Html.fromHtml("&#956;");
            String str_sigma = "" + Html.fromHtml("&#963;");

            String str_beta = "" + Html.fromHtml("&#946;");
            String str_angle = "" + Html.fromHtml("&#8736;");
            String str_mangle = "" + Html.fromHtml("&#8737;");
            String str_sangle = "" + Html.fromHtml("&#8738;");
            String str_rangle = "" + Html.fromHtml("&#8735;");
            String str_triangle = "" + Html.fromHtml("&#9651;");
            String str_rectangle = "" + Html.fromHtml("&#9645;");
            String str_parallelogram = "" + Html.fromHtml("&#9649;");
            String str_perpendicular = "" + Html.fromHtml("&#8869;");
            String str_congruent = "" + Html.fromHtml("&#8773;");
            String str_similarty = "" + Html.fromHtml("&#8764;");
            String str_parallel = "" + Html.fromHtml("&#8741;");

            String str_arcm = "" + Html.fromHtml("&#8242;");
            String str_arcs = "" + Html.fromHtml("&#8243;");

            str_final = str_final.replace("op_plus", str_plus);
            str_final = str_final.replace("op_minus", str_minus);
            str_final = str_final.replace("op_equal", str_equal);
            str_final = str_final.replace("op_multi", str_multi);
            str_final = str_final.replace("op_divide", str_divide);
            str_final = str_final.replace("op_union", str_union);
            str_final = str_final.replace("op_intersection", str_intersection);
            str_final = str_final.replace("op_pi", str_pi);
            str_final = str_final.replace("op_factorial", str_factorial);
            str_final = str_final.replace("op_percentage", str_percentage);
            str_final = str_final.replace("op_goe", str_goe);
            str_final = str_final.replace("op_loe", str_loe);
            str_final = str_final.replace("op_grthan", str_grthan);
            str_final = str_final.replace("op_lethan", str_lethan);
            str_final = str_final.replace("op_infinity", str_infinity);
            str_final = str_final.replace("op_degree", str_degree);
            str_final = str_final.replace("op_xbar", str_xbar);
            str_final = str_final.replace("op_muxbar", str_muxbar);
            str_final = str_final.replace("op_sigmaxbar", str_sigmaxbar);


            // Added merging siddhi infosoft

            str_final = str_final.replace("op_alpha", str_alpha);
            str_final = str_final.replace("op_theta", str_theta);
            str_final = str_final.replace("op_mu", str_mu);
            str_final = str_final.replace("op_sigma", str_sigma);

            str_final = str_final.replace("op_beta", str_beta);
            str_final = str_final.replace("op_angle", str_angle);
            str_final = str_final.replace("op_mangle", str_mangle);
            str_final = str_final.replace("op_sangle", str_sangle);
            str_final = str_final.replace("op_rangle", str_rangle);
            str_final = str_final.replace("op_triangle", str_triangle);
            str_final = str_final.replace("op_rectangle", str_rectangle);
            str_final = str_final.replace("op_parallelogram", str_parallelogram);
            str_final = str_final.replace("op_perpendicular", str_perpendicular);
            str_final = str_final.replace("op_congruent", str_congruent);
            str_final = str_final.replace("op_similarty", str_similarty);
            str_final = str_final.replace("op_parallel", str_parallel);

            str_final = str_final.replace("op_arcm", str_arcm);
            str_final = str_final.replace("op_arcs", str_arcs);

            str_final = str_final.replace("op_integral", str_integral);
            str_final = str_final.replace("op_summation", str_summation);

            for (int i = 0; i < str_final.length(); i++) {

                String ss = String.valueOf(str_final.charAt(i));

                if (ss.equals(str_integral) || ss.equals(str_summation)) {
                    SpannableString ss1 = new SpannableString(ss);
                    ss1.setSpan(new RelativeSizeSpan(1.6f), 0, 1, 0);
                    ed.getText().insert(ed.getSelectionStart(), ss1);
                } else if (ss.equals(str_perpendicular) || ss.equals(str_parallel) ||
                        ss.equals(str_rectangle) || ss.equals(str_parallelogram)) {
                    SpannableString ss1 = new SpannableString(ss);
                    ss1.setSpan(new RelativeSizeSpan(1.2f), 0, 1, 0);
                    ed.getText().insert(ed.getSelectionStart(), ss1);
                } else if (ss.equals(str_angle) || ss.equals(str_mangle) ||
                        ss.equals(str_sangle) || ss.equals(str_rangle)) {
                    SpannableString ss1 = new SpannableString(ss);
                    ss1.setSpan(new RelativeSizeSpan(1.5f), 0, 1, 0);
                    ed.getText().insert(ed.getSelectionStart(), ss1);
                } else {
                    ed.getText().insert(ed.getSelectionStart(), ss);
                }

                if (ss.equals(str_integral)) {
                    ed.getText().insert(ed.getSelectionStart(), " ");
                }

            }
        } catch (Exception e) {

        }

        return str_final;
    }

    //to hide the answer layout from teacher check homework
    private void setVisiblilityOfBtnCloseViewAnswer(){
        btnCloseViewAnswer.setVisibility(View.VISIBLE);
        btnDeleteAndSelectImageOption.setVisibility(View.INVISIBLE);
        btnAddLinks.setVisibility(View.GONE);
    }

    //to hide the answer layout from teacher check homework
    private void closeStudentAnswerLayout() {
        try {
            isAnswerLayoutShown = !isAnswerLayoutShown;
            if(isAnswerLayoutShown) {
                imgCloseAnswerLayout.setText(closeAnswersText);
                answerLayout.setVisibility(View.VISIBLE);
            }else{
                imgCloseAnswerLayout.setText(showAnswersText);
                answerLayout.setVisibility(View.GONE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Return the paint object
     *
     * @param
     * @return
     *//*
    private Paint getPaint() {
        Paint mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(Color.BLACK);
        mPaint.setStrokeWidth(3);
        mPaint.setStyle(Paint.Style.STROKE);
        return mPaint;
    }*/

    /**
     * Return the bitmap view
     *
     * @param w
     * @param h
     * @return
     *//*
    private Bitmap getBitMap(int w, int h) {
        return Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
    }*/

    /*private void setUpDrawingView(Bitmap bitmap){
        *//*DisplayMetrics metrics =  getActivity().getBaseContext().getResources().getDisplayMetrics();
        int w = metrics.widthPixels;
        int h = metrics.heightPixels;
        if (bitmap != null) {
            if (bitmap.getHeight() > h) {
                h = bitmap.getHeight();
            } else {
                h = h + 1000;
            }
        } else {
            h = h + 1000;
        }

        if(bitmap == null){
            bitmap = this.getBitMap(w, h);
        }
        mySideDrawingView = new MyView(getActivity(), h, w, bitmap,
                this.getPaint(), mySideDrawingCallback);
        this.setViewHeightAndWidth(w, h, mySideDrawingView);
        mySideDrawingView.setBackgroundColor(Color.TRANSPARENT);
        mySideDrawingView.setDrawingCacheEnabled(true);
        mySideDrawingLayout.addView(mySideDrawingView);
        mySideDrawingView.setDrawMode(false);*//*
    }*/

     /* *//**
     * Drawing callbacks
     *//*
    DrawingCallback mySideDrawingCallback = new DrawingCallback() {

        @Override
        public void onUp(final float x, final float y) {

        }

        *//**
     * x = mX
     * y = mY
     * ex = device points x
     * ey = device points y
     *//*
        @Override
        public void onStart(final float x, final float y,
                            final float ex, final float ey) {
        }

        *//**
     * x = mX
     * y = mY
     * ex = device points x
     * ey = device points y
     *//*
        @Override
        public void onMove(final float x, final float y, final float ex, final float ey) {

        }


        @Override
        public void disableDrawing() {

        }
    };*/
}
