package com.mathfriendzy.controller.homework;

public class OffLineHomeworkResponse {
	
	private int halfCredit;
	private int fullCredit;
	private int zeroCredit;
	private int totalProblem;
	
	//for practice time and score
	private int practiceTime;
	private int practiceScore;
	
	//for word time and score
	private int wordTime;
	private int wordScore;
	
	//for custome
	private int customeScore;
	
	//for average score
	private int avgScore;
	
	public int getHalfCredit() {
		return halfCredit;
	}
	public void setHalfCredit(int halfCredit) {
		this.halfCredit = halfCredit;
	}
	public int getFullCredit() {
		return fullCredit;
	}
	public void setFullCredit(int fullCredit) {
		this.fullCredit = fullCredit;
	}
	public int getZeroCredit() {
		return zeroCredit;
	}
	public void setZeroCredit(int zeroCredit) {
		this.zeroCredit = zeroCredit;
	}
	public int getTotalProblem() {
		return totalProblem;
	}
	public void setTotalProblem(int totalProblem) {
		this.totalProblem = totalProblem;
	}
	public int getPracticeTime() {
		return practiceTime;
	}
	public void setPracticeTime(int practiceTime) {
		this.practiceTime = practiceTime;
	}
	public int getPracticeScore() {
		return practiceScore;
	}
	public void setPracticeScore(int practiceScore) {
		this.practiceScore = practiceScore;
	}
	public int getWordTime() {
		return wordTime;
	}
	public void setWordTime(int wordTime) {
		this.wordTime = wordTime;
	}
	public int getWordScore() {
		return wordScore;
	}
	public void setWordScore(int wordScore) {
		this.wordScore = wordScore;
	}
	public int getCustomeScore() {
		return customeScore;
	}
	public void setCustomeScore(int customeScore) {
		this.customeScore = customeScore;
	}
	public int getAvgScore() {
		return avgScore;
	}
	public void setAvgScore(int avgScore) {
		this.avgScore = avgScore;
	}
}
