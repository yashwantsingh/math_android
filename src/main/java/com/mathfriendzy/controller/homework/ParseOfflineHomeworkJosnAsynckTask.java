package com.mathfriendzy.controller.homework;

import com.mathfriendzy.serveroperation.FileParser;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.serveroperation.ServerDialogs;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class ParseOfflineHomeworkJosnAsynckTask extends AsyncTask<Void, Void, HttpResponseBase>{
	
	private String responseJson;
	private int requestCode;
	private Context context;
	private int diaologCode;
	private boolean isDisplayDialog;

	private HttpResponseInterface responseIntefrface;
	private String dialogMsg;

	private ProgressDialog pd;

	/**This is the common AsyncTask which will send a request to the server 
	 * and return the response in the form of HttpResponseBase class object
	 * @param responseJson - json string which need to be parse
	 * @param requestCode - This is the request which you are sending , You will get the same request code in
	 *  the serverResponse(HttpResponseBase httpResponseBase,int requestCode) method
	 * @param context - This is current activity object
	 * @param responseIntefrface - Send the response interface object , Which you have impleted
	 * @param diaologCode - Send the dialog code
	 * @param isDisplayDialog - If you want to display the progress of loading the send it true otherwise false 
	 * @param dialogMsg - Send the message which you want to see in the progress dailog at the time of loading
	 */
	public ParseOfflineHomeworkJosnAsynckTask(String responseJson, 
			int requestCode , Context context , 
			HttpResponseInterface responseIntefrface
			, int diaologCode , boolean isDisplayDialog
			, String dialogMsg){

		this.responseJson = responseJson;
		this.requestCode = requestCode;
		this.context = context;
		this.responseIntefrface = responseIntefrface;
		this.diaologCode = diaologCode;
		this.isDisplayDialog = isDisplayDialog;
		this.dialogMsg = dialogMsg;
	}

	@Override
	protected void onPreExecute() {

		if(isDisplayDialog){
			pd = ServerDialogs.getProgressDialog(context, dialogMsg , diaologCode);
			pd.show();
		}
		super.onPreExecute();
	}

	@Override
	protected HttpResponseBase doInBackground(Void... params) {
				
		if(responseJson != null){
			HttpResponseBase httpResponseBase = new FileParser()
				.ParseJsonString(responseJson, requestCode);
			return httpResponseBase;
		}
		return null;
	}

	@Override
	protected void onPostExecute(HttpResponseBase httpResponseBase) {

		if(isDisplayDialog){
			pd.cancel();
		}
		responseIntefrface.serverResponse(httpResponseBase, requestCode);
		super.onPostExecute(httpResponseBase);
	}
}
