package com.mathfriendzy.controller.homework;

import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.model.homework.GetHelpStudentListResponse;

public class GetHelpStudentAdapter extends BaseAdapter{

	public ArrayList<GetHelpStudentListResponse> studentList = null;
	private Context context;
	private ViewHolder vHolder;
	private LayoutInflater mInflator;

	public GetHelpStudentAdapter(Context context , ArrayList<GetHelpStudentListResponse> studentList){
		this.context = context;
		this.studentList = studentList;
		mInflator = LayoutInflater.from(context);				
	}

	@Override
	public int getCount() {
		return studentList.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		if(view == null){
			vHolder = new ViewHolder();
			view = mInflator.inflate(R.layout.get_help_student_list_layout, null); 
			vHolder.txtName = (TextView) view.findViewById(R.id.txtName);
			vHolder.imgStar1 = (ImageView) view.findViewById(R.id.imgStar1);
			vHolder.imgStar2 = (ImageView) view.findViewById(R.id.imgStar2);
			vHolder.imgStar3 = (ImageView) view.findViewById(R.id.imgStar3);
			vHolder.imgStar4 = (ImageView) view.findViewById(R.id.imgStar4);
			vHolder.imgStar5 = (ImageView) view.findViewById(R.id.imgStar5);

			view.setTag(vHolder);		
		}else{
			vHolder = (ViewHolder) view.getTag();
		}

		vHolder.txtName.setText(studentList.get(position).getfName() + " " + studentList.get(position).getlName());
		this.setRatingstar(studentList.get(position).getRating(),
				vHolder.imgStar1, vHolder.imgStar2, vHolder.imgStar3, 
				vHolder.imgStar4, vHolder.imgStar5);
		return view;
	}

	private class ViewHolder{
		private TextView txtName;
		private ImageView imgStar1;
		private ImageView imgStar2;
		private ImageView imgStar3;
		private ImageView imgStar4;
		private ImageView imgStar5;

	}

	/**
	 * Set rating star
	 * @param rating
	 * @param imgStar1
	 * @param imgStar2
	 * @param imgStar3
	 * @param imgStar4
	 * @param imgStar5
	 */
	private void setRatingstar(int rating , ImageView imgStar1 , ImageView imgStar2 
			, ImageView imgStar3 , ImageView imgStar4 ,ImageView imgStar5){
		switch(rating){
			case 0:
				imgStar1.setBackgroundResource(R.drawable.start_unselected);
				imgStar2.setBackgroundResource(R.drawable.start_unselected);
				imgStar3.setBackgroundResource(R.drawable.start_unselected);
				imgStar4.setBackgroundResource(R.drawable.start_unselected);
				imgStar5.setBackgroundResource(R.drawable.start_unselected);
				break;
			case 1:
				imgStar1.setBackgroundResource(R.drawable.star_selected);
				imgStar2.setBackgroundResource(R.drawable.start_unselected);
				imgStar3.setBackgroundResource(R.drawable.start_unselected);
				imgStar4.setBackgroundResource(R.drawable.start_unselected);
				imgStar5.setBackgroundResource(R.drawable.start_unselected);
				break;
			case 2:
				imgStar1.setBackgroundResource(R.drawable.star_selected);
				imgStar2.setBackgroundResource(R.drawable.star_selected);
				imgStar3.setBackgroundResource(R.drawable.start_unselected);
				imgStar4.setBackgroundResource(R.drawable.start_unselected);
				imgStar5.setBackgroundResource(R.drawable.start_unselected);
				break;
			case 3:
				imgStar1.setBackgroundResource(R.drawable.star_selected);
				imgStar2.setBackgroundResource(R.drawable.star_selected);
				imgStar3.setBackgroundResource(R.drawable.star_selected);
				imgStar4.setBackgroundResource(R.drawable.start_unselected);
				imgStar5.setBackgroundResource(R.drawable.start_unselected);
				break;
			case 4:
				imgStar1.setBackgroundResource(R.drawable.star_selected);
				imgStar2.setBackgroundResource(R.drawable.star_selected);
				imgStar3.setBackgroundResource(R.drawable.star_selected);
				imgStar4.setBackgroundResource(R.drawable.star_selected);
				imgStar5.setBackgroundResource(R.drawable.start_unselected);
				break;
			case 5:
				imgStar1.setBackgroundResource(R.drawable.star_selected);
				imgStar2.setBackgroundResource(R.drawable.star_selected);
				imgStar3.setBackgroundResource(R.drawable.star_selected);
				imgStar4.setBackgroundResource(R.drawable.star_selected);
				imgStar5.setBackgroundResource(R.drawable.star_selected);
				break;
		}
	}
}
