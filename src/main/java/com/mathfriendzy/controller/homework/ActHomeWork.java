package com.mathfriendzy.controller.homework;


import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.learningcenter.LearningCenterEquationSolveWithTimer;
import com.mathfriendzy.controller.learningcenter.schoolcurriculum.LearningCenterSchoolCurriculumEquationSolve;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.ClassSelectedListener;
import com.mathfriendzy.listener.HomeworkClickListener;
import com.mathfriendzy.model.homework.CustomePlayerAns;
import com.mathfriendzy.model.homework.CustomeResult;
import com.mathfriendzy.model.homework.GetAnswerDetailForStudentHWParam;
import com.mathfriendzy.model.homework.GetHomeWorkForStudentWithCustomeResponse;
import com.mathfriendzy.model.homework.GetHomeworksForStudentWithCustomParam;
import com.mathfriendzy.model.homework.GetStudentDetailAnswerResponse;
import com.mathfriendzy.model.homework.PracticeResultSubCat;
import com.mathfriendzy.model.homework.SaveHomeWorkServerResponse;
import com.mathfriendzy.model.homework.SelectAssignHomeworkClassParam;
import com.mathfriendzy.model.homework.WordSubCatResult;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.classes.ClassWithName;
import com.mathfriendzy.model.tutor.RegisterUserOnQBCallback;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerDialogs;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TreeSet;

import static com.mathfriendzy.helper.MathFriendzyHelper.isUserAlreadyRegisterOnQuickBlox;
import static com.mathfriendzy.helper.MathFriendzyHelper.registerUserToQuickBlox;


public class ActHomeWork extends ActBase {

    private final String TAG = this.getClass().getSimpleName();

    private TextView txtTeacherName 	= null;
    private TextView txtGrade 			= null;
    private TextView txtToImprove 		= null;
    private TextView txtDueDate 		= null;
    private TextView txtPracticeSkill 	= null;
    private TextView txtSchoolCurriculum= null;
    private TextView txtCustome 		= null;
    private TextView txtAvgScore 		= null;

    private Button btnShowMore 			= null;
    private ListView schoolHomeWorkList = null;
    private UserPlayerDto selectedPlayerData = null;

    private int homeWorkRecordOffSet = 0;
    private final int MAX_RECORD_FOR_HW_ONE_TIME  = 10;

    private HomeWorkListAdapter adapter = null;

    //for start activity for result
    private final int PLAY_HOME_WORK_QUIZZ_REQUEST = 1001;
    //private String currentSelectedHomeWordId = "";
    public static int selectedPosition = 0;
    private ProgressDialog pd = null;
    private String alertNoHomeworkMsg = null;
    private ProgressDialog progDialog = null;
    //private Spinner spinnerGrade = null;
    private ArrayList<String> gradeList = null;
    private boolean isRecordLoadedFirstTime = false;


    private TextView txtSelectAPrevHW = null;
    private TextView txtAssignDate = null;
    private RelativeLayout rlFromDateLayout = null;
    private TextView edtDatePickerFrom = null;
    private RelativeLayout rlToDateLayout = null;
    private TextView edtDatePickerTo = null;
    private RelativeLayout selectClassLayout = null;
    private TextView txtSelectClassValue = null;
    private TextView txtSubject = null;
    private TextView txtSubjectValues = null;
    private TextView txtClass = null;
    private TextView txtClassValues = null;
    private TextView txtTo = null;
    private String startDate = "";
    private String endDate = "";
    private final String SERVICE_TIME_MY_FORMATE = "yyyy-MM-dd";
    private final String SERVICE_TIME_CONVERTED_FORMATE = "MMM. dd, yyyy";
    private String lblStartDateCantLater = null;
    private int selectedGrade = 1;
    private final int NUMBER_OF_FORWARD_DAYS = 15;
    private final int NUMBER_OF_FORWARD_MONTH = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_home_work);

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside onCreate()");

        selectedPlayerData = this.getPlayerData();

        this.init();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.saveOfflineHWData();
        this.setInitialStartEndDate();
        //this.checkForWordCategoriesAndGetHomeworkData();
        this.getHomeworkForStudent();
        //this.setGrade(selectedPlayerData.getGrade());
        //this.registerUserOnQuickBlox();

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside onCreate()");
    }

    private void init(){
        gradeList = MathFriendzyHelper.getUpdatedGradeList(this);

        //new homework changes
        startDate  = MathFriendzyHelper.getCurrentDateInGiveGformateDate(SERVICE_TIME_CONVERTED_FORMATE);
        endDate = MathFriendzyHelper.getBackAndForthDateInGivenFormatDate
                (SERVICE_TIME_CONVERTED_FORMATE, NUMBER_OF_FORWARD_MONTH, 0, 0);
    }

    private void saveOfflineHWData(){
        if(CommonUtils.isInternetConnectionAvailable(this)){
            pd = ServerDialogs.getProgressDialog(this,
                    getString(R.string.please_wait_dialog_msg), 1);
            pd.show();
            MathFriendzyHelper.saveOfflineHWDataOnServer(this, new HttpServerRequest() {
                @Override
                public void onRequestComplete() {
                    if(pd != null && pd.isShowing()){
                        pd.cancel();
                    }
                }
            });
        }
    }

    private void getHomeworkForStudent(){
        if(CommonUtils.isInternetConnectionAvailable(this)){
            getHomeworksForStudentWithCustom(selectedGrade + "" , "0" , true);
        }else{
            this.getHomeworkDataWithCustomeFromLocalDB();
        }
    }

    /*private void setGrade(String grade){
        MathFriendzyHelper.setAdapterToSpinner(this,
                MathFriendzyHelper.getGradeForSelection(grade)
                , gradeList, spinnerGrade);
    }*/


    private void showProgressDialog(){
        progDialog = new ProgressDialog(this);
        progDialog.setIndeterminate(false);//back button does not work
        progDialog.setCancelable(false);
        progDialog.setMessage("Please Wait...");
        progDialog.show();
    }

    private void hideProgressDialog(){
        if(progDialog != null && progDialog.isShowing())
            progDialog.cancel();
    }

    /**
     * Register user on quick blox
     */
    private void registerUserOnQuickBlox(){
        if (!isUserAlreadyRegisterOnQuickBlox(selectedPlayerData)) {
            showProgressDialog();
            registerUserToQuickBlox(selectedPlayerData, this,
                    new RegisterUserOnQBCallback() {
                        @Override
                        public void onSuccess(QBUser qbUser, Bundle bundle) {
                            hideProgressDialog();
                        }
                    });
        }
    }

    /**
     * This method get the Home work data
     */
    private void getHomeworksForStudentWithCustom(String selectedGrade , String classIds
            , boolean isInitialCall){
        try{
            GetHomeworksForStudentWithCustomParam param =
                    new GetHomeworksForStudentWithCustomParam();
            /*if(isInitialCall) {
                param.setAction("getClassesWithHwInDateRange");
            }else{*/
            param.setAction("getAssignedHomeworksForStudent");
            //}
            param.setClassId(classIds);
            param.setOffset(homeWorkRecordOffSet);
            param.setPlayerId(selectedPlayerData.getPlayerid());
            param.setUserId(selectedPlayerData.getParentUserId());
            param.setStartDate(startDate);
            param.setEndDate(endDate);
            param.setLimit(MAX_RECORD_FOR_HW_ONE_TIME);
            param.setGrade(selectedGrade);
            if(MathFriendzyHelper.getUserAccountType(this)
                    == MathFriendzyHelper.TEACHER)
                param.setForTeacher(1);
            else
                param.setForTeacher(0);

            if(CommonUtils.isInternetConnectionAvailable(this)){
                new MyAsyckTask(ServerOperation
                        .createPostRequestForGetHomeworksForStudentWithCustom(param)
                        , null, ServerOperationUtil.GETHOMEWORKSFORSTUDENTSWITHCUSTOME, this,
                        this, ServerOperationUtil.SIMPLE_DIALOG , true ,
                        getString(R.string.please_wait_dialog_msg))
                        .execute();
            }else{
                CommonUtils.showInternetDialog(this);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Get the homework data from local , When Internet is not connected
     */
    private void getHomeworkDataWithCustomeFromLocalDB(){
        String jsonString = MathFriendzyHelper.getHomeworkJsonByUserIdAndPlayerId
                (this, selectedPlayerData.getParentUserId(),
                        selectedPlayerData.getPlayerid());

        this.setVisibilityOfShowMoreButton(false);

        if(jsonString != null && jsonString.length() > 0){
            HttpResponseInterface request = new HttpResponseInterface() {
                @Override
                public void serverResponse(HttpResponseBase httpResponseBase,
                                           int requestCode) {
                    if(httpResponseBase != null){
                        if(requestCode == ServerOperationUtil.GETHOMEWORKSFORSTUDENTSWITHCUSTOME){
                            GetHomeWorkForStudentWithCustomeResponse getHomeWorkResponse =
                                    (GetHomeWorkForStudentWithCustomeResponse) httpResponseBase;
                            setListAdapter(getHomeWorkResponse.getFinalDataList());
                        }
                    }
                }
            };

            new ParseOfflineHomeworkJosnAsynckTask(jsonString,
                    ServerOperationUtil.GETHOMEWORKSFORSTUDENTSWITHCUSTOME, this,
                    request, ServerOperationUtil.SIMPLE_DIALOG , true ,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        }
    }

    private void setVisibilityOfShowMoreButton(boolean bValue){
        if(bValue){
            btnShowMore.setVisibility(Button.VISIBLE);
        }else{
            btnShowMore.setVisibility(Button.INVISIBLE);
        }
    }

    @Override
    protected void setWidgetsReferences() {

        txtTeacherName 	= (TextView) findViewById(R.id.txtTeacherName);
        txtGrade 		= (TextView) findViewById(R.id.txtGrade);
        txtToImprove 	= (TextView) findViewById(R.id.txtToImprove);
        txtDueDate 		= (TextView) findViewById(R.id.txtDueDate);
        txtPracticeSkill= (TextView) findViewById(R.id.txtPracticeSkill);
        txtSchoolCurriculum = (TextView) findViewById(R.id.txtSchoolCurriculum);
        txtCustome 		= (TextView) findViewById(R.id.txtCustome);
        txtAvgScore 	= (TextView) findViewById(R.id.txtAvgScore);

        btnShowMore     = (Button) findViewById(R.id.btnShowMore);
        schoolHomeWorkList = (ListView) findViewById(R.id.schoolHomeWorkList);

        //from base class
        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        //spinnerGrade = (Spinner) findViewById(R.id.spinnerGrade);


        //new homework changes
        txtSelectAPrevHW = (TextView) findViewById(R.id.txtSelectAPrevHW);
        txtAssignDate = (TextView) findViewById(R.id.txtAssignDate);
        rlFromDateLayout = (RelativeLayout) findViewById(R.id.rlFromDateLayout);
        txtTo = (TextView) findViewById(R.id.txtTo);
        edtDatePickerFrom = (TextView) findViewById(R.id.edtDatePickerFrom);
        rlToDateLayout = (RelativeLayout) findViewById(R.id.rlToDateLayout);
        edtDatePickerTo = (TextView) findViewById(R.id.edtDatePickerTo);
        selectClassLayout = (RelativeLayout) findViewById(R.id.selectClassLayout);
        txtSelectClassValue = (TextView) findViewById(R.id.txtSelectClassValue);
        txtSubject = (TextView) findViewById(R.id.txtSubject);
        txtSubjectValues = (TextView) findViewById(R.id.txtSubjectValues);
        txtClass = (TextView) findViewById(R.id.txtClass);
        txtClassValues = (TextView) findViewById(R.id.txtClassValues);
    }

    @Override
    protected void setTextFromTranslation() {
        try{
            Translation transeletion = new Translation(this);
            transeletion.openConnection();
            txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier
                    ("lblSchoolInfoSchoolText") + " " +
                    transeletion.getTranselationTextByTextIdentifier("lblHomework"));
            txtToImprove.setText(transeletion.getTranselationTextByTextIdentifier
                    ("lblToImproveYourScore"));
            txtDueDate.setText(transeletion.getTranselationTextByTextIdentifier("lblDueDate")
                    +"\n"+"MM-DD-YY");
            txtPracticeSkill.setText(transeletion.getTranselationTextByTextIdentifier
                    ("lblSolveEquations"));

            txtSchoolCurriculum.setText(transeletion.getTranselationTextByTextIdentifier
                    ("lblWordProblem"));

            txtCustome.setText(transeletion.getTranselationTextByTextIdentifier("lblClass")
                    + " " + transeletion.getTranselationTextByTextIdentifier("lblWork")
                    + "/" + transeletion.getTranselationTextByTextIdentifier("lblQuizzes"));

            txtAvgScore.setText(transeletion.getTranselationTextByTextIdentifier("lblAverage")
                    + "\n" + transeletion.getTranselationTextByTextIdentifier("mfBtnTitleScore"));
            btnShowMore.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleShowMore"));
            txtGrade.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade") + ":");

            if(selectedPlayerData != null){
                txtTeacherName.setText(transeletion.getTranselationTextByTextIdentifier
                        ("mfLblTeacherName")
                        + ": " + selectedPlayerData.getTeacherFirstName() + " "
                        + selectedPlayerData.getTeacheLastName());
                /*txtGrade.setText(transeletion.getTranselationTextByTextIdentifier
                        ("lblAddPlayerGrade") + ": " + selectedPlayerData.getGrade());*/
            }

            //new homework changes
            txtSelectAPrevHW.setText(transeletion.getTranselationTextByTextIdentifier("lblEasyWayToDoHw"));
            txtAssignDate.setText(transeletion.getTranselationTextByTextIdentifier("lblDueDate"));
            txtTo.setText(transeletion.getTranselationTextByTextIdentifier("lblTo"));
            txtSubject.setText(transeletion.getTranselationTextByTextIdentifier("lblSubjectHeader") + ":");
            txtClass.setText(transeletion.getTranselationTextByTextIdentifier("lblClass") + ":");
            edtDatePickerFrom.setHint(transeletion.getTranselationTextByTextIdentifier("lblSelect"));
            edtDatePickerTo.setHint(transeletion.getTranselationTextByTextIdentifier("lblSelect"));
            txtSelectClassValue.setHint(transeletion.getTranselationTextByTextIdentifier("lblSelect"));
            lblStartDateCantLater = transeletion.getTranselationTextByTextIdentifier("lblStartDateCantLater");

            alertNoHomeworkMsg = transeletion.getTranselationTextByTextIdentifier("alertNoHomeworkMsg");
            transeletion.closeConnection();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void setListenerOnWidgets() {
        rlFromDateLayout.setOnClickListener(this);
        rlToDateLayout.setOnClickListener(this);
        selectClassLayout.setOnClickListener(this);

        btnShowMore.setOnClickListener(this);

        /*schoolHomeWorkList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @SuppressWarnings("static-access")
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    final int position, long id) {
                MathFriendzyHelper.downLoadQuestionForWordProblemForHW(ActHomeWork.this,
                        Integer.parseInt(adapter.homeWorkresult.get(position).getGrade()),
                        new HttpServerRequest() {
                            @Override
                            public void onRequestComplete() {
                                goForActHomeQuizzes(adapter.homeWorkresult.get(position) , position);
                            }
                        });
            }
        });*/


        /*spinnerGrade.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int selectedGrade = MathFriendzyHelper.getGrade
                        (spinnerGrade.getSelectedItem().toString());
                if(CommonUtils.isInternetConnectionAvailable(ActHomeWork.this)) {
                    homeWorkRecordOffSet = 0;
                    getHomeworksForStudentWithCustom(selectedGrade + "");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/
    }

    /**
     * Go to the ActHomeQuizz screen after every think is correct
     * @param homeWorkresult
     */
    private void goForActHomeQuizzes
    (GetHomeWorkForStudentWithCustomeResponse homeWorkresult , int position){
        try{
            selectedPosition = position;
            //currentSelectedHomeWordId = homeWorkresult.getHomeWorkId();
            Intent intent = new Intent(ActHomeWork.this , ActHomeWorkQuizzes.class);
            intent.putExtra("isExpireQuizz",
                    MathFriendzyHelper.isDateExpire(homeWorkresult.getDate()));
            intent.putExtra("homeWorkQuizzData", homeWorkresult);
            intent.putExtra("teacherName", selectedPlayerData.getTeacherFirstName()
                    + " " + selectedPlayerData.getTeacheLastName());
            intent.putExtra("grade", selectedPlayerData.getGrade());
            startActivityForResult(intent, PLAY_HOME_WORK_QUIZZ_REQUEST);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Set the show more button visibility and offset value
     * @param homeWorkresult
     */
    private void setOffsetAndShowMoreVisibility(ArrayList<GetHomeWorkForStudentWithCustomeResponse>
                                                        homeWorkresult){
        try {
            if(!CommonUtils.isInternetConnectionAvailable(this)){
                this.setVisibilityOfShowMoreButton(false);
                return ;
            }

            if (homeWorkresult.size() >= MAX_RECORD_FOR_HW_ONE_TIME) {
                homeWorkRecordOffSet = homeWorkRecordOffSet + MAX_RECORD_FOR_HW_ONE_TIME;
                this.setVisibilityOfShowMoreButton(true);
            } else {
                this.setVisibilityOfShowMoreButton(false);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * This method set the adapter for home work
     * @param homeWorkresult
     */
    @SuppressWarnings("static-access")
    private void setListAdapter(ArrayList<GetHomeWorkForStudentWithCustomeResponse>
                                        homeWorkresult){
        if(homeWorkresult.size() == 0 && !isRecordLoadedFirstTime){
            MathFriendzyHelper.showWarningDialog(this, alertNoHomeworkMsg,
                    new HttpServerRequest() {
                        @Override
                        public void onRequestComplete() {
                            //finish();
                        }
                    });
            //return;
        }

        isRecordLoadedFirstTime = true;
        if(homeWorkRecordOffSet == 0){
            adapter = new HomeWorkListAdapter(this, homeWorkresult);
            HomeworkClickListener homeworkClickListener = new HomeworkClickListener() {
                @Override
                public void onCustomClick(CustomeResult customeResult,
                                          GetHomeWorkForStudentWithCustomeResponse homeworkData , int index) {
                    try {
                        goForCustomClick(customeResult, homeworkData, index);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void onPracticeClick(PracticeResultSubCat practiceResultSubCat,
                                            GetHomeWorkForStudentWithCustomeResponse homeworkData , int index) {
                    try {
                        goForPracticeClick(practiceResultSubCat, homeworkData, index);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void onWordClick(final WordSubCatResult subCatResult,
                                        final GetHomeWorkForStudentWithCustomeResponse homeworkData ,
                                        final int index) {
                    try {
                        final boolean isExpireQuizz = MathFriendzyHelper.isDateExpire(homeworkData.getDate());
                        if(isExpireQuizz){
                            showExpireQuizzDialog();
                            return ;
                        }
                        MathFriendzyHelper.downLoadQuestionForWordProblemForHW(ActHomeWork.this,
                                Integer.parseInt(homeworkData.getGrade()),
                                new HttpServerRequest() {
                                    @Override
                                    public void onRequestComplete() {
                                        goForWordProblem(subCatResult, homeworkData, index);
                                    }
                                });
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void onHomeworkClick(final int position) {
                    MathFriendzyHelper.downLoadQuestionForWordProblemForHW(ActHomeWork.this,
                            Integer.parseInt(adapter.homeWorkresult.get(position).getGrade()),
                            new HttpServerRequest() {
                                @Override
                                public void onRequestComplete() {
                                    goForActHomeQuizzes(adapter.homeWorkresult.get(position) , position);
                                }
                            });
                }
            };
            adapter.initializeListener(homeworkClickListener);
            schoolHomeWorkList.setAdapter(adapter);
        }else{
            adapter.homeWorkresult.addAll(homeWorkresult);
            adapter.notifyDataSetChanged();
        }
        this.saveNumberOfActiveHomework();
        try{
            if(CommonUtils.isInternetConnectionAvailable(this)) {
                MathFriendzyHelper.createJsonForHomeworkAndSaveIntoLocal
                        (this, HomeWorkListAdapter.homeWorkresult,
                                selectedPlayerData.getParentUserId()
                                , selectedPlayerData.getPlayerid(),
                                new HttpServerRequest() {
                                    @Override
                                    public void onRequestComplete() {

                                    }
                                });
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        this.setOffsetAndShowMoreVisibility(homeWorkresult);
    }


    /**
     * Save Number of active homework
     */
    private void saveNumberOfActiveHomework(){
        try {
            MathFriendzyHelper.saveNumberOfActiveHomeWork
                    (this, selectedPlayerData.getParentUserId(),
                            selectedPlayerData.getPlayerid(),
                            adapter.getNumberOfActiveHomeWork());
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnShowMore:
               /* int selectedGrade = MathFriendzyHelper.getGrade
                        (spinnerGrade.getSelectedItem().toString());*/
                this.getHomeworksForStudentWithCustom(selectedGrade + "" , "0" , false);
                break;
            case R.id.rlFromDateLayout:
                this.selectFromDate();
                break;
            case R.id.rlToDateLayout:
                this.selectToDate();
                break;
            case R.id.selectClassLayout:
                this.clickToSelectClass();
                break;
        }
    }

    /**
     * Save the json response from server into Local for offline Homework
     */
	/*private void saveJsonIntoLocalDB(String jsonString){
		MathFriendzyHelper.saveHomeworkJsonIntoLocalDB
		(this, selectedPlayerData.getParentUserId() ,
				selectedPlayerData.getPlayerid(), jsonString);
	}*/

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase,
                               int requestCode) {
        if(requestCode == ServerOperationUtil.GETHOMEWORKSFORSTUDENTSWITHCUSTOME){
            GetHomeWorkForStudentWithCustomeResponse getHomeWorkResponse =
                    (GetHomeWorkForStudentWithCustomeResponse) httpResponseBase;
            if(getHomeWorkResponse != null) {
                if(getHomeWorkResponse.isClassArrayExist()) {
                    ArrayList<ClassWithName> classWithNames = getHomeWorkResponse.getClassWithNames();
                    if (classWithNames != null && classWithNames.size() > 0) {
                        this.initClasses(classWithNames, getHomeWorkResponse.getLastClassAssignedForHw());
                    }else{
                        if(classWithNames == null)
                            classWithNames = new ArrayList<ClassWithName>();
                        this.initClasses(classWithNames, getHomeWorkResponse.getLastClassAssignedForHw());
                    }
                }
            }

            this.checkForWordCategoriesAndGetHomeworkData(getHomeWorkResponse.getFinalDataList());
            //this.setListAdapter(getHomeWorkResponse.getFinalDataList());
            /*if(getHomeWorkResponse.getFinalDataList().size() >= MAX_RECORD_FOR_HW_ONE_TIME){
                homeWorkRecordOffSet = homeWorkRecordOffSet + MAX_RECORD_FOR_HW_ONE_TIME;
                this.setVisibilityOfShowMoreButton(true);
            }else{
                this.setVisibilityOfShowMoreButton(false);
            }*/
        }
    }

    /**
     * This method first check for the work categories, if not found in db then download that
     * and then get the homework data
     */
    private void checkForWordCategoriesAndGetHomeworkData(
            final ArrayList<GetHomeWorkForStudentWithCustomeResponse> finalDataList){

        if(CommonUtils.isInternetConnectionAvailable(this)){
            ArrayList<Integer> graList = this.getGradeList(finalDataList);
            if(!(graList != null && graList.size() > 0)){
                setListAdapter(finalDataList);
                return ;
            }
            MathFriendzyHelper.downloadAllGradeWordCategories(this , new HttpResponseInterface() {
                @Override
                public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                    setListAdapter(finalDataList);
                }
            } , true , graList);
        }else{
            setListAdapter(finalDataList);
        }
    }

    private ArrayList<Integer> getGradeList(ArrayList<GetHomeWorkForStudentWithCustomeResponse> finalDataList){
        TreeSet<Integer> gradeList = new TreeSet<Integer>();
        for(GetHomeWorkForStudentWithCustomeResponse data : finalDataList) {
            if(data.getWordResultList() != null && data.getWordResultList().size() > 0) {
                gradeList.add(MathFriendzyHelper.parseInt(data.getGrade()));
            }
        }
        return new ArrayList<Integer>(gradeList);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            switch(requestCode){
                case PLAY_HOME_WORK_QUIZZ_REQUEST:
                    boolean onlyRefreshList = data.getBooleanExtra("onlyRefreshList" , false);
                    if(onlyRefreshList){
                        adapter.notifyDataSetChanged();
                        this.saveNumberOfActiveHomework();
                        return ;
                    }
                    this.updateHomeWordResultAfterPlayed
                            ((SaveHomeWorkServerResponse)data.getSerializableExtra("resultAfterPlayed"));
                    break;

                case PLAY_CUSTOM_PROBLEM_REQUEST:
                    boolean isOnlyForOponentInput = data.getBooleanExtra("isOnlyForOponentInput" , false);
                    if(isOnlyForOponentInput) {
                        updateNumberOfActiveInputInMainResult(data.getIntExtra("numberOfActiveOponentInput" , 0));
                        return ;
                    }
                    updatedData = (SaveHomeWorkServerResponse)data.getSerializableExtra("resultAfterPlayed");
                    if(updatedData != null){
                        isDataUpdatedByPlaying = true;
                    }else{
                        isDataUpdatedByPlaying = false;
                    }
                    ArrayList<CustomePlayerAns> customePlayListByStudent =
                            (ArrayList<CustomePlayerAns>) data.getSerializableExtra("customePlayData");
                    this.updateCustomePlayedScore(updatedData , customePlayListByStudent);
                    if(adapter != null){
                        adapter.notifyDataSetChanged();
                    }
                    break;
                case PLAY_PRACTICE_SKILL_REQUEST:
                    isDataUpdatedByPlaying = true;
                    updatedData = (SaveHomeWorkServerResponse)data.getSerializableExtra("resultAfterPlayed");
                    this.updatePracticePlayedScore(updatedData);
                    if(adapter != null){
                        adapter.notifyDataSetChanged();
                    }
                    break;
                case PLAY_WORD_PROBLEM_REQUEST:
                    isDataUpdatedByPlaying = true;
                    updatedData = (SaveHomeWorkServerResponse)data.getSerializableExtra("resultAfterPlayed");
                    this.updateWordPlayedScore(updatedData);
                    if(adapter != null){
                        adapter.notifyDataSetChanged();
                    }
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * This method update the current
     * data into the list when data is updated by playing
     * @param response
     */
    @SuppressWarnings("static-access")
    private void updateHomeWordResultAfterPlayed(SaveHomeWorkServerResponse response){
        adapter.homeWorkresult.get(selectedPosition).setFinished(response.getFinished());
        adapter.homeWorkresult.get(selectedPosition).setPracticeTime(response.getPracticeTime());
        adapter.homeWorkresult.get(selectedPosition).setPracticeScore(response.getPracticeScore());
        adapter.homeWorkresult.get(selectedPosition).setWordTime(response.getWordTime());
        adapter.homeWorkresult.get(selectedPosition).setWordScore(response.getWordScore());
        adapter.homeWorkresult.get(selectedPosition).setCustomeScore(response.getCustomeScore());
        adapter.homeWorkresult.get(selectedPosition).setAvgScore(response.getAvgScore());
        adapter.notifyDataSetChanged();
        this.saveNumberOfActiveHomework();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    protected void onResume() {
        MathFriendzyHelper.initializeProcessDialog(this);
        super.onResume();
    }


    //new homework changes
    /**
     * Set the start and end date initially
     */
    private void setInitialStartEndDate() {
        try {
            edtDatePickerFrom.setText(startDate);
            edtDatePickerTo.setText(endDate);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Show from date from date picker
     */
    private void selectFromDate(){
        final Calendar calender = MathFriendzyHelper.getCurrentCalender();
        MathFriendzyHelper.showDatePickerDialogPrevDateSelected(this, calender.get(Calendar.YEAR),
                calender.get(Calendar.MONTH), calender.get(Calendar.DAY_OF_MONTH),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);

                        //if (newDate.after(calender)) {
                        setDateText(edtDatePickerFrom, year, monthOfYear, dayOfMonth, true);
                        //}
                    }
                }, calender);
    }

    /**
     * Show to date from date picker
     */
    private void selectToDate(){
        final Calendar calender = MathFriendzyHelper.getCurrentCalender();
        MathFriendzyHelper.showDatePickerDialogPrevDateSelected(this, calender.get(Calendar.YEAR),
                calender.get(Calendar.MONTH), calender.get(Calendar.DAY_OF_MONTH),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);

                        //if (newDate.after(calender)) {
                        setDateText(edtDatePickerTo, year, monthOfYear, dayOfMonth, false);
                        //}
                    }
                }, calender);
    }


    /**
     * Set date text
     * @param txtView
     * @param year
     * @param month
     * @param dayOfMonth
     * @param isStartDate
     */
    private void setDateText(TextView txtView , int year , int month , int dayOfMonth ,
                             boolean isStartDate){

        String tempStartDate = startDate;
        String tempEndDate   = endDate;

        month ++;
        String formateString = MathFriendzyHelper.getFourDigitFormatNumber(year) + "-" +
                MathFriendzyHelper.getDoubleDigitFormatNumber(month) + "-" +
                MathFriendzyHelper.getDoubleDigitFormatNumber(dayOfMonth);
        String formatedDate = MathFriendzyHelper.formatDataInGivenFormat(formateString ,
                SERVICE_TIME_MY_FORMATE, SERVICE_TIME_CONVERTED_FORMATE);
        if(isStartDate){
            tempStartDate = formatedDate;
        }else{
            tempEndDate = formatedDate;
        }
        Date fromDate = MathFriendzyHelper.getValidateDate(tempStartDate , SERVICE_TIME_CONVERTED_FORMATE);
        Date toDate   = MathFriendzyHelper.getValidateDate(tempEndDate , SERVICE_TIME_CONVERTED_FORMATE);
        try {
            if(fromDate != null && toDate != null) {
                if (fromDate.compareTo(toDate) <= 0) {
                    startDate = tempStartDate;
                    endDate = tempEndDate;
                    //txtView.setText(formatedDate);
                    this.setDateTextToTextView(txtView, formatedDate);
                } else {
                    MathFriendzyHelper.showWarningDialog(this, lblStartDateCantLater);
                }
            }else{
                startDate = tempStartDate;
                endDate = tempEndDate;
                //txtView.setText(formatedDate);
                this.setDateTextToTextView(txtView , formatedDate);
            }
        }catch (Exception e){
            e.printStackTrace();
            startDate = tempStartDate;
            endDate = tempEndDate;
            txtView.setText(formatedDate);
        }
    }

    /**
     * Set the date text
     * @param txtView
     * @param dateText
     */
    private void setDateTextToTextView(TextView txtView , String dateText){
        if(CommonUtils.isInternetConnectionAvailable(this)) {
            this.resetValues();
            String selectedClass = "0";
            if(selectedClassList != null && selectedClassList.size() > 0)
                selectedClass = getCommaSeparatedClassIds(selectedClassList);
            this.getHomeworksForStudentWithCustom(selectedGrade + "",
                    selectedClass , true);
        }else{
            CommonUtils.showInternetDialog(this);
        }
        txtView.setText(dateText);
    }

    private ArrayList<ClassWithName> classWithNames = null;
    private ArrayList<ClassWithName> selectedClassList = null;
    private RegistereUserDto registerUserFromPreff = null;

    private void initClasses(ArrayList<ClassWithName> classWithNames ,
                             int lastClassAssignedForHw){
        this.classWithNames = classWithNames;
        if(classWithNames != null && classWithNames.size() > 0){
            for(int i = 0 ; i < classWithNames.size() ; i ++ ){
                if(classWithNames.get(i).getClassId() == lastClassAssignedForHw){
                    //classWithNames.get(i).setSelected(true);
                    //initialize the selected class
                    selectedClassList = new ArrayList<ClassWithName>();
                    selectedClassList.add(classWithNames.get(i));
                    this.setTextToSelectedClassValues(classWithNames.get(i).getClassName());
                    this.setTextToSelectedSubjectValues(classWithNames.get(i).getSubject());
                    return ;
                }
            }
        }else{
            selectedClassList = null;
            this.setTextToSelectedClassValues("");
            this.setTextToSelectedSubjectValues("");
        }
    }

    /*private ArrayList<ClassWithName> getUserClasses(){
        try {
            if (registerUserFromPreff != null) {
                ArrayList<UserClasses> userClasseses = registerUserFromPreff.getUserClasses();
                ArrayList<ClassWithName> allGradeClasses = new ArrayList<ClassWithName>();
                for (int i = 0; i < userClasseses.size(); i++) {
                    UserClasses userClass = userClasseses.get(i);
                    allGradeClasses.addAll(userClass.getClassList());
                }
                return allGradeClasses;
            } else {
                return null;
            }
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }*/

    private void clickToSelectClass() {
        SelectAssignHomeworkClassParam param = new SelectAssignHomeworkClassParam();
        param.setShowForPrevSaveHomework(true);
        MathFriendzyHelper.showClassSelectionDialog(this , classWithNames , new ClassSelectedListener() {
            @Override
            public void onClassSelectionDone(ArrayList<ClassWithName> selectedClass) {
                selectedClassList = selectedClass;
                if(selectedClass != null && selectedClass.size() > 0){
                    setTextToSelectedClassValues(getCommaSeparatedClassName(selectedClass));
                    setTextToSelectedSubjectValues(getCommaSeparatedSubjectName(selectedClass));
                }else{
                    setTextToSelectedClassValues("");
                    setTextToSelectedSubjectValues("");
                }

                resetValues();
                getHomeworksForStudentWithCustom(selectedGrade + "" ,
                        getCommaSeparatedClassIds(selectedClassList) , false);
            }

            @Override
            public void onUpdatedClasses(ArrayList<ClassWithName> updatedList) {
                classWithNames = updatedList;
            }
        } , param);
    }

    private void setTextToSelectedClassValues(String commaSeparatedClasses){
        txtSelectClassValue.setText(commaSeparatedClasses);
        txtClassValues.setText(commaSeparatedClasses);
    }

    private void setTextToSelectedSubjectValues(String commaSeparatedSubjects){
        txtSubjectValues.setText(commaSeparatedSubjects);
    }

    private String getCommaSeparatedClassName(ArrayList<ClassWithName> selectedClassList){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0 ; i < selectedClassList.size() ; i ++ ){
            stringBuilder.append(stringBuilder.length() > 0 ? "," + selectedClassList.get(i).getClassName()
                    : selectedClassList.get(i).getClassName());
        }
        return stringBuilder.toString();
    }

    private String getCommaSeparatedSubjectName(ArrayList<ClassWithName> selectedClassList){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0 ; i < selectedClassList.size() ; i ++ ){
            stringBuilder.append(stringBuilder.length() > 0 ? "," + selectedClassList.get(i).getSubject()
                    : selectedClassList.get(i).getSubject());
        }
        return stringBuilder.toString();
    }

    private String getCommaSeparatedClassIds(ArrayList<ClassWithName> selectedClassList) {
        try {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < selectedClassList.size(); i++) {
                stringBuilder.append(stringBuilder.length() > 0 ? "," + selectedClassList.get(i).getClassId()
                        : selectedClassList.get(i).getClassId());
            }
            return stringBuilder.toString();
        }catch (Exception e){
            e.printStackTrace();
            return "0";
        }
    }

    private void resetValues(){
        try {
            homeWorkRecordOffSet = 0;
            adapter = null;
            isRecordLoadedFirstTime = false;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //for start activity for result
    private SaveHomeWorkServerResponse updatedData = null;
    private boolean isDataUpdatedByPlaying	    = false;
    private CustomeResult selectedCustomeResult = null;
    private PracticeResultSubCat selectedPracticeResultSubCat = null;
    private WordSubCatResult selectedSubCatResult = null;

    private final int PLAY_PRACTICE_SKILL_REQUEST = 1002;
    private final int PLAY_WORD_PROBLEM_REQUEST   = 1003;
    private final int PLAY_CUSTOM_PROBLEM_REQUEST = 1004;

    private void goForCustomClick(final CustomeResult customeResult,
                                  GetHomeWorkForStudentWithCustomeResponse homeworkData , int index){
        this.selectedCustomeResult = customeResult;
        final String teacherName = selectedPlayerData.getTeacherFirstName()
                + " " + selectedPlayerData.getTeacheLastName();
        final String grade = selectedPlayerData.getGrade();
        final GetHomeWorkForStudentWithCustomeResponse homeWorkQuizzData = homeworkData;
        final String dueDate = homeWorkQuizzData.getDate();
        final boolean isExpireQuizz = MathFriendzyHelper.isDateExpire(homeWorkQuizzData.getDate());

        getAnswerDetailForStudent(new HttpResponseInterface() {
            @Override
            public void serverResponse(HttpResponseBase httpResponseBase,
                                       int requestCode) {
                if(httpResponseBase != null && requestCode > 0){
                    GetStudentDetailAnswerResponse response
                            = (GetStudentDetailAnswerResponse) httpResponseBase;
                    customeResult.setCustomePlayerAnsList
                            (response.getCustomePlayerAnsList());
                    customeResult.setScore(response.getScore() + "");
                    if(response.getCustomePlayerAnsList().size() > 0){
                        customeResult.setAlreadyPlayed(true);
                    }
                }

                Intent intent = new Intent(ActHomeWork.this ,
                        ActCheckHomeWork.class);
                intent.putExtra("teacherName", teacherName);
                intent.putExtra("grade", grade);
                intent.putExtra("dueDate", dueDate);
                intent.putExtra("isExpireQuizz", isExpireQuizz);
                intent.putExtra("customeDetail", customeResult);
                intent.putExtra("homeWorkQuizzData", homeWorkQuizzData);
                startActivityForResult(intent , PLAY_CUSTOM_PROBLEM_REQUEST);
            }
        } , homeWorkQuizzData.getHomeWorkId() , customeResult.getCustomHwId());
    }

    private void getAnswerDetailForStudent(final HttpResponseInterface responseInterface
            , String hwId , int cusHWId){
        try {
            if (!CommonUtils.isInternetConnectionAvailable(this)) {
                responseInterface.serverResponse(null,
                        MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE);
                return;
            }

            GetAnswerDetailForStudentHWParam param = new GetAnswerDetailForStudentHWParam();
            param.setAction("getAnswersDetailsForStudent");
            param.setUserId(selectedPlayerData.getParentUserId());
            param.setPlayerId(selectedPlayerData.getPlayerid());
            param.setHwId(hwId);
            param.setCusHWId(cusHWId + "");
            MathFriendzyHelper.getAnswerDetailForStudentHW(this, param, new HttpResponseInterface() {
                @Override
                public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                    responseInterface.serverResponse(httpResponseBase, requestCode);
                }
            }, true);
        }catch(Exception e){
            e.printStackTrace();
            responseInterface.serverResponse(null,
                    MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE);
        }
    }

    /**
     * Update only active inactive oponent input
     * @param numberOfActiveInActiveInput
     */
    private void updateNumberOfActiveInputInMainResult(int numberOfActiveInActiveInput){
        try{
            if(numberOfActiveInActiveInput < 0)
                numberOfActiveInActiveInput = 0;
            selectedCustomeResult.setNumberOfActiveInput(numberOfActiveInActiveInput);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Update custome played score
     * @param response
     * @param customePlayListByStudent
     */
    private void updateCustomePlayedScore(SaveHomeWorkServerResponse response,
                                          ArrayList<CustomePlayerAns> customePlayListByStudent){
        try{
            if(response != null){
                selectedCustomeResult.setScore
                        (response.getCurrentPlaySubCatScore());
                selectedCustomeResult.setAlreadyPlayed(true);
            }
            selectedCustomeResult.setCustomePlayerAnsList(customePlayListByStudent);
            selectedCustomeResult.setNumberOfActiveInput(this.getUpdatedNumberOfActiveOponent
                    (customePlayListByStudent));
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Return the updated active input
     * @param customePlayListByStudent
     * @return
     */
    private int getUpdatedNumberOfActiveOponent(ArrayList<CustomePlayerAns> customePlayListByStudent){
        int activeOponentInput = 0;
        try {
            for(int  i = 0 ; i < customePlayListByStudent.size(); i ++ ){
                if(customePlayListByStudent.get(i).getOpponentInput()
                        .equalsIgnoreCase(MathFriendzyHelper.YES + ""))
                    activeOponentInput ++ ;
                if(customePlayListByStudent.get(i).getWorkInput()
                        .equalsIgnoreCase(MathFriendzyHelper.YES + ""))
                    activeOponentInput ++;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return activeOponentInput;
    }

    /**
     * Show the dialog for expire homework
     */
    private void showExpireQuizzDialog(){
        MathFriendzyHelper.showWarningDialog(this, MathFriendzyHelper.getTreanslationTextById(this ,
                "lblSorryYouAreNotAllowedToPlay"));
    }


    private void goForPracticeClick(PracticeResultSubCat practiceResultSubCat,
                                    GetHomeWorkForStudentWithCustomeResponse homeworkData , int index){
        final boolean isExpireQuizz = MathFriendzyHelper.isDateExpire(homeworkData.getDate());
        if(isExpireQuizz){
            this.showExpireQuizzDialog();
            return ;
        }
        this.selectedPracticeResultSubCat = practiceResultSubCat;
        ArrayList<Integer> categories =	new ArrayList<Integer>();
        categories.add(practiceResultSubCat.getSubCatId());
        Intent intent = new Intent(this,LearningCenterEquationSolveWithTimer.class);
        intent.putIntegerArrayListExtra("selectedCategories", categories);
        intent.putExtra("isForHomeWork", true);
        intent.putExtra("numberOfProblems", practiceResultSubCat.getProblems());
        intent.putExtra("HWId", homeworkData.getHomeWorkId());
        intent.putExtra("catId", practiceResultSubCat.getCatId());
        intent.putExtra("subCatId", practiceResultSubCat.getSubCatId());
        startActivityForResult(intent, PLAY_PRACTICE_SKILL_REQUEST);
    }

    /**
     * This method update the current play score for the current selected cat and subCat
     * for the practice skill
     * @param response
     */
    private void updatePracticePlayedScore(SaveHomeWorkServerResponse response){
        selectedPracticeResultSubCat
                .setScore(response.getCurrentPlaySubCatScore());
        //for offline mode
        this.savePlayedDataToLocal(response);
    }

    /**
     * Update the list and create json string and save into LocalDB
     */
    private void savePlayedDataToLocal(SaveHomeWorkServerResponse response){
        try{
            MathFriendzyHelper.updateMainHomeworkList(response);
            MathFriendzyHelper.createJsonForHomeworkAndSaveIntoLocal
                    (this, HomeWorkListAdapter.homeWorkresult,
                            selectedPlayerData.getParentUserId()
                            , selectedPlayerData.getPlayerid() , new HttpServerRequest() {
                                @Override
                                public void onRequestComplete() {

                                }
                            });

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void goForWordProblem(WordSubCatResult subCatResult,
                                  GetHomeWorkForStudentWithCustomeResponse homeworkData , int index){
        this.selectedSubCatResult = subCatResult;
        Intent intent = new Intent(this , LearningCenterSchoolCurriculumEquationSolve.class);
        intent.putExtra("isFromHomeWork", true);
        intent.putExtra("categoryId",Integer.parseInt(subCatResult.getCatId()));
        intent.putExtra("subCategoryId", subCatResult.getSubCatId());
        intent.putExtra("playerSelectedGrade", MathFriendzyHelper
                .parseInt(homeworkData.getGrade()));
        intent.putExtra("HWId", homeworkData.getHomeWorkId());
        intent.putExtra("subCatName",
                MathFriendzyHelper.
                        getSchoolCurriculumSubCatNameByCatIdAndSubCatId
                                (this, subCatResult.getCatId(),
                                        subCatResult.getSubCatId() + ""));
        startActivityForResult(intent , PLAY_WORD_PROBLEM_REQUEST);
    }

    /**
     * This method update the current play score for the current selected cat and subCat
     * for the word problem
     * @param response
     */
    private void updateWordPlayedScore(SaveHomeWorkServerResponse response){
        selectedSubCatResult.setScore(response.getCurrentPlaySubCatScore());
        //for offline mode
        this.savePlayedDataToLocal(response);
    }
}
