package com.mathfriendzy.controller.homework.checkhomeworkquiz;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.homework.ActCheckHomeWork;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.homework.CustomePlayerAns;
import com.mathfriendzy.model.homework.CustomeResult;
import com.mathfriendzy.model.homework.PracticeResult;
import com.mathfriendzy.model.homework.PracticeResultSubCat;
import com.mathfriendzy.model.homework.PracticeSkillSortByCategoryId;
import com.mathfriendzy.model.homework.SaveHomeWorkServerResponse;
import com.mathfriendzy.model.homework.WordResult;
import com.mathfriendzy.model.homework.WordSubCatResult;
import com.mathfriendzy.model.homework.checkhomework.GetDetailOfHomeworkWithCustomeResponse;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.utils.CommonUtils;

import java.util.ArrayList;
import java.util.Collections;

public class ActCheckStudentHWQuiz extends ActBase {

    private final String TAG = this.getClass().getSimpleName();

    private TextView txtStudentName = null;
    private TextView txtGradeWithDueDate = null;
    private TextView txtTotalProblems = null;
    private TextView txtZeroCredit = null;
    private TextView txtHaltCredit = null;
    private TextView txtFullCredit = null;
    private TextView txtAvgScore = null;

    private LinearLayout homeWorkQuizzLayout = null;

    //student detail with homework data
    private GetDetailOfHomeworkWithCustomeResponse studentDetail = null;

    private String practiceSkillText = null;
    private String wordProblemText = null;
    private String customeText = null;

    //for the layout for practice,word and custom in which only one layout shows the txtNo ans score
    private boolean isAlreadyTextNoAndScoreDisplayed = false;

    //for custom
    private ArrayList<RelativeLayout> customSubCatLayout = null;

    //To open the Check home work screen for the custom homework
    private final int CHECK_CUSTOME_HW_REQUEST = 1001;
    private CustomeResult customeResult = null;//selcted custome result

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_check_student_hwquiz);

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside onCreate()");

        this.init();
        this.getIntentValues();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setCreditAndScore();
        this.setListenerOnWidgets();
        this.setHomeWorkData();

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "outside onCreate()");
    }


    /**
     * Initialize the initial values
     */
    private void init() {
        customSubCatLayout = new ArrayList<RelativeLayout>();
    }


    /**
     * Get the intent values which are set in the previous screen
     */
    private void getIntentValues() {
        studentDetail = (GetDetailOfHomeworkWithCustomeResponse)
                this.getIntent().getSerializableExtra("studentHWDetail");
    }


    @Override
    protected void setWidgetsReferences() {

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside setWidgetsReferences()");

        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        txtStudentName = (TextView) findViewById(R.id.txtStudentName);
        txtGradeWithDueDate = (TextView) findViewById(R.id.txtGradeWithDueDate);
        txtTotalProblems = (TextView) findViewById(R.id.txtTotalProblems);
        txtZeroCredit = (TextView) findViewById(R.id.txtZeroCredit);
        txtHaltCredit = (TextView) findViewById(R.id.txtHaltCredit);
        txtFullCredit = (TextView) findViewById(R.id.txtFullCredit);
        txtAvgScore = (TextView) findViewById(R.id.txtAvgScore);

        homeWorkQuizzLayout = (LinearLayout) findViewById(R.id.homeWorkQuizzLayout);

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "outside setWidgetsReferences()");

    }

    @Override
    protected void setTextFromTranslation() {

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside setTextFromTranslation()");

        try {
            Translation transeletion = new Translation(this);
            transeletion.openConnection();
            txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblCheckHomework")
                    + "/" + transeletion.getTranselationTextByTextIdentifier("lblQuizzes"));
            txtStudentName.setText(transeletion.getTranselationTextByTextIdentifier("mfLblStudentsName")
                    + ": " + studentDetail.getfName() + " " + studentDetail.getlName());
            txtGradeWithDueDate.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade")
                    + ": " + studentDetail.getGrade() + "          "
                    + transeletion.getTranselationTextByTextIdentifier("lblDueDate")
                    + ": " + studentDetail.getDueDate());
            txtTotalProblems.setText(transeletion.getTranselationTextByTextIdentifier("lblTotal")
                    + " " + transeletion.getTranselationTextByTextIdentifier("lblNumOfProblems")
                    + ": " + studentDetail.getTotalQuestions());

            practiceSkillText = transeletion.getTranselationTextByTextIdentifier("lblSolveEquations");
            wordProblemText = transeletion.getTranselationTextByTextIdentifier("lblWordProblem");
            customeText = transeletion.getTranselationTextByTextIdentifier("lblClass")
                    + " " + transeletion.getTranselationTextByTextIdentifier("lblWork")
                    + "/" + transeletion.getTranselationTextByTextIdentifier("lblQuizzes");
            transeletion.closeConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "outside setTextFromTranslation()");

    }

    @Override
    protected void setListenerOnWidgets() {

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside setListenerOnWidgets()");

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "outside setListenerOnWidgets()");

    }

    /**
     * Set the credit and avg score on the bottom bar of the screen
     */
    private void setCreditAndScore() {
        try {
            Translation transeletion = new Translation(this);
            transeletion.openConnection();
            txtZeroCredit.setText(transeletion.getTranselationTextByTextIdentifier("lblZero")
                    + " " + transeletion.getTranselationTextByTextIdentifier("lblCredit")
                    + ": " + studentDetail.getZeroCredit());
            txtHaltCredit.setText(transeletion.getTranselationTextByTextIdentifier("lblHalf")
                    + " " + transeletion.getTranselationTextByTextIdentifier("lblCredit")
                    + ": " + studentDetail.getHalfCredit());
            txtFullCredit.setText(transeletion.getTranselationTextByTextIdentifier("lblFull")
                    + " " + transeletion.getTranselationTextByTextIdentifier("lblCredit")
                    + ": " + studentDetail.getFullCredit());
            txtAvgScore.setText(transeletion.getTranselationTextByTextIdentifier("lblAverage")
                    + " " + transeletion.getTranselationTextByTextIdentifier("mfBtnTitleScore")
                    + ": " + studentDetail.getAvgScore() + "%");
            transeletion.closeConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method set the home work data
     */
    private void setHomeWorkData() {
        homeWorkQuizzLayout.removeAllViews();

        if (studentDetail.getCustomeresultList().size() > 0) {
            homeWorkQuizzLayout.addView
                    (this.addCustomeProblemData(studentDetail.getCustomeresultList()));
        }

        if (studentDetail.getPractiseResultList().size() > 0) {
            Collections.sort(studentDetail.getPractiseResultList(),
                    new PracticeSkillSortByCategoryId());
            homeWorkQuizzLayout.addView
                    (this.addPracticeSkillData(studentDetail.getPractiseResultList()));
        }

        if (studentDetail.getWordResultList().size() > 0) {
            homeWorkQuizzLayout.addView
                    (this.addWordProblemData(studentDetail.getWordResultList()));
        }
    }

    /**
     * This method create and return a linear layout
     *
     * @return
     */
    private LinearLayout getLinearLayout() {
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);
        return layout;
    }

    private View getInflatedViewForQuizzQuestionType(int inflateFor) {
        View view = LayoutInflater.from(this)
                .inflate(R.layout.homework_quizz_question_type_layout, null);
        TextView txtView = (TextView) view.findViewById(R.id.txtQuestionType);
        TextView txtNo = (TextView) view.findViewById(R.id.txtNo);
        TextView txtScore = (TextView) view.findViewById(R.id.txtScore);

        if (inflateFor == 1) {//for practice skill
            txtView.setText(practiceSkillText);
        } else if (inflateFor == 2) {//for word problem
            txtView.setText(wordProblemText);
        } else if (inflateFor == 3) {//for custome
            txtView.setText(customeText);
        }

        //for show the txtNo and txtScore text
        if (!isAlreadyTextNoAndScoreDisplayed) {
            isAlreadyTextNoAndScoreDisplayed = true;
            txtNo.setVisibility(TextView.VISIBLE);
            txtScore.setVisibility(TextView.VISIBLE);
        }
        return view;
    }

    private View getCategoryInflatedLayout(int inflateFor) {
        View view = LayoutInflater.from(this)
                .inflate(R.layout.school_home_question_categories_layout, null);
        return view;
    }

    private View getSubCategoryInflatedLayout(int inflateFor) {
        View view = LayoutInflater.from(this)
                .inflate(R.layout.home_work_quizz_sub_catgory_layout, null);
        ImageView imgArrow = (ImageView) view.findViewById(R.id.imgArrow);
        if (inflateFor != 3) {//for custom
            imgArrow.setVisibility(ImageView.INVISIBLE);
        }
        return view;
    }

    /**
     * Get practice skill subCat Name
     *
     * @param catId
     * @param subCatId
     * @return
     */
    private String getPracticeSkillSubCatName(String catId, String subCatId) {

        String subCatName = null;
        LearningCenterimpl laerningCenter = new LearningCenterimpl(this);
        laerningCenter.openConn();
        subCatName = laerningCenter.
                getMathOperationCategoriesByCatIdAndSubCatId(catId, subCatId)
                .getMathOperationCategory();
        laerningCenter.closeConn();

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        subCatName = transeletion.getTranselationTextByTextIdentifier(subCatName);
        transeletion.closeConnection();

        return subCatName;
    }

    /**
     * This method add the practice layout data into the main layout to display
     *
     * @param arrayList
     */
    private LinearLayout addPracticeSkillData(ArrayList<PracticeResult> arrayList) {
        LinearLayout layout = this.getLinearLayout();
        layout.addView(this.getInflatedViewForQuizzQuestionType(1));
        layout.addView(this.addPracticeSkillLayout(arrayList));
        return layout;
    }

    /**
     * This method add the word problem data into main layout to display
     *
     * @param arrayList
     */
    private LinearLayout addWordProblemData(ArrayList<WordResult> arrayList) {
        LinearLayout layout = this.getLinearLayout();
        layout.addView(this.getInflatedViewForQuizzQuestionType(2));
        layout.addView(this.addWordProblemLayout(arrayList));
        return layout;
    }

    /**
     * This method add the custome data into main layout to display
     *
     * @param arrayList
     */
    private LinearLayout addCustomeProblemData(ArrayList<CustomeResult> arrayList) {
        LinearLayout layout = this.getLinearLayout();
        layout.addView(this.getInflatedViewForQuizzQuestionType(3));
        layout.addView(this.addCustomeLayout(arrayList));
        return layout;
    }

    /**
     * This method add the practice skill layout with cat and subCat
     *
     * @param arrayList
     * @return
     */
    private LinearLayout addPracticeSkillLayout(final ArrayList<PracticeResult> arrayList) {

        LinearLayout layout = this.getLinearLayout();
        ArrayList<LearningCenterTransferObj> laernignCenterFunctionsList =
                MathFriendzyHelper.getPracticeSkillCategories(this);
        ArrayList<String> updatedCatName = MathFriendzyHelper
                .getUpdatedPracticeSkillCatNameList(laernignCenterFunctionsList, this);

        for (int i = 0; i < arrayList.size(); i++) {
            for (int j = 0; j < laernignCenterFunctionsList.size(); j++) {
                if (arrayList.get(i).getCatId().equals
                        (laernignCenterFunctionsList.get(j)
                                .getLearningCenterMathOperationId() + "")) {
                    View view = this.getCategoryInflatedLayout(1);
                    ImageView imgSign = (ImageView) view.findViewById(R.id.imgSign);
                    TextView txtCatName = (TextView) view.findViewById(R.id.txtCategoryName);
                    imgSign.setBackgroundResource(getResources().getIdentifier
                            ("mf_" + laernignCenterFunctionsList.get(j)
                                            .getLearningCenterOperation()
                                            .toLowerCase().replace(" ", "_") + "_sign", "drawable",
                                    getPackageName()));
                    txtCatName.setText(updatedCatName.get(j));
                    layout.addView(view);

                    final ArrayList<PracticeResultSubCat> practiceResultSubCatList =
                            arrayList.get(i).getPracticeResultSubCatList();

                    for (int k = 0; k < practiceResultSubCatList.size(); k++) {
                        View subCatView = this.getSubCategoryInflatedLayout(1);
                        TextView txtSubCatName = (TextView) subCatView.findViewById(R.id.txtSubCategoryName);
                        TextView txtNoOfQuestions = (TextView) subCatView.findViewById(R.id.txtNo);
                        TextView txtScore = (TextView) subCatView.findViewById(R.id.txtScore);

                        txtSubCatName.setText(this.getPracticeSkillSubCatName
                                (arrayList.get(i).getCatId(), practiceResultSubCatList.get(k).getSubCatId() + ""));
                        txtNoOfQuestions.setText(practiceResultSubCatList.get(k).getProblems() + "");
                        txtScore.setText(practiceResultSubCatList.get(k).getScore() + "%");
                        layout.addView(subCatView);
                        practiceResultSubCatList.get(k).setCatId(arrayList.get(i).getCatId());
                    }
                    break;
                }
            }
        }

        return layout;
    }

    /**
     * This method add the word problem layout with Cat and SubCat
     *
     * @param arrayList
     * @return
     */
    private LinearLayout addWordProblemLayout(ArrayList<WordResult> arrayList) {
        LinearLayout layout = this.getLinearLayout();

        for (int i = 0; i < arrayList.size(); i++) {
            View view = this.getCategoryInflatedLayout(2);
            ImageView imgSign = (ImageView) view.findViewById(R.id.imgSign);
            TextView txtCatName = (TextView) view.findViewById(R.id.txtCategoryName);
            txtCatName.setText(MathFriendzyHelper
                    .getSchoolCurriculumCategoryNameByCatId
                            (this, arrayList.get(i).getCatIg()));
            imgSign.setVisibility(ImageView.GONE);
            layout.addView(view);

            ArrayList<WordSubCatResult> subCatList = arrayList.get(i).getSubCatResultList();
            for (int j = 0; j < subCatList.size(); j++) {
                View subCatView = this.getSubCategoryInflatedLayout(2);
                TextView txtSubCatName = (TextView) subCatView.findViewById(R.id.txtSubCategoryName);
                TextView txtNoOfQuestions = (TextView) subCatView.findViewById(R.id.txtNo);
                TextView txtScore = (TextView) subCatView.findViewById(R.id.txtScore);
                txtSubCatName.setText(MathFriendzyHelper.
                        getSchoolCurriculumSubCatNameByCatIdAndSubCatId
                                (this, arrayList.get(i).getCatIg(), subCatList.get(j).getSubCatId() + ""));
                txtNoOfQuestions.setText("10");
                txtScore.setText(subCatList.get(j).getScore() + "%");
                layout.addView(subCatView);

                subCatList.get(j).setCatId(arrayList.get(i).getCatIg());
            }
        }
        return layout;
    }

    /**
     * Add the custom layout with chapter
     *
     * @param arrayList
     * @return
     */
    private LinearLayout addCustomeLayout(final ArrayList<CustomeResult> arrayList) {
        LinearLayout layout = this.getLinearLayout();
        for (int i = 0; i < arrayList.size(); i++) {

            View subCatView = this.getSubCategoryInflatedLayout(3);
            RelativeLayout rllayout = (RelativeLayout) subCatView.findViewById(R.id.layout);
            TextView txtSubCatName = (TextView) subCatView.findViewById(R.id.txtSubCategoryName);
            TextView txtNoOfQuestions = (TextView) subCatView.findViewById(R.id.txtNo);
            TextView txtScore = (TextView) subCatView.findViewById(R.id.txtScore);

            txtSubCatName.setText(arrayList.get(i).getTitle());
            txtNoOfQuestions.setText(arrayList.get(i).getProblem() + "");
            txtScore.setText(arrayList.get(i).getScore() + "%");
            layout.addView(subCatView);

            customSubCatLayout.add(rllayout);
            rllayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*if(isExpireQuizz){
						showExpireQuizzDialog();
					}else{*/
                    for (int i = 0; i < customSubCatLayout.size(); i++) {
                        if (v == customSubCatLayout.get(i)) {
                            customeResult = arrayList.get(i);
                            //Log.e(TAG, "click");
                            Intent intent = new Intent(ActCheckStudentHWQuiz.this,
                                    ActCheckHomeWork.class);
                            //selectedCustomeHWId = arrayList.get(i).getCustomHwId();
                            intent.putExtra("isCalledFortecherCheckHW", true);
                            intent.putExtra("teacherName", studentDetail.getfName() + " "
                                    + studentDetail.getlName());
                            intent.putExtra("grade", studentDetail.getGrade());
                            intent.putExtra("dueDate", studentDetail.getDueDate());
                            intent.putExtra("isExpireQuizz", true);
                            intent.putExtra("studentDetail", studentDetail);
                            intent.putExtra("customeDetail", arrayList.get(i));
                            startActivityForResult(intent, CHECK_CUSTOME_HW_REQUEST);
                        }
                        //}
                    }
                }
            });
        }
        return layout;
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase,
                               int requestCode) {
        // TODO Auto-generated method stub
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHECK_CUSTOME_HW_REQUEST:
                    SaveHomeWorkServerResponse response =
                            (SaveHomeWorkServerResponse) data.getSerializableExtra("resultAfterPlayed");
                    ArrayList<CustomePlayerAns> customePlayListByStudent =
                            (ArrayList<CustomePlayerAns>) data.getSerializableExtra("customePlayData");
                    this.updateTheTeacherCheckDataAndRefreshUI(response, customePlayListByStudent);
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Update the Teacher checked data into the original list and Refresh the UI based on the
     * New Data
     *
     * @param response
     * @param customePlayListByStudent
     */
    private void updateTheTeacherCheckDataAndRefreshUI(SaveHomeWorkServerResponse response,
                                                       ArrayList<CustomePlayerAns> customePlayListByStudent) {
        try {
            studentDetail.setHalfCredit(response.getHalfCredit());
            studentDetail.setZeroCredit(response.getZeroCredit());
            studentDetail.setFullCredit(response.getFullCredit());
            studentDetail.setAvgScore(response.getAvgScore());
            studentDetail.setCustomeScore(response.getCustomeScore());
            customeResult.setScore(response.getCustomeScore());
            customeResult.setCustomePlayerAnsList(customePlayListByStudent);
            //refresh the new data
            this.init();
            this.setCreditAndScore();
            this.setHomeWorkData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("studentDetail", studentDetail);
        setResult(RESULT_OK, intent);
        finish();
        super.onBackPressed();
    }
}
