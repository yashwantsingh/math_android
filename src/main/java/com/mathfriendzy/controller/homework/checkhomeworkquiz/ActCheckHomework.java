package com.mathfriendzy.controller.homework.checkhomeworkquiz;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.homework.assignhomeworkquiz.ActAssignHomework;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.ClassSelectedListener;
import com.mathfriendzy.listener.HomeworkClickListener;
import com.mathfriendzy.model.homework.CustomeResult;
import com.mathfriendzy.model.homework.GetHomeWorkForStudentWithCustomeResponse;
import com.mathfriendzy.model.homework.HomeworkData;
import com.mathfriendzy.model.homework.PracticeResultSubCat;
import com.mathfriendzy.model.homework.SelectAssignHomeworkClassParam;
import com.mathfriendzy.model.homework.WordSubCatResult;
import com.mathfriendzy.model.homework.checkhomework.GetHomeworksWithCustomForTeacher;
import com.mathfriendzy.model.homework.checkhomework.GetHomeworksWithCustomForTeacherResponse;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.classes.ClassWithName;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TreeSet;

public class ActCheckHomework extends ActBase {

    private final String TAG = this.getClass().getSimpleName();

    private TextView txtGrade = null;
    private TextView txtClassAverage = null;
    private TextView txtDueDate = null;
    private TextView txtLayoutGrade = null;
    private TextView txtPracticeSkill = null;
    private TextView txtSchoolCurriculum = null;
    private TextView txtCustome = null;
    private TextView txtAvgScore = null;
    private ImageView imgInfo = null;
    private Button btnShowMore 	= null;
    //private Spinner spinnerGrade = null;

    private ListView schoolHomeWorkList = null;

    //for info dialog msg
    private String lblAlertStudentAvgScoreCalculated = null;

    private UserPlayerDto selectedPlayerData = null;
    private int offset = 0;

    /*private LinkedHashMap<String, ArrayList<GetHomeworksWithCustomForTeacherResponse>>
            homwworkWithCustometeacherMap = null;*/

    private final String ALL = "All";
    private String selectedGrade = ALL;//by default
    private GetHomeworkWithCustomeForteacherAdapter adapter = null;

    private TextView txtSelectAPrevHW = null;
    private TextView txtAssignDate = null;
    private RelativeLayout rlFromDateLayout = null;
    private TextView edtDatePickerFrom = null;
    private RelativeLayout rlToDateLayout = null;
    private TextView edtDatePickerTo = null;
    private RelativeLayout selectClassLayout = null;
    private TextView txtSelectClassValue = null;
    private TextView txtSubject = null;
    private TextView txtSubjectValues = null;
    private TextView txtClass = null;
    private TextView txtClassValues = null;
    private TextView txtTo = null;
    private String startDate = "";
    private String endDate = "";
    private final String SERVICE_TIME_MY_FORMATE = "yyyy-MM-dd";
    private final String SERVICE_TIME_CONVERTED_FORMATE = "MMM. dd, yyyy";
    private String lblStartDateCantLater = null;
    //private int selectedGrade = 1;
    private final int NUMBER_OF_FORWARD_DAYS = 14;
    private final int NUMBER_OF_FORWARD_MONTH = 1;
    private final int LIMIT = 10;

    private final int DEFAULT_ALL_CLASS_ID  = 0;

    private final int EDIT_HOMEWORK_REQUEST_CODE = 1001;

    private LinearLayout rlSubjectClassLayout = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_check_homework);

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside onCreate()");

        selectedPlayerData = this.getPlayerData();

        this.init();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.setInitialStartEndDate();
        this.checkForWordCategoriesAndGetHomeworkData();
        this.setVisibilityClassSubjectLayout(false);

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside onCreate()");
    }

    private void setVisibilityClassSubjectLayout(boolean isVisible){
        if(isVisible){
            rlSubjectClassLayout.setVisibility(View.VISIBLE);
        }else{
            rlSubjectClassLayout.setVisibility(View.GONE);
        }
    }

    /**
     * Initialize the initial variables
     */
    private void init() {
        /*homwworkWithCustometeacherMap = new LinkedHashMap
                <String, ArrayList<GetHomeworksWithCustomForTeacherResponse>>();*/

        //new homework changes
        /*startDate  = MathFriendzyHelper.getCurrentDateInGiveGformateDate(SERVICE_TIME_CONVERTED_FORMATE);
        endDate = MathFriendzyHelper.getBackAndForthDateInGivenFormatDate
                (SERVICE_TIME_CONVERTED_FORMATE, NUMBER_OF_FORWARD_MONTH, 0, 0);*/
        startDate  = MathFriendzyHelper.getBackAndForthDateInGivenFormatDate
                (SERVICE_TIME_CONVERTED_FORMATE, 0, 0, -NUMBER_OF_FORWARD_DAYS);
        endDate = MathFriendzyHelper.getCurrentDateInGiveGformateDate(SERVICE_TIME_CONVERTED_FORMATE);
    }

    @Override
    protected void setWidgetsReferences() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside setWidgetsReferences()");

        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        txtGrade = (TextView) findViewById(R.id.txtGrade);
        txtClassAverage = (TextView) findViewById(R.id.txtClassAverage);
        txtDueDate = (TextView) findViewById(R.id.txtDueDate);
        txtLayoutGrade = (TextView) findViewById(R.id.txtLayoutGrade);
        txtPracticeSkill = (TextView) findViewById(R.id.txtPracticeSkill);
        txtSchoolCurriculum = (TextView) findViewById(R.id.txtSchoolCurriculum);
        txtCustome = (TextView) findViewById(R.id.txtCustome);
        txtAvgScore = (TextView) findViewById(R.id.txtAvgScore);
        imgInfo = (ImageView) findViewById(R.id.imgInfo);
        btnShowMore = (Button) findViewById(R.id.btnShowMore);
        //spinnerGrade = (Spinner) findViewById(R.id.spinnerGrade);
        schoolHomeWorkList = (ListView) findViewById(R.id.schoolHomeWorkList);


        //new homework changes
        txtSelectAPrevHW = (TextView) findViewById(R.id.txtSelectAPrevHW);
        txtAssignDate = (TextView) findViewById(R.id.txtAssignDate);
        rlFromDateLayout = (RelativeLayout) findViewById(R.id.rlFromDateLayout);
        txtTo = (TextView) findViewById(R.id.txtTo);
        edtDatePickerFrom = (TextView) findViewById(R.id.edtDatePickerFrom);
        rlToDateLayout = (RelativeLayout) findViewById(R.id.rlToDateLayout);
        edtDatePickerTo = (TextView) findViewById(R.id.edtDatePickerTo);
        selectClassLayout = (RelativeLayout) findViewById(R.id.selectClassLayout);
        txtSelectClassValue = (TextView) findViewById(R.id.txtSelectClassValue);
        txtSubject = (TextView) findViewById(R.id.txtSubject);
        txtSubjectValues = (TextView) findViewById(R.id.txtSubjectValues);
        txtClass = (TextView) findViewById(R.id.txtClass);
        txtClassValues = (TextView) findViewById(R.id.txtClassValues);

        rlSubjectClassLayout = (LinearLayout) findViewById(R.id.rlSubjectClassLayout);

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside setWidgetsReferences()");

    }

    @Override
    protected void setTextFromTranslation() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside setTextFromTranslation()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblHomeWorkAssignment"));
        txtGrade.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade"));
        txtClassAverage.setText(transeletion.getTranselationTextByTextIdentifier("lblClass")
                + " " + transeletion.getTranselationTextByTextIdentifier("lblAverage"));
        txtDueDate.setText(transeletion.getTranselationTextByTextIdentifier("lblDueDate")
                +"\n"+"MM-DD-YY");
        txtLayoutGrade.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade"));
        txtPracticeSkill.setText(transeletion.getTranselationTextByTextIdentifier("lblSolveEquations"));

        txtSchoolCurriculum.setText(transeletion.getTranselationTextByTextIdentifier("lblWordProblem"));

        txtCustome.setText(transeletion.getTranselationTextByTextIdentifier("lblCustom"));

        txtAvgScore.setText(transeletion.getTranselationTextByTextIdentifier("lblAverage")
                + "\n" + transeletion.getTranselationTextByTextIdentifier("mfBtnTitleScore"));
        btnShowMore.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleShowMore"));

        lblAlertStudentAvgScoreCalculated = transeletion.getTranselationTextByTextIdentifier
                ("lblAlertStudentAvgScoreCalculated");

        //new homework changes
        txtSelectAPrevHW.setText(transeletion.getTranselationTextByTextIdentifier("lblEsyWyGrdHw"));
        txtAssignDate.setText(transeletion.getTranselationTextByTextIdentifier("lblDueDate"));
        txtTo.setText(transeletion.getTranselationTextByTextIdentifier("lblTo"));
        txtSubject.setText(transeletion.getTranselationTextByTextIdentifier("lblSubjectHeader") + ":");
        txtClass.setText(transeletion.getTranselationTextByTextIdentifier("lblClass") + ":");
        edtDatePickerFrom.setHint(transeletion.getTranselationTextByTextIdentifier("lblSelect"));
        edtDatePickerTo.setHint(transeletion.getTranselationTextByTextIdentifier("lblSelect"));
        txtSelectClassValue.setHint(transeletion.getTranselationTextByTextIdentifier("lblSelect"));
        lblStartDateCantLater = transeletion.getTranselationTextByTextIdentifier("lblStartDateCantLater");

        transeletion.closeConnection();

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside setTextFromTranslation()");
    }

    @Override
    protected void setListenerOnWidgets() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside setListenerOnWidgets()");

        imgInfo.setOnClickListener(this);
        btnShowMore.setOnClickListener(this);

        rlFromDateLayout.setOnClickListener(this);
        rlToDateLayout.setOnClickListener(this);
        selectClassLayout.setOnClickListener(this);

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside setListenerOnWidgets()");
    }


    /**
     * GetHomework data
     */
    private void getHomeworksWithCustomForTeacher(String selectedClasses , int firstVisit){
        try{
            GetHomeworksWithCustomForTeacher param = new GetHomeworksWithCustomForTeacher();
            //param.setAction("getHomeworksWithCustomForTeacher");
            param.setAction("getAssignedHwInDateRange");
            param.setTeacherId(CommonUtils.getUserId(this));
            param.setStartDate(startDate);
            param.setEndDate(endDate);
            param.setClassId(selectedClasses);
            param.setLimit(LIMIT);
            param.setOffset(offset);
            param.setFirstVisit(firstVisit);

            if(CommonUtils.isInternetConnectionAvailable(this)){
                new MyAsyckTask(ServerOperation.createPostRequestForGetHomeworksWithCustomForTeacher(param)
                        , null, ServerOperationUtil.GETHOMEWORK_WITH_CUSTOME_TEACHER_REQUEST, this,
                        this, ServerOperationUtil.SIMPLE_DIALOG , true ,
                        getString(R.string.please_wait_dialog_msg))
                        .execute();
            }else{
                CommonUtils.showInternetDialog(this);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Set the adapter
     * @param homeworkWithCustomeList
     */
    private void setAdapterData(ArrayList<GetHomeworksWithCustomForTeacherResponse> homeworkWithCustomeList){
		/*try{
			if(adapter == null){
				adapter = new GetHomeworkWithCustomeForteacherAdapter(this,
						homwworkWithCustometeacherMap.get(selectedGrade));
				schoolHomeWorkList.setAdapter(adapter);
			}else{
				adapter.homweworkWithCustomeList = homwworkWithCustometeacherMap.get(selectedGrade);
				adapter.notifyDataSetChanged();
			}
			
			schoolHomeWorkList.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					Intent intent = new Intent(ActCheckHomework.this , 
							ActCheckHomeworkStudents.class);
					intent.putExtra("homeworkCustome", 
							adapter.homweworkWithCustomeList.get(position));
					startActivity(intent);
				}
			});
			
		}catch(Exception e){
			e.printStackTrace();
		}*/

        if(adapter == null){
            if(!(homeworkWithCustomeList != null && homeworkWithCustomeList.size() > 0)){
                MathFriendzyHelper.showWarningDialog(this ,
                        MathFriendzyHelper.getTreanslationTextById(this , "lblThereAreNoClass"));
                return ;
            }

            adapter = new GetHomeworkWithCustomeForteacherAdapter(this , homeworkWithCustomeList);
            adapter.setClassWithNameList(this.classWithNames);
            schoolHomeWorkList.setAdapter(adapter);

            HomeworkClickListener homeworkClickListener = new HomeworkClickListener() {

                //work as edit homework
                @Override
                public void onCustomClick(CustomeResult customeResult,
                                          GetHomeWorkForStudentWithCustomeResponse homeworkData , int index) {
                    openAssignHWScreenToEditHW(adapter.homweworkWithCustomeList.get(index));
                }

                @Override
                public void onPracticeClick(PracticeResultSubCat practiceResultSubCat,
                                            GetHomeWorkForStudentWithCustomeResponse homeworkData , int index) {

                }

                @Override
                public void onWordClick(final WordSubCatResult subCatResult,
                                        final GetHomeWorkForStudentWithCustomeResponse homeworkData ,
                                        final int index) {

                }

                @Override
                public void onHomeworkClick(final int position) {
                    Intent intent = new Intent(ActCheckHomework.this , ActCheckHomeworkStudents.class);
                    intent.putExtra("homeworkCustome", adapter.homweworkWithCustomeList.get(position));
                    if(selectedClassList != null && selectedClassList.size() > 0) {
                        intent.putExtra("selectedClassId", selectedClassList.get(0).getClassId());
                        intent.putExtra("selectedClass", selectedClassList.get(0));
                    }else{
                        intent.putExtra("selectedClassId", 0);
                    }
                    startActivity(intent);
                }
            };
            adapter.initializeListener(homeworkClickListener);
        }else{
            adapter.addMoreRecord(homeworkWithCustomeList);
        }
    }

    /**
     * Set the grade adapter to spinner
     */
	/*private void setGradeAdapterToSpinner(){
		try{
			ArrayList<String> gradeList = this.getKeyList();
			ArrayAdapter<String> gradeAdapter = new ArrayAdapter<String>(this, R.layout.spinner_textview_layout,gradeList);
			gradeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinnerGrade.setAdapter(gradeAdapter);
			spinnerGrade.setSelection(gradeAdapter.getPosition(selectedGrade));

			spinnerGrade.setOnItemSelectedListener(new OnItemSelectedListener(){	
				@Override
				public void onItemSelected(AdapterView<?> parent, View view,
						int pos, long id) {
					selectedGrade = spinnerGrade.getSelectedItem().toString();
					setAdapterData();
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {

				}
			});
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/

    /**
     * Get the key list to set in the Grade Adapter
     * @return
     */
    /*private ArrayList<String> getKeyList(){
        ArrayList<String> keyList = new ArrayList<String>();
        for(String key : homwworkWithCustometeacherMap.keySet()){
            keyList.add(key);
        }
        return keyList;
    }*/

    /**
     * called when clicked on show more
     */
    private void clicOnShowMore(){
        //this.getHomeworksWithCustomForTeacher("0");
        String selectedClass = "0";
        if(selectedClassList != null && selectedClassList.size() > 0)
            selectedClass = getCommaSeparatedClassIds(selectedClassList);
        this.getHomeworksWithCustomForTeacher(selectedClass , 0);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.imgInfo:
                MathFriendzyHelper.warningDialog(this, lblAlertStudentAvgScoreCalculated);
                break;
            case R.id.btnShowMore:
                this.clicOnShowMore();
                break;
            case R.id.rlFromDateLayout:
                this.selectFromDate();
                break;
            case R.id.rlToDateLayout:
                this.selectToDate();
                break;
            case R.id.selectClassLayout:
                this.clickToSelectClass();
                break;
        }
    }

    private ClassWithName addAllClass(){
        ClassWithName classWithName = new ClassWithName();
        classWithName.setStartDate("");
        classWithName.setEndDate("");
        classWithName.setClassName("All");
        classWithName.setSubjectId(DEFAULT_ALL_CLASS_ID);
        classWithName.setClassId(DEFAULT_ALL_CLASS_ID);
        classWithName.setSubject("All");
        return classWithName;
    }
    @Override
    public void serverResponse(HttpResponseBase httpResponseBase,
                               int requestCode) {
        if(requestCode == ServerOperationUtil.GETHOMEWORK_WITH_CUSTOME_TEACHER_REQUEST){
            GetHomeworksWithCustomForTeacherResponse response =
                    (GetHomeworksWithCustomForTeacherResponse) httpResponseBase;
            if(response != null) {
                if(response.isClassArrayExist()) {
                    ArrayList<ClassWithName> classWithNames = response.getClassWithNames();
                    if (classWithNames != null && classWithNames.size() > 0) {
                        classWithNames.add(0 , this.addAllClass());
                        this.initClasses(classWithNames, response.getLastClassAssignedForHw());
                    }else{
                        if(classWithNames == null)
                            classWithNames = new ArrayList<ClassWithName>();
                        this.initClasses(classWithNames, response.getLastClassAssignedForHw());
                    }
                }
            }

            //this.setAdapterData(response.getHomeworkWithCustomeList());
            this.checkForWordCategoriesAndSetAdapter(response.getHomeworkWithCustomeList());
            if(response.getHomeworkWithCustomeList().size() >= LIMIT){
                offset += LIMIT;
                this.setVisibilityOfShowMoreButton(true);
            }else{
                this.setVisibilityOfShowMoreButton(false);
            }
        }
    }


    /**
     * This method first check for the work categories, if not found in db then download that
     * and then get the homework data
     */
    private void checkForWordCategoriesAndSetAdapter(
            final ArrayList<GetHomeworksWithCustomForTeacherResponse> finalDataList){

        if(CommonUtils.isInternetConnectionAvailable(this)){
            ArrayList<Integer> graList = this.getGradeList(finalDataList);
            if(!(graList != null && graList.size() > 0)){
                setAdapterData(finalDataList);
                return ;
            }
            MathFriendzyHelper.downloadAllGradeWordCategories(this , new HttpResponseInterface() {
                @Override
                public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                    setAdapterData(finalDataList);
                }
            } , true , graList);
        }else{
            setAdapterData(finalDataList);
        }
    }

    private ArrayList<Integer> getGradeList(ArrayList<GetHomeworksWithCustomForTeacherResponse> finalDataList){
        TreeSet<Integer> gradeList = new TreeSet<Integer>();
        for(GetHomeworksWithCustomForTeacherResponse data : finalDataList) {
            if(data.getWordResultList() != null && data.getWordResultList().size() > 0) {
                gradeList.add(MathFriendzyHelper.parseInt(data.getGrade()));
            }
        }
        return new ArrayList<Integer>(gradeList);
    }

    //new homework changes
    private void setVisibilityOfShowMoreButton(boolean bValue){
        if(bValue){
            btnShowMore.setVisibility(Button.VISIBLE);
        }else{
            btnShowMore.setVisibility(Button.INVISIBLE);
        }
    }

    /**
     * Set the start and end date initially
     */
    private void setInitialStartEndDate() {
        try {
            edtDatePickerFrom.setText(startDate);
            edtDatePickerTo.setText(endDate);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Show from date from date picker
     */
    private void selectFromDate(){
        final Calendar calender = MathFriendzyHelper.getCurrentCalender();
        MathFriendzyHelper.showDatePickerDialogPrevDateSelected(this, calender.get(Calendar.YEAR),
                calender.get(Calendar.MONTH), calender.get(Calendar.DAY_OF_MONTH),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);

                        //if (newDate.after(calender)) {
                        setDateText(edtDatePickerFrom, year, monthOfYear, dayOfMonth, true);
                        //}
                    }
                }, calender);
    }

    /**
     * Show to date from date picker
     */
    private void selectToDate(){
        final Calendar calender = MathFriendzyHelper.getCurrentCalender();
        MathFriendzyHelper.showDatePickerDialogPrevDateSelected(this, calender.get(Calendar.YEAR),
                calender.get(Calendar.MONTH), calender.get(Calendar.DAY_OF_MONTH),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);

                        //if (newDate.after(calender)) {
                        setDateText(edtDatePickerTo, year, monthOfYear, dayOfMonth, false);
                        //}
                    }
                }, calender);
    }


    /**
     * Set date text
     * @param txtView
     * @param year
     * @param month
     * @param dayOfMonth
     * @param isStartDate
     */
    private void setDateText(TextView txtView , int year , int month , int dayOfMonth ,
                             boolean isStartDate){

        String tempStartDate = startDate;
        String tempEndDate   = endDate;

        month ++;
        String formateString = MathFriendzyHelper.getFourDigitFormatNumber(year) + "-" +
                MathFriendzyHelper.getDoubleDigitFormatNumber(month) + "-" +
                MathFriendzyHelper.getDoubleDigitFormatNumber(dayOfMonth);
        String formatedDate = MathFriendzyHelper.formatDataInGivenFormat(formateString ,
                SERVICE_TIME_MY_FORMATE, SERVICE_TIME_CONVERTED_FORMATE);
        if(isStartDate){
            tempStartDate = formatedDate;
        }else{
            tempEndDate = formatedDate;
        }
        Date fromDate = MathFriendzyHelper.getValidateDate(tempStartDate , SERVICE_TIME_CONVERTED_FORMATE);
        Date toDate   = MathFriendzyHelper.getValidateDate(tempEndDate , SERVICE_TIME_CONVERTED_FORMATE);
        try {
            if(fromDate != null && toDate != null) {
                if (fromDate.compareTo(toDate) <= 0) {
                    startDate = tempStartDate;
                    endDate = tempEndDate;
                    //txtView.setText(formatedDate);
                    this.setDateTextToTextView(txtView, formatedDate);
                } else {
                    MathFriendzyHelper.showWarningDialog(this, lblStartDateCantLater);
                }
            }else{
                startDate = tempStartDate;
                endDate = tempEndDate;
                //txtView.setText(formatedDate);
                this.setDateTextToTextView(txtView , formatedDate);
            }
        }catch (Exception e){
            e.printStackTrace();
            startDate = tempStartDate;
            endDate = tempEndDate;
            txtView.setText(formatedDate);
        }
    }

    /**
     * Set the date text
     * @param txtView
     * @param dateText
     */
    private void setDateTextToTextView(TextView txtView , String dateText){
        if(CommonUtils.isInternetConnectionAvailable(this)) {
            this.resetValues();
            String selectedClass = "0";
            if(selectedClassList != null && selectedClassList.size() > 0)
                selectedClass = getCommaSeparatedClassIds(selectedClassList);
            this.getHomeworksWithCustomForTeacher(selectedClass , 0);
        }else{
            CommonUtils.showInternetDialog(this);
        }
        txtView.setText(dateText);
    }

    private ArrayList<ClassWithName> classWithNames = null;
    private ArrayList<ClassWithName> selectedClassList = null;
    private RegistereUserDto registerUserFromPreff = null;

    private void initClasses(ArrayList<ClassWithName> classWithNames ,
                             int lastClassAssignedForHw){
        this.classWithNames = classWithNames;
        if(classWithNames != null && classWithNames.size() > 0){
            for(int i = 0 ; i < classWithNames.size() ; i ++ ){
                if(classWithNames.get(i).getClassId() == lastClassAssignedForHw){
                    //classWithNames.get(i).setSelected(true);
                    //initialize the selected class
                    selectedClassList = new ArrayList<ClassWithName>();
                    selectedClassList.add(classWithNames.get(i));
                    this.setTextToSelectedClassValues(classWithNames.get(i).getClassName());
                    this.setTextToSelectedSubjectValues(classWithNames.get(i).getSubject());
                    return ;
                }
            }
        }else{
            selectedClassList = null;
            this.setTextToSelectedClassValues("");
            this.setTextToSelectedSubjectValues("");
        }
    }

    private void clickToSelectClass() {
        SelectAssignHomeworkClassParam param = new SelectAssignHomeworkClassParam();
        param.setShowForPrevSaveHomework(true);
        MathFriendzyHelper.showClassSelectionDialog(this , classWithNames , new ClassSelectedListener() {
            @Override
            public void onClassSelectionDone(ArrayList<ClassWithName> selectedClass) {
                selectedClassList = selectedClass;
                if(selectedClass != null && selectedClass.size() > 0){
                    setTextToSelectedClassValues(getCommaSeparatedClassName(selectedClass));
                    setTextToSelectedSubjectValues(getCommaSeparatedSubjectName(selectedClass));
                }else{
                    setTextToSelectedClassValues("");
                    setTextToSelectedSubjectValues("");
                }
                resetValues();
                getHomeworksWithCustomForTeacher(getCommaSeparatedClassIds(selectedClassList) , 0);
            }

            @Override
            public void onUpdatedClasses(ArrayList<ClassWithName> updatedList) {
                classWithNames = updatedList;
            }
        } , param);
    }

    private void setTextToSelectedClassValues(String commaSeparatedClasses){
        txtSelectClassValue.setText(commaSeparatedClasses);
        txtClassValues.setText(commaSeparatedClasses);
    }

    private void setTextToSelectedSubjectValues(String commaSeparatedSubjects){
        txtSubjectValues.setText(commaSeparatedSubjects);
    }

    private String getCommaSeparatedClassName(ArrayList<ClassWithName> selectedClassList){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0 ; i < selectedClassList.size() ; i ++ ){
            stringBuilder.append(stringBuilder.length() > 0 ? "," + selectedClassList.get(i).getClassName()
                    : selectedClassList.get(i).getClassName());
        }
        return stringBuilder.toString();
    }

    private String getCommaSeparatedSubjectName(ArrayList<ClassWithName> selectedClassList){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0 ; i < selectedClassList.size() ; i ++ ){
            stringBuilder.append(stringBuilder.length() > 0 ? "," + selectedClassList.get(i).getSubject()
                    : selectedClassList.get(i).getSubject());
        }
        return stringBuilder.toString();
    }

    private String getCommaSeparatedClassIds(ArrayList<ClassWithName> selectedClassList) {
        try {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < selectedClassList.size(); i++) {
                stringBuilder.append(stringBuilder.length() > 0 ? "," + selectedClassList.get(i).getClassId()
                        : selectedClassList.get(i).getClassId());
            }
            return stringBuilder.toString();
        }catch (Exception e){
            e.printStackTrace();
            return "0";
        }
    }

    private void resetValues(){
        try {
            adapter = null;
            offset = 0;
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    /**
     * This method first check for the work categories, if not found in db then download that
     * and then get the homework data
     */
    private void checkForWordCategoriesAndGetHomeworkData(){
        /*if(CommonUtils.isInternetConnectionAvailable(this)){
            MathFriendzyHelper.downloadAllGradeWordCategories(this , new HttpResponseInterface() {
                @Override
                public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                    getHomeworksWithCustomForTeacher("0" , 1);
                }
            } , true);
        }else{
            this.getHomeworksWithCustomForTeacher("0" , 1);
        }*/
        this.getHomeworksWithCustomForTeacher("0" , 1);
    }

    /**
     * Open Assign HW Screen to Edit HW
     * @param getHomeworksWithCustomForTeacherResponse
     */
    private void openAssignHWScreenToEditHW(GetHomeworksWithCustomForTeacherResponse
                                                    getHomeworksWithCustomForTeacherResponse){
        Intent intent = new Intent(this, ActAssignHomework.class);
        intent.putExtra("isTeacherEditHomework" , true);
        intent.putExtra("TeacherEditHWData" , this.getHomeworkData(getHomeworksWithCustomForTeacherResponse));
        startActivityForResult(intent, EDIT_HOMEWORK_REQUEST_CODE);
    }

    /**
     * Get the homework data
     * @param getHomeworksWithCustomForTeacherResponse
     * @return
     */
    private HomeworkData getHomeworkData(GetHomeworksWithCustomForTeacherResponse
                                                 getHomeworksWithCustomForTeacherResponse){
        HomeworkData homeworkData = new HomeworkData();
        homeworkData.setTeacherId(CommonUtils.getUserId(this));
        homeworkData.setDate(MathFriendzyHelper.
                formatDataInGivenFormat(getHomeworksWithCustomForTeacherResponse.getDate() ,
                        "MM-dd-yyyy" , "yyyy-MM-dd"));
        homeworkData.setGrade(getHomeworksWithCustomForTeacherResponse.getGrade());
        homeworkData.setMessage(getHomeworksWithCustomForTeacherResponse.getMessage());
        homeworkData.setCustomeQuizList(getHomeworksWithCustomForTeacherResponse.getCustomeresultList());
        homeworkData.setPracticeResults(getHomeworksWithCustomForTeacherResponse.getPractiseResultList());
        homeworkData.setWordResults(getHomeworksWithCustomForTeacherResponse.getWordResultList());
        homeworkData.setSavedHwId(MathFriendzyHelper.
                parseInt(getHomeworksWithCustomForTeacherResponse.getHomeworkId()));
        return homeworkData;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            switch(requestCode){
                case EDIT_HOMEWORK_REQUEST_CODE:
                    finish();
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
