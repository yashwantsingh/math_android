package com.mathfriendzy.controller.homework.checkhomeworkquiz;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.homework.assignhomeworkquiz.ActAssignHomework;
import com.mathfriendzy.controller.homework.assignhomeworkquiz.ClassesAdapter;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.OnRequestComplete;
import com.mathfriendzy.model.homework.CustomeAns;
import com.mathfriendzy.model.homework.CustomePlayerAns;
import com.mathfriendzy.model.homework.CustomeResult;
import com.mathfriendzy.model.homework.PracticeResult;
import com.mathfriendzy.model.homework.WordResult;
import com.mathfriendzy.model.homework.assignhomework.GetStudentByGradeResponse;
import com.mathfriendzy.model.homework.checkhomework.GetDetailOfHomeworkWithCustomeResponse;
import com.mathfriendzy.model.homework.checkhomework.GetDetailsOfHomeworkWithCustomParam;
import com.mathfriendzy.model.homework.checkhomework.GetHomeworksWithCustomForTeacherResponse;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.classes.ClassWithName;
import com.mathfriendzy.model.registration.classes.UserClasses;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.MathVersions;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ActCheckHomeworkStudents extends ActBase {

    private final String TAG = this.getClass().getSimpleName();

    private TextView txtGrade = null;
    private TextView txtStudent = null;
    private TextView txtDueDate = null;
    private TextView txtPracticeSkill = null;
    private TextView txtSchoolCurriculum = null;
    private TextView txtCustome = null;
    private TextView txtAvgScore = null;
    private Button btnExport = null;

    private ListView schoolHomeWorkList = null;
    private CheckHomeworkStudentAdapter adapter = null;

    private GetHomeworksWithCustomForTeacherResponse homeworkCustomeObj = null;
    private ArrayList<GetDetailOfHomeworkWithCustomeResponse> responseStudentHWList = null;

    //for update the list for new updated data by teacher check homework
    int selctedStudentIndex = 0;
    //for open the check Homework/Quizzes screen to get the updated result from the previous screen
    private final int CHECK_STUDENT_HW_QUIZZ_REQUEST = 1001;

    //class title changes
    private RegistereUserDto registerUserFromPreff = null;
    private RelativeLayout selectClassLayout = null;
    private TextView txtSelectClass = null;
    private TextView txtSelectClassValue = null;
    private RelativeLayout titleListLayout = null;
    private Button btnClassListDone = null;
    private ListView lstClassesList = null;
    private ClassesAdapter classAdapter = null;

    //for the new homework change
    private int selectedClassId = 0;
    private ClassWithName selectedClass = null;

    private Button btnCreateAssignment = null;
    private ProgressDialog pd  = null;

    private Button btnSortByName = null;
    public Button btnSortByAvgScore = null;

    private RegistereUserDto loginUser = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_check_homework_students);

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside onCreate()");

        this.init();
        this.getIntentValues();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.gethomeworkStudents();
        this.setScoreTextsVisibility();
        //class title changes
        /*try {
            this.setClassTitle(MathFriendzyHelper.parseInt(homeworkCustomeObj.getGrade()));
        }catch (Exception e){
            e.printStackTrace();
        }*/

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside onCreate()");
    }

    private void setScoreTextsVisibility(){
        if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS){
            txtPracticeSkill.setVisibility(View.INVISIBLE);
            txtSchoolCurriculum.setVisibility(View.INVISIBLE);
            txtCustome.setVisibility(View.INVISIBLE);
        }
    }

    private void init() {
        loginUser = CommonUtils.getLoginUser(this);
        registerUserFromPreff = MathFriendzyHelper.getUserFromPreff(this);
    }

    /**
     * Get the intent values which are set in previous screen
     */
    private void getIntentValues() {
        homeworkCustomeObj = (GetHomeworksWithCustomForTeacherResponse)
                this.getIntent().getSerializableExtra("homeworkCustome");
        selectedClassId = this.getIntent().getIntExtra("selectedClassId" , 0);
        try {
            selectedClass = (ClassWithName) this.getIntent().getSerializableExtra("selectedClass");
        }catch (Exception e){
            selectedClass = null;
            e.printStackTrace();
        }
    }

    @Override
    protected void setWidgetsReferences() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside onCreate()");

        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        txtGrade = (TextView) findViewById(R.id.txtGrade);
        txtDueDate = (TextView) findViewById(R.id.txtDueDate);
        txtStudent = (TextView) findViewById(R.id.txtStudent);
        txtPracticeSkill = (TextView) findViewById(R.id.txtPracticeSkill);
        txtSchoolCurriculum = (TextView) findViewById(R.id.txtSchoolCurriculum);
        txtCustome = (TextView) findViewById(R.id.txtCustome);
        txtAvgScore = (TextView) findViewById(R.id.txtAvgScore);
        btnExport = (Button) findViewById(R.id.btnExport);

        schoolHomeWorkList = (ListView) findViewById(R.id.schoolHomeWorkList);


        //class title changes
        selectClassLayout = (RelativeLayout) findViewById(R.id.selectClassLayout);
        txtSelectClass = (TextView) findViewById(R.id.txtSelectClass);
        txtSelectClassValue = (TextView) findViewById(R.id.txtSelectClassValue);
        titleListLayout = (RelativeLayout) findViewById(R.id.titleListLayout);
        btnClassListDone = (Button) findViewById(R.id.btnClassListDone);
        lstClassesList = (ListView) findViewById(R.id.lstClassesList);

        btnCreateAssignment = (Button) findViewById(R.id.btnCreateAssignment);


        btnSortByName = (Button) findViewById(R.id.btnSortByName);
        btnSortByAvgScore = (Button) findViewById(R.id.btnSortByAvgScore);

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside onCreate()");
    }



    @Override
    protected void setTextFromTranslation() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside onCreate()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblCheckHomework")
                + "/" + transeletion.getTranselationTextByTextIdentifier("lblQuizzes"));
        txtGrade.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade")
                + "\n" + homeworkCustomeObj.getGrade());
        txtDueDate.setText(transeletion.getTranselationTextByTextIdentifier("lblDueDate")
                +"\n"+ homeworkCustomeObj.getDate());
        txtStudent.setText(transeletion.getTranselationTextByTextIdentifier("lblStudent"));
        txtPracticeSkill.setText(transeletion.getTranselationTextByTextIdentifier("lblSolveEquations"));

        txtSchoolCurriculum.setText(transeletion.getTranselationTextByTextIdentifier("lblWordProblem"));

        txtCustome.setText(transeletion.getTranselationTextByTextIdentifier("lblCustom"));

        txtAvgScore.setText(transeletion.getTranselationTextByTextIdentifier("lblAverage")
                + "\n" + transeletion.getTranselationTextByTextIdentifier("mfBtnTitleScore"));
        btnExport.setText(transeletion.getTranselationTextByTextIdentifier("lblExport"));

        //class title changes
        txtSelectClass.setText(transeletion.getTranselationTextByTextIdentifier("lblClass"));
        btnClassListDone.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleDone"));
        txtSelectClassValue.setHint(transeletion.getTranselationTextByTextIdentifier("lblClass")
                + " " + transeletion.getTranselationTextByTextIdentifier("lblClass"));

        btnCreateAssignment.setText(transeletion.getTranselationTextByTextIdentifier("lblAssignHomework"));
        transeletion.closeConnection();

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside onCreate()");

    }

    @Override
    protected void setListenerOnWidgets() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside onCreate()");

        btnExport.setOnClickListener(this);

        //class title changes
        selectClassLayout.setOnClickListener(this);
        btnClassListDone.setOnClickListener(this);

        btnCreateAssignment.setOnClickListener(this);
        btnSortByName.setOnClickListener(this);
        btnSortByAvgScore.setOnClickListener(this);

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside onCreate()");

    }

    /**
     * Get detail for homwwork
     */
    private void gethomeworkStudents() {
        try{
            pd = MathFriendzyHelper.getProgressDialog(this , getString(R.string.please_wait_dialog_msg));
            MathFriendzyHelper.showProgressDialog(pd);
            GetDetailsOfHomeworkWithCustomParam param = new GetDetailsOfHomeworkWithCustomParam();
            param.setAction("getDetailsOfHomeworkWithCustom");
            param.setHWId(homeworkCustomeObj.getHomeworkId());
            param.setSelectedClassId(selectedClassId);
            if(CommonUtils.isInternetConnectionAvailable(this)){
                new MyAsyckTask(ServerOperation.createPostRequestForGetDetailsOfHomeworkWithCustom(param)
                        , null, ServerOperationUtil.GET_DETAIL_STUDENT_HOME_WORK_REQUEST, this,
                        this, ServerOperationUtil.SIMPLE_DIALOG , false ,
                        getString(R.string.please_wait_dialog_msg))
                        .execute();
            }else{
                CommonUtils.showInternetDialog(this);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Set the list adapter
     */
    private void setAdapter(){
        try{
            //responseStudentHWList initialize after get result form server , in serverResponse()
            adapter = new CheckHomeworkStudentAdapter
                    (this, responseStudentHWList);
            schoolHomeWorkList.setAdapter(adapter);

            schoolHomeWorkList.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    //for selected student index
                    selctedStudentIndex = position;

                    adapter.filterList.get(position).
                            setGrade(homeworkCustomeObj.getGrade());
                    adapter.filterList.get(position)
                            .setDueDate(homeworkCustomeObj.getDate());
                    adapter.filterList.get(position)
                            .setHomeWorkId(homeworkCustomeObj.getHomeworkId());

                    Intent intent = new Intent(ActCheckHomeworkStudents.this ,
                            ActCheckStudentHWQuiz.class);
                    intent.putExtra("studentHWDetail",
                            adapter.filterList.get(position));
                    startActivityForResult(intent , CHECK_STUDENT_HW_QUIZZ_REQUEST);
                }
            });
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnExport:
                //MathFriendzyHelper.showWarningDialog(this , "Under Development!!!");
                try {
                    if (adapter != null) {
                        this.exportStudents(adapter.filterList, this.getTeacherDetailToExport());
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            case R.id.selectClassLayout:
                this.clickToSelectClass();
                break;
            case R.id.btnClassListDone:
                this.clickToClassSelectionDone();
                break;
            case R.id.btnCreateAssignment:
                this.clickToCreateAssignment();
                break;
            case R.id.btnSortByName:
                this.sortStudentList(CheckHomeworkStudentAdapter.SORT_BY_NAME);
                break;
            case R.id.btnSortByAvgScore:
                this.sortStudentList(CheckHomeworkStudentAdapter.SORT_BY_AVG_SCORE);
                break;

        }
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase,
                               int requestCode) {
        if(requestCode == ServerOperationUtil.GET_DETAIL_STUDENT_HOME_WORK_REQUEST){
            GetDetailOfHomeworkWithCustomeResponse response =
                    (GetDetailOfHomeworkWithCustomeResponse) httpResponseBase;
            responseStudentHWList = response.getResposeList();
            new AsyncTask<Void,Void,Void>(){

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected Void doInBackground(Void... params) {
                    setCorrectAndWrongAnsCounter();
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    MathFriendzyHelper.hideProgressDialog(pd);
                    setAdapter();
                }
            }.execute();
            //this.setCorrectAndWrongAnsCounter();
        }
    }

    private void setCorrectAndWrongAnsCounter(){
        if(responseStudentHWList != null && responseStudentHWList.size() > 0){
            for(int i = 0 ; i < responseStudentHWList.size() ; i ++ ){
                ArrayList<CustomeResult> customeResultList = responseStudentHWList.get(i).getCustomeresultList();
                for(int j = 0 ; j < customeResultList.size() ; j ++ ){
                    CustomeResult customeResult = customeResultList.get(j);
                    ArrayList<CustomeAns> playerAnsList = customeResult.getCustomeAnsList();
                    for(int k = 0 ; k < playerAnsList.size() ; k ++ ){
                        CustomeAns customeAns = playerAnsList.get(k);
                        this.setCorrectAndWrongAnsCounter(customeAns ,
                                customeResult.getCustomHwId() ,customeAns.getQueNo());
                    }
                }
            }
        }
    }

    private void setCorrectAndWrongAnsCounter(CustomeAns customeAns , int cusHWId , String quesNo){
        if(responseStudentHWList != null && responseStudentHWList.size() > 0) {
            int correctAndCounter = 0;
            int wrongAndCounter = 0;
            for (int i = 0; i < responseStudentHWList.size(); i++) {
                ArrayList<CustomeResult> customeResultList = responseStudentHWList.get(i).getCustomeresultList();
                for(int j = 0 ; j < customeResultList.size() ; j ++ ){
                    if(customeResultList.get(j).getCustomHwId() == cusHWId){
                        CustomeResult customeResult = customeResultList.get(j);
                        ArrayList<CustomePlayerAns> playerAnsList = customeResult.getCustomePlayerAnsList();
                        for(int k = 0 ; k < playerAnsList.size() ; k ++ ){
                            CustomePlayerAns playerAns = playerAnsList.get(k);
                            if(playerAns.getQueNo().equalsIgnoreCase(quesNo)){
                                if(playerAns.getIsCorrect() == MathFriendzyHelper.YES
                                        && playerAns.getIsGetHelp() == MathFriendzyHelper.NO
                                        && playerAns.getFirstTimeWrong() == MathFriendzyHelper.NO){
                                    correctAndCounter ++ ;
                                }else{
                                    wrongAndCounter ++ ;
                                }
                                break;
                            }
                        }
                        break;
                    }
                }
            }
            customeAns.setCorrectAnsCounter(correctAndCounter);
            customeAns.setWrongAnsCounter(wrongAndCounter);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode ==  RESULT_OK){
            switch(requestCode){
                case CHECK_STUDENT_HW_QUIZZ_REQUEST:
                    try{
                        GetDetailOfHomeworkWithCustomeResponse studentDetail =
                                (GetDetailOfHomeworkWithCustomeResponse)
                                        data.getSerializableExtra("studentDetail");
                        adapter.filterList.set(selctedStudentIndex, studentDetail);
                        adapter.notifyDataSetChanged();
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    //below class title changes
    private void setClassTitle(int selectedGrade){
        this.setVisibilityOfTitleListLayout(false);
        ArrayList<ClassWithName> classWithNames = this.getClassTitleList(selectedGrade);
        this.clearDataWhenUserSelectOtherGrade(classWithNames);
        if(classWithNames != null && classWithNames.size() > 0){
            this.setVisibilityOfClassesLayout(true);
            this.setClassListAdapter(classWithNames);
        }else{
            this.setVisibilityOfClassesLayout(false);
        }
    }

    private void clearDataWhenUserSelectOtherGrade
            (ArrayList<ClassWithName> classWithNames){
        classAdapter = null;
        txtSelectClassValue.setText("");
        if(classWithNames != null && classWithNames.size() > 0) {
            for (int i = 0; i < classWithNames.size(); i++) {
                classWithNames.get(i).setSelected(false);
            }
        }
    }

    private ArrayList<ClassWithName> getClassTitleList(int grade){
        if(registerUserFromPreff != null){
            ArrayList<UserClasses> userClasseses = registerUserFromPreff.getUserClasses();
            for (int i = 0 ; i < userClasseses.size() ; i ++ ){
                UserClasses userClass = userClasseses.get(i);
                if(userClass.getGrade() == grade){
                    return userClass.getClassList();
                }
            }
            return null;
        }else{
            return null;
        }
    }

    private void setVisibilityOfClassesLayout(boolean isVisible){
        if(isVisible){
            selectClassLayout.setVisibility(RelativeLayout.VISIBLE);
        }else{
            selectClassLayout.setVisibility(RelativeLayout.GONE);
        }
    }

    private void setVisibilityOfTitleListLayout(boolean isVisible){
        if(isVisible){
            titleListLayout.setVisibility(RelativeLayout.VISIBLE);
            lstClassesList.setVisibility(ListView.VISIBLE);
        }else{
            titleListLayout.setVisibility(RelativeLayout.GONE);
            lstClassesList.setVisibility(ListView.GONE);
        }
    }

    private void clickToSelectClass() {
        this.setVisibilityOfTitleListLayout(true);
    }

    private void clickToClassSelectionDone() {
        txtSelectClassValue.setText("");
        this.setVisibilityOfTitleListLayout(false);
        ArrayList<ClassWithName> selectedClassList = this.getSelectedClasses();
        if(selectedClassList != null && selectedClassList.size() > 0) {
            txtSelectClassValue.setText(this.getCommaSeparatedClassName(selectedClassList));
            try {
                if (adapter != null)
                    adapter.filterListBasedOnClassId(selectedClassList);
            }catch (Exception e){
                e.printStackTrace();
            }
        }else{
            try {
                if (adapter != null)
                    adapter.filterListBasedOnClassId(null);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private ArrayList<ClassWithName> getSelectedClasses(){
        if(classAdapter != null && classAdapter.getSelectedList().size() > 0){
            return classAdapter.getSelectedList();
        }
        return null;
    }

    private void setClassListAdapter(ArrayList<ClassWithName> classWithNames){
        classAdapter = new ClassesAdapter(this , classWithNames);
        lstClassesList.setAdapter(classAdapter);
    }

    private String getCommaSeparatedClassName(ArrayList<ClassWithName> selectedClassList){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0 ; i < selectedClassList.size() ; i ++ ){
            stringBuilder.append(stringBuilder.length() > 0 ? "," + selectedClassList.get(i).getClassName()
                    : selectedClassList.get(i).getClassName());
        }
        return stringBuilder.toString();
    }
    //end class title changes

    /**
     * Create the assignment
     */
    private void clickToCreateAssignment() {
        try {
            if (adapter != null) {
                ArrayList<GetStudentByGradeResponse> selectedPlayerList =
                        adapter.getSelectedPlayerList(selectedClassId);
                if(selectedPlayerList != null && selectedPlayerList.size() > 0) {
                    Intent intent = new Intent(this, ActAssignHomework.class);
                    intent.putExtra("isTeacherCreateAssignmentFromCheckHW", true);
                    intent.putExtra("selectedStudentList", selectedPlayerList);
                    intent.putExtra("selectedClass", selectedClass);
                    startActivity(intent);
                }else{
                    MathFriendzyHelper.showWarningDialog(this , MathFriendzyHelper
                            .getTreanslationTextById(this , "lblTapOnNameOfOneOrMoreStudents"));
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void sortStudentList(int tag){
        try {
            if (adapter != null) {
                adapter.sortList(tag);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //export students
    private TeacherDetailToExport getTeacherDetailToExport(){
        TeacherDetailToExport teacherDetailToExport = new TeacherDetailToExport();
        teacherDetailToExport.setTeacherName(loginUser.getFirstName()
                + " " + loginUser.getLastName());
        if(adapter != null && adapter.filterList.size() > 0){
            teacherDetailToExport.setSubjects(adapter.filterList.get(0).getSubjectName());
            teacherDetailToExport.setClasses(adapter.filterList.get(0).getClassName());
        }else {
            teacherDetailToExport.setSubjects("");
            teacherDetailToExport.setClasses("");
        }
        teacherDetailToExport.setDueDate(homeworkCustomeObj.getDate());
        this.setScoreVisibilityValue(teacherDetailToExport);
        return teacherDetailToExport;
    }

    private void setScoreVisibilityValue(TeacherDetailToExport teacherDetailToExport){
        try {
            ArrayList<CustomeResult> customeQuizList = homeworkCustomeObj.getCustomeresultList();
            ArrayList<PracticeResult> practiceList = homeworkCustomeObj.getPractiseResultList();
            ArrayList<WordResult> workProblemList = homeworkCustomeObj.getWordResultList();
            if (customeQuizList != null && customeQuizList.size() > 0) {
                teacherDetailToExport.setCustom(true);
            }
            if (practiceList != null && practiceList.size() > 0) {
                teacherDetailToExport.setPractice(true);
            }
            if (workProblemList != null && workProblemList.size() > 0) {
                teacherDetailToExport.setWordProblem(true);
            }
        }catch (Exception e){
            e.printStackTrace();
            teacherDetailToExport.setCustom(true);
            teacherDetailToExport.setPractice(true);
            teacherDetailToExport.setWordProblem(true);
        }
    }

    private void exportStudents(ArrayList<GetDetailOfHomeworkWithCustomeResponse> studentList
     , TeacherDetailToExport teacherDetailToExport){
        try {
            final ProgressDialog pd = MathFriendzyHelper.getProgressDialog(this , "");
            MathFriendzyHelper.showProgressDialog(pd);
            MathFriendzyHelper.createStudentsCSVFile(MathFriendzyHelper.STUDENT_CSV_FILE_NAME
                    , new OnRequestComplete() {
                @Override
                public void onComplete() {
                    MathFriendzyHelper.hideProgressDialog(pd);
                    sendEmailWithCSVFile(MathFriendzyHelper
                            .getFullPathOfCSVFile(MathFriendzyHelper.STUDENT_CSV_FILE_NAME));
                }
            } , studentList , teacherDetailToExport);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendEmailWithCSVFile(String fileUrl){
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "");
        sendIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(fileUrl)));
        sendIntent.setType("text/html");
        final PackageManager pm = this.getPackageManager();
        final List<ResolveInfo> matches = pm.queryIntentActivities(sendIntent, 0);
        ResolveInfo best = null;
        for(final ResolveInfo info : matches)
            if (info.activityInfo.packageName.endsWith(".gm")
                    || info.activityInfo.name.toLowerCase().contains("gmail"))
                best = info;
        if (best != null)
            sendIntent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
        startActivity(sendIntent);
    }
}
