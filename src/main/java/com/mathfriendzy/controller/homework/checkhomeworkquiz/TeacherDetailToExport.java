package com.mathfriendzy.controller.homework.checkhomeworkquiz;

/**
 * Created by root on 26/8/16.
 */
public class TeacherDetailToExport {
    private String teacherName;
    private String subjects;
    private String classes;
    private String dueDate;
    private boolean isPractice;
    private boolean isWordProblem;
    private boolean isCustom;

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getSubjects() {
        return subjects;
    }

    public void setSubjects(String subjects) {
        this.subjects = subjects;
    }

    public String getClasses() {
        return classes;
    }

    public void setClasses(String classes) {
        this.classes = classes;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public boolean isPractice() {
        return isPractice;
    }

    public void setPractice(boolean isPractice) {
        this.isPractice = isPractice;
    }

    public boolean isWordProblem() {
        return isWordProblem;
    }

    public void setWordProblem(boolean isWordProblem) {
        this.isWordProblem = isWordProblem;
    }

    public boolean isCustom() {
        return isCustom;
    }

    public void setCustom(boolean isCustom) {
        this.isCustom = isCustom;
    }
}
