package com.mathfriendzy.controller.homework.checkhomeworkquiz;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.model.homework.assignhomework.GetStudentByGradeResponse;
import com.mathfriendzy.model.homework.checkhomework.GetDetailOfHomeworkWithCustomeResponse;
import com.mathfriendzy.model.registration.classes.ClassWithName;
import com.mathfriendzy.soringclasses.GetStudentByGradeResponseSortByAvgSc;
import com.mathfriendzy.soringclasses.GetStudentByGradeResponseSortByName;
import com.mathfriendzy.utils.MathVersions;

import java.util.ArrayList;
import java.util.Collections;

public class CheckHomeworkStudentAdapter extends BaseAdapter{

    public ArrayList<GetDetailOfHomeworkWithCustomeResponse> filterList = null;
    private ArrayList<GetDetailOfHomeworkWithCustomeResponse> originalList = null;
    public static int SORT_BY_NAME = 1;
    public static int SORT_BY_AVG_SCORE = 2;

    private LayoutInflater mInflator = null;
    private ViewHolder viewHolder 	 = null;
    private Context context = null;
    private boolean isSortByNameInAscending = true;
    private boolean isSortByAvgScInAscending = false;

    public CheckHomeworkStudentAdapter(Context context ,
                                       ArrayList<GetDetailOfHomeworkWithCustomeResponse> responseStudentHWList){
        this.filterList = responseStudentHWList;
        this.originalList = responseStudentHWList;
        mInflator = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return filterList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        if(view == null){
            view = mInflator.inflate(R.layout.school_home_work_list_layout, null);
            viewHolder = new ViewHolder();
            viewHolder.layout = (RelativeLayout) view.findViewById(R.id.layout);
            viewHolder.txtDueDate = (TextView) view.findViewById(R.id.txtDueDate);
            viewHolder.txtPracticeSkill = (TextView) view.findViewById(R.id.txtPracticeSkill);
            viewHolder.txtSchoolCurriculum = (TextView) view.findViewById(R.id.txtSchoolCurriculum);
            viewHolder.txtCustome = (TextView) view.findViewById(R.id.txtCustome);
            viewHolder.txtAvgScore = (TextView) view.findViewById(R.id.txtAvgScore);
            view.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.txtDueDate.setText((position + 1)
                + ". " + filterList.get(position).getfName() + " " + filterList.get(position).getlName());
        this.setScore(filterList.get(position).getPracticeScore(), viewHolder.txtPracticeSkill);
        this.setScore(filterList.get(position).getWordScore(), viewHolder.txtSchoolCurriculum);
        this.setScore(filterList.get(position).getCustomeScore(), viewHolder.txtCustome);
        this.setScore(filterList.get(position).getAvgScore(), viewHolder.txtAvgScore);
        this.setScoreTextVisibility();
        this.setBackGroundVisibility(viewHolder , filterList.get(position));

        viewHolder.txtDueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterList.get(position).setSelected(!filterList.get(position).isSelected());
                CheckHomeworkStudentAdapter.this.notifyDataSetChanged();
            }
        });
        return view;
    }

    private void setBackGroundVisibility(ViewHolder viewHolder , GetDetailOfHomeworkWithCustomeResponse data){
        if(data.isSelected()){
            viewHolder.layout.setBackgroundColor(context.getResources().getColor(R.color.GREEN));
        }else{
            viewHolder.layout.setBackgroundColor(context.getResources().getColor(R.color.WHITE));
        }
    }

    private void setScoreTextVisibility(){
        if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS){
            viewHolder.txtPracticeSkill.setVisibility(View.INVISIBLE);
            viewHolder.txtSchoolCurriculum.setVisibility(View.INVISIBLE);
            viewHolder.txtCustome.setVisibility(View.INVISIBLE);
        }
    }
    private void setScore(String score , TextView txtView){
        if(score != null && score.length() > 0 /*&& !score.equals("0")*/){
            txtView.setText(score + "%");
        }else{
            txtView.setText("-");
        }
    }

    private class ViewHolder{
        private RelativeLayout layout;
        private TextView txtDueDate;
        private TextView txtPracticeSkill;
        private TextView txtSchoolCurriculum;
        private TextView txtCustome;
        private TextView txtAvgScore;
    }

    public void filterListBasedOnClassId(ArrayList<ClassWithName> selectedClassList){
        filterList = new ArrayList<GetDetailOfHomeworkWithCustomeResponse>();
        if(selectedClassList == null){
            filterList = this.originalList;
        }else {
            for (int i = 0; i < this.originalList.size(); i++) {
                if (this.ifStudentExistForSelectedClass(selectedClassList
                        , this.originalList.get(i).getClassId())) {
                    filterList.add(originalList.get(i));
                }
            }
        }
        this.notifyDataSetChanged();
    }

    private boolean ifStudentExistForSelectedClass(ArrayList<ClassWithName> selectedClassList , int classId){
        if(selectedClassList != null && selectedClassList.size() > 0){
            for(int i = 0 ; i < selectedClassList.size() ; i ++ ){
                if(selectedClassList.get(i).getClassId() == classId){
                    return true;
                }
            }
        }
        return false;
    }

    public ArrayList<GetStudentByGradeResponse> getSelectedPlayerList(int selectedClassId){
        ArrayList<GetStudentByGradeResponse> selectedPlayerList = new ArrayList<GetStudentByGradeResponse>();
        if(filterList != null && filterList.size() > 0){
            for(int i = 0 ; i < filterList.size() ; i ++ ){
                if(filterList.get(i).isSelected()){
                    GetStudentByGradeResponse player = new GetStudentByGradeResponse();
                    player.setPlayerId(filterList.get(i).getPlayerId());
                    player.setParentUserId(filterList.get(i).getParentId());
                    player.setfName(filterList.get(i).getfName());
                    player.setlName(filterList.get(i).getlName());
                    player.setSubjectId(filterList.get(i).getSubjectId());
                    player.setClassId(selectedClassId);
                    player.setSelected(true);
                    selectedPlayerList.add(player);
                }
            }
        }
        return selectedPlayerList;
    }

    public void sortList(int tag){
        if(tag == SORT_BY_NAME){
            isSortByNameInAscending = !isSortByNameInAscending;
            Collections.sort(filterList , new GetStudentByGradeResponseSortByName(isSortByNameInAscending));
        }else if(tag == SORT_BY_AVG_SCORE){
            isSortByAvgScInAscending = !isSortByAvgScInAscending;
            Collections.sort(filterList , new GetStudentByGradeResponseSortByAvgSc(isSortByAvgScInAscending));
        }
        this.notifyDataSetChanged();
    }
}
