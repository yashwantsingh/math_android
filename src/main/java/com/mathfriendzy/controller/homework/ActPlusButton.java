package com.mathfriendzy.controller.homework;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.homework.homwworkworkarea.AddUrlToWorkArea;
import com.mathfriendzy.controller.homework.homwworkworkarea.HomeWorkWorkArea;
import com.mathfriendzy.controller.tutor.ActFindTutor;
import com.mathfriendzy.customview.EditTextBlink;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.helper.YesNoListenerInterface;
import com.mathfriendzy.model.homework.CustomeAns;
import com.mathfriendzy.model.homework.CustomePlayerAns;
import com.mathfriendzy.model.homework.CustomeResult;
import com.mathfriendzy.model.homework.GetHomeWorkForStudentWithCustomeResponse;
import com.mathfriendzy.model.homework.MultipleChoiceAnswer;
import com.mathfriendzy.model.homework.secondworkarea.AddDuplicateWorkAreaParam;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.tutor.FindTutorResponse;
import com.mathfriendzy.model.tutor.GetChatRequestResponse;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;

import java.util.ArrayList;

public class ActPlusButton extends ActBase {

    private TextView txtTeacherName = null;
    private TextView txtGradeAndDueData = null;
    private TextView txtWorkTitle = null;
    private TextView txtCorrectAnsweris = null;
    private LinearLayout layout = null;

    private String teacherName = null;
    private String grade = null;
    private String dueDate = null;
    private CustomeResult customeResult = null;
    private GetHomeWorkForStudentWithCustomeResponse homeWorkQuizzData = null;
    private CustomeAns customeAns = null;

    //private boolean isExpireQuizz = false;

    private final String TAG = this.getClass().getSimpleName();

    private Button btnGetHelp = null;
    private Button btnWorkArea = null;

    private UserPlayerDto selectedPlayerData = null;

    private CustomePlayerAns playerAns = null;
    private int index = 0;

    private String teacherStudentMsg = null;
    private final int START_WORK_AREA_REQUEST_CODE = 1001;
    private final int START_GET_HELP_REQUEST = 1002;

    private int workareapublic = 1;
    private int isGetHelp = 0;

    private boolean isImageSaved = false;
    private String saveFileName = "";

    //for question image name
    private String cropImageUriFromPdf = "";
    private boolean isClickOnPaste = false;
    private boolean isDeleteImage = false;
    private String questionImageName = "";
    private String playerAndQuestionImage = "";

    //Contains the name list of the downloaded images on this screen
    private ArrayList<String> downloadImageList = null;

    //for tutoring project change
    private TextView txtTeacherNameValue = null;
    private TextView txtGradeAndDueDataValue = null;//only for grade
    private TextView txtDueDate = null;
    private TextView txtDueDateValue = null;
    private TextView txtNeedHelpWithThisProblem = null;
    private Button btnSeeAnswer = null;
    private RelativeLayout seeCorrectAnswerLayout = null;
    private Button btnSeeHowOtherDoIt = null;
    private Button btnConnectWithATutor = null;
    private ScrollView scrollLayout = null;

    private boolean hasSeenAnswer = false;

    //for adding tab in work area
    private boolean isTutorWorkArea = false;
    private final int FIND_TUTOR_REQUEST = 1003;
    private FindTutorResponse findTutorDetail = null;
    private int chatRequestId = 0;
    //private static ActPlusButton currentObj = null;

    private boolean isWorkInputSeen = false;
    private boolean isOpponentInputSeen = false;

    private int isFirstTimeWrong = 0;

    private String lblTeacherNotGivenAnswer = null;


    private String lblThisWillDeleteSession, btnTitleYes, btnTitleNoThanks;

    private ArrayList<AddUrlToWorkArea> urlToWorkAreaArrayList = null;


    //for student input on workarea
    private boolean isAnsUpdated = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_plus_button);

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside onCreate()");

        selectedPlayerData = this.getPlayerData();

        //this.initializeCurrentObj();
        this.getIntentValues();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.setCustomeDataToListLayout();
        this.setVisibilityOfConnectWithTutorButton();

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "outside onCreate()");
    }

    private void setVisibilityOfConnectWithTutorButton() {
        if(MathFriendzyHelper.getUserAccountType(this) == MathFriendzyHelper.TEACHER){
            btnConnectWithATutor.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * Get set intent values
     */
    private void getIntentValues() {
        teacherName = this.getIntent().getStringExtra("teacherName");
        grade = this.getIntent().getStringExtra("grade");
        dueDate = this.getIntent().getStringExtra("dueDate");
        customeResult = (CustomeResult) this.getIntent().getSerializableExtra("customeDetail");
        homeWorkQuizzData = (GetHomeWorkForStudentWithCustomeResponse)
                this.getIntent().getSerializableExtra("homeWorkQuizzData");
        //isExpireQuizz = this.getIntent().getBooleanExtra("isExpireQuizz", false);
        customeAns = (CustomeAns) this.getIntent().getSerializableExtra("customeAns");
        index = this.getIntent().getIntExtra("index", 0);
        playerAns = (CustomePlayerAns) this.getIntent().getSerializableExtra("playerAns");
        teacherStudentMsg = this.getIntent().getStringExtra("playerMsg");
        workareapublic = this.getIntent().getIntExtra("workareapublic", 1);
        //isGetHelp	   = this.getIntent().getIntExtra("isGetHelp", 0);
        //for question image
        cropImageUriFromPdf = this.getIntent().getStringExtra("cropImageUriFromPdf");
        playerAndQuestionImage = this.getIntent().getStringExtra("playerAndQuestionImage");


        //for downloaded images
        downloadImageList = this.getIntent().getStringArrayListExtra("downloadImageList");
        chatRequestId = this.getIntent().getIntExtra("chatRequestId", 0);

        try {
            urlToWorkAreaArrayList = playerAns.getUrlLinks();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void setWidgetsReferences() {
        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        txtTeacherName = (TextView) findViewById(R.id.txtTeacherName);
        txtGradeAndDueData = (TextView) findViewById(R.id.txtGradeAndDueData);
        txtWorkTitle = (TextView) findViewById(R.id.txtWorkTitle);
        txtCorrectAnsweris = (TextView) findViewById(R.id.txtCorrectAnsweris);
        btnGetHelp = (Button) findViewById(R.id.btnGetHelp);
        btnWorkArea = (Button) findViewById(R.id.btnWorkArea);
        layout = (LinearLayout) findViewById(R.id.checkHomeWorkListLayout);


        //for tutoring project changes
        txtTeacherNameValue = (TextView) findViewById(R.id.txtTeacherNameValue);
        txtGradeAndDueDataValue = (TextView) findViewById(R.id.txtGradeAndDueDataValue);
        txtDueDate = (TextView) findViewById(R.id.txtDueDate);
        txtDueDateValue = (TextView) findViewById(R.id.txtDueDateValue);
        txtNeedHelpWithThisProblem = (TextView) findViewById(R.id.txtNeedHelpWithThisProblem);
        btnSeeAnswer = (Button) findViewById(R.id.btnSeeAnswer);
        btnSeeHowOtherDoIt = (Button) findViewById(R.id.btnSeeHowOtherDoIt);
        btnConnectWithATutor = (Button) findViewById(R.id.btnConnectWithATutor);
        seeCorrectAnswerLayout = (RelativeLayout) findViewById(R.id.seeCorrectAnswerLayout);
        scrollLayout = (ScrollView) findViewById(R.id.scrollLayout);
    }

    @Override
    protected void setListenerOnWidgets() {
        btnGetHelp.setOnClickListener(this);
        btnWorkArea.setOnClickListener(this);

        btnSeeAnswer.setOnClickListener(this);
        btnSeeHowOtherDoIt.setOnClickListener(this);
        btnConnectWithATutor.setOnClickListener(this);
    }

    @Override
    protected void setTextFromTranslation() {
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblCheckHomework") + "/" +
                transeletion.getTranselationTextByTextIdentifier("lblQuizzes"));

        txtCorrectAnsweris.setText(transeletion.getTranselationTextByTextIdentifier("lblTheCorrectAnswerForProblem"));
        txtTeacherName.setText(transeletion.getTranselationTextByTextIdentifier("mfLblTeacherName"));
        txtTeacherNameValue.setText(teacherName);
        txtGradeAndDueData.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade"));
        txtGradeAndDueDataValue.setText(grade + "");
        txtWorkTitle.setText(customeResult.getTitle());
        txtDueDate.setText(transeletion.getTranselationTextByTextIdentifier("lblDueDate"));
        txtDueDateValue.setText(dueDate + "");
        txtNeedHelpWithThisProblem.setText(transeletion.getTranselationTextByTextIdentifier("lblNeedHelpWithProblem"));
        btnSeeAnswer.setText(transeletion.getTranselationTextByTextIdentifier("lblSeeCorrectAnswer"));
        btnSeeHowOtherDoIt.setText(transeletion.getTranselationTextByTextIdentifier("lblSeeHowOthersDidIt"));
        btnConnectWithATutor.setText(transeletion.getTranselationTextByTextIdentifier("lblConnectWithTutor"));
        lblTeacherNotGivenAnswer = transeletion.getTranselationTextByTextIdentifier("lblTeacherNotGivenAnswer");

        lblThisWillDeleteSession = transeletion.getTranselationTextByTextIdentifier("lblThisWillDeleteSession");
        btnTitleYes = transeletion.getTranselationTextByTextIdentifier("btnTitleYes");
        btnTitleNoThanks = transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks");
        transeletion.closeConnection();
    }

    /**
     * Set correct result data
     */
    private void setCustomeDataToListLayout() {
        layout.removeAllViews();
        if (customeAns.getFillInType() == 1) {
            layout.addView(this.getCheckFillInLayout(customeAns, 0));
        } else if (customeAns.getAnswerType() == 1) {//for multiple choice
            layout.addView(this.getCheckMultiPleChoiceLayout(customeAns, 0));
        } else if (customeAns.getAnswerType() == 2) {//for true/false
            layout.addView(this.getCheckTrueFalseLayout(customeAns, 0));
        } else if (customeAns.getAnswerType() == 3) {//for yes/no
            layout.addView(this.getCheckTrueFalseLayout(customeAns, 0));
        }
    }

    /**
     * Inflate the layout for the fill in type
     *
     * @param customeAns
     * @return
     */
    private View getCheckFillInLayout(final CustomeAns customeAns, final int index) {
        View view = LayoutInflater.from(this).inflate(R.layout.kb_check_home_fill_in_blank_layout, null);
        @SuppressWarnings("unused")
        RelativeLayout viewLayout = (RelativeLayout) view.findViewById(R.id.checkHomeLayout);
        Button btnSeeAnswer = (Button) view.findViewById(R.id.btnSeeAnswer);
        TextView txtNumber = (TextView) view.findViewById(R.id.txtNumber);
        TextView txtLeftUnit = (TextView) view.findViewById(R.id.txtLeftUnit);
        //  TextView ansBox = (TextView) view.findViewById(R.id.ansBox);
        TextView txtRighttUnit = (TextView) view.findViewById(R.id.txtRighttUnit);
        final Button btnRoughWork = (Button) view.findViewById(R.id.btnRoughWork);
        final LinearLayout ll_ans_box = (LinearLayout) view.findViewById(R.id.ll_ans_box);
        RelativeLayout rlQuestionLayout = (RelativeLayout) view.findViewById(R.id.rlQuestionLayout);
        rlQuestionLayout.setVisibility(View.GONE);

        btnSeeAnswer.setVisibility(Button.INVISIBLE);
        btnRoughWork.setVisibility(Button.INVISIBLE);

        txtNumber.setText(customeAns.getQueNo());
        if (customeAns.getIsLeftUnit() == 0) {
            txtLeftUnit.setText("");
            txtRighttUnit.setText(customeAns.getAnsSuffix());
        } else {
            txtLeftUnit.setText(customeAns.getAnsSuffix());
            txtRighttUnit.setText("");
        }
        //  ansBox.setText(customeAns.getCorrectAns()); // siddhiinfosoft
        this.setstudentansbox(ll_ans_box, customeAns.getCorrectAns());

        TextView edtQuestion = (TextView) view.findViewById(R.id.edtQuestion);
        edtQuestion.setVisibility(View.GONE);
        return view;
    }

    //siddhiinfosoft

    private String replacesignfromchar(final String str, final EditTextBlink ed) {
        String str_final = str;

        try {

            String str_plus = "" + Html.fromHtml("&#43;");
            String str_minus = "" + Html.fromHtml("&#8722;");
            String str_equal = "" + Html.fromHtml("&#61;");
            String str_multi = "" + Html.fromHtml("&#215;");
            String str_divide = "" + Html.fromHtml("&#247;");
            String str_union = "" + Html.fromHtml("&#8746;");
            String str_intersection = "" + Html.fromHtml("&#8745;");
            String str_pi = "" + Html.fromHtml("&#960;");
            String str_factorial = "" + Html.fromHtml("&#33;");
            String str_percentage = "" + Html.fromHtml("&#37;");
            String str_goe = "" + Html.fromHtml("&#8805;");
            String str_loe = "" + Html.fromHtml("&#8804;");
            String str_grthan = "" + Html.fromHtml("&#62;");
            String str_lethan = "" + Html.fromHtml("&#60;");
            String str_infinity = "" + Html.fromHtml("&#8734;");
            String str_degree = "" + Html.fromHtml("&#176;");
            String str_xbar = "" + Html.fromHtml("x&#x0304;");
            String str_muxbar = "" + Html.fromHtml("&#956;") + "" + Html.fromHtml("x&#772;");
            String str_sigmaxbar = "" + Html.fromHtml("&#963;") + "" + Html.fromHtml("x&#772;");

            //Added Siddhiinfosoft
            String str_integral = "" + Html.fromHtml("&#8747;");
            String str_summation = "" + Html.fromHtml("&#8721;");
            String str_alpha = "" + Html.fromHtml("&#945;");
            String str_theta = "" + Html.fromHtml("ø");
            String str_mu = "" + Html.fromHtml("&#956;");
            String str_sigma = "" + Html.fromHtml("&#963;");

            String str_beta = "" + Html.fromHtml("&#946;");
            String str_angle = "" + Html.fromHtml("&#8736;");
            String str_mangle = "" + Html.fromHtml("&#8737;");
            String str_sangle = "" + Html.fromHtml("&#8738;");
            String str_rangle = "" + Html.fromHtml("&#8735;");
            String str_triangle = "" + Html.fromHtml("&#9651;");
            String str_rectangle = "" + Html.fromHtml("&#9645;");
            String str_parallelogram = "" + Html.fromHtml("&#9649;");
            String str_perpendicular = "" + Html.fromHtml("&#8869;");
            String str_congruent = "" + Html.fromHtml("&#8773;");
            String str_similarty = "" + Html.fromHtml("&#8764;");
            String str_parallel = "" + Html.fromHtml("&#8741;");

            String str_arcm = "" + Html.fromHtml("&#8242;");
            String str_arcs = "" + Html.fromHtml("&#8243;");

            str_final = str_final.replace("op_plus", str_plus);
            str_final = str_final.replace("op_minus", str_minus);
            str_final = str_final.replace("op_equal", str_equal);
            str_final = str_final.replace("op_multi", str_multi);
            str_final = str_final.replace("op_divide", str_divide);
            str_final = str_final.replace("op_union", str_union);
            str_final = str_final.replace("op_intersection", str_intersection);
            str_final = str_final.replace("op_pi", str_pi);
            str_final = str_final.replace("op_factorial", str_factorial);
            str_final = str_final.replace("op_percentage", str_percentage);
            str_final = str_final.replace("op_goe", str_goe);
            str_final = str_final.replace("op_loe", str_loe);
            str_final = str_final.replace("op_grthan", str_grthan);
            str_final = str_final.replace("op_lethan", str_lethan);
            str_final = str_final.replace("op_infinity", str_infinity);
            str_final = str_final.replace("op_degree", str_degree);
            str_final = str_final.replace("op_xbar", str_xbar);
            str_final = str_final.replace("op_muxbar", str_muxbar);
            str_final = str_final.replace("op_sigmaxbar", str_sigmaxbar);

            //Added Siddhiinfosoft

            str_final = str_final.replace("op_alpha", str_alpha);
            str_final = str_final.replace("op_theta", str_theta);
            str_final = str_final.replace("op_mu", str_mu);
            str_final = str_final.replace("op_sigma", str_sigma);

            str_final = str_final.replace("op_beta", str_beta);
            str_final = str_final.replace("op_angle", str_angle);
            str_final = str_final.replace("op_mangle", str_mangle);
            str_final = str_final.replace("op_sangle", str_sangle);
            str_final = str_final.replace("op_rangle", str_rangle);
            str_final = str_final.replace("op_triangle", str_triangle);
            str_final = str_final.replace("op_rectangle", str_rectangle);
            str_final = str_final.replace("op_parallelogram", str_parallelogram);
            str_final = str_final.replace("op_perpendicular", str_perpendicular);
            str_final = str_final.replace("op_congruent", str_congruent);
            str_final = str_final.replace("op_similarty", str_similarty);
            str_final = str_final.replace("op_parallel", str_parallel);

            str_final = str_final.replace("op_arcm", str_arcm);
            str_final = str_final.replace("op_arcs", str_arcs);

            str_final = str_final.replace("op_integral", str_integral);
            str_final = str_final.replace("op_summation", str_summation);

            for (int i = 0; i < str_final.length(); i++) {

                String ss = String.valueOf(str_final.charAt(i));

                if (ss.equals(str_integral) || ss.equals(str_summation)) {
                    SpannableString ss1 = new SpannableString(ss);
                    ss1.setSpan(new RelativeSizeSpan(1.6f), 0, 1, 0);
                    ed.getText().insert(ed.getSelectionStart(), ss1);
                } else if (ss.equals(str_perpendicular) || ss.equals(str_parallel) ||
                        ss.equals(str_rectangle) || ss.equals(str_parallelogram)) {
                    SpannableString ss1 = new SpannableString(ss);
                    ss1.setSpan(new RelativeSizeSpan(1.2f), 0, 1, 0);
                    ed.getText().insert(ed.getSelectionStart(), ss1);
                } else if (ss.equals(str_angle) || ss.equals(str_mangle) ||
                        ss.equals(str_sangle) || ss.equals(str_rangle)) {
                    SpannableString ss1 = new SpannableString(ss);
                    ss1.setSpan(new RelativeSizeSpan(1.5f), 0, 1, 0);
                    ed.getText().insert(ed.getSelectionStart(), ss1);
                } else {
                    ed.getText().insert(ed.getSelectionStart(), ss);
                }

                if (ss.equals(str_integral)) {
                    ed.getText().insert(ed.getSelectionStart(), " ");
                }

            }

        } catch (Exception e) {

        }

        return str_final;
    }

/*
    public void setstudentansbox(final LinearLayout ll_ans_box, final String str_ans) {


        final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
        ll_last.setTag("text");
        final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
        ed_centre.setMinWidth(20);

        ll_ans_box.addView(ll_last);


    }
*/

    /*  protected void setstudentansbox(final LinearLayout ll_ans_box, String correct_ans) {*/
    public void setstudentansbox(final LinearLayout ll_ans_box, final String str_ans) {
        try {
            ll_ans_box.setTag(R.id.first, "1");
            ll_ans_box.setTag(R.id.second, "main");
            if (str_ans.trim().length() != 0) {
                setstudentansboxsub(str_ans, ll_ans_box);
            }

            final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
            ll_last.setTag("text");
            final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
            ed_centre.setMinWidth(20);

            ll_ans_box.addView(ll_last);

        } catch (Exception e) {

        }
    }


    public void setstudentansboxsub(final String str_ans, final LinearLayout ll_ans_box) {

        String[] str_1 = str_ans.split(ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first));

        String sss = String.valueOf(ll_ans_box.getTag(R.id.first));
        int tag_main_ll = Integer.valueOf(sss);

        for (int i = 0; i < str_1.length; i++) {

            String str_1_value = str_1[i];

            if (str_1_value.startsWith("text")) {

                str_1_value = str_1_value.substring(5);

                String sss1 = String.valueOf(ll_ans_box.getTag(R.id.first));
                int tag1 = Integer.valueOf(sss1);
                String tag2 = String.valueOf(ll_ans_box.getTag(R.id.second));
                int text_size = ActPlusButton.this.getResources().getInteger(R.integer.kb_textsize_main_int);

                try {

                    if (tag2.equals("super_top") || tag2.equals("sub_bottom") || tag2.equals("ss_top") ||
                            tag2.equals("ss_bottom") || tag2.equals("nth_top") || tag2.equals("lim_left") || tag2.equals("lim_right")) {
                        text_size = (text_size - 1) - (tag1 + 1);
                    } else if (tag2.equals("frac_top") || tag2.equals("frac_bottom") || tag2.equals("sqrt_c") || tag2.equals("nth_bottom")
                            || tag2.equals("parenthesis_center") || tag2.equals("absolute_center")
                            || tag2.equals("line_center") || tag2.equals("lsegment_center")
                            || tag2.equals("ray_center") || tag2.equals("arc_center")) {
                        text_size = text_size - (tag1 + 1);
                    }

                } catch (Exception e) {

                }

                final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                ll_last.setTag("text");
                final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                this.replacesignfromchar(str_1_value, ed_centre);
                ed_centre.setMinWidth(20);
                ed_centre.setTextSize(text_size);

                ll_ans_box.addView(ll_last);

            }
            if (str_1_value.startsWith("fraction")) {

                str_1_value = str_1_value.substring(8);

                final LinearLayout ll_fraction = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_fraction, null);
                final LinearLayout ll_fraction_top = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_top);
                final LinearLayout ll_fraction_bottom = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_bottom);
                ll_fraction_top.setTag(R.id.first, (tag_main_ll + 1));
                ll_fraction_bottom.setTag(R.id.first, (tag_main_ll + 1));
                ll_fraction_top.setTag(R.id.second, "frac_top");
                ll_fraction_bottom.setTag(R.id.second, "frac_bottom");

                final String[] str_2 = str_1_value.split("frac_btm_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_fraction);

                this.setstudentansboxsub(str_2[0], ll_fraction_top);
                this.setstudentansboxsub(str_2[1], ll_fraction_bottom);

            } else if (str_1_value.startsWith("super_script")) {
                str_1_value = str_1_value.substring(12);

                final LinearLayout ll_super_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_superscript, null);
                final LinearLayout ll_top_superscript = (LinearLayout) ll_super_script.findViewById(R.id.ll_top_superscript);
                ll_top_superscript.setTag(R.id.first, (tag_main_ll + 1));
                ll_top_superscript.setTag(R.id.second, "super_top");

                ll_ans_box.addView(ll_super_script);

                this.setstudentansboxsub(str_1_value, ll_top_superscript);

            } else if (str_1_value.startsWith("sub_script")) {
                str_1_value = str_1_value.substring(10);

                final LinearLayout ll_super_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_subscript, null);
                final LinearLayout ll_bottom_subscript = (LinearLayout) ll_super_script.findViewById(R.id.ll_bottom_subscript);
                ll_bottom_subscript.setTag(R.id.first, (tag_main_ll + 1));
                ll_bottom_subscript.setTag(R.id.second, "sub_bottom");

                ll_ans_box.addView(ll_super_script);

                this.setstudentansboxsub(str_1_value, ll_bottom_subscript);

            } else if (str_1_value.startsWith("super_sub_script")) {

                str_1_value = str_1_value.substring(16);

                final LinearLayout ll_super_sub_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_super_sub_script, null);
                final LinearLayout ll_top_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_top_super_sub_script);
                final LinearLayout ll_bottom_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_bottom_super_sub_script);
                ll_top_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                ll_bottom_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                ll_top_super_sub_script.setTag(R.id.second, "ss_top");
                ll_bottom_super_sub_script.setTag(R.id.second, "ss_bottom");

                final String[] str_2 = str_1_value.split("ss_btm_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_super_sub_script);

                this.setstudentansboxsub(str_2[0], ll_top_super_sub_script);
                this.setstudentansboxsub(str_2[1], ll_bottom_super_sub_script);

            } else if (str_1_value.startsWith("sqrt")) {

                str_1_value = str_1_value.substring(4);
                final LinearLayout ll_sqrt = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_square_root, null);
                final LinearLayout ll_center_square_root = (LinearLayout) ll_sqrt.findViewById(R.id.ll_center_square_root);
                ll_center_square_root.setTag(R.id.first, (tag_main_ll + 1));
                ll_center_square_root.setTag(R.id.second, "sqrt_c");

                ll_ans_box.addView(ll_sqrt);

                this.setstudentansboxsub(str_1_value, ll_center_square_root);

            } else if (str_1_value.startsWith("nsqrt")) {

                str_1_value = str_1_value.substring(5);

                final LinearLayout ll_nsqrt = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_nth_root, null);
                final LinearLayout ll_top_nth_root = (LinearLayout) ll_nsqrt.findViewById(R.id.ll_top_nth_root);
                final LinearLayout ll_center_nth_root = (LinearLayout) ll_nsqrt.findViewById(R.id.ll_center_nth_root);
                final ImageView img = (ImageView) ll_nsqrt.findViewById(R.id.img);
                ll_top_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                ll_center_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                ll_top_nth_root.setTag(R.id.second, "nth_top");
                ll_center_nth_root.setTag(R.id.second, "nth_bottom");

                img.getLayoutParams().height = ll_center_nth_root.getHeight();
                img.requestLayout();
                ll_center_nth_root.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                    @Override
                    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                        Log.e("het", "" + v.getHeight());
                        img.getLayoutParams().height = v.getHeight();
                        img.requestLayout();
                    }
                });

                final String[] str_2 = str_1_value.split("nsqrt_center_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_nsqrt);

                this.setstudentansboxsub(str_2[0], ll_top_nth_root);
                this.setstudentansboxsub(str_2[1], ll_center_nth_root);

            } else if (str_1_value.startsWith("lim")) {

                str_1_value = str_1_value.substring(3);

                final LinearLayout ll_lim = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_lim, null);
                final TextView tv_text = (TextView) ll_lim.findViewById(R.id.tv_text);
                tv_text.setText(Html.fromHtml("&#8594;"));
                final LinearLayout ll_lim_left = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_left);
                final LinearLayout ll_lim_right = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_right);
                ll_lim_left.setTag(R.id.first, (tag_main_ll + 1));
                ll_lim_right.setTag(R.id.first, (tag_main_ll + 1));
                ll_lim_left.setTag(R.id.second, "lim_left");
                ll_lim_right.setTag(R.id.second, "lim_right");

                final String[] str_2 = str_1_value.split("lim_rht_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_lim);

                this.setstudentansboxsub(str_2[0], ll_lim_left);
                this.setstudentansboxsub(str_2[1], ll_lim_right);

            } else if (str_1_value.startsWith("parenthesis")) {
                str_1_value = str_1_value.substring(11);

                final LinearLayout ll_super_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_parenthesis, null);
                final LinearLayout ll_parenthesis_center = (LinearLayout) ll_super_script.findViewById(R.id.ll_parenthesis_center);
                ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                ll_parenthesis_center.setTag(R.id.second, "parenthesis_center");

                ll_ans_box.addView(ll_super_script);

                this.setstudentansboxsub(str_1_value, ll_parenthesis_center);

            } else if (str_1_value.startsWith("absolute")) {
                str_1_value = str_1_value.substring(8);

                final LinearLayout ll_super_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_absolute, null);
                final LinearLayout ll_parenthesis_center = (LinearLayout) ll_super_script.findViewById(R.id.ll_absolute_center);
                ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                ll_parenthesis_center.setTag(R.id.second, "absolute_center");

                ll_ans_box.addView(ll_super_script);

                this.setstudentansboxsub(str_1_value, ll_parenthesis_center);

            } else if (str_1_value.startsWith("line") || str_1_value.startsWith("lsegment") || str_1_value.startsWith("ray")
                    || str_1_value.startsWith("arc")) {

                final LinearLayout ll_parenthesis = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_lsegment, null);
                final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_lsegment_center);
                ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                final ImageView img_top = (ImageView) ll_parenthesis.findViewById(R.id.img_top);
                // ll_parenthesis_center.setTag(R.id.second,"absolute_center");

                if (str_1_value.startsWith("line")) {
                    img_top.setImageResource(R.drawable.line_icon);
                    str_1_value = str_1_value.substring(4);
                    ll_parenthesis.setTag("line");
                    ll_parenthesis_center.setTag(R.id.second, "line_center");
                } else if (str_1_value.startsWith("lsegment")) {
                    str_1_value = str_1_value.substring(8);
                    img_top.setImageResource(R.drawable.line_segment);
                    ll_parenthesis.setTag("lsegment");
                    ll_parenthesis_center.setTag(R.id.second, "lsegment_center");
                } else if (str_1_value.startsWith("ray")) {
                    str_1_value = str_1_value.substring(3);
                    img_top.setImageResource(R.drawable.ray_icon);
                    ll_parenthesis.setTag("ray");
                    ll_parenthesis_center.setTag(R.id.second, "ray_center");
                } else if (str_1_value.startsWith("arc")) {
                    str_1_value = str_1_value.substring(3);
                    img_top.setImageResource(R.drawable.arc_icon);
                    ll_parenthesis.setTag("arc");
                    ll_parenthesis_center.setTag(R.id.second, "arc_center");
                }

                ll_ans_box.addView(ll_parenthesis);

                this.setstudentansboxsub(str_1_value, ll_parenthesis_center);

            }

        }
    }


    /**
     * Inflate the layout for not fill in type but for multiple choice answer type = 1
     *
     * @param customeAns
     * @return
     */
    private View getCheckMultiPleChoiceLayout(final CustomeAns customeAns, final int index) {

        View view = LayoutInflater.from(this).inflate(R.layout.check_home_multiple_choice_layout, null);
        @SuppressWarnings("unused")
        RelativeLayout viewLayout = (RelativeLayout) view.findViewById(R.id.checkHomeLayout);
        Button btnSeeAnswer = (Button) view.findViewById(R.id.btnSeeAnswer);
        TextView txtNumber = (TextView) view.findViewById(R.id.txtNumber);
        final Button btnRoughWork = (Button) view.findViewById(R.id.btnRoughWork);
        LinearLayout optionLayout = (LinearLayout) view.findViewById(R.id.optionLayout);
        RelativeLayout rlQuestionLayout = (RelativeLayout) view.findViewById(R.id.rlQuestionLayout);
        rlQuestionLayout.setVisibility(View.GONE);

        btnSeeAnswer.setVisibility(Button.INVISIBLE);
        btnRoughWork.setVisibility(Button.INVISIBLE);

        final MultipleChoiceAnswer multipleChiceAns = new MultipleChoiceAnswer();
        ArrayList<TextView> optionList = new ArrayList<TextView>();
        multipleChiceAns.setOptionList(optionList);
        String[] commaSepratedOption = MathFriendzyHelper.
                getCommaSepratedOption(customeAns.getOptions(), ",");

        for (int i = 0; i < commaSepratedOption.length; i++) {
            optionLayout.addView(this.getMultipleOptionLayout
                    (commaSepratedOption[i], customeAns, optionList,
                            multipleChiceAns, null));
        }
        txtNumber.setText(customeAns.getQueNo());

        TextView edtQuestion = (TextView) view.findViewById(R.id.edtQuestion);
        edtQuestion.setVisibility(View.GONE);
        return view;
    }

    /**
     * Inflate the layout for not fill in type but for multiple choice answer type = 2 or 3
     * for true false or yes no
     *
     * @param customeAns
     * @return
     */
    private View getCheckTrueFalseLayout(final CustomeAns customeAns, final int index) {
        View view = LayoutInflater.from(this).inflate(R.layout.check_home_true_false_layout, null);
        @SuppressWarnings("unused")
        RelativeLayout viewLayout = (RelativeLayout) view.findViewById(R.id.checkHomeLayout);
        Button btnSeeAnswer = (Button) view.findViewById(R.id.btnSeeAnswer);
        TextView txtNumber = (TextView) view.findViewById(R.id.txtNumber);
        final TextView ansTrue = (TextView) view.findViewById(R.id.ansTrue);
        final TextView ansFalse = (TextView) view.findViewById(R.id.ansFalse);
        final Button btnRoughWork = (Button) view.findViewById(R.id.btnRoughWork);
        RelativeLayout rlQuestionLayout = (RelativeLayout) view.findViewById(R.id.rlQuestionLayout);
        rlQuestionLayout.setVisibility(View.GONE);

        btnSeeAnswer.setVisibility(Button.INVISIBLE);
        btnRoughWork.setVisibility(Button.INVISIBLE);

        txtNumber.setText(customeAns.getQueNo());
        String[] ansArray = MathFriendzyHelper.getCommaSepratedOption(customeAns.getOptions(), ",");
        ansTrue.setText(ansArray[0]);
        ansFalse.setText(ansArray[1]);

        if (customeAns.getCorrectAns().equals("Yes")) {
            ansTrue.setBackgroundResource(R.drawable.checked_box_home_work);
            ansFalse.setBackgroundResource(R.drawable.unchecked_box_home_work);
        } else if (customeAns.getCorrectAns().equals("No")) {
            ansTrue.setBackgroundResource(R.drawable.unchecked_box_home_work);
            ansFalse.setBackgroundResource(R.drawable.checked_box_home_work);
        } else if (customeAns.getCorrectAns().equals("True")) {
            ansTrue.setBackgroundResource(R.drawable.checked_box_home_work);
            ansFalse.setBackgroundResource(R.drawable.unchecked_box_home_work);
        } else if (customeAns.getCorrectAns().equals("False")) {
            ansTrue.setBackgroundResource(R.drawable.unchecked_box_home_work);
            ansFalse.setBackgroundResource(R.drawable.checked_box_home_work);
        }

        TextView edtQuestion = (TextView) view.findViewById(R.id.edtQuestion);
        edtQuestion.setVisibility(View.GONE);
        return view;
    }


    /**
     * Return the inflated view to multiple choice option
     *
     * @param optionText
     * @param customeAns
     * @param optionList
     * @param multipleChiceAns
     * @param playerAns
     * @return
     */
    private View getMultipleOptionLayout(String optionText,
                                         final CustomeAns customeAns, ArrayList<TextView> optionList,
                                         MultipleChoiceAnswer multipleChiceAns, CustomePlayerAns playerAns) {
        View view = LayoutInflater.from(this).inflate(R.layout.check_home_work_option_layout, null);
        final TextView option = (TextView) view.findViewById(R.id.option);
        option.setText(optionText);
        optionList.add(option);

        if (customeAns != null) {
            if (customeAns.getCorrectAns().contains(optionText)) {
                option.setBackgroundResource(R.drawable.checked_small_box_home_work);
            }
        }
        return view;
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase,
                               int requestCode) {
        // TODO Auto-generated method stub
    }

    /**
     * Click On GetHelp
     */
    private void clickOnGetHelp() {
        Intent intent = new Intent(this, ActGetHelp.class);
        intent.putExtra("homeWorkQuizzData", homeWorkQuizzData);
        intent.putExtra("customeDetail", customeResult);
        intent.putExtra("customeAns", customeAns);
        startActivityForResult(intent, START_GET_HELP_REQUEST);
    }

    /**
     * Click On WorkArea
     */
    private void clickOnWrokArea() {
        String fileName = playerAns.getWorkImage();
        final Intent intent = new Intent(this, HomeWorkWorkArea.class);

        //for adding tab pn work area
        intent.putExtra("isTutorWorkArea", isTutorWorkArea);
        intent.putExtra("findTutorDetail", findTutorDetail);

        intent.putExtra("correctAns", customeAns.getCorrectAns());
        intent.putExtra("isCallForUpdateAns", true);
        intent.putExtra("playerMsg", teacherStudentMsg);
        intent.putExtra("workareapublic", workareapublic);
        intent.putExtra("callFromPlusAct", true);
        intent.putExtra("playerAndQuestionImage", playerAndQuestionImage);
        intent.putExtra("questionImageName", this.getQuestionImageName
                (customeAns, index));
        //changes for update text to server
        intent.putExtra("customeDetail", customeResult);
        intent.putExtra("homeWorkQuizzData", homeWorkQuizzData);
        intent.putExtra("customeAns", customeAns);
        intent.putExtra("playerAns", playerAns);
        intent.putExtra("chatRequestId", chatRequestId);

        //for view pdf
        if (cropImageUriFromPdf != null && cropImageUriFromPdf.length() > 0) {
            intent.putExtra("isImageCrop", true);
            intent.putExtra("cropImageUriFromPdf", cropImageUriFromPdf);
            intent.putExtra("questionImageName", this.getQuestionImageName
                    (customeAns, index));
        }

        if (fileName == null || fileName.length() == 0)
            fileName = this.getImageName(customeAns, index);

        if (fileName != null && fileName.length() > 0) {
            intent.putExtra("fileName", fileName);
            this.deleWorkAreaImageFromSDCard(fileName);
            if (MathFriendzyHelper.checkForExistenceOfFile(fileName)) {
                intent.putExtra("isFileExist", true);
                //startActivityForResult(intent , START_WORK_AREA_REQUEST_CODE);
                startWorkAreaAct(intent, playerAndQuestionImage);
            } else {
                if (CommonUtils.isInternetConnectionAvailable(this)) {
                    MathFriendzyHelper.downLoadImageFromUrl
                            (this, ICommonUtils.DOWNLOD_WORK_IMAGE_URL, fileName, new HttpServerRequest() {
                                @Override
                                public void onRequestComplete() {
                                    intent.putExtra("isFileExist", true);
                                    //startActivityForResult(intent , START_WORK_AREA_REQUEST_CODE);
                                    startWorkAreaAct(intent, playerAndQuestionImage);
                                }
                            });
                } else {
                    intent.putExtra("isFileExist", false);
                    startWorkAreaAct(intent, playerAndQuestionImage);
                }
            }
        } else {
            fileName = getImageName(customeAns, index);
            //this.addToDownloadImageList(fileName);
            if (MathFriendzyHelper.checkForExistenceOfFile(fileName)) {
                intent.putExtra("isFileExist", true);
            } else {
                intent.putExtra("isFileExist", false);
            }
            intent.putExtra("fileName", fileName);
            //startActivityForResult(intent , START_WORK_AREA_REQUEST_CODE);
            startWorkAreaAct(intent, playerAndQuestionImage);
        }
    }

    /**
     * Delete the image from sd card
     * @param imageName
     *//*
    private void deleteImageFromSDCard(String imageName){
		try{
			if(CommonUtils.isInternetConnectionAvailable(this)
					&& customeResult.isAlreadyPlayed()){		
				String pngImageName = MathFriendzyHelper.getPNGFormateImageName(imageName);
				if(!downloadImageList.contains(pngImageName)){
					downloadImageList.add(pngImageName);
					MathFriendzyHelper.deleteImageFromGivenUri
					(MathFriendzyHelper.getMathFilePath() + "/" +
							MathFriendzyHelper.getPNGFormateImageName(imageName));
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/

    /**
     * Delete the image from sd card
     *
     * @param imageName
     */
    private void deleteQuestionImageFromSDCard(String imageName) {
        try {
            if (CommonUtils.isInternetConnectionAvailable(this)
                    && customeResult.isAlreadyPlayed()) {
                String pngImageName = MathFriendzyHelper.getPNGFormateImageName(imageName);
                if (!downloadImageList.contains(pngImageName)) {
                    //downloadImageList.add(pngImageName);
                    MathFriendzyHelper.deleteImageFromGivenUri
                            (MathFriendzyHelper.getMathFilePath() + "/" +
                                    MathFriendzyHelper.getPNGFormateImageName(imageName));
                } else {
                    //Log.e(TAG, "inside else");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete the work area image name from SD card
     *
     * @param imageName
     */
    private void deleWorkAreaImageFromSDCard(String imageName) {
        try {
            if (CommonUtils.isInternetConnectionAvailable(this)
                    && customeResult.isAlreadyPlayed()) {
                String pngImageName = MathFriendzyHelper.getPNGFormateImageName(imageName);
                if (!downloadImageList.contains(pngImageName)) {
                    MathFriendzyHelper.deleteImageFromGivenUri
                            (MathFriendzyHelper.getMathFilePath() + "/" +
                                    MathFriendzyHelper.getPNGFormateImageName(imageName));
                } else {
                    //Log.e(TAG, "inside else");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Add to the download image list , Name of images which are downloaded
     *
     * @param imageName
     */
    private void addToDownloadImageList(String imageName) {
        String pngImageName = MathFriendzyHelper.getPNGFormateImageName(imageName);
        if (!downloadImageList.contains(pngImageName)) {
            downloadImageList.add(pngImageName);
        }
    }

    /**
     * start the work area activity
     *
     * @param intent
     */
    private void startWorkAreaAct(final Intent intent, final String questionImageName) {
        if (questionImageName != null &&
                questionImageName.length() > 0) {
            this.deleteQuestionImageFromSDCard(questionImageName);
            if (MathFriendzyHelper.checkForExistenceOfFile(questionImageName)) {
                startActivityForResult(intent, START_WORK_AREA_REQUEST_CODE);
            } else {
                if (CommonUtils.isInternetConnectionAvailable(this)) {
                    MathFriendzyHelper.downLoadImageFromUrl
                            (this, ICommonUtils.DOWNLOAD_QUESTION_IMAGE_URL, questionImageName,
                                    new HttpServerRequest() {
                                        @Override
                                        public void onRequestComplete() {
                                            startActivityForResult(intent, START_WORK_AREA_REQUEST_CODE);
                                        }
                                    });
                } else {
                    startActivityForResult(intent, START_WORK_AREA_REQUEST_CODE);
                }
            }
        } else {
            startActivityForResult(intent, START_WORK_AREA_REQUEST_CODE);
        }
    }

    /**
     * Return the work image name
     *
     * @param customeAns
     * @param indexOfQuestion
     * @return
     */
    private String getImageName(CustomeAns customeAns, int indexOfQuestion) {
        return customeResult.getCustomHwId() + "_" +
                selectedPlayerData.getPlayerid() + "_" + indexOfQuestion + ".png";
    }

    /**
     * Question image name
     *
     * @param customeAns
     * @param indexOfQuestion
     * @return
     */
    private String getQuestionImageName(CustomeAns customeAns, int indexOfQuestion) {
        try {
            return "q_" + customeResult.getCustomHwId() + "_" +
                    selectedPlayerData.getPlayerid() + "_" + indexOfQuestion + ".png";
        } catch (Exception e) {
            return "";
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnGetHelp:
                this.clickOnGetHelp();
                break;
            case R.id.btnWorkArea:
                isTutorWorkArea = false;
                this.clickOnWrokArea();
                break;
            case R.id.btnSeeAnswer:
                if (customeAns.getFillInType() == 1
                        && customeAns.getIsAnswerAvailable() == MathFriendzyHelper.NO) {
                    MathFriendzyHelper.showWarningDialog(this, lblTeacherNotGivenAnswer);
                    return;
                }
                //hasSeenAnswer = true;
                isFirstTimeWrong = 1;
                this.clickOnSeeCorrectAnswer();
                MathFriendzyHelper.addDuplicateWorkAreaContent(this,
                        AddDuplicateWorkAreaParam.SAW_ANSWER_PARAM);
                break;
            case R.id.btnSeeHowOtherDoIt:
                //this.clickOnHowOtherDitIt();
                //hasSeenAnswer = true;
                this.clickOnGetHelp();
                break;
            case R.id.btnConnectWithATutor:
                //hasSeenAnswer = true;
                this.clickOnConnectWithATutor();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == START_WORK_AREA_REQUEST_CODE) {
                teacherStudentMsg = data.getStringExtra("studentMessage");
                workareapublic = data.getIntExtra("workareapublic", 1);
                saveFileName = data.getStringExtra("imageName");
                isImageSaved = data.getBooleanExtra("isImageSaved", false);
                //if(data.getIntExtra("chatRequestId" , 0) > 0) {
                //playerAns.setChatRequestedId(data.getIntExtra("chatRequestId", 0));
                chatRequestId = data.getIntExtra("chatRequestId", 0);
                //}

                //Add Gdoc and web links
                try {
                     urlToWorkAreaArrayList
                             = (ArrayList<AddUrlToWorkArea>) data.getSerializableExtra("urlLinks");
                    playerAns.setUrlLinks(urlToWorkAreaArrayList);
                }catch (Exception e){
                    e.printStackTrace();
                }
                //end changes

                if (data.getBooleanExtra("isImageSaved", false)) {
                    //for downloaded image
                    //this.addToDownloadImageList(data.getStringExtra("imageName"));
                }

                //for question image
                if (data.getBooleanExtra("isDeleteImage", false)) {
                    questionImageName = data.getStringExtra("questionImageName");
                    playerAndQuestionImage = questionImageName;
                    isDeleteImage = data.getBooleanExtra("isDeleteImage", false);
                    playerAns.setQuestionImage(questionImageName);
                    //for downloaded image
                    this.addToDownloadImageList(questionImageName);
                } else {
                    if (data.getBooleanExtra("isClickOnPaste", false)) {
                        isClickOnPaste = data.getBooleanExtra("isClickOnPaste", false);
                        questionImageName = data.getStringExtra("questionImageName");
                        playerAndQuestionImage = questionImageName;
                        playerAns.setQuestionImage(questionImageName);
                        //for downloaded image
                        this.addToDownloadImageList(questionImageName);
                        cropImageUriFromPdf = "";
                    }
                }
                isWorkInputSeen = data.getBooleanExtra("isWorkInputSeen", false);
                isOpponentInputSeen = data.getBooleanExtra("isOpponentInputSeen", false);

                if (chatRequestId > 0) {
                    hasSeenAnswer = true;
                    isGetHelp = 1;
                }

                if (data.getBooleanExtra("isAnsUpdated", false)) {
                    this.setUpdatedDataAndRefreshView(data);
                }
            } else if (requestCode == FIND_TUTOR_REQUEST) {
                chatRequestId = 0;
                isTutorWorkArea = true;
                findTutorDetail = (FindTutorResponse) data.getSerializableExtra("findTutorResponse");
                this.clickOnWrokArea();
            } else {
                isGetHelp = data.getIntExtra("isGetHelp", 0);
                if (isGetHelp == MathFriendzyHelper.YES) {
                    hasSeenAnswer = true;
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        if (hasSeenAnswer) {
            isGetHelp = 1;
        }
        Intent intent = new Intent();
        intent.putExtra("studentMessage", teacherStudentMsg);
        intent.putExtra("workareapublic", workareapublic);
        intent.putExtra("isGetHelp", isGetHelp);
        intent.putExtra("isImageSaved", isImageSaved);
        intent.putExtra("imageName", saveFileName);
        intent.putExtra("isClickOnPaste", isClickOnPaste);
        intent.putExtra("isDeleteImage", isDeleteImage);
        intent.putExtra("questionImageName", questionImageName);
        intent.putExtra("hasSeenAnswer", hasSeenAnswer);
        intent.putExtra("chatRequestId", chatRequestId);

        //for downloaded image
        intent.putStringArrayListExtra("downloadImageList", downloadImageList);

        //for new green background to the pencil
        intent.putExtra("isWorkInputSeen", isWorkInputSeen);
        intent.putExtra("isOpponentInputSeen", isOpponentInputSeen);
        intent.putExtra("isFirstTimeWrong", isFirstTimeWrong);

        try {
            //for url links
            intent.putExtra("urlLinks", urlToWorkAreaArrayList);

            //for put answer on workarea
            intent.putExtra("isAnsUpdated", isAnsUpdated);
            intent.putExtra("updatedPlayerAns", this.playerAns);
        }catch (Exception e){
            e.printStackTrace();
        }
        setResult(RESULT_OK, intent);
        finish();
        super.onBackPressed();
    }


    //for tutoring project

    /**
     * Click to see answer
     */
    private void clickOnSeeCorrectAnswer() {
        if (seeCorrectAnswerLayout.getVisibility() == RelativeLayout.GONE) {
            seeCorrectAnswerLayout.setVisibility(RelativeLayout.VISIBLE);
            scrollLayout.setVisibility(ScrollView.VISIBLE);
        } else {
            seeCorrectAnswerLayout.setVisibility(RelativeLayout.GONE);
            scrollLayout.setVisibility(ScrollView.GONE);
        }
    }

    /**
     * Click how other did it
     */
    /*private void clickOnHowOtherDitIt(){

	}*/

    /**
     * Click to connect with tutor
     */
    private void clickOnConnectWithATutor() {
        if (CommonUtils.isInternetConnectionAvailable(this)) {
            if (chatRequestId > 0) {
                MathFriendzyHelper.getChatRequestData(chatRequestId, this, new HttpResponseInterface() {
                    @Override
                    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                        GetChatRequestResponse response = (GetChatRequestResponse) httpResponseBase;
                        if (response.getResult().equals("success")) {
                            if (response.getDisconnected() == 1 && response.getRating() > 0) {
                                MathFriendzyHelper.yesNoConfirmationDialog
                                        (ActPlusButton.this, lblThisWillDeleteSession, btnTitleYes,
                                                btnTitleNoThanks, new YesNoListenerInterface() {
                                                    @Override
                                                    public void onYes() {
                                                        openFindTutorScreen();
                                                    }

                                                    @Override
                                                    public void onNo() {

                                                    }
                                                });
                                //openFindTutorScreen();
                            } else {
                                goForChatConnected();
                            }
                        } else {
                            goForChatConnected();
                        }
                    }
                }, true);
                //this.goForChatConnected();
            } else {
                this.openFindTutorScreen();
            }
        } else {
            CommonUtils.showInternetDialog(this);
        }
    }

    private void goForChatConnected() {
        isTutorWorkArea = true;
        this.clickOnWrokArea();
    }

    private void openFindTutorScreen() {
        Intent intent = new Intent(this, ActFindTutor.class);
        intent.putExtra("subjectId" , this.getSubjectId());
        startActivityForResult(intent, FIND_TUTOR_REQUEST);
    }

    private int getSubjectId(){
        try{
            return homeWorkQuizzData.getSubId();
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    private void setUpdatedDataAndRefreshView(Intent data) {
        try {
            this.isAnsUpdated = true;
            this.playerAns = (CustomePlayerAns)
                    data.getSerializableExtra("updatedPlayerAns");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /*private void initializeCurrentObj(){
        currentObj = this;
    }

    public static ActPlusButton getCurrentObj(){
        return currentObj;
    }

    public void setIsGetHelp(int isGetHelp){

    }*/
}