package com.mathfriendzy.controller.homework.homwworkworkarea;


import android.annotation.SuppressLint;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.homework.ActCheckHomeWork;
import com.mathfriendzy.controller.homework.Utils;
import com.mathfriendzy.controller.homework.addhomeworksheet.ActViewPdf;
import com.mathfriendzy.controller.homework.assignhomeworkquiz.ActAssignCustomAns;
import com.mathfriendzy.controller.homework.assignhomeworkquiz.ActMultipleChoiceAnsSheet;
import com.mathfriendzy.controller.homework.secondworkarea.SecondWorkAreaFragment;
import com.mathfriendzy.controller.homework.secondworkarea.SecondWorkBundleArgument;
import com.mathfriendzy.controller.homework.workimageoption.HomeWorkImageOption;
import com.mathfriendzy.controller.homework.workimageoption.HomeworkImageOptionCallback;
import com.mathfriendzy.controller.homework.workimageoption.ResizeView;
import com.mathfriendzy.controller.homework.workimageoption.ResizeViewCallback;
import com.mathfriendzy.controller.homework.workimageoption.SelectImageConstants;
import com.mathfriendzy.controller.homework.workimageoption.SelectImageOption;
import com.mathfriendzy.controller.homework.workimageoption.TakeScreenShotAndSaveToMemoryCallback;
import com.mathfriendzy.controller.tutor.ActFindTutor;
import com.mathfriendzy.controller.tutor.ChatRequestIdCallback;
import com.mathfriendzy.controller.tutor.TutorSessionBundleArgument;
import com.mathfriendzy.controller.tutor.TuturSessionFragment;
import com.mathfriendzy.customview.AutoResizeEditText;
import com.mathfriendzy.customview.EditTextBlink;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.helper.QuizzSelectionInterface;
import com.mathfriendzy.helper.YesNoListenerInterface;
import com.mathfriendzy.homewatcher.HomeWatcher;
import com.mathfriendzy.homewatcher.OnHomePressedListener;
import com.mathfriendzy.listener.OnRequestComplete;
import com.mathfriendzy.listener.OnScrollChange;
import com.mathfriendzy.model.grade.GetWorkAreaChatResponse;
import com.mathfriendzy.model.homework.CheckUserAnswer;
import com.mathfriendzy.model.homework.CustomeAns;
import com.mathfriendzy.model.homework.CustomePlayerAns;
import com.mathfriendzy.model.homework.CustomeResult;
import com.mathfriendzy.model.homework.GetHelpStudentListResponse;
import com.mathfriendzy.model.homework.GetHomeWorkForStudentWithCustomeResponse;
import com.mathfriendzy.model.homework.GetWorkAreaChatMsgParam;
import com.mathfriendzy.model.homework.HomeWorkImpl;
import com.mathfriendzy.model.homework.MultipleChoiceAnswer;
import com.mathfriendzy.model.homework.SaveGDocLinkOnServerParam;
import com.mathfriendzy.model.homework.SaveRatingStarForHelpWorkAreaParam;
import com.mathfriendzy.model.homework.SaveStudentRatingResponse;
import com.mathfriendzy.model.homework.SaveWorkAreaChatParam;
import com.mathfriendzy.model.homework.StudentAnsBase;
import com.mathfriendzy.model.homework.UpdateInputStatusForWorkAreaParam;
import com.mathfriendzy.model.homework.checkhomework.GetDetailOfHomeworkWithCustomeResponse;
import com.mathfriendzy.model.homework.checkhomework.UpdateCreditByTeacherForStudentQuestionParam;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.tutor.FindTutorResponse;
import com.mathfriendzy.model.tutor.StopWaitingForTutorParam;
import com.mathfriendzy.notification.DisconnectStudentNotif;
import com.mathfriendzy.notification.HelpSetudentResponse;
import com.mathfriendzy.notification.PopUpActivityForNotification;
import com.mathfriendzy.notification.TutorImageUpdateNotif;
import com.mathfriendzy.notification.TutorWorkAreaImageUpdateNotif;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.uploadimages.UploadImage;
import com.mathfriendzy.uploadimages.UploadImageRequest;
import com.mathfriendzy.utils.CommonUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


public class HomeWorkWorkArea extends ActBase implements OnScrollChange, ChatRequestIdCallback {

    private LinearLayout layRough = null;
    private Paint mPaint, mBitmapPaint = null;
    private MyView mView = null;
    private Bitmap mBitmap = null;
    private Canvas mCanvas = null;
    private Path mPath = null;
    private Button clearPaint = null;
    private Button drawPaint = null;
    private Button btnText = null;
    private TextView txtTitleScreen = null;
    private Button btnCheck = null;

    private final String TAG = this.getClass().getSimpleName();

    private String fileName = null;
    private boolean isFileExist = false;
    private boolean isStartDrowing = false;
    //for update ans after seen correct teacher answer
    private boolean isCallForUpdateAns = false;

    private boolean isCallForGetHelp = false;
    private int ratingStar = 0;

    private RelativeLayout rateMeLayout = null;
    private RelativeLayout editLayout = null;
    private RelativeLayout studentTeacherLayout = null;

    private TextView txtRateMe = null;
    private ImageView imgStar1 = null;
    private ImageView imgStar2 = null;
    private ImageView imgStar3 = null;
    private ImageView imgStar4 = null;
    private ImageView imgStar5 = null;
    private Button btnTextForGetHelp = null;

    private CustomeResult customeResult = null;
    private GetHomeWorkForStudentWithCustomeResponse homeWorkQuizzData = null;
    private UserPlayerDto selectedPlayerData = null;

    //for Activity open GetHelp or for Teacher check answer
    private CustomeAns customeAns = null;
    //for Activity open for Teacher check answer
    private CustomePlayerAns playerAns = null;

    //Get Help student
    private GetHelpStudentListResponse student = null;

    //for teacher student text input
    private EditText edtTeacherStudentText = null;
    private Button btnCross = null;

    public String teacherStudentMsg = null;

    private TextView txtMakeItPrivate = null;
    private int workareapublic = 1;

    private Button btnScroll = null;
    //for draw or scroll
    private boolean isScroll = false;

    private Bitmap alreadyWorkingBitmap = null;
    private ListView lstteacherStudentMsgList = null;
    private HomeWorkMsgAdapter adapter = null;

    private boolean isWorkAreaForstudent = true;//true for student , false for teacher
    private Button btnSend = null;

    //for teacher view
    private RelativeLayout teacherViewAnswerLayout = null;
    private LinearLayout answerLayout = null;
    //for credit layout for teacher view
    private TextView txtGotHelp = null;
    private TextView txtViewedAnswer = null;
    private TextView txtCredit = null;
    private ImageView imgCheckGotHelp = null;
    private ImageView imgCheckViewdAnswer = null;
    private TextView txtCreditPercentage = null;

    private final int FILL_IN_TYPE = 1;
    private final int MULTIPLE_CHOICE_ANS_TYPE = 1;
    private final int TRUE_FALSE_ANS_TYPE = 2;
    private final int YES_NO_ANS_TYPE = 3;

    private final int HALF_CREDIT = 3;
    private final int NONE_CREDITT = 1;
    private final int FULL_CREDIT = 4;
    private final int ZERO_CREDIT = 2;

    private final String PERCENTAGE_0 = "0%";
    private final String PERCENTAGE_50 = "50%";
    private final String PERCENTAGE_100 = "100%";

    private String lblStudentAnswer = null;
    private String lblcorrectAns = null;

    //for teacher check answer
    private View selectedView = null;//player answer view , need to set background color on credit changes

    //for open pdf question image
    private String cropImageUriFromPdf = null;
    //private ImageView imgQuestionImage = null;
    private boolean isImageCrop = false;

    private Button btnPaste = null;
    private boolean isClickOnPaste = false;//used for question image operation like paste
    private boolean isDeleteImage = false;
    private String questionImageName = "";
    private String playerAnsQuestionImage = "";

    private String btnTitleYes = null;
    private String lblNo = null;
    private String lblWouldYouLikeToDeleteImage = null;

    private boolean isTab = false;

    private ScrollView scrollView = null;
    private Button btnScrollUp = null;
    private Button btnScrollDown = null;

    //for undo redo
    private ArrayList<Path> paths = new ArrayList<Path>();
    private ArrayList<Path> undonePaths = new ArrayList<Path>();
    private Button btnUndo = null;
    private Button btnRedo = null;

    //for erase mode
    private boolean eraserMode = false;
    private GetDetailOfHomeworkWithCustomeResponse studentDetail = null;

    //for loading chat
    private ProgressBar loadingChatProgressDialog = null;

    //for eraser circle
    private ImageView imgEraser = null;
    /*private float _xDelta = 0;
    private float _yDelta = 0;*/

    //for adding tab
    private RelativeLayout myWorkAreaContainer = null;
    private RelativeLayout fragmentContainer = null;
    private Button myWorkArea = null;
    private Button btnTutorWorkArea = null;
    private TuturSessionFragment tutorSessionFragment = null;
    private RelativeLayout relativeTopBar = null;

    //for adding new tutor tab
    private boolean isTutorWorkArea = false;
    private FindTutorResponse findTutorDetail = null;
    private final int FIND_TUTOR_REQUEST = 1003;
    private final int FIND_TUTOR_REQUEST_FOR_MEGNIFIER = 1010;
    private int chatRequestId = 0;
    private String lblYouAreNotConnectedWithTutor = null;
    private String lblPleaseEnterQuestionToNotifyTutor = null;
    private String btnTitleOK = null;
    private String btnTitleCancel = null;
    private String lblOrCancelTutorRequest = null;

    //for rate dialog
    private LinearLayout tutorContentLayout = null;
    private ImageView rateUsStar = null;
    private ImageView imgDisconnect = null;

    //for new submit question and answer button
    private String lblYourQuestionWillNotBeSubmitted =
            "Your question will not be submitted to available tutors until you tap on \"Submit Question.\"";
    private String alertWouldYouLikeToStopWaiting = null;

    private ImageView imgOnline = null;
    private ImageView imgHand = null;
    private ImageView imgMegnifier = null;

    //for notificaiton
    private static HomeWorkWorkArea currentObj = null;
    private final int MY_WORK_AREA = 1;
    private final int TUTOR_WORK_AREA = 2;
    private final int SECOND_WORK_AREA = 3;
    private int selectedWorkAreaTab = 1;

    //for visibility of tutor tab
    private RelativeLayout layoutTutorTab = null;
    private boolean isOpponentActiveInApp = true;
    private boolean isOpponentOnline = false;

    private String alertWouldULikeToConnectOther = null;
    private String lblTutorArea = null;

    //for image operation
    private Button btnDeleteAndSelectImageOption = null;
    private HomeWorkImageOption imageOptionSelector = null;
    private SelectImageOption selectedOption = null;
    private ResizeView resizeView = null;
    private RelativeLayout bottomLayout = null;
    private ImageView imgStudentImage = null;
    private RelativeLayout studentImageLayout = null;

    //for make it private new changes
    private String lblMakeItPrivate = null;

    private RelativeLayout workAreaContentLayout = null;//for work area drawing on the tutor session

    //for second work area
    private RelativeLayout secondWorkAreaContainer = null;
    private Button btnSecondWorkArea = null;
    private SecondWorkAreaFragment secondWorkAreaFragment = null;
    private boolean isShowSecondTab = false;

    //for green button rough button on the act check homework screen
    private boolean isWorkAreaUpdated = false;
    private boolean isWorkInputSeen = false;
    private boolean isOpponentInputSeen = false;
    private String alertMessageImagePasteOrNot = null;

    private String lblYouHaveAlreadyRated = "You have already rated this work area.";
    private RelativeLayout viewGroup = null;

    private boolean isScrollEnable = false;
    private int drawViewLeftMargin = 0;

    private String alertMsgAbusingWord = null;


    //Added siddhiinfosoft

    private String lblAnswerOnWorkAreaCreditGiven = "Answer on Work Area Credit will be given when teacher views the answer.";


    //private boolean isExpireQuizz = false;

    //to hide the answer layout from teacher check homework
    private Button imgCloseAnswerLayout = null;
    private String closeAnswersText = null;
    private String showAnswersText = null;
    private boolean isAnswerLayoutShown = true;
    private Button btnCloseViewAnswer = null;

    //for add google doc or web link
    private Button btnAddLinks = null;
    private ArrayList<AddUrlToWorkArea> urlList = null;
    private AddUrlToQuestionDialog addUrlLinkDialog = null;


    //student put answer on work area
    private RelativeLayout studentPutAnswerLayout = null;
    private LinearLayout checkHomeWorkListLayout = null;
    private RelativeLayout keyboardLayout = null;
    private boolean isExpireQuizz = false;
    private boolean isAllowChanges = false;
    private Button btnShowInputAnswer = null;

    //audio calling changes
    private Button btnAudioCall = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_work_work_area);

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside onCreate()");

        isTab = getResources().getBoolean(R.bool.isTablet);
        selectedPlayerData = this.getPlayerData();

        this.init();
        this.initializeCurrentObj();
        this.getIntentValues();
        MathFriendzyHelper.initializeHeightAndWidth(this, isTab);
        this.initializeImageSelector();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.setCustomView();
        this.showCorrectAnsPopUp();
        this.setVisibilityOfRateMeLayout();
        this.setRatingstar(ratingStar);
        this.setCheckedBackground();
        this.setVisibilityOftextLayoutOnTeacherMsg();
        //this.setStudentTeacherMsg();
        this.setVisibilityOfMakeItPrivate();
        this.setVisibilityOfTeacherAnswerLayout();
        //for Activity open for teacher to check homework
        this.setStudentAnswerLayout();
        this.setScrollButtonSelected();
        this.setQuestionImage();
        this.setVisibilityOfPasteButton();
        this.getChatMessage();
        //this.setLayoutParam();

        this.setPencilSelected(true);
        this.setEraserSelected(false);

        if (isTutorWorkArea) {
            isOpponentInputSeen = true;
            //original work area take time in setup. This will create problem when taking screen
            //shot without tabbing on that for draw work area on tutor session
            //,So need to put this line into handler , Now everything will work fine
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    clickOnTutorSeesionWorkArea();
                }
            }, 250);
        } else {
            isWorkInputSeen = true;
            this.markInputSeenForWorkArea();
            this.setVisibilityOfTutorSessionTab();
        }

        //this.setVisibilityOfSecondWorkAreaTab();

        this.startHomePressWatcher();

        this.setVisibilityOfAddLinkButton();
        this.setVisiblilityOfBtnCloseViewAnswer();
        this.closeStudentAnswerLayout();

        //student put answer on work area
        //this.initStudentPutAnsWithKeyboardObj();
        this.initStudentInputViewAndData();
        //this.setStudentAnswerLayoutToPutAnswer();

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "outside onCreate()");
    }

    /**
     * Set Pencil Selected And UnSelected Background
     *
     * @param isSelected
     */
    private void setPencilSelected(boolean isSelected) {
        if (isSelected) {
            drawPaint.setBackgroundResource(R.drawable.pencil);
        } else {
            drawPaint.setBackgroundResource(R.drawable.grey_pencil);
        }
    }

    /**
     * Set Erase Selected and Unselected background
     *
     * @param isSelected
     */
    private void setEraserSelected(boolean isSelected) {
        if (isSelected) {
            clearPaint.setBackgroundResource(R.drawable.eraser);
        } else {
            clearPaint.setBackgroundResource(R.drawable.grey_eraser);
        }
    }

    /*
     * Only for check homework
     * Set tutor tab and second work tab visibility for check homework
    */
    private void setVisibilityOfSecondWorkAreaTab() {
        isShowSecondTab = ((playerAns != null && (playerAns.getIsGetHelp() == 1 ||
                playerAns.getFirstTimeWrong() == 1))) ? true : false;
        if (isShowSecondTab || chatRequestId > 0) {
            txtTitleScreen.setVisibility(TextView.GONE);
            myWorkArea.setText("New Work Area");
            myWorkArea.setVisibility(Button.VISIBLE);
            if (isShowSecondTab) {
                btnSecondWorkArea.setVisibility(Button.VISIBLE);
            }
        } else {
            myWorkArea.setVisibility(Button.GONE);
            txtTitleScreen.setVisibility(TextView.VISIBLE);
        }
    }

    private void init() {
        try {
            isExpireQuizz = ActCheckHomeWork.getCurrentObj().isExpireQuizz;
            isAllowChanges = ActCheckHomeWork.getCurrentObj().isAllowChanges;
        }catch (Exception e){
            e.printStackTrace();
        }

        selectedOption = new SelectImageOption();
        selectedOption.setId(SelectImageConstants.SELECT_FROM_CAMERA);
        selectedOption.setOptionName("Camera");
        selectedOption.setIsVisible(0);

        resizeView = new ResizeView(this, new ResizeViewCallback() {
            @Override
            public void onResizeDone(int tag, Bitmap bitmap, RelativeLayout.LayoutParams lp) {
                onResizeOption(tag, bitmap, lp);
            }
        });
    }

    /**
     * Show or hide the chat loading dialog
     *
     * @param isShow
     */
    private void visibleInvisibleChatLoadingDialog(boolean isShow) {
        if (isShow) {
            loadingChatProgressDialog.setVisibility(ProgressBar.VISIBLE);
        } else {
            loadingChatProgressDialog.setVisibility(ProgressBar.GONE);
        }
    }

    /**
     * Get the chat message from server if internet is connected otherwise set it from the previous
     * screen message
     */
    private void getChatMessage() {
        try {
            if (CommonUtils.isInternetConnectionAvailable(this)) {
                GetWorkAreaChatMsgParam param = new GetWorkAreaChatMsgParam();
                if (isCallForGetHelp) {
                    param.setUserId(student.getUserId());
                    param.setPlayerId(student.getPlayerId());
                    param.setHWId(homeWorkQuizzData.getHomeWorkId());
                } else {
                    if (isWorkAreaForstudent) {
                        param.setUserId(selectedPlayerData.getParentUserId());
                        param.setPlayerId(selectedPlayerData.getPlayerid());
                        param.setHWId(homeWorkQuizzData.getHomeWorkId());
                    } else {//work area for teacher
                        param.setUserId(studentDetail.getParentId());
                        param.setPlayerId(studentDetail.getPlayerId());
                        param.setHWId(studentDetail.getHomeWorkId());
                    }
                }
                param.setCustomeHWId(customeResult.getCustomHwId() + "");
                param.setQuestionNo(customeAns.getQueNo());
                param.setAction("getMessageForHomeworkQuestion");
                this.visibleInvisibleChatLoadingDialog(true);
                MathFriendzyHelper.getWorkAreaChatMessage(param, this);
            } else {
                this.setStudentTeacherMsg();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method show the text msg box , if there is some thing in the text
     */
    private void setVisibilityOftextLayoutOnTeacherMsg() {
        this.setVisibilityOfTextLayout(true);
        //set editable false from getHelp
        if (isCallForGetHelp) {
            edtTeacherStudentText.setEnabled(false);
        }
    }

    /**
     * Set visibility of the make it private check box and text
     */
    private void setVisibilityOfMakeItPrivate() {
        if (!isWorkAreaForstudent) {
            btnCheck.setVisibility(Button.INVISIBLE);
            txtMakeItPrivate.setVisibility(TextView.INVISIBLE);
            btnDeleteAndSelectImageOption.setVisibility(Button.INVISIBLE);
        }
    }

    /**
     * Set visibility of teacher answer layout when Activity open for Teacher
     */
    private void setVisibilityOfTeacherAnswerLayout() {
        if (!isWorkAreaForstudent) {
            teacherViewAnswerLayout.setVisibility(RelativeLayout.VISIBLE);
        }
    }

    /**
     * If Activity is open for the Teacher for check homework , then set the answer layout
     * In this layout the student answer and correct answer is displayed
     */
    private void setStudentAnswerLayout() {
        try {
            if (!isWorkAreaForstudent) {
                answerLayout.removeAllViews();
                answerLayout.addView(this.getStudentAnswerLayout(1));
                answerLayout.addView(this.getStudentAnswerLayout(2));

                if (playerAns.getIsGetHelp() == 1) {
                    imgCheckGotHelp.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
                }

                if (playerAns.getFirstTimeWrong() == 1) {
                    imgCheckViewdAnswer.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
                }

                if (playerAns.getTeacherCredit().equalsIgnoreCase(HALF_CREDIT + "")) {
                    txtCreditPercentage.setText(PERCENTAGE_50);
                } else if (playerAns.getTeacherCredit().equalsIgnoreCase(FULL_CREDIT + "")) {
                    txtCreditPercentage.setText(PERCENTAGE_100);
                } else if (playerAns.getTeacherCredit().equalsIgnoreCase(ZERO_CREDIT + "")) {
                    txtCreditPercentage.setText(PERCENTAGE_0);
                } else if (playerAns.getTeacherCredit().equalsIgnoreCase(NONE_CREDITT + "")) {
                    if (playerAns.getIsCorrect() == 1) {
                        txtCreditPercentage.setText(PERCENTAGE_100);
                    } else {
                        txtCreditPercentage.setText(PERCENTAGE_0);
                    }
                }

                txtCreditPercentage.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            if (!isWorkAreaForstudent) {
                                if (!MathFriendzyHelper.isExpireHomwWork(studentDetail.getDueDate())) {
                                    MathFriendzyHelper.showWarningDialogForSomeLongMessage(HomeWorkWorkArea.this,
                                            MathFriendzyHelper.getTreanslationTextById(HomeWorkWorkArea.this,
                                                    "lblCanChangeCreditAfterDueDate"));
                                    return;
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        MathFriendzyHelper.showHomeworkQuizzSelectionDialog
                                (HomeWorkWorkArea.this, new QuizzSelectionInterface() {

                                    //for PERCENTAGE_50 50%
                                    @Override
                                    public void onWordProblemSelected() {
                                        txtCreditPercentage.setText(PERCENTAGE_50);
                                        playerAns.setTeacherCredit(HALF_CREDIT + "");
                                        setViewBackGroundByTeacherCredit(selectedView, playerAns);
                                    }

                                    //for PERCENTAGE_0 0%
                                    @Override
                                    public void onPracticeSkillSelected() {
                                        txtCreditPercentage.setText(PERCENTAGE_0);
                                        playerAns.setTeacherCredit(ZERO_CREDIT + "");
                                        setViewBackGroundByTeacherCredit(selectedView, playerAns);
                                    }

                                    //for PERCENTAGE_100 100%
                                    @Override
                                    public void onCustomeHomeworkSelected() {
                                        txtCreditPercentage.setText(PERCENTAGE_100);
                                        playerAns.setTeacherCredit(FULL_CREDIT + "");
                                        setViewBackGroundByTeacherCredit(selectedView, playerAns);
                                    }
                                }, PERCENTAGE_0, PERCENTAGE_50, PERCENTAGE_100);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set the view background according to the teacher credit
     *
     * @param playerAns
     */
    private void setViewBackGroundByTeacherCredit(View view, CustomePlayerAns playerAns) {

        try {
            //for save view
            selectedView = view;

            if (playerAns.getAns().length() > 0) {
                if (playerAns.getTeacherCredit().equals("1")) {//for no credit
                    if (playerAns.getIsCorrect() == 0) {
                        view.setBackgroundColor(getResources().getColor(R.color.ZERO_CREDIT));
                    } else {
                        if (!isWorkAreaForstudent) {
                            if (playerAns.getFirstTimeWrong() == 1) {
                                view.setBackgroundColor(getResources()
                                        .getColor(R.color.STUDENT_VIEW_ANSWER_COLOR));
                            }
                        }
                    }
                } else if (playerAns.getTeacherCredit().equals("2")) {//for zero credit
                    view.setBackgroundColor(getResources().getColor(R.color.ZERO_CREDIT));
                } else if (playerAns.getTeacherCredit().equals("3")) {//half credit
                    view.setBackgroundColor(getResources().getColor(R.color.HALF_CREDIT));
                } else if (playerAns.getTeacherCredit().equals("4")) {//full credit
                    if (!isWorkAreaForstudent) {
                        if (playerAns.getFirstTimeWrong() == 1) {
                            view.setBackgroundColor(getResources()
                                    .getColor(R.color.STUDENT_VIEW_ANSWER_COLOR));
                        } else {
                            view.setBackgroundColor(getResources().getColor(R.color.FULL_CREDIT));
                        }
                    } else {
                        view.setBackgroundColor(getResources().getColor(R.color.FULL_CREDIT));
                    }
                }
            }else{
                view.setBackgroundColor(getResources().getColor(R.color.FULL_CREDIT));
            }
            if (!isWorkAreaForstudent) {
                this.updateCreditByTeacherForStudentQuestion(playerAns.getTeacherCredit());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * For fillInType
     *
     * @param customeAns
     * @param playerAns
     * @param forStudentAns
     */
    private void setFillInLayout(CustomeAns customeAns, CustomePlayerAns playerAns,
                                 int forStudentAns, ViewHolder viewHolder) {

        if (customeAns.getFillInType() == FILL_IN_TYPE) {
            viewHolder.fillInLayout.setVisibility(RelativeLayout.VISIBLE);
            viewHolder.multipleChoiceLayout.setVisibility(RelativeLayout.GONE);
            viewHolder.trueFalseLayout.setVisibility(RelativeLayout.GONE);

            if (forStudentAns == 1) {//for player answer
                viewHolder.txtStudentAnswer.setText(customeAns.getQueNo() + "." +
                        lblStudentAnswer + ":");
                //viewHolder.ansBox.setText(playerAns.getAns()); //siddhiinfosoft
                //this.setstudentansbox(viewHolder.ll_ans_box, playerAns.getAns());
                //Added siddhiinfosoft
                this.setstudentansbox(viewHolder.ll_ans_box, playerAns.getAns(), playerAns.getIsAnswerAvailable());

                this.setViewBackGroundByTeacherCredit(viewHolder.mainLayout, playerAns);
            } else if (forStudentAns == 2) {//for correct answer
                viewHolder.txtStudentAnswer.setText(customeAns.getQueNo() + "." +
                        lblcorrectAns + ":");
                //viewHolder.ansBox.setText(customeAns.getCorrectAns()); //siddhiinfosoft
                //this.setstudentansbox(viewHolder.ll_ans_box, customeAns.getCorrectAns());
                //Added siddhiinfosoft
                this.setstudentansbox(viewHolder.ll_ans_box, customeAns.getCorrectAns(), customeAns.getIsAnswerAvailable());


            }

            if (customeAns.getIsLeftUnit() == 1) {
                viewHolder.txtLeftUnit.setText(customeAns.getAnsSuffix());
                viewHolder.txtRighttUnit.setText("");
            } else {
                viewHolder.txtLeftUnit.setText("");
                viewHolder.txtRighttUnit.setText(customeAns.getAnsSuffix());
            }
        }
    }

    //siddhiinfosoft BELOW TWO METHODS ADDED BY SIDDHIINFOSOFT

    public void setstudentansbox(final LinearLayout ll_ans_box, final
    String strAns, final int isansavail) {
        try {
            ll_ans_box.setTag(R.id.first, "1");
            ll_ans_box.setTag(R.id.second, "main");
            if (isansavail == MathFriendzyHelper.NO) {

                final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_textview, null);
                ll_last.setTag("vtext");
                final TextView tv = (TextView) ll_last.findViewById(R.id.tv);
                tv.setText(lblAnswerOnWorkAreaCreditGiven);

//                tv.setOnClickListener(new OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        MathFriendzyHelper.showWarningDialog(HomeWorkWorkArea.this,
//                                lblAnswerOnWorkAreaCreditGiven);
//                    }
//                });

                ll_ans_box.addView(ll_last);

            } else {

                if (strAns.trim().length() != 0) {
                    setstudentansboxsub(strAns, ll_ans_box);
                }

            }

        } catch (Exception e) {

        }
    }

    /*public void setstudentansboxsub(final String str_ans, final LinearLayout ll_ans_box) {

        String[] str_1 = str_ans.split(ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first));

        String sss = String.valueOf(ll_ans_box.getTag(R.id.first));
        int tag_main_ll = Integer.valueOf(sss);

        for (int i = 0; i < str_1.length; i++) {

            String str_1_value = str_1[i];

            if (str_1_value.startsWith("text")) {

                str_1_value = str_1_value.substring(5);

                String sss1 = String.valueOf(ll_ans_box.getTag(R.id.first));
                int tag1 = Integer.valueOf(sss1);
                String tag2 = String.valueOf(ll_ans_box.getTag(R.id.second));
                int text_size = HomeWorkWorkArea.this.getResources().getInteger(R.integer.kb_textsize_main_int);

                try {

                    if (tag2.equals("super_top") || tag2.equals("sub_bottom") || tag2.equals("ss_top") ||
                            tag2.equals("ss_bottom") || tag2.equals("nth_top") || tag2.equals("lim_left") || tag2.equals("lim_right")) {
                        text_size = (text_size - 1) - (tag1 + 1);
                    } else if (tag2.equals("frac_top") || tag2.equals("frac_bottom") || tag2.equals("sqrt_c") || tag2.equals("nth_bottom")
                            || tag2.equals("parenthesis_center") || tag2.equals("absolute_center")
                            || tag2.equals("line_center") || tag2.equals("lsegment_center")
                            || tag2.equals("ray_center") || tag2.equals("arc_center")) {
                        text_size = text_size - (tag1 + 1);
                    }

                } catch (Exception e) {

                }

                final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                ll_last.setTag("text");
                final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                this.replacesignfromchar(str_1_value, ed_centre);
                ed_centre.setMinWidth(20);
                ed_centre.setTextSize(text_size);

                ll_ans_box.addView(ll_last);

            }
            if (str_1_value.startsWith("fraction")) {

                str_1_value = str_1_value.substring(8);

                final LinearLayout ll_fraction = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_fraction, null);
                final LinearLayout ll_fraction_top = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_top);
                final LinearLayout ll_fraction_bottom = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_bottom);
                ll_fraction_top.setTag(R.id.first, (tag_main_ll + 1));
                ll_fraction_bottom.setTag(R.id.first, (tag_main_ll + 1));
                ll_fraction_top.setTag(R.id.second, "frac_top");
                ll_fraction_bottom.setTag(R.id.second, "frac_bottom");

                final String[] str_2 = str_1_value.split("frac_btm_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_fraction);

                this.setstudentansboxsub(str_2[0], ll_fraction_top);
                this.setstudentansboxsub(str_2[1], ll_fraction_bottom);

            } else if (str_1_value.startsWith("super_script")) {
                str_1_value = str_1_value.substring(12);

                final LinearLayout ll_super_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_superscript, null);
                final LinearLayout ll_top_superscript = (LinearLayout) ll_super_script.findViewById(R.id.ll_top_superscript);
                ll_top_superscript.setTag(R.id.first, (tag_main_ll + 1));
                ll_top_superscript.setTag(R.id.second, "super_top");

                ll_ans_box.addView(ll_super_script);

                this.setstudentansboxsub(str_1_value, ll_top_superscript);

            } else if (str_1_value.startsWith("sub_script")) {
                str_1_value = str_1_value.substring(10);

                final LinearLayout ll_super_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_subscript, null);
                final LinearLayout ll_bottom_subscript = (LinearLayout) ll_super_script.findViewById(R.id.ll_bottom_subscript);
                ll_bottom_subscript.setTag(R.id.first, (tag_main_ll + 1));
                ll_bottom_subscript.setTag(R.id.second, "sub_bottom");

                ll_ans_box.addView(ll_super_script);

                this.setstudentansboxsub(str_1_value, ll_bottom_subscript);

            } else if (str_1_value.startsWith("super_sub_script")) {

                str_1_value = str_1_value.substring(16);

                final LinearLayout ll_super_sub_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_super_sub_script, null);
                final LinearLayout ll_top_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_top_super_sub_script);
                final LinearLayout ll_bottom_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_bottom_super_sub_script);
                ll_top_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                ll_bottom_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                ll_top_super_sub_script.setTag(R.id.second, "ss_top");
                ll_bottom_super_sub_script.setTag(R.id.second, "ss_bottom");

                final String[] str_2 = str_1_value.split("ss_btm_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_super_sub_script);

                this.setstudentansboxsub(str_2[0], ll_top_super_sub_script);
                this.setstudentansboxsub(str_2[1], ll_bottom_super_sub_script);

            } else if (str_1_value.startsWith("sqrt")) {

                str_1_value = str_1_value.substring(4);
                final LinearLayout ll_sqrt = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_square_root, null);
                final LinearLayout ll_center_square_root = (LinearLayout) ll_sqrt.findViewById(R.id.ll_center_square_root);
                ll_center_square_root.setTag(R.id.first, (tag_main_ll + 1));
                ll_center_square_root.setTag(R.id.second, "sqrt_c");

                ll_ans_box.addView(ll_sqrt);

                this.setstudentansboxsub(str_1_value, ll_center_square_root);

            } else if (str_1_value.startsWith("nsqrt")) {

                str_1_value = str_1_value.substring(5);

                final LinearLayout ll_nsqrt = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_nth_root, null);
                final LinearLayout ll_top_nth_root = (LinearLayout) ll_nsqrt.findViewById(R.id.ll_top_nth_root);
                final LinearLayout ll_center_nth_root = (LinearLayout) ll_nsqrt.findViewById(R.id.ll_center_nth_root);
                final ImageView img = (ImageView) ll_nsqrt.findViewById(R.id.img);
                ll_top_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                ll_center_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                ll_top_nth_root.setTag(R.id.second, "nth_top");
                ll_center_nth_root.setTag(R.id.second, "nth_bottom");

                img.getLayoutParams().height = ll_center_nth_root.getHeight();
                img.requestLayout();
                ll_center_nth_root.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                    @Override
                    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                        Log.e("het", "" + v.getHeight());
                        img.getLayoutParams().height = v.getHeight();
                        img.requestLayout();
                    }
                });

                final String[] str_2 = str_1_value.split("nsqrt_center_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_nsqrt);

                this.setstudentansboxsub(str_2[0], ll_top_nth_root);
                this.setstudentansboxsub(str_2[1], ll_center_nth_root);

            } else if (str_1_value.startsWith("lim")) {

                str_1_value = str_1_value.substring(3);

                final LinearLayout ll_lim = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_lim, null);
                final TextView tv_text = (TextView) ll_lim.findViewById(R.id.tv_text);
                tv_text.setText(Html.fromHtml("&#8594;"));
                final LinearLayout ll_lim_left = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_left);
                final LinearLayout ll_lim_right = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_right);
                ll_lim_left.setTag(R.id.first, (tag_main_ll + 1));
                ll_lim_right.setTag(R.id.first, (tag_main_ll + 1));
                ll_lim_left.setTag(R.id.second, "lim_left");
                ll_lim_right.setTag(R.id.second, "lim_right");

                final String[] str_2 = str_1_value.split("lim_rht_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_lim);

                this.setstudentansboxsub(str_2[0], ll_lim_left);
                this.setstudentansboxsub(str_2[1], ll_lim_right);

            } else if (str_1_value.startsWith("parenthesis")) {
                str_1_value = str_1_value.substring(11);

                final LinearLayout ll_super_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_parenthesis, null);
                final LinearLayout ll_parenthesis_center = (LinearLayout) ll_super_script.findViewById(R.id.ll_parenthesis_center);
                ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                ll_parenthesis_center.setTag(R.id.second, "parenthesis_center");

                ll_ans_box.addView(ll_super_script);

                this.setstudentansboxsub(str_1_value, ll_parenthesis_center);

            } else if (str_1_value.startsWith("absolute")) {
                str_1_value = str_1_value.substring(8);

                final LinearLayout ll_super_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_absolute, null);
                final LinearLayout ll_parenthesis_center = (LinearLayout) ll_super_script.findViewById(R.id.ll_absolute_center);
                ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                ll_parenthesis_center.setTag(R.id.second, "absolute_center");

                ll_ans_box.addView(ll_super_script);

                this.setstudentansboxsub(str_1_value, ll_parenthesis_center);

            } else if (str_1_value.startsWith("line") || str_1_value.startsWith("lsegment") || str_1_value.startsWith("ray")
                    || str_1_value.startsWith("arc")) {

                final LinearLayout ll_parenthesis = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_lsegment, null);
                final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_lsegment_center);
                ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                final ImageView img_top = (ImageView) ll_parenthesis.findViewById(R.id.img_top);
                // ll_parenthesis_center.setTag(R.id.second,"absolute_center");

                if (str_1_value.startsWith("line")) {
                    img_top.setImageResource(R.drawable.line_icon);
                    str_1_value = str_1_value.substring(4);
                    ll_parenthesis.setTag("line");
                    ll_parenthesis_center.setTag(R.id.second, "line_center");
                } else if (str_1_value.startsWith("lsegment")) {
                    str_1_value = str_1_value.substring(8);
                    img_top.setImageResource(R.drawable.line_segment);
                    ll_parenthesis.setTag("lsegment");
                    ll_parenthesis_center.setTag(R.id.second, "lsegment_center");
                } else if (str_1_value.startsWith("ray")) {
                    str_1_value = str_1_value.substring(3);
                    img_top.setImageResource(R.drawable.ray_icon);
                    ll_parenthesis.setTag("ray");
                    ll_parenthesis_center.setTag(R.id.second, "ray_center");
                } else if (str_1_value.startsWith("arc")) {
                    str_1_value = str_1_value.substring(3);
                    img_top.setImageResource(R.drawable.arc_icon);
                    ll_parenthesis.setTag("arc");
                    ll_parenthesis_center.setTag(R.id.second, "arc_center");
                }

                ll_ans_box.addView(ll_parenthesis);

                this.setstudentansboxsub(str_1_value, ll_parenthesis_center);

            }

        }
    }*/


    private String replacesignfromchar(final String str, final EditTextBlink ed) {
        String str_final = str;

        try {

            String str_plus = "" + Html.fromHtml("&#43;");
            String str_minus = "" + Html.fromHtml("&#8722;");
            String str_equal = "" + Html.fromHtml("&#61;");
            String str_multi = "" + Html.fromHtml("&#215;");
            String str_divide = "" + Html.fromHtml("&#247;");
            String str_union = "" + Html.fromHtml("&#8746;");
            String str_intersection = "" + Html.fromHtml("&#8745;");
            String str_pi = "" + Html.fromHtml("&#960;");
            String str_factorial = "" + Html.fromHtml("&#33;");
            String str_percentage = "" + Html.fromHtml("&#37;");
            String str_goe = "" + Html.fromHtml("&#8805;");
            String str_loe = "" + Html.fromHtml("&#8804;");
            String str_grthan = "" + Html.fromHtml("&#62;");
            String str_lethan = "" + Html.fromHtml("&#60;");
            String str_infinity = "" + Html.fromHtml("&#8734;");
            String str_degree = "" + Html.fromHtml("&#176;");
            String str_xbar = "" + Html.fromHtml("x&#x0304;");
            String str_muxbar = "" + Html.fromHtml("&#956;") + "" + Html.fromHtml("x&#772;");
            String str_sigmaxbar = "" + Html.fromHtml("&#963;") + "" + Html.fromHtml("x&#772;");

            // Added merging siddhi infosoft
            String str_integral = "" + Html.fromHtml("&#8747;");
            String str_summation = "" + Html.fromHtml("&#8721;");
            String str_alpha = "" + Html.fromHtml("&#945;");
            String str_theta = "" + Html.fromHtml("ø");
            String str_mu = "" + Html.fromHtml("&#956;");
            String str_sigma = "" + Html.fromHtml("&#963;");

            String str_beta = "" + Html.fromHtml("&#946;");
            String str_angle = "" + Html.fromHtml("&#8736;");
            String str_mangle = "" + Html.fromHtml("&#8737;");
            String str_sangle = "" + Html.fromHtml("&#8738;");
            String str_rangle = "" + Html.fromHtml("&#8735;");
            String str_triangle = "" + Html.fromHtml("&#9651;");
            String str_rectangle = "" + Html.fromHtml("&#9645;");
            String str_parallelogram = "" + Html.fromHtml("&#9649;");
            String str_perpendicular = "" + Html.fromHtml("&#8869;");
            String str_congruent = "" + Html.fromHtml("&#8773;");
            String str_similarty = "" + Html.fromHtml("&#8764;");
            String str_parallel = "" + Html.fromHtml("&#8741;");

            String str_arcm = "" + Html.fromHtml("&#8242;");
            String str_arcs = "" + Html.fromHtml("&#8243;");

            str_final = str_final.replace("op_plus", str_plus);
            str_final = str_final.replace("op_minus", str_minus);
            str_final = str_final.replace("op_equal", str_equal);
            str_final = str_final.replace("op_multi", str_multi);
            str_final = str_final.replace("op_divide", str_divide);
            str_final = str_final.replace("op_union", str_union);
            str_final = str_final.replace("op_intersection", str_intersection);
            str_final = str_final.replace("op_pi", str_pi);
            str_final = str_final.replace("op_factorial", str_factorial);
            str_final = str_final.replace("op_percentage", str_percentage);
            str_final = str_final.replace("op_goe", str_goe);
            str_final = str_final.replace("op_loe", str_loe);
            str_final = str_final.replace("op_grthan", str_grthan);
            str_final = str_final.replace("op_lethan", str_lethan);
            str_final = str_final.replace("op_infinity", str_infinity);
            str_final = str_final.replace("op_degree", str_degree);
            str_final = str_final.replace("op_xbar", str_xbar);
            str_final = str_final.replace("op_muxbar", str_muxbar);
            str_final = str_final.replace("op_sigmaxbar", str_sigmaxbar);


            // Added merging siddhi infosoft

            str_final = str_final.replace("op_alpha", str_alpha);
            str_final = str_final.replace("op_theta", str_theta);
            str_final = str_final.replace("op_mu", str_mu);
            str_final = str_final.replace("op_sigma", str_sigma);

            str_final = str_final.replace("op_beta", str_beta);
            str_final = str_final.replace("op_angle", str_angle);
            str_final = str_final.replace("op_mangle", str_mangle);
            str_final = str_final.replace("op_sangle", str_sangle);
            str_final = str_final.replace("op_rangle", str_rangle);
            str_final = str_final.replace("op_triangle", str_triangle);
            str_final = str_final.replace("op_rectangle", str_rectangle);
            str_final = str_final.replace("op_parallelogram", str_parallelogram);
            str_final = str_final.replace("op_perpendicular", str_perpendicular);
            str_final = str_final.replace("op_congruent", str_congruent);
            str_final = str_final.replace("op_similarty", str_similarty);
            str_final = str_final.replace("op_parallel", str_parallel);

            str_final = str_final.replace("op_arcm", str_arcm);
            str_final = str_final.replace("op_arcs", str_arcs);

            str_final = str_final.replace("op_integral", str_integral);
            str_final = str_final.replace("op_summation", str_summation);

            for (int i = 0; i < str_final.length(); i++) {

                String ss = String.valueOf(str_final.charAt(i));

                if (ss.equals(str_integral) || ss.equals(str_summation)) {
                    SpannableString ss1 = new SpannableString(ss);
                    ss1.setSpan(new RelativeSizeSpan(1.6f), 0, 1, 0);
                    ed.getText().insert(ed.getSelectionStart(), ss1);
                } else if (ss.equals(str_perpendicular) || ss.equals(str_parallel) ||
                        ss.equals(str_rectangle) || ss.equals(str_parallelogram)) {
                    SpannableString ss1 = new SpannableString(ss);
                    ss1.setSpan(new RelativeSizeSpan(1.2f), 0, 1, 0);
                    ed.getText().insert(ed.getSelectionStart(), ss1);
                } else if (ss.equals(str_angle) || ss.equals(str_mangle) ||
                        ss.equals(str_sangle) || ss.equals(str_rangle)) {
                    SpannableString ss1 = new SpannableString(ss);
                    ss1.setSpan(new RelativeSizeSpan(1.5f), 0, 1, 0);
                    ed.getText().insert(ed.getSelectionStart(), ss1);
                } else {
                    ed.getText().insert(ed.getSelectionStart(), ss);
                }

                if (ss.equals(str_integral)) {
                    ed.getText().insert(ed.getSelectionStart(), " ");
                }

            }
        } catch (Exception e) {

        }

        return str_final;
    }

    /**
     * For True False or Yes No Type
     *
     * @param customeAns
     * @param playerAns
     * @param forStudentAns
     * @param viewHolder
     */
    private void setTrueFalseOrYesNoLayout(CustomeAns customeAns, CustomePlayerAns playerAns,
                                           int forStudentAns, ViewHolder viewHolder) {
        if (customeAns.getAnswerType() == TRUE_FALSE_ANS_TYPE
                || customeAns.getAnswerType() == YES_NO_ANS_TYPE) {
            viewHolder.fillInLayout.setVisibility(RelativeLayout.GONE);
            viewHolder.multipleChoiceLayout.setVisibility(RelativeLayout.GONE);
            viewHolder.trueFalseLayout.setVisibility(RelativeLayout.VISIBLE);

            if (customeAns.getAnswerType() == TRUE_FALSE_ANS_TYPE) {
                viewHolder.ansTrue.setText(ActMultipleChoiceAnsSheet.TRUE);
                viewHolder.ansFalse.setText(ActMultipleChoiceAnsSheet.FALSE);
            } else if (customeAns.getAnswerType() == YES_NO_ANS_TYPE) {
                viewHolder.ansTrue.setText(ActMultipleChoiceAnsSheet.YES);
                viewHolder.ansFalse.setText(ActMultipleChoiceAnsSheet.NO);
            }

            if (forStudentAns == 1) {//for player answer
                viewHolder.txtStudentAnswer.setText(customeAns.getQueNo() + "." +
                        lblStudentAnswer + ":");
                if (customeAns.getAnswerType() == TRUE_FALSE_ANS_TYPE) {
                    if (playerAns.getAns().equalsIgnoreCase
                            (ActMultipleChoiceAnsSheet.TRUE)) {
                        viewHolder.ansTrue.setBackgroundResource
                                (R.drawable.checked_box_home_work);
                        viewHolder.ansFalse.setBackgroundResource
                                (R.drawable.unchecked_box_home_work);
                    } else if (playerAns.getAns().equalsIgnoreCase
                            (ActMultipleChoiceAnsSheet.FALSE)) {
                        viewHolder.ansTrue.setBackgroundResource
                                (R.drawable.unchecked_box_home_work);
                        viewHolder.ansFalse.setBackgroundResource
                                (R.drawable.checked_box_home_work);
                    }
                } else if (customeAns.getAnswerType() == YES_NO_ANS_TYPE) {
                    if (playerAns.getAns().equalsIgnoreCase
                            (ActMultipleChoiceAnsSheet.YES)) {
                        viewHolder.ansTrue.setBackgroundResource
                                (R.drawable.checked_box_home_work);
                        viewHolder.ansFalse.setBackgroundResource
                                (R.drawable.unchecked_box_home_work);
                    } else if (playerAns.getAns().equalsIgnoreCase
                            (ActMultipleChoiceAnsSheet.NO)) {
                        viewHolder.ansTrue.setBackgroundResource
                                (R.drawable.unchecked_box_home_work);
                        viewHolder.ansFalse.setBackgroundResource
                                (R.drawable.checked_box_home_work);
                    }
                }
                this.setViewBackGroundByTeacherCredit(viewHolder.mainLayout, playerAns);
            } else if (forStudentAns == 2) {//for correct answer
                viewHolder.txtStudentAnswer.setText(customeAns.getQueNo() + "." +
                        lblcorrectAns + ":");
                if (customeAns.getAnswerType() == TRUE_FALSE_ANS_TYPE) {
                    if (customeAns.getCorrectAns().equalsIgnoreCase
                            (ActMultipleChoiceAnsSheet.TRUE)) {
                        viewHolder.ansTrue.setBackgroundResource
                                (R.drawable.checked_box_home_work);
                        viewHolder.ansFalse.setBackgroundResource
                                (R.drawable.unchecked_box_home_work);
                    } else if (customeAns.getCorrectAns().equalsIgnoreCase
                            (ActMultipleChoiceAnsSheet.FALSE)) {
                        viewHolder.ansTrue.setBackgroundResource
                                (R.drawable.unchecked_box_home_work);
                        viewHolder.ansFalse.setBackgroundResource
                                (R.drawable.checked_box_home_work);
                    }
                } else if (customeAns.getAnswerType() == YES_NO_ANS_TYPE) {
                    if (customeAns.getCorrectAns().equalsIgnoreCase
                            (ActMultipleChoiceAnsSheet.YES)) {
                        viewHolder.ansTrue.setBackgroundResource
                                (R.drawable.checked_box_home_work);
                        viewHolder.ansFalse.setBackgroundResource
                                (R.drawable.unchecked_box_home_work);
                    } else if (customeAns.getCorrectAns().equalsIgnoreCase
                            (ActMultipleChoiceAnsSheet.NO)) {
                        viewHolder.ansTrue.setBackgroundResource
                                (R.drawable.unchecked_box_home_work);
                        viewHolder.ansFalse.setBackgroundResource
                                (R.drawable.checked_box_home_work);
                    }
                }
            }
        }
    }


    /**
     * For Multiple choice layout
     *
     * @param customeAns
     * @param playerAns
     * @param forStudentAns
     * @param viewHolder
     */
    private void setMultipleChoiceLayout(CustomeAns customeAns, CustomePlayerAns playerAns,
                                         int forStudentAns, ViewHolder viewHolder) {
        if (customeAns.getAnswerType() == MULTIPLE_CHOICE_ANS_TYPE) {
            viewHolder.fillInLayout.setVisibility(RelativeLayout.GONE);
            viewHolder.multipleChoiceLayout.setVisibility(RelativeLayout.VISIBLE);
            viewHolder.trueFalseLayout.setVisibility(RelativeLayout.GONE);

            if (forStudentAns == 1) {//for student answer
                viewHolder.txtStudentAnswer.setText(customeAns.getQueNo() + "." +
                        lblStudentAnswer + ":");
                this.setViewBackGroundByTeacherCredit(viewHolder.mainLayout, playerAns);
            } else if (forStudentAns == 2) {//for correct answer
                viewHolder.txtStudentAnswer.setText(customeAns.getQueNo() + "." +
                        lblcorrectAns + ":");
            }

            String[] commaSepratedOption = MathFriendzyHelper.
                    getCommaSepratedOption(customeAns.getOptions(), ",");
            for (int i = 0; i < commaSepratedOption.length; i++) {
                viewHolder.optionLayout.addView(this.getMultipleOptionLayout
                        (commaSepratedOption[i], customeAns, playerAns, forStudentAns));
            }
        }
    }

    /**
     * Multiple choice option with selection
     *
     * @param optionText
     * @param forStudentAns
     * @return
     */
    @SuppressLint("InflateParams")
    private View getMultipleOptionLayout(String optionText, CustomeAns customeAns,
                                         CustomePlayerAns playerAns, int forStudentAns) {
        View view = LayoutInflater.from(this).inflate(R.layout.check_home_work_option_layout
                , null);
        TextView option = (TextView) view.findViewById(R.id.option);
        option.setText(optionText);
        if (forStudentAns == 1) {//1 for student answer
            if (playerAns.getAns().contains(optionText)) {
                option.setBackgroundResource(R.drawable.checked_small_box_home_work);
            }
        } else if (forStudentAns == 2) {//2 for correct answer
            if (customeAns.getCorrectAns().contains(optionText)) {
                option.setBackgroundResource(R.drawable.checked_small_box_home_work);
            }
        }
        return view;
    }


    /**
     * Return the inflated answer layout , for the student answer and correct answer display
     *
     * @param forStudentAns
     * @return
     */
    @SuppressLint("InflateParams")
    private View getStudentAnswerLayout(int forStudentAns) {
        View view = LayoutInflater.from(this)
                .inflate(R.layout.kb_check_hw_workarea_answer_layout, null);

        ViewHolder viewHolder = new ViewHolder();
        viewHolder.mainLayout = (RelativeLayout) view.findViewById(R.id.mainLayout);
        viewHolder.txtStudentAnswer = (TextView) view.findViewById(R.id.txtStudentAnswer);
        viewHolder.fillInLayout = (RelativeLayout) view.findViewById(R.id.fillInLayout);
        viewHolder.txtLeftUnit = (TextView) view.findViewById(R.id.txtLeftUnit);
        viewHolder.ansBox = (TextView) view.findViewById(R.id.ansBox);
        viewHolder.txtRighttUnit = (TextView) view.findViewById(R.id.txtRighttUnit);
        viewHolder.multipleChoiceLayout = (RelativeLayout) view.findViewById(R.id.multipleChoiceLayout);
        viewHolder.optionLayout = (LinearLayout) view.findViewById(R.id.optionLayout);
        viewHolder.trueFalseLayout = (RelativeLayout) view.findViewById(R.id.trueFalseLayout);
        viewHolder.ansTrue = (TextView) view.findViewById(R.id.ansTrue);
        viewHolder.ansFalse = (TextView) view.findViewById(R.id.ansFalse);
        viewHolder.ll_ans_box = (LinearLayout) view.findViewById(R.id.ll_ans_box);

        this.setFillInLayout(customeAns, playerAns, forStudentAns, viewHolder);
        this.setTrueFalseOrYesNoLayout(customeAns, playerAns, forStudentAns, viewHolder);
        this.setMultipleChoiceLayout(customeAns, playerAns, forStudentAns, viewHolder);
        return view;
    }


    /**
     * Get the intent data which are set in previous screen
     */
    private void getIntentValues() {
        //for adding tutor tab in work area
        isTutorWorkArea = this.getIntent().getBooleanExtra("isTutorWorkArea", false);
        findTutorDetail = (FindTutorResponse) this.getIntent().getSerializableExtra("findTutorDetail");
        //end for adding tutor work area


        isCallForGetHelp = this.getIntent().getBooleanExtra("isCallForGetHelp", false);
        if (isCallForGetHelp) {
            ratingStar = this.getIntent().getIntExtra("ratingStar", 0);
            //customeAns = (CustomeAns) this.getIntent().getSerializableExtra("customeAns");
            student = (GetHelpStudentListResponse) this.getIntent().getSerializableExtra("helperStudent");
        }

        try {
            customeResult = (CustomeResult) this.getIntent().getSerializableExtra("customeDetail");
            homeWorkQuizzData = (GetHomeWorkForStudentWithCustomeResponse)
                    this.getIntent().getSerializableExtra("homeWorkQuizzData");
        } catch (Exception e) {
            e.printStackTrace();
        }

        fileName = this.getIntent().getStringExtra("fileName");
        isFileExist = this.getIntent().getBooleanExtra("isFileExist", false);
        isCallForUpdateAns = this.getIntent().getBooleanExtra("isCallForUpdateAns", false);
        //correctAns = this.getIntent().getStringExtra("correctAns");
        teacherStudentMsg = this.getIntent().getStringExtra("playerMsg");
        workareapublic = this.getIntent().getIntExtra("workareapublic", 1);
        //playerAns  = (CustomePlayerAns) this.getIntent().getSerializableExtra("playerAns");

        //For Activity open for Teacher Check homework
        if (this.getIntent().getBooleanExtra("isCalledFortecherCheckHW", false)) {
            isWorkAreaForstudent = false;
            studentDetail = (GetDetailOfHomeworkWithCustomeResponse) this.getIntent()
                    .getSerializableExtra("studentDetail");
        }

        customeAns = (CustomeAns) this.getIntent().getSerializableExtra("customeAns");
        playerAns = (CustomePlayerAns) this.getIntent().getSerializableExtra("playerAns");

        //for view question image
        playerAnsQuestionImage = this.getIntent().getStringExtra("playerAndQuestionImage");
        isImageCrop = this.getIntent().getBooleanExtra("isImageCrop", false);
        questionImageName = this.getIntent().getStringExtra("questionImageName");
        if (isImageCrop) {
            cropImageUriFromPdf = this.getIntent().getStringExtra("cropImageUriFromPdf");
            questionImageName = this.getIntent().getStringExtra("questionImageName");
        }
        chatRequestId = this.getIntent().getIntExtra("chatRequestId", 0);

        //isExpireQuizz = this.getIntent().getBooleanExtra("isExpireQuizz" , false);
        this.initializeAddUrlList();
    }

    /**
     * Set visibility of tutor tab
     */
    private void setVisibilityOfTutorSessionTab() {
        if (chatRequestId > 0) {
            layoutTutorTab.setVisibility(RelativeLayout.VISIBLE);
        } else {
            layoutTutorTab.setVisibility(RelativeLayout.GONE);
        }
    }

    @Override
    protected void setWidgetsReferences() {
        viewGroup = (RelativeLayout) findViewById(R.id.viewGroup);
        layRough = (LinearLayout) findViewById(R.id.layoutRough);
        clearPaint = (Button) findViewById(R.id.clearPaint);
        drawPaint = (Button) findViewById(R.id.draw);
        txtTitleScreen = (TextView) findViewById(R.id.txtTitleScreen);

        btnText = (Button) findViewById(R.id.btnText);
        btnCheck = (Button) findViewById(R.id.btnCheck);

        rateMeLayout = (RelativeLayout) findViewById(R.id.rateMeLayout);
        editLayout = (RelativeLayout) findViewById(R.id.editLayout);
        studentTeacherLayout = (RelativeLayout) findViewById(R.id.studentTeacherLayout);

        txtRateMe = (TextView) findViewById(R.id.txtRateMe);
        imgStar1 = (ImageView) findViewById(R.id.imgStar1);
        imgStar2 = (ImageView) findViewById(R.id.imgStar2);
        imgStar3 = (ImageView) findViewById(R.id.imgStar3);
        imgStar4 = (ImageView) findViewById(R.id.imgStar4);
        imgStar5 = (ImageView) findViewById(R.id.imgStar5);

        //for teacher student text input
        edtTeacherStudentText = (EditText) findViewById(R.id.edtTeacherStudentText);
        btnCross = (Button) findViewById(R.id.btnCross);

        //edtTeacherStudentText.setText(teacherStudentMsg);

        txtMakeItPrivate = (TextView) findViewById(R.id.txtMakeItPrivate);

        btnTextForGetHelp = (Button) findViewById(R.id.btnTextForGetHelp);
        btnScroll = (Button) findViewById(R.id.btnScroll);

        lstteacherStudentMsgList = (ListView) findViewById(R.id.teacherStudentMsgList);
        btnSend = (Button) findViewById(R.id.btnSend);

        if (!isWorkAreaForstudent) {
            edtTeacherStudentText.setTextColor(getResources().getColor(R.color.PURPULE));
        }

        //for Activity open for teacher for check homework
        teacherViewAnswerLayout = (RelativeLayout) findViewById(R.id.teacherViewAnswerLayout);
        answerLayout = (LinearLayout) findViewById(R.id.answerLayout);

        txtGotHelp = (TextView) findViewById(R.id.txtGotHelp);
        txtViewedAnswer = (TextView) findViewById(R.id.txtViewedAnswer);
        txtCredit = (TextView) findViewById(R.id.txtCredit);
        txtCreditPercentage = (TextView) findViewById(R.id.txtCreditPercentage);
        imgCheckGotHelp = (ImageView) findViewById(R.id.imgCheckGotHelp);
        imgCheckViewdAnswer = (ImageView) findViewById(R.id.imgCheckViewdAnswer);
        //imgQuestionImage = (ImageView) findViewById(R.id.imgQuestionImage);
        btnPaste = (Button) findViewById(R.id.btnPaste);

        //for click scrolling
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        btnScrollUp = (Button) findViewById(R.id.btnScrollUp);
        btnScrollDown = (Button) findViewById(R.id.btnScrollDown);

        btnScroll.setVisibility(Button.GONE);

        //for undo redo
        btnUndo = (Button) findViewById(R.id.btnUndo);
        btnRedo = (Button) findViewById(R.id.btnRedo);
        btnUndo.setVisibility(Button.GONE);
        btnRedo.setVisibility(Button.GONE);

        //for chat loading
        loadingChatProgressDialog = (ProgressBar) findViewById(R.id.loadingChatProgressDialog);

        //for eraser circle
        imgEraser = (ImageView) findViewById(R.id.imgEraser);

        //for adding tab on the top bar
        myWorkAreaContainer = (RelativeLayout) findViewById(R.id.myWorkAreaContainer);
        fragmentContainer = (RelativeLayout) findViewById(R.id.fragmentContainer);
        myWorkArea = (Button) findViewById(R.id.myWorkArea);
        btnTutorWorkArea = (Button) findViewById(R.id.btnTutorWorkArea);

        relativeTopBar = (RelativeLayout) findViewById(R.id.relativeTopBar);

        //for rate dialog
        tutorContentLayout = (LinearLayout) findViewById(R.id.tutorContentLayout);
        rateUsStar = (ImageView) findViewById(R.id.rateUsStar);
        imgDisconnect = (ImageView) findViewById(R.id.imgDisconnect);
        imgOnline = (ImageView) findViewById(R.id.imgOnline);
        imgHand = (ImageView) findViewById(R.id.imgHand);
        imgMegnifier = (ImageView) findViewById(R.id.imgMegnifier);

        layoutTutorTab = (RelativeLayout) findViewById(R.id.layoutTutorTab);

        //for image selection option
        imgStudentImage = (ImageView) findViewById(R.id.imgStudentImage);
        studentImageLayout = (RelativeLayout) findViewById(R.id.studentImageLayout);
        btnDeleteAndSelectImageOption = (Button) findViewById(R.id.btnDeleteAndSelectImageOption);
        bottomLayout = (RelativeLayout) findViewById(R.id.layout);
        workAreaContentLayout = (RelativeLayout) findViewById(R.id.workAreaContentLayout);
        resizeView.setWidgetsReferences();
        resizeView.setOtherView(studentTeacherLayout, relativeTopBar, bottomLayout);

        //for second work area
        secondWorkAreaContainer = (RelativeLayout) findViewById(R.id.secondWorkAreaContainer);
        btnSecondWorkArea = (Button) findViewById(R.id.btnSecondWorkArea);

        if (isWorkAreaForstudent) {
            txtTitleScreen.setVisibility(TextView.GONE);
        } else {
            this.setVisibilityOfSecondWorkAreaTab();
        }

        //to hide the answer layout from teacher check homework
        imgCloseAnswerLayout = (Button) findViewById(R.id.imgCloseAnswerLayout);
        btnCloseViewAnswer = (Button) findViewById(R.id.btnCloseViewAnswer);

        //for add google doc or web link
        btnAddLinks = (Button) findViewById(R.id.btnAddLinks);


        //student put answer on work area
        studentPutAnswerLayout = (RelativeLayout) findViewById(R.id.studentPutAnswerLayout);
        checkHomeWorkListLayout = (LinearLayout) findViewById(R.id.checkHomeWorkListLayout);
        keyboardLayout = (RelativeLayout) findViewById(R.id.rl_math_keyboard);
        btnShowInputAnswer = (Button) findViewById(R.id.btnShowInputAnswer);

        this.setcustomkeyboardwidgetsreference();

        //audio calling changes
        btnAudioCall = (Button) findViewById(R.id.btnAudioCall);
    }

    @Override
    protected void setListenerOnWidgets() {
        clearPaint.setOnClickListener(this);
        drawPaint.setOnClickListener(this);
        btnText.setOnClickListener(this);
        btnCheck.setOnClickListener(this);

        imgStar1.setOnClickListener(this);
        imgStar2.setOnClickListener(this);
        imgStar3.setOnClickListener(this);
        imgStar4.setOnClickListener(this);
        imgStar5.setOnClickListener(this);

        //for teacher student text input
        btnCross.setOnClickListener(this);
        btnTextForGetHelp.setOnClickListener(this);
        btnScroll.setOnClickListener(this);
        btnSend.setOnClickListener(this);

        btnPaste.setOnClickListener(this);
        imgOnline.setOnClickListener(this);
        imgHand.setOnClickListener(this);

        MathFriendzyHelper.scrollUp(btnScrollUp, scrollView, this);
        MathFriendzyHelper.scrollDown(btnScrollDown, scrollView, this);

        //for undo redo
        btnRedo.setOnClickListener(this);
        btnUndo.setOnClickListener(this);


        //for adding tab on the top bar
        myWorkArea.setOnClickListener(this);
        btnTutorWorkArea.setOnClickListener(this);

        //for tutor rate us session
        rateUsStar.setOnClickListener(this);
        imgDisconnect.setOnClickListener(this);
        imgMegnifier.setOnClickListener(this);

        //for image selection option
        btnDeleteAndSelectImageOption.setOnClickListener(this);
        btnSecondWorkArea.setOnClickListener(this);

        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return isScrollEnable;
            }
        });

        //to hide the answer layout from teacher check homework
        imgCloseAnswerLayout.setOnClickListener(this);
        btnCloseViewAnswer.setOnClickListener(this);

        //for add google doc or web link
        btnAddLinks.setOnClickListener(this);


        //For Student Input Answer Change
        this.setListenerOnNewKeys();
        btnShowInputAnswer.setOnClickListener(this);

        //audio calling changes
        //btnAudioCall.setOnClickListener(this);
    }



    /**
     * Student formatted string
     *
     * @param edtString
     * @return
     */
    private String getStudentFormatMsg(String edtString) {
        if (teacherStudentMsg == null)
            teacherStudentMsg = new String("");

        if (teacherStudentMsg != null
                && teacherStudentMsg.length() > 0) {
            teacherStudentMsg = teacherStudentMsg +
                    MathFriendzyHelper.MSG_DELIMETER +
                    this.getStudentMsg(edtString);
        } else {
            teacherStudentMsg = this.getStudentMsg(edtString);
        }
        return teacherStudentMsg;
    }

    /**
     * Return teacher formatted string
     *
     * @param edtString
     * @return
     */
    private String getTeacherFormatMsg(String edtString) {
        if (teacherStudentMsg == null)
            teacherStudentMsg = new String("");

        if (teacherStudentMsg != null
                && teacherStudentMsg.length() > 0) {
            teacherStudentMsg = teacherStudentMsg +
                    MathFriendzyHelper.MSG_DELIMETER +
                    this.getTeacherMsg(edtString);
        } else {
            teacherStudentMsg = this.getTeacherMsg(edtString);
        }
        return teacherStudentMsg;
    }

    /**
     * Return student msg with delimeter
     *
     * @param edtString
     * @return
     */
    private String getStudentMsg(String edtString) {
        return MathFriendzyHelper.STUDENT_MSG_PREFF +
                MathFriendzyHelper.STUDENT_TEACHER_MSG_DELIMETER +
                MathFriendzyHelper.COLON_DELIMETER + " " +
                edtString;
    }

    /**
     * Return the teacher msg with delimeter
     *
     * @param edtString
     * @return
     */
    private String getTeacherMsg(String edtString) {
        return MathFriendzyHelper.TEACHER_MSG_PREFF +
                MathFriendzyHelper.STUDENT_TEACHER_MSG_DELIMETER +
                MathFriendzyHelper.COLON_DELIMETER + " " +
                edtString;
    }

    /**
     * Set the teacher student msg
     */
    private void setStudentTeacherMsg() {
        //teacherStudentMsg = "Student&&&: How r u?$$$Teacher&&&: Fine thank you.$$$Student&&&: Hey you.";
        if (teacherStudentMsg != null &&
                teacherStudentMsg.length() > 0) {
            ArrayList<String> studemtTeacherMsgList = MathFriendzyHelper
                    .getCommaSepratedOptionInArrayList(teacherStudentMsg,
                            MathFriendzyHelper.FULL_MESSAGE_DELIMETER);
            adapter = new HomeWorkMsgAdapter(this, studemtTeacherMsgList);
            lstteacherStudentMsgList.setAdapter(adapter);
            lstteacherStudentMsgList.setSelection
                    (lstteacherStudentMsgList.getAdapter().getCount() - 1);
        }
    }

    @Override
    protected void setTextFromTranslation() {
        Translation translate = new Translation(this);
        translate.openConnection();
        txtTitleScreen.setText(translate.getTranselationTextByTextIdentifier("titleWorkArea"));
        txtRateMe.setText(translate.getTranselationTextByTextIdentifier("lblRateMe"));
        txtMakeItPrivate.setText(translate.getTranselationTextByTextIdentifier("lblMakeItPrivate"));
        lblMakeItPrivate = translate.getTranselationTextByTextIdentifier("lblMakeItPrivate") + "?";
        btnSend.setText(translate.getTranselationTextByTextIdentifier("lblSend"));
        txtGotHelp.setText(translate.getTranselationTextByTextIdentifier("lblGotHelp"));
        txtViewedAnswer.setText(translate.getTranselationTextByTextIdentifier("lblViewedAnswer"));
        txtCredit.setText(translate.getTranselationTextByTextIdentifier("lblCredit"));

        lblStudentAnswer = translate.getTranselationTextByTextIdentifier("lblStudentAnswer");
        lblcorrectAns = translate.getTranselationTextByTextIdentifier("mfLblCorrect")
                + " " + translate.getTranselationTextByTextIdentifier("mfLblAnswers");

        lblWouldYouLikeToDeleteImage = translate.getTranselationTextByTextIdentifier("lblWouldYouLikeToDeleteImage");
        lblNo = translate.getTranselationTextByTextIdentifier("lblNo");
        btnTitleYes = translate.getTranselationTextByTextIdentifier("btnTitleYes");

//Added Siddhiinfosoft
        lblAnswerOnWorkAreaCreditGiven = translate.getTranselationTextByTextIdentifier("lblAnsOnWorkArea");

        //for paste button text
        //if(isTab){
        btnPaste.setText(translate.getTranselationTextByTextIdentifier("lblPaste"));

        lblYouAreNotConnectedWithTutor = translate.getTranselationTextByTextIdentifier
                ("lblYouAreNotConnectedWithTutor");
        lblPleaseEnterQuestionToNotifyTutor = translate.getTranselationTextByTextIdentifier
                ("lblPleaseEnterQuestionToNotifyTutor");
        btnTitleOK = translate.getTranselationTextByTextIdentifier("btnTitleOK");
        btnTitleCancel = translate.getTranselationTextByTextIdentifier("btnTitleCancel");
        btnTutorWorkArea.setText(translate.getTranselationTextByTextIdentifier("lblTutorArea"));
        lblTutorArea = translate.getTranselationTextByTextIdentifier("lblTutorArea");
        lblOrCancelTutorRequest = translate.getTranselationTextByTextIdentifier("lblOrCancelTutorRequest");
        alertWouldYouLikeToStopWaiting = translate.getTranselationTextByTextIdentifier
                ("alertWouldYouLikeToStopWaiting");
        lblYourQuestionWillNotBeSubmitted = translate.getTranselationTextByTextIdentifier
                ("alertYourQuestionWillNotSubmit");
        alertWouldULikeToConnectOther = translate.getTranselationTextByTextIdentifier
                ("alertWouldULikeToConnectOther");

        btnSecondWorkArea.setText(translate.getTranselationTextByTextIdentifier("titleWorkArea"));
        if (isShowSecondTab) {
            myWorkArea.setText(translate.getTranselationTextByTextIdentifier("lblNewWorkArea"));
        } else {
            myWorkArea.setText(translate.getTranselationTextByTextIdentifier("titleWorkArea"));
        }
        alertMessageImagePasteOrNot = translate.getTranselationTextByTextIdentifier
                ("alertMessageImagePasteOrNot");
        lblYouHaveAlreadyRated = translate.getTranselationTextByTextIdentifier("lblYouHaveAlreadyRated");
        alertMsgAbusingWord = translate.getTranselationTextByTextIdentifier("alertMsgAbusingWord");
        //}

        closeAnswersText = translate.getTranselationTextByTextIdentifier("pickerClose") + " "
                + translate.getTranselationTextByTextIdentifier("mfLblAnswers");
        showAnswersText = translate.getTranselationTextByTextIdentifier("lblView") + " "
                + translate.getTranselationTextByTextIdentifier("mfLblAnswers");
        imgCloseAnswerLayout.setText(closeAnswersText);
        translate.closeConnection();
    }

    /**
     * Rate me layout visibility
     */
    private void setVisibilityOfRateMeLayout() {
        if (isCallForGetHelp) {
            rateMeLayout.setVisibility(RelativeLayout.VISIBLE);
            editLayout.setVisibility(RelativeLayout.GONE);
        } else {
            rateMeLayout.setVisibility(RelativeLayout.GONE);
            editLayout.setVisibility(RelativeLayout.VISIBLE);
        }
    }

    /**
     * This method set the rating star
     *
     * @param ratingStar
     */
    private void setRatingstar(int ratingStar) {
        if (isCallForGetHelp) {
            switch (ratingStar) {
                case 0:
                    imgStar1.setBackgroundResource(R.drawable.start_unselected);
                    imgStar2.setBackgroundResource(R.drawable.start_unselected);
                    imgStar3.setBackgroundResource(R.drawable.start_unselected);
                    imgStar4.setBackgroundResource(R.drawable.start_unselected);
                    imgStar5.setBackgroundResource(R.drawable.start_unselected);
                    break;
                case 1:
                    imgStar1.setBackgroundResource(R.drawable.star_selected);
                    imgStar2.setBackgroundResource(R.drawable.start_unselected);
                    imgStar3.setBackgroundResource(R.drawable.start_unselected);
                    imgStar4.setBackgroundResource(R.drawable.start_unselected);
                    imgStar5.setBackgroundResource(R.drawable.start_unselected);
                    break;
                case 2:
                    imgStar1.setBackgroundResource(R.drawable.star_selected);
                    imgStar2.setBackgroundResource(R.drawable.star_selected);
                    imgStar3.setBackgroundResource(R.drawable.start_unselected);
                    imgStar4.setBackgroundResource(R.drawable.start_unselected);
                    imgStar5.setBackgroundResource(R.drawable.start_unselected);
                    break;
                case 3:
                    imgStar1.setBackgroundResource(R.drawable.star_selected);
                    imgStar2.setBackgroundResource(R.drawable.star_selected);
                    imgStar3.setBackgroundResource(R.drawable.star_selected);
                    imgStar4.setBackgroundResource(R.drawable.start_unselected);
                    imgStar5.setBackgroundResource(R.drawable.start_unselected);
                    break;
                case 4:
                    imgStar1.setBackgroundResource(R.drawable.star_selected);
                    imgStar2.setBackgroundResource(R.drawable.star_selected);
                    imgStar3.setBackgroundResource(R.drawable.star_selected);
                    imgStar4.setBackgroundResource(R.drawable.star_selected);
                    imgStar5.setBackgroundResource(R.drawable.start_unselected);
                    break;
                case 5:
                    imgStar1.setBackgroundResource(R.drawable.star_selected);
                    imgStar2.setBackgroundResource(R.drawable.star_selected);
                    imgStar3.setBackgroundResource(R.drawable.star_selected);
                    imgStar4.setBackgroundResource(R.drawable.star_selected);
                    imgStar5.setBackgroundResource(R.drawable.star_selected);
                    break;
            }
        }
    }

    /**
     * This method show the correct ans pop up when user open work area
     * to update ans after seen the correct ans
     */
    private void showCorrectAnsPopUp() {
        if (isCallForUpdateAns) {
            /*MathFriendzyHelper.warningDialog(this, correctAnsForThisProblemText
                    + ": \"" + correctAns + "\". " + youMustNowShowHowText);*/
        }
    }

    /**
     * Return the working bitmap
     *
     * @return
     */
    private Bitmap getWorkingBitmapForAlreadyExistFile() {
        try {
            if (isFileExist) {
                Bitmap workingBitMap = MathFriendzyHelper.getFileByFileNameForHW(this, fileName);
                return workingBitMap;
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Set the paint color
     */
    private void setPaintColor() {
        if (isWorkAreaForstudent) {
            mPaint.setColor(Color.BLACK);
        } else {
            mPaint.setColor(getResources().getColor(R.color.PURPULE));
        }
    }

    /**
     * Set the custom view to the rough layout
     */
    private void setCustomView() {
        alreadyWorkingBitmap = this.getWorkingBitmapForAlreadyExistFile();
        //DisplayMetrics metrics = getBaseContext().getResources().getDisplayMetrics();
        int w = MathFriendzyHelper.drawViewWidth;
        int h = MathFriendzyHelper.drawViewHeight;
        /*int w = metrics.widthPixels;
        int h = metrics.heightPixels;*/

        if (MathFriendzyHelper.isDeviceSizeGreaterThanDraViewLimit) {
            this.setWorkAreaContentViewLayoutParam(w, h);
            this.initializeHeightAndWidthForFixedViewLimit(w, workAreaContentLayout);
            workAreaContentLayout.setBackgroundResource(R.drawable.black_border_transparent_background);
        }

        if (alreadyWorkingBitmap != null) {
            if (alreadyWorkingBitmap.getHeight() > h) {
               /*if(MathFriendzyHelper.isChromeBookRelease){
                    h = h + 1000;
                }else{*/
                //h = alreadyWorkingBitmap.getHeight();
                h = h * 3;
                //}
            } else {
                h = h * 3;
            }
        } else {
            h = h * 3;
        }
        mView = new MyView(this, w, h);
        this.setViewHeightAndWidth(w, h);
        mView.setBackgroundColor(Color.TRANSPARENT);
        mView.setDrawingCacheEnabled(true);
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        this.setPaintColor();
        mPaint.setStrokeWidth(MathFriendzyHelper.DRAW_LINE_THICKNESS);
        mPaint.setStyle(Paint.Style.STROKE);
        layRough.addView(mView);
    }//END setCustomView method

    /**
     * set the main view height and width
     *
     * @param w
     * @param h
     */
    private void setViewHeightAndWidth(int w, int h) {
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(w, h);
        mView.setLayoutParams(param);
        RelativeLayout.LayoutParams rlParam = new RelativeLayout.LayoutParams(w, h);
        studentImageLayout.setLayoutParams(rlParam);
        imgStudentImage.setLayoutParams(rlParam);
    }

    private void setWorkAreaContentViewLayoutParam(int w, int h) {
        ViewGroup.LayoutParams lp = workAreaContentLayout.getLayoutParams();
        lp.width = w;
        workAreaContentLayout.setLayoutParams(lp);
    }

    /*private void setQuestionImageLayoutParam(){

    }*/

    /**
     * Set the checked background image
     */
    private void setCheckedBackground() {
        if (workareapublic == 0) {//0 for private
            btnCheck.setBackgroundResource(R.drawable.lock_icon_ipad);
        } else {
            btnCheck.setBackgroundResource(R.drawable.unlock_icon_ipad);
        }
    }

    /**
     * Call when click on btnText
     */
    private void setVisibilityOfTextLayout(boolean bValue) {
        if (bValue) {
            studentTeacherLayout.setVisibility(RelativeLayout.VISIBLE);
        } else {
            studentTeacherLayout.setVisibility(RelativeLayout.GONE);
        }
    }

    /**
     * This method hide the keyboard
     */
    private void hideKeyBoard() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(edtTeacherStudentText.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Send the text when user click on send
     */
    private void clickOnSend() {
        if (!MathFriendzyHelper.isEmpty(edtTeacherStudentText.getText().toString())) {

            //profanity filter added
            String messageText = edtTeacherStudentText.getText().toString();
            if (MathFriendzyHelper.isProfinityWordExistInString(messageText)) {
                MathFriendzyHelper.showWarningDialog(this, alertMsgAbusingWord);
                edtTeacherStudentText.setText("");
                return;
            }
            //end changes

            this.updateIsWorkAreaUpdated(true);
            if (isWorkAreaForstudent) {
                teacherStudentMsg = getStudentFormatMsg
                        (edtTeacherStudentText.getText().toString());
            } else {
                teacherStudentMsg = getTeacherFormatMsg
                        (edtTeacherStudentText.getText().toString());
            }

            if (adapter != null) {
                if (isWorkAreaForstudent) {
                    adapter.msgList.add(getStudentMsg
                            (edtTeacherStudentText.getText().toString()));
                } else {
                    adapter.msgList.add(getTeacherMsg
                            (edtTeacherStudentText.getText().toString()));
                }
                adapter.notifyDataSetChanged();
                lstteacherStudentMsgList.setSelection
                        (lstteacherStudentMsgList.getAdapter().getCount() - 1);
            } else {//for first time
                setStudentTeacherMsg();
            }
            edtTeacherStudentText.setText("");
        }
    }

    /**
     * If work area open for teacher
     */
    private void setScrollButtonSelected() {
        if (!isWorkAreaForstudent) {
            this.setPencilSelected(true);
            this.setEraserSelected(false);
        }
    }


    /**
     * Set the question image
     */
    private void setQuestionImage() {
        try {
            if (playerAnsQuestionImage != null
                    && playerAnsQuestionImage.length() > 0) {
                questionImageName = playerAnsQuestionImage;
                imgStudentImage.setImageBitmap(MathFriendzyHelper
                        .getFileByFileUri(this, MathFriendzyHelper.getQuestionImageUri
                                (playerAnsQuestionImage)));

                if (this.selectedOption != null && imageOptionSelector != null) {
                    imageOptionSelector.setVisibilityAfterOptionSelectionDone(this.selectedOption);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set the paste button visibility
     */
    private void setVisibilityOfPasteButton() {
        if (isImageCrop) {
            btnPaste.setVisibility(Button.VISIBLE);
        } else {
            btnPaste.setVisibility(Button.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.clearPaint:
                this.setEraserSelected(true);
                this.setPencilSelected(false);
                mPaint.setStrokeWidth(80);
                mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                eraserMode = true;
                isScroll = false;
                break;
            case R.id.draw:
                this.setEraserSelected(false);
                this.setPencilSelected(true);
                mPaint.setStrokeWidth(MathFriendzyHelper.DRAW_LINE_THICKNESS);
                mPaint.setXfermode(null);
                this.setPaintColor();
                eraserMode = false;
                isScroll = false;
                break;
            case R.id.btnText:
                this.setVisibilityOfTextLayout(true);
                break;
            case R.id.btnCheck:
                if (workareapublic == 1) {//1 for public work area and 0 for private
                    MathFriendzyHelper.yesNoConfirmationDialog(this, lblMakeItPrivate,
                            btnTitleYes, lblNo, new YesNoListenerInterface() {
                                @Override
                                public void onYes() {
                                    workareapublic = (workareapublic + 1) % 2;
                                    setCheckedBackground();
                                }

                                @Override
                                public void onNo() {

                                }
                            });
                } else {
                    workareapublic = (workareapublic + 1) % 2;
                    this.setCheckedBackground();
                }
                break;
            case R.id.imgStar1:
                if (this.isAlreadyRated(1))
                    return;
                this.setRatingstar(1);
                this.saveRatingOnServer(1);
                break;
            case R.id.imgStar2:
                if (this.isAlreadyRated(2))
                    return;
                this.setRatingstar(2);
                this.saveRatingOnServer(2);
                break;
            case R.id.imgStar3:
                if (this.isAlreadyRated(3))
                    return;
                this.setRatingstar(3);
                this.saveRatingOnServer(3);
                break;
            case R.id.imgStar4:
                if (this.isAlreadyRated(4))
                    return;
                this.setRatingstar(4);
                this.saveRatingOnServer(4);
                break;
            case R.id.imgStar5:
                if (this.isAlreadyRated(5))
                    return;
                this.setRatingstar(5);
                this.saveRatingOnServer(5);
                break;
            case R.id.btnCross:
                this.setVisibilityOfTextLayout(false);
                this.hideKeyBoard();
                break;
            case R.id.btnTextForGetHelp:
                this.setVisibilityOfTextLayout(true);
                break;
            case R.id.btnScroll:
                isScroll = true;
                clearPaint.setBackgroundResource(R.drawable.eraser);
                drawPaint.setBackgroundResource(R.drawable.pencil);
                btnScroll.setBackgroundResource(R.drawable.grey_up_down);
                break;
            case R.id.btnSend:
                if (!isCallForGetHelp) {
                    this.clickOnSend();
                }
                break;
            case R.id.btnPaste:
                //this.clickOnPaste();
                break;
            case R.id.btnUndo:
                mView.undoDrwaing();
                break;
            case R.id.btnRedo:
                mView.redoDrawing();
                break;
            case R.id.myWorkArea:
                this.clickOnMyWorkArea();
                break;
            case R.id.btnTutorWorkArea:
                if (CommonUtils.isInternetConnectionAvailable(this)) {
                    if (tutorSessionFragment == null) {//for first time click
                        if (chatRequestId > 0) {
                            this.clickOnTutorSeesionWorkArea();
                        } else {
                            MathFriendzyHelper.yesNoConfirmationDialogForLongMessage(this,
                                    lblYouAreNotConnectedWithTutor,
                                    btnTitleYes, lblNo, new YesNoListenerInterface() {
                                        @Override
                                        public void onYes() {
                                            Intent intent = new Intent(HomeWorkWorkArea.this,
                                                    ActFindTutor.class);
                                            startActivityForResult(intent, FIND_TUTOR_REQUEST);
                                        }

                                        @Override
                                        public void onNo() {
                                        }
                                    });
                        }
                    } else {
                        this.clickOnTutorSeesionWorkArea();
                    }
                } else {
                    CommonUtils.showInternetDialog(this);
                }
                break;
            case R.id.rateUsStar:
                if (tutorSessionFragment != null) {
                    tutorSessionFragment.showRateUsDialog();
                }
                break;
            case R.id.imgDisconnect:
                MathFriendzyHelper.yesNoConfirmationDialog(this,
                        alertWouldYouLikeToStopWaiting
                        , btnTitleYes, lblNo,
                        new YesNoListenerInterface() {
                            @Override
                            public void onYes() {
                                stopWaitingForTutor();
                            }

                            @Override
                            public void onNo() {

                            }
                        });
                break;
            case R.id.imgOnline:
                break;
            case R.id.imgHand:
                this.clickOnHand();
                break;
            case R.id.imgMegnifier:
                this.clickOnMegnifier();
                break;
            case R.id.btnDeleteAndSelectImageOption:
                this.clickToSelectAndDeleteImage();
                break;
            case R.id.btnSecondWorkArea:
                this.clickOnSecondWorkArea();
                break;
            case R.id.imgCloseAnswerLayout:
                this.closeStudentAnswerLayout();
                break;
            case R.id.btnCloseViewAnswer:
                this.closeStudentAnswerLayout();
                break;
            case R.id.btnAddLinks:
                this.showAddLinksDialog();
                break;
            case R.id.btnShowInputAnswer:
                this.setVisibilityOfStudentPutAnswerLayout(!this.isVisibleStudentInput);
                break;
            //audio calling changes
            /*case R.id.btnAudioCall:
                this.clickOnAudioCall();
                break;*/

        }
    }

    private void showDialogOnTabChangeAndBackClickForTutorArea(HttpServerRequest httpServerRequest) {
        MathFriendzyHelper.showWarningDialog(this, lblYourQuestionWillNotBeSubmitted, httpServerRequest);
    }


    /**
     * Save rating star for the already played user workarea
     *
     * @param ratingStar
     */
    private void saveRatingOnServer(int ratingStar) {

        SaveRatingStarForHelpWorkAreaParam param = new SaveRatingStarForHelpWorkAreaParam();
        param.setAction("rateWorkArea");
        param.setHWId(homeWorkQuizzData.getHomeWorkId());
        param.setCutomeHWId(customeResult.getCustomHwId() + "");
        param.setQuestionId(customeAns.getQueNo());
        param.setPlayerId(student.getPlayerId());
        param.setRatingStar(ratingStar);
        param.setUserId(student.getUserId());
        param.setSenderUserId(selectedPlayerData.getParentUserId());
        param.setSenderPlayerId(selectedPlayerData.getPlayerid());
        param.setWorkImageName(student.getWorkImageName());

        student.setYourRating(ratingStar);

        new MyAsyckTask(ServerOperation.createPostToSaveStudentWorkAreaHelp(param)
                , null, ServerOperationUtil.SAVE_STUDENT_WORK_AREA_RATING_REQUEST, this,
                this, ServerOperationUtil.SIMPLE_DIALOG, true,
                getString(R.string.please_wait_dialog_msg))
                .execute();
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase,
                               int requestCode) {
        if (requestCode == ServerOperationUtil.SAVE_STUDENT_WORK_AREA_RATING_REQUEST) {
            SaveStudentRatingResponse response = (SaveStudentRatingResponse) httpResponseBase;
            if (response.getResult().equals("success")) {
                student.setRating(response.getAvgRatingStar());
            }
        } else if (requestCode == ServerOperationUtil.GET_WORK_AREA_CHAT_MSG_REQUEST) {
            GetWorkAreaChatResponse response = (GetWorkAreaChatResponse) httpResponseBase;
            this.visibleInvisibleChatLoadingDialog(false);
            if (response.getResult().equalsIgnoreCase("success")
                    && response.getData().length() > 0) {
                teacherStudentMsg = response.getData();
            }
            this.setStudentTeacherMsg();
        } else if (requestCode == ServerOperationUtil.SAVE_WORK_AREA_CHAT_MSG_REQUEST) {
            //code to get save response
        } else if (requestCode == ServerOperationUtil.STOP_WAITING_FOR_TUTOR_REQUEST) {
            goBack();
        }
    }

    /**
     * Return the bitmap view
     *
     * @param w
     * @param h
     * @return
     */
    private Bitmap getBitMap(int w, int h) {
        if (isFileExist) {
            if (alreadyWorkingBitmap != null)
                return Bitmap.createScaledBitmap(alreadyWorkingBitmap, w, h, true);
            return MathFriendzyHelper.createBitmap(w, h);
        } else {
            return MathFriendzyHelper.createBitmap(w, h);
        }
    }

    ////////////******************* Printing view *******************///////////////////
    public class MyView extends View {

        @SuppressLint("NewApi")
        public MyView(Context c, int w, int h) {
            super(c);

            if (android.os.Build.VERSION.SDK_INT >= 11) {
                setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            }
            mBitmap = getBitMap(w, h);
            mCanvas = new Canvas(mBitmap);
            mPath = new Path();
            mBitmapPaint = new Paint(Paint.DITHER_FLAG);
            mBitmapPaint.setXfermode(null);
            //mBitmap.eraseColor(Color.TRANSPARENT);
        }

        @SuppressLint("InlinedApi")
        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
            canvas.drawPath(mPath, mPaint);
        }

        // //////************touching events for painting**************///////
        private float mX, mY;
        private static final float TOUCH_TOLERANCE = 2;

        private void touch_start(float x, float y) {
            mPath.reset();
            mPath.moveTo(x, y);
            mX = x;
            mY = y;
        }

        private void touch_move(float x, float y) {
            float dx = Math.abs(x - mX);
            float dy = Math.abs(y - mY);
            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
                //for erasing the written
                if (eraserMode) {
                    mPath.lineTo(mX, mY);
                    mCanvas.drawPath(mPath, mPaint);
                    mPath.reset();
                    mPath.moveTo(mX, mY);
                }
                //end for erase change
                mX = x;
                mY = y;
            }
        }

        private void touch_up() {
            if (eraserMode)
                mPath.reset();
            else {
                mPath.lineTo(mX, mY);
                // commit the path to our off screen
                mCanvas.drawPath(mPath, mPaint);
                // kill this so we don't double draw
                mPath.reset();
            }
        }

        @SuppressLint("ClickableViewAccessibility")
        @Override
        public boolean onTouchEvent(MotionEvent event) {

            if (isScroll) {
                //Log.e(TAG, "for scrool");
            } else {
                if (!isCallForGetHelp) {//work area open for help not to edit
                    getParent().requestDisallowInterceptTouchEvent(true);
                    isStartDrowing = true;
                    updateIsWorkAreaUpdated(true);
                    float x = (int) event.getX();
                    float y = (int) event.getY();
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            touch_start(x, y);
                            onActionDown(mX, mY);
                            invalidate();
                            break;
                        case MotionEvent.ACTION_MOVE:
                            touch_move(x, y);
                            onActionMove(mX, mY);
                            invalidate();
                            break;
                        case MotionEvent.ACTION_UP:
                            touch_up();
                            onActionUp();
                            invalidate();
                            break;
                    }
                }
            }
            return true;
        } // end of touch events for image

        private void undoDrwaing() {
            if (paths.size() > 0) {
                undonePaths.add(paths.remove(paths.size() - 1));
                invalidate();
            } else {

            }
        }

        private void redoDrawing() {
            if (undonePaths.size() > 0) {
                paths.add(undonePaths.remove(undonePaths.size() - 1));
                invalidate();
            } else {

            }
        }
    }// END MyView Class

    /**
     * Save the last text in the current message
     */
    private void saveTextFromEdittext() {
        if (edtTeacherStudentText.getText().toString().trim().length() > 0) {
            if (isWorkAreaForstudent) {
                teacherStudentMsg = getStudentFormatMsg
                        (edtTeacherStudentText.getText().toString());
            } else {
                teacherStudentMsg = getTeacherFormatMsg
                        (edtTeacherStudentText.getText().toString());
            }
        }
    }

    /*@Override
    public void onBackPressed() {

        if (this.isShowPopUpForPasteImage()) {
            MathFriendzyHelper.showWarningDialog(this, alertMessageImagePasteOrNot);
            return;
        }
        //added for submit question answer button on the tutor area
        if (tutorSessionFragment != null
                && tutorSessionFragment.isEnterSomeThingToNotify()) {
            MathFriendzyHelper.yesNoConfirmationDialog(this,
                    lblPleaseEnterQuestionToNotifyTutor + "\n\n" + lblOrCancelTutorRequest,
                    btnTitleOK, btnTitleCancel,
                    new YesNoListenerInterface() {
                        @Override
                        public void onYes() {

                        }

                        @Override
                        public void onNo() {
                            goBack();
                        }
                    });

        } else {
            this.goBack();
        }
    }*/

    private boolean isShowPopUpForPasteImage() {
        try {
            if (resizeView.isResizeImageLayoutOpen()) {
                return true;
            } else if (tutorSessionFragment != null) {
                return tutorSessionFragment.isResizeImageLayoutOpen();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private void goBack() {
        if (!isCallForGetHelp) {
            this.saveTextFromEdittext();
            this.saveWorkAreaChat();
            if (isStartDrowing)
                new SaveWorkImage().execute();
            else
                callSuperBackPressed(0);
            updateInputStatusForWorkArea();
        } else {
            this.callOnBackForGetHelpStudentRating();
        }
    }

    /**
     * Save the work area chat message
     */
    private void saveWorkAreaChat() {
        try {
            if (CommonUtils.isInternetConnectionAvailable(this)) {
                SaveWorkAreaChatParam param = new SaveWorkAreaChatParam();
                if (isCallForGetHelp) {
                    param.setUserId(student.getUserId());
                    param.setPlayerId(student.getPlayerId());
                    param.setHWId(homeWorkQuizzData.getHomeWorkId());
                } else {
                    if (isWorkAreaForstudent) {
                        param.setUserId(selectedPlayerData.getParentUserId());
                        param.setPlayerId(selectedPlayerData.getPlayerid());
                        param.setHWId(homeWorkQuizzData.getHomeWorkId());
                    } else {//work area for teacher
                        param.setUserId(studentDetail.getParentId());
                        param.setPlayerId(studentDetail.getPlayerId());
                        param.setHWId(studentDetail.getHomeWorkId());
                    }
                }

                param.setCustomeHWId(customeResult.getCustomHwId() + "");
                param.setQuestionNo(customeAns.getQueNo());
                param.setAction("updateMessageForHomeworkQuestion");
                param.setMessage(teacherStudentMsg);
                MathFriendzyHelper.saveWorkAreaChatMessage(param, this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Call for to update the student rating on the previous screen
     */
    private void callOnBackForGetHelpStudentRating() {
        Intent intent = new Intent();
        intent.putExtra("helperStudent", student);
        setResult(RESULT_OK, intent);
        finish();
    }

    /**
     * Upload the question image
     *
     * @param request
     */
    private void uploadQuestionImageFromUri(HttpServerRequest request) {
        if (!isDeleteImage) {
            if (isClickOnPaste) {
                if (questionImageName != null
                        && questionImageName.length() > 0) {
                    if (CommonUtils.isInternetConnectionAvailable(this)) {
                        MathFriendzyHelper
                                .uploadImageOnserverFromSDCard
                                        (this, questionImageName, request,
                                                MathFriendzyHelper.COMPRESS_IMAGE_PERCENTAGE);
                    } else {
                        HomeWorkImpl implObj = new HomeWorkImpl(HomeWorkWorkArea.this);
                        implObj.openConnection();
                        implObj.insertIntoHomeWorkImageTable(MathFriendzyHelper.
                                HOME_WORK_QUE_IMAGE_PREFF +
                                MathFriendzyHelper.getPNGFormateImageName(questionImageName));
                        implObj.closeConnection();
                        request.onRequestComplete();
                    }
                } else {
                    request.onRequestComplete();
                }
            } else {
                request.onRequestComplete();
            }
        } else {
            request.onRequestComplete();
        }
    }

    /**
     * Perform operation on back
     * afterImageSaved 1 if it is called after saving image , means from the AsynckTask
     */
    private void callSuperBackPressed(final int afterImageSaved) {

        this.uploadQuestionImageFromUri(new HttpServerRequest() {

            @Override
            public void onRequestComplete() {
                setAndFinishActivity(afterImageSaved);
            }
        });
    }

    /**
     * Set the data and finish activity
     *
     * @param afterImageSaved
     */
    private void setAndFinishActivity(int afterImageSaved) {
        this.removeFragment();
        tutorSessionFragment = null;
        Intent intent = new Intent();
        intent.putExtra("studentMessage", teacherStudentMsg);
        intent.putExtra("workareapublic", workareapublic);

        //for question image
        if (isDeleteImage) {
            intent.putExtra("isDeleteImage", isDeleteImage);
            intent.putExtra("questionImageName", "");
        } else {
            if (isClickOnPaste) {
                intent.putExtra("isClickOnPaste", isClickOnPaste);
                intent.putExtra("questionImageName", questionImageName);
            }
        }
        //end question image

        if (!isWorkAreaForstudent) {
            intent.putExtra("teacherCredit", this.getTeacherCredit());
        }
        if (afterImageSaved == 1) {
            intent.putExtra("isImageSaved", true);
            intent.putExtra("imageName", fileName);
        }
        intent.putExtra("chatRequestId", chatRequestId);

        //for new green background to the pencil
        intent.putExtra("isWorkInputSeen", isWorkInputSeen);
        intent.putExtra("isOpponentInputSeen", isOpponentInputSeen);

        try {
            //for url links
            intent.putExtra("urlLinks", HomeWorkWorkArea.this.urlList);

            //for put answer on workarea
            intent.putExtra("isAnsUpdated", isAnsUpdated);
            intent.putExtra("updatedPlayerAns", playerAns);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setResult(RESULT_OK, intent);
        finish();
    }


    /**
     * Return the teacher credit
     *
     * @return
     */
    private int getTeacherCredit() {
        if (txtCreditPercentage.getText().toString().equalsIgnoreCase(PERCENTAGE_0)) {
            return ZERO_CREDIT;
        } else if (txtCreditPercentage.getText().toString().equalsIgnoreCase(PERCENTAGE_50)) {
            return HALF_CREDIT;
        } else if (txtCreditPercentage.getText().toString().equalsIgnoreCase(PERCENTAGE_100)) {
            return FULL_CREDIT;
        }
        return NONE_CREDITT;
    }

    /**
     * This AsynckTask take the work area screen shot and save it into SD Card
     *
     * @author yashwant
     */
    class SaveWorkImage extends AsyncTask<Void, Void, Boolean> {

        private ProgressDialog pd = null;

        @Override
        protected void onPreExecute() {
            pd = CommonUtils.getProgressDialog(HomeWorkWorkArea.this);
            pd.show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {

                Bitmap bitMap = MathFriendzyHelper.
                        takeScreenShot(HomeWorkWorkArea.this, mView);

                if (CommonUtils.isInternetConnectionAvailable(HomeWorkWorkArea.this)) {
                    UploadImage.executeMultipartPost(bitMap,
                            MathFriendzyHelper.getPNGFormateImageName(fileName), "",
                            new UploadImageRequest() {
                                @Override
                                public void onComplete(String response) {
                                    if (response != null) {
                                        if (CommonUtils.LOG_ON)
                                            Log.e(TAG, "Response " + response);
                                    }
                                }
                            });
                } else {
                    HomeWorkImpl implObj = new HomeWorkImpl(HomeWorkWorkArea.this);
                    implObj.openConnection();
                    implObj.insertIntoHomeWorkImageTable(MathFriendzyHelper.
                            HOME_WORK_IMAGE_PREFF + MathFriendzyHelper
                            .getPNGFormateImageName(fileName));
                    implObj.closeConnection();
                }

                return MathFriendzyHelper.saveFileToSDCard(bitMap, fileName);
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {

            if (!result.booleanValue()) {
                Toast.makeText(HomeWorkWorkArea.this,
                        "Work image not save, Please free your sd card memory.",
                        Toast.LENGTH_LONG).show();
            }

            if (pd != null && pd.isShowing()) {
                pd.cancel();
            }

            callSuperBackPressed(1);

            super.onPostExecute(result);
        }
    }


    //for Activity open for teacher
    class ViewHolder {
        private RelativeLayout mainLayout;
        private TextView txtStudentAnswer;

        //for fillIn layout
        private RelativeLayout fillInLayout;
        private TextView txtLeftUnit;
        private TextView ansBox;
        private TextView txtRighttUnit;

        //for multiple choice layout
        private RelativeLayout multipleChoiceLayout;
        private LinearLayout optionLayout, ll_ans_box;

        //for True False or Yes No layout
        private RelativeLayout trueFalseLayout;
        private TextView ansTrue;
        private TextView ansFalse;
    }

    @Override
    public void onScrolling(int state) {
        switch (state) {
            case MathFriendzyHelper.ON_SCROLLING:
                btnScrollDown.setBackgroundResource(R.drawable.down);
                btnScrollUp.setBackgroundResource(R.drawable.up);
                break;
            case MathFriendzyHelper.SCROLL_DOWN_COMPLETE:
                btnScrollDown.setBackgroundResource(R.drawable.unselected_down);
                btnScrollUp.setBackgroundResource(R.drawable.up);
                break;
            case MathFriendzyHelper.SCROLL_UP_COMPLETE:
                btnScrollDown.setBackgroundResource(R.drawable.down);
                btnScrollUp.setBackgroundResource(R.drawable.unselected_up);
                break;
        }
    }


    //For reaser circle shape

    /**
     * Set the stating point of the eraser circle on action down , meanse on touch start
     *
     * @param X
     * @param Y
     */
    private void onActionDown(float X, float Y) {
        if (eraserMode) {
            imgEraser.setVisibility(ImageView.VISIBLE);
            //imgEraser.setX((int) X - imgEraser.getWidth() / 2 + workAreaContentLayout.getLeft());
            imgEraser.setX((int) X - imgEraser.getWidth() / 2);
            /*imgEraser.setY((int) Y + studentTeacherLayout.getHeight()
                    + imgEraser.getHeight() - (int) scrollView.getScrollY()
                    - relativeTopBar.getHeight());*/
            imgEraser.setY((int) Y + imgEraser.getHeight() / 2 - relativeTopBar.getHeight());

        }
    }

    /**
     * Set the eraser moving position when action is move
     *
     * @param X
     * @param Y
     */
    private void onActionMove(float X, float Y) {
        if (eraserMode) {
            //imgEraser.setX((int) X - imgEraser.getWidth() / 2 + workAreaContentLayout.getLeft());
            imgEraser.setX((int) X - imgEraser.getWidth() / 2);
            /*imgEraser.setY((int) Y + studentTeacherLayout.getHeight()
                    + imgEraser.getHeight() / 2 - (int) scrollView.getScrollY()
                    - relativeTopBar.getHeight());*/
            imgEraser.setY((int) Y + imgEraser.getHeight() / 2 - relativeTopBar.getHeight());
        }
    }

    /**
     * Invisible the eraser circle when action up
     */
    private void onActionUp() {
        if (eraserMode) {
            imgEraser.setVisibility(ImageView.GONE);
        }
    }
    //end for eraser mode


    //for adding tab on the top bar

    /**
     * For the current work area
     */
    private void clickOnMyWorkArea() {
        if (!isWorkInputSeen) {
            this.markInputSeenForWorkArea();
        }
        isWorkInputSeen = true;
        selectedWorkAreaTab = MY_WORK_AREA;
        //myWorkArea.setBackgroundColor(getResources().getColor(R.color.WHITE));
        myWorkArea.setBackgroundResource(R.drawable.white_tab);
        btnTutorWorkArea.setBackgroundResource(R.drawable.light_green_tab);
        btnSecondWorkArea.setBackgroundResource(R.drawable.light_green_tab);
        //btnTutorWorkArea.setBackgroundColor(getResources().getColor(R.color.LIGHT_GREEN));
        myWorkAreaContainer.setVisibility(RelativeLayout.VISIBLE);
        fragmentContainer.setVisibility(RelativeLayout.GONE);
        secondWorkAreaContainer.setVisibility(RelativeLayout.GONE);

        //audio calling changes
        this.setAudioButtonVisibility(false);
        //this.removeFragment();
    }

    /**
     * For tutor session work area
     */
    private void clickOnTutorSeesionWorkArea() {
        isOpponentInputSeen = true;
        selectedWorkAreaTab = TUTOR_WORK_AREA;
        //myWorkArea.setBackgroundColor(getResources().getColor(R.color.LIGHT_GREEN));
        myWorkArea.setBackgroundResource(R.drawable.light_green_tab);
        btnTutorWorkArea.setBackgroundResource(R.drawable.white_tab);
        btnSecondWorkArea.setBackgroundResource(R.drawable.light_green_tab);
        //btnTutorWorkArea.setBackgroundColor(getResources().getColor(R.color.WHITE));
        myWorkAreaContainer.setVisibility(RelativeLayout.GONE);
        fragmentContainer.setVisibility(RelativeLayout.VISIBLE);
        secondWorkAreaContainer.setVisibility(RelativeLayout.GONE);

        //audio calling changes
        this.setAudioButtonVisibility(true);

        this.addTutorSessionFragment();
    }

    /**
     * Click on second work area
     */
    private void clickOnSecondWorkArea() {
        selectedWorkAreaTab = SECOND_WORK_AREA;
        myWorkArea.setBackgroundResource(R.drawable.light_green_tab);
        btnTutorWorkArea.setBackgroundResource(R.drawable.light_green_tab);
        btnSecondWorkArea.setBackgroundResource(R.drawable.white_tab);
        myWorkAreaContainer.setVisibility(RelativeLayout.GONE);
        fragmentContainer.setVisibility(RelativeLayout.GONE);
        secondWorkAreaContainer.setVisibility(RelativeLayout.VISIBLE);
        this.addSecondWorkArea();

        //audio calling changes
        this.setAudioButtonVisibility(false);
    }

    /**
     * Tutorial sesion argument
     *
     * @return
     */
    private Bundle getTutorBundleParam() {

        TutorSessionBundleArgument argument = new TutorSessionBundleArgument();
        argument.setFindTutorDetail(findTutorDetail);
        argument.setPlayerAns(playerAns);
        argument.setChatRequestedId(chatRequestId);
        argument.setQuesion(customeAns.getQueNo());
        if (isCallForGetHelp) {
            argument.setHwId(homeWorkQuizzData.getHomeWorkId());
        } else {
            if (isWorkAreaForstudent) {
                argument.setHwId(homeWorkQuizzData.getHomeWorkId());
            } else {//work area for teacher
                argument.setHwId(studentDetail.getHomeWorkId());
            }
        }
        argument.setCustomeHwId(customeResult.getCustomHwId() + "");
        argument.setHomeworkSheetName(customeResult.getSheetName());

        //tutor session open for check homework
        argument.setWorkAreaForstudent(isWorkAreaForstudent);
        argument.setStudentDetail(studentDetail);
        //end changes

        Bundle bundle = new Bundle();
        bundle.putSerializable("arguments", argument);
        return bundle;
    }

    private Bundle getSecondTabBundleParam() {
        SecondWorkBundleArgument argument = new SecondWorkBundleArgument();
        argument.setPlayerAns(playerAns);
        argument.setCustomeAns(customeAns);
        Bundle bundle = new Bundle();
        bundle.putSerializable("arguments", argument);
        return bundle;
    }

    /**
     * Add Tutor Session fragment to the view
     */
    private void addTutorSessionFragment() {
        if (tutorSessionFragment == null) {
            FragmentTransaction fragmentTransaction = MathFriendzyHelper
                    .getFragmentTransaction(MathFriendzyHelper.getFragmentManager(this));
            tutorSessionFragment = new TuturSessionFragment();
            tutorSessionFragment.setArguments(this.getTutorBundleParam());
            fragmentTransaction.add(fragmentContainer.getId(), tutorSessionFragment);
            fragmentTransaction.commit();
            this.initializeAudioButtonObject();
        } else {

        }
    }

    /**
     * Addd Second work area fragment
     */
    private void addSecondWorkArea() {
        if (secondWorkAreaFragment == null) {
            FragmentTransaction fragmentTransaction = MathFriendzyHelper
                    .getFragmentTransaction(MathFriendzyHelper.getFragmentManager(this));
            secondWorkAreaFragment = new SecondWorkAreaFragment();
            secondWorkAreaFragment.setArguments(this.getSecondTabBundleParam());
            fragmentTransaction.add(secondWorkAreaContainer.getId(), secondWorkAreaFragment);
            fragmentTransaction.commit();
        }
    }

    /**
     * Remove fragment
     */
    private void removeFragment() {
        if (tutorSessionFragment != null) {
            FragmentTransaction transaction =
                    getFragmentManager().beginTransaction();
            transaction.remove(tutorSessionFragment);
            transaction.commit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case FIND_TUTOR_REQUEST:
                    findTutorDetail = (FindTutorResponse) data.getSerializableExtra("findTutorResponse");
                    this.clickOnTutorSeesionWorkArea();
                    break;
                case MathFriendzyHelper.FIND_TUTOR_REQUEST_FROM_FRAGMENT:
                    tutorSessionFragment = null;
                    findTutorDetail = (FindTutorResponse) data.getSerializableExtra("findTutorResponse");
                    this.clickOnTutorSeesionWorkArea();
                    break;
                case FIND_TUTOR_REQUEST_FOR_MEGNIFIER:
                    chatRequestId = 0;
                    removeFragment();
                    tutorSessionFragment = null;
                    btnTutorWorkArea.setText(lblTutorArea);
                    this.showTutorTabContent(false, false, false, false);
                    findTutorDetail = (FindTutorResponse) data.getSerializableExtra("findTutorResponse");
                    this.clickOnTutorSeesionWorkArea();
                    break;
                case HomeWorkImageOption.PICK_IMAGE_GALLERY:
                    String pickImageUrl = MathFriendzyHelper.getPath(this, data.getData());
                    if (pickImageUrl != null) {
                        this.openHomeworkSheetActivity(pickImageUrl);
                    }
                    break;
                case HomeWorkImageOption.PICK_CAMERA_IMAGE:
                    String capturedImageUrl = MathFriendzyHelper
                            .getImagePathUsingUri(this,
                                    HomeWorkImageOption.getCameraImageUri());
                    if (capturedImageUrl != null) {
                        this.openHomeworkSheetActivity(capturedImageUrl);
                    }
                    break;
                case HomeWorkImageOption.PICK_HOME_WORK_IMAGE:
                    String homeworkSheetImageUri = data.getStringExtra("cropImageUri");
                    if (homeworkSheetImageUri != null && homeworkSheetImageUri.length() > 0) {
                        if (selectedWorkAreaTab == TUTOR_WORK_AREA
                                && tutorSessionFragment != null) {
                            tutorSessionFragment.onImageSelected(homeworkSheetImageUri);
                        } else {
                            this.onImageSelected(homeworkSheetImageUri);
                        }
                    }
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Open the homwwork sheet activity for image view
     *
     * @param pdfFileName
     */
    public void openHomeworkSheetActivity(String pdfFileName) {
        Intent intent = new Intent(this, ActViewPdf.class);
        intent.putExtra("pdfFileName", pdfFileName);
        intent.putExtra("isForImageSelection", true);
        intent.putExtra("isForImageView", true);
        startActivityForResult(intent, HomeWorkImageOption.PICK_HOME_WORK_IMAGE);
    }


    @Override
    public void onChatRequesId(int chatRequestId) {
        this.chatRequestId = chatRequestId;
    }


    /**
     * Show tutor content view
     *
     * @param bValue
     */
    private void showTutorContectView(boolean bValue) {
        if (bValue) {
            tutorContentLayout.setVisibility(LinearLayout.VISIBLE);
        } else {
            tutorContentLayout.setVisibility(LinearLayout.GONE);
        }
    }

    @Override
    public void onTutorTabTextSet(String tutorTabText) {
        //this.showTutorContectView(true);
        if (isWorkAreaForstudent) {
            btnTutorWorkArea.setText(tutorTabText);
        }
    }

    @Override
    public void onTimeSpent(int timeSpent) {

    }

    private void setImageVisibility(ImageView imgView, boolean isVisible) {
        if (isVisible) {
            imgView.setVisibility(ImageView.VISIBLE);
        } else {
            imgView.setVisibility(ImageView.GONE);
        }
    }

    @Override
    public void showTutorTabContent(boolean... isShow) {
        if (isWorkAreaForstudent) {
            boolean isShowOnline = isShow[0];
            boolean isShowHand = isShow[1];
            boolean isShowDisconnect = isShow[2];
            this.setImageVisibility(imgOnline, isShowOnline);
            this.setImageVisibility(imgHand, isShowHand);
            this.setImageVisibility(imgDisconnect, isShowDisconnect);
            if (isShow.length == 4) {
                boolean isShowMegnifier = isShow[3];
                this.setImageVisibility(imgMegnifier, isShowMegnifier);
            }
        }
        //this.showTutorContectView(isShow[0]);
    }

    @Override
    public void onlineStatus(boolean isOnline) {
        if (isOnline) {
            isOpponentOnline = true;
            imgOnline.setBackgroundResource(R.drawable.online_icon);
        } else {
            isOpponentOnline = false;
            if (isOpponentActiveInApp) {
                imgOnline.setBackgroundResource(R.drawable.offline_icon);
            } else {
                imgOnline.setBackgroundResource(R.drawable.red_circle_icon);
            }
        }
    }


    @Override
    public void onActiveInActiveStatus(int inActive) {
        if (isOpponentOnline == false) {
            if (inActive == MathFriendzyHelper.YES) {
                isOpponentActiveInApp = false;
                imgOnline.setBackgroundResource(R.drawable.red_circle_icon);
            } else {
                isOpponentActiveInApp = true;
                imgOnline.setBackgroundResource(R.drawable.offline_icon);
            }
        }
    }

    /**
     * Cancel the created request
     */
    private void stopWaitingForTutor() {
        if (CommonUtils.isInternetConnectionAvailable(this)) {
            StopWaitingForTutorParam param = new StopWaitingForTutorParam();
            param.setRequestId(this.chatRequestId);
            if (tutorSessionFragment.isChatConnected()) {
                param.setAction("tutorSessionDisconnectByStudnent");
            } else {
                this.chatRequestId = 0;
                param.setAction("stopWaitingForTutor");
            }
            new MyAsyckTask(ServerOperation.createPostRequestForStopWaitingForTutor(param)
                    , null, ServerOperationUtil.STOP_WAITING_FOR_TUTOR_REQUEST,
                    this, this, ServerOperationUtil.SIMPLE_DIALOG, true,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        } else {
            CommonUtils.showInternetDialog(this);
        }
    }

    /**
     * Click on hand
     */
    private void clickOnHand() {
        if (tutorSessionFragment != null) {
            tutorSessionFragment.showRateUsDialog();
        }
    }

    /**
     * Click on  magnifier
     */
    private void clickOnMegnifier() {
        MathFriendzyHelper.yesNoConfirmationDialog(this,
                alertWouldULikeToConnectOther,
                btnTitleYes, lblNo, new YesNoListenerInterface() {
                    @Override
                    public void onYes() {
                        //removeFragment();
                        Intent intent = new Intent(HomeWorkWorkArea.this,
                                ActFindTutor.class);
                        startActivityForResult(intent, FIND_TUTOR_REQUEST_FOR_MEGNIFIER);
                    }

                    @Override
                    public void onNo() {

                    }
                });
    }

    private void initializeCurrentObj() {
        currentObj = this;
    }

    public static HomeWorkWorkArea getCurrentObj() {
        return currentObj;
    }

    /**
     * Update UI from notification when tutor accept and submit answer request for student
     *
     * @param response
     */
    public void updateUIFromNotoficationForConnectWithStudent(final HelpSetudentResponse response) {
        if (selectedWorkAreaTab == TUTOR_WORK_AREA) {
            if (tutorSessionFragment != null && (Integer.parseInt(response.getChatReqId())
                    == tutorSessionFragment.chatRequestId)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tutorSessionFragment.updateUIFromNotificationForConnectWithStudent(response);
                    }
                });
                //return ;
            }
        }
        Intent newIntent = new Intent(this, PopUpActivityForNotification.class);
        newIntent.putExtra(PopUpActivityForNotification.ACT_CALL_FOR,
                PopUpActivityForNotification.CALL_FOR_HELP_STUDENT);
        newIntent.putExtra("notifData", response);
        startActivity(newIntent);
    }


    /**
     * Update UI from notification when tutor accept and submit answer request for student
     *
     * @param response
     */
    public void updateUIFromNotoficationForDisConnectWithStudent(final DisconnectStudentNotif response) {
        if (selectedWorkAreaTab == TUTOR_WORK_AREA) {
            if (tutorSessionFragment != null && (Integer.parseInt(response.getChatRequestId())
                    == tutorSessionFragment.chatRequestId)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tutorSessionFragment.updateUIFromNotificationForDisConnectWithStudent
                                (response);
                    }
                });
                //return ;
            }
        }
        Intent newIntent = new Intent(this, PopUpActivityForNotification.class);
        newIntent.putExtra(PopUpActivityForNotification.ACT_CALL_FOR,
                PopUpActivityForNotification.CALL_FOR_DISCONNECT_STUDENT);
        newIntent.putExtra("notifData", response);
        startActivity(newIntent);
    }

    public void updateUIFromNotoficationForTutorImageUpdate(final TutorImageUpdateNotif response) {
        if (selectedWorkAreaTab == TUTOR_WORK_AREA) {
            if (tutorSessionFragment != null && (Integer.parseInt(response.getChatRequestId())
                    == tutorSessionFragment.chatRequestId)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        response.setUpdateFor(TutorImageUpdateNotif.STUDENT);
                        tutorSessionFragment.updateUIFromNotificationForTutorImageUpdate(response);
                    }
                });
                return;
            }
        }

        Intent newIntent = new Intent(this, PopUpActivityForNotification.class);
        newIntent.putExtra(PopUpActivityForNotification.ACT_CALL_FOR,
                PopUpActivityForNotification.CALL_FOR_TUTOR_AREA_IMAGE_UPDATE);
        newIntent.putExtra("notifData", response);
        startActivity(newIntent);
    }

    public void updateUIFromNotoficationForTutorWorkImageUpdate(final TutorWorkAreaImageUpdateNotif response) {
        if (selectedWorkAreaTab == TUTOR_WORK_AREA) {
            if (tutorSessionFragment != null && (Integer.parseInt(response.getChatRequestId())
                    == tutorSessionFragment.chatRequestId)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tutorSessionFragment.updateUIFromNotoficationForTutorWorkImageUpdate(response);
                    }
                });
                return;
            }
        }

        Intent newIntent = new Intent(this, PopUpActivityForNotification.class);
        newIntent.putExtra(PopUpActivityForNotification.ACT_CALL_FOR,
                PopUpActivityForNotification.CALL_FOR_TUTOR_WORK_AREA_IMAGE_UPDATE);
        newIntent.putExtra("notifData", response);
        startActivity(newIntent);
    }

    /**
     * Click to select image dialog
     */
    private void clickToSelectAndDeleteImage() {
        if (imageOptionSelector != null) {
            imageOptionSelector.showOptionDialog();
        }
    }

    /**
     * Initialize image selector dialog
     */
    private void initializeImageSelector() {
        imageOptionSelector = new HomeWorkImageOption(this,
                new HomeworkImageOptionCallback() {
                    @Override
                    public void onOptionSelect(SelectImageOption option) {
                        onImageOptionSelect(option);
                    }
                });
        imageOptionSelector.setShowOption(true, true, this.isShowHomeworkSheetOptionToSelectImage()
                , false, false);
        if (this.isShowHomeworkSheetOptionToSelectImage()) {
            imageOptionSelector.setHomeworkSheetName(customeResult.getSheetName());
        }
    }

    /**
     * Check to show homework sheet option in image selection dialog
     *
     * @return
     */
    public boolean isShowHomeworkSheetOptionToSelectImage() {
        try {
            if (customeResult.getSheetName() != null
                    && customeResult.getSheetName().length() > 0)
                return true;
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public String getHomeworkSheetName() {
        try {
            return customeResult.getSheetName();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Check for showing text option in select dialog in the tutor session
     *
     * @return
     */
    public boolean isShowOptionForWorkAreaText() {
        if (adapter != null && adapter.msgList.size() > 0)
            return true;
        return false;
    }

    /**
     * Take screen shot and save into memory
     *
     * @param callback
     */
    public void takeScreenShotAndSaveIntoMemory(final TakeScreenShotAndSaveToMemoryCallback callback
            , Context context) {
        MathFriendzyHelper.takeScreenShotAndSaveIntoMemory(context, workAreaContentLayout, callback);
    }

    /**
     * Call when option select
     *
     * @param option
     */
    private void onImageOptionSelect(SelectImageOption option) {
        this.selectedOption = option;
        switch (option.getId()) {
            case SelectImageConstants.SELECT_FROM_GALLARY:
                break;
            case SelectImageConstants.SELECT_FROM_CAMERA:
                break;
            case SelectImageConstants.SELECT_FROM_HOMEWORK_SHEET:
                break;
            case SelectImageConstants.SELECT_FROM_WORK_AREA_IMAGE:
                break;
            case SelectImageConstants.SELECT_WORK_AREA_TEXT:
                break;
            case SelectImageConstants.DELETE_IMAGE:
                isDeleteImage = true;
                imgStudentImage.setImageBitmap(null);
                this.updateIsWorkAreaUpdated(true);
                break;
            case SelectImageConstants.DELETE_WORK_AREA_IMAGE:
                break;
            case SelectImageConstants.DELETE_WORK_AREA_TEXT:
                break;
        }
    }

    private void stopScrolling(boolean isStopScrolling) {
        isScrollEnable = isStopScrolling;
    }

    public void onImageSelected(String selectedImageUri) {
        this.stopScrolling(true);
        resizeView.setResizeLayoutVisibility(true);
        resizeView.setImageFromUri(selectedImageUri,
                MathFriendzyHelper.isDeviceSizeGreaterThanDraViewLimit,
                MathFriendzyHelper.MAX_DRAW_VIEW_WIDTH);
        //this.onImageSelectionDone();
    }

    private void onResizeOption(int tag, Bitmap bitmap, RelativeLayout.LayoutParams lp) {
        switch (tag) {
            case ResizeView.RESIZE_DONE:
                this.stopScrolling(false);
                resizeView.setResizeLayoutVisibility(false);
                this.onImageSelectionDone(bitmap, lp);
                break;
            case ResizeView.RESIZE_DELETE:
                this.stopScrolling(false);
                resizeView.setResizeLayoutVisibility(false);
                break;
        }
    }

    private void onImageSelectionDone(final Bitmap bitmap, RelativeLayout.LayoutParams lp) {
        if (bitmap != null) {
            this.updateIsWorkAreaUpdated(true);
            isClickOnPaste = true;
            isDeleteImage = false;
            lp.topMargin = lp.topMargin + scrollView.getScrollY();

            if (MathFriendzyHelper.isDeviceSizeGreaterThanDraViewLimit) {
                lp.leftMargin = lp.leftMargin - drawViewLeftMargin;
            }

            imgStudentImage.setLayoutParams(lp);
            imgStudentImage.setImageBitmap(bitmap);

            if (this.selectedOption != null && imageOptionSelector != null) {
                imageOptionSelector.setVisibilityAfterOptionSelectionDone(this.selectedOption);
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        new AsyncTask<Void, Void, Bitmap>() {
                            private ProgressDialog pd = null;

                            protected void onPreExecute() {
                                pd = CommonUtils.getProgressDialog(HomeWorkWorkArea.this);
                                pd.show();
                            }

                            @Override
                            protected Bitmap doInBackground(Void... params) {
                                Bitmap bitmap = MathFriendzyHelper.takeScreenShot
                                        (HomeWorkWorkArea.this, studentImageLayout);
                                MathFriendzyHelper.saveFileToSDCard(bitmap, questionImageName);
                                return bitmap;
                                //return null;
                            }

                            protected void onPostExecute(Bitmap result) {
                                if (pd != null && pd.isShowing()) {
                                    pd.cancel();
                                }
                            }
                        }.execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 500);//half seconds
        }
    }

    //for adding second work area content
    private String getWorkImageName() {
        if (playerAns != null
                && playerAns.getWorkImage().length() > 0) {
            return playerAns.getWorkImage();
        } else if (isStartDrowing) {
            return fileName;
        } else {
            return fileName;
        }
    }

    private String getQuestionImageName() {
        if (isDeleteImage) {
            return "";
        } else {
            if (isClickOnPaste) {
                return questionImageName;
            } else {
                if (playerAnsQuestionImage != null
                        && playerAnsQuestionImage.length() > 0)
                    return questionImageName;
                else
                    return "";
            }
        }
    }

    public void uploadWorkAreaContentForDuplicateWorkArea(final Context context,
                                                          final OnRequestComplete listener) {
        if (ActCheckHomeWork.getCurrentObj().isAddWorkAreaContentToDuplicateArea()) {
            this.saveTextFromEdittext();
            this.saveWorkAreaChat();
            final String workImageName = this.getWorkImageName();
            final String questionImageName = this.getQuestionImageName();
            if (!MathFriendzyHelper.isEmpty(workImageName)) {
                this.uploadWorkImageDrawingWithoutLoader(new HttpServerRequest() {
                    @Override
                    public void onRequestComplete() {
                        if (!MathFriendzyHelper.isEmpty(questionImageName)) {
                            uploadQuestionImageFromUriWithoutLoader(new HttpServerRequest() {
                                @Override
                                public void onRequestComplete() {
                                    updateDuplicateContent(context, workImageName, questionImageName,
                                            listener);
                                }
                            });
                        } else {
                            updateDuplicateContent(context, workImageName, questionImageName, listener);
                        }
                    }
                });
            } else {
                if (!MathFriendzyHelper.isEmpty(questionImageName)) {
                    uploadQuestionImageFromUriWithoutLoader(new HttpServerRequest() {
                        @Override
                        public void onRequestComplete() {
                            updateDuplicateContent(context, workImageName, questionImageName, listener);
                        }
                    });
                } else {
                    updateDuplicateContent(context, workImageName, questionImageName, listener);
                }
            }
        } else {
            listener.onComplete();
        }
    }

    private void updateDuplicateContent(Context context, String workImageName, String questionImageName
            , OnRequestComplete listener) {
        ActCheckHomeWork.getCurrentObj().updateWorkAreaContentForDuplicateWorkArea
                (context, workImageName, questionImageName,
                        teacherStudentMsg, listener);
    }

    private void uploadWorkImageDrawing(final HttpServerRequest request) {
        new AsyncTask<Void, Void, Boolean>() {
            private ProgressDialog pd = null;

            @Override
            protected void onPreExecute() {
                pd = CommonUtils.getProgressDialog(HomeWorkWorkArea.this);
                pd.show();
            }

            @Override
            protected Boolean doInBackground(Void... params) {
                try {
                    Bitmap bitMap = MathFriendzyHelper.
                            takeScreenShot(HomeWorkWorkArea.this, mView);

                    if (CommonUtils.isInternetConnectionAvailable(HomeWorkWorkArea.this)) {
                        UploadImage.executeMultipartPost(bitMap,
                                MathFriendzyHelper.getPNGFormateImageName(fileName), "",
                                new UploadImageRequest() {
                                    @Override
                                    public void onComplete(String response) {
                                        if (response != null) {
                                            if (CommonUtils.LOG_ON)
                                                Log.e(TAG, "Response " + response);
                                        }
                                    }
                                });
                    } else {
                        HomeWorkImpl implObj = new HomeWorkImpl(HomeWorkWorkArea.this);
                        implObj.openConnection();
                        implObj.insertIntoHomeWorkImageTable(MathFriendzyHelper.
                                HOME_WORK_IMAGE_PREFF + MathFriendzyHelper
                                .getPNGFormateImageName(fileName));
                        implObj.closeConnection();
                    }
                    return MathFriendzyHelper.saveFileToSDCard(bitMap, fileName);
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Boolean result) {
                if (!result.booleanValue()) {
                    Toast.makeText(HomeWorkWorkArea.this,
                            "Work image not save, Please free your sd card memory.",
                            Toast.LENGTH_LONG).show();
                }
                if (pd != null && pd.isShowing()) {
                    pd.cancel();
                }
                request.onRequestComplete();
            }
        }.execute();
    }

    private void updateInputStatusForWorkArea() {
        if (!isCallForGetHelp) {
            if (isWorkAreaUpdated) {
                UpdateInputStatusForWorkAreaParam param = new UpdateInputStatusForWorkAreaParam();
                param.setAction("updateInputStatusForWorkArea");
                if (isWorkAreaForstudent) {
                    param.setUserId(selectedPlayerData.getParentUserId());
                    param.setPlayerId(selectedPlayerData.getPlayerid());
                    param.setHwId(homeWorkQuizzData.getHomeWorkId());
                    param.setIsStudent(1);
                } else {//for teacher
                    param.setUserId(studentDetail.getParentId());
                    param.setPlayerId(studentDetail.getPlayerId());
                    param.setHwId(studentDetail.getHomeWorkId());
                    param.setIsStudent(0);
                }
                param.setCustomHWId(customeResult.getCustomHwId() + "");
                param.setQuestion(customeAns.getQueNo());

                if (CommonUtils.isInternetConnectionAvailable(this)) {
                    new MyAsyckTask(ServerOperation.createPostRequestForUpdateInputStatusForWorkArea(param)
                            , null, ServerOperationUtil.UPDATE_INPUT_STATUS_FOR_WORK_AREA,
                            this, this, ServerOperationUtil.SIMPLE_DIALOG, false,
                            getString(R.string.please_wait_dialog_msg))
                            .execute();
                } else {
                    MathFriendzyHelper.saveWorkAreaInputForOffline(this, param);
                }
            }
        }
    }

    /**
     * Mark input seeb by the student or tutor
     */
    private void markInputSeenForWorkArea() {
        try {
            if (!isCallForGetHelp && customeResult.isAlreadyPlayed()) {
                UpdateInputStatusForWorkAreaParam param = new UpdateInputStatusForWorkAreaParam();
                param.setAction("markInputSeenForWorkArea");
                if (isWorkAreaForstudent) {
                    param.setUserId(selectedPlayerData.getParentUserId());
                    param.setPlayerId(selectedPlayerData.getPlayerid());
                    param.setHwId(homeWorkQuizzData.getHomeWorkId());
                    param.setIsStudent(1);
                } else {//for teacher
                    param.setUserId(studentDetail.getParentId());
                    param.setPlayerId(studentDetail.getPlayerId());
                    param.setHwId(studentDetail.getHomeWorkId());
                    param.setIsStudent(0);
                }
                param.setCustomHWId(customeResult.getCustomHwId() + "");
                param.setQuestion(customeAns.getQueNo());

                if (CommonUtils.isInternetConnectionAvailable(this)) {
                    new MyAsyckTask(ServerOperation.createPostRequestForUpdateInputStatusForWorkArea(param)
                            , null, ServerOperationUtil.UPDATE_INPUT_STATUS_FOR_WORK_AREA,
                            this, this, ServerOperationUtil.SIMPLE_DIALOG, false,
                            getString(R.string.please_wait_dialog_msg))
                            .execute();
                } else {
                    MathFriendzyHelper.saveWorkAreaInputForOffline(this, param);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateIsWorkAreaUpdated(boolean isUpdated) {
        this.isWorkAreaUpdated = isUpdated;
    }


    private void uploadWorkImageDrawingWithoutLoader(final HttpServerRequest request) {
        new AsyncTask<Void, Void, Boolean>() {

            @Override
            protected void onPreExecute() {
            }

            @Override
            protected Boolean doInBackground(Void... params) {
                try {
                    Bitmap bitMap = MathFriendzyHelper.
                            takeScreenShot(HomeWorkWorkArea.this, mView);

                    if (CommonUtils.isInternetConnectionAvailable(HomeWorkWorkArea.this)) {
                        UploadImage.executeMultipartPost(bitMap,
                                MathFriendzyHelper.getPNGFormateImageName(fileName), "",
                                new UploadImageRequest() {
                                    @Override
                                    public void onComplete(String response) {
                                        if (response != null) {
                                            if (CommonUtils.LOG_ON)
                                                Log.e(TAG, "Response " + response);
                                        }
                                    }
                                });
                    } else {
                        HomeWorkImpl implObj = new HomeWorkImpl(HomeWorkWorkArea.this);
                        implObj.openConnection();
                        implObj.insertIntoHomeWorkImageTable(MathFriendzyHelper.
                                HOME_WORK_IMAGE_PREFF + MathFriendzyHelper
                                .getPNGFormateImageName(fileName));
                        implObj.closeConnection();
                    }
                    return MathFriendzyHelper.saveFileToSDCard(bitMap, fileName);
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Boolean result) {
                if (!result.booleanValue()) {
                    Toast.makeText(HomeWorkWorkArea.this,
                            "Work image not save, Please free your sd card memory.",
                            Toast.LENGTH_LONG).show();
                }
                request.onRequestComplete();
            }
        }.execute();
    }


    /**
     * Upload the question image
     *
     * @param request
     */
    private void uploadQuestionImageFromUriWithoutLoader(HttpServerRequest request) {
        if (!isDeleteImage) {
            if (isClickOnPaste) {
                if (questionImageName != null
                        && questionImageName.length() > 0) {
                    if (CommonUtils.isInternetConnectionAvailable(this)) {
                        MathFriendzyHelper
                                .uploadImageOnserverFromSDCardWithoutLoader
                                        (this, questionImageName, request);
                    } else {
                        HomeWorkImpl implObj = new HomeWorkImpl(HomeWorkWorkArea.this);
                        implObj.openConnection();
                        implObj.insertIntoHomeWorkImageTable(MathFriendzyHelper.
                                HOME_WORK_QUE_IMAGE_PREFF +
                                MathFriendzyHelper.getPNGFormateImageName(questionImageName));
                        implObj.closeConnection();
                        request.onRequestComplete();
                    }
                } else {
                    request.onRequestComplete();
                }
            } else {
                request.onRequestComplete();
            }
        } else {
            request.onRequestComplete();
        }
    }

    /**
     * Open for work area for see how other did it
     *
     * @return
     */
    private boolean isAlreadyRated(int newRatingStar) {
        if (ratingStar > 0) {
            MathFriendzyHelper.showWarningDialog(this,
                    lblYouHaveAlreadyRated);
            return true;
        }
        ratingStar = newRatingStar;
        return false;
    }

    @Override
    protected void onDestroy() {
        CommonUtils.printLog(TAG, "inside onDestroy()");
        this.clearAllBitmap();
        this.stopWatcher();
        super.onDestroy();
    }

    private void clearAllBitmap() {
        try {
            this.cleanUpView();
            //MathFriendzyHelper.removeView(MathFriendzyHelper.getViewGroup(this));
            MathFriendzyHelper.removeView(viewGroup);
            MathFriendzyHelper.clearBitmap(alreadyWorkingBitmap, mBitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cleanUpView() {
        try {
            imgStudentImage.setImageBitmap(null);
            mView.destroyDrawingCache();
            mCanvas = null;
            mView = null;
            layRough = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeHeightAndWidthForFixedViewLimit(final int width, final View view) {
        view.getViewTreeObserver().addOnGlobalLayoutListener
                (new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        if (android.os.Build.VERSION.SDK_INT >=
                                android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        } else {
                            view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        }
                        drawViewLeftMargin = view.getLeft();
                        resizeView.initializeLeftAndRightMarginLimit(drawViewLeftMargin
                                , drawViewLeftMargin + width);
                        //resizeView.setRlResizeLayoutParam(drawViewLeftMargin);
                    }
                });
    }


    private void updateCreditByTeacherForStudentQuestion(String credit) {
        try {
            UpdateCreditByTeacherForStudentQuestionParam param =
                    new UpdateCreditByTeacherForStudentQuestionParam();
            param.setAction("updateCreditByTeacherForStudentQuestion");
            param.setDataJson(this.getDataJsonToUpdateTeacherCreditByTeacher(credit));
            MathFriendzyHelper.updateCreditByTeacherForStudentQuestion(this, param,
                    new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {

                        }
                    }, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getDataJsonToUpdateTeacherCreditByTeacher(String credit) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userId", studentDetail.getParentId());
            jsonObject.put("playerId", studentDetail.getPlayerId());
            jsonObject.put("homeworkId", studentDetail.getHomeWorkId());
            jsonObject.put("customHwId", customeResult.getCustomHwId());
            jsonObject.put("question", customeAns.getQueNo());
            jsonObject.put("credit", credit);
            return jsonObject.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    //change to resume from the background when user press home button
    @Override
    protected void onResume() {
        this.doTheThingsOnResume();
        super.onResume();
    }


    private boolean isAppMinimize = false;
    private int normalTimerProgress = 0;
    private int paidTimerProgress = 0;

    private void doTheThingOnPause() {
        isAppMinimize = true;
    }

    private void doTheThingsOnResume() {
        if (isAppMinimize) {
            isAppMinimize = false;
            if (layoutTutorTab.getVisibility()
                    == RelativeLayout.VISIBLE) {
                if (!CommonUtils.isInternetConnectionAvailable(this)) {
                    CommonUtils.showInternetDialog(this);
                    return;
                }
                removeFragment();
                tutorSessionFragment = null;
                this.addTutorSessionFragment();
            }
        }
    }

    HomeWatcher mHomeWatcher = null;

    /**
     * Start the homewatcher
     */
    private void startHomePressWatcher() {
        mHomeWatcher = new HomeWatcher(this);
        mHomeWatcher.setOnHomePressedListener(new OnHomePressedListener() {
            @Override
            public void onHomePressed() {
                //CommonUtils.printLog(TAG , "Homebutton press");
                doTheThingOnPause();
            }

            @Override
            public void onHomeLongPressed() {
                //CommonUtils.printLog(TAG , "Homebutton long press");
            }
        });
        mHomeWatcher.startWatch();
    }


    /**
     * Stop the homewatcher
     */
    private void stopWatcher() {
        try {
            if (mHomeWatcher != null) {
                mHomeWatcher.stopWatch();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param isConnect - true when internet is connected and false otherwise
     */
    public void updateTutorSessionOnInternetConnectionChange(boolean isConnect) {
        if (selectedWorkAreaTab == TUTOR_WORK_AREA && tutorSessionFragment != null) {
            if (isConnect) {
                //tutorSessionFragment.internetConnected();
                doTheThingsOnResume();
            } else {
                tutorSessionFragment.internetConnectionLost();
                doTheThingOnPause();
                /*CommonUtils.showInternetDialog(this);
                doTheThingOnPause();*/
            }
        }
    }

    @Override
    public void onLoadingCompleted() {

    }
    //change done for resume from background


    //change for creating new request with subjects
    public GetHomeWorkForStudentWithCustomeResponse getHomeworkData() {
        return homeWorkQuizzData;
    }

    //to hide the answer layout from teacher check homework
    private void setVisiblilityOfBtnCloseViewAnswer() {
        if (this.isWorkAreaForstudent) {
            btnCloseViewAnswer.setVisibility(View.GONE);
        } else {
            btnCloseViewAnswer.setVisibility(View.VISIBLE);
        }
    }

    private void closeStudentAnswerLayout() {
        if (!this.isWorkAreaForstudent) {
            try {
                isAnswerLayoutShown = !isAnswerLayoutShown;
                if (isAnswerLayoutShown) {
                    imgCloseAnswerLayout.setText(closeAnswersText);
                    answerLayout.setVisibility(View.VISIBLE);
                } else {
                    imgCloseAnswerLayout.setText(showAnswersText);
                    answerLayout.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    //Add links changes
    private void setVisibilityOfAddLinkButton() {
        if (this.isWorkAreaForstudent) {
            this.setVisibilityOfBtnAddLinks(true);
        } else {
            if (urlList != null && urlList.size() > 0) {
                this.setVisibilityOfBtnAddLinks(true);
            } else {
                this.setVisibilityOfBtnAddLinks(false);
            }
        }
    }

    private void setVisibilityOfBtnAddLinks(boolean bValue) {
        if (bValue) {
            btnAddLinks.setVisibility(View.VISIBLE);
        } else {
            btnAddLinks.setVisibility(View.GONE);
        }
    }

    private void initializeAddUrlList() {
        try {
            if (playerAns != null) {
                urlList = playerAns.getUrlLinks();
            }

            if (this.isWorkAreaForstudent) {
                if (!(urlList != null && urlList.size() > 0)) {
                    urlList = new ArrayList<AddUrlToWorkArea>();
                    AddUrlToWorkArea url = new AddUrlToWorkArea();
                    url.setTitle("");
                    url.setUrl("");
                    urlList.add(url);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showAddLinksDialog() {
        if (addUrlLinkDialog == null) {
            addUrlLinkDialog = new AddUrlToQuestionDialog(this, isWorkAreaForstudent);
            addUrlLinkDialog.initializeCallback(new OnUrlSaveCallback() {
                @Override
                public void onSave(ArrayList<AddUrlToWorkArea> updatedUrlLinkList, int tag) {
                    HomeWorkWorkArea.this.urlList = updatedUrlLinkList;
                    if (tag == OnUrlSaveCallback.ON_SAVE) {
                        saveLinksOnServer(updatedUrlLinkList);
                    }
                }
            });
        }
        addUrlLinkDialog.showDialog(urlList);
    }

    private void saveLinksOnServer(ArrayList<AddUrlToWorkArea> updatedUrlLinkList) {
        /*if (isCallForGetHelp) {
            param.setUserId(student.getUserId());
            param.setPlayerId(student.getPlayerId());
            param.setHWId(homeWorkQuizzData.getHomeWorkId());
        } else {
            if (isWorkAreaForstudent) {
                param.setUserId(selectedPlayerData.getParentUserId());
                param.setPlayerId(selectedPlayerData.getPlayerid());
                param.setHWId(homeWorkQuizzData.getHomeWorkId());
            } else {//work area for teacher
                param.setUserId(studentDetail.getParentId());
                param.setPlayerId(studentDetail.getPlayerId());
                param.setHWId(studentDetail.getHomeWorkId());
            }
        }*/
        SaveGDocLinkOnServerParam param = new SaveGDocLinkOnServerParam();
        param.setAction("addGdocLinks");
        param.setUserId(selectedPlayerData.getParentUserId());
        param.setPlayerId(selectedPlayerData.getPlayerid());
        param.setHwId(homeWorkQuizzData.getHomeWorkId());
        param.setCusHwId(customeResult.getCustomHwId() + "");
        param.setQuestionNo(customeAns.getQueNo());
        param.setLinks(MathFriendzyHelper.getLinkString(updatedUrlLinkList));

        if (CommonUtils.isInternetConnectionAvailable(this)) {
            new MyAsyckTask(ServerOperation.createPostToSaveGDocOrWebLinks(param)
                    , null, ServerOperationUtil.SAVE_GDOC_WEB_LINK_REQUEST, this,
                    this, ServerOperationUtil.SIMPLE_DIALOG, true,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        }else{
            MathFriendzyHelper.saveUserLinksIntoLocalDB(this , param);
        }
    }


    //student put answer on work area
    /*private StudentPutAnswerWithKeyboard studentPutAnswerWithKeyboard = null;

    private void initStudentPutAnsWithKeyboardObj() {
        studentPutAnswerWithKeyboard
                = new StudentPutAnswerWithKeyboard(this, customeAns,
                playerAns, checkHomeWorkListLayout, keyboardLayout, lblAnswerOnWorkAreaCreditGiven,
                customeResult);
        studentPutAnswerWithKeyboard.setListenerOnNewKeys();
    }

    private void setStudentAnswerLayoutToPutAnswer() {
        studentPutAnswerWithKeyboard.setCustomeDataToListLayout();
    }*/

    /*public void onclkeqsmbl(View v) {
        studentPutAnswerWithKeyboard.onclkeqsmbl(v);
    }

    public void onkbclick(View v) {
        studentPutAnswerWithKeyboard.onkbclick(v);
    }

    public void onkb_abc_click(View v) {
        studentPutAnswerWithKeyboard.onkb_abc_click(v);
    }

    public void onkb_123_click(View v) {
        studentPutAnswerWithKeyboard.onkb_123_click(v);
    }

    public void onkb_123_symbol_click(View v) {
        studentPutAnswerWithKeyboard.onkb_123_symbol_click(v);
    }

    public void onkb_sub_symbol_click(View v) {
        studentPutAnswerWithKeyboard.onkb_sub_symbol_click(v);
    }

    public void onkb_sum_symbol_click(View v) {
        studentPutAnswerWithKeyboard.onkb_sum_symbol_click(v);
    }

    *//*public void onnextsubcall(final LinearLayout ll_sub) {
        studentPutAnswerWithKeyboard.onnextsubcall(ll_sub);
    }

    public void sub_next_sqrt_c(final LinearLayout ll_main_ll) {
        studentPutAnswerWithKeyboard.sub_next_sqrt_c(ll_main_ll);
    }

    public void sub_next_frac_bottom(final LinearLayout ll_main_ll) {
        studentPutAnswerWithKeyboard.sub_next_frac_bottom(ll_main_ll);
    }

    public void onpresubcall(final LinearLayout ll_sub) {
        studentPutAnswerWithKeyboard.onpresubcall(ll_sub);
    }*//*

    public void onnextclick() {
        studentPutAnswerWithKeyboard.onnextclick();
    }

    public void ondeleteclick() {
        studentPutAnswerWithKeyboard.ondeleteclick();
    }

    public void onpreviousclick() {
        studentPutAnswerWithKeyboard.onpreviousclick();
    }

    *//*public void sub_pre_sqrt_c(final LinearLayout ll_main_ll, final int is_main, final boolean is_add_ed) {
        studentPutAnswerWithKeyboard.sub_pre_sqrt_c(ll_main_ll , is_main , is_add_ed);
    }

    public void sub_pre_frac_top(final LinearLayout ll_main_ll, final int is_main, final boolean is_add_ed) {
        studentPutAnswerWithKeyboard.sub_pre_frac_top(ll_main_ll , is_main , is_add_ed);
    }*//*

    public void onplusminusclick(View v) {
        studentPutAnswerWithKeyboard.onplusminusclick(v);
    }*/


    //student put answer on work area
    private EditText selectedTextView = null, selected_ed_2 = null;
    private LinearLayout ll_visible_view = null, ll_ans_box_selected = null;
    private boolean is_keyboard_show = false;
    protected Animation animation = null;
    private LinearLayout ll_selected_view = null;
    private int dpwidth = 600;
    private boolean isVisibleStudentInput = false;
    private boolean initialSetData = true;

    private void initStudentInputViewAndData() {
        try {
            this.setVisibilityOfStudentPutAnswerLayout(isVisibleStudentInput);
            this.setVisibilityOfBtnShowInputAnswer(isWorkAreaForstudent);
            this.initializeWidth();
            this.setCustomeDataToListLayout();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initializeWidth(){
        try {
            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            // float dpWidth2 = displayMetrics.widthPixels / displayMetrics.density;
            float dpWidth2 = displayMetrics.widthPixels;
            dpwidth = Math.round(dpWidth2);
            if (dpwidth < 600) {
                dpwidth = 600;
            }
        } catch (Exception e) {
            dpwidth = 600;
        }
    }

    private void setVisibilityOfStudentPutAnswerLayout(boolean isVisible) {
        this.isVisibleStudentInput = isVisible;
        if (isVisible) {
            btnShowInputAnswer.setBackgroundResource(R.drawable.t_button_selected);
            studentPutAnswerLayout.setVisibility(View.VISIBLE);
        } else {
            btnShowInputAnswer.setBackgroundResource(R.drawable.show_student_input);
            studentPutAnswerLayout.setVisibility(View.GONE);
        }
    }

    private void setVisibilityOfBtnShowInputAnswer(boolean isVisible){
        if(isVisible){
            btnShowInputAnswer.setVisibility(View.VISIBLE);
        }else{
            btnShowInputAnswer.setVisibility(View.GONE);
        }
    }

    /**
     * Set the custom result data to the layout
     */
    public void setCustomeDataToListLayout() {
        checkHomeWorkListLayout.removeAllViews();
        //putViewOnKeyQuestionNumber.clear();
        View view = null;
        if (customeAns.getFillInType() == 1) {
            view = this.getCheckFillInLayout();
        } else if (customeAns.getAnswerType() == 1) {//for multiple choice
            view = this.getCheckMultiPleChoiceLayout();
        } else if (customeAns.getAnswerType() == 2) {//for true/false
            view = this.getCheckTrueFalseLayout();
        } else if (customeAns.getAnswerType() == 3) {//for yes/no
            view = this.getCheckTrueFalseLayout();
        }
        if (view != null) {
            checkHomeWorkListLayout.addView(view);
                /*putViewOnKeyQuestionNumber.put(customeAns.getQueNo()
                        , view);*/
        }
    }

    private void setViewBackGroundByTeacherCreditForInitialData(View view, CustomePlayerAns playerAns){
        try {
            if (playerAns.getTeacherCredit().equals("1")) {//for no credit
                view.setBackgroundColor(getResources().getColor(R.color.FULL_CREDIT));
            } else if (playerAns.getTeacherCredit().equals("2")) {//for zero credit
                view.setBackgroundColor(getResources().getColor(R.color.ZERO_CREDIT));
            } else if (playerAns.getTeacherCredit().equals("3")) {//half credit
                view.setBackgroundColor(getResources().getColor(R.color.HALF_CREDIT));
            } else if (playerAns.getTeacherCredit().equals("4")) {//full credit
                view.setBackgroundColor(getResources().getColor(R.color.FULL_CREDIT));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Inflate the layout for the fill in type
     *
     * @param
     * @return
     */
    @SuppressLint("InflateParams")
    public View getCheckFillInLayout() {
        final CustomePlayerAns playerAns = this.playerAns;
        View view = LayoutInflater.from(this).inflate(R.layout.kb_check_home_fill_in_blank_layout, null);
        RelativeLayout viewLayout = (RelativeLayout) view.findViewById(R.id.checkHomeLayout);
        Button btnSeeAnswer = (Button) view.findViewById(R.id.btnSeeAnswer);
        TextView txtNumber = (AutoResizeEditText) view.findViewById(R.id.txtNumber);
        TextView txtLeftUnit = (TextView) view.findViewById(R.id.txtLeftUnit);
        // TextView ansBox = (TextView) view.findViewById(R.id.ansBox);  //siddhiinfosoft
        TextView txtRighttUnit = (TextView) view.findViewById(R.id.txtRighttUnit);
        final Button btnRoughWork = (Button) view.findViewById(R.id.btnRoughWork);
        final LinearLayout ll_ans_box = (LinearLayout) view.findViewById(R.id.ll_ans_box); //siddhiinfosoft
        this.setRoughButtonBackGroundResources(btnRoughWork);

        txtNumber.setFocusable(false);
        txtNumber.setFocusableInTouchMode(false);

        //Add Question
        TextView edtQuestion = (TextView) view.findViewById(R.id.edtQuestion);
        LinearLayout ll_que_box = (LinearLayout) view.findViewById(R.id.ll_que_box);
        RelativeLayout rlQuestionLayout = (RelativeLayout) view.findViewById(R.id.rlQuestionLayout);
        this.setQuestion(customeAns , rlQuestionLayout , ll_que_box);

        txtNumber.setText(customeAns.getQueNo());
        if (customeAns.getIsLeftUnit() == 0) {
            txtLeftUnit.setText("");
            txtRighttUnit.setText(customeAns.getAnsSuffix());
        } else {
            txtLeftUnit.setText("");
            txtRighttUnit.setText(customeAns.getAnsSuffix());
        }
        ll_ans_box.setMinimumWidth(dpwidth - 170); // siddhiinfosoft
        ll_ans_box.setTag(R.id.first, "1");
        ll_ans_box.setTag(R.id.second, "main");
        this.setstudentansbox(customeAns, ll_ans_box, playerAns);  // siddhiinfosoft

        /*if(initialSetData){
            this.setViewBackGroundByTeacherCreditForInitialData(viewLayout, playerAns);
        }else {*/
        this.setViewBackGroundByTeacherCredit(viewLayout, playerAns);
        //}

        ll_ans_box.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (customeAns.getIsAnswerAvailable() == MathFriendzyHelper.NO) {
                    MathFriendzyHelper.showWarningDialog(getCurrentObj(),
                            lblAnswerOnWorkAreaCreditGiven);
                } else {
                    if (isQuizExpireForStudent()) {
                        showCantEditAnswerDialog();
                        return;
                    }else if(!isPlayerAnswerEditable(playerAns)){
                        showNoLongerChangeThisQuestion();
                        return ;
                    }else {
                        callclickmethod(customeAns, (LinearLayout) v, 0);
                    }
                }
            }
        });

        btnRoughWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculateAns(playerAns , makeansfromview(ll_ans_box));
            }
        });
        return view;
    }

    /**
     * Inflate the layout for not fill in type but for multiple choice answer type = 1
     *
     * @param
     * @return
     */
    @SuppressLint("InflateParams")
    public View getCheckMultiPleChoiceLayout() {
        final CustomePlayerAns playerAns = this.playerAns;

        View view = LayoutInflater.from(this).inflate(R.layout.check_home_multiple_choice_layout, null);
        RelativeLayout viewLayout = (RelativeLayout) view.findViewById(R.id.checkHomeLayout);
        Button btnSeeAnswer = (Button) view.findViewById(R.id.btnSeeAnswer);
        TextView txtNumber = (TextView) view.findViewById(R.id.txtNumber);
        final Button btnRoughWork = (Button) view.findViewById(R.id.btnRoughWork);
        LinearLayout optionLayout = (LinearLayout) view.findViewById(R.id.optionLayout);
        this.setRoughButtonBackGroundResources(btnRoughWork);

        //Add Question
        TextView edtQuestion = (TextView) view.findViewById(R.id.edtQuestion);
        LinearLayout ll_que_box = (LinearLayout) view.findViewById(R.id.ll_que_box);
        RelativeLayout rlQuestionLayout = (RelativeLayout) view.findViewById(R.id.rlQuestionLayout);
        this.setQuestion(customeAns , rlQuestionLayout , ll_que_box);

        final MultipleChoiceAnswer multipleChiceAns = new MultipleChoiceAnswer();
        multipleChiceAns.setQueId(customeAns.getQueNo());
        multipleChiceAns.setCustomeAns(customeAns);
        multipleChiceAns.setFirstTimeWrong(0);

        /*if(initialSetData){
            this.setViewBackGroundByTeacherCreditForInitialData(viewLayout, playerAns);
        }else {*/
        this.setViewBackGroundByTeacherCredit(viewLayout, playerAns);
        //}

        ArrayList<TextView> optionList = new ArrayList<TextView>();
        multipleChiceAns.setOptionList(optionList);
        String[] commaSepratedOption = MathFriendzyHelper.
                getCommaSepratedOption(customeAns.getOptions(), ",");

        for (int i = 0; i < commaSepratedOption.length; i++) {
            optionLayout.addView(this.getMultipleOptionLayout
                    (commaSepratedOption[i], customeAns, optionList,
                            multipleChiceAns, playerAns));
        }
        txtNumber.setText(customeAns.getQueNo());

        btnRoughWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculateAns(playerAns , multipleChiceAns.getUserAns());
            }
        });
        return view;
    }

    /**
     * Inflate the layout for not fill in type but for multiple choice answer type = 2 or 3
     * for true false or yes no
     *
     * @param
     * @return
     */
    @SuppressLint("InflateParams")
    public View getCheckTrueFalseLayout() {
        final CustomePlayerAns playerAns = this.playerAns;
        View view = LayoutInflater.from(this).inflate(R.layout.check_home_true_false_layout, null);
        RelativeLayout viewLayout = (RelativeLayout) view.findViewById(R.id.checkHomeLayout);
        Button btnSeeAnswer = (Button) view.findViewById(R.id.btnSeeAnswer);
        TextView txtNumber = (TextView) view.findViewById(R.id.txtNumber);
        final TextView ansTrue = (TextView) view.findViewById(R.id.ansTrue);
        final TextView ansFalse = (TextView) view.findViewById(R.id.ansFalse);
        final Button btnRoughWork = (Button) view.findViewById(R.id.btnRoughWork);
        this.setRoughButtonBackGroundResources(btnRoughWork);
        txtNumber.setText(customeAns.getQueNo());
        String[] ansArray = MathFriendzyHelper.getCommaSepratedOption(customeAns.getOptions(), ",");
        ansTrue.setText(ansArray[0]);
        ansFalse.setText(ansArray[1]);

        //Add Question
        TextView edtQuestion = (TextView) view.findViewById(R.id.edtQuestion);
        LinearLayout ll_que_box = (LinearLayout) view.findViewById(R.id.ll_que_box);
        RelativeLayout rlQuestionLayout = (RelativeLayout) view.findViewById(R.id.rlQuestionLayout);
        this.setQuestion(customeAns , rlQuestionLayout , ll_que_box);


        final CheckUserAnswer checkUserAns = new CheckUserAnswer();
        checkUserAns.setQueId(customeAns.getQueNo());
        checkUserAns.setCustomeAns(customeAns);
        checkUserAns.setTxtYes(ansTrue);
        checkUserAns.setTxtNo(ansFalse);
        checkUserAns.setFirstTimeWrong(0);

        /*if(initialSetData){
            this.setViewBackGroundByTeacherCreditForInitialData(viewLayout, playerAns);
        }else {*/
        this.setViewBackGroundByTeacherCredit(viewLayout, playerAns);
        //}

        if (playerAns != null) {
            if (playerAns.getAns().equals("Yes")) {
                checkUserAns.setUserAns(checkUserAns.getTxtYes().getText().toString());
                checkUserAns.getTxtYes().setBackgroundResource(R.drawable.checked_box_home_work);
                checkUserAns.getTxtNo().setBackgroundResource(R.drawable.unchecked_box_home_work);
            } else if (playerAns.getAns().equals("No")) {
                checkUserAns.setUserAns(checkUserAns.getTxtNo().getText().toString());
                checkUserAns.getTxtYes().setBackgroundResource(R.drawable.unchecked_box_home_work);
                checkUserAns.getTxtNo().setBackgroundResource(R.drawable.checked_box_home_work);
            } else if (playerAns.getAns().equals("True")) {
                checkUserAns.setUserAns(checkUserAns.getTxtYes().getText().toString());
                checkUserAns.getTxtYes().setBackgroundResource(R.drawable.checked_box_home_work);
                checkUserAns.getTxtNo().setBackgroundResource(R.drawable.unchecked_box_home_work);
            } else if (playerAns.getAns().equals("False")) {
                checkUserAns.setUserAns(checkUserAns.getTxtNo().getText().toString());
                checkUserAns.getTxtYes().setBackgroundResource(R.drawable.unchecked_box_home_work);
                checkUserAns.getTxtNo().setBackgroundResource(R.drawable.checked_box_home_work);
            }
        }

        if(this.isPlayerAnswerEditable(playerAns)) {
            ansTrue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkUserAns.setUserAns(checkUserAns.getTxtYes().getText().toString());
                    checkUserAns.getTxtYes().setBackgroundResource(R.drawable.checked_box_home_work);
                    checkUserAns.getTxtNo().setBackgroundResource(R.drawable.unchecked_box_home_work);
                }
            });

            ansFalse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkUserAns.setUserAns(checkUserAns.getTxtNo().getText().toString());
                    checkUserAns.getTxtNo().setBackgroundResource(R.drawable.checked_box_home_work);
                    checkUserAns.getTxtYes().setBackgroundResource(R.drawable.unchecked_box_home_work);

                }
            });
        }else{
            ansTrue.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isQuizExpireForStudent()){
                        showCantEditAnswerDialog();
                        return ;
                    }else if(!isPlayerAnswerEditable(playerAns)){
                        showNoLongerChangeThisQuestion();
                        return ;
                    }
                }
            });

            ansFalse.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isQuizExpireForStudent()){
                        showCantEditAnswerDialog();
                        return ;
                    }else if(!isPlayerAnswerEditable(playerAns)){
                        showNoLongerChangeThisQuestion();
                        return ;
                    }
                }
            });
        }

        btnRoughWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculateAns(playerAns , checkUserAns.getUserAns());
            }
        });
        return view;
    }

    private void setRoughButtonBackGroundResources(Button btnRoughWork){
        btnRoughWork.setBackgroundResource(R.drawable.calculator);
    }

    //Add Question
    private void setQuestion(CustomeAns customeAns, RelativeLayout rlQuestionLayout ,
                             LinearLayout ll_que_box) {
        try{
            if(MathFriendzyHelper.isEmpty(customeAns.getQuestionString())){
                rlQuestionLayout.setVisibility(View.GONE);
            }else{
                ll_que_box.setMinimumWidth(ActAssignCustomAns.dpwidth - 200);
                ll_que_box.setTag(R.id.first, "1");
                ll_que_box.setTag(R.id.second, "main");
                this.addansboxview(ll_que_box, this.getUpdatedQuestionString(customeAns.getQuestionString())
                        , MathFriendzyHelper.YES);
                rlQuestionLayout.setVisibility(View.VISIBLE);
            }
        }catch (Exception e){
            e.printStackTrace();
            rlQuestionLayout.setVisibility(View.GONE);
        }
    }

    //update the question string to show the old question with the new keyboard
    private String getUpdatedQuestionString(String string){
        if(string.contains("#")){
            return string;
        }
        return "text#" + string;
    }

    /**
     * Return the inflated view to multiple choice option
     *
     * @param optionText
     * @param customeAns
     * @param optionList
     * @param multipleChiceAns
     * @param playerAns
     * @return
     */
    @SuppressLint("InflateParams")
    private View getMultipleOptionLayout(String optionText,
                                         final CustomeAns customeAns, ArrayList<TextView> optionList,
                                         final MultipleChoiceAnswer multipleChiceAns, final CustomePlayerAns playerAns) {
        View view = LayoutInflater.from(this).inflate(R.layout.check_home_work_option_layout, null);
        final TextView option = (TextView) view.findViewById(R.id.option);
        option.setText(optionText);
        optionList.add(option);

        if (playerAns != null) {
            if (playerAns.getAns().contains(optionText)) {
                if (multipleChiceAns.getUserAns() != null) {
                    multipleChiceAns.setUserAns(multipleChiceAns.getUserAns() + option.getText().toString());
                } else {
                    multipleChiceAns.setUserAns(option.getText().toString());
                }
                option.setBackgroundResource(R.drawable.checked_small_box_home_work);
            }
        }

        if(this.isPlayerAnswerEditable(playerAns)) {
            option.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int j = 0; j < multipleChiceAns.getOptionList().size(); j++) {
                        if (v == multipleChiceAns.getOptionList().get(j)) {
                            if (multipleChiceAns.getUserAns() != null) {
                                String strBuilder = multipleChiceAns.getUserAns();

                                if (multipleChiceAns.getUserAns().contains
                                        (multipleChiceAns
                                                .getOptionList().get(j).getText().toString())) {
                                    multipleChiceAns.getOptionList().get(j)
                                            .setBackgroundResource
                                                    (R.drawable.unchecked_small_box_home_work);
                                    strBuilder = strBuilder.replace(multipleChiceAns
                                            .getOptionList().get(j).getText().toString(), "");
                                } else {
                                    multipleChiceAns.getOptionList().get(j)
                                            .setBackgroundResource
                                                    (R.drawable.checked_small_box_home_work);
                                    strBuilder = strBuilder + multipleChiceAns
                                            .getOptionList().get(j).getText().toString();

                                }
                                multipleChiceAns.setUserAns(strBuilder);
                            } else {
                                multipleChiceAns.getOptionList().get(j)
                                        .setBackgroundResource
                                                (R.drawable.checked_small_box_home_work);
                                multipleChiceAns.setUserAns(multipleChiceAns
                                        .getOptionList().get(j).getText().toString());
                            }
                        }
                    }
                }
            });
        }else{
            option.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isQuizExpireForStudent()){
                        showCantEditAnswerDialog();
                        return ;
                    }else if(!isPlayerAnswerEditable(playerAns)){
                        showNoLongerChangeThisQuestion();
                        return ;
                    }
                }
            });
        }
        return view;
    }


    private void reInitializeVariableForFillInType(){
        selectedTextView = null;
        selected_ed_2 = null;
        ll_visible_view = null;
        ll_ans_box_selected = null;
        ll_selected_view = null;
    }

    private boolean isAnsUpdated = false;
    private void calculateAns(CustomePlayerAns playerAns , String userAns){
        try {
            this.hideKeyboard();
            initialSetData = false;
            isAnsUpdated = true;
            playerAns.setAns(userAns);
            if (customeAns.getFillInType() == 1) {
                if(customeAns.getIsAnswerAvailable() == MathFriendzyHelper.NO){
                    MathFriendzyHelper.showWarningDialog(getCurrentObj(),
                            lblAnswerOnWorkAreaCreditGiven);
                    return ;
                }
                if (this.compareFillInAns(customeAns.getCorrectAns(), userAns)) {
                    playerAns.setIsCorrect(MathFriendzyHelper.YES);
                    ActCheckHomeWork.getCurrentObj().setTeacherCredit(ActCheckHomeWork
                            .getCurrentObj().clickedStudent);
                } else {
                    ActCheckHomeWork.getCurrentObj().setTeacherCreditForWrongAns(ActCheckHomeWork
                            .getCurrentObj().clickedStudent);
                }
            } else if (customeAns.getAnswerType() == 1) {//for multiple choice
                userAns = MathFriendzyHelper.
                        convertStringIntoSperatedStringWithDelimeter(userAns, ",");
                if (this.compareMultiPleChoiceAns(customeAns.getCorrectAns(), userAns)) {
                    playerAns.setIsCorrect(MathFriendzyHelper.YES);
                    ActCheckHomeWork.getCurrentObj().setTeacherCredit(ActCheckHomeWork
                            .getCurrentObj().clickedStudent);
                } else {
                    ActCheckHomeWork.getCurrentObj().setTeacherCreditForWrongAns(ActCheckHomeWork
                            .getCurrentObj().clickedStudent);
                }
            } else if (customeAns.getAnswerType() == 2) {//for true/false
                if (this.compareTrueFalseOrYesNoAns(customeAns.getCorrectAns(), userAns)) {
                    playerAns.setIsCorrect(MathFriendzyHelper.YES);
                    ActCheckHomeWork.getCurrentObj().setTeacherCredit(ActCheckHomeWork
                            .getCurrentObj().clickedStudent);
                } else {
                    ActCheckHomeWork.getCurrentObj().setTeacherCreditForWrongAns(ActCheckHomeWork
                            .getCurrentObj().clickedStudent);
                }
            } else if (customeAns.getAnswerType() == 3) {//for yes/no
                if (this.compareTrueFalseOrYesNoAns(customeAns.getCorrectAns(), userAns)) {
                    playerAns.setIsCorrect(MathFriendzyHelper.YES);
                    ActCheckHomeWork.getCurrentObj().setTeacherCredit(ActCheckHomeWork
                            .getCurrentObj().clickedStudent);
                } else {
                    ActCheckHomeWork.getCurrentObj().setTeacherCreditForWrongAns(ActCheckHomeWork
                            .getCurrentObj().clickedStudent);
                }
            }
            playerAns.setTeacherCredit("" + (ActCheckHomeWork
                    .getCurrentObj().clickedStudent.getTeacherCredit()));
            this.reInitializeVariableForFillInType();
            this.setCustomeDataToListLayout();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * compare multiple choice ans
     *
     * @param teacherAns
     * @param studentAns
     * @return
     */
    private boolean compareMultiPleChoiceAns(String teacherAns, String studentAns) {
        //Log.e(TAG, "teacher ans " + teacherAns + " student and " + studentAns);
        String[] commaSepratedString = MathFriendzyHelper.getCommaSepratedOption(studentAns, ",");
        String[] commaSepratedTeacherString = MathFriendzyHelper
                .getCommaSepratedOption(teacherAns, ",");

        if (commaSepratedString.length == commaSepratedTeacherString.length) {
            if (commaSepratedString.length > 0) {
                for (int i = 0; i < commaSepratedString.length; i++) {
                    if (!teacherAns.contains(commaSepratedString[i])) {
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * compare true/false and yes/no ans
     *
     * @param teacherAns
     * @param studentAns
     * @return
     */
    private boolean compareTrueFalseOrYesNoAns(String teacherAns, String studentAns) {
        return teacherAns.equalsIgnoreCase(studentAns);
    }

    /**
     * Based on has seen by student
     */
    private void setTeacherCredit(StudentAnsBase std) {
        if (std.isHasSeenAns() || std.isGetHelpFromOther() == 1/*|| std.getFirstTimeWrong() == 1*/) {
            std.setTeacherCredit(HALF_CREDIT);//for half credit
            std.setGetHelpFromOther(1);
        } else {
            std.setTeacherCredit(FULL_CREDIT);//for half credit
        }
    }

    /**
     * Set teacher credit for wrong ans
     *
     * @param std
     */
    private void setTeacherCreditForWrongAns(StudentAnsBase std) {
        //std.setTeacherCredit(NONE_CREDITT);
        if (customeResult.isAlreadyPlayed()) {
            std.setTeacherCredit(ZERO_CREDIT);
//            if(std.isPrevAnsCorrect() == 1){
//                std.setTeacherCredit(NONE_CREDITT);
//            }
        } else {
            std.setTeacherCredit(NONE_CREDITT);
        }
    }

    private boolean isQuizExpireForStudent() {
        if (!isWorkAreaForstudent) {
            return false;
        } else {
            if (isExpireQuizz)
                return true;
        }
        return false;
    }

    private void showCantEditAnswerDialog() {
        MathFriendzyHelper.showWarningDialog(this, MathFriendzyHelper.getTreanslationTextById(this ,
                "lblSorryYouAreNotAllowedToPlay"));
    }

    private void showNoLongerChangeThisQuestion(){
        if(isWorkAreaForstudent) {
            MathFriendzyHelper.showWarningDialog(this,
                    MathFriendzyHelper.getTreanslationTextById(this, "lblCantChangeAns"));
        }
    }

    //change for the edit those answer for not allow change which are not given by student
    private boolean isPlayerAnswerEditable(CustomePlayerAns playerAns){
        try {
            if (isExpireQuizz) {
                return false;
            } else {
                if (customeResult.getAllowChanges() == 1 ||
                        !customeResult.isAlreadyPlayed()) {
                    return true;
                } else {
                    if (this.isPlayerAnsGiven(playerAns))
                        return false;
                    else//chage for to update the answer not given
                        return true;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            return true;
        }
    }

    private boolean isPlayerAnsGiven(CustomePlayerAns playerAns){
        try {
            if (playerAns != null) {
                if (playerAns.getAns().length() > 0) {
                    return true;
                }
            }
            return false;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Convert the str teacher credit into int
     *
     * @param strTeacherCredit
     * @return
     */
    private int getIntTeacherCreditValue(String strTeacherCredit) {
        try {
            return Integer.parseInt(strTeacherCredit);
        } catch (Exception e) {
            return 1;
        }
    }

    private boolean compareFillInAns(String teacherAns, String studentAns) { // siddhiinfosoft
        try {
            boolean brt = false;

            teacherAns = teacherAns.replace(" ", "");
            studentAns = studentAns.replace(" ", "");

            teacherAns = teacherAns.replace("-", "op_minus");
            studentAns = studentAns.replace("-", "op_minus");

            if (!(teacherAns.contains("#"))) { // for old keyboard

                String str = studentAns.replace("text#", "");

                brt = teacherAns.replace(",", "").equalsIgnoreCase(str.replace(",", ""));
                return brt;
            }

            try {
                String stsr = teacherAns.substring(teacherAns.length() - 4);

                if (stsr.equals("@op#")) {
                    teacherAns = teacherAns.substring(0, (teacherAns.length() - 4));
                }

            } catch (Exception e) {

            }

            brt = teacherAns.replace(",", "").equalsIgnoreCase(studentAns.replace(",", ""));

            if (brt) {
                return brt;
            } else {

                brt = this.comparereversestring(teacherAns, studentAns);

                return brt;
            }
        } catch (Exception e) {
            return false;
        }
    }

    private boolean comparereversestring(String correct_ans, String user_ans) {
        try {
            String[] str_1 = correct_ans.split("@");
            String[] str_2 = user_ans.split("@");

            Collections.reverse(Arrays.asList(str_1));

            boolean btr = false;

            for (int i = 0; i < str_1.length; i++) {

                String sstr = str_1[i];

                if (sstr.contains("op") || sstr.contains("text")) {

                    if (sstr.contains("op_equal") || sstr.contains("op_plus") || sstr.contains("op_multi") || sstr.contains("text")) {
                        btr = true;
                    } else {
                        btr = false;
                        break;
                    }

                } else {
                    btr = false;
                    break;

                }

            }

            if (btr) {

                if (Arrays.equals(str_1, str_2)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public void showKeyboard() {

        try {    // siddhiinfosoft
            this.hideDeviceKeyboard();
            if (!is_keyboard_show) {
                if (isWorkAreaForstudent) {

                    if(this.isQuizExpireForStudent()){
                        this.showCantEditAnswerDialog();
                        return ;
                    }

                    is_keyboard_show = true;
                    keyboardLayout.setVisibility(RelativeLayout.VISIBLE);
                    animation = AnimationUtils.loadAnimation(this, R.anim.keyboard_show_animation);
                    keyboardLayout.setAnimation(animation);
                }
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    hideKeyboardAfterSomeTime();
                }
            } , 10);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void hideKeyboard() {

        try {   // siddhiinfosoft
            if (is_keyboard_show) {
                is_keyboard_show = false;
                animation = AnimationUtils.loadAnimation(this, R.anim.keyboard_hide_animation);
                keyboardLayout.setAnimation(animation);
                keyboardLayout.setVisibility(RelativeLayout.GONE);
                //this.cleasFocusFromEditText();
                ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
                ll_main_eq_sub_symbol.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String replacecharfromsign(final String str) {
        String str_final = str;

        try {

            String str_plus = "" + Html.fromHtml("&#43;");
            String str_minus = "" + Html.fromHtml("&#8722;");
            String str_equal = "" + Html.fromHtml("&#61;");
            String str_multi = "" + Html.fromHtml("&#215;");
            String str_divide = "" + Html.fromHtml("&#247;");
            String str_union = "" + Html.fromHtml("&#8746;");
            String str_intersection = "" + Html.fromHtml("&#8745;");
            String str_pi = "" + Html.fromHtml("&#960;");
            String str_factorial = "" + Html.fromHtml("&#33;");
            String str_percentage = "" + Html.fromHtml("&#37;");
            String str_goe = "" + Html.fromHtml("&#8805;");
            String str_loe = "" + Html.fromHtml("&#8804;");
            String str_grthan = "" + Html.fromHtml("&#62;");
            String str_lethan = "" + Html.fromHtml("&#60;");
            String str_infinity = "" + Html.fromHtml("&#8734;");
            String str_degree = "" + Html.fromHtml("&#176;");
            String str_xbar = "" + Html.fromHtml("x&#x0304;");

            String str_integral = "" + Html.fromHtml("&#8747;");
            String str_summation = "" + Html.fromHtml("&#8721;");
            String str_alpha = "" + Html.fromHtml("&#945;");
            String str_theta = "" + Html.fromHtml("ø");
            String str_mu = "" + Html.fromHtml("&#956;");
            String str_sigma = "" + Html.fromHtml("&#963;");

            String str_beta = "" + Html.fromHtml("&#946;");
            String str_angle = "" + Html.fromHtml("&#8736;");
            String str_mangle = "" + Html.fromHtml("&#8737;");
            String str_sangle = "" + Html.fromHtml("&#8738;");
            String str_rangle = "" + Html.fromHtml("&#8735;");
            String str_triangle = "" + Html.fromHtml("&#9651;");
            String str_rectangle = "" + Html.fromHtml("&#9645;");
            String str_parallelogram = "" + Html.fromHtml("&#9649;");
            String str_perpendicular = "" + Html.fromHtml("&#8869;");
            String str_congruent = "" + Html.fromHtml("&#8773;");
            String str_similarty = "" + Html.fromHtml("&#8764;");
            String str_parallel = "" + Html.fromHtml("&#8741;");

            String str_arcm = "" + Html.fromHtml("&#8242;");
            String str_arcs = "" + Html.fromHtml("&#8243;");

            str_final = str_final.replace(str_plus, "op_plus");
            str_final = str_final.replace(str_minus, "op_minus");
            str_final = str_final.replace(str_equal, "op_equal");
            str_final = str_final.replace(str_multi, "op_multi");
            str_final = str_final.replace(str_divide, "op_divide");
            str_final = str_final.replace(str_union, "op_union");
            str_final = str_final.replace(str_intersection, "op_intersection");
            str_final = str_final.replace(str_pi, "op_pi");
            str_final = str_final.replace(str_factorial, "op_factorial");
            str_final = str_final.replace(str_percentage, "op_percentage");
            str_final = str_final.replace(str_goe, "op_goe");
            str_final = str_final.replace(str_loe, "op_loe");
            str_final = str_final.replace(str_grthan, "op_grthan");
            str_final = str_final.replace(str_lethan, "op_lethan");
            str_final = str_final.replace(str_infinity, "op_infinity");
            str_final = str_final.replace(str_degree, "op_degree");
            str_final = str_final.replace(str_xbar, "op_xbar");

            str_final = str_final.replace(str_integral, "op_integral");
            str_final = str_final.replace(str_summation, "op_summation");
            str_final = str_final.replace(str_alpha, "op_alpha");
            str_final = str_final.replace(str_theta, "op_theta");
            str_final = str_final.replace(str_mu, "op_mu");
            str_final = str_final.replace(str_sigma, "op_sigma");

            str_final = str_final.replace(str_beta, "op_beta");
            str_final = str_final.replace(str_angle, "op_angle");
            str_final = str_final.replace(str_mangle, "op_mangle");
            str_final = str_final.replace(str_sangle, "op_sangle");
            str_final = str_final.replace(str_rangle, "op_rangle");
            str_final = str_final.replace(str_triangle, "op_triangle");
            str_final = str_final.replace(str_rectangle, "op_rectangle");
            str_final = str_final.replace(str_parallelogram, "op_parallelogram");
            str_final = str_final.replace(str_perpendicular, "op_perpendicular");
            str_final = str_final.replace(str_congruent, "op_congruent");
            str_final = str_final.replace(str_similarty, "op_similarty");
            str_final = str_final.replace(str_parallel, "op_parallel");

            str_final = str_final.replace(str_arcm, "op_arcm");
            str_final = str_final.replace(str_arcs, "op_arcs");


            //by a1
            str_final = str_final.replace("-", "op_minus");


        } catch (Exception e) {
            e.printStackTrace();
        }

        return str_final;
    }

    private void setvisibilityofabc123layout(int int_layout) { // siddhiinfosoft

        if (int_layout == 1) {
            kb_ll_123.setVisibility(View.VISIBLE);
            kb_ll_abc.setVisibility(View.GONE);
        } else {
            kb_ll_123.setVisibility(View.GONE);
            kb_ll_abc.setVisibility(View.VISIBLE);
        }

    }

    protected void setanimationtoeqsymbols1() {  // siddhiinfosoft
        try {
            animation = AnimationUtils.loadAnimation(this, R.anim.symbol_hide_animation);
            hsv_keyboard_eq_symbol.setAnimation(animation);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void setanimationtoeqsymbols2() {
        try {
            animation = AnimationUtils.loadAnimation(this, R.anim.symbol_show_animation);
            hsv_keyboard_eq_symbol.setAnimation(animation);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onclkeqsmbl(View v) {

        try {

            int id_view = v.getId();
            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            if (ll_visible_view != null) {
                ll_visible_view.setVisibility(View.GONE);
            }

            switch (id_view) {

                case R.id.kb_eq_symbol_basicmath_sign1:
                case R.id.kb_eq_symbol_basicmath_sign1_arrow:
                    ll_visible_view = ll_kb_sub_sumbol_basicmath_sign1;
                    Utils.setvisibility_basicmath_subeq(ll_kb_sub_sumbol_basicmath_sign1, ll_kb_sub_sumbol_basicmath_sign2, ll_kb_sub_sumbol_basicmath_sign5);

                    break;

                case R.id.kb_eq_symbol_basicmath_sign2:
                case R.id.kb_eq_symbol_basicmath_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_basicmath_sign2;
                    Utils.setvisibility_basicmath_subeq(ll_kb_sub_sumbol_basicmath_sign2, ll_kb_sub_sumbol_basicmath_sign1, ll_kb_sub_sumbol_basicmath_sign5);

                    break;

                case R.id.kb_eq_symbol_basicmath_sign5:
                case R.id.kb_eq_symbol_basicmath_sign5_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_basicmath_sign5;
                    Utils.setvisibility_basicmath_subeq(ll_kb_sub_sumbol_basicmath_sign5, ll_kb_sub_sumbol_basicmath_sign1, ll_kb_sub_sumbol_basicmath_sign2);

                    break;

                case R.id.kb_eq_symbol_prealgebra_sign1:
                case R.id.kb_eq_symbol_prealgebra_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_prealgebra_sign1;
                    Utils.setvisibility_prealgebra_subeq(ll_kb_sub_sumbol_prealgebra_sign1, ll_kb_sub_sumbol_prealgebra_sign2,
                            ll_kb_sub_sumbol_prealgebra_sign4, ll_kb_sub_sumbol_prealgebra_sign5);

                    break;

                case R.id.kb_eq_symbol_prealgebra_sign2:
                case R.id.kb_eq_symbol_prealgebra_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_prealgebra_sign2;
                    Utils.setvisibility_prealgebra_subeq(ll_kb_sub_sumbol_prealgebra_sign2, ll_kb_sub_sumbol_prealgebra_sign1,
                            ll_kb_sub_sumbol_prealgebra_sign4, ll_kb_sub_sumbol_prealgebra_sign5);

                    break;

                case R.id.kb_eq_symbol_prealgebra_sign4:
                case R.id.kb_eq_symbol_prealgebra_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_prealgebra_sign4;
                    Utils.setvisibility_prealgebra_subeq(ll_kb_sub_sumbol_prealgebra_sign4, ll_kb_sub_sumbol_prealgebra_sign2,
                            ll_kb_sub_sumbol_prealgebra_sign1, ll_kb_sub_sumbol_prealgebra_sign5);


                    break;

                case R.id.kb_eq_symbol_prealgebra_sign5:
                case R.id.kb_eq_symbol_prealgebra_sign5_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_prealgebra_sign5;
                    Utils.setvisibility_prealgebra_subeq(ll_kb_sub_sumbol_prealgebra_sign5, ll_kb_sub_sumbol_prealgebra_sign1, ll_kb_sub_sumbol_prealgebra_sign2,
                            ll_kb_sub_sumbol_prealgebra_sign4);


                    break;

                case R.id.kb_eq_symbol_algebra_sign1:
                case R.id.kb_eq_symbol_algebra_sign1_arrow:
                case R.id.kb_eq_symbol_geometry_sign1:
                case R.id.kb_eq_symbol_geometry_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_algebra_sign1;
                    Utils.setvisibility_algebra_subeq(ll_kb_sub_sumbol_algebra_sign1, ll_kb_sub_sumbol_algebra_sign2,
                            ll_kb_sub_sumbol_algebra_sign4, ll_kb_sub_sumbol_algebra_sign5,
                            ll_kb_sub_sumbol_algebra_sign8);


                    break;

                case R.id.kb_eq_symbol_algebra_sign2:
                case R.id.kb_eq_symbol_algebra_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_algebra_sign2;
                    Utils.setvisibility_algebra_subeq(ll_kb_sub_sumbol_algebra_sign2, ll_kb_sub_sumbol_algebra_sign1,
                            ll_kb_sub_sumbol_algebra_sign4, ll_kb_sub_sumbol_algebra_sign5,
                            ll_kb_sub_sumbol_algebra_sign8);

                    break;

                case R.id.kb_eq_symbol_algebra_sign4:
                case R.id.kb_eq_symbol_algebra_sign4_arrow:
                case R.id.kb_eq_symbol_geometry_sign3:
                case R.id.kb_eq_symbol_geometry_sign3_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_algebra_sign4;
                    Utils.setvisibility_algebra_subeq(ll_kb_sub_sumbol_algebra_sign4, ll_kb_sub_sumbol_algebra_sign2,
                            ll_kb_sub_sumbol_algebra_sign1,
                            ll_kb_sub_sumbol_algebra_sign5,
                            ll_kb_sub_sumbol_algebra_sign8);

                    break;

                case R.id.kb_eq_symbol_algebra_sign5:
                case R.id.kb_eq_symbol_algebra_sign5_arrow:
                case R.id.kb_eq_symbol_geometry_sign4:
                case R.id.kb_eq_symbol_geometry_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_algebra_sign5;
                    Utils.setvisibility_algebra_subeq(ll_kb_sub_sumbol_algebra_sign5, ll_kb_sub_sumbol_algebra_sign4,
                            ll_kb_sub_sumbol_algebra_sign2, ll_kb_sub_sumbol_algebra_sign1,
                            ll_kb_sub_sumbol_algebra_sign8);

                    break;


                case R.id.kb_eq_symbol_algebra_sign8:
                case R.id.kb_eq_symbol_algebra_sign8_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_algebra_sign8;
                    Utils.setvisibility_algebra_subeq(ll_kb_sub_sumbol_algebra_sign8, ll_kb_sub_sumbol_algebra_sign5, ll_kb_sub_sumbol_algebra_sign4,
                            ll_kb_sub_sumbol_algebra_sign2, ll_kb_sub_sumbol_algebra_sign1
                    );

                    break;

                case R.id.kb_eq_symbol_trignometry_sign1:
                case R.id.kb_eq_symbol_trignometry_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign1;
                    ll_kb_sub_sumbol_trignometry_sign1.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign2:
                case R.id.kb_eq_symbol_trignometry_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign2;
                    ll_kb_sub_sumbol_trignometry_sign2.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign3:

                    break;

                case R.id.kb_eq_symbol_trignometry_sign4:
                case R.id.kb_eq_symbol_trignometry_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign4;
                    ll_kb_sub_sumbol_trignometry_sign4.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign5:
                case R.id.kb_eq_symbol_trignometry_sign5_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign5;
                    ll_kb_sub_sumbol_trignometry_sign5.setVisibility(View.VISIBLE);

                    break;


                case R.id.kb_eq_symbol_trignometry_sign7:
                case R.id.kb_eq_symbol_trignometry_sign7_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign7;
                    ll_kb_sub_sumbol_trignometry_sign7.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign8:
                case R.id.kb_eq_symbol_trignometry_sign8_arrow:
                case R.id.kb_eq_symbol_geometry_sign8:
                case R.id.kb_eq_symbol_geometry_sign8_arrow:
                case R.id.kb_eq_symbol_calculus_sign11:
                case R.id.kb_eq_symbol_calculus_sign11_arrow:
                case R.id.kb_eq_symbol_precalculus_sign10:
                case R.id.kb_eq_symbol_precalculus_sign10_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign8;
                    ll_kb_sub_sumbol_trignometry_sign8.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign9:
                case R.id.kb_eq_symbol_trignometry_sign9_arrow:
                case R.id.kb_eq_symbol_geometry_sign9:
                case R.id.kb_eq_symbol_geometry_sign9_arrow:
                case R.id.kb_eq_symbol_calculus_sign12:
                case R.id.kb_eq_symbol_calculus_sign12_arrow:
                case R.id.kb_eq_symbol_precalculus_sign11:
                case R.id.kb_eq_symbol_precalculus_sign11_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign9;
                    ll_kb_sub_sumbol_trignometry_sign9.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign10:
                case R.id.kb_eq_symbol_trignometry_sign10_arrow:
                case R.id.kb_eq_symbol_geometry_sign10:
                case R.id.kb_eq_symbol_geometry_sign10_arrow:
                case R.id.kb_eq_symbol_calculus_sign13:
                case R.id.kb_eq_symbol_calculus_sign13_arrow:
                case R.id.kb_eq_symbol_precalculus_sign12:
                case R.id.kb_eq_symbol_precalculus_sign12_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign10;
                    ll_kb_sub_sumbol_trignometry_sign10.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign11:
                case R.id.kb_eq_symbol_trignometry_sign11_arrow:
                case R.id.kb_eq_symbol_geometry_sign11:
                case R.id.kb_eq_symbol_geometry_sign11_arrow:
                case R.id.kb_eq_symbol_calculus_sign14:
                case R.id.kb_eq_symbol_calculus_sign14_arrow:
                case R.id.kb_eq_symbol_precalculus_sign13:
                case R.id.kb_eq_symbol_precalculus_sign13_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign11;
                    ll_kb_sub_sumbol_trignometry_sign11.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign12:
                case R.id.kb_eq_symbol_trignometry_sign12_arrow:
                case R.id.kb_eq_symbol_geometry_sign12:
                case R.id.kb_eq_symbol_geometry_sign12_arrow:
                case R.id.kb_eq_symbol_calculus_sign15:
                case R.id.kb_eq_symbol_calculus_sign15_arrow:
                case R.id.kb_eq_symbol_precalculus_sign14:
                case R.id.kb_eq_symbol_precalculus_sign14_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign12;
                    ll_kb_sub_sumbol_trignometry_sign12.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign13:
                case R.id.kb_eq_symbol_trignometry_sign13_arrow:
                case R.id.kb_eq_symbol_geometry_sign13:
                case R.id.kb_eq_symbol_geometry_sign13_arrow:
                case R.id.kb_eq_symbol_calculus_sign16:
                case R.id.kb_eq_symbol_calculus_sign16_arrow:
                case R.id.kb_eq_symbol_precalculus_sign15:
                case R.id.kb_eq_symbol_precalculus_sign15_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign13;
                    ll_kb_sub_sumbol_trignometry_sign13.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_precalculus_sign1:
                case R.id.kb_eq_symbol_precalculus_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_precalculus_sign1;
                    ll_kb_sub_sumbol_precalculus_sign1.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_precalculus_sign2:
                case R.id.kb_eq_symbol_precalculus_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_precalculus_sign2;
                    ll_kb_sub_sumbol_precalculus_sign2.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_precalculus_sign3:

                    break;

                case R.id.kb_eq_symbol_precalculus_sign4:
                case R.id.kb_eq_symbol_precalculus_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_precalculus_sign4;
                    ll_kb_sub_sumbol_precalculus_sign4.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_precalculus_sign5:
                case R.id.kb_eq_symbol_precalculus_sign5_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_precalculus_sign5;
                    ll_kb_sub_sumbol_precalculus_sign5.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_precalculus_sign6:

                    break;

                case R.id.kb_eq_symbol_precalculus_sign7:

                    break;

                case R.id.kb_eq_symbol_precalculus_sign9:
                case R.id.kb_eq_symbol_precalculus_sign9_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_precalculus_sign9;
                    ll_kb_sub_sumbol_precalculus_sign9.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_geometry_sign2:
                case R.id.kb_eq_symbol_geometry_sign2_arrow:

                    ll_visible_view = ll_kb_sub_symbol_geometry_sign2;
                    ll_kb_sub_symbol_geometry_sign2.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_geometry_sign5:
                case R.id.kb_eq_symbol_geometry_sign5_arrow:

                    ll_visible_view = ll_kb_sub_symbol_geometry_sign5;
                    ll_kb_sub_symbol_geometry_sign5.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_geometry_sign6:
                case R.id.kb_eq_symbol_geometry_sign6_arrow:

                    ll_visible_view = ll_kb_sub_symbol_geometry_sign6;
                    ll_kb_sub_symbol_geometry_sign6.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_geometry_sign7:
                case R.id.kb_eq_symbol_geometry_sign7_arrow:

                    ll_visible_view = ll_kb_sub_symbol_geometry_sign7;
                    ll_kb_sub_symbol_geometry_sign7.setVisibility(View.VISIBLE);

                    break;


                case R.id.kb_eq_symbol_calculus_sign1:
                case R.id.kb_eq_symbol_calculus_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign1;
                    ll_kb_sub_sumbol_calculus_sign1.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_calculus_sign2:
                case R.id.kb_eq_symbol_calculus_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign2;
                    ll_kb_sub_sumbol_calculus_sign2.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_calculus_sign3:

                    break;

                case R.id.kb_eq_symbol_calculus_sign4:
                case R.id.kb_eq_symbol_calculus_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign4;
                    ll_kb_sub_sumbol_calculus_sign4.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_calculus_sign5:
                case R.id.kb_eq_symbol_calculus_sign5_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign5;
                    ll_kb_sub_sumbol_calculus_sign5.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_calculus_sign6:

                    break;

                case R.id.kb_eq_symbol_calculus_sign7:

                    break;

                case R.id.kb_eq_symbol_calculus_sign10:
                case R.id.kb_eq_symbol_calculus_sign10_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign10;
                    ll_kb_sub_sumbol_calculus_sign10.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_statistics_sign1:
                case R.id.kb_eq_symbol_statistics_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_statistics_sign1;
                    ll_kb_sub_sumbol_statistics_sign1.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_statistics_sign2:

                    break;

                case R.id.kb_eq_symbol_statistics_sign3:
                case R.id.kb_eq_symbol_statistics_sign3_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_statistics_sign3;
                    ll_kb_sub_sumbol_statistics_sign3.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_statistics_sign4:
                case R.id.kb_eq_symbol_statistics_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_statistics_sign4;
                    ll_kb_sub_sumbol_statistics_sign4.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_statistics_sign5:

                    break;

                case R.id.kb_eq_symbol_statistics_sign6:
                case R.id.kb_eq_symbol_statistics_sign6_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_statistics_sign6;
                    ll_kb_sub_sumbol_statistics_sign6.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_statistics_sign7:
                case R.id.kb_eq_symbol_statistics_sign7_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_statistics_sign7;
                    ll_kb_sub_sumbol_statistics_sign7.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_finitemath_sign1:
                case R.id.kb_eq_symbol_finitemath_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_finitemath_sign1;
                    ll_kb_sub_sumbol_finitemath_sign1.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_finitemath_sign2:
                case R.id.kb_eq_symbol_finitemath_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_finitemath_sign2;
                    ll_kb_sub_sumbol_finitemath_sign2.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_finitemath_sign3:

                    break;

                case R.id.kb_eq_symbol_finitemath_sign4:
                case R.id.kb_eq_symbol_finitemath_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_finitemath_sign4;
                    ll_kb_sub_sumbol_finitemath_sign4.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_finitemath_sign5:
                case R.id.kb_eq_symbol_finitemath_sign5_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_finitemath_sign5;
                    ll_kb_sub_sumbol_finitemath_sign5.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_finitemath_sign8:
                case R.id.kb_eq_symbol_finitemath_sign8_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_finitemath_sign8;
                    ll_kb_sub_sumbol_finitemath_sign8.setVisibility(View.VISIBLE);

                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onkbclick(View v) {
        try {


            int id_view = v.getId();

            switch (id_view) {

                case R.id.btn_eq_name_pre_scroll:

                    this.callscrollmethod(1);

                    break;

                case R.id.btn_eq_name_next_scroll:

                    this.callscrollmethod(2);

                    break;

                case R.id.btn_eq_symbol_pre_scroll:

                    this.callscrollmethod(3);

                    break;

                case R.id.btn_eq_symbol_next_scroll:

                    this.callscrollmethod(4);

                    break;

                case R.id.kb_btn_abc_close:

                    hideKeyboard();

                    break;

                case R.id.kb_btn_123_close:

                    hideKeyboard();

                    break;

                case R.id.kb_btn_abc:

                    setvisibilityofabc123layout(2);

                    break;

                case R.id.kb_btn_123:

                    setvisibilityofabc123layout(1);

                    break;

                case R.id.kb_btn_abc_case:

                    this.changecasevalue();

                    break;

                case R.id.kb_tv_eq_name_basicmath:

                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_basicmath, kb_tv_eq_name_geometry, kb_tv_eq_name_prealgebra, kb_tv_eq_name_algebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {


                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_basicmath, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_trignometry,
                                    kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();

                        }
                    }, 500);

                    this.setvisibility_scroll_btn(false);

                    break;

                case R.id.kb_tv_eq_name_prealgebra:

                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_prealgebra, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_algebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler2 = new Handler();
                    handler2.postDelayed(new Runnable() {
                        @Override
                        public void run() {


                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_basicmath, kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_trignometry,
                                    kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();

                        }
                    }, 500);

                    this.setvisibility_scroll_btn(false);

                    break;

                case R.id.kb_tv_eq_name_algebra:

                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_algebra, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler3 = new Handler();
                    handler3.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath, kb_ll_eq_symbol_trignometry,
                                    kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;

                case R.id.kb_tv_eq_name_geometry:

                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_geometry, kb_tv_eq_name_algebra, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler33 = new Handler();
                    handler33.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath, kb_ll_eq_symbol_trignometry,
                                    kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;

                case R.id.kb_tv_eq_name_trignometry:
                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_trignometry, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_algebra,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler4 = new Handler();
                    handler4.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_trignometry, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath,
                                    kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;

                case R.id.kb_tv_eq_name_precalculus:
                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_precalculus, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_algebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_calculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler5 = new Handler();
                    handler5.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_trignometry, kb_ll_eq_symbol_algebra,
                                    kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath,
                                    kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;

                case R.id.kb_tv_eq_name_calculus:
                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_calculus, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_algebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler6 = new Handler();
                    handler6.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_trignometry,
                                    kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath,
                                    kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;

                case R.id.kb_tv_eq_name_statistics:
                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_statistics, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_algebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_finitemath);

                    Handler handler7 = new Handler();
                    handler7.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_precalculus,
                                    kb_ll_eq_symbol_trignometry, kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath,
                                    kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;

                case R.id.kb_tv_eq_name_finitemath:
                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_finitemath, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_algebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_statistics);

                    Handler handler8 = new Handler();
                    handler8.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_finitemath, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_calculus,
                                    kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_trignometry, kb_ll_eq_symbol_algebra,
                                    kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;


            }
            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onkb_abc_click(View v) { // Added By Siddhi Info Soft

        try {

            String btn_tag = (String) v.getTag();
            this.changeCase(btn_tag);
            this.setTextToSelectedTextView(selectedTextView, btn_tag);
            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onkb_123_click(View v) { // Added By Siddhi Info Soft

        try {

            String btn_tag = String.valueOf(v.getTag());
            this.setTextToSelectedTextView(selectedTextView, btn_tag);
            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onkb_123_symbol_click(View v) { // Added By Siddhi Info Soft

        try {

            String btn_tag = String.valueOf(v.getTag());
            this.add_sign_into_box(btn_tag);
            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*public void onkb_sub_symbol_click(View v) { // Added By Siddhi Info Soft

        try {

            String btn_tag = String.valueOf(v.getTag());

            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);

            if (btn_tag.equals("fraction") ||
                    btn_tag.equals("pointview")
                    || btn_tag.equals("lim") || btn_tag.equals("parenthesis") || btn_tag.equals("absolute") | btn_tag.equals("squareroot") || btn_tag.equals("nthroot")
                    || btn_tag.equals("super_script") || btn_tag.equals("sub_script") || btn_tag.equals("super_sub_script")
                    || btn_tag.equals("line") || btn_tag.equals("lsegment") || btn_tag.equals("ray")
                    || btn_tag.equals("arc")) {
                this.addviewtoselectedview(ll_ans_box_selected, btn_tag, "view");
            } else if (btn_tag.equals("absolute")) {
                setTextToSelectedTextView(selectedTextView, "|  |");
            } else {
                this.add_sign_into_box(btn_tag);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }*/

    private void add_sign_into_box(final String tag) {

        String str_text = "";
        boolean is_operator = false;

        if (tag.equals("op_plus")) {
            str_text = "" + Html.fromHtml("&#43;");
            is_operator = true;
        } else if (tag.equals("op_minus")) {
            str_text = "" + Html.fromHtml("&#8722;");
            is_operator = true;
        } else if (tag.equals("op_equal")) {
            str_text = "" + Html.fromHtml("&#61;");
            is_operator = true;
        } else if (tag.equals("op_multi")) {
            str_text = "" + Html.fromHtml("&#215;");
            is_operator = true;
        } else if (tag.equals("op_divide")) {
            str_text = "" + Html.fromHtml("&#247;");
            is_operator = true;
        } else if (tag.equals("op_union")) {
            str_text = "" + Html.fromHtml("&#8746;");
        } else if (tag.equals("op_intersection")) {
            str_text = "" + Html.fromHtml("&#8745;");
        } else if (tag.equals("op_pi")) {
            str_text = "" + Html.fromHtml("&#960;");
        } else if (tag.equals("op_factorial")) {
            str_text = "" + Html.fromHtml("&#33;");
        } else if (tag.equals("op_percentage")) {
            str_text = "" + Html.fromHtml("&#37;");
        } else if (tag.equals("op_rarrow")) {
            str_text = "" + Html.fromHtml("&#8594;");
        } else if (tag.equals("op_xbar")) {
            str_text = "" + Html.fromHtml("x&#x0304;");
        } else if (tag.equals("op_muxbar")) {
            str_text = "" + Html.fromHtml("&#956;") + "" + Html.fromHtml("x&#772;");
        } else if (tag.equals("op_sigmaxbar")) {
            str_text = "" + Html.fromHtml("&#963;") + "" + Html.fromHtml("x&#772;");
        } else if (tag.equals("op_goe")) {
            str_text = "" + Html.fromHtml("&#8805;");
            is_operator = true;
        } else if (tag.equals("op_loe")) {
            str_text = "" + Html.fromHtml("&#8804;");
            is_operator = true;
        } else if (tag.equals("op_grthan")) {
            str_text = "" + Html.fromHtml("&#62;");
            is_operator = true;
        } else if (tag.equals("op_lethan")) {
            str_text = "" + Html.fromHtml("&#60;");
            is_operator = true;
        } else if (tag.equals("op_infinity")) {
            str_text = "" + Html.fromHtml("&#8734;");
        } else if (tag.equals("op_degree")) {
            str_text = "" + Html.fromHtml("&#176;");
        } else if (tag.equals("op_integral")) {
            str_text = " " + Html.fromHtml("&#8747;");
        } else if (tag.equals("op_summation")) {
            str_text = " " + Html.fromHtml("&#8721;");
        } else if (tag.equals("op_alpha")) {
            str_text = " " + Html.fromHtml("&#945;");
            //  str_text = " "+Html.fromHtml("&#913;");
        } else if (tag.equals("op_theta")) {
            str_text = " ø";
        } else if (tag.equals("op_mu")) {
            str_text = " " + Html.fromHtml("&#956;");
        } else if (tag.equals("op_sigma")) {
            str_text = " " + Html.fromHtml("&#963;");
        } else if (tag.equals("op_beta")) {
            str_text = " " + Html.fromHtml("&#946;");
            //  str_text = " "+Html.fromHtml("&#7526;");
        } else if (tag.equals("op_angle")) {
            str_text = " " + Html.fromHtml("&#8736;");
        } else if (tag.equals("op_mangle")) {
            str_text = " " + Html.fromHtml("&#8737;");
        } else if (tag.equals("op_sangle")) {
            str_text = " " + Html.fromHtml("&#8738;");
        } else if (tag.equals("op_rangle")) {
            str_text = " " + Html.fromHtml("&#8735;");
        } else if (tag.equals("op_triangle")) {
            str_text = " " + Html.fromHtml("&#9651;");
        } else if (tag.equals("op_rectangle")) {
            str_text = " " + Html.fromHtml("&#9645;");
        } else if (tag.equals("op_parallelogram")) {
            str_text = " " + Html.fromHtml("&#9649;");
        } else if (tag.equals("op_line")) {
            str_text = " " + Html.fromHtml("&#963;");
        } else if (tag.equals("op_lsegment")) {
            str_text = " " + Html.fromHtml("&#963;");
        } else if (tag.equals("op_ray")) {
            str_text = " " + Html.fromHtml("&#963;");
        } else if (tag.equals("op_arc")) {
            str_text = " " + Html.fromHtml("&#963;");
        } else if (tag.equals("op_perpendicular")) {
            str_text = " " + Html.fromHtml("&#8869;");
        } else if (tag.equals("op_congruent")) {
            str_text = " " + Html.fromHtml("&#8773;");
        } else if (tag.equals("op_similarty")) {
            str_text = " " + Html.fromHtml("&#8764;");
        } else if (tag.equals("op_parallel")) {
            str_text = " " + Html.fromHtml("&#8741;");
        } else if (tag.equals("op_arcm")) {
            str_text = "" + Html.fromHtml("&#8242;");
        } else if (tag.equals("op_arcs")) {
            str_text = "" + Html.fromHtml("&#8243;");
        } else {
            str_text = tag;
        }

        if (is_operator) {
            setTextToSelectedTextView2(selectedTextView, " " + str_text + " ");
        } else {
            setTextToSelectedTextView2(selectedTextView, str_text);
        }

    }

    public void onkb_sum_symbol_click(View v) {

        try {

            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);

            String btn_tag = String.valueOf(v.getTag());
            String str_text = "";
            if (btn_tag.equals("op_integral")) {
                str_text = " " + Html.fromHtml("&#8747;");
            } else if (btn_tag.equals("op_summation")) {
                str_text = " " + Html.fromHtml("&#8721;");
            } else if (btn_tag.equals("op_perpendicular")) {
                str_text = " " + Html.fromHtml("&#8869;");
            } else if (btn_tag.equals("op_parallel")) {
                str_text = " " + Html.fromHtml("&#8741;");
            } else if (btn_tag.equals("op_rectangle")) {
                str_text = " " + Html.fromHtml("&#9645;");
            } else if (btn_tag.equals("op_parallelogram")) {
                str_text = " " + Html.fromHtml("&#9649;");
            } else if (btn_tag.equals("op_angle")) {
                str_text = " " + Html.fromHtml("&#8736;");
            } else if (btn_tag.equals("op_mangle")) {
                str_text = " " + Html.fromHtml("&#8737;");
            } else if (btn_tag.equals("op_sangle")) {
                str_text = " " + Html.fromHtml("&#8738;");
            } else if (btn_tag.equals("op_rangle")) {
                str_text = " " + Html.fromHtml("&#8735;");
            }

            if (btn_tag.equals("op_integral") || btn_tag.equals("op_summation")) {
                SpannableString ss1 = new SpannableString(str_text);
                ss1.setSpan(new RelativeSizeSpan(1.6f), 1, 2, 0);
                selectedTextView.getText().insert(selectedTextView.getSelectionStart(), ss1);
            } else if (btn_tag.equals("op_perpendicular") || btn_tag.equals("op_parallel") ||
                    btn_tag.equals("op_rectangle") || btn_tag.equals("op_parallelogram")) {
                SpannableString ss1 = new SpannableString(str_text);
                ss1.setSpan(new RelativeSizeSpan(1.2f), 1, 2, 0);
                selectedTextView.getText().insert(selectedTextView.getSelectionStart(), ss1);
            } else if (btn_tag.equals("op_angle") || btn_tag.equals("op_mangle") ||
                    btn_tag.equals("op_sangle") || btn_tag.equals("op_rangle")) {
                SpannableString ss1 = new SpannableString(str_text);
                ss1.setSpan(new RelativeSizeSpan(1.5f), 1, 2, 0);
                selectedTextView.getText().insert(selectedTextView.getSelectionStart(), ss1);
            }

            if (btn_tag.equals("op_integral")) {
                selectedTextView.getText().insert(selectedTextView.getSelectionStart(), " ");
            }


//            SpannableString ss1=  new SpannableString(str_text);
//            ss1.setSpan(new RelativeSizeSpan(2f), 1,2, 0);
//            selectedTextView.getText().insert(selectedTextView.getSelectionStart(), ss1);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void addnewed(final LinearLayout ll_main, final String str_cut, final int pos, final int text_size) {

//        String sss = String.valueOf(ll_main.getTag(R.id.first));
//        int tag1 = Integer.valueOf(sss);
//        String tag2 = String.valueOf(ll_main.getTag(R.id.second));
//        int text_size = 22;
//
//        try {
//
//            if (tag2.equals("super_top") || tag2.equals("sub_bottom") || tag2.equals("ss_top") ||
//                    tag2.equals("ss_bottom") || tag2.equals("nth_top") || tag2.equals("lim_left") || tag2.equals("lim_right")){
//                text_size = (text_size - 3)-(tag1+1);
//            } else if(tag2.equals("frac_top") || tag2.equals("frac_bottom") || tag2.equals("sqrt_c") || tag2.equals("nth_bottom")
//                    || tag2.equals("parenthesis_center")){
//                text_size = text_size-(tag1+1);
//            }
//
//        } catch (Exception e){
//
//        }

        final LinearLayout ll_last = (LinearLayout) (this).getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
        ll_last.setTag("text");
        final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
        // ed_centre.setText(str_cut);
        this.replacesignfromchar(str_cut, ed_centre);
        ed_centre.setMinWidth(20);
        ed_centre.setTextSize(text_size);
        ll_last.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
            }
        });
        ed_centre.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
                return false;
            }
        });
        ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
                    selectedTextView = ed_centre;
                    ll_selected_view = ll_last;
                    selected_ed_2 = null;
                    ll_ans_box_selected = ll_main;
                }
            }
        });

        ll_main.addView(ll_last, (pos + 2));

        //   }

    }

    private void add_last_ed(final LinearLayout ll_main, final int textsize, final int pos) {

        String sss = String.valueOf(ll_main.getTag(R.id.first));
        int tag1 = Integer.valueOf(sss);
        String tag2 = String.valueOf(ll_main.getTag(R.id.second));
        int text_size = getResources().getInteger(R.integer.kb_textsize_main_int);

        try {

            if (tag2.equals("super_top") || tag2.equals("sub_bottom") || tag2.equals("ss_top") ||
                    tag2.equals("ss_bottom") || tag2.equals("nth_top") || tag2.equals("lim_left") || tag2.equals("lim_right")) {
                text_size = (text_size - 1) - (tag1 + 1);
            } else if (tag2.equals("frac_top") || tag2.equals("frac_bottom") || tag2.equals("sqrt_c") || tag2.equals("nth_bottom")
                    || tag2.equals("parenthesis_center") || tag2.equals("absolute_center")
                    || tag2.equals("line_center") || tag2.equals("lsegment_center")
                    || tag2.equals("ray_center") || tag2.equals("arc_center")) {
                text_size = text_size - (tag1 + 1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (text_size < 5) {
            text_size = 5;
        }

        final LinearLayout ll_last = (LinearLayout) (this).getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
        ll_last.setTag("text");
        final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
        ed_centre.setMinWidth(3);
        ed_centre.setMinimumWidth(3);
        ed_centre.requestLayout();
        ed_centre.setTextSize(text_size);
        ll_last.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
            }
        });
        ed_centre.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
                return false;
            }
        });
        ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    selectedTextView = ed_centre;
                    ll_selected_view = ll_last;
                    selected_ed_2 = null;
                    ll_ans_box_selected = ll_main;
                }
            }
        });
        Log.e("delete6", "1234");
        ll_main.addView(ll_last, pos);
        Log.e("delete7", "1234");
        ed_centre.requestFocus();
    }

    private void add_ed_after_delete(final LinearLayout ll_main, final int textsize, final int pos) {

        String sss = String.valueOf(ll_main.getTag(R.id.first));
        int tag1 = Integer.valueOf(sss);
        String tag2 = String.valueOf(ll_main.getTag(R.id.second));
        int text_size = getResources().getInteger(R.integer.kb_textsize_main_int);

        try {

            if (tag2.equals("super_top") || tag2.equals("sub_bottom") || tag2.equals("ss_top") ||
                    tag2.equals("ss_bottom") || tag2.equals("nth_top") || tag2.equals("lim_left") || tag2.equals("lim_right")) {
                text_size = (text_size - 1) - (tag1 + 1);
            } else if (tag2.equals("frac_top") || tag2.equals("frac_bottom") || tag2.equals("sqrt_c") || tag2.equals("nth_bottom")
                    || tag2.equals("parenthesis_center") || tag2.equals("absolute_center")
                    || tag2.equals("line_center") || tag2.equals("lsegment_center")
                    || tag2.equals("ray_center") || tag2.equals("arc_center")) {
                text_size = text_size - (tag1 + 1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (text_size < 5) {
            text_size = 5;
        }
        final LinearLayout ll_last = (LinearLayout) (this).getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
        ll_last.setTag("text");
        final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
        ed_centre.setMinWidth(20);
        ed_centre.setTextSize(text_size);
        ll_last.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
            }
        });
        ed_centre.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
                return false;
            }
        });
        ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                custom_ed_focus(ed_centre, ll_last, ll_main, hasFocus);
            }
        });
        //   Utils.change_edittext_bg(true, ed_centre);
        Log.e("edd2", "" + pos);
        ll_main.addView(ll_last, pos);
        ed_centre.requestFocus();

    }

    /*private void custom_ed_touch(final EditTextBlink ed, final LinearLayout ll_sub, final LinearLayout ll_main) {

        try {

            if(isWorkAreaForstudent) {
                ed.setCursorVisible(true);
                selectedTextView = ed;
                ll_selected_view = ll_sub;
                ll_ans_box_selected = ll_main;
                selected_ed_2 = null;
                showKeyboard();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    private void add_inside_ed(final LinearLayout ll_main, final boolean req_focus, int text_size) {

        String tag2 = String.valueOf(ll_main.getTag(R.id.second));

        try {

            if (tag2.equals("super_top") || tag2.equals("sub_bottom") || tag2.equals("ss_top") ||
                    tag2.equals("ss_bottom") || tag2.equals("nth_top") || tag2.equals("lim_left") || tag2.equals("lim_right")) {
                // text_size = (text_size - 3)-(tag1+1);
                text_size = text_size - 3;
            } else if (tag2.equals("frac_top") || tag2.equals("frac_bottom") || tag2.equals("sqrt_c") || tag2.equals("nth_bottom")
                    || tag2.equals("parenthesis_center") || tag2.equals("absolute_center")
                    || tag2.equals("line_center") || tag2.equals("lsegment_center")
                    || tag2.equals("ray_center") || tag2.equals("arc_center")) {
                //  text_size = text_size-(tag1+1);
                text_size = text_size - 1;
            }

        } catch (Exception e) {
            text_size = 22;
        }

        Log.e("text_size", "" + text_size);
        final LinearLayout ll_last = (LinearLayout) (this).getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
        ll_last.setTag("text");
        final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
        ed_centre.setMinWidth(25);
        ed_centre.setTextSize(text_size);
        ll_last.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
            }
        });
        ed_centre.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
                return false;
            }
        });
        ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                custom_ed_focus(ed_centre, ll_last, ll_main, hasFocus);
            }
        });
        Utils.change_edittext_bg(true, ed_centre);
        ll_main.addView(ll_last);

        Log.e("het", "" + ll_last.getHeight());
        ll_main.requestLayout();

        if (req_focus) {
            ed_centre.requestFocus();
        }

    }

    private void custom_ed_focus(final EditTextBlink ed, final LinearLayout ll_sub, final LinearLayout ll_main, final boolean hasFocus) {

        if (!(ll_main.getTag(R.id.second).equals("main"))) {
            Utils.change_edittext_bg(hasFocus, ed);
        }

        if (hasFocus) {
            selectedTextView = ed;
            ll_selected_view = ll_sub;
            selected_ed_2 = null;
            ll_ans_box_selected = ll_main;
        }

    }

    /*private void addviewtoselectedview(final LinearLayout ll_ans_box, String tag, String type) {  // Added By Siddhi Info Soft
        boolean is_devide = false;
        int start_pos = 0, ed_legnth = 0, ll_sub_selected_pos = 0;
        String cut_str = "";
        String sss = String.valueOf(ll_ans_box.getTag(R.id.first));
        int tag_main_ll = Integer.valueOf(sss);
        boolean need_delete = false;
        int text_size = getResources().getInteger(R.integer.kb_textsize_main_int);

        try {
            DisplayMetrics metrics;
            metrics = getApplicationContext().getResources().getDisplayMetrics();
            float Textsize = selectedTextView.getTextSize() / metrics.density;
            text_size = Math.round(Textsize);
            Log.e("text_size", "" + text_size);
            if (text_size < 5) {
                text_size = 5;
            }

        } catch (Exception e) {
            text_size = 16;
        }

        try {

            start_pos = selectedTextView.getSelectionStart();
            ed_legnth = selectedTextView.getText().toString().length();

            try {
                ll_sub_selected_pos = ll_ans_box_selected.indexOfChild(ll_selected_view);
            } catch (Exception e) {
                ll_sub_selected_pos = 0;
            }

            try {
                if (ll_sub_selected_pos == -1) {
                    ll_sub_selected_pos = 0;
                }

                if (start_pos != 0 && start_pos < ed_legnth) {
                    cut_str = selectedTextView.getText().toString();
                    cut_str = cut_str.substring(start_pos, ed_legnth);
                    is_devide = true;
                    Log.e("math_ed_pos", cut_str + "" + ll_sub_selected_pos);
                    selectedTextView.getText().delete(start_pos, ed_legnth);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                if (selectedTextView.getText().toString().trim().length() == 0) {
                    need_delete = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {

                final LinearLayout ll_last = (LinearLayout) ll_ans_box.getChildAt((ll_ans_box.getChildCount() - 1));
                String str_tag = String.valueOf(ll_last.getTag());
                // Log.e("tagg",str_tag);
                if (str_tag.equals("text")) {
                    final EditTextBlink ed_last = (EditTextBlink) ll_last.getChildAt(0);
                    if (ed_last.getText().toString().trim().length() == 0) {

                        try {
                            if (ll_ans_box.getChildCount() > 1) {
                                LinearLayout ll_last_2 = (LinearLayout) ll_ans_box.getChildAt((ll_ans_box.getChildCount() - 2));
                                String str_tag_2 = String.valueOf(ll_last_2.getTag());
                                if (str_tag_2.equals("text")) {
                                    ll_ans_box.removeView(ll_last);
                                } else {
                                    ed_last.setMinWidth(4);
                                }
                            } else {
                                ed_last.setMinWidth(4);
                            }
                        } catch (Exception e) {
                        }
                    } else {
                        //  Log.e("tagg12", str_tag);
                        ed_last.setMinWidth(4);
                        ed_last.setMinimumWidth(4);
                        ed_last.requestLayout();
                        //  Log.e("tagg123", str_tag);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (type.equals("view")) {
                selected_ed_2 = null;
                if (tag.equals("super_script")) {

                    final LinearLayout ll_super_script = (LinearLayout) (this).getLayoutInflater().inflate(R.layout.kb_new_view_superscript, null);
                    ll_super_script.setTag("super_script");
                    final LinearLayout ll_top_superscript = (LinearLayout) ll_super_script.findViewById(R.id.ll_top_superscript);
                    ll_top_superscript.setTag(R.id.first, (tag_main_ll + 1));
                    ll_top_superscript.setTag(R.id.second, "super_top");

                    add_inside_ed(ll_top_superscript, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_super_script, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_super_script, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("sub_script")) {

                    final LinearLayout ll_sub_script = (LinearLayout) (this).getLayoutInflater().inflate(R.layout.kb_new_view_subscript, null);
                    ll_sub_script.setTag("sub_script");
                    final LinearLayout ll_bottom_subscript = (LinearLayout) ll_sub_script.findViewById(R.id.ll_bottom_subscript);
                    ll_bottom_subscript.setTag(R.id.first, (tag_main_ll + 1));
                    ll_bottom_subscript.setTag(R.id.second, "sub_bottom");
                    add_inside_ed(ll_bottom_subscript, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_sub_script, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_sub_script, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("super_sub_script")) {

                    final LinearLayout ll_super_sub_script = (LinearLayout) (this).getLayoutInflater().inflate(R.layout.kb_new_view_super_sub_script, null);
                    ll_super_sub_script.setTag("super_sub_script");

                    final LinearLayout ll_top_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_top_super_sub_script);
                    final LinearLayout ll_bottom_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_bottom_super_sub_script);
                    ll_top_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                    ll_bottom_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                    ll_top_super_sub_script.setTag(R.id.second, "ss_top");
                    ll_bottom_super_sub_script.setTag(R.id.second, "ss_bottom");
                    add_inside_ed(ll_top_super_sub_script, true, text_size);
                    add_inside_ed(ll_bottom_super_sub_script, false, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_super_sub_script, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_super_sub_script, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("fraction")) {

                    final LinearLayout ll_fraction = (LinearLayout) (this).getLayoutInflater().inflate(R.layout.kb_view_fraction, null);
                    final LinearLayout ll_fraction_top = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_top);
                    final LinearLayout ll_fraction_bottom = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_bottom);
                    ll_fraction_top.setTag(R.id.first, (tag_main_ll + 1));
                    ll_fraction_bottom.setTag(R.id.first, (tag_main_ll + 1));
                    ll_fraction_top.setTag(R.id.second, "frac_top");
                    ll_fraction_bottom.setTag(R.id.second, "frac_bottom");
                    add_inside_ed(ll_fraction_top, true, text_size);
                    add_inside_ed(ll_fraction_bottom, false, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_fraction, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_fraction, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }

                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("squareroot")) {

                    final LinearLayout ll_squareroot = (LinearLayout) (this).getLayoutInflater().inflate(R.layout.kb_view_square_root, null);
                    final LinearLayout ll_center_square_root = (LinearLayout) ll_squareroot.findViewById(R.id.ll_center_square_root);
                    ll_center_square_root.setTag(R.id.first, (tag_main_ll + 1));
                    ll_center_square_root.setTag(R.id.second, "sqrt_c");
                    add_inside_ed(ll_center_square_root, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_squareroot, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_squareroot, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("nthroot")) {

                    final LinearLayout ll_nthroot = (LinearLayout) (this).getLayoutInflater().inflate(R.layout.kb_view_nth_root, null);

                    final LinearLayout ll_top_nth_root = (LinearLayout) ll_nthroot.findViewById(R.id.ll_top_nth_root);
                    final LinearLayout ll_center_nth_root = (LinearLayout) ll_nthroot.findViewById(R.id.ll_center_nth_root);
                    final ImageView img = (ImageView) ll_nthroot.findViewById(R.id.img);
                    ll_top_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                    ll_center_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                    ll_top_nth_root.setTag(R.id.second, "nth_top");
                    ll_center_nth_root.setTag(R.id.second, "nth_bottom");
                    add_inside_ed(ll_top_nth_root, true, text_size);
                    add_inside_ed(ll_center_nth_root, false, text_size);
                    img.getLayoutParams().height = ll_center_nth_root.getHeight();
                    img.requestLayout();
                    ll_center_nth_root.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                        @Override
                        public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                            Log.e("het", "" + v.getHeight());
                            img.getLayoutParams().height = v.getHeight();
                            img.requestLayout();
                        }
                    });

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_nthroot, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_nthroot, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("lim")) {

                    final LinearLayout ll_lim = (LinearLayout) (this).getLayoutInflater().inflate(R.layout.kb_new_view_lim, null);
                    final TextView tv_text = (TextView) ll_lim.findViewById(R.id.tv_text);
                    tv_text.setText(Html.fromHtml("&#8594;"));
                    final LinearLayout ll_lim_left = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_left);
                    final LinearLayout ll_lim_right = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_right);
                    ll_lim_left.setTag(R.id.first, (tag_main_ll + 1));
                    ll_lim_right.setTag(R.id.first, (tag_main_ll + 1));
                    ll_lim_left.setTag(R.id.second, "lim_left");
                    ll_lim_right.setTag(R.id.second, "lim_right");
                    add_inside_ed(ll_lim_left, true, text_size);
                    add_inside_ed(ll_lim_right, false, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_lim, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_lim, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }

                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("parenthesis")) {

                    final LinearLayout ll_parenthesis = (LinearLayout) (this).getLayoutInflater().inflate(R.layout.kb_view_parenthesis, null);
                    ll_parenthesis.setTag("parenthesis");
                    final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_parenthesis_center);
                    ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                    ll_parenthesis_center.setTag(R.id.second, "parenthesis_center");
                    add_inside_ed(ll_parenthesis_center, true, text_size);
                    final ImageView img_left = (ImageView) ll_parenthesis.findViewById(R.id.img_left);
                    final ImageView img_rht = (ImageView) ll_parenthesis.findViewById(R.id.img_rht);

                    img_left.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final LinearLayout lll = (LinearLayout) ll_parenthesis_center.getChildAt(0);
                            if (lll.getTag().equals("text")) {
                                final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                edd.setSelection(0);
                                edd.requestFocus();
                            } else {
                                add_last_ed(ll_parenthesis_center, 22, 0);
                            }
                        }
                    });

                    img_rht.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final LinearLayout lll = (LinearLayout) ll_parenthesis_center.getChildAt((ll_parenthesis_center.getChildCount() - 1));
                            if (lll.getTag().equals("text")) {
                                final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                edd.setSelection(edd.getText().toString().length());
                                edd.requestFocus();
                            } else {
                                add_last_ed(ll_parenthesis_center, 22, (ll_parenthesis_center.getChildCount()));
                            }
                        }
                    });

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_parenthesis, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_parenthesis, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("absolute")) {

                    final LinearLayout ll_parenthesis = (LinearLayout) (this).getLayoutInflater().inflate(R.layout.kb_view_absolute, null);
                    ll_parenthesis.setTag("absolute");
                    final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_absolute_center);
                    ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                    ll_parenthesis_center.setTag(R.id.second, "absolute_center");
                    add_inside_ed(ll_parenthesis_center, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_parenthesis, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_parenthesis, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("line") || tag.equals("lsegment")
                        || tag.equals("ray") || tag.equals("arc")) {

                    final LinearLayout ll_parenthesis = (LinearLayout) (this).getLayoutInflater().inflate(R.layout.kb_view_lsegment, null);

                    final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_lsegment_center);
                    final ImageView img_top = (ImageView) ll_parenthesis.findViewById(R.id.img_top);
                    ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                    if (tag.equals("line")) {
                        img_top.setImageResource(R.drawable.line_icon);
                        ll_parenthesis.setTag("line");
                        ll_parenthesis_center.setTag(R.id.second, "line_center");
                    } else if (tag.equals("lsegment")) {
                        img_top.setImageResource(R.drawable.line_segment);
                        ll_parenthesis.setTag("lsegment");
                        ll_parenthesis_center.setTag(R.id.second, "lsegment_center");
                    } else if (tag.equals("ray")) {
                        img_top.setImageResource(R.drawable.ray_icon);
                        ll_parenthesis.setTag("ray");
                        ll_parenthesis_center.setTag(R.id.second, "ray_center");
                    } else if (tag.equals("arc")) {
                        img_top.setImageResource(R.drawable.arc_icon);
                        ll_parenthesis.setTag("arc");
                        ll_parenthesis_center.setTag(R.id.second, "arc_center");
                    }

                    add_inside_ed(ll_parenthesis_center, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_parenthesis, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_parenthesis, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                }
            }

            LinearLayout ll_last_2 = (LinearLayout) ll_ans_box.getChildAt((ll_ans_box.getChildCount() - 1));
            String str_tag = String.valueOf(ll_last_2.getTag());
            if (str_tag.equals("text")) {
                EditTextBlink ed_last = (EditTextBlink) ll_last_2.getChildAt(0);
                ed_last.setMinWidth(5);

            } else {

                final LinearLayout ll_last = (LinearLayout) (this).getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                ll_last.setTag("text");
                final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                ed_centre.setTextSize(text_size);
                ed_centre.setMinWidth(4);
                ed_centre.setMinimumWidth(4);
                ed_centre.requestLayout();
                ll_last.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ed_centre.setCursorVisible(true);
                        selectedTextView = ed_centre;
                        ll_selected_view = ll_last;
                        ll_ans_box_selected = ll_ans_box;
                        selected_ed_2 = null;
                        showKeyboard();
                    }
                });
                ed_centre.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        ed_centre.setCursorVisible(true);
                        selectedTextView = ed_centre;
                        ll_selected_view = ll_last;
                        ll_ans_box_selected = ll_ans_box;
                        selected_ed_2 = null;
                        showKeyboard();
                        return false;
                    }
                });
                ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            selectedTextView = ed_centre;
                            ll_selected_view = ll_last;
                            selected_ed_2 = null;
                            ll_ans_box_selected = ll_ans_box;
                        }
                    }
                });

                ll_ans_box.addView(ll_last);

            }

        } catch (Exception e) {

        }
    }*/


    public void setstudentansbox(final CustomeAns customeAns, final LinearLayout ll_ans_box, final
    CustomePlayerAns playerAns) {
        try {

            if (customeAns.getIsAnswerAvailable() == MathFriendzyHelper.NO) {

                final LinearLayout ll_last = (LinearLayout) (this).getLayoutInflater().inflate(R.layout.kb_view_textview, null);
                ll_last.setTag("vtext");
                final TextView tv = (TextView) ll_last.findViewById(R.id.tv);
                tv.setText(lblAnswerOnWorkAreaCreditGiven);

                tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MathFriendzyHelper.showWarningDialog(HomeWorkWorkArea.this,
                                lblAnswerOnWorkAreaCreditGiven);
                    }
                });

                ll_ans_box.addView(ll_last);

            } else {

                if (playerAns != null) {

                    String str_ans = playerAns.getAns();

                    Log.e("stdans", str_ans);

                    if (str_ans.trim().length() != 0) {
                        setstudentansboxsub(str_ans, ll_ans_box);
                    }
                }

                Log.e("stdansC", customeAns.getCorrectAns());

                boolean is_ed_last = false;

                if (ll_ans_box.getChildCount() > 0) {
                    LinearLayout lt = (LinearLayout) ll_ans_box.getChildAt(ll_ans_box.getChildCount() - 1);
                    if (lt.getTag().toString().equals("text")) {
                        is_ed_last = true;
                    } else {
                        is_ed_last = false;
                    }
                } else {
                    is_ed_last = false;
                }

                if (!is_ed_last) {

                    final LinearLayout ll_last = (LinearLayout) (this).getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                    ll_last.setTag("text");
                    final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                    ed_centre.setMinWidth(20);
                    ll_last.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            custom_ed_touch(ed_centre, ll_last, ll_ans_box);
                        }
                    });
                    ed_centre.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            custom_ed_touch(ed_centre, ll_last, ll_ans_box);
                            return false;
                        }
                    });
                    ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                selectedTextView = ed_centre;
                                ll_selected_view = ll_last;
                                ll_ans_box_selected = ll_ans_box;
                            }
                        }
                    });
                    ll_ans_box.addView(ll_last);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void onnextsubcall(final LinearLayout ll_sub) {

        if (ll_sub.getTag().toString().equals("text")) {

            EditTextBlink ed = (EditTextBlink) ll_sub.getChildAt(0);
            ed.requestFocus();
            ed.setSelection(0);

        } else if (ll_sub.getTag().toString().equals("fraction") || ll_sub.getTag().toString().equals("super_script") || ll_sub.getTag().toString().equals("super_sub_script")
                || ll_sub.getTag().toString().equals("nsqrt")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(0);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, 0);
            } else {
                this.onnextsubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("sub_script")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, 0);
            } else {
                this.onnextsubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("parenthesis") || ll_sub.getTag().toString().equals("absolute") ||
                ll_sub.getTag().toString().equals("line") || ll_sub.getTag().toString().equals("lsegment")
                || ll_sub.getTag().toString().equals("ray") || ll_sub.getTag().toString().equals("arc")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, 0);
            } else {
                this.onnextsubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("sqrt")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt(0);
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_2, 22, 0);
            } else {
                this.onnextsubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("lim")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(0);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt(0);
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_2, 22, 0);
            } else {
                this.onnextsubcall(ll_sub_1);
            }

        }

    }

    /*public void onnextclick() {
        CommonUtils.printLog("onnextclick");
        try {
            int cur_pos = 0, ed_legnth = 0;

            try {
                cur_pos = selectedTextView.getSelectionStart();
                ed_legnth = selectedTextView.getText().toString().length();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if ((cur_pos != ed_legnth)) {

                String str_bar_1 = "" + Html.fromHtml("&#x0304;");
                String str_bar_2 = "" + Html.fromHtml("&#772;");

                if (selectedTextView.getText().toString().length() > 1) {
                    String ss = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos));
                    if (ss.equals("x")) {
                        if ((cur_pos + 1) < ed_legnth) {
                            String ss_1 = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos + 1));
                            if (ss_1.equals(str_bar_1) || ss_1.equals(str_bar_2)) {
                                selectedTextView.setSelection((cur_pos + 2));
                            } else {
                                selectedTextView.setSelection((cur_pos + 1));
                            }
                        } else {
                            selectedTextView.setSelection((cur_pos + 1));
                        }
                    } else {
                        selectedTextView.setSelection((cur_pos + 1));
                    }
                } else {
                    selectedTextView.setSelection((cur_pos + 1));
                }

                if (is_touch_kb_next) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onnextclick();
                        }
                    }, 150);
                }

            } else {
                final LinearLayout ll_main_ll = ll_ans_box_selected;
                final LinearLayout ll_sub_ll = ll_selected_view;

                int pos_sub_ll = ll_main_ll.indexOfChild(ll_sub_ll);

                if (ll_main_ll.getChildCount() > 1 && pos_sub_ll < (ll_main_ll.getChildCount() - 1)) {

                    LinearLayout ll_sub = (LinearLayout) ll_main_ll.getChildAt(pos_sub_ll + 1);
                    onnextsubcall(ll_sub);

                } else if (pos_sub_ll == (ll_main_ll.getChildCount() - 1)) {

                    String main_tag = (String) ll_main_ll.getTag(R.id.second);

                    if (main_tag.equals("frac_top")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("ss_top")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(1);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("nth_top")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("lim_left")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("sqrt_c") || main_tag.equals("nth_bottom") || main_tag.equals("lim_right")) {

                        sub_next_sqrt_c(ll_main_ll);

                    } else if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom") ||
                            main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                            || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                            || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {

                        sub_next_frac_bottom(ll_main_ll);

                    }
                }

                if (is_touch_kb_next) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onnextclick();
                        }
                    }, 150);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    public void sub_next_sqrt_c(final LinearLayout ll_main_ll) {
        try {

            try {

                final LinearLayout ll_mainm = (LinearLayout) ll_main_ll.getParent();
                final LinearLayout ll_main_1m = (LinearLayout) ll_mainm.getParent();
                final LinearLayout ll_main2 = (LinearLayout) ll_main_1m.getParent();
                int main_pos = ll_main2.indexOfChild(ll_main_1m);

                final String main_tag = String.valueOf(ll_main2.getTag(R.id.second));
                if (main_pos == (ll_main2.getChildCount() - 1)) {
                    add_last_ed(ll_main2, 22, main_pos + 1);
                } else if (ll_main2.getChildCount() > 1 && main_pos < (ll_main2.getChildCount() - 1)) {
                    final LinearLayout ll_sub = (LinearLayout) ll_main2.getChildAt(main_pos + 1);
                    if (!(ll_sub.getTag().equals("text"))) {
                        add_last_ed(ll_main2, 22, main_pos + 1);
                    } else {
                        onnextsubcall(ll_sub);
                    }
                } else {

                    if (main_tag.equals("sqrt_c") || main_tag.equals("nth_bottom") || main_tag.equals("lim_right")) {
                        sub_next_sqrt_c(ll_main2);
                    } else if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")
                            || main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                            || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                            || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {
                        sub_next_frac_bottom(ll_main2);
                    } else if (main_tag.equals("frac_top")) {
                        final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                        final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        final LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);
                    } else if (main_tag.equals("ss_top")) {
                        final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                        final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(1);
                        final LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);
                    } else if (main_tag.equals("nth_top")) {
                        final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                        final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        final LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
                        final LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt(0);
                        onnextsubcall(ll_sub_1);
                    } else if (main_tag.equals("lim_left")) {

                        LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("main")) {
                        add_last_ed(ll_main2, 18, ll_main2.getChildCount());
                    }
                }

            } catch (Exception e) {

            }

        } catch (Exception e) {
        }
    }

    public void sub_next_frac_bottom(final LinearLayout ll_main_ll) {
        try {
            final LinearLayout ll_mainm = (LinearLayout) ll_main_ll.getParent();
            final LinearLayout ll_main2 = (LinearLayout) ll_mainm.getParent();
            int main_pos = ll_main2.indexOfChild(ll_mainm);
            final String main_tag = String.valueOf(ll_main2.getTag(R.id.second));

            if (main_pos == (ll_main2.getChildCount() - 1)) {
                add_last_ed(ll_main2, 22, main_pos + 1);
            } else if (ll_main2.getChildCount() > 1 && main_pos < (ll_main2.getChildCount() - 1)) {
                final LinearLayout ll_sub = (LinearLayout) ll_main2.getChildAt(main_pos + 1);
                if (!(ll_sub.getTag().equals("text"))) {
                    add_last_ed(ll_main2, 22, main_pos + 1);
                } else {
                    onnextsubcall(ll_sub);
                }

            } else {
                if (main_tag.equals("sqrt_c") || main_tag.equals("nth_bottom") || main_tag.equals("lim_right")) {
                    sub_next_sqrt_c(ll_main2);
                } else if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")
                        || main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                        || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                        || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {
                    sub_next_frac_bottom(ll_main2);
                } else if (main_tag.equals("frac_top")) {
                    final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                    final LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                    onnextsubcall(ll_sub_1);
                } else if (main_tag.equals("ss_top")) {
                    final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(1);
                    final LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                    onnextsubcall(ll_sub_1);
                } else if (main_tag.equals("nth_top")) {
                    final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                    final LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
                    final LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt(0);
                    onnextsubcall(ll_sub_1);
                } else if (main_tag.equals("lim_left")) {

                    LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                    LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                    onnextsubcall(ll_sub_1);

                } else if (main_tag.equals("main")) {
                    add_last_ed(ll_main2, 22, ll_main2.getChildCount());
                }

            }
        } catch (Exception e) {
        }
    }

    public void onpresubcall(final LinearLayout ll_sub) {

        if (ll_sub.getTag().toString().equals("text")) {

            EditTextBlink ed = (EditTextBlink) ll_sub.getChildAt(0);
            ed.setSelection(ed.getText().toString().length());
            ed.requestFocus();

        } else if (ll_sub.getTag().toString().equals("sub_script")
                ) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, ll_main_1.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("super_script")
                ) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(0);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, ll_main_1.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("fraction")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(2);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, ll_main_1.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("parenthesis") || ll_sub.getTag().toString().equals("absolute")
                || ll_sub.getTag().toString().equals("line") || ll_sub.getTag().toString().equals("lsegment")
                || ll_sub.getTag().toString().equals("ray") || ll_sub.getTag().toString().equals("arc")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, ll_main_1.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("super_sub_script")) {
            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, ll_main_1.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("sqrt")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_2, 22, ll_main_2.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("nsqrt")) {
            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(2);
            LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_2, 22, ll_main_2.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("lim")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(2);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_2, 22, ll_main_2.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        }

    }

    /*public void ondeleteclick() {

        final LinearLayout ll_main_ll = ll_ans_box_selected;
        final LinearLayout ll_sub_ll = ll_selected_view;

        try {
            int ed_pos = 0, ed_legnth = 0;
            try {
                ed_pos = selectedTextView.getSelectionStart();
                ed_legnth = selectedTextView.getText().toString().trim().length();
            } catch (Exception e) {
            }
            if (ed_pos > 0 && ed_legnth > 0) {
                String str_bar_1 = "" + Html.fromHtml("&#x0304;");
                String str_bar_2 = "" + Html.fromHtml("&#772;");
                String str_integral = "" + Html.fromHtml("&#8747;");
                if (selectedTextView.getText().toString().length() > 1) {
                    String ss = String.valueOf(selectedTextView.getText().toString().charAt(ed_pos - 1));
                    if (ss.equals(str_bar_1) || ss.equals(str_bar_2)) {

                        selectedTextView.getText().delete(ed_pos - 2, ed_pos);

                    } else if (ss.equals("x")) {
                        if (ed_pos < ed_legnth) {
                            String ss_2 = String.valueOf(selectedTextView.getText().toString().charAt(ed_pos));
                            if (ss_2.equals(str_bar_1) || ss_2.equals(str_bar_2)) {
                                selectedTextView.getText().delete(ed_pos - 1, ed_pos + 1);

                            } else {
                                selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                            }
                        } else {
                            selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                        }
                    } else if (ss.equals(" ")) {
                        try {
                            String ss2 = String.valueOf(selectedTextView.getText().toString().charAt(ed_pos - 2));
                            if (ss2.equals(str_integral)) {
                                selectedTextView.getText().delete(ed_pos - 2, ed_pos);
                            } else {
                                selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                            }
                        } catch (Exception e) {
                            selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                        }
                    } else {
                        selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                    }
                } else {
                    selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                }
                try {
                    if (selectedTextView.getText().toString().trim().length() == 0) {
                        selectedTextView.setSelection(0);
                    }
                } catch (Exception e) {
                }

                if (is_touch_kb) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ondeleteclick();
                        }
                    }, 150);
                }

            } else if (ed_pos == 0 && ed_legnth == 0) {

                int pos_sub_ll = ll_main_ll.indexOfChild(ll_sub_ll);
                Log.e("delete4", "" + pos_sub_ll);

                if (pos_sub_ll != 0 && pos_sub_ll != -1) {

                    LinearLayout ll_sub = (LinearLayout) ll_main_ll.getChildAt(pos_sub_ll - 1);
                    onpresubcall(ll_sub);
                    ll_main_ll.removeViewAt(pos_sub_ll);

                } else if (pos_sub_ll == 0) {

                    String main_tag = (String) ll_main_ll.getTag(R.id.second);

                    Log.e("delete5", "" + main_tag);

                    if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                        if (ll_main_ll.getChildCount() > 1) {
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        }

                    } else if (main_tag.equals("nth_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                        if (ll_main_ll.getChildCount() > 1) {
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        }

                    } else if (main_tag.equals("lim_right")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                        if (ll_main_ll.getChildCount() > 1) {
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        }

                    } else if (main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                            || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                            || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {

                        try {
                            LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            LinearLayout ll_main2 = (LinearLayout) ll_main.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_frac_top(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {

                                if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                    sub_pre_frac_top(ll_main_ll, 2, false);
                                    ll_main2.removeViewAt(main_pos);
                                    this.add_ed_after_delete(ll_main2, 18, main_pos);
                                } else {
                                    if (main_pos != 0) {
                                        sub_pre_frac_top(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                    } else {
                                        sub_pre_frac_top(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                        try {
                                            final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                            if (lll.getTag().toString().equals("text")) {
                                                EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                edd.setMinWidth(20);
                                                edd.setMinimumWidth(20);
                                                edd.requestLayout();
                                                edd.requestFocus();
                                                if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                    Utils.change_edittext_bg(true, edd);
                                                }
                                            }
                                        } catch (Exception e) {
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("sqrt_c")) {

                        try {

                            final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            final LinearLayout ll_main1 = (LinearLayout) ll_main.getParent();
                            final LinearLayout ll_main2 = (LinearLayout) ll_main1.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main1);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_sqrt_c(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {
                                if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                    Log.e("edd", "" + main_pos);
                                    sub_pre_sqrt_c(ll_main_ll, 2, false);
                                    ll_main2.removeViewAt(main_pos);
                                    Log.e("edd1", "" + main_pos);
                                    this.add_ed_after_delete(ll_main2, 18, main_pos);
                                } else {
                                    if (main_pos != 0) {
                                        sub_pre_sqrt_c(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                    } else {
                                        sub_pre_sqrt_c(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                        try {
                                            final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                            if (lll.getTag().toString().equals("text")) {
                                                final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                edd.setMinWidth(20);
                                                edd.setMinimumWidth(20);
                                                edd.requestLayout();
                                                edd.requestFocus();
                                                if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                    Utils.change_edittext_bg(true, edd);
                                                }
                                            }
                                        } catch (Exception e) {
                                        }
                                    }
                                }
                            }

                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("nth_top")) {

                        try {
                            final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            final LinearLayout ll_main2 = (LinearLayout) ll_main.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_frac_top(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {
                                final LinearLayout ll_sqrt = (LinearLayout) ll_main.getChildAt(2);
                                final LinearLayout ll_sqrt2 = (LinearLayout) ll_sqrt.getChildAt(1);
                                if (this.makeansfromview(ll_sqrt2).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_frac_top(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            }

                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("lim_left")) {

                        try {
                            final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            final LinearLayout ll_main1 = (LinearLayout) ll_main.getParent();
                            final LinearLayout ll_main2 = (LinearLayout) ll_main1.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main1);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_sqrt_c(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {
                                final LinearLayout ll_lim_rht = (LinearLayout) ll_main.getChildAt(2);
                                if (this.makeansfromview(ll_lim_rht).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_sqrt_c(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_sqrt_c(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_sqrt_c(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            }

                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("frac_top") || main_tag.equals("ss_top")) {

                        final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        final LinearLayout ll_main2 = (LinearLayout) ll_main.getParent();
                        int main_pos = ll_main2.indexOfChild(ll_main);

                        if (ll_main_ll.getChildCount() > 1) {
                            sub_pre_frac_top(ll_main_ll, 0, false);
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        } else {
                            if (main_tag.equals("frac_top")) {
                                LinearLayout ll_frac = (LinearLayout) ll_main.getChildAt(2);
                                if (this.makeansfromview(ll_frac).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_frac_top(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            } else {
                                LinearLayout ll_frac = (LinearLayout) ll_main.getChildAt(1);

                                if (this.makeansfromview(ll_frac).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_frac_top(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            }
                        }
                    }
                }

                if (is_touch_kb) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ondeleteclick();
                        }
                    }, 150);
                }
            } else {
                this.onpreviousclick();
                if (is_touch_kb) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ondeleteclick();
                        }
                    }, 150);
                }
            }

        } catch (Exception e) {
        }
    }*/

    /*public void onpreviousclick() {

        try {
            int cur_pos = 0;

            try {
                cur_pos = selectedTextView.getSelectionStart();
            } catch (Exception e) {
            }

            if (cur_pos != 0) {

                String str_bar_1 = "" + Html.fromHtml("&#x0304;");
                String str_bar_2 = "" + Html.fromHtml("&#772;");

                if (selectedTextView.getText().toString().length() > 1) {
                    String ss = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos - 1));
                    if (ss.equals(str_bar_1) || ss.equals(str_bar_2)) {

                        selectedTextView.setSelection((cur_pos - 2));

                    } else {
                        selectedTextView.setSelection((cur_pos - 1));
                    }
                } else {
                    selectedTextView.setSelection((cur_pos - 1));
                }

                if (is_touch_kb_pre) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onpreviousclick();
                        }
                    }, 150);
                }

            } else {

                final LinearLayout ll_main_ll = ll_ans_box_selected;
                final LinearLayout ll_sub_ll = ll_selected_view;

                int pos_sub_ll = ll_main_ll.indexOfChild(ll_sub_ll);

                if (pos_sub_ll != 0 && pos_sub_ll != -1) {

                    LinearLayout ll_sub = (LinearLayout) ll_main_ll.getChildAt(pos_sub_ll - 1);
                    onpresubcall(ll_sub);

                } else if (pos_sub_ll == 0) {

                    String main_tag = (String) ll_main_ll.getTag(R.id.second);

                    if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                    } else if (main_tag.equals("nth_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                    } else if (main_tag.equals("lim_right")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                    } else if (main_tag.equals("sqrt_c") || main_tag.equals("lim_left")) {

                        try {
                            sub_pre_sqrt_c(ll_main_ll, 0, true);
                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("frac_top") || main_tag.equals("ss_top") ||
                            main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("nth_top") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                            || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                            || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {

                        try {
                            sub_pre_frac_top(ll_main_ll, 0, true);
                        } catch (Exception e) {
                        }

                    }
                }

                if (is_touch_kb_pre) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onpreviousclick();
                        }
                    }, 150);
                }

            }

        } catch (Exception e) {
        }

    }*/

    public void sub_pre_sqrt_c(final LinearLayout ll_main_ll, final int is_main, final boolean is_add_ed) {

        try {

            LinearLayout ll_mainm = (LinearLayout) ll_main_ll.getParent();
            LinearLayout ll_main1 = (LinearLayout) ll_mainm.getParent();
            LinearLayout ll_main2 = (LinearLayout) ll_main1.getParent();
            int main_pos = ll_main2.indexOfChild(ll_main1);
            final String main_tag = String.valueOf(ll_main2.getTag(R.id.second));
            if (main_pos == 0 && is_add_ed) {
                add_last_ed(ll_main2, 22, main_pos);
            } else if (ll_main2.getChildCount() > 1 && main_pos != 0) {
                LinearLayout ll_sub = (LinearLayout) ll_main2.getChildAt(main_pos - 1);
                if (!(ll_sub.getTag().equals("text")) && is_add_ed) {
                    add_last_ed(ll_main2, 22, main_pos);
                } else {
                    onpresubcall(ll_sub);
                }
            } else {
                if (main_tag.equals("sqrt_c") || main_tag.equals("lim_left")) {
                    this.sub_pre_sqrt_c(ll_main2, 0, is_add_ed);
                } else if (main_tag.equals("frac_top") || main_tag.equals("ss_top")
                        || main_tag.equals("super_top") || main_tag.equals("sub_bottom")
                        || main_tag.equals("nth_top") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                        || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                        || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {
                    this.sub_pre_frac_top(ll_main2, 0, is_add_ed);
                } else if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")) {
                    final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(0);
                    final LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
                    onpresubcall(ll_sub_1);
                } else if (main_tag.equals("nth_bottom")) {
                    LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    LinearLayout ll_main_1 = (LinearLayout) ll_main.getParent();
                    LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(0);
                    LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                    onpresubcall(ll_sub_1);
                } else if (main_tag.equals("lim_right")) {

                    LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    LinearLayout ll_main_2 = (LinearLayout) ll_main.getChildAt(0);
                    LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                    onpresubcall(ll_sub_1);

                } else if (main_tag.equals("main")) {
                    if (is_main == 0) {
                        add_last_ed(ll_main2, 18, 0);
                    } else if (is_main == 1) {
                        this.onnextsubcall((LinearLayout) ll_main2.getChildAt(1));
                    }
                }
            }

        } catch (Exception e) {
        }

    }

    public void sub_pre_frac_top(final LinearLayout ll_main_ll, final int is_main, final boolean is_add_ed) {

        try {
            final LinearLayout ll_mainm = (LinearLayout) ll_main_ll.getParent();
            final LinearLayout ll_main2 = (LinearLayout) ll_mainm.getParent();
            int main_pos = ll_main2.indexOfChild(ll_mainm);
            final String main_tag = String.valueOf(ll_main2.getTag(R.id.second));
            if (main_pos == 0 && is_add_ed) {
                add_last_ed(ll_main2, 22, main_pos);
            } else if (ll_main2.getChildCount() > 1 && main_pos != 0) {
                final LinearLayout ll_sub = (LinearLayout) ll_main2.getChildAt(main_pos - 1);
                if (!(ll_sub.getTag().equals("text")) && is_add_ed) {
                    add_last_ed(ll_main2, 22, main_pos);
                } else {
                    onpresubcall(ll_sub);
                }
            } else {
                Log.e("delete2", "" + ll_main2.getTag(R.id.second));
                if (main_tag.equals("sqrt_c") || main_tag.equals("lim_left")) {
                    this.sub_pre_sqrt_c(ll_main2, 0, is_add_ed);
                } else if (main_tag.equals("frac_top") || main_tag.equals("ss_top")
                        || main_tag.equals("super_top") || main_tag.equals("sub_bottom")
                        || main_tag.equals("nth_top") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                        || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                        || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {
                    this.sub_pre_frac_top(ll_main2, 0, is_add_ed);
                } else if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")) {
                    final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(0);
                    final LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
                    onpresubcall(ll_sub_1);
                } else if (main_tag.equals("nth_bottom")) {
                    LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    LinearLayout ll_main_1 = (LinearLayout) ll_main.getParent();
                    LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(0);
                    LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                    onpresubcall(ll_sub_1);
                } else if (main_tag.equals("lim_right")) {

                    LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    LinearLayout ll_main_2 = (LinearLayout) ll_main.getChildAt(0);
                    LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                    onpresubcall(ll_sub_1);

                } else if (main_tag.equals("main")) {
                    if (is_main == 0) {
                        add_last_ed(ll_main2, 18, 0);
                    } else if (is_main == 1) {
                        this.onnextsubcall((LinearLayout) ll_main2.getChildAt(1));
                    }
                }
            }
        } catch (Exception e) {
        }

    }

    private String makeansfromview(final LinearLayout ll_ans_box) {

        String str = "";

        for (int i = 0; i < ll_ans_box.getChildCount(); i++) {

            LinearLayout ll_view = (LinearLayout) ll_ans_box.getChildAt(i);

            String main_tag = String.valueOf(ll_view.getTag());

            if (main_tag.equals("text")) {

                EditTextBlink ed_1 = (EditTextBlink) ll_view.getChildAt(0);
                if (ed_1.getText().toString().trim().length() != 0) {
                    str = str + "text#" + this.replacecharfromsign(ed_1.getText().toString().trim()) + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }

            } else if (main_tag.equals("fraction")) {

                LinearLayout ll_fraction_top = (LinearLayout) ll_view.getChildAt(0);
                LinearLayout ll_fraction_bottom = (LinearLayout) ll_view.getChildAt(2);

                String ss = this.makeansfromview(ll_fraction_top);
                String ss1 = this.makeansfromview(ll_fraction_bottom);
                if (ss.trim().length() != 0 && ss1.trim().length() != 0) {
                    str = str + "fraction" + ss + "frac_btm_" + ll_ans_box.getTag(R.id.first)
                            + ss1 + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }
            } else if (main_tag.equals("super_script")) {

                LinearLayout ll_top_superscript = (LinearLayout) ll_view.getChildAt(0);

                String ss = this.makeansfromview(ll_top_superscript);

                if (ss.trim().length() != 0) {
                    str = str + "super_script" + ss + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }

            } else if (main_tag.equals("sub_script")) {

                LinearLayout ll_bottom_subscript = (LinearLayout) ll_view.getChildAt(1);

                String ss = this.makeansfromview(ll_bottom_subscript);

                if (ss.trim().length() != 0) {
                    str = str + "sub_script" + ss + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }


            } else if (main_tag.equals("super_sub_script")) {

                LinearLayout ll_top_super_sub_script = (LinearLayout) ll_view.getChildAt(0);
                LinearLayout ll_bottom_super_sub_script = (LinearLayout) ll_view.getChildAt(1);

                String ss = this.makeansfromview(ll_top_super_sub_script);
                String ss1 = this.makeansfromview(ll_bottom_super_sub_script);
                if (ss.trim().length() != 0 && ss1.trim().length() != 0) {
                    str = str + "super_sub_script" + ss + "ss_btm_" + ll_ans_box.getTag(R.id.first)
                            + ss1 + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }
            } else if (main_tag.equals("sqrt")) {

                LinearLayout ll_l = (LinearLayout) ll_view.getChildAt(1);
                LinearLayout ll_center_square_root = (LinearLayout) ll_l.getChildAt(1);

                String ss = this.makeansfromview(ll_center_square_root);

                if (ss.trim().length() != 0) {
                    str = str + "sqrt" + ss + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }

            } else if (main_tag.equals("nsqrt")) {

                LinearLayout ll_top_nth_root = (LinearLayout) ll_view.getChildAt(0);
                LinearLayout ll_l = (LinearLayout) ll_view.getChildAt(2);
                LinearLayout ll_center_nth_root = (LinearLayout) ll_l.getChildAt(1);

                String ss = this.makeansfromview(ll_top_nth_root);
                String ss1 = this.makeansfromview(ll_center_nth_root);
                if (ss.trim().length() != 0 && ss1.trim().length() != 0) {
                    str = str + "nsqrt" + ss + "nsqrt_center_" + ll_ans_box.getTag(R.id.first)
                            + ss1 + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }
            } else if (main_tag.equals("lim")) {

                LinearLayout ll_1 = (LinearLayout) ll_view.getChildAt(1);
                LinearLayout ll_lim_left = (LinearLayout) ll_1.getChildAt(0);
                LinearLayout ll_lim_right = (LinearLayout) ll_1.getChildAt(2);

                String ss = this.makeansfromview(ll_lim_left);
                String ss1 = this.makeansfromview(ll_lim_right);
                if (ss.trim().length() != 0 && ss1.trim().length() != 0) {
                    str = str + "lim" + ss + "lim_rht_" + ll_ans_box.getTag(R.id.first)
                            + ss1 + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }
            } else if (main_tag.equals("parenthesis")) {

                LinearLayout ll_top_superscript = (LinearLayout) ll_view.getChildAt(1);

                String ss = this.makeansfromview(ll_top_superscript);

                if (ss.trim().length() != 0) {
                    str = str + "parenthesis" + ss + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }

            } else if (main_tag.equals("absolute")) {

                LinearLayout ll_top_superscript = (LinearLayout) ll_view.getChildAt(1);

                String ss = this.makeansfromview(ll_top_superscript);

                if (ss.trim().length() != 0) {
                    str = str + "absolute" + ss + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }

            } else if (main_tag.equals("line") || main_tag.equals("lsegment")
                    || main_tag.equals("ray") || main_tag.equals("arc")) {

                LinearLayout ll_top_superscript = (LinearLayout) ll_view.getChildAt(1);

                String ss = this.makeansfromview(ll_top_superscript);

                if (ss.trim().length() != 0) {
                    str = str + main_tag + ss + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }

            }
        }
        try {
            str = str.substring(0, (str.length() - 3));
        } catch (Exception e) {

        }

        try {
            String cut_str = str.substring(str.length() - 8);
            if (cut_str.equals("fraction")) {
                str = str.substring(0, (str.length() - 11));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 12);
            if (cut_str.equals("super_script")) {
                str = str.substring(0, (str.length() - 15));
            }
        } catch (Exception e) {
        }
        try {
            String cut_str = str.substring(str.length() - 10);
            if (cut_str.equals("sub_script")) {
                str = str.substring(0, (str.length() - 13));
            }
        } catch (Exception e) {
        }
        try {
            String cut_str = str.substring(str.length() - 16);
            if (cut_str.equals("super_sub_script")) {
                str = str.substring(0, (str.length() - 19));
            }
        } catch (Exception e) {
        }
        try {
            String cut_str = str.substring(str.length() - 4);
            if (cut_str.equals("sqrt")) {
                str = str.substring(0, (str.length() - 7));
            }
        } catch (Exception e) {
        }
        try {
            String cut_str = str.substring(str.length() - 5);
            if (cut_str.equals("nsqrt")) {
                str = str.substring(0, (str.length() - 8));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 3);
            if (cut_str.equals("lim")) {
                str = str.substring(0, (str.length() - 6));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 11);
            if (cut_str.equals("parenthesis")) {
                str = str.substring(0, (str.length() - 14));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 8);
            if (cut_str.equals("absolute")) {
                str = str.substring(0, (str.length() - 11));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 4);
            if (cut_str.equals("line")) {
                str = str.substring(0, (str.length() - 7));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 8);
            if (cut_str.equals("lsegment")) {
                str = str.substring(0, (str.length() - 11));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 3);
            if (cut_str.equals("ray")) {
                str = str.substring(0, (str.length() - 6));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 3);
            if (cut_str.equals("arc")) {
                str = str.substring(0, (str.length() - 6));
            }
        } catch (Exception e) {
        }

        return str;
    }

    private boolean is_char_match(String ss) {

        String str_plus = "" + Html.fromHtml("&#43;");
        String str_minus = "" + Html.fromHtml("&#8722;");
        String str_equal = "" + Html.fromHtml("&#61;");
        String str_multi = "" + Html.fromHtml("&#215;");
        String str_divide = "" + Html.fromHtml("&#247;");
        String str_goe = "" + Html.fromHtml("&#8805;");
        String str_loe = "" + Html.fromHtml("&#8804;");
        String str_grthan = "" + Html.fromHtml("&#62;");
        String str_lethan = "" + Html.fromHtml("&#60;");

        if (ss.equals(str_plus) || ss.equals(str_minus) || ss.equals(str_equal) || ss.equals(str_multi) || ss.equals(str_divide)
                || ss.equals(str_goe) || ss.equals(str_loe) || ss.equals(str_grthan)
                || ss.equals(str_lethan)) {
            return true;
        } else {
            return false;
        }

    }

    public void onplusminusclick(View v) {
        try {
            if (selected_ed_2 == null) {

                int pos = selectedTextView.getSelectionStart();

                try {

                    if (pos == 0) {

                        if (selectedTextView.getText().toString().trim().length() > 0) {

                            String ss = String.valueOf(selectedTextView.getText().toString().charAt(0));

                            if (ss.equals("-")) {
                                selectedTextView.getText().delete(0, 1);
                            } else {
                                selectedTextView.getText().insert(0, "-");
                            }

                        } else {
                            selectedTextView.getText().insert(0, "-");
                        }

                    }

                } catch (Exception e) {

                }

                for (int i = (pos - 1); i >= 0; i--) {

                    String ss = String.valueOf(selectedTextView.getText().toString().charAt(i));

                    if (ss.equals("-")) {
                        selectedTextView.getText().delete(i, i + 1);
                        break;
                    } else if (ss.equals(" ")) {
                        selectedTextView.getText().insert(i + 1, "-");
                        break;
                    } else if (ss.equals("(") || ss.equals(")")) {
                        selectedTextView.getText().insert(i + 1, "-");
                        break;
                    } else if (is_char_match(ss)) {
                        selectedTextView.getText().insert(i + 1, "-");
                        break;
                    } else if (i == 0) {
                        selectedTextView.getText().insert(i, "-");
                        break;
                    }

                }

            } else {

                int cur_pos = ll_ans_box_selected.indexOfChild(ll_selected_view);

                if (cur_pos < ll_ans_box_selected.getChildCount()) {

                    LinearLayout ll_next = (LinearLayout) ll_ans_box_selected.getChildAt((cur_pos + 1));

                    if (String.valueOf(ll_next.getTag()).equals("text")) {
                        EditTextBlink ed_text = (EditTextBlink) ll_next.getChildAt(0);
                        if (ed_text.getText().toString().contains("-")) {
                            ed_text.getText().delete(0, 1);
                        } else {
                            ed_text.getText().insert(0, "-");
                        }
                        ed_text.setSelection(1);
                        ed_text.requestFocus();
                    } else {
                        final LinearLayout ll_last = (LinearLayout) (this).getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                        ll_last.setTag("text");
                        final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                        ed_centre.setText("-");
                        ed_centre.requestFocus();
                        ll_last.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ed_centre.setCursorVisible(true);
                                selectedTextView = ed_centre;
                                ll_selected_view = ll_last;
                                selected_ed_2 = null;
                                showKeyboard();
                            }
                        });
                        ed_centre.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                ed_centre.setCursorVisible(true);
                                selectedTextView = ed_centre;
                                ll_selected_view = ll_last;
                                selected_ed_2 = null;
                                showKeyboard();
                                return false;
                            }
                        });
                        ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                if (hasFocus) {
                                    selectedTextView = ed_centre;
                                    ll_selected_view = ll_last;
                                    selected_ed_2 = null;
                                }
                            }
                        });

                        ll_ans_box_selected.addView(ll_last, (cur_pos + 1));
                    }
                }
            }
        } catch (Exception e) {

        }
    }

    private void setTextToSelectedTextView(EditText txtView, String text) { // Changes by Siddhi info soft

        final LinearLayout ll_ans_box = ll_ans_box_selected;

        try {

            if (text.equals("op_pi")) {
                text = "" + Html.fromHtml("&#960;");
            }

            if (selected_ed_2 == null) {

                setTextToSelectedTextView2(txtView, text);

            } else {

                int cur_pos = ll_ans_box_selected.indexOfChild(ll_selected_view);

                if (cur_pos < ll_ans_box_selected.getChildCount()) {

                    final LinearLayout ll_last = (LinearLayout) (this).getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                    ll_last.setTag("text");
                    final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                    ed_centre.setText(text);
                    ll_last.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ed_centre.setCursorVisible(true);
                            selectedTextView = ed_centre;
                            ll_selected_view = ll_last;
                            selected_ed_2 = null;
                            ll_ans_box_selected = ll_ans_box;
                            showKeyboard();
                        }
                    });
                    ed_centre.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            ed_centre.setCursorVisible(true);
                            selectedTextView = ed_centre;
                            ll_selected_view = ll_last;
                            selected_ed_2 = null;
                            ll_ans_box_selected = ll_ans_box;
                            showKeyboard();
                            return false;
                        }
                    });
                    ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                selectedTextView = ed_centre;
                                ll_selected_view = ll_last;
                                selected_ed_2 = null;
                                ll_ans_box_selected = ll_ans_box;
                            }
                        }
                    });

                    ll_ans_box.addView(ll_last, (cur_pos + 1));

                    ed_centre.requestFocus();

                }
            }

        } catch (Exception e) {

        }

        selectedTextView.setCursorVisible(true);

    }

    public void setTextToSelectedTextView2(EditText txtView, String text) { //added by siddhiinfosoft
        try {
            if (selectedTextView != null) {

                int cur_pos = selectedTextView.getSelectionStart();
                int ed_length = selectedTextView.getText().toString().length();

                if (selectedTextView.getText().toString().length() > 1) {

                    String str_bar_1 = "" + Html.fromHtml("&#x0304;");
                    String str_bar_2 = "" + Html.fromHtml("&#772;");

                    if (cur_pos != 0 && cur_pos < ed_length) {
                        String ss = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos - 1));

                        if (ss.equals("x")) {

                            String ss_1 = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos));

                            if (ss_1.equals(str_bar_1) || ss_1.equals(str_bar_2)) {

                            } else {
                                selectedTextView.getText().insert(selectedTextView.getSelectionStart(), text);
                            }

                        } else {
                            selectedTextView.getText().insert(selectedTextView.getSelectionStart(), text);
                        }

                    } else {
                        selectedTextView.getText().insert(selectedTextView.getSelectionStart(), text);
                    }

                } else {

                    selectedTextView.getText().insert(selectedTextView.getSelectionStart(), text);

                }

                if (text.equals("|  |")) {
                    selectedTextView.setSelection((selectedTextView.getSelectionStart() - 3));
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        selectedTextView.setCursorVisible(true);
    }

    private void callclickmethod(final CustomeAns customeAns, final LinearLayout ll_ans_box , int index) {

        try {
            if (customeAns.getFillInType() == FILL_IN_TYPE) {

                LinearLayout ll_last = (LinearLayout) ll_ans_box.getChildAt((ll_ans_box.getChildCount() - 1));
                if (ll_last.getTag().toString().equals("text")) {
                    final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                    ed_centre.setSelection(ed_centre.getText().toString().length());
                    if(ed_centre.hasFocus()) {
                        ed_centre.clearFocus();
                    }
                    ed_centre.requestFocus();
                } else {
                    add_last_ed(ll_ans_box, 22, ll_ans_box.getChildCount());
                }
                showKeyboard();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void setstudentansboxsub(final String str_ans, final LinearLayout ll_ans_box) {

        String[] str_1 = str_ans.split(ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first));

        String sss = String.valueOf(ll_ans_box.getTag(R.id.first));
        int tag_main_ll = Integer.valueOf(sss);

        for (int i = 0; i < str_1.length; i++) {

            String str_1_value = str_1[i];

            if (str_1_value.startsWith("text")) {

                str_1_value = str_1_value.substring(5);

                String sss1 = String.valueOf(ll_ans_box.getTag(R.id.first));
                int tag1 = Integer.valueOf(sss1);
                final String tag2 = String.valueOf(ll_ans_box.getTag(R.id.second));
                int text_size = HomeWorkWorkArea.this.getResources().getInteger(R.integer.kb_textsize_main_int);

                try {

                    if (tag2.equals("super_top") || tag2.equals("sub_bottom") || tag2.equals("ss_top") ||
                            tag2.equals("ss_bottom") || tag2.equals("nth_top") || tag2.equals("lim_left") || tag2.equals("lim_right")) {
                        text_size = (text_size - 1) - (tag1 + 1);
                    } else if (tag2.equals("frac_top") || tag2.equals("frac_bottom") || tag2.equals("sqrt_c") || tag2.equals("nth_bottom")
                            || tag2.equals("parenthesis_center") || tag2.equals("absolute_center")
                            || tag2.equals("line_center") || tag2.equals("lsegment_center")
                            || tag2.equals("ray_center") || tag2.equals("arc_center")) {
                        text_size = text_size - (tag1 + 1);
                    }

                } catch (Exception e) {

                }

                final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                ll_last.setTag("text");
                final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                this.replacesignfromchar(str_1_value, ed_centre);
                ed_centre.setMinWidth(20);
                ed_centre.setTextSize(text_size);
                ll_last.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        custom_ed_touch(ed_centre, ll_last, ll_ans_box);
                    }
                });
                ed_centre.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        custom_ed_touch(ed_centre, ll_last, ll_ans_box);
                        return false;
                    }
                });
                ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (!(tag2.equals("main"))) {
                            Utils.change_edittext_bg(hasFocus, ed_centre);
                        }
                        if (hasFocus) {
                            selectedTextView = ed_centre;
                            ll_selected_view = ll_last;
                            ll_ans_box_selected = ll_ans_box;
                        }
                    }
                });

                ll_ans_box.addView(ll_last);

            }
            if (str_1_value.startsWith("fraction")) {

                str_1_value = str_1_value.substring(8);

                final LinearLayout ll_fraction = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_fraction, null);
                final LinearLayout ll_fraction_top = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_top);
                final LinearLayout ll_fraction_bottom = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_bottom);
                ll_fraction_top.setTag(R.id.first, (tag_main_ll + 1));
                ll_fraction_bottom.setTag(R.id.first, (tag_main_ll + 1));
                ll_fraction_top.setTag(R.id.second, "frac_top");
                ll_fraction_bottom.setTag(R.id.second, "frac_bottom");

                final String[] str_2 = str_1_value.split("frac_btm_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_fraction);

                this.setstudentansboxsub(str_2[0], ll_fraction_top);
                this.setstudentansboxsub(str_2[1], ll_fraction_bottom);

            } else if (str_1_value.startsWith("super_script")) {
                str_1_value = str_1_value.substring(12);

                final LinearLayout ll_super_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_superscript, null);
                final LinearLayout ll_top_superscript = (LinearLayout) ll_super_script.findViewById(R.id.ll_top_superscript);
                ll_top_superscript.setTag(R.id.first, (tag_main_ll + 1));
                ll_top_superscript.setTag(R.id.second, "super_top");

                ll_ans_box.addView(ll_super_script);

                this.setstudentansboxsub(str_1_value, ll_top_superscript);

            } else if (str_1_value.startsWith("sub_script")) {
                str_1_value = str_1_value.substring(10);

                final LinearLayout ll_super_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_subscript, null);
                final LinearLayout ll_bottom_subscript = (LinearLayout) ll_super_script.findViewById(R.id.ll_bottom_subscript);
                ll_bottom_subscript.setTag(R.id.first, (tag_main_ll + 1));
                ll_bottom_subscript.setTag(R.id.second, "sub_bottom");

                ll_ans_box.addView(ll_super_script);

                this.setstudentansboxsub(str_1_value, ll_bottom_subscript);

            } else if (str_1_value.startsWith("super_sub_script")) {

                str_1_value = str_1_value.substring(16);

                final LinearLayout ll_super_sub_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_super_sub_script, null);
                final LinearLayout ll_top_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_top_super_sub_script);
                final LinearLayout ll_bottom_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_bottom_super_sub_script);
                ll_top_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                ll_bottom_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                ll_top_super_sub_script.setTag(R.id.second, "ss_top");
                ll_bottom_super_sub_script.setTag(R.id.second, "ss_bottom");

                final String[] str_2 = str_1_value.split("ss_btm_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_super_sub_script);

                this.setstudentansboxsub(str_2[0], ll_top_super_sub_script);
                this.setstudentansboxsub(str_2[1], ll_bottom_super_sub_script);

            } else if (str_1_value.startsWith("sqrt")) {

                str_1_value = str_1_value.substring(4);
                final LinearLayout ll_sqrt = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_square_root, null);
                final LinearLayout ll_center_square_root = (LinearLayout) ll_sqrt.findViewById(R.id.ll_center_square_root);
                ll_center_square_root.setTag(R.id.first, (tag_main_ll + 1));
                ll_center_square_root.setTag(R.id.second, "sqrt_c");

                ll_ans_box.addView(ll_sqrt);

                this.setstudentansboxsub(str_1_value, ll_center_square_root);

            } else if (str_1_value.startsWith("nsqrt")) {

                str_1_value = str_1_value.substring(5);

                final LinearLayout ll_nsqrt = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_nth_root, null);
                final LinearLayout ll_top_nth_root = (LinearLayout) ll_nsqrt.findViewById(R.id.ll_top_nth_root);
                final LinearLayout ll_center_nth_root = (LinearLayout) ll_nsqrt.findViewById(R.id.ll_center_nth_root);
                final ImageView img = (ImageView) ll_nsqrt.findViewById(R.id.img);
                ll_top_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                ll_center_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                ll_top_nth_root.setTag(R.id.second, "nth_top");
                ll_center_nth_root.setTag(R.id.second, "nth_bottom");

                img.getLayoutParams().height = ll_center_nth_root.getHeight();
                img.requestLayout();
                ll_center_nth_root.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                    @Override
                    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                        Log.e("het", "" + v.getHeight());
                        img.getLayoutParams().height = v.getHeight();
                        img.requestLayout();
                    }
                });

                final String[] str_2 = str_1_value.split("nsqrt_center_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_nsqrt);

                this.setstudentansboxsub(str_2[0], ll_top_nth_root);
                this.setstudentansboxsub(str_2[1], ll_center_nth_root);

            } else if (str_1_value.startsWith("lim")) {

                str_1_value = str_1_value.substring(3);

                final LinearLayout ll_lim = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_lim, null);
                final TextView tv_text = (TextView) ll_lim.findViewById(R.id.tv_text);
                tv_text.setText(Html.fromHtml("&#8594;"));
                final LinearLayout ll_lim_left = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_left);
                final LinearLayout ll_lim_right = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_right);
                ll_lim_left.setTag(R.id.first, (tag_main_ll + 1));
                ll_lim_right.setTag(R.id.first, (tag_main_ll + 1));
                ll_lim_left.setTag(R.id.second, "lim_left");
                ll_lim_right.setTag(R.id.second, "lim_right");

                final String[] str_2 = str_1_value.split("lim_rht_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_lim);

                this.setstudentansboxsub(str_2[0], ll_lim_left);
                this.setstudentansboxsub(str_2[1], ll_lim_right);

            } else if (str_1_value.startsWith("parenthesis")) {
                str_1_value = str_1_value.substring(11);

                final LinearLayout ll_parenthesis = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_parenthesis, null);
                final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_parenthesis_center);
                ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                ll_parenthesis_center.setTag(R.id.second, "parenthesis_center");

                final ImageView img_left = (ImageView) ll_parenthesis.findViewById(R.id.img_left);
                final ImageView img_rht = (ImageView) ll_parenthesis.findViewById(R.id.img_rht);

                img_left.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final LinearLayout lll = (LinearLayout) ll_parenthesis_center.getChildAt(0);
                        if (lll.getTag().equals("text")) {
                            final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                            edd.setSelection(0);
                            edd.requestFocus();
                        } else {
                            add_last_ed(ll_parenthesis_center, 22, 0);
                        }
                    }
                });

                img_rht.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final LinearLayout lll = (LinearLayout) ll_parenthesis_center.getChildAt((ll_parenthesis_center.getChildCount() - 1));
                        if (lll.getTag().equals("text")) {
                            final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                            edd.setSelection(edd.getText().toString().length());
                            edd.requestFocus();
                        } else {
                            add_last_ed(ll_parenthesis_center, 22, (ll_parenthesis_center.getChildCount()));
                        }
                    }
                });

                ll_ans_box.addView(ll_parenthesis);

                this.setstudentansboxsub(str_1_value, ll_parenthesis_center);

            } else if (str_1_value.startsWith("absolute")) {
                str_1_value = str_1_value.substring(8);

                final LinearLayout ll_super_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_absolute, null);
                final LinearLayout ll_parenthesis_center = (LinearLayout) ll_super_script.findViewById(R.id.ll_absolute_center);
                ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                ll_parenthesis_center.setTag(R.id.second, "absolute_center");

                ll_ans_box.addView(ll_super_script);

                this.setstudentansboxsub(str_1_value, ll_parenthesis_center);

            } else if (str_1_value.startsWith("line") || str_1_value.startsWith("lsegment") || str_1_value.startsWith("ray")
                    || str_1_value.startsWith("arc")) {

                final LinearLayout ll_parenthesis = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_lsegment, null);
                final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_lsegment_center);
                ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                final ImageView img_top = (ImageView) ll_parenthesis.findViewById(R.id.img_top);
                // ll_parenthesis_center.setTag(R.id.second,"absolute_center");

                if (str_1_value.startsWith("line")) {
                    img_top.setImageResource(R.drawable.line_icon);
                    str_1_value = str_1_value.substring(4);
                    ll_parenthesis.setTag("line");
                    ll_parenthesis_center.setTag(R.id.second, "line_center");
                } else if (str_1_value.startsWith("lsegment")) {
                    str_1_value = str_1_value.substring(8);
                    img_top.setImageResource(R.drawable.line_segment);
                    ll_parenthesis.setTag("lsegment");
                    ll_parenthesis_center.setTag(R.id.second, "lsegment_center");
                } else if (str_1_value.startsWith("ray")) {
                    str_1_value = str_1_value.substring(3);
                    img_top.setImageResource(R.drawable.ray_icon);
                    ll_parenthesis.setTag("ray");
                    ll_parenthesis_center.setTag(R.id.second, "ray_center");
                } else if (str_1_value.startsWith("arc")) {
                    str_1_value = str_1_value.substring(3);
                    img_top.setImageResource(R.drawable.arc_icon);
                    ll_parenthesis.setTag("arc");
                    ll_parenthesis_center.setTag(R.id.second, "arc_center");
                }

                ll_ans_box.addView(ll_parenthesis);

                this.setstudentansboxsub(str_1_value, ll_parenthesis_center);

            }

        }
    }

    public void setListenerOnNewKeys() {
        try {
            kb_btn_123_delete.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            is_touch_kb = true;
                            Handler hnd = new Handler();
                            hnd.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    ondeleteclick();
                                }
                            }, 200);
                            break;
                        case MotionEvent.ACTION_UP:
                            is_touch_kb = false;
                            break;
                    }
                    return true;
                }
            });

            kb_btn_abc_delete.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            is_touch_kb = true;
                            Handler hnd = new Handler();
                            hnd.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    ondeleteclick();
                                }
                            }, 200);
                            break;
                        case MotionEvent.ACTION_UP:
                            is_touch_kb = false;
                            break;
                    }
                    return true;
                }
            });

            kb_btn_123_previous.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            is_touch_kb_pre = true;
                            Handler hnd = new Handler();
                            hnd.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    onpreviousclick();
                                }
                            }, 200);
                            break;
                        case MotionEvent.ACTION_UP:
                            is_touch_kb_pre = false;
                            break;
                    }
                    return true;
                }
            });

            kb_btn_123_next.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            is_touch_kb_next = true;
                            Handler hnd = new Handler();
                            hnd.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    onnextclick();
                                }
                            }, 200);
                            break;
                        case MotionEvent.ACTION_UP:
                            is_touch_kb_next = false;
                            break;
                    }
                    return true;
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //For adding question with keyboard
    public void addansboxview(final LinearLayout ll_ans_box, String correct_ans, final int is_ans_available) {  // siddhiinfosoft

        try {

            try {
                ll_ans_box.removeAllViews();
            } catch (Exception e) {

            }

            if (is_ans_available == MathFriendzyHelper.NO) {

                final LinearLayout ll_last = (LinearLayout) this.getLayoutInflater().inflate(R.layout.kb_view_textview, null);
                ll_last.setTag("vtext");
                final TextView tv = (TextView) ll_last.findViewById(R.id.tv);
                String sttr = lblAnswerOnWorkAreaCreditGiven;
                tv.setText(sttr);

                tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MathFriendzyHelper.showWarningDialog(getCurrentObj(),
                                lblAnswerOnWorkAreaCreditGiven);
                    }
                });

                ll_ans_box.addView(ll_last);

            } else {

                if (correct_ans.trim().length() != 0) {
                    setstudentansboxsub(correct_ans, ll_ans_box);
                    Log.e("1234", "" + correct_ans);
                }

                boolean is_ed_last = false;

                if (ll_ans_box.getChildCount() > 0) {
                    LinearLayout lt = (LinearLayout) ll_ans_box.getChildAt(ll_ans_box.getChildCount() - 1);
                    if (lt.getTag().toString().equals("text")) {
                        is_ed_last = true;
                    } else {
                        is_ed_last = false;
                    }
                } else {
                    is_ed_last = false;
                }

                if (!is_ed_last) {

                    final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                    ll_last.setTag("text");
                    final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                    ed_centre.setMinWidth(20);
                    ll_last.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            custom_ed_touch(ed_centre, ll_last, ll_ans_box);
                        }
                    });
                    ed_centre.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            custom_ed_touch(ed_centre, ll_last, ll_ans_box);
                            return false;
                        }
                    });
                    ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                selectedTextView = ed_centre;
                                ll_selected_view = ll_last;
                                ll_ans_box_selected = ll_ans_box;
                            }
                        }
                    });

                    ll_ans_box.addView(ll_last);

                }

            }
            MathFriendzyHelper.enableDisableControls(false , ll_ans_box);
            /*for (int i = 0; i < ll_ans_box.getChildCount(); i++) {
                View child = ll_ans_box.getChildAt(i);
                child.setEnabled(false);
            }*/
        } catch (Exception e) {

        }
    }

    //Keyboard issues resolve by Hardip
    public EditTextBlink selectionEdittext = null;
    String blank_string;
    int cursor_postion;
    boolean parenth = false;
    public static boolean isGreen_student = false;

    /*@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            try {
                int cur_pos = selectedTextView.getSelectionStart();
                if (cur_pos != 0) {
                    ActAssignCustomAns.isGreen = false;
                }
            } catch (Exception e) {
            }

            if (isGreen_student) {
                try {
                    BlankFieldAlert();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }
        } else if (keyCode == KeyEvent.KEYCODE_ENTER) {

            this.hideDeviceKeyboard();
            this.showKeyboard();

        }
        return super.onKeyDown(keyCode, event);
    }*/

    @Override
    public void onBackPressed() {

        /*try {
            blank_string = selectedTextView.getText().toString().trim();
        }catch (Exception e){
            e.printStackTrace();
        }*/

        if (this.isShowPopUpForPasteImage()) {
            MathFriendzyHelper.showWarningDialog(this, alertMessageImagePasteOrNot);
            return;
        }
        //added for submit question answer button on the tutor area
        if (tutorSessionFragment != null
                && tutorSessionFragment.isEnterSomeThingToNotify()) {
            MathFriendzyHelper.yesNoConfirmationDialog(this,
                    lblPleaseEnterQuestionToNotifyTutor + "\n\n" + lblOrCancelTutorRequest,
                    btnTitleOK, btnTitleCancel,
                    new YesNoListenerInterface() {
                        @Override
                        public void onYes() {

                        }

                        @Override
                        public void onNo() {
                            goBack();
                        }
                    });

        } else {
            this.goBack();
        }
    }

    public void onkb_sub_symbol_click(View v) { // Added By Siddhi Info Soft

        try {

            String btn_tag = String.valueOf(v.getTag());

            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);

            if (btn_tag.equals("parenthesis")) {

                parenth = true;

            }

            if (btn_tag.equals("fraction") ||
                    btn_tag.equals("pointview")
                    || btn_tag.equals("lim") || btn_tag.equals("parenthesis") || btn_tag.equals("absolute") | btn_tag.equals("squareroot") || btn_tag.equals("nthroot")
                    || btn_tag.equals("super_script") || btn_tag.equals("sub_script") || btn_tag.equals("super_sub_script")
                    || btn_tag.equals("line") || btn_tag.equals("lsegment") || btn_tag.equals("ray")
                    || btn_tag.equals("arc")) {
                this.addviewtoselectedview(ll_ans_box_selected, btn_tag, "view");
            } else if (btn_tag.equals("absolute")) {
                setTextToSelectedTextView(selectedTextView, "|  |");
            } else {
                this.add_sign_into_box(btn_tag);
            }

        } catch (Exception e) {

        }
    }

    private void custom_ed_touch(final EditTextBlink ed, final LinearLayout ll_sub, final LinearLayout ll_main) {

//        parenth = true;

        try {

            ed.setCursorVisible(true);
            selectedTextView = ed;
            ll_selected_view = ll_sub;
            ll_ans_box_selected = ll_main;
            selected_ed_2 = null;
            showKeyboard();

        } catch (Exception e) {
        }
    }

    private void addviewtoselectedview(final LinearLayout ll_ans_box, String tag, String type) {  // Added By Siddhi Info Soft
        boolean is_devide = false;
        int start_pos = 0, ed_legnth = 0, ll_sub_selected_pos = 0;
        String cut_str = "";
        String sss = String.valueOf(ll_ans_box.getTag(R.id.first));
        int tag_main_ll = Integer.valueOf(sss);
        boolean need_delete = false;
        int text_size = HomeWorkWorkArea.this.getResources().getInteger(R.integer.kb_textsize_main_int);

        try {
            DisplayMetrics metrics;
            metrics = getApplicationContext().getResources().getDisplayMetrics();
            float Textsize = selectedTextView.getTextSize() / metrics.density;
            text_size = Math.round(Textsize);
            Log.e("text_size", "" + text_size);
            if (text_size < 5) {
                text_size = 5;
            }

        } catch (Exception e) {
            text_size = 16;
        }

        try {

            start_pos = selectedTextView.getSelectionStart();
            ed_legnth = selectedTextView.getText().toString().length();

            String str = selectedTextView.getText().toString().trim();

            boolean pluscheck = false;
            boolean minuscheck = false;
            boolean multicheck = false;
            boolean divicheck = false;
            boolean equalcheck = false;


            if (!TextUtils.isEmpty(str)) {


                // coding for 96 or 96 + 96 for add substring


                if (!str.contains("" + Html.fromHtml("&#43;"))) {
                    pluscheck = true;

                } else {
                    int s = 0;

                    if (str.endsWith("" + Html.fromHtml("&#43;"))) {

                    } else {
                        s = str.length() - 1;
                    }


                    String temstring = str.substring(0, s);


                    // coding for 96+ or 96 + 96 + for alert

                    if (temstring.contains("" + Html.fromHtml("&#43;"))) {
                        //str.contains("" + Html.fromHtml("&#215;")) || str.contains("" + Html.fromHtml("&#247;")) || str.contains("" + Html.fromHtml("&#61;")
                        pluscheck = true;
                    } else {

                        pluscheck = false;
                    }
                }


            /*    if (str.contains("" + Html.fromHtml("&#43;"))) {
                    pluscheck = false;
                } else {
                    pluscheck = true;
                }*/


                if (!str.contains("" + Html.fromHtml("&#8722;"))) {
                    minuscheck = true;

                } else {

                    int s = 0;

                    if (str.endsWith("" + Html.fromHtml("&#8722;"))) {

                    } else {
                        s = str.length() - 1;
                    }


                    String temstring = str.substring(0, s);

                    // coding for 96+ or 96 + 96 + for alert

                    if (temstring.contains("" + Html.fromHtml("&#8722;"))) {
                        //str.contains("" + Html.fromHtml("&#215;")) || str.contains("" + Html.fromHtml("&#247;")) || str.contains("" + Html.fromHtml("&#61;")
                        minuscheck = true;
                    } else {


                        minuscheck = false;
                    }
                }

              /*  if (str.contains("" + Html.fromHtml("&#8722;"))) {
                    minuscheck = false;
                } else {
                    minuscheck = true;
                }*/

                if (!str.contains("" + Html.fromHtml("&#215;"))) {
                    multicheck = true;

                } else {
                    int s = 0;

                    if (str.endsWith("" + Html.fromHtml("&#215;"))) {

                    } else {
                        s = str.length() - 1;
                    }

                    String temstring = str.substring(0, s);

                    // coding for 96+ or 96 + 96 + for alert

                    if (temstring.contains("" + Html.fromHtml("&#215;"))) {
                        //str.contains("" + Html.fromHtml("&#215;")) || str.contains("" + Html.fromHtml("&#247;")) || str.contains("" + Html.fromHtml("&#61;")
                        multicheck = true;
                    } else {


                        multicheck = false;
                    }
                }

/*
                if (str.contains("" + Html.fromHtml("&#215;"))) {
                    multicheck = false;
                } else {
                    multicheck = true;
                }*/


                if (!str.contains("" + Html.fromHtml("&#247;"))) {
                    divicheck = true;

                } else {
                    int s = 0;

                    if (str.endsWith("" + Html.fromHtml("&#247;"))) {

                    } else {
                        s = str.length() - 1;
                    }

                    String temstring = str.substring(0, s);

                    // coding for 96+ or 96 + 96 + for alert

                    if (temstring.contains("" + Html.fromHtml("&#247;"))) {
                        //str.contains("" + Html.fromHtml("&#215;")) || str.contains("" + Html.fromHtml("&#247;")) || str.contains("" + Html.fromHtml("&#61;")
                        divicheck = true;
                    } else {


                        divicheck = false;
                    }
                }

             /*   if (str.contains("" + Html.fromHtml("&#247;"))) {
                    divicheck = false;
                } else {
                    divicheck = true;
                }*/

                if (!str.contains("" + Html.fromHtml("&#61;"))) {
                    equalcheck = true;

                } else {
                    int s = 0;

                    if (str.endsWith("" + Html.fromHtml("&#61;"))) {

                    } else {
                        s = str.length() - 1;
                    }


                    String temstring = str.substring(0, s);

                    // coding for 96+ or 96 + 96 + for alert

                    if (temstring.contains("" + Html.fromHtml("&#61;"))) {
                        //str.contains("" + Html.fromHtml("&#215;")) || str.contains("" + Html.fromHtml("&#247;")) || str.contains("" + Html.fromHtml("&#61;")
                        equalcheck = true;
                    } else {


                        equalcheck = false;
                    }
                }

               /* if (str.contains("" + Html.fromHtml("&#61;"))) {
                    equalcheck = false;
                } else {
                    equalcheck = true;
                }*/


            }

            try {
                ll_sub_selected_pos = ll_ans_box_selected.indexOfChild(ll_selected_view);
            } catch (Exception e) {
                ll_sub_selected_pos = 0;
            }

            try {
                if (ll_sub_selected_pos == -1) {
                    ll_sub_selected_pos = 0;
                }

                if (start_pos != 0 && start_pos < ed_legnth) {
                    cut_str = selectedTextView.getText().toString();
                    cut_str = cut_str.substring(start_pos, ed_legnth);
                    is_devide = true;
                    Log.e("math_ed_pos", cut_str + "" + ll_sub_selected_pos);
                    selectedTextView.getText().delete(start_pos, ed_legnth);
                }
            } catch (Exception e) {
            }

            try {
                if (selectedTextView.getText().toString().trim().length() == 0) {
                    need_delete = true;
                }
            } catch (Exception e) {
            }

            try {

                final LinearLayout ll_last = (LinearLayout) ll_ans_box.getChildAt((ll_ans_box.getChildCount() - 1));
                String str_tag = String.valueOf(ll_last.getTag());
                // Log.e("tagg",str_tag);
                if (str_tag.equals("text")) {
                    final EditTextBlink ed_last = (EditTextBlink) ll_last.getChildAt(0);
                    if (ed_last.getText().toString().trim().length() == 0) {

                        try {
                            if (ll_ans_box.getChildCount() > 1) {
                                LinearLayout ll_last_2 = (LinearLayout) ll_ans_box.getChildAt((ll_ans_box.getChildCount() - 2));
                                String str_tag_2 = String.valueOf(ll_last_2.getTag());
                                if (str_tag_2.equals("text")) {
                                    ll_ans_box.removeView(ll_last);
                                } else {
                                    ed_last.setMinWidth(4);
                                }
                            } else {
                                ed_last.setMinWidth(4);
                            }
                        } catch (Exception e) {
                        }
                    } else {
                        //  Log.e("tagg12", str_tag);
                        ed_last.setMinWidth(4);
                        ed_last.setMinimumWidth(4);
                        ed_last.requestLayout();
                        //  Log.e("tagg123", str_tag);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (type.equals("view")) {
                selected_ed_2 = null;
                if (tag.equals("super_script")) {

                    final LinearLayout ll_super_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_superscript, null);


                    ll_super_script.setTag("super_script");
                    final LinearLayout ll_top_superscript = (LinearLayout) ll_super_script.findViewById(R.id.ll_top_superscript);
                    ll_top_superscript.setTag(R.id.first, (tag_main_ll + 1));
                    ll_top_superscript.setTag(R.id.second, "super_top");

                    add_inside_ed(ll_top_superscript, true, text_size);

                    /*if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_super_script, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_super_script, (ll_sub_selected_pos + 1));
                    }*/

                    if (start_pos == 0 && !parenth) {
                        isGreen_student = false;

                        AlertScript();
                        custom_ed_focus(selectionEdittext, ll_ans_box, ll_super_script, parenth);

                        //need for validation changes
                    } else if (sss.equals("2")) {

                        AlertScript();
                        custom_ed_focus(selectionEdittext, ll_ans_box, ll_super_script, parenth);

                    } /*else if (ll_sub_selected_pos != 0) {

                        if (!parenth) {

                            parenth = false;

                            AlertScript();
                        } else {
                            parenth = true;

                            ll_ans_box.addView(ll_super_script, (ll_sub_selected_pos + 1));
                        }

                    }*/ else if (start_pos != 0 && !pluscheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !minuscheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !multicheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !divicheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !equalcheck) {

                        AlertScript();


                    } else if (start_pos == 1 && !need_delete) {

                        parenth = false;
                        ll_ans_box.addView(ll_super_script, (ll_sub_selected_pos + 1));

                    } else {
                        parenth = false;
                        ll_ans_box.addView(ll_super_script, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("sub_script")) {

                    final LinearLayout ll_sub_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_subscript, null);
                    ll_sub_script.setTag("sub_script");
                    final LinearLayout ll_bottom_subscript = (LinearLayout) ll_sub_script.findViewById(R.id.ll_bottom_subscript);
                    ll_bottom_subscript.setTag(R.id.first, (tag_main_ll + 1));
                    ll_bottom_subscript.setTag(R.id.second, "sub_bottom");
                    add_inside_ed(ll_bottom_subscript, true, text_size);

                  /*  if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_sub_script, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_sub_script, (ll_sub_selected_pos + 1));
                    }*/

                    if (start_pos == 0 && !parenth) {
                        isGreen_student = false;

                        AlertScript();
                        custom_ed_focus(selectionEdittext, ll_ans_box, ll_sub_script, parenth);

                        //need for validation changes
                    } else if (sss.equals("2")) {

                        AlertScript();
                        custom_ed_focus(selectionEdittext, ll_ans_box, ll_sub_script, parenth);

                    }/* else if (ll_sub_selected_pos != 0) {

                        if (!parenth) {

                            parenth = false;

                            AlertScript();
                        } else {
                            parenth = true;

                            ll_ans_box.addView(ll_sub_script, (ll_sub_selected_pos + 1));
                        }

                    }*/ else if (start_pos != 0 && !pluscheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !minuscheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !multicheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !divicheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !equalcheck) {

                        AlertScript();


                    } else if (start_pos == 1 && !need_delete) {

                        parenth = false;
//                        ll_ans_box.addView(ll_sub_script, ll_sub_selected_pos);
                        ll_ans_box.addView(ll_sub_script, (ll_sub_selected_pos + 1));
                    } else {

                        parenth = false;
                        ll_ans_box.addView(ll_sub_script, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("super_sub_script")) {

                    final LinearLayout ll_super_sub_script = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_super_sub_script, null);
                    ll_super_sub_script.setTag("super_sub_script");

                    final LinearLayout ll_top_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_top_super_sub_script);
                    final LinearLayout ll_bottom_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_bottom_super_sub_script);
                    ll_top_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                    ll_bottom_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                    ll_top_super_sub_script.setTag(R.id.second, "ss_top");
                    ll_bottom_super_sub_script.setTag(R.id.second, "ss_bottom");
                    add_inside_ed(ll_top_super_sub_script, true, text_size);
                    add_inside_ed(ll_bottom_super_sub_script, false, text_size);

                  /*  if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_super_sub_script, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_super_sub_script, (ll_sub_selected_pos + 1));
                    }*/

                    if (start_pos == 0 && !parenth) {
                        isGreen_student = false;

                        AlertScript();
                        custom_ed_focus(selectionEdittext, ll_ans_box, ll_super_sub_script, parenth);

                        //need for validation changes
                    } else if (sss.equals("2")) {

                        AlertScript();
                        custom_ed_focus(selectionEdittext, ll_ans_box, ll_super_sub_script, parenth);

                    }/* else if (ll_sub_selected_pos != 0) {

                        if (!parenth) {

                            parenth = false;

                            AlertScript();
                        } else {
                            parenth = true;

                            ll_ans_box.addView(ll_super_sub_script, (ll_sub_selected_pos + 1));
                        }

                    }*/ else if (start_pos != 0 && !pluscheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !minuscheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !multicheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !divicheck) {

                        AlertScript();


                    } else if (start_pos != 0 && !equalcheck) {

                        AlertScript();


                    } else if (start_pos == 1 && !need_delete) {
                        parenth = false;
//                        ll_ans_box.addView(ll_sub_script, ll_sub_selected_pos);
                        ll_ans_box.addView(ll_super_sub_script, (ll_sub_selected_pos + 1));
                    } else {

                        parenth = false;
                        ll_ans_box.addView(ll_super_sub_script, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("fraction")) {

                    final LinearLayout ll_fraction = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_fraction, null);
                    final LinearLayout ll_fraction_top = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_top);
                    final LinearLayout ll_fraction_bottom = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_bottom);
                    ll_fraction_top.setTag(R.id.first, (tag_main_ll + 1));
                    ll_fraction_bottom.setTag(R.id.first, (tag_main_ll + 1));
                    ll_fraction_top.setTag(R.id.second, "frac_top");
                    ll_fraction_bottom.setTag(R.id.second, "frac_bottom");
                    add_inside_ed(ll_fraction_top, true, text_size);
                    add_inside_ed(ll_fraction_bottom, false, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_fraction, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_fraction, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }

                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("squareroot")) {

                    final LinearLayout ll_squareroot = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_square_root, null);
                    final LinearLayout ll_center_square_root = (LinearLayout) ll_squareroot.findViewById(R.id.ll_center_square_root);
                    ll_center_square_root.setTag(R.id.first, (tag_main_ll + 1));
                    ll_center_square_root.setTag(R.id.second, "sqrt_c");
                    add_inside_ed(ll_center_square_root, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_squareroot, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_squareroot, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("nthroot")) {

                    final LinearLayout ll_nthroot = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_nth_root, null);

                    final LinearLayout ll_top_nth_root = (LinearLayout) ll_nthroot.findViewById(R.id.ll_top_nth_root);
                    final LinearLayout ll_center_nth_root = (LinearLayout) ll_nthroot.findViewById(R.id.ll_center_nth_root);
                    final ImageView img = (ImageView) ll_nthroot.findViewById(R.id.img);
                    ll_top_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                    ll_center_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                    ll_top_nth_root.setTag(R.id.second, "nth_top");
                    ll_center_nth_root.setTag(R.id.second, "nth_bottom");
                    add_inside_ed(ll_top_nth_root, true, text_size);
                    add_inside_ed(ll_center_nth_root, false, text_size);
                    img.getLayoutParams().height = ll_center_nth_root.getHeight();
                    img.requestLayout();
                    ll_center_nth_root.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                        @Override
                        public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                            Log.e("het", "" + v.getHeight());
                            img.getLayoutParams().height = v.getHeight();
                            img.requestLayout();
                        }
                    });

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_nthroot, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_nthroot, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("lim")) {

                    final LinearLayout ll_lim = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_new_view_lim, null);
                    final TextView tv_text = (TextView) ll_lim.findViewById(R.id.tv_text);
                    tv_text.setText(Html.fromHtml("&#8594;"));
                    final LinearLayout ll_lim_left = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_left);
                    final LinearLayout ll_lim_right = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_right);
                    ll_lim_left.setTag(R.id.first, (tag_main_ll + 1));
                    ll_lim_right.setTag(R.id.first, (tag_main_ll + 1));
                    ll_lim_left.setTag(R.id.second, "lim_left");
                    ll_lim_right.setTag(R.id.second, "lim_right");
                    add_inside_ed(ll_lim_left, true, text_size);
                    add_inside_ed(ll_lim_right, false, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_lim, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_lim, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }

                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("parenthesis")) {

                    final LinearLayout ll_parenthesis = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_parenthesis, null);
                    ll_parenthesis.setTag("parenthesis");
                    final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_parenthesis_center);
                    ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                    ll_parenthesis_center.setTag(R.id.second, "parenthesis_center");
                    add_inside_ed(ll_parenthesis_center, true, text_size);
                    final ImageView img_left = (ImageView) ll_parenthesis.findViewById(R.id.img_left);
                    final ImageView img_rht = (ImageView) ll_parenthesis.findViewById(R.id.img_rht);

                    img_left.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final LinearLayout lll = (LinearLayout) ll_parenthesis_center.getChildAt(0);
                            if (lll.getTag().equals("text")) {
                                final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                edd.setSelection(0);
                                edd.requestFocus();
                            } else {
                                add_last_ed(ll_parenthesis_center, 22, 0);
                            }
                        }
                    });

                    img_rht.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final LinearLayout lll = (LinearLayout) ll_parenthesis_center.getChildAt((ll_parenthesis_center.getChildCount() - 1));
                            if (lll.getTag().equals("text")) {
                                final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                edd.setSelection(edd.getText().toString().length());
                                edd.requestFocus();
                            } else {
                                add_last_ed(ll_parenthesis_center, 22, (ll_parenthesis_center.getChildCount()));
                            }
                        }
                    });

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_parenthesis, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_parenthesis, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("absolute")) {

                    final LinearLayout ll_parenthesis = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_absolute, null);
                    ll_parenthesis.setTag("absolute");
                    final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_absolute_center);
                    ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                    ll_parenthesis_center.setTag(R.id.second, "absolute_center");
                    add_inside_ed(ll_parenthesis_center, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_parenthesis, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_parenthesis, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("line") || tag.equals("lsegment")
                        || tag.equals("ray") || tag.equals("arc")) {

                    final LinearLayout ll_parenthesis = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_lsegment, null);

                    final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_lsegment_center);
                    final ImageView img_top = (ImageView) ll_parenthesis.findViewById(R.id.img_top);
                    ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                    if (tag.equals("line")) {
                        img_top.setImageResource(R.drawable.line_icon);
                        ll_parenthesis.setTag("line");
                        ll_parenthesis_center.setTag(R.id.second, "line_center");
                    } else if (tag.equals("lsegment")) {
                        img_top.setImageResource(R.drawable.line_segment);
                        ll_parenthesis.setTag("lsegment");
                        ll_parenthesis_center.setTag(R.id.second, "lsegment_center");
                    } else if (tag.equals("ray")) {
                        img_top.setImageResource(R.drawable.ray_icon);
                        ll_parenthesis.setTag("ray");
                        ll_parenthesis_center.setTag(R.id.second, "ray_center");
                    } else if (tag.equals("arc")) {
                        img_top.setImageResource(R.drawable.arc_icon);
                        ll_parenthesis.setTag("arc");
                        ll_parenthesis_center.setTag(R.id.second, "arc_center");
                    }

                    add_inside_ed(ll_parenthesis_center, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_parenthesis, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_parenthesis, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                }
            }

            LinearLayout ll_last_2 = (LinearLayout) ll_ans_box.getChildAt((ll_ans_box.getChildCount() - 1));
            String str_tag = String.valueOf(ll_last_2.getTag());
            if (str_tag.equals("text")) {
                EditTextBlink ed_last = (EditTextBlink) ll_last_2.getChildAt(0);
                ed_last.setMinWidth(5);

            } else {

                final LinearLayout ll_last = (LinearLayout) getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                ll_last.setTag("text");
                final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                ed_centre.setTextSize(text_size);
                ed_centre.setMinWidth(4);
                ed_centre.setMinimumWidth(4);
                ed_centre.requestLayout();
                ll_last.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ed_centre.setCursorVisible(true);
                        selectedTextView = ed_centre;
                        ll_selected_view = ll_last;
                        ll_ans_box_selected = ll_ans_box;
                        selected_ed_2 = null;
                        showKeyboard();
                    }
                });
                ed_centre.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        ed_centre.setCursorVisible(true);
                        selectedTextView = ed_centre;
                        ll_selected_view = ll_last;
                        ll_ans_box_selected = ll_ans_box;
                        selected_ed_2 = null;
                        showKeyboard();
                        return false;
                    }
                });
                ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            selectedTextView = ed_centre;
                            ll_selected_view = ll_last;
                            selected_ed_2 = null;
                            ll_ans_box_selected = ll_ans_box;
                        }
                    }
                });

                ll_ans_box.addView(ll_last);

            }

        } catch (Exception e) {

        }
    }

    public void onnextclick() {


        try {
            int cur_pos = 0, ed_legnth = 0;

            try {
                cur_pos = selectedTextView.getSelectionStart();
                ed_legnth = selectedTextView.getText().toString().length();
            } catch (Exception e) {
            }


            if ((cur_pos != ed_legnth)) {

                String str_bar_1 = "" + Html.fromHtml("&#x0304;");
                String str_bar_2 = "" + Html.fromHtml("&#772;");

                if (selectedTextView.getText().toString().length() > 1) {
                    String ss = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos));
                    if (ss.equals("x")) {
                        if ((cur_pos + 1) < ed_legnth) {
                            String ss_1 = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos + 1));
                            if (ss_1.equals(str_bar_1) || ss_1.equals(str_bar_2)) {
                                selectedTextView.setSelection((cur_pos + 2));
                            } else {
                                selectedTextView.setSelection((cur_pos + 1));
                            }
                        } else {
                            selectedTextView.setSelection((cur_pos + 1));
                        }
                    } else {
                        selectedTextView.setSelection((cur_pos + 1));
                    }
                } else {
                    selectedTextView.setSelection((cur_pos + 1));
                }

                if (is_touch_kb_next) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onnextclick();
                        }
                    }, 150);
                }

            } else {
                final LinearLayout ll_main_ll = ll_ans_box_selected;
                final LinearLayout ll_sub_ll = ll_selected_view;

                int pos_sub_ll = ll_main_ll.indexOfChild(ll_sub_ll);

                if (ll_main_ll.getChildCount() > 1 && pos_sub_ll < (ll_main_ll.getChildCount() - 1)) {

                    LinearLayout ll_sub = (LinearLayout) ll_main_ll.getChildAt(pos_sub_ll + 1);
                    onnextsubcall(ll_sub);

                } else if (pos_sub_ll == (ll_main_ll.getChildCount() - 1)) {

                    String main_tag = (String) ll_main_ll.getTag(R.id.second);

                    if (main_tag.equals("frac_top")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("ss_top")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(1);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("nth_top")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("lim_left")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("sqrt_c") || main_tag.equals("nth_bottom") || main_tag.equals("lim_right")) {

                        sub_next_sqrt_c(ll_main_ll);

                    } else if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom") ||
                            main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                            || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                            || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {

                        if (main_tag.equals("parenthesis_center")) {

                            parenth = true;
                        }

                        sub_next_frac_bottom(ll_main_ll);

                    }
                }

                if (is_touch_kb_next) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onnextclick();
                        }
                    }, 150);
                }
            }

        } catch (Exception e) {
        }
    }

    public void ondeleteclick() {

        ActAssignCustomAns.isGreen = false;


//        parenth = false;
        final LinearLayout ll_main_ll = ll_ans_box_selected;
        final LinearLayout ll_sub_ll = ll_selected_view;

        try {
            int ed_pos = 0, ed_legnth = 0;
            try {
                ed_pos = selectedTextView.getSelectionStart();
                ed_legnth = selectedTextView.getText().toString().trim().length();
            } catch (Exception e) {
            }
            if (ed_pos > 0 && ed_legnth > 0) {
                String str_bar_1 = "" + Html.fromHtml("&#x0304;");
                String str_bar_2 = "" + Html.fromHtml("&#772;");
                String str_integral = "" + Html.fromHtml("&#8747;");
                if (selectedTextView.getText().toString().length() > 1) {
                    String ss = String.valueOf(selectedTextView.getText().toString().charAt(ed_pos - 1));
                    if (ss.equals(str_bar_1) || ss.equals(str_bar_2)) {

                        selectedTextView.getText().delete(ed_pos - 2, ed_pos);

                    } else if (ss.equals("x")) {
                        if (ed_pos < ed_legnth) {
                            String ss_2 = String.valueOf(selectedTextView.getText().toString().charAt(ed_pos));
                            if (ss_2.equals(str_bar_1) || ss_2.equals(str_bar_2)) {
                                selectedTextView.getText().delete(ed_pos - 1, ed_pos + 1);

                            } else {
                                selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                            }
                        } else {
                            selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                        }
                    } else if (ss.equals(" ")) {
                        try {
                            String ss2 = String.valueOf(selectedTextView.getText().toString().charAt(ed_pos - 2));
                            if (ss2.equals(str_integral)) {
                                selectedTextView.getText().delete(ed_pos - 2, ed_pos);
                            } else {
                                selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                            }
                        } catch (Exception e) {
                            selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                        }
                    } else {
                        selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                    }
                } else {
                    selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                }
                try {
                    if (selectedTextView.getText().toString().trim().length() == 0) {
                        selectedTextView.setSelection(0);
                    }
                } catch (Exception e) {
                }

                if (is_touch_kb) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ondeleteclick();
                        }
                    }, 150);
                }

            } else if (ed_pos == 0 && ed_legnth == 0) {

                int pos_sub_ll = ll_main_ll.indexOfChild(ll_sub_ll);
                Log.e("delete4", "" + pos_sub_ll);

                if (pos_sub_ll != 0 && pos_sub_ll != -1) {

                    LinearLayout ll_sub = (LinearLayout) ll_main_ll.getChildAt(pos_sub_ll - 1);
                    onpresubcall(ll_sub);
                    ll_main_ll.removeViewAt(pos_sub_ll);

                } else if (pos_sub_ll == 0) {

                    String main_tag = (String) ll_main_ll.getTag(R.id.second);

                    Log.e("delete5", "" + main_tag);

                    if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                        if (ll_main_ll.getChildCount() > 1) {
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        }

                    } else if (main_tag.equals("nth_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                        if (ll_main_ll.getChildCount() > 1) {
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        }

                    } else if (main_tag.equals("lim_right")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                        if (ll_main_ll.getChildCount() > 1) {
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        }

                    } else if (main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                            || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                            || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {

                        try {
                            LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            LinearLayout ll_main2 = (LinearLayout) ll_main.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_frac_top(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {

                                if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                    sub_pre_frac_top(ll_main_ll, 2, false);
                                    ll_main2.removeViewAt(main_pos);
                                    this.add_ed_after_delete(ll_main2, 18, main_pos);
                                } else {
                                    if (main_pos != 0) {
                                        sub_pre_frac_top(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                    } else {
                                        sub_pre_frac_top(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                        try {
                                            final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                            if (lll.getTag().toString().equals("text")) {
                                                EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                edd.setMinWidth(20);
                                                edd.setMinimumWidth(20);
                                                edd.requestLayout();
                                                edd.requestFocus();
                                                if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                    Utils.change_edittext_bg(true, edd);
                                                }
                                            }
                                        } catch (Exception e) {
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("sqrt_c")) {

                        try {

                            final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            final LinearLayout ll_main1 = (LinearLayout) ll_main.getParent();
                            final LinearLayout ll_main2 = (LinearLayout) ll_main1.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main1);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_sqrt_c(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {
                                if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                    Log.e("edd", "" + main_pos);
                                    sub_pre_sqrt_c(ll_main_ll, 2, false);
                                    ll_main2.removeViewAt(main_pos);
                                    Log.e("edd1", "" + main_pos);
                                    this.add_ed_after_delete(ll_main2, 18, main_pos);
                                } else {
                                    if (main_pos != 0) {
                                        sub_pre_sqrt_c(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                    } else {
                                        sub_pre_sqrt_c(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                        try {
                                            final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                            if (lll.getTag().toString().equals("text")) {
                                                final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                edd.setMinWidth(20);
                                                edd.setMinimumWidth(20);
                                                edd.requestLayout();
                                                edd.requestFocus();
                                                if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                    Utils.change_edittext_bg(true, edd);
                                                }
                                            }
                                        } catch (Exception e) {
                                        }
                                    }
                                }
                            }

                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("nth_top")) {

                        try {
                            final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            final LinearLayout ll_main2 = (LinearLayout) ll_main.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_frac_top(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {
                                final LinearLayout ll_sqrt = (LinearLayout) ll_main.getChildAt(2);
                                final LinearLayout ll_sqrt2 = (LinearLayout) ll_sqrt.getChildAt(1);
                                if (this.makeansfromview(ll_sqrt2).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_frac_top(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            }

                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("lim_left")) {

                        try {
                            final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            final LinearLayout ll_main1 = (LinearLayout) ll_main.getParent();
                            final LinearLayout ll_main2 = (LinearLayout) ll_main1.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main1);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_sqrt_c(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {
                                final LinearLayout ll_lim_rht = (LinearLayout) ll_main.getChildAt(2);
                                if (this.makeansfromview(ll_lim_rht).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_sqrt_c(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_sqrt_c(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_sqrt_c(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            }

                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("frac_top") || main_tag.equals("ss_top")) {

                        final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        final LinearLayout ll_main2 = (LinearLayout) ll_main.getParent();
                        int main_pos = ll_main2.indexOfChild(ll_main);

                        if (ll_main_ll.getChildCount() > 1) {
                            sub_pre_frac_top(ll_main_ll, 0, false);
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        } else {
                            if (main_tag.equals("frac_top")) {
                                LinearLayout ll_frac = (LinearLayout) ll_main.getChildAt(2);
                                if (this.makeansfromview(ll_frac).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_frac_top(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            } else {
                                LinearLayout ll_frac = (LinearLayout) ll_main.getChildAt(1);

                                if (this.makeansfromview(ll_frac).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_frac_top(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            }
                        }
                    }
                }

                if (is_touch_kb) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ondeleteclick();
                        }
                    }, 150);
                }
            } else {
                this.onpreviousclick();
                if (is_touch_kb) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ondeleteclick();
                        }
                    }, 150);
                }
            }

        } catch (Exception e) {
        }
    }

    public void onpreviousclick() {

        parenth = false;

        try {
            int cur_pos = 0;

            try {
                cur_pos = selectedTextView.getSelectionStart();
            } catch (Exception e) {
            }

            if (cur_pos != 0) {

                String str_bar_1 = "" + Html.fromHtml("&#x0304;");
                String str_bar_2 = "" + Html.fromHtml("&#772;");

                if (selectedTextView.getText().toString().length() > 1) {
                    String ss = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos - 1));
                    if (ss.equals(str_bar_1) || ss.equals(str_bar_2)) {

                        selectedTextView.setSelection((cur_pos - 2));

                    } else {
                        selectedTextView.setSelection((cur_pos - 1));
                    }
                } else {
                    selectedTextView.setSelection((cur_pos - 1));
                }

                if (is_touch_kb_pre) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onpreviousclick();
                        }
                    }, 150);
                }

            } else {

                final LinearLayout ll_main_ll = ll_ans_box_selected;
                final LinearLayout ll_sub_ll = ll_selected_view;

                int pos_sub_ll = ll_main_ll.indexOfChild(ll_sub_ll);

                if (pos_sub_ll != 0 && pos_sub_ll != -1) {

                    LinearLayout ll_sub = (LinearLayout) ll_main_ll.getChildAt(pos_sub_ll - 1);
                    onpresubcall(ll_sub);

                } else if (pos_sub_ll == 0) {

                    String main_tag = (String) ll_main_ll.getTag(R.id.second);

                    if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                    } else if (main_tag.equals("nth_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                    } else if (main_tag.equals("lim_right")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                    } else if (main_tag.equals("sqrt_c") || main_tag.equals("lim_left")) {

                        try {
                            sub_pre_sqrt_c(ll_main_ll, 0, true);
                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("frac_top") || main_tag.equals("ss_top") ||
                            main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("nth_top") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                            || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                            || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {

                        try {
                            sub_pre_frac_top(ll_main_ll, 0, true);
                        } catch (Exception e) {
                        }

                    }
                }

                if (is_touch_kb_pre) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onpreviousclick();
                        }
                    }, 150);
                }

            }

        } catch (Exception e) {
        }

    }

    public void hideDeviceKeyboardAndClearFocus() {
        try {
            //this.cleasFocusFromEditText();
            this.hideDeviceKeyboard();
            this.hideKeyboard();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void AlertScript() {
        /*final Dialog dialog = new Dialog(ActAssignCustomAns.this);
        dialog.setContentView(R.layout.script_alert_dialoug);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_title);
        TextView dialogButton = (TextView) dialog.findViewById(R.id.tv_ok);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                parenth = false;
                hideDeviceKeyboardAndClearFocus();
            }
        });
        dialog.show();*/

        MathFriendzyHelper.showWarningDialog(this,
                MathFriendzyHelper.getTreanslationTextById(this, "lblProbWithAns"),
                new HttpServerRequest() {
                    @Override
                    public void onRequestComplete() {
                        parenth = false;
                        hideDeviceKeyboardAndClearFocus();
//                        ActCheckHomeWork.this.cleasFocusFromEditText();
                    }
                });
    }

    private void BlankFieldAlert() {
        /*final Dialog dialog = new Dialog(ActAssignCustomAns.this);
        dialog.setContentView(R.layout.blank_field_alert_dialoug);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView text = (TextView) dialog.findViewById(R.id.tv_title_blank);

        TextView dialogButton = (TextView) dialog.findViewById(R.id.tv_ok_blank);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                parenth = false;
                hideDeviceKeyboardAndClearFocus();
            }
        });
        dialog.show();*/

        MathFriendzyHelper.showWarningDialog(this,
                MathFriendzyHelper.getTreanslationTextById(this, "lblLeavingFieldEmpty"),
                new HttpServerRequest() {
                    @Override
                    public void onRequestComplete() {
                        parenth = false;
                        //hideDeviceKeyboardAndClearFocus();
                    }
                });
    }


    //audio calling changes
    private void setAudioButtonVisibility(boolean isVisible){
        if(isVisible) {
            btnAudioCall.setVisibility(View.VISIBLE);
        }else{
            btnAudioCall.setVisibility(View.GONE);
        }
    }

    /*private void clickOnAudioCall() {
        try {
            if (tutorSessionFragment != null) {
                tutorSessionFragment.initializeAudioCallButton(btnAudioCall);
                tutorSessionFragment.startEndAudioCall();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }*/

    private void initializeAudioButtonObject(){
        if (tutorSessionFragment != null) {
            tutorSessionFragment.initializeAudioCallButton(btnAudioCall);
        }
    }
}