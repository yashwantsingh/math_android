package com.mathfriendzy.controller.homework.homwworkworkarea;

import java.util.ArrayList;

/**
 * Created by root on 23/8/16.
 */
public interface OnUrlSaveCallback {
    int ON_SAVE = 1 , ON_CLOSE = 2;
    void onSave(ArrayList<AddUrlToWorkArea> updatedUrlLinkList , int tag);
}
