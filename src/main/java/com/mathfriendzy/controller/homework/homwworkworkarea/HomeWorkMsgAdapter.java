package com.mathfriendzy.controller.homework.homwworkworkarea;

import java.util.ArrayList;

import com.mathfriendzy.R;
import com.mathfriendzy.helper.MathFriendzyHelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class HomeWorkMsgAdapter extends BaseAdapter{
	
	private Context context = null;
	public ArrayList<String> msgList = null;
	private ViewHolder vHolder = null;
	private LayoutInflater mInflator = null;
	
	public HomeWorkMsgAdapter(Context context , ArrayList<String> msgList){
		this.context = context;
		this.msgList = msgList;
		mInflator = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return msgList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		if(view  == null){
			vHolder = new ViewHolder();
			view = mInflator.inflate(R.layout.work_area_teacher_studemt_message_layout, null);
			vHolder.txtView = (TextView) view.findViewById(R.id.txtMsg);
			view.setTag(vHolder);
		}else{
			vHolder = (ViewHolder) view.getTag();
		}
		if(msgList.get(position).startsWith(MathFriendzyHelper.STUDENT_MSG_PREFF)){
			vHolder.txtView.setTextColor(context.getResources().getColor(R.color.BLACK));
		}else{
			vHolder.txtView.setTextColor(context.getResources().getColor(R.color.PURPULE));
		}
		vHolder.txtView.setText(msgList.get(position).replace
				(MathFriendzyHelper.STUDENT_TEACHER_MSG_DELIMETER, ""));
		return view;
	}

	private class ViewHolder{
		private TextView txtView;
	}
}
