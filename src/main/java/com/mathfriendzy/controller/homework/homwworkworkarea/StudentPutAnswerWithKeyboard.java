package com.mathfriendzy.controller.homework.homwworkworkarea;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.homework.Utils;
import com.mathfriendzy.customview.AutoResizeEditText;
import com.mathfriendzy.customview.EditTextBlink;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.homework.CheckUserAnswer;
import com.mathfriendzy.model.homework.CustomeAns;
import com.mathfriendzy.model.homework.CustomePlayerAns;
import com.mathfriendzy.model.homework.CustomeResult;
import com.mathfriendzy.model.homework.MultipleChoiceAnswer;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.utils.CommonUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Created by root on 1/9/16.
 */
public class StudentPutAnswerWithKeyboard extends HomeWorkWorkArea{

    private CustomeAns customeAns = null;
    private LinearLayout checkHomeWorkListLayout = null;
    private Context context = null;
    private CustomePlayerAns playerAns = null;
    private ArrayList<MultipleChoiceAnswer> multipleChoiceAnsList = null;
    private int dpwidth = 600;
    private EditText selectedTextView = null, selected_ed_2 = null;
    private LinearLayout ll_visible_view = null, ll_ans_box_selected = null;
    //boolean is_on_calculate = false;
    private final int FILL_IN_TYPE = 1;
    private LinearLayout ll_selected_view = null;
    private boolean is_keyboard_show = false;
    protected Animation animation = null;
    private RelativeLayout keyboardLayout = null;
    private String lblAnswerOnWorkAreaCreditGiven;
    //private CustomeResult customeResult = null;

    public StudentPutAnswerWithKeyboard(Context context ,
                                        CustomeAns customeAns ,
                                        CustomePlayerAns playerAns , LinearLayout checkHomeWorkListLayout ,
                                         RelativeLayout keyboardLayout ,
                                        String lblAnswerOnWorkAreaCreditGiven ,
                                        CustomeResult customeResult ){
        this.checkHomeWorkListLayout = checkHomeWorkListLayout;
        this.customeAns = customeAns;
        this.playerAns = playerAns;
        this.context = context;
        multipleChoiceAnsList = new ArrayList<MultipleChoiceAnswer>();
        this.keyboardLayout = keyboardLayout;
        this.lblAnswerOnWorkAreaCreditGiven = lblAnswerOnWorkAreaCreditGiven;
        //this.customeResult = customeResult;
        this.initializeWidth();
    }

    private void initializeWidth(){
        try {
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            // float dpWidth2 = displayMetrics.widthPixels / displayMetrics.density;
            float dpWidth2 = displayMetrics.widthPixels;
            dpwidth = Math.round(dpWidth2);
            if (dpwidth < 600) {
                dpwidth = 600;
            }
        } catch (Exception e) {
            dpwidth = 600;
        }
    }

    /**
     * Set the custom result data to the layout
     */
    public void setCustomeDataToListLayout() {
        checkHomeWorkListLayout.removeAllViews();
        //putViewOnKeyQuestionNumber.clear();
        View view = null;
        if (customeAns.getFillInType() == 1) {
            view = this.getCheckFillInLayout();
        } else if (customeAns.getAnswerType() == 1) {//for multiple choice
            view = this.getCheckMultiPleChoiceLayout();
        } else if (customeAns.getAnswerType() == 2) {//for true/false
            view = this.getCheckTrueFalseLayout();
        } else if (customeAns.getAnswerType() == 3) {//for yes/no
            view = this.getCheckTrueFalseLayout();
        }
        if (view != null) {
            checkHomeWorkListLayout.addView(view);
                /*putViewOnKeyQuestionNumber.put(customeAns.getQueNo()
                        , view);*/
        }
    }

    /**
     * Inflate the layout for the fill in type
     *
     * @param customeAns
     * @return
     */
    @SuppressLint("InflateParams")
    public View getCheckFillInLayout() {
        final CustomePlayerAns playerAns = this.playerAns;
        View view = LayoutInflater.from(context).inflate(R.layout.kb_check_home_fill_in_blank_layout, null);
        RelativeLayout viewLayout = (RelativeLayout) view.findViewById(R.id.checkHomeLayout);
        Button btnSeeAnswer = (Button) view.findViewById(R.id.btnSeeAnswer);
        TextView txtNumber = (AutoResizeEditText) view.findViewById(R.id.txtNumber);
        TextView txtLeftUnit = (TextView) view.findViewById(R.id.txtLeftUnit);
        // TextView ansBox = (TextView) view.findViewById(R.id.ansBox);  //siddhiinfosoft
        TextView txtRighttUnit = (TextView) view.findViewById(R.id.txtRighttUnit);
        final Button btnRoughWork = (Button) view.findViewById(R.id.btnRoughWork);
        final LinearLayout ll_ans_box = (LinearLayout) view.findViewById(R.id.ll_ans_box); //siddhiinfosoft
        RelativeLayout rlCorrectInCorrectLayout = (RelativeLayout) view.findViewById(R.id.rlCorrectInCorrectLayout);
        TextView txtCorrectAnsCounter = (TextView) view.findViewById(R.id.txtCorrectAnsCounter);
        TextView txtWrongAnsCounter = (TextView) view.findViewById(R.id.txtWrongAnsCounter);
        //Add Question
        TextView edtQuestion = (TextView) view.findViewById(R.id.edtQuestion);
        this.setQuestion(customeAns , edtQuestion);
        //for Teacher Check Homework
       /* this.setVisibilityOfRoughButton(btnRoughWork);
        this.setRoughAreaButtonBackGround(playerAns, btnRoughWork);*/
        txtNumber.setText(customeAns.getQueNo());
        if (customeAns.getIsLeftUnit() == 0) {
            txtLeftUnit.setText("");
            txtRighttUnit.setText(customeAns.getAnsSuffix());
        } else {
//            txtLeftUnit.setText(customeAns.getAnsSuffix());
//            txtRighttUnit.setText("");
            txtLeftUnit.setText("");
            txtRighttUnit.setText(customeAns.getAnsSuffix());
        }
        ll_ans_box.setMinimumWidth(dpwidth - 170); // siddhiinfosoft
        ll_ans_box.setTag(R.id.first, "1");
        ll_ans_box.setTag(R.id.second, "main");
        this.setstudentansbox(customeAns, ll_ans_box, playerAns);  // siddhiinfosoft

        //userFillInAnsList.add(fillInAns);

        /*if (isAllowChanges && this.isPlayerAnswerEditable(playerAns)) {

        } else {
            ll_ans_box.setOnClickListener(new View.OnClickListener() { // add by siddhi info soft
                @Override
                public void onClick(View v) {
                    if (isQuizExpireForStudent()) {
                        showCantEditAnswerDialog();
                        return;
                    }else if(!isPlayerAnswerEditable(playerAns)){
                        showNoLongerChangeThisQuestion();
                        return ;
                    }
                }
            });
        }*/

        ll_ans_box.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (customeAns.getIsAnswerAvailable() == MathFriendzyHelper.NO) {
                    MathFriendzyHelper.showWarningDialog(context,
                            lblAnswerOnWorkAreaCreditGiven);
                } else {
                    if (isQuizExpireForStudent()) {
                        showCantEditAnswerDialog();
                        return;
                    }else if(!isPlayerAnswerEditable(playerAns)){
                        showNoLongerChangeThisQuestion();
                        return ;
                    }else {
                        callclickmethod(customeAns, (LinearLayout) v, 0);
                    }
                }*/
                callclickmethod(customeAns, (LinearLayout) v, 0);
            }
        });

        return view;
    }

    /**
     * Convert the str teacher credit into int
     *
     * @param strTeacherCredit
     * @return
     */
    private int getIntTeacherCreditValue(String strTeacherCredit) {
        try {
            return Integer.parseInt(strTeacherCredit);
        } catch (Exception e) {
            return 1;
        }
    }

    /**
     * Inflate the layout for not fill in type but for multiple choice answer type = 1
     *
     * @param customeAns
     * @return
     */
    @SuppressLint("InflateParams")
    public View getCheckMultiPleChoiceLayout() {
        final CustomePlayerAns playerAns = this.playerAns;

        View view = LayoutInflater.from(context).inflate(R.layout.check_home_multiple_choice_layout, null);
        RelativeLayout viewLayout = (RelativeLayout) view.findViewById(R.id.checkHomeLayout);
        Button btnSeeAnswer = (Button) view.findViewById(R.id.btnSeeAnswer);
        TextView txtNumber = (TextView) view.findViewById(R.id.txtNumber);
        final Button btnRoughWork = (Button) view.findViewById(R.id.btnRoughWork);
        LinearLayout optionLayout = (LinearLayout) view.findViewById(R.id.optionLayout);
        RelativeLayout rlCorrectInCorrectLayout = (RelativeLayout) view.findViewById(R.id.rlCorrectInCorrectLayout);
        TextView txtCorrectAnsCounter = (TextView) view.findViewById(R.id.txtCorrectAnsCounter);
        TextView txtWrongAnsCounter = (TextView) view.findViewById(R.id.txtWrongAnsCounter);

        //Add Question
        TextView edtQuestion = (TextView) view.findViewById(R.id.edtQuestion);
        this.setQuestion(customeAns , edtQuestion);

        final MultipleChoiceAnswer multipleChiceAns = new MultipleChoiceAnswer();
        multipleChiceAns.setQueId(customeAns.getQueNo());
        multipleChiceAns.setCustomeAns(customeAns);
        multipleChiceAns.setFirstTimeWrong(0);

        ArrayList<TextView> optionList = new ArrayList<TextView>();
        multipleChiceAns.setOptionList(optionList);
        String[] commaSepratedOption = MathFriendzyHelper.
                getCommaSepratedOption(customeAns.getOptions(), ",");

        for (int i = 0; i < commaSepratedOption.length; i++) {
            optionLayout.addView(this.getMultipleOptionLayout
                    (commaSepratedOption[i], customeAns, optionList,
                            multipleChiceAns, playerAns));
        }
        txtNumber.setText(customeAns.getQueNo());

        return view;
    }

    /**
     * Inflate the layout for not fill in type but for multiple choice answer type = 2 or 3
     * for true false or yes no
     *
     * @param customeAns
     * @return
     */
    @SuppressLint("InflateParams")
    public View getCheckTrueFalseLayout() {
        final CustomePlayerAns playerAns = this.playerAns;
        View view = LayoutInflater.from(context).inflate(R.layout.check_home_true_false_layout, null);
        RelativeLayout viewLayout = (RelativeLayout) view.findViewById(R.id.checkHomeLayout);
        Button btnSeeAnswer = (Button) view.findViewById(R.id.btnSeeAnswer);
        TextView txtNumber = (TextView) view.findViewById(R.id.txtNumber);
        final TextView ansTrue = (TextView) view.findViewById(R.id.ansTrue);
        final TextView ansFalse = (TextView) view.findViewById(R.id.ansFalse);
        final Button btnRoughWork = (Button) view.findViewById(R.id.btnRoughWork);
        RelativeLayout rlCorrectInCorrectLayout = (RelativeLayout) view.findViewById(R.id.rlCorrectInCorrectLayout);
        TextView txtCorrectAnsCounter = (TextView) view.findViewById(R.id.txtCorrectAnsCounter);
        TextView txtWrongAnsCounter = (TextView) view.findViewById(R.id.txtWrongAnsCounter);

        txtNumber.setText(customeAns.getQueNo());
        String[] ansArray = MathFriendzyHelper.getCommaSepratedOption(customeAns.getOptions(), ",");
        ansTrue.setText(ansArray[0]);
        ansFalse.setText(ansArray[1]);

        //Add Question
        TextView edtQuestion = (TextView) view.findViewById(R.id.edtQuestion);
        this.setQuestion(customeAns , edtQuestion);


        //for Teacher Check Homework
        //this.setVisibilityOfRoughButton(btnRoughWork);
        //this.setRoughAreaButtonBackGround(playerAns, btnRoughWork);

        final CheckUserAnswer checkUserAns = new CheckUserAnswer();
        checkUserAns.setQueId(customeAns.getQueNo());
        checkUserAns.setCustomeAns(customeAns);
        checkUserAns.setTxtYes(ansTrue);
        checkUserAns.setTxtNo(ansFalse);
        checkUserAns.setFirstTimeWrong(0);

        if (playerAns != null) {
            if (playerAns.getAns().equals("Yes")) {
                checkUserAns.setUserAns(checkUserAns.getTxtYes().getText().toString());
                checkUserAns.getTxtYes().setBackgroundResource(R.drawable.checked_box_home_work);
                checkUserAns.getTxtNo().setBackgroundResource(R.drawable.unchecked_box_home_work);
            } else if (playerAns.getAns().equals("No")) {
                checkUserAns.setUserAns(checkUserAns.getTxtNo().getText().toString());
                checkUserAns.getTxtYes().setBackgroundResource(R.drawable.unchecked_box_home_work);
                checkUserAns.getTxtNo().setBackgroundResource(R.drawable.checked_box_home_work);
            } else if (playerAns.getAns().equals("True")) {
                checkUserAns.setUserAns(checkUserAns.getTxtYes().getText().toString());
                checkUserAns.getTxtYes().setBackgroundResource(R.drawable.checked_box_home_work);
                checkUserAns.getTxtNo().setBackgroundResource(R.drawable.unchecked_box_home_work);
            } else if (playerAns.getAns().equals("False")) {
                checkUserAns.setUserAns(checkUserAns.getTxtNo().getText().toString());
                checkUserAns.getTxtYes().setBackgroundResource(R.drawable.unchecked_box_home_work);
                checkUserAns.getTxtNo().setBackgroundResource(R.drawable.checked_box_home_work);
            }
        }

        ansTrue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkUserAns.setUserAns(checkUserAns.getTxtYes().getText().toString());
                checkUserAns.getTxtYes().setBackgroundResource(R.drawable.checked_box_home_work);
                checkUserAns.getTxtNo().setBackgroundResource(R.drawable.unchecked_box_home_work);
            }
        });

        ansFalse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkUserAns.setUserAns(checkUserAns.getTxtNo().getText().toString());
                checkUserAns.getTxtNo().setBackgroundResource(R.drawable.checked_box_home_work);
                checkUserAns.getTxtYes().setBackgroundResource(R.drawable.unchecked_box_home_work);

            }
        });
        return view;
    }

    //Add Question
    private void setQuestion(CustomeAns customeAns, TextView edtQuestion) {
        try{
            if(MathFriendzyHelper.isEmpty(customeAns.getQuestionString())){
                edtQuestion.setVisibility(View.GONE);
            }else{
                edtQuestion.setText(customeAns.getQuestionString());
                edtQuestion.setVisibility(View.VISIBLE);
            }
        }catch (Exception e){
            e.printStackTrace();
            edtQuestion.setVisibility(View.GONE);
        }
    }

    /**
     * Return the inflated view to multiple choice option
     *
     * @param optionText
     * @param customeAns
     * @param optionList
     * @param multipleChiceAns
     * @param playerAns
     * @return
     */
    @SuppressLint("InflateParams")
    private View getMultipleOptionLayout(String optionText,
                                         final CustomeAns customeAns, ArrayList<TextView> optionList,
                                         MultipleChoiceAnswer multipleChiceAns, final CustomePlayerAns playerAns) {
        View view = LayoutInflater.from(context).inflate(R.layout.check_home_work_option_layout, null);
        final TextView option = (TextView) view.findViewById(R.id.option);
        option.setText(optionText);
        optionList.add(option);

        if (playerAns != null) {
            if (playerAns.getAns().contains(optionText)) {
                if (multipleChiceAns.getUserAns() != null) {
                    multipleChiceAns.setUserAns(multipleChiceAns.getUserAns() + option.getText().toString());
                } else {
                    multipleChiceAns.setUserAns(option.getText().toString());
                }
                option.setBackgroundResource(R.drawable.checked_small_box_home_work);
            }
        }

        option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return view;
    }

    private boolean compareFillInAns(String teacherAns, String studentAns) { // siddhiinfosoft
        try {
            boolean brt = false;

            teacherAns = teacherAns.replace(" ", "");
            studentAns = studentAns.replace(" ", "");

            teacherAns = teacherAns.replace("-", "op_minus");
            studentAns = studentAns.replace("-", "op_minus");

            if (!(teacherAns.contains("#"))) { // for old keyboard

                String str = studentAns.replace("text#", "");

                brt = teacherAns.replace(",", "").equalsIgnoreCase(str.replace(",", ""));
                return brt;
            }

            try {
                String stsr = teacherAns.substring(teacherAns.length() - 4);

                if (stsr.equals("@op#")) {
                    teacherAns = teacherAns.substring(0, (teacherAns.length() - 4));
                }

            } catch (Exception e) {

            }

            brt = teacherAns.replace(",", "").equalsIgnoreCase(studentAns.replace(",", ""));

            if (brt) {
                return brt;
            } else {

                brt = this.comparereversestring(teacherAns, studentAns);

                return brt;
            }
        } catch (Exception e) {
            return false;
        }
    }

    private boolean comparereversestring(String correct_ans, String user_ans) {
        try {
            String[] str_1 = correct_ans.split("@");
            String[] str_2 = user_ans.split("@");

            Collections.reverse(Arrays.asList(str_1));

            boolean btr = false;

            for (int i = 0; i < str_1.length; i++) {

                String sstr = str_1[i];

                if (sstr.contains("op") || sstr.contains("text")) {

                    if (sstr.contains("op_equal") || sstr.contains("op_plus") || sstr.contains("op_multi") || sstr.contains("text")) {
                        btr = true;
                    } else {
                        btr = false;
                        break;
                    }

                } else {
                    btr = false;
                    break;

                }

            }

            if (btr) {

                if (Arrays.equals(str_1, str_2)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public void showKeyboard() {

        try {    // siddhiinfosoft
            this.hideDeviceKeyboard();
            if (!is_keyboard_show) {
                    is_keyboard_show = true;
                    keyboardLayout.setVisibility(RelativeLayout.VISIBLE);
                    animation = AnimationUtils.loadAnimation(context, R.anim.keyboard_show_animation);
                    keyboardLayout.setAnimation(animation);
                }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void hideKeyboard() {

        try {   // siddhiinfosoft
            if (is_keyboard_show) {
                is_keyboard_show = false;
                animation = AnimationUtils.loadAnimation(context, R.anim.keyboard_hide_animation);
                keyboardLayout.setAnimation(animation);
                keyboardLayout.setVisibility(RelativeLayout.GONE);
                this.cleasFocusFromEditText();
                ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
                ll_main_eq_sub_symbol.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String replacecharfromsign(final String str) {
        String str_final = str;

        try {

            String str_plus = "" + Html.fromHtml("&#43;");
            String str_minus = "" + Html.fromHtml("&#8722;");
            String str_equal = "" + Html.fromHtml("&#61;");
            String str_multi = "" + Html.fromHtml("&#215;");
            String str_divide = "" + Html.fromHtml("&#247;");
            String str_union = "" + Html.fromHtml("&#8746;");
            String str_intersection = "" + Html.fromHtml("&#8745;");
            String str_pi = "" + Html.fromHtml("&#960;");
            String str_factorial = "" + Html.fromHtml("&#33;");
            String str_percentage = "" + Html.fromHtml("&#37;");
            String str_goe = "" + Html.fromHtml("&#8805;");
            String str_loe = "" + Html.fromHtml("&#8804;");
            String str_grthan = "" + Html.fromHtml("&#62;");
            String str_lethan = "" + Html.fromHtml("&#60;");
            String str_infinity = "" + Html.fromHtml("&#8734;");
            String str_degree = "" + Html.fromHtml("&#176;");
            String str_xbar = "" + Html.fromHtml("x&#x0304;");

            String str_integral = "" + Html.fromHtml("&#8747;");
            String str_summation = "" + Html.fromHtml("&#8721;");
            String str_alpha = "" + Html.fromHtml("&#945;");
            String str_theta = "" + Html.fromHtml("ø");
            String str_mu = "" + Html.fromHtml("&#956;");
            String str_sigma = "" + Html.fromHtml("&#963;");

            String str_beta = "" + Html.fromHtml("&#946;");
            String str_angle = "" + Html.fromHtml("&#8736;");
            String str_mangle = "" + Html.fromHtml("&#8737;");
            String str_sangle = "" + Html.fromHtml("&#8738;");
            String str_rangle = "" + Html.fromHtml("&#8735;");
            String str_triangle = "" + Html.fromHtml("&#9651;");
            String str_rectangle = "" + Html.fromHtml("&#9645;");
            String str_parallelogram = "" + Html.fromHtml("&#9649;");
            String str_perpendicular = "" + Html.fromHtml("&#8869;");
            String str_congruent = "" + Html.fromHtml("&#8773;");
            String str_similarty = "" + Html.fromHtml("&#8764;");
            String str_parallel = "" + Html.fromHtml("&#8741;");

            String str_arcm = "" + Html.fromHtml("&#8242;");
            String str_arcs = "" + Html.fromHtml("&#8243;");

            str_final = str_final.replace(str_plus, "op_plus");
            str_final = str_final.replace(str_minus, "op_minus");
            str_final = str_final.replace(str_equal, "op_equal");
            str_final = str_final.replace(str_multi, "op_multi");
            str_final = str_final.replace(str_divide, "op_divide");
            str_final = str_final.replace(str_union, "op_union");
            str_final = str_final.replace(str_intersection, "op_intersection");
            str_final = str_final.replace(str_pi, "op_pi");
            str_final = str_final.replace(str_factorial, "op_factorial");
            str_final = str_final.replace(str_percentage, "op_percentage");
            str_final = str_final.replace(str_goe, "op_goe");
            str_final = str_final.replace(str_loe, "op_loe");
            str_final = str_final.replace(str_grthan, "op_grthan");
            str_final = str_final.replace(str_lethan, "op_lethan");
            str_final = str_final.replace(str_infinity, "op_infinity");
            str_final = str_final.replace(str_degree, "op_degree");
            str_final = str_final.replace(str_xbar, "op_xbar");

            str_final = str_final.replace(str_integral, "op_integral");
            str_final = str_final.replace(str_summation, "op_summation");
            str_final = str_final.replace(str_alpha, "op_alpha");
            str_final = str_final.replace(str_theta, "op_theta");
            str_final = str_final.replace(str_mu, "op_mu");
            str_final = str_final.replace(str_sigma, "op_sigma");

            str_final = str_final.replace(str_beta, "op_beta");
            str_final = str_final.replace(str_angle, "op_angle");
            str_final = str_final.replace(str_mangle, "op_mangle");
            str_final = str_final.replace(str_sangle, "op_sangle");
            str_final = str_final.replace(str_rangle, "op_rangle");
            str_final = str_final.replace(str_triangle, "op_triangle");
            str_final = str_final.replace(str_rectangle, "op_rectangle");
            str_final = str_final.replace(str_parallelogram, "op_parallelogram");
            str_final = str_final.replace(str_perpendicular, "op_perpendicular");
            str_final = str_final.replace(str_congruent, "op_congruent");
            str_final = str_final.replace(str_similarty, "op_similarty");
            str_final = str_final.replace(str_parallel, "op_parallel");

            str_final = str_final.replace(str_arcm, "op_arcm");
            str_final = str_final.replace(str_arcs, "op_arcs");


            //by a1
            str_final = str_final.replace("-", "op_minus");


        } catch (Exception e) {
            e.printStackTrace();
        }

        return str_final;
    }

    private String replacesignfromchar(final String str, final EditTextBlink ed) {
        String str_final = str;

        try {

            String str_plus = "" + Html.fromHtml("&#43;");
            String str_minus = "" + Html.fromHtml("&#8722;");
            String str_equal = "" + Html.fromHtml("&#61;");
            String str_multi = "" + Html.fromHtml("&#215;");
            String str_divide = "" + Html.fromHtml("&#247;");
            String str_union = "" + Html.fromHtml("&#8746;");
            String str_intersection = "" + Html.fromHtml("&#8745;");
            String str_pi = "" + Html.fromHtml("&#960;");
            String str_factorial = "" + Html.fromHtml("&#33;");
            String str_percentage = "" + Html.fromHtml("&#37;");
            String str_goe = "" + Html.fromHtml("&#8805;");
            String str_loe = "" + Html.fromHtml("&#8804;");
            String str_grthan = "" + Html.fromHtml("&#62;");
            String str_lethan = "" + Html.fromHtml("&#60;");
            String str_infinity = "" + Html.fromHtml("&#8734;");
            String str_degree = "" + Html.fromHtml("&#176;");
            String str_xbar = "" + Html.fromHtml("x&#x0304;");

            String str_integral = "" + Html.fromHtml("&#8747;");
            String str_summation = "" + Html.fromHtml("&#8721;");
            String str_alpha = "" + Html.fromHtml("&#945;");
            String str_theta = "" + Html.fromHtml("ø");
            String str_mu = "" + Html.fromHtml("&#956;");
            String str_sigma = "" + Html.fromHtml("&#963;");

            String str_beta = "" + Html.fromHtml("&#946;");
            String str_angle = "" + Html.fromHtml("&#8736;");
            String str_mangle = "" + Html.fromHtml("&#8737;");
            String str_sangle = "" + Html.fromHtml("&#8738;");
            String str_rangle = "" + Html.fromHtml("&#8735;");
            String str_triangle = "" + Html.fromHtml("&#9651;");
            String str_rectangle = "" + Html.fromHtml("&#9645;");
            String str_parallelogram = "" + Html.fromHtml("&#9649;");
            String str_perpendicular = "" + Html.fromHtml("&#8869;");
            String str_congruent = "" + Html.fromHtml("&#8773;");
            String str_similarty = "" + Html.fromHtml("&#8764;");
            String str_parallel = "" + Html.fromHtml("&#8741;");

            String str_arcm = "" + Html.fromHtml("&#8242;");
            String str_arcs = "" + Html.fromHtml("&#8243;");

            str_final = str_final.replace("op_plus", str_plus);
            str_final = str_final.replace("op_minus", str_minus);
            str_final = str_final.replace("op_equal", str_equal);
            str_final = str_final.replace("op_multi", str_multi);
            str_final = str_final.replace("op_divide", str_divide);
            str_final = str_final.replace("op_union", str_union);
            str_final = str_final.replace("op_intersection", str_intersection);
            str_final = str_final.replace("op_pi", str_pi);
            str_final = str_final.replace("op_factorial", str_factorial);
            str_final = str_final.replace("op_percentage", str_percentage);
            str_final = str_final.replace("op_goe", str_goe);
            str_final = str_final.replace("op_loe", str_loe);
            str_final = str_final.replace("op_grthan", str_grthan);
            str_final = str_final.replace("op_lethan", str_lethan);
            str_final = str_final.replace("op_infinity", str_infinity);
            str_final = str_final.replace("op_degree", str_degree);
            str_final = str_final.replace("op_xbar", str_xbar);

            str_final = str_final.replace("op_alpha", str_alpha);
            str_final = str_final.replace("op_theta", str_theta);
            str_final = str_final.replace("op_mu", str_mu);
            str_final = str_final.replace("op_sigma", str_sigma);

            str_final = str_final.replace("op_beta", str_beta);
            str_final = str_final.replace("op_angle", str_angle);
            str_final = str_final.replace("op_mangle", str_mangle);
            str_final = str_final.replace("op_sangle", str_sangle);
            str_final = str_final.replace("op_rangle", str_rangle);
            str_final = str_final.replace("op_triangle", str_triangle);
            str_final = str_final.replace("op_rectangle", str_rectangle);
            str_final = str_final.replace("op_parallelogram", str_parallelogram);
            str_final = str_final.replace("op_perpendicular", str_perpendicular);
            str_final = str_final.replace("op_congruent", str_congruent);
            str_final = str_final.replace("op_similarty", str_similarty);
            str_final = str_final.replace("op_parallel", str_parallel);

            str_final = str_final.replace("op_arcm", str_arcm);
            str_final = str_final.replace("op_arcs", str_arcs);

            str_final = str_final.replace("op_integral", str_integral);
            str_final = str_final.replace("op_summation", str_summation);

            for (int i = 0; i < str_final.length(); i++) {

                String ss = String.valueOf(str_final.charAt(i));

                if (ss.equals(str_integral) || ss.equals(str_summation)) {
                    SpannableString ss1 = new SpannableString(ss);
                    ss1.setSpan(new RelativeSizeSpan(1.6f), 0, 1, 0);
                    ed.getText().insert(ed.getSelectionStart(), ss1);
                } else if (ss.equals(str_perpendicular) || ss.equals(str_parallel) ||
                        ss.equals(str_rectangle) || ss.equals(str_parallelogram)) {
                    SpannableString ss1 = new SpannableString(ss);
                    ss1.setSpan(new RelativeSizeSpan(1.2f), 0, 1, 0);
                    ed.getText().insert(ed.getSelectionStart(), ss1);
                } else if (ss.equals(str_angle) || ss.equals(str_mangle) ||
                        ss.equals(str_sangle) || ss.equals(str_rangle)) {
                    SpannableString ss1 = new SpannableString(ss);
                    ss1.setSpan(new RelativeSizeSpan(1.5f), 0, 1, 0);
                    ed.getText().insert(ed.getSelectionStart(), ss1);
                } else {
                    ed.getText().insert(ed.getSelectionStart(), ss);
                }

                if (ss.equals(str_integral)) {
                    ed.getText().insert(ed.getSelectionStart(), " ");
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return str_final;
    }


    private void setvisibilityofabc123layout(int int_layout) { // siddhiinfosoft

        if (int_layout == 1) {
            kb_ll_123.setVisibility(View.VISIBLE);
            kb_ll_abc.setVisibility(View.GONE);
        } else {
            kb_ll_123.setVisibility(View.GONE);
            kb_ll_abc.setVisibility(View.VISIBLE);
        }

    }

    protected void setanimationtoeqsymbols1() {  // siddhiinfosoft
        try {
            animation = AnimationUtils.loadAnimation(context, R.anim.symbol_hide_animation);
            hsv_keyboard_eq_symbol.setAnimation(animation);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void setanimationtoeqsymbols2() {
        try {
            animation = AnimationUtils.loadAnimation(context, R.anim.symbol_show_animation);
            hsv_keyboard_eq_symbol.setAnimation(animation);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onclkeqsmbl(View v) {

        try {

            int id_view = v.getId();
            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            if (ll_visible_view != null) {
                ll_visible_view.setVisibility(View.GONE);
            }

            switch (id_view) {

                case R.id.kb_eq_symbol_basicmath_sign1:
                case R.id.kb_eq_symbol_basicmath_sign1_arrow:
                    ll_visible_view = ll_kb_sub_sumbol_basicmath_sign1;
                    Utils.setvisibility_basicmath_subeq(ll_kb_sub_sumbol_basicmath_sign1, ll_kb_sub_sumbol_basicmath_sign2, ll_kb_sub_sumbol_basicmath_sign5);

                    break;

                case R.id.kb_eq_symbol_basicmath_sign2:
                case R.id.kb_eq_symbol_basicmath_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_basicmath_sign2;
                    Utils.setvisibility_basicmath_subeq(ll_kb_sub_sumbol_basicmath_sign2, ll_kb_sub_sumbol_basicmath_sign1, ll_kb_sub_sumbol_basicmath_sign5);

                    break;

                case R.id.kb_eq_symbol_basicmath_sign5:
                case R.id.kb_eq_symbol_basicmath_sign5_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_basicmath_sign5;
                    Utils.setvisibility_basicmath_subeq(ll_kb_sub_sumbol_basicmath_sign5, ll_kb_sub_sumbol_basicmath_sign1, ll_kb_sub_sumbol_basicmath_sign2);

                    break;

                case R.id.kb_eq_symbol_prealgebra_sign1:
                case R.id.kb_eq_symbol_prealgebra_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_prealgebra_sign1;
                    Utils.setvisibility_prealgebra_subeq(ll_kb_sub_sumbol_prealgebra_sign1, ll_kb_sub_sumbol_prealgebra_sign2,
                            ll_kb_sub_sumbol_prealgebra_sign4, ll_kb_sub_sumbol_prealgebra_sign5);

                    break;

                case R.id.kb_eq_symbol_prealgebra_sign2:
                case R.id.kb_eq_symbol_prealgebra_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_prealgebra_sign2;
                    Utils.setvisibility_prealgebra_subeq(ll_kb_sub_sumbol_prealgebra_sign2, ll_kb_sub_sumbol_prealgebra_sign1,
                            ll_kb_sub_sumbol_prealgebra_sign4, ll_kb_sub_sumbol_prealgebra_sign5);

                    break;

                case R.id.kb_eq_symbol_prealgebra_sign4:
                case R.id.kb_eq_symbol_prealgebra_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_prealgebra_sign4;
                    Utils.setvisibility_prealgebra_subeq(ll_kb_sub_sumbol_prealgebra_sign4, ll_kb_sub_sumbol_prealgebra_sign2,
                            ll_kb_sub_sumbol_prealgebra_sign1, ll_kb_sub_sumbol_prealgebra_sign5);


                    break;

                case R.id.kb_eq_symbol_prealgebra_sign5:
                case R.id.kb_eq_symbol_prealgebra_sign5_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_prealgebra_sign5;
                    Utils.setvisibility_prealgebra_subeq(ll_kb_sub_sumbol_prealgebra_sign5, ll_kb_sub_sumbol_prealgebra_sign1, ll_kb_sub_sumbol_prealgebra_sign2,
                            ll_kb_sub_sumbol_prealgebra_sign4);


                    break;

                case R.id.kb_eq_symbol_algebra_sign1:
                case R.id.kb_eq_symbol_algebra_sign1_arrow:
                case R.id.kb_eq_symbol_geometry_sign1:
                case R.id.kb_eq_symbol_geometry_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_algebra_sign1;
                    Utils.setvisibility_algebra_subeq(ll_kb_sub_sumbol_algebra_sign1, ll_kb_sub_sumbol_algebra_sign2,
                            ll_kb_sub_sumbol_algebra_sign4, ll_kb_sub_sumbol_algebra_sign5,
                            ll_kb_sub_sumbol_algebra_sign8);


                    break;

                case R.id.kb_eq_symbol_algebra_sign2:
                case R.id.kb_eq_symbol_algebra_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_algebra_sign2;
                    Utils.setvisibility_algebra_subeq(ll_kb_sub_sumbol_algebra_sign2, ll_kb_sub_sumbol_algebra_sign1,
                            ll_kb_sub_sumbol_algebra_sign4, ll_kb_sub_sumbol_algebra_sign5,
                            ll_kb_sub_sumbol_algebra_sign8);

                    break;

                case R.id.kb_eq_symbol_algebra_sign4:
                case R.id.kb_eq_symbol_algebra_sign4_arrow:
                case R.id.kb_eq_symbol_geometry_sign3:
                case R.id.kb_eq_symbol_geometry_sign3_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_algebra_sign4;
                    Utils.setvisibility_algebra_subeq(ll_kb_sub_sumbol_algebra_sign4, ll_kb_sub_sumbol_algebra_sign2,
                            ll_kb_sub_sumbol_algebra_sign1,
                            ll_kb_sub_sumbol_algebra_sign5,
                            ll_kb_sub_sumbol_algebra_sign8);

                    break;

                case R.id.kb_eq_symbol_algebra_sign5:
                case R.id.kb_eq_symbol_algebra_sign5_arrow:
                case R.id.kb_eq_symbol_geometry_sign4:
                case R.id.kb_eq_symbol_geometry_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_algebra_sign5;
                    Utils.setvisibility_algebra_subeq(ll_kb_sub_sumbol_algebra_sign5, ll_kb_sub_sumbol_algebra_sign4,
                            ll_kb_sub_sumbol_algebra_sign2, ll_kb_sub_sumbol_algebra_sign1,
                            ll_kb_sub_sumbol_algebra_sign8);

                    break;


                case R.id.kb_eq_symbol_algebra_sign8:
                case R.id.kb_eq_symbol_algebra_sign8_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_algebra_sign8;
                    Utils.setvisibility_algebra_subeq(ll_kb_sub_sumbol_algebra_sign8, ll_kb_sub_sumbol_algebra_sign5, ll_kb_sub_sumbol_algebra_sign4,
                            ll_kb_sub_sumbol_algebra_sign2, ll_kb_sub_sumbol_algebra_sign1
                    );

                    break;

                case R.id.kb_eq_symbol_trignometry_sign1:
                case R.id.kb_eq_symbol_trignometry_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign1;
                    ll_kb_sub_sumbol_trignometry_sign1.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign2:
                case R.id.kb_eq_symbol_trignometry_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign2;
                    ll_kb_sub_sumbol_trignometry_sign2.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign3:

                    break;

                case R.id.kb_eq_symbol_trignometry_sign4:
                case R.id.kb_eq_symbol_trignometry_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign4;
                    ll_kb_sub_sumbol_trignometry_sign4.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign5:
                case R.id.kb_eq_symbol_trignometry_sign5_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign5;
                    ll_kb_sub_sumbol_trignometry_sign5.setVisibility(View.VISIBLE);

                    break;


                case R.id.kb_eq_symbol_trignometry_sign7:
                case R.id.kb_eq_symbol_trignometry_sign7_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign7;
                    ll_kb_sub_sumbol_trignometry_sign7.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign8:
                case R.id.kb_eq_symbol_trignometry_sign8_arrow:
                case R.id.kb_eq_symbol_geometry_sign8:
                case R.id.kb_eq_symbol_geometry_sign8_arrow:
                case R.id.kb_eq_symbol_calculus_sign11:
                case R.id.kb_eq_symbol_calculus_sign11_arrow:
                case R.id.kb_eq_symbol_precalculus_sign10:
                case R.id.kb_eq_symbol_precalculus_sign10_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign8;
                    ll_kb_sub_sumbol_trignometry_sign8.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign9:
                case R.id.kb_eq_symbol_trignometry_sign9_arrow:
                case R.id.kb_eq_symbol_geometry_sign9:
                case R.id.kb_eq_symbol_geometry_sign9_arrow:
                case R.id.kb_eq_symbol_calculus_sign12:
                case R.id.kb_eq_symbol_calculus_sign12_arrow:
                case R.id.kb_eq_symbol_precalculus_sign11:
                case R.id.kb_eq_symbol_precalculus_sign11_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign9;
                    ll_kb_sub_sumbol_trignometry_sign9.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign10:
                case R.id.kb_eq_symbol_trignometry_sign10_arrow:
                case R.id.kb_eq_symbol_geometry_sign10:
                case R.id.kb_eq_symbol_geometry_sign10_arrow:
                case R.id.kb_eq_symbol_calculus_sign13:
                case R.id.kb_eq_symbol_calculus_sign13_arrow:
                case R.id.kb_eq_symbol_precalculus_sign12:
                case R.id.kb_eq_symbol_precalculus_sign12_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign10;
                    ll_kb_sub_sumbol_trignometry_sign10.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign11:
                case R.id.kb_eq_symbol_trignometry_sign11_arrow:
                case R.id.kb_eq_symbol_geometry_sign11:
                case R.id.kb_eq_symbol_geometry_sign11_arrow:
                case R.id.kb_eq_symbol_calculus_sign14:
                case R.id.kb_eq_symbol_calculus_sign14_arrow:
                case R.id.kb_eq_symbol_precalculus_sign13:
                case R.id.kb_eq_symbol_precalculus_sign13_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign11;
                    ll_kb_sub_sumbol_trignometry_sign11.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign12:
                case R.id.kb_eq_symbol_trignometry_sign12_arrow:
                case R.id.kb_eq_symbol_geometry_sign12:
                case R.id.kb_eq_symbol_geometry_sign12_arrow:
                case R.id.kb_eq_symbol_calculus_sign15:
                case R.id.kb_eq_symbol_calculus_sign15_arrow:
                case R.id.kb_eq_symbol_precalculus_sign14:
                case R.id.kb_eq_symbol_precalculus_sign14_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign12;
                    ll_kb_sub_sumbol_trignometry_sign12.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_trignometry_sign13:
                case R.id.kb_eq_symbol_trignometry_sign13_arrow:
                case R.id.kb_eq_symbol_geometry_sign13:
                case R.id.kb_eq_symbol_geometry_sign13_arrow:
                case R.id.kb_eq_symbol_calculus_sign16:
                case R.id.kb_eq_symbol_calculus_sign16_arrow:
                case R.id.kb_eq_symbol_precalculus_sign15:
                case R.id.kb_eq_symbol_precalculus_sign15_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_trignometry_sign13;
                    ll_kb_sub_sumbol_trignometry_sign13.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_precalculus_sign1:
                case R.id.kb_eq_symbol_precalculus_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_precalculus_sign1;
                    ll_kb_sub_sumbol_precalculus_sign1.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_precalculus_sign2:
                case R.id.kb_eq_symbol_precalculus_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_precalculus_sign2;
                    ll_kb_sub_sumbol_precalculus_sign2.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_precalculus_sign3:

                    break;

                case R.id.kb_eq_symbol_precalculus_sign4:
                case R.id.kb_eq_symbol_precalculus_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_precalculus_sign4;
                    ll_kb_sub_sumbol_precalculus_sign4.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_precalculus_sign5:
                case R.id.kb_eq_symbol_precalculus_sign5_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_precalculus_sign5;
                    ll_kb_sub_sumbol_precalculus_sign5.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_precalculus_sign6:

                    break;

                case R.id.kb_eq_symbol_precalculus_sign7:

                    break;

                case R.id.kb_eq_symbol_precalculus_sign9:
                case R.id.kb_eq_symbol_precalculus_sign9_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_precalculus_sign9;
                    ll_kb_sub_sumbol_precalculus_sign9.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_geometry_sign2:
                case R.id.kb_eq_symbol_geometry_sign2_arrow:

                    ll_visible_view = ll_kb_sub_symbol_geometry_sign2;
                    ll_kb_sub_symbol_geometry_sign2.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_geometry_sign5:
                case R.id.kb_eq_symbol_geometry_sign5_arrow:

                    ll_visible_view = ll_kb_sub_symbol_geometry_sign5;
                    ll_kb_sub_symbol_geometry_sign5.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_geometry_sign6:
                case R.id.kb_eq_symbol_geometry_sign6_arrow:

                    ll_visible_view = ll_kb_sub_symbol_geometry_sign6;
                    ll_kb_sub_symbol_geometry_sign6.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_geometry_sign7:
                case R.id.kb_eq_symbol_geometry_sign7_arrow:

                    ll_visible_view = ll_kb_sub_symbol_geometry_sign7;
                    ll_kb_sub_symbol_geometry_sign7.setVisibility(View.VISIBLE);

                    break;


                case R.id.kb_eq_symbol_calculus_sign1:
                case R.id.kb_eq_symbol_calculus_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign1;
                    ll_kb_sub_sumbol_calculus_sign1.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_calculus_sign2:
                case R.id.kb_eq_symbol_calculus_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign2;
                    ll_kb_sub_sumbol_calculus_sign2.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_calculus_sign3:

                    break;

                case R.id.kb_eq_symbol_calculus_sign4:
                case R.id.kb_eq_symbol_calculus_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign4;
                    ll_kb_sub_sumbol_calculus_sign4.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_calculus_sign5:
                case R.id.kb_eq_symbol_calculus_sign5_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign5;
                    ll_kb_sub_sumbol_calculus_sign5.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_calculus_sign6:

                    break;

                case R.id.kb_eq_symbol_calculus_sign7:

                    break;

                case R.id.kb_eq_symbol_calculus_sign10:
                case R.id.kb_eq_symbol_calculus_sign10_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_calculus_sign10;
                    ll_kb_sub_sumbol_calculus_sign10.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_statistics_sign1:
                case R.id.kb_eq_symbol_statistics_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_statistics_sign1;
                    ll_kb_sub_sumbol_statistics_sign1.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_statistics_sign2:

                    break;

                case R.id.kb_eq_symbol_statistics_sign3:
                case R.id.kb_eq_symbol_statistics_sign3_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_statistics_sign3;
                    ll_kb_sub_sumbol_statistics_sign3.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_statistics_sign4:
                case R.id.kb_eq_symbol_statistics_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_statistics_sign4;
                    ll_kb_sub_sumbol_statistics_sign4.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_statistics_sign5:

                    break;

                case R.id.kb_eq_symbol_statistics_sign6:
                case R.id.kb_eq_symbol_statistics_sign6_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_statistics_sign6;
                    ll_kb_sub_sumbol_statistics_sign6.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_statistics_sign7:
                case R.id.kb_eq_symbol_statistics_sign7_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_statistics_sign7;
                    ll_kb_sub_sumbol_statistics_sign7.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_finitemath_sign1:
                case R.id.kb_eq_symbol_finitemath_sign1_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_finitemath_sign1;
                    ll_kb_sub_sumbol_finitemath_sign1.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_finitemath_sign2:
                case R.id.kb_eq_symbol_finitemath_sign2_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_finitemath_sign2;
                    ll_kb_sub_sumbol_finitemath_sign2.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_finitemath_sign3:

                    break;

                case R.id.kb_eq_symbol_finitemath_sign4:
                case R.id.kb_eq_symbol_finitemath_sign4_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_finitemath_sign4;
                    ll_kb_sub_sumbol_finitemath_sign4.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_finitemath_sign5:
                case R.id.kb_eq_symbol_finitemath_sign5_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_finitemath_sign5;
                    ll_kb_sub_sumbol_finitemath_sign5.setVisibility(View.VISIBLE);

                    break;

                case R.id.kb_eq_symbol_finitemath_sign8:
                case R.id.kb_eq_symbol_finitemath_sign8_arrow:

                    ll_visible_view = ll_kb_sub_sumbol_finitemath_sign8;
                    ll_kb_sub_sumbol_finitemath_sign8.setVisibility(View.VISIBLE);

                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onkbclick(View v) {
        try {


            int id_view = v.getId();

            switch (id_view) {

                case R.id.btn_eq_name_pre_scroll:

                    this.callscrollmethod(1);

                    break;

                case R.id.btn_eq_name_next_scroll:

                    this.callscrollmethod(2);

                    break;

                case R.id.btn_eq_symbol_pre_scroll:

                    this.callscrollmethod(3);

                    break;

                case R.id.btn_eq_symbol_next_scroll:

                    this.callscrollmethod(4);

                    break;

                case R.id.kb_btn_abc_close:

                    hideKeyboard();

                    break;

                case R.id.kb_btn_123_close:

                    hideKeyboard();

                    break;

                case R.id.kb_btn_abc:

                    setvisibilityofabc123layout(2);

                    break;

                case R.id.kb_btn_123:

                    setvisibilityofabc123layout(1);

                    break;

                case R.id.kb_btn_abc_case:

                    this.changecasevalue();

                    break;

                case R.id.kb_tv_eq_name_basicmath:

                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_basicmath, kb_tv_eq_name_geometry, kb_tv_eq_name_prealgebra, kb_tv_eq_name_algebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {


                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_basicmath, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_trignometry,
                                    kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();

                        }
                    }, 500);

                    this.setvisibility_scroll_btn(false);

                    break;

                case R.id.kb_tv_eq_name_prealgebra:

                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_prealgebra, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_algebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler2 = new Handler();
                    handler2.postDelayed(new Runnable() {
                        @Override
                        public void run() {


                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_basicmath, kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_trignometry,
                                    kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();

                        }
                    }, 500);

                    this.setvisibility_scroll_btn(false);

                    break;

                case R.id.kb_tv_eq_name_algebra:

                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_algebra, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler3 = new Handler();
                    handler3.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath, kb_ll_eq_symbol_trignometry,
                                    kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;

                case R.id.kb_tv_eq_name_geometry:

                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_geometry, kb_tv_eq_name_algebra, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler33 = new Handler();
                    handler33.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath, kb_ll_eq_symbol_trignometry,
                                    kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;

                case R.id.kb_tv_eq_name_trignometry:
                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_trignometry, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_algebra,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler4 = new Handler();
                    handler4.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_trignometry, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath,
                                    kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;

                case R.id.kb_tv_eq_name_precalculus:
                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_precalculus, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_algebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_calculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler5 = new Handler();
                    handler5.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_trignometry, kb_ll_eq_symbol_algebra,
                                    kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath,
                                    kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;

                case R.id.kb_tv_eq_name_calculus:
                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_calculus, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_algebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_statistics, kb_tv_eq_name_finitemath);

                    Handler handler6 = new Handler();
                    handler6.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_trignometry,
                                    kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath,
                                    kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;

                case R.id.kb_tv_eq_name_statistics:
                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_statistics, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_algebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_finitemath);

                    Handler handler7 = new Handler();
                    handler7.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_calculus, kb_ll_eq_symbol_precalculus,
                                    kb_ll_eq_symbol_trignometry, kb_ll_eq_symbol_algebra, kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath,
                                    kb_ll_eq_symbol_finitemath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;

                case R.id.kb_tv_eq_name_finitemath:
                    setanimationtoeqsymbols1();

                    Utils.changetextbg(kb_tv_eq_name_finitemath, kb_tv_eq_name_geometry, kb_tv_eq_name_basicmath, kb_tv_eq_name_prealgebra, kb_tv_eq_name_algebra, kb_tv_eq_name_trignometry,
                            kb_tv_eq_name_precalculus, kb_tv_eq_name_calculus, kb_tv_eq_name_statistics);

                    Handler handler8 = new Handler();
                    handler8.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Utils.setvisibility_eq_symbol(kb_ll_eq_symbol_finitemath, kb_ll_eq_symbol_geometry, kb_ll_eq_symbol_statistics, kb_ll_eq_symbol_calculus,
                                    kb_ll_eq_symbol_precalculus, kb_ll_eq_symbol_trignometry, kb_ll_eq_symbol_algebra,
                                    kb_ll_eq_symbol_prealgebra, kb_ll_eq_symbol_basicmath);
                            setanimationtoeqsymbols2();
                        }
                    }, 500);

                    this.setvisibility_scroll_btn(true);

                    break;


            }
            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onkb_abc_click(View v) { // Added By Siddhi Info Soft

        try {

            String btn_tag = (String) v.getTag();
            this.setTextToSelectedTextView(selectedTextView, btn_tag);
            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onkb_123_click(View v) { // Added By Siddhi Info Soft

        try {

            String btn_tag = String.valueOf(v.getTag());
            this.setTextToSelectedTextView(selectedTextView, btn_tag);
            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onkb_123_symbol_click(View v) { // Added By Siddhi Info Soft

        try {

            String btn_tag = String.valueOf(v.getTag());
            this.add_sign_into_box(btn_tag);
            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onkb_sub_symbol_click(View v) { // Added By Siddhi Info Soft

        try {

            String btn_tag = String.valueOf(v.getTag());

            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);

            if (btn_tag.equals("fraction") ||
                    btn_tag.equals("pointview")
                    || btn_tag.equals("lim") || btn_tag.equals("parenthesis") || btn_tag.equals("absolute") | btn_tag.equals("squareroot") || btn_tag.equals("nthroot")
                    || btn_tag.equals("super_script") || btn_tag.equals("sub_script") || btn_tag.equals("super_sub_script")
                    || btn_tag.equals("line") || btn_tag.equals("lsegment") || btn_tag.equals("ray")
                    || btn_tag.equals("arc")) {
                this.addviewtoselectedview(ll_ans_box_selected, btn_tag, "view");
            } else if (btn_tag.equals("absolute")) {
                setTextToSelectedTextView(selectedTextView, "|  |");
            } else {
                this.add_sign_into_box(btn_tag);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void add_sign_into_box(final String tag) {

        String str_text = "";
        boolean is_operator = false;

        if (tag.equals("op_plus")) {
            str_text = "" + Html.fromHtml("&#43;");
            is_operator = true;
        } else if (tag.equals("op_minus")) {
            str_text = "" + Html.fromHtml("&#8722;");
            is_operator = true;
        } else if (tag.equals("op_equal")) {
            str_text = "" + Html.fromHtml("&#61;");
            is_operator = true;
        } else if (tag.equals("op_multi")) {
            str_text = "" + Html.fromHtml("&#215;");
            is_operator = true;
        } else if (tag.equals("op_divide")) {
            str_text = "" + Html.fromHtml("&#247;");
            is_operator = true;
        } else if (tag.equals("op_union")) {
            str_text = "" + Html.fromHtml("&#8746;");
        } else if (tag.equals("op_intersection")) {
            str_text = "" + Html.fromHtml("&#8745;");
        } else if (tag.equals("op_pi")) {
            str_text = "" + Html.fromHtml("&#960;");
        } else if (tag.equals("op_factorial")) {
            str_text = "" + Html.fromHtml("&#33;");
        } else if (tag.equals("op_percentage")) {
            str_text = "" + Html.fromHtml("&#37;");
        } else if (tag.equals("op_rarrow")) {
            str_text = "" + Html.fromHtml("&#8594;");
        } else if (tag.equals("op_xbar")) {
            str_text = "" + Html.fromHtml("x&#x0304;");
        } else if (tag.equals("op_muxbar")) {
            str_text = "" + Html.fromHtml("&#956;") + "" + Html.fromHtml("x&#772;");
        } else if (tag.equals("op_sigmaxbar")) {
            str_text = "" + Html.fromHtml("&#963;") + "" + Html.fromHtml("x&#772;");
        } else if (tag.equals("op_goe")) {
            str_text = "" + Html.fromHtml("&#8805;");
            is_operator = true;
        } else if (tag.equals("op_loe")) {
            str_text = "" + Html.fromHtml("&#8804;");
            is_operator = true;
        } else if (tag.equals("op_grthan")) {
            str_text = "" + Html.fromHtml("&#62;");
            is_operator = true;
        } else if (tag.equals("op_lethan")) {
            str_text = "" + Html.fromHtml("&#60;");
            is_operator = true;
        } else if (tag.equals("op_infinity")) {
            str_text = "" + Html.fromHtml("&#8734;");
        } else if (tag.equals("op_degree")) {
            str_text = "" + Html.fromHtml("&#176;");
        } else if (tag.equals("op_integral")) {
            str_text = " " + Html.fromHtml("&#8747;");
        } else if (tag.equals("op_summation")) {
            str_text = " " + Html.fromHtml("&#8721;");
        } else if (tag.equals("op_alpha")) {
            str_text = " " + Html.fromHtml("&#945;");
            //  str_text = " "+Html.fromHtml("&#913;");
        } else if (tag.equals("op_theta")) {
            str_text = " ø";
        } else if (tag.equals("op_mu")) {
            str_text = " " + Html.fromHtml("&#956;");
        } else if (tag.equals("op_sigma")) {
            str_text = " " + Html.fromHtml("&#963;");
        } else if (tag.equals("op_beta")) {
            str_text = " " + Html.fromHtml("&#946;");
            //  str_text = " "+Html.fromHtml("&#7526;");
        } else if (tag.equals("op_angle")) {
            str_text = " " + Html.fromHtml("&#8736;");
        } else if (tag.equals("op_mangle")) {
            str_text = " " + Html.fromHtml("&#8737;");
        } else if (tag.equals("op_sangle")) {
            str_text = " " + Html.fromHtml("&#8738;");
        } else if (tag.equals("op_rangle")) {
            str_text = " " + Html.fromHtml("&#8735;");
        } else if (tag.equals("op_triangle")) {
            str_text = " " + Html.fromHtml("&#9651;");
        } else if (tag.equals("op_rectangle")) {
            str_text = " " + Html.fromHtml("&#9645;");
        } else if (tag.equals("op_parallelogram")) {
            str_text = " " + Html.fromHtml("&#9649;");
        } else if (tag.equals("op_line")) {
            str_text = " " + Html.fromHtml("&#963;");
        } else if (tag.equals("op_lsegment")) {
            str_text = " " + Html.fromHtml("&#963;");
        } else if (tag.equals("op_ray")) {
            str_text = " " + Html.fromHtml("&#963;");
        } else if (tag.equals("op_arc")) {
            str_text = " " + Html.fromHtml("&#963;");
        } else if (tag.equals("op_perpendicular")) {
            str_text = " " + Html.fromHtml("&#8869;");
        } else if (tag.equals("op_congruent")) {
            str_text = " " + Html.fromHtml("&#8773;");
        } else if (tag.equals("op_similarty")) {
            str_text = " " + Html.fromHtml("&#8764;");
        } else if (tag.equals("op_parallel")) {
            str_text = " " + Html.fromHtml("&#8741;");
        } else if (tag.equals("op_arcm")) {
            str_text = "" + Html.fromHtml("&#8242;");
        } else if (tag.equals("op_arcs")) {
            str_text = "" + Html.fromHtml("&#8243;");
        } else {
            str_text = tag;
        }

        if (is_operator) {
            setTextToSelectedTextView2(selectedTextView, " " + str_text + " ");
        } else {
            setTextToSelectedTextView2(selectedTextView, str_text);
        }

    }

    public void onkb_sum_symbol_click(View v) {

        try {

            ll_main_eq_sub_symbol.setVisibility(View.VISIBLE);
            ll_main_eq_sub_symbol.setVisibility(View.GONE);

            String btn_tag = String.valueOf(v.getTag());
            String str_text = "";
            if (btn_tag.equals("op_integral")) {
                str_text = " " + Html.fromHtml("&#8747;");
            } else if (btn_tag.equals("op_summation")) {
                str_text = " " + Html.fromHtml("&#8721;");
            } else if (btn_tag.equals("op_perpendicular")) {
                str_text = " " + Html.fromHtml("&#8869;");
            } else if (btn_tag.equals("op_parallel")) {
                str_text = " " + Html.fromHtml("&#8741;");
            } else if (btn_tag.equals("op_rectangle")) {
                str_text = " " + Html.fromHtml("&#9645;");
            } else if (btn_tag.equals("op_parallelogram")) {
                str_text = " " + Html.fromHtml("&#9649;");
            } else if (btn_tag.equals("op_angle")) {
                str_text = " " + Html.fromHtml("&#8736;");
            } else if (btn_tag.equals("op_mangle")) {
                str_text = " " + Html.fromHtml("&#8737;");
            } else if (btn_tag.equals("op_sangle")) {
                str_text = " " + Html.fromHtml("&#8738;");
            } else if (btn_tag.equals("op_rangle")) {
                str_text = " " + Html.fromHtml("&#8735;");
            }

            if (btn_tag.equals("op_integral") || btn_tag.equals("op_summation")) {
                SpannableString ss1 = new SpannableString(str_text);
                ss1.setSpan(new RelativeSizeSpan(1.6f), 1, 2, 0);
                selectedTextView.getText().insert(selectedTextView.getSelectionStart(), ss1);
            } else if (btn_tag.equals("op_perpendicular") || btn_tag.equals("op_parallel") ||
                    btn_tag.equals("op_rectangle") || btn_tag.equals("op_parallelogram")) {
                SpannableString ss1 = new SpannableString(str_text);
                ss1.setSpan(new RelativeSizeSpan(1.2f), 1, 2, 0);
                selectedTextView.getText().insert(selectedTextView.getSelectionStart(), ss1);
            } else if (btn_tag.equals("op_angle") || btn_tag.equals("op_mangle") ||
                    btn_tag.equals("op_sangle") || btn_tag.equals("op_rangle")) {
                SpannableString ss1 = new SpannableString(str_text);
                ss1.setSpan(new RelativeSizeSpan(1.5f), 1, 2, 0);
                selectedTextView.getText().insert(selectedTextView.getSelectionStart(), ss1);
            }

            if (btn_tag.equals("op_integral")) {
                selectedTextView.getText().insert(selectedTextView.getSelectionStart(), " ");
            }


//            SpannableString ss1=  new SpannableString(str_text);
//            ss1.setSpan(new RelativeSizeSpan(2f), 1,2, 0);
//            selectedTextView.getText().insert(selectedTextView.getSelectionStart(), ss1);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void addnewed(final LinearLayout ll_main, final String str_cut, final int pos, final int text_size) {

//        String sss = String.valueOf(ll_main.getTag(R.id.first));
//        int tag1 = Integer.valueOf(sss);
//        String tag2 = String.valueOf(ll_main.getTag(R.id.second));
//        int text_size = 22;
//
//        try {
//
//            if (tag2.equals("super_top") || tag2.equals("sub_bottom") || tag2.equals("ss_top") ||
//                    tag2.equals("ss_bottom") || tag2.equals("nth_top") || tag2.equals("lim_left") || tag2.equals("lim_right")){
//                text_size = (text_size - 3)-(tag1+1);
//            } else if(tag2.equals("frac_top") || tag2.equals("frac_bottom") || tag2.equals("sqrt_c") || tag2.equals("nth_bottom")
//                    || tag2.equals("parenthesis_center")){
//                text_size = text_size-(tag1+1);
//            }
//
//        } catch (Exception e){
//
//        }

        final LinearLayout ll_last = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
        ll_last.setTag("text");
        final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
        // ed_centre.setText(str_cut);
        this.replacesignfromchar(str_cut, ed_centre);
        ed_centre.setMinWidth(20);
        ed_centre.setTextSize(text_size);
        ll_last.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
            }
        });
        ed_centre.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
                return false;
            }
        });
        ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
                    selectedTextView = ed_centre;
                    ll_selected_view = ll_last;
                    selected_ed_2 = null;
                    ll_ans_box_selected = ll_main;
                }
            }
        });

        ll_main.addView(ll_last, (pos + 2));

        //   }

    }

    private void add_last_ed(final LinearLayout ll_main, final int textsize, final int pos) {

        String sss = String.valueOf(ll_main.getTag(R.id.first));
        int tag1 = Integer.valueOf(sss);
        String tag2 = String.valueOf(ll_main.getTag(R.id.second));
        int text_size = context.getResources().getInteger(R.integer.kb_textsize_main_int);

        try {

            if (tag2.equals("super_top") || tag2.equals("sub_bottom") || tag2.equals("ss_top") ||
                    tag2.equals("ss_bottom") || tag2.equals("nth_top") || tag2.equals("lim_left") || tag2.equals("lim_right")) {
                text_size = (text_size - 1) - (tag1 + 1);
            } else if (tag2.equals("frac_top") || tag2.equals("frac_bottom") || tag2.equals("sqrt_c") || tag2.equals("nth_bottom")
                    || tag2.equals("parenthesis_center") || tag2.equals("absolute_center")
                    || tag2.equals("line_center") || tag2.equals("lsegment_center")
                    || tag2.equals("ray_center") || tag2.equals("arc_center")) {
                text_size = text_size - (tag1 + 1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (text_size < 5) {
            text_size = 5;
        }

        final LinearLayout ll_last = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
        ll_last.setTag("text");
        final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
        ed_centre.setMinWidth(3);
        ed_centre.setMinimumWidth(3);
        ed_centre.requestLayout();
        ed_centre.setTextSize(text_size);
        ll_last.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
            }
        });
        ed_centre.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
                return false;
            }
        });
        ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    selectedTextView = ed_centre;
                    ll_selected_view = ll_last;
                    selected_ed_2 = null;
                    ll_ans_box_selected = ll_main;
                }
            }
        });
        Log.e("delete6", "1234");
        ll_main.addView(ll_last, pos);
        Log.e("delete7", "1234");
        ed_centre.requestFocus();
    }

    private void add_ed_after_delete(final LinearLayout ll_main, final int textsize, final int pos) {

        String sss = String.valueOf(ll_main.getTag(R.id.first));
        int tag1 = Integer.valueOf(sss);
        String tag2 = String.valueOf(ll_main.getTag(R.id.second));
        int text_size = context.getResources().getInteger(R.integer.kb_textsize_main_int);

        try {

            if (tag2.equals("super_top") || tag2.equals("sub_bottom") || tag2.equals("ss_top") ||
                    tag2.equals("ss_bottom") || tag2.equals("nth_top") || tag2.equals("lim_left") || tag2.equals("lim_right")) {
                text_size = (text_size - 1) - (tag1 + 1);
            } else if (tag2.equals("frac_top") || tag2.equals("frac_bottom") || tag2.equals("sqrt_c") || tag2.equals("nth_bottom")
                    || tag2.equals("parenthesis_center") || tag2.equals("absolute_center")
                    || tag2.equals("line_center") || tag2.equals("lsegment_center")
                    || tag2.equals("ray_center") || tag2.equals("arc_center")) {
                text_size = text_size - (tag1 + 1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (text_size < 5) {
            text_size = 5;
        }
        final LinearLayout ll_last = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
        ll_last.setTag("text");
        final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
        ed_centre.setMinWidth(20);
        ed_centre.setTextSize(text_size);
        ll_last.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
            }
        });
        ed_centre.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
                return false;
            }
        });
        ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                custom_ed_focus(ed_centre, ll_last, ll_main, hasFocus);
            }
        });
        //   Utils.change_edittext_bg(true, ed_centre);
        Log.e("edd2", "" + pos);
        ll_main.addView(ll_last, pos);
        ed_centre.requestFocus();

    }

    private void custom_ed_touch(final EditTextBlink ed, final LinearLayout ll_sub, final LinearLayout ll_main) {

        try {

            ed.setCursorVisible(true);
            selectedTextView = ed;
            ll_selected_view = ll_sub;
            ll_ans_box_selected = ll_main;
            selected_ed_2 = null;
            showKeyboard();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void add_inside_ed(final LinearLayout ll_main, final boolean req_focus, int text_size) {

        String tag2 = String.valueOf(ll_main.getTag(R.id.second));

        try {

            if (tag2.equals("super_top") || tag2.equals("sub_bottom") || tag2.equals("ss_top") ||
                    tag2.equals("ss_bottom") || tag2.equals("nth_top") || tag2.equals("lim_left") || tag2.equals("lim_right")) {
                // text_size = (text_size - 3)-(tag1+1);
                text_size = text_size - 3;
            } else if (tag2.equals("frac_top") || tag2.equals("frac_bottom") || tag2.equals("sqrt_c") || tag2.equals("nth_bottom")
                    || tag2.equals("parenthesis_center") || tag2.equals("absolute_center")
                    || tag2.equals("line_center") || tag2.equals("lsegment_center")
                    || tag2.equals("ray_center") || tag2.equals("arc_center")) {
                //  text_size = text_size-(tag1+1);
                text_size = text_size - 1;
            }

        } catch (Exception e) {
            text_size = 22;
        }

        Log.e("text_size", "" + text_size);
        final LinearLayout ll_last = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
        ll_last.setTag("text");
        final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
        ed_centre.setMinWidth(25);
        ed_centre.setTextSize(text_size);
        ll_last.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
            }
        });
        ed_centre.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                custom_ed_touch(ed_centre, ll_last, ll_main);
                return false;
            }
        });
        ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                custom_ed_focus(ed_centre, ll_last, ll_main, hasFocus);
            }
        });
        Utils.change_edittext_bg(true, ed_centre);
        ll_main.addView(ll_last);

        Log.e("het", "" + ll_last.getHeight());
        ll_main.requestLayout();

        if (req_focus) {
            ed_centre.requestFocus();
        }

    }

    private void custom_ed_focus(final EditTextBlink ed, final LinearLayout ll_sub, final LinearLayout ll_main, final boolean hasFocus) {

        if (!(ll_main.getTag(R.id.second).equals("main"))) {
            Utils.change_edittext_bg(hasFocus, ed);
        }

        if (hasFocus) {
            selectedTextView = ed;
            ll_selected_view = ll_sub;
            selected_ed_2 = null;
            ll_ans_box_selected = ll_main;
        }

    }

    private void addviewtoselectedview(final LinearLayout ll_ans_box, String tag, String type) {  // Added By Siddhi Info Soft
        boolean is_devide = false;
        int start_pos = 0, ed_legnth = 0, ll_sub_selected_pos = 0;
        String cut_str = "";
        String sss = String.valueOf(ll_ans_box.getTag(R.id.first));
        int tag_main_ll = Integer.valueOf(sss);
        boolean need_delete = false;
        int text_size = context.getResources().getInteger(R.integer.kb_textsize_main_int);

        try {
            DisplayMetrics metrics;
            metrics = getApplicationContext().getResources().getDisplayMetrics();
            float Textsize = selectedTextView.getTextSize() / metrics.density;
            text_size = Math.round(Textsize);
            Log.e("text_size", "" + text_size);
            if (text_size < 5) {
                text_size = 5;
            }

        } catch (Exception e) {
            text_size = 16;
        }

        try {

            start_pos = selectedTextView.getSelectionStart();
            ed_legnth = selectedTextView.getText().toString().length();

            try {
                ll_sub_selected_pos = ll_ans_box_selected.indexOfChild(ll_selected_view);
            } catch (Exception e) {
                ll_sub_selected_pos = 0;
            }

            try {
                if (ll_sub_selected_pos == -1) {
                    ll_sub_selected_pos = 0;
                }

                if (start_pos != 0 && start_pos < ed_legnth) {
                    cut_str = selectedTextView.getText().toString();
                    cut_str = cut_str.substring(start_pos, ed_legnth);
                    is_devide = true;
                    Log.e("math_ed_pos", cut_str + "" + ll_sub_selected_pos);
                    selectedTextView.getText().delete(start_pos, ed_legnth);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                if (selectedTextView.getText().toString().trim().length() == 0) {
                    need_delete = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {

                final LinearLayout ll_last = (LinearLayout) ll_ans_box.getChildAt((ll_ans_box.getChildCount() - 1));
                String str_tag = String.valueOf(ll_last.getTag());
                // Log.e("tagg",str_tag);
                if (str_tag.equals("text")) {
                    final EditTextBlink ed_last = (EditTextBlink) ll_last.getChildAt(0);
                    if (ed_last.getText().toString().trim().length() == 0) {

                        try {
                            if (ll_ans_box.getChildCount() > 1) {
                                LinearLayout ll_last_2 = (LinearLayout) ll_ans_box.getChildAt((ll_ans_box.getChildCount() - 2));
                                String str_tag_2 = String.valueOf(ll_last_2.getTag());
                                if (str_tag_2.equals("text")) {
                                    ll_ans_box.removeView(ll_last);
                                } else {
                                    ed_last.setMinWidth(4);
                                }
                            } else {
                                ed_last.setMinWidth(4);
                            }
                        } catch (Exception e) {
                        }
                    } else {
                        //  Log.e("tagg12", str_tag);
                        ed_last.setMinWidth(4);
                        ed_last.setMinimumWidth(4);
                        ed_last.requestLayout();
                        //  Log.e("tagg123", str_tag);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (type.equals("view")) {
                selected_ed_2 = null;
                if (tag.equals("super_script")) {

                    final LinearLayout ll_super_script = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_new_view_superscript, null);
                    ll_super_script.setTag("super_script");
                    final LinearLayout ll_top_superscript = (LinearLayout) ll_super_script.findViewById(R.id.ll_top_superscript);
                    ll_top_superscript.setTag(R.id.first, (tag_main_ll + 1));
                    ll_top_superscript.setTag(R.id.second, "super_top");

                    add_inside_ed(ll_top_superscript, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_super_script, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_super_script, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("sub_script")) {

                    final LinearLayout ll_sub_script = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_new_view_subscript, null);
                    ll_sub_script.setTag("sub_script");
                    final LinearLayout ll_bottom_subscript = (LinearLayout) ll_sub_script.findViewById(R.id.ll_bottom_subscript);
                    ll_bottom_subscript.setTag(R.id.first, (tag_main_ll + 1));
                    ll_bottom_subscript.setTag(R.id.second, "sub_bottom");
                    add_inside_ed(ll_bottom_subscript, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_sub_script, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_sub_script, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("super_sub_script")) {

                    final LinearLayout ll_super_sub_script = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_new_view_super_sub_script, null);
                    ll_super_sub_script.setTag("super_sub_script");

                    final LinearLayout ll_top_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_top_super_sub_script);
                    final LinearLayout ll_bottom_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_bottom_super_sub_script);
                    ll_top_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                    ll_bottom_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                    ll_top_super_sub_script.setTag(R.id.second, "ss_top");
                    ll_bottom_super_sub_script.setTag(R.id.second, "ss_bottom");
                    add_inside_ed(ll_top_super_sub_script, true, text_size);
                    add_inside_ed(ll_bottom_super_sub_script, false, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_super_sub_script, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_super_sub_script, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("fraction")) {

                    final LinearLayout ll_fraction = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_view_fraction, null);
                    final LinearLayout ll_fraction_top = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_top);
                    final LinearLayout ll_fraction_bottom = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_bottom);
                    ll_fraction_top.setTag(R.id.first, (tag_main_ll + 1));
                    ll_fraction_bottom.setTag(R.id.first, (tag_main_ll + 1));
                    ll_fraction_top.setTag(R.id.second, "frac_top");
                    ll_fraction_bottom.setTag(R.id.second, "frac_bottom");
                    add_inside_ed(ll_fraction_top, true, text_size);
                    add_inside_ed(ll_fraction_bottom, false, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_fraction, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_fraction, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }

                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("squareroot")) {

                    final LinearLayout ll_squareroot = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_view_square_root, null);
                    final LinearLayout ll_center_square_root = (LinearLayout) ll_squareroot.findViewById(R.id.ll_center_square_root);
                    ll_center_square_root.setTag(R.id.first, (tag_main_ll + 1));
                    ll_center_square_root.setTag(R.id.second, "sqrt_c");
                    add_inside_ed(ll_center_square_root, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_squareroot, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_squareroot, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("nthroot")) {

                    final LinearLayout ll_nthroot = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_view_nth_root, null);

                    final LinearLayout ll_top_nth_root = (LinearLayout) ll_nthroot.findViewById(R.id.ll_top_nth_root);
                    final LinearLayout ll_center_nth_root = (LinearLayout) ll_nthroot.findViewById(R.id.ll_center_nth_root);
                    final ImageView img = (ImageView) ll_nthroot.findViewById(R.id.img);
                    ll_top_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                    ll_center_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                    ll_top_nth_root.setTag(R.id.second, "nth_top");
                    ll_center_nth_root.setTag(R.id.second, "nth_bottom");
                    add_inside_ed(ll_top_nth_root, true, text_size);
                    add_inside_ed(ll_center_nth_root, false, text_size);
                    img.getLayoutParams().height = ll_center_nth_root.getHeight();
                    img.requestLayout();
                    ll_center_nth_root.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                        @Override
                        public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                            Log.e("het", "" + v.getHeight());
                            img.getLayoutParams().height = v.getHeight();
                            img.requestLayout();
                        }
                    });

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_nthroot, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_nthroot, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("lim")) {

                    final LinearLayout ll_lim = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_new_view_lim, null);
                    final TextView tv_text = (TextView) ll_lim.findViewById(R.id.tv_text);
                    tv_text.setText(Html.fromHtml("&#8594;"));
                    final LinearLayout ll_lim_left = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_left);
                    final LinearLayout ll_lim_right = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_right);
                    ll_lim_left.setTag(R.id.first, (tag_main_ll + 1));
                    ll_lim_right.setTag(R.id.first, (tag_main_ll + 1));
                    ll_lim_left.setTag(R.id.second, "lim_left");
                    ll_lim_right.setTag(R.id.second, "lim_right");
                    add_inside_ed(ll_lim_left, true, text_size);
                    add_inside_ed(ll_lim_right, false, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_lim, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_lim, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }

                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("parenthesis")) {

                    final LinearLayout ll_parenthesis = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_view_parenthesis, null);
                    ll_parenthesis.setTag("parenthesis");
                    final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_parenthesis_center);
                    ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                    ll_parenthesis_center.setTag(R.id.second, "parenthesis_center");
                    add_inside_ed(ll_parenthesis_center, true, text_size);
                    final ImageView img_left = (ImageView) ll_parenthesis.findViewById(R.id.img_left);
                    final ImageView img_rht = (ImageView) ll_parenthesis.findViewById(R.id.img_rht);

                    img_left.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final LinearLayout lll = (LinearLayout) ll_parenthesis_center.getChildAt(0);
                            if (lll.getTag().equals("text")) {
                                final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                edd.setSelection(0);
                                edd.requestFocus();
                            } else {
                                add_last_ed(ll_parenthesis_center, 22, 0);
                            }
                        }
                    });

                    img_rht.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final LinearLayout lll = (LinearLayout) ll_parenthesis_center.getChildAt((ll_parenthesis_center.getChildCount() - 1));
                            if (lll.getTag().equals("text")) {
                                final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                edd.setSelection(edd.getText().toString().length());
                                edd.requestFocus();
                            } else {
                                add_last_ed(ll_parenthesis_center, 22, (ll_parenthesis_center.getChildCount()));
                            }
                        }
                    });

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_parenthesis, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_parenthesis, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("absolute")) {

                    final LinearLayout ll_parenthesis = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_view_absolute, null);
                    ll_parenthesis.setTag("absolute");
                    final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_absolute_center);
                    ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                    ll_parenthesis_center.setTag(R.id.second, "absolute_center");
                    add_inside_ed(ll_parenthesis_center, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_parenthesis, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_parenthesis, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                } else if (tag.equals("line") || tag.equals("lsegment")
                        || tag.equals("ray") || tag.equals("arc")) {

                    final LinearLayout ll_parenthesis = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_view_lsegment, null);

                    final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_lsegment_center);
                    final ImageView img_top = (ImageView) ll_parenthesis.findViewById(R.id.img_top);
                    ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                    if (tag.equals("line")) {
                        img_top.setImageResource(R.drawable.line_icon);
                        ll_parenthesis.setTag("line");
                        ll_parenthesis_center.setTag(R.id.second, "line_center");
                    } else if (tag.equals("lsegment")) {
                        img_top.setImageResource(R.drawable.line_segment);
                        ll_parenthesis.setTag("lsegment");
                        ll_parenthesis_center.setTag(R.id.second, "lsegment_center");
                    } else if (tag.equals("ray")) {
                        img_top.setImageResource(R.drawable.ray_icon);
                        ll_parenthesis.setTag("ray");
                        ll_parenthesis_center.setTag(R.id.second, "ray_center");
                    } else if (tag.equals("arc")) {
                        img_top.setImageResource(R.drawable.arc_icon);
                        ll_parenthesis.setTag("arc");
                        ll_parenthesis_center.setTag(R.id.second, "arc_center");
                    }

                    add_inside_ed(ll_parenthesis_center, true, text_size);

                    if (start_pos == 0 && !need_delete) {
                        ll_ans_box.addView(ll_parenthesis, ll_sub_selected_pos);
                    } else {
                        ll_ans_box.addView(ll_parenthesis, (ll_sub_selected_pos + 1));
                    }

                    if (is_devide) {
                        addnewed(ll_ans_box, cut_str, ll_sub_selected_pos, text_size);
                    }
                    if (need_delete) {
                        ll_ans_box.removeViewAt(ll_sub_selected_pos);
                    }

                }
            }

            LinearLayout ll_last_2 = (LinearLayout) ll_ans_box.getChildAt((ll_ans_box.getChildCount() - 1));
            String str_tag = String.valueOf(ll_last_2.getTag());
            if (str_tag.equals("text")) {
                EditTextBlink ed_last = (EditTextBlink) ll_last_2.getChildAt(0);
                ed_last.setMinWidth(5);

            } else {

                final LinearLayout ll_last = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                ll_last.setTag("text");
                final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                ed_centre.setTextSize(text_size);
                ed_centre.setMinWidth(4);
                ed_centre.setMinimumWidth(4);
                ed_centre.requestLayout();
                ll_last.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ed_centre.setCursorVisible(true);
                        selectedTextView = ed_centre;
                        ll_selected_view = ll_last;
                        ll_ans_box_selected = ll_ans_box;
                        selected_ed_2 = null;
                        showKeyboard();
                    }
                });
                ed_centre.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        ed_centre.setCursorVisible(true);
                        selectedTextView = ed_centre;
                        ll_selected_view = ll_last;
                        ll_ans_box_selected = ll_ans_box;
                        selected_ed_2 = null;
                        showKeyboard();
                        return false;
                    }
                });
                ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            selectedTextView = ed_centre;
                            ll_selected_view = ll_last;
                            selected_ed_2 = null;
                            ll_ans_box_selected = ll_ans_box;
                        }
                    }
                });

                ll_ans_box.addView(ll_last);

            }

        } catch (Exception e) {

        }
    }

    public void setstudentansboxsub(final String str_ans, final LinearLayout ll_ans_box) {

        String[] str_1 = str_ans.split(ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first));

        String sss = String.valueOf(ll_ans_box.getTag(R.id.first));
        int tag_main_ll = Integer.valueOf(sss);

        for (int i = 0; i < str_1.length; i++) {

            String str_1_value = str_1[i];

            if (str_1_value.startsWith("text")) {

                str_1_value = str_1_value.substring(5);

                String sss1 = String.valueOf(ll_ans_box.getTag(R.id.first));
                int tag1 = Integer.valueOf(sss1);
                final String tag2 = String.valueOf(ll_ans_box.getTag(R.id.second));
                int text_size = context.getResources().getInteger(R.integer.kb_textsize_main_int);

                try {

                    if (tag2.equals("super_top") || tag2.equals("sub_bottom") || tag2.equals("ss_top") ||
                            tag2.equals("ss_bottom") || tag2.equals("nth_top") || tag2.equals("lim_left") || tag2.equals("lim_right")) {
                        text_size = (text_size - 1) - (tag1 + 1);
                    } else if (tag2.equals("frac_top") || tag2.equals("frac_bottom") || tag2.equals("sqrt_c") || tag2.equals("nth_bottom")
                            || tag2.equals("parenthesis_center") || tag2.equals("absolute_center")
                            || tag2.equals("line_center") || tag2.equals("lsegment_center")
                            || tag2.equals("ray_center") || tag2.equals("arc_center")) {
                        text_size = text_size - (tag1 + 1);
                    }

                } catch (Exception e) {

                }

                final LinearLayout ll_last = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                ll_last.setTag("text");
                final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                this.replacesignfromchar(str_1_value, ed_centre);
                ed_centre.setMinWidth(20);
                ed_centre.setTextSize(text_size);
                ll_last.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        custom_ed_touch(ed_centre, ll_last, ll_ans_box);
                    }
                });
                ed_centre.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        custom_ed_touch(ed_centre, ll_last, ll_ans_box);
                        return false;
                    }
                });
                ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (!(tag2.equals("main"))) {
                            Utils.change_edittext_bg(hasFocus, ed_centre);
                        }
                        if (hasFocus) {
                            selectedTextView = ed_centre;
                            ll_selected_view = ll_last;
                            ll_ans_box_selected = ll_ans_box;
                        }
                    }
                });

                ll_ans_box.addView(ll_last);

            }
            if (str_1_value.startsWith("fraction")) {

                str_1_value = str_1_value.substring(8);

                final LinearLayout ll_fraction = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_view_fraction, null);
                final LinearLayout ll_fraction_top = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_top);
                final LinearLayout ll_fraction_bottom = (LinearLayout) ll_fraction.findViewById(R.id.ll_fraction_bottom);
                ll_fraction_top.setTag(R.id.first, (tag_main_ll + 1));
                ll_fraction_bottom.setTag(R.id.first, (tag_main_ll + 1));
                ll_fraction_top.setTag(R.id.second, "frac_top");
                ll_fraction_bottom.setTag(R.id.second, "frac_bottom");

                final String[] str_2 = str_1_value.split("frac_btm_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_fraction);

                this.setstudentansboxsub(str_2[0], ll_fraction_top);
                this.setstudentansboxsub(str_2[1], ll_fraction_bottom);

            } else if (str_1_value.startsWith("super_script")) {
                str_1_value = str_1_value.substring(12);

                final LinearLayout ll_super_script = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_new_view_superscript, null);
                final LinearLayout ll_top_superscript = (LinearLayout) ll_super_script.findViewById(R.id.ll_top_superscript);
                ll_top_superscript.setTag(R.id.first, (tag_main_ll + 1));
                ll_top_superscript.setTag(R.id.second, "super_top");

                ll_ans_box.addView(ll_super_script);

                this.setstudentansboxsub(str_1_value, ll_top_superscript);

            } else if (str_1_value.startsWith("sub_script")) {
                str_1_value = str_1_value.substring(10);

                final LinearLayout ll_super_script = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_new_view_subscript, null);
                final LinearLayout ll_bottom_subscript = (LinearLayout) ll_super_script.findViewById(R.id.ll_bottom_subscript);
                ll_bottom_subscript.setTag(R.id.first, (tag_main_ll + 1));
                ll_bottom_subscript.setTag(R.id.second, "sub_bottom");

                ll_ans_box.addView(ll_super_script);

                this.setstudentansboxsub(str_1_value, ll_bottom_subscript);

            } else if (str_1_value.startsWith("super_sub_script")) {

                str_1_value = str_1_value.substring(16);

                final LinearLayout ll_super_sub_script = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_new_view_super_sub_script, null);
                final LinearLayout ll_top_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_top_super_sub_script);
                final LinearLayout ll_bottom_super_sub_script = (LinearLayout) ll_super_sub_script.findViewById(R.id.ll_bottom_super_sub_script);
                ll_top_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                ll_bottom_super_sub_script.setTag(R.id.first, (tag_main_ll + 1));
                ll_top_super_sub_script.setTag(R.id.second, "ss_top");
                ll_bottom_super_sub_script.setTag(R.id.second, "ss_bottom");

                final String[] str_2 = str_1_value.split("ss_btm_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_super_sub_script);

                this.setstudentansboxsub(str_2[0], ll_top_super_sub_script);
                this.setstudentansboxsub(str_2[1], ll_bottom_super_sub_script);

            } else if (str_1_value.startsWith("sqrt")) {

                str_1_value = str_1_value.substring(4);
                final LinearLayout ll_sqrt = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_view_square_root, null);
                final LinearLayout ll_center_square_root = (LinearLayout) ll_sqrt.findViewById(R.id.ll_center_square_root);
                ll_center_square_root.setTag(R.id.first, (tag_main_ll + 1));
                ll_center_square_root.setTag(R.id.second, "sqrt_c");

                ll_ans_box.addView(ll_sqrt);

                this.setstudentansboxsub(str_1_value, ll_center_square_root);

            } else if (str_1_value.startsWith("nsqrt")) {

                str_1_value = str_1_value.substring(5);

                final LinearLayout ll_nsqrt = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_view_nth_root, null);
                final LinearLayout ll_top_nth_root = (LinearLayout) ll_nsqrt.findViewById(R.id.ll_top_nth_root);
                final LinearLayout ll_center_nth_root = (LinearLayout) ll_nsqrt.findViewById(R.id.ll_center_nth_root);
                final ImageView img = (ImageView) ll_nsqrt.findViewById(R.id.img);
                ll_top_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                ll_center_nth_root.setTag(R.id.first, (tag_main_ll + 1));
                ll_top_nth_root.setTag(R.id.second, "nth_top");
                ll_center_nth_root.setTag(R.id.second, "nth_bottom");

                img.getLayoutParams().height = ll_center_nth_root.getHeight();
                img.requestLayout();
                ll_center_nth_root.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                    @Override
                    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                        Log.e("het", "" + v.getHeight());
                        img.getLayoutParams().height = v.getHeight();
                        img.requestLayout();
                    }
                });

                final String[] str_2 = str_1_value.split("nsqrt_center_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_nsqrt);

                this.setstudentansboxsub(str_2[0], ll_top_nth_root);
                this.setstudentansboxsub(str_2[1], ll_center_nth_root);

            } else if (str_1_value.startsWith("lim")) {

                str_1_value = str_1_value.substring(3);

                final LinearLayout ll_lim = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_new_view_lim, null);
                final TextView tv_text = (TextView) ll_lim.findViewById(R.id.tv_text);
                tv_text.setText(Html.fromHtml("&#8594;"));
                final LinearLayout ll_lim_left = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_left);
                final LinearLayout ll_lim_right = (LinearLayout) ll_lim.findViewById(R.id.ll_lim_right);
                ll_lim_left.setTag(R.id.first, (tag_main_ll + 1));
                ll_lim_right.setTag(R.id.first, (tag_main_ll + 1));
                ll_lim_left.setTag(R.id.second, "lim_left");
                ll_lim_right.setTag(R.id.second, "lim_right");

                final String[] str_2 = str_1_value.split("lim_rht_" + ll_ans_box.getTag(R.id.first));

                ll_ans_box.addView(ll_lim);

                this.setstudentansboxsub(str_2[0], ll_lim_left);
                this.setstudentansboxsub(str_2[1], ll_lim_right);

            } else if (str_1_value.startsWith("parenthesis")) {
                str_1_value = str_1_value.substring(11);

                final LinearLayout ll_parenthesis = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_view_parenthesis, null);
                final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_parenthesis_center);
                ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                ll_parenthesis_center.setTag(R.id.second, "parenthesis_center");

                final ImageView img_left = (ImageView) ll_parenthesis.findViewById(R.id.img_left);
                final ImageView img_rht = (ImageView) ll_parenthesis.findViewById(R.id.img_rht);

                img_left.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final LinearLayout lll = (LinearLayout) ll_parenthesis_center.getChildAt(0);
                        if (lll.getTag().equals("text")) {
                            final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                            edd.setSelection(0);
                            edd.requestFocus();
                        } else {
                            add_last_ed(ll_parenthesis_center, 22, 0);
                        }
                    }
                });

                img_rht.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final LinearLayout lll = (LinearLayout) ll_parenthesis_center.getChildAt((ll_parenthesis_center.getChildCount() - 1));
                        if (lll.getTag().equals("text")) {
                            final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                            edd.setSelection(edd.getText().toString().length());
                            edd.requestFocus();
                        } else {
                            add_last_ed(ll_parenthesis_center, 22, (ll_parenthesis_center.getChildCount()));
                        }
                    }
                });

                ll_ans_box.addView(ll_parenthesis);

                this.setstudentansboxsub(str_1_value, ll_parenthesis_center);

            } else if (str_1_value.startsWith("absolute")) {
                str_1_value = str_1_value.substring(8);

                final LinearLayout ll_super_script = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_view_absolute, null);
                final LinearLayout ll_parenthesis_center = (LinearLayout) ll_super_script.findViewById(R.id.ll_absolute_center);
                ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                ll_parenthesis_center.setTag(R.id.second, "absolute_center");

                ll_ans_box.addView(ll_super_script);

                this.setstudentansboxsub(str_1_value, ll_parenthesis_center);

            } else if (str_1_value.startsWith("line") || str_1_value.startsWith("lsegment") || str_1_value.startsWith("ray")
                    || str_1_value.startsWith("arc")) {

                final LinearLayout ll_parenthesis = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_view_lsegment, null);
                final LinearLayout ll_parenthesis_center = (LinearLayout) ll_parenthesis.findViewById(R.id.ll_lsegment_center);
                ll_parenthesis_center.setTag(R.id.first, (tag_main_ll + 1));
                final ImageView img_top = (ImageView) ll_parenthesis.findViewById(R.id.img_top);
                // ll_parenthesis_center.setTag(R.id.second,"absolute_center");

                if (str_1_value.startsWith("line")) {
                    img_top.setImageResource(R.drawable.line_icon);
                    str_1_value = str_1_value.substring(4);
                    ll_parenthesis.setTag("line");
                    ll_parenthesis_center.setTag(R.id.second, "line_center");
                } else if (str_1_value.startsWith("lsegment")) {
                    str_1_value = str_1_value.substring(8);
                    img_top.setImageResource(R.drawable.line_segment);
                    ll_parenthesis.setTag("lsegment");
                    ll_parenthesis_center.setTag(R.id.second, "lsegment_center");
                } else if (str_1_value.startsWith("ray")) {
                    str_1_value = str_1_value.substring(3);
                    img_top.setImageResource(R.drawable.ray_icon);
                    ll_parenthesis.setTag("ray");
                    ll_parenthesis_center.setTag(R.id.second, "ray_center");
                } else if (str_1_value.startsWith("arc")) {
                    str_1_value = str_1_value.substring(3);
                    img_top.setImageResource(R.drawable.arc_icon);
                    ll_parenthesis.setTag("arc");
                    ll_parenthesis_center.setTag(R.id.second, "arc_center");
                }

                ll_ans_box.addView(ll_parenthesis);

                this.setstudentansboxsub(str_1_value, ll_parenthesis_center);

            }

        }
    }

    public void setstudentansbox(final CustomeAns customeAns, final LinearLayout ll_ans_box, final
    CustomePlayerAns playerAns) {
        try {

            if (customeAns.getIsAnswerAvailable() == MathFriendzyHelper.NO) {

                final LinearLayout ll_last = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_view_textview, null);
                ll_last.setTag("vtext");
                final TextView tv = (TextView) ll_last.findViewById(R.id.tv);
                tv.setText(lblAnswerOnWorkAreaCreditGiven);

                tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MathFriendzyHelper.showWarningDialog(context,
                                lblAnswerOnWorkAreaCreditGiven);
                    }
                });

                ll_ans_box.addView(ll_last);

            } else {

                if (playerAns != null) {

                    String str_ans = playerAns.getAns();

                    Log.e("stdans", str_ans);

                    if (str_ans.trim().length() != 0) {
                        setstudentansboxsub(str_ans, ll_ans_box);
                    }
                }

                Log.e("stdansC", customeAns.getCorrectAns());

                boolean is_ed_last = false;

                if (ll_ans_box.getChildCount() > 0) {
                    LinearLayout lt = (LinearLayout) ll_ans_box.getChildAt(ll_ans_box.getChildCount() - 1);
                    if (lt.getTag().toString().equals("text")) {
                        is_ed_last = true;
                    } else {
                        is_ed_last = false;
                    }
                } else {
                    is_ed_last = false;
                }

                if (!is_ed_last) {

                    final LinearLayout ll_last = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                    ll_last.setTag("text");
                    final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                    ed_centre.setMinWidth(20);
                    ll_last.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            custom_ed_touch(ed_centre, ll_last, ll_ans_box);
                        }
                    });
                    ed_centre.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            custom_ed_touch(ed_centre, ll_last, ll_ans_box);
                            return false;
                        }
                    });
                    ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                selectedTextView = ed_centre;
                                ll_selected_view = ll_last;
                                ll_ans_box_selected = ll_ans_box;
                            }
                        }
                    });
                    ll_ans_box.addView(ll_last);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void onnextsubcall(final LinearLayout ll_sub) {

        if (ll_sub.getTag().toString().equals("text")) {

            EditTextBlink ed = (EditTextBlink) ll_sub.getChildAt(0);
            ed.requestFocus();
            ed.setSelection(0);

        } else if (ll_sub.getTag().toString().equals("fraction") || ll_sub.getTag().toString().equals("super_script") || ll_sub.getTag().toString().equals("super_sub_script")
                || ll_sub.getTag().toString().equals("nsqrt")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(0);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, 0);
            } else {
                this.onnextsubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("sub_script")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, 0);
            } else {
                this.onnextsubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("parenthesis") || ll_sub.getTag().toString().equals("absolute") ||
                ll_sub.getTag().toString().equals("line") || ll_sub.getTag().toString().equals("lsegment")
                || ll_sub.getTag().toString().equals("ray") || ll_sub.getTag().toString().equals("arc")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, 0);
            } else {
                this.onnextsubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("sqrt")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt(0);
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_2, 22, 0);
            } else {
                this.onnextsubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("lim")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(0);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt(0);
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_2, 22, 0);
            } else {
                this.onnextsubcall(ll_sub_1);
            }

        }

    }

    public void onnextclick() {
        CommonUtils.printLog("onnextclick");
        try {
            int cur_pos = 0, ed_legnth = 0;

            try {
                cur_pos = selectedTextView.getSelectionStart();
                ed_legnth = selectedTextView.getText().toString().length();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if ((cur_pos != ed_legnth)) {

                String str_bar_1 = "" + Html.fromHtml("&#x0304;");
                String str_bar_2 = "" + Html.fromHtml("&#772;");

                if (selectedTextView.getText().toString().length() > 1) {
                    String ss = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos));
                    if (ss.equals("x")) {
                        if ((cur_pos + 1) < ed_legnth) {
                            String ss_1 = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos + 1));
                            if (ss_1.equals(str_bar_1) || ss_1.equals(str_bar_2)) {
                                selectedTextView.setSelection((cur_pos + 2));
                            } else {
                                selectedTextView.setSelection((cur_pos + 1));
                            }
                        } else {
                            selectedTextView.setSelection((cur_pos + 1));
                        }
                    } else {
                        selectedTextView.setSelection((cur_pos + 1));
                    }
                } else {
                    selectedTextView.setSelection((cur_pos + 1));
                }

                if (is_touch_kb_next) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onnextclick();
                        }
                    }, 150);
                }

            } else {
                final LinearLayout ll_main_ll = ll_ans_box_selected;
                final LinearLayout ll_sub_ll = ll_selected_view;

                int pos_sub_ll = ll_main_ll.indexOfChild(ll_sub_ll);

                if (ll_main_ll.getChildCount() > 1 && pos_sub_ll < (ll_main_ll.getChildCount() - 1)) {

                    LinearLayout ll_sub = (LinearLayout) ll_main_ll.getChildAt(pos_sub_ll + 1);
                    onnextsubcall(ll_sub);

                } else if (pos_sub_ll == (ll_main_ll.getChildCount() - 1)) {

                    String main_tag = (String) ll_main_ll.getTag(R.id.second);

                    if (main_tag.equals("frac_top")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("ss_top")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(1);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("nth_top")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("lim_left")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("sqrt_c") || main_tag.equals("nth_bottom") || main_tag.equals("lim_right")) {

                        sub_next_sqrt_c(ll_main_ll);

                    } else if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom") ||
                            main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                            || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                            || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {

                        sub_next_frac_bottom(ll_main_ll);

                    }
                }

                if (is_touch_kb_next) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onnextclick();
                        }
                    }, 150);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sub_next_sqrt_c(final LinearLayout ll_main_ll) {
        try {

            try {

                final LinearLayout ll_mainm = (LinearLayout) ll_main_ll.getParent();
                final LinearLayout ll_main_1m = (LinearLayout) ll_mainm.getParent();
                final LinearLayout ll_main2 = (LinearLayout) ll_main_1m.getParent();
                int main_pos = ll_main2.indexOfChild(ll_main_1m);

                final String main_tag = String.valueOf(ll_main2.getTag(R.id.second));
                if (main_pos == (ll_main2.getChildCount() - 1)) {
                    add_last_ed(ll_main2, 22, main_pos + 1);
                } else if (ll_main2.getChildCount() > 1 && main_pos < (ll_main2.getChildCount() - 1)) {
                    final LinearLayout ll_sub = (LinearLayout) ll_main2.getChildAt(main_pos + 1);
                    if (!(ll_sub.getTag().equals("text"))) {
                        add_last_ed(ll_main2, 22, main_pos + 1);
                    } else {
                        onnextsubcall(ll_sub);
                    }
                } else {

                    if (main_tag.equals("sqrt_c") || main_tag.equals("nth_bottom") || main_tag.equals("lim_right")) {
                        sub_next_sqrt_c(ll_main2);
                    } else if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")
                            || main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                            || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                            || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {
                        sub_next_frac_bottom(ll_main2);
                    } else if (main_tag.equals("frac_top")) {
                        final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                        final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        final LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);
                    } else if (main_tag.equals("ss_top")) {
                        final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                        final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(1);
                        final LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);
                    } else if (main_tag.equals("nth_top")) {
                        final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                        final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        final LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
                        final LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt(0);
                        onnextsubcall(ll_sub_1);
                    } else if (main_tag.equals("lim_left")) {

                        LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                        onnextsubcall(ll_sub_1);

                    } else if (main_tag.equals("main")) {
                        add_last_ed(ll_main2, 18, ll_main2.getChildCount());
                    }
                }

            } catch (Exception e) {

            }

        } catch (Exception e) {
        }
    }

    public void sub_next_frac_bottom(final LinearLayout ll_main_ll) {
        try {
            final LinearLayout ll_mainm = (LinearLayout) ll_main_ll.getParent();
            final LinearLayout ll_main2 = (LinearLayout) ll_mainm.getParent();
            int main_pos = ll_main2.indexOfChild(ll_mainm);
            final String main_tag = String.valueOf(ll_main2.getTag(R.id.second));

            if (main_pos == (ll_main2.getChildCount() - 1)) {
                add_last_ed(ll_main2, 22, main_pos + 1);
            } else if (ll_main2.getChildCount() > 1 && main_pos < (ll_main2.getChildCount() - 1)) {
                final LinearLayout ll_sub = (LinearLayout) ll_main2.getChildAt(main_pos + 1);
                if (!(ll_sub.getTag().equals("text"))) {
                    add_last_ed(ll_main2, 22, main_pos + 1);
                } else {
                    onnextsubcall(ll_sub);
                }

            } else {
                if (main_tag.equals("sqrt_c") || main_tag.equals("nth_bottom") || main_tag.equals("lim_right")) {
                    sub_next_sqrt_c(ll_main2);
                } else if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")
                        || main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                        || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                        || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {
                    sub_next_frac_bottom(ll_main2);
                } else if (main_tag.equals("frac_top")) {
                    final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                    final LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                    onnextsubcall(ll_sub_1);
                } else if (main_tag.equals("ss_top")) {
                    final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(1);
                    final LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                    onnextsubcall(ll_sub_1);
                } else if (main_tag.equals("nth_top")) {
                    final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                    final LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
                    final LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt(0);
                    onnextsubcall(ll_sub_1);
                } else if (main_tag.equals("lim_left")) {

                    LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(2);
                    LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt(0);
                    onnextsubcall(ll_sub_1);

                } else if (main_tag.equals("main")) {
                    add_last_ed(ll_main2, 22, ll_main2.getChildCount());
                }

            }
        } catch (Exception e) {
        }
    }

    public void onpresubcall(final LinearLayout ll_sub) {

        if (ll_sub.getTag().toString().equals("text")) {

            EditTextBlink ed = (EditTextBlink) ll_sub.getChildAt(0);
            ed.setSelection(ed.getText().toString().length());
            ed.requestFocus();

        } else if (ll_sub.getTag().toString().equals("sub_script")
                ) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, ll_main_1.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("super_script")
                ) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(0);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, ll_main_1.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("fraction")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(2);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, ll_main_1.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("parenthesis") || ll_sub.getTag().toString().equals("absolute")
                || ll_sub.getTag().toString().equals("line") || ll_sub.getTag().toString().equals("lsegment")
                || ll_sub.getTag().toString().equals("ray") || ll_sub.getTag().toString().equals("arc")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, ll_main_1.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("super_sub_script")) {
            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_1, 22, ll_main_1.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("sqrt")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_2, 22, ll_main_2.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("nsqrt")) {
            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(2);
            LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(1);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_2, 22, ll_main_2.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        } else if (ll_sub.getTag().toString().equals("lim")) {

            LinearLayout ll_main_1 = (LinearLayout) ll_sub.getChildAt(1);
            LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(2);
            LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
            if (!(ll_sub_1.getTag().toString().equals("text"))) {
                add_last_ed(ll_main_2, 22, ll_main_2.getChildCount());
            } else {
                this.onpresubcall(ll_sub_1);
            }

        }

    }

    public void ondeleteclick() {

        final LinearLayout ll_main_ll = ll_ans_box_selected;
        final LinearLayout ll_sub_ll = ll_selected_view;

        try {
            int ed_pos = 0, ed_legnth = 0;
            try {
                ed_pos = selectedTextView.getSelectionStart();
                ed_legnth = selectedTextView.getText().toString().trim().length();
            } catch (Exception e) {
            }
            if (ed_pos > 0 && ed_legnth > 0) {
                String str_bar_1 = "" + Html.fromHtml("&#x0304;");
                String str_bar_2 = "" + Html.fromHtml("&#772;");
                String str_integral = "" + Html.fromHtml("&#8747;");
                if (selectedTextView.getText().toString().length() > 1) {
                    String ss = String.valueOf(selectedTextView.getText().toString().charAt(ed_pos - 1));
                    if (ss.equals(str_bar_1) || ss.equals(str_bar_2)) {

                        selectedTextView.getText().delete(ed_pos - 2, ed_pos);

                    } else if (ss.equals("x")) {
                        if (ed_pos < ed_legnth) {
                            String ss_2 = String.valueOf(selectedTextView.getText().toString().charAt(ed_pos));
                            if (ss_2.equals(str_bar_1) || ss_2.equals(str_bar_2)) {
                                selectedTextView.getText().delete(ed_pos - 1, ed_pos + 1);

                            } else {
                                selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                            }
                        } else {
                            selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                        }
                    } else if (ss.equals(" ")) {
                        try {
                            String ss2 = String.valueOf(selectedTextView.getText().toString().charAt(ed_pos - 2));
                            if (ss2.equals(str_integral)) {
                                selectedTextView.getText().delete(ed_pos - 2, ed_pos);
                            } else {
                                selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                            }
                        } catch (Exception e) {
                            selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                        }
                    } else {
                        selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                    }
                } else {
                    selectedTextView.getText().delete(ed_pos - 1, ed_pos);
                }
                try {
                    if (selectedTextView.getText().toString().trim().length() == 0) {
                        selectedTextView.setSelection(0);
                    }
                } catch (Exception e) {
                }

                if (is_touch_kb) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ondeleteclick();
                        }
                    }, 150);
                }

            } else if (ed_pos == 0 && ed_legnth == 0) {

                int pos_sub_ll = ll_main_ll.indexOfChild(ll_sub_ll);
                Log.e("delete4", "" + pos_sub_ll);

                if (pos_sub_ll != 0 && pos_sub_ll != -1) {

                    LinearLayout ll_sub = (LinearLayout) ll_main_ll.getChildAt(pos_sub_ll - 1);
                    onpresubcall(ll_sub);
                    ll_main_ll.removeViewAt(pos_sub_ll);

                } else if (pos_sub_ll == 0) {

                    String main_tag = (String) ll_main_ll.getTag(R.id.second);

                    Log.e("delete5", "" + main_tag);

                    if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                        if (ll_main_ll.getChildCount() > 1) {
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        }

                    } else if (main_tag.equals("nth_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                        if (ll_main_ll.getChildCount() > 1) {
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        }

                    } else if (main_tag.equals("lim_right")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                        if (ll_main_ll.getChildCount() > 1) {
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        }

                    } else if (main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                            || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                            || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {

                        try {
                            LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            LinearLayout ll_main2 = (LinearLayout) ll_main.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_frac_top(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {

                                if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                    sub_pre_frac_top(ll_main_ll, 2, false);
                                    ll_main2.removeViewAt(main_pos);
                                    this.add_ed_after_delete(ll_main2, 18, main_pos);
                                } else {
                                    if (main_pos != 0) {
                                        sub_pre_frac_top(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                    } else {
                                        sub_pre_frac_top(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                        try {
                                            final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                            if (lll.getTag().toString().equals("text")) {
                                                EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                edd.setMinWidth(20);
                                                edd.setMinimumWidth(20);
                                                edd.requestLayout();
                                                edd.requestFocus();
                                                if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                    Utils.change_edittext_bg(true, edd);
                                                }
                                            }
                                        } catch (Exception e) {
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("sqrt_c")) {

                        try {

                            final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            final LinearLayout ll_main1 = (LinearLayout) ll_main.getParent();
                            final LinearLayout ll_main2 = (LinearLayout) ll_main1.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main1);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_sqrt_c(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {
                                if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                    Log.e("edd", "" + main_pos);
                                    sub_pre_sqrt_c(ll_main_ll, 2, false);
                                    ll_main2.removeViewAt(main_pos);
                                    Log.e("edd1", "" + main_pos);
                                    this.add_ed_after_delete(ll_main2, 18, main_pos);
                                } else {
                                    if (main_pos != 0) {
                                        sub_pre_sqrt_c(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                    } else {
                                        sub_pre_sqrt_c(ll_main_ll, 1, false);
                                        ll_main2.removeViewAt(main_pos);
                                        try {
                                            final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                            if (lll.getTag().toString().equals("text")) {
                                                final EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                edd.setMinWidth(20);
                                                edd.setMinimumWidth(20);
                                                edd.requestLayout();
                                                edd.requestFocus();
                                                if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                    Utils.change_edittext_bg(true, edd);
                                                }
                                            }
                                        } catch (Exception e) {
                                        }
                                    }
                                }
                            }

                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("nth_top")) {

                        try {
                            final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            final LinearLayout ll_main2 = (LinearLayout) ll_main.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_frac_top(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {
                                final LinearLayout ll_sqrt = (LinearLayout) ll_main.getChildAt(2);
                                final LinearLayout ll_sqrt2 = (LinearLayout) ll_sqrt.getChildAt(1);
                                if (this.makeansfromview(ll_sqrt2).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_frac_top(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            }

                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("lim_left")) {

                        try {
                            final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                            final LinearLayout ll_main1 = (LinearLayout) ll_main.getParent();
                            final LinearLayout ll_main2 = (LinearLayout) ll_main1.getParent();
                            int main_pos = ll_main2.indexOfChild(ll_main1);

                            if (ll_main_ll.getChildCount() > 1) {
                                sub_pre_sqrt_c(ll_main_ll, 0, false);
                                ll_main_ll.removeViewAt(pos_sub_ll);
                            } else {
                                final LinearLayout ll_lim_rht = (LinearLayout) ll_main.getChildAt(2);
                                if (this.makeansfromview(ll_lim_rht).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_sqrt_c(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_sqrt_c(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_sqrt_c(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            }

                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("frac_top") || main_tag.equals("ss_top")) {

                        final LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        final LinearLayout ll_main2 = (LinearLayout) ll_main.getParent();
                        int main_pos = ll_main2.indexOfChild(ll_main);

                        if (ll_main_ll.getChildCount() > 1) {
                            sub_pre_frac_top(ll_main_ll, 0, false);
                            ll_main_ll.removeViewAt(pos_sub_ll);
                        } else {
                            if (main_tag.equals("frac_top")) {
                                LinearLayout ll_frac = (LinearLayout) ll_main.getChildAt(2);
                                if (this.makeansfromview(ll_frac).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_frac_top(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            } else {
                                LinearLayout ll_frac = (LinearLayout) ll_main.getChildAt(1);

                                if (this.makeansfromview(ll_frac).trim().length() == 0) {
                                    if (main_pos == 0 && ll_main2.getChildCount() == 1) {
                                        sub_pre_frac_top(ll_main_ll, 2, false);
                                        ll_main2.removeViewAt(main_pos);
                                        this.add_ed_after_delete(ll_main2, 18, main_pos);
                                    } else {
                                        if (main_pos != 0) {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                        } else {
                                            sub_pre_frac_top(ll_main_ll, 1, false);
                                            ll_main2.removeViewAt(main_pos);
                                            try {
                                                final LinearLayout lll = (LinearLayout) ll_main2.getChildAt(main_pos);
                                                if (lll.getTag().toString().equals("text")) {
                                                    EditTextBlink edd = (EditTextBlink) lll.getChildAt(0);
                                                    edd.setMinWidth(20);
                                                    edd.setMinimumWidth(20);
                                                    edd.requestLayout();
                                                    edd.requestFocus();
                                                    if (!(ll_main2.getTag(R.id.second).equals("main"))) {
                                                        Utils.change_edittext_bg(true, edd);
                                                    }
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                } else {
                                    this.onpreviousclick();
                                }
                            }
                        }
                    }
                }

                if (is_touch_kb) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ondeleteclick();
                        }
                    }, 150);
                }
            } else {
                this.onpreviousclick();
                if (is_touch_kb) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ondeleteclick();
                        }
                    }, 150);
                }
            }

        } catch (Exception e) {
        }
    }

    public void onpreviousclick() {

        try {
            int cur_pos = 0;

            try {
                cur_pos = selectedTextView.getSelectionStart();
            } catch (Exception e) {
            }

            if (cur_pos != 0) {

                String str_bar_1 = "" + Html.fromHtml("&#x0304;");
                String str_bar_2 = "" + Html.fromHtml("&#772;");

                if (selectedTextView.getText().toString().length() > 1) {
                    String ss = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos - 1));
                    if (ss.equals(str_bar_1) || ss.equals(str_bar_2)) {

                        selectedTextView.setSelection((cur_pos - 2));

                    } else {
                        selectedTextView.setSelection((cur_pos - 1));
                    }
                } else {
                    selectedTextView.setSelection((cur_pos - 1));
                }

                if (is_touch_kb_pre) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onpreviousclick();
                        }
                    }, 150);
                }

            } else {

                final LinearLayout ll_main_ll = ll_ans_box_selected;
                final LinearLayout ll_sub_ll = ll_selected_view;

                int pos_sub_ll = ll_main_ll.indexOfChild(ll_sub_ll);

                if (pos_sub_ll != 0 && pos_sub_ll != -1) {

                    LinearLayout ll_sub = (LinearLayout) ll_main_ll.getChildAt(pos_sub_ll - 1);
                    onpresubcall(ll_sub);

                } else if (pos_sub_ll == 0) {

                    String main_tag = (String) ll_main_ll.getTag(R.id.second);

                    if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                    } else if (main_tag.equals("nth_bottom")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_1 = (LinearLayout) ll_main.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                    } else if (main_tag.equals("lim_right")) {

                        LinearLayout ll_main = (LinearLayout) ll_main_ll.getParent();
                        LinearLayout ll_main_2 = (LinearLayout) ll_main.getChildAt(0);
                        LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                        onpresubcall(ll_sub_1);

                    } else if (main_tag.equals("sqrt_c") || main_tag.equals("lim_left")) {

                        try {
                            sub_pre_sqrt_c(ll_main_ll, 0, true);
                        } catch (Exception e) {
                        }

                    } else if (main_tag.equals("frac_top") || main_tag.equals("ss_top") ||
                            main_tag.equals("super_top") || main_tag.equals("sub_bottom") || main_tag.equals("nth_top") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                            || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                            || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {

                        try {
                            sub_pre_frac_top(ll_main_ll, 0, true);
                        } catch (Exception e) {
                        }

                    }
                }

                if (is_touch_kb_pre) {
                    Handler hnd = new Handler();
                    hnd.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onpreviousclick();
                        }
                    }, 150);
                }

            }

        } catch (Exception e) {
        }

    }

    public void sub_pre_sqrt_c(final LinearLayout ll_main_ll, final int is_main, final boolean is_add_ed) {

        try {

            LinearLayout ll_mainm = (LinearLayout) ll_main_ll.getParent();
            LinearLayout ll_main1 = (LinearLayout) ll_mainm.getParent();
            LinearLayout ll_main2 = (LinearLayout) ll_main1.getParent();
            int main_pos = ll_main2.indexOfChild(ll_main1);
            final String main_tag = String.valueOf(ll_main2.getTag(R.id.second));
            if (main_pos == 0 && is_add_ed) {
                add_last_ed(ll_main2, 22, main_pos);
            } else if (ll_main2.getChildCount() > 1 && main_pos != 0) {
                LinearLayout ll_sub = (LinearLayout) ll_main2.getChildAt(main_pos - 1);
                if (!(ll_sub.getTag().equals("text")) && is_add_ed) {
                    add_last_ed(ll_main2, 22, main_pos);
                } else {
                    onpresubcall(ll_sub);
                }
            } else {
                if (main_tag.equals("sqrt_c") || main_tag.equals("lim_left")) {
                    this.sub_pre_sqrt_c(ll_main2, 0, is_add_ed);
                } else if (main_tag.equals("frac_top") || main_tag.equals("ss_top")
                        || main_tag.equals("super_top") || main_tag.equals("sub_bottom")
                        || main_tag.equals("nth_top") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                        || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                        || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {
                    this.sub_pre_frac_top(ll_main2, 0, is_add_ed);
                } else if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")) {
                    final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(0);
                    final LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
                    onpresubcall(ll_sub_1);
                } else if (main_tag.equals("nth_bottom")) {
                    LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    LinearLayout ll_main_1 = (LinearLayout) ll_main.getParent();
                    LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(0);
                    LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                    onpresubcall(ll_sub_1);
                } else if (main_tag.equals("lim_right")) {

                    LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    LinearLayout ll_main_2 = (LinearLayout) ll_main.getChildAt(0);
                    LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                    onpresubcall(ll_sub_1);

                } else if (main_tag.equals("main")) {
                    if (is_main == 0) {
                        add_last_ed(ll_main2, 18, 0);
                    } else if (is_main == 1) {
                        this.onnextsubcall((LinearLayout) ll_main2.getChildAt(1));
                    }
                }
            }

        } catch (Exception e) {
        }

    }

    public void sub_pre_frac_top(final LinearLayout ll_main_ll, final int is_main, final boolean is_add_ed) {

        try {
            final LinearLayout ll_mainm = (LinearLayout) ll_main_ll.getParent();
            final LinearLayout ll_main2 = (LinearLayout) ll_mainm.getParent();
            int main_pos = ll_main2.indexOfChild(ll_mainm);
            final String main_tag = String.valueOf(ll_main2.getTag(R.id.second));
            if (main_pos == 0 && is_add_ed) {
                add_last_ed(ll_main2, 22, main_pos);
            } else if (ll_main2.getChildCount() > 1 && main_pos != 0) {
                final LinearLayout ll_sub = (LinearLayout) ll_main2.getChildAt(main_pos - 1);
                if (!(ll_sub.getTag().equals("text")) && is_add_ed) {
                    add_last_ed(ll_main2, 22, main_pos);
                } else {
                    onpresubcall(ll_sub);
                }
            } else {
                Log.e("delete2", "" + ll_main2.getTag(R.id.second));
                if (main_tag.equals("sqrt_c") || main_tag.equals("lim_left")) {
                    this.sub_pre_sqrt_c(ll_main2, 0, is_add_ed);
                } else if (main_tag.equals("frac_top") || main_tag.equals("ss_top")
                        || main_tag.equals("super_top") || main_tag.equals("sub_bottom")
                        || main_tag.equals("nth_top") || main_tag.equals("absolute_center") || main_tag.equals("parenthesis_center")
                        || main_tag.equals("line_center") || main_tag.equals("lsegment_center")
                        || main_tag.equals("ray_center") || main_tag.equals("arc_center")) {
                    this.sub_pre_frac_top(ll_main2, 0, is_add_ed);
                } else if (main_tag.equals("frac_bottom") || main_tag.equals("ss_bottom")) {
                    final LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    final LinearLayout ll_main_1 = (LinearLayout) ll_main.getChildAt(0);
                    final LinearLayout ll_sub_1 = (LinearLayout) ll_main_1.getChildAt((ll_main_1.getChildCount() - 1));
                    onpresubcall(ll_sub_1);
                } else if (main_tag.equals("nth_bottom")) {
                    LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    LinearLayout ll_main_1 = (LinearLayout) ll_main.getParent();
                    LinearLayout ll_main_2 = (LinearLayout) ll_main_1.getChildAt(0);
                    LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                    onpresubcall(ll_sub_1);
                } else if (main_tag.equals("lim_right")) {

                    LinearLayout ll_main = (LinearLayout) ll_main2.getParent();
                    LinearLayout ll_main_2 = (LinearLayout) ll_main.getChildAt(0);
                    LinearLayout ll_sub_1 = (LinearLayout) ll_main_2.getChildAt((ll_main_2.getChildCount() - 1));
                    onpresubcall(ll_sub_1);

                } else if (main_tag.equals("main")) {
                    if (is_main == 0) {
                        add_last_ed(ll_main2, 18, 0);
                    } else if (is_main == 1) {
                        this.onnextsubcall((LinearLayout) ll_main2.getChildAt(1));
                    }
                }
            }
        } catch (Exception e) {
        }

    }

    private String makeansfromview(final LinearLayout ll_ans_box) {

        String str = "";

        for (int i = 0; i < ll_ans_box.getChildCount(); i++) {

            LinearLayout ll_view = (LinearLayout) ll_ans_box.getChildAt(i);

            String main_tag = String.valueOf(ll_view.getTag());

            if (main_tag.equals("text")) {

                EditTextBlink ed_1 = (EditTextBlink) ll_view.getChildAt(0);
                if (ed_1.getText().toString().trim().length() != 0) {
                    str = str + "text#" + this.replacecharfromsign(ed_1.getText().toString().trim()) + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }

            } else if (main_tag.equals("fraction")) {

                LinearLayout ll_fraction_top = (LinearLayout) ll_view.getChildAt(0);
                LinearLayout ll_fraction_bottom = (LinearLayout) ll_view.getChildAt(2);

                String ss = this.makeansfromview(ll_fraction_top);
                String ss1 = this.makeansfromview(ll_fraction_bottom);
                if (ss.trim().length() != 0 && ss1.trim().length() != 0) {
                    str = str + "fraction" + ss + "frac_btm_" + ll_ans_box.getTag(R.id.first)
                            + ss1 + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }
            } else if (main_tag.equals("super_script")) {

                LinearLayout ll_top_superscript = (LinearLayout) ll_view.getChildAt(0);

                String ss = this.makeansfromview(ll_top_superscript);

                if (ss.trim().length() != 0) {
                    str = str + "super_script" + ss + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }

            } else if (main_tag.equals("sub_script")) {

                LinearLayout ll_bottom_subscript = (LinearLayout) ll_view.getChildAt(1);

                String ss = this.makeansfromview(ll_bottom_subscript);

                if (ss.trim().length() != 0) {
                    str = str + "sub_script" + ss + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }


            } else if (main_tag.equals("super_sub_script")) {

                LinearLayout ll_top_super_sub_script = (LinearLayout) ll_view.getChildAt(0);
                LinearLayout ll_bottom_super_sub_script = (LinearLayout) ll_view.getChildAt(1);

                String ss = this.makeansfromview(ll_top_super_sub_script);
                String ss1 = this.makeansfromview(ll_bottom_super_sub_script);
                if (ss.trim().length() != 0 && ss1.trim().length() != 0) {
                    str = str + "super_sub_script" + ss + "ss_btm_" + ll_ans_box.getTag(R.id.first)
                            + ss1 + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }
            } else if (main_tag.equals("sqrt")) {

                LinearLayout ll_l = (LinearLayout) ll_view.getChildAt(1);
                LinearLayout ll_center_square_root = (LinearLayout) ll_l.getChildAt(1);

                String ss = this.makeansfromview(ll_center_square_root);

                if (ss.trim().length() != 0) {
                    str = str + "sqrt" + ss + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }

            } else if (main_tag.equals("nsqrt")) {

                LinearLayout ll_top_nth_root = (LinearLayout) ll_view.getChildAt(0);
                LinearLayout ll_l = (LinearLayout) ll_view.getChildAt(2);
                LinearLayout ll_center_nth_root = (LinearLayout) ll_l.getChildAt(1);

                String ss = this.makeansfromview(ll_top_nth_root);
                String ss1 = this.makeansfromview(ll_center_nth_root);
                if (ss.trim().length() != 0 && ss1.trim().length() != 0) {
                    str = str + "nsqrt" + ss + "nsqrt_center_" + ll_ans_box.getTag(R.id.first)
                            + ss1 + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }
            } else if (main_tag.equals("lim")) {

                LinearLayout ll_1 = (LinearLayout) ll_view.getChildAt(1);
                LinearLayout ll_lim_left = (LinearLayout) ll_1.getChildAt(0);
                LinearLayout ll_lim_right = (LinearLayout) ll_1.getChildAt(2);

                String ss = this.makeansfromview(ll_lim_left);
                String ss1 = this.makeansfromview(ll_lim_right);
                if (ss.trim().length() != 0 && ss1.trim().length() != 0) {
                    str = str + "lim" + ss + "lim_rht_" + ll_ans_box.getTag(R.id.first)
                            + ss1 + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }
            } else if (main_tag.equals("parenthesis")) {

                LinearLayout ll_top_superscript = (LinearLayout) ll_view.getChildAt(1);

                String ss = this.makeansfromview(ll_top_superscript);

                if (ss.trim().length() != 0) {
                    str = str + "parenthesis" + ss + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }

            } else if (main_tag.equals("absolute")) {

                LinearLayout ll_top_superscript = (LinearLayout) ll_view.getChildAt(1);

                String ss = this.makeansfromview(ll_top_superscript);

                if (ss.trim().length() != 0) {
                    str = str + "absolute" + ss + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }

            } else if (main_tag.equals("line") || main_tag.equals("lsegment")
                    || main_tag.equals("ray") || main_tag.equals("arc")) {

                LinearLayout ll_top_superscript = (LinearLayout) ll_view.getChildAt(1);

                String ss = this.makeansfromview(ll_top_superscript);

                if (ss.trim().length() != 0) {
                    str = str + main_tag + ss + "" + ll_ans_box.getTag(R.id.first) + "_" + ll_ans_box.getTag(R.id.first);
                }

            }
        }
        try {
            str = str.substring(0, (str.length() - 3));
        } catch (Exception e) {

        }

        try {
            String cut_str = str.substring(str.length() - 8);
            if (cut_str.equals("fraction")) {
                str = str.substring(0, (str.length() - 11));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 12);
            if (cut_str.equals("super_script")) {
                str = str.substring(0, (str.length() - 15));
            }
        } catch (Exception e) {
        }
        try {
            String cut_str = str.substring(str.length() - 10);
            if (cut_str.equals("sub_script")) {
                str = str.substring(0, (str.length() - 13));
            }
        } catch (Exception e) {
        }
        try {
            String cut_str = str.substring(str.length() - 16);
            if (cut_str.equals("super_sub_script")) {
                str = str.substring(0, (str.length() - 19));
            }
        } catch (Exception e) {
        }
        try {
            String cut_str = str.substring(str.length() - 4);
            if (cut_str.equals("sqrt")) {
                str = str.substring(0, (str.length() - 7));
            }
        } catch (Exception e) {
        }
        try {
            String cut_str = str.substring(str.length() - 5);
            if (cut_str.equals("nsqrt")) {
                str = str.substring(0, (str.length() - 8));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 3);
            if (cut_str.equals("lim")) {
                str = str.substring(0, (str.length() - 6));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 11);
            if (cut_str.equals("parenthesis")) {
                str = str.substring(0, (str.length() - 14));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 8);
            if (cut_str.equals("absolute")) {
                str = str.substring(0, (str.length() - 11));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 4);
            if (cut_str.equals("line")) {
                str = str.substring(0, (str.length() - 7));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 8);
            if (cut_str.equals("lsegment")) {
                str = str.substring(0, (str.length() - 11));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 3);
            if (cut_str.equals("ray")) {
                str = str.substring(0, (str.length() - 6));
            }
        } catch (Exception e) {
        }

        try {
            String cut_str = str.substring(str.length() - 3);
            if (cut_str.equals("arc")) {
                str = str.substring(0, (str.length() - 6));
            }
        } catch (Exception e) {
        }

        return str;
    }

    private boolean is_char_match(String ss) {

        String str_plus = "" + Html.fromHtml("&#43;");
        String str_minus = "" + Html.fromHtml("&#8722;");
        String str_equal = "" + Html.fromHtml("&#61;");
        String str_multi = "" + Html.fromHtml("&#215;");
        String str_divide = "" + Html.fromHtml("&#247;");
        String str_goe = "" + Html.fromHtml("&#8805;");
        String str_loe = "" + Html.fromHtml("&#8804;");
        String str_grthan = "" + Html.fromHtml("&#62;");
        String str_lethan = "" + Html.fromHtml("&#60;");

        if (ss.equals(str_plus) || ss.equals(str_minus) || ss.equals(str_equal) || ss.equals(str_multi) || ss.equals(str_divide)
                || ss.equals(str_goe) || ss.equals(str_loe) || ss.equals(str_grthan)
                || ss.equals(str_lethan)) {
            return true;
        } else {
            return false;
        }

    }

    public void onplusminusclick(View v) {
        try {
            if (selected_ed_2 == null) {

                int pos = selectedTextView.getSelectionStart();

                try {

                    if (pos == 0) {

                        if (selectedTextView.getText().toString().trim().length() > 0) {

                            String ss = String.valueOf(selectedTextView.getText().toString().charAt(0));

                            if (ss.equals("-")) {
                                selectedTextView.getText().delete(0, 1);
                            } else {
                                selectedTextView.getText().insert(0, "-");
                            }

                        } else {
                            selectedTextView.getText().insert(0, "-");
                        }

                    }

                } catch (Exception e) {

                }

                for (int i = (pos - 1); i >= 0; i--) {

                    String ss = String.valueOf(selectedTextView.getText().toString().charAt(i));

                    if (ss.equals("-")) {
                        selectedTextView.getText().delete(i, i + 1);
                        break;
                    } else if (ss.equals(" ")) {
                        selectedTextView.getText().insert(i + 1, "-");
                        break;
                    } else if (ss.equals("(") || ss.equals(")")) {
                        selectedTextView.getText().insert(i + 1, "-");
                        break;
                    } else if (is_char_match(ss)) {
                        selectedTextView.getText().insert(i + 1, "-");
                        break;
                    } else if (i == 0) {
                        selectedTextView.getText().insert(i, "-");
                        break;
                    }

                }

            } else {

                int cur_pos = ll_ans_box_selected.indexOfChild(ll_selected_view);

                if (cur_pos < ll_ans_box_selected.getChildCount()) {

                    LinearLayout ll_next = (LinearLayout) ll_ans_box_selected.getChildAt((cur_pos + 1));

                    if (String.valueOf(ll_next.getTag()).equals("text")) {
                        EditTextBlink ed_text = (EditTextBlink) ll_next.getChildAt(0);
                        if (ed_text.getText().toString().contains("-")) {
                            ed_text.getText().delete(0, 1);
                        } else {
                            ed_text.getText().insert(0, "-");
                        }
                        ed_text.setSelection(1);
                        ed_text.requestFocus();
                    } else {
                        final LinearLayout ll_last = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                        ll_last.setTag("text");
                        final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                        ed_centre.setText("-");
                        ed_centre.requestFocus();
                        ll_last.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ed_centre.setCursorVisible(true);
                                selectedTextView = ed_centre;
                                ll_selected_view = ll_last;
                                selected_ed_2 = null;
                                showKeyboard();
                            }
                        });
                        ed_centre.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                ed_centre.setCursorVisible(true);
                                selectedTextView = ed_centre;
                                ll_selected_view = ll_last;
                                selected_ed_2 = null;
                                showKeyboard();
                                return false;
                            }
                        });
                        ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                if (hasFocus) {
                                    selectedTextView = ed_centre;
                                    ll_selected_view = ll_last;
                                    selected_ed_2 = null;
                                }
                            }
                        });

                        ll_ans_box_selected.addView(ll_last, (cur_pos + 1));
                    }
                }
            }
        } catch (Exception e) {

        }
    }

    private void setTextToSelectedTextView(EditText txtView, String text) { // Changes by Siddhi info soft

        final LinearLayout ll_ans_box = ll_ans_box_selected;

        try {

            if (text.equals("op_pi")) {
                text = "" + Html.fromHtml("&#960;");
            }

            if (selected_ed_2 == null) {

                setTextToSelectedTextView2(txtView, text);

            } else {

                int cur_pos = ll_ans_box_selected.indexOfChild(ll_selected_view);

                if (cur_pos < ll_ans_box_selected.getChildCount()) {

                    final LinearLayout ll_last = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.kb_view_ed_last, null);
                    ll_last.setTag("text");
                    final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                    ed_centre.setText(text);
                    ll_last.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ed_centre.setCursorVisible(true);
                            selectedTextView = ed_centre;
                            ll_selected_view = ll_last;
                            selected_ed_2 = null;
                            ll_ans_box_selected = ll_ans_box;
                            showKeyboard();
                        }
                    });
                    ed_centre.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            ed_centre.setCursorVisible(true);
                            selectedTextView = ed_centre;
                            ll_selected_view = ll_last;
                            selected_ed_2 = null;
                            ll_ans_box_selected = ll_ans_box;
                            showKeyboard();
                            return false;
                        }
                    });
                    ed_centre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                selectedTextView = ed_centre;
                                ll_selected_view = ll_last;
                                selected_ed_2 = null;
                                ll_ans_box_selected = ll_ans_box;
                            }
                        }
                    });

                    ll_ans_box.addView(ll_last, (cur_pos + 1));

                    ed_centre.requestFocus();

                }
            }

        } catch (Exception e) {

        }

        selectedTextView.setCursorVisible(true);

    }

    public void setTextToSelectedTextView2(EditText txtView, String text) { //added by siddhiinfosoft
        try {
            if (selectedTextView != null) {

                int cur_pos = selectedTextView.getSelectionStart();
                int ed_length = selectedTextView.getText().toString().length();

                if (selectedTextView.getText().toString().length() > 1) {

                    String str_bar_1 = "" + Html.fromHtml("&#x0304;");
                    String str_bar_2 = "" + Html.fromHtml("&#772;");

                    if (cur_pos != 0 && cur_pos < ed_length) {
                        String ss = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos - 1));

                        if (ss.equals("x")) {

                            String ss_1 = String.valueOf(selectedTextView.getText().toString().charAt(cur_pos));

                            if (ss_1.equals(str_bar_1) || ss_1.equals(str_bar_2)) {

                            } else {
                                selectedTextView.getText().insert(selectedTextView.getSelectionStart(), text);
                            }

                        } else {
                            selectedTextView.getText().insert(selectedTextView.getSelectionStart(), text);
                        }

                    } else {
                        selectedTextView.getText().insert(selectedTextView.getSelectionStart(), text);
                    }

                } else {

                    selectedTextView.getText().insert(selectedTextView.getSelectionStart(), text);

                }

                if (text.equals("|  |")) {
                    selectedTextView.setSelection((selectedTextView.getSelectionStart() - 3));
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        selectedTextView.setCursorVisible(true);
    }

    private void callclickmethod(final CustomeAns customeAns, final LinearLayout ll_ans_box , int index) {

        try {
            if (customeAns.getFillInType() == FILL_IN_TYPE) {

                LinearLayout ll_last = (LinearLayout) ll_ans_box.getChildAt((ll_ans_box.getChildCount() - 1));
                if (ll_last.getTag().toString().equals("text")) {
                    final EditTextBlink ed_centre = (EditTextBlink) ll_last.findViewById(R.id.ed_centre);
                    ed_centre.setSelection(ed_centre.getText().toString().length());
                    ed_centre.requestFocus();
                } else {
                    add_last_ed(ll_ans_box, 22, ll_ans_box.getChildCount());
                }
                showKeyboard();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void setListenerOnNewKeys() {
        try {
            kb_btn_123_delete.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            is_touch_kb = true;
                            Handler hnd = new Handler();
                            hnd.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    ondeleteclick();
                                }
                            }, 200);
                            break;
                        case MotionEvent.ACTION_UP:
                            is_touch_kb = false;
                            break;
                    }
                    return true;
                }
            });

            kb_btn_abc_delete.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            is_touch_kb = true;
                            Handler hnd = new Handler();
                            hnd.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    ondeleteclick();
                                }
                            }, 200);
                            break;
                        case MotionEvent.ACTION_UP:
                            is_touch_kb = false;
                            break;
                    }
                    return true;
                }
            });

            kb_btn_123_previous.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            is_touch_kb_pre = true;
                            Handler hnd = new Handler();
                            hnd.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    onpreviousclick();
                                }
                            }, 200);
                            break;
                        case MotionEvent.ACTION_UP:
                            is_touch_kb_pre = false;
                            break;
                    }
                    return true;
                }
            });

            kb_btn_123_next.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            is_touch_kb_next = true;
                            Handler hnd = new Handler();
                            hnd.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    onnextclick();
                                }
                            }, 200);
                            break;
                        case MotionEvent.ACTION_UP:
                            is_touch_kb_next = false;
                            break;
                    }
                    return true;
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //Only for adding
    @Override
    protected void setWidgetsReferences() {

    }

    @Override
    protected void setListenerOnWidgets() {

    }

    @Override
    protected void setTextFromTranslation() {

    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {

    }

    @Override
    public void onClick(View v) {

    }
    //end
}
