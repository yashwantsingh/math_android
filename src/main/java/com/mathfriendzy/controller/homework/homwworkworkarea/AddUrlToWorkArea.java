package com.mathfriendzy.controller.homework.homwworkworkarea;

import java.io.Serializable;

/**
 * Created by root on 23/8/16.
 */
public class AddUrlToWorkArea implements Cloneable , Serializable{
    private String title;
    private String url;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
