package com.mathfriendzy.controller.homework.homwworkworkarea;

import android.app.Dialog;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.helper.MathFriendzyHelper;

import java.util.ArrayList;

/**
 * Created by root on 23/8/16.
 */
public class AddUrlToQuestionDialog {

    private Context context = null;
    private OnUrlSaveCallback callback = null;
    private boolean isWorkAreaForStudent = false;
    private Dialog dialog = null;
    private ArrayList<AddUrlToWorkArea> originalList = null;

    public AddUrlToQuestionDialog(Context context , boolean isWorkAreaForStudent){
        this.context = context;
        this.isWorkAreaForStudent = isWorkAreaForStudent;
    }

    private AddLinkAdapter adapter = null;

    public void initializeCallback(OnUrlSaveCallback callback){
        this.callback = callback;
    }

    public void showDialog(ArrayList<AddUrlToWorkArea> urlList){
        this.putObjectInOriginalList(urlList);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.add_gdoc_or_web_links_dialog);
        dialog.show();

        ViewHolder vHolder = new ViewHolder();
        vHolder.txtAddMoreLink = (TextView) dialog.findViewById(R.id.txtAddMoreLink);
        vHolder.lstUrlList = (ListView) dialog.findViewById(R.id.lstUrlList);
        vHolder.btnAddMoreLink = (Button) dialog.findViewById(R.id.btnAddMoreLink);
        vHolder.btnSave = (Button) dialog.findViewById(R.id.btnSave);
        vHolder.txtToGoogleDoc = (TextView) dialog.findViewById(R.id.txtToGoogleDoc);
        vHolder.btnCrossButtonOnLinkDialog = (Button) dialog.findViewById(R.id.btnCrossButtonOnLinkDialog);
        vHolder.mainLayout = (RelativeLayout) dialog.findViewById(R.id.mainLayout);

        this.setWidgetsTexts(vHolder);
        this.setListAdapter(vHolder , vHolder.lstUrlList , urlList);
        vHolder.btnAddMoreLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddUrlToWorkArea url = new AddUrlToWorkArea();
                url.setTitle("");
                url.setUrl("");
                addMoreRecord(url);
            }
        });

        vHolder.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(adapter != null){
                    hideDeviceKeyboard();
                    clearFocusFromEditText();
                    if(adapter.isClearLink()){//if user delete all link and hit on save
                        clickOnSave(adapter.getUpdatedList() , OnUrlSaveCallback.ON_SAVE);
                        hideDialog(dialog);
                        return ;
                    }
                    if(adapter.isAllFieldFilled()) {
                        clickOnSave(adapter.getUpdatedList() , OnUrlSaveCallback.ON_SAVE);
                        hideDialog(dialog);
                    }else{
                        MathFriendzyHelper.showWarningDialog(context ,
                               MathFriendzyHelper.getTreanslationTextById(context , "lblTitleUrlNeeded"));
                    }
                }
            }
        });


        vHolder.btnCrossButtonOnLinkDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(adapter != null){
                    clickOnSave(originalList , OnUrlSaveCallback.ON_CLOSE);
                }
                hideDialog(dialog);
            }
        });


        vHolder.mainLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                clearFocusAndNotifyAdapter();
                return true;
            }
        });
        this.setVisibilityOfViews(vHolder);
    }

    /**
     * Clear the focus and notify adapter
     */
    private void clearFocusAndNotifyAdapter(){
        clearFocusFromEditText();
        if(adapter != null){
            adapter.notifyDataSetChanged();
        }
    }

    private void putObjectInOriginalList(ArrayList<AddUrlToWorkArea> originalList){
        this.originalList = new ArrayList<AddUrlToWorkArea>(originalList.size());
        for (AddUrlToWorkArea foo: originalList) {
            try {
                this.originalList.add((AddUrlToWorkArea)foo.clone());
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        }
    }

    private void setWidgetsTexts(ViewHolder vHolder){
        String text[] =  MathFriendzyHelper.getTreanslationTextById(context , "lblAdd" , "btnTitleSave" ,
                "lblAddLinks" , "lblToGDoc" , "lblImpInfoInAsg");
        vHolder.btnAddMoreLink.setText(text[0]);
        vHolder.btnSave.setText(text[1]);
        if(this.isWorkAreaForStudent){
            vHolder.txtAddMoreLink.setText(text[2]);
        }else{
            vHolder.txtAddMoreLink.setText(text[4]);
        }
        vHolder.txtToGoogleDoc.setText(text[3]);
        if(!this.isWorkAreaForStudent){
            vHolder.txtToGoogleDoc.setVisibility(View.GONE);
        }
    }

    private void setVisibilityOfViews(ViewHolder viewHolder){
        if(!this.isWorkAreaForStudent){//for teacher
            viewHolder.btnAddMoreLink.setVisibility(View.GONE);
            viewHolder.btnSave.setVisibility(View.GONE);
        }
    }

    /**
     * Clear the focus on the Adapter when user input value in the Edittext
     */
    public void clearFocusFromEditText() {
        try {
            dialog.getCurrentFocus().clearFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Hide the device kayboard
     */
    public void hideDeviceKeyboard() {
        try {
            InputMethodManager inputManager = (InputMethodManager)
                    context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(dialog.getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void clickOnSave(ArrayList<AddUrlToWorkArea> updatedList , int tag){
        try {
            hideDeviceKeyboard();
            clearFocusFromEditText();
            if (this.callback != null) {
                this.callback.onSave(updatedList , tag);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    class ViewHolder{
        TextView txtAddMoreLink;
        ListView lstUrlList;
        Button btnAddMoreLink;
        Button btnSave;
        TextView txtToGoogleDoc;
        Button btnCrossButtonOnLinkDialog;
        RelativeLayout mainLayout;
    }

    //hide the dialog
    public void hideDialog(Dialog dialog){
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    private void setListAdapter(ViewHolder viewHolder , ListView lstUrlList , ArrayList<AddUrlToWorkArea> urlList) {
        adapter = new AddLinkAdapter(context , urlList , lstUrlList , viewHolder);
        lstUrlList.setAdapter(adapter);
        this.setListViewHeight(adapter , lstUrlList , viewHolder);
    }

    private void addMoreRecord(AddUrlToWorkArea url){
        if(adapter != null){
            adapter.addMoreUrl(url);
        }
    }

    private void setListViewHeight(AddLinkAdapter adapter , ListView listView , ViewHolder viewHolder){
        if(this.isWorkAreaForStudent) {
            if (adapter.getCount() > 2) {
                View item = adapter.getView(0, null, listView);
                item.measure(0, 0);
                RelativeLayout.LayoutParams params = new RelativeLayout.
                        LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                        (int) (2.2 * item.getMeasuredHeight()));
                //params.addRule(RelativeLayout.LEFT_OF, viewHolder.btnAddMoreLink.getId());
                listView.setLayoutParams(params);
            }
        }else{
            if (adapter.getCount() > 4) {
                View item = adapter.getView(0, null, listView);
                item.measure(0, 0);
                RelativeLayout.LayoutParams params = new RelativeLayout.
                        LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                        (int) (4.4 * item.getMeasuredHeight()));
                //params.addRule(RelativeLayout.LEFT_OF, viewHolder.btnAddMoreLink.getId());
                listView.setLayoutParams(params);
            }
        }
    }

    class AddLinkAdapter extends BaseAdapter{

        private Context context = null;
        private ArrayList<AddUrlToWorkArea> urlList = null;
        private AdapterViewHolder viewHolder = null;
        private LayoutInflater mInflater = null;
        private ListView lstUrlList = null;
        private ViewHolder mainViewHolder = null;
        private String text[] = null;

        AddLinkAdapter(Context context , ArrayList<AddUrlToWorkArea> urlList ,
                       ListView lstUrlList , ViewHolder mainViewHolder){
            this.context = context;
            this.urlList = urlList;
            this.mInflater = LayoutInflater.from(context);
            this.lstUrlList = lstUrlList;
            this.mainViewHolder = mainViewHolder;

            text = MathFriendzyHelper.getTreanslationTextById(context , "lblEnterTitle" ,
                    "lblEnterUrl" , "btnTitleGo");
        }

        @Override
        public int getCount() {
            return urlList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            if(view == null){
                view = mInflater.inflate(R.layout.add_gdoc_or_web_link_list_item , null);
                viewHolder = new AdapterViewHolder();
                viewHolder.edtEnterTitle = (EditText) view.findViewById(R.id.edtEnterTitle);
                viewHolder.edtEnterUrl = (EditText) view.findViewById(R.id.edtEnterUrl);
                viewHolder.btnDeleteLink = (Button) view.findViewById(R.id.btnDeleteLink);
                viewHolder.btnGo = (Button) view.findViewById(R.id.btnGo);
                view.setTag(viewHolder);
            }else{
                viewHolder = (AdapterViewHolder) view.getTag();
            }

            viewHolder.edtEnterTitle.setHint(text[0]);
            viewHolder.edtEnterUrl.setHint(text[1]);
            viewHolder.btnGo.setText(text[2]);
            viewHolder.edtEnterTitle.setText(urlList.get(position).getTitle());
            viewHolder.edtEnterUrl.setText(urlList.get(position).getUrl());

            viewHolder.edtEnterTitle.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(!hasFocus){
                        urlList.get(position).setTitle(((EditText)v).getText().toString());
                    }
                }
            });

            viewHolder.edtEnterUrl.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(!hasFocus){
                        urlList.get(position).setUrl(((EditText) v).getText().toString());
                        AddLinkAdapter.this.notifyDataSetChanged();
                    }
                }
            });

            viewHolder.btnDeleteLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(urlList != null && urlList.size() > 0){
                        clearFocusFromEditText();
                        if(urlList.size() == 1){
                            urlList.get(position).setUrl("");
                            urlList.get(position).setTitle("");
                        }else{
                            urlList.remove(position);
                        }
                        AddLinkAdapter.this.notifyDataSetChanged();
                    }
                }
            });


            viewHolder.btnGo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clearFocusFromEditText();
                    MathFriendzyHelper.openUrl(context ,
                            getUpdatedUrl(urlList.get(position).getUrl()));
                }
            });

            this.setVisibilityOfViewForStudent(viewHolder , urlList.get(position));
            this.setVisibilityOfGoButton(viewHolder , urlList.get(position));
            return view;
        }

        private void setVisibilityOfViewForStudent(AdapterViewHolder viewHolder ,
                                                   final AddUrlToWorkArea addUrlToWorkArea){
            if(!AddUrlToQuestionDialog.this.isWorkAreaForStudent){//work area for teacher
                viewHolder.edtEnterUrl.setVisibility(View.GONE);
                viewHolder.btnDeleteLink.setVisibility(View.GONE);
                viewHolder.btnGo.setVisibility(View.GONE);
                viewHolder.edtEnterTitle.setFocusable(false);
                viewHolder.edtEnterTitle.setClickable(true);
                viewHolder.edtEnterTitle.setTextColor(context.getResources().getColor(R.color.SKY_BLUE));
                viewHolder.edtEnterTitle.setText(Html.fromHtml(MathFriendzyHelper
                        .convertStringToUnderLineString(addUrlToWorkArea.getTitle())));
                viewHolder.edtEnterTitle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideDialog(dialog);
                        MathFriendzyHelper.openUrl(context ,
                                getUpdatedUrl(addUrlToWorkArea.getUrl()));
                    }
                });
            }
        }

        private String getUpdatedUrl(String url){
            if(url.startsWith("http")){
                return url;
            }
            return "http://" + url;
        }

        private class AdapterViewHolder{
            EditText edtEnterTitle;
            EditText edtEnterUrl;
            Button btnDeleteLink;
            Button btnGo;
        }

        public void addMoreUrl(AddUrlToWorkArea addUrlToWorkArea){
            clearFocusFromEditText();
            this.urlList.add(addUrlToWorkArea);
            this.notifyDataSetChanged();
            setListViewHeight(this , this.lstUrlList ,  this.mainViewHolder);
            this.lstUrlList.setSelection
                    (this.lstUrlList.getAdapter().getCount() - 1);
        }

        public ArrayList<AddUrlToWorkArea> getUpdatedList(){
            return urlList;
        }

        public boolean isAllFieldFilled(){
            if(this.urlList != null && this.urlList.size() > 0){
                for(int i  = 0 ; i < this.urlList.size() ; i ++ ){
                    AddUrlToWorkArea url = this.urlList.get(i);
                    if(MathFriendzyHelper.isEmpty(url.getTitle())
                            || MathFriendzyHelper.isEmpty(url.getUrl())){
                        return false;
                    }
                }
            }
            return true;
        }

        public boolean isClearLink(){
            if(this.urlList != null && this.urlList.size() == 1){
                AddUrlToWorkArea url = this.urlList.get(0);
                if(MathFriendzyHelper.isEmpty(url.getTitle())
                        && MathFriendzyHelper.isEmpty(url.getUrl())){
                    return true;
                }
            }
            return false;
        }

        private void setVisibilityOfGoButton(AdapterViewHolder viewHolder ,
                                             final AddUrlToWorkArea addUrlToWorkArea){

            if(isWorkAreaForStudent) {
                if (MathFriendzyHelper.isEmpty(addUrlToWorkArea.getUrl())) {
                    viewHolder.btnGo.setVisibility(View.GONE);
                } else {
                    viewHolder.btnGo.setVisibility(View.VISIBLE);
                }
            }else{
                viewHolder.btnGo.setVisibility(View.GONE);
            }
        }
    }
}
