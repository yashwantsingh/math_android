package com.mathfriendzy.controller.homework;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.learningcenter.LearningCenterEquationSolveWithTimer;
import com.mathfriendzy.controller.learningcenter.schoolcurriculum.LearningCenterSchoolCurriculumEquationSolve;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.homework.CustomePlayerAns;
import com.mathfriendzy.model.homework.CustomeResult;
import com.mathfriendzy.model.homework.GetAnswerDetailForStudentHWParam;
import com.mathfriendzy.model.homework.GetHomeWorkForStudentWithCustomeResponse;
import com.mathfriendzy.model.homework.GetStudentDetailAnswerResponse;
import com.mathfriendzy.model.homework.PracticeResult;
import com.mathfriendzy.model.homework.PracticeResultSubCat;
import com.mathfriendzy.model.homework.PracticeSkillSortByCategoryId;
import com.mathfriendzy.model.homework.SaveHomeWorkServerResponse;
import com.mathfriendzy.model.homework.WordResult;
import com.mathfriendzy.model.homework.WordSubCatResult;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.utils.CommonUtils;

import java.util.ArrayList;
import java.util.Collections;

public class ActHomeWorkQuizzes extends ActBase {

    private final String TAG = this.getClass().getSimpleName();
    private LinearLayout homeWorkQuizzLayout = null;
    private GetHomeWorkForStudentWithCustomeResponse homeWorkQuizzData = null;

    private String practiceSkillText = null;
    private String wordProblemText = null;
    private String customeText = null;

    //for performing listener
    //for practice skill
    private ArrayList<PracticeResultSubCat> mPracticeResultSubCatList = null;
    private ArrayList<RelativeLayout> practiceSubCatLayout = null;

    //for word problem
    private ArrayList<WordSubCatResult> mWordProblemSubCat = null;
    private ArrayList<RelativeLayout> wordSubCatLayout = null;

    //for custom
    private ArrayList<RelativeLayout> customSubCatLayout = null;
    private boolean isExpireQuizz = false;
    private String expireQuizzWarningMsg = "";

    //for start activity for result
    private final int PLAY_PRACTICE_SKILL_REQUEST = 1001;
    private final int PLAY_WORD_PROBLEM_REQUEST = 1002;
    private final int PLAY_CUSTOM_PROBLEM_REQUEST = 1003;

    private String currentPracticePlayCatId = "";
    private int currentPracticePlaySubCatId = 0;
    private String currentPlayWordCatId = "";
    private int currentPlayWordSubCatId = 0;
    private boolean isDataUpdatedByPlaying = false;
    private SaveHomeWorkServerResponse updatedData = null;
    private int selectedCustomeHWId = 0;

    private String teacherName = null;
    private String grade = null;
    private String dueDate = null;

    private TextView txtTeacherName = null;
    private TextView txtGradeWithDueDate = null;
    private EditText txtTeacherMessage = null;
    private String avgScoreText = null;
    private String zeroCrediteText = null;
    private String halFCredittext = null;
    private String fullCreditText = null;
    private String creditText = null;

    //for the layout for practice,word and custome in which only one layout shows the txtNo ans score
    private boolean isAlreadyTextNoAndScoreDisplayed = false;

    private UserPlayerDto selectedPlayerData = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_home_work_quizzes);

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside onCreate()");

        selectedPlayerData = this.getPlayerData();

        this.init();
        this.getIntentValue();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.setHomeWorkData();

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "outside onCreate()");
    }

    private void init() {
        mPracticeResultSubCatList = new ArrayList<PracticeResultSubCat>();
        practiceSubCatLayout = new ArrayList<RelativeLayout>();

        mWordProblemSubCat = new ArrayList<WordSubCatResult>();
        wordSubCatLayout = new ArrayList<RelativeLayout>();

        customSubCatLayout = new ArrayList<RelativeLayout>();
    }

    /**
     * This method clear the old layout
     */
    private void clearLayout() {
        if (homeWorkQuizzLayout != null) {
            homeWorkQuizzLayout.removeAllViews();
        }
    }

    /**
     * This method getIntent vlaue which are set in previous screen
     */
    private void getIntentValue() {
        isExpireQuizz = this.getIntent().getBooleanExtra("isExpireQuizz", false);
        homeWorkQuizzData = (GetHomeWorkForStudentWithCustomeResponse)
                this.getIntent().getSerializableExtra("homeWorkQuizzData");
        teacherName = this.getIntent().getStringExtra("teacherName");
        grade = this.getIntent().getStringExtra("grade");
        dueDate = homeWorkQuizzData.getDate();

    }

    @Override
    protected void setWidgetsReferences() {

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside setWidgetsReferences()");

        //from base class
        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        homeWorkQuizzLayout = (LinearLayout) findViewById(R.id.homeWorkQuizzLayout);

        txtTeacherName = (TextView) findViewById(R.id.txtTeacherName);
        txtGradeWithDueDate = (TextView) findViewById(R.id.txtGradeWithDueDate);
        txtTeacherMessage = (EditText) findViewById(R.id.txtTeacherMessage);

        this.setBottomLayoutWidgetsReferences();

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "outside setWidgetsReferences()");
    }

    @Override
    protected void setListenerOnWidgets() {

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside setListenerOnWidgets()");

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "outside setListenerOnWidgets()");

    }

    @Override
    protected void setTextFromTranslation() {

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside setTextFromTranslation()");

        try {
            Translation transeletion = new Translation(this);
            transeletion.openConnection();
            txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblHomework") + " " +
                    transeletion.getTranselationTextByTextIdentifier("lblQuizzes"));

            practiceSkillText = transeletion.getTranselationTextByTextIdentifier("lblSolveEquations");
            wordProblemText = transeletion.getTranselationTextByTextIdentifier("lblWordProblem");
            customeText = transeletion.getTranselationTextByTextIdentifier("lblClass")
                    + " " + transeletion.getTranselationTextByTextIdentifier("lblWork")
                    + "/" + transeletion.getTranselationTextByTextIdentifier("lblQuizzes");
            expireQuizzWarningMsg = transeletion.getTranselationTextByTextIdentifier("lblSorryYouAreNotAllowedToPlay");
            //if(selectedPlayerData != null){
            txtTeacherName.setText(transeletion.getTranselationTextByTextIdentifier("mfLblTeacherName")
                    + ": " + teacherName);
            txtGradeWithDueDate.setText(/*transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade") + ": " + grade
                    + "   " + */transeletion.getTranselationTextByTextIdentifier("lblDueDate") + ": " + dueDate);

            txtTotalProblems.setText(transeletion.getTranselationTextByTextIdentifier
                    ("lblTotal") + " " + transeletion.getTranselationTextByTextIdentifier("lblNumOfProblems")
                    + ": " + homeWorkQuizzData.getTotalQuestion());

            txtZeroCredit.setText(transeletion.getTranselationTextByTextIdentifier
                    ("lblZero") + " " + transeletion.getTranselationTextByTextIdentifier("lblCredit")
                    + ": " + homeWorkQuizzData.getZeroCredit());
            txtHaltCredit.setText(transeletion.getTranselationTextByTextIdentifier
                    ("lblHalf") + " " + transeletion.getTranselationTextByTextIdentifier("lblCredit")
                    + ": " + homeWorkQuizzData.getHalfCredit());
            txtFullCredit.setText(transeletion.getTranselationTextByTextIdentifier
                    ("lblFull") + " " + transeletion.getTranselationTextByTextIdentifier("lblCredit")
                    + ": " + homeWorkQuizzData.getFullCredit());

            avgScoreText = transeletion.getTranselationTextByTextIdentifier
                    ("lblAverage") + " " + transeletion.getTranselationTextByTextIdentifier("mfBtnTitleScore");

            txtAvgScore.setText(transeletion.getTranselationTextByTextIdentifier
                    ("lblAverage") + " " + transeletion.getTranselationTextByTextIdentifier("mfBtnTitleScore")
                    + ": " + homeWorkQuizzData.getAvgScore() + "%");

            creditText = transeletion.getTranselationTextByTextIdentifier("lblCredit");
            zeroCrediteText = transeletion.getTranselationTextByTextIdentifier("lblZero");
            halFCredittext = transeletion.getTranselationTextByTextIdentifier("lblHalf");
            fullCreditText = transeletion.getTranselationTextByTextIdentifier("lblFull");

            txtTeacherMessage.setText(homeWorkQuizzData.getMessage());

            //}
            transeletion.closeConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "outside setTextFromTranslation()");
    }

    /**
     * This method set the home work data
     */
    private void setHomeWorkData() {

        if (homeWorkQuizzData.getCustomeresultList().size() > 0) {
            homeWorkQuizzLayout.addView
                    (this.addCustomeProblemData(homeWorkQuizzData.getCustomeresultList()));
        }

        if (homeWorkQuizzData.getPractiseResultList().size() > 0) {
            Collections.sort(homeWorkQuizzData.getPractiseResultList(),
                    new PracticeSkillSortByCategoryId());
            homeWorkQuizzLayout.addView
                    (this.addPracticeSkillData(homeWorkQuizzData.getPractiseResultList()));
        }

        if (homeWorkQuizzData.getWordResultList().size() > 0) {
            homeWorkQuizzLayout.addView
                    (this.addWordProblemData(homeWorkQuizzData.getWordResultList()));
        }
    }

    /**
     * This method create and return a linear layout
     *
     * @return
     */
    private LinearLayout getLinearLayout() {
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);
        return layout;
    }


    /**
     * This method add the practice layout data into the main layout to display
     *
     * @param arrayList
     */
    private LinearLayout addPracticeSkillData(ArrayList<PracticeResult> arrayList) {
        LinearLayout layout = this.getLinearLayout();
        layout.addView(this.getInflatedViewForQuizzQuestionType(1));
        layout.addView(this.addPracticeSkillLayout(arrayList));
        return layout;
    }

    /**
     * This method add the word problem data into main layout to display
     *
     * @param arrayList
     */
    private LinearLayout addWordProblemData(ArrayList<WordResult> arrayList) {
        LinearLayout layout = this.getLinearLayout();
        layout.addView(this.getInflatedViewForQuizzQuestionType(2));
        layout.addView(this.addWordProblemLayout(arrayList));
        return layout;
    }

    /**
     * This method add the custome data into main layout to display
     *
     * @param arrayList
     */
    private LinearLayout addCustomeProblemData(ArrayList<CustomeResult> arrayList) {
        LinearLayout layout = this.getLinearLayout();
        layout.addView(this.getInflatedViewForQuizzQuestionType(3));
        layout.addView(this.addCustomeLayout(arrayList));
        return layout;
    }

    private View getInflatedViewForQuizzQuestionType(int inflateFor) {
        View view = LayoutInflater.from(this)
                .inflate(R.layout.homework_quizz_question_type_layout, null);
        TextView txtView = (TextView) view.findViewById(R.id.txtQuestionType);
        TextView txtNo = (TextView) view.findViewById(R.id.txtNo);
        TextView txtScore = (TextView) view.findViewById(R.id.txtScore);

        if (inflateFor == 1) {//for practice skill
            txtView.setText(practiceSkillText);
        } else if (inflateFor == 2) {//for word problem
            txtView.setText(wordProblemText);
        } else if (inflateFor == 3) {//for custome
            txtView.setText(customeText);
        }

        //for show the txtNo and txtScore text
        if (!isAlreadyTextNoAndScoreDisplayed) {
            isAlreadyTextNoAndScoreDisplayed = true;
            txtNo.setVisibility(TextView.VISIBLE);
            txtScore.setVisibility(TextView.VISIBLE);
        }
        return view;
    }


    /**
     * This method add the practice skill layout with cat and subCat
     *
     * @param arrayList
     * @return
     */
    private LinearLayout addPracticeSkillLayout(final ArrayList<PracticeResult> arrayList) {

        LinearLayout layout = this.getLinearLayout();
        ArrayList<LearningCenterTransferObj> laernignCenterFunctionsList =
                MathFriendzyHelper.getPracticeSkillCategories(this);
        ArrayList<String> updatedCatName = MathFriendzyHelper
                .getUpdatedPracticeSkillCatNameList(laernignCenterFunctionsList, this);

        for (int i = 0; i < arrayList.size(); i++) {
            for (int j = 0; j < laernignCenterFunctionsList.size(); j++) {
                if (arrayList.get(i).getCatId().equals
                        (laernignCenterFunctionsList.get(j)
                                .getLearningCenterMathOperationId() + "")) {
                    View view = this.getCategoryInflatedLayout(1);
                    ImageView imgSign = (ImageView) view.findViewById(R.id.imgSign);
                    TextView txtCatName = (TextView) view.findViewById(R.id.txtCategoryName);
                    imgSign.setBackgroundResource(getResources().getIdentifier
                            ("mf_" + laernignCenterFunctionsList.get(j)
                                            .getLearningCenterOperation()
                                            .toLowerCase().replace(" ", "_") + "_sign", "drawable",
                                    getPackageName()));
                    txtCatName.setText(updatedCatName.get(j));
                    layout.addView(view);

                    final ArrayList<PracticeResultSubCat> practiceResultSubCatList =
                            arrayList.get(i).getPracticeResultSubCatList();

                    for (int k = 0; k < practiceResultSubCatList.size(); k++) {
                        View subCatView = this.getSubCategoryInflatedLayout(1);
                        RelativeLayout rllayout = (RelativeLayout) subCatView.findViewById(R.id.layout);
                        TextView txtSubCatName = (TextView) subCatView.findViewById(R.id.txtSubCategoryName);
                        TextView txtNoOfQuestions = (TextView) subCatView.findViewById(R.id.txtNo);
                        TextView txtScore = (TextView) subCatView.findViewById(R.id.txtScore);

                        txtSubCatName.setText(this.getPracticeSkillSubCatName
                                (arrayList.get(i).getCatId(), practiceResultSubCatList.get(k).getSubCatId() + ""));
                        txtNoOfQuestions.setText(practiceResultSubCatList.get(k).getProblems() + "");
                        txtScore.setText(practiceResultSubCatList.get(k).getScore() + "%");
                        layout.addView(subCatView);
                        practiceResultSubCatList.get(k).setCatId(arrayList.get(i).getCatId());

                        mPracticeResultSubCatList.add(practiceResultSubCatList.get(k));
                        practiceSubCatLayout.add(rllayout);
                        rllayout.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (isExpireQuizz) {
                                    showExpireQuizzDialog();
                                } else {
                                    for (int i = 0; i < practiceSubCatLayout.size(); i++) {
                                        if (v == practiceSubCatLayout.get(i)) {
                                            ArrayList<Integer> categories = new ArrayList<Integer>();
                                            categories.add(mPracticeResultSubCatList.get(i).getSubCatId());

                                            currentPracticePlayCatId = mPracticeResultSubCatList.get(i).getCatId();
                                            currentPracticePlaySubCatId = mPracticeResultSubCatList.get(i).getSubCatId();

                                            Intent intent = new Intent(ActHomeWorkQuizzes.this, LearningCenterEquationSolveWithTimer.class);
                                            intent.putIntegerArrayListExtra("selectedCategories", categories);
                                            intent.putExtra("isForHomeWork", true);
                                            intent.putExtra("numberOfProblems", mPracticeResultSubCatList.get(i).getProblems());
                                            intent.putExtra("HWId", homeWorkQuizzData.getHomeWorkId());
                                            intent.putExtra("catId", currentPracticePlayCatId);
                                            intent.putExtra("subCatId", currentPracticePlaySubCatId);
                                            startActivityForResult(intent, PLAY_PRACTICE_SKILL_REQUEST);
                                        }
                                    }
                                }
                            }
                        });
                    }
                    break;
                }
            }
        }

        return layout;
    }


    /**
     * Get practice skill subCat Name
     *
     * @param catId
     * @param subCatId
     * @return
     */
    private String getPracticeSkillSubCatName(String catId, String subCatId) {

        String subCatName = null;
        LearningCenterimpl laerningCenter = new LearningCenterimpl(this);
        laerningCenter.openConn();
        subCatName = laerningCenter.
                getMathOperationCategoriesByCatIdAndSubCatId(catId, subCatId)
                .getMathOperationCategory();
        laerningCenter.closeConn();

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        subCatName = transeletion.getTranselationTextByTextIdentifier(subCatName);
        transeletion.closeConnection();

        return subCatName;
    }

    /**
     * This method add the word problem layout with Cat and SubCat
     *
     * @param arrayList
     * @return
     */
    private LinearLayout addWordProblemLayout(ArrayList<WordResult> arrayList) {
        LinearLayout layout = this.getLinearLayout();

        for (int i = 0; i < arrayList.size(); i++) {
            View view = this.getCategoryInflatedLayout(2);
            ImageView imgSign = (ImageView) view.findViewById(R.id.imgSign);
            TextView txtCatName = (TextView) view.findViewById(R.id.txtCategoryName);
            txtCatName.setText(MathFriendzyHelper
                    .getSchoolCurriculumCategoryNameByCatId
                            (this, arrayList.get(i).getCatIg()));
            imgSign.setVisibility(ImageView.GONE);
            layout.addView(view);

            ArrayList<WordSubCatResult> subCatList = arrayList.get(i).getSubCatResultList();
            for (int j = 0; j < subCatList.size(); j++) {
                View subCatView = this.getSubCategoryInflatedLayout(2);
                RelativeLayout rllayout = (RelativeLayout) subCatView.findViewById(R.id.layout);
                TextView txtSubCatName = (TextView) subCatView.findViewById(R.id.txtSubCategoryName);
                TextView txtNoOfQuestions = (TextView) subCatView.findViewById(R.id.txtNo);
                TextView txtScore = (TextView) subCatView.findViewById(R.id.txtScore);
                txtSubCatName.setText(MathFriendzyHelper.
                        getSchoolCurriculumSubCatNameByCatIdAndSubCatId
                                (this, arrayList.get(i).getCatIg(), subCatList.get(j).getSubCatId() + ""));
                txtNoOfQuestions.setText("10");
                txtScore.setText(subCatList.get(j).getScore() + "%");
                layout.addView(subCatView);

                subCatList.get(j).setCatId(arrayList.get(i).getCatIg());
                mWordProblemSubCat.add(subCatList.get(j));
                wordSubCatLayout.add(rllayout);

                rllayout.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        if (isExpireQuizz) {
                            showExpireQuizzDialog();
                        } else {
                            MathFriendzyHelper.isWordQuestionDownloaded
                                    (ActHomeWorkQuizzes.this, Integer.parseInt(grade),
                                            new HttpServerRequest() {
                                                @Override
                                                public void onRequestComplete() {
                                                    playWordProblem(v);
                                                }
                                            });
                        }
                    }
                });
            }
        }
        return layout;
    }

    /**
     * Show the dialog for expire homework
     */
    private void showExpireQuizzDialog() {
        MathFriendzyHelper.showWarningDialog(this, expireQuizzWarningMsg);
    }

    /**
     * Click to play word problem
     *
     * @param v
     */
    private void playWordProblem(View v) {
        try {
            for (int i = 0; i < wordSubCatLayout.size(); i++) {
                if (v == wordSubCatLayout.get(i)) {
                    Intent intent = new Intent(ActHomeWorkQuizzes.this,
                            LearningCenterSchoolCurriculumEquationSolve.class);
                    currentPlayWordCatId = mWordProblemSubCat.get(i).getCatId();
                    currentPlayWordSubCatId = mWordProblemSubCat.get(i)
                            .getSubCatId();
                    intent.putExtra("isFromHomeWork", true);
                    intent.putExtra("categoryId", Integer.parseInt
                            (mWordProblemSubCat.get(i).getCatId()));
                    intent.putExtra("subCategoryId", mWordProblemSubCat.get(i)
                            .getSubCatId());
                    //Log.e(TAG , "Grade " + MathFriendzyHelper.parseInt(grade));
                    intent.putExtra("playerSelectedGrade", MathFriendzyHelper.parseInt(grade));
                    intent.putExtra("HWId", homeWorkQuizzData.getHomeWorkId());
                    intent.putExtra("subCatName",
                            MathFriendzyHelper.
                                    getSchoolCurriculumSubCatNameByCatIdAndSubCatId
                                            (ActHomeWorkQuizzes.this, mWordProblemSubCat.get(i)
                                                            .getCatId(),
                                                    mWordProblemSubCat.get(i).getSubCatId() + ""));
                    startActivityForResult(intent, PLAY_WORD_PROBLEM_REQUEST);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtils.showInternetDialog(ActHomeWorkQuizzes.this);
        }
    }


    /**
     * Add the custom layout with chapter
     *
     * @param arrayList
     * @return
     */
    private LinearLayout addCustomeLayout(final ArrayList<CustomeResult> arrayList) {
        LinearLayout layout = this.getLinearLayout();
        for (int i = 0; i < arrayList.size(); i++) {
            View subCatView = this.getSubCategoryInflatedLayout(3);
            RelativeLayout rllayout = (RelativeLayout) subCatView.findViewById(R.id.layout);
            TextView txtSubCatName = (TextView) subCatView.findViewById(R.id.txtSubCategoryName);
            TextView txtNoOfQuestions = (TextView) subCatView.findViewById(R.id.txtNo);
            TextView txtScore = (TextView) subCatView.findViewById(R.id.txtScore);
            TextView txtNumberOfActiveInput = (TextView) subCatView.findViewById(R.id.txtNumberOfActiveInput);
            txtSubCatName.setText(arrayList.get(i).getTitle());
            txtNoOfQuestions.setText(arrayList.get(i).getProblem() + "");
            txtScore.setText(arrayList.get(i).getScore() + "%");
            layout.addView(subCatView);
            int numberOfOponentInput = arrayList.get(i).getNumberOfActiveInput();
            if (numberOfOponentInput > 0) {
                txtNumberOfActiveInput.setVisibility(TextView.VISIBLE);
                txtNumberOfActiveInput.setText(numberOfOponentInput + "");
            } else {
                txtNumberOfActiveInput.setVisibility(TextView.GONE);
            }
            customSubCatLayout.add(rllayout);
            rllayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*if(isExpireQuizz){
                        showExpireQuizzDialog();
					}else{*/
                    for (int i = 0; i < customSubCatLayout.size(); i++) {
                        if (v == customSubCatLayout.get(i)) {
                            selectedCustomeHWId = arrayList.get(i).getCustomHwId();
                            final CustomeResult customeResult = arrayList.get(i);

                            getAnswerDetailForStudent(new HttpResponseInterface() {
                                @Override
                                public void serverResponse(HttpResponseBase httpResponseBase,
                                                           int requestCode) {
                                    if (httpResponseBase != null && requestCode > 0) {
                                        GetStudentDetailAnswerResponse response
                                                = (GetStudentDetailAnswerResponse) httpResponseBase;
                                        customeResult.setCustomePlayerAnsList
                                                (response.getCustomePlayerAnsList());
                                        customeResult.setScore(response.getScore() + "");
                                        if (response.getCustomePlayerAnsList().size() > 0) {
                                            customeResult.setAlreadyPlayed(true);
                                        }
                                    }

                                    Intent intent = new Intent(ActHomeWorkQuizzes.this,
                                            ActCheckHomeWork.class);
                                    intent.putExtra("teacherName", teacherName);
                                    intent.putExtra("grade", grade);
                                    intent.putExtra("dueDate", dueDate);
                                    intent.putExtra("isExpireQuizz", isExpireQuizz);
                                    intent.putExtra("customeDetail", customeResult);
                                    intent.putExtra("homeWorkQuizzData", homeWorkQuizzData);
                                    startActivityForResult(intent, PLAY_CUSTOM_PROBLEM_REQUEST);
                                }
                            }, homeWorkQuizzData.getHomeWorkId(), customeResult.getCustomHwId());

							/*Intent intent = new Intent(ActHomeWorkQuizzes.this ,
									ActCheckHomeWork.class);
							selectedCustomeHWId = arrayList.get(i).getCustomHwId();
							intent.putExtra("teacherName", teacherName);
							intent.putExtra("grade", grade);
							intent.putExtra("dueDate", dueDate);
							intent.putExtra("isExpireQuizz", isExpireQuizz);
							intent.putExtra("customeDetail", arrayList.get(i));
							intent.putExtra("homeWorkQuizzData", homeWorkQuizzData);
							startActivityForResult(intent , PLAY_CUSTOM_PROBLEM_REQUEST);*/
                        }
                        //}
                    }
                }
            });
        }
        return layout;
    }

    private void getAnswerDetailForStudent(final HttpResponseInterface responseInterface
            , String hwId, int cusHWId) {
        try {
            if (!CommonUtils.isInternetConnectionAvailable(this)) {
                responseInterface.serverResponse(null,
                        MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE);
                return;
            }

            GetAnswerDetailForStudentHWParam param = new GetAnswerDetailForStudentHWParam();
            param.setAction("getAnswersDetailsForStudent");
            param.setUserId(selectedPlayerData.getParentUserId());
            param.setPlayerId(selectedPlayerData.getPlayerid());
            param.setHwId(hwId);
            param.setCusHWId(cusHWId + "");
            MathFriendzyHelper.getAnswerDetailForStudentHW(this, param, new HttpResponseInterface() {
                @Override
                public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                    responseInterface.serverResponse(httpResponseBase, requestCode);
                }
            }, true);
        } catch (Exception e) {
            e.printStackTrace();
            responseInterface.serverResponse(null,
                    MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE);
        }
    }

    private View getCategoryInflatedLayout(int inflateFor) {
        View view = LayoutInflater.from(this)
                .inflate(R.layout.school_home_question_categories_layout, null);
        return view;
    }

    private View getSubCategoryInflatedLayout(int inflateFor) {
        View view = LayoutInflater.from(this)
                .inflate(R.layout.home_work_quizz_sub_catgory_layout, null);
        return view;
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase,
                               int requestCode) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
    }

    /**
     * Update avg score text
     *
     * @param updatedData
     */
    private void updateAvgScore(SaveHomeWorkServerResponse updatedData) {
        try {
            if (updatedData != null) {
                txtAvgScore.setText(avgScoreText + ": " + updatedData.getAvgScore() + "%");
                txtZeroCredit.setText(zeroCrediteText + " " + creditText
                        + ": " + updatedData.getZeroCredit());
                txtHaltCredit.setText(halFCredittext + " "
                        + creditText + ": " + updatedData.getHalfCredit());
                txtFullCredit.setText(fullCreditText + " "
                        + creditText + ": " + updatedData.getFullCredit());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PLAY_PRACTICE_SKILL_REQUEST:
                    isDataUpdatedByPlaying = true;
                    updatedData = (SaveHomeWorkServerResponse) data.getSerializableExtra("resultAfterPlayed");
                    this.updatePracticePlayedScore(updatedData);
                    this.updateAvgScore(updatedData);
                    break;
                case PLAY_WORD_PROBLEM_REQUEST:
                    isDataUpdatedByPlaying = true;
                    updatedData = (SaveHomeWorkServerResponse) data.getSerializableExtra("resultAfterPlayed");
                    this.updateWordPlayedScore(updatedData);
                    this.updateAvgScore(updatedData);
                    break;
                case PLAY_CUSTOM_PROBLEM_REQUEST:
                    boolean isOnlyForOponentInput = data.getBooleanExtra("isOnlyForOponentInput", false);
                    if (isOnlyForOponentInput) {
                        updateNumberOfActiveInputInMainResult(data.getIntExtra("numberOfActiveOponentInput", 0));
                        return;
                    }
                    updatedData = (SaveHomeWorkServerResponse) data.getSerializableExtra("resultAfterPlayed");
                    if (updatedData != null) {
                        isDataUpdatedByPlaying = true;
                    } else {
                        isDataUpdatedByPlaying = false;
                    }
                    ArrayList<CustomePlayerAns> customePlayListByStudent =
                            (ArrayList<CustomePlayerAns>) data.getSerializableExtra("customePlayData");
                    this.updateCustomePlayedScore(updatedData, customePlayListByStudent);
                /*for(int i = 0 ; i < customePlayListByStudent.size() ; i ++ ) {
                    Log.e(TAG, " 1 work area  oponent input" + customePlayListByStudent.get(i).getOpponentInput()
                            + " work input" + customePlayListByStudent.get(i).getWorkInput());
                }*/
                    this.updateAvgScore(updatedData);
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * This method update the current play score for the current selected cat and subCat
     * for the practice skill
     *
     * @param response
     */
    private void updatePracticePlayedScore(SaveHomeWorkServerResponse response) {
        for (int i = 0; i < homeWorkQuizzData.getPractiseResultList().size(); i++) {
            if (currentPracticePlayCatId.
                    equals(homeWorkQuizzData.getPractiseResultList()
                            .get(i).getCatId())) {
                for (int j = 0; j < homeWorkQuizzData.getPractiseResultList()
                        .get(i).getPracticeResultSubCatList().size(); j++) {
                    if (currentPracticePlaySubCatId == homeWorkQuizzData.getPractiseResultList()
                            .get(i).getPracticeResultSubCatList().get(j).getSubCatId()) {
                        homeWorkQuizzData.getPractiseResultList()
                                .get(i).getPracticeResultSubCatList().get(j)
                                .setScore(response.getCurrentPlaySubCatScore());
                    }
                }
            }
        }

        //global update
        for (int i = 0; i < HomeWorkListAdapter.homeWorkresult.
                get(ActHomeWork.selectedPosition).getPractiseResultList().size(); i++) {
            if (currentPracticePlayCatId.
                    equals(HomeWorkListAdapter.homeWorkresult
                            .get(ActHomeWork.selectedPosition).getPractiseResultList()
                            .get(i).getCatId())) {
                for (int j = 0; j < HomeWorkListAdapter.homeWorkresult
                        .get(ActHomeWork.selectedPosition).getPractiseResultList()
                        .get(i).getPracticeResultSubCatList().size(); j++) {
                    if (currentPracticePlaySubCatId == HomeWorkListAdapter.homeWorkresult
                            .get(ActHomeWork.selectedPosition).getPractiseResultList()
                            .get(i).getPracticeResultSubCatList().get(j).getSubCatId()) {
                        HomeWorkListAdapter.homeWorkresult
                                .get(ActHomeWork.selectedPosition).getPractiseResultList()
                                .get(i).getPracticeResultSubCatList().get(j)
                                .setScore(response.getCurrentPlaySubCatScore());
                    }
                }
            }
        }

        this.init();
        this.clearLayout();
        this.setHomeWorkData();

        //for offline mode
        this.savePlayedDataToLocal(response);
    }

    /**
     * This method update the current play score for the current selected cat and subCat
     * for the word problem
     *
     * @param response
     */
    private void updateWordPlayedScore(SaveHomeWorkServerResponse response) {
        for (int i = 0; i < homeWorkQuizzData.getWordResultList().size(); i++) {
            if (currentPlayWordCatId.
                    equals(homeWorkQuizzData.getWordResultList()
                            .get(i).getCatIg())) {
                for (int j = 0; j < homeWorkQuizzData.getWordResultList()
                        .get(i).getSubCatResultList().size(); j++) {
                    if (currentPlayWordSubCatId == homeWorkQuizzData.getWordResultList()
                            .get(i).getSubCatResultList().get(j).getSubCatId()) {
                        homeWorkQuizzData.getWordResultList()
                                .get(i).getSubCatResultList().get(j)
                                .setScore(response.getCurrentPlaySubCatScore());
                    }
                }
            }
        }

        //global update
        for (int i = 0; i < HomeWorkListAdapter.homeWorkresult.get(ActHomeWork.selectedPosition)
                .getWordResultList().size(); i++) {
            if (currentPlayWordCatId.
                    equals(HomeWorkListAdapter.homeWorkresult.get(ActHomeWork.selectedPosition)
                            .getWordResultList()
                            .get(i).getCatIg())) {
                for (int j = 0; j < HomeWorkListAdapter.homeWorkresult.get(ActHomeWork.selectedPosition)
                        .getWordResultList()
                        .get(i).getSubCatResultList().size(); j++) {
                    if (currentPlayWordSubCatId == HomeWorkListAdapter.homeWorkresult.get(ActHomeWork.selectedPosition)
                            .getWordResultList()
                            .get(i).getSubCatResultList().get(j).getSubCatId()) {
                        HomeWorkListAdapter.homeWorkresult.get(ActHomeWork.selectedPosition)
                                .getWordResultList()
                                .get(i).getSubCatResultList().get(j)
                                .setScore(response.getCurrentPlaySubCatScore());

                    }
                }
            }
        }

        this.init();
        this.clearLayout();
        this.setHomeWorkData();

        //for offline mode
        this.savePlayedDataToLocal(response);
    }

    /**
     * Update custome played score
     *
     * @param response
     * @param customePlayListByStudent
     */
    private void updateCustomePlayedScore(SaveHomeWorkServerResponse response,
                                          ArrayList<CustomePlayerAns> customePlayListByStudent) {

        try {
            for (int i = 0; i < homeWorkQuizzData.getCustomeresultList().size(); i++) {
                if (selectedCustomeHWId ==
                        homeWorkQuizzData.getCustomeresultList().get(i).getCustomHwId()) {
                    if (response != null) {
                        homeWorkQuizzData.getCustomeresultList().get(i).setScore
                                (response.getCurrentPlaySubCatScore());
                        homeWorkQuizzData.getCustomeresultList().get(i)
                                .setAlreadyPlayed(true);
                    }
                    homeWorkQuizzData.getCustomeresultList().get(i)
                            .setCustomePlayerAnsList(customePlayListByStudent);
                    homeWorkQuizzData.getCustomeresultList().get(i)
                            .setNumberOfActiveInput(this.getUpdatedNumberOfActiveOponent
                                    (customePlayListByStudent));
                }
            }

            //global update
            for (int i = 0; i < HomeWorkListAdapter.homeWorkresult
                    .get(ActHomeWork.selectedPosition)
                    .getCustomeresultList().size(); i++) {
                if (selectedCustomeHWId ==
                        HomeWorkListAdapter.homeWorkresult.get
                                (ActHomeWork.selectedPosition).
                                getCustomeresultList().get(i)
                                .getCustomHwId()) {
                    if (response != null) {
                        HomeWorkListAdapter.homeWorkresult.get
                                (ActHomeWork.selectedPosition).
                                getCustomeresultList().get(i)
                                .setScore(response.getCurrentPlaySubCatScore());
                        HomeWorkListAdapter.homeWorkresult.get
                                (ActHomeWork.selectedPosition).
                                getCustomeresultList().get(i)
                                .setAlreadyPlayed(true);
                    }
                    HomeWorkListAdapter.homeWorkresult.get
                            (ActHomeWork.selectedPosition).
                            getCustomeresultList().get(i)
                            .setCustomePlayerAnsList(customePlayListByStudent);
                    HomeWorkListAdapter.homeWorkresult.get
                            (ActHomeWork.selectedPosition).
                            getCustomeresultList().get(i)
                            .setNumberOfActiveInput(this
                                    .getUpdatedNumberOfActiveOponent(customePlayListByStudent));
                }
            }

            this.init();
            this.clearLayout();
            this.setHomeWorkData();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update the list and create json string and save into LocalDB
     */
    private void savePlayedDataToLocal(SaveHomeWorkServerResponse response) {
        try {
            MathFriendzyHelper.updateMainHomeworkList(response);
            MathFriendzyHelper.createJsonForHomeworkAndSaveIntoLocal
                    (this, HomeWorkListAdapter.homeWorkresult,
                            selectedPlayerData.getParentUserId()
                            , selectedPlayerData.getPlayerid(), new HttpServerRequest() {
                                @Override
                                public void onRequestComplete() {

                                }
                            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (isDataUpdatedByPlaying) {
            Intent intent = new Intent();
            intent.putExtra("resultAfterPlayed", updatedData);
            setResult(Activity.RESULT_OK, intent);
            finish();
        } else {
            Intent intent = new Intent();
            intent.putExtra("onlyRefreshList", true);
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
        super.onBackPressed();
    }

    /**
     * Return the updated active input
     *
     * @param customePlayListByStudent
     * @return
     */
    private int getUpdatedNumberOfActiveOponent(ArrayList<CustomePlayerAns> customePlayListByStudent) {
        int activeOponentInput = 0;
        try {
            for (int i = 0; i < customePlayListByStudent.size(); i++) {
                if (customePlayListByStudent.get(i).getOpponentInput()
                        .equalsIgnoreCase(MathFriendzyHelper.YES + ""))
                    activeOponentInput++;
                if (customePlayListByStudent.get(i).getWorkInput()
                        .equalsIgnoreCase(MathFriendzyHelper.YES + ""))
                    activeOponentInput++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return activeOponentInput;
    }

    /**
     * Update only active inactive oponent input
     *
     * @param numberOfActiveInActiveInput
     */
    private void updateNumberOfActiveInputInMainResult(int numberOfActiveInActiveInput) {
        try {
            if (numberOfActiveInActiveInput < 0)
                numberOfActiveInActiveInput = 0;

            for (int i = 0; i < homeWorkQuizzData.getCustomeresultList().size(); i++) {
                if (selectedCustomeHWId ==
                        homeWorkQuizzData.getCustomeresultList().get(i).getCustomHwId()) {
                    homeWorkQuizzData.getCustomeresultList().get(i)
                            .setNumberOfActiveInput(numberOfActiveInActiveInput);
                }
            }

            //global update
            for (int i = 0; i < HomeWorkListAdapter.homeWorkresult
                    .get(ActHomeWork.selectedPosition)
                    .getCustomeresultList().size(); i++) {
                if (selectedCustomeHWId ==
                        HomeWorkListAdapter.homeWorkresult.get
                                (ActHomeWork.selectedPosition).
                                getCustomeresultList().get(i)
                                .getCustomHwId()) {
                    HomeWorkListAdapter.homeWorkresult.get
                            (ActHomeWork.selectedPosition).
                            getCustomeresultList().get(i)
                            .setNumberOfActiveInput(numberOfActiveInActiveInput);
                }
            }
            this.init();
            this.clearLayout();
            this.setHomeWorkData();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
