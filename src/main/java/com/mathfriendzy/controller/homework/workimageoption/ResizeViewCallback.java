package com.mathfriendzy.controller.homework.workimageoption;

import android.graphics.Bitmap;
import android.widget.RelativeLayout;

/**
 * Created by root on 1/6/15.
 */
public interface ResizeViewCallback {
    void onResizeDone(int tag , Bitmap bitmap , RelativeLayout.LayoutParams lp);
}
