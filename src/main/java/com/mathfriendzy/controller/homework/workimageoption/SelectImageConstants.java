package com.mathfriendzy.controller.homework.workimageoption;

/**
 * Created by root on 26/5/15.
 */
public class SelectImageConstants {

    public static final int SELECT_FROM_CAMERA = 1;
    public static final int SELECT_FROM_GALLARY = 2;
    public static final int SELECT_FROM_HOMEWORK_SHEET = 3;
    public static final int SELECT_FROM_WORK_AREA_IMAGE = 4;
    public static final int SELECT_WORK_AREA_TEXT = 5;
    public static final int DELETE_IMAGE = 6;
    public static final int DELETE_WORK_AREA_IMAGE = 7;
    public static final int DELETE_WORK_AREA_TEXT = 8;
 }
