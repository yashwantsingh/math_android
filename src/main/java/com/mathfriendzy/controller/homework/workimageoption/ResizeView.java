package com.mathfriendzy.controller.homework.workimageoption;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mathfriendzy.R;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

/**
 * Created by root on 1/6/15.
 */
public class ResizeView implements View.OnClickListener , View.OnTouchListener{

    private RelativeLayout rlResizeLayout = null;
    private RelativeLayout resizeLayout = null;

    private ImageView imgSelectedImage = null;
    private ImageView imgTopLeft = null;
    private ImageView imgTopRight = null;
    private ImageView imgBottomLeft = null;
    private ImageView imgBottomRight = null;
    private Button btnDeleteWithSelectedImage = null;
    private Button btnDoneWithSelectedImage = null;
    private Context context = null;
    private ImageLoader loader;//images loader
    private DisplayImageOptions options; // contains the display option for image in grid
    private ResizeViewCallback callback = null;

    public static final int RESIZE_DONE = 1;
    public static final int RESIZE_DELETE = 2;

    private RelativeLayout studentTeacherLayout = null;
    private RelativeLayout relativeTopBar = null;
    private RelativeLayout bottomLayout = null;

    private int leftMarginLimit = 0;
    private int rightMarginLimit = 0;
    private int topMarginLimit = 0;
    private int bottomMarginLimit = 0;
    private Bitmap bitmap = null;//contains the image bitmap

    private int min_resize_width_limit = 0;
    private int min_resize_height_limit = 0;

    private boolean isResizeLayoutOpen = false;

    public ResizeView(Context context , ResizeViewCallback callback) {
        this.context = context;
        this.callback = callback;
        loader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.ic_empty)
                .showImageOnFail(R.drawable.ic_error)
                .resetViewBeforeLoading(true)
		        /*.cacheOnDisc(true)*/
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .considerExifParams(true)
                .displayer(new FadeInBitmapDisplayer(300))
                .build();
    }

    public void setWidgetsReferences() {
        resizeLayout = (RelativeLayout) ((Activity) context).findViewById(R.id.resizeLayout);
        rlResizeLayout = (RelativeLayout) ((Activity) context).findViewById(R.id.rlResizeLayout);
        imgSelectedImage = (ImageView) ((Activity) context).findViewById(R.id.imgSelectedImage);
        imgTopLeft = (ImageView) ((Activity) context).findViewById(R.id.imgTopLeft);
        imgTopRight = (ImageView) ((Activity) context).findViewById(R.id.imgTopRight);
        imgBottomLeft = (ImageView) ((Activity) context).findViewById(R.id.imgBottomLeft);
        imgBottomRight = (ImageView) ((Activity) context).findViewById(R.id.imgBottomRight);
        btnDeleteWithSelectedImage = (Button) ((Activity) context).findViewById(R.id.btnDeleteWithSelectedImage);
        btnDoneWithSelectedImage = (Button) ((Activity) context).findViewById(R.id.btnDoneWithSelectedImage);
        this.setWidgetsListener();
    }


    public void setWidgetsReferences(View view) {
        resizeLayout = (RelativeLayout) view.findViewById(R.id.resizeLayout);
        rlResizeLayout = (RelativeLayout) view.findViewById(R.id.rlResizeLayout);
        imgSelectedImage = (ImageView) view.findViewById(R.id.imgSelectedImage);
        imgTopLeft = (ImageView) view.findViewById(R.id.imgTopLeft);
        imgTopRight = (ImageView) view.findViewById(R.id.imgTopRight);
        imgBottomLeft = (ImageView) view.findViewById(R.id.imgBottomLeft);
        imgBottomRight = (ImageView) view.findViewById(R.id.imgBottomRight);
        btnDeleteWithSelectedImage = (Button) view.findViewById(R.id.btnDeleteWithSelectedImage);
        btnDoneWithSelectedImage = (Button) view.findViewById(R.id.btnDoneWithSelectedImage);
        this.setWidgetsListener();
    }

    private void setWidgetsListener(){
        btnDeleteWithSelectedImage.setOnClickListener(this);
        btnDoneWithSelectedImage.setOnClickListener(this);
        resizeLayout.setOnTouchListener(this);
        imgTopLeft.setOnTouchListener(this);
        imgTopRight.setOnTouchListener(this);
        imgBottomRight.setOnTouchListener(this);
        imgBottomLeft.setOnTouchListener(this);
    }

    public void setResizeLayoutVisibility(boolean isVisible) {
        if (isVisible) {
            isResizeLayoutOpen = true;
            rlResizeLayout.setVisibility(RelativeLayout.VISIBLE);
        } else {
            isResizeLayoutOpen = false;
            rlResizeLayout.setVisibility(RelativeLayout.GONE);
        }
    }

    /**
     * Set the rlResize Layout margin
     * Newly added
     */
    public void setRlResizeLayoutParam(int leftMargin){
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams)
                rlResizeLayout.getLayoutParams();
        lp.leftMargin = leftMargin;
        resizeLayout.setLayoutParams(lp);
    }

    /**
     * Check for resize image layout open
     * @return
     */
    public boolean isResizeImageLayoutOpen(){
        return isResizeLayoutOpen;
    }

    public void setImageFromUri(String imageUri) {
        this.setHeightAndWidthForResizeLayout(imageUri);
        this.setImageFromUri(imageUri, imgSelectedImage);
    }

    public void setImageFromUri(String imageUri , boolean isDeviceSizeGreater , int maxWidth) {
        this.setHeightAndWidthForResizeLayoutForNewChanges(imageUri, isDeviceSizeGreater , maxWidth);
        this.setImageFromUri(imageUri, imgSelectedImage);
    }

    private int getDecreaseHeightPercentage(int imageHeight , int heightDiff){
        return (heightDiff * 100) / imageHeight;
    }

    private int getNewWidth(int imageWidth , int decreasePercantage){
        return (imageWidth - ((imageWidth * decreasePercantage) / 100));
    }

    /**
     * Set height and width for the resize layout
     * @param imageUri
     */
    private void setHeightAndWidthForResizeLayout(String imageUri){
        if(bitmap != null){
            bitmap.recycle();
            bitmap = null;
        }

        bitmap = MathFriendzyHelper.getFileByFileUri(context, imageUri);
        int width  = bitmap.getWidth();
        int height = bitmap.getHeight();
        int heightDiff = height - bottomMarginLimit;

        if(heightDiff > 0){
            width = this.getNewWidth(width , this.
                    getDecreaseHeightPercentage(height , heightDiff));
        }else {
            if (width > rightMarginLimit) {
                width = rightMarginLimit;
            }
        }

        if(height > bottomMarginLimit){
            height = bottomMarginLimit;
        }

        RelativeLayout.LayoutParams lp =
                new RelativeLayout.LayoutParams(width, height);
        min_resize_width_limit = (int)context.getResources()
                .getDimension(R.dimen.resize_layout_min_width);
        min_resize_height_limit = (int)context.getResources()
                .getDimension(R.dimen.resize_layout_min_height);
        resizeLayout.setLayoutParams(lp);
    }

    /**
     * Set height and width for the resize layout
     * @param imageUri
     */
    private void setHeightAndWidthForResizeLayoutForNewChanges(String imageUri ,
           boolean isDeviceSizeGreaterThanDraViewLimit , int maxWidth){
        if(bitmap != null){
            bitmap.recycle();
            bitmap = null;
        }
        bitmap = MathFriendzyHelper.getFileByFileUri(context, imageUri);
        int width  = bitmap.getWidth();
        int height = bitmap.getHeight();
        int heightDiff = height - bottomMarginLimit;

        if(heightDiff > 0){
            width = this.getNewWidth(width , this.
                    getDecreaseHeightPercentage(height , heightDiff));
        }else {
            if(isDeviceSizeGreaterThanDraViewLimit){
                if(width > maxWidth){
                    width = maxWidth;
                }
            }else {
                if (width > rightMarginLimit) {
                    width = rightMarginLimit;
                }
            }
        }

        if(height > bottomMarginLimit){
            height = bottomMarginLimit;
        }

        RelativeLayout.LayoutParams lp =
                new RelativeLayout.LayoutParams(width, height);
        min_resize_width_limit = (int)context.getResources()
                .getDimension(R.dimen.resize_layout_min_width);
        min_resize_height_limit = (int)context.getResources()
                .getDimension(R.dimen.resize_layout_min_height);
        resizeLayout.setLayoutParams(lp);
    }


    /**
     * For loading image from uri
     *
     * @param uri
     * @param imageView
     */
    private void setImageFromUri(String uri, ImageView imageView) {
        if (!uri.startsWith(MathFriendzyHelper.IMAGE_FILE_PREFF))
            uri = MathFriendzyHelper.IMAGE_FILE_PREFF + uri;
        loader.displayImage(uri, imageView, options,
                new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String arg0, View arg1) {
                    }
                    @Override
                    public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
                    }
                    @Override
                    public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
                    }
                    @Override
                    public void onLoadingCancelled(String arg0, View arg1) {
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnDoneWithSelectedImage:
                if(MathFriendzyHelper.getMaxImageByteSizeInMb(bitmap)
                        > MathFriendzyHelper.MAX_IMAGE_BYTE_SIZE_IN_MB){
                    String message = MathFriendzyHelper.getTreanslationTextById(this.context ,
                            "lblYourFileCantLarger");
                    MathFriendzyHelper.showWarningDialog(this.context , message);
                    return;
                }
                callback.onResizeDone(RESIZE_DONE , bitmap , (RelativeLayout.LayoutParams)
                        resizeLayout.getLayoutParams());
                break;
            case R.id.btnDeleteWithSelectedImage:
                callback.onResizeDone(RESIZE_DELETE , null , null);
                break;
        }
    }

    public void setOtherView(RelativeLayout studentTeacherLayout ,
                             RelativeLayout relativeTopBar ,
                             RelativeLayout bottomLayout){
        this.studentTeacherLayout = studentTeacherLayout;
        this.relativeTopBar = relativeTopBar;
        this.bottomLayout = bottomLayout;
        this.initializeMarginLimits();
    }

    private void initializeMarginLimits(){
        topMarginLimit = 0;
        DisplayMetrics metrics = MathFriendzyHelper.getDeviceDimention(context);
        rightMarginLimit = metrics.widthPixels;
        bottomMarginLimit = metrics.heightPixels;
        //for initializing bottom limit
        this.initializeHeightAndWidth(studentTeacherLayout);
        if(this.relativeTopBar != null)
            this.initializeHeightAndWidth(relativeTopBar);
        this.initializeHeightAndWidth(bottomLayout);
    }

    /**
     * Initialize laft right margin limit
     * Newly added
     * @param leftMargin
     * @param rightMargin
     */
    public void initializeLeftAndRightMarginLimit(int leftMargin
            , int rightMargin){
        leftMarginLimit = leftMargin;
        rightMarginLimit = rightMargin;
    }

    private float mX, mY;
    @Override
    public boolean onTouch(View view, MotionEvent event) {
        float x = event.getRawX();
        float y = event.getRawY();
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view.getLayoutParams();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mX = x;
                mY = y;
                break;
            case MotionEvent.ACTION_MOVE:
                switch(view.getId()){
                    case R.id.resizeLayout:
                        this.moveResizeImageLayout(lp , view , x , y);
                        break;
                    case R.id.imgTopLeft:
                        this.resizeImageViewOnTopLeft(lp , view , x , y);
                        break;
                    case R.id.imgTopRight:
                        this.resizeImageViewOnTopRight(lp , view ,x , y);
                        break;
                    case R.id.imgBottomRight:
                        this.resizeImageViewOnBottomRight(x , y);
                        break;
                    case R.id.imgBottomLeft:
                        this.resizeImageViewOnBottomLeft(x , y);
                        break;

                }
                break;
            case MotionEvent.ACTION_UP:
                break;
        }
        return true;
    }

    /**
     * Move resize image layout
     * @param lp
     * @param view
     * @param x
     * @param y
     */
    private void moveResizeImageLayout(RelativeLayout.LayoutParams lp , View view , float x , float y){
        float dx = x - mX;
        float dy = y - mY;

        lp.topMargin = lp.topMargin + (int)dy;
        lp.leftMargin = lp.leftMargin + (int)dx;

        mX = x;
        mY = y;

        if(lp.leftMargin < leftMarginLimit) {//for top bound
            lp.leftMargin = leftMarginLimit;
        }

        if(lp.topMargin < topMarginLimit){//for left bound
            lp.topMargin = topMarginLimit;
        }

        if((lp.leftMargin + view.getWidth())//for right bound
                > rightMarginLimit){
            lp.leftMargin = rightMarginLimit - view.getWidth();
        }

        if(lp.topMargin + view.getHeight() >= bottomMarginLimit){//for bottom bound
            lp.topMargin = bottomMarginLimit - view.getHeight();
        }
        view.setLayoutParams(lp);
    }


    /**
     * Return the resize layout params
     * @return
     */
    private RelativeLayout.LayoutParams getResizeLayoutParam(){
        return (RelativeLayout.LayoutParams) resizeLayout.getLayoutParams();
    }

    /**
     * Set the resize layout params
     * @param lp
     */
    private void setResizeLayoutParams(RelativeLayout.LayoutParams lp){
        resizeLayout.setLayoutParams(lp);
    }

    private static final float TOUCH_TOLERANCE = 5;
    private int DEFAULT_MOVE = 5;

    /**
     * Check for minimum resize limit
     * @param width
     * @param height
     * @return
     */
    private boolean isResizeView(int width , int height){
        if(width <= min_resize_width_limit
                || height <= min_resize_height_limit){
            return false;
        }
        return true;
    }

    /**
     * Resize image on top left
     * @param x
     * @param y
     */
    private void resizeImageViewOnTopLeft(RelativeLayout.LayoutParams viewlp ,
                                          View view , float x , float y){
        float dx = x - mX;
        float dy = y - mY;
        int tempLeftMargin = 0;
        int tempTopMargin = 0;
        int tempHeight = 0;
        int tempWidth = 0;

        if (((mX > x && mY > y) || (mX < x && mY < y))
                && Math.abs(dx) >= TOUCH_TOLERANCE && Math.abs(dy) >= TOUCH_TOLERANCE){
            RelativeLayout.LayoutParams lp = this.getResizeLayoutParam();

            if (Math.abs(dx) > Math.abs(dy)) {
                DEFAULT_MOVE = (int) Math.abs(dx);
            } else {
                DEFAULT_MOVE = (int) Math.abs(dy);
            }

            if ((mX > x && mY > y)) {
                tempTopMargin = lp.topMargin - DEFAULT_MOVE;
                tempLeftMargin = lp.leftMargin - DEFAULT_MOVE;
                tempHeight = lp.height + DEFAULT_MOVE;
                tempWidth = lp.width + DEFAULT_MOVE;
            } else {
                tempTopMargin = lp.topMargin + DEFAULT_MOVE;
                tempLeftMargin = lp.leftMargin + DEFAULT_MOVE;
                tempHeight = lp.height - DEFAULT_MOVE;
                tempWidth = lp.width - DEFAULT_MOVE;
            }

            if(tempTopMargin  < topMarginLimit
                    || tempLeftMargin < leftMarginLimit){
                if(tempTopMargin  < topMarginLimit) {
                    lp.topMargin = 0;
                }
                if(tempLeftMargin < leftMarginLimit) {
                    lp.leftMargin = leftMarginLimit;
                }
            }else{
                if(this.isResizeView(tempWidth , tempHeight)) {
                    if ((mX > x && mY > y)) {
                        lp.topMargin = lp.topMargin - DEFAULT_MOVE;
                        lp.leftMargin = lp.leftMargin - DEFAULT_MOVE;
                        lp.height = lp.height + DEFAULT_MOVE;
                        lp.width = lp.width + DEFAULT_MOVE;
                    } else {
                        lp.topMargin = lp.topMargin + DEFAULT_MOVE;
                        lp.leftMargin = lp.leftMargin + DEFAULT_MOVE;
                        lp.height = lp.height - DEFAULT_MOVE;
                        lp.width = lp.width - DEFAULT_MOVE;
                    }
                }
            }
            this.setResizeLayoutParams(lp);
        }

        mX = x;
        mY = y;
    }

    /**
     * Resize image on top right
     * @param x
     * @param y
     */
    private void resizeImageViewOnTopRight(RelativeLayout.LayoutParams viewlp ,
                                           View view ,float x , float y){
        float dx = x - mX;
        float dy = y - mY;
        int tempRightMargin = 0;
        int tempTopMargin = 0;
        int tempHeight = 0;
        int tempWidth = 0;

        if (((mX < x && mY > y) || (mX > x && mY < y))
                && Math.abs(dx) >= TOUCH_TOLERANCE && Math.abs(dy) >= TOUCH_TOLERANCE) {
            RelativeLayout.LayoutParams lp = this.getResizeLayoutParam();
            if(Math.abs(dx) > Math.abs(dy)){
                DEFAULT_MOVE = (int)Math.abs(dx);
            }else{
                DEFAULT_MOVE = (int)Math.abs(dy);
            }


            if ((mX < x && mY > y)) {
                tempTopMargin = lp.topMargin - DEFAULT_MOVE;
                tempRightMargin = lp.leftMargin + resizeLayout.getWidth();
                tempHeight = lp.height + DEFAULT_MOVE;
                tempWidth = lp.width + DEFAULT_MOVE;
            } else {
                tempTopMargin = lp.topMargin + DEFAULT_MOVE;
                tempRightMargin = lp.leftMargin + resizeLayout.getWidth();
                tempHeight = lp.height - DEFAULT_MOVE;
                tempWidth = lp.width - DEFAULT_MOVE;
            }

            if(tempTopMargin < topMarginLimit){
                if(tempTopMargin  < topMarginLimit) {
                    lp.topMargin = 0;
                }
            }else {
                if(this.isResizeView(tempWidth , tempHeight)) {
                    if ((mX < x && mY > y)) {
                        if (!(tempRightMargin >= rightMarginLimit)) {
                            lp.topMargin = lp.topMargin - DEFAULT_MOVE;
                            lp.height = lp.height + DEFAULT_MOVE;
                            lp.width = lp.width + DEFAULT_MOVE;
                        } else {
                            lp.leftMargin = rightMarginLimit -
                                    resizeLayout.getWidth();
                        }
                    } else {
                        lp.topMargin = lp.topMargin + DEFAULT_MOVE;
                        lp.height = lp.height - DEFAULT_MOVE;
                        lp.width = lp.width - DEFAULT_MOVE;
                    }
                }
            }
            this.setResizeLayoutParams(lp);
        }

        mX = x;
        mY = y;
    }

    /**
     * Resize image on bottom left
     * @param x
     * @param y
     */
    private void resizeImageViewOnBottomLeft(float x , float y){
        float dx = x - mX;
        float dy = y - mY;
        int tempLeftMargin = 0;
        int tempBottomMargin = 0;
        int tempHeight = 0;
        int tempWidth = 0;

        if (((mX < x && mY > y) || (mX > x && mY < y))
                && Math.abs(dx) >= TOUCH_TOLERANCE && Math.abs(dy) >= TOUCH_TOLERANCE) {
            RelativeLayout.LayoutParams lp = this.getResizeLayoutParam();

            if(Math.abs(dx) > Math.abs(dy)){
                DEFAULT_MOVE = (int)Math.abs(dx);
            }else{
                DEFAULT_MOVE = (int)Math.abs(dy);
            }

            if((mX < x && mY > y)){
                tempLeftMargin = lp.leftMargin + DEFAULT_MOVE;
                tempBottomMargin = lp.topMargin + resizeLayout.getHeight();
                tempHeight = lp.height - DEFAULT_MOVE;
                tempWidth = lp.width - DEFAULT_MOVE;
            }else{
                tempLeftMargin = lp.leftMargin - DEFAULT_MOVE;
                tempBottomMargin = lp.topMargin + resizeLayout.getHeight();
                tempHeight = lp.height + DEFAULT_MOVE;
                tempWidth = lp.width + DEFAULT_MOVE;
            }

            if(tempLeftMargin < leftMarginLimit){
                if(tempLeftMargin < leftMarginLimit)
                    lp.leftMargin = leftMarginLimit;
            }else{
                if(this.isResizeView(tempWidth , tempHeight)) {
                    if ((mX < x && mY > y)) {
                        lp.height = lp.height - DEFAULT_MOVE;
                        lp.leftMargin = lp.leftMargin + DEFAULT_MOVE;
                        lp.width = lp.width - DEFAULT_MOVE;
                    } else {
                        if (!(tempBottomMargin >= bottomMarginLimit)) {
                            lp.height = lp.height + DEFAULT_MOVE;
                            lp.leftMargin = lp.leftMargin - DEFAULT_MOVE;
                            lp.width = lp.width + DEFAULT_MOVE;
                        } else {
                            lp.topMargin = bottomMarginLimit - resizeLayout.getHeight();
                        }
                    }
                }
            }
            this.setResizeLayoutParams(lp);
        }
        mX = x;
        mY = y;
    }

    /**
     * Resize image on bottom right
     * @param x
     * @param y
     */
    private void resizeImageViewOnBottomRight(float x , float y){
        float dx = x - mX;
        float dy = y - mY;
        int tempRightMargin = 0;
        int tempBottomMargin = 0;
        int tempHeight = 0;
        int tempWidth = 0;

        if (((mX > x && mY > y) || (mX < x && mY < y))
                && Math.abs(dx) >= TOUCH_TOLERANCE && Math.abs(dy) >= TOUCH_TOLERANCE) {
            RelativeLayout.LayoutParams lp = this.getResizeLayoutParam();

            if(Math.abs(dx) > Math.abs(dy)){
                DEFAULT_MOVE = (int)Math.abs(dx);
            }else{
                DEFAULT_MOVE = (int)Math.abs(dy);
            }

            if((mX > x && mY > y)){
                tempBottomMargin = lp.topMargin + resizeLayout.getHeight();
                tempRightMargin = lp.leftMargin + resizeLayout.getWidth();
                tempHeight = lp.height - DEFAULT_MOVE;
                tempWidth = lp.width - DEFAULT_MOVE;
            }else{
                tempBottomMargin = lp.topMargin + resizeLayout.getHeight();
                tempRightMargin = lp.leftMargin + resizeLayout.getWidth();
                tempHeight = lp.height + DEFAULT_MOVE;
                tempWidth = lp.width + DEFAULT_MOVE;
            }

            if((mX > x && mY > y)){
                if(this.isResizeView(tempWidth , tempHeight)) {
                    lp.height = lp.height - DEFAULT_MOVE;
                    lp.width = lp.width - DEFAULT_MOVE;
                }
            }else{

                if (tempRightMargin >= rightMarginLimit
                        || tempBottomMargin >= bottomMarginLimit) {
                    if (tempRightMargin >= rightMarginLimit) {
                        lp.leftMargin = rightMarginLimit -
                                resizeLayout.getWidth();
                    }
                    if (tempBottomMargin >= bottomMarginLimit) {
                        lp.topMargin = bottomMarginLimit -
                                resizeLayout.getHeight();
                    }
                } else {
                    if(this.isResizeView(tempWidth , tempHeight)) {
                        lp.height = lp.height + DEFAULT_MOVE;
                        lp.width = lp.width + DEFAULT_MOVE;
                    }
                }
            }
            this.setResizeLayoutParams(lp);
        }

        mX = x;
        mY = y;
    }

    private void initializeHeightAndWidth(final View view){
        view.getViewTreeObserver().addOnGlobalLayoutListener
                (new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        }
                        else {
                            view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        }
                        bottomMarginLimit = bottomMarginLimit - view.getHeight();
                    }
                });
    }
}
