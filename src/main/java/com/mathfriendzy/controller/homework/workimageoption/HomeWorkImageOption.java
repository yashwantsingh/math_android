package com.mathfriendzy.controller.homework.workimageoption;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.homework.addhomeworksheet.ActViewPdf;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.OnRequestSuccess;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.pdfoperation.PDFOperation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;

import java.util.ArrayList;

/**
 * Created by root on 26/5/15.
 */
public class HomeWorkImageOption {

    private boolean isShowGallaryOption = false;
    private boolean isShowCameraOption = false;
    private boolean isShowViewHomeWorkSheetOption = false;
    private boolean isShowWorkImageOption = false;
    private boolean isShowWorkTextOption = false;

    /*private boolean isImageSelected = false;
    private boolean isWorkImageSelected = false;
    private boolean isWorkTextSelected = false;*/

    private String camera = "Camera";
    private String gallary = "Gallary";
    private String workAreaText = "Work Area Text";
    private String workAreaDrawing = "Work Area Drawing";
    private String homeworkSheet = "HomeworkSheet";
    private String deleteImage = "Delete Image";
    private String deleteWorkArea = "Delete Work Area";
    private String deleteWorkAreaText = "Delete Work Area Text";
    private String attachFormTitle = "Add Image From";

    private final int YES = 1;
    private final int NO = 0;

    private final int CAMERA = 0;
    private final int GALLARY = 1;
    private final int HOMEWORK_SHEET = 2;
    private final int WORK_AREA_IMAGE = 3;
    private final int WORK_AREA_TEXT = 4;
    private final int DELETE_IMAGE = 5;
    private final int DELETE_WORK_AREA_IMAGE = 6;
    private final int DELETE_WORK_AREA_TEXT = 7;

    private ArrayList<SelectImageOption> options
            = new ArrayList<SelectImageOption>();

    private Context context = null;
    //private  Dialog dialog = null;

    private final int NOT_SELECTED = 0;
    private OptionAdapter adapter = null;
    private HomeworkImageOptionCallback callback = null;

    //start activity request code
    public static final int PICK_IMAGE_GALLERY = 100001;
    public static final int PICK_CAMERA_IMAGE = 100002;
    public static final int PICK_HOME_WORK_IMAGE = 100003;

    private final String TAG = this.getClass().getSimpleName();
    private static Uri cameraImageUri = null;
    private String homeworkSheetName = null;

    public HomeWorkImageOption(Context context ,
                               HomeworkImageOptionCallback callback){
        this.context = context;
        this.callback = callback;
        this.initializeText();
    }

    public void setShowOption( boolean isShowGallaryOption,
                               boolean isShowCameraOption,
                               boolean isShowViewHomeWorkSheetOption ,
                               boolean isShowWorkImageOption ,
                               boolean isShowWorkTextOption ){
        this.isShowCameraOption = isShowCameraOption;
        this.isShowGallaryOption = isShowGallaryOption;
        this.isShowViewHomeWorkSheetOption = isShowViewHomeWorkSheetOption;
        this.isShowWorkImageOption = isShowWorkImageOption;
        this.isShowWorkTextOption = isShowWorkTextOption;

        if(this.isShowCameraOption)
            this.addOptions(this.isShowCameraOption , SelectImageConstants.SELECT_FROM_CAMERA , camera ,
                    CAMERA ,YES);
        else
            this.addOptions(this.isShowCameraOption , SelectImageConstants.SELECT_FROM_CAMERA , camera ,
                    CAMERA , NO);

        if(this.isShowGallaryOption)
            this.addOptions(this.isShowGallaryOption , SelectImageConstants.SELECT_FROM_GALLARY , gallary ,
                    GALLARY , YES);
        else
            this.addOptions(this.isShowGallaryOption , SelectImageConstants.SELECT_FROM_GALLARY , gallary ,
                    GALLARY , NO);

        if(this.isShowViewHomeWorkSheetOption)
            this.addOptions(this.isShowViewHomeWorkSheetOption ,
                    SelectImageConstants.SELECT_FROM_HOMEWORK_SHEET , homeworkSheet , HOMEWORK_SHEET , YES);
        else
            this.addOptions(this.isShowViewHomeWorkSheetOption ,
                    SelectImageConstants.SELECT_FROM_HOMEWORK_SHEET , homeworkSheet , HOMEWORK_SHEET , NO);

        if(this.isShowWorkImageOption)
            this.addOptions(this.isShowWorkImageOption ,
                    SelectImageConstants.SELECT_FROM_WORK_AREA_IMAGE , workAreaDrawing , WORK_AREA_IMAGE ,
                    YES);
        else
            this.addOptions(this.isShowWorkImageOption ,
                    SelectImageConstants.SELECT_FROM_WORK_AREA_IMAGE , workAreaDrawing , WORK_AREA_IMAGE ,
                    NO);

        if(this.isShowWorkTextOption)
            this.addOptions(this.isShowWorkTextOption ,
                    SelectImageConstants.SELECT_WORK_AREA_TEXT , workAreaText , WORK_AREA_TEXT , YES);
        else
            this.addOptions(this.isShowWorkTextOption ,
                    SelectImageConstants.SELECT_WORK_AREA_TEXT , workAreaText , WORK_AREA_TEXT , NO);

        this.addOptions(true,
                SelectImageConstants.DELETE_IMAGE , deleteImage , DELETE_IMAGE , NO);
        this.addOptions(true ,
                SelectImageConstants.DELETE_WORK_AREA_IMAGE , deleteWorkArea , DELETE_WORK_AREA_IMAGE , NO);
        this.addOptions(true ,
                SelectImageConstants.DELETE_WORK_AREA_TEXT , deleteWorkAreaText , DELETE_WORK_AREA_TEXT , NO);
    }

    private void addOptions(boolean bValue , int id , String name , int index , int isVisible){
        //if(bValue) {
        SelectImageOption option = new SelectImageOption();
        option.setId(id);
        option.setOptionName(name);
        option.setIsVisible(isVisible);
        //options.add(index , option);
        options.add(option);
        //}
    }

    private void initializeText(){
        Translation translate = new Translation(context);
        translate.openConnection();
        camera = translate.getTranselationTextByTextIdentifier("lblCamera");
        gallary = translate.getTranselationTextByTextIdentifier("lblGallery");
        workAreaText = translate.getTranselationTextByTextIdentifier("titleWorkArea") + " "
                + translate.getTranselationTextByTextIdentifier("lblText");
        workAreaDrawing = translate.getTranselationTextByTextIdentifier("titleWorkArea") + " "
                + translate.getTranselationTextByTextIdentifier("lblDrawing");
        homeworkSheet = translate.getTranselationTextByTextIdentifier("lblHomework") + " "
                + translate.getTranselationTextByTextIdentifier("lblSheet");
        deleteImage = translate.getTranselationTextByTextIdentifier("btnTitleDelete") + " "
                + translate.getTranselationTextByTextIdentifier("lblImage");
        deleteWorkArea = translate.getTranselationTextByTextIdentifier("btnTitleDelete") + " "
                + translate.getTranselationTextByTextIdentifier("titleWorkArea");
        deleteWorkAreaText = translate.getTranselationTextByTextIdentifier("btnTitleDelete") + " "
                + translate.getTranselationTextByTextIdentifier("titleWorkArea")+ " "
                + translate.getTranselationTextByTextIdentifier("lblText");
        attachFormTitle = translate.getTranselationTextByTextIdentifier("attachFormTitle");
        translate.closeConnection();
    }

    private Activity getActivityObj(){
        try {
            if (context != null)
                return (Activity) context;
            return null;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * On Option selection
     * @param option
     */
    private void onOptionSelect(SelectImageOption option){
        try {
            switch (option.getId()) {
                case SelectImageConstants.SELECT_FROM_GALLARY:
                    Intent galleryIntent = new Intent();
                    galleryIntent.setType("image/*");
                    galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                    this.getActivityObj().startActivityForResult(Intent.createChooser
                                    (galleryIntent, "Select Picture"),
                            HomeWorkImageOption.PICK_IMAGE_GALLERY);
                    break;
                case SelectImageConstants.SELECT_FROM_CAMERA:
                    Uri fileUri = MathFriendzyHelper.getOutputMediaFileUri
                            (MathFriendzyHelper.MEDIA_TYPE_IMAGE);// create a file to save the image
                    if(fileUri != null) {
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        cameraImageUri = fileUri;
                        // set the image file name
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                        this.getActivityObj().startActivityForResult(cameraIntent,
                                PICK_CAMERA_IMAGE);
                    }else{
                        MathFriendzyHelper.showWarningDialog(context ,
                                "Failed To Create Directory...");
                    }
                    break;
                case SelectImageConstants.SELECT_FROM_HOMEWORK_SHEET:
                    if(this.homeworkSheetName != null) {
                        if (PDFOperation.checkForExistanceOfPdf(this.getActivityObj(),
                                this.homeworkSheetName) /*&& (!PDFOperation.isFileCorrupted(this.homeworkSheetName
                                , this.getActivityObj()))*/) {
                            PDFOperation.isFileCorrupted(this.homeworkSheetName , getActivityObj()
                                    , new OnRequestSuccess() {
                                @Override
                                public void onRequestSuccess(boolean isSuccess) {
                                    CommonUtils.printLog(TAG , "inside clickOnViewSheet "
                                            + isSuccess);
                                    if(isSuccess){//true if file corrupted
                                        downloadPdf(homeworkSheetName);
                                    }else{
                                        openHomeworkSheetActivity(homeworkSheetName);
                                    }
                                }
                            });
                            //openHomeworkSheetActivity(homeworkSheetName);
                        } else {
                            downloadPdf(this.homeworkSheetName);
                            /*if (CommonUtils.isInternetConnectionAvailable(this.getActivityObj())) {
                                PDFOperation.downloadPdf(this.getActivityObj(),
                                        ICommonUtils.DOWNLOAD_WORK_PDF_URL,
                                        this.homeworkSheetName, new OnRequestSuccess() {
                                            @Override
                                            public void onRequestSuccess(boolean isSuccess) {
                                                if(isSuccess) {
                                                    openHomeworkSheetActivity(homeworkSheetName);
                                                }else{
                                                    MathFriendzyHelper.showWarningDialog(getActivityObj() ,
                                                            MathFriendzyHelper.
                                                                    getTreanslationTextById(getActivityObj(),
                                                                            "alertNoHomeworkSheetAttached"));
                                                }
                                            }
                                        });
                            } else {
                                CommonUtils.showInternetDialog(this.getActivityObj());
                            }*/
                        }
                    }
                    break;
                case SelectImageConstants.SELECT_FROM_WORK_AREA_IMAGE:
                     break;
                case SelectImageConstants.SELECT_WORK_AREA_TEXT:
                    break;
                case SelectImageConstants.DELETE_IMAGE:
                    break;
                case SelectImageConstants.DELETE_WORK_AREA_IMAGE:
                    break;
                case SelectImageConstants.DELETE_WORK_AREA_TEXT:
                    break;
            }
            if (callback != null)
                callback.onOptionSelect(option);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void downloadPdf(final String pdfFileName){
        if (CommonUtils.isInternetConnectionAvailable(this.getActivityObj())) {
            PDFOperation.downloadPdf(this.getActivityObj(),
                    ICommonUtils.DOWNLOAD_WORK_PDF_URL,
                    pdfFileName, new OnRequestSuccess() {
                        @Override
                        public void onRequestSuccess(boolean isSuccess) {
                            if(isSuccess) {
                                openHomeworkSheetActivity(pdfFileName);
                            }else{
                                MathFriendzyHelper.showWarningDialog(getActivityObj() ,
                                        MathFriendzyHelper.
                                                getTreanslationTextById(getActivityObj(),
                                                        "alertNoHomeworkSheetAttached"));
                            }
                        }
                    });
        } else {
            CommonUtils.showInternetDialog(this.getActivityObj());
        }
    }

    public void setHomeworkSheetName(String homeworkSheetName){
        this.homeworkSheetName = homeworkSheetName;
    }

    private void openHomeworkSheetActivity(String pdfFileName){
        Intent intent = new Intent(this.getActivityObj() , ActViewPdf.class);
        intent.putExtra("pdfFileName",pdfFileName);
        intent.putExtra("isForImageSelection" , true);
        this.getActivityObj().startActivityForResult(intent, PICK_HOME_WORK_IMAGE);
    }

    public static Uri getCameraImageUri(){
        return cameraImageUri;
    }

    private ArrayList<SelectImageOption> getAdapterList(){
        ArrayList<SelectImageOption> adapterList =
                new ArrayList<SelectImageOption>();
        for(int i = 0 ; i < options.size() ; i ++ ){
            if(options.get(i).getIsVisible() == YES){
                adapterList.add(options.get(i));
            }
        }
        return adapterList;
    }

    public void showOptionDialog(){
        final Dialog dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.work_area_image_selection_dialog);
        dialog.show();
        ListView lstOptions = (ListView) dialog.findViewById(R.id.lstOptions);
        Button btncancel = (Button) dialog.findViewById(R.id.btncancel);
        TextView txtAddForm = (TextView) dialog.findViewById(R.id.txtAddForm);
        txtAddForm.setText(attachFormTitle);
        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        adapter = new OptionAdapter(context , this.getAdapterList());
        lstOptions.setAdapter(adapter);
        lstOptions.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                dialog.dismiss();
                onOptionSelection(adapter.options.get(position));
            }
        });
    }

    /**
     * Set work area text visibility option
     * @param visibility
     */
    public void setVisibilityOfWorkAreaTextOption(int visibility){
        options.get(WORK_AREA_TEXT).setIsVisible(visibility);
    }

    public void setVisibleOptionsWhenCameraSelected(){
        options.get(CAMERA).setIsVisible(NO);
        options.get(GALLARY).setIsVisible(NO);
        options.get(HOMEWORK_SHEET).setIsVisible(NO);
        options.get(DELETE_IMAGE).setIsVisible(YES);
    }

    public void setVisibilityOptionsWhenGallarySelected(){
        options.get(CAMERA).setIsVisible(NO);
        options.get(GALLARY).setIsVisible(NO);
        options.get(HOMEWORK_SHEET).setIsVisible(NO);
        options.get(DELETE_IMAGE).setIsVisible(YES);
    }

    public void setVisibilityOptionsWhenWorkSheetSelected(){
        options.get(CAMERA).setIsVisible(NO);
        options.get(GALLARY).setIsVisible(NO);
        options.get(HOMEWORK_SHEET).setIsVisible(NO);
        options.get(DELETE_IMAGE).setIsVisible(YES);
    }

    public void setVisibilityOptionsWhenWorkAreaImageSelected(){
        options.get(WORK_AREA_IMAGE).setIsVisible(NO);
        options.get(DELETE_WORK_AREA_IMAGE).setIsVisible(YES);
    }

    public void setVisibilityOptionsWhenWorkAreaTextSelected(){
        /*options.get(WORK_AREA_TEXT).setIsVisible(NO);
        options.get(DELETE_WORK_AREA_TEXT).setIsVisible(YES);*/
    }

    public void setVisibilityOptionsWhenDeleteImageSelected(){
        if(this.isShowCameraOption)
            options.get(CAMERA).setIsVisible(YES);
        if(this.isShowGallaryOption)
            options.get(GALLARY).setIsVisible(YES);
        if(this.isShowViewHomeWorkSheetOption)
            options.get(HOMEWORK_SHEET).setIsVisible(YES);
        options.get(DELETE_IMAGE).setIsVisible(NO);
    }

    public void setVisibilityOptionsWhenDeleteWorkAreaImageSelected(){
        if(this.isShowWorkImageOption)
            options.get(WORK_AREA_IMAGE).setIsVisible(YES);
        options.get(DELETE_WORK_AREA_IMAGE).setIsVisible(NO);
    }

    public void setVisibilityOptionsWhenDeleteWorkAreaTextSelected(){
        if(this.isShowWorkTextOption)
            options.get(WORK_AREA_TEXT).setIsVisible(YES);
        options.get(DELETE_WORK_AREA_TEXT).setIsVisible(NO);
    }

    private void onOptionSelection(SelectImageOption option){
        try {
            switch (option.getId()) {
               /* case SelectImageConstants.SELECT_FROM_GALLARY:
                    this.setVisibilityOptionsWhenGallarySelected();
                    break;
                case SelectImageConstants.SELECT_FROM_CAMERA:
                    this.setVisibleOptionsWhenCameraSelected();
                    break;
                case SelectImageConstants.SELECT_FROM_HOMEWORK_SHEET:
                    this.setVisibilityOptionsWhenWorkSheetSelected();
                    break;*/
                case SelectImageConstants.SELECT_FROM_WORK_AREA_IMAGE:
                    this.setVisibilityOptionsWhenWorkAreaImageSelected();
                    break;
                case SelectImageConstants.SELECT_WORK_AREA_TEXT:
                    this.setVisibilityOptionsWhenWorkAreaTextSelected();
                    break;
                case SelectImageConstants.DELETE_IMAGE:
                    this.setVisibilityOptionsWhenDeleteImageSelected();
                    break;
                case SelectImageConstants.DELETE_WORK_AREA_IMAGE:
                    this.setVisibilityOptionsWhenDeleteWorkAreaImageSelected();
                    break;
                case SelectImageConstants.DELETE_WORK_AREA_TEXT:
                    this.setVisibilityOptionsWhenDeleteWorkAreaTextSelected();
                    break;
            }
            this.onOptionSelect(option);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Set visibility after option selection done
     * @param option
     */
    public void setVisibilityAfterOptionSelectionDone(SelectImageOption option){
        try {
            switch (option.getId()) {
                case SelectImageConstants.SELECT_FROM_GALLARY:
                    this.setVisibilityOptionsWhenGallarySelected();
                    break;
                case SelectImageConstants.SELECT_FROM_CAMERA:
                    this.setVisibleOptionsWhenCameraSelected();
                    break;
                case SelectImageConstants.SELECT_FROM_HOMEWORK_SHEET:
                    this.setVisibilityOptionsWhenWorkSheetSelected();
                    break;
                case SelectImageConstants.SELECT_FROM_WORK_AREA_IMAGE:
                    this.setVisibilityOptionsWhenWorkAreaImageSelected();
                    break;
                case SelectImageConstants.SELECT_WORK_AREA_TEXT:
                    this.setVisibilityOptionsWhenWorkAreaTextSelected();
                    break;
                case SelectImageConstants.DELETE_IMAGE:
                    this.setVisibilityOptionsWhenDeleteImageSelected();
                    break;
                case SelectImageConstants.DELETE_WORK_AREA_IMAGE:
                    this.setVisibilityOptionsWhenDeleteWorkAreaImageSelected();
                    break;
                case SelectImageConstants.DELETE_WORK_AREA_TEXT:
                    this.setVisibilityOptionsWhenDeleteWorkAreaTextSelected();
                    break;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private class OptionAdapter extends BaseAdapter{
        private Context context = null;
        public ArrayList<SelectImageOption> options = null;
        private LayoutInflater mInflator = null;
        private ViewHolder vHolder = null;

        public OptionAdapter(Context context ,
                             ArrayList<SelectImageOption> options){
            this.options = options;
            mInflator = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return options.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            if(view == null){
                view = mInflator.inflate(R.layout.work_area_image_selection_dialog_list_layout , null);
                vHolder = new ViewHolder();
                vHolder.imgIcon = (ImageView) view.findViewById(R.id.imgIcon);
                vHolder.txtOptionName = (TextView) view.findViewById(R.id.txtOptionName);
                view.setTag(vHolder);
            }else{
                vHolder = (ViewHolder) view.getTag();
            }

            if(options.get(position).getIsVisible() == YES){
                view.setVisibility(View.VISIBLE);
                this.setIcon(vHolder , options.get(position).getId() ,
                        options.get(position).getStatus());
                vHolder.txtOptionName.setText(options.get(position).getOptionName());
            }else{
                view.setVisibility(View.GONE);
            }
            return view;
        }

        private void setIcon(ViewHolder vHolder , int id , int status){
           /* if(status == NOT_SELECTED) {*/
            switch (id) {
                case SelectImageConstants.SELECT_FROM_GALLARY:
                    vHolder.imgIcon.setBackgroundResource(R.drawable.gallery_icon_ipad);
                    break;
                case SelectImageConstants.SELECT_FROM_CAMERA:
                    vHolder.imgIcon.setBackgroundResource(R.drawable.camera_icon_ipad);
                    break;
                case SelectImageConstants.SELECT_FROM_HOMEWORK_SHEET:
                    vHolder.imgIcon.setBackgroundResource(R.drawable.homepage_icon_ipad);
                    break;
                case SelectImageConstants.SELECT_FROM_WORK_AREA_IMAGE:
                    vHolder.imgIcon.setBackgroundResource(R.drawable.homepage_icon_ipad);
                    break;
                case SelectImageConstants.SELECT_WORK_AREA_TEXT:
                    vHolder.imgIcon.setBackgroundResource(R.drawable.homepage_icon_ipad);
                    break;
                case SelectImageConstants.DELETE_IMAGE:
                    vHolder.imgIcon.setBackgroundResource(R.drawable.cancel_icon_ipad);
                    break;
                case SelectImageConstants.DELETE_WORK_AREA_IMAGE:
                    vHolder.imgIcon.setBackgroundResource(R.drawable.cancel_icon_ipad);
                    break;
                case SelectImageConstants.DELETE_WORK_AREA_TEXT:
                    vHolder.imgIcon.setBackgroundResource(R.drawable.cancel_icon_ipad);
                    break;
            }
            /*}else{
                vHolder.imgIcon.setBackgroundResource(R.drawable.cancel_icon_ipad);
            }*/
        }

        private class ViewHolder{
            private ImageView imgIcon;
            private TextView txtOptionName;
        }
    }
}
