package com.mathfriendzy.controller.homework.workimageoption;

import android.graphics.Bitmap;

import java.io.File;

/**
 * Created by root on 29/5/15.
 */
public interface TakeScreenShotAndSaveToMemoryCallback {
    void onComplete(File file , Bitmap bitmap);
}
