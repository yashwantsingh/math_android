package com.mathfriendzy.controller.homework.workimageoption;

/**
 * Created by root on 27/5/15.
 */
public interface HomeworkImageOptionCallback {
    void onOptionSelect(SelectImageOption option);
}
