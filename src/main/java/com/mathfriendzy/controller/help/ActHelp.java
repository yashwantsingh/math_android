package com.mathfriendzy.controller.help;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.MathVersions;

public class ActHelp extends ActBaseClass implements OnClickListener{

	public static final int Introduction_Id = 1;
	public static final int StudentFunction_Id = 2;
	public static final int TeacherFunction_Id = 3;
	public static final int HomePage_Id = 4;

	private TextView mfTitleHomeScreen;
	private TextView txtSelectCategory;
	private TextView txtIntroduction;
	private TextView txtStudentFunction;
	private TextView txtHomePage;
	private TextView txtTeacherFunction;
	private TextView lblTheGameOfMath;
	private TextView lblLearningCenter;
	private TextView lblVideosFlashCardsPlay;
	private TextView lblSingleFriendzy;
	private TextView lblPlayAroundTheWorld;
	private TextView lblBestOf5Friendzy;
	private TextView lblPlayWithYourFriends;
	private TextView lblSchoolFunction;
	private TextView lblLearnWithFriends;
	private TextView txtReportForSchool;
	private TextView lblTeacherFunction;

	//private ImageView imgFriendzy;

	private Button btnIntroduction;
	private Button btnStudentFunction;
	private Button btnHomePage;
	private Button btnTeacherFunction;
	private Button btnHelp;
	private Button btnRateUs;
	private Button btnTitleTop100;
	/*private Button btn_setting;
	private Button btn_add;
	private Button btn_info;
	private Button btn_share;*/
	private Button btn_introduction_video;
	private Button btnAssessmentTest;
	private Button btnAssessmentTestVideo;
	private Button btnSchoolChallenges;
	private Button btnSchoolChallengesVideo;
	private Button btnHomeworkQuizzess;
	private Button btnAssessmentReportVideo;
	private Button btnConnectToutor;
	private Button btnConnectToutorVideo;
	private Button btnStudyGroup;
	private Button btnAssignHomeWorkQuizzessVideo;
	private Button btnHelpAStudent;
	private Button btnCheckHomeworkQuizzessVideo;
	private Button btnTop100Video;
	private Button btnFriendzyVideo;
	private Button btnlearningCenterVideo;
	private Button btnsingleFriendzyVideo;
	private Button btnmultiFriendzyVideo;
	private Button btnSchoolFunctionVideo;
	private Button btnshareVideo;
	private Button btnsettingVideo;
	private Button btnPractice;
	private Button btnInfoPracticeVideo;
	private Button btnAssessmentsByStudent;
	private Button btnAssessmentsReportByClass;
	private Button btnInfoAssessmentVideo;
	private Button btnAssignHomeWorkQuizzess;
	private Button btnCheckHomeWorkQuizzess;
	private Button btnInfoAssignHomeWorkVideo;
	private Button btnCreateChallenge;
	private Button btnInfoCreateChallengeVideo;
	private Button btnStudentsAccount;
	private Button btnInfoStudentsAccountVideo;

	private Button home_btnHelp;
	private Button home_btnRateUs;
	private Button home_btnTitleTop100;
	private TextView home_lblTheGameOfMath;
	private TextView home_lblLearningCenter;
	private TextView home_lblVideosFlashCardsPlay;
	private TextView home_lblSingleFriendzy;
	private TextView home_lblPlayAroundTheWorld;
	private TextView home_lblBestOf5Friendzy;
	private TextView home_lblPlayWithYourFriends;
	private TextView home_lblSchoolFunction;
	private TextView home_lblLearnWithFriends;

	private RelativeLayout relativeIntro;
	private RelativeLayout relativeStudent;
	private RelativeLayout relativeHome;
	private RelativeLayout relativeTeacher;
	private RelativeLayout pageLayout;

	private int defaultId = Introduction_Id;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_act_help);

        if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS){
            this.startSchoolFriendzyHelp();
            finish();
            return ;
        }

		setWidgetRefrences();
		setTextFromTranslation();
		setListenerOnWidgets();
		this.setFirstVisibility();
	}

    /**
     * Start the school friendzy help screen
     */
    private void startSchoolFriendzyHelp(){
        startActivity(new Intent(this , ActHelpSchoolFriendzy.class));
    }

	/**
	 * Set the first time visibility
	 */
	private void setFirstVisibility(){
		if(this.getIntent().getIntExtra("visibleId", 0) > 0){
			defaultId = this.getIntent().getIntExtra("visibleId", 0);
		}
		this.setVisiblity(defaultId);
	}
	
	private void setWidgetRefrences() {

		btnInfoStudentsAccountVideo 	= (Button)findViewById(R.id.btnInfoStudentsAccountVideo);
		btnStudentsAccount 				= (Button)findViewById(R.id.btnStudentsAccount);
		btnInfoCreateChallengeVideo 	= (Button)findViewById(R.id.btnInfoCreateChallengeVideo);
		btnCreateChallenge				= (Button)findViewById(R.id.btnCreateChallenge);
		btnInfoAssignHomeWorkVideo 		= (Button)findViewById(R.id.btnInfoAssignHomeWorkVideo);
		btnCheckHomeWorkQuizzess 		= (Button)findViewById(R.id.btnCheckHomeWorkQuizzess);
		btnAssignHomeWorkQuizzess 		= (Button)findViewById(R.id.btnAssignHomeWorkQuizzess);
		btnInfoAssessmentVideo			= (Button)findViewById(R.id.btnInfoAssessmentVideo);
		btnAssessmentsReportByClass 	= (Button)findViewById(R.id.btnAssessmentsReportByClass);
		btnAssessmentsByStudent 		= (Button)findViewById(R.id.btnAssessmentsByStudent);
		btnInfoPracticeVideo 			= (Button)findViewById(R.id.btnInfoPracticeVideo);
		btnPractice						= (Button)findViewById(R.id.btnPractice);
		btnsettingVideo 				= (Button)findViewById(R.id.btnsettingVideo);
		btnshareVideo 					= (Button)findViewById(R.id.btnshareVideo);		
		btnSchoolFunctionVideo 			= (Button)findViewById(R.id.btnSchoolFunctionVideo);
		btnmultiFriendzyVideo			= (Button)findViewById(R.id.btnmultiFriendzyVideo);
		btnsingleFriendzyVideo 			= (Button)findViewById(R.id.btnsingleFriendzyVideo);
		btnlearningCenterVideo 			= (Button)findViewById(R.id.btnlearningCenterVideo);	
		btnFriendzyVideo 				= (Button)findViewById(R.id.btnFriendzyVideo);
		btnTop100Video					= (Button)findViewById(R.id.btnTop100Video);
		btnCheckHomeworkQuizzessVideo 	= (Button)findViewById(R.id.btnCheckHomeworkQuizzessVideo);	
		btnHelpAStudent 				= (Button)findViewById(R.id.btnHelpAStudent);
		btnAssignHomeWorkQuizzessVideo 	= (Button)findViewById(R.id.btnAssignHomeWorkQuizzessVideo);
		btnStudyGroup					= (Button)findViewById(R.id.btnStudyGroup);
		btnConnectToutorVideo 			= (Button)findViewById(R.id.btnConnectToutorVideo);
		btnConnectToutor 				= (Button)findViewById(R.id.btnConnectToutor);
		btnAssessmentReportVideo 		= (Button)findViewById(R.id.btnAssessmentReportVideo);
		btnHomeworkQuizzess				= (Button)findViewById(R.id.btnHomeworkQuizzess);
		btnSchoolChallengesVideo 		= (Button)findViewById(R.id.btnSchoolChallengesVideo);
		btnSchoolChallenges 			= (Button)findViewById(R.id.btnSchoolChallenges);
		btnAssessmentTestVideo 			= (Button)findViewById(R.id.btnAssessmentTestVideo);
		btnAssessmentTest				= (Button)findViewById(R.id.btnAssessmentTest);
		btn_introduction_video 			= (Button)findViewById(R.id.btn_introduction_video);
		/*btn_share 						= (Button)findViewById(R.id.btn_share);
		btn_info 						= (Button)findViewById(R.id.btn_info);
		btn_add							= (Button)findViewById(R.id.btn_add);
		btn_setting 					= (Button)findViewById(R.id.btn_setting);*/
		btnTitleTop100 					= (Button)findViewById(R.id.btnTitleTop100);
		btnRateUs 						= (Button)findViewById(R.id.btnRateUs);
		btnHelp							= (Button)findViewById(R.id.btnHelp);
		btnStudentFunction 				= (Button)findViewById(R.id.btnStudentFunction);
		btnHomePage						= (Button)findViewById(R.id.btnHomePage);
		btnIntroduction 				= (Button)findViewById(R.id.btnIntroduction);
		btnTeacherFunction				= (Button)findViewById(R.id.btnTeacherFunction);

		mfTitleHomeScreen 				= (TextView)findViewById(R.id.mfTitleHomeScreen);
		txtSelectCategory 				= (TextView)findViewById(R.id.txtSelectCategory);
		txtIntroduction 				= (TextView)findViewById(R.id.txtIntroduction);
		txtStudentFunction 				= (TextView)findViewById(R.id.txtStudentFunction);
		txtHomePage 					= (TextView)findViewById(R.id.txtHomePage);
		txtTeacherFunction 				= (TextView)findViewById(R.id.txtTeacherFunction);
		lblTheGameOfMath 				= (TextView)findViewById(R.id.lblTheGameOfMath);
		lblLearningCenter 				= (TextView)findViewById(R.id.lblLearningCenter);
		lblVideosFlashCardsPlay 		= (TextView)findViewById(R.id.lblVideosFlashCardsPlay);
		lblSingleFriendzy 				= (TextView)findViewById(R.id.lblSingleFriendzy);
		lblPlayAroundTheWorld 			= (TextView)findViewById(R.id.lblPlayAroundTheWorld);
		lblBestOf5Friendzy 				= (TextView)findViewById(R.id.lblBestOf5Friendzy);
		lblPlayWithYourFriends 			= (TextView)findViewById(R.id.lblPlayWithYourFriends);
		lblSchoolFunction 				= (TextView)findViewById(R.id.lblSchoolFunction);
		lblLearnWithFriends 			= (TextView)findViewById(R.id.lblLearnWithFriends);
		txtReportForSchool 				= (TextView)findViewById(R.id.txtReportForSchool);
		lblTeacherFunction 				= (TextView)findViewById(R.id.lblTeacherFunction);

		//imgFriendzy	 					= (ImageView)findViewById(R.id.imgFriendzy);

		relativeIntro 					= (RelativeLayout)findViewById(R.id.relativeIntro);
		relativeStudent	 				= (RelativeLayout)findViewById(R.id.relativeStudent);
		relativeHome 					= (RelativeLayout)findViewById(R.id.relativeHome);
		relativeTeacher 				= (RelativeLayout)findViewById(R.id.relativeTeacher);
		pageLayout 						= (RelativeLayout)findViewById(R.id.pageLayout);

		home_lblLearnWithFriends 		= (TextView)findViewById(R.id.home_lblLearnWithFriends);
		home_lblSchoolFunction 			= (TextView)findViewById(R.id.home_lblSchoolFunction);
		home_lblPlayWithYourFriends 	= (TextView)findViewById(R.id.home_lblPlayWithYourFriends);
		home_lblBestOf5Friendzy 		= (TextView)findViewById(R.id.home_lblBestOf5Friendzy);
		home_lblPlayAroundTheWorld 		= (TextView)findViewById(R.id.home_lblPlayAroundTheWorld);
		home_lblSingleFriendzy 			= (TextView)findViewById(R.id.home_lblSingleFriendzy);
		home_lblVideosFlashCardsPlay 	= (TextView)findViewById(R.id.home_lblVideosFlashCardsPlay);
		home_lblLearningCenter 			= (TextView)findViewById(R.id.home_lblLearningCenter);
		home_lblTheGameOfMath 			= (TextView)findViewById(R.id.home_lblTheGameOfMath);
		home_btnTitleTop100 			= (Button)findViewById(R.id.home_btnTitleTop100);
		home_btnRateUs 					= (Button)findViewById(R.id.home_btnRateUs);
		home_btnHelp 					= (Button)findViewById(R.id.home_btnHelp);

	}

	private void setTextFromTranslation() {
		Translation transeletion = new Translation(this);
		transeletion.openConnection();

		mfTitleHomeScreen.setText(transeletion.getTranselationTextByTextIdentifier("mfTitleHomeScreen")
				+ " " + transeletion.getTranselationTextByTextIdentifier("lblTutorial"));	
		txtSelectCategory.setText(transeletion.getTranselationTextByTextIdentifier("lblViewVideoFor"));
		txtIntroduction.setText(transeletion.getTranselationTextByTextIdentifier("lblIntroduction"));
		txtStudentFunction.setText(transeletion.getTranselationTextByTextIdentifier("mfBtnTitleStudents")
				+ "' " + transeletion.getTranselationTextByTextIdentifier("lblFunctions"));
		txtTeacherFunction.setText(transeletion.getTranselationTextByTextIdentifier("mfBtnTitleTeachers")
				+ "' " + transeletion.getTranselationTextByTextIdentifier("lblFunctions"));	
		txtHomePage.setText(transeletion.getTranselationTextByTextIdentifier("lblHomePage"));
		btnHelp.setText(transeletion.getTranselationTextByTextIdentifier("lblHelp"));
		btnRateUs.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleRateUs"));
		btnTitleTop100.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleTop100"));
		lblTheGameOfMath.setText(transeletion.getTranselationTextByTextIdentifier("lblTheGameOfMath"));
		lblLearningCenter.setText(transeletion.getTranselationTextByTextIdentifier("lblLearningCenter"));
		lblVideosFlashCardsPlay.setText(transeletion.getTranselationTextByTextIdentifier("lblVideosFlashCardsPlay"));
		lblSingleFriendzy.setText(transeletion.getTranselationTextByTextIdentifier("lblSingleFriendzy"));
		lblPlayAroundTheWorld.setText(transeletion.getTranselationTextByTextIdentifier("lblPlayAroundTheWorld"));
		lblBestOf5Friendzy.setText(transeletion.getTranselationTextByTextIdentifier("lblBestOf5Friendzy"));
		lblPlayWithYourFriends.setText(transeletion.getTranselationTextByTextIdentifier("lblPlayWithYourFriends"));
		lblSchoolFunction.setText(transeletion.getTranselationTextByTextIdentifier("lblSchoolInfoSchoolText")
				+ " " + transeletion.getTranselationTextByTextIdentifier("lblFunctions"));			
		lblLearnWithFriends.setText(transeletion.getTranselationTextByTextIdentifier("lblLearnWithFriends"));
		txtReportForSchool.setText(transeletion.getTranselationTextByTextIdentifier("lblSchoolInfoSchoolText")
				+ " " + transeletion.getTranselationTextByTextIdentifier("lblFunctions"));
		btnAssessmentTest.setText(transeletion.getTranselationTextByTextIdentifier("lblAssessmentTest"));
		btnSchoolChallenges.setText(transeletion.getTranselationTextByTextIdentifier("lblSchoolInfoSchoolText")
				+ " " + transeletion.getTranselationTextByTextIdentifier("btnTitleChallenges"));		
		btnHomeworkQuizzess.setText(transeletion.getTranselationTextByTextIdentifier("lblHomework")
				+ "/" + transeletion.getTranselationTextByTextIdentifier("lblQuizzes"));
		btnConnectToutor.setText(transeletion.getTranselationTextByTextIdentifier("lblConnectWithTutor"));
		btnStudyGroup.setText(transeletion.getTranselationTextByTextIdentifier("lblStudyGroup"));
		btnHelpAStudent.setText(transeletion.getTranselationTextByTextIdentifier("lblHelpAStudent"));
		lblTeacherFunction.setText(transeletion.getTranselationTextByTextIdentifier("lblReportsFor"));
		btnPractice.setText(transeletion.getTranselationTextByTextIdentifier("lblPractice"));
		btnAssessmentsByStudent.setText(transeletion.getTranselationTextByTextIdentifier("lblAssessmentsReports")
				+ " " + transeletion.getTranselationTextByTextIdentifier("lblByStudents"));
		btnAssessmentsReportByClass.setText(transeletion.getTranselationTextByTextIdentifier("lblAssessmentsReports")
				+ " " + transeletion.getTranselationTextByTextIdentifier("lblByCcss"));
		btnAssignHomeWorkQuizzess.setText(transeletion.getTranselationTextByTextIdentifier("lblAssignHomework")
				+ "/" + transeletion.getTranselationTextByTextIdentifier("lblQuizzes"));
		btnCheckHomeWorkQuizzess.setText(transeletion.getTranselationTextByTextIdentifier("lblCheckHomework")
				+ "/" + transeletion.getTranselationTextByTextIdentifier("lblQuizzes"));
		btnStudentsAccount.setText(transeletion.getTranselationTextByTextIdentifier("mfBtnTitleStudents")
				+ "' " + transeletion.getTranselationTextByTextIdentifier("textAccount"));
		btnCreateChallenge.setText(transeletion.getTranselationTextByTextIdentifier("lblFriendzyChallenge"));

		home_btnHelp.setText(transeletion.getTranselationTextByTextIdentifier("lblHelp"));
		home_btnRateUs.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleRateUs"));
		home_btnTitleTop100.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleTop100"));
		home_lblTheGameOfMath.setText(transeletion.getTranselationTextByTextIdentifier("lblTheGameOfMath"));
		home_lblLearningCenter.setText(transeletion.getTranselationTextByTextIdentifier("lblLearningCenter"));
		home_lblVideosFlashCardsPlay.setText(transeletion.getTranselationTextByTextIdentifier("lblVideosFlashCardsPlay"));
		home_lblSingleFriendzy.setText(transeletion.getTranselationTextByTextIdentifier("lblSingleFriendzy"));
		home_lblPlayAroundTheWorld.setText(transeletion.getTranselationTextByTextIdentifier("lblPlayAroundTheWorld"));
		home_lblBestOf5Friendzy.setText(transeletion.getTranselationTextByTextIdentifier("lblBestOf5Friendzy"));
		home_lblPlayWithYourFriends.setText(transeletion.getTranselationTextByTextIdentifier("lblPlayWithYourFriends"));
		home_lblSchoolFunction.setText(transeletion.getTranselationTextByTextIdentifier("lblSchoolInfoSchoolText")
				+ " " + transeletion.getTranselationTextByTextIdentifier("lblFunctions"));			
		home_lblLearnWithFriends.setText(transeletion.getTranselationTextByTextIdentifier("lblLearnWithFriends"));
		transeletion.closeConnection();
	}

	private void setListenerOnWidgets(){

		btn_introduction_video.setOnClickListener(this);
		btnAssessmentTestVideo.setOnClickListener(this);	
		btnSchoolChallengesVideo.setOnClickListener(this);
		btnAssessmentReportVideo.setOnClickListener(this);	

		btnConnectToutorVideo.setOnClickListener(this);
		btnAssignHomeWorkQuizzessVideo.setOnClickListener(this);

		btnCheckHomeworkQuizzessVideo.setOnClickListener(this);
		btnInfoPracticeVideo.setOnClickListener(this);


		btnInfoAssessmentVideo.setOnClickListener(this);
		btnInfoAssignHomeWorkVideo.setOnClickListener(this);

		btnInfoCreateChallengeVideo.setOnClickListener(this);
		btnInfoStudentsAccountVideo.setOnClickListener(this);
		btnTop100Video.setOnClickListener(this);
		btnFriendzyVideo.setOnClickListener(this);
		btnlearningCenterVideo.setOnClickListener(this);
		btnsingleFriendzyVideo.setOnClickListener(this);
		btnmultiFriendzyVideo.setOnClickListener(this);
		btnSchoolFunctionVideo.setOnClickListener(this);
		btnshareVideo.setOnClickListener(this);
		btnsettingVideo.setOnClickListener(this);


		btnIntroduction.setOnClickListener(this);
		btnStudentFunction.setOnClickListener(this);
		btnHomePage.setOnClickListener(this);
		btnTeacherFunction.setOnClickListener(this);

	}

	/**
	 * Open the video
	 * @param url
	 */
	private void openVideoUrl(String url){
		//startActivity(new Intent(Intent.ACTION_VIEW , Uri.parse(url)));
		MathFriendzyHelper.converUrlIntoEmbaddedAndPlay(this, url);
	}

	/**
	 * Open the no video dialog
	 */
	private void noVideoDialog(){
		CommonUtils.comingSoonPopUp(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnIntroduction:
			setVisiblity(Introduction_Id);
			break;
		case R.id.btnStudentFunction:
			setVisiblity(StudentFunction_Id);
			break;
		case R.id.btnHomePage:
			setVisiblity(HomePage_Id);
			break;
		case R.id.btnTeacherFunction:
			setVisiblity(TeacherFunction_Id);
			break;
		case R.id.btn_introduction_video:
			this.openVideoUrl("https://www.youtube.com/watch?v=5lrr2gND02g");
			break;
		case R.id.btnAssessmentTestVideo:
			this.openVideoUrl("https://www.youtube.com/watch?v=kzk2VdsnSWc");
			break;
		case R.id.btnSchoolChallengesVideo:
			this.openVideoUrl("https://www.youtube.com/watch?v=YOSqqVPJOPA");
			break;
		case R.id.btnAssessmentReportVideo:
			this.openVideoUrl("https://www.youtube.com/watch?v=LsYqJ8zCQlw");
			break;
		case R.id.btnConnectToutorVideo:
			this.noVideoDialog();
			break;
		case R.id.btnAssignHomeWorkQuizzessVideo:
			this.noVideoDialog();
			break;
		case R.id.btnCheckHomeworkQuizzessVideo:
			this.noVideoDialog();
			break;
		case R.id.btnInfoPracticeVideo:
			this.openVideoUrl("https://www.youtube.com/watch?v=0Tn8ir2W6JY");
			break;
		case R.id.btnInfoAssessmentVideo:
			this.openVideoUrl("https://www.youtube.com/watch?v=8j7A0S1xcO8");
			break;
		case R.id.btnInfoAssignHomeWorkVideo:
			this.openVideoUrl("https://www.youtube.com/watch?v=uTcBLBULW98");
			break;
		case R.id.btnInfoCreateChallengeVideo:
			this.openVideoUrl("https://www.youtube.com/watch?v=pvPrby1xQxU");
			break;
		case R.id.btnInfoStudentsAccountVideo:
			this.openVideoUrl("https://www.youtube.com/watch?v=8a-dTnmkbNg");
			break;
		case R.id.btnTop100Video:
			this.openVideoUrl("https://www.youtube.com/watch?v=Cqebs1a-z2k");
			break;
		case R.id.btnFriendzyVideo:
			this.openVideoUrl("https://www.youtube.com/watch?v=aNh0rOkE-VA");
			break;
		case R.id.btnlearningCenterVideo:
			this.openVideoUrl("https://www.youtube.com/watch?v=G5mpa1H3Pkg");
			break;
		case R.id.btnsingleFriendzyVideo:
			this.openVideoUrl("https://www.youtube.com/watch?v=y3uVliszBqM");
			break;
		case R.id.btnshareVideo:
			setVisiblity(TeacherFunction_Id);
			break;
		case R.id.btnSchoolFunctionVideo:
			setVisiblity(StudentFunction_Id);
			break;
		case R.id.btnmultiFriendzyVideo:
			this.openVideoUrl("https://www.youtube.com/watch?v=RmknLMlg-4s");
			break;
		case R.id.btnsettingVideo:
			this.openVideoUrl("https://www.youtube.com/watch?v=WNGQJ2V7iis");
			break;
		}
	}


	private void setVisiblity(int id) {

		switch (id) {
		case Introduction_Id :

			btnIntroduction.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
			btnStudentFunction.setBackgroundResource(R.drawable.mf_check_box_ipad);
			btnHomePage.setBackgroundResource(R.drawable.mf_check_box_ipad);
			btnTeacherFunction.setBackgroundResource(R.drawable.mf_check_box_ipad);
			relativeIntro.setVisibility(RelativeLayout.VISIBLE);
			relativeStudent.setVisibility(RelativeLayout.GONE);
			relativeHome.setVisibility(RelativeLayout.GONE);
			relativeTeacher.setVisibility(RelativeLayout.GONE);
			pageLayout.setVisibility(RelativeLayout.VISIBLE);

			break;

		case  TeacherFunction_Id:


			btnIntroduction.setBackgroundResource(R.drawable.mf_check_box_ipad);
			btnStudentFunction.setBackgroundResource(R.drawable.mf_check_box_ipad);
			btnHomePage.setBackgroundResource(R.drawable.mf_check_box_ipad);
			btnTeacherFunction.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
			relativeIntro.setVisibility(RelativeLayout.GONE);
			relativeStudent.setVisibility(RelativeLayout.GONE);
			relativeHome.setVisibility(RelativeLayout.GONE);
			relativeTeacher.setVisibility(RelativeLayout.VISIBLE);
			pageLayout.setVisibility(RelativeLayout.GONE);

			break;

		case  HomePage_Id :


			btnIntroduction.setBackgroundResource(R.drawable.mf_check_box_ipad);
			btnStudentFunction.setBackgroundResource(R.drawable.mf_check_box_ipad);
			btnHomePage.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
			btnTeacherFunction.setBackgroundResource(R.drawable.mf_check_box_ipad);
			relativeIntro.setVisibility(RelativeLayout.GONE);
			relativeStudent.setVisibility(RelativeLayout.GONE);
			relativeHome.setVisibility(RelativeLayout.VISIBLE);
			relativeTeacher.setVisibility(RelativeLayout.GONE);
			pageLayout.setVisibility(RelativeLayout.VISIBLE);

			break;

		case  StudentFunction_Id:


			btnIntroduction.setBackgroundResource(R.drawable.mf_check_box_ipad);
			btnStudentFunction.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
			btnHomePage.setBackgroundResource(R.drawable.mf_check_box_ipad);
			btnTeacherFunction.setBackgroundResource(R.drawable.mf_check_box_ipad);
			relativeIntro.setVisibility(RelativeLayout.GONE);
			relativeStudent.setVisibility(RelativeLayout.VISIBLE);
			relativeHome.setVisibility(RelativeLayout.GONE);
			relativeTeacher.setVisibility(RelativeLayout.GONE);
			pageLayout.setVisibility(RelativeLayout.GONE);

			break;

		default:
			break;
		}
	}

	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}
}
