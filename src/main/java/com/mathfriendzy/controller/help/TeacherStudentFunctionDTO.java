package com.mathfriendzy.controller.help;

/**
 * Created by root on 4/8/16.
 */
public class TeacherStudentFunctionDTO {
    private String txtName;
    private int backGroundImageId;
    private String url;

    public String getTxtName() {
        return txtName;
    }

    public void setTxtName(String txtName) {
        this.txtName = txtName;
    }

    public int getBackGroundImageId() {
        return backGroundImageId;
    }

    public void setBackGroundImageId(int backGroundImageId) {
        this.backGroundImageId = backGroundImageId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
