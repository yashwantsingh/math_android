package com.mathfriendzy.controller.reportaproblem;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.controller.timer.MyTimerInrerface;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.LearnignCenterSchoolCurriculumServerOperation;
import com.mathfriendzy.model.tutor.ReportThisTutorParam;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ICommonUtils;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.app.Activity;
import android.app.ProgressDialog;

/**
 * This Activity open when user repeort the problem with question.
 * @author Yashwant Singh
 *
 */
public class ReportProblemActivity extends ActBaseClass implements OnClickListener{

    private TextView mfTitleHomeScreen 		= null;
    private TextView txtExplainWhatIsWrong 	= null;
    private EditText edtReportProblem       = null;
    private Button   btnSubmit              = null;

    private final String TAG = this.getClass().getSimpleName();

    private int questionId = 0;
    public static MyTimerInrerface timer  = null;
    private String msg     = "";

    public static final String CALLING_ACTIVITY = "callingActivity";
    public static final int REPORT_THIS_TUTOR_REQ = 1;
    private int requestedActivity = 0;
    private ReportThisTutorParam param = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_problem);

        if(ICommonUtils.REPORT_PROBLEM)
            Log.e(TAG, "inside onCreate()");

        this.getIntentValues();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();

        if(ICommonUtils.REPORT_PROBLEM)
            Log.e(TAG, "outside onCreate()");

    }

    /**
     * This method get the intent values which are set in previous screen
     */
    private void getIntentValues(){

        if(ICommonUtils.REPORT_PROBLEM)
            Log.e(TAG, "inside getIntentValues()");

        requestedActivity = this.getIntent().getIntExtra(CALLING_ACTIVITY , 0);
        if(requestedActivity == REPORT_THIS_TUTOR_REQ){
            param = (ReportThisTutorParam) this.getIntent().getSerializableExtra("ReportThisTutorParam");
        }else {
            questionId = this.getIntent().getIntExtra("questionId", 0);
            //timer      = (MyTimerInrerface) this.getIntent().getSerializableExtra("timer");
        }

        if(ICommonUtils.REPORT_PROBLEM)
            Log.e(TAG, "outside getIntentValues()");

    }


    /**
     * This method set the widgets references from th exml layout
     */
    private void setWidgetsReferences(){

        if(ICommonUtils.REPORT_PROBLEM)
            Log.e(TAG, "inside setWidgetsReferences()");

        mfTitleHomeScreen 		= (TextView) findViewById(R.id.mfTitleHomeScreen);
        txtExplainWhatIsWrong 	= (TextView) findViewById(R.id.txtExplainWhatIsWrong);
        edtReportProblem 		= (EditText) findViewById(R.id.edtReportProblem);
        btnSubmit 				= (Button) findViewById(R.id.btnSubmit);

        if(ICommonUtils.REPORT_PROBLEM)
            Log.e(TAG, "outside setWidgetsReferences()");
    }

    /**
     * This method set the translation text from translation table
     */
    private void setTextFromTranslation(){

        if(ICommonUtils.REPORT_PROBLEM)
            Log.e(TAG, "inside setTextFromTranslation()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();

        if(requestedActivity == REPORT_THIS_TUTOR_REQ){
            mfTitleHomeScreen.setText(transeletion
                    .getTranselationTextByTextIdentifier("lblReportThisTutor"));
            txtExplainWhatIsWrong.setText(transeletion
                    .getTranselationTextByTextIdentifier("lblPleaseLetus"));
        }else{
            mfTitleHomeScreen.setText(transeletion.getTranselationTextByTextIdentifier("lblReportAProblem"));
            txtExplainWhatIsWrong.setText(transeletion.getTranselationTextByTextIdentifier("lblPleaseBrieflyExplainWhatWrong"));
        }

        btnSubmit.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleSubmit"));

        transeletion.closeConnection();

        if(ICommonUtils.REPORT_PROBLEM)
            Log.e(TAG, "outside setTextFromTranslation()");
    }

    /**
     * This method set the listener on widgets
     */
    private void setListenerOnWidgets(){

        if(ICommonUtils.REPORT_PROBLEM)
            Log.e(TAG, "inside setListenerOnWidgets()");

        btnSubmit.setOnClickListener(this);

        if(ICommonUtils.REPORT_PROBLEM)
            Log.e(TAG, "outside setListenerOnWidgets()");
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnSubmit :
                msg = edtReportProblem.getText().toString();

                if(msg.length() > 0){
                    if(requestedActivity == REPORT_THIS_TUTOR_REQ){
                        this.reportThisTutor();
                    }else {
                        if (CommonUtils.isInternetConnectionAvailable(this)) {
                            new SendEmailForWrongQuestion(questionId, msg).execute(null, null, null);
                        } else {
                            CommonUtils.showInternetDialog(this);
                        }
                    }
                }else{
                    Translation transeletion = new Translation(this);
                    transeletion.openConnection();
                    DialogGenerator dg = new DialogGenerator(this);
                    dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("lblPleaseEnterYourProblemToContinue"));
                    transeletion.closeConnection();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if(requestedActivity == REPORT_THIS_TUTOR_REQ){

        }else {
            if (timer != null)
                timer.resumeTimerFromTimeSetAtCancelTime();
        }
        super.onBackPressed();
    }

    /**
     * Report this tutor
     */
    private void reportThisTutor(){
        if(param != null){
            param.setMessage(msg);
        }

        new MyAsyckTask(ServerOperation.createPostRequestForReportThisTutor(param)
                , null, ServerOperationUtil.REPORT_THIS_TUTOR_REQUEST,
                this, new HttpResponseInterface() {
            @Override
            public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                finish();
            }
        }, ServerOperationUtil.SIMPLE_DIALOG, true,
                "Please Wait...").execute();
    }

    /**
     * This class send email
     * @author Yashwant Singh
     *
     */
    class SendEmailForWrongQuestion extends AsyncTask<Void, Void, Void>{

        private int questionId = 0;
        private ProgressDialog pd;
        private String msg;
        SendEmailForWrongQuestion(int questionId , String msg){
            this.questionId = questionId;
            this.msg        = msg;
        }

        @Override
        protected void onPreExecute() {
            pd = CommonUtils.getProgressDialog(ReportProblemActivity.this);
            pd.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            LearnignCenterSchoolCurriculumServerOperation serverObj =
                    new LearnignCenterSchoolCurriculumServerOperation();
            serverObj.sendEmailForWorngQuestion(questionId , msg);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            pd.cancel();

            if(timer != null)
                timer.resumeTimerFromTimeSetAtCancelTime();

            //finish the current activity
            finish();
            super.onPostExecute(result);
        }
    }
}
