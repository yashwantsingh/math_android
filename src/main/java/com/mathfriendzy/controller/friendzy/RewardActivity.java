package com.mathfriendzy.controller.friendzy;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.CommonUtils;

public class RewardActivity extends ActBaseClass
{
	private TextView txtReward				= null;
	private TextView mfTitleHomeScreen		= null;
	private EditText edtReward				= null;
	
	private Button btnSubmit				= null;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reward);
		
		getWidgetId();	
		setTextOnWidget();
		this.getIntentValues();
	}

	private void getWidgetId()
	{
		txtReward			= (TextView) findViewById(R.id.txtReward);
		mfTitleHomeScreen	= (TextView) findViewById(R.id.mfTitleHomeScreen);
		edtReward			= (EditText) findViewById(R.id.edtReward);
		btnSubmit			= (Button) findViewById(R.id.btnSubmit);
		
		btnSubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v)
			{
                hideDeviceKeyboard();
				Intent i = new Intent();
				i.putExtra("reward", edtReward.getText()+"");
				setResult(RESULT_OK, i);
				finish();
			}
		});
	}
	
	/**
	 * This method getIntent values from previous screen
	 */
	private void getIntentValues(){
		edtReward.setText(this.getIntent().getStringExtra("rewardForEdit"));
	}
	
	private void setTextOnWidget()
	{
		Translation tr = new Translation(this);
		tr.openConnection();
		
		mfTitleHomeScreen.setText(tr.getTranselationTextByTextIdentifier("btnTitleCreateChallenge"));
		txtReward.setText(tr.getTranselationTextByTextIdentifier("lblEnterRewardsThatYouWouldLikeToOffer"));
		edtReward.setHint(tr.getTranselationTextByTextIdentifier("lblExampleRewardsOffer"));
		btnSubmit.setText(tr.getTranselationTextByTextIdentifier("btnTitleSubmit"));
		
		tr.closeConnection();
	}

	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}

    @Override
    public void onBackPressed() {
       //this.hideDeviceKeyboard();
       super.onBackPressed();
    }

    /**
     * Hide the device keyboard
     */
    public void hideDeviceKeyboard(){
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }
}
