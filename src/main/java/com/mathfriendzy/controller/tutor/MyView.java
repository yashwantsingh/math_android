package com.mathfriendzy.controller.tutor;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.view.MotionEvent;
import android.view.View;

import com.mathfriendzy.helper.MathFriendzyHelper;

import java.util.ArrayList;

public class MyView extends View{

    private Canvas mCanvas	= null;
    private Path mPath	    = null;
    private Paint mPaint, mBitmapPaint = null;
    private Bitmap mBitmap = null;
    private boolean isEraseMode = false;
    private boolean isDraw = true;
    //private final String TAG = this.getClass().getSimpleName();
    private DrawingCallback callback = null;
    private int drawingColor = 0;
    private boolean isScrollMode = false;
    private final int ERASE_STORE_WIDTH = 60;

    /**
     * Return the paint object
     * @return
     */
	/*private Paint getPaint(){
		Paint mPaint = new Paint();
		mPaint.setAntiAlias(true);
		mPaint.setDither(true);
		if(getpaint == MY_SIDE_DRAWING)
			mPaint.setColor(Color.BLACK);
		else if(getpaint == OTHER_SIDE_DRAWING)
			mPaint.setColor(Color.RED);
		mPaint.setStrokeWidth(3);
		mPaint.setStyle(Paint.Style.STROKE);
		return mPaint;
	}*/

    @SuppressLint("NewApi")
    public MyView(Context c, int w, int h , Bitmap mBitmap , Paint mPaint,
                  DrawingCallback callback) {
        super(c);

        if (android.os.Build.VERSION.SDK_INT >= 11){
            setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        this.callback = callback;
        this.mPaint = mPaint;
        this.mBitmap = mBitmap;
        mCanvas = new Canvas(mBitmap);
        mPath = new Path();
        mBitmapPaint = new Paint(Paint.DITHER_FLAG);
        mBitmapPaint.setXfermode(null);
    }

    @Override
    protected void onDraw(Canvas canvas){
        canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
        canvas.drawPath(mPath, mPaint);
    }

    // //////************touching events for painting**************///////
    private float mX, mY;
    private static final float TOUCH_TOLERANCE = 2;

    public void touch_start(float x, float y) {
        mPath.reset();
        mPath.moveTo(x, y);
        mX = x;
        mY = y;
        callback.onStart(mX, mY , x , y);
    }

    public void touch_move(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
            //for erasing the written
            if(isEraseMode){
                mPath.lineTo(mX, mY);
                mCanvas.drawPath(mPath, mPaint);
                mPath.reset();
                mPath.moveTo(mX, mY);
            }
            //end for erase change
            mX = x;
            mY = y;
            //addPoint(mX,mY);
            callback.onMove(mX, mY , x , y);
        }
    }

    public void touch_up() {
        if(isEraseMode)
            mPath.reset();
        else{
            mPath.lineTo(mX, mY);
            // commit the path to our off screen
            mCanvas.drawPath(mPath, mPaint);
            // kill this so we don't double draw
            mPath.reset();
        }
        callback.onUp(mX, mY);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event){
        if(isScrollMode){
            //nothing to do
        }else {
            if (isDraw) {
                getParent().requestDisallowInterceptTouchEvent(true);
                float x = event.getX();
                float y = event.getY();
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        touch_start(x, y);
                        invalidate();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        touch_move(x, y);
                        invalidate();
                        break;
                    case MotionEvent.ACTION_UP:
                        touch_up();
                        invalidate();
                        break;
                }
            } else {
                getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        callback.disableDrawing();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        break;
                    case MotionEvent.ACTION_UP:
                        break;
                }
            }
        }
        return true;
    }

    public void setDrawColor(int color){
        drawingColor = color;
    }

    private int getDrawColor(){
        return drawingColor;
    }

    public boolean getEraseMode(){
        return isEraseMode;
    }

    public void setEraseMode(boolean isEraseMode){
        this.isEraseMode = isEraseMode;
        if(isEraseMode){
            mPaint.setStrokeWidth(ERASE_STORE_WIDTH);
            mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        }else{
            mPaint.setStrokeWidth(MathFriendzyHelper.DRAW_LINE_THICKNESS);
            mPaint.setXfermode(null);
            mPaint.setColor(getDrawColor());
        }
    }

    public void setDrawMode(boolean isDraw){
        this.isDraw = isDraw;
    }

    public boolean isScrollMode() {
        return isScrollMode;
    }

    public void setScrollMode(boolean isScrollMode) {
        this.isScrollMode = isScrollMode;
    }

    public void touch_start_for_other(float x, float y) {
        /*if(CommonUtils.LOG_ON)
            Log.e("MyView" , "onTouch start for other");*/
        mPath.reset();
        mPath.moveTo(x, y);
        mX = x;
        mY = y;
        this.invalidate();

    }

    public void touch_move_for_other(float x, float y) {
        /*if(CommonUtils.LOG_ON)
            Log.e("MyView" , "onTouch move for other");*/
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
            //for erasing the written
            if(isEraseMode){
                mPath.lineTo(mX, mY);
                mCanvas.drawPath(mPath, mPaint);
                mPath.reset();
                mPath.moveTo(mX, mY);
            }
            //end for erase change
            mX = x;
            mY = y;
            /*if(CommonUtils.LOG_ON)
                Log.e("TAG" , "mx " + mX + " my " + mY);*/
            this.invalidate();
        }
    }

    public void touch_up_for_other() {

        /*if(CommonUtils.LOG_ON)
            Log.e("MyView", "OnTouch up for other");*/

        if(isEraseMode)
            // kill this so we don't double draw
            mPath.reset();
        else{
            mPath.lineTo(mX, mY);
            // commit the path to our off screen
            mCanvas.drawPath(mPath, mPaint);
            // kill this so we don't double draw
            mPath.reset();
        }
        this.invalidate();
    }

    public void touchUp(){
        try {
            if (mPath != null)
                mPath.reset();
        }catch(Exception e){
            e.printStackTrace();
        }
        this.invalidate();
    }

    /*public void drawPoints(float mX , float mY, float x, float y){
        if(x == -1){//on up
            mPath.lineTo(mX, mY);
            // commit the path to our off screen
            mCanvas.drawPath(mPath, mPaint);
            // kill this so we don't double draw
            mPath.reset();
        }else if(mX == -2){//on start
            mPath.reset();
            mPath.moveTo(x, y);
        }else{
			*//*mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
			mPath.lineTo(mX, mY);
			mCanvas.drawPath(mPath, mPaint);
			mPath.reset();
			mPath.moveTo(mX, mY);
			this.invalidate();*//*
            float dx = Math.abs(x - mX);
            float dy = Math.abs(y - mY);
            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
                //for erasing the written
                if(isEraseMode){
                    mPath.lineTo(mX, mY);
                    mCanvas.drawPath(mPath, mPaint);
                    mPath.reset();
                    mPath.moveTo(mX, mY);
                }
                //end for erase change
                mX = x;
                mY = y;
            }
        }
        this.invalidate();
    }*/


    public void touch_start_for_otherA(float x, float y) {
        mPath.reset();
        mPath.moveTo(x, y);
        mX = x;
        mY = y;
        //this.invalidate();

    }

    public void touch_move_for_otherA(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
            //for erasing the written
            if(isEraseMode){
                mPath.lineTo(mX, mY);
                mCanvas.drawPath(mPath, mPaint);
                mPath.reset();
                mPath.moveTo(mX, mY);
            }
            //end for erase change
            mX = x;
            mY = y;
            /*if(CommonUtils.LOG_ON)
                Log.e("TAG" , "mx " + mX + " my " + mY);*/
            //this.invalidate();
        }
    }

    public void touch_up_for_otherA() {
        if(isEraseMode)
            // kill this so we don't double draw
            mPath.reset();
        else{
            mPath.lineTo(mX, mY);
            // commit the path to our off screen
            mCanvas.drawPath(mPath, mPaint);
            // kill this so we don't double draw
            mPath.reset();
        }
        //this.invalidate();
    }

    public void drawPaths(ArrayList<MyPath> mPathList , boolean isTutor , boolean isMySideView){
        if(mPathList != null && mPathList.size() > 0) {
            for (int i = 0; i < mPathList.size(); i++) {
                if (mPathList.get(i).isEraseMode() && !this.getEraseMode()) {
                    this.setEraseMode(true);
                } else if (!mPathList.get(i).isEraseMode() && this.getEraseMode()) {
                    if (isMySideView) {
                        if (isTutor) {
                            this.setDrawColor(Color.BLUE);
                        } else {
                            this.setDrawColor(Color.BLACK);
                        }
                    } else {
                        if (isTutor) {
                            this.setDrawColor(Color.BLACK);
                        } else {
                            this.setDrawColor(Color.BLUE);
                        }
                    }
                    this.setEraseMode(false);
                }
                mPath = mPathList.get(i).getPath();
                mCanvas.drawPath(mPath, mPaint);
                mPath.reset();
                this.invalidate();
            }
        }
    }

    public void invalidateView(){
        this.invalidate();
    }


    public Bitmap getmBitmap(){
        return mBitmap;
    }

    public Canvas getmCanvas(){
        return mCanvas;
    }

    public void setCanvasToNull(){
        mCanvas = null;
    }
}