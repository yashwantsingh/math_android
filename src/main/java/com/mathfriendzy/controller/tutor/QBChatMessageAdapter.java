package com.mathfriendzy.controller.tutor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.tutor.ChatMessageModel;

import java.util.ArrayList;

/**
 * Created by root on 25/3/15.
 */
public class QBChatMessageAdapter extends BaseAdapter{

    private Context context = null;
    private ArrayList<ChatMessageModel> messages = null;
    private ViewHolder vHolder = null;
    private LayoutInflater mInflator = null;
    private String currentUserId = null;
    private final String TAG = this.getClass().getSimpleName();

    private TuturSessionFragment tutorSessionFragment = null;
    public QBChatMessageAdapter(Context context , ArrayList<ChatMessageModel> messages
            , TuturSessionFragment tutorSessionFragment){
        this.context = context;
        this.tutorSessionFragment = tutorSessionFragment;
        this.messages = messages;
        mInflator = LayoutInflater.from(context);
        currentUserId = this.getCurrectUserId();
    }

    private String getCurrectUserId(){
        /*try{
            return QuickBloxSingleton.getInstance().getCurrentUser().getId();
        }catch(Exception e){
            e.printStackTrace();
            return -1;
        }*/
        return MathFriendzyHelper.getSelectedPlayerID(context);
    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if(view  == null){
            vHolder = new ViewHolder();
            view = mInflator.inflate(R.layout.quick_blox_tutor_chatting_layout, null);
            vHolder.txtView = (TextView) view.findViewById(R.id.txtMsg);
            vHolder.txtSenderName = (TextView) view.findViewById(R.id.txtSenderName);
            view.setTag(vHolder);
        }else{
            vHolder = (ViewHolder) view.getTag();
        }

        if(messages.get(position).getIsTutor() == MathFriendzyHelper.YES){
            vHolder.txtView.setTextColor(context.getResources().getColor(R.color.SKY_BLUE));
        }else{
            vHolder.txtView.setTextColor(context.getResources().getColor(R.color.BLACK));
        }
        vHolder.txtSenderName.setText(messages.get(position).getName());
        //vHolder.txtSenderName.setText(messages.get(position).getProperties().get("name"));
        vHolder.txtView.setText(getCorrectMessage(messages.get(position).getMessage()));
        return view;
    }

    private String getCorrectMessage(String message){
        if(message != null && message.length() > 0
                && !message.equalsIgnoreCase("null"))
            return message;
        return "";
    }

    private class ViewHolder{
        private TextView txtView;
        private TextView txtSenderName;
    }

    /**
     * Check for own message
     * @param message
     * @return
     */
    private boolean isOwmMessage(ChatMessageModel message){
        try {
            if (currentUserId.equalsIgnoreCase(message.getSenderId()))
                return true;
            return false;
        }catch(Exception e){
            return true;
        }
    }


    public void addMessage(ChatMessageModel message){
       this.messages.add(message);
    }

    public String getAnonymousRequestMessage(){
        try {
            StringBuilder anonymousStudentTutorMessage = new StringBuilder("");
            for (int i = 0 ; i < this.messages.size(); i++) {
                if(i > 0)
                    anonymousStudentTutorMessage.append("\n" +
                            getCorrectMessage(messages.get(i).getMessage()));
                else
                    anonymousStudentTutorMessage.append(
                            getCorrectMessage(messages.get(i).getMessage()));
            }
            return anonymousStudentTutorMessage.toString();
        }catch(Exception e){
            return "";
        }
    }
}
