package com.mathfriendzy.controller.tutor;

/**
 * Created by root on 27/3/15.
 */
public interface ChatRequestIdCallback {
    void onChatRequesId(int chatRequestId);
    void onTutorTabTextSet(String tutorTabText);
    void onTimeSpent(int timeSpent);
    void showTutorTabContent(boolean ...isShow);
    void onlineStatus(boolean isOnline);
    void onActiveInActiveStatus(int inActive);
    void onLoadingCompleted();
    //void showTutorTabContent(TutorTabIconVisibility iconVisibility);
    //void onFragmentLoadFinish();
}
