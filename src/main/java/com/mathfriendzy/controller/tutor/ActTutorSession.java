package com.mathfriendzy.controller.tutor;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.homework.addhomeworksheet.ActViewPdf;
import com.mathfriendzy.controller.homework.workimageoption.HomeWorkImageOption;
import com.mathfriendzy.controller.managetutor.mystudent.ActMyStudentsViewDetail;
import com.mathfriendzy.controller.professionaltutoring.ActTutoringPackage;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.helper.YesNoListenerInterface;
import com.mathfriendzy.homewatcher.HomeWatcher;
import com.mathfriendzy.homewatcher.OnHomePressedListener;
import com.mathfriendzy.model.helpastudent.GetPlayerNeedHelpForTutorResponse;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.professionaltutoring.GetPlayerPurchaseTimeParam;
import com.mathfriendzy.model.professionaltutoring.GetPlayerPurchaseTimeResponse;
import com.mathfriendzy.model.professionaltutoring.PurchaseTimeCallback;
import com.mathfriendzy.model.professionaltutoring.UpdatePurchaseTimeResponse;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.tutor.FindTutorResponse;
import com.mathfriendzy.model.tutor.GetChatRequestParam;
import com.mathfriendzy.model.tutor.GetChatRequestResponse;
import com.mathfriendzy.model.tutor.StopWaitingForTutorParam;
import com.mathfriendzy.model.tutor.TutorSessionDisconnectedBuTutorParam;
import com.mathfriendzy.model.tutoringsessions.TutoringSessionsForStudentResponse;
import com.mathfriendzy.notification.DisconnectStudentNotif;
import com.mathfriendzy.notification.DisconnectWithTutorNotif;
import com.mathfriendzy.notification.HelpSetudentResponse;
import com.mathfriendzy.notification.PopUpActivityForNotification;
import com.mathfriendzy.notification.TutorImageUpdateNotif;
import com.mathfriendzy.notification.TutorWorkAreaImageUpdateNotif;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;

public class ActTutorSession extends ActBase implements ChatRequestIdCallback {

    private final String TAG = this.getClass().getSimpleName();
    private TuturSessionFragment fragment = null;
    private GetPlayerNeedHelpForTutorResponse selectedTutor = null;
    private Button btnTutorTab = null;
    private int tutorTimeSpentInSession = 0;
    private ImageView rateUsStar = null;
    private LinearLayout tutorContentLayout = null;
    private String alertPleaseAnswerQuestion = null;
    private String btnTitleOK = null;
    private String btnTitleCancel = null;
    private ImageView imgDisconnect = null;
    private final String DATE_FORMATE = "yyyy-MM-dd HH:mm:ss zzz";
    private UserPlayerDto selectedPlayer = null;
    private String alertWouldYouLikeToLetOtherAnswer = null;
    private String btnTitleYes = null;
    private String lblNo = null;
    private ImageView imgOnline = null;
    private ImageView imgHand = null;

    //for notificaiton
    private static ActTutorSession currentObj = null;

    private boolean isClickOnBackPress = false;
    private boolean isOpponentActiveInApp = true;
    private boolean isOpponentOnline = false;

    private String lbldisconnectPopupToTutor = null;
    private String alertMessageImagePasteOrNot = null;
    private boolean isShowTutorTabToTeacher = false;
    private UserPlayerDto playerFromManageTutor = null;
    private int selectedCategory = ActMyStudentsViewDetail.MY_TUTOR;

    //for tutoring session
    private boolean isForTutoringSession = false;
    private TutoringSessionsForStudentResponse tutoringObj = null;
    private ImageView imgMegnifier = null;
    private String alertWouldYouLikeToStopWaiting = null;
    private String alertWouldULikeToConnectOther = null;

    private final int FIND_TUTOR_REQUEST_FOR_MEGNIFIER = 1010;
    private FindTutorResponse findTutorDetail = null;
    private String  lblTutorArea = null;
    private int reqId = 0;


    //professional tutoring changes
    private boolean isForProfessionalTutoring = false;
    private String lblPleaseEnterQuestionToNotifyTutor = null;
    private String lblOrCancelTutorRequest = null;
    private String lbldisconnectPopupToStudent = null;
    private String waitingForATutorText = "Waiting for a tutor..";
    private String alertIfYouLeaveTutorArea = null;
    private boolean isAlertIfYouLeaveTutorAreaShown = false;
    private String lblHasYourQuesAnswered = null;
    private String tutorSessionTitle = null;
    private String lblYouHaveReachedYourTime = null;
    private final int OPEN_PURCHASE_PACKAGE_FROM_CURRENT_SCREEN_REQUEST = 500001;
    private TutorSessionBundleArgument savedArgument = null;
    private String lblThisStudentRunOutOfTime = null;
    private int timeSpentByTutorOrStudent = 0;
    private int onlyTutorTimeSpent = 0;
    private boolean isPaidSession = false;
    //end professional tutoring changes

    //audio calling changes
    private Button btnAudioCall = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_tutor_session);

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside onCreate()");

        selectedPlayer = this.getPlayerData();

        this.initializeCurrentObj();
        this.getIntentValues();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.addFragment();
        this.setTutorTabVisibility();

        this.startHomePressWatcher();

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "outside onCreate()");
    }

    private void setTutorTabVisibility(){
        if(isShowTutorTabToTeacher){
            btnTutorTab.setVisibility(Button.GONE);
        }
    }

    /**
     * Get the intent values
     */
    private void getIntentValues() {
        //professional tutoring changes
        isForProfessionalTutoring = this.getIntent().getBooleanExtra("isForProfessionalTutoring"
                , false);
        if(isForProfessionalTutoring){
            tutorSessionTitle = this.getIntent().getStringExtra("tutorSessionTitle");
        }
        //end professional tutoring changes

        isForTutoringSession = this.getIntent().getBooleanExtra("isForTutoringSession" , false);
        if(isForTutoringSession){
            tutoringObj = (TutoringSessionsForStudentResponse) this.getIntent()
                    .getSerializableExtra("tutoringSessionData");
        }else {
            isShowTutorTabToTeacher = this.getIntent().getBooleanExtra("isShowTutorTabToTeacher"
                    , false);
            selectedTutor = (GetPlayerNeedHelpForTutorResponse)
                    this.getIntent().getSerializableExtra("selectedTutor");
            if (isShowTutorTabToTeacher) {
                playerFromManageTutor = (UserPlayerDto) this.getIntent()
                        .getSerializableExtra("player");
                selectedCategory = this.getIntent().getIntExtra("selectedCategory", 0);
            }
        }
    }

    private TutorSessionBundleArgument getTutorSessionArgument(){
        TutorSessionBundleArgument argument = new TutorSessionBundleArgument();
        if(isForTutoringSession){
            reqId = tutoringObj.getRequestId();
            argument.setChatRequestedId(tutoringObj.getRequestId());
            argument.setQuesion(tutoringObj.getQueNo());
            argument.setWorkAreaForstudent(true);
            argument.setFindTutorDetail(findTutorDetail);
            argument.setForProfessionalTutoring(isForProfessionalTutoring);
            argument.setPaidSession(tutoringObj.getPaidSession());
            //professional tutoring changes
            if(isForProfessionalTutoring){
                argument.setQuesion("");
                argument.setHwId("0");
                argument.setCustomeHwId("0");
                argument.setTutorSessionTitle(tutorSessionTitle);
            }
            //endprofessional tutoring changes
        }else {
            argument.setFindTutorDetail(null);
            argument.setPlayerAns(null);
            if (selectedTutor != null) {
                reqId = selectedTutor.getRequestId();
                argument.setChatRequestedId(selectedTutor.getRequestId());
                argument.setTutor(true);
                argument.setChatDialogId(selectedTutor.getChatDialogId());
                argument.setAnonymous(selectedTutor.getIsAnonymous());
                argument.setSelectedTutor(selectedTutor);
            }
            argument.setShowTutorTabToTeacher(isShowTutorTabToTeacher);
            argument.setWorkAreaForstudent(true);
            if (isShowTutorTabToTeacher) {
                argument.setPlayerFromManageTutor(playerFromManageTutor);
                if (selectedCategory == ActMyStudentsViewDetail.MY_TUTOR) {//change student and tutor data
                    this.changeTutorAndManageStudentPlayerData();
                    argument.setSelectedTutor(selectedTutor);
                    argument.setPlayerFromManageTutor(playerFromManageTutor);
                } else if (selectedCategory == ActMyStudentsViewDetail.STUDENT_TUTORED) {
                    //nothing to do
                }
            }
            argument.setPaidSession(selectedTutor.getPaidSession());
        }
        return argument;
    }

    private Bundle getTutorSessionBundle(TutorSessionBundleArgument argument){
        Bundle bundle = new Bundle();
        bundle.putSerializable("arguments", argument);
        return bundle;
    }

    /**
     * Swap tutor and player data
     */
    private void changeTutorAndManageStudentPlayerData(){
        try {
            String tUdi = selectedTutor.getUserId();
            String tPid = selectedTutor.getPlayerId();
            String tChatId = selectedTutor.getChatId();
            String tChatUserName = selectedTutor.getChatUserName();
            String pUdi = playerFromManageTutor.getParentUserId();
            String pPid = playerFromManageTutor.getPlayerid();
            String pChatId = playerFromManageTutor.getChatId();
            String pChatUserName = playerFromManageTutor.getChatUserName();

            selectedTutor.setUserId(pUdi);
            selectedTutor.setPlayerId(pPid);
            selectedTutor.setChatId(pChatId);
            selectedTutor.setChatUserName(pChatUserName);
            playerFromManageTutor.setParentUserId(tUdi);
            playerFromManageTutor.setPlayerid(tPid);
            playerFromManageTutor.setChatId(tChatId);
            playerFromManageTutor.setChatUserName(tChatUserName);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Add the tutor session fragment
     */
    private void addFragment() {

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside addFragment()");

        final TutorSessionBundleArgument argument = this.getTutorSessionArgument();
        if(argument.getPaidSession() != null && argument.getPaidSession()
                .equalsIgnoreCase(MathFriendzyHelper.YES + "")){
            String userId = "";
            String playerId = "";
            if(isForProfessionalTutoring || isForTutoringSession){
                userId = selectedPlayer.getParentUserId();
                playerId = selectedPlayer.getPlayerid();
            }else{
                userId   = selectedTutor.getUserId();
                playerId = selectedTutor.getPlayerId();
            }

            this.getStudentPurchaseTime(new HttpResponseInterface() {
                @Override
                public void serverResponse
                        (HttpResponseBase httpResponseBase, int requestCode) {
                    GetPlayerPurchaseTimeResponse response =
                            (GetPlayerPurchaseTimeResponse) httpResponseBase;
                    if(response.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)){
                        if(response.getTime() > 0) {
                            argument.setStudentPurchaseTime(response.getTime());
                            addFragmentToShow(argument);
                        }else{
                            String alertMessage = "";
                            if(argument.isTutor()){
                                alertMessage = lblThisStudentRunOutOfTime;
                            }else{
                                //alertMessage = lblYouHaveReachedYourTime;
                                argument.setStudentPurchaseTime(0);
                                addFragmentToShow(argument);
                                return ;
                            }

                            MathFriendzyHelper.showWarningDialog(ActTutorSession.this ,
                                    alertMessage , new HttpServerRequest() {
                                        @Override
                                        public void onRequestComplete() {
                                            if(argument.isTutor()){
                                                finishActicity();
                                            }else {
                                                savedArgument = argument;
                                                Intent intent = new Intent(ActTutorSession.this
                                                        , ActTutoringPackage.class);
                                                startActivityForResult(intent,
                                                        OPEN_PURCHASE_PACKAGE_FROM_CURRENT_SCREEN_REQUEST);
                                            }
                                        }
                                    });
                        }
                    }
                }
            } , userId , playerId);

        }else{
            this.addFragmentToShow(argument);
        }

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "outside addFragment()");
    }

    private void addFragmentToShow(TutorSessionBundleArgument argument){
        FragmentTransaction fragmentTransaction = MathFriendzyHelper
                .getFragmentTransaction(MathFriendzyHelper.getFragmentManager(this));
        if (fragment == null) {
            fragment = new TuturSessionFragment();
            fragment.setArguments(this.getTutorSessionBundle(argument));
            fragmentTransaction.add(R.id.tutorSessionContainer, fragment);
        } else {
            fragmentTransaction.show(fragment);
        }
        fragmentTransaction.commit();
        this.initializeAudioButtonObject();
    }

    private void getStudentPurchaseTime(HttpResponseInterface responseInterface ,
                                        String userId , String playerId){
        if(CommonUtils.isInternetConnectionAvailable(this) &&
                selectedPlayer != null){
            GetPlayerPurchaseTimeParam param = new GetPlayerPurchaseTimeParam();
            param.setAction("getPlayersPurchasedTime");
            param.setpId(playerId);
            param.setuId(userId);
            new MyAsyckTask(ServerOperation
                    .createPostRequestForGetPlayersPurchasedTime(param)
                    , null, ServerOperationUtil.GET_PLAYER_PURCHASE_TIME, this,
                    responseInterface, ServerOperationUtil.SIMPLE_DIALOG , true ,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        }else{
            CommonUtils.showInternetDialog(this);
        }
    }

    @Override
    protected void setWidgetsReferences() {

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside setWidgetsReferences()");

        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        txtTopbar.setVisibility(TextView.GONE);
        btnTutorTab = (Button) findViewById(R.id.btnTutorTab);
        rateUsStar = (ImageView) findViewById(R.id.rateUsStar);
        tutorContentLayout = (LinearLayout) findViewById(R.id.tutorContentLayout);
        imgDisconnect = (ImageView) findViewById(R.id.imgDisconnect);
        imgOnline = (ImageView) findViewById(R.id.imgOnline);
        imgHand = (ImageView) findViewById(R.id.imgHand);
        imgMegnifier = (ImageView) findViewById(R.id.imgMegnifier);

        //audio calling changes
        btnAudioCall = (Button) findViewById(R.id.btnAudioCall);

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "outside setWidgetsReferences()");
    }

    @Override
    protected void setListenerOnWidgets() {
        imgDisconnect.setOnClickListener(this);
        imgOnline.setOnClickListener(this);
        imgHand.setOnClickListener(this);
        imgMegnifier.setOnClickListener(this);

        //audio calling changes
        //btnAudioCall.setOnClickListener(this);
    }

    @Override
    protected void setTextFromTranslation() {

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside setTextFromTranslation()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblTutor"));

        if (selectedTutor != null) {
            if (selectedTutor.getIsAnonymous() == 1) {//for anonymous tutor
                btnTutorTab.setText(transeletion.getTranselationTextByTextIdentifier("lblStudent")
                        + ": " + transeletion.getTranselationTextByTextIdentifier("lblAnonymous"));
            } else {//tutor find by user name
                btnTutorTab.setText(transeletion.getTranselationTextByTextIdentifier("lblStudent")
                        + ": " + selectedTutor.getPlayerFName() + " " + selectedTutor.getPlayerLName());
            }
        }

        alertPleaseAnswerQuestion = transeletion.getTranselationTextByTextIdentifier("alertPleaseAnswerQuestion");
        btnTitleOK = transeletion.getTranselationTextByTextIdentifier("btnTitleOK");
        btnTitleCancel = transeletion.getTranselationTextByTextIdentifier("btnTitleCancel");

        //before connecting with student popup , for disconnect
        alertWouldYouLikeToLetOtherAnswer = transeletion.getTranselationTextByTextIdentifier
                ("alertWouldYouLikeToLetOtherAnswer");
        //after connecting with student popup for disconnect
        lbldisconnectPopupToTutor = transeletion.getTranselationTextByTextIdentifier
                ("lbldisconnectPopupToTutor");
        btnTitleYes = transeletion.getTranselationTextByTextIdentifier("btnTitleYes");
        lblNo = transeletion.getTranselationTextByTextIdentifier("lblNo");
        alertMessageImagePasteOrNot = transeletion.getTranselationTextByTextIdentifier
                ("alertMessageImagePasteOrNot");
        alertWouldYouLikeToStopWaiting = transeletion.getTranselationTextByTextIdentifier
                ("alertWouldYouLikeToStopWaiting");
        alertWouldULikeToConnectOther = transeletion.getTranselationTextByTextIdentifier
                ("alertWouldULikeToConnectOther");
        lblTutorArea = transeletion.getTranselationTextByTextIdentifier("lblTutorArea");

        lblPleaseEnterQuestionToNotifyTutor = transeletion
                .getTranselationTextByTextIdentifier("lblPleaseEnterQuestionToNotifyTutor");
        lblOrCancelTutorRequest = transeletion.getTranselationTextByTextIdentifier("lblOrCancelTutorRequest");

        lbldisconnectPopupToStudent = transeletion
                .getTranselationTextByTextIdentifier("lbldisconnectPopupToStudent");
        waitingForATutorText = transeletion.getTranselationTextByTextIdentifier("lblWaitingForTutor");
        alertIfYouLeaveTutorArea = transeletion
                .getTranselationTextByTextIdentifier("alertIfYouLeaveTutorArea");
        lblHasYourQuesAnswered = transeletion
                .getTranselationTextByTextIdentifier("lblHasYourQuesAnswered");
        lblYouHaveReachedYourTime = transeletion
                .getTranselationTextByTextIdentifier("lblYouHaveReachedYourTime");
        lblThisStudentRunOutOfTime = transeletion
                .getTranselationTextByTextIdentifier("lblThisStudentRunOutOfTime");
        transeletion.closeConnection();

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "outside setTextFromTranslation()");
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase,
                               int requestCode) {
        this.initializeTimeSpentAndPaidSession();
        if (requestCode == ServerOperationUtil.TUTOR_SESSION_DESCONNECT_BY_TUTOR_REQUEST) {
            this.addTimeSpentOnBackPress();
        }else if (requestCode == ServerOperationUtil.STOP_WAITING_FOR_TUTOR_REQUEST) {
            this.finishActicity();
        }
    }

    //changes for professional tutoring
    private void disconnectWithTutorWhenScreenOpenForProfessionalTutoring(){
        if(isForTutoringSession){
            MathFriendzyHelper.yesNoConfirmationDialog(this,
                    lbldisconnectPopupToStudent
                    , btnTitleYes, lblNo,
                    new YesNoListenerInterface() {
                        @Override
                        public void onYes() {
                            stopWaitingForTutor();
                        }

                        @Override
                        public void onNo() {

                        }
                    });
        }
    }
    //end changes for professional tutoring

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgDisconnect:
                if(isForProfessionalTutoring){
                    this.disconnectWithTutorWhenScreenOpenForProfessionalTutoring();
                    return ;
                }

                if(isForTutoringSession){
                    MathFriendzyHelper.yesNoConfirmationDialog(this,
                            alertWouldYouLikeToStopWaiting
                            , btnTitleYes, lblNo,
                            new YesNoListenerInterface() {
                                @Override
                                public void onYes() {
                                    stopWaitingForTutor();
                                }

                                @Override
                                public void onNo() {

                                }
                            });
                    return ;
                }

                String dialogMessage = null;
                if (fragment != null
                        && fragment.isNeedToShowTutorPopUpOnBackPress()) {
                    dialogMessage = alertWouldYouLikeToLetOtherAnswer;
                }else{
                    dialogMessage = lbldisconnectPopupToTutor;
                }
                MathFriendzyHelper.yesNoConfirmationDialog(this,
                        dialogMessage , btnTitleYes, lblNo,
                        new YesNoListenerInterface() {
                            @Override
                            public void onYes() {
                                deleteTheTutorRequest();
                            }

                            @Override
                            public void onNo() {

                            }
                        });
                break;
            case R.id.imgOnline:
                break;
            case R.id.imgHand:
                if(isForTutoringSession){
                    this.clickOnHand();
                }
                break;
            case R.id.imgMegnifier:
                if(isForTutoringSession){
                    this.clickOnMegnifier();
                }
                break;
            //audio calling changes
            /*case R.id.btnAudioCall:
                this.clickOnAudioCall();
                break;*/
        }
    }

    /**
     * Click on hand
     */
    private void clickOnHand() {
        if (fragment != null) {
            fragment.showRateUsDialog();
        }
    }
    /**
     * Click on  magnifier
     */
    private void clickOnMegnifier() {
        MathFriendzyHelper.yesNoConfirmationDialog(this ,
                alertWouldULikeToConnectOther ,
                btnTitleYes , lblNo , new YesNoListenerInterface() {
                    @Override
                    public void onYes() {
                        //removeFragment();
                        Intent intent = new Intent(ActTutorSession.this,
                                ActFindTutor.class);
                        startActivityForResult(intent, FIND_TUTOR_REQUEST_FOR_MEGNIFIER);
                    }

                    @Override
                    public void onNo() {

                    }
                });
    }


    /**
     * Cancel the created request
     */
    private void stopWaitingForTutor() {
        if (CommonUtils.isInternetConnectionAvailable(this)) {
            StopWaitingForTutorParam param = new StopWaitingForTutorParam();
            param.setRequestId(tutoringObj.getRequestId());
            if(fragment.isChatConnected()){
                param.setAction("tutorSessionDisconnectByStudnent");
            }else{
                tutoringObj.setRequestId(0);
                param.setAction("stopWaitingForTutor");
            }
            new MyAsyckTask(ServerOperation.createPostRequestForStopWaitingForTutor(param)
                    , null, ServerOperationUtil.STOP_WAITING_FOR_TUTOR_REQUEST,
                    this, this, ServerOperationUtil.SIMPLE_DIALOG, true,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        } else {
            CommonUtils.showInternetDialog(this);
        }
    }

    @Override
    public void onChatRequesId(int chatRequestId) {

        //professional tutoring changes
        reqId = chatRequestId;
        if(isForTutoringSession){
            if(tutoringObj != null)
                tutoringObj.setRequestId(chatRequestId);
        }else{
            if(selectedTutor != null)
                selectedTutor.setRequestId(chatRequestId);
        }
        //end professional tutoring changes
    }

    @Override
    public void onTutorTabTextSet(String tutorTabText) {
        btnTutorTab.setText(tutorTabText);
    }

    @Override
    public void onTimeSpent(int timeSpent) {
        this.tutorTimeSpentInSession = timeSpent;
    }

    private void setImageVisibility(ImageView imgView, boolean isVisible) {
        if (isVisible) {
            imgView.setVisibility(ImageView.VISIBLE);
        } else {
            imgView.setVisibility(ImageView.GONE);
        }
    }

    @Override
    public void showTutorTabContent(boolean... isShow) {
        boolean isShowOnline = isShow[0];
        boolean isShowHand = isShow[1];
        boolean isShowDisconnect = isShow[2];
        this.setImageVisibility(imgOnline, isShowOnline);
        this.setImageVisibility(imgHand, isShowHand);
        this.setImageVisibility(imgDisconnect, isShowDisconnect);
        /*if(isShow.length == 4){
            boolean isShowMegnifier = isShow[3];
            this.setImageVisibility(imgMegnifier , isShowMegnifier);
        }*/
    }

    @Override
    public void onlineStatus(boolean isOnline) {
        if(isOnline){
            isOpponentOnline = true;
            imgOnline.setBackgroundResource(R.drawable.online_icon);
        }else{
            isOpponentOnline = false;
            if(isOpponentActiveInApp){
                imgOnline.setBackgroundResource(R.drawable.offline_icon);
            }else{
                imgOnline.setBackgroundResource(R.drawable.red_circle_icon);
            }
        }
    }

    @Override
    public void onActiveInActiveStatus(int inActive) {
        if(isOpponentOnline == false) {
            if (inActive == MathFriendzyHelper.YES) {
                isOpponentActiveInApp = false;
                imgOnline.setBackgroundResource(R.drawable.red_circle_icon);
            } else {
                isOpponentActiveInApp = true;
                imgOnline.setBackgroundResource(R.drawable.offline_icon);
            }
        }
    }

    private void backWhenScreenOPenForProfessionalTutoring() {
        if(reqId == 0){
            MathFriendzyHelper.yesNoConfirmationDialog(this ,
                    lblPleaseEnterQuestionToNotifyTutor + " " + lblOrCancelTutorRequest
                    , btnTitleOK , btnTitleCancel , new YesNoListenerInterface() {
                        @Override
                        public void onYes() {

                        }

                        @Override
                        public void onNo() {
                            finishActicity();
                        }
                    });
        }else{
            if (btnTutorTab.getText().toString()
                    .equalsIgnoreCase(waitingForATutorText)) {
                if(isAlertIfYouLeaveTutorAreaShown){
                    finishActicity();
                    return;
                }
                MathFriendzyHelper.showWarningDialogForSomeLongMessage(this,
                        alertIfYouLeaveTutorArea
                        , new HttpServerRequest() {
                            @Override
                            public void onRequestComplete() {
                                isAlertIfYouLeaveTutorAreaShown = true;
                            }
                        });
            }else{
                if(fragment != null && !fragment.isSessionRated()){
                    MathFriendzyHelper.yesNoConfirmationDialog(this ,
                            lblHasYourQuesAnswered ,
                            btnTitleYes , lblNo , new YesNoListenerInterface() {
                                @Override
                                public void onYes() {
                                    fragment.showRateUsDialog();
                                }

                                @Override
                                public void onNo() {
                                    finishActicity();
                                }
                            });
                }else{
                    finishActicity();
                }
            }
        }
    }

    private void initializeTimeSpentAndPaidSession(){
        try{
            if(fragment != null){
                isPaidSession = fragment.isPaidSession();
                if(isPaidSession) {
                    timeSpentByTutorOrStudent = fragment.getPaidSessionTimeSpent();
                }
                onlyTutorTimeSpent = fragment.getOnlyTutorTimeSpent();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        this.initializeTimeSpentAndPaidSession();

        //professional tutoring changes
        if(isForProfessionalTutoring){
            this.backWhenScreenOPenForProfessionalTutoring();
            return;
        }
        //end professional tutoring changes

        if(isForTutoringSession){
            if(fragment != null){
                if (btnTutorTab.getText().toString()
                        .equalsIgnoreCase(waitingForATutorText)) {
                    this.finishActicity();
                    return;
                }

                if(tutoringObj != null && tutoringObj.getRating() > 0){
                    this.finishActicity();
                    return;
                }

                if(!fragment.isSessionRated()){
                    MathFriendzyHelper.yesNoConfirmationDialog(this ,
                            lblHasYourQuesAnswered ,
                            btnTitleYes , lblNo , new YesNoListenerInterface() {
                                @Override
                                public void onYes() {
                                    fragment.showRateUsDialog();
                                }

                                @Override
                                public void onNo() {
                                    Intent intent = new Intent();
                                    intent.putExtra("timeSpent" ,
                                            timeSpentByTutorOrStudent
                                                    + fragment.getTimeSpentBeforePurchaseTimeFinish());
                                    setResult(RESULT_OK , intent);
                                    finishActicity();
                                }
                            });
                }else{
                    Intent intent = new Intent();
                    intent.putExtra("numberOfStarRated" ,
                            fragment.getNumberOfStarRated());
                    intent.putExtra("timeSpent" , timeSpentByTutorOrStudent
                            + fragment.getTimeSpentBeforePurchaseTimeFinish());
                    setResult(RESULT_OK , intent);
                    this.finishActicity();
                }
            }else {
                this.finishActicity();
            }
            return;
        }

        if(isShowTutorTabToTeacher){
            //finish();
            this.finishActicity();
            return ;
        }

        if(this.isShowPopUpForPasteImage()){
            MathFriendzyHelper.showWarningDialog(this , alertMessageImagePasteOrNot);
            return ;
        }

        isClickOnBackPress = true;
        if (fragment != null && fragment.isNeedToShowTutorPopUpOnBackPress()) {
            MathFriendzyHelper.yesNoConfirmationDialog(this,
                    alertPleaseAnswerQuestion, btnTitleOK,
                    btnTitleCancel,
                    new YesNoListenerInterface() {
                        @Override
                        public void onYes() {

                        }

                        @Override
                        public void onNo() {
                            deleteTheTutorRequest();
                        }
                    });
        } else {
            this.addTimeSpentOnBackPress();
        }
    }

    private boolean isShowPopUpForPasteImage(){
        try {
            if (fragment != null) {
                return fragment.isResizeImageLayoutOpen();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Go back to the previous screen
     */
    private void goBack() {
        Intent intent = new Intent();
        intent.putExtra("isNeedToRefereshList", true);
        setResult(RESULT_OK, intent);
        this.finishActicity();
    }

    private void addTimeSpentOnBackPress() {
        if (fragment != null && !isPaidSession) { //end changes and param added to addTimeSpentInTutorSessionOnBackPress
            fragment.addTimeSpentInTutorSessionOnBackPress(new HttpResponseInterface() {
                @Override
                public void serverResponse
                        (HttpResponseBase httpResponseBase, int requestCode) {
                    goBack();
                }
            } , -1);
        }else{
            goBack();
        }
    }

    /**
     * Disconnect the tutor session
     */
    private void deleteTheTutorRequest() {
        if (CommonUtils.isInternetConnectionAvailable(this)) {
            TutorSessionDisconnectedBuTutorParam param = new TutorSessionDisconnectedBuTutorParam();
            param.setChatRequestId(selectedTutor.getRequestId());
            param.setTutorPid(selectedPlayer.getPlayerid());
            param.setTutorUid(selectedPlayer.getParentUserId());
            param.setDate(MathFriendzyHelper
                    .getCurrentDateInGiveGformateDateWithDefaultTimeZone(DATE_FORMATE));
            param.setName(selectedPlayer.getFirstname() + " " + selectedPlayer.getLastname());

            if (fragment.isConnectedTutorWithoutSubmitButton()) {
                param.setAction("disconnectSessionWithoutAnswer");
                param.setDiconnectTutorWithoutAnswer(true);
            } else {
                param.setAction("tutorSessionDisconnectByTutor");
                param.setDiconnectTutorWithoutAnswer(false);
            }

            new MyAsyckTask(ServerOperation.createPostRequestForTutorSessionDisconnectByTutor(param)
                    , null, ServerOperationUtil.TUTOR_SESSION_DESCONNECT_BY_TUTOR_REQUEST,
                    ActTutorSession.this, ActTutorSession.this, ServerOperationUtil.SIMPLE_DIALOG, true,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        }
    }

    private void initializeCurrentObj() {
        currentObj = this;
    }

    public static ActTutorSession getCurrentObj() {
        return currentObj;
    }

    public void updateUIFromNotoficationForDisConnectWithTutor
            (final DisconnectWithTutorNotif response) {
        if (reqId ==
                MathFriendzyHelper.parseInt(response.getChatRequestId())) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(fragment != null) {
                        fragment.updateUIFromNotoficationForDisConnectWithTutor(response);
                        MathFriendzyHelper.showWarningDialog(ActTutorSession.this ,
                                response.getMessage() , new HttpServerRequest() {
                                    @Override
                                    public void onRequestComplete() {
                                        onBackPressed();
                                    }
                                });
                    }
                }
            });
            return;
        }else {
            Intent newIntent = new Intent(this, PopUpActivityForNotification.class);
            newIntent.putExtra(PopUpActivityForNotification.ACT_CALL_FOR,
                    PopUpActivityForNotification.CALL_FOR_DISCONNECT_TUTOR);
            newIntent.putExtra("notifData", response);
            startActivity(newIntent);
        }
    }

    /**
     * Update UI from notification when tutor accept and submit answer request for student
     * @param response
     */
    public void updateUIFromNotoficationForDisConnectWithStudent
    (final DisconnectStudentNotif response){
        if(fragment != null && (Integer.parseInt(response.getChatRequestId())
                == reqId)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(fragment != null)
                        fragment.updateUIFromNotificationForDisConnectWithStudent
                                (response);
                }
            });
        }
        Intent newIntent = new Intent(this, PopUpActivityForNotification.class);
        newIntent.putExtra(PopUpActivityForNotification.ACT_CALL_FOR,
                PopUpActivityForNotification.CALL_FOR_DISCONNECT_STUDENT);
        newIntent.putExtra("notifData", response);
        startActivity(newIntent);
    }


    public void updateUIFromNotoficationForTutorImageUpdate(final TutorImageUpdateNotif response) {
        if (reqId ==
                MathFriendzyHelper.parseInt(response.getChatRequestId())) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(isForTutoringSession)
                        response.setUpdateFor(TutorImageUpdateNotif.STUDENT);
                    else
                        response.setUpdateFor(TutorImageUpdateNotif.TUTOR);
                    if(fragment != null)
                        fragment.updateUIFromNotificationForTutorImageUpdate(response);
                }
            });
            return;
        }

        Intent newIntent = new Intent(this, PopUpActivityForNotification.class);
        newIntent.putExtra(PopUpActivityForNotification.ACT_CALL_FOR,
                PopUpActivityForNotification.CALL_FOR_TUTOR_AREA_IMAGE_UPDATE);
        newIntent.putExtra("notifData", response);
        startActivity(newIntent);
    }

    public void updateUIFromNotoficationForTutorWorkImageUpdate
            (final TutorWorkAreaImageUpdateNotif response) {
        if (reqId ==
                MathFriendzyHelper.parseInt(response.getChatRequestId())) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(fragment != null)
                        fragment.updateUIFromNotoficationForTutorWorkImageUpdate(response);
                }
            });
            return;
        }

        Intent newIntent = new Intent(this, PopUpActivityForNotification.class);
        newIntent.putExtra(PopUpActivityForNotification.ACT_CALL_FOR,
                PopUpActivityForNotification.CALL_FOR_TUTOR_WORK_AREA_IMAGE_UPDATE);
        newIntent.putExtra("notifData", response);
        startActivity(newIntent);
    }


    //for professional tutoring change
    /**
     * Update UI from notification when tutor accept and submit answer request for student
     * @param response
     */
    public void updateUIFromNotoficationForConnectWithStudent(final HelpSetudentResponse response){
        if (reqId == MathFriendzyHelper.parseInt(response.getChatReqId())) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(fragment != null)
                        fragment.updateUIFromNotificationForConnectWithStudent(response);
                }
            });
        }
        Intent newIntent = new Intent(this, PopUpActivityForNotification.class);
        newIntent.putExtra(PopUpActivityForNotification.ACT_CALL_FOR,
                PopUpActivityForNotification.CALL_FOR_HELP_STUDENT);
        newIntent.putExtra("notifData", response);
        startActivity(newIntent);
    }
    //end for professional tutoring change

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case HomeWorkImageOption.PICK_IMAGE_GALLERY:
                    String pickImageUrl = MathFriendzyHelper.getPath(this, data.getData());
                    if (pickImageUrl != null) {
                        this.openHomeworkSheetActivity(pickImageUrl);
                    }
                    break;
                case HomeWorkImageOption.PICK_CAMERA_IMAGE:
                    String capturedImageUrl = MathFriendzyHelper
                            .getImagePathUsingUri(this,
                                    HomeWorkImageOption.getCameraImageUri());
                    if (capturedImageUrl != null) {
                        this.openHomeworkSheetActivity(capturedImageUrl);
                    }
                    break;
                case HomeWorkImageOption.PICK_HOME_WORK_IMAGE:
                    String homeworkSheetImageUri = data.getStringExtra("cropImageUri");
                    if (homeworkSheetImageUri != null && homeworkSheetImageUri.length() > 0) {
                        if (fragment != null) {
                            fragment.onImageSelected(homeworkSheetImageUri);
                        }
                    }
                    break;
                case FIND_TUTOR_REQUEST_FOR_MEGNIFIER:
                    tutoringObj.setRequestId(0);
                    removeFragment();
                    fragment = null;
                    this.onTutorTabTextSet(lblTutorArea);
                    this.showTutorTabContent(false,false,false,false);
                    findTutorDetail = (FindTutorResponse) data.getSerializableExtra("findTutorResponse");
                    this.addFragment();
                    break;
                case MathFriendzyHelper.START_PURCHASE_SCREEN_REQUEST_CODE:
                    if(fragment != null){
                        fragment.updatePurchaseTime(data.getIntExtra("purchaseTime" , 0));
                    }
                    break;
                case OPEN_PURCHASE_PACKAGE_FROM_CURRENT_SCREEN_REQUEST:
                    int purchaseTime = data.getIntExtra("purchaseTime" , 0);
                    if(purchaseTime > 0){
                        savedArgument.setStudentPurchaseTime(purchaseTime);
                        addFragmentToShow(savedArgument);
                    }else{
                        finish();
                    }
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Open the homwwork sheet activity for image view
     *
     * @param pdfFileName
     */
    public void openHomeworkSheetActivity(String pdfFileName) {
        Intent intent = new Intent(this, ActViewPdf.class);
        intent.putExtra("pdfFileName", pdfFileName);
        intent.putExtra("isForImageSelection", true);
        intent.putExtra("isForImageView", true);
        startActivityForResult(intent, HomeWorkImageOption.PICK_HOME_WORK_IMAGE);
    }

    /**
     * Remove fragment
     */
    private void removeFragment(){
        if(fragment != null){
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.remove(fragment);
            transaction.commit();
        }
    }

    private void finishActicity(){
        if(fragment != null){
            if(isPaidSession){
                //new change for the time spent update on 24/09/2015 ,
                // the below method is called from hare
                this.initializeTimeSpentAndPaidSession();
                if(btnTutorTab.getText().toString()
                        .equalsIgnoreCase(waitingForATutorText)) {
                    removeFragment();
                    fragment = null;
                    finish();
                    return;
                }

                //end change
                fragment.deductStudentPurchaseTime(new PurchaseTimeCallback() {
                    @Override
                    public void onPurchase(UpdatePurchaseTimeResponse response) {
                        fragment.addTimeSpentInTutorSessionOnBackPress(new HttpResponseInterface() {
                            @Override
                            public void serverResponse(HttpResponseBase httpResponseBase,
                                                       int requestCode) {
                                removeFragment();
                                fragment = null;
                                finish();
                            }
                        } , (timeSpentByTutorOrStudent
                                + fragment.getTimeSpentBeforePurchaseTimeFinish()));
                    }
                } , timeSpentByTutorOrStudent , true);
            }else{
                removeFragment();
                fragment = null;
                finish();
            }
        }else {
            removeFragment();
            fragment = null;
            finish();
        }
    }


    private boolean isAppMinimize = false;
    private int normalTimerProgress = 0;
    private int paidTimerProgress = 0;
    private int onlineTimeSpent = 0;
    private int offlineTimeSpent = 0;
    private boolean isAppResumeFromBackground = false;
    private int timeSpentByStudentOrTutorBeforeTimeFinish = 0;

    private void doTheThingOnPause(){
        isAppMinimize = true;
        if(fragment != null) {
            normalTimerProgress = fragment.getNormalTimerProgress();
            paidTimerProgress = fragment.getPaidTimerProgress();
            onlineTimeSpent = fragment.getOnLineTimeSpent();
            offlineTimeSpent = fragment.getOfflineTimeSpent();
            timeSpentByStudentOrTutorBeforeTimeFinish = fragment.getTimeSpentByStudentOrTutorBeforeTimeFinish();
            fragment.pauseNormalTimer();
            fragment.pausePaidTimer();
        }
    }


    //change to resume from the background when user press home button
    @Override
    protected void onResume() {
        this.doTheThingsOnResume();
        super.onResume();
    }

    private void doTheThingsOnResume(){
        if(isAppMinimize){
            isAppMinimize = false;
            isAppResumeFromBackground = true;
            if(!CommonUtils.isInternetConnectionAvailable(this)){
                CommonUtils.showInternetDialog(this);
                return ;
            }

            if(!isForTutoringSession && selectedTutor != null){
                if(reqId > 0) {
                    GetChatRequestParam param = new GetChatRequestParam();
                    param.setAction("getChatRequest");
                    param.setChatRequestId(reqId);
                    new MyAsyckTask(ServerOperation.createPostTOGetChatRequest(param)
                            , null, ServerOperationUtil.GET_CHAT_REQUEST_SERVER_REQUEST,
                            this, new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                            GetChatRequestResponse response = (GetChatRequestResponse) httpResponseBase;
                            setSelectedTutorData(response);
                            removeAndAddFragment();
                        }
                    }, ServerOperationUtil.SIMPLE_DIALOG, true,
                            this.getString(R.string.please_wait_dialog_msg))
                            .execute();
                }
            }else{
                this.removeAndAddFragment();
            }
        }
    }

    private void removeAndAddFragment(){
        removeFragment();
        fragment = null;
        this.addFragment();
    }

    @Override
    public void onLoadingCompleted() {
        if(isAppResumeFromBackground) {
            isAppResumeFromBackground = false;
            if (fragment != null) {
                fragment.setNormalTimerProgress(normalTimerProgress);
                fragment.setPaidTimerProgress(paidTimerProgress);
                fragment.initializeOnlineTimeSpent(onlineTimeSpent);
                fragment.initializeOfflineTimeSpent(offlineTimeSpent);
                fragment.initializeTimeSpentByTutorOrStudentBeforeTimiFinish
                        (timeSpentByStudentOrTutorBeforeTimeFinish);
            }
        }
    }

    private void setSelectedTutorData(GetChatRequestResponse response){
        selectedTutor.setRequestId(response.getRequestId());
        //selectedTutor.setUserId(response.getUserId());
        //selectedTutor.setPlayerId(response.getPlayerId());
        //selectedTutor.setPlayerFName();
        //selectedTutor.setPlayerLName();
        //selectedTutor.setGrade();
        //selectedTutor.setChatId();
        //selectedTutor.setChatUserName();
        //selectedTutor.setInActive();
        selectedTutor.setReqDate(response.getReqDate());
        //selectedTutor.setMessage(response.getMessage());
        selectedTutor.setIsConnected(response.getIsConnected());
        //selectedTutor.setTutorUid(response.getTuturUid() + "");
        //selectedTutor.setTutorPid(response.getTutorPid() + "");
        //selectedTutor.setChatDialogId(response.getChatDialogId());
        selectedTutor.setIsAnonymous(response.getIsAnonymous());
        //selectedTutor.setTimeSpent(MathFriendzyHelper.parseInt(response.getTimeSpent()));
        selectedTutor.setStudentImage(response.getStudentImage());
        selectedTutor.setTutorImage(response.getTutorImage());
        selectedTutor.setWorkAreaImage(response.getWorkAreaImage());
        selectedTutor.setLastUpdatedByTutor(response.getLastUpdateByTutor() + "");
        selectedTutor.setStars(response.getRating());
        selectedTutor.setDisconnected(response.getDisconnected());
        //selectedTutor.setOpponentInput(response.getOpponentInput());
        //selectedTutor.setPaidSession(response.getP);
        //selectedTutor.setStudentOnline(response.getS);
    }

    private HomeWatcher mHomeWatcher = null;
    private void startHomePressWatcher(){
        mHomeWatcher = new HomeWatcher(this);
        mHomeWatcher.setOnHomePressedListener(new OnHomePressedListener() {
            @Override
            public void onHomePressed() {
                //CommonUtils.printLog(TAG , "Homebutton press");
                doTheThingOnPause();
            }
            @Override
            public void onHomeLongPressed() {
                //CommonUtils.printLog(TAG , "Homebutton long press");
            }
        });
        mHomeWatcher.startWatch();
    }

    @Override
    protected void onDestroy() {
        this.stopWatcher();
        super.onDestroy();
    }

    /**
     * Stop the homewatcher
     */
    private void stopWatcher(){
        try {
            if (mHomeWatcher != null) {
                mHomeWatcher.stopWatch();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     *
     * @param isConnect - true when internet is connected and false otherwise
     */
    public void updateTutorSessionOnInternetConnectionChange(boolean isConnect){
        if(fragment != null){
            if(isConnect){
                //fragment.internetConnected();
                doTheThingsOnResume();
            }else{
                fragment.internetConnectionLost();
                doTheThingOnPause();
                //CommonUtils.showInternetDialog(this);
                //doTheThingOnPause();
            }
        }
    }
    //change done for resume from background


    //audio calling changes
    /*private void clickOnAudioCall() {
        try {
            if (fragment != null) {
                fragment.startEndAudioCall();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }*/

    private void initializeAudioButtonObject(){
        if (fragment != null) {
            fragment.initializeAudioCallButton(btnAudioCall);
        }
    }
}
