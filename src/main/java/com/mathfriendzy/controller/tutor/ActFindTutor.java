package com.mathfriendzy.controller.tutor;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.tutor.FindTutorParam;
import com.mathfriendzy.model.tutor.FindTutorResponse;
import com.mathfriendzy.model.tutor.GetActiveTutorParam;
import com.mathfriendzy.model.tutor.GetActiveTutorResponse;
import com.mathfriendzy.model.tutor.GetAnnonymousTutorParam;
import com.mathfriendzy.model.tutor.GetAnnonymousTutorResponse;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;

public class ActFindTutor extends ActBase{

    private final String TAG = this.getClass().getSimpleName();
    private TextView txtFindATutorByUserName = null;
    private EditText edtSearchTutor = null;
    private Button btnGo = null;
    private TextView txtOrRequestAn = null;
    private Button btnRequestATutor = null;
    private UserPlayerDto selectedPlayerData = null;

    //for message
    private String alertMsgWeCouldNotFindThatPlayer = null;

    private TextView txtRequestAnAnonymousTutor = null;
    private TextView txtActiveTutor = null;
    private Button btnRequestPreviousTutor = null;
    private Button btnTutoringSession = null;
    private TextView txtPleaseNoteYourTeacher = null;

    private String lblActiveTutors = null;

    private String alertYouMayNoRequestLowerGradeTutor =
            "You may not request a tutor who is in a lower grade.";

    private int subjectId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_find_tutor);

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside onCreate()");

        selectedPlayerData = this.getPlayerData();

        MathFriendzyHelper.initializeProcessDialog(this);

        this.getIntentValues();
        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.getActiveTutor();

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside onCreate()");
    }

    private void getIntentValues() {
        subjectId = this.getIntent().getIntExtra("subjectId" , 0);
    }

    /**
     * Get Active anonymous tutor
     */
    private void getActiveTutor() {
        if(CommonUtils.isInternetConnectionAvailable(this)) {
            GetActiveTutorParam param = new GetActiveTutorParam();
            param.setAction("getTutorsFromOtherSchools");
            param.setUserId(selectedPlayerData.getParentUserId());
            param.setPlayerId(selectedPlayerData.getPlayerid());
            param.setGrade(selectedPlayerData.getGrade());
            param.setSchoolId(selectedPlayerData.getSchoolId());
            param.setSubjectId(subjectId);

            new MyAsyckTask(ServerOperation.createPostToGetActiveTutor(param)
                    , null, ServerOperationUtil.GET_ACTIVE_TUTOR_REQUEST, this,
                    this, ServerOperationUtil.SIMPLE_DIALOG, true,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        }
    }

    @Override
    protected void setWidgetsReferences() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside setWidgetsReferences()");

        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        txtFindATutorByUserName = (TextView) findViewById(R.id.txtFindATutorByUserName);
        edtSearchTutor = (EditText) findViewById(R.id.edtSearchTutor);
        btnGo = (Button) findViewById(R.id.btnGo);
        txtOrRequestAn = (TextView) findViewById(R.id.txtOrRequestAn);
        btnRequestATutor = (Button) findViewById(R.id.btnRequestATutor);

        txtRequestAnAnonymousTutor = (TextView) findViewById(R.id.txtRequestAnAnonymousTutor);
        txtActiveTutor = (TextView) findViewById(R.id.txtActiveTutor);
        txtPleaseNoteYourTeacher = (TextView) findViewById(R.id.txtPleaseNoteYourTeacher);
        btnRequestPreviousTutor = (Button) findViewById(R.id.btnRequestPreviousTutor);
        btnTutoringSession = (Button) findViewById(R.id.btnTutoringSession);

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside setWidgetsReferences()");

    }



    @Override
    protected void setTextFromTranslation() {

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside setTextFromTranslation()");
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblHomework") + "/" +
                transeletion.getTranselationTextByTextIdentifier("lblQuizzes"));
        txtFindATutorByUserName.setText(transeletion
                .getTranselationTextByTextIdentifier("lblFindTutorByUsername"));
        btnGo.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGo"));
        txtOrRequestAn.setText(transeletion.getTranselationTextByTextIdentifier
                ("lblOrRequestAnAnonymousTutor"));
        btnRequestATutor.setText(transeletion.getTranselationTextByTextIdentifier("lblRequestATutor"));
        txtRequestAnAnonymousTutor.setText(transeletion.getTranselationTextByTextIdentifier("lblRequestAnonymous"));
        txtActiveTutor.setText(transeletion.getTranselationTextByTextIdentifier("lblActiveTutors"));
        txtPleaseNoteYourTeacher.setText(transeletion.getTranselationTextByTextIdentifier("lblTeacherNote"));
        btnRequestPreviousTutor.setText(transeletion.getTranselationTextByTextIdentifier("btnRequestPreviousTutor"));
        btnTutoringSession.setText(transeletion.getTranselationTextByTextIdentifier("btnTutoringSession"));

        lblActiveTutors = transeletion.getTranselationTextByTextIdentifier("lblActiveTutors");
        alertMsgWeCouldNotFindThatPlayer = transeletion
                .getTranselationTextByTextIdentifier("alertMsgWeCouldNotFindThatPlayer");
        alertYouMayNoRequestLowerGradeTutor = transeletion
                .getTranselationTextByTextIdentifier("alertYouMayNoRequestLowerGradeTutor");
        transeletion.closeConnection();

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "outside setTextFromTranslation()");

    }

    /**
     * Set Number of active tutors
     * @param tutors
     */
    private void setActiveTutors(int tutors){
        txtActiveTutor.setText(lblActiveTutors + " " + tutors);
    }

    @Override
    protected void setListenerOnWidgets() {
        btnGo.setOnClickListener(this);
        btnRequestATutor.setOnClickListener(this);
        btnRequestPreviousTutor.setOnClickListener(this);
        btnTutoringSession.setOnClickListener(this);
    }

    private void clearEditText(){
        edtSearchTutor.setText("");
    }

    /**
     * Search a tutor by username
     */
    private void clickOnGo(){
        String userName = edtSearchTutor.getText().toString();

        if(!MathFriendzyHelper.checkForEmptyFields(userName)){
            FindTutorParam param = new FindTutorParam();
            param.setAction("getPlayersByUserNameForChat");
            param.setPlayerId(selectedPlayerData.getPlayerid());
            param.setUserId(selectedPlayerData.getParentUserId());
            param.setUserName(userName);

            if(CommonUtils.isInternetConnectionAvailable(this)){
                new MyAsyckTask(ServerOperation.createPostToGetTutorByUserName(param)
                        , null, ServerOperationUtil.GET_TUTOR_BY_USERNAME_REQUEST, this,
                        this, ServerOperationUtil.SIMPLE_DIALOG , true ,
                        getString(R.string.please_wait_dialog_msg))
                        .execute();
            }else{
                CommonUtils.showInternetDialog(this);
            }
        }

    }

    /**
     * Request a ananomous tutor
     */
    private void clickOnRequestATutor(){
        GetAnnonymousTutorParam param = new GetAnnonymousTutorParam();
        param.setAction("getAnnonymousTutors");
        param.setPlayerId(selectedPlayerData.getPlayerid());
        param.setSchoolId(selectedPlayerData.getSchoolId());
        param.setUserId(selectedPlayerData.getParentUserId());

        if(CommonUtils.isInternetConnectionAvailable(this)){
            new MyAsyckTask(ServerOperation.createPostToGetAnnonymousTutor(param)
                    , null, ServerOperationUtil.GET_ANNONYMOUS_TUTOR_REQUEST, this,
                    this, ServerOperationUtil.SIMPLE_DIALOG , true ,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        }else{
            CommonUtils.showInternetDialog(this);
        }
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnGo:
                this.clickOnGo();
                break;
            case R.id.btnRequestATutor:
                this.clickOnRequestATutor();
                break;
            case R.id.btnRequestPreviousTutor:
                MathFriendzyHelper.warningDialog(this , "Coming Soon!!!");
                break;
            case R.id.btnTutoringSession:
                MathFriendzyHelper.warningDialog(this , "Coming Soon!!!");
                break;
        }
    }

    /**
     * Check for tutor is in the lower grade that the player grade
     * @param response
     * @return
     */
    private boolean isTutorForLowerGrade(FindTutorResponse response){
        int playerGrade = MathFriendzyHelper.parseInt(selectedPlayerData.getGrade());
        int tutorGrade = MathFriendzyHelper.parseInt(response.getGrade());
        if(playerGrade > tutorGrade)
            return true;
        return false;
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase,
                               int requestCode) {
        if(requestCode == ServerOperationUtil.GET_TUTOR_BY_USERNAME_REQUEST){
            FindTutorResponse response = (FindTutorResponse) httpResponseBase;
            if(response.getResult().equals("success")){
                if(response.getIsTutorFind()){
                    if(this.isTutorForLowerGrade(response)){
                        this.clearEditText();
                        MathFriendzyHelper.showWarningDialog(this, alertYouMayNoRequestLowerGradeTutor);
                    }else{
                        this.doneAfterGetPlayerToChat(response);
                    }
                }else{
                    MathFriendzyHelper.showWarningDialog(this, alertMsgWeCouldNotFindThatPlayer);
                }
            }
        }else if(requestCode == ServerOperationUtil.GET_ANNONYMOUS_TUTOR_REQUEST){
            GetAnnonymousTutorResponse response = (GetAnnonymousTutorResponse) httpResponseBase;
            if(response.getResult().equals("success")){
                this.doneAfterGetPlayerToChat(null);
            }else{
                MathFriendzyHelper.showWarningDialog(this, alertMsgWeCouldNotFindThatPlayer);
            }
        }else if(requestCode == ServerOperationUtil.GET_ACTIVE_TUTOR_REQUEST){
            GetActiveTutorResponse response = (GetActiveTutorResponse) httpResponseBase;
            if(response.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)){
                this.setActiveTutors(response.getTutors());
            }
        }
    }


    private void doneAfterGetPlayerToChat(FindTutorResponse response){
        Intent intent = new Intent();
        intent.putExtra("findTutorResponse" , response);
        setResult(RESULT_OK , intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        //QBCreateSession.getInstance(this).logoutFromChat();
        super.onBackPressed();
    }
}
