package com.mathfriendzy.controller.tutor;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.homework.homwworkworkarea.HomeWorkWorkArea;
import com.mathfriendzy.controller.homework.workimageoption.HomeWorkImageOption;
import com.mathfriendzy.controller.homework.workimageoption.HomeworkImageOptionCallback;
import com.mathfriendzy.controller.homework.workimageoption.ResizeView;
import com.mathfriendzy.controller.homework.workimageoption.ResizeViewCallback;
import com.mathfriendzy.controller.homework.workimageoption.SelectImageConstants;
import com.mathfriendzy.controller.homework.workimageoption.SelectImageOption;
import com.mathfriendzy.controller.homework.workimageoption.TakeScreenShotAndSaveToMemoryCallback;
import com.mathfriendzy.controller.professionaltutoring.ActTutoringPackage;
import com.mathfriendzy.controller.reportaproblem.ReportProblemActivity;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.helper.YesNoListenerInterface;
import com.mathfriendzy.listener.OnRequestComplete;
import com.mathfriendzy.listener.OnScrollChange;
import com.mathfriendzy.listener.PhraseListListener;
import com.mathfriendzy.listener.RateUsCallback;
import com.mathfriendzy.model.devicedimention.DrawDeviceDimention;
import com.mathfriendzy.model.helpastudent.GetPlayerNeedHelpForTutorResponse;
import com.mathfriendzy.model.homework.checkhomework.GetDetailOfHomeworkWithCustomeResponse;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.professionaltutoring.PurchaseTimeCallback;
import com.mathfriendzy.model.professionaltutoring.UpdatePurchaseTimeResponse;
import com.mathfriendzy.model.ratetutorsession.RateTutorSessionCallback;
import com.mathfriendzy.model.ratetutorsession.RateTutorSessionParam;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.tutor.AddTimeSpentInTutorSessionParam;
import com.mathfriendzy.model.tutor.ChatMessageModel;
import com.mathfriendzy.model.tutor.EditTutorAreaParam;
import com.mathfriendzy.model.tutor.FindTutorResponse;
import com.mathfriendzy.model.tutor.GetChatRequestIdForProblemResponse;
import com.mathfriendzy.model.tutor.GetChatRequestIdParam;
import com.mathfriendzy.model.tutor.GetChatRequestParam;
import com.mathfriendzy.model.tutor.GetChatRequestResponse;
import com.mathfriendzy.model.tutor.GetDrawingPointForChatOnSuccess;
import com.mathfriendzy.model.tutor.GetDrawingPointForChatParam;
import com.mathfriendzy.model.tutor.GetDrawingPointForChatResponse;
import com.mathfriendzy.model.tutor.GetMessagesForTutorRequestParam;
import com.mathfriendzy.model.tutor.InputSeenByPlayerParam;
import com.mathfriendzy.model.tutor.PhraseCatagory;
import com.mathfriendzy.model.tutor.ReportThisTutorParam;
import com.mathfriendzy.model.tutor.SaveMessageForSingleChatCallback;
import com.mathfriendzy.model.tutor.SaveMessageForSingleChatParam;
import com.mathfriendzy.model.tutor.SaveMessageForSingleChatResponse;
import com.mathfriendzy.model.tutor.SendMessageToTutorParam;
import com.mathfriendzy.model.tutor.SendMessageToTutorResponse;
import com.mathfriendzy.model.tutor.UpdateDrawPointsForPlayerParam;
import com.mathfriendzy.model.tutor.UpdateTutorAreaImageName;
import com.mathfriendzy.notification.DisconnectStudentNotif;
import com.mathfriendzy.notification.DisconnectWithTutorNotif;
import com.mathfriendzy.notification.HelpSetudentResponse;
import com.mathfriendzy.notification.TutorImageUpdateNotif;
import com.mathfriendzy.notification.TutorWorkAreaImageUpdateNotif;
import com.mathfriendzy.oovoo.LoginListener;
import com.mathfriendzy.oovoo.OOVOOSDK;
import com.mathfriendzy.oovoo.OOVOOSDKListener;
import com.mathfriendzy.quickblox.QBCreateSession;
import com.mathfriendzy.quickblox.QBDialogs;
import com.mathfriendzy.quickblox.core.ChatManager;
import com.mathfriendzy.quickblox.core.GroupChatManagerImpl;
import com.mathfriendzy.quickblox.core.MyChatCallBack;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.serveroperation.serveroperationparamresponseclasses.GetActiveInActiveStatusOfStudentForTutorResponse;
import com.mathfriendzy.serveroperation.serveroperationparamresponseclasses.SetTutorAreaStatusParam;
import com.mathfriendzy.socketprograming.ClientCallback;
import com.mathfriendzy.socketprograming.MyGlobalWebSocket;
import com.mathfriendzy.socketprograming.MyWebSocket;
import com.mathfriendzy.timer.MyTimer;
import com.mathfriendzy.timer.TimerProgressCallback;
import com.mathfriendzy.uploadimages.UploadImageRequest;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.oovoo.sdk.api.sdk_error;
import com.oovoo.sdk.interfaces.AVChatListener;
import com.oovoo.sdk.interfaces.Participant;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.core.QBEntityCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.mathfriendzy.helper.MathFriendzyHelper.ON_SCROLLING;
import static com.mathfriendzy.helper.MathFriendzyHelper.SCROLL_DOWN_COMPLETE;
import static com.mathfriendzy.helper.MathFriendzyHelper.SCROLL_UP_COMPLETE;
import static com.mathfriendzy.helper.MathFriendzyHelper.getClientWebSocketTask;
import static com.mathfriendzy.helper.MathFriendzyHelper.getPNGFormateImageName;
import static com.mathfriendzy.helper.MathFriendzyHelper.getSelectedPlayerDataById;
import static com.mathfriendzy.helper.MathFriendzyHelper.getSelectedPlayerID;
import static com.mathfriendzy.helper.MathFriendzyHelper.initializeProcessDialog;
import static com.mathfriendzy.helper.MathFriendzyHelper.scrollDown;
import static com.mathfriendzy.helper.MathFriendzyHelper.scrollUp;


public class TuturSessionFragment extends Fragment implements OnClickListener , OnScrollChange ,
        ClientCallback , HttpResponseInterface , MyChatCallBack , TimerProgressCallback{

    private LinearLayout mySideDrawingLayout, othersideDrawingLayout = null;
    private Paint mySidePaint, otherSidePaint = null;
    private MyView mySideDrawingView, otherSideDrawingView = null;
    private Bitmap mySideBitmap, otherSideBitmap = null;
    private Button clearPaint = null;
    private Button drawPaint = null;
    private Button btnText = null;
    private Button btnCheck = null;
    private final String TAG = this.getClass().getSimpleName();
    private RelativeLayout studentTeacherLayout = null;
    //for teacher student text input
    private EditText edtTeacherStudentText = null;
    private TextView txtMakeItPrivate = null;
    private ListView lstteacherStudentMsgList = null;
    private QBChatMessageAdapter adapter = null;
    private boolean isWorkAreaForstudent = true;//true for student , false for teacher
    private Button btnSend = null;
    private ScrollView scrollView = null;
    private Button btnScrollUp = null;
    private Button btnScrollDown = null;
    //for erase mode
    private boolean eraserMode = false;
    //for eraser circle
    private ImageView imgEraser = null;
    private int workareapublic = 1;
    private TutorSessionBundleArgument arguments = null;
    private RelativeLayout tutorSessionTopBar = null;
    //for drawing chat
    private final int MY_SIDE_DRAWING = 1;
    private final int OTHER_SIDE_DRAWING = 2;
    private MyWebSocket drawingTask = null;
    //for quick blox chat
    UserPlayerDto selectedPlayerData = null;
    //Fragment Argument
    private FindTutorResponse findTutorDetail = null;
    //for qb Chat
    private ChatManager chat;
    private QBDialogs qbDialogs = null;
    public int chatRequestId = 0;
    private QBDialog newDialog = null;
    private String lblSorryCantConnectToTutor = null;
    private ChatRequestIdCallback callback = null;
    private boolean isChatAlreadyConnected = false;
    private String lblPleaseEnterProblemHere = null;
    private String lblYourSchollTutorWillBeNotified = "You school tutor will be notified";
    public boolean isTutor = false;
    private final int YES = 1;
    private final int NO = 0;
    private int isAnnonymous = 0;
    private String tutorText = "Tutor";
    private String studentText = "Student";
    private String anonymousText = "Anonymous";
    private String waitingForATutorText = "Waiting for a tutor..";
    private String lblDisconnect = "Disconnect";
    private String anonymousStudentTutorMessage = "";
    //for drawing chat
    private GetChatRequestResponse getChatResponse = null;
    private GetPlayerNeedHelpForTutorResponse selectedTutor = null;
    private final int POINT_TYPE_START = 1;
    private final int POINT_TYPE_MOVE = 2;
    private final int POINT_TYPE_STOP = 3;
    private String BEGIN = "begin";
    private String ERASE = "er";
    private String END = "end";
    private String SIZE = "size";
    private boolean isFirstDrawingFromRecieveMessage = true;

    //for update the draw points on the server
    private Handler updatePointsHandler = new Handler();
    private final int TIME_INTERVAL_AFTER_POINTS_SAVE = 5 * 1000;
    private ArrayList<String> drawingPointsList = new ArrayList<String>();
    private DisplayMetrics deviceDimention = null;
    private final String POINTS_PREFF = "p:";

    //for idle time
    private final int IDLE_TIME_LIMIT = 300;//in seconds
    private MyTimer timer;
    //for drawing from server
    private RelativeLayout drawingLayouts = null;
    //for rate us dialog
    private String lblPleaseRateThisSession = null;
    private String lblRate = null;
    private String lblReportThisTutor = null;

    //for submit question and answer
    private Button btnSubmitQuestionAnswer = null;
    private String alertSendQuestionToTutor = null;
    private String lblIfYouFeelNotQualified = null;
    private boolean isEditingEnable = true;
    private String alertYouCanNoLongerAddToQuestion = null;
    private boolean isStartDrawing = false;
    private boolean isClickOnSubmitQuestionAnswer = false;
    private String firstMessageToSend = "";

    //for student disconnect
    private boolean isChatConnected = false;
    private boolean isDisconnected = false;

    private String alertSessionHasBeenTerminated = null;
    private String alertYouCanRequestOtherTutor = null;

    //for student side data will not be updated until tutor will not click on submit button
    private boolean isTutorDataEnableForStudent = true;
    private Handler activeInactiveHandler = null;

    //for image operation
    private Button btnDeleteAndSelectImageOption = null;
    private HomeWorkImageOption imageOptionSelector = null;
    private String homeworkSheetName = null;
    private HomeWorkWorkArea homeworkAreaObj = null;
    private ImageView imgWorkAreaDrawing = null;
    private ImageLoader loader;//images loader
    private DisplayImageOptions options; // contains the display option for image in grid
    private SelectImageOption selectedOption = null;
    private ResizeView resizeView = null;
    private RelativeLayout bottomLayout = null;
    private ImageView imgStudentImage = null;
    private ImageView imgTutorImage = null;
    private RelativeLayout studentImageLayout = null;
    private RelativeLayout tutorImageLayout = null;
    //private String studentTutorImageName = "";

    private SelectImageOption imageSelectedOption = null;
    private SelectImageOption workAreaSelectedOption = null;

    private final int STUDENT_IMAGE = 1;
    private final int TUTOR_IMAGE = 2;
    private final int WORK_AREA_IMAGE = 3;
    private Bitmap selectedImageBitmap = null;
    private Bitmap studentWorkAreaImageBitmap = null;
    private RelativeLayout.LayoutParams rlParam = null;

    //for tutor image paste
    private int shouldSendPN = 0;
    private GetDetailOfHomeworkWithCustomeResponse studentDetail = null;
    private boolean isOpponentOnline = false;
    private boolean isSubmitButtonVisible = false;
    private String alertMsgAbusingWord = null;

    private boolean isShowTutorTabToTeacher = false;
    /*private String netConneted = "Internet Connected.";
    private String internetConnectionLost = "Internet Connection Lost.";*/
    private boolean isInternetConnected = true;
    private boolean isEditNotificationSent = false;
    private QBDialog joinChatDialog = null;
    /*private boolean joinChatSuccess = true;
    private final int MAX_TIME_TO_JOIN_CHAT_WHEN_RECONNECT = 10000;
    private String msgJoiningChat = "Joining Chat , Please wait...";
    private String msgJoinChatSuccess = "Join Chat Success.";*/
    private String alertYouCanNoLongerAddToAnswer
            = "You can't add to your answer until the student has responded.";
    private String dialogId = null;

    //for student tutoring session
    private boolean isSessionRated = false;
    private int numberOfStarRated = 0;

    private DisplayMetrics currentDevice = null;
    private DrawDeviceDimention dimention = null;

    //for drawing from server
    boolean myDrawingCompleted = false;
    boolean otherDrawingCompleted = false;

    private ProgressDialog pd = null;

    private boolean isDrawingDone = false;
    private boolean isQBLoginSuccess = false;
    private boolean isJoinChatSuccess = false;
    private boolean isStudentImageDone = false;
    private boolean isTutorImageDone = false;
    private boolean isWorkImageDone = false;
    private boolean isGetChatResponseDone = false;
    private boolean isRequestCreated = false;
    private boolean isDuplicateAreaAdded = false;

    private RelativeLayout viewGroup = null;
    private boolean isTab = false;

    //changes for professional tutoring
    private String alertEnterQuestionForPaidTutor = null;
    private boolean isForProfessionalTutoring = false;

    //change for tutoring session for professional tutoring
    private boolean isPaidSession = false;
    private int studentPurchaseTime = 0;
    private MyTimer studentPaidTutoringTimer = null;
    private boolean isSessionConnected = false;
    private String lblYouHaveReachedYourTime = null;
    private String lblThisStudentRunOutOfTime = null;
    private boolean isPurchaseTimeFinish = false;
    private boolean isAcceptAnswerButtonVisible = false;
    private Button btnAcceptQuestion = null;
    private final int IDEAL_TIME_FOR_PAID_SESSION = 300;
    private boolean isTutorInIdealMode = false;
    private int timeSpentByStudentOrTutorBeforeTimeFinish = 0;

    private int onLineTimeSpent = 0;
    private int offlineTimeSpent = 0;
    //end changes for professional tutoring

    private ExpandableListView lstPhraseList = null;
    private RelativeLayout phraseLayout = null;
    private Button btnPhraseList = null;
    private boolean isShowPhraseList = false;
    private ArrayList<PhraseCatagory> myPhraseList = null;
    private ExpandablePhraseAdapter phraseAdapter = null;

    private String lblPleaseReviewThisQuestion = null;
    private String lblOutTutorAvailableBtw = null;


    private boolean myDrawingCompletedFromHistoty = false;
    private boolean otherDrawingCompletedFromHistry = false;
    private boolean isSocketConnected = false;


    private GetDrawingPointForChatResponse drawingPointForChatResponseresponse = null;
    private GetDrawingPointForChatResponse drawingPointsFromChatRoomHistory = null;
    private boolean isDrawingFromServerRecieve = false;
    private boolean isDrawingFromChatHistoryRecieve = false;

    //Audio Call Changes
    private Button btnAudioCall = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        isTab = getResources().getBoolean(R.bool.isTablet);
        QBCreateSession.getInstance(getActivity()).logoutFromChat();
        this.init();
        this.initializeArguments();
        View view = inflater.inflate(R.layout.tutor_session_fragment, container, false);
        MathFriendzyHelper.initializeHeightAndWidth(getActivity() , isTab);
        this.setWidgetsReferences(view);
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.drawingMySideView();
        this.drawingOtherSideView();
        initializeProcessDialog(getActivity());
        //this.initializeQuickBlox();
        this.loginSuccess();
        this.setPencilSelected(true);
        this.setEraserSelected(false);

        if (isWorkAreaForstudent) {
            this.showChatWarningDialog();
            this.getActiveInActiveStatus();
        }

        //this.setTutorTabText();
        //getLoaderManager().initLoader(0, null, this);
        if(isTutor && isShowTutorTabToTeacher) {
            this.setDisableWholeWorkArea();
        }
        this.setBtnPhraseListButtonVisibility();

        //this.startHomePressWatcher();

        //Audio Call Changes
        this.initVoiceChat();

        return view;
    }


    /**
     * Create popup
     */
    private void showPopUp(){
        if(pd == null)
            pd = MathFriendzyHelper.getProgressDialog(getActivity());
        if(!pd.isShowing())
            pd.show();
    }

    /**
     * Hide popup
     */
    private void hidePopUp(){
        if(isDrawingDone && isStudentImageDone && isTutorImageDone && isWorkImageDone
                && isGetChatResponseDone) {//tutor
            if (pd != null && pd.isShowing()) {
                pd.cancel();
                this.startStudentPaidTutorTimer();
                this.startNormalTutorTimerInitially();
                if(callback != null){
                    callback.onLoadingCompleted();
                }
            }
        }
    }

    private void hidePopWithoutCondition(){
        if (pd != null && pd.isShowing())
            pd.cancel();
    }

    private void hidePopForRequestCreation(){
        if(isRequestCreated && isDuplicateAreaAdded) {
            isRequestCreated = false;
            isDuplicateAreaAdded = false;
            if (pd != null && pd.isShowing())
                pd.cancel();
            this.showPopUpAfterRequestCreatedByPaidStudent();
        }
    }

    private void showPopUpAfterRequestCreatedByPaidStudent() {
        if (this.isPaidSession() && !isTutor) {
            MathFriendzyHelper.showWarningDialogForSomeLongMessage(this.getActivity()
                    , lblOutTutorAvailableBtw, new HttpServerRequest() {
                @Override
                public void onRequestComplete() {

                }
            });
        }
    }

    /**
     * Set Disable All the thing when the tab is open for teacher to see the tutor session
     */
    private void setDisableWholeWorkArea() {
        edtTeacherStudentText.setEnabled(false);
        edtTeacherStudentText.setVisibility(EditText.GONE);
        btnSend.setVisibility(Button.GONE);
        btnSend.setBackgroundResource(R.drawable.light_gray);
        clearPaint.setVisibility(Button.INVISIBLE);
        drawPaint.setVisibility(Button.INVISIBLE);
        btnScrollUp.setVisibility(Button.INVISIBLE);
        btnScrollDown.setVisibility(Button.INVISIBLE);
        btnDeleteAndSelectImageOption.setVisibility(Button.INVISIBLE);
    }


    /**
     * Set Pencil Selected And UnSelected Background
     * @param isSelected
     */
    private void setPencilSelected(boolean isSelected){
        if(isSelected){
            drawPaint.setBackgroundResource(R.drawable.pencil);
        }else{
            drawPaint.setBackgroundResource(R.drawable.grey_pencil);
        }
    }

    /**
     * Set Erase Selected and Unselected background
     * @param isSelected
     */
    private void setEraserSelected(boolean isSelected){
        if(isSelected){
            clearPaint.setBackgroundResource(R.drawable.eraser);
        }else{
            clearPaint.setBackgroundResource(R.drawable.grey_eraser);
        }
    }


    /**
     * Initialize the variables
     */
    private void init() {
        currentDevice = MathFriendzyHelper.getDeviceDimention(getActivity());
        resizeView = new ResizeView(getActivity(), new ResizeViewCallback() {
            @Override
            public void onResizeDone(int tag, Bitmap bitmap, RelativeLayout.LayoutParams lp) {
                onResizeOption(tag, bitmap, lp);
            }
        });

        loader = ImageLoader.getInstance();
        options = MathFriendzyHelper.getDisplayOptionForTutoringSession();

        imageSelectedOption = new SelectImageOption();
        imageSelectedOption.setId(SelectImageConstants.SELECT_FROM_CAMERA);
        imageSelectedOption.setOptionName("Camera");
        imageSelectedOption.setIsVisible(0);

        workAreaSelectedOption = new SelectImageOption();
        workAreaSelectedOption.setId(SelectImageConstants.SELECT_FROM_WORK_AREA_IMAGE);
        workAreaSelectedOption.setOptionName("Work Area Drawing");
        workAreaSelectedOption.setIsVisible(0);
    }

    /**
     * Update UI after connect with student notif ication
     *
     * @param response
     */
    public void updateUIFromNotificationForConnectWithStudent(HelpSetudentResponse response) {
        //QBCreateSession.getInstance(getActivity()).logoutFromChat();
        this.pauseStudentPaidTimer();
        this.showPopUp();
        this.releaseChatHandler();
        this.reinitializevariables();
        this.getDrawingPointsFromServerForChat();
        this.setUpTimer();
        this.inputSeenByPlayer();
        //this.initializeQuickBlox();
        this.loginSuccess();
    }

    /**
     * Update UI after connect with student notification
     *
     * @param response
     */
    public void updateUIFromNotificationForDisConnectWithStudent(DisconnectStudentNotif response) {
        //QBCreateSession.getInstance(getActivity()).logoutFromChat();
        this.pauseStudentPaidTimer();
        this.showPopUp();
        this.releaseChatHandler();
        this.reinitializevariables();
        this.getDrawingPointsFromServerForChat();
        this.getDrawingPointsHistory();
        this.setUpTimer();
        this.inputSeenByPlayer();
        //this.initializeQuickBlox();
        this.loginSuccess();
    }

    /**
     * Disconnect by student for tutor
     *
     * @param response
     */
    public void updateUIFromNotoficationForDisConnectWithTutor
    (DisconnectWithTutorNotif response) {
        this.pauseStudentPaidTimer();
        this.stopTimer();
        //this.disconnectTutor();
    }

    private void reinitializevariables() {
        isStartDrawing = false;
        isEditingEnable = true;
        isClickOnSubmitQuestionAnswer = false;
        isChatConnected = false;
        isDisconnected = false;
        isTutorDataEnableForStudent = true;

        isDrawingDone  = false ;
        isQBLoginSuccess = false;
        isGetChatResponseDone = false;
        isJoinChatSuccess = false;
        isStudentImageDone = false;
        isTutorImageDone = false;
        isWorkImageDone = false;

        //professional tutoring change
        isTutorInIdealMode = false;
        //end change

        myDrawingCompletedFromHistoty = false;
        otherDrawingCompletedFromHistry = false;
        isSocketConnected = false;
        isDrawingFromServerRecieve = false;
        isDrawingFromChatHistoryRecieve = false;
    }

    /**
     * Initialize the argument
     */
    private void initializeArguments() {
        //drawingTask = getClientSocketTask(this);
        this.showPopUp();
        arguments = (TutorSessionBundleArgument) getArguments().getSerializable("arguments");
        //changes for professional tutoring
        isForProfessionalTutoring = arguments.isForProfessionalTutoring();
        //end changes for professional tutoring
        deviceDimention = MathFriendzyHelper.getDeviceDimention(getActivity());
        isWorkAreaForstudent = arguments.isWorkAreaForstudent();
        selectedPlayerData = this.getSelectedPlayer();
        if (!isWorkAreaForstudent) {
            studentDetail = arguments.getStudentDetail();
            if (studentDetail != null) {
                QBCreateSession.getInstance(getActivity()).logoutFromChat();
                selectedPlayerData = new UserPlayerDto();
                selectedPlayerData.setParentUserId(studentDetail.getParentId());
                selectedPlayerData.setPlayerid(studentDetail.getPlayerId());
                selectedPlayerData.setFirstname(studentDetail.getfName());
                selectedPlayerData.setLastname(studentDetail.getlName());
                selectedPlayerData.setChatId(studentDetail.getChatId());
                selectedPlayerData.setChatUserName(studentDetail.getChatUserName());
            }
        }

        qbDialogs = new QBDialogs(getActivity());
        //playerAns = arguments.getPlayerAns();
        findTutorDetail = arguments.getFindTutorDetail();
        chatRequestId = arguments.getChatRequestedId();

        if (chatRequestId > 0)
            isChatAlreadyConnected = true;
        else
            isChatAlreadyConnected = false;

        isTutor = arguments.isTutor();
        if (isTutor) {
            this.createDrawingTaskObject();
            isAnnonymous = arguments.isAnonymous();
            selectedTutor = arguments.getSelectedTutor();
            isShowTutorTabToTeacher = arguments.isShowTutorTabToTeacher();
            if(isShowTutorTabToTeacher) {
                selectedPlayerData = arguments.getPlayerFromManageTutor();
                isWorkAreaForstudent = false;
            }
            //callback.showTutorTabContent(true);
        }

        //for image selection option
        homeworkSheetName = arguments.getHomeworkSheetName();
        this.getDrawingPointsFromServerForChat();
        if (isWorkAreaForstudent) {
            this.setUpTimer();
            this.inputSeenByPlayer();
            this.initializeImageSelector();
        }

        this.initializeIsPaidSession(arguments.getPaidSession());
        this.initializeStudentPurchaseTime(arguments.getStudentPurchaseTime());
        this.updateTutorAreaStatus(MathFriendzyHelper.YES);
    }

    /**
     * Create drawing task object for socket programing
     */
    private void createDrawingTaskObject() {
        if (drawingTask == null && chatRequestId > 0) {
            String playerId = "";
            if(selectedPlayerData != null){
                playerId = selectedPlayerData.getPlayerid();
            }
            drawingTask = getClientWebSocketTask(getActivity() , this , chatRequestId , playerId);
        }
    }

    /**
     * Show chat warning dialog
     */
    private void showChatWarningDialog() {
        if (isTutor) {//for tutor
            if (selectedTutor != null && selectedTutor.isFromDisconnectedList()) {
                this.setSubmitQuestionAnswerButtnVisibility(true);
                callback.showTutorTabContent(true, false, true);
                //change for tutoring session for professional tutoring
                if(isPaidSession) {
                    this.setVisibilityOfAcceptAnswerButton(true);
                    MathFriendzyHelper.warningDialogForLongMsg(getActivity(),
                            lblPleaseReviewThisQuestion);
                }else{
                    MathFriendzyHelper.showWarningDialogForSomeLongMessage(getActivity(),
                            lblIfYouFeelNotQualified);
                }
                //end  change for tutoring session for professional tutoring
            } else {
                if (selectedTutor.getDisconnected() == YES) {
                    this.disconnectTutor();
                } else {
                    callback.showTutorTabContent(true, false, true);
                }
            }
        } else {//for student
            if (chatRequestId == 0) {
                this.setSubmitQuestionAnswerButtnVisibility(true);
                //changes for professional tutoring if block added
                if(isForProfessionalTutoring){
                    this.showProfessionalTutoringDialog();
                }else {
                    MathFriendzyHelper.showWarningDialogForSomeLongMessage(getActivity(),
                            alertSendQuestionToTutor);
                }
            }
        }
    }

    private void showProfessionalTutoringDialog(){
        if(!MathFriendzyHelper.getBooleanValueFromSharedPreff(getActivity() ,
                MathFriendzyHelper.IS_ALREADY_PAID_TUTOR_SHOW_DIALOG_KEY)) {
            MathFriendzyHelper.showProfessionalTutoringPaidTutorDialog(getActivity(),
                    alertEnterQuestionForPaidTutor);
        }
    }

    /**
     * Disconnect tutor
     */
    public void disconnectTutor() {
        isDisconnected = true;
        callback.onTutorTabTextSet(studentText + ":" + lblDisconnect);
        this.initilizaEnableVariable(false);
        callback.showTutorTabContent(false, false, false);
    }

    /**
     * Set the submit question answer button visibility
     *
     * @param isVisible
     */
    private void setSubmitQuestionAnswerButtnVisibility(boolean isVisible) {
        isSubmitButtonVisible = isVisible;
        if (isVisible) {
            btnSubmitQuestionAnswer.setVisibility(Button.VISIBLE);
        } else {
            btnSubmitQuestionAnswer.setVisibility(Button.GONE);
        }
    }

    /**
     * Set the submit question answer button visibility
     *
     * @param isVisible
     */
    private void setVisibilityOfAcceptAnswerButton(boolean isVisible) {
        isAcceptAnswerButtonVisible = isVisible;
        if (isVisible) {
            btnAcceptQuestion.setVisibility(Button.VISIBLE);
        } else {
            btnAcceptQuestion.setVisibility(Button.GONE);
        }
    }

    /**
     * Return the selected player
     *
     * @return
     */
    private UserPlayerDto getSelectedPlayer() {
        return getSelectedPlayerDataById
                (getActivity(), getSelectedPlayerID(getActivity()));
    }

    /**
     * Call after login success to quick blox
     */
    private void loginSuccess() {
        if (CommonUtils.LOG_ON)
            Log.e(TAG, "loginSuccess");

        isQBLoginSuccess = true;
        this.hidePopUp();

        if (isTutor) {
            isGetChatResponseDone = true;
            this.hidePopUp();
            this.getChatDialog(arguments.getChatDialogId());
            this.swapStudentAndTutorImageLayout(Integer.parseInt
                    (selectedTutor.getLastUpdatedByTutor()));
            this.setStudentImage(selectedTutor.getStudentImage());
            this.setTutorImage(selectedTutor.getTutorImage());
            this.setWorkAreaImage(selectedTutor.getWorkAreaImage());
        } else {
            try {
                if (chatRequestId > 0) {
                    CommonUtils.printLog(TAG, "chat requested id " + chatRequestId);
                    GetChatRequestParam param = new GetChatRequestParam();
                    param.setAction("getChatRequest");
                    param.setChatRequestId(chatRequestId);
                    new MyAsyckTask(ServerOperation.createPostTOGetChatRequest(param)
                            , null, ServerOperationUtil.GET_CHAT_REQUEST_SERVER_REQUEST,
                            getActivity(), this, ServerOperationUtil.SIMPLE_DIALOG, false,
                            getActivity().getString(R.string.please_wait_dialog_msg))
                            .execute();
                } else {
                    CommonUtils.printLog(TAG, " Dialog not created " + chatRequestId);
                    isGetChatResponseDone = true;
                    isStudentImageDone = true;
                    isTutorImageDone = true;
                    isWorkImageDone = true;
                    isDrawingDone = true;
                    hidePopUp();
                    this.createChatDialog();
                }
            } catch (Exception e) {
                e.printStackTrace();
                CommonUtils.printLog(TAG, "exception " + e.toString());
            }
        }
    }


    /**
     * Get Chat dialog
     *
     * @param dialogId
     */
    private void getChatDialog(String dialogId) {
        joinChat(null);
    }

    /**
     * Create chat dialog
     */
    private void createChatDialog() {
        if (findTutorDetail != null) {
            joinChat(null);
        }else{
            isJoinChatSuccess = true;
            hidePopUp();
            isAnnonymous = YES;
        }
    }

    /**
     * Join the quick blox chat
     *
     * @param dialog
     */
    private void joinChat(final QBDialog dialog) {
        loadChatHistory(dialog);
    }


    /**
     * Load chat history
     */
    private void loadChatHistory(QBDialog dialog) {
        if(CommonUtils.isInternetConnectionAvailable(getActivity()) && chatRequestId > 0){
            GetMessagesForTutorRequestParam param = new GetMessagesForTutorRequestParam();
            param.setAction("getMessagesForTutorRequest");
            param.setReqId(chatRequestId);
            MathFriendzyHelper.getChatMessageForTutorRequest(getActivity() ,
                    param , new ChatMessageCallback() {
                        @Override
                        public void onMessages(ArrayList<ChatMessageModel> messages) {
                            try {
                                adapter = new QBChatMessageAdapter(getActivity(), messages,
                                        TuturSessionFragment.this);
                                lstteacherStudentMsgList.setAdapter(adapter);
                                lstteacherStudentMsgList.setSelection
                                        (lstteacherStudentMsgList.getAdapter().getCount() - 1);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            //startInitialTimerForTutor();
                        }
                    });
        }else{
            //startInitialTimerForTutor();
        }
        startInitialTimerForTutor();
    }

    private void startInitialTimerForTutor(){
        isJoinChatSuccess = true;
        hidePopUp();
        /*//start timer here
        setInitialTimerProgress();
        startTimer();*/
    }

    private void startNormalTutorTimerInitially(){
        try {
            if (timer != null) {
                setInitialTimerProgress();
                startTimer();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Set the widgets references
     *
     * @param view
     */
    private void setWidgetsReferences(View view) {
        viewGroup = (RelativeLayout) view.findViewById(R.id.viewGroup);
        mySideDrawingLayout = (LinearLayout) view.findViewById(R.id.mySideDrawingLayout);
        othersideDrawingLayout = (LinearLayout) view.findViewById(R.id.othersideDrawingLayout);
        clearPaint = (Button) view.findViewById(R.id.clearPaint);
        drawPaint = (Button) view.findViewById(R.id.draw);
        btnText = (Button) view.findViewById(R.id.btnText);
        btnCheck = (Button) view.findViewById(R.id.btnCheck);
        studentTeacherLayout = (RelativeLayout) view.findViewById(R.id.studentTeacherLayout);

        //for teacher student text input
        edtTeacherStudentText = (EditText) view.findViewById(R.id.edtTeacherStudentText);
        txtMakeItPrivate = (TextView) view.findViewById(R.id.txtMakeItPrivate);
        txtMakeItPrivate.setVisibility(TextView.GONE);
        lstteacherStudentMsgList = (ListView) view.findViewById(R.id.teacherStudentMsgList);
        btnSend = (Button) view.findViewById(R.id.btnSend);

        //for click scrolling
        scrollView = (ScrollView) view.findViewById(R.id.scrollView);
        btnScrollUp = (Button) view.findViewById(R.id.btnScrollUp);
        btnScrollDown = (Button) view.findViewById(R.id.btnScrollDown);

        //for eraser circle
        imgEraser = (ImageView) view.findViewById(R.id.imgEraser);
        tutorSessionTopBar = (RelativeLayout) getActivity().findViewById(R.id.relativeTopBar);
        //for drawing from server
        drawingLayouts = (RelativeLayout) view.findViewById(R.id.drawingLayouts);
        //for submit question and answer
        btnSubmitQuestionAnswer = (Button) view.findViewById(R.id.btnSubmitQuestionAnswer);
        //for image operation
        imgStudentImage = (ImageView) view.findViewById(R.id.imgStudentImage);
        imgTutorImage = (ImageView) view.findViewById(R.id.imgTutorImage);
        studentImageLayout = (RelativeLayout) view.findViewById(R.id.studentImageLayout);
        tutorImageLayout = (RelativeLayout) view.findViewById(R.id.tutorImageLayout);

        btnDeleteAndSelectImageOption = (Button) view.findViewById(R.id.btnDeleteAndSelectImageOption);
        imgWorkAreaDrawing = (ImageView) view.findViewById(R.id.imgWorkAreaDrawing);
        bottomLayout = (RelativeLayout) view.findViewById(R.id.layout);
        resizeView.setWidgetsReferences(view);
        resizeView.setOtherView(studentTeacherLayout, tutorSessionTopBar, bottomLayout);
        this.setVisibilityOfContent();

        //change for tutoring session for professional tutoring
        btnAcceptQuestion = (Button) view.findViewById(R.id.btnAcceptQuestion);
        //end change for tutoring session for professional tutoring

        //for phrase change
        lstPhraseList = (ExpandableListView) view.findViewById(R.id.lstPhraseList);
        phraseLayout = (RelativeLayout) view.findViewById(R.id.phraseLayout);
        btnPhraseList = (Button) view.findViewById(R.id.btnPhraseList);

        //Audio Call Changes
        //btnAudioCall = (Button) view.findViewById(R.id.btnAudioCall);
    }

    /**
     * Hide the visibility of the content when tutor session is open for check homework by the teacher
     */
    private void setVisibilityOfContent() {
        if (!isWorkAreaForstudent) {//for check homework
            this.setDisableWholeWorkArea();
        }
    }


    /**
     * Set text from translation
     */
    private void setTextFromTranslation() {
        Translation translate = new Translation(getActivity());
        translate.openConnection();
        txtMakeItPrivate.setText(translate.getTranselationTextByTextIdentifier("lblMakeItPrivate"));
        btnSend.setText(translate.getTranselationTextByTextIdentifier("lblSend"));
        lblSorryCantConnectToTutor = translate.getTranselationTextByTextIdentifier("lblSorryCantConnectToTutor");
        lblPleaseEnterProblemHere = translate.getTranselationTextByTextIdentifier("lblPleaseEnterProblemHere");

        tutorText = translate.getTranselationTextByTextIdentifier("lblTutor");
        studentText = translate.getTranselationTextByTextIdentifier("lblStudent");
        anonymousText = translate.getTranselationTextByTextIdentifier("lblAnonymous");
        waitingForATutorText = translate.getTranselationTextByTextIdentifier("lblWaitingForTutor");

        lblRate = translate.getTranselationTextByTextIdentifier("lblRate");
        lblPleaseRateThisSession = translate.getTranselationTextByTextIdentifier("lblRateThisTutorAlert");
        lblReportThisTutor = translate.getTranselationTextByTextIdentifier("lblReportThisTutor");

        if (isTutor) {//for tutor
            btnSubmitQuestionAnswer.setText
                    (translate.getTranselationTextByTextIdentifier("lblSubmitAnswer"));
        } else {//for student
            btnSubmitQuestionAnswer.setText
                    (translate.getTranselationTextByTextIdentifier("lblSubmitQuestion"));
        }

        alertSendQuestionToTutor = translate.getTranselationTextByTextIdentifier("alertSendQuestionToTutor");
        lblIfYouFeelNotQualified = translate.getTranselationTextByTextIdentifier("lblIfYouFeelNotQualified");
        alertYouCanNoLongerAddToQuestion = translate
                .getTranselationTextByTextIdentifier("alertYouCanNoLongerAddToQuestion");
        lblDisconnect = translate.getTranselationTextByTextIdentifier("lblDisconnect");
        alertSessionHasBeenTerminated = translate.
                getTranselationTextByTextIdentifier("alertSessionHasBeenTerminated");
        alertYouCanRequestOtherTutor = translate.
                getTranselationTextByTextIdentifier("alertYouCanRequestOtherTutor");
        alertMsgAbusingWord = translate.getTranselationTextByTextIdentifier("alertMsgAbusingWord");
        alertYouCanNoLongerAddToAnswer = translate.getTranselationTextByTextIdentifier("alertYouCanNoLongerAddToAnswer");
        //netConneted = translate.getTranselationTextByTextIdentifier("lblNetConnected");
        //internetConnectionLost = translate.getTranselationTextByTextIdentifier("lblNetConnectionLost");

        //changes for professional tutoring
        alertEnterQuestionForPaidTutor = translate.getTranselationTextByTextIdentifier("alertEnterQuestionForPaidTutor");
        lblYouHaveReachedYourTime = translate.getTranselationTextByTextIdentifier("lblYouHaveReachedYourTime");
        lblThisStudentRunOutOfTime = translate.getTranselationTextByTextIdentifier("lblThisStudentRunOutOfTime");
        btnAcceptQuestion.setText(translate.getTranselationTextByTextIdentifier("lblAcceptQuestion"));
        lblPleaseReviewThisQuestion = translate.getTranselationTextByTextIdentifier("lblPleaseReviewThisQuestion");
        lblOutTutorAvailableBtw = translate.getTranselationTextByTextIdentifier("lblOurTutorAreAvailable");
        //end changes for professional tutoring
        translate.closeConnection();
    }


    /**
     * Set the listener on the widgets
     */
    private void setListenerOnWidgets() {

        clearPaint.setOnClickListener(this);
        drawPaint.setOnClickListener(this);
        btnText.setOnClickListener(this);
        btnCheck.setOnClickListener(this);
        btnSend.setOnClickListener(this);
        btnSubmitQuestionAnswer.setOnClickListener(this);

        btnDeleteAndSelectImageOption.setOnClickListener(this);

        scrollUp(btnScrollUp, scrollView, this);
        scrollDown(btnScrollDown, scrollView, this);

        //change for professional tutoring
        btnAcceptQuestion.setOnClickListener(this);
        //end change for professional tutoring

        //phrase list
        btnPhraseList.setOnClickListener(this);

        //Audio Call Changes
        //btnAudioCall.setOnClickListener(this);
    }

    private void showCantEditDialog() {
        if(!(CommonUtils.isInternetConnectionAvailable
                (getActivity()) && isInternetConnected)){
            CommonUtils.showInternetDialog(getActivity());
            return;
        }

        if (isDisconnected) {
            if (isTutor) {
                MathFriendzyHelper.showWarningDialogForSomeLongMessage(getActivity(),
                        alertSessionHasBeenTerminated);
            } else {
                MathFriendzyHelper.showWarningDialogForSomeLongMessage(getActivity(),
                        alertSessionHasBeenTerminated + " " + alertYouCanRequestOtherTutor);
            }
        } else {
            if(isTutor){
                MathFriendzyHelper.showWarningDialogForSomeLongMessage(getActivity(),
                        alertYouCanNoLongerAddToAnswer);
            }else{
                if(isPaidSession && isPurchaseTimeFinish){
                    warningDialogWhenPurchaseTimeForStudentFinish();
                }else {
                    MathFriendzyHelper.showWarningDialogForSomeLongMessage(getActivity(),
                            alertYouCanNoLongerAddToQuestion);
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.clearPaint:
                if (this.isEditingEnable) {
                    this.eraseMode();
                } else {
                    this.showCantEditDialog();
                }
                break;
            case R.id.draw:
                if (this.isEditingEnable) {
                    this.drawMode();
                } else {
                    this.showCantEditDialog();
                }
                break;
            case R.id.btnCheck:
                workareapublic = (workareapublic + 1) % 2;
                this.setCheckedBackground();
                break;
            case R.id.btnScroll:
                clearPaint.setBackgroundResource(R.drawable.eraser);
                drawPaint.setBackgroundResource(R.drawable.pencil);
                break;
            case R.id.btnSend:
                if(CommonUtils.isInternetConnectionAvailable(getActivity())
                        && isInternetConnected) {
                    if (isWorkAreaForstudent) {
                        if (this.isEditingEnable) {
                            this.sendMessage(edtTeacherStudentText.getText().toString());
                            //this.checkForIdleTimeAndSetProgress();
                            startTimer();
                            this.setLastUpdatedTimeCounter();

                            //professional tutoring change
                            this.activeTutorOnSession();
                            //end change
                        } else {
                            this.showCantEditDialog();
                        }
                    }
                }else{
                    CommonUtils.showInternetDialog(getActivity());
                }
                break;
            case R.id.btnSubmitQuestionAnswer:
                if(!CommonUtils.isInternetConnectionAvailable(getActivity())
                        || !isInternetConnected){
                    CommonUtils.showInternetDialog(getActivity());
                    return ;
                }
                isClickOnSubmitQuestionAnswer = true;
                isTutorInIdealMode = false;
                this.clickOnSubmitQuestionAnswerButton();
                break;
            case R.id.btnDeleteAndSelectImageOption:
                if(!CommonUtils.isInternetConnectionAvailable(getActivity())
                        || !isInternetConnected){
                    CommonUtils.showInternetDialog(getActivity());
                    return ;
                }

                if (this.isEditingEnable) {
                    this.clickToSelectAndDeleteImage();
                    startTimer();
                    this.setLastUpdatedTimeCounter();

                    //professional tutoring change
                    this.activeTutorOnSession();
                    //end change
                } else {
                    this.showCantEditDialog();
                }
                break;
            case R.id.btnAcceptQuestion:
                this.setVisibilityOfAcceptAnswerButton(false);
                this.startStudentPaidTutorTimer();
                break;
            case R.id.btnPhraseList:
                if(!this.isEditingEnable) {
                    this.showCantEditDialog();
                    return ;
                }
                this.getPhraseListAndSetAdapter();
                break;
            /*case R.id.btnAudioCall:
                this.startEndAudioCall();
                break;*/
        }
    }

    /**
     * Set the checked background image
     */
    private void setCheckedBackground() {
        if (workareapublic == 0) {//0 for private
            btnCheck.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
        } else {
            btnCheck.setBackgroundResource(R.drawable.mf_check_box_ipad);
        }
    }

    /**
     * Drawing mode
     */
    private void drawMode() {
        eraserMode = false;
        this.setEraserSelected(false);
        this.setPencilSelected(true);
        if (isTutor) {
            mySideDrawingView.setDrawColor(Color.BLUE);
        } else {
            mySideDrawingView.setDrawColor(Color.BLACK);
        }
        mySideDrawingView.setDrawMode(true);
        mySideDrawingView.setEraseMode(false);
    }

    /**
     * Erase mode
     */
    private void eraseMode() {
        eraserMode = true;
        this.setEraserSelected(true);
        this.setPencilSelected(false);
        mySideDrawingView.setDrawMode(true);
        mySideDrawingView.setEraseMode(true);
    }

    /**
     * Return the paint object
     *
     * @param getpaint
     * @return
     */
    private Paint getPaint(int getpaint) {
        Paint mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        if (getpaint == MY_SIDE_DRAWING) {
            if (isTutor) {
                mPaint.setColor(Color.BLUE);
            } else {
                mPaint.setColor(Color.BLACK);
            }
        } else if (getpaint == OTHER_SIDE_DRAWING) {
            if (isTutor) {
                mPaint.setColor(Color.BLACK);
            } else {
                mPaint.setColor(Color.BLUE);
            }

        }
        mPaint.setStrokeWidth(MathFriendzyHelper.DRAW_LINE_THICKNESS);
        mPaint.setStyle(Paint.Style.STROKE);
        return mPaint;
    }

    /**
     * My side drawing view
     */
    private void drawingMySideView() {
        try {
            mySidePaint = this.getPaint(MY_SIDE_DRAWING);
            DisplayMetrics metrics = getActivity().getBaseContext()
                    .getResources().getDisplayMetrics();
            int w = metrics.widthPixels;
            int h = metrics.heightPixels;
            h = h * 3;
            mySideBitmap = this.getBitMap(w, h);
            mySideDrawingView = new MyView(getActivity(), h, w, mySideBitmap,
                    mySidePaint, mySideDrawingCallback);
            this.setViewHeightAndWidth(w, h, mySideDrawingView);
            mySideDrawingView.setBackgroundColor(Color.TRANSPARENT);
            mySideDrawingView.setDrawingCacheEnabled(true);
            mySideDrawingLayout.addView(mySideDrawingView);
            //for image option
            this.SetHeightAndWidthOfOtherImages(w, h);
            //end for image option

            if (!isWorkAreaForstudent) {
                mySideDrawingView.setScrollMode(true);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Set height and width for the image from work area
     *
     * @param w
     * @param h
     */
    private void SetHeightAndWidthOfOtherImages(int w, int h) {
        rlParam = new RelativeLayout.LayoutParams(w, h);
        imgWorkAreaDrawing.setLayoutParams(rlParam);
        this.setStudentImageLayoutParam();
        this.setTutorImageLayoutParam();
    }

    private void setStudentImageLayoutParam() {
        if (rlParam != null) {
            studentImageLayout.setLayoutParams(rlParam);
            imgStudentImage.setLayoutParams(rlParam);
        }
    }

    private void setTutorImageLayoutParam() {
        if (rlParam != null) {
            tutorImageLayout.setLayoutParams(rlParam);
            imgTutorImage.setLayoutParams(rlParam);
        }
    }

    /**
     * Other side drawing view
     */
    private void drawingOtherSideView() {
        try {
            otherSidePaint = this.getPaint(OTHER_SIDE_DRAWING);
            DisplayMetrics metrics = getActivity().getBaseContext()
                    .getResources().getDisplayMetrics();
            int w = metrics.widthPixels;
            int h = metrics.heightPixels;
            h = h * 3;
            otherSideBitmap = this.getBitMap(w, h);
            otherSideDrawingView = new MyView(getActivity(), h, w, otherSideBitmap,
                    otherSidePaint, otherSideSideDrawingCallback);
            this.setViewHeightAndWidth(w, h, otherSideDrawingView);
            otherSideDrawingView.setBackgroundColor(Color.TRANSPARENT);
            otherSideDrawingView.setDrawingCacheEnabled(true);
            othersideDrawingLayout.addView(otherSideDrawingView);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Drawing callbacks
     */
    DrawingCallback mySideDrawingCallback = new DrawingCallback() {

        @Override
        public void onUp(final float x, final float y) {
            /*if(!mySideDrawingView.getEraseMode())
                otherSideDrawingView.touch_up_for_other();*/
            if (mySideDrawingView.getEraseMode())
                sendDrawingMessage(x, y, POINT_TYPE_STOP, true);
            else
                sendDrawingMessage(x, y, POINT_TYPE_STOP, false);
            onActionUp();
            setLastUpdatedTimeCounter();
            //professional tutoring change
            setLastUpdatedTimeCounterForPaidSession();
            //end change
        }

        /**
         * x = mX
         * y = mY
         * ex = device points x
         * ey = device points y
         */
        @Override
        public void onStart(final float x, final float y,
                            final float ex, final float ey) {
            /*if(!mySideDrawingView.getEraseMode())
                otherSideDrawingView.touch_start_for_other(ex , ey + 50);*/
            isStartDrawing = true;
            if (mySideDrawingView.getEraseMode())
                sendDrawingMessage(x, y, POINT_TYPE_START, true);
            else
                sendDrawingMessage(x, y, POINT_TYPE_START, false);
            onActionDown(x, y);
            startTimer();

            //professional tutoring change
            isTutorInIdealMode = false;
            startStudentPaidSessionTimer();
            //end change
        }

        /**
         * x = mX
         * y = mY
         * ex = device points x
         * ey = device points y
         */
        @Override
        public void onMove(final float x, final float y, final float ex, final float ey) {

            if (mySideDrawingView.getEraseMode())
                sendDrawingMessage(x, y, POINT_TYPE_MOVE, true);
            else
                sendDrawingMessage(x, y, POINT_TYPE_MOVE, false);
            onActionMove(x, y);
        }


        @Override
        public void disableDrawing() {
            showCantEditDialog();
        }
    };

    /**
     * Drawing callbacks
     */
    DrawingCallback otherSideSideDrawingCallback = new DrawingCallback() {

        @Override
        public void onUp(final float x, final float y) {
        }

        /**
         * x = mX
         * y = mY
         * ex = device points x
         * ey = device points y
         */
        @Override
        public void onStart(final float x, final float y,
                            final float ex, final float ey) {
        }

        /**
         * x = mX
         * y = mY
         * ex = device points x
         * ey = device points y
         */
        @Override
        public void onMove(final float x, final float y, final float ex, final float ey) {

        }

        @Override
        public void disableDrawing() {

        }
    };

    /**
     * set the main view height and width
     *
     * @param w
     * @param h
     */
    private void setViewHeightAndWidth(int w, int h, View view) {
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(w, h);
        view.setLayoutParams(param);
    }


    /**
     * Return the bitmap view
     *
     * @param w
     * @param h
     * @return
     */
    private Bitmap getBitMap(int w, int h) {
        return MathFriendzyHelper.createBitmap(w , h);
    }

    /**
     * Set the stating point of the eraser circle on action down , meanse on touch start
     *
     * @param X
     * @param Y
     */
    private void onActionDown(float X, float Y) {
        if (eraserMode) {
            imgEraser.setVisibility(ImageView.VISIBLE);
            imgEraser.setX((int) X - imgEraser.getWidth() / 2);
            imgEraser.setY((int) Y + studentTeacherLayout.getHeight()
                    + imgEraser.getHeight() - (int) scrollView.getScrollY()
                    - tutorSessionTopBar.getHeight());

        }
    }

    /**
     * Set the eraser moving position when action is move
     *
     * @param X
     * @param Y
     */
    private void onActionMove(float X, float Y) {
        if (eraserMode) {
            imgEraser.setX((int) X - imgEraser.getWidth() / 2);
            imgEraser.setY((int) Y + studentTeacherLayout.getHeight()
                    + imgEraser.getHeight() / 2 - (int) scrollView.getScrollY()
                    - tutorSessionTopBar.getHeight());
        }
    }

    /**
     * Invisible the eraser circle when action up
     */
    private void onActionUp() {
        if (eraserMode) {
            imgEraser.setVisibility(ImageView.GONE);
        }
    }

    @Override
    public void onScrolling(int state) {
        switch (state) {
            case ON_SCROLLING:
                btnScrollDown.setBackgroundResource(R.drawable.down);
                btnScrollUp.setBackgroundResource(R.drawable.up);
                break;
            case SCROLL_DOWN_COMPLETE:
                btnScrollDown.setBackgroundResource(R.drawable.unselected_down);
                btnScrollUp.setBackgroundResource(R.drawable.up);
                break;
            case SCROLL_UP_COMPLETE:
                btnScrollDown.setBackgroundResource(R.drawable.down);
                btnScrollUp.setBackgroundResource(R.drawable.unselected_up);
                break;
        }
    }

    private String getRoundOffValue(float floatValue) {
        return String.format("%.1f", floatValue);
    }

    /**
     * Send drawing message
     *
     * @param x
     * @param y
     * @param pointsType
     * @param isErase
     */
    private void sendDrawingMessage(float x, float y, int pointsType, boolean isErase) {
        try {
            String roundOfX = this.getRoundOffValue(x);
            String roundOfY = this.getRoundOffValue(y);

            String message = POINTS_PREFF;
            if (pointsType == POINT_TYPE_START) {
                this.removeCallback();
                message = message + BEGIN + "{" + roundOfX + "," + roundOfY + "}";
            } else if (pointsType == POINT_TYPE_MOVE) {
                message = message + "{" + roundOfX + "," + roundOfY + "}";
            } else {
                message = message + END + "{" + roundOfX + "," + roundOfY + "}";

                if (isTutor) {
                    if (!selectedTutor.isFromDisconnectedList()) {
                        this.updateDrawPointsForPlayer();
                    }
                } else {
                    if (chatRequestId > 0) {
                        this.updateDrawPointsForPlayer();
                    }
                }
                this.setNotificationWhenTutorAreaUpdated();
            }

            if (isErase) {
                message = message + ERASE;
            }

            if(message.contains(BEGIN)) {
                String deviceSizeMessage = POINTS_PREFF + SIZE +
                        currentDevice.widthPixels + "," + currentDevice.heightPixels;
                this.sendDrawingMessage(deviceSizeMessage);
                drawingPointsList.add(deviceSizeMessage);
            }
            this.sendDrawingMessage(message);

            drawingPointsList.add(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Send drawing message
     * @param message
     */
    private void sendDrawingMessage(String message){
        try {
            if (drawingTask != null) {
                drawingTask.sendMessage(message);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onConnected() {
       /* "user->iam:%@---%lld&receivers:%@&requestId:%d&xScale:%f&yScale:%f"";*/
        //drawingTask.sendMessage("iam:df---121");
        if (drawingTask != null) {
            isSocketConnected = true;
            drawingTask.getAllOnlineUserInRoom(MathFriendzyHelper.SERVER_PREFIX ,
                    selectedPlayerData.getPlayerid());
            /*drawingTask.getRoomHistory(MathFriendzyHelper.SERVER_PREFIX ,
                    selectedPlayerData.getPlayerid());*/
            getDrawingPointsHistory();
        }
    }

    @Override
    public void onConnectionFail() {
        CommonUtils.printLog(TAG, "connection fail");
    }


    @Override
    public void onMessageSent(boolean isSuccess) {
        CommonUtils.printLog(TAG , "message sent " + isSuccess);
    }

    /**
     * Draw point on view
     *
     * @param mView
     * @param message
     * @param dimention
     */
    private void drawPointOnView(final MyView mView, final String message,
                                 final boolean isMySideView, final DrawDeviceDimention dimention) {
        if (message.contains(ERASE) && !mView.getEraseMode()) {
            mView.setEraseMode(true);
        } else if (!message.contains(ERASE) && mView.getEraseMode()) {
            if (isMySideView) {
                if (isTutor) {
                    mView.setDrawColor(Color.BLUE);
                } else {
                    mView.setDrawColor(Color.BLACK);
                }
            } else {
                if (isTutor) {
                    mView.setDrawColor(Color.BLACK);
                } else {
                    mView.setDrawColor(Color.BLUE);
                }
            }
            mView.setEraseMode(false);
        }

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                PointF point = MathFriendzyHelper.getDeviceScalePoints(message, dimention);
                if (point != null) {
                    if (message.contains(BEGIN)) {
                        mView.touch_start_for_other(point.x, point.y);
                    } else if (message.contains(END)) {
                        mView.touch_up_for_other();
                    } else {
                        mView.touch_move_for_other(point.x, point.y);
                    }
                }
            }
        });
    }

    @Override
    public void onMessageRecieved(final String message) {
        try {
            CommonUtils.printLog(TAG , "message recieved " + message);
            if (MathFriendzyHelper.isEmpty(message))
                return;

            if (message.startsWith(MathFriendzyHelper.SERVER_PREFIX)) {
                this.processServerMessage(message);
                return;
            }

            if (message.startsWith(MathFriendzyHelper.MESSAGE_PREFIX)) {
                ChatMessageModel messageObj = this.parseReceivedMessage(message);
                if(messageObj.getMessage().startsWith(MathFriendzyHelper.AUDIO_PREFIX)){
                    this.processAudioMessage(messageObj.getMessage());
                    return ;
                }
                this.showChatMessage(messageObj);
                return;
            }

            if (message.contains(ERASE) && !otherSideDrawingView.getEraseMode()) {
                otherSideDrawingView.setEraseMode(true);
            } else if (!message.contains(ERASE) && otherSideDrawingView.getEraseMode()) {
                if (isTutor) {
                    otherSideDrawingView.setDrawColor(Color.BLACK);
                } else {
                    otherSideDrawingView.setDrawColor(Color.BLUE);
                }
                otherSideDrawingView.setEraseMode(false);
            }

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //do the code if the multiple message like p:size678,1024p:begin{20,10}p:{20,30}p:{23,24}er
                    //do the code like this replece the p from the string and split by : and send 1 by 1 message
                    //to the MathFriendzyHelper.getPoints(message , getDrawDimention(message));
                    PointF point = MathFriendzyHelper.getPoints(message, getDrawDimention(message));
                    if (point != null) {
                        if (message.contains(BEGIN)) {
                            otherSideDrawingView.touch_start_for_other(point.x, point.y);
                        } else if (message.contains(END)) {
                            otherSideDrawingView.touch_up_for_other();
                        } else {
                            otherSideDrawingView.touch_move_for_other(point.x, point.y);
                        }
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Return the drawing dimention
     * @param message
     * @return
     */
    private DrawDeviceDimention getDrawDimention(String message){
        try {
            //p:size678,1024
            if (message.contains(SIZE)) {
                String otherDeviceSize = message.replaceAll(POINTS_PREFF , "").replaceAll(SIZE , "");
                dimention = MathFriendzyHelper.getDrawDeviceDimention(currentDevice ,
                        otherDeviceSize);
            }
            return dimention;
        }catch(Exception e){
            e.printStackTrace();
            return dimention;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        callback = (ChatRequestIdCallback) activity;
        try {
            homeworkAreaObj = (HomeWorkWorkArea) activity;
        } catch (Exception e) {
            homeworkAreaObj = null;
            e.printStackTrace();
        }
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        CommonUtils.printLog(TAG, "onDetach");
        super.onDetach();
    }

    @Override
    public void onStart() {
        CommonUtils.printLog(TAG, "onStart");
        super.onStart();
    }


    @Override
    public void onStop() {
        CommonUtils.printLog(TAG, "onStop");
        super.onStop();
    }

    @Override
    public void onDestroy() {
        CommonUtils.printLog(TAG, "onDestroy");
        this.releaseFromAudioChat();
        this.releaseFromChat();
        this.updateTutorAreaStatus(MathFriendzyHelper.NO);
        this.clearAllBitmap();
        super.onDestroy();
    }

    /**
     * Release from chat
     */
    private void releaseFromChat() {
        try {
            stopTimer();
            stopStudentPaidTutorTimer();
            this.releaseActiveInactiveHandler();
            if (drawingTask != null) {
                drawingTask.disconnect();
                this.removeCallback();
                //Update comment
                //this.updateDrawingPonits();
            }
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtils.printLog(TAG, "Release exception " + e.toString());
        }
    }


    /**
     * Get new chat request id for the disconnected student
     */
    private void getChatRequestIdForParam() {
        GetChatRequestIdParam param = new GetChatRequestIdParam();
        param.setAction("getChatRequestIdForProblem");
        param.setHwId(arguments.getHwId());
        param.setCustomeHwId(arguments.getCustomeHwId());
        param.setUserId(selectedPlayerData.getParentUserId());
        param.setPlayerId(selectedPlayerData.getPlayerid());
        param.setQuestion(arguments.getQuesion());

        new MyAsyckTask(ServerOperation.createPostTOGetChatRequestIdForProblem(param)
                , null, ServerOperationUtil.GET_CHAT_REQUEST_ID_FOR_PROBLEM_REQUEST,
                getActivity(), this, ServerOperationUtil.SIMPLE_DIALOG, true,
                getActivity().getString(R.string.please_wait_dialog_msg))
                .execute();
    }


    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
        if (requestCode == ServerOperationUtil.GET_CHAT_REQUEST_SERVER_REQUEST) {
            //dismissDialog();
            isGetChatResponseDone = true;
            hidePopUp();

            GetChatRequestResponse response = (GetChatRequestResponse) httpResponseBase;
            if (response.getResult().equals("success")) {
                //for drawing chat
                this.getChatResponse = response;

                String tutotTabText = "";
                if (response.getIsAnonymous() == YES) {
                    tutotTabText = tutorText + ": " + anonymousText;
                } else {
                    tutotTabText = tutorText + ": " + response.getTutorName();
                }
                isAnnonymous = response.getIsAnonymous();

                if (response.getDisconnected() == 1) {//for disconnected tutor
                    //this.getChatRequestIdForParam();
                    isDisconnected = true;
                    callback.onTutorTabTextSet(tutorText + ":" + lblDisconnect);
                    this.initilizaEnableVariable(false);
                    if (response.getRating() > 0) {
                        callback.showTutorTabContent(false, false, false, true);
                    } else {
                        callback.showTutorTabContent(false, true, false);
                    }
                } //else {

                if (MathFriendzyHelper.parseInt
                        (response.getTimeSpent()) > 0) {
                    isSessionConnected = true;
                    isTutorDataEnableForStudent = true;
                } else {
                    isTutorDataEnableForStudent = false;
                    isSessionConnected = false;
                }

                if (response.getIsConnected() == 1 && MathFriendzyHelper.parseInt
                        (response.getTimeSpent()) > 0) {
                    isChatAlreadyConnected = true;
                    this.getChatDialog(response.getChatDialogId());
                } else {//Anonymous tutor not connected
                    isChatAlreadyConnected = false;
                    anonymousStudentTutorMessage = response.getMessage();
                    this.showMessageForAnonymousRequest(this.getQBChatMessage(response.getMessage(),
                            "Student"));
                    isJoinChatSuccess = true;
                    hidePopUp();
                }

                if (response.getIsConnected() == 1) {
                    isChatConnected = true;
                    if (!this.isDisconnected) {
                        if (isTutorDataEnableForStudent) {
                            callback.onTutorTabTextSet(tutotTabText);
                            callback.showTutorTabContent(true, true, false);
                            this.initilizaEnableVariable(true);
                        } else {
                            callback.onTutorTabTextSet(waitingForATutorText);
                            callback.showTutorTabContent(false, false, true);
                            this.initilizaEnableVariable(false);
                        }
                    }
                } else {
                    isChatConnected = false;
                    if (!this.isDisconnected) {
                        callback.onTutorTabTextSet(waitingForATutorText);
                        callback.showTutorTabContent(false, false, true);
                        this.initilizaEnableVariable(false);
                    }
                }

                if(MathFriendzyHelper.parseInt
                        (response.getTimeSpent()) > 0) {
                    this.createDrawingTaskObject();
                }else{
                    isDrawingFromChatHistoryRecieve = true;
                    drawPointFromServer();
                }

                this.swapStudentAndTutorImageLayout(getChatResponse.getLastUpdateByTutor());
                this.setStudentImage(getChatResponse.getStudentImage());
                if(MathFriendzyHelper.parseInt
                        (response.getTimeSpent()) > 0) {
                    this.setTutorImage(getChatResponse.getTutorImage());
                }else{
                    isTutorImageDone = true;
                    hidePopUp();
                }
                this.setWorkAreaImage(getChatResponse.getWorkAreaImage());
            } else {
                CommonUtils.showInternetDialog(getActivity());
            }
        } else if (requestCode == ServerOperationUtil.SEND_MESSAGE_TO_ANONYMOUS_TUTOR_REQUEST) {
            SendMessageToTutorResponse response = (SendMessageToTutorResponse) httpResponseBase;
            if (response.getResult().equalsIgnoreCase("success")) {
                chatRequestId = response.getRequestId();
                if (callback != null)
                    callback.onChatRequesId(chatRequestId);
                //this.createDrawingTaskObject();
                this.updateDrawingPonits();
                if(adapter != null) {
                    this.saveMessageToServer(adapter.getAnonymousRequestMessage());
                }
                this.sendNotificationAfterRequestCreate();
                this.updateTutorAreaStatus(MathFriendzyHelper.YES);
            }
        } else if (requestCode == ServerOperationUtil.GET_CHAT_REQUEST_ID_FOR_PROBLEM_REQUEST) {
            GetChatRequestIdForProblemResponse response = (GetChatRequestIdForProblemResponse) httpResponseBase;
            if (response.getResult().equals("success")) {
                chatRequestId = response.getChatId();
                if (callback != null)
                    callback.onChatRequesId(chatRequestId);

                if (chatRequestId > 0) {
                    this.loginSuccess();
                } else {
                    Intent intent = new Intent(getActivity(), ActFindTutor.class);
                    getActivity().startActivityForResult(intent,
                            MathFriendzyHelper.FIND_TUTOR_REQUEST_FROM_FRAGMENT);
                }
            }
        }
    }

    /**
     * Send the notification after the request created
     */
    private void sendNotificationAfterRequestCreate(){
        if(selectedImageBitmap == null && studentWorkAreaImageBitmap == null){
            this.sendNotificationAreaEditTutorOrStudent();
        }else {
            if(selectedImageBitmap != null && studentWorkAreaImageBitmap != null){
                if (selectedImageBitmap != null) {
                    uploadBitmapImage(selectedImageBitmap, STUDENT_IMAGE,
                            getTutorAreaImageName(false), new UploadImageRequest() {
                                @Override
                                public void onComplete(String response) {
                                    initializeSelectedImageBitmap(null);
                                    if (studentWorkAreaImageBitmap != null) {
                                        uploadBitmapImage(studentWorkAreaImageBitmap,
                                                WORK_AREA_IMAGE,
                                                getTutorAreaImageName(true),
                                                new UploadImageRequest() {
                                                    @Override
                                                    public void onComplete(String response){
                                                        initializeSelectedWorkImageBitmap(null);
                                                        sendNotificationAreaEditTutorOrStudent();
                                                    }
                                                });
                                    }
                                }
                            });
                }
            }else{
                if (selectedImageBitmap != null) {
                    uploadBitmapImage(selectedImageBitmap, STUDENT_IMAGE,
                            getTutorAreaImageName(false), new UploadImageRequest() {
                                @Override
                                public void onComplete(String response) {
                                    initializeSelectedImageBitmap(null);
                                    sendNotificationAreaEditTutorOrStudent();
                                }
                            });
                }

                if (studentWorkAreaImageBitmap != null) {
                    uploadBitmapImage(studentWorkAreaImageBitmap,
                            WORK_AREA_IMAGE,
                            getTutorAreaImageName(true),
                            new UploadImageRequest() {
                                @Override
                                public void onComplete(String response){
                                    initializeSelectedWorkImageBitmap(null);
                                    sendNotificationAreaEditTutorOrStudent();
                                }
                            });
                }
            }
        }
    }

    /**
     * Format message for anonymous tutor
     *
     * @param anonymousStudentTutorMessage
     * @return
     */
    private String getAnonymousStudentTutorMessage(String anonymousStudentTutorMessage) {
        return anonymousStudentTutorMessage.length() > 0
                ? (anonymousStudentTutorMessage + "\n"
                + edtTeacherStudentText.getText().toString()) :
                (edtTeacherStudentText.getText().toString());
    }

    private String getInitialMessageToSendInCreateAnonymousRequest(){
        try {
            if (adapter != null) {
                return adapter.getAnonymousRequestMessage();
            }else{
                return firstMessageToSend;
            }
        }catch(Exception e){
            return firstMessageToSend;
        }
    }

    /**
     * Send message to anonymous tutor
     *
     * @param anonymous - 1 for anonymous and 0 for other
     * @return
     */
    private String getSendMessageToTutorJson(int anonymous) {
        JSONObject json = new JSONObject();
        try {
            json.put("requestId", chatRequestId);
            json.put("userId", selectedPlayerData.getParentUserId());
            json.put("playerId", selectedPlayerData.getPlayerid());
            json.put("date", MathFriendzyHelper.getCurrentDateInGiveGformateDateWithDefaultTimeZone
                    ("yy-MM-dd hh:mm:ss"));
            if (chatRequestId > 0) {
                if (anonymous == YES) {
                    anonymousStudentTutorMessage =
                            this.getAnonymousStudentTutorMessage(anonymousStudentTutorMessage);
                    json.put("message", anonymousStudentTutorMessage);
                } else
                    json.put("message", edtTeacherStudentText.getText().toString());
            } else {
                //changes for submit question answer button
                //json.put("message", firstMessageToSend);
                json.put("message", getInitialMessageToSendInCreateAnonymousRequest());
                //end
            }

            json.put("isConnected", 0);

            if (anonymous == YES) {//for anonymous tutor
                json.put("tutorUid", 0);
                json.put("tutorPid", 0);
            } else {//0 for other
                json.put("tutorUid", findTutorDetail.getParentUserId());
                json.put("tutorPid", findTutorDetail.getPlayerId());
            }
            json.put("isAnonymous", anonymous);

            if (newDialog != null)
                json.put("dialogId", newDialog.getDialogId());
            else
                json.put("dialogId", "");

            json.put("hwId", arguments.getHwId());
            json.put("cusHwId", arguments.getCustomeHwId());
            json.put("question", arguments.getQuesion());

            //change for professional tutoring
            if(isForProfessionalTutoring){
                json.put("paidSessionTitle",
                        arguments.getTutorSessionTitle());
                json.put("paidSession", "1");
                json.put("attachedToQuestion", "0");
            }

            try{
                if(homeworkAreaObj != null
                        && homeworkAreaObj.getHomeworkData() != null){
                    json.put("subjectId", homeworkAreaObj.getHomeworkData().getSubId());
                }else{
                    json.put("subjectId" , 0);
                }
            }catch (Exception e){
                e.printStackTrace();
                json.put("subjectId" , 0);
            }
            //end change for professional tutoring
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    /**
     * Return the QB Chat message
     *
     * @param messageText
     * @param senderName
     * @return
     */
    private ChatMessageModel getQBChatMessage(String messageText, String senderName) {
        ChatMessageModel chatMessage = new ChatMessageModel();
        chatMessage.setMessage(messageText);
        if (isAnnonymous == NO) {
            chatMessage.setName(senderName);
        } else {
            if (isTutor) {
                chatMessage.setName("Tutor");
            } else {
                chatMessage.setName("Student");
            }
        }
        if(isTutor){
            chatMessage.setIsTutor(1);
        }else{
            chatMessage.setIsTutor(0);
        }
        chatMessage.setSenderId(selectedPlayerData.getPlayerid());
        return chatMessage;
    }

    /**
     * Send the text when user click on send
     */
    private void sendMessage(String messageText) {
        //String messageText = edtTeacherStudentText.getText().toString();
        if (TextUtils.isEmpty(messageText)) {
            return;
        }

        if(MathFriendzyHelper.isProfinityWordExistInString(messageText)){
            MathFriendzyHelper.showWarningDialog(getActivity() , alertMsgAbusingWord);
            edtTeacherStudentText.setText("");
            return ;
        }

        if(chatRequestId == 0){
            //Audio Call Changes
            if(messageText.startsWith(MathFriendzyHelper.AUDIO_PREFIX)){
                return ;
            }

            if(findTutorDetail != null)
                this.showChatMessage(this.getQBChatMessage(messageText,
                        selectedPlayerData.getFirstname()
                                + " " + selectedPlayerData.getLastname()));
            else
                this.showChatMessage(this.getQBChatMessage(messageText, "Student"));
            edtTeacherStudentText.setText("");
            return;
        }

        // Send chat message
        String currentPlayerName = selectedPlayerData.getFirstname() + " "
                + selectedPlayerData.getLastname();
        String messageToSend = this.getMessageToSend(messageText ,
                this.getSenderNameForMessage(currentPlayerName) , isTutor);

        this.sendDrawingMessage(messageToSend);

        //Audio Call Changes
        if(messageText.startsWith(MathFriendzyHelper.AUDIO_PREFIX)){
            return ;
        }

        this.showChatMessage(this.getQBChatMessage(messageText, currentPlayerName));
        this.saveMessageToServer(messageText);
        this.setNotificationWhenTutorAreaUpdated();
        edtTeacherStudentText.setText("");
    }

    private String getMessageToSend(String message , String name , boolean isTutor){
        //msg:{“txt”:msgToSend, “params”:{“name”:”NAME”, “tutor”:”1”}}
        try {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("txt" , message);
            jsonObj.put("params" , this.getParamJson(name , isTutor));
            return MathFriendzyHelper.MESSAGE_PREFIX + jsonObj;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private JSONObject getParamJson(String name , boolean isTutor){
        try {
            JSONObject subJson = new JSONObject();
            subJson.put("name", this.getSenderNameForMessage(name));
            if (isTutor) {
                subJson.put("tutor", 1);
            } /*else {
                subJson.put("tutor", 0);
            }*/

            return subJson;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private String getSenderNameForMessage(String senderName){
        if (isAnnonymous == NO) {
            return senderName;
        } else {
            if (isTutor) {
                return "Tutor";
            } else {
                return  "Student";
            }
        }
    }

    private ChatMessageModel parseReceivedMessage(String message){
        try {
            message = message.replaceAll(MathFriendzyHelper.MESSAGE_PREFIX , "");
            ChatMessageModel messageObj = new ChatMessageModel();
            JSONObject jsonObject = new JSONObject(message);
            messageObj.setMessage(jsonObject.getString("txt"));
            JSONObject jsonParamObj = jsonObject.getJSONObject("params");
            messageObj.setName(jsonParamObj.getString("name"));
            if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonParamObj , "tutor")) {
                messageObj.setIsTutor(jsonParamObj.getInt("tutor"));
            }else{
                messageObj.setIsTutor(0);
            }
            return messageObj;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    private void showChatMessage(ChatMessageModel chatMessage){
        if (CommonUtils.LOG_ON)
            Log.e(TAG, "show message");
        if(chatMessage == null)
            return ;

        if (isTutorDataEnableForStudent) {
            if (adapter == null) {
                ArrayList<ChatMessageModel> messages = new ArrayList<ChatMessageModel>();
                messages.add(chatMessage);
                adapter = new QBChatMessageAdapter(getActivity(), messages, this);
                lstteacherStudentMsgList.setAdapter(adapter);
            } else {
                adapter.addMessage(chatMessage);
                adapter.notifyDataSetChanged();
            }
            lstteacherStudentMsgList.setSelection
                    (lstteacherStudentMsgList.getAdapter().getCount() - 1);
        }
    }

    /**
     * This method show the message for Anonymous request for first time
     */
    private void showMessageForAnonymousRequest(ChatMessageModel chatMessage) {
        if (CommonUtils.LOG_ON)
            Log.e(TAG, "showMessageForAnonymousRequest");
        if (adapter == null) {
            ArrayList<ChatMessageModel> messages = new ArrayList<ChatMessageModel>();
            messages.add(chatMessage);
            adapter = new QBChatMessageAdapter(getActivity(), messages, this);
            lstteacherStudentMsgList.setAdapter(adapter);
        } else {
            adapter.addMessage(chatMessage);
            adapter.notifyDataSetChanged();
        }
        lstteacherStudentMsgList.setSelection
                (lstteacherStudentMsgList.getAdapter().getCount() - 1);
    }

    private SaveMessageForSingleChatParam getSaveSingleChatParam(String messageText , String params) {
        SaveMessageForSingleChatParam param = new SaveMessageForSingleChatParam();
        param.setAction("saveMessagesForSingleChat");
        param.setMessage(messageText);
        param.setParams(params);
        param.setUserId(selectedPlayerData.getParentUserId());
        param.setPlayerId(selectedPlayerData.getPlayerid());
        param.setRequestId(chatRequestId);
        return param;
    }

    @Override
    public void showMessage(QBChatMessage chatMessage) {
    }

    @Override
    public void joinSuccessFull(QBEntityCallback callback) {

    }

    @Override
    public void onError(List list) {

    }

    @Override
    public void stillJoiningGroupChat() {

    }

    @Override
    public void joinUnsuccessFull() {
        MathFriendzyHelper.dismissDialog();
    }

    @Override
    public void showProcessImage(QBChatMessage message) {

    }

    @Override
    public void onlineUsers(ArrayList<Integer> dialogOccupents,
                            Collection<Integer> onlineGroupUsers) {
    }


    /**
     * Check for notify school tutor
     *
     * @return
     */
    public boolean isEnterSomeThingToNotify() {
        if (chatRequestId == 0) {
            if ((adapter != null && adapter.getCount() > 0)
                    || isStartDrawing)
                return true;
            return false;
        } else {
            return false;
        }
    }

    /**
     * Return the drawing points list
     *
     * @return
     */
    private String getDrawingPoints() {
        StringBuilder stringBuilder = new StringBuilder("");
        for (int i = 0; i < drawingPointsList.size(); i++)
            stringBuilder.append(stringBuilder.length() == 0 ? drawingPointsList.get(i)
                    .replaceAll(POINTS_PREFF, "") :
                    ";" + drawingPointsList.get(i).replaceAll(POINTS_PREFF, ""));
        return stringBuilder.toString() + ";";
    }

    /**
     * Get update drawing points param
     *
     * @return
     */
    private UpdateDrawPointsForPlayerParam getUpdateDrawingPointsForPlayerParam() {
        UpdateDrawPointsForPlayerParam param = new UpdateDrawPointsForPlayerParam();
        param.setAction("updateDrawnPointsForPlayer");
        param.setRequestId(chatRequestId);
        param.setPlayerId(selectedPlayerData.getPlayerid());
        param.setDrawPoints(this.getDrawingPoints());
        param.setDeviceSize(deviceDimention.widthPixels + "," + deviceDimention.heightPixels);
        return param;
    }

    /**
     * Call when click on submit question answer button
     */
    private void clickOnSubmitQuestionAnswerButton() {
        /*if(this.isPaidSession() && !isTutor){
            MathFriendzyHelper.showWarningDialog(this.getActivity()
                    , lblOutTutorAvailableBtw , new HttpServerRequest() {
                @Override
                public void onRequestComplete() {

                }
            });
        }*/
        this.setSubmitQuestionAnswerButtnVisibility(false);
        if (!isTutor && chatRequestId == 0) {
            if (callback != null) {
                callback.onTutorTabTextSet(waitingForATutorText);
                callback.showTutorTabContent(false, false, true);
            }
            this.initilizaEnableVariable(false);
            isTutorDataEnableForStudent = false;
            SendMessageToTutorParam param = new SendMessageToTutorParam();
            param.setAction("sendMessageToTutor");

            if (findTutorDetail != null)
                param.setData(this.getSendMessageToTutorJson(0));
            else
                param.setData(this.getSendMessageToTutorJson(1));

            this.showPopUp();
            new MyAsyckTask(ServerOperation.createPostTOSendMessageToAnonymousTutor(param)
                    , null, ServerOperationUtil.SEND_MESSAGE_TO_ANONYMOUS_TUTOR_REQUEST,
                    getActivity(), this, ServerOperationUtil.SIMPLE_DIALOG, false,
                    getActivity().getString(R.string.please_wait_dialog_msg))
                    .execute();

            //check for duplicate area content save to server or not
            isDuplicateAreaAdded = true;
            hidePopForRequestCreation();
            this.addContentToDuplicateArea(new OnRequestComplete() {
                @Override
                public void onComplete() {
                    /*isDuplicateAreaAdded = true;
                    hidePopForRequestCreation();*/
                }
            });
        } else {
            if (selectedTutor.isFromDisconnectedList()) {
                selectedTutor.setFromDisconnectedList(false);
                this.initilizaEnableVariable(false);
                callback.showTutorTabContent(true, false, true);
                //move the code sent notification in addTimeSpent method because sent the notification
                //after time spent add.
                addTimeSpentInTutorSession();
                //Update comment
                //updateDrawingPonits();
            }
        }
    }

    private void initilizaEnableVariable(boolean enableDisable) {
        this.isEditingEnable = enableDisable;
        if (mySideDrawingView != null)
            mySideDrawingView.setDrawMode(enableDisable);
        edtTeacherStudentText.setEnabled(enableDisable);
    }

    /**
     * Update drawing points on server handler
     */
    private void updateDrawPointsForPlayer() {
        if (chatRequestId == 0) {

        } else {
           /* updatePointsHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    updateDrawingPonits();
                }
            }, TIME_INTERVAL_AFTER_POINTS_SAVE);*/
        }
    }

    /**
     * Update drawing point
     */
    private void updateDrawingPonits() {
        if (drawingPointsList.size() > 0) {
            if(CommonUtils.isInternetConnectionAvailable(getActivity())) {
                MathFriendzyHelper.updateDrawPonitsForPlayer(getActivity(),
                        getUpdateDrawingPointsForPlayerParam());
                drawingPointsList.clear();
            }
        }
    }

    private void removeCallback() {
        updatePointsHandler.removeCallbacksAndMessages(null);
    }

    /**
     * Get drawing point from server
     */
    private void getDrawingPointsFromServerForChat() {
        if (chatRequestId > 0) {
            GetDrawingPointForChatParam param = new GetDrawingPointForChatParam();
            param.setAction("getDrawingPointsForChat");
            param.setRequestId(chatRequestId);
            MathFriendzyHelper.getDrawingPointsFromServer(getActivity(), param,
                    new GetDrawingPointForChatOnSuccess() {
                        @Override
                        public void onSuccess(GetDrawingPointForChatResponse response) {
                            if (response.getResult().equals("success")) {
                                TuturSessionFragment.this.drawingPointForChatResponseresponse = response;
                                isDrawingFromServerRecieve = true;
                                drawPointFromServer();
                            }
                        }
                    });
        }
    }

    private GetDrawingPointForChatResponse getMyDrawingPoint
            (ArrayList<GetDrawingPointForChatResponse> pointsList){
        for (GetDrawingPointForChatResponse mPoint : pointsList) {
            if (selectedPlayerData.getPlayerid()
                    .equalsIgnoreCase(mPoint.getPlayerId())) {
                return mPoint;
            }
        }
        return null;
    }


    private GetDrawingPointForChatResponse getOtherDrawingPoint
            (ArrayList<GetDrawingPointForChatResponse> pointsList){
        for (GetDrawingPointForChatResponse mPoint : pointsList) {
            if (!selectedPlayerData.getPlayerid()
                    .equalsIgnoreCase(mPoint.getPlayerId())) {
                return mPoint;
            }
        }
        return null;
    }

    /**
     * Drawing points from the server
     *
     * @param response
     */
    /*private void drawPointFromServer(final GetDrawingPointForChatResponse response) {
        final ArrayList<GetDrawingPointForChatResponse> pointsList =
                response.getDrawingList();
        this.drawPathForAndroid(pointsList);
    }*/


    /**
     * Drawing points from the server
     *
     *
     */
    private void drawPointFromServer() {
        if(isDrawingFromServerRecieve
                && isDrawingFromChatHistoryRecieve){
            final ArrayList<GetDrawingPointForChatResponse> pointsList =
                    drawingPointForChatResponseresponse.getDrawingList();
            this.drawPathForAndroid(pointsList);
        }
        /*final ArrayList<GetDrawingPointForChatResponse> pointsList =
                response.getDrawingList();
        this.drawPathForAndroid(pointsList);*/
    }

    private void drawPathForAndroid(final ArrayList<GetDrawingPointForChatResponse> pointsList){
        AsyncTask<Void, Void, Void> myDrawingTask = new AsyncTask<Void, Void, Void>() {
            private DrawDeviceDimention myDrawingDimen = null;
            @Override
            protected void onPreExecute() {
                drawingLayouts.setVisibility(RelativeLayout.INVISIBLE);
                super.onPreExecute();
            }
            @Override
            protected Void doInBackground(Void... params) {
                GetDrawingPointForChatResponse myDrawingPoints = getMyDrawingPoint(pointsList);
                if(myDrawingPoints != null){
                    StringBuilder stringBuilder = new StringBuilder(myDrawingPoints.getDrawPoints());
                    stringBuilder.append(getMyDrawingPointsFromRoomChatHistory(true));
                    myDrawingPoints.setDrawPoints(stringBuilder.toString());
                    myDrawingDimen = MathFriendzyHelper.getDrawDeviceDimention
                            (MathFriendzyHelper.getDeviceDimention(getActivity()),
                                    myDrawingPoints.getDeviceSize());
                    String[] points = myDrawingPoints.getDrawPoints().split(";");
                    drawPathList(mySideDrawingView, getPathList(points, myDrawingDimen), true);
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                if (isTutor) {
                    mySideDrawingView.setDrawColor(Color.BLUE);
                } else {
                    mySideDrawingView.setDrawColor(Color.BLACK);
                }
                mySideDrawingView.setEraseMode(false);
                myDrawingCompleted = true;
                if(myDrawingCompleted && otherDrawingCompleted) {
                    isDrawingDone = true;
                    hidePopUp();
                    drawingLayouts.setVisibility(RelativeLayout.VISIBLE);
                    /*setInitialTimerProgress();
                    startTimer();*/
                    //getDrawingPointsHistory();
                }
                super.onPostExecute(aVoid);
            }
        };
        MathFriendzyHelper.executeTask(myDrawingTask);

        AsyncTask<Void, Void, Void> otherDrawingTask = new AsyncTask<Void, Void, Void>() {
            private DrawDeviceDimention otherDrawingDimen = null;
            @Override
            protected void onPreExecute() {
                drawingLayouts.setVisibility(RelativeLayout.INVISIBLE);
                super.onPreExecute();
            }
            @Override
            protected Void doInBackground(Void... params) {
                GetDrawingPointForChatResponse otherDrawingPoints = getOtherDrawingPoint(pointsList);
                if(otherDrawingPoints != null){
                    StringBuilder stringBuilder = new StringBuilder(otherDrawingPoints.getDrawPoints());
                    stringBuilder.append(getMyDrawingPointsFromRoomChatHistory(false));
                    otherDrawingPoints.setDrawPoints(stringBuilder.toString());
                    otherDrawingDimen = MathFriendzyHelper.getDrawDeviceDimention
                            (MathFriendzyHelper.getDeviceDimention(getActivity()),
                                    otherDrawingPoints.getDeviceSize());
                    String[] points = otherDrawingPoints.getDrawPoints().split(";");
                    drawPathList(otherSideDrawingView , getPathList(points , otherDrawingDimen) , false);
                }else{
                    StringBuilder stringBuilder = new StringBuilder("");
                    stringBuilder.append(getMyDrawingPointsFromRoomChatHistory(false));
                    String[] points = stringBuilder.toString().split(";");
                    drawPathList(otherSideDrawingView , getPathList(points , otherDrawingDimen) , false);
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                if (isTutor) {
                    otherSideDrawingView.setDrawColor(Color.BLACK);
                } else {
                    otherSideDrawingView.setDrawColor(Color.BLUE);
                }
                otherSideDrawingView.setEraseMode(false);
                otherDrawingCompleted = true;
                if(myDrawingCompleted && otherDrawingCompleted) {
                    isDrawingDone = true;
                    hidePopUp();
                    drawingLayouts.setVisibility(RelativeLayout.VISIBLE);
                    /*setInitialTimerProgress();
                    startTimer();*/
                    //getDrawingPointsHistory();
                }
                super.onPostExecute(aVoid);
            }
        };
        MathFriendzyHelper.executeTask(otherDrawingTask);
    }

    //for timer progress

    private void setUpTimer() {
        try {
            if (isTutor)
                timer = new MyTimer(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setInitialTimerProgress() {
        try {
            if (isTutor)
                timer.setProgress(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startTimer() {
        try {
            if (isTutor)
                timer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopTimer() {
        try {
            if (isTutor)
                timer.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkForIdleTimeAndSetProgress() {
        try {
            if (isTutor) {
                if (timer.getProgress()
                        - timer.getLastUpdateTimeCunter()
                        >= IDLE_TIME_LIMIT) {
                    this.setLastUpdatedTimeCounter();
                    timer.stop();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setLastUpdatedTimeCounter() {
        try {
            if (isTutor)
                timer.setLastUpdatedCount(timer.getProgress());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgress(final int progress) {
        try {
            CommonUtils.printLog(TAG , "Normal Tutor Timer " + progress);
            (getActivity()).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    checkForIdleTimeAndSetProgress();
                    if (callback != null)
                        callback.onTimeSpent(progress);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Add time spent by the tutor in this session
     */
    private void addTimeSpentInTutorSession() {
        if(isPaidSession){
            this.stopStudentPaidTutorTimer();
            final int timeSpent = getPaidSessionTimeSpent();
            this.deductStudentPurchaseTime(new PurchaseTimeCallback() {
                @Override
                public void onPurchase(UpdatePurchaseTimeResponse response) {
                    addTimeSpentInTutorSessionOnBackPress(new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase,
                                                   int requestCode) {
                            sendNotificationAreaEditTutorOrStudent();
                            studentPurchaseTime = studentPurchaseTime - timeSpent;
                            offlineTimeSpent = 0;
                            onLineTimeSpent = 0;
                            timeSpentByStudentOrTutorBeforeTimeFinish = 0;
                            if(studentPaidTutoringTimer != null)
                                studentPaidTutoringTimer.setProgress(0);
                            startStudentPaidSessionTimer();
                        }
                    }, (timeSpent + getTimeSpentBeforePurchaseTimeFinish()));
                }
            }, timeSpent, true);
        }else {
            if (isTutor) {
                if (selectedTutor.isFromDisconnectedList()
                        && !isClickOnSubmitQuestionAnswer) {
                    return;
                }
                AddTimeSpentInTutorSessionParam param = new AddTimeSpentInTutorSessionParam();
                param.setAction("addTimeSpentInTutorSession");
                param.setRequestId(chatRequestId);
                param.setTimeSpent(timer.getProgress());
                MathFriendzyHelper.addTimeSpentInTutorSession(getActivity(), param,
                        new HttpResponseInterface() {
                            @Override
                            public void serverResponse(HttpResponseBase httpResponseBase,
                                                       int requestCode) {
                                sendNotificationAreaEditTutorOrStudent();
                                setInitialTimerProgress();
                            }
                        });
            }
        }
    }

    /**
     * Add tutor time on back press
     *
     * @param responseIntefrface
     */
    public void addTimeSpentInTutorSessionOnBackPress
    (final HttpResponseInterface responseIntefrface , int timeSpent) {
        //change for professional tutoring, if block added
        if(isPaidSession){
            if(timeSpent > 0 && !isSubmitButtonVisible){
                AddTimeSpentInTutorSessionParam param = new AddTimeSpentInTutorSessionParam();
                if(isTutor) {
                    param.setAction("addTimeSpentInTutorSession");
                }else{
                    param.setAction("addTimeSpentInTutorSessionByStudent");
                }
                param.setRequestId(chatRequestId);
                param.setTimeSpent(timeSpent);
                param.setShowDialog(true);
                MathFriendzyHelper.addTimeSpentInTutorSession(getActivity(), param,
                        new HttpResponseInterface() {
                            @Override
                            public void serverResponse(HttpResponseBase httpResponseBase,
                                                       int requestCode) {
                                responseIntefrface.serverResponse(httpResponseBase, requestCode);
                            }
                        });
            }else{
                responseIntefrface.serverResponse(null, -1);
            }
        }else {
            if (isTutor) {
                if ((selectedTutor.isFromDisconnectedList()
                        && !isClickOnSubmitQuestionAnswer)
                        || isDisconnected) {
                    responseIntefrface.serverResponse(null, -1);
                    return;
                }
                AddTimeSpentInTutorSessionParam param = new AddTimeSpentInTutorSessionParam();
                param.setAction("addTimeSpentInTutorSession");
                param.setRequestId(chatRequestId);
                param.setTimeSpent(timer.getProgress());
                param.setShowDialog(true);
                MathFriendzyHelper.addTimeSpentInTutorSession(getActivity(), param,
                        new HttpResponseInterface() {
                            @Override
                            public void serverResponse(HttpResponseBase httpResponseBase,
                                                       int requestCode) {
                                responseIntefrface.serverResponse(httpResponseBase, requestCode);
                            }
                        });
            } else {
                responseIntefrface.serverResponse(null, -1);
            }
        }
    }

    /**
     * Show the rate us dialog
     */
    public void showRateUsDialog() {
        String message = lblPleaseRateThisSession;
        MathFriendzyHelper.showRateUsDialog(getActivity(), message,
                lblRate, MathFriendzyHelper.convertStringToUnderLineString(lblReportThisTutor),
                new RateUsCallback() {
                    @Override
                    public void numberOfStar(final int stars) {
                        if (chatRequestId > 0 && stars > 0) {
                            RateTutorSessionParam param = new RateTutorSessionParam();
                            param.setAction("rateTheTutoringSession");
                            param.setRequestId(chatRequestId);
                            param.setStars(stars);
                            MathFriendzyHelper.rateTutorSession(getActivity(), param,
                                    new RateTutorSessionCallback() {
                                        @Override
                                        public void onServerResponse(HttpResponseBase httpResponseBase,
                                                                     int requestCode) {
                                            isSessionRated = true;
                                            numberOfStarRated = stars;
                                            stopStudentPaidTutorTimer();
                                            if (callback != null) {
                                                isDisconnected = true;
                                                callback.onTutorTabTextSet(tutorText + ":" + lblDisconnect);
                                                callback.showTutorTabContent(false, false, false, true);
                                                initilizaEnableVariable(false);
                                            }
                                        }
                                    });
                        }
                    }

                    @Override
                    public void clickOnRateThisTutor() {
                        if (chatRequestId > 0) {
                            ReportThisTutorParam param = new ReportThisTutorParam();
                            param.setAction("reportTutor");
                            param.setSenderEmail("");
                            param.setStudentName(selectedPlayerData.getFirstname() + " "
                                    + selectedPlayerData.getLastname());
                            param.setStudentSchool(selectedPlayerData.getSchoolName());
                            param.setStudentTeacher(selectedPlayerData.getTeacherFirstName() + " "
                                    + selectedPlayerData.getTeacheLastName());
                            param.setRequestId(chatRequestId);
                            Intent intent = new Intent(getActivity(), ReportProblemActivity.class);
                            intent.putExtra(ReportProblemActivity.CALLING_ACTIVITY,
                                    ReportProblemActivity.REPORT_THIS_TUTOR_REQ);
                            intent.putExtra("ReportThisTutorParam", param);
                            startActivity(intent);
                        }
                    }
                });
    }


    /**
     * Check for tutor popup on back press
     *
     * @return
     */
    public boolean isNeedToShowTutorPopUpOnBackPress() {
        if (selectedTutor.isFromDisconnectedList()) {
            if (isClickOnSubmitQuestionAnswer) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * Send notification when tutor area is updated by tutor or student
     */
    private void setNotificationWhenTutorAreaUpdated(){
        if(isSubmitButtonVisible)
            return;

        if(chatRequestId > 0 && !isOpponentOnline){
            this.sendNotificationAreaEditTutorOrStudent();
        }
    }

    /**
     * Send the notification when student create request or tutor connect with first time
     */
    private void sendNotificationAreaEditTutorOrStudent() {
        isRequestCreated = true;
        hidePopForRequestCreation();

        if (CommonUtils.isInternetConnectionAvailable(getActivity())
                && !isEditNotificationSent){
            isEditNotificationSent = true;
            EditTutorAreaParam param = new EditTutorAreaParam();
            if (isTutor) {
                param.setAction("editTutorAreaByTutor");
            } else {
                param.setAction("editTutorAreaByStudent");
                param.setForSubject(1);
            }
            param.setReqId(chatRequestId);
            new MyAsyckTask(ServerOperation.createPostRequestForEditTutorArea(param)
                    , null, ServerOperationUtil.EDIT_TUTOR_AREA_BY_STUDENT_OR_TUTOR,
                    getActivity(), this, ServerOperationUtil.SIMPLE_DIALOG, false,
                    getActivity().getString(R.string.please_wait_dialog_msg))
                    .execute();
        }
    }

    /**
     * Check for chat connected
     *
     * @return
     */
    public boolean isChatConnected() {
        return isChatConnected;
    }

    /**
     * Check for tutor connected without submit button
     *
     * @return
     */
    public boolean isConnectedTutorWithoutSubmitButton() {
        if (selectedTutor.isFromDisconnectedList()
                && (isClickOnSubmitQuestionAnswer == false)) {
            try {
                drawingPointsList.clear();
            }catch (Exception e){
                e.printStackTrace();
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * input seen by player
     */
    public void inputSeenByPlayer() {
        if (CommonUtils.isInternetConnectionAvailable(getActivity())) {
            if (chatRequestId > 0) {
                InputSeenByPlayerParam param = new InputSeenByPlayerParam();
                param.setAction("inputSeenByPlayer");
                param.setCharRequestId(chatRequestId);
                if (isTutor) {
                    param.setIsStudent(0);
                } else {
                    param.setIsStudent(1);
                }
                new MyAsyckTask(ServerOperation.createPostRequestForInputSeenByPlauer(param)
                        , null, ServerOperationUtil.INPUT_SEEN_BY_PLAYER_REQUEST,
                        getActivity(), this, ServerOperationUtil.SIMPLE_DIALOG, false,
                        getActivity().getString(R.string.please_wait_dialog_msg))
                        .execute();
            }
        }
    }

    /**
     * Get Active inactive status
     */
    private void getActiveInActiveStatus() {
        if (activeInactiveHandler != null) {
            this.releaseActiveInactiveHandler();
        } else {
            activeInactiveHandler = new Handler();
        }
        activeInactiveHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isChatAlreadyConnected) {
                    /*try {
                        GetUserActiveStatusParam param = new GetUserActiveStatusParam();
                        param.setAction("getUserActiveStatus");
                        if (isTutor) {
                            param.setUserId(selectedTutor.getUserId());
                            param.setPlayerId(selectedTutor.getPlayerId());
                        } else {
                            param.setUserId(getChatResponse.getTuturUid() + "");
                            param.setPlayerId(getChatResponse.getTutorPid() + "");
                        }
                        AsyncTask<Void, Void, HttpResponseBase> task =
                                new MyAsyckTask(ServerOperation.createPostRequestForGetUserActiveStatus(param)
                                        , null, ServerOperationUtil.GET_USER_ACTIVE_STATUS_REQUEST,
                                        getActivity(), new HttpResponseInterface() {
                                    @Override
                                    public void serverResponse(HttpResponseBase httpResponseBase,
                                                               int requestCode) {
                                        if (requestCode == ServerOperationUtil.GET_USER_ACTIVE_STATUS_REQUEST) {
                                            ActiveInactiveUserResponse response = (ActiveInactiveUserResponse)
                                                    httpResponseBase;
                                            if (response.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)) {
                                                if (callback != null) {
                                                    callback.onActiveInActiveStatus(response.getInActive());
                                                }
                                            }
                                        }
                                    }
                                }, ServerOperationUtil.SIMPLE_DIALOG, false,
                                        getActivity().getString(R.string.please_wait_dialog_msg));
                        MathFriendzyHelper.executeCommonTask(task);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/

                    createGlobalRoomObjectOrGetUserStatus();
                }
                activeInactiveHandler.postDelayed(this,
                        MathFriendzyHelper.ACTIVE_INACTIVE_UPDATE_STATUE_TIME);
            }
        }, MathFriendzyHelper.ACTIVE_INACTIVE_UPDATE_STATUE_TIME);
    }

    private void releaseActiveInactiveHandler() {
        if (activeInactiveHandler != null)
            activeInactiveHandler.removeCallbacksAndMessages(null);
    }

    /**
     * Click to select image dialog
     */
    private void clickToSelectAndDeleteImage() {
        if (imageOptionSelector != null) {
            if (!isTutor) {
                if (homeworkAreaObj != null && homeworkAreaObj.isShowOptionForWorkAreaText())
                    imageOptionSelector.setVisibilityOfWorkAreaTextOption(YES);
                else
                    imageOptionSelector.setVisibilityOfWorkAreaTextOption(NO);
            }
            imageOptionSelector.showOptionDialog();
        }
    }

    /**
     * Initialize image selector dialog
     */
    private void initializeImageSelector() {
        imageOptionSelector = new HomeWorkImageOption(getActivity(),
                new HomeworkImageOptionCallback() {
                    @Override
                    public void onOptionSelect(SelectImageOption option) {
                        onImageOptionSelect(option);
                    }
                });
        if (isTutor) {
            imageOptionSelector.setShowOption(true, true,
                    false, false, false);
        } else {
            if (homeworkAreaObj != null) {
                imageOptionSelector.setShowOption(true, true,
                        homeworkAreaObj.isShowHomeworkSheetOptionToSelectImage()
                        , true, false);
                if (homeworkAreaObj.isShowHomeworkSheetOptionToSelectImage()) {
                    imageOptionSelector.setHomeworkSheetName(homeworkAreaObj.getHomeworkSheetName());
                }
            }else{//for tutoring session
                imageOptionSelector.setShowOption(true, true,
                        false, false, false);
            }
        }
    }

    /**
     * Select work area drawing
     *
     * @param option
     */
    private void onSelectWorkAreaImage(SelectImageOption option) {
        if (!isTutor) {
            if (homeworkAreaObj != null) {
                homeworkAreaObj.takeScreenShotAndSaveIntoMemory
                        (new TakeScreenShotAndSaveToMemoryCallback() {
                            @Override
                            public void onComplete(File file, Bitmap bitmap) {
                                if (bitmap != null) {
                                    //imgWorkAreaDrawing.setImageBitmap(bitmap);
                                    if (chatRequestId > 0) {
                                        showPopUp();
                                        uploadBitmapImage(bitmap, WORK_AREA_IMAGE,
                                                getTutorAreaImageName(true));
                                    } else {
                                        initializeSelectedWorkImageBitmap(bitmap);
                                    }
                                }
                                try {
                                    String fileUri = "file:///" + file;
                                    imgWorkAreaDrawing.setImageBitmap(null);
                                    loader.displayImage(fileUri,
                                            imgWorkAreaDrawing, options);
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                        }, getActivity());
            }
        }
    }

    /**
     * On Delete work area drawing image
     */
    private void onDeleteWorkAreaImage() {
        try {
            if (imgWorkAreaDrawing != null) {
                imgWorkAreaDrawing.setImageBitmap(null);
                if (chatRequestId > 0)
                    updateImageNameOnServer(WORK_AREA_IMAGE, "");
                else
                    initializeSelectedWorkImageBitmap(null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Copy the text from the work area
     */
    private void onWorkAreaText() {
        if (homeworkAreaObj != null) {
            this.sendMessage(MathFriendzyHelper.getCopyTextMessageAfterReplacement
                    (homeworkAreaObj.teacherStudentMsg));
        }
    }

    /**
     * Call when option select
     *
     * @param option
     */
    private void onImageOptionSelect(SelectImageOption option) {
        this.selectedOption = option;
        switch (option.getId()) {
            case SelectImageConstants.SELECT_FROM_GALLARY:
                break;
            case SelectImageConstants.SELECT_FROM_CAMERA:
                break;
            case SelectImageConstants.SELECT_FROM_HOMEWORK_SHEET:
                break;
            case SelectImageConstants.SELECT_FROM_WORK_AREA_IMAGE:
                this.onSelectWorkAreaImage(option);
                break;
            case SelectImageConstants.SELECT_WORK_AREA_TEXT:
                this.onWorkAreaText();
                break;
            case SelectImageConstants.DELETE_IMAGE:
                if (isTutor) {
                    imgTutorImage.setImageBitmap(null);
                } else {
                    imgStudentImage.setImageBitmap(null);
                }
                if (chatRequestId > 0) {
                    if (isTutor) {
                        updateImageNameOnServer(TUTOR_IMAGE, "");
                    } else {
                        updateImageNameOnServer(STUDENT_IMAGE, "");
                    }
                } else {
                    initializeSelectedImageBitmap(null);
                }
                break;
            case SelectImageConstants.DELETE_WORK_AREA_IMAGE:
                this.onDeleteWorkAreaImage();
                break;
            case SelectImageConstants.DELETE_WORK_AREA_TEXT:
                break;
        }
    }

    public void onImageSelected(String selectedImageUri) {
        resizeView.setResizeLayoutVisibility(true);
        resizeView.setImageFromUri(selectedImageUri);
        //this.onImageSelectionDone();
    }

    private void onResizeOption(int tag, Bitmap bitmap, RelativeLayout.LayoutParams lp) {
        switch (tag) {
            case ResizeView.RESIZE_DONE:
                resizeView.setResizeLayoutVisibility(false);
                this.onImageSelectionDone(bitmap, lp);
                break;
            case ResizeView.RESIZE_DELETE:
                resizeView.setResizeLayoutVisibility(false);
                break;
        }
    }

    private String getTutorAreaImageName(boolean isWorkAreaImage) {
        if (isWorkAreaImage) {
            return "wk_" + chatRequestId + "_" + selectedPlayerData.getPlayerid();
        } else {
            return chatRequestId + "_" + selectedPlayerData.getPlayerid();
        }
    }

    private void onImageSelectionDone(Bitmap bitmap, RelativeLayout.LayoutParams lp) {
        if (bitmap != null) {
            lp.topMargin = lp.topMargin + scrollView.getScrollY();
            if (isTutor) {
                this.swapStudentAndTutorImageLayout(YES);
                imgTutorImage.setLayoutParams(lp);
                imgTutorImage.setImageBitmap(bitmap);
            } else {
                this.swapStudentAndTutorImageLayout(NO);
                imgStudentImage.setLayoutParams(lp);
                imgStudentImage.setImageBitmap(bitmap);
            }
        }

        if (this.selectedOption != null && imageOptionSelector != null) {
            imageOptionSelector.setVisibilityAfterOptionSelectionDone(this.selectedOption);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    new AsyncTask<Void, Void, Bitmap>() {
                        private ProgressDialog pd = null;

                        protected void onPreExecute() {
                            showPopUp();
                        }

                        @Override
                        protected Bitmap doInBackground(Void... params) {
                            Bitmap bitmap = null;
                            if (isTutor) {
                                bitmap = MathFriendzyHelper.takeScreenShot(getActivity(),
                                        tutorImageLayout);
                            } else {
                                bitmap = MathFriendzyHelper.takeScreenShot(getActivity(),
                                        studentImageLayout);
                            }
                            return bitmap;
                        }

                        protected void onPostExecute(Bitmap bitmap) {

                            if (chatRequestId > 0) {
                                if (isTutor) {
                                    uploadBitmapImage(bitmap,
                                            TUTOR_IMAGE, getTutorAreaImageName(false));
                                } else {
                                    uploadBitmapImage(bitmap,
                                            STUDENT_IMAGE, getTutorAreaImageName(false));
                                }
                            } else {//for first time until chat request not created
                                hidePopWithoutCondition();
                                initializeSelectedImageBitmap(bitmap);
                            }
                        }
                    }.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 500);//half seconds
    }


    /**
     * Set student image
     *
     * @param imageName
     */
    private void setStudentImage(String imageName) {
        if (imageName != null && imageName.length() > 0) {
            String newImageName = getPNGFormateImageName(imageName);
            this.setImageFromUrl(imgStudentImage ,
                    newImageName , new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String s, View view) {
                        }

                        @Override
                        public void onLoadingFailed(String s, View view, FailReason failReason) {
                            isStudentImageDone = true;
                            hidePopUp();
                        }

                        @Override
                        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                            setStudentImageLayoutParam();
                            if (!isTutor) {
                                if (imageSelectedOption != null && imageOptionSelector != null) {
                                    imageOptionSelector.setVisibilityAfterOptionSelectionDone
                                            (imageSelectedOption);
                                }
                            }
                            isStudentImageDone = true;
                            hidePopUp();
                        }

                        @Override
                        public void onLoadingCancelled(String s, View view) {
                            isStudentImageDone = true;
                            hidePopUp();
                        }
                    });
        }else{
            /*try {
                imgStudentImage.setImageBitmap(null);
            }catch (Exception e){
                e.printStackTrace();
            }*/
            isStudentImageDone = true;
            hidePopUp();
        }
    }

    private void setImageFromUrl(ImageView imgView , String imageName , ImageLoadingListener listener){
        loader.displayImage(ICommonUtils.DOWNLOAD_CUSTOM_TUTOR_IMAGE_URL + imageName,
                imgView, options , listener);
    }

    /**
     * Set tutot image
     *
     * @param imageName
     */
    private void setTutorImage(String imageName) {
        if (imageName != null && imageName.length() > 0) {

            String newImageName = getPNGFormateImageName(imageName);
            this.setImageFromUrl(imgTutorImage ,
                    newImageName , new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String s, View view) {
                        }

                        @Override
                        public void onLoadingFailed(String s, View view, FailReason failReason) {
                            isTutorImageDone = true;
                            hidePopUp();
                        }

                        @Override
                        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                            setTutorImageLayoutParam();
                            if (isTutor) {
                                if (imageSelectedOption != null && imageOptionSelector != null) {
                                    imageOptionSelector.setVisibilityAfterOptionSelectionDone
                                            (imageSelectedOption);
                                }
                            }
                            isTutorImageDone = true;
                            hidePopUp();
                        }

                        @Override
                        public void onLoadingCancelled(String s, View view) {
                            isTutorImageDone = true;
                            hidePopUp();
                        }
                    });
        }else{
            /*try {
                imgTutorImage.setImageBitmap(null);
            }catch (Exception e){
                e.printStackTrace();
            }*/
            isTutorImageDone = true;
            hidePopUp();
        }
    }

    /**
     * Set work area image
     *
     * @param imageName
     */
    private void setWorkAreaImage(String imageName) {
        if (imageName != null && imageName.length() > 0) {
            imgWorkAreaDrawing.setImageBitmap(null);
            String newImageName = getPNGFormateImageName(imageName);
            this.setImageFromUrl(imgWorkAreaDrawing ,
                    newImageName , new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String s, View view) {
                        }

                        @Override
                        public void onLoadingFailed(String s, View view, FailReason failReason) {
                            isWorkImageDone = true;
                            hidePopUp();
                        }

                        @Override
                        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                            if (!isTutor) {
                                if (workAreaSelectedOption != null && imageOptionSelector != null) {
                                    imageOptionSelector.setVisibilityAfterOptionSelectionDone
                                            (workAreaSelectedOption);
                                }
                            }
                            isWorkImageDone = true;
                            hidePopUp();
                        }

                        @Override
                        public void onLoadingCancelled(String s, View view) {
                            isWorkImageDone = true;
                            hidePopUp();
                        }
                    });
        }else{
            /*try {
                imgWorkAreaDrawing.setImageBitmap(null);
            }catch (Exception e){
                e.printStackTrace();
            }*/
            isWorkImageDone = true;
            hidePopUp();
        }
    }

    /**
     * Set work area image
     *
     * @param imageName
     */
    private void setWorkAreaImageWithLoadingWheel(String imageName) {
        if (imageName != null && imageName.length() > 0) {
            imgWorkAreaDrawing.setImageBitmap(null);
            String newImageName = getPNGFormateImageName(imageName);
            this.setImageFromUrl(imgWorkAreaDrawing ,
                    newImageName , new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String s, View view) {
                            showPopUp();
                        }

                        @Override
                        public void onLoadingFailed(String s, View view, FailReason failReason) {
                            hidePopWithoutCondition();
                        }

                        @Override
                        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                            if (!isTutor) {
                                if (workAreaSelectedOption != null && imageOptionSelector != null) {
                                    imageOptionSelector.setVisibilityAfterOptionSelectionDone
                                            (workAreaSelectedOption);
                                }
                            }
                            hidePopWithoutCondition();
                        }

                        @Override
                        public void onLoadingCancelled(String s, View view) {
                            hidePopWithoutCondition();
                        }
                    });

        }
    }

    /**
     * Set student image
     *
     * @param imageName
     */
    private void setStudentImageWithLoadingWheel(String imageName) {
        if (imageName != null && imageName.length() > 0) {

            String newImageName = getPNGFormateImageName(imageName);
            this.setImageFromUrl(imgStudentImage ,
                    newImageName , new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String s, View view) {
                            showPopUp();
                        }

                        @Override
                        public void onLoadingFailed(String s, View view, FailReason failReason) {
                            hidePopWithoutCondition();
                        }

                        @Override
                        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                            setStudentImageLayoutParam();
                            if (!isTutor) {
                                if (imageSelectedOption != null && imageOptionSelector != null) {
                                    imageOptionSelector.setVisibilityAfterOptionSelectionDone
                                            (imageSelectedOption);
                                }
                            }
                            hidePopWithoutCondition();
                        }

                        @Override
                        public void onLoadingCancelled(String s, View view) {
                            hidePopWithoutCondition();
                        }
                    });
        }
    }

    /**
     * Set tutot image
     *
     * @param imageName
     */
    private void setTutorImageWithLoadingWheel(String imageName) {
        if (imageName != null && imageName.length() > 0) {
            String newImageName = getPNGFormateImageName(imageName);
            this.setImageFromUrl(imgTutorImage ,
                    newImageName , new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String s, View view) {
                            showPopUp();
                        }

                        @Override
                        public void onLoadingFailed(String s, View view, FailReason failReason) {
                            hidePopWithoutCondition();
                        }

                        @Override
                        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                            setTutorImageLayoutParam();
                            if (isTutor) {
                                if (imageSelectedOption != null && imageOptionSelector != null) {
                                    imageOptionSelector.setVisibilityAfterOptionSelectionDone
                                            (imageSelectedOption);
                                }
                            }
                            hidePopWithoutCondition();
                        }

                        @Override
                        public void onLoadingCancelled(String s, View view) {
                            hidePopWithoutCondition();
                        }
                    });
        }
    }

    /**
     * Upload bitmap image on server
     *
     * @param bitmap
     * @param imageFor
     */
    private void uploadBitmapImage(Bitmap bitmap, final int imageFor, final String imageName) {
        MathFriendzyHelper.uploadBitmapImage(bitmap, imageName,
                ICommonUtils.UPLOAD_CUSTOM_TUTOR_IMAGE_URL
                        + "action=uploadTutorAreaImage"
                , new UploadImageRequest() {
                    @Override
                    public void onComplete(String response) {

                        initializeSelectedImageBitmap(null);
                        initializeSelectedWorkImageBitmap(null);
                        CommonUtils.printLog(TAG , response);
                        updateImageNameOnServer(imageFor, imageName);
                    }
                } , isTab);
    }

    /**
     * Upload bitmap image on server
     *
     * @param bitmap
     * @param imageFor
     */
    private void uploadBitmapImage(Bitmap bitmap, final int imageFor, final String imageName
            , final UploadImageRequest request) {
        MathFriendzyHelper.uploadBitmapImage(bitmap, imageName,
                ICommonUtils.UPLOAD_CUSTOM_TUTOR_IMAGE_URL
                        + "action=uploadTutorAreaImage"
                , new UploadImageRequest() {
                    @Override
                    public void onComplete(String response) {
                        CommonUtils.printLog(TAG , response);
                        updateImageNameOnServer(imageFor, imageName , request);
                    }
                } , isTab);
    }

    /**
     * Upload image name on server
     *
     * @param imageFor
     * @param imageName
     */
    private void updateImageNameOnServer(int imageFor, String imageName ,
                                         final UploadImageRequest request) {
        if (CommonUtils.isInternetConnectionAvailable(getActivity())) {
            UpdateTutorAreaImageName param = new UpdateTutorAreaImageName();
            if (imageFor == WORK_AREA_IMAGE) {
                param.setShouldSendPN(-1);
                param.setAction("updateWorkAreaImageInTutorArea");
            } else {
                if (isTutor) {
                    param.setAction("imageUpdateTutorAreaByTutor");
                    if(isSubmitButtonVisible)
                        param.setShouldSendPN(0);
                    else
                        param.setShouldSendPN(1);
                } else {
                    param.setShouldSendPN(-1);
                    param.setAction("imageUpdateTutorAreaByStudent");
                }
            }
            param.setRequestId(chatRequestId + "");
            param.setImageName(imageName);
            if (isTutor)
                param.setStudentUserId(selectedTutor.getUserId());
            else
                param.setStudentUserId(null);
            new MyAsyckTask(ServerOperation.createPostRequestForUpdateTutorAreaImageName(param)
                    , null, ServerOperationUtil.UPDATE_TUTOR_AREA_IMAGE_REQUEST,
                    getActivity(), new HttpResponseInterface() {
                @Override
                public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                    request.onComplete(null);
                }
            }, ServerOperationUtil.SIMPLE_DIALOG, false,
                    getActivity().getString(R.string.please_wait_dialog_msg))
                    .execute();
        }
    }

    /**
     * Upload image name on server
     *
     * @param imageFor
     * @param imageName
     */
    private void updateImageNameOnServer(int imageFor, String imageName) {
        if (CommonUtils.isInternetConnectionAvailable(getActivity())) {
            showPopUp();
            UpdateTutorAreaImageName param = new UpdateTutorAreaImageName();
            if (imageFor == WORK_AREA_IMAGE) {
                param.setShouldSendPN(-1);
                param.setAction("updateWorkAreaImageInTutorArea");
            } else {
                if (isTutor) {
                    param.setAction("imageUpdateTutorAreaByTutor");
                    if(isSubmitButtonVisible)
                        param.setShouldSendPN(0);
                    else
                        param.setShouldSendPN(1);
                } else {
                    param.setShouldSendPN(-1);
                    param.setAction("imageUpdateTutorAreaByStudent");
                }
            }
            param.setRequestId(chatRequestId + "");
            param.setImageName(imageName);
            if (isTutor)
                param.setStudentUserId(selectedTutor.getUserId());
            else
                param.setStudentUserId(null);
            new MyAsyckTask(ServerOperation.createPostRequestForUpdateTutorAreaImageName(param)
                    , null, ServerOperationUtil.UPDATE_TUTOR_AREA_IMAGE_REQUEST,
                    getActivity(), new HttpResponseInterface() {
                @Override
                public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                    hidePopWithoutCondition();
                }
            }, ServerOperationUtil.SIMPLE_DIALOG, false,
                    getActivity().getString(R.string.please_wait_dialog_msg))
                    .execute();
        }else{
            hidePopWithoutCondition();
        }
    }

    /**
     * Initialize the selected image bitmap if chat request not created
     * and use that at the time of click on Submit question
     *
     * @param bitmap
     */
    private void initializeSelectedImageBitmap(Bitmap bitmap) {
        selectedImageBitmap = bitmap;
    }

    /**
     * Initialize the selected work image bitmap if chat request not created
     * and use that at the time of click on Submit question
     *
     * @param bitmap
     */
    private void initializeSelectedWorkImageBitmap(Bitmap bitmap) {
        studentWorkAreaImageBitmap = bitmap;
    }

    /**
     * Tutor image update
     *
     * @param response
     */
    public void updateUIFromNotificationForTutorImageUpdate(TutorImageUpdateNotif response) {
        //if play as tutor then need to update the student image
        if (response.getUpdateFor() == TutorImageUpdateNotif.TUTOR) {
            if (response.getImage() != null
                    && response.getImage().length() > 0) {
                this.swapStudentAndTutorImageLayout(NO);
                setStudentImageWithLoadingWheel(response.getImage());
            } else {
                imgStudentImage.setImageBitmap(null);
            }
            //if play as student then need to update the tutor image
        } else if (response.getUpdateFor() == TutorImageUpdateNotif.STUDENT) {
            if (response.getImage() != null
                    && response.getImage().length() > 0) {
                this.swapStudentAndTutorImageLayout(YES);
                setTutorImageWithLoadingWheel(response.getImage());
            } else {
                imgTutorImage.setImageBitmap(null);
            }
        }
    }

    /**
     * work area image update
     *
     * @param response
     */
    public void updateUIFromNotoficationForTutorWorkImageUpdate
    (TutorWorkAreaImageUpdateNotif response) {
        if (response.getImage() != null
                && response.getImage().length() > 0) {
            setWorkAreaImageWithLoadingWheel(response.getImage());
        } else {
            imgWorkAreaDrawing.setImageBitmap(null);
        }
    }

    /**
     * Swap the student and tutor image view
     *
     * @param lastUpdatedByTutor
     */
    private void swapStudentAndTutorImageLayout(int lastUpdatedByTutor) {
        this.swapStudentAndTutorImageView(lastUpdatedByTutor
                , studentImageLayout, tutorImageLayout);
    }

    /**
     * Swap the student and tutor image view
     *
     * @param lastUpdatedByTutor
     * @param studentView
     * @param tutorView
     */
    private void swapStudentAndTutorImageView(int lastUpdatedByTutor
            , RelativeLayout studentView, RelativeLayout tutorView) {
        drawingLayouts.removeView(studentView);
        drawingLayouts.removeView(tutorView);
        if (lastUpdatedByTutor == YES) {
            drawingLayouts.addView(studentView, 1);
            drawingLayouts.addView(tutorView, 2);
        } else {
            drawingLayouts.addView(tutorView, 1);
            drawingLayouts.addView(studentView, 2);
        }
    }

    /**
     * For adding duplicate work area content
     */
    private void addContentToDuplicateArea(OnRequestComplete listener) {
        if (homeworkAreaObj != null) {
            homeworkAreaObj.uploadWorkAreaContentForDuplicateWorkArea(getActivity() , listener);
        }else{
            listener.onComplete();
        }
    }

    /**
     * Check for resize image layout is open
     * @return
     */
    public boolean isResizeImageLayoutOpen(){
        try {
            if (resizeView != null)
                return resizeView.isResizeImageLayoutOpen();
            return false;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public void releaseFromChatWhenInternetDisconnect(){
        try {
            if(drawingTask != null) {
                drawingTask.disconnect();
                isSocketConnected = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Release exception " + e.toString());
        }
    }

    /**
     * On Internet connection lost
     */
    public void internetConnectionLost(){
        if(isInternetConnected) {
            isInternetConnected = false;
            this.pauseStudentPaidTimer();
            this.endbleDisableDrawing(false);
            CommonUtils.showInternetDialog(getActivity());
        }
    }

    /**
     * Release chat handler for getting online offline status
     */
    private void releaseChatHandler(){
        try {
            if (chat != null)
                ((GroupChatManagerImpl) chat)
                        .releaseGetOnlineUsersHandler();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * On internet connected
     */
    public void internetConnected(){
        isInternetConnected = true;
        if (chatRequestId > 0) {
            //Update comment
            //updateDrawingPonits();
        }
        this.releaseFromChatWhenInternetDisconnect();
        drawingTask = null;
        createDrawingTaskObject();
        updateUIFromNotificationForConnectWithStudent(null);
    }


    public boolean isSessionRated(){
        return isSessionRated;
    }

    public int getNumberOfStarRated(){
        return numberOfStarRated;
    }

    /**
     * Return the drawing dimention
     * @param message
     * @return
     */
    private DrawDeviceDimention getDrawDimentionForServerDrawing(String message){
        DrawDeviceDimention dimention = null;
        try {
            if (message.contains(SIZE)) {
                String otherDeviceSize = message.replaceAll(POINTS_PREFF , "").replaceAll(SIZE , "");
                dimention = MathFriendzyHelper.getDrawDeviceDimention(currentDevice ,
                        otherDeviceSize);
            }
            return dimention;
        }catch(Exception e){
            e.printStackTrace();
            return dimention;
        }
    }

    private ArrayList<MyPath> getPathList(String[] points , DrawDeviceDimention dimention){
        try {
            float mX = 0, mY = 0;
            ArrayList<MyPath> pathList = new ArrayList<MyPath>();
            MyPath mPath = null;
            Path path = null;
            DrawDeviceDimention newdimention = dimention;
            for (int i = 0; i < points.length; i++) {
                if(this.getDrawDimentionForServerDrawing(points[i]) != null){
                    newdimention =  this.getDrawDimentionForServerDrawing(points[i]);
                    continue;
                }
                //PointF point = MathFriendzyHelper.getDeviceScalePoints(points[i], dimention);
                PointF point = MathFriendzyHelper.getDeviceScalePoints(points[i], newdimention);
                if(point != null) {
                    if (points[i].contains(BEGIN)) {
                        path = new Path();
                        mPath = new MyPath();
                        path.moveTo(point.x, point.y);
                        mX = point.x;
                        mY = point.y;
                        if (points[i].contains(ERASE)) {
                            mPath.setEraseMode(true);
                        } else {
                            mPath.setEraseMode(false);
                        }
                        //Log.e(TAG , "inside begin " + points[i] + " " + point.x + " " + point.y);
                    } else if (points[i].contains(END)) {
                        if(path != null) {
                            path.lineTo(point.x, point.y);
                            mPath.setPath(path);
                            pathList.add(mPath);
                        }
                    } else {
                        if(path != null) {
                            path.quadTo(mX, mY, (point.x + mX) / 2,
                                    (point.y + mY) / 2);
                            mX = point.x;
                            mY = point.y;
                        }
                    }
                }
            }
            return pathList;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    private void drawPathList(final MyView mView , final ArrayList<MyPath> pathList ,
                              final boolean isMySideView){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.drawPaths(pathList , isTutor , isMySideView);
            }
        });
    }

    private void clearAllBitmap(){
        try {
            this.cleanUpView();
            MathFriendzyHelper.removeView(viewGroup);
            MathFriendzyHelper.clearBitmap(mySideBitmap , otherSideBitmap , selectedImageBitmap ,
                    studentWorkAreaImageBitmap , mySideDrawingView.getmBitmap()
                    , otherSideDrawingView.getmBitmap());
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void cleanUpView(){
        try {
            imgWorkAreaDrawing.setImageBitmap(null);
            imgStudentImage.setImageBitmap(null);
            imgTutorImage.setImageBitmap(null);
            mySideDrawingView.destroyDrawingCache();
            otherSideDrawingView.destroyDrawingCache();
            mySideDrawingView.setCanvasToNull();
            otherSideDrawingView.setCanvasToNull();
            mySideDrawingView = null;
            otherSideDrawingView = null;
            mySideDrawingLayout = null;
            othersideDrawingLayout = null;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //changes for professional tutoring
    private void initializeIsPaidSession(String paidSession){
        if(!MathFriendzyHelper.isEmpty(paidSession)
                && paidSession.equalsIgnoreCase(MathFriendzyHelper.YES + "")){
            isPaidSession = true;
        }else{
            isPaidSession = false;
        }
    }

    private void initializeStudentPurchaseTime(int studentPurchaseTime){
        this.studentPurchaseTime = studentPurchaseTime;
    }

    public void updatePurchaseTime(int newPurchaseTime){
        if(newPurchaseTime > 0){
            isPurchaseTimeFinish = false;
            this.initializeStudentPurchaseTime(newPurchaseTime
                    + this.studentPurchaseTime);
            initilizaEnableVariable(true);
            this.startStudentPaidSessionTimer();
        }
    }

    private void startStudentPaidTutorTimer() {
        try {
            if (isPaidSession && !isDisconnected) {
                if (isTutor && !isAcceptAnswerButtonVisible) {
                    this.setUpStudentPaidTutorTimer();
                    this.startStudentPaidSessionTimer();
                } else {
                    if (isSessionConnected) {
                        this.setUpStudentPaidTutorTimer();
                        this.startStudentPaidSessionTimer();
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void setUpStudentPaidTutorTimer(){
        try {
            if (studentPaidTutoringTimer == null) {
                studentPaidTutoringTimer =
                        new MyTimer(new TimerProgressCallback() {
                            @Override
                            public void onProgress(int progress) {
                                CommonUtils.printLog(TAG, " student progress " +
                                        "progress " + progress + " " + studentPurchaseTime
                                        + " online time spent " + onLineTimeSpent +
                                        " offline time spent " + offlineTimeSpent);
                                //new change for the time spent update on 24/09/2015
                                updateOnlineOfflineTimeSpent();
                                //end change

                                checkForIdealTimeForPaidSession();
                                if(studentPurchaseTime == progress){
                                    studentPaidTutoringTimer.stop();
                                    studentPaidTutoringTimer.setProgress(0);
                                    /*timeSpentByStudentOrTutorBeforeTimeFinish =
                                            timeSpentByStudentOrTutorBeforeTimeFinish
                                                    + studentPurchaseTime;*/
                                    if(isTutor){
                                        timeSpentByStudentOrTutorBeforeTimeFinish =
                                                timeSpentByStudentOrTutorBeforeTimeFinish
                                                        + onLineTimeSpent + offlineTimeSpent;
                                    }else{
                                        timeSpentByStudentOrTutorBeforeTimeFinish =
                                                timeSpentByStudentOrTutorBeforeTimeFinish + offlineTimeSpent;
                                    }
                                    //new change for the time spent update on 24/09/2015
                                    initializeOnlineTimeSpent(0);
                                    initializeOfflineTimeSpent(0);
                                    //end change

                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            deductStudentPurchaseTime(new PurchaseTimeCallback() {
                                                @Override
                                                public void onPurchase
                                                        (UpdatePurchaseTimeResponse response) {

                                                }
                                            } , studentPurchaseTime , false);
                                            studentPurchaseTime = 0;
                                            warningDialogWhenPurchaseTimeForStudentFinish();
                                        }
                                    });
                                }
                            }
                        });
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void startStudentPaidSessionTimer(){
        try {
            if (studentPaidTutoringTimer != null) {
                if(isTutor) {
                    //new change for the time spent update on 24/09/2015 ,
                    //comment the isOpponent condition
                    if(/*!isOpponentOnline && */!isTutorInIdealMode) {
                        if (!studentPaidTutoringTimer.isTimerRunning())
                            studentPaidTutoringTimer.start();
                    }
                }else{
                    if(!studentPaidTutoringTimer.isTimerRunning())
                        studentPaidTutoringTimer.start();
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void stopStudentPaidTutorTimer(){
        try {
            if (studentPaidTutoringTimer != null) {
                if(studentPaidTutoringTimer.isTimerRunning())
                    studentPaidTutoringTimer.stop();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void pauseStudentPaidTimer(){
        try {
            if (studentPaidTutoringTimer != null) {
                studentPaidTutoringTimer.stop();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Show the dialog when student purchase time is finish
     */
    private void warningDialogWhenPurchaseTimeForStudentFinish(){
        String alertMessage = "";
        if(isTutor){
            alertMessage = lblThisStudentRunOutOfTime;
        }else{
            alertMessage = lblYouHaveReachedYourTime;
        }
        MathFriendzyHelper.showWarningDialog(getActivity() ,
                alertMessage , new HttpServerRequest() {
                    @Override
                    public void onRequestComplete() {
                        if(isTutor){
                            getActivity().onBackPressed();
                        }else{
                            isPurchaseTimeFinish = true;
                            initilizaEnableVariable(false);
                            startPurchaseTimeScreen(getActivity());
                        }
                    }
                });
    }

    private void startPurchaseTimeScreen(Activity activity){
        Intent intent = new Intent(activity , ActTutoringPackage.class);
        activity.startActivityForResult(intent,
                MathFriendzyHelper.START_PURCHASE_SCREEN_REQUEST_CODE);
    }

    public void deductStudentPurchaseTime(PurchaseTimeCallback callback , int deductTime ,
                                          boolean isShowDialog){
        try {
            if (studentPaidTutoringTimer != null
                    && !isSubmitButtonVisible &&
                    isPaidSession && !isDisconnected) {
                String userId = "";
                String playerId = "";
                if (isTutor) {
                    userId = selectedTutor.getUserId();
                    playerId = selectedTutor.getPlayerId();
                } else {
                    userId = selectedPlayerData.getParentUserId();
                    playerId = selectedPlayerData.getPlayerid();
                }
                MathFriendzyHelper.updatePurchaseTimeForStudent("deductPurchasedTimeForPlayer",
                        getActivity(), userId, playerId, deductTime,
                        callback , isShowDialog);
            }else{
                callback.onPurchase(null);
            }
        }catch(Exception e){
            e.printStackTrace();
            callback.onPurchase(null);
        }
    }

    private void checkForIdealTimeForPaidSession(){
        try {
            if(studentPaidTutoringTimer != null) {
                if (isTutor) {
                    if (studentPaidTutoringTimer.getProgress() -
                            studentPaidTutoringTimer.getLastUpdateTimeCunter()
                            >= IDEAL_TIME_FOR_PAID_SESSION) {
                        isTutorInIdealMode = true;
                        this.setLastUpdatedTimeCounterForPaidSession();
                        studentPaidTutoringTimer.stop();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setLastUpdatedTimeCounterForPaidSession() {
        try {
            if(studentPaidTutoringTimer != null) {
                if (isTutor)
                    studentPaidTutoringTimer
                            .setLastUpdatedCount(studentPaidTutoringTimer.getProgress());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void activeTutorOnSession() {
        isTutorInIdealMode = false;
        this.startStudentPaidSessionTimer();
        this.setLastUpdatedTimeCounterForPaidSession();
    }

    public boolean isPaidSession(){
        return isPaidSession;
    }

    public int getPaidSessionTimeSpent(){
        int timeSpent = 0;
        if(isPaidSession){
            if(studentPaidTutoringTimer != null){
                //new change for the time spent update on 24/09/2015
                //comment the below code
                //timeSpent = studentPaidTutoringTimer.getProgress();
                this.stopStudentPaidTutorTimer();
            }
            //new change for the time spent update on 24/09/2015 addedd the folowing line
            if(isTutor) {
                timeSpent = onLineTimeSpent + offlineTimeSpent;
            }else{
                timeSpent = offlineTimeSpent;
            }
        }
        return timeSpent;
    }

    public int getOnlyTutorTimeSpent(){
        int onlyTutorTimeSpent = 0;
        if ((selectedTutor.isFromDisconnectedList()
                && !isClickOnSubmitQuestionAnswer)
                || isDisconnected) {
            return onlyTutorTimeSpent;
        }else{
            if(timer != null){
                onlyTutorTimeSpent = timer.getProgress();
                timer.stop();
            }else{
                onlyTutorTimeSpent = 0;
            }
        }
        return onlyTutorTimeSpent;
    }

    public int getTimeSpentBeforePurchaseTimeFinish(){
        return timeSpentByStudentOrTutorBeforeTimeFinish;
    }

    private void updateOnlineOfflineTimeSpent(){
        if(isOpponentOnline){
            onLineTimeSpent ++;
        }else{
            offlineTimeSpent ++;
        }
    }

    public void initializeOnlineTimeSpent(int timeSpent){
        onLineTimeSpent = timeSpent;
    }

    public void initializeOfflineTimeSpent(int timeSpent){
        offlineTimeSpent = timeSpent;
    }
    //end changes for professional tutoring


    private void getPhraseListAndSetAdapter(){
        if(myPhraseList != null && myPhraseList.size() > 0){//first time its null
            if(phraseAdapter == null){
                setPhraseListAdapter(myPhraseList);
                clickOnPhraseButton(true);
            }else{
                clickOnPhraseButton(!isShowPhraseList);
            }
        }else {
            MathFriendzyHelper.getPhraseList(getActivity(), new PhraseListListener() {
                @Override
                public void onPhraseList(ArrayList<PhraseCatagory> phraseList) {
                    if(phraseList != null && phraseList.size() > 0) {
                        myPhraseList = phraseList;
                        setPhraseListAdapter(phraseList);
                        clickOnPhraseButton(true);
                    }
                }
            } , isTutor);
        }
    }

    private void setPhraseListAdapter(final ArrayList<PhraseCatagory> phraseCatList){
        phraseAdapter = new ExpandablePhraseAdapter(getActivity()
                , phraseCatList , this.isPaidSession());
        lstPhraseList.setAdapter(phraseAdapter);
        for(int i = 0 ; i < phraseCatList.size() ; i ++ ){
            lstPhraseList.expandGroup(i);
        }

        lstPhraseList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {

                if(phraseCatList.get(groupPosition).getSubCatList()
                        .get(childPosition).getIsSelectable() == MathFriendzyHelper.YES) {
                    clickOnChild(phraseCatList.get(groupPosition).getSubCatList()
                            .get(childPosition).getSubCatName());
                }
                return false;
            }
        });

        lstPhraseList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener(){
            @Override
            public boolean onGroupClick(ExpandableListView parent,
                                        View v, int groupPosition, long id){
                return true;
            }
        });
    }

    private void clickOnPhraseButton(boolean isShowPhraseList) {
        this.isShowPhraseList = isShowPhraseList;
        if(isShowPhraseList){
            phraseLayout.setVisibility(RelativeLayout.VISIBLE);
        }else{
            phraseLayout.setVisibility(RelativeLayout.GONE);
        }
    }

    private void clickOnChild(String text){
        try{
            edtTeacherStudentText.setText(edtTeacherStudentText.getText()
                    .toString() + " " + text);
            clickOnPhraseButton(false);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void setBtnPhraseListButtonVisibility(){
        if((isShowTutorTabToTeacher)
                || !isWorkAreaForstudent){
            btnPhraseList.setVisibility(Button.GONE);
            return ;
        }
        btnPhraseList.setVisibility(Button.VISIBLE);
    }

    private void processServerMessage(String message){
        try{
            message = message.replaceAll(MathFriendzyHelper.SERVER_PREFIX , "");
            if(message.equalsIgnoreCase(MathFriendzyHelper.PING)){
                this.sendDrawingMessage(MathFriendzyHelper.SERVER_PREFIX + MathFriendzyHelper.PONG);
                return ;
            }

            JSONObject jsonObject = new JSONObject(message);
            if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject, "cmd")){
                String onlineStatus = jsonObject.getString("cmd");
                if(onlineStatus.equalsIgnoreCase(MathFriendzyHelper.START)){
                    this.opponentOnlineStatus(true);
                }else if(onlineStatus.equalsIgnoreCase(MathFriendzyHelper.STOP)){
                    this.opponentOnlineStatus(false);
                }else{
                    this.opponentOnlineStatus(false);
                }
            }else if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject , "users")){
                String currentPlayerId = selectedPlayerData.getPlayerid();
                ArrayList<String> onlineUserIds = new ArrayList<String>();
                JSONArray jsonArray = jsonObject.getJSONArray("users");
                for(int i = 0 ; i < jsonArray.length() ; i ++ ){
                    JSONObject jsonUSerObj = jsonArray.getJSONObject(i);
                    String userId = jsonUSerObj.getString("user_id");
                    if(!currentPlayerId.equalsIgnoreCase(userId)){
                        onlineUserIds.add(userId);
                    }
                }

                if(onlineUserIds.size() > 0){
                    this.opponentOnlineStatus(true);
                }else{
                    this.opponentOnlineStatus(false);
                }
            }else if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject , "room_history")){
                this.drawPointFromChatHistory(jsonObject);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void opponentOnlineStatus(boolean isOpponentOnline){
        if (isOpponentOnline) {
            if (isTutor) {//for tutor
                if (isDisconnected) {
                    this.initilizaEnableVariable(false);
                } else {
                    this.initilizaEnableVariable(true);
                }
            } else {//for student
                if (getChatResponse != null) {
                    if (isDisconnected) {
                        this.initilizaEnableVariable(false);
                    } else {
                        if (getChatResponse.getIsConnected() == YES
                                && MathFriendzyHelper.parseInt
                                (getChatResponse.getTimeSpent()) > 0) {
                            this.initilizaEnableVariable(true);
                        }else{
                            this.initilizaEnableVariable(false);
                        }
                    }
                }
            }
            if (callback != null)
                callback.onlineStatus(true);
            this.isOpponentOnline = true;
            isEditNotificationSent = false;
        } else {//opponent is offline
            if (callback != null)
                callback.onlineStatus(false);
            this.isOpponentOnline = false;
        }
    }

    /**
     * Save Message To our local server
     * @param messageText
     */
    private void saveMessageToServer(String messageText){
        if(CommonUtils.isInternetConnectionAvailable(getActivity())) {
            String currentPlayerName = selectedPlayerData.getFirstname() + " "
                    + selectedPlayerData.getLastname();
            MathFriendzyHelper.saveMessageForSingleChat(getActivity(),
                    this.getSaveSingleChatParam(messageText,
                            this.getParamJson(currentPlayerName, isTutor).toString()),
                    new SaveMessageForSingleChatCallback() {
                        @Override
                        public void onSuccess
                                (SaveMessageForSingleChatResponse saveMessageResponse) {

                        }
                    });
        }
    }

    public void updateTutorAreaStatus(int status){
        try {
            if (chatRequestId > 0) {
                SetTutorAreaStatusParam param = new SetTutorAreaStatusParam();
                param.setAction("setTutorAreaStatus");
                if (isTutor) {
                    param.setIsStudent(0);
                } else {
                    param.setIsStudent(1);
                }
                param.setOnline(status);
                param.setPlayerId(selectedPlayerData.getPlayerid());
                param.setReqId(chatRequestId);
                new MyAsyckTask(ServerOperation.createPostRequestForSetTutorAreaStatus(param)
                        , null, ServerOperationUtil.SET_TUTOR_AREA_STATUS_REQUEST,
                        getActivity(), new HttpResponseInterface() {
                    @Override
                    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {

                    }
                }, ServerOperationUtil.SIMPLE_DIALOG, false,
                        getActivity().getString(R.string.please_wait_dialog_msg))
                        .execute();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void getDrawingPointsHistory(){
        if(drawingTask != null) {
            //if(isDrawingDone && isSocketConnected)
            drawingTask.getRoomHistory(MathFriendzyHelper.SERVER_PREFIX,
                    selectedPlayerData.getPlayerid());
        }else{
            isDrawingFromChatHistoryRecieve = true;
        }
    }

    private void drawPointFromChatHistory(JSONObject jsonObject){
        try {
            GetDrawingPointForChatResponse response = new GetDrawingPointForChatResponse();
            JSONArray jsonArray = jsonObject.getJSONArray("room_history");
            ArrayList<GetDrawingPointForChatResponse> dataList = new ArrayList<GetDrawingPointForChatResponse>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonDataObj = jsonArray.getJSONObject(i);
                GetDrawingPointForChatResponse data = new GetDrawingPointForChatResponse();
                data.setPlayerId(jsonDataObj.getString("user_id"));
                String devicePoints = jsonDataObj.getString("history");
                if(devicePoints.contains(SIZE)){
                    String deviceSize = devicePoints.split(";")[0];
                    data.setDeviceSize(deviceSize.replace(SIZE , ""));
                    //data.setDrawPoints(devicePoints.replaceAll((deviceSize + ";") , ""));
                    data.setDrawPoints(devicePoints);
                }else{
                    data.setDrawPoints(devicePoints);
                }
                dataList.add(data);
            }
            response.setDrawingList(dataList);
            isDrawingFromChatHistoryRecieve = true;
            this.drawingPointsFromChatRoomHistory = response;
            drawPointFromServer();
            //this.drawPointFromHistory(response);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private String getMyDrawingPointsFromRoomChatHistory(boolean isForMyPoints){
        StringBuilder stringBuilder = new StringBuilder("");
        if(drawingPointsFromChatRoomHistory != null){
            ArrayList<GetDrawingPointForChatResponse> drawingList
                    = drawingPointsFromChatRoomHistory.getDrawingList();
            for(int i = 0 ; i < drawingList.size() ; i ++ ){
                GetDrawingPointForChatResponse points = drawingList.get(i);
                String[] array = points.getDrawPoints().split(";");
                String firstString = array[1];
                String lastString = array[array.length - 1];
                if(!lastString.contains(END)){
                    points.setDrawPoints(points.getDrawPoints() + (END + lastString + ";"));
                    points.setDrawPoints((points.getDrawPoints() + (BEGIN + lastString + ";")));
                }
                if(!firstString.contains(BEGIN)){
                    points.setDrawPoints((BEGIN + firstString + ";") + points.getDrawPoints());
                }

                if(isForMyPoints){
                    if (selectedPlayerData.getPlayerid()
                            .equalsIgnoreCase(points.getPlayerId())) {
                        stringBuilder.append(points.getDrawPoints());
                    }
                }else{
                    if (!selectedPlayerData.getPlayerid()
                            .equalsIgnoreCase(points.getPlayerId())) {
                        stringBuilder.append(points.getDrawPoints());
                    }
                }
            }
        }
        return stringBuilder.toString();
    }

    //Online Status via socket
    private void createGlobalRoomObjectOrGetUserStatus() {
        if(MyGlobalWebSocket.getInstance() != null
                && MyGlobalWebSocket.getInstance().isSocketConnected()){
            MyGlobalWebSocket.getInstance().initializeCallback(globalRoomClientCallBack);
            if (isChatAlreadyConnected) {
                MyGlobalWebSocket.getInstance().getOnlineStatus(MathFriendzyHelper.SERVER_PREFIX,
                        this.getOpponentId());
            }
        }else{
            UserPlayerDto selectedPlayerData = MathFriendzyHelper.getSelectedPlayer(getActivity());
            if(selectedPlayerData != null){
                MyGlobalWebSocket.getInstance(getActivity() , globalRoomClientCallBack
                        , 0 , selectedPlayerData.getPlayerid());
            }
        }
    }

    private JSONArray getOpponentId(){
        JSONArray jsonArray = new JSONArray();
        try{
            if (isTutor) {
                jsonArray.put(selectedTutor.getPlayerId());
            } else {
                jsonArray.put(getChatResponse.getTutorPid() + "");
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return jsonArray;
    }

    ClientCallback globalRoomClientCallBack = new ClientCallback() {
        @Override
        public void onConnected() {
            if (isChatAlreadyConnected) {
                MyGlobalWebSocket.getInstance().getOnlineStatus(MathFriendzyHelper.SERVER_PREFIX, getOpponentId());
            }
        }

        @Override
        public void onConnectionFail() {

        }

        @Override
        public void onMessageSent(boolean isSuccess) {

        }

        @Override
        public void onMessageRecieved(String message) {
            try{
                CommonUtils.printLog(TAG, "Message Recieve from socket " + message);
                GetActiveInActiveStatusOfStudentForTutorResponse response =
                        MathFriendzyHelper.getActiveInActiveResponse(message);
                if(response != null) {
                    if (callback != null) {
                        if(response.getList().size() > 0) {
                            callback.onActiveInActiveStatus(0);
                        }else{
                            callback.onActiveInActiveStatus(1);
                        }
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    };



    /**
     * Drawing points from the server
     *
     * @param
     */
    /*private void drawPointFromHistory(final GetDrawingPointForChatResponse response) {
        final ArrayList<GetDrawingPointForChatResponse> pointsList =
                response.getDrawingList();
        if(pointsList != null && pointsList.size() > 0) {
            this.drawPathForAndroidFromHistory(pointsList);
        }
    }*/

    /*private void drawPathForAndroidFromHistory(final ArrayList<GetDrawingPointForChatResponse> pointsList){
        AsyncTask<Void, Void, Void> myDrawingTask = new AsyncTask<Void, Void, Void>() {
            private DrawDeviceDimention myDrawingDimen = null;
            @Override
            protected void onPreExecute() {
                drawingLayouts.setVisibility(RelativeLayout.INVISIBLE);
                super.onPreExecute();
            }
            @Override
            protected Void doInBackground(Void... params) {
                ArrayList<GetDrawingPointForChatResponse> myPointsList
                        = getMyDrawingPointForHistory(pointsList);
                for(int i = 0 ; i < myPointsList.size() ; i ++ ) {
                    GetDrawingPointForChatResponse myDrawingPoints = myPointsList.get(i);
                    if (myDrawingPoints != null) {
                        myDrawingDimen = MathFriendzyHelper.getDrawDeviceDimention
                                (MathFriendzyHelper.getDeviceDimention(getActivity()),
                                        myDrawingPoints.getDeviceSize());
                        String[] points = myDrawingPoints.getDrawPoints().split(";");
                        drawPathList(mySideDrawingView, getPathList(points, myDrawingDimen), true);
                    }
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                if (isTutor) {
                    mySideDrawingView.setDrawColor(Color.BLUE);
                } else {
                    mySideDrawingView.setDrawColor(Color.BLACK);
                }
                mySideDrawingView.setEraseMode(false);
                //drawingLayouts.setVisibility(RelativeLayout.VISIBLE);
                myDrawingCompletedFromHistoty = true;
                if(myDrawingCompletedFromHistoty && otherDrawingCompletedFromHistry) {
                    *//*isDrawingDone = true;
                    hidePopUp();*//*
                    drawingLayouts.setVisibility(RelativeLayout.VISIBLE);
                    *//*setInitialTimerProgress();
                    startTimer();*//*
                }
                super.onPostExecute(aVoid);
            }
        };
        MathFriendzyHelper.executeTask(myDrawingTask);

        AsyncTask<Void, Void, Void> otherDrawingTask = new AsyncTask<Void, Void, Void>() {
            private DrawDeviceDimention otherDrawingDimen = null;
            @Override
            protected void onPreExecute() {
                drawingLayouts.setVisibility(RelativeLayout.INVISIBLE);
                super.onPreExecute();
            }
            @Override
            protected Void doInBackground(Void... params) {
                ArrayList<GetDrawingPointForChatResponse> myPointsList
                        = getOtherDrawingPointForHistory(pointsList);
                for(int i = 0 ; i < myPointsList.size() ; i ++ ) {
                    GetDrawingPointForChatResponse otherDrawingPoints = myPointsList.get(i);
                    if (otherDrawingPoints != null) {
                        otherDrawingDimen = MathFriendzyHelper.getDrawDeviceDimention
                                (MathFriendzyHelper.getDeviceDimention(getActivity()),
                                        otherDrawingPoints.getDeviceSize());
                        String[] points = otherDrawingPoints.getDrawPoints().split(";");
                        drawPathList(otherSideDrawingView, getPathList(points, otherDrawingDimen), false);
                    }
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                if (isTutor) {
                    otherSideDrawingView.setDrawColor(Color.BLACK);
                } else {
                    otherSideDrawingView.setDrawColor(Color.BLUE);
                }
                otherSideDrawingView.setEraseMode(false);
                otherDrawingCompletedFromHistry = true;
                //drawingLayouts.setVisibility(RelativeLayout.VISIBLE);
                if(myDrawingCompletedFromHistoty && otherDrawingCompletedFromHistry) {
                    *//*isDrawingDone = true;
                    hidePopUp();*//*
                    drawingLayouts.setVisibility(RelativeLayout.VISIBLE);
                    *//*setInitialTimerProgress();
                    startTimer();*//*
                }
                super.onPostExecute(aVoid);
            }
        };
        MathFriendzyHelper.executeTask(otherDrawingTask);
    }*/

    /*private ArrayList<GetDrawingPointForChatResponse> getMyDrawingPointForHistory
            (ArrayList<GetDrawingPointForChatResponse> pointsList){
        ArrayList<GetDrawingPointForChatResponse> myPointsList
                = new ArrayList<GetDrawingPointForChatResponse>();
        for (GetDrawingPointForChatResponse mPoint : pointsList) {
            if (selectedPlayerData.getPlayerid()
                    .equalsIgnoreCase(mPoint.getPlayerId())) {
                myPointsList.add(mPoint);
            }
        }
        return myPointsList;
    }


    private ArrayList<GetDrawingPointForChatResponse> getOtherDrawingPointForHistory
            (ArrayList<GetDrawingPointForChatResponse> pointsList){
        ArrayList<GetDrawingPointForChatResponse> myPointsList
                = new ArrayList<GetDrawingPointForChatResponse>();
        for (GetDrawingPointForChatResponse mPoint : pointsList) {
            if (!selectedPlayerData.getPlayerid()
                    .equalsIgnoreCase(mPoint.getPlayerId())) {
                myPointsList.add(mPoint);
            }
        }
        return myPointsList;
    }*/

    private void endbleDisableDrawing(boolean isDisable){
        if (mySideDrawingView != null)
            mySideDrawingView.setDrawMode(isDisable);
    }



    //updated code to update or refresh data when user comes onResume after press homebutton
    @Override
    public void onResume() {
        CommonUtils.printLog(TAG, "onResume");
        //this.doTheThingsOnResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        CommonUtils.printLog(TAG, "onPause");
        //isAppMinimize = true;
        //this.doTheThingOnPause();
        super.onPause();
    }


    //App Pause and resume change when user press home button
    public void pauseNormalTimer(){
        try {
            if (timer != null) {
                timer.stop();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void pausePaidTimer(){
        try {
            if (studentPaidTutoringTimer != null) {
                studentPaidTutoringTimer.stop();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void setNormalTimerProgress(int progress){
        try {
            if (timer != null) {
                timer.setProgress(progress);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void setPaidTimerProgress(int progress){
        try {
            if (studentPaidTutoringTimer != null) {
                studentPaidTutoringTimer.setProgress(progress);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public int getNormalTimerProgress(){
        try {
            if (timer != null) {
                return timer.getProgress();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    public int getPaidTimerProgress(){
        try {
            if (studentPaidTutoringTimer != null) {
                return studentPaidTutoringTimer.getProgress();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    public int getOfflineTimeSpent(){
        return offlineTimeSpent;
    }

    public int getOnLineTimeSpent(){
        return onLineTimeSpent;
    }

    public int getTimeSpentByStudentOrTutorBeforeTimeFinish(){
        return timeSpentByStudentOrTutorBeforeTimeFinish;
    }

    public void initializeTimeSpentByTutorOrStudentBeforeTimiFinish(int time){
        this.timeSpentByStudentOrTutorBeforeTimeFinish = time;
    }
    //End changes regarding App Pause and resume change when user press home button


    //Audio Call Changes
    private OOVOOSDK oovoosdk = null;
    private String startCall = "Start Call";
    private String endCall = "End Call";
    private String connecting = "Connecting...";
    private String disconnecting = "Disconnecting...";
    private final String SUFFIX_TEXT_FOR_AUDIO_CHAT_USER_ID = "math_id";
    private String deviceNotSupported = "Device not  support audio calling.";
    private String logToAudioFail = "Login to Audio Chat Fail.";
    private String audioChatFirstMsg = "Hi Joined";
    private String audioChatNotAuthorized = "Audio Chat Not Authorized.";
    private String youAreNotConnectedWithTutor = "You must establish a connection with the " +
            "tutor before you can make a voice call.";
    private String opponentOffline = "Your opponent is not currently on the tutor area. " +
            "Please wait for the online indicator to turn green before starting a voice call.";
    private String accept = "Accept";
    private String reject = "Reject";
    private String callingMsg = "Your opponent is calling...";
    private String opponentRejectCall = "Your opponent is not available to talk now. " +
            "Please try again later.";
    
    private void initVoiceChat(){
        oovoosdk = new OOVOOSDK();
        oovoosdk.initOOVOOSDKListener(oovoosdkListener);
    }

    private String getAudioChatUserId(){
        return selectedPlayerData.getPlayerid() + SUFFIX_TEXT_FOR_AUDIO_CHAT_USER_ID;
    }

    private void startCall(){
        if(oovoosdk.isDeviceSupported()){
            this.authorisedOovooClient();
        }else{
            MathFriendzyHelper.showWarningDialog(getActivity() , deviceNotSupported);
        }
    }

    private void authorisedOovooClient(){
        if(oovoosdk.isAuthorized()){
            this.login();
        }else{
            oovoosdk.authorizedSDK();
        }
    }

    private void login(){
        if(oovoosdk.isLoginUser()){
            if(oovoosdk.isAudioStarted()){
                leaveConference();
            }else{
                joinConference();
            }
        }else {
            oovoosdk.loginUser( this.getAudioChatUserId(), new LoginListener() {
                @Override
                public void onLogin(boolean loginStatus) {
                    if(loginStatus){
                        joinConference();
                    }else{
                        MathFriendzyHelper.showWarningDialog(getActivity() , logToAudioFail);
                    }
                }
            });
        }
    }

    private void joinConference(){
        oovoosdk.joinConference(chatRequestId + "", audioChatFirstMsg);
    }

    private void leaveConference(){
        oovoosdk.leave();
    }

    private void logoutAudioChatUser(){
        oovoosdk.logoutUser();
    }

    OOVOOSDKListener oovoosdkListener = new OOVOOSDKListener() {
        @Override
        public void onAuthorized(String message) {
            if(message.equalsIgnoreCase(OOVOOSDK.AUTHORIZED)) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        login();
                    }
                });
            }else{
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        MathFriendzyHelper.showWarningDialog(getActivity() , audioChatNotAuthorized);
                    }
                });
            }
        }

        @Override
        public void onLogin(boolean loginStatus) {
            /*if(loginStatus){
                oovoosdk.joinConference(chatRequestId + "", "Hi Joined");
            }*/
        }

        @Override
        public void onConferenceStateChange(AVChatListener.ConferenceState conferenceState,
                                            sdk_error sdk_error , boolean isSendNotification) {
            btnAudioCall.setEnabled(true);
            if(conferenceState == AVChatListener.ConferenceState.Joined) {
                //btnAudioCall.setText(endCall);
                btnAudioCall.setTag(endCall);
                //btnAudioCall.setBackgroundResource(R.drawable.end_call);
                if(isSendNotification)
                    sendMessage(MathFriendzyHelper.AUDIO_PREFIX + OOVOOSDK.CALLING);
            }else if(conferenceState == AVChatListener.ConferenceState.Disconnected) {
                //btnAudioCall.setText(startCall);
                btnAudioCall.setTag(startCall);
                btnAudioCall.setBackgroundResource(R.drawable.start_call);
                if(isSendNotification)
                    sendMessage(MathFriendzyHelper.AUDIO_PREFIX + OOVOOSDK.DISCONNECT);
            }
        }

        @Override
        public void onParticipantJoined(Participant participant, String s) {
            btnAudioCall.setBackgroundResource(R.drawable.end_call);
        }
    };


    public void startEndAudioCall() {
        if(chatRequestId == 0){
            MathFriendzyHelper.showWarningDialog(getActivity() , youAreNotConnectedWithTutor);
            return ;
        }

        if(!this.isEditingEnable) {
            this.showCantEditDialog();
            return ;
        }

        if(!this.isOpponentOnline){
            this.setNotificationWhenTutorAreaUpdated();
            MathFriendzyHelper.showWarningDialog(getActivity() , opponentOffline);
            return ;
        }

        btnAudioCall.setEnabled(false);
        if(btnAudioCall.getTag().toString().equalsIgnoreCase(startCall)) {
            btnAudioCall.setTag(connecting);
            btnAudioCall.setBackgroundResource(R.drawable.audio_calling);
            //btnAudioCall.setText(connecting);
        }else if(btnAudioCall.getTag().toString().equalsIgnoreCase(endCall)) {
            //btnAudioCall.setText(disconnecting);
            btnAudioCall.setTag(disconnecting);
            btnAudioCall.setBackgroundResource(R.drawable.audio_disconnect_calling);
        }
        this.startCall();
    }

    private void releaseFromAudioChat(){
        if(oovoosdk.isLoginUser()) {
            if (oovoosdk.isAudioStarted()) {
                sendMessage(MathFriendzyHelper.AUDIO_PREFIX + OOVOOSDK.DISCONNECT);
            }
        }

        this.leaveConference();
        this.logoutAudioChatUser();
    }

    private void processAudioMessage(String message) {
        try {
            message = message.replaceAll(MathFriendzyHelper.AUDIO_PREFIX, "");
            if (message.equalsIgnoreCase(OOVOOSDK.CALLING)) {
                if(oovoosdk.isAudioStarted()){
                    return ;
                }
                MathFriendzyHelper.yesNoConfirmationDialog(getActivity(), callingMsg,
                        accept, reject, new YesNoListenerInterface() {
                            @Override
                            public void onYes() {
                                startEndAudioCall();
                            }

                            @Override
                            public void onNo() {
                                sendMessage(MathFriendzyHelper.AUDIO_PREFIX + OOVOOSDK.REJECT);
                            }
                        });
            } else if (message.equalsIgnoreCase(OOVOOSDK.DISCONNECT)) {
                if(oovoosdk.isAudioStarted()){
                    leaveConference();
                    return ;
                }
            }else if (message.equalsIgnoreCase(OOVOOSDK.REJECT)) {
                if(oovoosdk.isAudioStarted()){
                    leaveConference();
                    MathFriendzyHelper.showWarningDialog(getActivity() , opponentRejectCall);
                    return ;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void initializeAudioCallButton(Button btnAudioCall){
        if(this.btnAudioCall == null){
            this.btnAudioCall = btnAudioCall;
            this.btnAudioCall.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    startEndAudioCall();
                }
            });
        }
    }
}