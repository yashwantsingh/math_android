package com.mathfriendzy.controller.tutor;

import com.mathfriendzy.model.tutor.ChatMessageModel;

import java.util.ArrayList;

/**
 * Created by root on 30/12/15.
 */
public interface ChatMessageCallback {
    void onMessages(ArrayList<ChatMessageModel> messages);
}
