package com.mathfriendzy.controller.tutor;

import com.mathfriendzy.model.helpastudent.GetPlayerNeedHelpForTutorResponse;
import com.mathfriendzy.model.homework.CustomePlayerAns;
import com.mathfriendzy.model.homework.checkhomework.GetDetailOfHomeworkWithCustomeResponse;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.tutor.FindTutorResponse;

import java.io.Serializable;

public class TutorSessionBundleArgument implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    private CustomePlayerAns playerAns = null;
    private FindTutorResponse findTutorDetail = null;
    private int chatRequestedId = 0;

    public CustomePlayerAns getPlayerAns() {
        return playerAns;
    }
    public void setPlayerAns(CustomePlayerAns playerAns) {
        this.playerAns = playerAns;
    }

    public FindTutorResponse getFindTutorDetail() {
        return findTutorDetail;
    }

    //for homework data
    private String hwId;
    private String customeHwId;
    private String quesion;

    //for tutor
    private boolean isTutor = false;
    private String chatDialogId = null;
    private int isAnonymous = 0;

    //change for professional tutoring
    private boolean isForProfessionalTutoring;
    //end change for professional tutoring

    //for drawing chat
    GetPlayerNeedHelpForTutorResponse selectedTutor = null;

    //for image selection
    private String homeworkSheetName = null;

    //for check home work
    private boolean isWorkAreaForstudent;
    private GetDetailOfHomeworkWithCustomeResponse studentDetail = null;

    private boolean isShowTutorTabToTeacher;

    public void setFindTutorDetail(FindTutorResponse findTutorDetail) {
        this.findTutorDetail = findTutorDetail;
    }

    private UserPlayerDto playerFromManageTutor = null;

    //changes for professional tutoring
    private String tutorSessionTitle = null;
    private String paidSession = null;
    private int studentPurchaseTime = 0;
    //end changes for professional tutoring
    public int getChatRequestedId() {
        return chatRequestedId;
    }

    public void setChatRequestedId(int chatRequestedId) {
        this.chatRequestedId = chatRequestedId;
    }


    public String getHwId() {
        return hwId;
    }

    public void setHwId(String hwId) {
        this.hwId = hwId;
    }

    public String getCustomeHwId() {
        return customeHwId;
    }

    public void setCustomeHwId(String customeHwId) {
        this.customeHwId = customeHwId;
    }

    public String getQuesion() {
        return quesion;
    }

    public void setQuesion(String quesion) {
        this.quesion = quesion;
    }

    public boolean isTutor() {
        return isTutor;
    }

    public void setTutor(boolean isTutor) {
        this.isTutor = isTutor;
    }

    public String getChatDialogId() {
        return chatDialogId;
    }

    public void setChatDialogId(String chatDialogId) {
        this.chatDialogId = chatDialogId;
    }

    public int isAnonymous() {
        return isAnonymous;
    }

    public void setAnonymous(int isAnonymous) {
        this.isAnonymous = isAnonymous;
    }

    public GetPlayerNeedHelpForTutorResponse getSelectedTutor() {
        return selectedTutor;
    }

    public void setSelectedTutor(GetPlayerNeedHelpForTutorResponse selectedTutor) {
        this.selectedTutor = selectedTutor;
    }

    public String getHomeworkSheetName() {
        return homeworkSheetName;
    }

    public void setHomeworkSheetName(String homeworkSheetName) {
        this.homeworkSheetName = homeworkSheetName;
    }

    public boolean isWorkAreaForstudent() {
        return isWorkAreaForstudent;
    }

    public void setWorkAreaForstudent(boolean isWorkAreaForstudent) {
        this.isWorkAreaForstudent = isWorkAreaForstudent;
    }

    public GetDetailOfHomeworkWithCustomeResponse getStudentDetail() {
        return studentDetail;
    }

    public void setStudentDetail(GetDetailOfHomeworkWithCustomeResponse studentDetail) {
        this.studentDetail = studentDetail;
    }

    public boolean isShowTutorTabToTeacher() {
        return isShowTutorTabToTeacher;
    }

    public void setShowTutorTabToTeacher(boolean isShowTutorTabToTeacher) {
        this.isShowTutorTabToTeacher = isShowTutorTabToTeacher;
    }

    public UserPlayerDto getPlayerFromManageTutor() {
        return playerFromManageTutor;
    }

    public void setPlayerFromManageTutor(UserPlayerDto playerFromManageTutor) {
        this.playerFromManageTutor = playerFromManageTutor;
    }

    public boolean isForProfessionalTutoring() {
        return isForProfessionalTutoring;
    }

    public void setForProfessionalTutoring(boolean isForProfessionalTutoring) {
        this.isForProfessionalTutoring = isForProfessionalTutoring;
    }

    public String getTutorSessionTitle() {
        return tutorSessionTitle;
    }

    public void setTutorSessionTitle(String tutorSessionTitle) {
        this.tutorSessionTitle = tutorSessionTitle;
    }

    public String getPaidSession() {
        return paidSession;
    }

    public void setPaidSession(String paidSession) {
        this.paidSession = paidSession;
    }

    public int getStudentPurchaseTime() {
        return studentPurchaseTime;
    }

    public void setStudentPurchaseTime(int studentPurchaseTime) {
        this.studentPurchaseTime = studentPurchaseTime;
    }
}
