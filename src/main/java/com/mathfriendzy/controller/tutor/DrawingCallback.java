package com.mathfriendzy.controller.tutor;

public interface DrawingCallback {
	void onStart(float x, float y, float ex, float ey);
	void onMove(float x, float y, float ex, float ey);
	void onUp(float x, float y);
    void disableDrawing();
}
