package com.mathfriendzy.controller.tutor;

import android.graphics.Path;

/**
 * Created by root on 5/8/15.
 */
public class MyPath {
    private Path path;
    private boolean eraseMode;

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    public boolean isEraseMode() {
        return eraseMode;
    }

    public void setEraseMode(boolean eraseMode) {
        this.eraseMode = eraseMode;
    }
}
