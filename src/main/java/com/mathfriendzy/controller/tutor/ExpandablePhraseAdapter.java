package com.mathfriendzy.controller.tutor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.tutor.PhraseCatagory;
import com.mathfriendzy.model.tutor.PhraseSubCatagory;

import java.util.ArrayList;

/**
 * Created by root on 4/11/15.
 */
public class ExpandablePhraseAdapter extends BaseExpandableListAdapter {

    ArrayList<PhraseCatagory> phraseCatList = null;
    private Context context = null;
    private LayoutInflater mInflater = null;
    private GroupViewHolder gVHolder = null;
    private ChildViewHolder cVHolder = null;
    private boolean isPaidSession = false;

    public ExpandablePhraseAdapter(Context context , ArrayList<PhraseCatagory> phraseCatList
     , boolean isPaidSession){
        this.phraseCatList = phraseCatList;
        this.context = context;
        mInflater = LayoutInflater.from(context);
        this.isPaidSession = isPaidSession;
    }


    @Override
    public int getGroupCount() {
        return phraseCatList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return phraseCatList.get(groupPosition).getSubCatList().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return phraseCatList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return phraseCatList.get(groupPosition).getSubCatList().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
                             ViewGroup parent) {
        if(convertView == null){
            gVHolder = new GroupViewHolder();
            convertView = mInflater.inflate(R.layout.phrase_list_cat_layout , null);
            gVHolder.txtCatId = (TextView)  convertView.findViewById(R.id.txtCatId);
            convertView.setTag(gVHolder);
        }else {
            gVHolder = (GroupViewHolder) convertView.getTag();
        }
        gVHolder.txtCatId.setText(phraseCatList.get(groupPosition).getCatName());
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {
        if(convertView == null){
            cVHolder = new ChildViewHolder();
            convertView = mInflater.inflate(R.layout.phrase_list_sub_cat_layout , null);
            cVHolder.txtCatId = (TextView)  convertView.findViewById(R.id.txtCatId);
            convertView.setTag(cVHolder);
        }else {
            cVHolder = (ChildViewHolder) convertView.getTag();
        }

        PhraseSubCatagory phraseSubCatagory = phraseCatList.get(groupPosition)
                .getSubCatList().get(childPosition);
        cVHolder.txtCatId.setText(phraseSubCatagory.getSubCatName());
        if(phraseSubCatagory.getIsSelectable() == MathFriendzyHelper.NO){
            cVHolder.txtCatId.setTextColor(context.getResources().getColor(R.color.SKY_BLUE));
        }else{
            cVHolder.txtCatId.setTextColor(context.getResources().getColor(R.color.BLACK));
        }
        this.setVisibilityOfPhrase(cVHolder , phraseSubCatagory);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private class GroupViewHolder{
        TextView txtCatId = null;
    }

    private class ChildViewHolder{
        TextView txtCatId = null;
    }

    private void setVisibilityOfPhrase(ChildViewHolder cVHolder , PhraseSubCatagory phraseSubCatagory){
        if(!this.isPaidSession){
            if(phraseSubCatagory.getForFreeTutor() == MathFriendzyHelper.YES){
                cVHolder.txtCatId.setVisibility(TextView.VISIBLE);
            }else{
                cVHolder.txtCatId.setVisibility(TextView.GONE);
            }
        }
    }
}
