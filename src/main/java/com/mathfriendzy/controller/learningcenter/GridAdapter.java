package com.mathfriendzy.controller.learningcenter;

import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.mathfriendzy.R;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;

public class GridAdapter extends BaseAdapter 
{
	private LayoutInflater mInflater 		   = null;
	private Context context 				   = null;
	private int resource 					   = 0;
	private ViewHolder vholder 				   = null;
	private ArrayList<LearningCenterTransferObj> euationList       = null;
	private ArrayList<ImageView> imgChekedList					   = null;
	public static ArrayList<Boolean> isChecked  			 = null;
	private ArrayList<ImageButton> imgListForDefaultCategory = null;
	
	public GridAdapter(Context context,  int resource , ArrayList<LearningCenterTransferObj> equationList,
			ArrayList<Integer> defaultCategoryIdList)

	{
		this.resource 			= resource;
		mInflater 				= LayoutInflater.from(context);
		this.context 			= context;

		imgListForDefaultCategory = new ArrayList<ImageButton>();

		this.euationList  = equationList;

		imgChekedList = new ArrayList<ImageView>();
		isChecked     = new ArrayList<Boolean>();

		for(int i = 0 ; i < this.euationList.size() ; i ++ )
		{
			isChecked.add(false);
		}
	}

	@Override
	public int getCount() 
	{
		return euationList.size();
	}

	@Override
	public Object getItem(int arg0) 
	{
		return null;
	}

	@Override
	public long getItemId(int position) 
	{
		return 0;
	}

	@Override
	public View getView(final int index, View view, ViewGroup parent) 
	{	
		vholder 			   = new ViewHolder();
		if(view == null)
		{			
			view 				   = mInflater.inflate(resource, null);
			vholder.txtEquationType= (TextView) view.findViewById(R.id.txtEquationType);
			vholder.imgButton      = (ImageButton) view.findViewById(R.id.imgCheck);

			imgChekedList.add(vholder.imgButton);

			view.setTag(vholder);
		}
		else
		{
			vholder = (ViewHolder) view.getTag();
		}

		vholder.txtEquationType.setText(euationList.get(index).getMathOperationCategory());
				
		vholder.imgButton.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				if(isChecked.get(index).booleanValue())
				{
					imgChekedList.get(index).setBackgroundResource(R.drawable.mf_check_box_ipad);
					isChecked.set(index, false);
				}
				else
				{
					imgChekedList.get(index).setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
					isChecked.set(index, true);
				}
			}
		});

		return view;
	}

	/**
	 * static class for view which hole the static objects of views
	 * @author Yashwant Singh
	 *
	 */
	static class ViewHolder
	{
		TextView  txtEquationType ;
		ImageButton imgButton;
	}

}

