package com.mathfriendzy.controller.learningcenter;

import java.util.ArrayList;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mathfriendzy.R;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;

/**
 * Adapter for show player play problems
 * @author Yashwant Singh
 *
 */
public class SeeAnswerAdapter extends BaseAdapter
{
	private ArrayList<LearningCenterTransferObj> playEquationList = null;
	private ArrayList<String> userAnswerList 					  = null;

	private LayoutInflater mInflater 		   = null;
	private Context context 				   = null;
	private int resource 					   = 0;
	private ViewHolder vholder 				   = null;
	private ArrayList<Integer> srNoLits        = null;
	private ArrayList<RelativeLayout> problemLayoutList = null;
	
	SeeAnswerAdapter(Context context,  int resource , ArrayList<LearningCenterTransferObj> playEquationList , 
			ArrayList<String> userAnswerList)
	{
		this.resource 	= resource;
		mInflater 		= LayoutInflater.from(context);
		this.context 	= context;
		
		srNoLits = new ArrayList<Integer>();
		problemLayoutList = new ArrayList<RelativeLayout>();
		
		this.playEquationList 	= playEquationList;
		this.userAnswerList 	= userAnswerList;
		
		for( int i = 0 ; i < userAnswerList.size() ; i ++ )
		{
			srNoLits.add(i + 1);
			problemLayoutList.add(null);
		}
	}
	
	@Override
	public int getCount() 
	{
		return userAnswerList.size();
	}

	@Override
	public Object getItem(int position) 
	{		
		return null;
	}

	@Override
	public long getItemId(int position) 
	{
		return 0;
	}

	@Override
	public View getView(int index, View view, ViewGroup viewGroup) 
	{	
		if(view == null)
		{
			vholder 			   	= new ViewHolder();
			view 				  	= mInflater.inflate(resource, null);
			vholder.txtSrNo   		= (TextView)   view.findViewById(R.id.txtSrNumber);
			vholder.txtproblem   	= (TextView)   view.findViewById(R.id.txtProblem);
			vholder.txtUserAnswer	= (TextView)   view.findViewById(R.id.txtUserAnswer);
			vholder.problemLayout   = (RelativeLayout) view.findViewById(R.id.problemLayout);
					
			view.setTag(vholder);
		}
		else
		{
			vholder = (ViewHolder) view.getTag();
		}
			
		problemLayoutList.set(index, vholder.problemLayout);
	
		vholder.txtSrNo.setText(srNoLits.get(index).intValue() + ".");
		
		vholder.txtproblem.setText(playEquationList.get(index).getNumber1Str() + " " 
				+ playEquationList.get(index).getOperator() + " "
				+ playEquationList.get(index).getNumber2Str() + " = " 
				+ playEquationList.get(index).getProductStr());
				
		
		vholder.txtUserAnswer.setText(userAnswerList.get(index));
		
		if(! userAnswerList.get(index).equals(playEquationList.get(index).getProductStr())){
			problemLayoutList.get(index).setBackgroundColor(Color.parseColor("#DA9284"));
		}
		else{
			problemLayoutList.get(index).setBackgroundResource(0);
		}
		
		return view;
	}
	
	/**
	 * static class for view which hole the static objects of views
	 * @author Yashwant Singh
	 *
	 */
	static class ViewHolder
	{
		TextView    txtSrNo ;
		TextView    txtproblem;
		TextView    txtUserAnswer;
		RelativeLayout problemLayout;
	}
}
