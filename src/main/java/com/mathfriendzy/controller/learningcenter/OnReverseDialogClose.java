package com.mathfriendzy.controller.learningcenter;

public interface OnReverseDialogClose {
	void onClose();
}
