package com.mathfriendzy.controller.learningcenter;

import java.util.ArrayList;
import com.mathfriendzy.R;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * This Adapter set the equations to the Learning Center Choose Equation ListView 
 * @author Yashwant Singh
 *
 */
public class ChooseEquationAdapter extends BaseAdapter 
{
	private LayoutInflater mInflater 		   = null;
	private Context context 				   = null;
	private int resource 					   = 0;
	private ViewHolder vholder 				   = null;
	private ArrayList<LearningCenterTransferObj> euationList       = null;
	private ArrayList<Integer> defaultCategoryIdList               = null;
	private ArrayList<ImageView> imgChekedList					   = null;
	public static ArrayList<Boolean> isChecked  			 = null;
	private ArrayList<ImageButton> imgListForDefaultCategory = null;
	//private final int MAX_LENGTH = 4;

	public ChooseEquationAdapter(Context context,  int resource , ArrayList<LearningCenterTransferObj> equationList,
			ArrayList<Integer> defaultCategoryIdList)

	{
		this.resource 			= resource;
		mInflater 				= LayoutInflater.from(context);
		this.context 			= context;

		imgListForDefaultCategory = new ArrayList<ImageButton>();

		if(defaultCategoryIdList.size() > 0 )
		{
			euationList = new ArrayList<LearningCenterTransferObj>();
			
			LearningCenterTransferObj learningCenterObj = new LearningCenterTransferObj();

			learningCenterObj.setMathOperationCategory("mfCategoryDefault");

			learningCenterObj.setMathOperationCategoryId(0);
			this.euationList.add(learningCenterObj);

			for( int i  = 0 ; i < equationList.size() ; i ++ ) 
			{
				this.euationList.add(equationList.get(i));
			}
		}
		else
		{
			this.euationList  = equationList;
		}

		this.defaultCategoryIdList = defaultCategoryIdList;

		imgChekedList = new ArrayList<ImageView>();
		isChecked     = new ArrayList<Boolean>();

		for(int i = 0 ; i < this.euationList.size() ; i ++ )
		{
			isChecked.add(false);
		}
	}

	@Override
	public int getCount() 
	{
		return euationList.size();
	}

	@Override
	public Object getItem(int arg0) 
	{
		return null;
	}

	@Override
	public long getItemId(int position) 
	{
		return 0;
	}

	@Override
	public View getView(final int index, View view, ViewGroup parent) 
	{	
		vholder 			   = new ViewHolder();
		if(view == null)
		{			
			view 				   = mInflater.inflate(resource, null);
			vholder.txtEquationType= (TextView) view.findViewById(R.id.txtEquationType);
			vholder.imgButton      = (ImageButton) view.findViewById(R.id.imgCheck);

			imgChekedList.add(vholder.imgButton);

			view.setTag(vholder);
		}
		else
		{
			vholder = (ViewHolder) view.getTag();
		}

		if(euationList.get(index).getMathOperationCategoryId() == 0)
		{
			vholder.imgButton.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
			isChecked.set(index, true);
		}

		Translation transeletion = new Translation(context);
		transeletion.openConnection();
		vholder.txtEquationType.setText(transeletion.getTranselationTextByTextIdentifier(euationList.get(index).getMathOperationCategory()));
		transeletion.closeConnection();

		this.setBackGroundOfCheckImage(vholder.imgButton,euationList.get(index).getMathOperationCategoryId(),index);

		vholder.imgButton.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				if(euationList.get(index).getMathOperationCategoryId() == 0)
				{
					if(isChecked.get(index).booleanValue())
					{
						imgChekedList.get(index).setBackgroundResource(R.drawable.mf_check_box_ipad);
						isChecked.set(index, false);
						
						//changes for fraction
						for(int i = 1 ; i < imgChekedList.size() ; i++)
						{
							if(imgListForDefaultCategory.contains(imgChekedList.get(i)))
							{
								imgChekedList.get(i).setBackgroundResource(R.drawable.mf_check_box_ipad);
								isChecked.set(i, false);
							}
						}
					}
					else
					{
						imgChekedList.get(index).setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
						isChecked.set(index, true);

						for( int i = 0 ; i < defaultCategoryIdList.size() ; i ++ )
						{
							imgListForDefaultCategory.get(i).setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
							isChecked.set(index + i + 1, true);
						}
						
						//changes for fraction
						for(int i = 1 ; i < imgChekedList.size() ; i++)
						{
							if(! imgListForDefaultCategory.contains(imgChekedList.get(i)))
							{
								imgChekedList.get(i).setBackgroundResource(R.drawable.mf_check_box_ipad);
								isChecked.set(i, false);
							}
						}
					}
				}
				else
				{
					if(defaultCategoryIdList.size() > 0 )
					{
						imgChekedList.get(0).setBackgroundResource(R.drawable.mf_check_box_ipad);
						isChecked.set(0, false);
					}

					if(isChecked.get(index).booleanValue())
					{
						imgChekedList.get(index).setBackgroundResource(R.drawable.mf_check_box_ipad);
						isChecked.set(index, false);
					}
					else
					{
						imgChekedList.get(index).setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
						isChecked.set(index, true);
					}
				}
			}
		});

		return view;
	}


	private void setBackGroundOfCheckImage(ImageButton imgButton,int mathOperationCategoryId,int index) 
	{
		for ( int  i = 0  ; i <  defaultCategoryIdList.size() ; i ++ )
		{
			if(defaultCategoryIdList.get(i) == mathOperationCategoryId)
			{
				if(isChecked.get(index).booleanValue() == false)
				{
					isChecked.set(index, true);
					imgListForDefaultCategory.add(imgButton);
					imgButton.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
				}
				break;
			}
		}
	}

	/**
	 * static class for view which hole the static objects of views
	 * @author Yashwant Singh
	 *
	 */
	static class ViewHolder
	{
		TextView  txtEquationType ;
		ImageButton imgButton;
	}

}
