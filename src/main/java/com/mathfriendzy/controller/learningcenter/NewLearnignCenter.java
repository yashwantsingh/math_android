package com.mathfriendzy.controller.learningcenter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.controller.inapp.GetMoreCoins;
import com.mathfriendzy.controller.learningcenter.schoolcurriculum.LearningCenterSchoolCurriculumEquationSolve;
import com.mathfriendzy.controller.learningcenter.showbyccss.ShowByCCSSMainStandards;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.dawnloadimagesfromserver.DawnloadImagesFromserver;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.OnPurchaseDone;
import com.mathfriendzy.model.appunlock.GetAppUnlockStatusResponse;
import com.mathfriendzy.model.assessmenttest.AssessmentStandardDto;
import com.mathfriendzy.model.assessmenttest.AssessmentTestImpl;
import com.mathfriendzy.model.assessmenttest.AssessmentTestServerOperation;
import com.mathfriendzy.model.assessmenttest.CatagoriesDto;
import com.mathfriendzy.model.grade.Grade;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.CoinsFromServerObj;
import com.mathfriendzy.model.learningcenter.LearningCenteServerOperation;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.PlayerEquationLevelObj;
import com.mathfriendzy.model.player.temp.TempPlayerOperation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.CategoryListTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.LearnCenterTimeObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.LearnignCenterSchoolCurriculumServerOperation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.QuestionLoadedTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SchoolCurriculumLearnignCenterimpl;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SubCatergoryTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.TestDetails;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.UpdatedInfoTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.WordProblemLevelTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.WordProblemQuestionTransferObj;
import com.mathfriendzy.model.spanishchanges.SpanishChangesImpl;
import com.mathfriendzy.model.spanishchanges.SpanishServerOperation;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ICommonUtils;
import com.mathfriendzy.utils.MathVersions;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LEARNING_CENTER_BG_INFO;
import static com.mathfriendzy.utils.ICommonUtils.LEARNING_CENTER_NEW_MAIN_FLAG;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;
import static com.mathfriendzy.utils.ITextIds.BEGINNER;
import static com.mathfriendzy.utils.ITextIds.BRAINIAC;
import static com.mathfriendzy.utils.ITextIds.EXPERT;
import static com.mathfriendzy.utils.ITextIds.FLASH_CARD;
import static com.mathfriendzy.utils.ITextIds.INTERMEDIATE;
import static com.mathfriendzy.utils.ITextIds.MASTER;
import static com.mathfriendzy.utils.ITextIds.NOVICE;
import static com.mathfriendzy.utils.ITextIds.PRODIGY;
import static com.mathfriendzy.utils.ITextIds.PROFESSIONAL;
import static com.mathfriendzy.utils.ITextIds.ROOKIE;
import static com.mathfriendzy.utils.ITextIds.WHIZ;

/**
 * This class for new learnign center
 *
 * @author Yashwant Singh
 *
 */
public class NewLearnignCenter extends ActBaseClass implements OnClickListener{

    private TextView learningCenterTop 		= null;
    private TextView lblSolveEquation 		= null;
    private TextView lblSchoolCurriculum 	= null;
    private ImageView imgSolveEquation      = null;
    private ImageView imgSchoolCurriculum   = null;
    private TextView txtCategory 			= null;
    private TextView txtProgress 			= null;

    private RelativeLayout childLayout    = null;
    private RelativeLayout subchildLayout = null;//
    private RelativeLayout clickedLayout  = null;// this will hold the clicked layout

    private LinearLayout linearLayout = null;// layout where child and sub child added

    //for school curriculum
    private Spinner spinnerGrade 		= null;
    private RelativeLayout gradeLayout 	= null;
    private TextView txtGrade           = null;

    //private static boolean isClickedSolveEquation = true;

    private int clickedIndex = -1;
    private int appStatus    = 0;

    private String TAG = this.getClass().getSimpleName();

    private ArrayList<RelativeLayout> chaildlayoutList = null;
    private ArrayList<RelativeLayout> subchaildlayoutList = null;
    private ArrayList<ImageView>      expendedImageList= null;
    private ImageView expendedImage = null; // this will hold the expend current image object

    private ArrayList<LearningCenterTransferObj> laernignCenterFunctionsList1 = null;
    private ArrayList<LearningCenterTransferObj> laernignCenterFunctionsList  = null;
    private ArrayList<String> childCategoryList = null;
    private ArrayList<PlayerEquationLevelObj> playerDataList = null;
    private ArrayList<WordProblemLevelTransferObj> wordProblemLevelList = null;//for word problem level data

    //parent view
    private ImageView imgExpandImage;
    private ImageView imgMedalImage;
    private ImageView imgCategorySign;
    private TextView txtCategoryName;
    private TextView txtProgressDetail;

    //child view
    private TextView txtChildLevelName;
    private ImageView imgStars;
    private ImageView imgThumbImage;
    private ImageView imgVedioUrl;

    SharedPreferences sharedPreferences = null;

    private ArrayList<Integer> mathOperationIdList		= null;
    private final int DELAY_TIME = 1000;

    private boolean isTab = false;

    //for school curriculum
    //private static int selectedGrade = 1;

    //for setting grade value
    public static boolean isCallFromMainActivity = false;

    //changes for open selected categories
    private LinearLayout linearChildLayout ;

    //changes for open selected categories
    private boolean isFromMain = false;

    //added for show by CCSS
    private Button btnShowByCCSS = null;
    private TextView txtSubCategoryName = null;
    private int stdId 		= 0;
    private int subStdId 	= 0;
    private boolean isFromShowByCCSS = false;
    private ArrayList<CatagoriesDto> categoryInfoList = null;
    private boolean isSubCatOpen = false;
    private String subCatShortName = "";


    //for show timer
    private TextView lblShowTimer 	= null;
    private ImageView imgShowTimer  = null;


    //new change for math tutor plus
    private RelativeLayout layoutCheckBox = null;
    private RelativeLayout layoutSolveEquation = null;
    private RelativeLayout layoutSchrollCurriculum  = null;
    private RelativeLayout layoutShowTimer = null;


    //NewInApp Changes
    private boolean isNeedToRefreshUI = false;
    private ProgressDialog progressDialogForNewInApp = null;

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_learnign_center);

        if (LEARNING_CENTER_NEW_MAIN_FLAG)
            Log.e(TAG, "inside onCreate()");

        this.init();
        if(!CommonUtils.isInternetConnectionAvailable(this))
        {
            DialogGenerator dg = new DialogGenerator(this);
            Translation transeletion = new Translation(this);
            transeletion.openConnection();
            //changes when testing by client
            dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnected"));
            transeletion.closeConnection();
        }

        //added for show by CCSS
        categoryInfoList = new ArrayList<CatagoriesDto>();
        //end changes

        childCategoryList   = new ArrayList<String>();
        expendedImageList   = new ArrayList<ImageView>();
        chaildlayoutList    = new ArrayList<RelativeLayout>();
        subchaildlayoutList = new ArrayList<RelativeLayout>();

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        isTab = getResources().getBoolean(R.bool.isTablet);

        this.setwidgetsReferences();
        this.setTextFromTranslation();

        //added for show by CCSSS
        this.getIntentValues();
        //end changes

        if(isCallFromMainActivity){
            isCallFromMainActivity = false;
            this.setIsFromMainValue();
            SharedPreferences sharedPrefPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
            CommonUtils.selectedGrade = sharedPrefPlayerInfo.getInt("grade", 1);
            //changes for 1-8 grade
            if(CommonUtils.selectedGrade > 8)
                CommonUtils.selectedGrade = 8;
        }

        if(CommonUtils.isClickedSolveEquation)
            this.solveEquation();
        else{
            this.schoolCurriculum(CommonUtils.selectedGrade);
        }

        //for show timer or not
        this.setShowTimerCheck();
        //end changes

        this.setListeneronWidgets();

        //changes for word problem
        //new GetTimeLearnCenter().execute(null,null,null);

        if(CommonUtils.isInternetConnectionAvailable(this)){
            SharedPreferences learnCenterTimePreff = getSharedPreferences(ICommonUtils.LEARN_CENTER_TIME_PREFF, 0);
            if(learnCenterTimePreff.getLong(ICommonUtils.LEARN_CENTER_TIME, 0) == 0){
                new GetTimeLearnCenter().execute(null,null,null);
            }else{
                long remainTime = learnCenterTimePreff.getLong(ICommonUtils.LEARN_CENTER_TIME, 0);
                long callTime   = learnCenterTimePreff.getLong(ICommonUtils.LEARN_CENTER_TIME_API_CALL_TIME, 0);
                Date date = new Date();
                if(((date.getTime() - callTime) / 1000) > remainTime){
                    new GetTimeLearnCenter().execute(null,null,null);
                }
            }
        }

        this.setVisibilityOfLayoutBasedOnAppVersion();

        if (LEARNING_CENTER_NEW_MAIN_FLAG)
            Log.e(TAG, "outside onCreate()");
    }

    //New InAppp Changes
    private void init(){
        try{
            progressDialogForNewInApp = MathFriendzyHelper.getProgressDialog(this , "");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //added for show by CCSS
    private void getIntentValues(){
        stdId 		= this.getIntent().getIntExtra("stdId", 1);
        subStdId    = this.getIntent().getIntExtra("subStdId", 1);
        isFromShowByCCSS = this.getIntent().getBooleanExtra("isCallFromSubStandardGoForShowByCCSS", false);
        isSubCatOpen = this.getIntent().getBooleanExtra("isOpenCat", false);
        subCatShortName = this.getIntent().getStringExtra("subCatShortName");

        if(isFromShowByCCSS){
            txtSubCategoryName.setVisibility(TextView.VISIBLE);
            txtSubCategoryName.setTextColor(Color.parseColor("#31862A"));
            txtSubCategoryName.setText(subCatShortName);
            this.getWordAssessmentSubCategoriesInfo(stdId, subStdId);
        }
    }

    /**
     * This method get  word Assessment sub Categories info
     */
    private void getWordAssessmentSubCategoriesInfo(int stdId , int subStdId){
        AssessmentTestImpl implObj = new AssessmentTestImpl(this);
        implObj.openConnection();
        categoryInfoList = implObj.getSubCategoriesInfoByStandardIdAndSubStdId(stdId, subStdId);
        implObj.closeConnection();
    }

    /**
     * This method set the isFromMainValue to true
     */
    private void setIsFromMainValue(){
        //changes for open selected category
        isFromMain = true;
        //end changes
        isSubCatOpen = false;
    }

    /**
     * This method for solve equation category for new learning center
     */
    private void solveEquation(){

        CommonUtils.isClickedSolveEquation = true;
        imgSolveEquation.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
        imgSchoolCurriculum.setBackgroundResource(R.drawable.mf_check_box_ipad);
        gradeLayout.setVisibility(RelativeLayout.INVISIBLE);
        txtProgress.setVisibility(TextView.VISIBLE);

        //added for Show by CCSS
        this.setShowByCSSButtonVisibility(false);
        txtCategory.setVisibility(TextView.VISIBLE);
        txtSubCategoryName.setVisibility(TextView.GONE);
        //end changes

        this.getPlayerLevelData();
        this.getFunctionalList();
        this.setLayoutValue();
    }

    /**
     * This method for school curriculum category
     */
    private void schoolCurriculum(int grade) {
        CommonUtils.isClickedSolveEquation = false;
        imgSchoolCurriculum.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
        imgSolveEquation.setBackgroundResource(R.drawable.mf_check_box_ipad);
        gradeLayout.setVisibility(RelativeLayout.VISIBLE);
        txtProgress.setVisibility(TextView.INVISIBLE);

        //added for Show by CCSS
        this.setShowByCSSButtonVisibility(true);
        txtCategory.setVisibility(TextView.GONE);
        if(isFromShowByCCSS)
            txtSubCategoryName.setVisibility(TextView.VISIBLE);
        //end changes


        SharedPreferences sharedPrefPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
        this.getGrade(grade + "");
        String userId   = sharedPrefPlayerInfo.getString("userId", "");
        String playerId = sharedPrefPlayerInfo.getString("playerId", "");
        this.getWordProblemLevelData(userId, playerId);

        ArrayList<CategoryListTransferObj> categoryList = this.getCategories(grade);
        //Collections.sort(categoryList , new SortByCategoryName());
        this.setLayoutValueForSchoolCurriculum(categoryList);
    }


    /**
     * This method set the show time check
     */
    private void setShowTimerCheck(){
        if(CommonUtils.isShowTimer){
            imgShowTimer.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
        }else{
            imgShowTimer.setBackgroundResource(R.drawable.mf_check_box_ipad);
        }
    }

    /**
     * This method get categories detail from database according to the user grade
     * @param grade
     */
    private ArrayList<CategoryListTransferObj> getCategories(int grade){
        ArrayList<CategoryListTransferObj> categoryList = new ArrayList<CategoryListTransferObj>();
        //changes for Spanish
        if(CommonUtils.getUserLanguageCode(this) == CommonUtils.ENGLISH){
            SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
            schoolImpl.openConnection();
            categoryList = schoolImpl.getCategoriesForSchoolCurriculum(grade);
            schoolImpl.closeConnection();
        }else {
            SpanishChangesImpl schoolImpl = new SpanishChangesImpl(this);
            schoolImpl.openConn();
            categoryList = schoolImpl.getCategoriesForSchoolCurriculum(grade);
            schoolImpl.closeConn();
        }
        return categoryList;
    }

    /**
     * This method get the word problem data from database for school curriculum
     * @param
     * @param playerId
     */
    private void getWordProblemLevelData(String userId , String playerId){
        SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
        schoolImpl.openConnection();
        wordProblemLevelList = schoolImpl.getWordProblemLevelData(userId, playerId);
        schoolImpl.closeConnection();
    }

    /**
     * This method check for application is unlock or not
     * @param itemId
     * @param userId
     * @return
     */
    private int getApplicationUnLockStatus(int itemId,String userId){
        LearningCenterimpl learnignCenterImpl = new LearningCenterimpl(this);
        learnignCenterImpl.openConn();
        appStatus = learnignCenterImpl.getAppUnlockStatus(itemId,userId);
        learnignCenterImpl.closeConn();
        return appStatus;
    }


    /**
     * This method get player level data from database
     */
    private void getPlayerLevelData()
    {
        //int level = 0 ;
		/*SharedPreferences sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
		int operationId = sharedPreferences.getInt("operationId", 0); 
		 */
        SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
        LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
        learningCenterObj.openConn();
        playerDataList = learningCenterObj.getPlayerEquationLevelDataByPlayerId(sharedPreffPlayerInfo.getString("playerId", ""));
        learningCenterObj.closeConn();
    }

    /**
     * This method get function categories list from the database and set it to the list view
     */
    private void getFunctionalList()
    {
        laernignCenterFunctionsList = new ArrayList<LearningCenterTransferObj>();
        laernignCenterFunctionsList1 = new ArrayList<LearningCenterTransferObj>();
        LearningCenterimpl laerningCenter = new LearningCenterimpl(this);
        laerningCenter.openConn();
        laernignCenterFunctionsList = laerningCenter.getLearningCenterFunctions();
        laerningCenter.closeConn();

        Translation transeletion = new Translation(this);
        transeletion.openConnection();

        for(int i = 0; i < laernignCenterFunctionsList.size() ; i ++ )
        {
            LearningCenterTransferObj lc = new LearningCenterTransferObj();

            if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Fractions"))
                lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier("btnTitleFractions"));
            else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Decimals"))
                lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier("btnTitleDecimals"));
            else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Negative"))
                lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier("btnTitleNegatives"));
            else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Addition"))
                lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier("btnTitleAddition"));
            else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Subtraction"))
                lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier("btnTitleSubtraction"));
            else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Multiplication"))
                lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier("btnTitleMutiplication"));
            else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Division"))
                lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier("btnTitleDivision"));
            laernignCenterFunctionsList1.add(lc);
        }

        transeletion.closeConnection();
    }

    /**
     * This method set the widgets references from layout
     */
    private void setwidgetsReferences() {

        linearLayout = (LinearLayout) findViewById(R.id.learnignCenterLayout);

        learningCenterTop 	= (TextView) findViewById(R.id.learningCenterTop);
        lblSolveEquation 	= (TextView) findViewById(R.id.lblSolveEquation);
        lblSchoolCurriculum = (TextView) findViewById(R.id.lblSchoolCurriculum);
        txtCategory 		= (TextView) findViewById(R.id.txtCategory);
        txtProgress 		= (TextView) findViewById(R.id.txtProgress);

        imgSolveEquation 	= (ImageView) findViewById(R.id.imgSolveEquation);
        imgSchoolCurriculum = (ImageView) findViewById(R.id.imgSchoolCurriculum);

        //for school curriculum
        txtGrade     = (TextView) findViewById(R.id.txtGrade);
        spinnerGrade = (Spinner) findViewById(R.id.spinnerGrade);
        gradeLayout  = (RelativeLayout) findViewById(R.id.gradeLayout);

        //added for show by CCSS
        btnShowByCCSS 		= (Button) findViewById(R.id.btnShowByCCSS);
        txtSubCategoryName 	= (TextView) findViewById(R.id.txtSubCategoryName);

        //for show timer
        lblShowTimer        = (TextView) findViewById(R.id.lblShowTimer);
        imgShowTimer        = (ImageView) findViewById(R.id.imgShowTimer);

        //for Naw App Changes
        this.setWidgetsReferesncesForNewAppChanges();
    }

    /**
     * @Descritpion getGradeData from database
     * @param
     * @param gradeValue
     */
    private void getGrade(String gradeValue){
        Grade gradeObj = new Grade();
        ArrayList<String> gradeList = gradeObj.getGradeList(this);
        this.setGradeAdapter(gradeValue , gradeList);
    }

    /**
     * @Description set Grade data to adapter
     * @param
     * @param gradeValue
     * @param gradeList
     */
    private void setGradeAdapter(final String gradeValue, ArrayList<String> gradeList)
    {
        gradeList.subList(12, 13).clear();//New change on 02 feb 2016

        ArrayAdapter<String> gradeAdapter = new ArrayAdapter<String>(this, R.layout.spinner_textview_layout,gradeList);
        gradeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGrade.setAdapter(gradeAdapter);

        spinnerGrade.setSelection(gradeAdapter.getPosition(gradeValue));
        spinnerGrade.setOnItemSelectedListener(new OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {
				/*((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
				if(!isTab)
					((TextView) parent.getChildAt(0)).setTextSize(10);
				((TextView) parent.getChildAt(0)).setTypeface(null, Typeface.BOLD);*/

                //only for Spanish Changes
                if(CommonUtils.selectedGrade != Integer.parseInt(spinnerGrade.getSelectedItem().toString())){
                    CommonUtils.isOneTimeClick = true;
                }
                //end changes

                isSubCatOpen = false;
                setIsFromMainValue();

                CommonUtils.selectedGrade = Integer.parseInt(spinnerGrade.getSelectedItem().toString());

                if(!gradeValue.equals(spinnerGrade.getSelectedItem().toString()))
                    loadQuestionFromServer(Integer.parseInt(spinnerGrade.getSelectedItem().toString()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

    }

    /**
     * This method call when user click on grade
     */
    private void loadQuestionFromServer(final int selectedGrade){

        //changes for Spanish
        if(CommonUtils.getUserLanguageCode(this) == CommonUtils.ENGLISH){

            SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl =
                    new SchoolCurriculumLearnignCenterimpl(NewLearnignCenter.this);
            schooloCurriculumImpl.openConnection();
            boolean isQuestionLoaded = schooloCurriculumImpl.isQuestionLeaded(selectedGrade);
            schooloCurriculumImpl.closeConnection();

            if(isQuestionLoaded){
                schooloCurriculumImpl = new SchoolCurriculumLearnignCenterimpl(NewLearnignCenter.this);
                schooloCurriculumImpl.openConnection();
                String updatedDateFromDatabase = schooloCurriculumImpl.getUpdatedDate(selectedGrade);

                if(CommonUtils.isUpdateDateChange(updatedDateFromDatabase,selectedGrade,
                        MainActivity.worlProblemLevelDetail))
                {
                    String serverUpdatedDate = CommonUtils.getServerDate(selectedGrade ,
                            MainActivity.worlProblemLevelDetail);
                    schooloCurriculumImpl.updateNewUpdatedDateFromServer(selectedGrade, serverUpdatedDate);
                    schooloCurriculumImpl.closeConnection();

                    if(CommonUtils.isInternetConnectionAvailable(NewLearnignCenter.this)){
                        new GetWordProblemQuestion(selectedGrade , NewLearnignCenter.this)
                                .execute(null,null,null);
                    }else{
                        DialogGenerator dg = new DialogGenerator(this);
                        Translation transeletion = new Translation(this);
                        transeletion.openConnection();
                        dg.generateWarningDialog(transeletion.
                                getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
                        transeletion.closeConnection();
                    }
                }
                else{
                    schooloCurriculumImpl.closeConnection();
                    schoolCurriculum(selectedGrade);
                }
            }
            else
            {
                if(CommonUtils.isInternetConnectionAvailable(NewLearnignCenter.this)){
					/*new GetWordProblemQuestion(selectedGrade , NewLearnignCenter.this)
					.execute(null,null,null);*/
                    MathFriendzyHelper.downLoadQuestionForWordProblem(this , selectedGrade ,
                            new HttpServerRequest() {
                                @Override
                                public void onRequestComplete() {
                                    schoolCurriculum(selectedGrade);
                                }
                            });
                }else{
                    DialogGenerator dg = new DialogGenerator(this);
                    Translation transeletion = new Translation(this);
                    transeletion.openConnection();
                    dg.generateWarningDialog(transeletion.
                            getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
                    transeletion.closeConnection();
                }
            }
        }else{
            this.callWhenUserLanguageIsSpanish(this , CommonUtils.SPANISH , selectedGrade);
        }
    }

    //changes for Spanish
    /**
     * This method call when user language is Spanish
     * This method for downloading question from server for Spanish Language
     */
    private void callWhenUserLanguageIsSpanish(Context context , int lang , int grade){
        try {
            SpanishChangesImpl spanishImplObj = new SpanishChangesImpl(context);
            spanishImplObj.openConn();
            //CommonUtils.CheckWordProblemSpanishTable(context);
            boolean isSpanishQuestionLoaded = spanishImplObj.isQuestionLeaded(grade);
            if(isSpanishQuestionLoaded){
                if(CommonUtils.isInternetConnectionAvailable(NewLearnignCenter.this)){
                    new GetWordQuestionsUpdateDatesLang(lang , context , false , grade
                            ,1).execute(null,null,null);
                }else{
                    schoolCurriculum(grade);
                }
            }else{
                if(CommonUtils.isInternetConnectionAvailable(NewLearnignCenter.this)){
                    ArrayList<UpdatedInfoTransferObj> updatedList =
                            new GetWordQuestionsUpdateDatesLang(lang , context , true , grade
                                    ,1).execute(null,null,null).get();
                    spanishImplObj.insertIntoWordProblemUpdateDetails(updatedList);
                    //Log.e(TAG, "Question not loaded");
                    new GetWordProblemQuestion(grade , context)
                            .execute(null,null,null);
                }else{
                    DialogGenerator dg = new DialogGenerator(this);
                    Translation transeletion = new Translation(this);
                    transeletion.openConnection();
                    dg.generateWarningDialog(transeletion.
                            getTranselationTextByTextIdentifier
                                    ("alertMsgYouAreNotConnectedToTheInternet"));
                    transeletion.closeConnection();
                }
            }
            spanishImplObj.closeConn();

        } catch (InterruptedException e) {
            Log.e(TAG, "inside callWhenUserLanguageIsSpanish " + e.toString());
        } catch (ExecutionException e) {
            Log.e(TAG, "inside callWhenUserLanguageIsSpanish " + e.toString());
        }
    }
    //end changes

    /**
     * This method set text from translation
     */
    private void setTextFromTranslation() {
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        learningCenterTop.setText(transeletion.getTranselationTextByTextIdentifier("mfTitleHomeScreen"));
        lblSolveEquation.setText(transeletion.getTranselationTextByTextIdentifier("lblSolveEquations"));
        lblSchoolCurriculum.setText(transeletion.getTranselationTextByTextIdentifier("lblSchoolsCurriculum"));
        txtCategory.setText(transeletion.getTranselationTextByTextIdentifier("mfLblCategorySettings"));
        txtProgress.setText(transeletion.getTranselationTextByTextIdentifier("lblProgress"));
        txtGrade.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade") + ":");


        //added for show by CCSS
        btnShowByCCSS.setText(transeletion.getTranselationTextByTextIdentifier("lblShowByCcss"));

        //make child list multilanguage
        childCategoryList.add(transeletion.getTranselationTextByTextIdentifier(FLASH_CARD));
        childCategoryList.add(transeletion.getTranselationTextByTextIdentifier(BEGINNER));
        childCategoryList.add(transeletion.getTranselationTextByTextIdentifier(NOVICE));
        childCategoryList.add(transeletion.getTranselationTextByTextIdentifier(ROOKIE));
        childCategoryList.add(transeletion.getTranselationTextByTextIdentifier(INTERMEDIATE));
        childCategoryList.add(transeletion.getTranselationTextByTextIdentifier(PROFESSIONAL));
        childCategoryList.add(transeletion.getTranselationTextByTextIdentifier(WHIZ));
        childCategoryList.add(transeletion.getTranselationTextByTextIdentifier(EXPERT));
        childCategoryList.add(transeletion.getTranselationTextByTextIdentifier(MASTER));
        childCategoryList.add(transeletion.getTranselationTextByTextIdentifier(PRODIGY));
        childCategoryList.add(transeletion.getTranselationTextByTextIdentifier(BRAINIAC));

        //for show timer
        lblShowTimer.setText(transeletion.getTranselationTextByTextIdentifier("lblShowTimer"));

        transeletion.closeConnection();
    }

    /**
     * This method set the dynamic value to the layout
     */
    private void setLayoutValue() {

        linearLayout.removeAllViews();// for restarting when click on back press in learning center equation solve with timer
        chaildlayoutList    = new ArrayList<RelativeLayout>();
        expendedImageList   = new ArrayList<ImageView>();
        clickedLayout = null;
        clickedIndex = -1;
        expendedImage = null;

        for (int i = 0 ; i < laernignCenterFunctionsList.size() ; i++) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
            childLayout = (RelativeLayout) inflater.inflate(R.layout.new_learning_center_layout, null);

            imgExpandImage 		= (ImageView) childLayout.findViewById(R.id.imgExpand);
            imgCategorySign 	= (ImageView) childLayout.findViewById(R.id.imgSign);
            txtCategoryName 	= (TextView)  childLayout.findViewById(R.id.txtCategoryName);
            txtProgressDetail 	= (TextView)  childLayout.findViewById(R.id.txtProgress);
            imgMedalImage       = (ImageView) childLayout.findViewById(R.id.imgMedal);

            if(isTab)
                imgCategorySign.setBackgroundResource(getResources().getIdentifier
                        ("tab_mf_" + laernignCenterFunctionsList.get(i).getLearningCenterOperation().toLowerCase().replace(" ","_")+"_sign", "drawable",
                                getPackageName()));
            else
                imgCategorySign.setBackgroundResource(getResources().getIdentifier
                        ("mf_" + laernignCenterFunctionsList.get(i).getLearningCenterOperation().toLowerCase().replace(" ","_")+"_sign", "drawable",
                                getPackageName()));

            this.setProgress(txtProgressDetail, i , imgMedalImage);
            txtCategoryName.setText(laernignCenterFunctionsList1.get(i).getLearningCenterOperation());

            linearLayout.addView(childLayout);
            expendedImageList.add(imgExpandImage);
            chaildlayoutList.add(childLayout);

            childLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

					/*LinearLayout linearChildLayout = new LinearLayout(NewLearnignCenter.this);
					linearChildLayout.setOrientation(LinearLayout.VERTICAL);*/
                    //changes for open selected categories
                    linearChildLayout = new LinearLayout(NewLearnignCenter.this);
                    linearChildLayout.setOrientation(LinearLayout.VERTICAL);
                    //end changes

                    for (int i = 0; i < chaildlayoutList.size(); i++) {
                        if (v == chaildlayoutList.get(i)) {

                            if (clickedLayout != chaildlayoutList.get(i)) {

                                if(clickedIndex != -1)
                                    linearLayout.removeViewAt(clickedIndex + 1);

                                if(expendedImage != null){
                                    if(isTab)
                                        expendedImage.setBackgroundResource(R.drawable.tablet_expand_button);// set expended button image when click on other
                                    else
                                        expendedImage.setBackgroundResource(R.drawable.expand_button);// set expended button image when click on other
                                }

                                //changes for open selected categories
                                CommonUtils.selectedIndexForOpenCategory = i;
                                //end changes

                                linearChildLayout = addChildLayout(i);

                                linearLayout.addView(linearChildLayout, i + 1);
                                if(isTab)
                                    expendedImageList.get(i).setBackgroundResource(R.drawable.tablet_tab_expand_button);//set tab expend background image
                                else
                                    expendedImageList.get(i).setBackgroundResource(R.drawable.tab_expand_button);//set tab expend background image

                                clickedLayout = chaildlayoutList.get(i);
                                clickedIndex = i;
                                expendedImage = expendedImageList.get(i);
                            } else {
                                linearLayout.removeViewAt(clickedIndex + 1);
                                clickedLayout = null;
                                clickedIndex = -1;
                                if(isTab)
                                    expendedImage.setBackgroundResource(R.drawable.tablet_expand_button);// set expended button image when click on other
                                else
                                    expendedImage.setBackgroundResource(R.drawable.expand_button);// set expended button image when click on other
                                expendedImage = null;
                            }
                        }
                    }
                }
            });
        }

        //changes for open seleted category
        if(!isFromMain){

            linearChildLayout = new LinearLayout(NewLearnignCenter.this);
            linearChildLayout.setOrientation(LinearLayout.VERTICAL);
            linearChildLayout = addChildLayout(CommonUtils.selectedIndexForOpenCategory);
            linearLayout.addView(linearChildLayout, CommonUtils.selectedIndexForOpenCategory + 1);

            if(isTab)
                expendedImageList.get(CommonUtils.selectedIndexForOpenCategory).
                        setBackgroundResource(R.drawable.tablet_tab_expand_button);//set tab expend background image
            else
                expendedImageList.get(CommonUtils.selectedIndexForOpenCategory).
                        setBackgroundResource(R.drawable.tab_expand_button);//set tab expend background image

            clickedLayout = chaildlayoutList.get(CommonUtils.selectedIndexForOpenCategory);
            clickedIndex = CommonUtils.selectedIndexForOpenCategory;
            expendedImage = expendedImageList.get(CommonUtils.selectedIndexForOpenCategory);
        }
        //end changes
    }


    /**
     * This method check application status
     * @param itemId
     * @return
     */
    private boolean getUnlockApplicationStatuse(int itemId)
    {
        boolean isUnlock = false;
        SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
        if(this.getApplicationUnLockStatus(100 , sharedPreffPlayerInfo.getString("userId", "")) == 1)
        {
            isUnlock = true;
        }
        else
        {	//Log.e(TAG, " inside set background status "  + this.getApplicationUnLockStatus(1));
            if(this.getApplicationUnLockStatus(itemId ,  sharedPreffPlayerInfo.getString("userId", "")) == 1)
            {
                isUnlock = true;
            }
            else
            {
                isUnlock = false;
            }
        }
        return isUnlock;
    }

    /**
     * This method return the operation id
     * @param index
     * @return
     */
    private int getOperationId(int index)
    {
        int operationId = 0;

        switch(index)
        {
            case 0:
                operationId = 1;
                break;
            case 1:
                operationId = 2;
                break;
            case 2:
                operationId = 3;
                break;
            case 3:
                operationId = 4;
                break;
            case 4:
                operationId = 5;
                break;
            case 5:
                operationId = 6;
                break;
            case 6:
                operationId = 7;
                break;
            case 7:
                operationId = 8;
                break;
            case 8:
                operationId = 9;
                break;
            case 9:
                operationId = 10;
                break;
        }

        return operationId;
    }

    /**
     * This method set the progress
     * @param txtProgress
     * @param index
     * @param
     */
    private void setProgress(TextView txtProgress , int index, ImageView imgMedalImage)
    {
        int operationId = this.getOperationId(index);

        int maxLevel = 0;
        int totalStars = 0;
        for( int i  = 0 ; i < playerDataList.size() ; i ++ )
        {
            if(operationId == playerDataList.get(i).getEquationType())
            {
                maxLevel = playerDataList.get(i).getLevel();//contain the highest level
                totalStars = totalStars + playerDataList.get(i).getStars();
            }
        }

        txtProgress.setText(maxLevel + " of " + 10); // 10 is the maximum level

        if(maxLevel == 10){//where 10 is the highest level
            this.setMedal(imgMedalImage, totalStars , 30);//where 30 is the maximum stars
        }
    }

    /**
     * This method set the medal
     * @param
     */
    private void setMedal(ImageView imgMedalImage , int totalNumberOfStars , int maxStars){
        imgMedalImage.setVisibility(ImageView.VISIBLE);

        int percent = (totalNumberOfStars * 100)/maxStars; // where 30 is the total number of stars

        if(percent >=0 && percent <= 40){
            if(isTab)
                imgMedalImage.setBackgroundResource(R.drawable.tab_ribbon);
            else
                imgMedalImage.setBackgroundResource(R.drawable.ribbon);
        }
        else if(percent > 40 && percent <= 75){
            if(isTab)
                imgMedalImage.setBackgroundResource(R.drawable.tab_medal);
            else
                imgMedalImage.setBackgroundResource(R.drawable.medal);
        }
        else{
            if(isTab)
                imgMedalImage.setBackgroundResource(R.drawable.tab_cup);
            else
                imgMedalImage.setBackgroundResource(R.drawable.cup);
        }
    }


    /**
     * This method add the child layout
     * This methid add chold at the clicked + 1 position. on the basis of find the operation id and do work
     * @return
     */
    private LinearLayout addChildLayout(int index)
    {
        int operationId = this.getOperationId(index);

        sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.putString("bgImageName", laernignCenterFunctionsList.get(index).getLearningCenterOperation());
        editor.putInt("operationId", operationId);
        editor.commit();

        //boolean isUnlock = this.getUnlockApplicationStatuse(operationId);
        //New InApp Change
        boolean isUnlock = this.getUnlockApplicationStatusNewInApp(operationId);

        ArrayList<ImageView> imgStartList = new ArrayList<ImageView>();

        subchaildlayoutList = new ArrayList<RelativeLayout>();

        LinearLayout linearChildLayout = new LinearLayout(NewLearnignCenter.this);
        linearChildLayout.setOrientation(LinearLayout.VERTICAL);

        LinearLayout linearLockChildLayout = new LinearLayout(NewLearnignCenter.this);
        linearLockChildLayout.setOrientation(LinearLayout.VERTICAL);
        linearLockChildLayout.setBackgroundResource(R.drawable.border);

        for (int j = 0; j < childCategoryList.size(); j++) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(NewLearnignCenter.LAYOUT_INFLATER_SERVICE);
            subchildLayout = (RelativeLayout) inflater.inflate(R.layout.new_learning_center_child_layout,null);

            txtChildLevelName = (TextView) subchildLayout.findViewById(R.id.txtlevelName);
            imgStars          = (ImageView) subchildLayout.findViewById(R.id.imgStar);

            imgStartList.add(imgStars);

            if(j == 0)
                imgStars.setVisibility(ImageView.INVISIBLE);

            if(j >= 4)
            {
                if(!isUnlock){
                    if(j == 4){
                        ImageView imageLock = new ImageView(this);
                        imageLock.setBackgroundResource(R.drawable.sf_lock);
                        subchildLayout.addView(imageLock);
                    }
                    subchildLayout.setBackgroundColor(Color.GRAY);
                    linearLockChildLayout.addView(subchildLayout);
                }
            }

            txtChildLevelName.setText(childCategoryList.get(j));

            subchildLayout.setOnClickListener(new OnClickListener()
            {
                @Override
                public void onClick(View v) {
                    for(int i = 0 ; i < subchaildlayoutList.size() ; i ++ )
                    {
                        if(v == subchaildlayoutList.get(i))
                        {
                            if( i == 0)//if i is 0 then clicked on show flash cards
                            {
                                subchaildlayoutList.get(i).setBackgroundResource(R.drawable.mcg_green_bar);
                                Handler handlerDelay = new Handler();
                                handlerDelay.postDelayed(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        //changes for open already selected category
                                        subchaildlayoutList.get(0).setBackgroundResource(0);
                                        //end changes
                                        Intent intentEqu = new Intent(NewLearnignCenter.this,
                                                ShowFlashCardsEquations.class);
                                        //startActivity(intentEqu);
                                        startActivityForResult(intentEqu , 3);
                                    }
                                },DELAY_TIME);
                            }
                            else
                            {
                                clickOnLevel(i , subchaildlayoutList.get(i));
                            }
                        }
                    }
                }
            });

            subchaildlayoutList.add(subchildLayout);
            if(j >= 4)
            {
                if(isUnlock)
                    linearChildLayout.addView(subchildLayout);
            }else{
                linearChildLayout.addView(subchildLayout);
            }
        }

        //if lock then add lock child layout
        if(!isUnlock){
            linearChildLayout.addView(linearLockChildLayout);
        }

        int counter = 0; // hold the number of level for star
        for( int i  = 0 ; i < playerDataList.size() ; i ++ )
        {
            if(operationId == playerDataList.get(i).getEquationType())
            {
                setStarForPlayLevel(imgStartList.get(counter + 1), playerDataList.get(i).getStars());
                counter ++ ;
            }
        }
        return linearChildLayout;
    }


    /**
     * This method set the play level start at each level
     * @param imgStart
     * @param noOfStars
     */
    private void setStarForPlayLevel(ImageView imgStart,int noOfStars)
    {
        //Log.e(TAG, "number of stars" + noOfStars);
        switch(noOfStars)
        {
            case 1:
                if(isTab)
                    imgStart.setBackgroundResource(R.drawable.tab_yellow_stars1);
                else
                    imgStart.setBackgroundResource(R.drawable.yellow_stars1);
                break;
            case 2:
                if(isTab)
                    imgStart.setBackgroundResource(R.drawable.tab_yellow_stars2);
                else
                    imgStart.setBackgroundResource(R.drawable.yellow_stars2);
                break;
            case 3:
                if(isTab)
                    imgStart.setBackgroundResource(R.drawable.tab_yellow_stars3);
                else
                    imgStart.setBackgroundResource(R.drawable.yellow_stars3);
                break;
        }
    }


    /**
     * This method return the highest level for playing it
     * @param
     * @param
     * @return
     */
    private int  getHighestEquationLevel()
    {
        int highestLevel = 0;
        SharedPreferences sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
        int operationId = sharedPreferences.getInt("operationId", 0);

        SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);

        PlayerEquationLevelObj playerEquationObj = new PlayerEquationLevelObj();
        playerEquationObj.setPlayerId(sharedPreffPlayerInfo.getString("playerId", ""));
        playerEquationObj.setUserId(sharedPreffPlayerInfo.getString("userId", ""));
        playerEquationObj.setEquationType(operationId);

        LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
        learningCenterObj.openConn();
        highestLevel =  learningCenterObj.getHighetsLevel(playerEquationObj);
        learningCenterObj.closeConn();

        return highestLevel;
    }

    /**
     * This method call when user click on level
     * and start play
     * @param level
     * @param
     */
    private void clickOnLevel(final int level, final RelativeLayout clickedLayout){
        if(level >= 4){
            if(appStatus == 1){
                if(CommonUtils.isActivePlayer)
                    this.generateDalialogWhenPlayerIsActiveForFriendzyChallenge();
                else
                    this.clickOnLevelCommon(level , clickedLayout);
            }else{
                this.clickOnPracticeSkillLockLevel(level , clickedLayout);
                //this.getRequiredCoinsForPurchase();
            }
        }else{
            if(CommonUtils.isActivePlayer)
                this.generateDalialogWhenPlayerIsActiveForFriendzyChallenge();
            else
                this.clickOnLevelCommon(level , clickedLayout);
        }
    }

    /**
     * This method call is player participate in friendzy challenge means active for friendzy challenge
     */
    private void generateDalialogWhenPlayerIsActiveForFriendzyChallenge(){
        Translation translation = new Translation(this);
        translation.openConnection();
        DialogGenerator dg = new DialogGenerator(this);
        dg.generateWarningDialogForNoSufficientQuestionForSingleFriendzy(
                translation.getTranselationTextByTextIdentifier("lblYouAreCurrentlyParticipatingInChallenge"),
                this , 2);
        translation.closeConnection();
    }

    /**
     * This is common method which is call either condition true or flase
     */
    private void clickOnLevelCommon(final int level , final RelativeLayout clickedLayout)
    {
        int highestLevel = this.getHighestEquationLevel();

        if(((level == 1 ) || (level <= highestLevel + 1)))
        {
            clickedLayout.setBackgroundResource(R.drawable.mcg_green_bar);

            Handler handlerDelay = new Handler();
            handlerDelay.postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    SharedPreferences sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
                    int operationId = sharedPreferences.getInt("operationId", 0);

                    LearningCenterimpl learningCenter = new LearningCenterimpl(NewLearnignCenter.this);
                    learningCenter.openConn();
                    mathOperationIdList = learningCenter.getCategoryIdsFromMathEquationLeveCategory(operationId, level);
                    learningCenter.closeConn();

                    //changes for open already selected category
                    clickedLayout.setBackgroundResource(0);
                    //end changes

                    Intent intent = new Intent(NewLearnignCenter.this,LearningCenterEquationSolveWithTimer.class);
                    intent.putIntegerArrayListExtra("selectedCategories", mathOperationIdList);
                    intent.putExtra("level", level);
                    startActivityForResult(intent, 1);
                }
            },DELAY_TIME);
        }
        else
        {
            Translation transeletion = new Translation(this);
            transeletion.openConnection();
            DialogGenerator dg = new DialogGenerator(this);
            dg.generateWarningDialog(transeletion
                    .getTranselationTextByTextIdentifier
                            ("alertMsgPlayThePreviousLevelToUnlockThis"));
            transeletion.closeConnection();
            //this.RefresgUIWhenNeeded();
        }
    }

    /**
     * This method get required coins from server
     */
    private void getRequiredCoinsForPurchase()
    {
        String api = null;
        SharedPreferences sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
        int operationId = sharedPreferences.getInt("operationId", 0);

        SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);

        TempPlayerOperation tempPlayer = new TempPlayerOperation(this);
        if(tempPlayer.isTempPlayerDeleted())
        {
            api = "itemId=" + operationId + "&userId="+sharedPreffPlayerInfo.getString("userId", "")
                    +"&playerId="+sharedPreffPlayerInfo.getString("playerId", "");
        }
        else
        {
            api = "itemId=" + operationId;
        }

        if(!CommonUtils.isInternetConnectionAvailable(this))
        {
            DialogGenerator dg = new DialogGenerator(this);
            Translation transeletion = new Translation(this);
            transeletion.openConnection();
            dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
            transeletion.closeConnection();
        }else{
            new GetRequiredCoinsForPurchaseItem(api).execute(null,null,null);
        }

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this , MainActivity.class));
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
		/*if(requestCode == 1)
		{
			if(resultCode == RESULT_OK)
			{
				DialogGenerator dg = new DialogGenerator(this);
				dg.generatetimeSpentDialog();
			}
		}else if(requestCode == 2){
		}
		else if(requestCode == 3){
		}*/

        if(resultCode == RESULT_OK){
            switch (requestCode){
                case 1:
                    DialogGenerator dg = new DialogGenerator(this);
                    dg.generatetimeSpentDialog();
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case GetMoreCoins.START_GET_MORE_COIN_ACTIVITY_REQUEST:
                    this.updateUIAfterPurchaseSuccess();
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * This method set listener on widgets
     */
    private void setListeneronWidgets() {

        imgSolveEquation.setOnClickListener(this);
        imgSchoolCurriculum.setOnClickListener(this);
        //added for Show By CCSS
        btnShowByCCSS.setOnClickListener(this);

        //for show timer
        imgShowTimer.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch(v.getId()){
            case R.id.imgSolveEquation :
                this.setIsFromMainValue();
                //changes for friendzy challenge
                if(CommonUtils.isActivePlayer){

                    CommonUtils.isClickedSolveEquation = true;
                    imgSolveEquation.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
                    imgSchoolCurriculum.setBackgroundResource(R.drawable.mf_check_box_ipad);
                    gradeLayout.setVisibility(RelativeLayout.INVISIBLE);
                    txtProgress.setVisibility(TextView.VISIBLE);

                    Intent intent = new Intent(NewLearnignCenter.this,LearningCenterEquationSolveWithTimer.class);
                    startActivity(intent);

                    this.solveEquation();
                }else{
                    this.solveEquation();
                }
                break;
            case R.id.imgSchoolCurriculum :
                if(CommonUtils.isClickedSolveEquation){
                    this.setIsFromMainValue();
                    this.schoolCurriculum(CommonUtils.selectedGrade);
                }
                break;

            case R.id.btnShowByCCSS :
			/*Intent intent = new Intent(this , ShowByCCSSMainStandards.class);
			intent.putExtra("selectedGrade", CommonUtils.selectedGrade);
			startActivity(intent);*/
                goOnMainStandardsScrennAfterClickOnShowByCCSS();
                break;
            case R.id.imgShowTimer :
                CommonUtils.isShowTimer = !CommonUtils.isShowTimer;
                setShowTimerCheck();
                break;
        }
    }

    /**
     * This method get number of sub category according to category id
     * @param categoryId
     * @return
     */
    private ArrayList<SubCatergoryTransferObj> getSubCategoryList(int categoryId){
        //changes for Spanish
        ArrayList<SubCatergoryTransferObj> subCategoryList = new ArrayList<SubCatergoryTransferObj>();
        if(CommonUtils.getUserLanguageCode(this) == CommonUtils.ENGLISH){
            SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
            schoolImpl.openConnection();
            subCategoryList = schoolImpl.getSubChildCategories(categoryId);
            schoolImpl.closeConnection();
        }else{
            SpanishChangesImpl schoolImpl = new SpanishChangesImpl(this);
            schoolImpl.openConn();
            subCategoryList = schoolImpl.getSubChildCategories(categoryId);
            schoolImpl.closeConn();
        }
        return subCategoryList;
    }

    /**
     * This method set the progress for school curriculum and edal also
     * @param categoryId
     * @param txtProgressDetail
     * @param playerId
     * @param userId
     * @param imgMedalImage
     */
    private void setProgressForSchoolCurriculum(TextView txtProgressDetail, int categoryId,
                                                String userId, String playerId, ImageView imgMedalImage){

        ArrayList<SubCatergoryTransferObj> subCategoryList = this.getSubCategoryList(categoryId);

        SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
        schoolImpl.openConnection();
        ArrayList<WordProblemLevelTransferObj> wordProblemLevelList = schoolImpl.getWordProblemLevelData(userId, playerId , categoryId);
        schoolImpl.closeConnection();

        txtProgressDetail.setText(wordProblemLevelList.size() + " of " + subCategoryList.size());

        if(wordProblemLevelList.size() == subCategoryList.size()){
            int totalStar = 0;
            for(int i = 0 ; i < wordProblemLevelList.size() ; i ++ ){
                totalStar = totalStar + wordProblemLevelList.get(i).getStars();
            }
            this.setMedal(imgMedalImage, totalStar , wordProblemLevelList.size() * 3);
        }
    }

    /**
     * This method add the child layout for school curriculum
     * This method add child at the clicked + 1 position. on the basis of find the operation id and do work
     * @param playerId
     * @param userId
     * @return
     */
    private LinearLayout addChildLayoutForSchooCurriculum(final int categoryId, String userId, String playerId)
    {
        //added for show by CCSS
        ArrayList<Integer> subCatListForShoeByCCSS = new ArrayList<Integer>();
        if(isFromShowByCCSS){
            subCatListForShoeByCCSS = CommonUtils.
                    getArrayListFromStringWithCommas(this.getSubCategoriesFromList(categoryId));
        }
        //end changes

        ArrayList<ImageView> imgStartList = new ArrayList<ImageView>();
        final ArrayList<SubCatergoryTransferObj> subCategoryList = this.getSubCategoryList(categoryId);

        subchaildlayoutList = new ArrayList<RelativeLayout>();

        LinearLayout linearChildLayout = new LinearLayout(NewLearnignCenter.this);
        linearChildLayout.setOrientation(LinearLayout.VERTICAL);

        //added for thumb images changes
        final ArrayList<ImageView> imgThumbList 		= new ArrayList<ImageView>();
        final ArrayList<Integer>   isRightAnswerList  	= new ArrayList<Integer>();

        final ArrayList<ImageView> imgVedioUrlList 		= new ArrayList<ImageView>();
        final ArrayList<String>    vedioUrlList       	= new ArrayList<String>();
        //end changes

        for (int j = 0; j < subCategoryList.size(); j++) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(NewLearnignCenter.LAYOUT_INFLATER_SERVICE);
            subchildLayout = (RelativeLayout) inflater.inflate(R.layout.new_learning_center_child_layout,null);

            txtChildLevelName = (TextView) 	subchildLayout.findViewById(R.id.txtlevelName);
            imgStars          = (ImageView) subchildLayout.findViewById(R.id.imgStar);
            imgThumbImage	  = (ImageView) subchildLayout.findViewById(R.id.imgThumbImage);
            imgVedioUrl		  = (ImageView) subchildLayout.findViewById(R.id.imgVedioUrl);

            imgStartList.add(imgStars);

            txtChildLevelName.setText(subCategoryList.get(j).getName());

            //added for show by CCSS
            if(subCatListForShoeByCCSS.contains(subCategoryList.get(j).getId()))
                txtChildLevelName.setTextColor(Color.GREEN);
            //end changes

            //changes for thumb images
            this.setThumbImage(imgThumbImage, categoryId, subCategoryList.get(j).getId() , imgThumbList
                    ,isRightAnswerList);
            //end changes

            //for vedio image url
            if(subCategoryList.get(j).getUrl() != null && subCategoryList.get(j).getUrl().length() > 0){
                imgVedioUrlList.add(imgVedioUrl);
                vedioUrlList.add(subCategoryList.get(j).getUrl());
                imgVedioUrl.setVisibility(ImageView.VISIBLE);
            }
            //end changes

            //changes for thumb image
            /*imgThumbImage.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    for(int i = 0 ; i < imgThumbList.size() ; i ++ ){
                        if(v == imgThumbList.get(i)){
                            DialogGenerator dg = new DialogGenerator(NewLearnignCenter.this);
                            Translation transeletion = new Translation(NewLearnignCenter.this);
                            transeletion.openConnection();
                            if(isRightAnswerList.get(i) == 0){
                                dg.generateWarningDialogForthumbImages(transeletion.
                                        getTranselationTextByTextIdentifier
                                                ("alertMsgYouAnsweredQuestionFromThisCategoryIncorrectly"));
                            }else{
                                dg.generateWarningDialogForthumbImages(transeletion.
                                        getTranselationTextByTextIdentifier
                                                ("alertMsgYouAnsweredQuestionFromThisCategoryCorrectly"));
                            }
                            transeletion.closeConnection();
                        }
                    }
                }
            });*/

            imgVedioUrl.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    try{
                        for(int i = 0 ; i < imgVedioUrlList.size() ; i ++ ){
                            if(v == imgVedioUrlList.get(i)){
                                //if(vedioUrlList.get(i).contains("https://")){
                                if(vedioUrlList.get(i).startsWith("http")){
                                    MathFriendzyHelper.converUrlIntoEmbaddedAndPlay
                                            (NewLearnignCenter.this,
                                                    vedioUrlList.get(i));
                                }else{
                                    MathFriendzyHelper.converUrlIntoEmbaddedAndPlay
                                            (NewLearnignCenter.this,
                                                    "https://" + vedioUrlList.get(i));
                                }
                            }
                        }
                    }catch (Exception e) {
                        Log.e(TAG, "No vedio exist " + e.toString());
                    }
                }
            });
            //end changes

            subchildLayout.setOnClickListener(new OnClickListener()
            {
                @Override
                public void onClick(View v) {

                    for(int i = 0 ; i < subchaildlayoutList.size() ; i ++ )
                    {
                        if(v == subchaildlayoutList.get(i)){
                            clickOnSubChildCategory(subCategoryList.get(i).getId() , categoryId ,
                                    subchaildlayoutList.get(i) , subCategoryList.get(i).getName());
                        }
                    }
                }
            });

            subchaildlayoutList.add(subchildLayout);
            linearChildLayout.addView(subchildLayout);
        }


        SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
        schoolImpl.openConnection();
        ArrayList<WordProblemLevelTransferObj> wordProblemLevelList = schoolImpl.getWordProblemLevelData(userId, playerId , categoryId);
        schoolImpl.closeConnection();

        for( int i  = 0 ; i < subCategoryList.size() ; i ++ )
        {	for(int j = 0 ; j < wordProblemLevelList.size() ; j ++ ){
            if(wordProblemLevelList.get(j).getSubCategoryId() == subCategoryList.get(i).getId())
            {
                this.setStarForPlayLevel(imgStartList.get(i), wordProblemLevelList.get(j).getStars());
            }
        }
        }

        return linearChildLayout;
    }

    /**
     * This method set the thump images
     * @param imgthumbImage
     * @param catId
     * @param subCatId
     */
    private void setThumbImage(ImageView imgthumbImage , int catId , int subCatId
            ,ArrayList<ImageView> imgThumbList , ArrayList<Integer>   isRightAnswerList){
        if(MainActivity.testDeatilList != null){
            for(int i = 0 ; i < MainActivity.testDeatilList.size() ; i ++ ){
                TestDetails testDetailObj = MainActivity.testDeatilList.get(i);
                if(testDetailObj.getCatId() == catId && testDetailObj.getsCatId() == subCatId){
                    imgthumbImage.setVisibility(ImageView.VISIBLE);
                    imgThumbList.add(imgthumbImage);

                    if(testDetailObj.getAns() == 0){
                        isRightAnswerList.add(0);
                        if(isTab)
                            imgthumbImage.setBackgroundResource(R.drawable.down_thumb_ipad);
                        else
                            imgthumbImage.setBackgroundResource(R.drawable.down_thumb);

                        imgthumbImage.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                DialogGenerator dg = new DialogGenerator(NewLearnignCenter.this);
                                Translation transeletion = new Translation(NewLearnignCenter.this);
                                transeletion.openConnection();
                                dg.generateWarningDialogForthumbImages(transeletion.
                                        getTranselationTextByTextIdentifier
                                                ("alertMsgYouAnsweredQuestionFromThisCategoryIncorrectly"));
                                transeletion.closeConnection();
                            }
                        });
                    }else{
                        isRightAnswerList.add(1);
                        if(isTab)
                            imgthumbImage.setBackgroundResource(R.drawable.up_thumb_ipad);
                        else
                            imgthumbImage.setBackgroundResource(R.drawable.up_thumb);

                        imgthumbImage.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                DialogGenerator dg = new DialogGenerator(NewLearnignCenter.this);
                                Translation transeletion = new Translation(NewLearnignCenter.this);
                                transeletion.openConnection();
                                dg.generateWarningDialogForthumbImages(transeletion.
                                        getTranselationTextByTextIdentifier
                                                ("alertMsgYouAnsweredQuestionFromThisCategoryCorrectly"));
                                transeletion.closeConnection();
                            }
                        });
                    }
                }
            }
        }
    }

    /**
     * This method call when user click on sub category of school curriculum
     * @param subCategoryId
     * @param categoryId
     * @param clickedChild
     */
    private void clickOnSubChildCategory(final int subCategoryId, final int categoryId,
                                         final RelativeLayout clickedChild , final String subCatName){
		/*DialogGenerator dg = new DialogGenerator(this);
		dg.generateWarningDialog("Under Developement!");*/

        //if(this.getUnlockApplicationStatuse(100))
        if(this.getUnlockApplicationStatusNewInApp
                (MathFriendzyHelper.APP_UNLOCK_ID)){
            clickedChild.setBackgroundResource(R.drawable.mcg_green_bar);
            Handler handlerDelay = new Handler();
            handlerDelay.postDelayed(new Runnable(){
                @Override
                public void run(){
                    openSchoolCurriculumPlayScreen(subCategoryId , categoryId , clickedChild , subCatName);
                }
            },DELAY_TIME);
        }
        else{
            /*SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
            String userId   = sharedPreffPlayerInfo.getString("userId", "");
            String playerId = sharedPreffPlayerInfo.getString("playerId", "");
            if(CommonUtils.isInternetConnectionAvailable(this)){
                new GetSuscriptionInfo(this , userId , playerId , subCategoryId ,
                        categoryId , subCatName).execute(null,null,null);
            }else{
                DialogGenerator dg = new DialogGenerator(this);
                Translation transeletion = new Translation(this);
                transeletion.openConnection();
                dg.generateWarningDialog(transeletion.
                getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
                transeletion.closeConnection();
            }*/
            this.clickOnSchoolCurriculumLockLevel(subCategoryId , categoryId ,
                    clickedChild , subCatName);

        }
    }

    private void openSchoolCurriculumPlayScreen(final int subCategoryId, final int categoryId,
                                                final RelativeLayout clickedChild ,
                                                final String subCatName){
        clickedChild.setBackgroundResource(0);
        Intent intent = new Intent(NewLearnignCenter.this ,
                LearningCenterSchoolCurriculumEquationSolve.class);
        intent.putExtra("categoryId", categoryId);
        intent.putExtra("subCategoryId", subCategoryId);
        intent.putExtra("playerSelectedGrade", CommonUtils.selectedGrade);
        intent.putExtra("subCatName", subCatName);
        startActivityForResult(intent , 2);
    }

    /**
     * This method set the layout value for school curriculum
     * @param categoryList
     */
    private void setLayoutValueForSchoolCurriculum(final ArrayList<CategoryListTransferObj> categoryList)
    {
        linearLayout.removeAllViews();// for restarting when click on back press in learning center equation solve with timer
        chaildlayoutList    = new ArrayList<RelativeLayout>();
        expendedImageList   = new ArrayList<ImageView>();
        clickedLayout = null;
        clickedIndex = -1;
        expendedImage = null;

        SharedPreferences sharedPrefPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
        final String userId   = sharedPrefPlayerInfo.getString("userId", "");
        final String playerId = sharedPrefPlayerInfo.getString("playerId", "");

        if(categoryList.size() > 0 ){
            for (int i = 0 ; i < categoryList.size() ; i++) {
                LayoutInflater inflater = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
                childLayout = (RelativeLayout) inflater.inflate(R.layout.new_learning_center_layout, null);

                imgExpandImage 		= (ImageView) childLayout.findViewById(R.id.imgExpand);
                imgCategorySign 	= (ImageView) childLayout.findViewById(R.id.imgSign);
                txtCategoryName 	= (TextView)  childLayout.findViewById(R.id.txtCategoryName);
                txtProgressDetail 	= (TextView)  childLayout.findViewById(R.id.txtProgress);
                imgMedalImage       = (ImageView) childLayout.findViewById(R.id.imgMedal);

                imgCategorySign.setVisibility(ImageView.GONE);

                //this method set progress and medal also
                this.setProgressForSchoolCurriculum(txtProgressDetail ,
                        categoryList.get(i).getCid() , userId , playerId , imgMedalImage);

                txtCategoryName.setText(categoryList.get(i).getName());

                //added for show by CCSS
                if(isFromShowByCCSS && this.isCatIdExistForShowByCCSS(categoryList.get(i).getCid()))
                    txtCategoryName.setTextColor(Color.GREEN);
                //end changes

                linearLayout.addView(childLayout);
                expendedImageList.add(imgExpandImage);
                chaildlayoutList.add(childLayout);

                childLayout.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
                        if(!sheredPreference.getBoolean(IS_LOGIN, false))
                        {
                            Translation transeletion = new Translation(NewLearnignCenter.this);
                            transeletion.openConnection();
                            DialogGenerator dg = new DialogGenerator(NewLearnignCenter.this);
                            dg.generateRegisterOrLogInDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouMustRegisterLoginToAccessThisFeature"));
                            transeletion.closeConnection();
                        }
                        else
                        {
							/*LinearLayout linearChildLayout = new LinearLayout(NewLearnignCenter.this);
							linearChildLayout.setOrientation(LinearLayout.VERTICAL);*/
                            //changes for open selected categories
                            linearChildLayout = new LinearLayout(NewLearnignCenter.this);
                            linearChildLayout.setOrientation(LinearLayout.VERTICAL);
                            //end changes

                            for (int i = 0; i < chaildlayoutList.size(); i++) {
                                if (v == chaildlayoutList.get(i)) {
                                    if (clickedLayout != chaildlayoutList.get(i)) {
                                        //added for show by CCSS
                                        isSubCatOpen = true;
                                        //end changes

                                        if(clickedIndex != -1)
                                            linearLayout.removeViewAt(clickedIndex + 1);

                                        if(expendedImage != null){
                                            if(isTab)
                                                expendedImage.setBackgroundResource(R.drawable.tablet_expand_button);// set expended button image when click on other
                                            else
                                                expendedImage.setBackgroundResource(R.drawable.expand_button);// set expended button image when click on other
                                        }

                                        //changes for open selected categories
                                        CommonUtils.selectedIndexForOpenCategory = i;
                                        CommonUtils.openCategoryId = categoryList.get(i).getCid();
                                        //end changes

                                        linearChildLayout = addChildLayoutForSchooCurriculum(categoryList.get(i).getCid()
                                                ,userId , playerId);
                                        linearLayout.addView(linearChildLayout, i + 1);

                                        if(isTab)
                                            expendedImageList.get(i).setBackgroundResource(R.drawable.tablet_tab_expand_button);//set tab expend background image
                                        else
                                            expendedImageList.get(i).setBackgroundResource(R.drawable.tab_expand_button);//set tab expend background image

                                        clickedLayout = chaildlayoutList.get(i);
                                        clickedIndex = i;
                                        expendedImage = expendedImageList.get(i);
                                    } else {

                                        //added for show by CCSS
                                        isSubCatOpen = false;
                                        //end changes

                                        linearLayout.removeViewAt(clickedIndex + 1);
                                        clickedLayout = null;
                                        clickedIndex = -1;
                                        if(isTab)
                                            expendedImage.setBackgroundResource(R.drawable.tablet_expand_button);// set expended button image when click on other
                                        else
                                            expendedImage.setBackgroundResource(R.drawable.expand_button);// set expended button image when click on other
                                        expendedImage = null;
                                    }
                                }
                            }
                        }
                    }
                });
            }
        }else{
            //CommonUtils.comingSoonPopUp(this);
            //comment code for not downloading question on MainScreen for temp playe , On 23 May 2014
            loadQuestionFromServer(Integer.parseInt(spinnerGrade.getSelectedItem().toString()));
        }

        //changes for open selected category
        if(isFromShowByCCSS){//added for show by CCSS
            if(isSubCatOpen){
                linearChildLayout = new LinearLayout(NewLearnignCenter.this);
                linearChildLayout.setOrientation(LinearLayout.VERTICAL);
                linearChildLayout = addChildLayoutForSchooCurriculum(categoryList.
                        get(CommonUtils.selectedIndexForOpenCategory).getCid(),userId , playerId);
                linearLayout.addView(linearChildLayout, CommonUtils.selectedIndexForOpenCategory + 1);

                if(isTab)
                    expendedImageList.get(CommonUtils.selectedIndexForOpenCategory).
                            setBackgroundResource(R.drawable.tablet_tab_expand_button);//set tab expend background image
                else
                    expendedImageList.get(CommonUtils.selectedIndexForOpenCategory).
                            setBackgroundResource(R.drawable.tab_expand_button);//set tab expend background image

                clickedLayout = chaildlayoutList.get(CommonUtils.selectedIndexForOpenCategory);
                clickedIndex = CommonUtils.selectedIndexForOpenCategory;
                expendedImage = expendedImageList.get(CommonUtils.selectedIndexForOpenCategory);
            }
        }else{
            if(!isFromMain){
                linearChildLayout = new LinearLayout(NewLearnignCenter.this);
                linearChildLayout.setOrientation(LinearLayout.VERTICAL);
                linearChildLayout = addChildLayoutForSchooCurriculum(categoryList.
                        get(CommonUtils.selectedIndexForOpenCategory).getCid(),userId , playerId);
                linearLayout.addView(linearChildLayout, CommonUtils.selectedIndexForOpenCategory + 1);

                if(isTab)
                    expendedImageList.get(CommonUtils.selectedIndexForOpenCategory).
                            setBackgroundResource(R.drawable.tablet_tab_expand_button);//set tab expend background image
                else
                    expendedImageList.get(CommonUtils.selectedIndexForOpenCategory).
                            setBackgroundResource(R.drawable.tab_expand_button);//set tab expend background image

                clickedLayout = chaildlayoutList.get(CommonUtils.selectedIndexForOpenCategory);
                clickedIndex = CommonUtils.selectedIndexForOpenCategory;
                expendedImage = expendedImageList.get(CommonUtils.selectedIndexForOpenCategory);
            }
        }
        //end changes
    }


    //added for show by CCSS
    private boolean isCatIdExistForShowByCCSS(int catId){
        for(int i = 0 ; i < categoryInfoList.size() ; i ++ ){
            if(categoryInfoList.get(i).getCatId() == catId)
                return true;
        }
        return false;
    }

    /**
     * This method return the categories from the list
     * @param catId
     * @return
     */
    private String getSubCategoriesFromList(int catId){
        for(int i = 0 ; i < categoryInfoList.size() ; i ++ ){
            if(categoryInfoList.get(i).getCatId() == catId)
                return categoryInfoList.get(i).getSubCatId();
        }
        return "";
    }

    //end changes

    /**
     * This class get coins from server
     * @author Yashwant Singh
     *
     */
    class GetRequiredCoinsForPurchaseItem extends AsyncTask<Void, Void, Void>
    {
        private String apiString = null;
        private CoinsFromServerObj coindFromServer;
        private ProgressDialog pg = null;

        GetRequiredCoinsForPurchaseItem(String apiValue)
        {
            this.apiString = apiValue;
        }

        @Override
        protected void onPreExecute()
        {
            pg = CommonUtils.getProgressDialog(NewLearnignCenter.this);
            pg.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            LearningCenteServerOperation learnignCenterOpr = new LearningCenteServerOperation();
            coindFromServer = learnignCenterOpr.getRequiredCoisForPurchase(apiString);
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            pg.cancel();

            if(coindFromServer != null){
				/*if(coindFromServer.getCoinsEarned() == -1 || coindFromServer.getCoinsEarned() >= coindFromServer.getCoinsRequired() 
					|| coindFromServer.getCoinsPurchase() == 0)
			{*/
                SharedPreferences sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
                int operationId = sharedPreferences.getInt("operationId", 0); //itme id

                if(coindFromServer.getCoinsEarned() == -1)
                {
                    LearningCenterimpl learnignCenterObj = new LearningCenterimpl(NewLearnignCenter.this);
                    learnignCenterObj.openConn();
                    DialogGenerator dg = new DialogGenerator(NewLearnignCenter.this);
                    dg.generateLevelWarningDialogForLearnignCenter(learnignCenterObj.getDataFromPlayerTotalPoints("0").getCoins() ,
                            coindFromServer , operationId , null);
                    learnignCenterObj.closeConn();
                }
                else
                {
                    DialogGenerator dg = new DialogGenerator(NewLearnignCenter.this);
                    dg.generateLevelWarningDialogForLearnignCenter(coindFromServer.getCoinsEarned() ,
                            coindFromServer , operationId , null);
                }
				/*}
			else
			{
				DialogGenerator dg = new DialogGenerator(NewLearnignCenter.this);
				dg.generateDialogForNeedCoinsForUlock(coindFromServer.getCoinsEarned() ,
						coindFromServer.getCoinsRequired());
			}*/
            }else{
                CommonUtils.showInternetDialog(NewLearnignCenter.this);
            }
            super.onPostExecute(result);
        }
    }


    /**
     * This asynctask get questions from server according to grade if not loaded in database
     * @author Yashwant Singh
     *
     */
    public class GetWordProblemQuestion extends AsyncTask<Void, Void, QuestionLoadedTransferObj>{

        private int grade;
        private ProgressDialog pd;
        private Context context;
        ArrayList<String> imageNameList = new ArrayList<String>();

        public GetWordProblemQuestion(int grade , Context context){
            this.grade 		= grade;
            this.context 	= context;
        }

        @Override
        protected void onPreExecute() {

            Translation translation = new Translation(context);
            translation.openConnection();
            pd = CommonUtils.getProgressDialog(context, translation.getTranselationTextByTextIdentifier
                    ("alertPleaseWaitWeAreDownloading") , true);
            translation.closeConnection();
            pd.show();

            super.onPreExecute();
        }

        @Override
        protected QuestionLoadedTransferObj doInBackground(Void... params) {
            LearnignCenterSchoolCurriculumServerOperation serverObj = new LearnignCenterSchoolCurriculumServerOperation();
            //QuestionLoadedTransferObj questionData = serverObj.getWordProblemQuestions(grade);

            //changes for Spanish Changes
            QuestionLoadedTransferObj questionData = new QuestionLoadedTransferObj();
            if(CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH)
                questionData = serverObj.getWordProblemQuestions(grade);
            else
                questionData= serverObj.getWordProblemQuestionsForSpanish(grade, CommonUtils.SPANISH);

			/*//changes for dialog loading wheel
			SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl = new SchoolCurriculumLearnignCenterimpl(context);
			schooloCurriculumImpl.openConnection();
			schooloCurriculumImpl.deleteFromWordProblemCategoriesbyGrade(grade);
			schooloCurriculumImpl.deleteFromWordProblemsSubCategoriesByCategoryId(questionData.getCategoryList());
			schooloCurriculumImpl.deleteFromWordProblemsQuestionsCategoryId(questionData.getCategoryList());

			schooloCurriculumImpl.insertIntoWordProblemCategories(questionData.getCategoryList(), grade);
			schooloCurriculumImpl.insertIntoWordProblemsSubCategories(questionData.getCategoryList());
			schooloCurriculumImpl.insertIntoWordProblemsQuestions(questionData.getQuestionList());*/
            //changes for Spanish Changes
            if(questionData != null){
                SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl = new SchoolCurriculumLearnignCenterimpl(context);
                schooloCurriculumImpl.openConnection();

                if(CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH){
                    //changes for dialog loading wheel
                    schooloCurriculumImpl.deleteFromWordProblemCategoriesbyGrade(grade);
                    schooloCurriculumImpl.deleteFromWordProblemsSubCategoriesByCategoryId(questionData.getCategoryList());
                    schooloCurriculumImpl.deleteFromWordProblemsQuestionsCategoryId(questionData.getCategoryList());

                    schooloCurriculumImpl.insertIntoWordProblemCategories(questionData.getCategoryList(), grade);
                    schooloCurriculumImpl.insertIntoWordProblemsSubCategories(questionData.getCategoryList());
                    schooloCurriculumImpl.insertIntoWordProblemsQuestions(questionData.getQuestionList());
                }else{
                    SpanishChangesImpl implObj = new SpanishChangesImpl(context);
                    implObj.openConn();

                    implObj.deleteFromWordProblemCategoriesbyGrade(grade);
                    implObj.deleteFromWordProblemsSubCategoriesByCategoryId(questionData.getCategoryList());
                    implObj.deleteFromWordProblemsQuestionsCategoryId(questionData.getCategoryList());

                    implObj.insertIntoWordProblemCategories(questionData.getCategoryList(), grade);
                    implObj.insertIntoWordProblemsSubCategories(questionData.getCategoryList());
                    implObj.insertIntoWordProblemsQuestions(questionData.getQuestionList());
                    implObj.closeConn();
                }

                for(int i = 0 ; i < questionData.getQuestionList().size() ; i ++ ){

                    WordProblemQuestionTransferObj questionObj = questionData.getQuestionList().get(i);

                    if(questionObj.getQuestion().contains(".png"))
                        imageNameList.add(questionObj.getQuestion());
                    if(questionObj.getOpt1().contains(".png"))
                        imageNameList.add(questionObj.getOpt1());
                    if(questionObj.getOpt2().contains(".png"))
                        imageNameList.add(questionObj.getOpt2());
                    if(questionObj.getOpt3().contains(".png"))
                        imageNameList.add(questionObj.getOpt3());
                    if(questionObj.getOpt4().contains(".png"))
                        imageNameList.add(questionObj.getOpt4());
                    if(questionObj.getOpt5().contains(".png"))
                        imageNameList.add(questionObj.getOpt5());
                    if(questionObj.getOpt6().contains(".png"))
                        imageNameList.add(questionObj.getOpt6());
                    if(questionObj.getOpt7().contains(".png"))
                        imageNameList.add(questionObj.getOpt7());
                    if(questionObj.getOpt8().contains(".png"))
                        imageNameList.add(questionObj.getOpt8());
                    if(!questionObj.getImage().equals(""))
                        imageNameList.add(questionObj.getImage());
                }

                if(!schooloCurriculumImpl.isImageTableExist()){
                    schooloCurriculumImpl.createSchoolCurriculumImageTable();
                }
                schooloCurriculumImpl.closeConnection();
            }
            //end changes
            return questionData;

        }

        @Override
        protected void onPostExecute(QuestionLoadedTransferObj questionData) {

            pd.cancel();

            if(questionData != null){
                DawnloadImagesFromserver serverObj = new DawnloadImagesFromserver(imageNameList, context);
                Thread imageDawnLoadThrad = new Thread(serverObj);
                imageDawnLoadThrad.start();

                schoolCurriculum(grade);
            }else{
                CommonUtils.showInternetDialog(context);
            }

            super.onPostExecute(questionData);
        }
    }

    /**
     * This class get learn center wprd problem time
     * @author Yashwant Singh
     *
     */
    class GetTimeLearnCenter extends AsyncTask<Void, Void, LearnCenterTimeObj>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected LearnCenterTimeObj doInBackground(Void... params) {
            LearnignCenterSchoolCurriculumServerOperation serverObj
                    = new LearnignCenterSchoolCurriculumServerOperation();
            LearnCenterTimeObj timeObj = serverObj.getTimeLearnCenter();
            return timeObj;
        }

        @SuppressWarnings("deprecation")
        @Override
        protected void onPostExecute(LearnCenterTimeObj timeObj) {
            if(timeObj != null){
                Date date = new Date();
                SharedPreferences learnCenterTimePreff = getSharedPreferences(ICommonUtils.LEARN_CENTER_TIME_PREFF, 0);
                SharedPreferences.Editor editor = learnCenterTimePreff.edit();
                editor.putLong(ICommonUtils.LEARN_CENTER_TIME, timeObj.getRemainingTime());
                editor.putLong(ICommonUtils.LEARN_CENTER_TIME_API_CALL_TIME, date.getTime());
                editor.commit();

                SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl
                        (NewLearnignCenter.this);
                schoolImpl.openConnection();
                schoolImpl.deleteFromWordLearnCenterTime();
                schoolImpl.insertIntoWordLearnCenterTime(timeObj.getCategoryTimeList());
                schoolImpl.closeConnection();
            }

            super.onPostExecute(timeObj);
        }
    }

    //chages for Spanish
    /**
     * This asyncktask get date details for Spanish word problem questions
     * @author Yashwant Singh
     *
     */
    public class GetWordQuestionsUpdateDatesLang extends AsyncTask<Void, Void, ArrayList<UpdatedInfoTransferObj>>{

        private int lang  = 1;
        private ProgressDialog pd;
        private Context context;
        private int grade;
        private boolean isFirstTimeLoaded;
        private int callingActivity = 0;

        GetWordQuestionsUpdateDatesLang(int lang , Context context , boolean isFirstTimeLoaded
                ,int grade , int callingActivity){
            this.lang = lang;
            this.context = context;
            this.isFirstTimeLoaded = isFirstTimeLoaded;
            this.grade = grade;
            this.callingActivity = callingActivity;
        }

        @Override
        protected void onPreExecute() {
            if(!isFirstTimeLoaded){
                pd = CommonUtils.getProgressDialog(context);
                pd.show();
            }
            super.onPreExecute();
        }

        @Override
        protected ArrayList<UpdatedInfoTransferObj> doInBackground(Void... params) {
            SpanishServerOperation serverObj = new SpanishServerOperation();
            ArrayList<UpdatedInfoTransferObj> updatedList = serverObj.getUpdateDateDetaile(lang);
            return updatedList;
        }

        @Override
        protected void onPostExecute(ArrayList<UpdatedInfoTransferObj> updatedList) {
            if(!isFirstTimeLoaded){
                pd.cancel();
                if(updatedList != null){
                    SpanishChangesImpl spanishImplObj = new SpanishChangesImpl(context);
                    spanishImplObj.openConn();
                    String updatedDateFromDatabase = spanishImplObj.getUpdatedDate(grade);
                    if(callingActivity == 1){//for main Activity
                        if(CommonUtils.isUpdateDateChange(updatedDateFromDatabase,grade,updatedList))
                        //if(true)
                        {
                            String serverUpdatedDate = CommonUtils.getServerDate(grade , updatedList);
                            spanishImplObj.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);

                            new GetWordProblemQuestion(grade , context)
                                    .execute(null,null,null);
                        }else{
                            schoolCurriculum(grade);
                        }
                    }
                    spanishImplObj.closeConn();
                }else{
                    CommonUtils.showInternetDialog(context);
                }
            }

            super.onPostExecute(updatedList);
        }
    }

    //for show by ccss

    /**
     * This method go to next screen after click on show by ccss
     */
    private void goOnMainStandardsList(){
        Intent intent = new Intent(this , ShowByCCSSMainStandards.class);
        intent.putExtra("selectedGrade", CommonUtils.selectedGrade);
        intent.putExtra("isOpenCat", isSubCatOpen);
        startActivity(intent);
    }

    /**
     * This method created when new changes Assessment Test
     * @param
     * @param
     */
    private void goOnMainStandardsScrennAfterClickOnShowByCCSS(){
        AssessmentTestImpl implObj = new AssessmentTestImpl(NewLearnignCenter.this);
        implObj.openConnection();
        boolean isStandardLoaded = implObj.isStandard(CommonUtils.selectedGrade);
        implObj.closeConnection();

        if(!isStandardLoaded && CommonUtils.isInternetConnectionAvailable(this)){
            new GetUpdatedDateAssessment("","").execute(null,null,null);
        }else{
            goOnMainStandardsList();
        }
    }

    /**
     * Get Assessment date from server for standards
     * @author Yashwant Singh
     *
     */
    class GetUpdatedDateAssessment extends AsyncTask<Void, Void, String>{

        private String userId;
        private String playerId;
        private ProgressDialog pd;
        private int lang = 1;

        GetUpdatedDateAssessment(String userId , String playerId){
            this.userId = userId;
            this.playerId = playerId;
            if(CommonUtils.getUserLanguageCode(NewLearnignCenter.this) == CommonUtils.ENGLISH)
                lang = CommonUtils.ENGLISH;
            else
                lang = CommonUtils.SPANISH;
        }

        @Override
        protected void onPreExecute() {
            pd = CommonUtils.getProgressDialog(NewLearnignCenter.this);
            pd.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            AssessmentTestServerOperation serverObj = new AssessmentTestServerOperation();
            String date = serverObj.getUpdatedDateAssessment(lang);
            return date;
        }

        @Override
        protected void onPostExecute(String date) {
            pd.cancel();

            if(date != null){
                SharedPreferences sharedPrefForData = getSharedPreferences
                        (ICommonUtils.DATE_ASSESSMENT_STANDARD_DOWNLOAD, 0);

                downLoadStandars(sharedPrefForData, date , lang , userId , playerId);
            }else{
                CommonUtils.showInternetDialog(NewLearnignCenter.this);
            }
            super.onPostExecute(date);
        }
    }

    /**
     * This method call when ready to download standards
     * @param sharedPrefForData
     * @param date
     * @param lang
     */
    private void downLoadStandars(SharedPreferences sharedPrefForData , String date , int lang
            ,String userId , String playerId){
        SharedPreferences.Editor editor = sharedPrefForData.edit();
        editor.putString("assessmentStandardDate", date);
        editor.commit();

        new GetAssessmentStandards(userId, playerId, lang).execute(null,null,null);
    }

    /**
     * Get Assessment Standards
     * @author Yashwant Singh
     *
     */
    class GetAssessmentStandards extends AsyncTask<Void, Void, AssessmentStandardDto>{

        private ProgressDialog pd;
        private int lang;
        private String userId;
        private String playerId;

        GetAssessmentStandards(String userId , String playerId , int lang){
            this.lang = lang;
            this.userId = userId;
            this.playerId = playerId;
        }

        @Override
        protected void onPreExecute() {
            pd = CommonUtils.getProgressDialog(NewLearnignCenter.this);
            pd.show();
            super.onPreExecute();
        }

        @Override
        protected AssessmentStandardDto doInBackground(Void... params) {
            AssessmentTestServerOperation serverObj = new AssessmentTestServerOperation();
            AssessmentStandardDto assessmentStandard = serverObj.getAssessmentStandards(lang);
            if(assessmentStandard != null){
                try{
                    AssessmentTestImpl implObj = new AssessmentTestImpl(NewLearnignCenter.this);
                    implObj.openConnection();
                    implObj.deleteFromWordAssessmentStandards();
                    implObj.deleteFromWordAssessmentSubStandards();
                    implObj.deleteFromWordAssessmentSubCategoriesInfo();
                    implObj.insertIntoWordAssessmentStandards(assessmentStandard.getStandardDataList());
                    implObj.insertIntoWordAssessmentSubStandards(assessmentStandard.getStandardDataList());
                    implObj.insertIntoWordAssessmentSubCategoriesInfo(assessmentStandard.getCategoriesList());
                    implObj.closeConnection();
                }catch(Exception e){
                    Log.e("AssessmentClass", "No Data on Server For Stabdards " + e.toString());
                }
            }
            return assessmentStandard;

        }

        @Override
        protected void onPostExecute(AssessmentStandardDto assessmentStandard) {
            pd.cancel();
            if(assessmentStandard != null)
                goOnMainStandardsList();
            else
                CommonUtils.showInternetDialog(NewLearnignCenter.this);

            super.onPostExecute(assessmentStandard);
        }
    }

    @Override
    protected void onResume() {
        //for show ad dialog
        CommonUtils.showAdDialog(this);
        //end ad dialog
        //this.RefresgUIWhenNeeded();
        super.onResume();
    }

    private void setWidgetsReferesncesForNewAppChanges(){
        layoutCheckBox = (RelativeLayout) findViewById(R.id.layoutCheckBox);
        layoutSolveEquation = (RelativeLayout) findViewById(R.id.layoutSolveEquation);
        layoutSchrollCurriculum = (RelativeLayout) findViewById(R.id.layoutSchrollCurriculum);
        layoutShowTimer = (RelativeLayout) findViewById(R.id.layoutShowTimer);
    }

    private void setVisibilityOfLayoutBasedOnAppVersion() {
        if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS){
            layoutSolveEquation.setVisibility(RelativeLayout.VISIBLE);
            layoutSchrollCurriculum.setVisibility(RelativeLayout.VISIBLE);
        }
    }

    private void setShowByCSSButtonVisibility(boolean isVisible){
        if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS){
            btnShowByCCSS.setVisibility(Button.GONE);
            return ;
        }

        if(isVisible){
            btnShowByCCSS.setVisibility(Button.VISIBLE);
        }else{
            btnShowByCCSS.setVisibility(Button.GONE);
        }
    }

    private void updateUIAfterPurchaseSuccess() {
        isFromMain = false;
        this.onCreate(null);
    }

    private boolean getUnlockApplicationStatusNewInApp(int itemId){
        if(MathFriendzyHelper.isAppUnlockCategoryPurchased(this)){
            appStatus = 1;
            return true;
        }
        boolean isUnlock = false;
        SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
        if (this.getApplicationUnLockStatus(100, sharedPreffPlayerInfo.getString("userId", "")) == 1) {
            isUnlock = true;
        } else {
            if (this.getApplicationUnLockStatus(itemId,
                    sharedPreffPlayerInfo.getString("userId", "")) == 1) {
                isUnlock = true;
            } else {
                isUnlock = false;
            }
        }
        return isUnlock;
    }

    private Activity getCurrentActivityObj(){
        return this;
    }

    /*private void RefresgUIWhenNeeded() {
        //New InApp Changes
        if(isNeedToRefreshUI){
            isNeedToRefreshUI = false;
            isFromMain = false;
            this.onCreate(null);
        }
    }*/

    //New InApp Changes
    private void clickOnPracticeSkillLockLevel(final int level, final RelativeLayout clickedLayout){
        //new InApp Changes
        if(!CommonUtils.isInternetConnectionAvailable(this)){
            CommonUtils.showInternetDialog(this);
            return ;
        }

        MathFriendzyHelper.showProgressDialog(progressDialogForNewInApp);
        MathFriendzyHelper.getAppUnlockStatusFromServerAndSaveIntoLocal(this ,
                CommonUtils.getUserId(this) , false , new HttpResponseInterface() {
                    @Override
                    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                        if(requestCode == MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE){
                            MathFriendzyHelper.hideProgressDialog(progressDialogForNewInApp);
                            return ;
                        }
                        GetAppUnlockStatusResponse response =
                                (GetAppUnlockStatusResponse) httpResponseBase;
                        if(response.getResult()
                                .equalsIgnoreCase(MathFriendzyHelper.SUCCESS)
                                && response.getAppUnlockStatus() == MathFriendzyHelper.APP_UNLOCK){
                            MathFriendzyHelper.hideProgressDialog(progressDialogForNewInApp);
                            updateUIAfterPurchaseSuccess();
                            return ;
                        }

                        MathFriendzyHelper.getInAppStatusAndShowDialog(getCurrentActivityObj(),
                                MathFriendzyHelper.SHOW_UNLOCK_FOR_LEARNING_CENTER ,
                                new HttpResponseInterface() {//interface call when save true from the server otherwise open dialog
                                    @Override
                                    public void serverResponse(HttpResponseBase httpResponseBase,
                                                               int requestCode) {
                                        MathFriendzyHelper.hideProgressDialog(progressDialogForNewInApp);
                                        if(requestCode ==
                                                MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE){
                                            return ;
                                        }
                                        updateUIAfterPurchaseSuccess();
                                    }
                                } , false , response , new OnPurchaseDone() {
                                    @Override
                                    public void onPurchaseDone(boolean isDone) {
                                        if(isDone) {
                                            updateUIAfterPurchaseSuccess();
                                        }
                                    }
                                });
                    }
                });

        /*MathFriendzyHelper.showProgressDialog(progressDialogForNewInApp);
        MathFriendzyHelper.getInAppStatusAndShowDialog(getCurrentActivityObj(),
                MathFriendzyHelper.SHOW_UNLOCK_FOR_LEARNING_CENTER ,
                new HttpResponseInterface() {//interface call when save true from the server otherwise open dialog
                    @Override
                    public void serverResponse(HttpResponseBase httpResponseBase,
                                               int requestCode) {
                        MathFriendzyHelper.hideProgressDialog(progressDialogForNewInApp);
                        if(requestCode ==
                                MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE){
                            return ;
                        }
                        //isNeedToRefreshUI = true;
                        //clickOnLevelCommon(level , clickedLayout);
                        updateUIAfterPurchaseSuccess();
                    }
                } , false);*/
    }

    private void clickOnSchoolCurriculumLockLevel(final int subCategoryId, final int categoryId,
                                                  final RelativeLayout clickedChild , final String subCatName){
        if(!CommonUtils.isInternetConnectionAvailable(this)){
            CommonUtils.showInternetDialog(this);
            return ;
        }

        MathFriendzyHelper.showProgressDialog(progressDialogForNewInApp);
        MathFriendzyHelper.getAppUnlockStatusFromServerAndSaveIntoLocal(this ,
                CommonUtils.getUserId(this) , false , new HttpResponseInterface() {
                    @Override
                    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                        if(requestCode == MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE){
                            MathFriendzyHelper.hideProgressDialog(progressDialogForNewInApp);
                            return ;
                        }
                        GetAppUnlockStatusResponse response =
                                (GetAppUnlockStatusResponse) httpResponseBase;
                        if(response.getResult()
                                .equalsIgnoreCase(MathFriendzyHelper.SUCCESS)
                                && response.getAppUnlockStatus() == MathFriendzyHelper.APP_UNLOCK){
                            MathFriendzyHelper.hideProgressDialog(progressDialogForNewInApp);
                            openSchoolCurriculumPlayScreen(subCategoryId , categoryId ,
                                    clickedChild , subCatName);
                            return ;
                        }

                        MathFriendzyHelper.getInAppStatusAndShowDialog(getCurrentActivityObj(),
                                MathFriendzyHelper.SHOW_UNLOCK_FOR_LEARNING_CENTER ,
                                new HttpResponseInterface() {//interface call when save true from the server otherwise open dialog
                                    @Override
                                    public void serverResponse(HttpResponseBase httpResponseBase,
                                                               int requestCode) {
                                        MathFriendzyHelper.hideProgressDialog(progressDialogForNewInApp);
                                        if(requestCode ==
                                                MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE){
                                            return ;
                                        }
                                        openSchoolCurriculumPlayScreen(subCategoryId , categoryId ,
                                                clickedChild , subCatName);
                                    }
                                } , false , response , new OnPurchaseDone() {
                                    @Override
                                    public void onPurchaseDone(boolean isDone) {
                                        if(isDone) {
                                            openSchoolCurriculumPlayScreen(subCategoryId, categoryId,
                                                    clickedChild, subCatName);
                                        }
                                    }
                                });
                    }
                });
    }
}
