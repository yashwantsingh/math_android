package com.mathfriendzy.controller.learningcenter;

public interface IOperationId 
{
	int ADDITION    						= 1;
	int SUBTRACTION 						= 2;
	int MULTIPLICATION 						= 3;
	int DIVISION    						= 4;
	int FRACTION_ADDITION_SUBTRACTION 		= 5;
	int FRACTION_MULTIPLICATION_DIVISION 	= 6;
	int DECIMAL_ADDITION_SUBTRACTION 		= 7;
	int DECIMAL_MULTIPLICATION_DIVISION 	= 8;
	int NEGATIVE_ADDITION_SUBTRACTION 		= 9;
	int NEGATIVE_MULTIPLICATION_DIVISION 	= 10;
}
