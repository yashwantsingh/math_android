package com.mathfriendzy.controller.learningcenter;

import static com.mathfriendzy.controller.learningcenter.IOperationId.ADDITION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.DECIMAL_ADDITION_SUBTRACTION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.DECIMAL_MULTIPLICATION_DIVISION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.DIVISION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.FRACTION_ADDITION_SUBTRACTION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.FRACTION_MULTIPLICATION_DIVISION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.MULTIPLICATION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.NEGATIVE_ADDITION_SUBTRACTION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.NEGATIVE_MULTIPLICATION_DIVISION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.SUBTRACTION;
import static com.mathfriendzy.utils.ICommonUtils.LEARNING_CENTER_EQUATION_SOLVE;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.MathScoreTransferObj;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.PlaySound;

/**
 * This Activity is open when user want to solve equation after selecting the type of equation in Choose equation
 * Activity
 * @author Yashwant Singh
 *
 */
public abstract class LearningCenterEquationSolve extends ActBaseClass implements OnClickListener
{
    protected TextView txtTopBarText  = null;
    protected Button btnRoughWork     = null;
    protected ImageView imgCross1     = null;
    protected ImageView imgCross2     = null;
    protected ImageView imgCross3     = null;
    protected ImageView imgWrong      = null;

    protected ImageView    imgResultLine    		= null;
    protected LinearLayout linearLayoutResultRow 	= null;
    protected TextView     txtBlackLine           	= null;
    protected TextView     txtNumber1             	= null;
    protected TextView     txtNumber2             	= null;
    protected TextView     txtCarry					= null;
    protected ImageView    imgSign                	= null;
    protected RelativeLayout relativeLayout         = null;

    protected Button btnDot               = null;
    protected Button btnMinus             = null;
    protected Button btn1                 = null;
    protected Button btn2                 = null;
    protected Button btn3                 = null;
    protected Button btn4                 = null;
    protected Button btn5                 = null;
    protected Button btn6                 = null;
    protected Button btn7                 = null;
    protected Button btn8                 = null;
    protected Button btn9                 = null;
    protected Button btn0                 = null;
    protected Button btnArrowBack         = null;
    protected Button btnGo                = null;

    protected TextView txtResultCarry     = null;

    //added for fraction number
    protected ImageView imgResultLineFirst  = null;
    protected ImageView imgResultLineSecond = null;
    protected ImageView imgResultLineThird  = null;
    protected LinearLayout linearLayoutQuotient 	= null;
    protected LinearLayout linearLayoutNumerator 	= null;
    protected LinearLayout linearLayoutDenominator 	= null;
    protected TextView     txtnumber1Numerator      = null;
    protected TextView     txtnumber1Denominator    = null;
    protected TextView     txtnumber2Numerator      = null;
    protected TextView     txtnumber2Denominator    = null;

    //added for multiplication
    protected LinearLayout linearLayoutMultiple     = null;
    protected LinearLayout linearLayoutResult       = null;

    //added for division

    protected LinearLayout linearLayoutRowsForDivision = null;

    protected ArrayList<Integer> equationsIdList 		= null;
    protected ArrayList<TextView> resultTextViewList 	= null;

    protected final String TAG = this.getClass().getSimpleName();
    protected int i = 0 ;

    protected Random randomGenerator      = null;
    protected int noOfDigitsInFisrtNumber = 0;
    protected int noOfDigitsInSecondNumber= 0;
    protected int noOfDigitsInResult      = 0;

    protected  int maxWidth          = 100;
    protected  int SCREEN_DENISITY   = 240;//Screen density for setting layout for the screen based on density

    //changes for sub classes
    protected String number2              = null;
    protected int numberOfRowsforDivision = 0;
    protected int numberOfBoxesForDivision= 0;
    protected String number1              = null;
    protected String result               = null;
    protected int numberOfBoxesForMultiplication = 0;
    protected RelativeLayout child        	   = null;
    protected int index 				  = 0;// index which holding the selected result textBox
    protected int selectedIndex 		  = 0;//this index contain the currently selected box in result
    protected boolean isYellowboxForFraction = false;//this is for set background of the first result digit is yellow
    protected int MAX_WIDTH_FOR_FRACTION_TEXT = 25;//for setting text width based on the number of
    protected int widthForFractionLayout = 45;
    protected int max_width_for_layout_maultiplication = 40;//for setting maximum width for inflated layout
    protected boolean isMiddleSelectedForDivision = false;
    protected int operationId             = 0;
    protected int startIndex = 0;//for multiplication click on back arrow
    protected String operator           = "";
    protected boolean isCarrySelected   = false;
    protected boolean isMinusSelected   = false;
    protected boolean isDotSelected     = false;
    protected TextView  txtSelectedTextWithDot   = null;

    protected boolean isTab = false;
    protected final int TAB_HEIGHT_1024 = 1024; // tab with 1024 height


    //for crrect timer valeus
    protected int TOTAL_TIME_TO_PLAY = 180;
    private static int MAX_SECOND_TO_BE_ADDED = 60;


    //for correct answer sleep time to show green box
    private final int SLEEP_TIME_FOR_GREEN_BOX = 750;//1000*3/4

    /**
     * This method get the array list from intent which contain the equation category id  which is selected in previoue
     * and get equation from the database based on these category id
     */
    protected abstract void getIntentValue();

    /**
     * This method show equation data
     */
    protected abstract void showEqautionData(LearningCenterTransferObj learningObj);

    /**
     * This method set sign corresponding to the operation id
     */
    protected abstract void setSign();


    //for sound for workarea
    public static boolean isClickOnWorkArea = false;
    public static boolean isStopSecondPlayerProgress = false;
    public static PlaySound playsound  = null;


    //changes 22 Jan 2015
    public static final int DEVISION_DESIGN = 4;
    public static final int MULTIPLICATION_DESIGN = 5;
    public static final int FRACTION_DESIGN = 6;
    private final int NUMBER_OF_MAX_ATTEMPTED = 3;

    /**
     * This method set the widgets references from the layout to the widgets Object
     */
    protected void setWidgetsReferences()
    {
        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "inside setWidgetsReferences()");

        txtTopBarText 	= (TextView) findViewById(R.id.labelTop);
        btnRoughWork 	= (Button) findViewById(R.id.btnRoughWork);
        imgCross1 		= (ImageView) findViewById(R.id.imgCross1);
        imgCross2 		= (ImageView) findViewById(R.id.imgCross2);
        imgCross3 		= (ImageView) findViewById(R.id.imgCross3);

        btnDot 		= (Button) findViewById(R.id.btnDot);
        btnMinus 	= (Button) findViewById(R.id.btnMinus);
        btn1 		= (Button) findViewById(R.id.btn1);
        btn2		= (Button) findViewById(R.id.btn2);
        btn3 		= (Button) findViewById(R.id.btn3);
        btn4 		= (Button) findViewById(R.id.btn4);
        btn5 		= (Button) findViewById(R.id.btn5);
        btn6 		= (Button) findViewById(R.id.btn6);
        btn7 		= (Button) findViewById(R.id.btn7);
        btn8 		= (Button) findViewById(R.id.btn8);
        btn9 		= (Button) findViewById(R.id.btn9);
        btn0 		= (Button) findViewById(R.id.btn0);
        btnArrowBack= (Button) findViewById(R.id.btnArrowBack);
        btnGo 		= (Button) findViewById(R.id.btnGo);

        relativeLayout = (RelativeLayout) findViewById(R.id.layoutNumber);

        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "outside setWidgetsReferences()");
    }


    /**
     * This method return the math equation from the database based on equation id one by one
     * @param index
     * @return
     */
    protected LearningCenterTransferObj getEquation(int index)
    {
        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "inside LearningCenterTransferObj()");

        LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
        learningCenterObj.openConn();
        LearningCenterTransferObj learningObj = learningCenterObj.getEquation(equationsIdList.get(index));
        learningCenterObj.closeConn();

        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "outside LearningCenterTransferObj()");

        return learningObj;
    }

    /**
     * This Method return the string from ArrayList Of digits
     * @param arrayList
     * @return
     */
    protected String getStringFromArrayList(ArrayList<String> arrayList)
    {
        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "inside getStringFromArrayList()");

        StringBuilder strBuilder = new StringBuilder("");

        for(int i = 0 ; i < arrayList.size() ; i ++ )
        {
            if(i == arrayList.size() - 1)
            {
                if(arrayList.get(i).contains(".") || arrayList.get(i).contains("-"))
                    strBuilder.append(arrayList.get(i));
                else
                {
                    if(isTab)//changes for tab
                        strBuilder.append("  " + arrayList.get(i));
                    else
                        strBuilder.append(" " + arrayList.get(i));
                }

            }
            else
            {
                if(arrayList.get(i).contains(".") || arrayList.get(i).contains("-")){
                    if(isTab)//changes for tab
                        strBuilder.append(arrayList.get(i) + " ");
                    else
                        strBuilder.append(arrayList.get(i) + " ");
                }else{
                    if(isTab)//changes for tab
                        strBuilder.append("  " + arrayList.get(i) + " ");
                    else
                        strBuilder.append(" " + arrayList.get(i) + " ");
                }
            }

        }

        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "outside getStringFromArrayList()");

        return strBuilder.toString();
    }


    /**
     * This method creates dynamic text view for result
     * @param listOfDigits
     * @param layout
     */
    protected abstract void createResultLayout(ArrayList<String> listOfDigits, LinearLayout layout);

    /**
     * This method return the number of digits after removing . and - sign into arrayList of string
     * @param stringDigits
     * @return
     */
    protected ArrayList<String> getDigit(String stringDigits)
    {
        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "inside getDigit()");
        ArrayList<String> digitList = new ArrayList<String>();

        try{
            int i = 0 ;
            while(i < stringDigits.length())
            {
                if(stringDigits.charAt(i) == '.')
                {
                    digitList.add(stringDigits.charAt(i) + "" + stringDigits.charAt(i+1) + "");
                    i++;
                }
                else if(stringDigits.charAt(i) == '-')
                {
                    digitList.add(stringDigits.charAt(i) + "" + stringDigits.charAt(i+1) + "");
                    i++;
                }
                else
                    digitList.add(stringDigits.charAt(i) + "");

                i++;
            }
        }
        catch(Exception e){
            Log.e(TAG, " inside getDigit Error while getting digits " + e.toString());
        }

        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "outside getDigit()");

        return digitList;
    }


    /**
     * This method set the text values from the translation
     */
    protected void setWidgetsValuesFromtranselation()
    {
        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "inside setWidgetsValuesFromtranselation()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopBarText.setText(transeletion.getTranselationTextByTextIdentifier("mfTitleHomeScreen"));
        transeletion.closeConnection();

        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "outside setWidgetsValuesFromtranselation()");
    }

    /**
     * This method disable the working of keyboard
     */
    protected void keyBoardDisable()
    {
        btnGo.setClickable(false);
        //txtCarry.setClickable(false);
        btnDot.setClickable(false);
        btnMinus.setClickable(false);
        btn1.setClickable(false);
        btn2.setClickable(false);
        btn3.setClickable(false);
        btn4.setClickable(false);
        btn5.setClickable(false);
        btn6.setClickable(false);
        btn7.setClickable(false);
        btn8.setClickable(false);
        btn9.setClickable(false);
        btn0.setClickable(false);
        btnArrowBack.setClickable(false);
    }


    /**
     * This method enable the working of keyboard
     */
    protected void keyBoardEnable()
    {
        btnGo.setClickable(true);
        //txtCarry.setClickable(true);
        btnDot.setClickable(true);
        btnMinus.setClickable(true);
        btn1.setClickable(true);
        btn2.setClickable(true);
        btn3.setClickable(true);
        btn4.setClickable(true);
        btn5.setClickable(true);
        btn6.setClickable(true);
        btn7.setClickable(true);
        btn8.setClickable(true);
        btn9.setClickable(true);
        btn0.setClickable(true);
        btnArrowBack.setClickable(true);
    }

    /**
     * This method set the number of rows and boxes for decimal division
     */
    protected void setNumberOfRowsAndboxForDivisionForDecimal()
    {
        int dotPos = 0;
        int counter = 0;

        for(int i = number2.length() - 1 ; i >= 0 ; i --)
        {
            if(number2.charAt(i) == '.')
            {
                dotPos = counter;
                break;
            }
            counter ++ ;
        }

        if(noOfDigitsInFisrtNumber == 1 && dotPos == 1)
        {
            numberOfRowsforDivision = 4;
            numberOfBoxesForDivision = 2;
        }
        else if(noOfDigitsInFisrtNumber == 2 && dotPos == 1)
        {
            numberOfRowsforDivision = 6;
            numberOfBoxesForDivision = 3;
        }
        else if(noOfDigitsInFisrtNumber == 3 && dotPos == 1)
        {
            numberOfRowsforDivision = 6;
            numberOfBoxesForDivision = 4;
        }
        else if(noOfDigitsInFisrtNumber == 1 && dotPos == 2)
        {
            numberOfRowsforDivision = 6;
            numberOfBoxesForDivision = 3;
        }
        else if(noOfDigitsInFisrtNumber == 2 && dotPos == 2)
        {
            numberOfRowsforDivision = 6;
            numberOfBoxesForDivision = 4;
        }
        else if(noOfDigitsInFisrtNumber == 3 && dotPos == 2)
        {
            numberOfRowsforDivision = 6;
            numberOfBoxesForDivision = 5;
        }

        this.setNumberOfRowsAndboxForDivisionForBoth();

        //add number of extra boxes
        int noOfExtraBoxes = 0 ;
        if(Integer.parseInt(number1.replace(".", "").charAt(0) + "")/Integer.parseInt(number2.replace(".", "")) == 0)
        {
            noOfExtraBoxes = 1;
        }

        if(number1.length() > 2)
        {
            if(Integer.parseInt(number1.replace(".", "").substring(0,2) + "")/Integer.parseInt(number2.replace(".", "")) == 0)
            {
                noOfExtraBoxes = 2;
            }
        }

        numberOfBoxesForDivision = noOfDigitsInResult + noOfExtraBoxes;

        if(noOfExtraBoxes > 0 && result.startsWith("0."))
        {
            numberOfBoxesForDivision = numberOfBoxesForDivision - 1;
        }

        int lengthOfDivident  = numberOfBoxesForDivision;

        if(!(lengthOfDivident == noOfDigitsInFisrtNumber))
        {
            for( int i = 0 ; i < (lengthOfDivident - noOfDigitsInFisrtNumber) ; i ++)
            {
                number1 = number1 + "0";
            }
        }
        else
        {
            numberOfBoxesForDivision = noOfDigitsInFisrtNumber;
        }
    }

    /**
     * This method set the number of rows and box for both case of division either decimal or Normal
     */
    protected void setNumberOfRowsAndboxForDivisionForBoth()
    {
        if(noOfDigitsInResult == 4 && noOfDigitsInSecondNumber == 1)
        {
            numberOfRowsforDivision = 6;
            numberOfBoxesForDivision = 4;
        }
        if(noOfDigitsInResult == 4 && noOfDigitsInSecondNumber == 2)
        {
            numberOfRowsforDivision = 8;
            numberOfBoxesForDivision = 5;
        }
        if(noOfDigitsInResult == 5)
        {
            numberOfRowsforDivision = 10;
            numberOfBoxesForDivision = 5;
        }
    }

    /**
     * This method set the number of boxes for multiplication with decimal
     */
    protected void setnumberOfBoxesForMultiplicationForDecimal()
    {
        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "inside setnumberOfBoxesForMultiplicationForDecimal()");

        numberOfBoxesForMultiplication = noOfDigitsInFisrtNumber + noOfDigitsInSecondNumber;
        int dotPosInFirstNumber  = number1.indexOf(".");
        int dotPosInSecondNumber = number2.indexOf(".");

        //Log.e(TAG, "firstDotposition" + dotPosInFirstNumber + " second " +  dotPosInSecondNumber);

        if((dotPosInFirstNumber + dotPosInSecondNumber + 1) > numberOfBoxesForMultiplication)
            numberOfBoxesForMultiplication = dotPosInFirstNumber + dotPosInSecondNumber + 1;

        if(noOfDigitsInResult > numberOfBoxesForMultiplication)
            numberOfBoxesForMultiplication = noOfDigitsInResult;

        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "outside setnumberOfBoxesForMultiplicationForDecimal()");
    }

    /**
     * This method show the equation on which the addition and subtraction operation perform
     * for decimal as well as negative number or normal number
     */
    protected void operationPerformForAdditionSubtraction(LearningCenterTransferObj learningObj)
    {
        btnDot.setEnabled(true);
        btnMinus.setEnabled(true);

        relativeLayout.removeAllViews();
        //changes for reset layout
        this.resetNumberLayout();
        //end changes

        LayoutInflater inflater = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
        child = (RelativeLayout) inflater.inflate(R.layout.addition_subtraction_layout, null);
        relativeLayout.addView(child);

        if(noOfDigitsInFisrtNumber == noOfDigitsInResult)
        {
            RelativeLayout.LayoutParams lp1 = new RelativeLayout.LayoutParams((noOfDigitsInResult + 1) * maxWidth + 20,RelativeLayout.LayoutParams.WRAP_CONTENT);
            lp1.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
            relativeLayout.setLayoutParams(lp1);
        }
        else
        {
            if(noOfDigitsInResult > noOfDigitsInFisrtNumber)
            {
                RelativeLayout.LayoutParams lp1 = new RelativeLayout.LayoutParams(noOfDigitsInResult * maxWidth + 20,RelativeLayout.LayoutParams.WRAP_CONTENT);
                lp1.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
                relativeLayout.setLayoutParams(lp1);
            }
            else
            {
                RelativeLayout.LayoutParams lp1 = new RelativeLayout.LayoutParams((noOfDigitsInFisrtNumber + 1) * maxWidth + 20,RelativeLayout.LayoutParams.WRAP_CONTENT);
                lp1.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
                relativeLayout.setLayoutParams(lp1);
            }
        }


        txtNumber1 = (TextView) child.findViewById(R.id.Number1);
        txtNumber2 = (TextView) child.findViewById(R.id.Number2);
        imgSign    = (ImageView)child.findViewById(R.id.imgSign);
        txtCarry   = (TextView) child.findViewById(R.id.txtCarry);
        imgResultLine = (ImageView)child.findViewById(R.id.imgResultLine);
        linearLayoutResultRow = (LinearLayout)child.findViewById(R.id.linearLayoutResultRow);
        imgWrong   = (ImageView) child.findViewById(R.id.imgWrongAnswer);

        txtCarry.setOnClickListener(this);

        this.setSign();

        txtNumber1.setWidth(noOfDigitsInFisrtNumber * maxWidth);//set the width of the textView
        txtNumber2.setWidth(noOfDigitsInFisrtNumber * maxWidth);

        txtNumber1.setText(this.getStringFromArrayList(this.getDigit(learningObj.getNumber1Str())));
        txtNumber2.setText(this.getStringFromArrayList(this.getDigit(learningObj.getNumber2Str())));

        index         = this.getDigit(learningObj.getProductStr()).size() - 1;
        selectedIndex = this.getDigit(learningObj.getProductStr()).size() - 1;

        this.createResultBox(learningObj);
    }

    /**
     * This method create result box based on the result value digits
     */
    protected void createResultBox(LearningCenterTransferObj learningObj)
    {
        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "inside createResultBox()");

        this.createResultLayout(this.getDigit(learningObj.getProductStr()), linearLayoutResultRow);

        /**
         * Set the left margin for displaying line image
         */
        int marginleft = (((noOfDigitsInFisrtNumber - noOfDigitsInResult) + 1) * maxWidth) + 5;
        RelativeLayout.LayoutParams lp1 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
        lp1.addRule(RelativeLayout.BELOW,R.id.Number2);
        lp1.setMargins(marginleft, 0, 0, 0);
        imgResultLine.setLayoutParams(lp1);

        //changes for tab
        if(isTab)
            imgResultLine.setBackgroundResource(getResources().getIdentifier("tab_ml_equation_line" + this.getDigit(learningObj.getProductStr()).size() + "_ipad","drawable",getPackageName()));
        else
            imgResultLine.setBackgroundResource(getResources().getIdentifier("ml_equation_line" + this.getDigit(learningObj.getProductStr()).size(),"drawable",getPackageName()));

        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "outside createResultBox()");
    }

    /**
     * This method create the result boxes for fraction
     * @param resultValue
     * @param layuot
     */
    protected void createResultLayoutForFraction(String resultValue ,LinearLayout layuot)
    {
        for( int i = 0 ; i < resultValue.length() ; i ++ )
        {
            TextView txtView = new TextView(this);
            txtView.setId(i + 1);
            txtView.setGravity(Gravity.CENTER);
            //changes for tab
            if(isTab)
                txtView.setTextSize(45);
            else
                txtView.setTextSize(25);

            txtView.setTypeface(null, Typeface.BOLD);
            txtView.setTextColor(Color.parseColor("#3CB3FF"));
            if(isYellowboxForFraction){
                //changes for tab
                if(isTab)
                    txtView.setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
                else
                    txtView.setBackgroundResource(R.drawable.whitebox_small);
            }
            else
            {
                //changes for tab
                if(isTab)
                    txtView.setBackgroundResource(R.drawable.tab_yellow_ipad);
                else
                    txtView.setBackgroundResource(R.drawable.yellow_small);

                isYellowboxForFraction = true;
            }

            resultTextViewList.add(txtView);
            layuot.addView(txtView);

            txtView.setOnClickListener(new OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    for ( int i = 0 ; i < resultTextViewList.size() ; i ++)
                    {
                        if(v == resultTextViewList.get(i))
                        {
                            index = i;
                            //changes for tab
                            if(isTab)
                                resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
                            else
                                resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow_small);
                        }
                        else
                        {
                            //changes for tab
                            if(isTab)
                                resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
                            else
                                resultTextViewList.get(i).setBackgroundResource(R.drawable.whitebox_small);
                        }
                    }
                }
            });
        }
    }


    /**
     * This method dynamic create result layout boxes for multiplication
     */
    protected void createResultLayoutForMultiplication(LinearLayout layout)
    {
        for( int i = 0 ; i < numberOfBoxesForMultiplication ; i ++ )
        {
            TextView txtView = new TextView(this);
            txtView.setId(i + 1);
            txtView.setGravity(Gravity.CENTER);

            //changes for tab
            if(isTab)
                txtView.setTextSize(45);
            else
                txtView.setTextSize(25);

            txtView.setTypeface(null, Typeface.BOLD);
            txtView.setTextColor(Color.parseColor("#3CB3FF"));

            if(i == numberOfBoxesForMultiplication - 1 && isYellowboxForFraction == false)
            {

                //changes for tab
                if(isTab)
                    txtView.setBackgroundResource(R.drawable.tab_yellow_ipad);
                else
                    txtView.setBackgroundResource(R.drawable.yellow_small);

                isYellowboxForFraction = true;
            }
            else
            {
                //changes for tab
                if(isTab)
                    txtView.setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
                else
                    txtView.setBackgroundResource(R.drawable.whitebox_small);
            }

            txtView.setOnClickListener(new OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(getDigit(number2).size() > 1)
                    {
                        for( int  i = 0 ; i < resultTextViewList.size() ; i ++ )
                        {
                            if(v == resultTextViewList.get(i))
                            {
                                index = i;

                                if(i % numberOfBoxesForMultiplication > 0)
                                {
                                    //changes
                                    if(((index % numberOfBoxesForMultiplication) + 1) < numberOfBoxesForMultiplication - 1
                                            && !(resultTextViewList.get(index + 2).getText().equals("")))
                                    {
                                        selectedIndex = 0;
                                    }
                                    else
                                        selectedIndex = i;
                                }
                                else
                                {
                                    selectedIndex = 0;
                                }

                                //changes for tab
                                if(isTab)
                                    resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
                                else
                                    resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow_small);
                            }
                            else
                            {
                                //changes for tab
                                if(isTab)
                                    resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
                                else
                                    resultTextViewList.get(i).setBackgroundResource(R.drawable.whitebox_small);
                            }
                        }
                    }
                    else
                    {
                        for( int  i = 0 ; i < resultTextViewList.size() ; i ++ )
                        {
                            if(v == resultTextViewList.get(i))
                            {
                                selectedIndex = i;
                                index = i;
                                //changes for tab
                                if(isTab)
                                    resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
                                else
                                    resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow_small);
                            }
                            else
                            {
                                //changes for tab
                                if(isTab)
                                    resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
                                else
                                    resultTextViewList.get(i).setBackgroundResource(R.drawable.whitebox_small);
                            }
                        }
                    }
                }
            });
            resultTextViewList.add(txtView);
            layout.addView(txtView);
        }
    }

    /**
     * This method return the width for setting for fraction number
     * @param length1
     * @param length2
     * @return
     */
    protected int getWidthForFractionText(int length1,int length2)
    {
        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "inside getWidthForFractionText()");

        int width = 0;
        if(length1 > length2)
        {
            width = length1 * MAX_WIDTH_FOR_FRACTION_TEXT;
        }
        else
        {
            width = length2 * MAX_WIDTH_FOR_FRACTION_TEXT;
        }

        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "outside getWidthForFractionText()");
        return width;
    }

    /**
     * This method show the equations on which fraction operation is perform
     */
    protected void operatioPerformForFraction()
    {
        btnDot.setEnabled(false);
        btnMinus.setEnabled(false);

        relativeLayout.removeAllViews();

        //changes for reset layout
        this.resetNumberLayout();
        //end changes

        LayoutInflater inflater = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
        child = (RelativeLayout) inflater.inflate(R.layout.fraction_layout, null);
        relativeLayout.addView(child);

        imgResultLineFirst  = (ImageView) child.findViewById(R.id.imgResultLineFirst);
        imgResultLineSecond = (ImageView) child.findViewById(R.id.imgResultLineSecond);
        imgResultLineThird  = (ImageView) child.findViewById(R.id.imgResultLineThird);

        linearLayoutQuotient    = (LinearLayout) child.findViewById(R.id.linearLayoutQuotient);
        linearLayoutNumerator   = (LinearLayout) child.findViewById(R.id.linearLayoutNumerator);
        linearLayoutDenominator = (LinearLayout) child.findViewById(R.id.linearLayoutDenominator);

        txtnumber1Numerator     = (TextView) child.findViewById(R.id.txtnumber1Numerator);
        txtnumber1Denominator   = (TextView) child.findViewById(R.id.txtnumber1Denominator);
        txtnumber2Numerator     = (TextView) child.findViewById(R.id.txtnumber2Numerator);
        txtnumber2Denominator   = (TextView) child.findViewById(R.id.txtnumber2Denominator);

        imgSign    = (ImageView) child.findViewById(R.id.imgSign);
        imgWrong   = (ImageView) child.findViewById(R.id.imgWrongAnswer);

        this.setSign();

        int width  = 0;

        String number1Numerator   = number1.substring(0, number1.indexOf("/"));
        String number1Denominator = number1.substring(number1.indexOf("/") + 1, number1.length());
        String number2Numerator   = number2.substring(0, number2.indexOf("/"));
        String number2Denominator = number2.substring(number2.indexOf("/") + 1, number2.length());
        String ansQuotient   = "";
        String ansNumerator  = "";
        String ansDenominator= "";

        if(result.contains(" "))
        {
            imgResultLineThird.setVisibility(ImageView.VISIBLE);
            ansQuotient        = result.substring(0, result.indexOf(" "));
            ansNumerator       = result.substring(result.indexOf(" ") + 1, result.indexOf("/"));
            ansDenominator     = result.substring(result.indexOf("/") + 1, result.length());
        }
        else if(result.contains("/"))
        {
            imgResultLineThird.setVisibility(ImageView.VISIBLE);
            ansNumerator       = result.substring(0, result.indexOf("/"));
            ansDenominator     = result.substring(result.indexOf("/") + 1, result.length());
        }
        else
        {
            imgResultLineThird.setVisibility(ImageView.INVISIBLE);
            ansQuotient        = result.substring(0, result.length());
        }

        width = this.getWidthForFractionText(number1Numerator.length(), number1Denominator.length());
        RelativeLayout.LayoutParams lp1 = new RelativeLayout.LayoutParams(width,RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp1.addRule(RelativeLayout.BELOW,R.id.txtnumber1Numerator);
        lp1.addRule(RelativeLayout.CENTER_VERTICAL,RelativeLayout.TRUE);
        txtnumber1Numerator.setWidth(width);
        txtnumber1Denominator.setWidth(width);
        imgResultLineFirst.setLayoutParams(lp1);

        width = this.getWidthForFractionText(number2Numerator.length(), number2Denominator.length());
        RelativeLayout.LayoutParams lp2 = new RelativeLayout.LayoutParams(width,RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp2.addRule(RelativeLayout.BELOW,R.id.txtnumber2Numerator);
        lp2.addRule(RelativeLayout.RIGHT_OF,R.id.imgSign);
        lp2.addRule(RelativeLayout.CENTER_VERTICAL,RelativeLayout.TRUE);
        txtnumber2Numerator.setWidth(width);
        txtnumber2Denominator.setWidth(width);
        imgResultLineSecond.setLayoutParams(lp2);

        txtnumber1Numerator.setText(number1Numerator);
        txtnumber1Denominator.setText(number1Denominator);

        txtnumber2Numerator.setText(number2Numerator);
        txtnumber2Denominator.setText(number2Denominator);

        width = this.getWidthForFractionText(ansNumerator.length(), ansDenominator.length()) + MAX_WIDTH_FOR_FRACTION_TEXT;
        RelativeLayout.LayoutParams lp3 = new RelativeLayout.LayoutParams(width,RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp3.setMargins(10, 0, 0, 0);
        lp3.addRule(RelativeLayout.BELOW,R.id.linearLayoutNumerator);
        lp3.addRule(RelativeLayout.RIGHT_OF,R.id.linearLayoutQuotient);
        lp3.addRule(RelativeLayout.CENTER_VERTICAL,RelativeLayout.TRUE);
        imgResultLineThird.setLayoutParams(lp3);

        //set layout width for quotient
        this.setLayoutWidthForFraction(ansQuotient.length(), linearLayoutQuotient);

        //set layout width for numerator and denominator
        if(ansNumerator.length() > ansDenominator.length())
        {
            this.setLayoutWidthForFraction(ansNumerator.length(), linearLayoutNumerator);
            this.setLayoutWidthForFraction(ansNumerator.length(), linearLayoutDenominator);
        }
        else
        {
            this.setLayoutWidthForFraction(ansDenominator.length(), linearLayoutNumerator);
            this.setLayoutWidthForFraction(ansDenominator.length(), linearLayoutDenominator);
        }

        this.createResultLayoutForFraction(ansQuotient,linearLayoutQuotient);
        this.createResultLayoutForFraction(ansNumerator,linearLayoutNumerator);
        this.createResultLayoutForFraction(ansDenominator,linearLayoutDenominator);
    }

    /**
     * This method set the layout width for fraction
     */
    protected void setLayoutWidthForFraction(int noOFDigits , LinearLayout layout)
    {
        int width = (noOFDigits * widthForFractionLayout ) + 15;
        if(layout == linearLayoutQuotient)
        {
            width = width - 15;
        }

        RelativeLayout.LayoutParams lp1 = new RelativeLayout.LayoutParams(width,LayoutParams.WRAP_CONTENT);

        if(layout == linearLayoutQuotient)
        {
            lp1.addRule(RelativeLayout.RIGHT_OF,R.id.imgEqual);
            lp1.addRule(RelativeLayout.CENTER_VERTICAL,RelativeLayout.TRUE);
        }
        else if(layout == linearLayoutNumerator)
        {
            lp1.setMargins(0, 5, 0, 0);
            lp1.addRule(RelativeLayout.RIGHT_OF,R.id.linearLayoutQuotient);

        }else if(layout == linearLayoutDenominator)
        {
            lp1.setMargins(0, 5, 0, 0);
            lp1.addRule(RelativeLayout.BELOW,R.id.imgResultLineThird);
            lp1.addRule(RelativeLayout.RIGHT_OF,R.id.linearLayoutQuotient);
        }

        layout.setLayoutParams(lp1);
    }

    /**
     * This method call when user play with multiplication
     */
    protected void operationPerformForMultiplication()
    {
        relativeLayout.removeAllViews();

        //changes for reset layout
        this.resetNumberLayout();
        //end changes

        LayoutInflater inflater = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
        child = (RelativeLayout) inflater.inflate(R.layout.multiplication_layout_learning_center, null);
        relativeLayout.addView(child);

        txtNumber1 = (TextView)  child.findViewById(R.id.Number1);
        txtNumber2 = (TextView)  child.findViewById(R.id.Number2);
        imgSign    = (ImageView) child.findViewById(R.id.imgSign);
        txtCarry   = (TextView)  child.findViewById(R.id.txtCarry);
        imgWrong   = (ImageView) child.findViewById(R.id.imgWrongAnswer);

        imgResultLineFirst = (ImageView) child.findViewById(R.id.imgResultLine1);
        imgResultLineSecond= (ImageView) child.findViewById(R.id.imgResultLine2);

        linearLayoutMultiple = (LinearLayout) child.findViewById(R.id.linearLayoutMultiple);
        linearLayoutResult   = (LinearLayout) child.findViewById(R.id.linearLayoutResult);

        txtNumber1.setText(" " + this.getStringFromArrayList(this.getDigit(number1)));
        txtNumber2.setText(this.getStringFromArrayList(this.getDigit(number2)));

        txtCarry.setOnClickListener(this);
        this.setSign();

        int width = 0;
        if(numberOfBoxesForMultiplication > this.getDigit(number1).size())
        {
            width = numberOfBoxesForMultiplication * max_width_for_layout_maultiplication + numberOfBoxesForMultiplication * 2;
        }
        else
        {
            width = ( this.getDigit(number1).size() + 1 )* max_width_for_layout_maultiplication + numberOfBoxesForMultiplication * 2;
        }

        RelativeLayout.LayoutParams lpLine1 = new RelativeLayout.LayoutParams(numberOfBoxesForMultiplication * max_width_for_layout_maultiplication + numberOfBoxesForMultiplication * 2,RelativeLayout.LayoutParams.WRAP_CONTENT);
        lpLine1.addRule(RelativeLayout.BELOW,R.id.Number2);
        lpLine1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,RelativeLayout.TRUE);
        imgResultLineFirst.setLayoutParams(lpLine1);

        RelativeLayout.LayoutParams lpLine2 = new RelativeLayout.LayoutParams(numberOfBoxesForMultiplication * max_width_for_layout_maultiplication + numberOfBoxesForMultiplication * 2,RelativeLayout.LayoutParams.WRAP_CONTENT);
        lpLine2.addRule(RelativeLayout.BELOW,R.id.linearLayoutMultiple);
        lpLine2.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,RelativeLayout.TRUE);
        imgResultLineSecond.setLayoutParams(lpLine2);

        RelativeLayout linearLayout = (RelativeLayout) child.findViewById(R.id.multiplicationlayout);
        RelativeLayout.LayoutParams lp1 = new RelativeLayout.LayoutParams(width,LayoutParams.WRAP_CONTENT);
        linearLayout.setLayoutParams(lp1);

        if(this.getDigit(number2).size() > 1)
        {
            imgResultLineSecond.setVisibility(ImageView.VISIBLE);
            this.createMultiplicativeLayout(linearLayoutMultiple);
        }
        else
        {
            imgResultLineSecond.setVisibility(ImageView.GONE);
        }

        selectedIndex = numberOfBoxesForMultiplication - 1;
        index         = numberOfBoxesForMultiplication - 1;
        this.createResultLayoutForMultiplication(linearLayoutResult);
    }

    /**
     * This method call when no of digits in second number is >1, then it will creates no Linear Layout
     * and add it to the multiplicative layout
     * @param linearLayoutMultiple
     */
    protected void createMultiplicativeLayout(LinearLayout linearLayoutMultiple)
    {

        for( int i = 0 ; i < this.getDigit(number2).size()  ; i++)
        {
            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            linearLayout.setMinimumHeight(max_width_for_layout_maultiplication);
            linearLayout.setGravity(Gravity.RIGHT);

            this.createResultLayoutForMultiplication(linearLayout);

            linearLayoutMultiple.addView(linearLayout);
        }
    }

    /**
     * This method creates number of boxes for result for division
     * @param linearLayoutResult
     */
    protected void createResultLayoutForDivision(LinearLayout linearLayoutResult)
    {
        //changes for number of boxes greater then 5
        if(numberOfBoxesForDivision > 5)
            numberOfBoxesForDivision = 5;

        for( int i = 0 ; i < numberOfBoxesForDivision ; i ++ )
        {
            TextView txtView = new TextView(this);
            txtView.setId(i + 1);
            txtView.setGravity(Gravity.CENTER);

            //changes for tab
            if(isTab)
                txtView.setTextSize(45);
            else
                txtView.setTextSize(25);

            txtView.setTypeface(null, Typeface.BOLD);
            txtView.setTextColor(Color.parseColor("#3CB3FF"));
            if(isYellowboxForFraction){
                //changes for tab
                if(isTab)
                    txtView.setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
                else
                    txtView.setBackgroundResource(R.drawable.whitebox_small);
            }
            else
            {
                //changes for tab
                if(isTab)
                    txtView.setBackgroundResource(R.drawable.tab_yellow_ipad);
                else
                    txtView.setBackgroundResource(R.drawable.yellow_small);
                isYellowboxForFraction = true;
            }

            resultTextViewList.add(txtView);//add text view to arrayList
            linearLayoutResult.addView(txtView);

            txtView.setOnClickListener(new OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    for( int  i = 0 ; i < resultTextViewList.size() ; i ++ )
                    {
                        if(v == resultTextViewList.get(i))
                        {

                            //index = i;
                            if( i < numberOfBoxesForDivision)
                            {
                                index = i;
                                selectedIndex = 0;
                                //clickOnSolveAreaForDivision = false;
                            }
                            else
                            {
                                index = i;
                                if(i % numberOfBoxesForDivision == 0)
                                {
                                    isMiddleSelectedForDivision = false;
                                    selectedIndex = 0;
                                }
                                else if(i % numberOfBoxesForDivision == numberOfBoxesForDivision - 1)
                                {
                                    isMiddleSelectedForDivision = false;
                                    selectedIndex = i;
                                }
                                else
                                {
                                    isMiddleSelectedForDivision = true;
                                }
                            }
                            //changes for tab
                            if(isTab)
                                resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
                            else
                                resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow_small);
                        }
                        else
                        {
                            //changes for tab
                            if(isTab)
                                resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
                            else
                                resultTextViewList.get(i).setBackgroundResource(R.drawable.whitebox_small);
                        }
                    }
                }
            });
        }
    }

    /**
     * This method call when user play with division
     */
    protected void operationPerfomrmForDivision()
    {
        relativeLayout.removeAllViews();

        //changes for reset layout
        this.resetNumberLayout();
        //end changes

        LayoutInflater inflater = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
        child = (RelativeLayout) inflater.inflate(R.layout.division_layout_learning_center, null);
        relativeLayout.addView(child);

        txtNumber1 = (TextView)  child.findViewById(R.id.txtdivident);
        txtNumber2 = (TextView)  child.findViewById(R.id.txtdivisor);
        txtCarry   = (TextView)  child.findViewById(R.id.txtCarry);
        imgResultLine = (ImageView) child.findViewById(R.id.imgDivideLine);
        imgWrong   = (ImageView) child.findViewById(R.id.imgWrongAnswer);

        txtCarry.setOnClickListener(this);

        txtNumber1.setText(this.getStringFromArrayList(this.getDigit(number1)));
        txtNumber2.setText(this.getStringFromArrayList(this.getDigit(number2)));

        linearLayoutResult   = (LinearLayout) child.findViewById(R.id.linearLayoutResult);
        linearLayoutRowsForDivision     = (LinearLayout) child.findViewById(R.id.linearLayoutRows);

        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(numberOfBoxesForDivision * max_width_for_layout_maultiplication
                ,RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(10, 0, 0, 0);
        lp.addRule(RelativeLayout.BELOW , R.id.linearLayoutResult);
        lp.addRule(RelativeLayout.RIGHT_OF,R.id.txtdivisor);
        imgResultLine.setLayoutParams(lp);

        selectedIndex = 0;
        index = 0;

        this.createResultLayoutForDivision(linearLayoutResult);
        this.createsRowLayoutForDivision(linearLayoutRowsForDivision);
    }

    /**
     * This method creates number of rows for division
     * @param linearLayoutRowsForDivision
     */
    protected void createsRowLayoutForDivision(LinearLayout linearLayoutRowsForDivision)
    {
        int counter = 0 ;
        LinearLayout linearLayout = null;
        int width = numberOfBoxesForDivision * max_width_for_layout_maultiplication
                + (max_width_for_layout_maultiplication / 4);

		/*//changes for tab
		if(isTab)
		{
			DisplayMetrics metrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(metrics);  
			if(metrics.heightPixels <= TAB_HEIGHT_1024){
				width = width - 15 - (numberOfBoxesForDivision * 5);
			}else{
				if (metrics.densityDpi > 160){
					width = width - 30 - (numberOfBoxesForDivision * 5);
				}else{
					width = width - 15 - (numberOfBoxesForDivision * 5);
				}
			}
		}
		//end changes
		 */

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(width
                ,LinearLayout.LayoutParams.WRAP_CONTENT);

        for( int i = 0 ; i < numberOfRowsforDivision ; i ++ )
        {
            linearLayout = new LinearLayout(this);
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            linearLayout.setLayoutParams(lp);

            if( i == 0 )
            {
                this.createResultLayoutForDivision(linearLayout);
                linearLayoutRowsForDivision.addView(linearLayout);

                ImageView imgLine = new ImageView(this);
                imgLine.setBackgroundResource(R.drawable.ml_equation_line3);
                imgLine.setLayoutParams(lp);
                linearLayoutRowsForDivision.addView(imgLine);
            }
            else if( i == numberOfRowsforDivision - 1 && (i - 1) != 0)
            {
                ImageView imgLine = new ImageView(this);
                imgLine.setBackgroundResource(R.drawable.ml_equation_line3);
                imgLine.setLayoutParams(lp);
                linearLayoutRowsForDivision.addView(imgLine);

                this.createResultLayoutForDivision(linearLayout);
                linearLayoutRowsForDivision.addView(linearLayout);
            }
            else
            {
                if(counter == 2)//if counter is 2 then add a break line to the layout
                {
                    ImageView imgLine = new ImageView(this);
                    imgLine.setBackgroundResource(R.drawable.ml_equation_line3);
                    imgLine.setLayoutParams(lp);
                    linearLayoutRowsForDivision.addView(imgLine);

                    this.createResultLayoutForDivision(linearLayout);
                    linearLayoutRowsForDivision.addView(linearLayout);
                    counter = 1;
                }
                else
                {
                    this.createResultLayoutForDivision(linearLayout);
                    linearLayoutRowsForDivision.addView(linearLayout);
                    counter ++;
                }

            }
        }
    }

    /**
     * Clear Text for fraction
     */
    protected void clearTextForFraction()
    {

        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "inside clearTextForFraction()");

        resultTextViewList.get(index).setText("");

        for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
        {
            if(i == index)
            {
                //changes for tab
                if(isTab)
                    resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
                else
                    resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow_small);
            }
            else
            {
                //changes for tab
                if(isTab)
                    resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
                else
                    resultTextViewList.get(i).setBackgroundResource(R.drawable.whitebox_small);
            }
        }

        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "outside clearTextForFraction()");
    }

    /**
     * This method call when click on back arrow
     */
    protected void clickOnBackArrow()
    {
        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "inside clickOnBackArrow()");

        if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION)//fraction
        {
            if(resultTextViewList.get(index).getText().toString().equals(""))
            {
                if(index > 0)
                {
                    index = index - 1;
                    this.clearTextForFraction();
                }
                else
                {
                    this.clearTextForFraction();
                }
            }
            else
            {
                this.clearTextForFraction();
            }
        }
        else if(operationId == MULTIPLICATION || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
                || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
        {
            if(resultTextViewList.get(index).getText().toString().equals(""))
            {
                if(this.getDigit(number2).size() > 1)
                {
                    if(index ==  resultTextViewList.size() - 1)
                    {
                        index = this.getDigit(number2).size() * numberOfBoxesForMultiplication;
                        this.clearTextForMultiplication();
                    }
                    else
                    {

                        if(index >= this.getDigit(number2).size() * numberOfBoxesForMultiplication)
                        {
                            index = index + 1;
                            this.clearTextForMultiplication();

                        }
                        else
                        {
							/*For moving index from left to right for clear text*/
                            if(index % numberOfBoxesForMultiplication == numberOfBoxesForMultiplication - 1 )
                            {
                                index = index - ((startIndex - 1) % numberOfBoxesForMultiplication);
                                this.clearTextForMultiplication();
                            }
                            else
                            {
                                index = index + 1;
                                this.clearTextForMultiplication();
                            }
                        }
                    }
                }
                else
                {
                    if(index  < resultTextViewList.size() - 1)
                    {
                        index = index + 1;
                        this.clearTextForMultiplication();
                    }
                    else
                    {
                        if(index == resultTextViewList.size() - 1)
                        {
                            index = 0;
                            this.clearTextForMultiplication();
                        }
                    }
                }
            }
            else
            {
                if(resultTextViewList.get(index).getText().toString().contains("-"))
                    resultTextViewList.get(index).setText(resultTextViewList.get(index).getText().toString().replace("-", ""));
                else if(resultTextViewList.get(index).getText().toString().contains("."))
                    resultTextViewList.get(index).setText(resultTextViewList.get(index).getText().toString().replace(".", ""));
                else
                    resultTextViewList.get(index).setText("");
            }

        }
        else if(operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
                || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))
        {
            if(resultTextViewList.get(index).getText().toString().equals(""))
            {
                if((index % numberOfBoxesForDivision) == 0)
                {
                    index = index + numberOfBoxesForDivision - 1;
                    this.clearTextForDivision();
                }
                else
                {
                    index -- ;
                    this.clearTextForDivision();
                }
            }
            else
            {
                if(resultTextViewList.get(index).getText().toString().contains("-"))
                    resultTextViewList.get(index).setText(resultTextViewList.get(index).getText().toString().replace("-", ""));
                else if(resultTextViewList.get(index).getText().toString().contains("."))
                    resultTextViewList.get(index).setText(resultTextViewList.get(index).getText().toString().replace(".", ""));
                else
                    resultTextViewList.get(index).setText("");
            }
        }
        else
        {
            if(resultTextViewList.get(index).getText().toString().equals(""))
            {
                index = (index + 1) % noOfDigitsInResult;
                this.clearText();
            }
            else
            {
                if(resultTextViewList.get(index).getText().toString().contains("-"))
                    resultTextViewList.get(index).setText(resultTextViewList.get(index).getText().toString().replace("-", ""));
                else if(resultTextViewList.get(index).getText().toString().contains("."))
                    resultTextViewList.get(index).setText(resultTextViewList.get(index).getText().toString().replace(".", ""));
                else
                    resultTextViewList.get(index).setText("");
            }
        }

        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "outside clickOnBackArrow()");
    }

    /**
     * This method clear text for multiplication when click on back Arrow
     */
    protected void clearTextForMultiplication()
    {
        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "inside clearTextForMultiplication()");

        try {
            resultTextViewList.get(index).setText("");
            if (this.getDigit(number2).size() > 1)
                startIndex = this.getDigit(number2).size() * numberOfBoxesForMultiplication;

            if (index < startIndex) {
                for (int i = 0; i < startIndex; i++) {
                    if (i == index) {
                        //changes for tab
                        if (isTab)
                            resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
                        else
                            resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow_small);
                    } else {
                        //changes for tab
                        if (isTab)
                            resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
                        else
                            resultTextViewList.get(i).setBackgroundResource(R.drawable.whitebox_small);
                    }
                }
            } else {
                for (int i = startIndex; i < resultTextViewList.size(); i++) {
                    if (i == index) {
                        //changes for tab
                        if (isTab)
                            resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
                        else
                            resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow_small);
                    } else {
                        //changes for tab
                        if (isTab)
                            resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
                        else
                            resultTextViewList.get(i).setBackgroundResource(R.drawable.whitebox_small);
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "outside clearTextForMultiplication()");
    }

    /**
     * This method clear text for division when click on back arrow
     */
    protected void clearTextForDivision()
    {
        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "inside clearTextForMultiplication()");

        resultTextViewList.get(index).setText("");

        for(int i = 0 ; i < resultTextViewList.size() ; i ++ )
        {
            if(i == index)
            {
                //changes for tab
                if(isTab)
                    resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
                else
                    resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow_small);
            }
            else
            {
                //changes for tab
                if(isTab)
                    resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
                else
                    resultTextViewList.get(i).setBackgroundResource(R.drawable.whitebox_small);
            }
        }
    }

    /**
     * This method is call when a user click on back Arrow key for clear text
     */
    protected void clearText()
    {
        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "inside clearText()");

        resultTextViewList.get(index).setText("");

        for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
        {
            if(i == index)
            {
                //changes for tab
                if(isTab)
                    resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
                else
                    resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow);
            }
            else
            {
                //changes for tab
                if(isTab)
                    resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
                else
                    resultTextViewList.get(i).setBackgroundResource(R.drawable.ml_no1box);
            }
        }

        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "outside clearText()");
    }

    /**
     * This method call when click on carry text box
     */
    protected void clickOnCarry()
    {
        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "inside clickOnCarry()");

        isCarrySelected = true;
        if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION
                || operationId == MULTIPLICATION
                || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
                ||(operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x"))
                ||operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
                ||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))//fraction and multiplication
        {
            //changes for tab
            if(isTab)
                resultTextViewList.get(index).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
            else
                resultTextViewList.get(index).setBackgroundResource(R.drawable.whitebox_small);
        }
        else{
            //changes for tab
            if(isTab)
                resultTextViewList.get(index).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
            else
                resultTextViewList.get(index).setBackgroundResource(R.drawable.ml_no1box);
        }

        txtCarry.setBackgroundResource(R.drawable.yellow);

        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "outside clickOnCarry()");
    }

    /**
     * This method call when click on minus button
     */
    protected void clickOnMinus()
    {
        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "inside clickOnMinus()");

        if(((operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
                || operationId == MULTIPLICATION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
                && this.getDigit(number2).size() > 1)
        {
            this.clickOnMinusForMultiplication();
        }
        else
        {
            if(!resultTextViewList.get(0).getText().toString().equals(""))
            {
                //if(isMinusSelected)
                if(resultTextViewList.get(0).getText().toString().contains("-"))
                {
                    //isMinusSelected = false;
                    resultTextViewList.get(0).setText(resultTextViewList.get(0).getText().toString().replace("-", ""));
                }
                else
                {
                    //isMinusSelected = true;

                    if(resultTextViewList.get(0).getText().toString().contains("."))
                        resultTextViewList.get(0).setText("-" + resultTextViewList.get(0).getText().toString().replace(".", ""));
                    else
                        resultTextViewList.get(0).setText("-" + resultTextViewList.get(0).getText().toString());
                }
            }
        }
        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "inside clickOnMinus()");
    }

    /**
     * This method is call when user click on minus and play with multiplication
     */
    protected void clickOnMinusForMultiplication()
    {
        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "inside clickOnMinusForMultiplication()");

        if(!resultTextViewList.get((this.getDigit(number2).size() * numberOfBoxesForMultiplication)).getText().toString().equals(""))
        {
            if(isMinusSelected)
            {
                isMinusSelected = false;
                resultTextViewList.get((this.getDigit(number2).size() * numberOfBoxesForMultiplication)).setText(resultTextViewList.get((this.getDigit(number2).size() * numberOfBoxesForMultiplication)).getText().toString().replace("-", ""));
            }
            else
            {
                isMinusSelected = true;
                if(resultTextViewList.get((this.getDigit(number2).size() * numberOfBoxesForMultiplication)).getText().toString().contains("."))
                    resultTextViewList.get((this.getDigit(number2).size() * numberOfBoxesForMultiplication)).setText("-" + resultTextViewList.get((this.getDigit(number2).size() * numberOfBoxesForMultiplication)).getText().toString().replace(".", ""));
                else
                    resultTextViewList.get((this.getDigit(number2).size() * numberOfBoxesForMultiplication)).setText("-" + resultTextViewList.get((this.getDigit(number2).size() * numberOfBoxesForMultiplication)).getText().toString());
            }
        }

        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "outside clickOnMinusForMultiplication()");
    }

    /**
     * This method call when click on dot Button
     */
    protected void clickonDot()
    {
        if(((operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
                || operationId == MULTIPLICATION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
                && this.getDigit(number2).size() > 1)
        {
            this.clickOnDotForMultiplication();
        }
        else if(operationId ==  DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
                || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))
        {
            this.clickonDotForDivision();
        }
        else
        {
            if(isDotSelected)
            {
                if(resultTextViewList.get(index) == txtSelectedTextWithDot)
                {
                    isDotSelected = false;
                    txtSelectedTextWithDot.setText(txtSelectedTextWithDot.getText().toString().replace(".", ""));
                }
                else
                {
                    txtSelectedTextWithDot.setText(txtSelectedTextWithDot.getText().toString().replace(".", ""));
                    if(resultTextViewList.get(index).getText().toString().equals("") && index == noOfDigitsInResult - 1)
                    {
                        resultTextViewList.get(index).setText(".0");
                    }
                    else if(resultTextViewList.get(index).getText().toString().equals("") && index == 0)
                    {
                        resultTextViewList.get(index).setText("0.");
                    }
                    else if(resultTextViewList.get(index).getText().toString().contains("-"))
                    {
                        resultTextViewList.get(index).setText(resultTextViewList.get(index).getText().toString().replace("-", "") + ".");
                    }
                    else
                    {
                        if(index == noOfDigitsInResult - 1)
                            resultTextViewList.get(index).setText("." + resultTextViewList.get(index).getText().toString());
                        else if(resultTextViewList.get(index).getText().toString().equals(""))
                            resultTextViewList.get(index).setText("0.");
                        else
                            resultTextViewList.get(index).setText(resultTextViewList.get(index).getText().toString() + ".");
                    }
                    txtSelectedTextWithDot = resultTextViewList.get(index);
                }
            }
            else
            {
                isDotSelected = true;
                if(resultTextViewList.get(index).getText().toString().equals("") && index == noOfDigitsInResult - 1)
                {
                    resultTextViewList.get(index).setText(".0");
                }
                else if(resultTextViewList.get(index).getText().toString().equals("") && index == 0)
                {
                    resultTextViewList.get(index).setText("0.");
                }
                else if(resultTextViewList.get(index).getText().toString().contains("-"))
                {
                    resultTextViewList.get(index).setText(resultTextViewList.get(index).getText().toString().replace("-", "") + ".");
                }
                else
                {
                    if(index == noOfDigitsInResult - 1)
                        resultTextViewList.get(index).setText("." + resultTextViewList.get(index).getText().toString());
                    else if(resultTextViewList.get(index).getText().toString().equals(""))
                        resultTextViewList.get(index).setText("0.");
                    else
                        resultTextViewList.get(index).setText(resultTextViewList.get(index).getText().toString() + ".");
                }

                txtSelectedTextWithDot = resultTextViewList.get(index);
            }
            this.moveOnDotClick();
        }
    }

    /**
     * This method is call when user play with multiplication and click on dot
     */
    protected void clickOnDotForMultiplication()
    {

        if(index >= (this.getDigit(number2).size() * numberOfBoxesForMultiplication))
        {
            if(isDotSelected)
            {
                if(resultTextViewList.get(index) == txtSelectedTextWithDot)
                {
                    isDotSelected = false;
                    txtSelectedTextWithDot.setText(txtSelectedTextWithDot.getText().toString().replace(".", ""));
                }
                else
                {
                    txtSelectedTextWithDot.setText(txtSelectedTextWithDot.getText().toString().replace(".", ""));
                    if(resultTextViewList.get(index).getText().toString().equals("") && index == resultTextViewList.size() - 1)
                    {
                        resultTextViewList.get(index).setText(".0");
                    }
                    else if(resultTextViewList.get(index).getText().toString().equals("") && index == (this.getDigit(number2).size() * numberOfBoxesForMultiplication))
                    {
                        resultTextViewList.get(index).setText("0.");
                    }
                    else if(resultTextViewList.get(index).getText().toString().contains("-"))
                    {
                        resultTextViewList.get(index).setText(resultTextViewList.get(index).getText().toString().replace("-", "") + ".");
                    }
                    else
                    {
                        if(index == resultTextViewList.size() - 1)
                            resultTextViewList.get(index).setText("." + resultTextViewList.get(index).getText().toString());
                        else if(resultTextViewList.get(index).getText().toString().equals(""))
                            resultTextViewList.get(index).setText("0.");
                        else
                            resultTextViewList.get(index).setText(resultTextViewList.get(index).getText().toString() + ".");
                    }
                    txtSelectedTextWithDot = resultTextViewList.get(index);
                }

            }
            else
            {
                isDotSelected = true;
                if(resultTextViewList.get(index).getText().toString().equals("") && index == resultTextViewList.size() - 1)
                {
                    resultTextViewList.get(index).setText(".0");
                }
                else if(resultTextViewList.get(index).getText().toString().equals("") && index == (this.getDigit(number2).size() * numberOfBoxesForMultiplication))
                {
                    resultTextViewList.get(index).setText("0.");
                }
                else if(resultTextViewList.get(index).getText().toString().contains("-"))
                {
                    resultTextViewList.get(index).setText(resultTextViewList.get(index).getText().toString().replace("-", "") + ".");
                }
                else
                {
                    if(index == resultTextViewList.size() - 1)
                        resultTextViewList.get(index).setText("." + resultTextViewList.get(index).getText().toString());
                    else if(resultTextViewList.get(index).getText().toString().equals(""))
                        resultTextViewList.get(index).setText("0.");
                    else
                        resultTextViewList.get(index).setText(resultTextViewList.get(index).getText().toString() + ".");
                }

                txtSelectedTextWithDot = resultTextViewList.get(index);
            }

            this.moveOnDotClick();
        }
    }

    /**
     * This method is call when user play with division and click on dot
     */
    protected void clickonDotForDivision()
    {

        if(index < resultTextViewList.size()/(numberOfRowsforDivision + 1))
        {
            if(isDotSelected)
            {
                if(resultTextViewList.get(index) == txtSelectedTextWithDot)
                {
                    isDotSelected = false;
                    txtSelectedTextWithDot.setText(txtSelectedTextWithDot.getText().toString().replace(".", ""));
                }
                else
                {
                    txtSelectedTextWithDot.setText(txtSelectedTextWithDot.getText().toString().replace(".", ""));
                    if(resultTextViewList.get(index).getText().toString().equals("") && index == resultTextViewList.size() - 1)
                    {
                        resultTextViewList.get(index).setText(".0");
                    }
                    else if(resultTextViewList.get(index).getText().toString().equals("") && index == (this.getDigit(number2).size() * numberOfBoxesForMultiplication))
                    {
                        resultTextViewList.get(index).setText("0.");
                    }
                    else if(resultTextViewList.get(index).getText().toString().contains("-"))
                    {
                        resultTextViewList.get(index).setText(resultTextViewList.get(index).getText().toString().replace("-", "") + ".");
                    }
                    else
                    {
                        if(index == resultTextViewList.size() - 1)
                            resultTextViewList.get(index).setText("." + resultTextViewList.get(index).getText().toString());
                        else if(resultTextViewList.get(index).getText().toString().equals(""))
                            resultTextViewList.get(index).setText("0.");
                        else
                            resultTextViewList.get(index).setText(resultTextViewList.get(index).getText().toString() + ".");
                    }
                    txtSelectedTextWithDot = resultTextViewList.get(index);
                }

            }
            else
            {
                isDotSelected = true;
                if(resultTextViewList.get(index).getText().toString().equals("") && index == resultTextViewList.size() - 1)
                {
                    resultTextViewList.get(index).setText(".0");
                }
                else if(resultTextViewList.get(index).getText().toString().equals("") && index == (this.getDigit(number2).size() * numberOfBoxesForMultiplication))
                {
                    resultTextViewList.get(index).setText("0.");
                }
                else if(resultTextViewList.get(index).getText().toString().contains("-"))
                {
                    resultTextViewList.get(index).setText(resultTextViewList.get(index).getText().toString().replace("-", "") + ".");
                }
                else
                {
                    if(index == resultTextViewList.size() - 1)
                        resultTextViewList.get(index).setText("." + resultTextViewList.get(index).getText().toString());
                    else if(resultTextViewList.get(index).getText().toString().equals(""))
                        resultTextViewList.get(index).setText("0.");
                    else
                        resultTextViewList.get(index).setText(resultTextViewList.get(index).getText().toString() + ".");
                }

                txtSelectedTextWithDot = resultTextViewList.get(index);
            }

            this.moveOnDotClick();
        }
    }

    /**
     * this method call after when user click on dot
     */
    protected void moveOnDotClick()
    {
        if(selectedIndex > 0)
        {
            if(index >= 1)
            {
                //check if id is for fraction addition and subtraction then add small box other wise add large box
                if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION
                        || operationId == MULTIPLICATION
                        ||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
                        ||(operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x"))
                        ||operationId == DIVISION
                        || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/"))
                        ||(operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/")))//fraction and multiplication
                {
                    //changes for tab
                    if(isTab)
                        resultTextViewList.get(index).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
                    else
                        resultTextViewList.get(index).setBackgroundResource(R.drawable.whitebox_small);
                }
                else{
                    //changes for tab
                    if(isTab)
                        resultTextViewList.get(index).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
                    else
                        resultTextViewList.get(index).setBackgroundResource(R.drawable.ml_no1box);
                }

                //index change for multiplication
                if(operationId == MULTIPLICATION || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
                        || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
                {
                    if(!((index) % numberOfBoxesForMultiplication == 0))
                        index -- ;
                }
                else if(operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
                        || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))
                {
                    if(!((index) % (numberOfBoxesForDivision) == 0))
                        index -- ;
                }
                else
                    index -- ;


                if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION
                        || operationId == MULTIPLICATION
                        || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
                        || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x"))
                        ||operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
                        || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))//fraction and multiplication
                {
                    //changes for tab
                    if(isTab)
                        resultTextViewList.get(index).setBackgroundResource(R.drawable.tab_yellow_ipad);
                    else
                        resultTextViewList.get(index).setBackgroundResource(R.drawable.yellow_small);
                }
                else{
                    //changes for tab
                    if(isTab)
                        resultTextViewList.get(index).setBackgroundResource(R.drawable.tab_yellow_ipad);
                    else
                        resultTextViewList.get(index).setBackgroundResource(R.drawable.yellow);
                }
            }
        }
        else
        {
            if(index < resultTextViewList.size() - 1)
            {
                if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION
                        || operationId == MULTIPLICATION
                        || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
                        || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x"))
                        ||operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
                        ||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))//fraction and multiplication
                {
                    //changes for tab
                    if(isTab)
                        resultTextViewList.get(index).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
                    else
                        resultTextViewList.get(index).setBackgroundResource(R.drawable.whitebox_small);
                }else{
                    //changes for tab
                    if(isTab)
                        resultTextViewList.get(index).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
                    else
                        resultTextViewList.get(index).setBackgroundResource(R.drawable.ml_no1box);
                }

                //index change for multiplication
                if(operationId == MULTIPLICATION//changes
                        || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
                        || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
                {
                    if(!((index) % numberOfBoxesForMultiplication == numberOfBoxesForMultiplication - 1))
                        index ++ ;
                }
                else if(operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
                        ||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))
                {
                    if(!((index) % (numberOfBoxesForDivision) == numberOfBoxesForDivision - 1))
                        index ++ ;
                }
                else
                    index ++ ;


                if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION
                        || operationId == MULTIPLICATION
                        || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
                        || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x"))
                        ||operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
                        ||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))//fraction and multiplication
                {
                    //changes for tab
                    if(isTab)
                        resultTextViewList.get(index).setBackgroundResource(R.drawable.tab_yellow_ipad);
                    else
                        resultTextViewList.get(index).setBackgroundResource(R.drawable.yellow_small);
                }
                else{
                    //changes for tab
                    if(isTab)
                        resultTextViewList.get(index).setBackgroundResource(R.drawable.tab_yellow_ipad);
                    else
                        resultTextViewList.get(index).setBackgroundResource(R.drawable.yellow);
                }
            }

        }
    }


    /**
     * This method set the text when user select one number this number will show on the result text view
     * @param value
     */
    protected void setTextResultValue(String value)
    {
        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "inside setTextResultValue()");

        if(selectedIndex > 0)
        {
            if(index >= 1)
            {
                //Log.e(TAG, "resultTextViewList " + resultTextViewList.size() + " index " + index);

                resultTextViewList.get(index).setText(value);
                //check if id is for fraction addition and subtraction then add small box other wise add large box
                if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION
                        || operationId == MULTIPLICATION
                        ||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
                        ||(operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x"))
                        ||operationId == DIVISION
                        || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/"))
                        ||(operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/")))//fraction and multiplication
                {
                    //changes for tab
                    if(isTab)
                        resultTextViewList.get(index).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
                    else
                        resultTextViewList.get(index).setBackgroundResource(R.drawable.whitebox_small);
                }
                else{
                    if(isTab)
                        resultTextViewList.get(index).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
                    else
                        resultTextViewList.get(index).setBackgroundResource(R.drawable.ml_no1box);
                }

                //index change for multiplication
                if(operationId == MULTIPLICATION || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
                        || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
                {
                    if(!((index) % numberOfBoxesForMultiplication == 0))
                        index -- ;
                }
                else if(operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
                        || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))
                {
                    if(!isMiddleSelectedForDivision)
                    {
                        if(!((index) % (numberOfBoxesForDivision) == 0))
                            index -- ;
                    }
                    else
                    {
                        if(resultTextViewList.get(index - 1).getText().equals("")
                                && resultTextViewList.get(index + 1).getText().equals(""))
                        {
                            isMiddleSelectedForDivision = true;
                        }
                        else if(resultTextViewList.get(index - 1).getText().equals("")
                                && !(resultTextViewList.get(index + 1).getText().equals("")))
                        {
                            isMiddleSelectedForDivision = false;
                            index = index - 1;
                            selectedIndex = 1;
                        }
                        else
                        {
                            isMiddleSelectedForDivision = false;
                            index = index + 1;
                            selectedIndex = 0;
                        }
                    }

                }
                else
                    index -- ;


                if(!isMiddleSelectedForDivision)
                {
                    if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION
                            || operationId == MULTIPLICATION
                            || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
                            || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x"))
                            ||operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
                            || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))//fraction and multiplication
                    {
                        //changes for tab
                        if(isTab)
                            resultTextViewList.get(index).setBackgroundResource(R.drawable.tab_yellow_ipad);
                        else
                            resultTextViewList.get(index).setBackgroundResource(R.drawable.yellow_small);
                    }else{
                        //changes for tab
                        if(isTab)
                            resultTextViewList.get(index).setBackgroundResource(R.drawable.tab_yellow_ipad);
                        else
                            resultTextViewList.get(index).setBackgroundResource(R.drawable.yellow);
                    }
                }
            }
            else
            {
                resultTextViewList.get(index).setText(value);

                //changes for android only , on 17 oct not in iphone
                if(index == 0 && (operationId == ADDITION || operationId == SUBTRACTION
                        || operationId == DECIMAL_ADDITION_SUBTRACTION
                        || operationId == NEGATIVE_ADDITION_SUBTRACTION)){
                    index = resultTextViewList.size() - 1;

                    for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
                    {
                        if(i == index)
                        {
                            if(isTab)
                                resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
                            else
                                resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow);
                        }
                        else
                        {
                            if(isTab)
                                resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
                            else
                                resultTextViewList.get(i).setBackgroundResource(R.drawable.ml_no1box);
                        }
                    }
                }
                //end changes
            }
        }
        else
        {
            if(index < resultTextViewList.size() - 1)
            {
                resultTextViewList.get(index).setText(value);
                if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION
                        || operationId == MULTIPLICATION
                        || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
                        || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x"))
                        ||operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
                        ||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))//fraction and multiplication
                {
                    //changes for tab
                    if(isTab)
                        resultTextViewList.get(index).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
                    else
                        resultTextViewList.get(index).setBackgroundResource(R.drawable.whitebox_small);
                }else{

                    //changes for tab
                    if(isTab)
                        resultTextViewList.get(index).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
                    else
                        resultTextViewList.get(index).setBackgroundResource(R.drawable.ml_no1box);
                }

                //index change for multiplication
                if(operationId == MULTIPLICATION//changes
                        || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
                        || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
                {
                    if(!((index) % numberOfBoxesForMultiplication == numberOfBoxesForMultiplication - 1))
                        index ++ ;
                }
                else if(operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
                        ||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))
                {
                    if(!isMiddleSelectedForDivision)
                    {
                        if(!((index) % (numberOfBoxesForDivision) == numberOfBoxesForDivision - 1))
                            index ++ ;
                    }
                    else
                    {
                        if(resultTextViewList.get(index - 1).getText().equals("")
                                && resultTextViewList.get(index + 1).getText().equals(""))
                        {
                            isMiddleSelectedForDivision = true;
                        }
                        else if(resultTextViewList.get(index - 1).getText().equals("")
                                && !(resultTextViewList.get(index + 1).getText().equals("")))
                        {
                            isMiddleSelectedForDivision = false;
                            index = index - 1;
                            selectedIndex = 1;
                        }
                        else
                        {
                            isMiddleSelectedForDivision = false;
                            index = index + 1;
                            selectedIndex = 0;
                        }
                    }
                }
                else
                    index ++ ;


                if(!isMiddleSelectedForDivision)
                {
                    if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION
                            || operationId == MULTIPLICATION
                            || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
                            || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x"))
                            ||operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
                            ||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))//fraction and multiplication
                    {
                        //changes for tab
                        if(isTab)
                            resultTextViewList.get(index).setBackgroundResource(R.drawable.tab_yellow_ipad);
                        else
                            resultTextViewList.get(index).setBackgroundResource(R.drawable.yellow_small);
                    }
                    else{
                        //changes for tab
                        if(isTab)
                            resultTextViewList.get(index).setBackgroundResource(R.drawable.tab_yellow_ipad);
                        else
                            resultTextViewList.get(index).setBackgroundResource(R.drawable.yellow);
                    }
                }
            }
            else
            {
                resultTextViewList.get(index).setText(value);

                //changes for android only , on 17 oct not in iphone
                if(index == resultTextViewList.size() - 1 && (operationId == ADDITION || operationId == SUBTRACTION
                        || operationId == DECIMAL_ADDITION_SUBTRACTION
                        || operationId == NEGATIVE_ADDITION_SUBTRACTION)){

                    index = 0;
                    for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
                    {
                        if(i == index)
                        {
                            if(isTab)
                                resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
                            else
                                resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow);
                        }
                        else
                        {
                            if(isTab)
                                resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
                            else
                                resultTextViewList.get(i).setBackgroundResource(R.drawable.ml_no1box);
                        }
                    }
                }
                //end changes
            }
        }

        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "outside setTextResultValue()");

    }


    /**
     * This method display wrong image for fraction
     */
    protected void visibleWrongImageForFraction()
    {
        //imgWrong.setVisibility(ImageView.VISIBLE);
        for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
        {
			/*//changes for tab
			if(isTab)
				resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
			else
				resultTextViewList.get(i).setBackgroundResource(R.drawable.whitebox_small);*/
            this.setRedBackgroundToTextViewForDivision(resultTextViewList.get(i));
            //resultTextViewList.get(i).setTextColor(Color.RED);
            resultTextViewList.get(i).setText(resultTextViewList.get(i).getText().toString());
        }
    }

    /**
     * This method show the wrong image when user answer is wrong
     */
    protected void visibleWrongImage()
    {
        for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
        {
			/*//changes for tab
			if(isTab)
				resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
			else
				resultTextViewList.get(i).setBackgroundResource(R.drawable.ml_no1box);*/

            this.setRedBackgroundToTextView(resultTextViewList.get(i));

            //resultTextViewList.get(i).setTextColor(Color.RED);
            resultTextViewList.get(i).setText(resultTextViewList.get(i).getText().toString());
        }

        int marginLeft = 0;
        if((noOfDigitsInFisrtNumber - noOfDigitsInResult) >= 2)
            marginLeft = ((noOfDigitsInFisrtNumber - noOfDigitsInResult) * maxWidth) - 20;
        else if((noOfDigitsInFisrtNumber - noOfDigitsInResult) == 0)
            marginLeft = ((noOfDigitsInFisrtNumber - noOfDigitsInResult) * maxWidth) + 20;
        else
            marginLeft = ((noOfDigitsInFisrtNumber - noOfDigitsInResult) * maxWidth);

        RelativeLayout.LayoutParams lp1 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT ,LayoutParams.WRAP_CONTENT);
        lp1.addRule(RelativeLayout.BELOW,R.id.imgResultLine);
        lp1.addRule(RelativeLayout.RIGHT_OF,R.id.txtCarry);//changes sign with txtcarry


        if(noOfDigitsInResult <= noOfDigitsInFisrtNumber)
            lp1.setMargins(marginLeft, 0, 0, 0);

        //imgWrong.setBackgroundResource(R.drawable.ml_wrong);
        //imgWrong.setVisibility(ImageView.VISIBLE);
        //imgWrong.setLayoutParams(lp1);
    }

    /**
     * This method display wrong image when user give wrong answer
     */
    protected void visibleWrongImageForMultiplication()
    {
        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "inside clickOnGoForMultiplication()");

        //imgWrong.setVisibility(ImageView.VISIBLE);

        if(this.getDigit(number2).size() > 1)
            startIndex = this.getDigit(number2).size() * numberOfBoxesForMultiplication;
        for( int i = startIndex ; i < resultTextViewList.size() ; i ++ )
        {
			/*//changes for tab
			if(isTab)
				resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
			else
				resultTextViewList.get(i).setBackgroundResource(R.drawable.whitebox_small);*/

            this.setRedBackgroundToTextViewForDivision(resultTextViewList.get(i));
            //resultTextViewList.get(i).setTextColor(Color.RED);
            resultTextViewList.get(i).setText(resultTextViewList.get(i).getText().toString());
        }


        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "outside clickOnGoForMultiplication()");
    }

    /**
     * This method display wrong image when user give wrong answer
     */
    protected void visibleWrongImageForDivision()
    {
        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "inside visibleWrongImageForDivision()");

        //imgWrong.setVisibility(ImageView.VISIBLE);

        for( int i = 0 ; i < numberOfBoxesForDivision ; i ++ )
        {
			/*if(isTab)
				resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
			else
				resultTextViewList.get(i).setBackgroundResource(R.drawable.whitebox_small);*/

            this.setRedBackgroundToTextViewForDivision(resultTextViewList.get(i));
            //resultTextViewList.get(i).setTextColor(Color.RED);
            resultTextViewList.get(i).setText(resultTextViewList.get(i).getText().toString());
        }

        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "outside visibleWrongImageForDivision()");
    }



    /**
     * This will set the back ground image for wrong attempts
     * @param noOfAttempts
     */
    protected void setWrongAttemptsImage(int noOfAttempts)
    {
        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "inside setWrongAttemptsImage()");

        switch(noOfAttempts)
        {
            case 1:
                imgCross3.setBackgroundResource(R.drawable.ml_red_x);
                break;
            case 2:
                imgCross2.setBackgroundResource(R.drawable.ml_red_x);
                break;
            case 3:
                imgCross1.setBackgroundResource(R.drawable.ml_red_x);
                break;
        }

        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "outside setWrongAttemptsImage()");
    }

    /**
     * This method set the text to carry
     * @param value
     */
    protected void setCarryText(String value)
    {
        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "inside setCarryText()");

        txtCarry.setText(value);
        txtCarry.setBackgroundResource(R.drawable.ml_no1box);

        if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION
                || operationId == MULTIPLICATION
                || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
                || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x"))
                || operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
                ||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))//fraction and multiplication
        {
            if(isTab)
                resultTextViewList.get(index).setBackgroundResource(R.drawable.tab_yellow_ipad);
            else
                resultTextViewList.get(index).setBackgroundResource(R.drawable.yellow_small);
        }
        else
        {
            if(isTab)
                resultTextViewList.get(index).setBackgroundResource(R.drawable.tab_yellow_ipad);
            else
                resultTextViewList.get(index).setBackgroundResource(R.drawable.yellow);
        }

        isCarrySelected = false;

        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "outside setCarryText()");
    }

    //edit for screen layout
    /**
     * This method reset the number layout
     */
    private void resetNumberLayout(){
        if(relativeLayout != null){
            index = 0;
            RelativeLayout.LayoutParams forLayout = new RelativeLayout.LayoutParams
                    (RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
            forLayout.addRule(RelativeLayout.CENTER_IN_PARENT);
            relativeLayout.setLayoutParams(forLayout);
        }
    }

    //for question UI , Issues list 5 march 2014 , issues no. 1

    /**
     * This method initilize the variable to create dynamic UI
     */
    protected void initilizeHeightAndWidthForDynamicLayoutCreation(){

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);

        if(tabletSize){
            isTab = true;
            if(metrics.heightPixels <= TAB_HEIGHT_1024){
                //added if for the chromebook tab
                if(metrics.densityDpi <= 120){
                    maxWidth = 60;
                    MAX_WIDTH_FOR_FRACTION_TEXT = 25;
                    widthForFractionLayout = 60;
                    max_width_for_layout_maultiplication = 60;
                }else {
                    maxWidth = 80;
                    MAX_WIDTH_FOR_FRACTION_TEXT = 35;
                    widthForFractionLayout = 80;
                    max_width_for_layout_maultiplication = 80;
                }
                //MathFriendzyHelper.showToast(this , "Tab1");
            }
            else {
                if(metrics.densityDpi >= 320 && metrics.heightPixels >= 1900
                        && metrics.widthPixels >= 1500){
                    maxWidth = 150;//changes for tab
                    MAX_WIDTH_FOR_FRACTION_TEXT = 100;
                    widthForFractionLayout = 150;
                    max_width_for_layout_maultiplication = 150;//changes for tab
                    //MathFriendzyHelper.showToast(this , "Tab2");
                }else{
                    if(metrics.densityDpi >= 320 && metrics.heightPixels >= 1800
                            && metrics.widthPixels >= 1200){
                        maxWidth = 150;//changes for tab
                        MAX_WIDTH_FOR_FRACTION_TEXT = 100;
                        widthForFractionLayout = 150;
                        max_width_for_layout_maultiplication = 150;//changes for tab
                        //MathFriendzyHelper.showToast(this , "Tab3");
                    }else if (metrics.densityDpi > 160){
                        maxWidth = 110;//changes for tab
                        MAX_WIDTH_FOR_FRACTION_TEXT = 50;
                        widthForFractionLayout = 110;
                        max_width_for_layout_maultiplication = 110;//changes for tab
                        //MathFriendzyHelper.showToast(this , "Tab4");
                    }else{
                        //added if for the chromebook tab
                        if(metrics.densityDpi <= 120){
                            maxWidth = 60;
                            MAX_WIDTH_FOR_FRACTION_TEXT = 25;
                            widthForFractionLayout = 60;
                            max_width_for_layout_maultiplication = 60;
                        }else {
                            maxWidth = 80;
                            MAX_WIDTH_FOR_FRACTION_TEXT = 35;
                            widthForFractionLayout = 80;
                            max_width_for_layout_maultiplication = 80;
                        }
                        //MathFriendzyHelper.showToast(this , "Tab5");
                    }
                }
            }
        }
        else{
            if((getResources().getConfiguration().screenLayout &
                    Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
                    metrics.densityDpi > 320){
                if(metrics.densityDpi > 600){//for the range of Nexus6
                    maxWidth = 200;
                    MAX_WIDTH_FOR_FRACTION_TEXT = 100;
                    widthForFractionLayout = 200;
                    max_width_for_layout_maultiplication = 160;
                }else if(metrics.densityDpi > 400){
                    maxWidth = 150;
                    MAX_WIDTH_FOR_FRACTION_TEXT = 75;
                    widthForFractionLayout = 145;
                    max_width_for_layout_maultiplication = 120;
                }else{
                    maxWidth = 112;
                    MAX_WIDTH_FOR_FRACTION_TEXT = 54;
                    widthForFractionLayout = 108;
                    max_width_for_layout_maultiplication = 90;
                }
            }else{
                if ((getResources().getConfiguration().screenLayout &
                        Configuration.SCREENLAYOUT_SIZE_MASK) ==
                        Configuration.SCREENLAYOUT_SIZE_NORMAL &&
                        metrics.densityDpi > SCREEN_DENISITY){
                    maxWidth = 100;
                    MAX_WIDTH_FOR_FRACTION_TEXT = 50;
                    widthForFractionLayout = 90;
                    max_width_for_layout_maultiplication = 80;
                }else{
                    if ((getResources().getConfiguration().screenLayout &
                            Configuration.SCREENLAYOUT_SIZE_MASK) ==
                            Configuration.SCREENLAYOUT_SIZE_NORMAL &&
                            metrics.densityDpi > 160
                            &&
                            (getResources().getConfiguration().screenLayout &
                                    Configuration.SCREENLAYOUT_SIZE_MASK) ==
                                    Configuration.SCREENLAYOUT_SIZE_NORMAL &&
                            metrics.densityDpi <= SCREEN_DENISITY){
                        maxWidth = 75;
                        MAX_WIDTH_FOR_FRACTION_TEXT = 38;
                        widthForFractionLayout = 67;
                        max_width_for_layout_maultiplication = 60;
                    }else{
                        maxWidth = 50;
                        MAX_WIDTH_FOR_FRACTION_TEXT = 25;
                        widthForFractionLayout = 45;
                        max_width_for_layout_maultiplication = 40;
                    }
                }
            }
        }
    }

    //for wrong time
    /**
     * This method calculate the time for currect scrore
     * @param playDatalist - playList
     * @return
     */
    protected int getNewMathPlayedData(ArrayList<MathScoreTransferObj> playDatalist){

        int secondsToBeAdded = 0;
        int totalTime = 0;

        if(playDatalist.size() > 0){
            for(int i = 0 ; i < playDatalist.size() ; i ++ ){
                totalTime = totalTime + playDatalist.get(i).getTimeTakenToAnswer();
            }

            secondsToBeAdded = (TOTAL_TIME_TO_PLAY - totalTime);

            //changes for new timer logic , on 11 Mar 2014
            if(isTimeDiffBetweenLastPlayQuestionAndCurrentTimeGreaterThen
                    (playDatalist.get(playDatalist.size() - 1).getEndDateTimeStr())){
                if(secondsToBeAdded > MAX_SECOND_TO_BE_ADDED)
                    secondsToBeAdded = MAX_SECOND_TO_BE_ADDED;
            }
            //end changes

            playDatalist.get(playDatalist.size() - 1).setEndDateTimeStr
                    (LearningCenterEquationSolve.getEndTimeAfterAddedExtraTime
                            (playDatalist.get(playDatalist.size() - 1).getEndDateTimeStr() , secondsToBeAdded));
        }

        return totalTime + secondsToBeAdded;
    }


    /**
     * for click on back press
     * This method update the time when user click on backPress
     * @param playedTime - time to play
     * @param playDatalist - playList
     * @return
     */
    protected int getNewMathPlayedData(int playedTime , ArrayList<MathScoreTransferObj> playDatalist){

        int secondsToBeAdded = 0;
        int totalTime = 0;

        if(playDatalist.size() > 0){

            for(int i = 0 ; i < playDatalist.size() ; i ++ ){
                totalTime = totalTime + playDatalist.get(i).getTimeTakenToAnswer();
            }

            secondsToBeAdded = (playedTime - totalTime);

            //changes for new timer logic , on 11 Mar 2014
            if(isTimeDiffBetweenLastPlayQuestionAndCurrentTimeGreaterThen
                    (playDatalist.get(playDatalist.size() - 1).getEndDateTimeStr())){
                if(secondsToBeAdded > MAX_SECOND_TO_BE_ADDED)
                    secondsToBeAdded = MAX_SECOND_TO_BE_ADDED;
            }
            //end changes

			/*playDatalist.get(playDatalist.size() - 1).setEndDateTimeStr
			(LearningCenterEquationSolve.getEndTimeAfterAddedExtraTime
					(playDatalist.get(playDatalist.size() - 1).getEndDateTimeStr() , (playedTime - totalTime)));*/

            playDatalist.get(playDatalist.size() - 1).setEndDateTimeStr
                    (LearningCenterEquationSolve.getEndTimeAfterAddedExtraTime
                            (playDatalist.get(playDatalist.size() - 1).getEndDateTimeStr() , secondsToBeAdded));

        }

        return totalTime + secondsToBeAdded;
    }

    /**
     * This method return the time diff in second
     * @param -secondsToBeAddedd start date in string formate
     * @param endDate   - end date in string formate
     * @return
     */
    @SuppressWarnings("deprecation")
    @SuppressLint("SimpleDateFormat")
    public static String getEndTimeAfterAddedExtraTime(String endDate , int secondsToBeAddedd){

		/*//changes for new timer logic , on 11 Mar 2014
		if(isTimeDiffBetweenLastPlayQuestionAndCurrentTimeGreaterThen(endDate)){
			if(secondsToBeAddedd > 30)
				secondsToBeAddedd = 30;
		}
		//end changes
		 */

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
        try {
            Date endDateFromString   = df.parse(endDate);
            endDateFromString.setSeconds(endDateFromString.getSeconds() + secondsToBeAddedd);
            return df.format(endDateFromString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return endDate;
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("SimpleDateFormat")
    /**
     * This method calculate the time diff b/w the last play equation and the current time
     * if its greater the 30 seconds then return true , otherwise false
     * @param lastEuqationEndTime
     * @return
     */
    public static boolean isTimeDiffBetweenLastPlayQuestionAndCurrentTimeGreaterThen(String lastEuqationEndTime){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
        try {
            Date currentDate         = df.parse((df.format(new Date())));
            Date endDateFromString   = df.parse(lastEuqationEndTime);
			/*int currentDateSeconds   		= currentDate.getSeconds();
			int endDateFromStringSeconds 	= endDateFromString.getSeconds();*/

			/*Log.e("LearnignCenterEquationSolve", "current seconds " + currentDateSeconds + " end seconds " + endDateFromStringSeconds);
			Log.e("LearnignCenterEquationSolve", "current seconds " + currentDate.getTime() + " end seconds " + endDateFromString.getTime()
					+ " diff " + ((currentDate.getTime() - endDateFromString.getTime()) / 1000));*/
			/*if(currentDateSeconds < endDateFromStringSeconds)
				currentDateSeconds = currentDateSeconds + 60;*/

            //calculate time in seconds , and compare
            if(((currentDate.getTime() - endDateFromString.getTime()) / 1000) > MAX_SECOND_TO_BE_ADDED)
                return true;
            return false;

        } catch (ParseException e) {
			/*Log.e("LearningCenterEquationSolve",
                    "inside isTimeDiffBetweenLastPlayQuestionAndCurrentTimeGreaterThen" +
					" Error while adding extraTime " + e.toString());*/
        }
        return false;
    }

    /**
     * Show the correct
     * @param onComplete
     * @param
     */
    protected void showCorrectAns(final OnAnswerGivenComplete onComplete,
                                  ArrayList<TextView> resultTextViewList){
        try{
            for(int i = 0 ; i < resultTextViewList.size() ; i ++ ){
                this.setGreenBackGroundToTextView(resultTextViewList.get(i));
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    onComplete.onComplete();
                }
            }, SLEEP_TIME_FOR_GREEN_BOX);
        }catch(Exception e){
            e.printStackTrace();
            onComplete.onComplete();
        }
    }

    /**
     * Show the correct
     * @param onComplete
     * @param
     */
    protected void showCorrectAns(final OnAnswerGivenComplete onComplete,
                                  ArrayList<TextView> resultTextViewList , int DESIGN_FOR){
        try{
            if(DESIGN_FOR == DEVISION_DESIGN){
                for(int i = 0 ; i < numberOfBoxesForDivision ; i ++ ){
                    this.setGreenBackGroundToTextViewForDivision(resultTextViewList.get(i));
                }
            }else if(DESIGN_FOR == MULTIPLICATION_DESIGN){
                if(this.getDigit(number2).size() > 1)
                    startIndex = this.getDigit(number2).size() * numberOfBoxesForMultiplication;
                for( int i = startIndex ; i < resultTextViewList.size() ; i ++ ){
                    this.setGreenBackGroundToTextViewForDivision(resultTextViewList.get(i));
                }
            }else if(DESIGN_FOR == FRACTION_DESIGN){
                for(int i = 0 ; i < resultTextViewList.size() ; i ++ ){
                    this.setGreenBackGroundToTextViewForDivision(resultTextViewList.get(i));
                }
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    onComplete.onComplete();
                }
            }, SLEEP_TIME_FOR_GREEN_BOX);
        }catch(Exception e){
            e.printStackTrace();
            onComplete.onComplete();
        }
    }

    /**
     * Set Green background to textview
     * @param txtView
     */
    protected void setGreenBackGroundToTextView(TextView txtView){
        //changes for tab
        if(isTab)
            txtView.setBackgroundResource(R.drawable.green_button_ipad);
        else
            txtView.setBackgroundResource(R.drawable.green_button);
    }

    /**
     * Set Red background to textview
     * @param txtView
     */
    protected void setRedBackgroundToTextView(TextView txtView){
        //changes for tab
        if(isTab)
            txtView.setBackgroundResource(R.drawable.red_button_ipad);
        else
            txtView.setBackgroundResource(R.drawable.red_button);
    }

    /**
     * Set Green background to textview
     * @param txtView
     */
    protected void setGreenBackGroundToTextViewForDivision(TextView txtView){
        //changes for tab
        if(isTab)
            txtView.setBackgroundResource(R.drawable.green_button_ipad);
        else
            txtView.setBackgroundResource(R.drawable.green_button_small);
    }

    /**
     * set Red background to textview
     * @param textView
     */
    private void setRedBackgroundToTextViewForDivision(TextView textView) {
        if(isTab)
            textView.setBackgroundResource(R.drawable.red_button_ipad);
        else
            textView.setBackgroundResource(R.drawable.red_button_small);
    }

    /**
     * Show the dialog for revere correct result
     * @param context
     * @param cdt
     * @param onClose
     * @param resultTextViewList
     * @param resultDigitsList
     */
    protected void showReverseCorrectAnsDialog(Context context , CountDownTimer cdt
            ,  final OnReverseDialogClose onClose , final ArrayList<TextView> resultTextViewList,
                                               final ArrayList<String> resultDigitsList ){
        try{
            DialogGenerator dg = new DialogGenerator(context);
            dg.generateDialogForSolvingProblem(cdt , new OnReverseDialogClose() {
                @Override
                public void onClose() {
                    for( int i = 0 ; i < resultTextViewList.size() ; i ++ )	{
                        setGreenBackGroundToTextView(resultTextViewList.get(i));
                        resultTextViewList.get(i).setText(resultDigitsList.get(i));
                    }

                    startFilping(onClose ,  resultTextViewList, resultDigitsList);
                }
            });
        }catch(Exception e){
            e.printStackTrace();
            onClose.onClose();
        }
    }

    /**
     * Show the dialog for revere correct result
     * @param context
     * @param cdt
     * @param onClose
     * @param resultTextViewList
     * @param resultDigitsList
     */
    protected void showReverseCorrectAnsDialog(Context context , CountDownTimer cdt
            ,  final OnReverseDialogClose onClose , final ArrayList<TextView> resultTextViewList,
                                               final ArrayList<String> resultDigitsList , final int DESIGN_FOR){
        try{
            DialogGenerator dg = new DialogGenerator(context);
            dg.generateDialogForSolvingProblem(cdt , new OnReverseDialogClose() {
                @Override
                public void onClose() {
                    if(DESIGN_FOR == MULTIPLICATION_DESIGN){
                        if(getDigit(number2).size() > 1)
                            startIndex = getDigit(number2).size() *
                                    numberOfBoxesForMultiplication;
                        for( int i = startIndex ; i < resultTextViewList.size() ; i ++ ){
                            setGreenBackGroundToTextViewForDivision(resultTextViewList.get(i));
                            resultTextViewList.get(i).setText(resultDigitsList.get(i));
                        }
                    }

                    startFilping(onClose ,  resultTextViewList, resultDigitsList , DESIGN_FOR);
                }
            });
        }catch(Exception e){
            e.printStackTrace();
            onClose.onClose();
        }
    }

    /**
     * Start flipping
     * @param onClose
     * @param resultTextViewList
     * @param resultDigitsList
     */
    private void startFilping(final OnReverseDialogClose onClose,
                              final ArrayList<TextView> resultTextViewList,
                              final ArrayList<String> resultDigitsList){

        //1st a flipping
        startHandler(resultTextViewList, resultDigitsList,
                1, new OnReverseDialogClose() {
                    @Override
                    public void onClose() {
                        //1st b flipping
                        startHandler(resultTextViewList, resultDigitsList,
                                0, new OnReverseDialogClose() {

                                    @Override
                                    public void onClose() {
                                        //2nd a flipping
                                        startHandler(resultTextViewList, resultDigitsList,
                                                1, new OnReverseDialogClose() {

                                                    @Override
                                                    public void onClose() {
                                                        //2nd b flipping
                                                        startHandler(resultTextViewList, resultDigitsList,
                                                                0, new OnReverseDialogClose() {

                                                                    @Override
                                                                    public void onClose() {

                                                                        new Handler().postDelayed(new Runnable() {
                                                                            @Override
                                                                            public void run() {
                                                                                onClose.onClose();
                                                                            }
                                                                        }, 500);
                                                                    }
                                                                });
                                                    }
                                                });
                                    }
                                });
                    }
                });
    }

    /**
     * Start flipping
     * @param onClose
     * @param resultTextViewList
     * @param resultDigitsList
     */
    private void startFilping(final OnReverseDialogClose onClose,
                              final ArrayList<TextView> resultTextViewList,
                              final ArrayList<String> resultDigitsList , final int DESIGN_FOR){

        //1st a flipping
        startHandler(resultTextViewList, resultDigitsList,
                1, new OnReverseDialogClose() {
                    @Override
                    public void onClose() {
                        //1st b flipping
                        startHandler(resultTextViewList, resultDigitsList,
                                0, new OnReverseDialogClose() {

                                    @Override
                                    public void onClose() {
                                        //2nd a flipping
                                        startHandler(resultTextViewList, resultDigitsList,
                                                1, new OnReverseDialogClose() {

                                                    @Override
                                                    public void onClose() {
                                                        //2nd b flipping
                                                        startHandler(resultTextViewList, resultDigitsList,
                                                                0, new OnReverseDialogClose() {

                                                                    @Override
                                                                    public void onClose() {

                                                                        new Handler().postDelayed(new Runnable() {
                                                                            @Override
                                                                            public void run() {
                                                                                onClose.onClose();
                                                                            }
                                                                        }, 500);
                                                                    }
                                                                } , DESIGN_FOR);
                                                    }
                                                } , DESIGN_FOR);
                                    }
                                } , DESIGN_FOR);
                    }
                } , DESIGN_FOR);
    }

    /**
     * start handler
     * @param resultTextViewList
     * @param resultDigitsList
     * @param isBlank
     * @param onClose
     */
    private void startHandler(final ArrayList<TextView> resultTextViewList,
                              final ArrayList<String> resultDigitsList , final int isBlank ,
                              final OnReverseDialogClose onClose){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                for( int j = 0 ; j < resultTextViewList.size() ; j ++ )	{
                    setGreenBackGroundToTextView(resultTextViewList.get(j));
                    if(isBlank == 1)
                        resultTextViewList.get(j).setText("");
                    else
                        resultTextViewList.get(j).setText(resultDigitsList.get(j));
                }
                onClose.onClose();
            }
        }, 300);
    }

    /**
     * start handler
     * @param resultTextViewList
     * @param resultDigitsList
     * @param isBlank
     * @param onClose
     */
    private void startHandler(final ArrayList<TextView> resultTextViewList,
                              final ArrayList<String> resultDigitsList , final int isBlank ,
                              final OnReverseDialogClose onClose , final int DESIGN_FOR){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(DESIGN_FOR == MULTIPLICATION_DESIGN){
                    if(getDigit(number2).size() > 1)
                        startIndex = getDigit(number2).size() *
                                numberOfBoxesForMultiplication;
                    for( int i = startIndex ; i < resultTextViewList.size() ; i ++ ){
                        setGreenBackGroundToTextViewForDivision(resultTextViewList.get(i));
                        if(isBlank == 1)
                            resultTextViewList.get(i).setText("");
                        else
                            resultTextViewList.get(i).setText(resultDigitsList.get(i));
                    }
                }
                onClose.onClose();
            }
        }, 300);
    }
	
	/*final private Runnable call = new Runnable() {
		public void run() {
			Log.v("test","this will run every minute");
		}
	};

	public static class Sync {
		private static Handler handler = new Handler();
		Runnable task;
		public Sync(Runnable task, long time) {
			this.task = task;
			handler.removeCallbacks(task);
			handler.postDelayed(task, time);
		}
	}*/

    /**
     * Return the time to wait for wrong ans
     * @param numberOfAttempt
     * @param sleepTime
     * @return
     */
    protected long getWrongAnswerShowTime(int numberOfAttempt , int sleepTime){
        if(numberOfAttempt == NUMBER_OF_MAX_ATTEMPTED)
            return sleepTime / 4 + 1000;//1000 for 1 more seconds
        return sleepTime / 4;
    }

}
