package com.mathfriendzy.controller.learningcenter.schoolcurriculum;

import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * This class contain the selected option data
 * @author Yashwant Singh
 *
 */
public class OptionSelectDataObj {

	private String textNumber;
	private RelativeLayout opationSeletedLayout;
	private int selectedOptionNumber;
	private TextView txtSelectedtext;
	private String selectedOptionTextValue;
	
	public String getSelectedOptionTextValue() {
		return selectedOptionTextValue;
	}
	public void setSelectedOptionTextValue(String selectedOptionTextValue) {
		this.selectedOptionTextValue = selectedOptionTextValue;
	}
	public TextView getTxtSelectedtext() {
		return txtSelectedtext;
	}
	public void setTxtSelectedtext(TextView txtSelectedtext) {
		this.txtSelectedtext = txtSelectedtext;
	}
	public String getTextNumber() {
		return textNumber;
	}
	public void setTextNumber(String textNumber) {
		this.textNumber = textNumber;
	}
	public RelativeLayout getOpationSeletedLayout() {
		return opationSeletedLayout;
	}
	public void setOpationSeletedLayout(RelativeLayout opationSeletedLayout) {
		this.opationSeletedLayout = opationSeletedLayout;
	}
	public int getSelectedOptionNumber() {
		return selectedOptionNumber;
	}
	public void setSelectedOptionNumber(int selectedOptionNumber) {
		this.selectedOptionNumber = selectedOptionNumber;
	}
	
}
