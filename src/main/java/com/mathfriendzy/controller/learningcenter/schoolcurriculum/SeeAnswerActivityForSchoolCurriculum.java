package com.mathfriendzy.controller.learningcenter.schoolcurriculum;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnUtteranceCompletedListener;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.learningcenter.NewLearnignCenter;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.singlefriendzy.SingleFriendzyMain;
import com.mathfriendzy.listener.OnSwipeTouchListener;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.LearnignCenterSchoolCurriculumServerOperation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.MathEquationTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SchoolCurriculumLearnignCenterimpl;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.UpdatedInfoTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.WordProblemQuestionTransferObj;
import com.mathfriendzy.model.schoolcurriculum.singlefriendzy.SingleFriendzyServerOperationForWordProblem;
import com.mathfriendzy.model.spanishchanges.SpanishChangesImpl;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.PlaySound;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

/**
 * This class for see answer for school curriculm
 * @author Yashwant Singh
 *
 */
public class SeeAnswerActivityForSchoolCurriculum extends Activity implements OnClickListener , 
TextToSpeech.OnInitListener{

	private TextView mfTitleHomeScreen = null;
	private Button   btnSeePrevious    = null;
	private Button   btnPlayAgain      = null;
	private Button   btnSeeNext        = null;

	private final int TAB_HEIGHT 	= 800;
	private final int TAB_WIDTH 	= 480;
	private final int TAB_DENISITY 	= 160;

	private RelativeLayout questionLayout = null;
	//for question and answer layout 
	private RelativeLayout  queLayout;
	private TextView txtQue;
	private ImageView txtQueImage;
	private TextView txtQueNo;
	private ImageView imgQuestion;
	private LinearLayout multipleChicelayout;

	//changes for Spanish
	private ImageView imgSpanish = null;
	private int languageCode  = 1;
	private final int ENGLISH = 1;
	private final int SPANISH = 2;
	private boolean isSpanish = false; 
	//end changes for Spanish

	private ScrollView scrollView = null;

	ArrayList<MathEquationTransferObj> playDataList = null;

	private int index = 0;
	private String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private boolean isTab = false;

	//for sound
	PlaySound playSound = null;
	private boolean isFromLearnignCenterSchoolCurriculum = false;

	//added for assessment test
	private boolean isCallForAssessment = false;

	private int selectedPlayGrade = 1;

	private final String TAG = this.getClass().getSimpleName();

	//for question translation , Issues list 5 march 2014 , issues no. 5
	private ShowTranslateQuestionAfterDownloading showQuestionAfterTranslation = null;
	//end chanegs

	//for tts
	private ImageView imgTTS = null;
	private TextToSpeech tts = null;
	private boolean isClickOnTTS = false;

	//open for result
	private boolean isCallForResult = false;
	private int playlistSize = 0;
    private OnSwipeTouchListener onSwipeTouchListener;


    //Assign Homework Show Question Detail Change
    private boolean isForAssignHomeworkShowQuestionDetail = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_see_answer_activity_for_school_curriculum);

		//for tts
		tts = new TextToSpeech(this, this);

		//only for Spanish Changes
		CommonUtils.isOneTimeClick = true;
		//end changes

		playSound = new PlaySound(this);
		playSound.setUserVolumeFromSeeAnswer();

		playDataList = new ArrayList<MathEquationTransferObj>();

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);  

		isTab = getResources().getBoolean(R.bool.isTablet);

		//changes for Spanish
		this.setLanguageCode();
		//end changes

		this.getIntentValue();
		this.setWidgetsReference();
		this.setTextFromTranslation();
		try{
			if(playDataList.size() > 0){
				//this.showQuestionAnswer(playDataList.get(index).getQuestionObj());
				//changes for Spanish
				this.getQuestionFromDatabase(playDataList.get(index).getQuestionObj().getQuestionId());
				//end changes
			}
		}catch (Exception e) {//added for assessment test result
			DialogGenerator dg = new DialogGenerator(this);
			dg.generateWarningDialog("Please Dowanload the Question For this Grade and see the result");
		}

		this.setListenerOnWidgets();
        this.setPlayAgainButtonVisibility();
	}

    //Assign Homework Show Question Detail Change
    private void setPlayAgainButtonVisibility(){
        if(isForAssignHomeworkShowQuestionDetail){
            btnPlayAgain.setVisibility(Button.GONE);
        }else{
            btnPlayAgain.setVisibility(Button.VISIBLE);
        }
    }

	//changes for Spanish
	private void setLanguageCode(){
		UserRegistrationOperation userObj = new UserRegistrationOperation(this);
		RegistereUserDto userPlayerObj = userObj.getUserData();
		if(userPlayerObj.getPreferedLanguageId().equals("1")){
			languageCode 	= ENGLISH;
		}else if(userPlayerObj.getPreferedLanguageId().equals("2")){
			languageCode 	= SPANISH;
		}
		this.setIsSpanishBooleanValue();
	}
	//end changes

	/**
	 * This method set or reset the isSpanish Value
	 */
	private void setIsSpanishBooleanValue(){
		if(languageCode == ENGLISH)
			isSpanish = true;
		else if(languageCode == SPANISH)
			isSpanish = false;
	}

	//changes for Spanish
	private void getQuestionFromDatabase(int questionId){
		/*languageCode = ENGLISH; 
		//changes for Spanish
		SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
		schoolImpl.openConnection();
		//get question from database into questionObj object
		WordProblemQuestionTransferObj questionObj  = schoolImpl.getQuestionByQuestionId(questionId);
		schoolImpl.closeConnection();*/

		WordProblemQuestionTransferObj questionObj = new WordProblemQuestionTransferObj();
		if(languageCode == SPANISH){
			SpanishChangesImpl schoolImpl = new SpanishChangesImpl(this);
			schoolImpl.openConn();
			//get question from database into questionObj object
			questionObj  = schoolImpl.getQuestionByQuestionId(questionId);
			schoolImpl.closeConn();
		}else if(languageCode == ENGLISH){
			SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
			schoolImpl.openConnection();
			//get question from database into questionObj object
			questionObj  = schoolImpl.getQuestionByQuestionId(questionId);
			schoolImpl.closeConnection();
		}
		this.setIsSpanishBooleanValue(); // changes after

		//this.showQuestionAnswer(questionObj);
        this.setLayoutForShowQuetionAndAnswerForSpanish(questionObj);
		//end changes
	}

	/**
	 * This method get intent value which is set into previous screen
	 * Play data list is get from intent
	 * which contains the user answer and right answer and is correct answer also
	 */
	@SuppressWarnings("unchecked")
	private void getIntentValue() {
        isForAssignHomeworkShowQuestionDetail = this.getIntent()
                .getBooleanExtra("isForAssignHomeworkShowQuestionDetail" , false);
        playDataList = (ArrayList<MathEquationTransferObj>)
				this.getIntent().getSerializableExtra("playDatalist");
		isFromLearnignCenterSchoolCurriculum = this.getIntent().
				getBooleanExtra("isFromLearnignCenterSchoolCurriculum", false);
		isCallForAssessment = this.getIntent().
				getBooleanExtra("isCallForAssessment", false);
		selectedPlayGrade = this.getIntent().getIntExtra("selectedGrade", 1);

		isCallForResult = this.getIntent().getBooleanExtra("isCallForresult", false);

		playlistSize = playDataList.size();
	}


	/**
	 * This method set widgets references
	 */
	private void setWidgetsReference() {
		mfTitleHomeScreen = (TextView) findViewById(R.id.mfTitleHomeScreen);
		btnSeePrevious    = (Button)   findViewById(R.id.btnSeePrevious);
		btnPlayAgain      = (Button)   findViewById(R.id.btnPlayAgain);
		btnSeeNext        = (Button)   findViewById(R.id.btnSeeNext);
		questionLayout    = (RelativeLayout) findViewById(R.id.questionLayout);
		scrollView        = (ScrollView) findViewById(R.id.scrollView);
		btnSeePrevious.setVisibility(Button.INVISIBLE);
	}

	/**
	 * This method set text from translation
	 */
	private void setTextFromTranslation() {
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		mfTitleHomeScreen.setText(transeletion.getTranselationTextByTextIdentifier("mfTitleHomeScreen"));
		btnPlayAgain.setText(transeletion.getTranselationTextByTextIdentifier("btnTitlePlayAgain"));
		transeletion.closeConnection();

		if(isCallForResult){
			this.setPlayAgainButtontext();
		}
	}


	/**
	 * This method show question and answer
	 */
	private void showQuestionAnswer(final WordProblemQuestionTransferObj questionObj) {

		try{		
			questionLayout.removeAllViews();

			boolean isAnswerCorrect = false;
			if(playDataList.get(index).getIsAnswerCorrect() == 1){
				isAnswerCorrect = true;
			}

			DisplayMetrics metrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(metrics);  
			LayoutInflater inflater = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
			RelativeLayout child;

			if(metrics.heightPixels <= TAB_HEIGHT && metrics.widthPixels >= TAB_WIDTH && metrics.densityDpi 
					== TAB_DENISITY)
			{
				child = (RelativeLayout) inflater.inflate
						(R.layout.schoolcurriculum_equation_solve_layout_for_fill_in_for_low_denisity, null);
			}
			else{
				child = (RelativeLayout) inflater.inflate
						(R.layout.schoolcurriculum_equation_solve_layout_for_fill_in, null);	
			}

			queLayout = (RelativeLayout) child.findViewById(R.id.queLayout);
			txtQue    = (TextView)       child.findViewById(R.id.txtQue);
			txtQueImage = (ImageView)    child.findViewById(R.id.txtQueImage);
			txtQueNo  = (TextView)       child.findViewById(R.id.txtQueNo);
			imgQuestion = (ImageView)    child.findViewById(R.id.imgQuestion);
			//for tts
			imgTTS    = (ImageView)      child.findViewById(R.id.imgSpeach);

			//changes for Spanish
			imgSpanish = (ImageView)     child.findViewById(R.id.imgSpanish);
			//imgSpanish.setVisibility(ImageView.INVISIBLE);
			imgSpanish.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					//Log.e("SeeAnserForSchoolCurriculum", "Click On Spanish");
					clickOnSpanish(questionObj);
				}
			});
			//end changes

			//for tts
			imgTTS.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if(questionObj.getQuestion().contains(".png")){

					}else{
						if(!isSpanish){
							textToSpeach(CommonUtils.SPANISH, questionObj.getQuestion() , questionObj.getQuestionId());
						}else{
							textToSpeach(CommonUtils.ENGLISH, questionObj.getQuestion() , questionObj.getQuestionId());
						}
					}
				}
			});
			//end changes

			multipleChicelayout = (LinearLayout) child.findViewById(R.id.multipleChicelayout);
			multipleChicelayout.removeAllViews();

			txtQueNo.setText((index + 1) + "");

			if(questionObj.getQuestion().contains(".png"))
			{
				imgTTS.setVisibility(ImageView.INVISIBLE);
				txtQueImage.setVisibility(ImageView.VISIBLE);
				txtQue.setVisibility(TextView.GONE);
				this.setBackGroundImageForQuestionImage(txtQueImage, questionObj.getQuestion());
			}else{
				txtQueImage.setVisibility(ImageView.GONE);
				txtQue.setVisibility(TextView.VISIBLE);
				//changes for Html or '' change into '
				if(questionObj.getQuestion().contains("&#"))
					txtQue.setText(Html.fromHtml(questionObj.getQuestion().replaceAll("''", "'")));
				else 
					txtQue.setText(questionObj.getQuestion().replaceAll("''", "'"));
				//txtQue.setText(Html.fromHtml(questionObj.getQuestion().replaceAll("''", "'")));
			}

			if(questionObj.getImage().length() > 0){
				this.setBackGroundImageForQuestionImage(imgQuestion, questionObj.getImage());
			}else{
				imgQuestion.setVisibility(ImageView.GONE);
			}

			//if(questionObj.getFillIn() != 1){
			ArrayList<Integer> userSelectedOption = 
					this.getCommaSepratedValueFromString(playDataList.get(index).getUserAnswer());
			ArrayList<Integer> databeseoption = 
					this.getCommaSepratedValueFromString(questionObj.getAns());

			ArrayList<RelativeLayout> optionLayoutList = new ArrayList<RelativeLayout>();
			ArrayList<TextView>      txtoptionTextlist = new ArrayList<TextView>();

			for(int i = 0 ; i < questionObj.getOptionList().size() ; i ++ ){
				if(questionObj.getOptionList().get(i).length() > 0){
					LayoutInflater inflaterOption = (LayoutInflater) 
							getSystemService(this.LAYOUT_INFLATER_SERVICE);
					RelativeLayout option = (RelativeLayout) inflater.inflate
							(R.layout.schoo_curricullum_equation_solve_layout_for_multiplechoice, null);
					RelativeLayout OptionLayout = (RelativeLayout) option.findViewById(R.id.OptionLayout);
					TextView       txtoptionNumber = (TextView) option.findViewById(R.id.txtoptionNumber);
					TextView       txtOption       = (TextView) option.findViewById(R.id.txtOption);
					ImageView      imgOptionImage  = (ImageView)option.findViewById(R.id.imgOptionImage);

					if(isTab)
						OptionLayout.setBackgroundResource(getResources().getIdentifier
								("option" + (i + 1) + "_ipad", "drawable",getPackageName()));
					else
						OptionLayout.setBackgroundResource(getResources().getIdentifier
								("option" + (i + 1) + "", "drawable",getPackageName()));

					txtoptionNumber.setText(alphabet.charAt(i) + "");

					if(questionObj.getOptionList().get(i).contains(".png")){
						imgOptionImage.setVisibility(ImageView.VISIBLE);
						txtOption.setVisibility(TextView.GONE);
						this.setBackGroundImageForQuestionImage(imgOptionImage, questionObj.
								getOptionList().get(i));
					}
					else{
						imgOptionImage.setVisibility(ImageView.GONE);
						txtOption.setVisibility(TextView.VISIBLE);
						//txtOption.setText(questionObj.getOptionList().get(i));
						//changes for Html or '' to '
						if(questionObj.getOptionList().get(i).contains("&#"))
							txtOption.setText(Html.fromHtml(questionObj.getOptionList().get(i).replaceAll("''", "'")));
						else
							txtOption.setText(questionObj.getOptionList().get(i).replaceAll("''", "'"));
					}

					optionLayoutList.add(OptionLayout);
					txtoptionTextlist.add(txtoptionNumber);
					multipleChicelayout.addView(option);
				}
			}

			if(questionObj.getFillIn() != 1){//without fill in
				if(isAnswerCorrect){
					for(int i = 0 ; i < userSelectedOption.size() ; i ++ ){
						for(int j = 0 ; j < databeseoption.size() ; j ++ ){
							if(userSelectedOption.get(i) == databeseoption.get(j)){
								txtoptionTextlist.get(userSelectedOption.get(i) - 1).setText("");
								if(isTab){
									optionLayoutList.get(userSelectedOption.get(i) - 1)
									.setBackgroundResource(R.drawable.right_ipad);
								}
								else{
									optionLayoutList.get(userSelectedOption.get(i) - 1)
									.setBackgroundResource(R.drawable.right);
								}
							}
						}
					}
				}else{

					for(int i = 0 ; i < databeseoption.size() ; i ++ ){
						if(isTab)
							optionLayoutList.get(databeseoption.get(i) - 1)
							.setBackgroundResource(R.drawable.not_selected_wrong_ipad);
						else
							optionLayoutList.get(databeseoption.get(i) - 1)
							.setBackgroundResource(R.drawable.not_selected_wrong);
					}

					boolean isUSerOptionRight = false;
					for(int i = 0 ; i < userSelectedOption.size() ; i ++ ){
						isUSerOptionRight = false;
						for(int j = 0 ; j < databeseoption.size() ; j ++ ){
							if(userSelectedOption.get(i) == databeseoption.get(j)){
								txtoptionTextlist.get(userSelectedOption.get(i) - 1).setText("");
								isUSerOptionRight = true;
								if(isTab){
									optionLayoutList.get(userSelectedOption.get(i) - 1)
									.setBackgroundResource(R.drawable.right_ipad);
								}
								else{
									optionLayoutList.get(userSelectedOption.get(i) - 1)
									.setBackgroundResource(R.drawable.right);
								}
								break;
							}
						}

						if(!isUSerOptionRight){
							txtoptionTextlist.get(userSelectedOption.get(i) - 1).setText("");
							if(isTab){
								optionLayoutList.get(userSelectedOption.get(i) - 1)
								.setBackgroundResource(R.drawable.ans_selected_wrong_ipad);
							}
							else{
								optionLayoutList.get(userSelectedOption.get(i) - 1)
								.setBackgroundResource(R.drawable.ans_selected_wrong);
							}
						}
					}
				}
			}
			else{//with fill

				if(isTab){
					optionLayoutList.get(0)
					.setBackgroundResource(R.drawable.not_selected_wrong_ipad);
				}
				else{
					optionLayoutList.get(0)
					.setBackgroundResource(R.drawable.not_selected_wrong);
				}

				if(isAnswerCorrect){
					txtoptionTextlist.get(0).setText("");
					if(isTab){
						optionLayoutList.get(0)
						.setBackgroundResource(R.drawable.right_ipad);
					}
					else{
						optionLayoutList.get(0)
						.setBackgroundResource(R.drawable.right);
					}
				}else{
					LayoutInflater inflaterOption = (LayoutInflater) 
							getSystemService(this.LAYOUT_INFLATER_SERVICE);
					RelativeLayout option = (RelativeLayout) inflater.inflate
							(R.layout.schoo_curricullum_equation_solve_layout_for_multiplechoice, null);
					RelativeLayout OptionLayout = (RelativeLayout) option.findViewById(R.id.OptionLayout);
					TextView       txtoptionNumber = (TextView) option.findViewById(R.id.txtoptionNumber);
					TextView       txtOption       = (TextView) option.findViewById(R.id.txtOption);
					ImageView      imgOptionImage  = (ImageView)option.findViewById(R.id.imgOptionImage);

					txtoptionNumber.setText("");
					imgOptionImage.setVisibility(ImageView.GONE);
					txtOption.setText(playDataList.get(index).getUserAnswer());

					if(isTab)
						OptionLayout.setBackgroundResource(R.drawable.ans_selected_wrong_ipad);
					else
						OptionLayout.setBackgroundResource(R.drawable.ans_selected_wrong);

					multipleChicelayout.addView(option);
				}
			}
			questionLayout.addView(child);
			scrollView.smoothScrollTo(0, 0);
		}catch (Exception e) {
			//Log.e("SeeAnswerActivityForSchoolCurriculum", "inside showQuestionAnswer Error while showing answer " + e.toString());
            e.printStackTrace();
		}
	}

	/**
	 * This method set the back to the question image
	 * @param imgQuestion
	 * @param imageName
	 */
	private void setBackGroundImageForQuestionImage(ImageView imgQuestion , String imageName){

		SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
		schoolImpl.openConnection();
		if(schoolImpl.getImage(imageName) != null){
			imgQuestion.setImageBitmap(CommonUtils.getBitmapFromByte(schoolImpl.getImage(imageName)));
		}
		schoolImpl.closeConnection();
	}

	/**
	 * This method convert the string with comma into the integer array list with comm saperated
	 * @param stringWithCommas
	 * @return
	 */
	private ArrayList<Integer> getCommaSepratedValueFromString(String stringWithCommas){
		stringWithCommas = stringWithCommas.replaceAll(" ", "");
		ArrayList<Integer> listWithoutComma = new ArrayList<Integer>();

		for(int i = 0 ; i < stringWithCommas.length() ; i ++ ){
			if(stringWithCommas.charAt(i) != ','){
				try{
					listWithoutComma.add(Integer.parseInt(stringWithCommas.charAt(i) +""));
				}
				catch(Exception e){
					/*Log.e("SeeAnswerActivityForSchoolCurriculum", " error while converting String to integer"
							+ e.toString());*/
                    e.printStackTrace();
				}
			}
		}
		return listWithoutComma;
	}

	/**
	 * This method set listener on widgets
	 */
	private void setListenerOnWidgets() {
		btnSeePrevious.setOnClickListener(this);
		btnSeeNext.setOnClickListener(this);
		btnPlayAgain.setOnClickListener(this);
        this.swipeListener();
	}

	/**
	 * This method set the visibility of see previous and next button
	 */
	private void setVisibilityOfPreviousAndNextButton(){
		if(index == 0)
			btnSeePrevious.setVisibility(Button.INVISIBLE);
		else
			btnSeePrevious.setVisibility(Button.VISIBLE);

		if(index == playDataList.size() - 1)
			btnSeeNext.setVisibility(Button.INVISIBLE);
		else
			btnSeeNext.setVisibility(Button.VISIBLE);
	}

	/**
	 * Number of question play text on play again button when it open
	 *  for the show result from the result screen
	 */
	private void setPlayAgainButtontext(){
		if(isCallForResult){
			btnPlayAgain.setText((index + 1) + " of " + playlistSize);
		}
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnSeePrevious : 
			playSound.playSoundForSchoolSurriculumPagePeel(this);
			if(index > 0){
				index -- ;
				//this.showQuestionAnswer(playDataList.get(index).getQuestionObj());
				//changes for Spanish
				this.getQuestionFromDatabase(playDataList.get(index).getQuestionObj().getQuestionId());
				//end changes
				this.setPlayAgainButtontext();
			}
			this.setVisibilityOfPreviousAndNextButton();
			break;
		case R.id.btnSeeNext : 
			playSound.playSoundForSchoolSurriculumPagePeel(this);
			if(index < playDataList.size() - 1){
				index ++ ;
				//this.showQuestionAnswer(playDataList.get(index).getQuestionObj());
				//changes for Spanish
				this.getQuestionFromDatabase(playDataList.get(index).getQuestionObj().getQuestionId());
				//end changes
				this.setPlayAgainButtontext();
			}
			this.setVisibilityOfPreviousAndNextButton();
			break;
		case R.id.btnPlayAgain :
			if(!isCallForResult){
				this.setDefoultSound();
				if(isCallForAssessment){
					startActivity(new Intent(this,MainActivity.class));
				}else{
					if(isFromLearnignCenterSchoolCurriculum)
						startActivity(new Intent(this,NewLearnignCenter.class));
					else
						startActivity(new Intent(this,SingleFriendzyMain.class));
				}
			}
			break;
		}
	}

	@Override
	public void onBackPressed() {
		if(isForAssignHomeworkShowQuestionDetail){
            finish();
            return ;
        }
        this.setDefoultSound();
		startActivity(new Intent(this,MainActivity.class));
		super.onBackPressed();
	}

	//changes for Spanish
	/**
	 * This method call when user click on button to convert language
	 * @param questionObj
	 */
	private void clickOnSpanish(WordProblemQuestionTransferObj questionObj){

		//for question translation , Issues list 5 march 2014 , issues no. 5
		final int questionId = questionObj.getQuestionId();

		/**
		 * This method call after question downloaded in the translated
		 */
		showQuestionAfterTranslation =	new ShowTranslateQuestionAfterDownloading() {
			@Override
			public void showQuestionAfterTranslationDownload(
					WordProblemQuestionTransferObj questionObj, boolean isAssessment) {

				if(isSpanish){
					SpanishChangesImpl schoolImpl = new SpanishChangesImpl
							(SeeAnswerActivityForSchoolCurriculum.this);
					schoolImpl.openConn();
					questionObj  = schoolImpl.getQuestionByQuestionId(questionId);
					schoolImpl.closeConn();
				}else{
					SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl
							(SeeAnswerActivityForSchoolCurriculum.this);
					schoolImpl.openConnection();
					questionObj  = schoolImpl.getQuestionByQuestionId(questionId);
					schoolImpl.closeConnection();
				}

				showLanguageChangeQuestion(questionObj);
			}
		};
		//end changes

		if(isSpanish){
			SpanishChangesImpl schoolImpl = new SpanishChangesImpl(this);
			schoolImpl.openConn();
			boolean isQuestionLoaded = schoolImpl.isQuestionLeaded(selectedPlayGrade);

			if(isQuestionLoaded){
				if(CommonUtils.isInternetConnectionAvailable(this)
						&& CommonUtils.isOneTimeClick){
					new GetWordProblemQuestionUpdatedDate(selectedPlayGrade , false
							,CommonUtils.SPANISH).execute(null,null,null);
				}else{
					//get question from database into questionObj object
					questionObj  = schoolImpl.getQuestionByQuestionId(questionObj.getQuestionId());
					this.showLanguageChangeQuestion(questionObj);
				}
			}
			else{
				if(CommonUtils.isOneTimeClick){
					this.showPopForDownLoadingQuestion(CommonUtils.SPANISH , selectedPlayGrade);
				}else{
					Log.e(TAG, "Nothing Happen");
				}
			}
			schoolImpl.closeConn();

		}else {//for English

			SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
			schoolImpl.openConnection();
			boolean isQuestionLoaded = schoolImpl.isQuestionLeaded(selectedPlayGrade);

			if(isQuestionLoaded){
				if(CommonUtils.isInternetConnectionAvailable(this)
						&& CommonUtils.isOneTimeClick){
					new GetWordProblemQuestionUpdatedDate(selectedPlayGrade , false
							,CommonUtils.ENGLISH).execute(null,null,null);
				}else{
					//get question from database into questionObj object
					questionObj  = schoolImpl.getQuestionByQuestionId(questionObj.getQuestionId());
					this.showLanguageChangeQuestion(questionObj);
				}
			}
			else{
				if(CommonUtils.isOneTimeClick){
					this.showPopForDownLoadingQuestion(CommonUtils.ENGLISH , selectedPlayGrade);
				}else{
					Log.e(TAG, "Nothing Happen");
				}
			}
			schoolImpl.closeConnection();
		}
	}

	/**
	 * This method show the new questions in diff language
	 * @param questionObj
	 */
	private void showLanguageChangeQuestion(WordProblemQuestionTransferObj questionObj){
		isSpanish = !isSpanish;//changes after discussing 
		this.setLayoutForShowQuetionAndAnswerForSpanish(questionObj);
	}


	/**
	 * This method call when user click on language change and question is not in database
	 */
	private void showPopForDownLoadingQuestion(int langCode , int grade){
		DialogGenerator dg = new DialogGenerator(this);
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		dg.generateDialogSpanishChagesQuestionDownload(transeletion.
				getTranselationTextByTextIdentifier("lblTappingThisFriendzyGlobe")
				+ " Spanish " + 
				transeletion.getTranselationTextByTextIdentifier("lblTextWhilePlayingWordProblems")
				+ " Spanish " + 
				transeletion.getTranselationTextByTextIdentifier("lblWordProblemsForYourGradeToUse")
				, langCode , grade ,true,false,null,null,0,null , showQuestionAfterTranslation);
		transeletion.closeConnection();
	}

	/**
	 * This method return the image list from question , answer,option,and imageField
	 * @param questionObj
	 * @return
	 */
	protected ArrayList<String> getImageArrayList(WordProblemQuestionTransferObj questionObj){

		ArrayList<String> imageNameList = new ArrayList<String>();

		if(questionObj.getQuestion().contains(".png"))
			imageNameList.add(questionObj.getQuestion());

		for(int i = 0 ; i < questionObj.getOptionList().size() ; i ++ ){
			if(questionObj.getOptionList().get(i).contains(".png"))
				imageNameList.add(questionObj.getOptionList().get(i));
		}

		if(!questionObj.getImage().equals(""))
			imageNameList.add(questionObj.getImage());

		return imageNameList;

	}

	/**
	 * This method check availabity of images in database
	 * if the the images not found the add into the the imageNot found list
	 * and return this list
	 * @param imageNameList
	 */
	protected ArrayList<String> checkImageAvalability(ArrayList<String> imageNameList){
		ArrayList<String> unAvailableImageList = new ArrayList<String>();

		SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
		schoolImpl.openConnection();
		for(int i = 0 ; i < imageNameList.size() ; i ++ ){
			if(!schoolImpl.checkImageAvailabity(imageNameList.get(i)))
				unAvailableImageList.add(imageNameList.get(i));
		}
		schoolImpl.closeConnection();

		return unAvailableImageList;
	}

	/**
	 * This method inflate the layout for show question and answer
	 * @param questionObj
	 */
	private void setLayoutForShowQuetionAndAnswerForSpanish(WordProblemQuestionTransferObj questionObj){
		ArrayList<String> imageNameList        = this.getImageArrayList(questionObj);
		ArrayList<String> unAvailableImageList = this.checkImageAvalability(imageNameList);

		if(unAvailableImageList.size() > 0){
			if(CommonUtils.isInternetConnectionAvailable(this)){
				new DawnloadImageForSchooCurriculum(unAvailableImageList, this, questionObj)
				.execute(null,null,null);
			}else{
				this.showQuestionAnswer(questionObj);
			}
		}
		else{
			this.showQuestionAnswer(questionObj);
		}
	}

	/**
	 * This class dawnload image from server for schoolcurriculum
	 * @author Yashwant Singh
	 *
	 */
	class DawnloadImageForSchooCurriculum extends AsyncTask<Void, Void, Void>{

		private ArrayList<String> imageNamelist;
		private byte [] imageFromServer;
		private Context context;
		SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl;
		LearnignCenterSchoolCurriculumServerOperation learningCenterServerobj;
		private WordProblemQuestionTransferObj questionObj;

		private ProgressDialog pd = null;

		public DawnloadImageForSchooCurriculum(ArrayList<String> imageNamelist , Context context,
				WordProblemQuestionTransferObj questionObj){
			this.imageNamelist 	= imageNamelist;
			this.context   		= context;
			this.questionObj    = questionObj;
		}

		@Override
		protected void onPreExecute() {
			pd = CommonUtils.getProgressDialog(context);
			pd.show();
			schooloCurriculumImpl   = new SchoolCurriculumLearnignCenterimpl(context);
			learningCenterServerobj = new LearnignCenterSchoolCurriculumServerOperation();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			for(int i = 0 ; i < imageNamelist.size() ; i ++ ){
				schooloCurriculumImpl.openConnection();
				if(!schooloCurriculumImpl.isImageAlreadyDawnloaded(imageNamelist.get(i))){
					imageFromServer = learningCenterServerobj.dawabloadImageFromServer(imageNamelist.
							get(i));
					schooloCurriculumImpl.insertIntoSchoolCurriculumImageTable(imageNamelist.get(i), 
							imageFromServer);
				}
				schooloCurriculumImpl.closeConnection();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			pd.cancel();
			showQuestionAnswer(questionObj);
			super.onPostExecute(result);
		}
	}
	//end changes

	//changes for Spanish
	//for downloading questions
	/**
	 * This class get word problem updated dates from server
	 * @author Yashwant Singh
	 *
	 */
	private class GetWordProblemQuestionUpdatedDate extends AsyncTask<Void, Void, ArrayList<UpdatedInfoTransferObj>>{

		private int grade;
		private ProgressDialog pd;
		private boolean isFisrtTimeCallForGrade;
		private int langCode = 1;

		GetWordProblemQuestionUpdatedDate(int grade , boolean isFisrtTimeCallForGrade
				,int langCode){
			this.grade = grade;
			this.isFisrtTimeCallForGrade = isFisrtTimeCallForGrade;
			this.langCode = langCode;
		}

		@Override
		protected void onPreExecute() {
			if(isFisrtTimeCallForGrade == false){
				pd = CommonUtils.getProgressDialog(SeeAnswerActivityForSchoolCurriculum.this);
				pd.show();
			}
			super.onPreExecute();
		}

		@Override
		protected ArrayList<UpdatedInfoTransferObj>  doInBackground(Void... params) {
			SingleFriendzyServerOperationForWordProblem serverObj = 
					new SingleFriendzyServerOperationForWordProblem();
			//changes for Spanish
			ArrayList<UpdatedInfoTransferObj>  updatedList = new ArrayList<UpdatedInfoTransferObj>();
			if(langCode == CommonUtils.ENGLISH)
				updatedList = serverObj.getWordProbleQuestionUpdatedData();
			else//for Spanish
				updatedList = serverObj.getWordProbleQuestionUpdatedDateForSpanish(CommonUtils.SPANISH);
			return updatedList;
		}

		@Override
		protected void onPostExecute(ArrayList<UpdatedInfoTransferObj>  updatedList) {
			if(isFisrtTimeCallForGrade == false){
				pd.cancel();
			}

			if(updatedList != null){
				SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl = 
						new SchoolCurriculumLearnignCenterimpl(SeeAnswerActivityForSchoolCurriculum.this);
				schooloCurriculumImpl.openConnection();

				//changes for Spanish
				SpanishChangesImpl implObj = new SpanishChangesImpl(SeeAnswerActivityForSchoolCurriculum.this);
				implObj.openConn();

				if(langCode	== CommonUtils.ENGLISH){
					if(!schooloCurriculumImpl.isWordProblemUpdateDetailsTableDataExist())
						schooloCurriculumImpl.insertIntoWordProblemUpdateDetails(updatedList);

					String updatedDateFromDatabase = schooloCurriculumImpl.getUpdatedDate(grade);
					if(CommonUtils.isUpdateDateChange(updatedDateFromDatabase , grade , updatedList))
					{
						String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
						schooloCurriculumImpl.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
						schooloCurriculumImpl.closeConnection();
						implObj.closeConn();
						//changes
						showPopForDownLoadingQuestion(CommonUtils.ENGLISH , grade);
					}
					else{
						CommonUtils.isOneTimeClick = false;
						schooloCurriculumImpl.closeConnection();
						implObj.closeConn();

						if(showQuestionAfterTranslation != null)
							showQuestionAfterTranslation.showQuestionAfterTranslationDownload(null, false);
					}

				}else{//for Spanish
					if(!implObj.isWordProblemUpdateDetailsTableDataExist())
						implObj.insertIntoWordProblemUpdateDetails(updatedList);

					String updatedDateFromDatabase = implObj.getUpdatedDate(grade);
					if(CommonUtils.isUpdateDateChange(updatedDateFromDatabase , grade , updatedList))
					{
						String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
						implObj.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
						schooloCurriculumImpl.closeConnection();
						implObj.closeConn();

						//changes
						showPopForDownLoadingQuestion(CommonUtils.SPANISH , grade);
					}
					else{
						CommonUtils.isOneTimeClick = false;
						schooloCurriculumImpl.closeConnection();
						implObj.closeConn();

						if(showQuestionAfterTranslation != null)
							showQuestionAfterTranslation.showQuestionAfterTranslationDownload(null, false);
					}
				}
			}else{
				if(isFisrtTimeCallForGrade == false){
					CommonUtils.showInternetDialog(SeeAnswerActivityForSchoolCurriculum.this);
				}
			}
			super.onPostExecute(updatedList);
		}
	}

	/**
	 * This method set the defoult sound of device
	 */
	private void setDefoultSound(){
		if(playSound != null)
			playSound.setDefaultVolumeWhenSoundClose(this);
	}

	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}

	/**
	 * this method speach text in spanish and english
	 * @param languageCode
	 * @param text
	 */
	@SuppressWarnings("deprecation")
	private void textToSpeach(int languageCode , String text , int questionId){

		if(!isClickOnTTS){
			try{
				isClickOnTTS = true;

				WordProblemQuestionTransferObj questionObj = null;

				if(languageCode == CommonUtils.SPANISH){
					SpanishChangesImpl schoolImpl = new SpanishChangesImpl(this);
					schoolImpl.openConn();
					questionObj  = schoolImpl.getQuestionByQuestionId(questionId);
					schoolImpl.closeConn();
				}else{
					SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
					schoolImpl.openConnection();
					questionObj  = schoolImpl.getQuestionByQuestionId(questionId);
					schoolImpl.closeConnection();
				}

				if(tts != null){
					Locale locale = null;
					if(languageCode == CommonUtils.ENGLISH)
						locale = Locale.ENGLISH;
					else if(languageCode == CommonUtils.SPANISH){
						//locale = new Locale("es", "ES");
						locale = new Locale("es", "mx");
					}
					tts.setLanguage(locale);

					HashMap<String, String> myHashAlarm = new HashMap<String, String>();
					myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "SOME MESSAGE");

					tts.speak(questionObj.getQuestion(), TextToSpeech.QUEUE_FLUSH, myHashAlarm);
					tts.setOnUtteranceCompletedListener(new OnUtteranceCompletedListener() {
						@Override
						public void onUtteranceCompleted(String utteranceId) {
							isClickOnTTS = false;
						}
					});
				}
			}catch(Exception e){
				Log.e(TAG, e.toString());
			}
		}
	}

	@Override
	public void onInit(int status) {
		// TODO Auto-generated method stub
	}

	@Override
	protected void onDestroy() {
		// Don't forget to shutdown tts!
		if (tts != null) {
			tts.stop();
			tts.shutdown();
		}
		super.onDestroy();
	}

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev){
        onSwipeTouchListener.getGestureDetector().onTouchEvent(ev);
        return super.dispatchTouchEvent(ev);
    }

    /**
     * Swipe listener
     */
    private void swipeListener(){
        onSwipeTouchListener = new OnSwipeTouchListener() {
            public void onSwipeRight() {
                try {
                    playSound.playSoundForSchoolSurriculumPagePeel(SeeAnswerActivityForSchoolCurriculum.this);
                    if (index > 0) {
                        index--;
                        getQuestionFromDatabase(playDataList.get(index).getQuestionObj().getQuestionId());
                        setPlayAgainButtontext();
                    }
                    setVisibilityOfPreviousAndNextButton();
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
            public void onSwipeLeft() {
                try {
                    playSound.playSoundForSchoolSurriculumPagePeel(SeeAnswerActivityForSchoolCurriculum.this);
                    if (index < playDataList.size() - 1) {
                        index++;
                        getQuestionFromDatabase(playDataList.get(index).getQuestionObj().getQuestionId());
                        setPlayAgainButtontext();
                    }
                    setVisibilityOfPreviousAndNextButton();
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        };
    }
}
