package com.mathfriendzy.controller.learningcenter.schoolcurriculum;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.OnScrollChange;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SchoolCurriculumLearnignCenterimpl;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.WordProblemQuestionTransferObj;
import com.mathfriendzy.utils.CommonUtils;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;

import static com.mathfriendzy.helper.MathFriendzyHelper.ON_SCROLLING;
import static com.mathfriendzy.helper.MathFriendzyHelper.SCROLL_DOWN_COMPLETE;
import static com.mathfriendzy.helper.MathFriendzyHelper.SCROLL_UP_COMPLETE;
import static com.mathfriendzy.helper.MathFriendzyHelper.scrollDown;
import static com.mathfriendzy.helper.MathFriendzyHelper.scrollUp;

public class WorkAreaForWordProblem extends ActBaseClass implements OnClickListener , OnScrollChange {

	private Paint mPaint, mBitmapPaint		= null;
	private MyView mView					= null;
	private LinearLayout layRough			= null;
	private Bitmap mBitmap					= null;
	private Canvas mCanvas					= null;
	private Path mPath						= null;
	private Button clearPaint				= null;
	private Button drawPaint				= null;
	private Button btnScroll				= null;

	//widgets
	private TextView txtTitleScreen         = null;
	private TextView txtTimer               = null;
	private TextView txtQueNo               = null;
	private TextView txtQue                 = null;
	private ImageView txtQueImage           = null;
	private ImageView imgQuestion           = null;

	private WordProblemQuestionTransferObj questionObj = null;
	private int questionNumber = 0;

	//for new timer
	private CountDownTimer timer = null;
	private long startTimerForRoughArea = 0;

	//for timer
	Handler customHandler = new Handler();

	long timeInMilliseconds = 0L;
	long timeSwapBuff = 0L;
	long updatedTime = 0L;
	long startTimeForTimer = 0L;

	//changes for assessment test
	private boolean isCallFromAssessmentTest = false;

	//for sound 
	private boolean isClickOnBack = false;

	//for home work
	private boolean isFromHomeWork = false;

    private Button btnScrollUp = null;
    private Button btnScrollDown = null;
    private ScrollView scroll = null;
    private boolean isEraseMode = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_work_area_for_word_problem);

		this.getIntentData();
		this.setWidgetsReferences();
		this.setTextOnWidget();
		this.setListenerOnWidgets();
		this.setQuestionData();
		this.setCustomView();
        this.setPencilSelected(true);
        this.setEraserSelected(false);

		//changes for assessment test
		if(isCallFromAssessmentTest)
		{
			txtTimer.setVisibility(TextView.INVISIBLE);	
		}else{
			//changes for new timer
			this.startCountDawnTimer();
			//this.startTimer();
		}

		//for show timer
		this.showTimer();

	}

    /**
     * Set Pencil Selected And UnSelected Background
     * @param isSelected
     */
    private void setPencilSelected(boolean isSelected){
        if(isSelected){
            drawPaint.setBackgroundResource(R.drawable.pencil);
        }else{
            drawPaint.setBackgroundResource(R.drawable.grey_pencil);
        }
    }

    /**
     * Set Erase Selected and Unselected background
     * @param isSelected
     */
    private void setEraserSelected(boolean isSelected){
        if(isSelected){
            clearPaint.setBackgroundResource(R.drawable.eraser);
        }else{
            clearPaint.setBackgroundResource(R.drawable.grey_eraser);
        }
    }

	/**
	 * This method start the countdawn timer
	 */
	private void startCountDawnTimer() {
		timer = new CountDownTimer(this.getIntent().getLongExtra("startTime", 0) , 1 * 1000)
		{
			@Override
			public void onTick(long millisUntilFinished) 
			{		
				startTimerForRoughArea = millisUntilFinished;
				if(((millisUntilFinished/1000) % 60) < 10)
					txtTimer.setText(((millisUntilFinished/1000) / 60) + ":0" + ((millisUntilFinished/1000) % 60));
				else
					txtTimer.setText(((millisUntilFinished/1000) / 60) + ":" + ((millisUntilFinished/1000) % 60));
			}

			@Override
			public void onFinish() 
			{				
				startTimer();
			}
		};
		timer.start();
	}


	/**
	 * This method get intent data which is set in previous screen
	 */
	private void getIntentData() {

		//for HomeWork
		isFromHomeWork = this.getIntent().getBooleanExtra("isFromHomeWork", false);
		questionObj  = (WordProblemQuestionTransferObj) this.getIntent().
				getSerializableExtra("workAreaData");
		questionNumber    = this.getIntent().getIntExtra("questionNo", 0);

		//changes for assessment test
		if(this.getIntent().getBooleanExtra("isCallFromAssessmentTest", false)){
			isCallFromAssessmentTest = true;
		}else{
			startTimeForTimer = this.getIntent().getLongExtra("startTimeForTimer", 0l);
		}
	}

	/**
	 * This method show timer or not
	 */
	private void showTimer(){
		if(!isFromHomeWork){
			if(this.getIntent().getBooleanExtra("isCallFromLearningCenter", false)){
				//for show timer or not
				if(CommonUtils.isShowTimer){
					txtTimer.setVisibility(TextView.VISIBLE);
				}else{
					txtTimer.setVisibility(TextView.GONE);
				}
			}
		}else{
			txtTimer.setVisibility(TextView.GONE);
		}
	}

	/**
	 * This method set the timer
	 */
	private void startTimer() {
		if(startTimeForTimer == 0)
			startTimeForTimer = SystemClock.uptimeMillis();

		Runnable updateTimerThread = new Runnable() {
			public void run() {
				timeInMilliseconds = SystemClock.uptimeMillis() - startTimeForTimer;
				updatedTime = timeSwapBuff + timeInMilliseconds;

				int secs = (int) (updatedTime / 1000);
				int mins = secs / 60;
				secs = secs % 60;
				//txtTimer.setText(String.format("%02d", mins) + ":"+ String.format("%02d", secs));
				//for format change
				txtTimer.setText(mins + ":"+ String.format("%02d", secs));
				//int milliseconds = (int) (updatedTime % 1000);
				/*Log.e(TAG, "" + String.format("%02d", mins) + ":"
						+ String.format("%02d", secs) + ":"
						+ String.format("%03d", milliseconds));*/

				customHandler.postDelayed(this, 1000);
			}
		};
		customHandler.postDelayed(updateTimerThread, 0);
	}



	/**
	 * This method set the question data
	 */
	private void setQuestionData() {

		txtQueNo.setText(questionNumber + "");
		if(questionObj.getQuestion().contains(".png"))
		{
			txtQueImage.setVisibility(ImageView.VISIBLE);
			txtQue.setVisibility(TextView.GONE);
			this.setBackGroundImageForQuestionImage(txtQueImage, questionObj.getQuestion());
		}else{
			txtQueImage.setVisibility(ImageView.GONE);
			txtQue.setVisibility(TextView.VISIBLE);
			//changes for Html or '' change into '
			if(questionObj.getQuestion().contains("&#"))
				txtQue.setText(Html.fromHtml(questionObj.getQuestion().replaceAll("''", "'")));
			else 
				txtQue.setText(questionObj.getQuestion().replaceAll("''", "'"));
			//txtQue.setText(Html.fromHtml(questionObj.getQuestion().replaceAll("''", "'")));
		}

		if(questionObj.getImage().length() > 0){
			this.setBackGroundImageForQuestionImage(imgQuestion, questionObj.getImage());
		}else{
			imgQuestion.setVisibility(ImageView.GONE);
		}		
	}

	/**
	 * This method set the back to the question image
	 * @param imgQuestion
	 * @param imageName
	 */
	private void setBackGroundImageForQuestionImage(ImageView imgQuestion , String imageName){
		SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
		schoolImpl.openConnection();
		if(schoolImpl.getImage(imageName) != null){
			imgQuestion.setImageBitmap(CommonUtils.getBitmapFromByte(schoolImpl.getImage(imageName)));
		}
		schoolImpl.closeConnection();
	}


	/**
	 * This method set widgets references
	 */
	private void setWidgetsReferences() {
		layRough 		= (LinearLayout) findViewById(R.id.layoutRough);
		clearPaint      = (Button) findViewById(R.id.clearPaint); 
		drawPaint       = (Button) findViewById(R.id.draw);
		btnScroll       = (Button) findViewById(R.id.btnScroll);

		txtTitleScreen  = (TextView) findViewById(R.id.txtTitleScreen);
		txtTimer        = (TextView) findViewById(R.id.txtTimer);
		txtQueNo        = (TextView) findViewById(R.id.txtQueNo);
		txtQue          = (TextView) findViewById(R.id.txtQue);
		txtQueImage     = (ImageView)findViewById(R.id.txtQueImage);
		imgQuestion     = (ImageView)findViewById(R.id.imgQuestion);

        btnScrollUp = (Button) findViewById(R.id.btnScrollUp);
        btnScrollDown = (Button) findViewById(R.id.btnScrollDown);
        scroll          = (ScrollView) findViewById(R.id.scroll);
	}

	/**
	 * This method set text from translation
	 */
	private void setTextOnWidget() 
	{	
		Translation translate = new Translation(this);
		translate.openConnection();		
		txtTitleScreen.setText(translate.getTranselationTextByTextIdentifier("titleWorkArea"));
		translate.closeConnection();
	}

	private void setCustomView() 
	{
		DisplayMetrics metrics = getBaseContext().getResources().getDisplayMetrics();
		int w = metrics.widthPixels;
		int h = metrics.heightPixels;
        h = h + 1000;

		mView = new MyView(this, w, h);
		mView.setBackgroundColor(Color.TRANSPARENT);
		mView.setDrawingCacheEnabled(true);

		mPaint = new Paint();
		mPaint.setAntiAlias(true);
		mPaint.setDither(true);
		mPaint.setColor(Color.BLUE);
		mPaint.setStrokeWidth(MathFriendzyHelper.DRAW_LINE_THICKNESS);
		mPaint.setStyle(Paint.Style.STROKE);
        this.setViewHeightAndWidth(w , h , mView);
		layRough.addView(mView);		
	}//END setCustomView method

    /**
     * set the main view height and width
     *
     * @param w
     * @param h
     */
    private void setViewHeightAndWidth(int w, int h, View view) {
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(w, h);
        view.setLayoutParams(param);
    }

	////////////******************* Printing view *******************///////////////////
	@SuppressLint("NewApi")
	public class MyView extends View 
	{
		public MyView(Context c, int w, int h) {
			super(c);

			if (android.os.Build.VERSION.SDK_INT >= 11) 
			{
				setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			}

			mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
			mCanvas = new Canvas(mBitmap);
			mPath = new Path();
			mBitmapPaint = new Paint(Paint.DITHER_FLAG);
			mBitmapPaint.setXfermode(null);
		}


		@Override
		protected void onDraw(Canvas canvas) 
		{
			Path path = mPath;
			canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
			canvas.drawPath(path, mPaint);
		}

		// //////************touching events for painting**************///////
		private float mX, mY;
		private static final float TOUCH_TOLERANCE = 5;

		private void touch_start(float x, float y) {
            mPath.reset();
            mPath.moveTo(x, y);
            mX = x;
            mY = y;
		}

		private void touch_move(float x, float y) {
            float dx = Math.abs(x - mX);
            float dy = Math.abs(y - mY);
            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
                //for erasing the written
                if(isEraseMode){
                    mPath.lineTo(mX, mY);
                    mCanvas.drawPath(mPath, mPaint);
                    mPath.reset();
                    mPath.moveTo(mX, mY);
                }
                //end for erase change
                mX = x;
                mY = y;
                //addPoint(mX,mY);
            }
		}

		private void touch_up() {
            if(isEraseMode)
                mPath.reset();
            else{
                mPath.lineTo(mX, mY);
                // commit the path to our off screen
                mCanvas.drawPath(mPath, mPaint);
                // kill this so we don't double draw
                mPath.reset();
            }
		}


		@Override
		public boolean onTouchEvent(MotionEvent event)
		{
            getParent().requestDisallowInterceptTouchEvent(true);
			float x = event.getX();
			float y = event.getY();
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				touch_start(x, y);
				invalidate();
				break;
			case MotionEvent.ACTION_MOVE:
				touch_move(x, y);
				invalidate();
				break;
			case MotionEvent.ACTION_UP:
				touch_up();
				invalidate();
				break;
			}
			return true;
		} // end of touch events for image

	}// END MyView Class

	/**
	 * This method set listener on widgets
	 */
	private void setListenerOnWidgets() {
		clearPaint.setOnClickListener(this);
		drawPaint.setOnClickListener(this);
		btnScroll.setOnClickListener(this);

        scrollUp(btnScrollUp, scroll, this);
        scrollDown(btnScrollDown, scroll, this);
	}

    @Override
    public void onScrolling(int state) {
        switch (state) {
            case ON_SCROLLING:
                btnScrollDown.setBackgroundResource(R.drawable.down);
                btnScrollUp.setBackgroundResource(R.drawable.up);
                break;
            case SCROLL_DOWN_COMPLETE:
                btnScrollDown.setBackgroundResource(R.drawable.unselected_down);
                btnScrollUp.setBackgroundResource(R.drawable.up);
                break;
            case SCROLL_UP_COMPLETE:
                btnScrollDown.setBackgroundResource(R.drawable.down);
                btnScrollUp.setBackgroundResource(R.drawable.unselected_up);
                break;
        }
    }


	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.clearPaint:
            isEraseMode = true;
			/*clearPaint.setBackgroundResource(R.drawable.grey_eraser);
			drawPaint.setBackgroundResource(R.drawable.pencil);*/
            this.setEraserSelected(true);
            this.setPencilSelected(false);
			mPaint.setStrokeWidth(50);
			mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
			break;

		case R.id.draw:
            isEraseMode = false;
            this.setEraserSelected(false);
            this.setPencilSelected(true);
			/*clearPaint.setBackgroundResource(R.drawable.eraser);
			drawPaint.setBackgroundResource(R.drawable.grey_pencil);*/
			mPaint.setStrokeWidth(MathFriendzyHelper.DRAW_LINE_THICKNESS);
			mPaint.setXfermode(null);
			mPaint.setColor(Color.BLUE);
			break;

		case R.id.btnScroll:
			clearPaint.setBackgroundResource(R.drawable.eraser);
			drawPaint.setBackgroundResource(R.drawable.pencil);
			break;
		}
	}


	@Override
	public void onBackPressed() {
		isClickOnBack = true;
		super.onBackPressed();
	}

	@Override
	protected void onPause() {
		if(!isClickOnBack){
			if(SchoolCurriculumEquationSolveBase.playSound != null){
				SchoolCurriculumEquationSolveBase.playSound.stopPlayer();
			}
			SchoolCurriculumEquationSolveBase.isClickOnWorkArea = false;
			finish();
		}	
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}
