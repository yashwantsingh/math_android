package com.mathfriendzy.controller.learningcenter.schoolcurriculum;

import com.mathfriendzy.model.schoolcurriculum.learnigncenter.WordProblemQuestionTransferObj;

/**
 * This interface is used for showing question after downloading
 * @author Yashwant Singh
 *
 */
public interface ShowTranslateQuestionAfterDownloading {
	
	/**
	 * This method show the quesiton after downloading translation
	 * @param questionObj - question object (translated)
	 * @param isAssessment - for assessment or not
	 */
	public void showQuestionAfterTranslationDownload(WordProblemQuestionTransferObj questionObj , boolean isAssessment);
}
