package com.mathfriendzy.controller.learningcenter.schoolcurriculum;

import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LEARNIGN_CENTER_SCHOOL_CURRICULUM_SOLVE_EQUATION_FLAG;
import static com.mathfriendzy.utils.ICommonUtils.LEARNING_CENTER_EQUATION_SOLVE;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Random;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.learningcenter.CongratulationActivity;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.helper.YesNoListenerInterface;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.homework.SaveHomeWorkServerResponse;
import com.mathfriendzy.model.homework.SaveHomeWorkWordPlayedDataParam;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.MathResultTransferObj;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.LearnignCenterSchoolCurriculumServerOperation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.MathEquationTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.MathScoreForSchoolCurriculumTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SchoolCurriculumLearnignCenterimpl;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.WordProblemLevelTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.WordProblemQuestionTransferObj;
import com.mathfriendzy.model.spanishchanges.SpanishChangesImpl;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerDialogs;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ICommonUtils;
import com.mathfriendzy.utils.PlaySound;

/**
 * This class is for solving school curriculum equations
 * @author Yashwant Singh
 *
 */
@SuppressWarnings("serial")
public class LearningCenterSchoolCurriculumEquationSolve extends SchoolCurriculumEquationSolveBase {

    private ImageView imgCross1 = null;
    private ImageView imgCross2 = null;
    private ImageView imgCross3 = null;

    private final String TAG = this.getClass().getSimpleName();

    private int categoryId     = 0;
    private int subCategoryId  = 0;
    private ArrayList<WordProblemQuestionTransferObj> questionAnswerList = null;//set from database
    private int index = 0 ;

    private int noOfAttempts = 0;

    private final int SLEEP_TIME = 1000;
    private final int SLEEP_TIME_FOR_WRONG_ANS_THIRD_ATTEMP = 500;

    private final int MAX_NUMBER_OF_ATTEMPTS = 3;
    private final int MAX_NUMBER_QUESTIONS_TO_SHOW = 10;

    //for sound
    //PlaySound playSound = null;

	/* //for animation
	 private Animation animation 	= null;	 */

    private int rightAnswerCounter = 0;
    private int stars              = 0;
    private int numberOfCoins      = 0;
    private int isCorrectAnsByUser = 0;
    private String answerByUser 	= "";

    //added for add sub category name
    private String subCategoryName = "";
    private TextView txtSubCatName = null;

    //for home work
    private String warningMessageOnBack = null;
    private String yesTextMsg = null;
    private String noTextMsg  = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learning_center_school_curriculum_equation_solve);

        if(LEARNIGN_CENTER_SCHOOL_CURRICULUM_SOLVE_EQUATION_FLAG)
            Log.e(TAG, "inside onCreate()");

		/*dialog = new Dialog(this, R.style.CustomDialogThemeForSchoolCurriculumDialog);
		dialog.setContentView(R.layout.school_curriculum_font_screen);*/

        //for tts
        tts = new TextToSpeech(this, this);

        playSound = new PlaySound(this);
        //playSound.playSoundForSchoolSurriculumEquationSolve(this);

        random = new Random();
        questionAnswerList = new ArrayList<WordProblemQuestionTransferObj>();
        playDataList = new ArrayList<MathEquationTransferObj>();

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        isTab = getResources().getBoolean(R.bool.isTablet);

        this.getIntentValue();//set the local variable categoryId , subCategoryId from intent
        this.setWidgetsReferences();
        this.setWidgetsReferencesRemainingWidgetsFromBase();

        //added for sub cat name
        txtSubCatName.setText(subCategoryName);
        //end changes

        this.setTextFromTranslation();
        this.setTranslationTextForCurrentActivity();
        this.setpPlayerDetail();
        //categoryId , subCategoryId Variable set in getIntentValue() method
        //and set the local quetionList from database
        this.getQuestionFromDatabase(categoryId , subCategoryId);

        txtTimer.setTextColor(Color.BLACK);

        //get learn center time
        startTimeForTimerAtFirstTime = this.getLearnCenterTime(categoryId, subCategoryId);

        this.showQuetion();//this method show question from database according to index value
        //which randomly generated
        this.setListenerOnWidgets();

        //for show timer
        this.showTimer();

        if(LEARNIGN_CENTER_SCHOOL_CURRICULUM_SOLVE_EQUATION_FLAG)
            Log.e(TAG, "outside onCreate()");
    }



    /**
     * Added for home work
     */
    private void setTranslationTextForCurrentActivity(){
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        warningMessageOnBack = transeletion.getTranselationTextByTextIdentifier("lblYouMustCompleteAllProblems");
        yesTextMsg           = transeletion.getTranselationTextByTextIdentifier("btnTitleYes");
        noTextMsg            = transeletion.getTranselationTextByTextIdentifier("lblNo");
        transeletion.closeConnection();
    }

    /**
     * This method show timer or not
     */
    private void showTimer(){
        //for show timer or not
        if(!isFromHomeWork){
            if(CommonUtils.isShowTimer){
                txtTimer.setVisibility(TextView.VISIBLE);
            }else{
                txtTimer.setVisibility(TextView.GONE);
            }
        }else{
            txtTimer.setVisibility(TextView.GONE);
        }
    }

    /**
     * This method get LearnCenterTime
     * @param catId
     * @param subCatId
     * @return
     */
    private long getLearnCenterTime(int catId , int subCatId){
        long time = 0;
        SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
        schoolImpl.openConnection();
        time = schoolImpl.getLearnCenterTime(catId, subCatId);
        schoolImpl.closeConnection();
        return time;
    }


    /**
     * This method set player detail
     */
    @SuppressWarnings("deprecation")
    private void setpPlayerDetail() {
        SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
        String fullName   = sharedPreffPlayerInfo.getString("playerName", "");
        String playerName = fullName.substring(0,(fullName.indexOf(" ") + 2));
        txtPlayerName.setText(playerName + ".");

        Country country = new Country();
        String coutryIso = country.getCountryIsoByCountryName(sharedPreffPlayerInfo.getString("countryName", ""), this);

        try{
            if(!(coutryIso.equals("-")))
                imgFlag.setBackgroundDrawable(Drawable.createFromStream(getAssets().open(getResources().
                        getString(R.string.countryImageFolder) +"/"
                        + coutryIso + ".png"), null));
        }
        catch (IOException e){
            Log.e(TAG, "Inside set player detail Error No Image Found");
        }

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtPoints.setText(transeletion.getTranselationTextByTextIdentifier("mfLblPts") + ": " + points);
        transeletion.closeConnection();
    }


    /**
     * This method get value from intent and set it to the local variable
     * categoryId , and subCategoryId
     */
    private void getIntentValue() {
        //for home work
        isFromHomeWork = this.getIntent().getBooleanExtra("isFromHomeWork", false);
        categoryId    = this.getIntent().getIntExtra("categoryId", 0);
        subCategoryId = this.getIntent().getIntExtra("subCategoryId", 0);
        //Log.e(TAG, "category Id " + categoryId + " sub category id " + subCategoryId);
        //changes for Spanish
        selectedPlayGrade = this.getIntent().getIntExtra("playerSelectedGrade", 1);
        //Log.e(TAG, "selectedPlayGrade inside intent" + selectedPlayGrade);
        subCategoryName   = this.getIntent().getStringExtra("subCatName");
        //Log.e(TAG, "subCatName " + subCategoryName);
    }

    /**
     * This method set widgets references from xml layout
     */
    private void setWidgetsReferencesRemainingWidgetsFromBase() {

        if(LEARNIGN_CENTER_SCHOOL_CURRICULUM_SOLVE_EQUATION_FLAG)
            Log.e(TAG, "inside setWidgetsReferences()");

        imgCross1 = (ImageView) findViewById(R.id.imgCross1);
        imgCross2 = (ImageView) findViewById(R.id.imgCross2);
        imgCross3 = (ImageView) findViewById(R.id.imgCross3);

        txtSubCatName = (TextView) findViewById(R.id.txtSubCatName);

        if(LEARNIGN_CENTER_SCHOOL_CURRICULUM_SOLVE_EQUATION_FLAG)
            Log.e(TAG, "outside setWidgetsReferences()");
    }

    /**
     * This method get questions and answer from database according to the category and sub category id
     * and set to the local array List ArrayList<WordProblemQuestionTransferObj> questionAnswerList
     */
    private void getQuestionFromDatabase(int categoryId , int subCategoryId) {
        //changes for Spanish
        if(CommonUtils.getUserLanguageCode(this) == CommonUtils.ENGLISH){
            SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
            schoolImpl.openConnection();
            questionAnswerList = schoolImpl.getQuestionAnswer(categoryId, subCategoryId);
            schoolImpl.closeConnection();
        }else{
            SpanishChangesImpl schoolImpl = new SpanishChangesImpl(this);
            schoolImpl.openConn();
            questionAnswerList = schoolImpl.getQuestionAnswer(categoryId, subCategoryId);
            schoolImpl.closeConn();
        }
    }

    /**
     * This method show question according to the index
     * index is a local variable which is increased after each question to show
     * initially set to 0
     */
    private void showQuetion(){

		/*startTime = new Date();*/

        numberOfQuestion ++ ;

        if(numberOfQuestion > 1 && numberOfQuestion <= MAX_NUMBER_QUESTIONS_TO_SHOW){
            animation = AnimationUtils.loadAnimation(this, R.anim.splash_animation_right);
            questionAnswerLayout.setAnimation(animation);
        }

        imgCross1.setBackgroundResource(R.drawable.ml_gray_x);
        imgCross2.setBackgroundResource(R.drawable.ml_gray_x);
        imgCross3.setBackgroundResource(R.drawable.ml_gray_x);

        //Log.e(TAG, "list size " + questionAnswerList.size());

        try{

            if(numberOfQuestion <= MAX_NUMBER_QUESTIONS_TO_SHOW){
                index = random.nextInt(questionAnswerList.size());
                questionObj = questionAnswerList.get(index);
                questionAnswerList.remove(index);

                this.setKeyBoardVisivility(questionObj.getFillIn());

                //changes for Spanish
                if(languageCode == SPANISH){
                    SpanishChangesImpl schoolImpl = new SpanishChangesImpl(this);
                    schoolImpl.openConn();
                    //get question from database into questionObj object
                    questionObj  = schoolImpl.getQuestionByQuestionId(questionObj.getQuestionId());
                    schoolImpl.closeConn();
                }else if(languageCode == ENGLISH){
                    SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
                    schoolImpl.openConnection();
                    //get question from database into questionObj object
                    questionObj  = schoolImpl.getQuestionByQuestionId(questionObj.getQuestionId());
                    //questionObj  = schoolImpl.getQuestionByQuestionId(1319);
                    schoolImpl.closeConnection();
                }

                this.setIsSpanishBooleanValue(); // changes after
                //end changes

                this.setLayoutForShowQuetionAndAnswer(questionObj);
            }
            else{

                //set it on CongratulationActivity for Alex Sheet.
                //playSound.playSoundForSchoolSurriculumForResultScreen(this);

                if(playSound != null)
                    playSound.stopPlayer();

                this.callAfterCompletion();
            }
        }catch(Exception e){
            Log.e(TAG, "Eroor while showing " + e.toString());
        }
    }

    //changes for update player points in local when Internet is not connected
    //updated on 26 11 2014
    private void savePlayerDataWhenNoInternetConnected(){
        try{
            SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
            String userId;
            String playerId;
            if(sharedPreffPlayerInfo.getString("userId", "").equals("0"))
                userId = "0";
            else
                userId = sharedPreffPlayerInfo.getString("userId", "");

            if(sharedPreffPlayerInfo.getString("playerId", "").equals("0"))
                playerId = "0";
            else
                playerId = sharedPreffPlayerInfo.getString("playerId", "");

            MathResultTransferObj mathResultObj = new MathResultTransferObj();
            mathResultObj.setUserId(userId);
            mathResultObj.setPlayerId(playerId);
            mathResultObj.setTotalScore(points);
            mathResultObj.setCoins((int) (points * ICommonUtils.COINS_PER_POINT));
            mathResultObj.setLevel(MathFriendzyHelper.getPlayerCompleteLavel
                    (mathResultObj.getPlayerId(), this));
            MathFriendzyHelper.insertPlayerPlayedDataIntoLocalEarnedScore(mathResultObj, this);
        }catch(Exception e){
            e.printStackTrace();
        }

    }//end updation on 26 11 2014

    /**
     * This method call when user play all the equation
     *
     */
    private void callAfterCompletion(){
        //updated on 26 11 2014
        this.savePlayerDataWhenNoInternetConnected();
        //end updation on 26 11 2014

        if(!isFromHomeWork){
            if(rightAnswerCounter >= 5 && rightAnswerCounter < 7)
                stars = 1;
            else if(rightAnswerCounter >= 7 && rightAnswerCounter < 9)
                stars = 2;
            else if(rightAnswerCounter >= 9)
                stars = 3;

            this.insertIntoPlayerTotalPoints();

            if(stars > 0)
                this.saveStar(stars);

            SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
            String userId;
            String playerId;

            if(sharedPreffPlayerInfo.getString("userId", "").equals("0"))
                userId = "0";
            else
                userId = sharedPreffPlayerInfo.getString("userId", "");

            if(sharedPreffPlayerInfo.getString("playerId", "").equals("0"))
                playerId = "0";
            else
                playerId = sharedPreffPlayerInfo.getString("playerId", "");

            MathScoreForSchoolCurriculumTransferObj mathScoreObj = new MathScoreForSchoolCurriculumTransferObj();
            mathScoreObj.setUserId(userId);
            mathScoreObj.setPlayerId(playerId);
            mathScoreObj.setCatId(categoryId);
            mathScoreObj.setSubCatId(subCategoryId);
            mathScoreObj.setStars(stars);
            mathScoreObj.setPoints(points);// this point updata in asyncktak at time of updation on server
            mathScoreObj.setCoins(numberOfCoins);
            mathScoreObj.setProblems(this.getEquationSolveXml(playDataList));

            SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
            if(sheredPreference.getBoolean(IS_LOGIN, false))
            {
                if(CommonUtils.isInternetConnectionAvailable(this))
                {
                    new AddMathWordProblemPlayLevelScore(mathScoreObj).execute(null,null,null);
                }else{

                    mathScoreObj.setProblems(this.getEquationSolveXmlForInterNetNotConnected(playDataList));
                    SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
                    schoolImpl.openConnection();
                    schoolImpl.insertIntoWordProblemResults(mathScoreObj);
                    if(stars > 0){
                        schoolImpl.insertIntoLocalWordProblemLevels(userId, playerId,
                                categoryId, subCategoryId, stars);
                    }
                    schoolImpl.closeConnection();
                }
            }

            Intent intent = new Intent(LearningCenterSchoolCurriculumEquationSolve.this,
                    CongratulationActivity.class);
            intent.putExtra("points", points);
            intent.putExtra("stars", stars);
            intent.putExtra("isFromLearnignCenterSchoolCurriculum", true);
            intent.putExtra("playDatalist", playDataList);
            intent.putExtra("selectedGrade", selectedPlayGrade);
            startActivity(intent);
        }else{
            this.updateDataForHome();
        }
    }


    /**
     * This method update the data for home work on server
     */
    private void updateDataForHome(){

        SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
        final String userId;
        final String playerId;
        if(sharedPreffPlayerInfo.getString("userId", "").equals("0"))
            userId = "0";
        else
            userId = sharedPreffPlayerInfo.getString("userId", "");

        if(sharedPreffPlayerInfo.getString("playerId", "").equals("0"))
            playerId = "0";
        else
            playerId = sharedPreffPlayerInfo.getString("playerId", "");

        final int score = (rightAnswerCounter * 100) / MAX_NUMBER_QUESTIONS_TO_SHOW;

        final SaveHomeWorkWordPlayedDataParam param = new SaveHomeWorkWordPlayedDataParam();
        param.setAction("addHomeworkMathWordProblemPlay");
        param.setUserId(userId);
        param.setPlayerId(playerId);
        param.setProblems("<equations>" + this.getEquationSolveXml(playDataList) + "</equations>");
        param.setCatId(categoryId);
        param.setSubCatId(subCategoryId);
        param.setScore(score);
        param.setHWId(this.getIntent().getStringExtra("HWId"));
        param.setPints(points);
        param.setCoins((int) (points * ICommonUtils.COINS_PER_POINT));
        param.setTime(this.getTotalTime() + "");

        HttpResponseInterface mInterface = new HttpResponseInterface() {
            @Override
            public void serverResponse(HttpResponseBase httpResponseBase,
                                       int requestCode) {
                if(requestCode == ServerOperationUtil.SAVE_HOME_WORK_WORD_PLAY_REQUEST){
                    SaveHomeWorkServerResponse response =
                            (SaveHomeWorkServerResponse) httpResponseBase;
                    if(response.getResult().equals("success")){
                        response.setCurrentPlaySubCatScore(score + "");
                        Intent intent = new Intent();
                        intent.putExtra("resultAfterPlayed", response);
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                    }
                }
            }
        };

        if(CommonUtils.isInternetConnectionAvailable(this)){
            new MyAsyckTask(ServerOperation.createPostRequestForSaveHomeWorkForWordProblem(param)
                    , null, ServerOperationUtil.SAVE_HOME_WORK_WORD_PLAY_REQUEST, this,
                    mInterface, ServerOperationUtil.SIMPLE_DIALOG , true ,
                    getString(R.string.please_wait_dialog_msg))
                    .execute();
        }else{

            new AsyncTask<Void, Void, SaveHomeWorkServerResponse>(){
                private ProgressDialog pd = null;

                @Override
                protected void onPreExecute() {
                    pd = ServerDialogs.getProgressDialog
                            (LearningCenterSchoolCurriculumEquationSolve.this,
                                    getString(R.string.please_wait_dialog_msg), 1);
                    pd.show();
                }

                @Override
                protected SaveHomeWorkServerResponse doInBackground(
                        Void... params) {
                    return getSaveHomeWorkServerResponse(userId , playerId , score , param);
                }

                @Override
                protected void onPostExecute(SaveHomeWorkServerResponse response) {
                    if(pd != null && pd.isShowing()){
                        pd.cancel();
                    }

                    if(response != null){
                        Intent intent = new Intent();
                        intent.putExtra("resultAfterPlayed", response);
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                    }
                }
            }.execute();
        }
    }

    /**
     * Get SavedHomework response
     * @param userId
     * @param playerId
     * @param score
     * @param param
     * @return
     */
    private SaveHomeWorkServerResponse getSaveHomeWorkServerResponse(String userId ,
                                                                     String playerId , int score, SaveHomeWorkWordPlayedDataParam param){


        //end saving local equation
        MathFriendzyHelper.saveWordForOfflineHW(this, param);
        SaveHomeWorkServerResponse response = MathFriendzyHelper
                .getUpdatedResultForWordProblemForOfflineHomework
                        (categoryId + "", subCategoryId, score + "",
                                this.getTotalTime());
        return response;
    }


    /**
     * This method calculate the total time to play
     * @return
     */
    private int getTotalTime(){
        int totalTime = 0;
        for(int i = 0 ; i < playDataList.size() ; i ++ ){
            totalTime = totalTime + playDataList.get(i).getTimeTakenToAnswer();
        }
        return totalTime;
    }

    /**
     * This method insert the data into total player points table
     * and also save data on server
     * @param
     */
    private void insertIntoPlayerTotalPoints()
    {
        SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
        String userId;
        String playerId;

        if(sharedPreffPlayerInfo.getString("userId", "").equals("0"))
            userId = "0";
        else
            userId = sharedPreffPlayerInfo.getString("userId", "");

        if(sharedPreffPlayerInfo.getString("playerId", "").equals("0"))
            playerId = "0";
        else
            playerId = sharedPreffPlayerInfo.getString("playerId", "");

        PlayerTotalPointsObj playerPoints = new PlayerTotalPointsObj();
        playerPoints.setUserId(userId);
        playerPoints.setPlayerId(playerId);
        playerPoints.setTotalPoints(points);

        numberOfCoins = (int) (points * ICommonUtils.COINS_PER_POINT);

        playerPoints.setCoins(numberOfCoins);
        playerPoints.setCompleteLevel(1);
        playerPoints.setPurchaseCoins(0);

        LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
        learningCenterObj.openConn();

        if(learningCenterObj.isPlayerTotalPointsExist(playerPoints.getPlayerId()))
        {
            int points = playerPoints.getTotalPoints();
            int coins  = playerPoints.getCoins();
            playerPoints.setTotalPoints(learningCenterObj.getDataFromPlayerTotalPoints
                    (playerPoints.getPlayerId()).getTotalPoints() + points);

            playerPoints.setCoins(learningCenterObj.getDataFromPlayerTotalPoints(playerPoints.getPlayerId()).getCoins()
                    + coins);

            learningCenterObj.deleteFromPlayerTotalPoints(playerPoints.getPlayerId());
        }

        learningCenterObj.insertIntoPlayerTotalPoints(playerPoints);

        learningCenterObj.closeConn();
    }

    /**
     * This method save stars into the WordProblemLevelStar
     */
    private void saveStar(int stars){

        SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
        String userId;
        String playerId;

        if(sharedPreffPlayerInfo.getString("userId", "").equals("0"))
            userId = "0";
        else
            userId = sharedPreffPlayerInfo.getString("userId", "");

        if(sharedPreffPlayerInfo.getString("playerId", "").equals("0"))
            playerId = "0";
        else
            playerId = sharedPreffPlayerInfo.getString("playerId", "");

        WordProblemLevelTransferObj wordProblemLevelData = new WordProblemLevelTransferObj();
        wordProblemLevelData.setUserId(userId);
        wordProblemLevelData.setPlayerId(playerId);
        wordProblemLevelData.setCategoryId(categoryId);
        wordProblemLevelData.setSubCategoryId(subCategoryId);
        wordProblemLevelData.setStars(stars);

        SchoolCurriculumLearnignCenterimpl schoolLearningCenter =
                new SchoolCurriculumLearnignCenterimpl(this);
        schoolLearningCenter.openConnection();

        if(schoolLearningCenter.isWordProblemLevelStarExist(userId, playerId, categoryId, subCategoryId)){
            schoolLearningCenter.updateWordProblemLevelStar(wordProblemLevelData);
        }
        else{
            schoolLearningCenter.insertIntoWordProblemLevelStar(wordProblemLevelData);
        }
        schoolLearningCenter.closeConnection();
    }

    /**
     * This method inflate the layout for show question and answer
     * @param questionObj
     */
    private void setLayoutForShowQuetionAndAnswer(WordProblemQuestionTransferObj questionObj){

        ArrayList<String> imageNameList        = this.getImageArrayList(questionObj);
        ArrayList<String> unAvailableImageList = this.checkImageAvalability(imageNameList);

        if(unAvailableImageList.size() > 0){
            if(CommonUtils.isInternetConnectionAvailable(this)){
                //changes for timer when download images from server
                if(isCountDawnTimer){
                    if(timer != null){
                        timer.cancel();
                        timer = new MyTimer(startTimerForRoughArea, 1 * 1000);//second argument is the interval
                    }
                }else{
                    if(customHandler != null)
                        customHandler.removeCallbacks(updateTimerThread);
                }
                //end changes

                new DawnloadImageForSchooCurriculum(unAvailableImageList, this, questionObj)
                        .execute(null,null,null);
            }else{
                //this.showQuetion();
                this.setQuestionAnswerLayout(questionObj);
            }
        }
        else{
            this.setQuestionAnswerLayout(questionObj);
        }
    }


    @Override
    public void onClick(View v) {

        switch(v.getId()){
            case R.id.btnGoForWithoutFillIn:
                endTime = new Date();
                isClickOnGo = true;
                setClickableGoButton(false);
                this.clickOnbtnGoForWithoutFillIn();
                break;
            case R.id.btnGoForFillIn:
                endTime = new Date();
                isClickOnGo = true;
                setClickableGoButton(false);
                this.clickOnbtnGoForFillIn();
                break;
            case R.id.btn1:
                this.clickOnButton("1");
                break;
            case R.id.btn2:
                this.clickOnButton("2");
                break;
            case R.id.btn3:
                this.clickOnButton("3");
                break;
            case R.id.btn4:
                this.clickOnButton("4");
                break;
            case R.id.btn5:
                this.clickOnButton("5");
                break;
            case R.id.btn6:
                this.clickOnButton("6");
                break;
            case R.id.btn7:
                this.clickOnButton("7");
                break;
            case R.id.btn8:
                this.clickOnButton("8");
                break;
            case R.id.btn9:
                this.clickOnButton("9");
                break;
            case R.id.btn0:
                this.clickOnButton("0");
                break;
            case R.id.btnSpace:
                this.clickOnButton(" ");
                break;
            case R.id.btnSynchronized :
                this.clearAnsBox();
                break;
            case R.id.btnsecondryKeyBoard :
                primaryKeyboard.setVisibility(LinearLayout.GONE);
                secondryKeyboard.setVisibility(LinearLayout.VISIBLE);
                btnSecandryDelete.setVisibility(Button.VISIBLE);
                break;
            case R.id.btnNumber :
                primaryKeyboard.setVisibility(LinearLayout.VISIBLE);
                secondryKeyboard.setVisibility(LinearLayout.GONE);
                btnSecandryDelete.setVisibility(Button.GONE);
                break;
            case R.id.btnDot:
                this.clickOnButton(".");
                break;
            case R.id.btnMinus:
                this.clickOnButton("-");
                break;
            case R.id.btnComma:
                this.clickOnButton(",");
                break;
            case R.id.btnPlus:
                this.clickOnButton("+");
                break;
            case R.id.btnDevide:
                this.clickOnButton("/");
                break;
            case R.id.btnMultiply:
                this.clickOnButton("*");
                break;
            case R.id.btnCollon:
                this.clickOnButton(":");
                break;
            case R.id.btnLessThan:
                this.clickOnButton("<");
                break;
            case R.id.btnGreaterThan:
                this.clickOnButton(">");
                break;
            case R.id.btnEqual:
                this.clickOnButton("=");
                break;
            case R.id.btnRoot:
                this.clickOnButton("√");
                break;
            case R.id.btnDevider:
                this.clickOnButton("|");
                break;
            case R.id.btnOpeningBracket:
                this.clickOnButton("(");
                break;
            case R.id.btnClosingBracket:
                this.clickOnButton(")");
                break;
            case R.id.btnRoughWork:
                this.clickOnWorkArea(true);
                break;
            case R.id.btnRoughWorkWithourFillIn:
                this.clickOnWorkArea(true);
                break;
            case R.id.imgTimerImage:
                timer.cancel();
                timer = new MyTimer(startTimerForRoughArea, 1 * 1000);//second argument is the interval
                DialogGenerator dg = new DialogGenerator(this);
                Translation transeletion = new Translation(this);
                transeletion.openConnection();
                String text = transeletion.getTranselationTextByTextIdentifier("lblTheCountDownRepresents");
                transeletion.closeConnection();
                dg.generateDialogForSchoolCurriculumTimer(text, timer);
                break;
            case R.id.btnEmailSendForWrongQuestion1:
                this.sendEmailFroWrongQuestion(questionObj.getQuestionId() , this , this);
                break;
            case R.id.btnEmailSendForWrongQuestion2:
                this.sendEmailFroWrongQuestion(questionObj.getQuestionId() , this , this);
                break;
            case R.id.btnSecandryDelete:
                this.clearAnsBox();
                break;
        }
    }

	/*@Override
	public void cancelCurrentTimerAndSetTheResumeTime() {

		//changes for timer when dawanload images from server
		if(isCountDawnTimer){
			if(timer != null){
				timer.cancel();
				timer = new MyTimer(startTimerForRoughArea, 1 * 1000);//second argument is the interval
			}
		}else{

			if(customHandler != null){
				resumeTimeForTimer = new Date();
				customHandler.removeCallbacks(updateTimerThread);
			}
		}
		//end changes
	}

	@Override
	public void resumeTimerFromTimeSetAtCancelTime() {

		if(isCountDawnTimer){
			if(timer != null)
				timer.start();
		}else{
			if(customHandler != null){
				Date dawnloadEndTime = new Date();
				startTimeForTimer = startTimeForTimer + (dawnloadEndTime.getTime() - resumeTimeForTimer.getTime());
				customHandler.postDelayed(updateTimerThread, 0);
			}
		}
	}*/

    /**
     * This method call when a user click on number button
     * @param text
     */
    private void clickOnButton(String text){
        //answerValue = answerValue + text;
        answerValue.append(text);
        this.setAnswertext(answerValue.toString());
    }

    /**
     * This method set the string to ans box
     * @param ansStringValue
     */
    private void setAnswertext(String ansStringValue){
        if(isAnswerConatin == 0)
            ansBox.setText(answerValue);
        else if(isAnswerConatin == 1){
            ansBox.setText(ansPrefixValue + " " +  answerValue);
        }
        else if(isAnswerConatin == 2){
            ansBox.setText(answerValue + " " + ansPostFixValue);
        }
        else if(isAnswerConatin == 3){
            ansBox.setText(ansPrefixValue + " " + answerValue + " " + ansPostFixValue);
        }
    }

    /**
     * This method clear the ans box
     */
    private void clearAnsBox() {
        if(answerValue.length() > 0){
            answerValue.deleteCharAt(answerValue.length() - 1);
            this.setAnswertext(answerValue.toString());
        }
    }

    /**
     * This method call when user btnGoForFillIn
     */
    private void clickOnbtnGoForFillIn() {

        if(!questionObj.getOptionList().get(0).contains(" ") && answerValue.toString().contains(" ")){
            answerValue = new StringBuilder(answerValue.toString().replaceAll(" ", ""));
        }

        answerByUser = answerValue.toString();

        String userAns = answerValue.toString().replaceAll(" ", "").replace(",", "");
        String dbAns   = questionObj.getOptionList().get(0).replaceAll(" ", "").replace(",", "");

		/*boolean isFloatValue = true;
		if(questionObj.getOptionList().get(0).contains(".")){
			try{
				Float.parseFloat(dbAns);
				isFloatValue = false;
			}catch(Exception e){
				isFloatValue = true;
			}
		}*/

        //if(!questionObj.getOptionList().get(0).contains(".") && isBooleanValue){
        if(this.getIsFloatValue(questionObj.getOptionList().get(0))){
            if(userAns.equals(dbAns)){//changes for space and ask to deepak about this
                isCorrectAnsByUser = 1;
                noOfAttempts = 0;
                answerValue = new StringBuilder("");
                ansBox.setBackgroundResource(R.drawable.right_bar_ipad);
                setProgress(true);
                this.setPoints();
                this.setHandlerForRightAnswer();
            }
            else{
                //dialog.show();
                isCorrectAnsByUser = 0;
                imgForGroundImage.setVisibility(ImageView.VISIBLE);
                playSound.playSoundForSchoolSurriculumEquationSolveForWrongAnswer(this);
                answerValue = new StringBuilder("");
                noOfAttempts ++ ;
                ansBox.setBackgroundResource(R.drawable.incorrect_bar_ipad);
                this.setWrongAttemptsImage(noOfAttempts);
                this.setHandlerForWrongAnswerForFillIn();
            }
        }else{//changes for float ans
            if(this.compareFloatAns(answerByUser, questionObj.getOptionList().get(0))){
                isCorrectAnsByUser = 1;
                noOfAttempts = 0;
                answerValue = new StringBuilder("");
                ansBox.setBackgroundResource(R.drawable.right_bar_ipad);
                setProgress(true);
                this.setPoints();
                this.setHandlerForRightAnswer();
            }else{
                isCorrectAnsByUser = 0;
                imgForGroundImage.setVisibility(ImageView.VISIBLE);
                playSound.playSoundForSchoolSurriculumEquationSolveForWrongAnswer(this);
                answerValue = new StringBuilder("");
                noOfAttempts ++ ;
                ansBox.setBackgroundResource(R.drawable.incorrect_bar_ipad);
                this.setWrongAttemptsImage(noOfAttempts);
                this.setHandlerForWrongAnswerForFillIn();
            }
        }
    }


    /**
     * This method call when click on btnGoForWithoutFillIn button
     */
    private void clickOnbtnGoForWithoutFillIn(){

        ArrayList<Integer> selectedIntegerOptionList = new ArrayList<Integer>();

        String userAnswer = "";
        for( int i = 0 ; i < selectedOptionList.size() ; i ++ ){
            for(int j = 0 ; j < questionObj.getOptionList().size() ; j ++ ){
                if(selectedOptionList.get(i).getSelectedOptionTextValue()
                        .equals(questionObj.getOptionList().get(j)))
                {
                    selectedIntegerOptionList.add(j + 1);
                    //userAnswer = userAnswer + (j + 1) + ",";
                }
            }
        }

        Collections.sort(selectedIntegerOptionList);
        for(int i = 0 ; i < selectedIntegerOptionList.size() ; i ++ ){
            userAnswer = userAnswer + selectedIntegerOptionList.get(i) + ",";
        }

        if(userAnswer.length() > 0){
            answerByUser = userAnswer.substring(0, userAnswer.length() - 1);
        }

        if(userAnswer.equals(questionObj.getAns().replaceAll(" ", "") + ",")){
            isCorrectAnsByUser = 1;
            noOfAttempts = 0;
            for(int i = 0 ; i < selectedOptionList.size() ; i ++ ){
                if(isTab){
                    selectedOptionList.get(i).getOpationSeletedLayout()
                            .setBackgroundResource(R.drawable.right_ipad);
                }
                else{
                    selectedOptionList.get(i).getOpationSeletedLayout()
                            .setBackgroundResource(R.drawable.right);
                }
            }
            this.setProgress(true);
            this.setPoints();
            this.setHandlerForRightAnswer();
        }
        else{
            //dialog.show();
            isCorrectAnsByUser = 0;
            imgForGroundImage.setVisibility(ImageView.VISIBLE);
            playSound.playSoundForSchoolSurriculumEquationSolveForWrongAnswer(this);
            noOfAttempts ++ ;
            this.setWrongAttemptsImage(noOfAttempts);
            this.setHandlerForWrongAnswerForWithoutFillIn();
        }
    }

    /**
     * This method set points
     */
    private void setPoints(){
        points = points + this.getPointsForSolvingEachEquation();
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtPoints.setText(transeletion.getTranselationTextByTextIdentifier("mfLblPts") + ": " + points);
        transeletion.closeConnection();
    }



    /**
     * This will set the back ground image for wrong attempts
     * @param noOfAttempts
     */
    private void setWrongAttemptsImage(int noOfAttempts)
    {
        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "inside setWrongAttemptsImage()");

        switch(noOfAttempts)
        {
            case 1:
                imgCross3.setBackgroundResource(R.drawable.ml_red_x);
                break;
            case 2:
                imgCross2.setBackgroundResource(R.drawable.ml_red_x);
                break;
            case 3:
                imgCross1.setBackgroundResource(R.drawable.ml_red_x);
                break;
        }

        if(LEARNING_CENTER_EQUATION_SOLVE)
            Log.e(TAG, "outside setWrongAttemptsImage()");
    }

    /**
     * This method wait for 1 second after giving right answer and show the next question
     */
    private void setHandlerForRightAnswer()
    {
        rightAnswerCounter ++ ;
        //this.setplayDataToArrayList();
        this.pauseTimer();
        disableKeyboard();
        playSound.playSoundForSchoolSurriculumEquationSolveForRightAnswer(this);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                resumeTimer();
                enableKeyboard();
                //changes for time on 11 Mar
                setplayDataToArrayList();
                //end changes

                playSound.playSoundForSchoolSurriculumPagePeel
                        (LearningCenterSchoolCurriculumEquationSolve.this);
                setClickableGoButton(true);
                showQuetion();
            }
        }, SLEEP_TIME);
    }

    /**
     * This method call when user give the wrong answer
     */
    private void setHandlerForWrongAnswerForWithoutFillIn(){

        this.pauseTimer();
        disableKeyboard();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                //dialog.dismiss();
                resumeTimer();
                enableKeyboard();
                imgForGroundImage.setVisibility(ImageView.INVISIBLE);
                selectedOptionList = new ArrayList<OptionSelectDataObj>();// for refreshing the old object
                //from list
                for(int i = 0 ; i < optionLayoutList.size() ; i ++ ){
                    optionNumberList.get(i).setText(alphabet.charAt(i) + "");//set the option text like
                    //A,B,c etc
                    if(isTab)
                        optionLayoutList.get(i).setBackgroundResource(getResources().getIdentifier
                                ("option" + (i+1) + "_ipad", "drawable",getPackageName()));
                    else
                        optionLayoutList.get(i).setBackgroundResource(getResources().getIdentifier
                                ("option" + (i + 1) + "", "drawable",getPackageName()));
                }

                if(noOfAttempts == MAX_NUMBER_OF_ATTEMPTS){

                    setProgress(false);
                    //setplayDataToArrayList();

                    ArrayList<Integer> listWithoutComma = getCommaSepratedValueFromString(questionObj.getAns());
                    for(int i = 0 ; i < listWithoutComma.size() ; i ++ ){
                        for(int j = 0 ; j < selectedOptionValue.size() ; j ++ ){
                            if(questionObj.getOptionList().get(listWithoutComma.get(i) - 1)//index in array
                                    // start from 0 to 7
                                    .equals(selectedOptionValue.get(j))){
                                optionNumberList.get(j).setText("");
                                if(isTab)
                                    optionLayoutList.get(j).setBackgroundResource(R.drawable.right_ipad);
                                else
                                    optionLayoutList.get(j).setBackgroundResource(R.drawable.right);
                            }
                        }
                    }

                    pauseTimer();
                    disableKeyboard();
                    Handler handler1 = new Handler();
                    handler1.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            resumeTimer();
                            enableKeyboard();
                            //changes for timer on 11 Mar
                            setplayDataToArrayList();
                            //end changes

                            noOfAttempts = 0;
                            playSound.playSoundForSchoolSurriculumPagePeel
                                    (LearningCenterSchoolCurriculumEquationSolve.this);
                            setClickableGoButton(true);
                            showQuetion();
                        }
                    }, SLEEP_TIME_FOR_WRONG_ANS_THIRD_ATTEMP);
                }//END if
                else{//changes when single friendzy
                    setClickableGoButton(true);
                }
            }
        }, SLEEP_TIME);
    }

    /**
     * This method call when user give wrong answer for fill in
     */
    private void setHandlerForWrongAnswerForFillIn(){
        this.pauseTimer();
        disableKeyboard();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                resumeTimer();
                enableKeyboard();
                //dialog.dismiss();
                imgForGroundImage.setVisibility(ImageView.INVISIBLE);
                if(!questionObj.getLeftAns().equals("") && !questionObj.getRightAns().equals(""))
                    ansBox.setText(questionObj.getLeftAns() + " " + questionObj.getRightAns());
                else if(!questionObj.getLeftAns().equals(""))
                    ansBox.setText(questionObj.getLeftAns());
                else if(!questionObj.getRightAns().equals(""))
                    ansBox.setText(questionObj.getRightAns());
                else
                    ansBox.setText("");
                ansBox.setBackgroundResource(+ R.layout.edittext);

                if(noOfAttempts == MAX_NUMBER_OF_ATTEMPTS){
                    setProgress(false);
                    //setplayDataToArrayList();

                    ansBox.setText(questionObj.getLeftAns() + " " + questionObj.getOptionList().get(0)
                            + " " + questionObj.getRightAns());
                    pauseTimer();
                    disableKeyboard();
                    Handler handler1 = new Handler();
                    handler1.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            resumeTimer();
                            enableKeyboard();
                            //changes for timer on 11 Mar
                            setplayDataToArrayList();
                            //end changes

                            noOfAttempts = 0;
                            playSound.playSoundForSchoolSurriculumPagePeel
                                    (LearningCenterSchoolCurriculumEquationSolve.this);
                            setClickableGoButton(true);
                            showQuetion();
                        }
                    }, SLEEP_TIME_FOR_WRONG_ANS_THIRD_ATTEMP);
                }//END if
                else{//changes when single friendzy
                    setClickableGoButton(true);
                }
            }
        }, SLEEP_TIME);
    }


    @Override
    public void onBackPressed() {
        if(!isFromHomeWork){
            if(playSound != null)
                playSound.stopPlayer();

            if(isClickOnGo)
                this.clickOnBackPress();
            super.onBackPressed();
        }else{
            if(numberOfQuestion > 1){//1 for play first question
                MathFriendzyHelper.yesNoConfirmationDialog
                        (this, warningMessageOnBack, yesTextMsg, noTextMsg, new YesNoListenerInterface() {
                            @Override
                            public void onYes() {

                            }

                            @Override
                            public void onNo() {
                                if(isClickOnGo)
                                    clickOnBackPress();
                                callSuperOnBackPressed();
                            }
                        });
            }else{
                super.onBackPressed();
            }
        }

    }

    /**
     * Call super back pressed method
     */
    private void callSuperOnBackPressed(){
        super.onBackPressed();
    }

    /**
     * This method call when user click on back press
     */
    private void clickOnBackPress() {

        this.savePlayerDataWhenNoInternetConnected();

        this.insertIntoPlayerTotalPoints();

        SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
        String userId;
        String playerId;
        if(sharedPreffPlayerInfo.getString("userId", "").equals("0"))
            userId = "0";
        else
            userId = sharedPreffPlayerInfo.getString("userId", "");

        if(sharedPreffPlayerInfo.getString("playerId", "").equals("0"))
            playerId = "0";
        else
            playerId = sharedPreffPlayerInfo.getString("playerId", "");

        MathScoreForSchoolCurriculumTransferObj mathScoreObj = new MathScoreForSchoolCurriculumTransferObj();
        mathScoreObj.setUserId(userId);
        mathScoreObj.setPlayerId(playerId);
        mathScoreObj.setProblems(this.getEquationSolveXmlForInterNetNotConnected(playDataList));

		/*SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
		schoolImpl.openConnection();
		schoolImpl.insertIntoWordProblemResults(mathScoreObj);
		schoolImpl.closeConnection();
		 */

        if(CommonUtils.isInternetConnectionAvailable(this)){
            PlayerTotalPointsObj playerObj = null;
            LearningCenterimpl learningCenterimpl = new LearningCenterimpl(LearningCenterSchoolCurriculumEquationSolve.this);
            learningCenterimpl.openConn();
            playerObj = learningCenterimpl.getDataFromPlayerTotalPoints(playerId);
            learningCenterimpl.closeConn();

            playerObj.setPlayerId(playerId);
            playerObj.setUserId(userId);

            new AddCoinAndPointsForLoginUser(playerObj , LearningCenterSchoolCurriculumEquationSolve.this)
                    .execute(null,null,null);

            new AddWordProblemScoreonServer(mathScoreObj).execute(null,null,null);
        }else{
            SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
            schoolImpl.openConnection();
            schoolImpl.insertIntoWordProblemResults(mathScoreObj);
            schoolImpl.closeConnection();
        }
    }

    /**
     * This method set the play equation data to the list
     * for each equation which is to be save after completion of the
     * play all equations
     */
    private void setplayDataToArrayList(){

        //changes for timer on 11 Mar
        endTime = new Date();
        //end changes

        MathEquationTransferObj mathObjData = new MathEquationTransferObj();
        mathObjData.setEqautionId(questionObj.getQuestionId());
        mathObjData.setStartDate(CommonUtils.formateDate(startTime));
        mathObjData.setEndData(CommonUtils.formateDate(endTime));

        int startTimeValue = startTime.getSeconds() ;
        int endTimeVlaue   = endTime.getSeconds();

        if(endTimeVlaue < startTimeValue)
            endTimeVlaue = endTimeVlaue + 60;

        mathObjData.setTimeTakenToAnswer(endTimeVlaue - startTimeValue);
        mathObjData.setCatId(categoryId);
        mathObjData.setSubCatId(subCategoryId);

        if(isCorrectAnsByUser == 0)
            mathObjData.setPoints(0);
        else
            mathObjData.setPoints(this.getPointsForSolvingEachEquation());

        mathObjData.setIsAnswerCorrect(isCorrectAnsByUser);

        if(answerByUser.length() > 0)
            mathObjData.setUserAnswer(answerByUser);
        else
            mathObjData.setUserAnswer("1");

        mathObjData.setQuestionObj(questionObj);

        playDataList.add(mathObjData);
    }

    /**
     * This class update math word problem score on server
     * @author Yashwant Singh
     *
     */
    private class AddMathWordProblemPlayLevelScore extends AsyncTask<Void, Void, Void>{
        private MathScoreForSchoolCurriculumTransferObj mathScoreObj;

        AddMathWordProblemPlayLevelScore(MathScoreForSchoolCurriculumTransferObj mathScoreObj){
            PlayerTotalPointsObj playerObj = null;
            LearningCenterimpl learningCenterimpl = new LearningCenterimpl(LearningCenterSchoolCurriculumEquationSolve.this);
            learningCenterimpl.openConn();
            playerObj = learningCenterimpl.getDataFromPlayerTotalPoints(mathScoreObj.getPlayerId());
            learningCenterimpl.closeConn();

            mathScoreObj.setPoints(playerObj.getTotalPoints());
            mathScoreObj.setCoins(playerObj.getCoins());
            this.mathScoreObj = mathScoreObj;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            LearnignCenterSchoolCurriculumServerOperation schoolServerObj =
                    new LearnignCenterSchoolCurriculumServerOperation();
            schoolServerObj.addMathWordProblemPlayLevelScore(mathScoreObj);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }
    }

    /**
     * This class add word problem on server
     * @author Yashwant Singh
     *
     */
    class AddWordProblemScoreonServer extends AsyncTask<Void, Void, Void>{

        MathScoreForSchoolCurriculumTransferObj mathObj;

        AddWordProblemScoreonServer(MathScoreForSchoolCurriculumTransferObj mathObj){
            this.mathObj = mathObj;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            LearnignCenterSchoolCurriculumServerOperation serverObj =
                    new LearnignCenterSchoolCurriculumServerOperation();
            serverObj.addWordProblemScore(mathObj , 0);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }
    }

    @Override
    public void onInit(int status) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void onResume() {
        //Log.e(TAG, "onResume()");
        if(!isClickOnWorkArea){
            if(playSound != null)
                playSound.playSoundForSchoolSurriculumEquationSolve(this);
        }else
            isClickOnWorkArea = false;

        super.onResume();
    }

    @Override
    protected void onPause() {
        //Log.e(TAG, "onPause()");
        if(!isClickOnWorkArea){
            if(playSound != null)
                playSound.stopPlayer();
        }
        super.onPause();
    }
}
