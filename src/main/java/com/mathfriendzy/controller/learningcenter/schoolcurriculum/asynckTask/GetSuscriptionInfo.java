package com.mathfriendzy.controller.learningcenter.schoolcurriculum.asynckTask;

import java.util.ArrayList;

import com.mathfriendzy.controller.learningcenter.schoolcurriculum.LearningCenterSchoolCurriculumEquationSolve;
import com.mathfriendzy.model.learningcenter.CoinsFromServerObj;
import com.mathfriendzy.model.learningcenter.LearningCenteServerOperation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.PurchaseItemObj;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

public class GetSuscriptionInfo extends AsyncTask<Void, Void, CoinsFromServerObj>{

	private Context context;
	private String userId;
	private String playerId;
	private int subCategoryId;
	private int categoryId;
	private String subCatName;

	private ProgressDialog pg = null;

	public GetSuscriptionInfo(Context context , String userId , String playerId , 
			int subCategoryId , int categoryId, String subCatName ){
		this.context = context;
		this.userId  = userId;
		this.playerId = playerId;

		this.categoryId = categoryId;
		this.subCategoryId = subCategoryId;
		this.subCatName   = subCatName;
	}

	@Override
	protected void onPreExecute() {
		pg = CommonUtils.getProgressDialog(context);
		pg.show();

		super.onPreExecute();
	}

	@Override
	protected CoinsFromServerObj doInBackground(Void... params) {
		LearningCenteServerOperation learnignCenterOpr = new LearningCenteServerOperation();
		CoinsFromServerObj coinsFromServer = learnignCenterOpr.getSubscritiptionInfo(userId , playerId);
		return coinsFromServer;
	}

	@Override
	protected void onPostExecute(CoinsFromServerObj coinsFromServer) {

		pg.cancel();

		if(coinsFromServer != null){
			//update app status in local database
			if(coinsFromServer.getAppUnlock() != -1){
				ArrayList<PurchaseItemObj> purchaseItem = new ArrayList<PurchaseItemObj>();
				PurchaseItemObj purchseObj = new PurchaseItemObj();
				purchseObj.setUserId(userId);
				purchseObj.setItemId(100);
				purchseObj.setStatus(coinsFromServer.getAppUnlock());
				purchaseItem.add(purchseObj);

				LearningCenterimpl learningCenter = new LearningCenterimpl(context);
				learningCenter.openConn();

				learningCenter.deleteFromPurchaseItem(userId , 100);
				learningCenter.insertIntoPurchaseItem(purchaseItem);		
				learningCenter.closeConn();
			}

			if(coinsFromServer.getAppUnlock() == -1 || coinsFromServer.getAppUnlock() == 0){
				DialogGenerator dg = new DialogGenerator(context);
				dg.generateDailogForSchoolCurriculumUnlock(coinsFromServer , userId , playerId ,
						coinsFromServer.getAppUnlock());
			}else{
				Intent intent = new Intent(context , LearningCenterSchoolCurriculumEquationSolve.class);
				intent.putExtra("categoryId", categoryId);
				intent.putExtra("subCategoryId", subCategoryId);
				intent.putExtra("playerSelectedGrade", CommonUtils.selectedGrade);
				intent.putExtra("subCatName", subCatName);
				((Activity)context).startActivityForResult(intent , 2);
			}
		}else{
			CommonUtils.showInternetDialog(context);
		}
		super.onPostExecute(coinsFromServer);
	}
}
