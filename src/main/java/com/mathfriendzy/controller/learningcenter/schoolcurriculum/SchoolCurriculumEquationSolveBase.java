package com.mathfriendzy.controller.learningcenter.schoolcurriculum;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnUtteranceCompletedListener;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.assessmenttest.NewAssessmentTestScore;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.registration.Register;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.LearnignCenterSchoolCurriculumServerOperation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.MathEquationTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SchoolCurriculumLearnignCenterimpl;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.UpdatedInfoTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.WordProblemQuestionTransferObj;
import com.mathfriendzy.model.schoolcurriculum.singlefriendzy.SingleFriendzyServerOperationForWordProblem;
import com.mathfriendzy.model.spanishchanges.SpanishChangesImpl;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.PlaySound;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;

import static com.mathfriendzy.utils.ICommonUtils.LEARNIGN_CENTER_SCHOOL_CURRICULUM_SOLVE_EQUATION_FLAG;

public abstract class SchoolCurriculumEquationSolveBase extends ActBaseClass implements OnClickListener
        , com.mathfriendzy.controller.timer.MyTimerInrerface,
        TextToSpeech.OnInitListener{


    /**
     *
     */
    private static final long serialVersionUID = 1L;

    protected TextView mfTitleHomeScreen = null;
    protected TextView txtTimer          = null;
    protected TextView txtPlayerName     = null;
    protected TextView txtPoints         = null;
    protected ImageView imgFlag          = null;

    protected LinearLayout progressBarLayout = null;

    protected ImageView imgForGroundImage = null;

    protected RelativeLayout relativeBottomBarWithoutFillIn = null;
    protected RelativeLayout relativeBottomBarFillIn        = null;
    protected LinearLayout   primaryKeyboard                = null;
    protected LinearLayout   secondryKeyboard               = null;

    protected RelativeLayout questionLayout = null;
    protected ScrollView      scrollView    = null;
    //for setting for ground color
    protected RelativeLayout questionAnswerLayout = null;

    //for relativeBottomBarWithoutFillIn
    protected Button btnRoughWorkWithourFillIn = null;
    protected Button btnGoForWithoutFillIn     = null;

    //for relativeBottomBarFillIn

    protected Button btnRoughWork       = null;
    protected TextView ansBox           = null;
    protected Button  btnSpace          = null;
    protected Button  btnsecondryKeyBoard = null;
    protected Button  btn1              = null;
    protected Button  btn2              = null;
    protected Button  btn3              = null;
    protected Button  btn4              = null;
    protected Button  btn5              = null;
    protected Button  btn6              = null;
    protected Button  btn7              = null;
    protected Button  btn8              = null;
    protected Button  btn9              = null;
    protected Button  btn0              = null;
    protected Button btnSynchronized    = null;// for clear ans box
    protected Button btnBackArrow       = null;
    protected Button btnGoForFillIn     = null;

    //for secondary key board
    protected Button btnNumber          = null;
    protected Button btnDot             = null;
    protected Button btnComma           = null;
    protected Button btnMinus           = null;
    protected Button btnPlus            = null;
    protected Button btnDevide          = null;
    protected Button btnMultiply        = null;
    protected Button btnCollon          = null;
    protected Button btnLessThan        = null;
    protected Button btnGreaterThan     = null;
    protected Button btnEqual           = null;
    protected Button btnRoot            = null;
    protected Button btnDevider         = null;
    protected Button btnOpeningBracket  = null;
    protected Button btnClosingBracket  = null;
    protected Button btnSecandryDelete  = null;

    protected ImageView imgTimerImage   = null;

    private final String TAG = this.getClass().getSimpleName();

    //changes when single friendzy added
    protected Date startTime = null;
    protected Date endTime   = null;
    protected final int TAB_HEIGHT = 800;
    protected final int TAB_WIDTH = 480;
    protected final int TAB_DENISITY = 160;
    protected RelativeLayout  queLayout;
    protected TextView txtQue;
    protected ImageView txtQueImage;
    protected TextView txtQueNo;
    protected ImageView imgQuestion;
    protected LinearLayout multipleChicelayout;
    protected int numberOfQuestion = 0;

    //for holding the option layout
    protected ArrayList<RelativeLayout> optionLayoutList = null;	//contain the number of option
    protected ArrayList<TextView> optionNumberList   = null;     	// and also contain the layout refrences for setting
    //back ground after wrong answer
    protected ArrayList<String>  selectedOptionValue = null;//contains the option text value
    protected ArrayList<OptionSelectDataObj> selectedOptionList = null;

    protected String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    protected boolean isTab = false;

    //for timer
    protected CountDownTimer timer        = null;
    protected boolean isCountDawnTimer    = true;
    protected long startTimerForRoughArea = 0;
    protected Runnable updateTimerThread  = null;
    //protected long startTimeForTimer  = 0L;
    public static long startTimeForTimer  = 0L;
    protected long timeInMilliseconds = 0L;
    protected long timeSwapBuff 	  = 0L;
    protected long updatedTime 		  = 0L;

    protected Handler customHandler = new Handler();
    protected long startTimeForTimerAtFirstTime = 0l;

    //random number
    protected Random random = null;
    //anser value
    //for setting answerBos Value
    protected int isAnswerConatin = 0; // for if answer contain prefix,postfix or both
    // 0 for nothing , 1 for prefix , 2 for postfix, 3 for both
    protected String ansPrefixValue  = null;
    protected String ansPostFixValue = null;
    protected StringBuilder answerValue     = new StringBuilder();

    //for holding the question which is display
    protected WordProblemQuestionTransferObj questionObj = null;

    //for animation
    protected Animation animation 	= null;

    //for click or not
    protected boolean isClickOnGo = false;

    //points
    protected final int MAX_POINTS = 250;
    protected int points = 0;

    //play datalist
    protected ArrayList<MathEquationTransferObj> playDataList = null;

    //added for emailSend for wrong question
    protected Button btnEmailSendForWrongQuestion1 = null;
    protected Button btnEmailSendForWrongQuestion2 = null;

    //changes for Spanish
    protected ImageView imgSpanish = null;
    protected int languageCode 	= 1;
    protected final int ENGLISH = 1;
    protected final int SPANISH = 2;
    protected boolean isSpanish = false;
    //end changes for Spanish

    protected int selectedPlayGrade = 1;

    //changes for Spanish , variables for setting the same option as in previous lang
    private ArrayList<TextView> txtOptionList;
    private ArrayList<Integer>  originalIndexList;
    private ArrayList<ImageView> imgOptionList;
    //for holding the question which is display
    private WordProblemQuestionTransferObj questionObjForWordArea = null;

    //for question translation , Issues list 5 march 2014 , issues no. 5
    private ShowTranslateQuestionAfterDownloading showQuestionAfterTranslation = null;
    //end chanegs

    //for resume timer  , up timer , adding for the report probelm updated feature on 31 march 2014
    protected Date resumeTimeForTimer = null; // for up timer handler thread (customHandler)

    //for tts
    protected ImageView imgTTS = null;
    protected TextToSpeech tts = null;
    private boolean isClickOnTTS = false;

    //for sound on work area
    public static boolean isClickOnWorkArea = false;

    //for play sound
    public static PlaySound playSound = null;

    //for HomeWork
    protected boolean isFromHomeWork = false;


    //for up timer
    protected Date pauseStartTime = null;
    protected Date resumeStartTime = null;

    //changes for float value , like (2,-2.5) or for .7 or 00.7
    protected boolean getIsFloatValue(String stringValue){
        if(stringValue.contains(".")){
            try{
                Float.parseFloat(stringValue.replaceAll(" ", "").replace(",", ""));
                return false;
            }catch(Exception e){
                return true;
            }
        }else
            return true;
    }

    //changes for Spanish
    protected void setLanguageCode(){
		/*UserRegistrationOperation userObj = new UserRegistrationOperation(this);
		RegistereUserDto userPlayerObj = userObj.getUserData();
		if(userPlayerObj.getPreferedLanguageId().equals("1")){
			languageCode 	= ENGLISH;
		}else if(userPlayerObj.getPreferedLanguageId().equals("2")){
			languageCode 	 = SPANISH;
		}*/
        if(CommonUtils.getUserLanguageCode(this) == CommonUtils.ENGLISH)
            languageCode = ENGLISH;
        else
            languageCode = SPANISH;

        this.setIsSpanishBooleanValue();
    }
    //end changes

    /**
     * This method set or reset the isSpanish value
     */
    protected void setIsSpanishBooleanValue(){
        if(languageCode == ENGLISH)
            isSpanish = true;
        else if(languageCode == SPANISH)
            isSpanish = false;
    }

    /**
     * This method set widgets references from xml layout
     */
    protected void setWidgetsReferences() {

        if(LEARNIGN_CENTER_SCHOOL_CURRICULUM_SOLVE_EQUATION_FLAG)
            Log.e(TAG, "inside setWidgetsReferences()");

        //changes for Spanish
        this.setLanguageCode();
        //end changes

        mfTitleHomeScreen = (TextView) findViewById(R.id.mfTitleHomeScreen);
        txtTimer = (TextView) findViewById(R.id.txtTimer);
        txtPlayerName = (TextView) findViewById(R.id.txtPlayerName);
        txtPoints = (TextView) findViewById(R.id.txtPoints);
        imgFlag   = (ImageView) findViewById(R.id.imgFlag);

        progressBarLayout = (LinearLayout) findViewById(R.id.progressBarLayout);

        questionLayout   = (RelativeLayout) findViewById(R.id.questionLayout);

        relativeBottomBarWithoutFillIn = (RelativeLayout) findViewById(R.id.relativeBottomBarWithoutFillIn);
        relativeBottomBarFillIn = (RelativeLayout) findViewById(R.id.relativeBottomBarFillIn);

        btnRoughWorkWithourFillIn = (Button) findViewById(R.id.btnRoughWorkWithourFillIn);
        btnGoForWithoutFillIn     = (Button) findViewById(R.id.btnGoForWithoutFillIn);

        primaryKeyboard = (LinearLayout) findViewById(R.id.primaryKeyboard);
        secondryKeyboard = (LinearLayout) findViewById(R.id.secondryKeyboard);

        ansBox = (TextView) findViewById(R.id.ansBox);

        btnRoughWork = (Button) findViewById(R.id.btnRoughWork);
        btnSpace = (Button) findViewById(R.id.btnSpace);
        btnsecondryKeyBoard = (Button) findViewById(R.id.btnsecondryKeyBoard);
        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);
        btn4 = (Button) findViewById(R.id.btn4);
        btn5 = (Button) findViewById(R.id.btn5);
        btn6 = (Button) findViewById(R.id.btn6);
        btn7 = (Button) findViewById(R.id.btn7);
        btn8 = (Button) findViewById(R.id.btn8);
        btn9 = (Button) findViewById(R.id.btn9);
        btn0 = (Button) findViewById(R.id.btn0);
        btnSynchronized = (Button) findViewById(R.id.btnSynchronized);
        btnBackArrow = (Button) findViewById(R.id.btnBackArrow);
        btnGoForFillIn = (Button) findViewById(R.id.btnGoForFillIn);

        btnNumber = (Button) findViewById(R.id.btnNumber);
        btnDot = (Button) findViewById(R.id.btnDot);
        btnComma = (Button) findViewById(R.id.btnComma);
        btnMinus = (Button) findViewById(R.id.btnMinus);
        btnPlus = (Button) findViewById(R.id.btnPlus);
        btnDevide = (Button) findViewById(R.id.btnDevide);
        btnMultiply = (Button) findViewById(R.id.btnMultiply);
        btnCollon = (Button) findViewById(R.id.btnCollon);
        btnLessThan = (Button) findViewById(R.id.btnLessThan);
        btnGreaterThan = (Button) findViewById(R.id.btnGreaterThan);
        btnEqual = (Button) findViewById(R.id.btnEqual);
        btnRoot = (Button) findViewById(R.id.btnRoot);
        btnDevider = (Button) findViewById(R.id.btnDevider);
        btnOpeningBracket = (Button) findViewById(R.id.btnOpeningBracket);
        btnClosingBracket = (Button) findViewById(R.id.btnClosingBracket);
        btnSecandryDelete = (Button) findViewById(R.id.btnSecandryDelete);

        questionAnswerLayout = (RelativeLayout) findViewById(R.id.questionAnswerLayout);

        imgForGroundImage = (ImageView) findViewById(R.id.imgForGroundImage);

        //for countdown time
        imgTimerImage     = (ImageView) findViewById(R.id.imgTimerImage);

        scrollView        = (ScrollView) findViewById(R.id.scrollView);

        //added for emailSend for wrong question
        btnEmailSendForWrongQuestion1 = (Button) findViewById(R.id.btnEmailSendForWrongQuestion1);
        btnEmailSendForWrongQuestion2 = (Button) findViewById(R.id.btnEmailSendForWrongQuestion2);

        if(LEARNIGN_CENTER_SCHOOL_CURRICULUM_SOLVE_EQUATION_FLAG)
            Log.e(TAG, "outside setWidgetsReferences()");
    }

    /**
     * This method set translation text
     */
    protected void setTextFromTranslation() {
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        mfTitleHomeScreen.setText(transeletion.getTranselationTextByTextIdentifier("mfTitleHomeScreen"));
        transeletion.closeConnection();
    }

    /**
     * This method set listener on widgets
     */
    protected void setListenerOnWidgets() {
        btnGoForWithoutFillIn.setOnClickListener(this);
        btnGoForFillIn.setOnClickListener(this);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btn9.setOnClickListener(this);
        btn0.setOnClickListener(this);

        btnSynchronized.setOnClickListener(this);
        btnSpace.setOnClickListener(this);
        btnsecondryKeyBoard.setOnClickListener(this);

        //for secondry keyboard
        btnNumber.setOnClickListener(this);
        btnDot.setOnClickListener(this);
        btnComma.setOnClickListener(this);
        btnMinus.setOnClickListener(this);
        btnPlus.setOnClickListener(this);
        btnDevide.setOnClickListener(this);
        btnMultiply.setOnClickListener(this);
        btnCollon.setOnClickListener(this);
        btnLessThan.setOnClickListener(this);
        btnGreaterThan.setOnClickListener(this);
        btnEqual.setOnClickListener(this);
        btnRoot.setOnClickListener(this);
        btnDevider.setOnClickListener(this);
        btnOpeningBracket.setOnClickListener(this);
        btnClosingBracket.setOnClickListener(this);
        btnSecandryDelete.setOnClickListener(this);

        btnRoughWork.setOnClickListener(this);
        btnRoughWorkWithourFillIn.setOnClickListener(this);
        //for new timer
        imgTimerImage.setOnClickListener(this);


        //added for emailSend for wrong question
        btnEmailSendForWrongQuestion1.setOnClickListener(this);
        btnEmailSendForWrongQuestion2.setOnClickListener(this);
    }


    //changes when single friendzy added
    /**
     *  This method set the key board visibility
     */
    protected void setKeyBoardVisivility(int fill_in_blank){
        if(fill_in_blank == 1){
            relativeBottomBarFillIn.setVisibility(RelativeLayout.VISIBLE);
            relativeBottomBarWithoutFillIn.setVisibility(RelativeLayout.GONE);
        }
        else{
            relativeBottomBarFillIn.setVisibility(RelativeLayout.GONE);
            relativeBottomBarWithoutFillIn.setVisibility(RelativeLayout.VISIBLE);
        }
    }

    /**
     * This method return the image list from question , answer,option,and imageField
     * @param questionObj
     * @return
     */
    protected ArrayList<String> getImageArrayList(WordProblemQuestionTransferObj questionObj){

        ArrayList<String> imageNameList = new ArrayList<String>();

        try {
            if (questionObj.getQuestion().contains(".png"))
                imageNameList.add(questionObj.getQuestion());
        }catch(Exception e){
            e.printStackTrace();
        }

        try {
            for (int i = 0; i < questionObj.getOptionList().size(); i++) {
                if (questionObj.getOptionList().get(i).contains(".png"))
                    imageNameList.add(questionObj.getOptionList().get(i));
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        try {
            if (!questionObj.getImage().equals(""))
                imageNameList.add(questionObj.getImage());
        }catch(Exception e){
            e.printStackTrace();
        }

        return imageNameList;
    }

    /**
     * This method check availability of images in database
     * if the the images not found the add into the the imageNot found list
     * and return this list
     * @param imageNameList
     */
    protected ArrayList<String> checkImageAvalability(ArrayList<String> imageNameList){
        ArrayList<String> unAvailableImageList = new ArrayList<String>();

        SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
        schoolImpl.openConnection();
        for(int i = 0 ; i < imageNameList.size() ; i ++ ){
            if(!schoolImpl.checkImageAvailabity(imageNameList.get(i)))
                unAvailableImageList.add(imageNameList.get(i));
        }
        schoolImpl.closeConnection();

        return unAvailableImageList;
    }

    /**
     * This method return index if option already selected
     * otherwise -1
     * @param clickedOptionLayout
     * @param selectedoptionList
     * @return
     */
    protected int isOptionAlreadySelected(RelativeLayout clickedOptionLayout , ArrayList<OptionSelectDataObj>
            selectedoptionList)
    {
        for(int i = 0 ; i < selectedoptionList.size() ; i ++ ){
            if(selectedoptionList.get(i).getOpationSeletedLayout() == clickedOptionLayout)
                return i;
        }
        return -1;
    }

    /**
     * This method set the back to the question image
     * @param imgQuestion
     * @param imageName
     */
    protected void setBackGroundImageForQuestionImage(ImageView imgQuestion , String imageName){
        SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
        schoolImpl.openConnection();
        if(schoolImpl.getImage(imageName) != null){
            if(schoolImpl.checkImageAvailabity(imageName)){
                //Bitmap bitmap = CommonUtils.getBitmapFromByte(schoolImpl.getImage(imageName));
                imgQuestion.setImageBitmap(CommonUtils.getBitmapFromByte(schoolImpl.getImage(imageName)));
            }
        }
        schoolImpl.closeConnection();
    }


    /**
     * This class show the timer for player
     * @author Yashwant Singh
     *
     */
    protected class MyTimer extends CountDownTimer
    {
        public MyTimer(long millisInFuture, long countDownInterval){
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished){
            startTimerForRoughArea = millisUntilFinished;
            if(((millisUntilFinished/1000) % 60) < 10) // Calculate the second if it less then 10 then add 0 prefix
                txtTimer.setText(((millisUntilFinished/1000) / 60) + ":0" + ((millisUntilFinished/1000) % 60));
            else
                txtTimer.setText(((millisUntilFinished/1000) / 60) + ":" + ((millisUntilFinished/1000) % 60));
        }

        @Override
        public void onFinish(){
            imgTimerImage.setVisibility(ImageView.GONE);
            startTimer();
        }
    }

    /**
     * This method set the timer
     */
    protected void startTimer() {
        isCountDawnTimer = false;
        startTimerForRoughArea = 0;

        txtTimer.setTextColor(Color.RED);
        //txtTimer.setTextColor(Color.BLACK);

        startTimeForTimer = SystemClock.uptimeMillis();

        updateTimerThread = new Runnable() {
            public void run() {
                timeInMilliseconds = SystemClock.uptimeMillis() - startTimeForTimer;
                updatedTime = timeSwapBuff + timeInMilliseconds;

                int secs = (int) (updatedTime / 1000);
                int mins = secs / 60;
                secs = secs % 60;
                //txtTimer.setText(String.format("%02d", mins) + ":"+ String.format("%02d", secs));
                //for format change
                txtTimer.setText(mins + ":"+ String.format("%02d", secs));
                //int milliseconds = (int) (updatedTime % 1000);
				/*Log.e(TAG, "" + String.format("%02d", mins) + ":"
						+ String.format("%02d", secs) + ":"
						+ String.format("%03d", milliseconds));*/

                customHandler.postDelayed(this, 1000);
            }
        };
        customHandler.postDelayed(updateTimerThread, 0);
    }

    /**
     * This class dawnload image from server for schoolcurriculum
     * @author Yashwant Singh
     *
     */
    protected class DawnloadImageForSchooCurriculum extends AsyncTask<Void, Void, Void>{

        private ArrayList<String> imageNamelist;
        private byte [] imageFromServer;
        private Context context;
        SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl;
        LearnignCenterSchoolCurriculumServerOperation learningCenterServerobj;
        private WordProblemQuestionTransferObj questionObj;

        private ProgressDialog pd = null;

        private Date dawnloadStartTime = null;

        public DawnloadImageForSchooCurriculum(ArrayList<String> imageNamelist , Context context,
                                               WordProblemQuestionTransferObj questionObj){
            this.imageNamelist 	= imageNamelist;
            this.context   		= context;
            this.questionObj    = questionObj;
            dawnloadStartTime   = new Date();
        }

        @Override
        protected void onPreExecute() {
            pd = CommonUtils.getProgressDialog(context);
            pd.show();
            schooloCurriculumImpl   = new SchoolCurriculumLearnignCenterimpl(context);
            learningCenterServerobj = new LearnignCenterSchoolCurriculumServerOperation();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            for(int i = 0 ; i < imageNamelist.size() ; i ++ ){
                schooloCurriculumImpl.openConnection();
                if(!schooloCurriculumImpl.isImageAlreadyDawnloaded(imageNamelist.get(i))){
                    imageFromServer = learningCenterServerobj.dawabloadImageFromServer(imageNamelist.
                            get(i));
                    schooloCurriculumImpl.insertIntoSchoolCurriculumImageTable(imageNamelist.get(i),
                            imageFromServer);
                }
                schooloCurriculumImpl.closeConnection();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            pd.cancel();

            if(isCountDawnTimer){
                if(timer != null)
                    timer.start();
            }else{
                if(customHandler != null){
                    Date dawnloadEndTime = new Date();
                    startTimeForTimer = startTimeForTimer + (dawnloadEndTime.getTime() -
                            dawnloadStartTime.getTime());
                    customHandler.postDelayed(updateTimerThread, 0);
                }
            }
            //setQuestionAfterLanguageChange(questionObj);
            setQuestionAnswerLayout(questionObj);
            super.onPostExecute(result);
        }
    }

    /**
     * Pause timer
     */
    protected void pauseTimer(){
        try {
            if (isCountDawnTimer) {
                if (timer != null) {
                    timer.cancel();
                    timer = new MyTimer(startTimerForRoughArea, 1 * 1000);//second argument is the interval
                }
            } else {
                if (customHandler != null) {
                    pauseStartTime = new Date();
                    customHandler.removeCallbacks(updateTimerThread);
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Resume timer
     */
    protected void resumeTimer(){
        try {
            if (isCountDawnTimer) {
                if (timer != null)
                    timer.start();
            } else {
                if (customHandler != null) {
                    resumeStartTime = new Date();
                    if(pauseStartTime != null && resumeStartTime != null) {
                        startTimeForTimer = startTimeForTimer + (resumeStartTime.getTime() -
                                pauseStartTime.getTime());
                    }
                    customHandler.postDelayed(updateTimerThread, 0);
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    protected void disableKeyboard(){
        btn1.setClickable(false);
        btn2.setClickable(false);
        btn3.setClickable(false);
        btn4.setClickable(false);
        btn5.setClickable(false);
        btn6.setClickable(false);
        btn7.setClickable(false);
        btn8.setClickable(false);
        btn9.setClickable(false);
        btn0.setClickable(false);
        btnSpace.setClickable(false);
        btnSynchronized.setClickable(false);
        btnsecondryKeyBoard.setClickable(false);
        btnNumber.setClickable(false);
        btnDot.setClickable(false);
        btnMinus.setClickable(false);
        btnComma.setClickable(false);
        btnPlus.setClickable(false);
        btnDevide.setClickable(false);
        btnMultiply.setClickable(false);

        btnCollon.setClickable(false);
        btnLessThan.setClickable(false);
        btnGreaterThan.setClickable(false);
        btnEqual.setClickable(false);
        btnRoot.setClickable(false);
        btnDevider.setClickable(false);
        btnOpeningBracket.setClickable(false);
        btnClosingBracket.setClickable(false);
        btnRoughWork.setClickable(false);
        btnRoughWorkWithourFillIn.setClickable(false);
        btnEmailSendForWrongQuestion1.setClickable(false);
        btnEmailSendForWrongQuestion2.setClickable(false);
    }

    protected void enableKeyboard(){
        btn1.setClickable(true);
        btn2.setClickable(true);
        btn3.setClickable(true);
        btn4.setClickable(true);
        btn5.setClickable(true);
        btn6.setClickable(true);
        btn7.setClickable(true);
        btn8.setClickable(true);
        btn9.setClickable(true);
        btn0.setClickable(true);
        btnSpace.setClickable(true);
        btnSynchronized.setClickable(true);
        btnsecondryKeyBoard.setClickable(true);
        btnNumber.setClickable(true);
        btnDot.setClickable(true);
        btnMinus.setClickable(true);
        btnComma.setClickable(true);
        btnPlus.setClickable(true);
        btnDevide.setClickable(true);
        btnMultiply.setClickable(true);

        btnCollon.setClickable(true);
        btnLessThan.setClickable(true);
        btnGreaterThan.setClickable(true);
        btnEqual.setClickable(true);
        btnRoot.setClickable(true);
        btnDevider.setClickable(true);
        btnOpeningBracket.setClickable(true);
        btnClosingBracket.setClickable(true);
        btnRoughWork.setClickable(true);
        btnRoughWorkWithourFillIn.setClickable(true);
        btnEmailSendForWrongQuestion1.setClickable(true);
        btnEmailSendForWrongQuestion2.setClickable(true);
    }

    /**
     * This method set question answer layout
     */
    protected void setQuestionAnswerLayout(final WordProblemQuestionTransferObj questionObj){

        //changes for Spanish
        questionObjForWordArea = questionObj;
        //end changes

        //start timer when first question is display
        if(timer == null){
            timer = new MyTimer(startTimeForTimerAtFirstTime * 1000, 1 * 1000);//second argument is the interval
            timer.start();
        }

        startTime = new Date();
        questionLayout.removeAllViews();

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        LayoutInflater inflater = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
        RelativeLayout child;

        if(metrics.heightPixels <= TAB_HEIGHT && metrics.widthPixels >= TAB_WIDTH && metrics.densityDpi
                == TAB_DENISITY)
        {
            child = (RelativeLayout) inflater.inflate
                    (R.layout.schoolcurriculum_equation_solve_layout_for_fill_in_for_low_denisity, null);
        }
        else{
            child = (RelativeLayout) inflater.inflate
                    (R.layout.schoolcurriculum_equation_solve_layout_for_fill_in, null);
        }

        queLayout = (RelativeLayout) child.findViewById(R.id.queLayout);
        txtQue    = (TextView)       child.findViewById(R.id.txtQue);
        txtQueImage = (ImageView)    child.findViewById(R.id.txtQueImage);
        txtQueNo  = (TextView)       child.findViewById(R.id.txtQueNo);
        imgQuestion = (ImageView)    child.findViewById(R.id.imgQuestion);

        //for tts
        imgTTS    = (ImageView)      child.findViewById(R.id.imgSpeach);

        //changes for spanish
        imgSpanish = (ImageView)     child.findViewById(R.id.imgSpanish);
        imgSpanish.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.e(TAG, "Click on language conversion");
                clickOnSpanish(questionObj , false);
            }
        });
        //end changes

        //for tts
        imgTTS.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isSpanish){
                    textToSpeach(CommonUtils.SPANISH, questionObj.getQuestion() , questionObj.getQuestionId());
                }else{
                    textToSpeach(CommonUtils.ENGLISH, questionObj.getQuestion() , questionObj.getQuestionId());
                }
            }
        });
        //end changes

        multipleChicelayout = (LinearLayout) child.findViewById(R.id.multipleChicelayout);

        multipleChicelayout.removeAllViews();

        txtQueNo.setText(numberOfQuestion + "");

        if(questionObj.getQuestion().contains(".png"))
        {
            imgTTS.setVisibility(ImageView.INVISIBLE);
            txtQueImage.setVisibility(ImageView.VISIBLE);
            txtQue.setVisibility(TextView.GONE);
            this.setBackGroundImageForQuestionImage(txtQueImage, questionObj.getQuestion());
        }else{
            txtQueImage.setVisibility(ImageView.GONE);
            txtQue.setVisibility(TextView.VISIBLE);
            //changes for Html or '' change into '
            if(questionObj.getQuestion().contains("&#"))
                txtQue.setText(Html.fromHtml(questionObj.getQuestion().replaceAll("''", "'")));
            else
                txtQue.setText(questionObj.getQuestion().replaceAll("''", "'"));
            //txtQue.setText(questionObj.getQuestion().replaceAll("''", "'"));
        }

        //this.setMarginTop(imgSpanish, queLayout);

        if(questionObj.getImage().length() > 0){
            this.setBackGroundImageForQuestionImage(imgQuestion, questionObj.getImage());
        }else{
            imgQuestion.setVisibility(ImageView.GONE);
        }

        if(questionObj.getFillIn() != 1){
            optionLayoutList    = new ArrayList<RelativeLayout>();
            optionNumberList    = new ArrayList<TextView>();
            selectedOptionValue = new ArrayList<String>();
            selectedOptionList  = new ArrayList<OptionSelectDataObj>();

            //changes for Spanish
            txtOptionList       = new ArrayList<TextView>();
            originalIndexList   = new ArrayList<Integer>();
            imgOptionList       = new ArrayList<ImageView>();
            //end changes

            //copying original optionlist into tempArray List
            ArrayList<String>  tempoperationlist = new ArrayList<String>(questionObj.getOptionList());
            //WordProblemQuestionTransferObj questionObjTemp = new WordProblemQuestionTransferObj();

            //set option layout data
            int numberOfOption = 0 ;

            while(tempoperationlist.size() > 0){
                int randomNumber = 	random.nextInt(tempoperationlist.size());

                if(tempoperationlist.get(randomNumber).length() > 0){//option which has value
                    numberOfOption ++ ;

                    LayoutInflater inflaterOption = (LayoutInflater)
                            getSystemService(this.LAYOUT_INFLATER_SERVICE);
                    RelativeLayout option = (RelativeLayout) inflater.inflate
                            (R.layout.schoo_curricullum_equation_solve_layout_for_multiplechoice, null);
                    RelativeLayout OptionLayout = (RelativeLayout) option.findViewById(R.id.OptionLayout);
                    TextView       txtoptionNumber = (TextView) option.findViewById(R.id.txtoptionNumber);
                    TextView       txtOption       = (TextView) option.findViewById(R.id.txtOption);
                    ImageView      imgOptionImage  = (ImageView)option.findViewById(R.id.imgOptionImage);

                    //changes for Spanish
                    txtOptionList.add(txtOption);
                    imgOptionList.add(imgOptionImage);
                    for(int i = 0 ; i < questionObj.getOptionList().size() ; i ++ ){
                        if(questionObj.getOptionList().get(i).
                                equals(tempoperationlist.get(randomNumber)))
                            originalIndexList.add(i);

                    }
                    //end changes

                    if(isTab)
                        OptionLayout.setBackgroundResource(getResources().getIdentifier
                                ("option" + numberOfOption + "_ipad", "drawable",getPackageName()));
                    else
                        OptionLayout.setBackgroundResource(getResources().getIdentifier
                                ("option" + numberOfOption + "", "drawable",getPackageName()));

                    txtoptionNumber.setText(alphabet.charAt(numberOfOption - 1) + "");

                    if(tempoperationlist.get(randomNumber).contains(".png")){
                        imgOptionImage.setVisibility(ImageView.VISIBLE);
                        txtOption.setVisibility(TextView.GONE);

                        //this.setBackGroundImageForQuestionImage(imgOptionImage, questionObj.
                        //		getOptionList().get(randomNumber));
                        this.setBackGroundImageForQuestionImage(imgOptionImage, tempoperationlist.get(randomNumber));
                    }
                    else{
                        imgOptionImage.setVisibility(ImageView.GONE);
                        txtOption.setVisibility(TextView.VISIBLE);

                        //changes for Html or '' to '
                        if(tempoperationlist.get(randomNumber).contains("&#"))
                            txtOption.setText(Html.fromHtml(tempoperationlist.get(randomNumber).replaceAll("''", "'")));
                        else
                            txtOption.setText(tempoperationlist.get(randomNumber).replaceAll("''", "'"));
                    }

                    OptionLayout.setOnClickListener(new OnClickListener()
                    {
                        @Override
                        public void onClick(View v) {
                            for(int i = 0 ; i < optionLayoutList.size() ; i ++ ){
                                if(v == optionLayoutList.get(i)){

                                    int selectedindex = isOptionAlreadySelected(optionLayoutList.get(i),
                                            selectedOptionList);

                                    if(selectedindex != -1){
                                        selectedOptionList.get(selectedindex).getTxtSelectedtext().setText
                                                (selectedOptionList.get(selectedindex).getTextNumber());

                                        if(isTab){
                                            selectedOptionList.get(selectedindex).getOpationSeletedLayout()
                                                    .setBackgroundResource(getResources().getIdentifier
                                                            ("option" + (selectedOptionList.get(selectedindex).
                                                                    getSelectedOptionNumber())
                                                                    + "_ipad", "drawable",getPackageName()));
                                        }else{
                                            selectedOptionList.get(selectedindex).getOpationSeletedLayout()
                                                    .setBackgroundResource(getResources().getIdentifier
                                                            ("option" + (selectedOptionList.get(selectedindex).
                                                                            getSelectedOptionNumber()) + "",
                                                                    "drawable",getPackageName()));
                                        }
                                        selectedOptionList.remove(selectedindex);
                                    }
                                    else{
                                        OptionSelectDataObj sectedoptionData = new OptionSelectDataObj();
                                        sectedoptionData.setTextNumber(optionNumberList.get(i).getText()
                                                + "");
                                        sectedoptionData.setOpationSeletedLayout(optionLayoutList.get(i));
                                        sectedoptionData.setSelectedOptionNumber(i + 1);
                                        sectedoptionData.setTxtSelectedtext(optionNumberList.get(i));
                                        sectedoptionData.setSelectedOptionTextValue(selectedOptionValue.get(i));
                                        selectedOptionList.add(sectedoptionData);

                                        //set text as blank text
                                        optionNumberList.get(i).setText("");
                                        if(isTab)
                                            optionLayoutList.get(i).setBackgroundResource(getResources()
                                                    .getIdentifier
                                                            ("optionselect" + (i + 1) + "_ipad", "drawable",
                                                                    getPackageName()));
                                        else
                                            optionLayoutList.get(i).setBackgroundResource(getResources()
                                                    .getIdentifier
                                                            ("optionselect" + (i + 1) + "", "drawable",
                                                                    getPackageName()));
                                    }
                                }
                            }
                        }
                    });

                    selectedOptionValue.add(tempoperationlist.get(randomNumber));
                    optionNumberList.add(txtoptionNumber);
                    optionLayoutList.add(OptionLayout);
                    multipleChicelayout.addView(option);
                }
                tempoperationlist.remove(randomNumber);
            }
        }
        else{
            if(!questionObj.getLeftAns().equals("") && !questionObj.getRightAns().equals("")){
                isAnswerConatin = 3;
                ansPrefixValue  = questionObj.getLeftAns();
                ansPostFixValue = questionObj.getRightAns();
                ansBox.setText(questionObj.getLeftAns() + " " + questionObj.getRightAns());
            }else if(!questionObj.getLeftAns().equals("")){
                isAnswerConatin = 1;
                ansPrefixValue  = questionObj.getLeftAns();
                ansBox.setText(questionObj.getLeftAns());
            }else if(!questionObj.getRightAns().equals("")){
                isAnswerConatin = 2;
                ansPostFixValue = questionObj.getRightAns();
                ansBox.setText(questionObj.getRightAns());
            }else{
                isAnswerConatin = 0;
                ansBox.setText("");
            }
            ansBox.setBackgroundResource(+ R.layout.edittext);
        }
        questionLayout.addView(child);
        scrollView.smoothScrollTo(0, 0);
    }


    /**
     * This method set the question in other language when user changes language
     */
    private void setQuestionAfterLanguageChange(WordProblemQuestionTransferObj questionObj){

        if(questionObj.getQuestion().contains(".png"))
        {
            this.setBackGroundImageForQuestionImage(txtQueImage, questionObj.getQuestion());
        }else{
            //for '' change into '
            txtQue.setText(Html.fromHtml(questionObj.getQuestion().replaceAll("''", "'")));
        }

        if(questionObj.getImage().length() > 0){
            this.setBackGroundImageForQuestionImage(imgQuestion, questionObj.getImage());
        }

        if(questionObj.getFillIn() != 1){
            for(int i = 0 ; i < originalIndexList.size() ; i ++ ){
                if(questionObj.getOptionList().get(originalIndexList.get(i)).contains(".png")){
                    this.setBackGroundImageForQuestionImage
                            (imgOptionList.get(i), questionObj.getOptionList().get(originalIndexList.get(i)));
                }else{
                    txtOptionList.get(i).setText(questionObj.getOptionList().get(originalIndexList.get(i)));
                }
            }
        }else{
            if(!questionObj.getLeftAns().equals("") && !questionObj.getRightAns().equals("")){
                ansPrefixValue  = questionObj.getLeftAns();
                ansPostFixValue = questionObj.getRightAns();
                ansBox.setText(questionObj.getLeftAns() + " " + answerValue + " "
                        + questionObj.getRightAns());
            }else if(!questionObj.getLeftAns().equals("")){
                ansPrefixValue  = questionObj.getLeftAns();
                ansBox.setText(questionObj.getLeftAns() + " " + answerValue);
            }else if(!questionObj.getRightAns().equals("")){
                ansPostFixValue = questionObj.getRightAns();
                ansBox.setText(answerValue + " " + questionObj.getRightAns());
            }else{
                ansBox.setText(answerValue);
            }
            //ansBox.setText(answerValue);
        }
    }

    /**
     * This method when user click on work area
     */
    protected void clickOnWorkArea(boolean isCallFromLearningCenter) {

        //for sound on work area
        isClickOnWorkArea = true;

        Intent intent = new Intent(this,WorkAreaForWordProblem.class);
        intent.putExtra("questionNo", numberOfQuestion);
        //intent.putExtra("workAreaData", questionObj);
        //changes for Spanish
        intent.putExtra("workAreaData", questionObjForWordArea);
        intent.putExtra("isFromHomeWork", isFromHomeWork);
        //end changes
        intent.putExtra("startTimeForTimer", startTimeForTimer);//for timer
        intent.putExtra("startTime", startTimerForRoughArea);// for work area countdawn timer
        intent.putExtra("isCallFromLearningCenter", isCallFromLearningCenter);
        startActivity(intent);
    }

    /**
     * This method when user click on work area
     */
    protected void clickOnWorkAreaForAssessmentTest() {

        //for sound on work area
        isClickOnWorkArea = true;

        Intent intent = new Intent(this,WorkAreaForWordProblem.class);
        intent.putExtra("questionNo", (numberOfQuestion + 1));
        //intent.putExtra("workAreaData", questionObj);
        //changes for Spanish
        intent.putExtra("workAreaData", questionObjForWordArea);
        //end changes
        intent.putExtra("isCallFromAssessmentTest", true);
        startActivity(intent);
    }

    /**
     * This method convert the string with comma into the integer array list with comm saperated
     * @param stringWithCommas
     * @return
     */
    protected ArrayList<Integer> getCommaSepratedValueFromString(String stringWithCommas){
        stringWithCommas = stringWithCommas.replaceAll(" ", "");
        ArrayList<Integer> listWithoutComma = new ArrayList<Integer>();

        for(int i = 0 ; i < stringWithCommas.length() ; i ++ ){
            if(stringWithCommas.charAt(i) != ','){
                try{
                    listWithoutComma.add(Integer.parseInt(stringWithCommas.charAt(i) +""));
                }
                catch(Exception e){
                    Log.e(TAG, " error while converting String to integer" + e.toString());
                }
            }
        }
        return listWithoutComma;
    }

    /**
     * This method set the progress
     */
    protected void setProgress(boolean isRightAnswer){
        ImageView imgProgressBar = new ImageView(this);
        if(isRightAnswer){
            if(isTab){
                imgProgressBar.setBackgroundResource(R.drawable.green_ipad);
            }
            else {
                imgProgressBar.setBackgroundResource(R.drawable.green);
            }
        }
        else{
            if(isTab){
                imgProgressBar.setBackgroundResource(R.drawable.red_ipad);
            }
            else{
                imgProgressBar.setBackgroundResource(R.drawable.red);
            }
        }
        progressBarLayout.addView(imgProgressBar);
    }


    /**
     * this method return the points for solve each equation
     * @return
     */
    protected int getPointsForSolvingEachEquation()
    {
        int startTimeValue = startTime.getSeconds();
        int endTimeValue   = endTime.getSeconds();

        int pointsForEachEquation = 0;
        if((endTimeValue - startTimeValue) < 0)
            endTimeValue = endTimeValue + 60;
        pointsForEachEquation = (MAX_POINTS -  (5 * (endTimeValue - startTimeValue)));
        if(pointsForEachEquation < 200)
            return 200;
        return pointsForEachEquation;
    }

    /**
     * This method set
     * go button for clickable
     */
    protected void setClickableGoButton(boolean clickable){
        btnGoForFillIn.setClickable(clickable);
        btnGoForWithoutFillIn.setClickable(clickable);
    }

    /**
     * This method convert the data for solving the equation into xml format
     * when Internet not connected
     */
    protected String getEquationSolveXmlForInterNetNotConnected(ArrayList<MathEquationTransferObj> playDatalist)
    {
        StringBuilder xml = new StringBuilder("");
        for( int i = 0 ;  i < playDatalist.size() ; i++)
        {
            xml.append("<equation>" +
                    "<equationId>"+playDatalist.get(i).getEqautionId()+"</equationId>"+
                    "<start_date_time>"+playDatalist.get(i).getStartDate()+"</start_date_time>"+
                    "<end_date_time>"+playDatalist.get(i).getEndData()+"</end_date_time>"+
                    "<category_id>"+playDatalist.get(i).getCatId()+"</category_id>"+
                    "<subCategory_id>"+playDatalist.get(i).getSubCatId()+"</subCategory_id>"+
                    "<points>" + playDatalist.get(i).getPoints() + "</points>"+
                    "<isAnswerCorrect>"+playDatalist.get(i).getIsAnswerCorrect()+"</isAnswerCorrect>"+
                    "<user_answer>"+playDatalist.get(i).getUserAnswer()+"</user_answer>"+
                    "</equation>");
        }
        return xml.toString();
    }

    /**
     * This method convert the data for solving the equation into xml format
     * when Internet not connected
     */
    protected String getEquationSolveXmlForInterNetNotConnectedForAssessment
    (ArrayList<MathEquationTransferObj> playDatalist)
    {
        StringBuilder xml = new StringBuilder("");
        for( int i = 0 ;  i < playDatalist.size() ; i++)
        {
            if(playDatalist.get(i).isQuestionAttempt()){
                xml.append("<equation>" +
                        "<equationId>"+playDatalist.get(i).getEqautionId()+"</equationId>"+
                        "<start_date_time>"+playDatalist.get(i).getStartDate()+"</start_date_time>"+
                        "<end_date_time>"+playDatalist.get(i).getEndData()+"</end_date_time>"+
                        "<category_id>"+playDatalist.get(i).getCatId()+"</category_id>"+
                        "<subCategory_id>"+playDatalist.get(i).getSubCatId()+"</subCategory_id>"+
                        "<points>" + playDatalist.get(i).getPoints() + "</points>"+
                        "<isAnswerCorrect>"+playDatalist.get(i).getIsAnswerCorrect()+"</isAnswerCorrect>"+
                        "<user_answer>"+playDatalist.get(i).getUserAnswer()+"</user_answer>"+
                        "</equation>");
            }else{
                break;
            }
        }
        return xml.toString();
    }

    /**
     * This method convert the data for solving the equation into xml format
     */
    protected String getEquationSolveXml(ArrayList<MathEquationTransferObj> playDatalist)
    {
        StringBuilder xml = new StringBuilder("");
        for( int i = 0 ;  i < playDatalist.size() ; i++)
        {
            xml.append("<equation>" +
                    "<equationId>"+playDatalist.get(i).getEqautionId()+"</equationId>"+
                    "<start_date_time>"+playDatalist.get(i).getStartDate()+"</start_date_time>"+
                    "<end_date_time>"+playDatalist.get(i).getEndData()+"</end_date_time>"+
                    "<time_taken_to_answer>"+playDatalist.get(i).getTimeTakenToAnswer()+"</time_taken_to_answer>"+
                    "<category_id>"+playDatalist.get(i).getCatId()+"</category_id>"+
                    "<subCategory_id>"+playDatalist.get(i).getSubCatId()+"</subCategory_id>"+
                    "<points>" + playDatalist.get(i).getPoints() + "</points>"+
                    "<isAnswerCorrect>"+playDatalist.get(i).getIsAnswerCorrect()+"</isAnswerCorrect>"+
                    "<user_answer>"+playDatalist.get(i).getUserAnswer()+"</user_answer>"+
                    "</equation>");
        }
        return xml.toString();
    }


    /**
     * This method compare the float value
     * @param userAns
     * @param databaseAns
     * @return
     */
    protected boolean compareFloatAns(String userAns , String databaseAns){

        try{
            float userFloatAns = Float.parseFloat(userAns.replace(" ", "").replace(",", ""));
            float dataFloatAns = Float.parseFloat(databaseAns.replace(" ", "").replace(",", ""));

            if(userFloatAns == dataFloatAns){
                //Log.e(TAG, "true in try");
                return true;
            }
            else{
                //Log.e(TAG, "false in try");
                return false;
            }
        }catch(Exception e){
            Log.e(TAG, "inside compareFloatAns Error in converting " + e.toString());
            return false;
        }
    }

    /**
     * This asyncTask Add points and coins on server
     * @author Yashwant Singh
     *
     */
    protected class AddCoinAndPointsForLoginUser extends AsyncTask<Void, Void, Void>
    {
        private PlayerTotalPointsObj playerObj = null;
        private Context context;

        public AddCoinAndPointsForLoginUser(PlayerTotalPointsObj playerObj , Context context)
        {
            this.playerObj = playerObj;
            this.context   = context;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            Register register = new Register(context);
            register.addPointsAndCoinsOnServerForloginUser(playerObj);
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            super.onPostExecute(result);

        }
    }

    //changes for assessment
    /**
     * This method set question answer layout
     */
    protected void setQuestionAnswerLayoutForAssessment(final WordProblemQuestionTransferObj questionObj){

        //changes for Spanish
        questionObjForWordArea = questionObj;
        //end changes

        startTime = new Date();
        questionLayout.removeAllViews();

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        LayoutInflater inflater = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
        RelativeLayout child;

        if(metrics.heightPixels <= TAB_HEIGHT && metrics.widthPixels >= TAB_WIDTH && metrics.densityDpi
                == TAB_DENISITY)
        {
            child = (RelativeLayout) inflater.inflate
                    (R.layout.schoolcurriculum_equation_solve_layout_for_fill_in_for_low_denisity, null);
        }
        else{
            child = (RelativeLayout) inflater.inflate
                    (R.layout.schoolcurriculum_equation_solve_layout_for_fill_in, null);
        }

        queLayout = (RelativeLayout) child.findViewById(R.id.queLayout);
        txtQue    = (TextView)       child.findViewById(R.id.txtQue);
        txtQueImage = (ImageView)    child.findViewById(R.id.txtQueImage);
        txtQueNo  = (TextView)       child.findViewById(R.id.txtQueNo);
        imgQuestion = (ImageView)    child.findViewById(R.id.imgQuestion);
        //for tts
        imgTTS    = (ImageView)      child.findViewById(R.id.imgSpeach);

        //changes for spanish
        imgSpanish = (ImageView)     child.findViewById(R.id.imgSpanish);
        imgSpanish.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.e(TAG, "Click on language conversion");
                clickOnSpanish(questionObj , true);
            }
        });
        //end changes

        //for tts
        imgTTS.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if(!isSpanish){
                    textToSpeach(CommonUtils.SPANISH, questionObj.getQuestion() , questionObj.getQuestionId());
                }else{
                    textToSpeach(CommonUtils.ENGLISH, questionObj.getQuestion() , questionObj.getQuestionId());
                }
            }
        });
        //end changes

        multipleChicelayout = (LinearLayout) child.findViewById(R.id.multipleChicelayout);
        multipleChicelayout.removeAllViews();

        txtQueNo.setText((numberOfQuestion + 1) + "");

        if(questionObj.getQuestion().contains(".png"))
        {
            imgTTS.setVisibility(ImageView.INVISIBLE);
            txtQueImage.setVisibility(ImageView.VISIBLE);
            txtQue.setVisibility(TextView.GONE);
            this.setBackGroundImageForQuestionImage(txtQueImage, questionObj.getQuestion());
        }else{
            txtQueImage.setVisibility(ImageView.GONE);
            txtQue.setVisibility(TextView.VISIBLE);
            //for '' change into '
            if(questionObj.getQuestion().contains("&#"))
                txtQue.setText(Html.fromHtml(questionObj.getQuestion().replaceAll("''", "'")));
            else
                txtQue.setText(questionObj.getQuestion().replaceAll("''", "'"));
            //txtQue.setText(Html.fromHtml(questionObj.getQuestion().replaceAll("''", "'")));
        }

        if(questionObj.getImage().length() > 0){
            this.setBackGroundImageForQuestionImage(imgQuestion, questionObj.getImage());
        }else{
            imgQuestion.setVisibility(ImageView.GONE);
        }

        //changes for assessment test
        ArrayList<String> newSelectedTextList = new ArrayList<String>();
        //end changes

        if(questionObj.getFillIn() != 1){
            optionLayoutList    = new ArrayList<RelativeLayout>();
            optionNumberList    = new ArrayList<TextView>();
            selectedOptionValue = new ArrayList<String>();
            selectedOptionList  = new ArrayList<OptionSelectDataObj>();

            //changes for Spanish
            txtOptionList       = new ArrayList<TextView>();
            originalIndexList   = new ArrayList<Integer>();
            imgOptionList       = new ArrayList<ImageView>();
            //end changes

            //copying original optionlist into tempArray List
            ArrayList<String>  tempoperationlist = new ArrayList<String>(questionObj.getOptionList());
            //WordProblemQuestionTransferObj questionObjTemp = new WordProblemQuestionTransferObj();

            //set option layout data
            int numberOfOption = 0 ;

            while(tempoperationlist.size() > 0){
                int randomNumber = 	random.nextInt(tempoperationlist.size());

                if(tempoperationlist.get(randomNumber).length() > 0){//option which has value
                    numberOfOption ++ ;

                    LayoutInflater inflaterOption = (LayoutInflater)
                            getSystemService(this.LAYOUT_INFLATER_SERVICE);
                    RelativeLayout option = (RelativeLayout) inflater.inflate
                            (R.layout.schoo_curricullum_equation_solve_layout_for_multiplechoice, null);
                    RelativeLayout OptionLayout = (RelativeLayout) option.findViewById(R.id.OptionLayout);
                    TextView       txtoptionNumber = (TextView) option.findViewById(R.id.txtoptionNumber);
                    TextView       txtOption       = (TextView) option.findViewById(R.id.txtOption);
                    ImageView      imgOptionImage  = (ImageView)option.findViewById(R.id.imgOptionImage);

                    //changes for Spanish
                    txtOptionList.add(txtOption);
                    imgOptionList.add(imgOptionImage);
                    for(int i = 0 ; i < questionObj.getOptionList().size() ; i ++ ){
                        if(questionObj.getOptionList().get(i).
                                equals(tempoperationlist.get(randomNumber)))
                            originalIndexList.add(i);

                    }
                    //end changes

                    if(isTab)
                        OptionLayout.setBackgroundResource(getResources().getIdentifier
                                ("option" + numberOfOption + "_ipad", "drawable",getPackageName()));
                    else
                        OptionLayout.setBackgroundResource(getResources().getIdentifier
                                ("option" + numberOfOption + "", "drawable",getPackageName()));

                    txtoptionNumber.setText(alphabet.charAt(numberOfOption - 1) + "");

                    if(tempoperationlist.get(randomNumber).contains(".png")){
                        imgOptionImage.setVisibility(ImageView.VISIBLE);
                        txtOption.setVisibility(TextView.GONE);

                        this.setBackGroundImageForQuestionImage
                                (imgOptionImage, tempoperationlist.get(randomNumber));
                    }
                    else{
                        imgOptionImage.setVisibility(ImageView.GONE);
                        txtOption.setVisibility(TextView.VISIBLE);

                        //changes for Html or '' to '
                        if(tempoperationlist.get(randomNumber).contains("&#"))
                            txtOption.setText(Html.fromHtml(tempoperationlist.get(randomNumber).replaceAll("''", "'")));
                        else
                            txtOption.setText(tempoperationlist.get(randomNumber).replaceAll("''", "'"));

                        //txtOption.setText(Html.fromHtml(tempoperationlist.get(randomNumber).replaceAll("''", "'")));
                    }

                    //changes for assessment
                    newSelectedTextList.add(tempoperationlist.get(randomNumber));
                    //end changes

                    OptionLayout.setOnClickListener(new OnClickListener()
                    {
                        @Override
                        public void onClick(View v) {
                            for(int i = 0 ; i < optionLayoutList.size() ; i ++ ){
                                if(v == optionLayoutList.get(i)){

                                    int selectedindex = isOptionAlreadySelected(optionLayoutList.get(i),
                                            selectedOptionList);

                                    if(selectedindex != -1){
                                        selectedOptionList.get(selectedindex).getTxtSelectedtext().setText
                                                (selectedOptionList.get(selectedindex).getTextNumber());

                                        if(isTab){
                                            selectedOptionList.get(selectedindex).getOpationSeletedLayout()
                                                    .setBackgroundResource(getResources().getIdentifier
                                                            ("option" + (selectedOptionList.get(selectedindex).
                                                                    getSelectedOptionNumber())
                                                                    + "_ipad", "drawable",getPackageName()));
                                        }else{
                                            selectedOptionList.get(selectedindex).getOpationSeletedLayout()
                                                    .setBackgroundResource(getResources().getIdentifier
                                                            ("option" + (selectedOptionList.get(selectedindex).
                                                                            getSelectedOptionNumber()) + "",
                                                                    "drawable",getPackageName()));
                                        }
                                        selectedOptionList.remove(selectedindex);
                                    }
                                    else{
                                        OptionSelectDataObj sectedoptionData = new OptionSelectDataObj();
                                        sectedoptionData.setTextNumber(optionNumberList.get(i).getText()
                                                + "");
                                        sectedoptionData.setOpationSeletedLayout(optionLayoutList.get(i));
                                        sectedoptionData.setSelectedOptionNumber(i + 1);
                                        sectedoptionData.setTxtSelectedtext(optionNumberList.get(i));
                                        sectedoptionData.setSelectedOptionTextValue(selectedOptionValue.get(i));
                                        selectedOptionList.add(sectedoptionData);

                                        //set text as blank text
                                        optionNumberList.get(i).setText("");
                                        if(isTab)
                                            optionLayoutList.get(i).setBackgroundResource(getResources()
                                                    .getIdentifier
                                                            ("optionselect" + (i + 1) + "_ipad", "drawable",
                                                                    getPackageName()));
                                        else
                                            optionLayoutList.get(i).setBackgroundResource(getResources()
                                                    .getIdentifier
                                                            ("optionselect" + (i + 1) + "", "drawable",
                                                                    getPackageName()));
                                    }
                                }
                            }
                        }
                    });

                    selectedOptionValue.add(tempoperationlist.get(randomNumber));
                    optionNumberList.add(txtoptionNumber);
                    optionLayoutList.add(OptionLayout);
                    multipleChicelayout.addView(option);
                }
                tempoperationlist.remove(randomNumber);
            }
        }
        else{
            if(!questionObj.getLeftAns().equals("") && !questionObj.getRightAns().equals("")){
                isAnswerConatin = 3;
                ansPrefixValue  = questionObj.getLeftAns();
                ansPostFixValue = questionObj.getRightAns();
                ansBox.setText(questionObj.getLeftAns() + " " + questionObj.getRightAns());
            }else if(!questionObj.getLeftAns().equals("")){
                isAnswerConatin = 1;
                ansPrefixValue  = questionObj.getLeftAns();
                ansBox.setText(questionObj.getLeftAns());
            }else if(!questionObj.getRightAns().equals("")){
                isAnswerConatin = 2;
                ansPostFixValue = questionObj.getRightAns();
                ansBox.setText(questionObj.getRightAns());
            }else{
                isAnswerConatin = 0;
                ansBox.setText("");
            }
            ansBox.setBackgroundResource(+ R.layout.edittext);
        }
        questionLayout.addView(child);

        //check for previous question , set the question answer for already played questions
        if(playDataList.get(numberOfQuestion).isQuestionAttempt()){
            if(questionObj.getFillIn() != 1){

                ArrayList<Integer> userSelectedOption =
                        this.getCommaSepratedValueFromString(playDataList.get(numberOfQuestion)
                                .getUserAnserForAssessment());

                for(int i = 0 ; i < newSelectedTextList.size() ; i ++ ){
                    for(int j = 0 ; j < questionObj.getOptionList().size() ; j ++ ){
                        if(newSelectedTextList.get(i).equals(questionObj.getOptionList().get(j))){
                            for(int k = 0 ; k < userSelectedOption.size() ; k ++ ){
                                if((j + 1) == userSelectedOption.get(k)){

                                    OptionSelectDataObj sectedoptionData = new OptionSelectDataObj();
                                    sectedoptionData.setTextNumber(optionNumberList.get(i).getText()
                                            + "");
                                    sectedoptionData.setOpationSeletedLayout(optionLayoutList.get(i));
                                    sectedoptionData.setSelectedOptionNumber(i + 1);
                                    sectedoptionData.setTxtSelectedtext(optionNumberList.get(i));
                                    sectedoptionData.setSelectedOptionTextValue(selectedOptionValue.get(i));
                                    selectedOptionList.add(sectedoptionData);

                                    //set text as blank text
                                    optionNumberList.get(i).setText("");
                                    if(isTab)
                                        optionLayoutList.get(i)
                                                .setBackgroundResource(getResources()
                                                        .getIdentifier
                                                                ("optionselect" + (i + 1)
                                                                                + "_ipad",
                                                                        "drawable",
                                                                        getPackageName()));
                                    else
                                        optionLayoutList.get(i).setBackgroundResource(getResources()
                                                .getIdentifier
                                                        ("optionselect" + (i + 1)
                                                                        + "", "drawable",
                                                                getPackageName()));

                                }
                            }
                        }
                    }
                }
            }else{
                //changes for assessment test answer already selected
                answerValue.append(playDataList.get(numberOfQuestion).getUserAnserForAssessment());
                ansBox.setText(playDataList.get(numberOfQuestion).getUserAnserForAssessment());
            }
        }
        //end changes

        scrollView.smoothScrollTo(0, 0);
    }

    /**
     * This class dawnload image from server for schoolcurriculum
     * @author Yashwant Singh
     *
     */
    protected class DawnloadImageForSchooCurriculumForAssessment extends AsyncTask<Void, Void, Void>{

        private ArrayList<String> imageNamelist;
        private byte [] imageFromServer;
        private Context context;
        SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl;
        LearnignCenterSchoolCurriculumServerOperation learningCenterServerobj;
        private WordProblemQuestionTransferObj questionObj;

        private ProgressDialog pd = null;

        public DawnloadImageForSchooCurriculumForAssessment(ArrayList<String> imageNamelist , Context context,
                                                            WordProblemQuestionTransferObj questionObj){
            this.imageNamelist 	= imageNamelist;
            this.context   		= context;
            this.questionObj    = questionObj;
        }

        @Override
        protected void onPreExecute() {
            pd = CommonUtils.getProgressDialog(context);
            pd.show();
            schooloCurriculumImpl   = new SchoolCurriculumLearnignCenterimpl(context);
            learningCenterServerobj = new LearnignCenterSchoolCurriculumServerOperation();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            for(int i = 0 ; i < imageNamelist.size() ; i ++ ){
                schooloCurriculumImpl.openConnection();
                if(!schooloCurriculumImpl.isImageAlreadyDawnloaded(imageNamelist.get(i))){
                    imageFromServer = learningCenterServerobj.dawabloadImageFromServer(imageNamelist.
                            get(i));
                    schooloCurriculumImpl.insertIntoSchoolCurriculumImageTable(imageNamelist.get(i),
                            imageFromServer);
                }
                schooloCurriculumImpl.closeConnection();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            pd.cancel();
            //setQuestionAfterLanguageChange(questionObj);
            setQuestionAnswerLayoutForAssessment(questionObj);
            super.onPostExecute(result);
        }
    }

    //added for email send for wrong question
    protected void sendEmailFroWrongQuestion(int questionId , Context context ,
                                             com.mathfriendzy.controller.timer.MyTimerInrerface myTimer){
        //Log.e(TAG, "Click");
        DialogGenerator dg = new DialogGenerator(context);
        dg.generateDialogForSendEmailForWrongQuestion(questionId , myTimer);
    }

    //changes for Spanish
    /**
     * This method inflate the layout for show question and answer
     * @param questionObj
     */
    private void setLayoutForShowQuetionAndAnswerForSpanishForAssessment(WordProblemQuestionTransferObj
                                                                                 questionObj){
        ArrayList<String> imageNameList        = this.getImageArrayList(questionObj);
        ArrayList<String> unAvailableImageList = this.checkImageAvalability(imageNameList);

        if(unAvailableImageList.size() > 0){
            if(CommonUtils.isInternetConnectionAvailable(this)){
				/*new DawnloadImageForSchooCurriculumForAssessmentForQuestionChanges
				(unAvailableImageList, this, questionObj)
				.execute(null,null,null);*/
                new DawnloadImageForSchooCurriculumForLanguageChange
                        (unAvailableImageList, this, questionObj , true)
                        .execute(null,null,null);
            }else{
                //this.setQuestionAnswerLayoutForAssessment(questionObj);
                this.setQuestionAfterLanguageChange(questionObj);
            }
        }
        else{
            //this.setQuestionAnswerLayoutForAssessment(questionObj);
            this.setQuestionAfterLanguageChange(questionObj);
        }
    }

    /**
     * This method inflate the layout for show question and answer
     * @param questionObj
     */
    private void setLayoutForShowQuetionAndAnswerForSpanish(WordProblemQuestionTransferObj questionObj){
        ArrayList<String> imageNameList        = this.getImageArrayList(questionObj);
        ArrayList<String> unAvailableImageList = this.checkImageAvalability(imageNameList);

        if(unAvailableImageList.size() > 0){
            if(CommonUtils.isInternetConnectionAvailable(this)){
                //changes for timer when dawanload images from server
                if(isCountDawnTimer){
                    if(timer != null){
                        timer.cancel();
                        timer = new MyTimer(startTimerForRoughArea, 1 * 1000);//second argument is the interval
                    }
                }else{
                    if(customHandler != null)
                        customHandler.removeCallbacks(updateTimerThread);
                }
                //end changes

				/*new DawnloadImageForSchooCurriculumForLanguageChange
				(unAvailableImageList, this, questionObj)
				.execute(null,null,null);*/
                new DawnloadImageForSchooCurriculumForLanguageChange
                        (unAvailableImageList, this, questionObj , false)
                        .execute(null,null,null);
            }else{
                //this.setQuestionAnswerLayout(questionObj);
                this.setQuestionAfterLanguageChange(questionObj);
            }
        }
        else{
            //this.setQuestionAnswerLayout(questionObj);
            this.setQuestionAfterLanguageChange(questionObj);
        }
    }

    /**
     * This method call when user click on button to convert language
     * @param questionObj
     */
    private void clickOnSpanish(WordProblemQuestionTransferObj questionObj , boolean isAssessment){

        //Log.e(TAG , "selectedPlayGrade" + selectedPlayGrade);

        //for question translation , Issues list 5 march 2014 , issues no. 5
        final int questionId = questionObj.getQuestionId();

        /**
         * This method call after question downloaded in the translated
         */
        showQuestionAfterTranslation =	new ShowTranslateQuestionAfterDownloading() {
            @Override
            public void showQuestionAfterTranslationDownload(
                    WordProblemQuestionTransferObj questionObj, boolean isAssessment) {

                if(isSpanish){
                    SpanishChangesImpl schoolImpl = new SpanishChangesImpl
                            (SchoolCurriculumEquationSolveBase.this);
                    schoolImpl.openConn();
                    questionObj  = schoolImpl.getQuestionByQuestionId(questionId);
                    schoolImpl.closeConn();
                }else{
                    SchoolCurriculumLearnignCenterimpl schoolImpl =
                            new SchoolCurriculumLearnignCenterimpl
                            (SchoolCurriculumEquationSolveBase.this);
                    schoolImpl.openConnection();
                    questionObj  = schoolImpl.getQuestionByQuestionId(questionId);
                    schoolImpl.closeConnection();
                }
                showLanguageChangeQuestion(questionObj, isAssessment);
            }
        };
        //end changes

        if(isSpanish){
            SpanishChangesImpl schoolImpl = new SpanishChangesImpl(this);
            schoolImpl.openConn();
            boolean isQuestionLoaded = schoolImpl.isQuestionLeaded(selectedPlayGrade);

            if(isQuestionLoaded){
                //code commented based on the requirement 24 11 2014 , point #6
                //commented if block for not downloading date for question is loaded
				/*if(CommonUtils.isInternetConnectionAvailable(this)
						&& CommonUtils.isOneTimeClick){
					new GetWordProblemQuestionUpdatedDate(selectedPlayGrade , false
							,CommonUtils.SPANISH , isAssessment).execute(null,null,null);
				}else{*/
                //get question from database into questionObj object
                questionObj  = schoolImpl.getQuestionByQuestionId(questionObj.getQuestionId());
                this.showLanguageChangeQuestion(questionObj, isAssessment);
                //}
            }
            else{
                if(CommonUtils.isOneTimeClick){
                    this.showPopForDownLoadingQuestion(CommonUtils.SPANISH , selectedPlayGrade
                            ,isAssessment);
                }else{
                    Log.e(TAG, "Nothing Happen");
                }
            }
            schoolImpl.closeConn();

        }else {//for English

            SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
            schoolImpl.openConnection();
            boolean isQuestionLoaded = schoolImpl.isQuestionLeaded(selectedPlayGrade);

            if(isQuestionLoaded){
                //code commented based on the requirement 24 11 2014 , point #6
                //commented if block for not downloading date for question is loaded
				
				/*if(CommonUtils.isInternetConnectionAvailable(this)
						&& CommonUtils.isOneTimeClick){
					new GetWordProblemQuestionUpdatedDate(selectedPlayGrade , false
							,CommonUtils.ENGLISH , isAssessment).execute(null,null,null);
				}else{*/
                //get question from database into questionObj object
                questionObj  = schoolImpl.getQuestionByQuestionId(questionObj.getQuestionId());
                this.showLanguageChangeQuestion(questionObj, isAssessment);
                //}
            }
            else{
                if(CommonUtils.isOneTimeClick){
                    this.showPopForDownLoadingQuestion(CommonUtils.ENGLISH , selectedPlayGrade
                            ,isAssessment);
                }else{
                    Log.e(TAG, "Nothing Happen");
                }
            }
            schoolImpl.closeConnection();
        }
    }
    //end changes

    /**
     * This method show the new questions in diff language
     * @param questionObj
     * @param isAssessment
     */
    private void showLanguageChangeQuestion(WordProblemQuestionTransferObj questionObj ,
                                            boolean isAssessment){
        isSpanish = !isSpanish;//changes after discussing
        //this.questionObj = questionObj;//changes in original object
        questionObjForWordArea = questionObj;
        if(isAssessment)
            this.setLayoutForShowQuetionAndAnswerForSpanishForAssessment(questionObj);
        else
            this.setLayoutForShowQuetionAndAnswerForSpanish(questionObj);
        //this.setQuestionAfterLanguageChange(questionObj);
    }

    /**
     * This method call when user click on language change and question is not in database
     */
    private void showPopForDownLoadingQuestion(int langCode , int grade , boolean isAssessment){

        if(!isAssessment){
            //changes for timer when dawanload images from server
            if(isCountDawnTimer){
                if(timer != null){
                    timer.cancel();
                    timer = new MyTimer(startTimerForRoughArea, 1 * 1000);//second argument is the interval
                }
            }else{
                if(customHandler != null)
                    customHandler.removeCallbacks(updateTimerThread);
            }
            //end changes
        }

        DialogGenerator dg = new DialogGenerator(this);
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        dg.generateDialogSpanishChagesQuestionDownload(transeletion.
                getTranselationTextByTextIdentifier("lblTappingThisFriendzyGlobe")
                + " Spanish " +
                transeletion.getTranselationTextByTextIdentifier("lblTextWhilePlayingWordProblems")
                + " Spanish " +
                transeletion.getTranselationTextByTextIdentifier("lblWordProblemsForYourGradeToUse")
                , langCode , grade , isAssessment , isCountDawnTimer , timer , customHandler
                , startTimeForTimer	,updateTimerThread , showQuestionAfterTranslation);
        transeletion.closeConnection();
    }

    //changes for Spanish
    //for downloading questions
    /**
     * This class get word problem updated dates from server
     * @author Yashwant Singh
     *
     */
    private class GetWordProblemQuestionUpdatedDate extends AsyncTask<Void, Void, ArrayList<UpdatedInfoTransferObj>>{

        private int grade;
        private ProgressDialog pd;
        private boolean isFisrtTimeCallForGrade;
        private int langCode = 1;
        private boolean isAssessment;
        GetWordProblemQuestionUpdatedDate(int grade , boolean isFisrtTimeCallForGrade
                ,int langCode , boolean isAssessment){
            this.grade = grade;
            this.isFisrtTimeCallForGrade = isFisrtTimeCallForGrade;
            this.langCode = langCode;
            this.isAssessment = isAssessment;
        }

        @Override
        protected void onPreExecute() {
            if(isFisrtTimeCallForGrade == false){
                pd = CommonUtils.getProgressDialog(SchoolCurriculumEquationSolveBase.this);
                pd.show();
            }
            super.onPreExecute();
        }

        @Override
        protected ArrayList<UpdatedInfoTransferObj>  doInBackground(Void... params) {
            SingleFriendzyServerOperationForWordProblem serverObj =
                    new SingleFriendzyServerOperationForWordProblem();
            //changes for Spanish
            ArrayList<UpdatedInfoTransferObj>  updatedList = new ArrayList<UpdatedInfoTransferObj>();
            if(langCode == CommonUtils.ENGLISH)
                updatedList = serverObj.getWordProbleQuestionUpdatedData();
            else//for Spanish
                updatedList = serverObj.getWordProbleQuestionUpdatedDateForSpanish(CommonUtils.SPANISH);
            return updatedList;
        }

        @Override
        protected void onPostExecute(ArrayList<UpdatedInfoTransferObj>  updatedList) {
            if(isFisrtTimeCallForGrade == false){
                pd.cancel();
            }

            if(updatedList != null){
                SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl =
                        new SchoolCurriculumLearnignCenterimpl(SchoolCurriculumEquationSolveBase.this);
                schooloCurriculumImpl.openConnection();

                //changes for Spanish
                SpanishChangesImpl implObj = new SpanishChangesImpl(SchoolCurriculumEquationSolveBase.this);
                implObj.openConn();

                if(langCode	== CommonUtils.ENGLISH){

                    if(!schooloCurriculumImpl.isWordProblemUpdateDetailsTableDataExist())
                        schooloCurriculumImpl.insertIntoWordProblemUpdateDetails(updatedList);

                    String updatedDateFromDatabase = schooloCurriculumImpl.getUpdatedDate(grade);
                    if(CommonUtils.isUpdateDateChange(updatedDateFromDatabase , grade , updatedList))
                    {
                        String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
                        schooloCurriculumImpl.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
                        schooloCurriculumImpl.closeConnection();
                        implObj.closeConn();
                        //changes
                        showPopForDownLoadingQuestion(CommonUtils.ENGLISH , grade , isAssessment);
                    }
                    else{
                        CommonUtils.isOneTimeClick = false;
                        schooloCurriculumImpl.closeConnection();
                        implObj.closeConn();
                        if(showQuestionAfterTranslation != null)
                            showQuestionAfterTranslation.showQuestionAfterTranslationDownload(null, isAssessment);
                    }

                }else{//for Spanish

                    if(!implObj.isWordProblemUpdateDetailsTableDataExist())
                        implObj.insertIntoWordProblemUpdateDetails(updatedList);

                    String updatedDateFromDatabase = implObj.getUpdatedDate(grade);
                    if(CommonUtils.isUpdateDateChange(updatedDateFromDatabase , grade , updatedList))
                    {
                        String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
                        implObj.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
                        schooloCurriculumImpl.closeConnection();
                        implObj.closeConn();

                        //changes
                        showPopForDownLoadingQuestion(CommonUtils.SPANISH , grade , isAssessment);
                    }
                    else{
                        CommonUtils.isOneTimeClick = false;
                        schooloCurriculumImpl.closeConnection();
                        implObj.closeConn();

                        if(showQuestionAfterTranslation != null)
                            showQuestionAfterTranslation.showQuestionAfterTranslationDownload(null, isAssessment);
                    }
                }
            }else{
                if(isFisrtTimeCallForGrade == false){
                    CommonUtils.showInternetDialog(SchoolCurriculumEquationSolveBase.this);
                }
            }
            super.onPostExecute(updatedList);
        }
    }

    //for language Changes
    /**
     * This class dawnload image from server for schoolcurriculum
     * @author Yashwant Singh
     *
     */
    protected class DawnloadImageForSchooCurriculumForLanguageChange extends AsyncTask<Void, Void, Void>{

        private ArrayList<String> imageNamelist;
        private byte [] imageFromServer;
        private Context context;
        SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl;
        LearnignCenterSchoolCurriculumServerOperation learningCenterServerobj;
        private WordProblemQuestionTransferObj questionObj;

        private ProgressDialog pd = null;

        private Date dawnloadStartTime = null;

        private boolean isAssessment   = false;

        public DawnloadImageForSchooCurriculumForLanguageChange(ArrayList<String> imageNamelist ,
                                                                Context context,
                                                                WordProblemQuestionTransferObj questionObj , boolean isAssessment){
            this.imageNamelist 	= imageNamelist;
            this.context   		= context;
            this.questionObj    = questionObj;
            this.isAssessment   = isAssessment;
            dawnloadStartTime   = new Date();
        }

        @Override
        protected void onPreExecute() {
            pd = CommonUtils.getProgressDialog(context);
            pd.show();
            schooloCurriculumImpl   = new SchoolCurriculumLearnignCenterimpl(context);
            learningCenterServerobj = new LearnignCenterSchoolCurriculumServerOperation();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            for(int i = 0 ; i < imageNamelist.size() ; i ++ ){
                schooloCurriculumImpl.openConnection();
                if(!schooloCurriculumImpl.isImageAlreadyDawnloaded(imageNamelist.get(i))){
                    imageFromServer = learningCenterServerobj.dawabloadImageFromServer(imageNamelist.
                            get(i));
                    schooloCurriculumImpl.insertIntoSchoolCurriculumImageTable(imageNamelist.get(i),
                            imageFromServer);
                }
                schooloCurriculumImpl.closeConnection();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            pd.cancel();

            if(!isAssessment){
                if(isCountDawnTimer){
                    if(timer != null)
                        timer.start();
                }else{
                    if(customHandler != null){
                        Date dawnloadEndTime = new Date();
                        startTimeForTimer = startTimeForTimer + (dawnloadEndTime.getTime() - dawnloadStartTime.getTime());
                        customHandler.postDelayed(updateTimerThread, 0);
                    }
                }
            }
            setQuestionAfterLanguageChange(questionObj);
            //setQuestionAnswerLayout(questionObj);
            super.onPostExecute(result);
        }
    }

    //changes for New Assessment
    protected String getJsonStringFromMap(HashMap<Integer, ArrayList<Integer>> subStandardListWithQuestionIds){
        int counter = 0;
        StringBuilder jsonString = new StringBuilder("{\"data\" : [");
        for(int subStandardIds : subStandardListWithQuestionIds.keySet()){
            if(counter == 0)
                jsonString.append("{\"subStdId\" :\"" + subStandardIds + "\"" + "," + "\"questionIds\" :\"" + this.getStringWithCommasFromArrayList(subStandardListWithQuestionIds.get(subStandardIds)) + "\"}");
            else
                jsonString.append("," + "{\"subStdId\" :\"" + subStandardIds + "\"" + "," + "\"questionIds\" :\"" + this.getStringWithCommasFromArrayList(subStandardListWithQuestionIds.get(subStandardIds)) + "\"}");
            counter ++ ;
        }
        jsonString.append("]}");
        return jsonString.toString();
    }

    /**
     * This method return the question to play
     * @return
     */
    protected String getStringWithCommasFromArrayList(ArrayList<Integer> arrayList){
        String remainingQuestion = "";
        for(int i = 0 ; i < arrayList.size() ; i ++ ){
            remainingQuestion = remainingQuestion + arrayList.get(i) + ",";
        }
        return remainingQuestion;
    }

    @SuppressLint("UseSparseArrays")
    protected HashMap<Integer, ArrayList<Integer>> getSubStandardsQuestionIds(String jsonString){
        HashMap<Integer, ArrayList<Integer>> subStandardListWithQuestionIds
                = new HashMap<Integer, ArrayList<Integer>>();

        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            JSONArray jsonDataArray = jsonObj.getJSONArray("data");

            for(int i = 0 ; i < jsonDataArray.length() ; i ++ ){
                JSONObject jsonDataObj = jsonDataArray.getJSONObject(i);
                subStandardListWithQuestionIds.put(jsonDataObj.getInt("subStdId"),
                        this.getCommSepratedListFromString(jsonDataObj.getString("questionIds")));
            }
        } catch (JSONException e) {
            Log.e(TAG, "inside getSubStandardsQuestionIds Error while parsing josn " + e.toString());
        }
        return subStandardListWithQuestionIds;
    }

    /**
     * This method return the ArrayList from string with commas
     * @param valueString , contain , at the last
     * @return
     */
    protected ArrayList<Integer> getCommSepratedListFromString(String valueString){

        ArrayList<Integer> questionIdList = new ArrayList<Integer>();
        String questionIds = "";
        for(int i = 0 ; i < valueString.length() ; i ++ ){
            if(valueString.charAt(i) == ',' ){
                questionIdList.add(Integer.parseInt(questionIds));
                questionIds = "";
            }
            else{
                questionIds = questionIds +  valueString.charAt(i);
            }
        }

        return questionIdList;
    }
    /**
     * This method return the time and score xml string for saving on server
     * @param scoreAndTimeForSubStandsrs
     * @return
     */
    protected String getXmlForSubStandardsTimeAndScore(HashMap<Integer, NewAssessmentTestScore>
                                                               scoreAndTimeForSubStandsrs){
        StringBuilder xml = new StringBuilder("");
        for(int subStdId : scoreAndTimeForSubStandsrs.keySet()){
            xml.append("<subStdScore>" +
                    "<id>" + subStdId +"</id>" +
                    "<score>" + scoreAndTimeForSubStandsrs.get(subStdId).getTotalScoce() + "</score>" +
                    "<time>"  + scoreAndTimeForSubStandsrs.get(subStdId).getTotalTime()  + "</time>" +
                    "</subStdScore>");
        }
        return xml.toString();
    }

    @Override
    public void cancelCurrentTimerAndSetTheResumeTime() {

        //changes for timer when dawanload images from server
        if(isCountDawnTimer){
            if(timer != null){
                timer.cancel();
                timer = new MyTimer(startTimerForRoughArea, 1 * 1000);//second argument is the interval
            }
        }else{

            if(customHandler != null){
                resumeTimeForTimer = new Date();
                customHandler.removeCallbacks(updateTimerThread);
            }
        }
        //end changes
    }

    @Override
    public void resumeTimerFromTimeSetAtCancelTime() {

        if(isCountDawnTimer){
            if(timer != null)
                timer.start();
        }else{
            if(customHandler != null){
                Date dawnloadEndTime = new Date();
                startTimeForTimer = startTimeForTimer + (dawnloadEndTime.getTime() - resumeTimeForTimer.getTime());
                customHandler.postDelayed(updateTimerThread, 0);
            }
        }
    }

    /**
     * this method speach text in spanish and english
     * @param languageCode
     * @param text
     * @param questionId
     */
    @SuppressWarnings("deprecation")
    private void textToSpeach(int languageCode , String text, int questionId){
        //Log.e(TAG, "inside TTS");

        if(!isClickOnTTS){
            //Log.e(TAG, "inside TTS if");
            try{
                isClickOnTTS = true;

                WordProblemQuestionTransferObj questionObj = null;

                if(languageCode == CommonUtils.SPANISH){
                    SpanishChangesImpl schoolImpl = new SpanishChangesImpl
                            (SchoolCurriculumEquationSolveBase.this);
                    schoolImpl.openConn();
                    questionObj  = schoolImpl.getQuestionByQuestionId(questionId);
                    schoolImpl.closeConn();
                }else{
                    SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl
                            (SchoolCurriculumEquationSolveBase.this);
                    schoolImpl.openConnection();
                    questionObj  = schoolImpl.getQuestionByQuestionId(questionId);
                    schoolImpl.closeConnection();
                }

                if(tts != null){
                    Locale locale = null;
                    if(languageCode == CommonUtils.ENGLISH)
                        locale = Locale.ENGLISH;
                    else if(languageCode == CommonUtils.SPANISH){
                        //locale = new Locale("es", "ES");
                        locale = new Locale("es", "mx");
                    }
                    tts.setLanguage(locale);

                    //Log.e(TAG, "text " + questionObj.getQuestion() + " code " + languageCode);

                    HashMap<String, String> myHashAlarm = new HashMap<String, String>();
                    myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "SOME MESSAGE");

                    tts.speak(questionObj.getQuestion(), TextToSpeech.QUEUE_FLUSH, myHashAlarm);
                    tts.setOnUtteranceCompletedListener(new OnUtteranceCompletedListener() {
                        @Override
                        public void onUtteranceCompleted(String utteranceId) {
                            isClickOnTTS = false;
                            //Log.e(TAG, "Complete");
                        }
                    });
                }
            }catch(Exception e){
                Log.e(TAG, "Error while TTS " + e.toString());
            }
        }
    }

    @Override
    protected void onDestroy() {
        // Don't forget to shutdown tts!
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }
}
