package com.mathfriendzy.controller.learningcenter;

import static com.mathfriendzy.utils.ICommonUtils.LEARNING_CENTER_BG_INFO;
import static com.mathfriendzy.utils.ICommonUtils.SEE_ANSWER_FLAG;
import java.io.IOException;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.singlefriendzy.SingleFriendzyEquationSolve;
import com.mathfriendzy.controller.singlefriendzy.SingleFriendzyMain;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.SeeAnswerTransferObj;
import com.mathfriendzy.utils.CommonUtils;

public class SeeAnswerActivity extends ActBaseClass implements OnClickListener
{
	private TextView labelTop 			= null;
	private TextView txtGreatJobResult 	= null;
	private TextView txtName 			= null;
	private TextView txtCorrect 		= null;
	private TextView txtLevel 			= null;
	private TextView txtPoints 			= null;
	private TextView txtNameValues 		= null;
	private TextView txtCorrectValues 	= null;
	private TextView txtLevelValues 	= null;
	private TextView txtPointsValues 	= null;
	private TextView txtProblem 		= null;
	private TextView txtAnswers 		= null;
	private ListView listSolutions 		= null;
	private Button   btnPlay  			= null;
	private ImageView imgFlag      		= null;
	
	/*private RelativeLayout listSolutions 	= null;*/
	
	private SeeAnswerTransferObj seeAnswerDataObj = null;
	private int isCallFromActivity = 0;
	
	private final String TAG = this.getClass().getSimpleName();
	
	public static boolean isFromLearnignCenter = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_see_answer);
		
		if(SEE_ANSWER_FLAG)
			Log.e(TAG, "inside on create()");
		
		/*if(LearningCenterEquationSolveWithTimer.seeAnswerDataObj != null)
		{
			isCallFromActivity = 1;
			seeAnswerDataObj = LearningCenterEquationSolveWithTimer.seeAnswerDataObj;
		}
		else if(SingleFriendzyEquationSolve.seeAnswerDataObj != null)
		{
			isCallFromActivity = 2;
			seeAnswerDataObj = SingleFriendzyEquationSolve.seeAnswerDataObj;
		}*/
		
		if(isFromLearnignCenter)
		{
			isCallFromActivity = 1;
			seeAnswerDataObj = LearningCenterEquationSolveWithTimer.seeAnswerDataObj;
		}
		else 
		{
			isCallFromActivity = 2;
			seeAnswerDataObj = SingleFriendzyEquationSolve.seeAnswerDataObj;
		}
		
		this.setWidgetsReferences();
		this.setTranselationTextValues();
		this.setListenerOnWidgets();
		this.setUserPlayData();
		
		
		if(SEE_ANSWER_FLAG)
			Log.e(TAG, "outside on create()");
	}

	
	private void setWidgetsReferences() 
	{
		if(SEE_ANSWER_FLAG)
			Log.e(TAG, "inside on setWidgetsReferences()");
	
		labelTop 			= (TextView) findViewById(R.id.labelTop);
		txtGreatJobResult 	= (TextView) findViewById(R.id.txtGreatJobResult);
		txtName 			= (TextView) findViewById(R.id.txtName);
		txtCorrect 			= (TextView) findViewById(R.id.txtCorrect);
		txtLevel 			= (TextView) findViewById(R.id.txtLevel);
		txtPoints 			= (TextView) findViewById(R.id.txtPoints);
		txtNameValues 		= (TextView) findViewById(R.id.txtNameValues);
		txtCorrectValues 	= (TextView) findViewById(R.id.txtCorrectValues);
		txtLevelValues 		= (TextView) findViewById(R.id.txtLevelValues);
		txtPointsValues 	= (TextView) findViewById(R.id.txtPointsValues);
		txtProblem 			= (TextView) findViewById(R.id.txtProblem);
		txtAnswers 			= (TextView) findViewById(R.id.txtAnswers);
		
		listSolutions 		= (ListView) findViewById(R.id.listSolutions);
		btnPlay        		= (Button)  findViewById(R.id.btnPlay);
		imgFlag				= (ImageView) findViewById(R.id.imgFlag);
		 
		if(SEE_ANSWER_FLAG)
			Log.e(TAG, "outside on setWidgetsReferences()");
	}
	
	private void setTranselationTextValues() 
	{
		if(SEE_ANSWER_FLAG)
			Log.e(TAG, "inside on setTranselationTextValues()");
		
		
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		labelTop.setText(transeletion.getTranselationTextByTextIdentifier("mfTitleHomeScreen"));
		txtGreatJobResult.setText(transeletion.getTranselationTextByTextIdentifier("titleGreatJob") + "!");
		txtName.setText(transeletion.getTranselationTextByTextIdentifier("lblRegName") + ":");
		txtCorrect.setText(transeletion.getTranselationTextByTextIdentifier("mfLblCorrect") + ":");
		txtLevel.setText(transeletion.getTranselationTextByTextIdentifier("mfLblLevel") + ":");
		txtPoints.setText(transeletion.getTranselationTextByTextIdentifier("btnTitlePoints") + ":");
		txtProblem.setText(transeletion.getTranselationTextByTextIdentifier("mfLblProblem") + ":");
		txtAnswers.setText(transeletion.getTranselationTextByTextIdentifier("mfLblAnswers") + ":");
		btnPlay.setText(transeletion.getTranselationTextByTextIdentifier("btnTitlePlayAgain"));
		transeletion.closeConnection();
		
		if(SEE_ANSWER_FLAG)
			Log.e(TAG, "outside on setTranselationTextValues()");
	}
	
	@SuppressWarnings("deprecation")
	private void setUserPlayData()
	{
		txtNameValues.setText(seeAnswerDataObj.getPlayerName());
		txtCorrectValues.setText(seeAnswerDataObj.getNoOfCorrectAnswer() + "");
		txtLevelValues.setText(seeAnswerDataObj.getLevel() + "");
		txtPointsValues.setText(seeAnswerDataObj.getPoints() + "");
		
		try 
		{
			if(!(seeAnswerDataObj.getCountryISO().equals("-")))
				imgFlag.setBackgroundDrawable(Drawable.createFromStream(getAssets().open(getResources().
						getString(R.string.countryImageFolder) +"/" 
						+ seeAnswerDataObj.getCountryISO() + ".png"), null));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		SeeAnswerAdapter adapter = new SeeAnswerAdapter(this,R.layout.see_answer_result_layout,seeAnswerDataObj.getEquationList() 
				, seeAnswerDataObj.getUserAnswerList());
		listSolutions.setAdapter(adapter);
		
		/*DynamicLayout dynamicLayout = new DynamicLayout(this);
		dynamicLayout.createDynamicLayoutForSeeAnswer(seeAnswerDataObj.getEquationList(), seeAnswerDataObj.getUserAnswerList(), listSolutions);*/
	}

	@Override
	public void onBackPressed() 
	{
		startActivity(new Intent(this,MainActivity.class));
		super.onBackPressed();
	}
	
	private void setListenerOnWidgets() 
	{
		btnPlay.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) 
	{

		switch(v.getId())
		{		
			case R.id.btnPlay :
				this.clickOnPlayAgain();
				break;
		}
	}
	
	/**
	 * This method call when click on play again
	 */
	private void clickOnPlayAgain()
	{
		switch(isCallFromActivity)
		{
		case 1 :
			//startActivity(new Intent(this,LearningCenterMain.class));
			SharedPreferences sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
			operationSelectShowActivity(sharedPreferences.getInt("operationId", 0));
			break;
		case 2 :
			startActivity(new Intent(this,SingleFriendzyMain.class));
		}
	}
	
	private void operationSelectShowActivity(int operationId)
	{
		startActivity(new Intent(this,NewLearnignCenter.class));
		/*switch(operationId)
		{
		case 1:
			Intent intentAddition = new Intent(this,Addition.class);
			startActivity(intentAddition);
			break;
		case 2:
			Intent intentSubtraction = new Intent(this,Subtraction.class);
			startActivity(intentSubtraction);
			break;
		case 3:
			Intent intentMultiPlication = new Intent(this,Multiplication.class);
			startActivity(intentMultiPlication);
			break;
		case 4:
			Intent intentDivision = new Intent(this,Division.class);
			startActivity(intentDivision);
			break;
		case 5:
			Intent intentAdditionSubtractionOffraction = new Intent(this,AdditionSubtractionOfFraction.class);
			startActivity(intentAdditionSubtractionOffraction);
			break;
		case 6:
			Intent intentMultiPlicationDivisionOfFraction = new Intent(this,MultiplicationDivisionOfFraction.class);
			startActivity(intentMultiPlicationDivisionOfFraction);
			break;
		case 7:
			Intent intentadditionSubtractionOfDecimal = new Intent(this,AdditionSubtractionOfDecimal.class);
			startActivity(intentadditionSubtractionOfDecimal);
			break;
		case 8:
			Intent intentMultiplicatioDivisionOFDecimal = new Intent(this,MultiplicationDivisionOfDecimal.class);
			startActivity(intentMultiplicatioDivisionOFDecimal);
			break;
		case 9:
			Intent intentAdditionSubtractionOfNegatives = new Intent(this,AdditionSubtractionOfNegativeNumber.class);
			startActivity(intentAdditionSubtractionOfNegatives);
			break;
		case 10:
			Intent intentMultiplicatioDivisionOfNegatives = new Intent(this,MultiplicationDivisionOfNegativeNumber.class);
			startActivity(intentMultiplicatioDivisionOfNegatives);
			break;
		}*/
	}
	
	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}
}
	
	
