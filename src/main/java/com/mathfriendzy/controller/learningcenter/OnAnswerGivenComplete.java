package com.mathfriendzy.controller.learningcenter;

public interface OnAnswerGivenComplete {
	void onComplete();
}
