package com.mathfriendzy.controller.learningcenter;

import static com.mathfriendzy.controller.learningcenter.IOperationId.ADDITION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.DECIMAL_ADDITION_SUBTRACTION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.DECIMAL_MULTIPLICATION_DIVISION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.DIVISION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.FRACTION_ADDITION_SUBTRACTION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.FRACTION_MULTIPLICATION_DIVISION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.MULTIPLICATION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.NEGATIVE_ADDITION_SUBTRACTION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.NEGATIVE_MULTIPLICATION_DIVISION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.SUBTRACTION;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LEARNING_CENTER_BG_INFO;
import static com.mathfriendzy.utils.ICommonUtils.LEARNING_CENTER_EQUATION_SOLVE;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import android.content.Intent;
import android.content.SharedPreferences;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;

import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mathfriendzy.R;
import com.mathfriendzy.controller.learningcenter.workarea.WorkAreaActivity;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.MathResultTransferObj;
import com.mathfriendzy.model.learningcenter.MathScoreTransferObj;
import com.mathfriendzy.model.registration.Register;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.PlaySound;

/**
 * This class solve the equation for flash cards
 * @author Yashwant Singh
 *
 */
public class SolveFlashCardsEquations extends LearningCenterEquationSolve //implements OnClickListener
{
	private int noOfAttempts 		  = 0;// this is for no of times user attempts wrong answer
	private String resultFromDatabase = "";

	private ArrayList<String> resultDigitsList = null;

	private final int SLEEP_TIME        = 2000; //for delay time for displaying result after 3 attempts wrong
	private final int MAX_ATTEMPTS 		= 3; 

	private Date startTime = null;
	private Date endTime   = null;
	private int equationId = 0;
	private String userAnswer 			= "";
	private int isCorrectAnsByUser 	= 0;
	private ArrayList<MathScoreTransferObj> playDatalist 	= null;//this list hold the equation time for solving equation
	private LearningCenterTransferObj learnignCenterobj		= null;
	private boolean isClickOnGo 				= false;

	private PlaySound playsound = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_learning_center_equation_solve);

		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "inside onCreate()");

		playsound = new PlaySound(this);

		resultTextViewList = new ArrayList<TextView>();
		playDatalist       = new ArrayList<MathScoreTransferObj>();
		learnignCenterobj  = new LearningCenterTransferObj();

		/*DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);  

		boolean tabletSize = getResources().getBoolean(R.bool.isTablet);

		Log.e(TAG, "denisy " + metrics.densityDpi);

		if(tabletSize)
		{
			Log.e(TAG, "Tab");
			isTab = true;
			if(metrics.heightPixels <= TAB_HEIGHT_1024)
			{
				//Log.e(TAG, "Tab1");
				maxWidth = 80;
				MAX_WIDTH_FOR_FRACTION_TEXT = 35;
				widthForFractionLayout = 80;
				max_width_for_layout_maultiplication = 80;
			}
			else
			{	
				if (metrics.densityDpi > 160){
					maxWidth = 110;//changes for tab
					MAX_WIDTH_FOR_FRACTION_TEXT = 50;
					widthForFractionLayout = 110;
					max_width_for_layout_maultiplication = 110;//changes for tab
				}
				else{
					//Log.e(TAG, "Tab3");
					maxWidth = 80;
					MAX_WIDTH_FOR_FRACTION_TEXT = 35;
					widthForFractionLayout = 80;
					max_width_for_layout_maultiplication = 80;
				}
			}

			MAX_WIDTH_FOR_FRACTION_TEXT = 50;
		  	widthForFractionLayout = 110;

			//Log.e(TAG, " max width " + max_width_for_layout_maultiplication);
		}
		else{
			Log.e(TAG, "Phone");
			if((getResources().getConfiguration().screenLayout &
					Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
					metrics.densityDpi > 320){

				if(metrics.densityDpi > 400){
					maxWidth = 150;
					MAX_WIDTH_FOR_FRACTION_TEXT = 75;
					widthForFractionLayout = 145;
					max_width_for_layout_maultiplication = 120;	
				}else{
					maxWidth = 112;
					MAX_WIDTH_FOR_FRACTION_TEXT = 54;
					widthForFractionLayout = 108;
					max_width_for_layout_maultiplication = 90;	
				}
			}else{
				if ((getResources().getConfiguration().screenLayout &
						Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
						metrics.densityDpi > SCREEN_DENISITY)
				{
					Log.e(TAG, "inside if density");

					maxWidth = 100;
					MAX_WIDTH_FOR_FRACTION_TEXT = 50;
					widthForFractionLayout = 90;
					max_width_for_layout_maultiplication = 80;
				}
				else
				{
					if ((getResources().getConfiguration().screenLayout &
							Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
							metrics.densityDpi > 160
							&&
							(getResources().getConfiguration().screenLayout &
									Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
									metrics.densityDpi <= SCREEN_DENISITY)
					{
						Log.e(TAG, "inside density");
						maxWidth = 75;
						MAX_WIDTH_FOR_FRACTION_TEXT = 38;
						widthForFractionLayout = 67;
						max_width_for_layout_maultiplication = 60;
					}
					else
					{
						Log.e(TAG, "inside else density");
						maxWidth = 50;
						MAX_WIDTH_FOR_FRACTION_TEXT = 25;
						widthForFractionLayout = 45;
						max_width_for_layout_maultiplication = 40;
					}
				}
			}
		}*/

		//for question UI , Issues list 5 march 2014 , issues no. 1
		this.initilizeHeightAndWidthForDynamicLayoutCreation();
		//end changes

		this.setWidgetsReferences();
		this.setWidgetsValuesFromtranselation();
		this.getIntentValue();
		this.setlistenerOnWidgets();

		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "outside onCreate()");
	}


	/**
	 * This method show equation data
	 */
	@Override
	protected void showEqautionData(LearningCenterTransferObj learningObj)
	{
		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "inside showEqautionData()");

		startTime = new Date();
		equationId = learningObj.getEquationsId();
		learnignCenterobj = learningObj;

		//send equation for workArea
		WorkAreaActivity.learningObj = learningObj;

		number1 = learningObj.getNumber1Str();
		number2 = learningObj.getNumber2Str();
		result  = learningObj.getProductStr();

		imgCross1.setBackgroundResource(R.drawable.ml_gray_x);
		imgCross2.setBackgroundResource(R.drawable.ml_gray_x);
		imgCross3.setBackgroundResource(R.drawable.ml_gray_x);

		resultFromDatabase = learningObj.getProductStr();
		resultDigitsList   = this.getDigit(learningObj.getProductStr());
		operator           = learningObj.getOperator();

		noOfDigitsInFisrtNumber  = this.getDigit(learningObj.getNumber1Str()).size();
		noOfDigitsInSecondNumber = this.getDigit(learningObj.getNumber2Str()).size();
		noOfDigitsInResult       = this.getDigit(learningObj.getProductStr()).size();

		/**
		 * Changing the layout 
		 */
		SharedPreferences sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
		operationId = sharedPreferences.getInt("operationId", 0); 

		if(operationId == ADDITION || operationId == SUBTRACTION
				|| operationId == DECIMAL_ADDITION_SUBTRACTION || operationId == NEGATIVE_ADDITION_SUBTRACTION)//1,2,7,9 are the constants which show the operation id's
		{
			this.operationPerformForAdditionSubtraction(learningObj);
		}
		else if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION)// 5,6 for operation on fraction number
		{		
			this.operatioPerformForFraction();
		}
		else if(operationId == MULTIPLICATION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
		{
			numberOfBoxesForMultiplication = noOfDigitsInResult;
			this.operationPerformForMultiplication();
		}
		else if(operationId == DECIMAL_MULTIPLICATION_DIVISION)
		{
			if(operator.equals("x"))
			{
				this.setnumberOfBoxesForMultiplicationForDecimal();
				//numberOfBoxesForMultiplication = noOfDigitsInResult;
				this.operationPerformForMultiplication();
			}
			else if(operator.equals("/"))		
			{				
				this.setNumberOfRowsAndboxForDivisionForDecimal();
				this.operationPerfomrmForDivision();
			}
		}
		else if(operationId ==  DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/")))
		{
			if(noOfDigitsInFisrtNumber == 1 && noOfDigitsInSecondNumber == 1)
				numberOfBoxesForDivision = 1;
			else if((noOfDigitsInFisrtNumber == 2 && noOfDigitsInSecondNumber == 1) || (noOfDigitsInFisrtNumber == 2 && noOfDigitsInSecondNumber == 2))
				numberOfBoxesForDivision = 2;
			else if((noOfDigitsInFisrtNumber == 3 && noOfDigitsInSecondNumber == 2))
				numberOfBoxesForDivision = 3;

			numberOfRowsforDivision = 2 * noOfDigitsInResult;
			this.setNumberOfRowsAndboxForDivisionForBoth();
			this.operationPerfomrmForDivision();
		}

		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "outside showEqautionData()");
	}


	@Override
	protected void getIntentValue() 
	{

		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "inside getIntentValue()");

		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();
		equationsIdList = learningCenterObj.getEquationsId(
				this.getIntent().getIntegerArrayListExtra("selectedCategories"));

		randomGenerator = new Random();

		/**
		 * 	Show the first equation
		 */
		this.showEqautionData(learningCenterObj.getEquation
				(equationsIdList.get(randomGenerator.nextInt(equationsIdList.size()))));

		learningCenterObj.closeConn();

		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "outside getIntentValue()");

	}


	/**
	 * This method creates dynamic text view for result
	 * @param listOfDigits
	 * @param layout
	 */
	@Override
	protected void createResultLayout(ArrayList<String> listOfDigits, LinearLayout layout)
	{
		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "inside createResultLayout()");

		/**
		 * Set the width of linear layout
		 */

		int width = ((noOfDigitsInFisrtNumber + 1) * maxWidth) + 10 ;

		RelativeLayout.LayoutParams lp1 = new RelativeLayout.LayoutParams(width,LayoutParams.WRAP_CONTENT);
		lp1.addRule(RelativeLayout.BELOW,R.id.imgResultLine);
		layout.setLayoutParams(lp1);

		for(i = 0 ; i < listOfDigits.size() ; i ++ )
		{		
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);

			lp.setMargins(2, 0, 0, 0);
			TextView txtView = new TextView(this);
			if(i == listOfDigits.size() - 1)
			{
				//changes for tab
				if(isTab)
					txtView.setBackgroundResource(R.drawable.tab_yellow_ipad);
				else
					txtView.setBackgroundResource(R.drawable.yellow);
			}
			else
			{
				//changes for tab
				if(isTab)
					txtView.setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
				else
					txtView.setBackgroundResource(R.drawable.ml_no1box);
			}

			txtView.setGravity(Gravity.CENTER);

			//changes for tab
			if(isTab)
				txtView.setTextSize(45);
			else
				txtView.setTextSize(32);

			txtView.setTypeface(null, Typeface.BOLD);
			txtView.setTextColor(Color.parseColor("#3CB3FF"));
			resultTextViewList.add(txtView);//add text view to arrayList
			layout.addView(txtView,lp);

			txtView.setOnClickListener(new OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					for( int  i = 0 ; i < resultTextViewList.size() ; i ++ )
					{
						if(v == resultTextViewList.get(i))
						{
							selectedIndex = i;
							index = i;

							//changes for tab
							if(isTab)
								resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
							else
								resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow);
						}
						else
						{
							//changes for tab
							if(isTab)
								resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
							else
								resultTextViewList.get(i).setBackgroundResource(R.drawable.ml_no1box);
						}
					}
				}
			});
		}

		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "outside createResultLayout()");
	}


	@Override
	protected void setSign()
	{
		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "inside setSign()");

		if(isTab){
			if(operator.equals("+"))
				imgSign.setImageResource(R.drawable.ml_plus_tab);
			else if(operator.equals("-"))
				imgSign.setImageResource(R.drawable.ml_minus_tab);
			else if(operator.equals("x"))
				imgSign.setImageResource(R.drawable.ml_multiplication_tab);
			else if(operator.equals("/"))
				imgSign.setImageResource(R.drawable.ml_div_tab);
		}
		else{
			if(operator.equals("+"))
				imgSign.setImageResource(R.drawable.ml_plus_sign);
			else if(operator.equals("-"))
				imgSign.setImageResource(R.drawable.ml_minus_sign);
			else if(operator.equals("x"))
				imgSign.setImageResource(R.drawable.ml_mul_sign);
			else if(operator.equals("/"))
				imgSign.setImageResource(R.drawable.ml_div_sign);
		}
		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "outside setSign()");
	}


	/**
	 * This method set the listener on widgets
	 */
	private void setlistenerOnWidgets() 
	{
		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "inside setlistenerOnWidgets()");

		if(txtCarry != null)
			txtCarry.setOnClickListener(this);
		btnDot.setOnClickListener(this);
		btnMinus.setOnClickListener(this);
		btn1.setOnClickListener(this);
		btn2.setOnClickListener(this);
		btn3.setOnClickListener(this);
		btn4.setOnClickListener(this);
		btn5.setOnClickListener(this);
		btn6.setOnClickListener(this);
		btn7.setOnClickListener(this);
		btn8.setOnClickListener(this);
		btn9.setOnClickListener(this);
		btn0.setOnClickListener(this);
		btnArrowBack.setOnClickListener(this);

		btnGo.setOnClickListener(this);
		btnRoughWork.setOnClickListener(this);

		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "outside setlistenerOnWidgets()");
	}


	@Override
	public void onClick(View v) 
	{	
		switch(v.getId())
		{
		case R.id.btnGo:
			endTime = new Date();
			isClickOnGo = true;		
			if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION)//fraction
			{
				this.clickonGoForFraction();
			}
			else if(operationId == MULTIPLICATION || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x")) 
					|| (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
			{
				this.clickOnGoForMultiplication();
			}
			else if(operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
					||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))
			{
				this.clickOnGoForDivision();
			}
			else
			{
				this.clickOnGo();
			}
			break;
		case R.id.btn1 :
			if(isCarrySelected)
				this.setCarryText("1");
			else
				this.setTextResultValue("1");
			break;
		case R.id.btn2 :
			if(isCarrySelected)
				this.setCarryText("2");
			else
				this.setTextResultValue("2");
			break;
		case R.id.btn3 :
			if(isCarrySelected)
				this.setCarryText("3");
			else
				this.setTextResultValue("3");
			break;
		case R.id.btn4 :
			if(isCarrySelected)
				this.setCarryText("4");
			else
				this.setTextResultValue("4");
			break;
		case R.id.btn5 :
			if(isCarrySelected)
				this.setCarryText("5");
			else
				this.setTextResultValue("5");
			break;
		case R.id.btn6 :
			if(isCarrySelected)
				this.setCarryText("6");
			else
				this.setTextResultValue("6");
			break;
		case R.id.btn7 :
			if(isCarrySelected)
				this.setCarryText("7");
			else
				this.setTextResultValue("7");
			break;
		case R.id.btn8 :
			if(isCarrySelected)
				this.setCarryText("8");
			else
				this.setTextResultValue("8");
			break;
		case R.id.btn9 :
			if(isCarrySelected)
				this.setCarryText("9");
			else
				this.setTextResultValue("9");
			break;
		case R.id.btn0 :
			if(isCarrySelected)
				this.setCarryText("0");
			else
				this.setTextResultValue("0");
			break;
		case R.id.btnDot :
			this.clickonDot();
			break;
		case R.id.btnMinus :
			this.clickOnMinus();
			break;
		case R.id.btnArrowBack :
			this.clickOnBackArrow();
			break;
		case R.id.txtCarry :
			this.clickOnCarry();
			break;
		case R.id.btnRoughWork :
			Intent intent = new Intent(this,WorkAreaActivity.class);
			intent.putExtra("callingActivity", TAG);
			intent.putExtra("startTime", 0);
			startActivity(intent);
			break;
		}
	}


	/**
	 * This method is called when the click on Go Button and operation perform on fraction number
	 */
	private void clickonGoForFraction()
	{

		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "inside clickonGoForFraction()");

		index = 0;

		StringBuilder resultValue = new StringBuilder("");
		for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
		{
			resultValue = resultValue.append(resultTextViewList.get(i).getText().toString());
		}

		userAnswer = resultValue.toString();

		if(userAnswer.endsWith("."))
			resultValue.deleteCharAt(userAnswer.length() - 1);

		if((result.replace("/", "").replace(" ", "")).equals(resultValue.toString()))
		{
			playsound.playSoundForRight(CommonUtils.LANGUAGE_CODE, this);

			isCorrectAnsByUser = 1;
			this.savePlayData();
			
			/*isYellowboxForFraction = false;
			resultTextViewList.clear();
			linearLayoutQuotient.removeAllViews();
			linearLayoutNumerator.removeAllViews();
			linearLayoutDenominator.removeAllViews();
			this.showEqautionData(this.getEquation(randomGenerator.nextInt(equationsIdList.size())));
			noOfAttempts = 0;*/
			
			
			//change
			this.keyBoardDisable();
			this.showCorrectAns(new OnAnswerGivenComplete() {
				@Override
				public void onComplete() {
					isYellowboxForFraction = false;
					resultTextViewList.clear();
					linearLayoutQuotient.removeAllViews();
					linearLayoutNumerator.removeAllViews();
					linearLayoutDenominator.removeAllViews();
					showEqautionData(getEquation(randomGenerator.nextInt(equationsIdList.size())));
					noOfAttempts = 0;
					keyBoardEnable();
				}
			} , resultTextViewList , FRACTION_DESIGN);
			
		}
		else
		{
			isCorrectAnsByUser = 0;
			playsound.playSoundForWrong(CommonUtils.LANGUAGE_CODE, this);
			noOfAttempts ++;
			this.setWrongAttemptsImage(noOfAttempts);
			this.keyBoardDisable();
			this.visibleWrongImageForFraction();
			this.setHandler();
		}
		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "outside clickonGoForFraction()");
	}


	/**
	 * This method is called when the click on Go Button
	 */
	private void clickOnGo()
	{

		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "inside clickOnGo()");

		index         = noOfDigitsInResult - 1;
		selectedIndex = noOfDigitsInResult - 1;

		StringBuilder resultValue = new StringBuilder("");
		for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
		{
			resultValue = resultValue.append(resultTextViewList.get(i).getText().toString());
		}

		userAnswer = resultValue.toString();

		if(userAnswer.endsWith("."))
			resultValue.deleteCharAt(userAnswer.length() - 1);

		if(resultValue.toString().equals(resultFromDatabase))
		{
			playsound.playSoundForRight(CommonUtils.LANGUAGE_CODE, this);

			isCorrectAnsByUser = 1;
			this.savePlayData();

			//change
			this.keyBoardDisable();
			this.showCorrectAns(new OnAnswerGivenComplete() {

				@Override
				public void onComplete() {
					resultTextViewList.clear();
					linearLayoutResultRow.removeViews(0, noOfDigitsInResult);
					showEqautionData(getEquation(randomGenerator.nextInt(equationsIdList.size())));
					noOfAttempts = 0;
					keyBoardEnable();
				}
			} , resultTextViewList);
			//end changes

			/*resultTextViewList.clear();
			linearLayoutResultRow.removeViews(0, noOfDigitsInResult);
			this.showEqautionData(this.getEquation(randomGenerator.nextInt(equationsIdList.size())));
			noOfAttempts = 0;*/


		}
		else
		{	
			playsound.playSoundForWrong(CommonUtils.LANGUAGE_CODE, this);

			/**
			 * check for reverse result
			 */

			if((resultValue.reverse().toString()).equals(resultFromDatabase))
			{
				txtCarry.setText("");
				txtCarry.setBackgroundResource(R.drawable.ml_no1box);

				for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
				{
					resultTextViewList.get(i).setText("");

					if(i == index)
					{
						//changes for tab
						if(isTab)
							resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
						else
							resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow);
					}
					else
					{
						//changes for tab
						if(isTab)
							resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
						else
							resultTextViewList.get(i).setBackgroundResource(R.drawable.ml_no1box);
					}
				}

				/*DialogGenerator dg = new DialogGenerator(this);
				dg.generateDialogForSolvingProblem(null);*/

				//changes on 22 Jan 2015
				try{
					isCorrectAnsByUser = 0;
					this.savePlayData();
					this.keyBoardDisable();

					this.showReverseCorrectAnsDialog(this, null, new OnReverseDialogClose() {
						@Override
						public void onClose() {
							resultTextViewList.clear();
							linearLayoutResultRow.removeViews(0, noOfDigitsInResult);
							showEqautionData(getEquation(randomGenerator.nextInt(equationsIdList.size())));
							noOfAttempts = 0;
							keyBoardEnable();
						}
					},resultTextViewList , resultDigitsList);
				}catch(Exception e){
					e.printStackTrace();
				}
				//end changes
			}
			else
			{
				isCorrectAnsByUser = 0;
				noOfAttempts ++;
				this.setWrongAttemptsImage(noOfAttempts);
				this.keyBoardDisable();

				visibleWrongImage();
				this.setHandler();
			}
		}

		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "outside clickOnGo()");
	}


	/**
	 * This method call when click on go for multiplication
	 */
	private void clickOnGoForMultiplication() 
	{
		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "inside clickOnGoForMultiplication()");

		StringBuilder resultValue = new StringBuilder("");
		if(this.getDigit(number2).size() > 1)
			startIndex = this.getDigit(number2).size() * numberOfBoxesForMultiplication;

		for( int i = startIndex ; i < resultTextViewList.size() ; i ++ )
		{
			resultValue = resultValue.append(resultTextViewList.get(i).getText().toString());
		}

		userAnswer = resultValue.toString();

		if(userAnswer.endsWith("."))
			resultValue.deleteCharAt(userAnswer.length() - 1);

		if(resultValue.toString().equals(resultFromDatabase))
		{
			playsound.playSoundForRight(CommonUtils.LANGUAGE_CODE, this);

			isCorrectAnsByUser = 1;
			this.savePlayData();
			/*isYellowboxForFraction = false;
			resultTextViewList.clear();
			linearLayoutMultiple.removeAllViews();
			linearLayoutResult.removeAllViews();
			this.showEqautionData(this.getEquation(randomGenerator.nextInt(equationsIdList.size())));
			noOfAttempts = 0;*/


			//change
			this.keyBoardDisable();
			this.showCorrectAns(new OnAnswerGivenComplete() {
				@Override
				public void onComplete() {
					isYellowboxForFraction = false;
					resultTextViewList.clear();
					linearLayoutMultiple.removeAllViews();
					linearLayoutResult.removeAllViews();
					showEqautionData(getEquation(randomGenerator.nextInt(equationsIdList.size())));
					noOfAttempts = 0;
					keyBoardEnable();
				}
			} , resultTextViewList , MULTIPLICATION_DESIGN);
		}
		else
		{	
			/**
			 * check for reverse result
			 */
			if(resultValue.reverse().toString().equals(resultFromDatabase))
			{
				txtCarry.setText("");
				txtCarry.setBackgroundResource(R.drawable.whitebox_small);

				if(getDigit(number2).size() > 1)
					startIndex = getDigit(number2).size() * numberOfBoxesForMultiplication;

				for( int i = startIndex ; i < resultTextViewList.size() ; i ++ )
				{
					resultTextViewList.get(i).setText("");

					if( i == resultTextViewList.size() - 1)
					{
						//changes for tab
						if(isTab)
							resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
						else
							resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow_small);
					}
					else
					{
						//changes for tab
						if(isTab)
							resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
						else
							resultTextViewList.get(i).setBackgroundResource(R.drawable.whitebox_small);
					}
				}
				index = resultTextViewList.size() - 1;

				/*DialogGenerator dg = new DialogGenerator(this);
				dg.generateDialogForSolvingProblem(null);*/

				//changes on 22 Jan 2015
				try{
					isCorrectAnsByUser = 0;
					this.savePlayData();

					this.keyBoardDisable();
					this.showReverseCorrectAnsDialog(this, null, new OnReverseDialogClose() {
						@Override
						public void onClose() {
							isYellowboxForFraction = false;
							resultTextViewList.clear();
							linearLayoutMultiple.removeAllViews();
							linearLayoutResult.removeAllViews();
							showEqautionData(getEquation(randomGenerator.nextInt(equationsIdList.size())));
							noOfAttempts = 0;
							keyBoardEnable();
						}
					},resultTextViewList , resultDigitsList , MULTIPLICATION_DESIGN);
				}catch(Exception e){
					e.printStackTrace();
				}
				//end changes
			}
			else
			{
				playsound.playSoundForWrong(CommonUtils.LANGUAGE_CODE, this);

				isCorrectAnsByUser = 0;
				noOfAttempts ++;
				this.setWrongAttemptsImage(noOfAttempts);
				this.visibleWrongImageForMultiplication();
				this.keyBoardDisable();
				this.setHandler();
			}
		}

		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "outside clickOnGoForMultiplication()");

	}


	/**
	 * This method method call when click on go for division
	 */
	private void clickOnGoForDivision()
	{
		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "inside clickOnGoForDivision()");

		StringBuilder resultValue = new StringBuilder("");

		boolean isStartFromZero = false;
		for( int i = 0 ; i < numberOfBoxesForDivision ; i ++ )
		{
			if(!(resultTextViewList.get(i).getText().toString().equals("0")
					&& isStartFromZero == false))
			{	
				resultValue = resultValue.append(resultTextViewList.get(i).getText().toString());
				isStartFromZero = true;
			}
		}

		userAnswer = resultValue.toString();

		if(userAnswer.endsWith("."))
			resultValue.deleteCharAt(userAnswer.length() - 1);

		if(resultValue.toString().equals(resultFromDatabase))
		{
			playsound.playSoundForRight(CommonUtils.LANGUAGE_CODE, this);

			isCorrectAnsByUser = 1;
			this.savePlayData();

			//change
			this.keyBoardDisable();
			this.showCorrectAns(new OnAnswerGivenComplete() {
				@Override
				public void onComplete() {
					isYellowboxForFraction = false;
					resultTextViewList.clear();
					linearLayoutResult.removeAllViews();
					linearLayoutRowsForDivision.removeAllViews();
					showEqautionData(getEquation(randomGenerator.nextInt(equationsIdList.size())));
					noOfAttempts = 0;
					keyBoardEnable();
				}
			} , resultTextViewList , DEVISION_DESIGN);

			/*isYellowboxForFraction = false;
			resultTextViewList.clear();
			linearLayoutResult.removeAllViews();
			linearLayoutRowsForDivision.removeAllViews();
			this.showEqautionData(this.getEquation(randomGenerator.nextInt(equationsIdList.size())));
			noOfAttempts = 0;*/
		}
		else
		{			
			playsound.playSoundForWrong(CommonUtils.LANGUAGE_CODE, this);
			isCorrectAnsByUser = 0;
			noOfAttempts ++;
			this.setWrongAttemptsImage(noOfAttempts);
			this.visibleWrongImageForDivision();
			this.keyBoardDisable();
			this.setHandler();
		}

		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "outsideside clickOnGoForDivision()");
	}


	/**
	 * This method for displaying cross image for wrong question and invisible it
	 */
	private void setHandler()
	{			
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() 
		{
			@Override
			public void run() 
			{
				imgWrong.setVisibility(ImageView.INVISIBLE);

				if(operationId == MULTIPLICATION || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
						||(operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
				{
					if(getDigit(number2).size() > 1)
						startIndex = getDigit(number2).size() * numberOfBoxesForMultiplication;

					for( int i = startIndex ; i < resultTextViewList.size() ; i ++ )
					{
						resultTextViewList.get(i).setText("");
						resultTextViewList.get(i).setTextColor(Color.parseColor("#3CB3FF"));
					}
				}
				else if(operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
						||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))
				{
					for( int i = 0; i < numberOfBoxesForDivision ; i ++ )
					{
						resultTextViewList.get(i).setText("");
						resultTextViewList.get(i).setTextColor(Color.parseColor("#3CB3FF"));
					}
				}
				else
				{
					for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
					{		
						resultTextViewList.get(i).setText("");
						resultTextViewList.get(i).setTextColor(Color.parseColor("#3CB3FF"));
					}
				}

				keyBoardEnable();

				if(noOfAttempts == MAX_ATTEMPTS)
				{	
					savePlayData();
					if(operationId == MULTIPLICATION || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
							||(operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
					{
						if(getDigit(number2).size() > 1)
							startIndex = getDigit(number2).size() * numberOfBoxesForMultiplication;

						for( int i = startIndex ; i < resultTextViewList.size() ; i ++ )
						{
							try{
								//changes on 22 Jan 2015
								setGreenBackGroundToTextViewForDivision(resultTextViewList.get(i));
								//end changes
								resultTextViewList.get(i).setText(resultDigitsList.get(i - startIndex));
							}catch(IndexOutOfBoundsException ee)
							{
								break;
							}
						}
					}
					else if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION)//fraction
					{
						String compareResult = result.replace("/", "").replace(" ", "");
						for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
						{
							//changes on 22 Jan 2015
							setGreenBackGroundToTextViewForDivision(resultTextViewList.get(i));
							//end changes
							resultTextViewList.get(i).setText(compareResult.charAt(i) + "");
						}
					}
					else if(operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
							||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))
					{
						for( int i = 0 ; i < numberOfBoxesForDivision ; i ++ )
						{
							try{
								//changes on 22 Jan 2015
								setGreenBackGroundToTextViewForDivision(resultTextViewList.get(i));
								//end changes
								resultTextViewList.get(i).setText(resultDigitsList.get(i));
							}catch(IndexOutOfBoundsException ee)
							{
								break;
							}
						}
					}
					else
					{

						for( int i = 0 ; i < resultTextViewList.size() ; i ++ )	{
							//changes on 22 Jan 2015
							setGreenBackGroundToTextView(resultTextViewList.get(i));
							//end changes

							resultTextViewList.get(i).setText(resultDigitsList.get(i));
						}
					}

					keyBoardDisable();
					Handler handlerSetResult = new Handler();
					handlerSetResult.postDelayed(new Runnable() 
					{
						@Override
						public void run() 
						{		
							if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION)//fraction
							{
								isYellowboxForFraction = false;
								resultTextViewList.clear();
								linearLayoutQuotient.removeAllViews();
								linearLayoutNumerator.removeAllViews();
								linearLayoutDenominator.removeAllViews();
								showEqautionData(getEquation(randomGenerator.nextInt(equationsIdList.size())));
								noOfAttempts = 0;
								keyBoardEnable();
							}
							else if(operationId == MULTIPLICATION 
									|| (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
									|| (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
							{
								isYellowboxForFraction = false;
								resultTextViewList.clear();
								linearLayoutMultiple.removeAllViews();
								linearLayoutResult.removeAllViews();
								txtCarry.setText("");
								txtCarry.setBackgroundResource(R.drawable.ml_no1box);
								showEqautionData(getEquation(randomGenerator.nextInt(equationsIdList.size())));
								noOfAttempts = 0;
								keyBoardEnable();
							}
							else if(operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
									||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))
							{
								isYellowboxForFraction = false;
								resultTextViewList.clear();
								linearLayoutResult.removeAllViews();
								linearLayoutRowsForDivision.removeAllViews();
								txtCarry.setText("");
								txtCarry.setBackgroundResource(R.drawable.ml_no1box);
								showEqautionData(getEquation(randomGenerator.nextInt(equationsIdList.size())));
								noOfAttempts = 0;
								keyBoardEnable();
							}
							else
							{								
								resultTextViewList.clear();
								linearLayoutResultRow.removeViews(0, noOfDigitsInResult);
								txtCarry.setText("");
								txtCarry.setBackgroundResource(R.drawable.ml_no1box);
								showEqautionData(getEquation(randomGenerator.nextInt(equationsIdList.size())));
								noOfAttempts = 0;
								keyBoardEnable();
							}
						}
					}, SLEEP_TIME);
				}
				else
				{
					if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION)//fraction
					{
						for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
						{
							if(i == index)
							{
								//changes for tab
								if(isTab)
									resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
								else
									resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow_small);
							}
							else
							{
								//changes for tab
								if(isTab)
									resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
								else
									resultTextViewList.get(i).setBackgroundResource(R.drawable.whitebox_small);
							}
						}
					}
					else if(operationId == MULTIPLICATION 
							||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
							|| (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
					{
						txtCarry.setText("");
						txtCarry.setBackgroundResource(R.drawable.whitebox_small);

						if(getDigit(number2).size() > 1)
							startIndex = getDigit(number2).size() * numberOfBoxesForMultiplication;

						for( int i = startIndex ; i < resultTextViewList.size() ; i ++ )
						{
							if( i == resultTextViewList.size() - 1)
							{
								//changes for tab
								if(isTab)
									resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
								else
									resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow_small);
							}
							else
							{
								//changes for tab
								if(isTab)
									resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
								else
									resultTextViewList.get(i).setBackgroundResource(R.drawable.whitebox_small);
							}
						}
						index = resultTextViewList.size() - 1;
					}
					else if(operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
							||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))
					{
						txtCarry.setText("");
						txtCarry.setBackgroundResource(R.drawable.ml_no1box);

						for( int i = 0 ; i <  resultTextViewList.size() ; i ++ )
						{
							if(i == index)
							{
								//changes for tab
								if(isTab)
									resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
								else
									resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow_small);
							}
							else
							{
								//changes for tab
								if(isTab)
									resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
								else
									resultTextViewList.get(i).setBackgroundResource(R.drawable.whitebox_small);
							}
						}
					}
					else
					{
						txtCarry.setText("");
						txtCarry.setBackgroundResource(R.drawable.ml_no1box);

						for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
						{
							if(i == index)
							{
								//changes for tab
								if(isTab)
									resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
								else
									resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow);
							}
							else
							{
								//changes for tab
								if(isTab)
									resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
								else
									resultTextViewList.get(i).setBackgroundResource(R.drawable.ml_no1box);
							}
						}
					}
				}
			}
		//}, SLEEP_TIME / 4);
		}, this.getWrongAnswerShowTime(noOfAttempts , SLEEP_TIME));
	}


	/**
	 * This method add the start and end time to solve equation and all the information related to the equation
	 * to the arrayList
	 */
	private void savePlayData()
	{		 
		MathScoreTransferObj mathObj = new MathScoreTransferObj();
		mathObj.setMathOperationID(operationId);
		mathObj.setEquationId(equationId);
		mathObj.setLap(0);
		mathObj.setStartDateTimeStr(CommonUtils.formateDate(startTime));
		mathObj.setEndDateTimeStr(CommonUtils.formateDate(endTime));
		mathObj.setPoints(0);
		//mathObj.setAnswer(userAnswer);

		if(userAnswer.length() > 0)
			mathObj.setAnswer(userAnswer);
		else
			mathObj.setAnswer("1");

		mathObj.setAnswerCorrect(isCorrectAnsByUser);
		mathObj.setTimeTakenToAnswer(endTime.getSeconds() - startTime.getSeconds());
		mathObj.setEquation(learnignCenterobj);

		playDatalist.add(mathObj);
	}


	/**
	 * This method convert the data for solving the equation into xml format
	 */
	private String getEquationSolveXml(ArrayList<MathScoreTransferObj> playDatalist)
	{
		String xml = "";

		for( int i = 0 ;  i < playDatalist.size() ; i++)
		{
			xml = xml +"<equation>" +
					"<equationId>"+playDatalist.get(i).getEquationId()+"</equationId>"+
					"<start_date_time>"+playDatalist.get(i).getStartDateTimeStr()+"</start_date_time>"+
					"<end_date_time>"+playDatalist.get(i).getEndDateTimeStr()+"</end_date_time>"+
					"<lap>"+playDatalist.get(i).getLap()+"</lap>"+
					"<math_operation_id>"+playDatalist.get(i).getMathOperationID()+"</math_operation_id>"+
					"<points>"+playDatalist.get(i).getPoints()+"</points>"+
					"<isAnswerCorrect>" + playDatalist.get(i).getAnswerCorrect() + "</isAnswerCorrect>"+
					"<user_answer>"+playDatalist.get(i).getAnswer()+"</user_answer>"+
					"</equation>";
		}

		return xml;
	}


	@Override
	public void onBackPressed() 
	{
		SharedPreferences sheredPreference = this.getSharedPreferences(LOGIN_SHARED_PREFF, 0);	
		if(!sheredPreference.getBoolean(IS_LOGIN, false))
		{			
			if(isClickOnGo)
			{
				this.inserPlayDataIntodatabase(savePlayDataToDataBase());
				Intent intent = new Intent();
				setResult(RESULT_OK,intent);
			}
		}
		else
		{
			if(CommonUtils.isInternetConnectionAvailable(this))
			{
				new SaveTempPlayerScoreOnServer(savePlayDataToDataBase()).execute(null,null,null);
			}
			else
			{
				this.inserPlayDataIntodatabase(savePlayDataToDataBase());
				/*DialogGenerator dg = new DialogGenerator(this);
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
				transeletion.closeConnection();*/
			}

		}

		super.onBackPressed();
	}


	/**
	 * This method save play data to the database(Math_Result table)
	 */
	private MathResultTransferObj savePlayDataToDataBase()
	{
		MathResultTransferObj mathResultObj = new MathResultTransferObj();
		mathResultObj.setRoundId(0);
		mathResultObj.setIsFirstRound("No");
		mathResultObj.setGameType("Learn");
		mathResultObj.setIsWin(0);

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		if(sharedPreffPlayerInfo.getString("userId", "").equals(""))
			mathResultObj.setUserId("0");
		else
			mathResultObj.setUserId(sharedPreffPlayerInfo.getString("userId", ""));

		if(sharedPreffPlayerInfo.getString("playerId", "").equals(""))
			mathResultObj.setPlayerId("0");
		else
			mathResultObj.setPlayerId(sharedPreffPlayerInfo.getString("playerId", ""));

		mathResultObj.setIsFakePlayer(0);
		mathResultObj.setProblems(this.getEquationSolveXml(playDatalist));
		mathResultObj.setTotalScore(0);

		return mathResultObj;

	}

	/**
	 * This method store data into database for Math_result
	 * @param mathResultObj
	 */
	private void inserPlayDataIntodatabase(MathResultTransferObj mathResultObj )
	{
		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();
		learningCenterObj.insertIntoMathResult(mathResultObj);
		learningCenterObj.closeConn();
	}


	/**
	 * Save Temp Player result on server
	 * @author Yashwant Singh
	 *
	 */
	class SaveTempPlayerScoreOnServer extends AsyncTask<Void, Void, Void>
	{

		private MathResultTransferObj mathobj = null;

		SaveTempPlayerScoreOnServer(MathResultTransferObj mathObj)
		{
			this.mathobj = mathObj;
		}

		@Override
		protected void onPreExecute() 
		{			
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			Register register = new Register(SolveFlashCardsEquations.this);
			register.savePlayerScoreOnServerForloginuser(mathobj);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			super.onPostExecute(result);

		}
	}
}
