package com.mathfriendzy.controller.learningcenter.showbyccss;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.controller.learningcenter.NewLearnignCenter;
import com.mathfriendzy.model.assessmenttest.AssessmentTestImpl;
import com.mathfriendzy.model.assessmenttest.StandardsDto;
import com.mathfriendzy.model.assessmenttest.SubStandardsDto;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.CommonUtils;

import java.util.ArrayList;

public class ShowByCCSSubStandards extends ActBaseClass {

	private TextView txtTop = null;
	private TextView txtSelectSubStandards = null;
	private LinearLayout selectGradeAndstandardlayout = null;
	//get value from previous screen
	private StandardsDto stdDto = null;

	//for displaying list
	private RelativeLayout childLayout    = null;
	private ArrayList<RelativeLayout> layoutList = null;
	private ArrayList<Button> goButtonList = null;//for perform click operation

	private final String TAG = this.getClass().getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_by_ccssub_standards);

		this.getIntentValues();
		this.setWidgetsReferences();
		this.setTextFromTranslation();
		//display the list of subCategories
		this.createStandardLayoutList(this.getSubStandardsByStdId(stdDto.getStdId()));
	}

	/**
	 * This method set the widgets references
	 */
	private void setWidgetsReferences(){
		txtSelectSubStandards 		= (TextView) findViewById(R.id.txtSelectSubStandards);
		txtTop                      = (TextView) findViewById(R.id.txtTop);
		selectGradeAndstandardlayout = (LinearLayout) findViewById(R.id.selectGradeAndstandardlayout);
	}

	/**
	 * This method get Intent values which are set in the previous screen
	 */
	private void getIntentValues(){
		stdDto = (StandardsDto) this.getIntent().getSerializableExtra("standardsDtoInfo");
	}

	/**
	 * This method get the substandards list from database by std id
	 * @param stdId
	 */
	private ArrayList<SubStandardsDto> getSubStandardsByStdId(int stdId){
		AssessmentTestImpl assessmentImpl = new AssessmentTestImpl(this);
		assessmentImpl.openConnection();
		ArrayList<SubStandardsDto> subStdList = assessmentImpl.getSubStandardListByStdId(stdId);
		assessmentImpl.closeConnection();
		return subStdList;
	}

	/**
	 * This method set the translation text
	 */
	private void setTextFromTranslation(){
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		txtTop.setText(transeletion.getTranselationTextByTextIdentifier("lblShowByCcss"));
		txtSelectSubStandards.setText(transeletion.getTranselationTextByTextIdentifier("lblNowSelectACcssSubStd"));
		transeletion.closeConnection();
	}

	/**
	 * This method create the standards layout
	 * @param
	 */
	private void createStandardLayoutList(final ArrayList<SubStandardsDto> subStdList){
		selectGradeAndstandardlayout.removeAllViews();
		layoutList = new ArrayList<RelativeLayout>();
		goButtonList = new ArrayList<Button>();

		if(subStdList.size() > 0 ){
			for(int i = 0 ; i < subStdList.size() ; i ++ ){
				LayoutInflater inflater = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
				childLayout = (RelativeLayout) inflater.inflate(R.layout.assessment_result_by_ccss_layout, null);
				TextView txtSubCategory = (TextView) childLayout.findViewById(R.id.txtStudentName);
				TextView txtScore		= (TextView) childLayout.findViewById(R.id.txtScore);
				Button   btnGo          = (Button)	childLayout.findViewById(R.id.btnGo);

				txtScore.setVisibility(TextView.GONE);
				btnGo.setVisibility(Button.VISIBLE);
				txtSubCategory.setText(subStdList.get(i).getSubStandardName());

				btnGo.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						for(int i = 0 ; i < goButtonList.size() ; i ++ ){
							if(v == goButtonList.get(i))
								clickOnSubStandards(subStdList.get(i));
						}
					}
				});

				goButtonList.add(btnGo);
				layoutList.add(childLayout);
				selectGradeAndstandardlayout.addView(childLayout);
			}
		}
	}

	/**
	 * This method call when user click on subStandards
	 * @param SubStdObj
	 */
	private void clickOnSubStandards(SubStandardsDto SubStdObj){

		String subCatShortName = "";
		try{
			subCatShortName = SubStdObj.getSubStandardName().substring(0, SubStdObj.getSubStandardName().indexOf(" "));
		}catch(Exception e){
			Log.e(TAG, "Error while taking short sub name from sub standards " + e.toString());
		}

		//updated on 06/11/2014 for assign homework 
		if(this.getIntent().getIntExtra("callingAct", 0) == 1){//Call from assign homework , Updated 06/11/2014
			Intent intent = new Intent();			
			intent.putExtra("stdId", stdDto.getStdId());
			intent.putExtra("subStdId", SubStdObj.getSubStdId());
			intent.putExtra("subCatShortName", subCatShortName);
			setResult(RESULT_OK, intent);
			finish();
		}else{
			Intent intent = new Intent(this,NewLearnignCenter.class);
			intent.putExtra("stdId", stdDto.getStdId());
			intent.putExtra("subStdId", SubStdObj.getSubStdId());
			intent.putExtra("isCallFromSubStandardGoForShowByCCSS", true);
			intent.putExtra("isOpenCat", this.getIntent().getBooleanExtra("isOpenCat", false));
			intent.putExtra("subCatShortName", subCatShortName);
			startActivity(intent);
		}
	}

	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}

}
