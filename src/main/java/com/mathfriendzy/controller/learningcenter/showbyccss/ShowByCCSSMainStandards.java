package com.mathfriendzy.controller.learningcenter.showbyccss;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.model.assessmenttest.AssessmentTestImpl;
import com.mathfriendzy.model.assessmenttest.StandardsDto;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.CommonUtils;

public class ShowByCCSSMainStandards extends ActBaseClass {

	private TextView txtToHighlightMainStandards = null;
	private TextView txtTop = null;
	private LinearLayout selectGradeAndstandardlayout = null;

	//for displaying list
	private RelativeLayout childLayout    = null;
	private TextView     txtStandardName  = null;
	private ArrayList<RelativeLayout> layoutList = null;

	private int selectedGrade = 1;
	private final String TAG = this.getClass().getSimpleName();

	//public final static int CALLING_ACT_ASSIGN_HOWE_WORK_SCH_CURRICULUM = 1;

	private final int SHOW_BY_SUBSTANDARD_REQUEST = 1002;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_by_ccssmain_standards);

		this.getIntentValue();
		this.setWidgetsReferences();
		this.setTextFromTranslation();
		this.getStandardsFromDatabase(selectedGrade);
	}

	/**
	 * This method get Intent values which are set in previous screen and set it to 
	 * local variables
	 */
	private void getIntentValue(){
		selectedGrade = this.getIntent().getIntExtra("selectedGrade", 1);
	}

	/**
	 * This method set the widgets references
	 */
	private void setWidgetsReferences(){
		txtToHighlightMainStandards = (TextView) findViewById(R.id.txtToHighlightMainStandards);
		txtTop                      = (TextView) findViewById(R.id.txtTop);
		selectGradeAndstandardlayout = (LinearLayout) findViewById(R.id.selectGradeAndstandardlayout);
	}

	/**
	 * This method set the translation text
	 */
	private void setTextFromTranslation(){
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		txtTop.setText(transeletion.getTranselationTextByTextIdentifier("lblShowByCcss"));
		txtToHighlightMainStandards.setText(transeletion.getTranselationTextByTextIdentifier("lblToHighlightSchoolCurriculum"));
		transeletion.closeConnection();
	}

	/**
	 * This method getStandards from database
	 * @param grade
	 */
	private void getStandardsFromDatabase(int grade){
		AssessmentTestImpl implObj = new AssessmentTestImpl(this);
		implObj.openConnection();
		ArrayList<StandardsDto> standardsList = implObj.getStandardsByGrade(grade);
		implObj.closeConnection();
		this.createStandardLayoutList(standardsList);
	}

	/**
	 * This method create the standards layout
	 * @param standardList
	 */
	private void createStandardLayoutList(final ArrayList<StandardsDto> standardList){

		selectGradeAndstandardlayout.removeAllViews();
		layoutList = new ArrayList<RelativeLayout>();

		if(standardList.size() > 0 ){
			for(int i = 0 ; i < standardList.size() ; i ++ ){
				LayoutInflater inflater = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
				childLayout = (RelativeLayout) inflater.inflate(R.layout.studentslistlayout, null);
				txtStandardName = (TextView) childLayout.findViewById(R.id.txtStudentName);
				txtStandardName.setText(standardList.get(i).getName());

				childLayout.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						for(int i = 0 ; i < layoutList.size() ; i ++ ){
							if(layoutList.get(i) == v){
								clickOnMainStandard(standardList.get(i));
							}
						}
					}
				});

				layoutList.add(childLayout);
				selectGradeAndstandardlayout.addView(childLayout);
			}
		}else{

			CommonUtils.showInternetDialog(ShowByCCSSMainStandards.this);
		}
	}

	/**
	 * This method call when userclick on main standards
	 * @param standardsDto
	 */
	private void clickOnMainStandard(StandardsDto standardsDto){
		Intent intent = new Intent(ShowByCCSSMainStandards.this , ShowByCCSSubStandards.class);
		intent.putExtra("standardsDtoInfo", standardsDto);
		intent.putExtra("isOpenCat", this.getIntent().getBooleanExtra("isOpenCat", false));
		//updated 06/11/2014 for assign homework
		intent.putExtra("callingAct", this.getIntent().getIntExtra("callingAct", 0));
		startActivityForResult(intent, SHOW_BY_SUBSTANDARD_REQUEST);
	}

	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == RESULT_OK){
			switch(requestCode){
			case SHOW_BY_SUBSTANDARD_REQUEST:
				setResult(RESULT_OK, data);
				finish();
				break;
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
}
