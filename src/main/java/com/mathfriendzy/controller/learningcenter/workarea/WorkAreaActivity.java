package com.mathfriendzy.controller.learningcenter.workarea;

import static com.mathfriendzy.controller.learningcenter.IOperationId.ADDITION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.DECIMAL_ADDITION_SUBTRACTION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.DECIMAL_MULTIPLICATION_DIVISION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.DIVISION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.FRACTION_ADDITION_SUBTRACTION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.FRACTION_MULTIPLICATION_DIVISION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.MULTIPLICATION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.NEGATIVE_ADDITION_SUBTRACTION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.NEGATIVE_MULTIPLICATION_DIVISION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.SUBTRACTION;
import static com.mathfriendzy.helper.MathFriendzyHelper.ON_SCROLLING;
import static com.mathfriendzy.helper.MathFriendzyHelper.SCROLL_DOWN_COMPLETE;
import static com.mathfriendzy.helper.MathFriendzyHelper.SCROLL_UP_COMPLETE;
import static com.mathfriendzy.helper.MathFriendzyHelper.scrollDown;
import static com.mathfriendzy.helper.MathFriendzyHelper.scrollUp;
import static com.mathfriendzy.utils.ICommonUtils.LEARNING_CENTER_BG_INFO;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.controller.learningcenter.LearningCenterEquationSolve;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.OnScrollChange;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import com.mathfriendzy.utils.CommonUtils;

public class WorkAreaActivity extends ActBaseClass implements OnClickListener , OnScrollChange
{
    private LinearLayout layRough			= null;
    private Paint mPaint, mBitmapPaint		= null;
    private MyView mView					= null;
    private Bitmap mBitmap					= null;
    private Canvas mCanvas					= null;
    private Path mPath						= null;
    private Button clearPaint				= null;
    private Button drawPaint				= null;
    private Button btnScroll				= null;
    private TextView txtTitleScreen			= null;
    private TextView txtTimer               = null;

    private RelativeLayout layoutNumber     = null;

    private RelativeLayout child        	= null;
    private int operationId                 = 0;

    private long startTimerForRoughArea = 0;
    private  int maxWidth          = 100;
    private  int SCREEN_DENISITY   = 210;//Screen density for setting layout for the screen based on density
    private int MAX_WIDTH_FOR_FRACTION_TEXT = 25;//for setting text width based on the number of

    public static  LearningCenterTransferObj learningObj = null;
    private CountDownTimer timer = null;
    protected final int TAB_HEIGHT_1024 = 1024;
    private boolean isTab = false;
    //changes for tab
    private int marginHightForDivision = 75;

    //digits in number
    //for sound
    private boolean isClickOnBack = false;

    private boolean isForHomeWork = false;

    private Button btnScrollUp = null;
    private Button btnScrollDown = null;
    private ScrollView scroll = null;
    private boolean isEraseMode = false;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_area);


        isForHomeWork = this.getIntent().getBooleanExtra("isForHomeWork", false);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);

        if(tabletSize){
                    isTab = true;
            if(metrics.heightPixels <= TAB_HEIGHT_1024){
                //added if for the chromebook tab
                if(metrics.densityDpi <= 120){
                    marginHightForDivision = 60;
                    maxWidth = 60;
                    MAX_WIDTH_FOR_FRACTION_TEXT = 25;
                }else {
                    maxWidth = 80;
                    MAX_WIDTH_FOR_FRACTION_TEXT = 35;
                }
            }
            else {
                if(metrics.densityDpi >= 320 && metrics.heightPixels >= 1900
                        && metrics.widthPixels >= 1500){
                    maxWidth = 150;//changes for tab
                    MAX_WIDTH_FOR_FRACTION_TEXT = 100;
                }else{
                    if(metrics.densityDpi >= 320 && metrics.heightPixels >= 1800
                            && metrics.widthPixels >= 1200){
                        maxWidth = 150;//changes for tab
                        MAX_WIDTH_FOR_FRACTION_TEXT = 100;
                    }else if (metrics.densityDpi > 160){
                        maxWidth = 110;//changes for tab
                        MAX_WIDTH_FOR_FRACTION_TEXT = 50;
                        marginHightForDivision = 100;
                    }else{
                        //added if for the chromebook tab
                        if(metrics.densityDpi <= 120){
                            marginHightForDivision = 60;
                            maxWidth = 60;
                            MAX_WIDTH_FOR_FRACTION_TEXT = 25;
                        }else {
                            maxWidth = 80;
                            MAX_WIDTH_FOR_FRACTION_TEXT = 35;
                        }
                    }
                }
            }
        }else{
            if((getResources().getConfiguration().screenLayout &
                    Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
                    metrics.densityDpi > 320){
                if(metrics.densityDpi > 600){//for the range of Nexus6
                    maxWidth = 200;
                    MAX_WIDTH_FOR_FRACTION_TEXT = 100;
                }else if(metrics.densityDpi > 400){
                    maxWidth = 150;
                    MAX_WIDTH_FOR_FRACTION_TEXT = 75;
                }else{
                    maxWidth = 112;
                    MAX_WIDTH_FOR_FRACTION_TEXT = 54;
               }
            }else{
                if ((getResources().getConfiguration().screenLayout &
                        Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
                        metrics.densityDpi > SCREEN_DENISITY){
                    maxWidth = 100;
                    MAX_WIDTH_FOR_FRACTION_TEXT = 50;
                 }
                else{
                    if ((getResources().getConfiguration().screenLayout &
                            Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
                            metrics.densityDpi > 160
                            &&
                            (getResources().getConfiguration().screenLayout &
                                    Configuration.SCREENLAYOUT_SIZE_MASK) ==
                                    Configuration.SCREENLAYOUT_SIZE_NORMAL &&
                            metrics.densityDpi <= SCREEN_DENISITY){
                        maxWidth = 75;
                        MAX_WIDTH_FOR_FRACTION_TEXT = 38;
                    }
                    else{
                        maxWidth = 50;
                        MAX_WIDTH_FOR_FRACTION_TEXT = 25;
                    }
                }
            }
        }

        getWidgetId();
        setTextOnWidget();
        setCustomView();
        this.inflateLayoutForShowEquation();
        this.setPencilSelected(true);
        this.setEraserSelected(false);

        if(!isForHomeWork){
            this.setTimer();
            //for show timer
            this.showTimer();
        }else{
            txtTimer.setVisibility(TextView.GONE);
        }

    }//END onClick Method


    /**
     * Set Pencil Selected And UnSelected Background
     * @param isSelected
     */
    private void setPencilSelected(boolean isSelected){
        if(isSelected){
            drawPaint.setBackgroundResource(R.drawable.pencil);
        }else{
            drawPaint.setBackgroundResource(R.drawable.grey_pencil);
        }
    }

    /**
     * Set Erase Selected and Unselected background
     * @param isSelected
     */
    private void setEraserSelected(boolean isSelected){
        if(isSelected){
            clearPaint.setBackgroundResource(R.drawable.eraser);
        }else{
            clearPaint.setBackgroundResource(R.drawable.grey_eraser);
        }
    }

    /**Changes for show timer
     * This method show timer or not
     */
    private void showTimer(){
        if(this.getIntent().getStringExtra("callingActivity")
                .equals("LearningCenterEquationSolveWithTimer")){
            if(CommonUtils.isShowTimer){
                txtTimer.setVisibility(TextView.VISIBLE);
            }else{
                txtTimer.setVisibility(TextView.GONE);
            }
        }
    }

    private void setTimer()
    {
        if(this.getIntent().getStringExtra("callingActivity").equals("LearningCenterEquationSolveWithTimer")
                || this.getIntent().getStringExtra("callingActivity").equals("SingleFriendzyEquationSolve")
                || this.getIntent().getStringExtra("callingActivity").equals("MultiFriendzyEquationSolve"))
        {
            timer = new CountDownTimer(this.getIntent().getLongExtra("startTime", 0) , 1 * 1000)
            {
                @Override
                public void onTick(long millisUntilFinished)
                {
                    startTimerForRoughArea = millisUntilFinished;
                    if(((millisUntilFinished/1000) % 60) < 10)
                        txtTimer.setText(((millisUntilFinished/1000) / 60) + ":0" + ((millisUntilFinished/1000) % 60));
                    else
                        txtTimer.setText(((millisUntilFinished/1000) / 60) + ":" + ((millisUntilFinished/1000) % 60));
                }

                @Override
                public void onFinish()
                {
                    Log.e("", "Stop");
                }
            };

            timer.start();
        }
        else
        {
            txtTimer.setVisibility(TextView.INVISIBLE);
        }
    }

    @Override
    public void onBackPressed()
    {
        //for sound resume
        isClickOnBack = true;

        if(timer != null)
            timer.cancel();

        if(this.getIntent().getStringExtra("callingActivity").equals("LearningCenterEquationSolveWithTimer")
                || this.getIntent().getStringExtra("callingActivity").equals("SingleFriendzyEquationSolve")
                || this.getIntent().getStringExtra("callingActivity").equals("MultiFriendzyEquationSolve"))
        {
            Intent intent = new Intent();
            intent.putExtra("timerStartTime", startTimerForRoughArea);
            setResult(RESULT_OK,intent);
            finish();
        }

        super.onBackPressed();
    }

    private void setTextOnWidget()
    {
        Translation translate = new Translation(this);
        translate.openConnection();

        txtTitleScreen.setText(translate.getTranselationTextByTextIdentifier("titleWorkArea"));

        translate.closeConnection();
    }

    private void setCustomView()
    {
        DisplayMetrics metrics = getBaseContext().getResources().getDisplayMetrics();
        int w = metrics.widthPixels;
        int h = metrics.heightPixels;
        h = h + 1000;

        mView = new MyView(this, w, h);
        mView.setBackgroundColor(Color.TRANSPARENT);
        mView.setDrawingCacheEnabled(true);

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(Color.BLUE);
        mPaint.setStrokeWidth(MathFriendzyHelper.DRAW_LINE_THICKNESS);
        mPaint.setStyle(Paint.Style.STROKE);
        this.setViewHeightAndWidth(w , h , mView);
        layRough.addView(mView);
    }//END setCustomView method

    /**
     * set the main view height and width
     *
     * @param w
     * @param h
     */
    private void setViewHeightAndWidth(int w, int h, View view) {
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(w, h);
        view.setLayoutParams(param);
    }


    private void getWidgetId()
    {
        layRough 		= (LinearLayout) findViewById(R.id.layoutRough);
        clearPaint 		= (Button) findViewById(R.id.clearPaint);
        drawPaint 		= (Button) findViewById(R.id.draw);
        txtTitleScreen	= (TextView) findViewById(R.id.txtTitleScreen);
        btnScroll		= (Button) findViewById(R.id.btnScroll);
        layoutNumber    = (RelativeLayout) findViewById(R.id.layoutNumber);
        txtTimer        = (TextView) findViewById(R.id.txtTimer);

        btnScrollUp = (Button) findViewById(R.id.btnScrollUp);
        btnScrollDown = (Button) findViewById(R.id.btnScrollDown);
        scroll          = (ScrollView) findViewById(R.id.scroll);

        clearPaint.setOnClickListener(this);
        drawPaint.setOnClickListener(this);
        btnScroll.setOnClickListener(this);

        scrollUp(btnScrollUp, scroll, this);
        scrollDown(btnScrollDown, scroll, this);
    }//END getWidgetId method


    /**
     * This method inflate layout for show equation for solving
     */
    private void inflateLayoutForShowEquation()
    {
        SharedPreferences sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
        operationId = sharedPreferences.getInt("operationId", 0);

        String number1 = learningObj.getNumber1Str();
        String number2 = learningObj.getNumber2Str();
        String result  = learningObj.getProductStr();
        String operator  = learningObj.getOperator();;

        if(operationId == ADDITION || operationId == SUBTRACTION
                ||operationId == MULTIPLICATION || operationId == DECIMAL_ADDITION_SUBTRACTION
                ||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
                || operationId == NEGATIVE_ADDITION_SUBTRACTION
                || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
        {
            layoutNumber.removeAllViews();
            LayoutInflater inflater = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
            child = (RelativeLayout) inflater.inflate(R.layout.addition_subtraction_layout, null);
            layoutNumber.addView(child);

            TextView txtNumber1 	= (TextView) child.findViewById(R.id.Number1);
            TextView txtNumber2 	= (TextView) child.findViewById(R.id.Number2);
            ImageView imgSign    	= (ImageView)child.findViewById(R.id.imgSign);
            TextView txtCarry   	= (TextView) child.findViewById(R.id.txtCarry);
            ImageView imgResultLine = (ImageView)child.findViewById(R.id.imgResultLine);

            txtCarry.setVisibility(TextView.INVISIBLE);

            txtNumber1.setWidth(number1.length() * maxWidth / 2);//set the width of the textView
            txtNumber2.setWidth(number1.length() * maxWidth / 2);

            txtNumber1.setText(number1);
            txtNumber2.setText(number2);

            this.setSign(operator, imgSign);

            /**
             * Set the left margin for displaying line image
             */

            RelativeLayout.LayoutParams lp1 = new RelativeLayout.LayoutParams((number1.length() * (maxWidth / 2)) + maxWidth,LayoutParams.WRAP_CONTENT);
            lp1.addRule(RelativeLayout.BELOW,R.id.Number2);
            imgResultLine.setLayoutParams(lp1);
            imgResultLine.setBackgroundResource(getResources().getIdentifier("ml_equation_line2" ,"drawable",getPackageName()));

        }
        else if(operationId == FRACTION_ADDITION_SUBTRACTION ||
                operationId == FRACTION_MULTIPLICATION_DIVISION)
        {
            layoutNumber.removeAllViews();
            LayoutInflater inflater = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
            child = (RelativeLayout) inflater.inflate(R.layout.fraction_layout, null);
            layoutNumber.addView(child);

            ImageView imgResultLineFirst  = (ImageView) child.findViewById(R.id.imgResultLineFirst);
            ImageView imgResultLineSecond = (ImageView) child.findViewById(R.id.imgResultLineSecond);
            ImageView imgResultLineThird  = (ImageView) child.findViewById(R.id.imgResultLineThird);
            ImageView imgEqual            = (ImageView) child.findViewById(R.id.imgEqual);

            TextView txtnumber1Numerator     = (TextView) child.findViewById(R.id.txtnumber1Numerator);
            TextView txtnumber1Denominator   = (TextView) child.findViewById(R.id.txtnumber1Denominator);
            TextView txtnumber2Numerator     = (TextView) child.findViewById(R.id.txtnumber2Numerator);
            TextView txtnumber2Denominator   = (TextView) child.findViewById(R.id.txtnumber2Denominator);

            ImageView imgSign    = (ImageView) child.findViewById(R.id.imgSign);

            String number1Numerator   = number1.substring(0, number1.indexOf("/"));
            String number1Denominator = number1.substring(number1.indexOf("/") + 1, number1.length());
            String number2Numerator   = number2.substring(0, number2.indexOf("/"));
            String number2Denominator = number2.substring(number2.indexOf("/") + 1, number2.length());

            imgResultLineThird.setVisibility(ImageView.INVISIBLE);
            imgEqual.setVisibility(ImageView.INVISIBLE);

            int width = this.getWidthForFractionText(number1Numerator.length(), number1Denominator.length());
            RelativeLayout.LayoutParams lp1 = new RelativeLayout.LayoutParams(width,RelativeLayout.LayoutParams.WRAP_CONTENT);
            lp1.addRule(RelativeLayout.BELOW,R.id.txtnumber1Numerator);
            lp1.addRule(RelativeLayout.CENTER_VERTICAL,RelativeLayout.TRUE);
            txtnumber1Numerator.setWidth(width);
            txtnumber1Denominator.setWidth(width);
            imgResultLineFirst.setLayoutParams(lp1);

            width = this.getWidthForFractionText(number2Numerator.length(), number2Denominator.length());
            RelativeLayout.LayoutParams lp2 = new RelativeLayout.LayoutParams(width,RelativeLayout.LayoutParams.WRAP_CONTENT);
            lp2.addRule(RelativeLayout.BELOW,R.id.txtnumber2Numerator);
            lp2.addRule(RelativeLayout.RIGHT_OF,R.id.imgSign);
            lp2.addRule(RelativeLayout.CENTER_VERTICAL,RelativeLayout.TRUE);
            txtnumber2Numerator.setWidth(width);
            txtnumber2Denominator.setWidth(width);
            imgResultLineSecond.setLayoutParams(lp2);

            txtnumber1Numerator.setText(number1Numerator);
            txtnumber1Denominator.setText(number1Denominator);

            txtnumber2Numerator.setText(number2Numerator);
            txtnumber2Denominator.setText(number2Denominator);

            this.setSign(operator, imgSign);
        }
        else if(operationId == DIVISION ||
                (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/"))
                ||(operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/")))
        {
            layoutNumber.removeAllViews();
            LayoutInflater inflater = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
            child = (RelativeLayout) inflater.inflate(R.layout.division_layout_learning_center, null);
            layoutNumber.addView(child);

            TextView txtNumber1 	= (TextView)  child.findViewById(R.id.txtdivident);
            TextView txtNumber2 	= (TextView)  child.findViewById(R.id.txtdivisor);
            TextView txtCarry   	= (TextView)  child.findViewById(R.id.txtCarry);
            ImageView imgResultLine = (ImageView) child.findViewById(R.id.imgDivideLine);

            txtCarry.setVisibility(TextView.INVISIBLE);
            txtNumber1.setText(number1);
            txtNumber2.setText(number2);

            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(number1.length() * maxWidth / 2
                    ,RelativeLayout.LayoutParams.WRAP_CONTENT);
            if(isTab)
                lp.setMargins(10, marginHightForDivision, 0, 0);
            else
                lp.setMargins(10, 0, 0, 0);
            lp.addRule(RelativeLayout.BELOW , R.id.linearLayoutResult);
            lp.addRule(RelativeLayout.RIGHT_OF,R.id.txtdivisor);
            imgResultLine.setLayoutParams(lp);
        }
    }


    /**
     * This method return the width for setting for fraction number
     * @param length1
     * @param length2
     * @return
     */
    private int getWidthForFractionText(int length1,int length2)
    {

        int width = 0;
        if(length1 > length2)
        {
            width = length1 * MAX_WIDTH_FOR_FRACTION_TEXT;
        }
        else
        {
            width = length2 * MAX_WIDTH_FOR_FRACTION_TEXT;
        }

        return width;
    }

    /**
     * This method set the sign
     * @param operator
     * @param imgSign
     */
    private void setSign(String operator,ImageView imgSign)
    {
        if(isTab){
            if(operator.equals("+"))
                imgSign.setImageResource(R.drawable.ml_plus_tab);
            else if(operator.equals("-"))
                imgSign.setImageResource(R.drawable.ml_minus_tab);
            else if(operator.equals("x"))
                imgSign.setImageResource(R.drawable.ml_multiplication_tab);
            else if(operator.equals("/"))
                imgSign.setImageResource(R.drawable.ml_div_tab);
        }
        else{
            if(operator.equals("+"))
                imgSign.setImageResource(R.drawable.ml_plus_sign);
            else if(operator.equals("-"))
                imgSign.setImageResource(R.drawable.ml_minus_sign);
            else if(operator.equals("x"))
                imgSign.setImageResource(R.drawable.ml_mul_sign);
            else if(operator.equals("/"))
                imgSign.setImageResource(R.drawable.ml_div_sign);
        }
    }

    @Override
    public void onScrolling(int state) {
        switch (state) {
            case ON_SCROLLING:
                btnScrollDown.setBackgroundResource(R.drawable.down);
                btnScrollUp.setBackgroundResource(R.drawable.up);
                break;
            case SCROLL_DOWN_COMPLETE:
                btnScrollDown.setBackgroundResource(R.drawable.unselected_down);
                btnScrollUp.setBackgroundResource(R.drawable.up);
                break;
            case SCROLL_UP_COMPLETE:
                btnScrollDown.setBackgroundResource(R.drawable.down);
                btnScrollUp.setBackgroundResource(R.drawable.unselected_up);
                break;
        }
    }

    // //////////******************* Printing view *******************///////////////////

    public class MyView extends View
    {
        @SuppressLint("NewApi")
        public MyView(Context c, int w, int h) {
            super(c);

            if (android.os.Build.VERSION.SDK_INT >= 11)
            {
                setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            }

            mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            mCanvas = new Canvas(mBitmap);
            mPath = new Path();
            mBitmapPaint = new Paint(Paint.DITHER_FLAG);
            mBitmapPaint.setXfermode(null);
        }


        @Override
        protected void onDraw(Canvas canvas)
        {
            Path path = mPath;
            canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
            canvas.drawPath(path, mPaint);
        }

        // //////************touching events for painting**************///////
        private float mX, mY;
        private static final float TOUCH_TOLERANCE = 5;

        private void touch_start(float x, float y) {
            mPath.reset();
            mPath.moveTo(x, y);
            mX = x;
            mY = y;
        }

        private void touch_move(float x, float y) {
            float dx = Math.abs(x - mX);
            float dy = Math.abs(y - mY);
            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
                //for erasing the written
                if(isEraseMode){
                    mPath.lineTo(mX, mY);
                    mCanvas.drawPath(mPath, mPaint);
                    mPath.reset();
                    mPath.moveTo(mX, mY);
                }
                //end for erase change
                mX = x;
                mY = y;
                //addPoint(mX,mY);
            }
        }

        private void touch_up() {
            if(isEraseMode)
                mPath.reset();
            else{
                mPath.lineTo(mX, mY);
                // commit the path to our off screen
                mCanvas.drawPath(mPath, mPaint);
                // kill this so we don't double draw
                mPath.reset();
            }
        }


        @Override
        public boolean onTouchEvent(MotionEvent event)
        {
            getParent().requestDisallowInterceptTouchEvent(true);
            float x = event.getX();
            float y = event.getY();
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    touch_start(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_MOVE:
                    touch_move(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_UP:
                    touch_up();
                    invalidate();
                    break;
            }
            return true;
        } // end of touch events for image

    }// END MyView Class



    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.clearPaint:
                isEraseMode = true;
                /*clearPaint.setBackgroundResource(R.drawable.grey_eraser);
                drawPaint.setBackgroundResource(R.drawable.pencil);*/
                this.setEraserSelected(true);
                this.setPencilSelected(false);
                mPaint.setStrokeWidth(50);
                mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                break;

            case R.id.draw:
                isEraseMode = false;
                /*clearPaint.setBackgroundResource(R.drawable.eraser);
                drawPaint.setBackgroundResource(R.drawable.grey_pencil);*/
                this.setPencilSelected(true);
                this.setEraserSelected(false);
                mPaint.setStrokeWidth(MathFriendzyHelper.DRAW_LINE_THICKNESS);
                mPaint.setXfermode(null);
                mPaint.setColor(Color.BLUE);
                break;

            case R.id.btnScroll:
                clearPaint.setBackgroundResource(R.drawable.eraser);
                drawPaint.setBackgroundResource(R.drawable.pencil);
                break;
        }
    }

    @Override
    protected void onPause() {
        if(!isClickOnBack){
            LearningCenterEquationSolve.isStopSecondPlayerProgress = true;//for stop the challenger player progresss
            LearningCenterEquationSolve.isClickOnWorkArea = false;
            if(LearningCenterEquationSolve.playsound != null)
                LearningCenterEquationSolve.playsound.stopPlayer();
            finish();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
