package com.mathfriendzy.controller.learningcenter;

import java.util.ArrayList;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mathfriendzy.R;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import static com.mathfriendzy.utils.ICommonUtils.LEARNING_CENTER_BG_INFO;
import static com.mathfriendzy.utils.ICommonUtils.LEARNING_CENTER_CHOOSE_EQUATION;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;

/**
 * This Activity is open when user click on flash cards
 * This Activity show the equations and
 * At this Activity user select the equation type to play 
 * @author Yashwant Singh
 *
 */
public class ShowFlashCardsEquations extends Activity implements OnClickListener
{
	private Button btnBack 				= null;
	private TextView txtTopBarText 	    = null;
	private ImageView imgSign           = null;
	private TextView txtChooseEquation  = null;
	private Button btnEquation			= null;
	private Button btnTable             = null;
	private ListView lstEquation        = null;
	private Button btnGo                = null;
	private GridView gridView           = null;
		
	private boolean isShowEquation = true;//showing equation for multiplication
	private boolean isShowTable    = false;//showing equation in table
	private String bgName          = null;
	private final int MAX_LENGTH   = 4;//this is for checking max length for string for multiplication
	
	private RelativeLayout layoutEquationTablesForMultiply 		= null;
	
	private SharedPreferences sharedPreferences 				= null;
	
	private final String TAG = this.getClass().getSimpleName();
	
	private ArrayList<LearningCenterTransferObj> categoriesList = null;
	private ArrayList<Integer> defaultCategoryIdList            = null;
	private ArrayList<Integer> selectedMathOperationIdList		= null;
	
	ArrayList<LearningCenterTransferObj> categoriesEquationForMultiplication = null;
	ArrayList<LearningCenterTransferObj> categoriesTableForMultiplication    = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_learning_center_choose_equation);
		
		if(LEARNING_CENTER_CHOOSE_EQUATION)
			Log.e(TAG, "inside onCreate()");
		
		this.setWidgetsReferences();
		this.setWidgetsValues();
		this.setWidgetsValuesFromSharedPereff();
		this.getEquationCategoriesFromSelectedOperations();
		this.setListenerOnWdgets();
		
		if(LEARNING_CENTER_CHOOSE_EQUATION)
			Log.e(TAG, "outside onCreate()");
	}
	
	/**
	 * This method set the widgets references from layout to the widgets objects
	 */
	private void setWidgetsReferences() 
	{
		if(LEARNING_CENTER_CHOOSE_EQUATION)
			Log.e(TAG, "inside setWidgetsReferences()");
		
		btnBack 			= (Button) 		findViewById(R.id.btnTitleBack);
		txtTopBarText 		= (TextView) 	findViewById(R.id.labelTop);
		imgSign 			= (ImageView) 	findViewById(R.id.imgLabelTop);
		txtChooseEquation 	= (TextView) 	findViewById(R.id.txtChooseEquations);
		btnEquation 		= (Button) 		findViewById(R.id.btnEquations);
		btnTable    		= (Button) 		findViewById(R.id.btnTables);
		lstEquation 		= (ListView) 	findViewById(R.id.listChooseEquations);
		btnGo       		= (Button)   	findViewById(R.id.btnGo);
		gridView            = (GridView)    findViewById(R.id.gridChooseEquation);
		
		layoutEquationTablesForMultiply = (RelativeLayout) findViewById(R.id.layoutEquationTablesForMultiply);
		
		
		if(LEARNING_CENTER_CHOOSE_EQUATION)
			Log.e(TAG, "outside setWidgetsReferences()");
		
 	}
	
	/**
	 * This method set the widgets Text values from the translation
	 */
	private void setWidgetsValues() 
	{
		if(LEARNING_CENTER_CHOOSE_EQUATION)
			Log.e(TAG, " inside setWidgetsReferences()");
		
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		btnBack.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBack"));
		btnGo.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGo"));
		transeletion.closeConnection();
		
		if(LEARNING_CENTER_CHOOSE_EQUATION)
			Log.e(TAG, " outside setWidgetsReferences()");
	}
	
	/**
	 * this method set the widgets value form shared preference
	 */
	private void setWidgetsValuesFromSharedPereff() 
	{
		if(LEARNING_CENTER_CHOOSE_EQUATION)
			Log.e(TAG, " inside setWidgetsValueFromIntent()");
		
		sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
		
		bgName = sharedPreferences.getString("bgImageName", "");
				
		txtTopBarText.setText(sharedPreferences.getString("topBarText", ""));
		
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		if(bgName.equals("Multiplication"))
		{
			layoutEquationTablesForMultiply.setVisibility(RelativeLayout.VISIBLE);
			txtChooseEquation.setText(transeletion.getTranselationTextByTextIdentifier("mfLblMultiplyBy"));
			btnEquation.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleEquations"));
			btnTable.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleTables"));
			btnEquation.setOnClickListener(this);
			btnTable.setOnClickListener(this);
			
		}
		else
		{
			layoutEquationTablesForMultiply.setVisibility(RelativeLayout.GONE);
			txtChooseEquation.setText(transeletion.getTranselationTextByTextIdentifier("mfLblChooseSizeOfEquation"));
		}
		transeletion.closeConnection();
		
		if(!(bgName.equals("Addition") || bgName.equals("Subtraction") || bgName.equals("Multiplication")
				||bgName.equals("Division")))
			imgSign.setBackgroundResource(getResources().getIdentifier(sharedPreferences.getString("topBarSign", ""),"drawable",getPackageName()));
		
		if(LEARNING_CENTER_CHOOSE_EQUATION)
			Log.e(TAG, " outside setWidgetsValueFromIntent()");
	}
	
	/**
	 * This method set listener on widgets
	 */
	private void setListenerOnWdgets() 
	{	
		if(LEARNING_CENTER_CHOOSE_EQUATION)
			Log.e(TAG, " inside setListenerOnWdgets()");
		
		btnBack.setOnClickListener(this);
		btnGo.setOnClickListener(this);
		
		if(LEARNING_CENTER_CHOOSE_EQUATION)
			Log.e(TAG, " outside setListenerOnWdgets()");
		
	}
	
	/**
	 * This method getCategoris id and name from the arraylist which is set in previous Actvitit
	 * like as on Addition,Subtraction etc
	 * and set data to adapter
	 */
	private void getEquationCategoriesFromSelectedOperations() 
	{
		
		if(LEARNING_CENTER_CHOOSE_EQUATION)
			Log.e(TAG, " inside getDefaultCatagoryByGrade()");
		
		SharedPreferences sharedPrefPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();
		SharedPreferences sharedPreferencesOperationId = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
		categoriesList = learningCenterObj.getMathOperationCategoriesById
														(sharedPreferencesOperationId.getInt("operationId", 0));
		defaultCategoryIdList =  learningCenterObj.getDefaultLearningCenterCategoryId(
																sharedPrefPlayerInfo.getInt("grade", 0), 
																sharedPreferencesOperationId.getInt("operationId", 0));
		learningCenterObj.closeConn();
		this.setAdapterData();//call function to set data to adapter
		
		if(LEARNING_CENTER_CHOOSE_EQUATION)
			Log.e(TAG, " outside getDefaultCatagoryByGrade()");
	
	}

	/**
	 * Set equation categories from the database to the equation adapter for display in list View
	 */
	private void setAdapterData() 
	{
	
		if(LEARNING_CENTER_CHOOSE_EQUATION)
			Log.e(TAG, " inside setAdapterData()");
		
		if(bgName.equals("Multiplication"))
		{
			this.setAdapterFormultiplication();
		}
		else
		{
			isShowEquation = false;
			isShowTable    = false;
			ChooseEquationAdapter adapter = new ChooseEquationAdapter(this,R.layout.list_learning_center_equations,
					categoriesList , defaultCategoryIdList);
			lstEquation.setAdapter(adapter);
		}
				
		if(LEARNING_CENTER_CHOOSE_EQUATION)
			Log.e(TAG, " outside setAdapterData()");
		
	}

	/**
	 * This method set Adapter when User want to play with multiplication
	 */
	private void setAdapterFormultiplication()
	{
		 categoriesEquationForMultiplication = new ArrayList<LearningCenterTransferObj>();
		 categoriesTableForMultiplication    = new ArrayList<LearningCenterTransferObj>();
		
		for( int i = 0 ; i < categoriesList.size() ; i++ )
		{
			if(categoriesList.get(i).getMathOperationCategory().length() > MAX_LENGTH)
			{
				categoriesEquationForMultiplication.add(categoriesList.get(i));
			}
			else
			{
				categoriesTableForMultiplication.add(categoriesList.get(i));
			}
		}
		
		this.setAdaptermultiplicationData();
	}
	
	/**
	 * This method set the adapter data for multiplication
	 */
	private void setAdaptermultiplicationData()
	{
		if(isShowEquation)
		{
			ChooseEquationAdapter adapter = new ChooseEquationAdapter(this,R.layout.list_learning_center_equations,
					categoriesEquationForMultiplication , defaultCategoryIdList);
			lstEquation.setAdapter(adapter);
		}
		else if(isShowTable)
		{
			GridAdapter adapter = new GridAdapter(this,R.layout.list_learning_center_equations,
					categoriesTableForMultiplication , defaultCategoryIdList);
			gridView.setAdapter(adapter);
		}
	}
	
	@Override
	public void onClick(View v) 
	{	
		switch(v.getId())
		{
			case R.id.btnTitleBack:
				//this.backButtonClick();
				break;
			case R.id.btnGo:
				this.clickOnGo();
				break;
			case R.id.btnEquations :
				isShowEquation = true;
				isShowTable    = false;
				lstEquation.setVisibility(ListView.VISIBLE);
				gridView.setVisibility(GridView.INVISIBLE);
				btnEquation.setBackgroundResource(R.drawable.mf_equations_ipad);
				btnTable.setBackgroundResource(R.drawable.mf_x_tables_ipad);
				this.setAdaptermultiplicationData();
				break;
			case R.id.btnTables:
				isShowEquation = false;
				isShowTable    = true;
				lstEquation.setVisibility(ListView.INVISIBLE);
				gridView.setVisibility(GridView.VISIBLE);
				btnEquation.setBackgroundResource(R.drawable.mf_x_tables2);
				btnTable.setBackgroundResource(R.drawable.mf_equations_table);
				this.setAdaptermultiplicationData();
				break;
		}
	}
		
	
	/**
	 * This method is call when click on back button , which identify the whcich Activity is open on back button 
	 * click 
	 *//*
	private void backButtonClick()
	{
		switch(this.getIntent().getIntExtra("callingActivity", 0))
		{
		case 1:
			startActivity(new Intent(this,Addition.class));
			break;
		case 2:
			startActivity(new Intent(this,Subtraction.class));
			break;
		case 3:
			startActivity(new Intent(this,Multiplication.class));
			break;
		case 4:
			startActivity(new Intent(this,Division.class));
			break;
		case 5:
			startActivity(new Intent(this,AdditionSubtractionOfFraction.class));
			break;
		case 6:
			startActivity(new Intent(this,MultiplicationDivisionOfFraction.class));
			break;
		case 7:
			startActivity(new Intent(this,AdditionSubtractionOfDecimal.class));
			break;
		case 8:
			startActivity(new Intent(this,MultiplicationDivisionOfDecimal.class));
			break;
		case 9:
			startActivity(new Intent(this,AdditionSubtractionOfNegativeNumber.class));
			break;
		case 10:
			startActivity(new Intent(this,MultiplicationDivisionOfNegativeNumber.class));
			break;
		}
	}*/
	
	/**
	 * This method is call when user click on Go Button
	 * In this method the get the selected equation type and sent these seleted type equation to the next 
	 * Activity through the intent
	 */
	private void clickOnGo()
	{
		boolean atleastOneSelected = false;//for checking atleast on of the equation type is selected
		
		selectedMathOperationIdList = new ArrayList<Integer>();
		
		if(isShowTable)
		{
			for( int  i = 0 ; i < GridAdapter.isChecked.size() ; i ++ )
			{
				if(GridAdapter.isChecked.get(i).booleanValue())
				{
					selectedMathOperationIdList.add(categoriesTableForMultiplication.get(i).getMathOperationCategoryId());
					atleastOneSelected = true;
				}
			}
		}
		else if(isShowEquation)
		{
			for( int  i = 0 ; i < ChooseEquationAdapter.isChecked.size() ; i ++ )
			{
				if(ChooseEquationAdapter.isChecked.get(i).booleanValue())
				{
					if(defaultCategoryIdList.size() > 0)
					{
						if(i != 0)
						{
							selectedMathOperationIdList.add(categoriesEquationForMultiplication.get(i-1).getMathOperationCategoryId());
						}
					}
					else
					{
						selectedMathOperationIdList.add(categoriesEquationForMultiplication.get(i).getMathOperationCategoryId());
					}
					atleastOneSelected = true;
				}
			}
		}
		else
		{
			for( int  i = 0 ; i < ChooseEquationAdapter.isChecked.size() ; i ++ )
			{
				if(ChooseEquationAdapter.isChecked.get(i).booleanValue())
				{
					if(defaultCategoryIdList.size() > 0)
					{
						if(i != 0)
						{
							selectedMathOperationIdList.add(categoriesList.get(i-1).getMathOperationCategoryId());
						}
					}
					else
					{
						selectedMathOperationIdList.add(categoriesList.get(i).getMathOperationCategoryId());
					}
					atleastOneSelected = true;
				}
			}
		}
		
		if(atleastOneSelected)
		{
			Intent intentDecimal = new Intent(this,SolveFlashCardsEquations.class);
			intentDecimal.putIntegerArrayListExtra("selectedCategories", selectedMathOperationIdList);
			startActivityForResult(intentDecimal, 1);
		}
		else
		{
			DialogGenerator dg = new DialogGenerator(this);
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("mfPleaseSelectOneOrMoreEquationType"));
			transeletion.closeConnection();
		}
	}

	
	/*@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
		{
			//this.backButtonClick();
			startActivity(new Intent(this , NewLearnignCenter.class));
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}*/

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		if(requestCode == 1)
		{
			if(resultCode == RESULT_OK)
			{
				DialogGenerator dg = new DialogGenerator(this);
				dg.generatetimeSpentDialog();
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}
}
