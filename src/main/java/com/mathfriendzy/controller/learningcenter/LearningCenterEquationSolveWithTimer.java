package com.mathfriendzy.controller.learningcenter;


import static com.mathfriendzy.controller.learningcenter.IOperationId.ADDITION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.DECIMAL_ADDITION_SUBTRACTION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.DECIMAL_MULTIPLICATION_DIVISION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.DIVISION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.FRACTION_ADDITION_SUBTRACTION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.FRACTION_MULTIPLICATION_DIVISION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.MULTIPLICATION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.NEGATIVE_ADDITION_SUBTRACTION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.NEGATIVE_MULTIPLICATION_DIVISION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.SUBTRACTION;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LEARNING_CENTER_BG_INFO;
import static com.mathfriendzy.utils.ICommonUtils.LEARNING_CENTER_EQUATION_SOLVE;
import static com.mathfriendzy.utils.ICommonUtils.LEARNING_CENTER_EQUATION_SOLVE_WITH_TIMER;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.learningcenter.workarea.WorkAreaActivity;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.helper.YesNoListenerInterface;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.friendzy.SaveTimePointsForFriendzyChallenge;
import com.mathfriendzy.model.homework.SaveHomeWorkScoreParam;
import com.mathfriendzy.model.homework.SaveHomeWorkServerResponse;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.MathEquationOperationCategorytransferObj;
import com.mathfriendzy.model.learningcenter.MathResultTransferObj;
import com.mathfriendzy.model.learningcenter.MathScoreTransferObj;
import com.mathfriendzy.model.learningcenter.PlayerEquationLevelObj;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.learningcenter.SeeAnswerTransferObj;
import com.mathfriendzy.model.multifriendzy.MultiFriendzyImpl;
import com.mathfriendzy.model.registration.Register;
import com.mathfriendzy.model.singleFriendzy.SingleFriendzyEquationObj;
import com.mathfriendzy.model.singleFriendzy.SingleFriendzyImpl;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerDialogs;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;
import com.mathfriendzy.utils.PlaySound;

/**
 * This class solve equation with timer for learning center
 * @author Yashwant Singh
 *
 */
public class LearningCenterEquationSolveWithTimer extends LearningCenterEquationSolve //implements OnClickListener
{
	private TextView txtUserName 		= null;
	private TextView txtLap        		= null;
	private TextView txtPoints     		= null;
	private ImageView imgFlag      		= null;
	private ProgressBar layoutProgress 	= null;
	private TextView txtTimer 			= null;

	private long  START_TIME 			= 180 * 1000;//start time for timer
	//private long  START_TIME 			= 60 * 1000;//start time for timer
	private final long END_TIME   		= 1 * 1000;//end time for timer
	private long startTimerForRoughArea = 0;

	private int lap    = 1;

	private String TAG = this.getClass().getSimpleName();
	private int noOfAttempts 		  = 0;// this is for no of times user attempts wrong answer

	private String resultFromDatabase = "";

	private ArrayList<String> resultDigitsList = null;

	private final int SLEEP_TIME        = 2000; //for delay time for displaying result after 3 attempts wrong
	private final int MAX_ATTEMPTS 		= 3; 

	private int progress   = 0;
	private Date startTime = null;
	private Date endTime   = null;

	private int startTimeValue = 0;//this variable contains the number of seconds of starttime
	private int endTimeVlaue   = 0;//this variable contain the number of seconds of endtime

	private int equationId = 0;
	private String userAnswer 		= "";
	private int isCorrectAnsByUser 	= 0;
	private ArrayList<MathScoreTransferObj> playDatalist 	= null;//this list hold the equation time for solving equation
	private LearningCenterTransferObj learnignCenterobj		= null;
	private ArrayList<MathEquationOperationCategorytransferObj> mathOperationList  		= null;
	private ArrayList<MathEquationOperationCategorytransferObj> newMathOperationList  	= null;

	private ArrayList<Integer> onebyoneDigitList 	= null;
	private ArrayList<Integer> twobyoneDigitList 	= null;
	private ArrayList<Integer> twobytwoDigitList 	= null;
	private ArrayList<Integer> threebytwoDigitList 	= null;
	private ArrayList<Integer> threebythreeDigitList= null;
	private ArrayList<Integer> onebytwoDigitList    = null;

	private int points = 0;
	private int pointsForEachQuestion = 0;

	private int showEquationIndex = 0;//this index hold the sequence for shoe equation
	private boolean isClickOnGo 				= false;
	private int rightAnsCounter   = 0;
	private int numberOfStars     = 0;
	private int numberOfCoins     = 0;
	//changes for problem 
	private int numberOfTimesQuestionRepeating = 0;

	//private float coinsPerPoint   = 0.05f;
	private int maxCoinsAfterPlayTenLevel = 200;//User can get 200 bonus coins after play all 10 level 

	private int level             = 0;
	private boolean isClickOnBackPressed = false;

	//private PlaySound playsound  = null;
	private CountDownTimer timer = null;
	//public static CountDownTimer timer = null;

	//variable which hold the data to transfer to the answer screen
	private ArrayList<LearningCenterTransferObj> playEquationList 	= null;
	private ArrayList<String> userAnswerList 						= null;

	public static SeeAnswerTransferObj seeAnswerDataObj = null;

	//added by shilpi for friendzy 
	private long totalTimeTaken = 0l;

	//changes for wrong timer , on 21 Mar
	private int totalTime  = 0 ;
	//end changes


	//for home work
	private boolean isForHomeWork = false;
	private int numberOfProblems  = 0;
	private TextView txtNumberOfProblemPlayed = null;
	private String warningMessageOnBack = null;
	private String yesTextMsg = null;
	private String noTextMsg  = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_learning__center__equation__solve__with__timer);

		if(LEARNING_CENTER_EQUATION_SOLVE_WITH_TIMER)
			Log.e(TAG, "inside oncreate()");


		/*DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);  

		boolean tabletSize = getResources().getBoolean(R.bool.isTablet);

		if(tabletSize)
		{
			//Log.e(TAG, "Tab");

			isTab = true;
			if(metrics.heightPixels <= TAB_HEIGHT_1024)
			{
				//Log.e(TAG, "Tab1");
				maxWidth = 80;
				MAX_WIDTH_FOR_FRACTION_TEXT = 35;
			  	widthForFractionLayout = 80;
				max_width_for_layout_maultiplication = 80;
			}
			else
			{
				if (metrics.densityDpi > 160){
					//Log.e(TAG, "Tab2");
					maxWidth = 110;//changes for tab
					MAX_WIDTH_FOR_FRACTION_TEXT = 50;
				  	widthForFractionLayout = 110;
				  	max_width_for_layout_maultiplication = 110;//changes for tab
				}
				else{
					//Log.e(TAG, "Tab3");
					maxWidth = 80;
					MAX_WIDTH_FOR_FRACTION_TEXT = 35;
				  	widthForFractionLayout = 80;
					max_width_for_layout_maultiplication = 80;
				}
			}

			//changes for tab
			//Log.e(TAG, " max width " + maxWidth);
		}
		else{
			if ((getResources().getConfiguration().screenLayout &
					Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
					metrics.densityDpi > SCREEN_DENISITY)
			{
				maxWidth = 100;
				MAX_WIDTH_FOR_FRACTION_TEXT = 50;
				widthForFractionLayout = 90;
				max_width_for_layout_maultiplication = 80;
			}
			else
			{		
				if ((getResources().getConfiguration().screenLayout &
					    Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
					  metrics.densityDpi > 160
					  &&
					  (getResources().getConfiguration().screenLayout &
							    Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
							  metrics.densityDpi <= SCREEN_DENISITY)
				{
					//Log.e(TAG, "inside denisity");
					maxWidth = 75;
					MAX_WIDTH_FOR_FRACTION_TEXT = 38;
					widthForFractionLayout = 67;
					max_width_for_layout_maultiplication = 60;
				  }
				else
				{
					//Log.e(TAG, "inside else denisity");
				  	maxWidth = 50;
				  	MAX_WIDTH_FOR_FRACTION_TEXT = 25;
				  	widthForFractionLayout = 45;
				  	max_width_for_layout_maultiplication = 40;
				}
			}
		}*/

		//changes for home work
		isForHomeWork = this.getIntent().getBooleanExtra("isForHomeWork", false);

		playsound = new PlaySound(this);

		resultTextViewList = new ArrayList<TextView>();
		playDatalist       = new ArrayList<MathScoreTransferObj>();
		learnignCenterobj  = new LearningCenterTransferObj();

		//Array List Which contain the category id for 1*1,1*2 or etc digits
		onebyoneDigitList 		= new ArrayList<Integer>();
		twobyoneDigitList 		= new ArrayList<Integer>();
		twobytwoDigitList 		= new ArrayList<Integer>();
		threebytwoDigitList 	= new ArrayList<Integer>();
		threebythreeDigitList 	= new ArrayList<Integer>();
		onebytwoDigitList       = new ArrayList<Integer>();

		//for transfer info to the answer screen
		playEquationList = new ArrayList<LearningCenterTransferObj>();
		userAnswerList   = new ArrayList<String>();
		seeAnswerDataObj = new SeeAnswerTransferObj();

		//
		this.initilizeHeightAndWidthForDynamicLayoutCreation();

		this.setWidgetsReferences();
		this.setWidgetsReferencesForTimerLayout();
		this.setWidgetsValuesFromtranselation();
		this.setPlayerDetail();
		this.getIntentValue();
		this.setlistenerOnWidgets();

		if(!isForHomeWork){
			//this.countDawnTimerStart();
			timer = new MyTimer(START_TIME, END_TIME);
			timer.start();
			//for show timer
			this.showTimer();
			txtNumberOfProblemPlayed.setVisibility(TextView.INVISIBLE);
		}else{
			txtTimer.setVisibility(TextView.INVISIBLE);
			txtNumberOfProblemPlayed.setVisibility(TextView.VISIBLE);
			//for set starting number of problem attampted
			this.setNumberOfProblemAttampted(1);
		}

		if(LEARNING_CENTER_EQUATION_SOLVE_WITH_TIMER)
			Log.e(TAG, "outside oncreate()");
	}

	//for homework for number of problem attampted from total problem
	private void setNumberOfProblemAttampted(int problemAttempted){
		txtNumberOfProblemPlayed.setText(problemAttempted + " of " + numberOfProblems);
	}

	/**
	 * This method show timer or not
	 */
	private void showTimer(){
		//for show timer or not
		if(CommonUtils.isShowTimer){
			txtTimer.setVisibility(TextView.VISIBLE);
		}else{
			txtTimer.setVisibility(TextView.INVISIBLE);
		}
	}

	/**
	 * This class show the timer for player
	 * @author Yashwant Singh
	 *
	 */
	private class MyTimer extends CountDownTimer
	{
		public MyTimer(long millisInFuture, long countDownInterval) 
		{
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onTick(long millisUntilFinished) 
		{		
			startTimerForRoughArea = millisUntilFinished;

			if(((millisUntilFinished/1000) % 60) < 10) // Calculate the second if it less then 10 then add 0 prefix
				txtTimer.setText(((millisUntilFinished/1000) / 60) + ":0" + ((millisUntilFinished/1000) % 60));
			else
				txtTimer.setText(((millisUntilFinished/1000) / 60) + ":" + ((millisUntilFinished/1000) % 60));
		}

		@Override
		public void onFinish() 
		{
			//Log.e("", "Stop");

			int totalQuestionPlayed = 0;
			if(newMathOperationList != null)
				totalQuestionPlayed = newMathOperationList.size() * numberOfTimesQuestionRepeating + showEquationIndex;

			if(totalQuestionPlayed >= 10) // if user play minimum 10 equation the star will count otherwise not
			{
				int percentage = (rightAnsCounter * 100 )/totalQuestionPlayed; // calculate the percentage

				//calculate number of star
				if(percentage >= 60 && percentage <=75)
					numberOfStars = 1;
				else if(percentage >= 76 && percentage <=89)
					numberOfStars = 2;
				else if(percentage >= 90 && percentage <=100)
					numberOfStars = 3;
			}

			numberOfCoins = (int) (points * ICommonUtils.COINS_PER_POINT);//calculate number of coins

			isClickOnBackPressed = true;


			//changs for correct time on result screen , on 07 Mar 2014
			totalTime = getNewMathPlayedData(playDatalist);
			//end changes

			MathResultTransferObj mathObj = savePlayDataToDataBase();

			PlayerEquationLevelObj playerEquationObj = new PlayerEquationLevelObj();
			playerEquationObj.setUserId(mathObj.getUserId());
			playerEquationObj.setPlayerId(mathObj.getPlayerId());

			playerEquationObj.setLevel(level);
			playerEquationObj.setStars(numberOfStars);
			playerEquationObj.setEquationType(operationId);


			LearningCenterimpl learningCenterObj = new LearningCenterimpl(LearningCenterEquationSolveWithTimer.this);
			learningCenterObj.openConn();

			int highestLevel =  learningCenterObj.getHighetsLevel(playerEquationObj);

			if(numberOfStars > 0)
			{
				if(learningCenterObj.isEquationPlayerExist(mathObj.getUserId(), mathObj.getPlayerId(), level, operationId))
				{
					learningCenterObj.updateEquationPlayer(mathObj.getUserId(), mathObj.getPlayerId(), level, operationId,numberOfStars);
				}
				else
				{
					learningCenterObj.insertIntoPlayerEquationLevel(playerEquationObj);
				}
			}

			int earnPoints = points;

			mathObj.setTotalPoints(points);//this point will update at the time of update on server

			//int highestLevel =  learningCenterObj.getHighetsLevel(playerEquationObj);

			//9 is the highest level , and 10 is the level playing at current
			if(highestLevel == 9 && level == 10 && numberOfStars > 0)
				numberOfCoins = numberOfCoins + maxCoinsAfterPlayTenLevel;//add 200 coins when user play all 10 levels

			learningCenterObj.closeConn();

			//calling for local database

			callOnBackPressed();

			mathObj.setTotalScore(points);
			mathObj.setEquationTypeId(operationId);
			mathObj.setLevel(level);
			mathObj.setStars(numberOfStars);

			//mathObj.setTotalPoints(points);//this will be changes when the player information is loaded from server

			mathObj.setCoins(numberOfCoins);

			SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);	
			if(sheredPreference.getBoolean(IS_LOGIN, false))
			{			
				if(CommonUtils.isInternetConnectionAvailable(LearningCenterEquationSolveWithTimer.this))
				{
					new AddMathPlayerLavelScoreOnServer(mathObj).execute(null,null,null);
				}
				else
				{					
					//changes according shilpi
					if(numberOfStars > 0){
						LearningCenterimpl learningImpl = 
								new LearningCenterimpl(LearningCenterEquationSolveWithTimer.this);
						learningImpl.openConn();
						learningImpl.inserIntoLocalPlayerLevels
						(mathObj.getUserId(), mathObj.getPlayerId(), operationId, 
								numberOfStars, level);
						learningImpl.closeConn();
					}
				}
			}

			//set data for transferring to the answer list
			SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
			//seeAnswerDataObj.setPlayerName(sharedPreffPlayerInfo.getString("playerName", ""));
			//chngaes for last name first character
			String fullName = sharedPreffPlayerInfo.getString("playerName", "");
			String playerName = fullName.substring(0,(fullName.indexOf(" ") + 2));
			seeAnswerDataObj.setPlayerName(playerName + ".");

			Country country = new Country();
			String coutryIso = country.getCountryIsoByCountryName(sharedPreffPlayerInfo.getString("countryName", ""), LearningCenterEquationSolveWithTimer.this);

			seeAnswerDataObj.setNoOfCorrectAnswer(rightAnsCounter);
			seeAnswerDataObj.setLevel(level);
			seeAnswerDataObj.setPoints(earnPoints);
			seeAnswerDataObj.setCountryISO(coutryIso);
			seeAnswerDataObj.setEquationList(playEquationList);
			seeAnswerDataObj.setUserAnswerList(userAnswerList);

			SeeAnswerActivity.isFromLearnignCenter = true;
			//end

			Intent intent = new Intent(LearningCenterEquationSolveWithTimer.this,CongratulationActivity.class);
			intent.putExtra("points", earnPoints);
			intent.putExtra("stars", numberOfStars);
			intent.putExtra("isFromLearnignCenterSchoolCurriculum", false);
			startActivity(intent);
		}
	}

	/**
	 * This method call when user give the reverse answer
	 */
	private void setTimerForReverseResult(int DESIGN_FOR){
		if(!isForHomeWork){
			START_TIME = startTimerForRoughArea;
			timer.cancel();
			timer = new MyTimer(START_TIME, END_TIME);
		}
		
		if(DESIGN_FOR == MULTIPLICATION_DESIGN){
			//changes on 22 Jan 2015
			try{
                //for the change on the 25 jun 2015  ,treat reverse answer as right, according to ryan
                this.setProgress();
                rightAnsCounter ++ ;
                isCorrectAnsByUser = 1;
                //end change , comment the following line isCorrectAnsByUser = 0;
                //isCorrectAnsByUser = 0;
                this.savePlayData();

				this.keyBoardDisable();
                //this.pauseTimer();
				this.showReverseCorrectAnsDialog(this, timer, new OnReverseDialogClose() {
					@Override
					public void onClose() {
						isYellowboxForFraction = false;
						resultTextViewList.clear();
						linearLayoutMultiple.removeAllViews();
						linearLayoutResult.removeAllViews();
						setIndexForShowEquation();
						noOfAttempts = 0;
						keyBoardEnable();
                        startTimer();
					}
				},resultTextViewList , resultDigitsList , MULTIPLICATION_DESIGN);
			}catch(Exception e){
				e.printStackTrace();
			}
			//end changes
		}
	}

    /**
     * pause timer
     */
    private void pauseTimer(){
        try {
            if(!isForHomeWork) {
                START_TIME = startTimerForRoughArea;
                timer.cancel();
                timer = new MyTimer(START_TIME, END_TIME);
            }
        }catch(Exception e){
           e.printStackTrace();
        }
    }

    /**
     * start timer
     */
    private void startTimer(){
        try {
            if(!isForHomeWork) {
                timer.start();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

	/**
	 * This method call when user give the reverse answer
	 */
	private void setTimerForReverseResult(){
		if(!isForHomeWork){
			START_TIME = startTimerForRoughArea;
			timer.cancel();
			timer = new MyTimer(START_TIME, END_TIME);
		}

		/*DialogGenerator dg = new DialogGenerator(this);
		dg.generateDialogForSolvingProblem(timer);*/

		//changes on 22 Jan 2015
		try{
            //for the change on the 25 jun 2015  ,treat reverse answer as right, according to ryan
            this.setProgress();
            rightAnsCounter ++ ;
            isCorrectAnsByUser = 1;
            //end change , comment the following line isCorrectAnsByUser = 0;
            //isCorrectAnsByUser = 0;
            this.savePlayData();

            this.keyBoardDisable();
            //this.pauseTimer();
			this.showReverseCorrectAnsDialog(this, timer, new OnReverseDialogClose() {
				@Override
				public void onClose() {
                    resultTextViewList.clear();
					linearLayoutResultRow.removeViews(0, noOfDigitsInResult);
					setIndexForShowEquation();
					noOfAttempts = 0;
					keyBoardEnable();
                    startTimer();
				}
			},resultTextViewList , resultDigitsList);
		}catch(Exception e){
			e.printStackTrace();
		}
		//end changes
	}


	/**
	 * This method set the listener on widgets
	 */
	private void setlistenerOnWidgets() 
	{
		if(LEARNING_CENTER_EQUATION_SOLVE_WITH_TIMER)
			Log.e(TAG, "inside setlistenerOnWidgets()");

		if(txtCarry != null)
			txtCarry.setOnClickListener(this);

		btnDot.setOnClickListener(this);
		btnMinus.setOnClickListener(this);
		btn1.setOnClickListener(this);
		btn2.setOnClickListener(this);
		btn3.setOnClickListener(this);
		btn4.setOnClickListener(this);
		btn5.setOnClickListener(this);
		btn6.setOnClickListener(this);
		btn7.setOnClickListener(this);
		btn8.setOnClickListener(this);
		btn9.setOnClickListener(this);
		btn0.setOnClickListener(this);
		btnArrowBack.setOnClickListener(this);

		btnGo.setOnClickListener(this);
		btnRoughWork.setOnClickListener(this);

		if(LEARNING_CENTER_EQUATION_SOLVE_WITH_TIMER)
			Log.e(TAG, "outside setlistenerOnWidgets()");

	}


	@SuppressWarnings("deprecation")
	private void setPlayerDetail() 
	{
		if(LEARNING_CENTER_EQUATION_SOLVE_WITH_TIMER)
			Log.e(TAG, "inside setPlayerDetail()");

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
        String fullName = sharedPreffPlayerInfo.getString("playerName", "");
        try {
            String playerName = fullName.substring(0, (fullName.indexOf(" ") + 2));
            txtUserName.setText(playerName + ".");
        }catch(Exception e){
            txtUserName.setText(fullName + ".");
        }

		Country country = new Country();
		String coutryIso = country.getCountryIsoByCountryName(sharedPreffPlayerInfo.getString("countryName", ""), this);

		try 
		{
			if(!(coutryIso.equals("-")))
				imgFlag.setBackgroundDrawable(Drawable.createFromStream(getAssets().open(getResources().
						getString(R.string.countryImageFolder) +"/" 
						+ coutryIso + ".png"), null));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}

		if(LEARNING_CENTER_EQUATION_SOLVE_WITH_TIMER)
			Log.e(TAG, "outside setPlayerDetail()");
	}

	/**
	 * This method set the widgets references from the timer layout 
	 * other then the Widgets Which references is not set in base class Set WidgetsReferences Method
	 */
	private void setWidgetsReferencesForTimerLayout() 
	{
		if(LEARNING_CENTER_EQUATION_SOLVE_WITH_TIMER)
			Log.e(TAG, "inside setWidgetsReferencesForTimerLayout()");

		txtUserName     = (TextView) findViewById(R.id.txtUserName);
		txtLap 			= (TextView) findViewById(R.id.txtLap);
		txtPoints 		= (TextView) findViewById(R.id.txtPoints);
		imgFlag			= (ImageView) findViewById(R.id.imgFlag);
		layoutProgress 	= (ProgressBar) findViewById(R.id.layoutProgress);
		txtTimer 		= (TextView) findViewById(R.id.txtTimer);

		//for homework
		txtNumberOfProblemPlayed = (TextView) findViewById(R.id.txtNumberOfProblemPlayed);

		if(LEARNING_CENTER_EQUATION_SOLVE_WITH_TIMER)
			Log.e(TAG, "outside setWidgetsReferencesForTimerLayout()");
	}


	/**
	 * This method set the progress and also set points and lap
	 */
	private void setProgress()
	{
		if(progress == 100) // if progress is 100 then increment lap and set progress 25
		{
			lap = lap + 1; // Increment lap when user progress is 100
			progress = 25; // set progress for progress bar 25

		}
		else
		{
			progress = progress + 25;//update progress bar progress
		}

		layoutProgress.setProgress(progress);

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		txtLap.setText(transeletion.getTranselationTextByTextIdentifier("mfLblLap") + ": " + lap);
		transeletion.closeConnection();
		this.setPoints();
	}


	/**
	 * This method set the points
	 */
	private void setPoints()
	{
		if(LEARNING_CENTER_EQUATION_SOLVE_WITH_TIMER)
			Log.e(TAG, "inside setPoints()");

		int calculatedpoints = 0;//initially 0
		if(operationId == ADDITION || operationId == SUBTRACTION)
		{					
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(50);
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(100);
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(125);
			}
			else if(threebythreeDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(150);
			}

			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			txtPoints.setText(transeletion.getTranselationTextByTextIdentifier("mfLblPts") + ": " + calculatedpoints);
			transeletion.closeConnection();
		}
		else if(operationId == MULTIPLICATION)
		{
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(50);
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(150);
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(250);
			}
			else if(threebytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(300);
			}

			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			txtPoints.setText(transeletion.getTranselationTextByTextIdentifier("mfLblPts") + ": " + calculatedpoints);
			transeletion.closeConnection();
		}
		else if(operationId == DIVISION)
		{
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(50);
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(150);
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(250);
			}
			else if(threebytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(400);
			}

			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			txtPoints.setText(transeletion.getTranselationTextByTextIdentifier("mfLblPts") + ": " + calculatedpoints);
			transeletion.closeConnection();
		}
		else if(operationId == FRACTION_ADDITION_SUBTRACTION)
		{
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(150);
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(200);
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(300);
			}

			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			txtPoints.setText(transeletion.getTranselationTextByTextIdentifier("mfLblPts") + ": " + calculatedpoints);
			transeletion.closeConnection();
		}
		else if(operationId == FRACTION_MULTIPLICATION_DIVISION)
		{
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(150);
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(250);
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(400);
			}

			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			txtPoints.setText(transeletion.getTranselationTextByTextIdentifier("mfLblPts") + ": " + calculatedpoints);
			transeletion.closeConnection();
		}
		else if(operationId == DECIMAL_ADDITION_SUBTRACTION)
		{
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(75);
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(100);
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(150);
			}
			else if(threebythreeDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(200);
			}

			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			txtPoints.setText(transeletion.getTranselationTextByTextIdentifier("mfLblPts") + ": " + calculatedpoints);
			transeletion.closeConnection();
		}
		else if(operationId == DECIMAL_MULTIPLICATION_DIVISION)
		{
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(75);
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(125);
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(200);
			}
			else if(threebytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(250);
			}

			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			txtPoints.setText(transeletion.getTranselationTextByTextIdentifier("mfLblPts") + ": " + calculatedpoints);
			transeletion.closeConnection();
		}
		else if(operationId == NEGATIVE_ADDITION_SUBTRACTION)
		{
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(100);
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(150);
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(200);
			}
			else if(threebythreeDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(250);
			}

			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			txtPoints.setText(transeletion.getTranselationTextByTextIdentifier("mfLblPts") + ": " + calculatedpoints);
			transeletion.closeConnection();
		}
		else if(operationId == NEGATIVE_MULTIPLICATION_DIVISION)
		{
			if(onebyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(100);
			}
			else if(twobyoneDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{					
				calculatedpoints = this.calculatePoints(200);
			}
			else if(twobytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(300);
			}
			else if(threebytwoDigitList.contains(newMathOperationList.get(showEquationIndex).getCategoryId()))
			{
				calculatedpoints = this.calculatePoints(500);
			}

			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			txtPoints.setText(transeletion.getTranselationTextByTextIdentifier("mfLblPts") + ": " + calculatedpoints);
			transeletion.closeConnection();
		}

		if(LEARNING_CENTER_EQUATION_SOLVE_WITH_TIMER)
			Log.e(TAG, "outside setPoints()");
	}


	/**
	 * This method calculate points and return it;
	 * @param maxPoints
	 */
	private int calculatePoints(int maxPoints)
	{		
		if((maxPoints - (endTime.getSeconds() - startTime.getSeconds())) > 3 && (endTime.getSeconds() - startTime.getSeconds()) > 0)
		{
			pointsForEachQuestion = (maxPoints - (endTime.getSeconds() - startTime.getSeconds()));
			points = points + (maxPoints - (endTime.getSeconds() - startTime.getSeconds()));
		}
		else
		{
			pointsForEachQuestion = 3; // add max 3 points when user give answer greater then max points time in seconds
			points = points + 3;
		}

		return points;
	}


	@Override
	protected void showEqautionData(LearningCenterTransferObj learningObj) 
	{
		if(LEARNING_CENTER_EQUATION_SOLVE_WITH_TIMER)
			Log.e(TAG, "inside showEqautionData()");

		/*//chnages for friendzy
		index 				  = 0;// index which holding the selected result textBox
		selectedIndex 		  = 0;//this index contain the currently selected box in result
		//end changes
		 */		

		playEquationList.add(learningObj);

		startTime = new Date();

		/*Log.e(TAG, "equationId" + learningObj.getEquationsId() + " number1 " + learningObj.getNumber1Str()
				+ " number2 " + learningObj.getNumber2Str() + " operator " + learningObj.getOperator() 
				+ " " + learningObj.getProductStr());*/


		//send equation for workArea
		WorkAreaActivity.learningObj = learningObj;

		//set default progress at the starting
		layoutProgress.setProgress(progress);

		equationId = learningObj.getEquationsId();
		learnignCenterobj = learningObj;

		number1 = learningObj.getNumber1Str();
		number2 = learningObj.getNumber2Str();
		result  = learningObj.getProductStr();

		imgCross1.setBackgroundResource(R.drawable.ml_gray_x);
		imgCross2.setBackgroundResource(R.drawable.ml_gray_x);
		imgCross3.setBackgroundResource(R.drawable.ml_gray_x);

		resultFromDatabase = learningObj.getProductStr();
		resultDigitsList   = this.getDigit(learningObj.getProductStr());
		operator           = learningObj.getOperator();

		noOfDigitsInFisrtNumber  = this.getDigit(learningObj.getNumber1Str()).size();
		noOfDigitsInSecondNumber = this.getDigit(learningObj.getNumber2Str()).size();
		noOfDigitsInResult       = this.getDigit(learningObj.getProductStr()).size();

		/**
		 * Changing the layout 
		 *//*
		SharedPreferences sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
		operationId = sharedPreferences.getInt("operationId", 0); */

		//changes for friendzy challenge
		SingleFriendzyImpl singleFriendzy = new SingleFriendzyImpl(this);
		singleFriendzy.openConn();
		SingleFriendzyEquationObj singleFriendzyEquation  
		= singleFriendzy.getEquationData(learningObj.getEquationsId());
		singleFriendzy.closeConn();
		operationId =  singleFriendzyEquation.getMathOperationId();

		SharedPreferences sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putInt("operationId", operationId);
		editor.commit();

		//Log.e(TAG, "operation Id " + operationId);
		//end changes

		if(operationId == ADDITION || operationId == SUBTRACTION
				|| operationId == DECIMAL_ADDITION_SUBTRACTION || operationId == NEGATIVE_ADDITION_SUBTRACTION)//1,2,7,9 are the constants which show the operation id's
		{
			this.operationPerformForAdditionSubtraction(learningObj);
		}
		else if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION)// 5,6 for operation on fraction number
		{		
			this.operatioPerformForFraction();
		}
		else if(operationId == MULTIPLICATION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
		{
			numberOfBoxesForMultiplication = noOfDigitsInResult;
			this.operationPerformForMultiplication();
		}
		else if(operationId == DECIMAL_MULTIPLICATION_DIVISION)
		{
			if(operator.equals("x"))
			{
				this.setnumberOfBoxesForMultiplicationForDecimal();
				this.operationPerformForMultiplication();
			}
			else if(operator.equals("/"))		
			{
				this.setNumberOfRowsAndboxForDivisionForDecimal();
				this.operationPerfomrmForDivision();
			}

		}
		else if(operationId ==  DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/")))
		{
			if(noOfDigitsInFisrtNumber == 1 && noOfDigitsInSecondNumber == 1)
				numberOfBoxesForDivision = 1;
			else if((noOfDigitsInFisrtNumber == 2 && noOfDigitsInSecondNumber == 1) || (noOfDigitsInFisrtNumber == 2 && noOfDigitsInSecondNumber == 2))
				numberOfBoxesForDivision = 2;
			else if((noOfDigitsInFisrtNumber == 3 && noOfDigitsInSecondNumber == 2))
				numberOfBoxesForDivision = 3;

			numberOfRowsforDivision = 2 * noOfDigitsInResult;
			this.setNumberOfRowsAndboxForDivisionForBoth();
			this.operationPerfomrmForDivision();
		}

		if(LEARNING_CENTER_EQUATION_SOLVE_WITH_TIMER)
			Log.e(TAG, "outside showEqautionData()");
	}


	/**
	 * This methos set the index for show equation and also calles showEqiuation method which
	 * show the equation on this selected index
	 */
	private void setIndexForShowEquation(){
		if(!isForHomeWork){
			if(showEquationIndex < newMathOperationList.size() - 1){
				this.showEqautionData(this.getEquation(++showEquationIndex));
			}
			else{
				showEquationIndex = 0 ;
				numberOfTimesQuestionRepeating ++ ;//for question repeating
				this.showEqautionData(this.getEquation(++showEquationIndex));
			}
		}else{
			if(numberOfProblems - 1 > showEquationIndex){
				if(showEquationIndex < newMathOperationList.size() - 1){
					this.showEqautionData(this.getEquation(++showEquationIndex));
				}
				else{
					showEquationIndex = 0 ;
					numberOfTimesQuestionRepeating ++ ;//for question repeating
					this.showEqautionData(this.getEquation(++showEquationIndex));
				}
				this.setNumberOfProblemAttampted(showEquationIndex + 1);
			}else{

				final SaveHomeWorkScoreParam param = new SaveHomeWorkScoreParam();
				param.setAction("addHomeworkMathsScore");
				param.setTime(totalTimeTaken + "");
				final int score = (rightAnsCounter * 100) / numberOfProblems;
				param.setScore(score + "");
				param.setHWID(this.getIntent().getStringExtra("HWId"));

				SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
				if(sharedPreffPlayerInfo.getString("userId", "").equals("0"))
					param.setUserId("0");
				else
					param.setUserId(sharedPreffPlayerInfo.getString("userId", ""));

				if(sharedPreffPlayerInfo.getString("playerId", "").equals("0"))
					param.setPlayerId("0");
				else
					param.setPlayerId(sharedPreffPlayerInfo.getString("playerId", ""));

				param.setQuesitonCount(numberOfProblems);
				param.setProblems("<equations>" + 
						this.getEquationSolveXml(playDatalist) + "</equations>");
				param.setPoints(points);
				numberOfCoins = (int) (points * ICommonUtils.COINS_PER_POINT);
				param.setCoinds(numberOfCoins);
				param.setMathCatId(this.getIntent().getStringExtra("catId"));
				param.setMathSubCatId(this.getIntent().getIntExtra("subCatId", 0));

				HttpResponseInterface mInterface = new HttpResponseInterface() {
					@Override
					public void serverResponse(HttpResponseBase httpResponseBase,
							int requestCode) {
						if(requestCode == ServerOperationUtil.SAVE_HOMEWORK_SCORE){
							SaveHomeWorkServerResponse response = 
									(SaveHomeWorkServerResponse) httpResponseBase;
							if(response.getResult().equals("success")){
								response.setCurrentPlaySubCatScore(score + "");
								Intent intent = new Intent();
								intent.putExtra("resultAfterPlayed", response);
								setResult(Activity.RESULT_OK, intent);
								finish();
							}
						}
					}
				};

				if(CommonUtils.isInternetConnectionAvailable(this)){
					new MyAsyckTask(ServerOperation.createPostRequestForSaveHomeWork(param)
							, null, ServerOperationUtil.SAVE_HOMEWORK_SCORE, this, 
							mInterface, ServerOperationUtil.SIMPLE_DIALOG , true , 
							getString(R.string.please_wait_dialog_msg))
					.execute();
				}else{
					new AsyncTask<Void, Void, SaveHomeWorkServerResponse>(){
						private ProgressDialog pd = null;

						@Override
						protected void onPreExecute() {
							pd = ServerDialogs.getProgressDialog
									(LearningCenterEquationSolveWithTimer.this, 
											getString(R.string.please_wait_dialog_msg), 1);
							pd.show();
						}

						@Override
						protected SaveHomeWorkServerResponse doInBackground(
								Void... params) {
							return getSaveHomeWorkServerResponse(score , param);
						}

						@Override
						protected void onPostExecute(SaveHomeWorkServerResponse response) {
							if(pd != null && pd.isShowing()){
								pd.cancel();
							}

							if(response != null){
								Intent intent = new Intent();
								intent.putExtra("resultAfterPlayed", response);
								setResult(Activity.RESULT_OK, intent);
								finish();
							}
						}
					}.execute();
				}
			}
		}
	}

	/**
	 * Get saved homework response
	 * @param score
	 * @param param 
	 * @return
	 */
	private SaveHomeWorkServerResponse getSaveHomeWorkServerResponse
	(int score, SaveHomeWorkScoreParam param){
		//this.inserPlayDataIntodatabase(savePlayDataToDataBase());
		MathFriendzyHelper.savePracticeSkillForOfflineHW(this, param);
		SaveHomeWorkServerResponse response = MathFriendzyHelper
				.getUpdatedResultForPracticeSkillForOfflineHomework
				(this.getIntent().getStringExtra("catId"), 
						this.getIntent().getIntExtra("subCatId", 0), 
						score + "" , totalTimeTaken);
		return response;
	}

	@Override
	protected void getIntentValue() 
	{
		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "inside getIntentValue()");


		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();

		if(!isForHomeWork){
			//changes for friendzy
			if(CommonUtils.isActivePlayer){
				String equationString = "";
				try{
					equationString = MainActivity.equationsObj.getEquationIds();
				}catch(Exception e){
					equationString = "1&1,2";
				}

				MultiFriendzyImpl multifrinedzyImpl = new MultiFriendzyImpl(this);
				multifrinedzyImpl.openConn();
				ArrayList<Integer> mathOperationIdList = multifrinedzyImpl.getEquationCategoryId
						(equationString.substring(equationString.indexOf('&') + 1 , equationString.length()), 
								equationString.substring(0 , equationString.indexOf('&')));

				mathOperationList = learningCenterObj.getMathEquationDataByCategories(mathOperationIdList);
				multifrinedzyImpl.closeConn();
				level = 1;
			}else{
				mathOperationList = learningCenterObj.getMathEquationDataByCategories(
						this.getIntent().getIntegerArrayListExtra("selectedCategories"));
				level = this.getIntent().getIntExtra("level", 0);
			}
		}else{
			mathOperationList = learningCenterObj.getMathEquationDataByCategories(
					this.getIntent().getIntegerArrayListExtra("selectedCategories"));
			numberOfProblems = this.getIntent().getIntExtra("numberOfProblems", 0);
			level = 1;
		}
		//end changes


		this.getCategoriyIdListByDigits();

		equationsIdList = new ArrayList<Integer>();
		for( int i = 0 ; i < newMathOperationList.size() ; i ++ )
		{
			equationsIdList.add(newMathOperationList.get(i).getEquationId());
		}

		/**
		 * 	Show the first equation
		 */
		this.showEqautionData(learningCenterObj.getEquation
				(equationsIdList.get(showEquationIndex)));

		learningCenterObj.closeConn();

		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "outside getIntentValue()");

	}


	/**
	 * This method get Category Id List From Database and set to the arrayList
	 * like 1*1 digit,2*1 and etc
	 */
	private void getCategoriyIdListByDigits()
	{
		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();
		onebyoneDigitList 	= learningCenterObj.getDigitType("1 digit%1 digit");
		twobyoneDigitList 	= learningCenterObj.getDigitType("2 digits%1 digit");
		twobytwoDigitList 	= learningCenterObj.getDigitType("2 digits%2 digits");
		threebytwoDigitList = learningCenterObj.getDigitType("3 digits%2 digits");
		threebythreeDigitList= learningCenterObj.getDigitType("3 digits%3 digits");
		onebytwoDigitList   = learningCenterObj.getDigitType("1 digit%2 digits");

		for( int i = 0 ; i < onebytwoDigitList.size() ; i ++ )
		{
			twobyoneDigitList.add(onebytwoDigitList.get(i));
		}

		learningCenterObj.closeConn();

		this.setSequaence();
	}


	/**
	 * This method set the sequence for displaying equations
	 */
	private void setSequaence()
	{
		ArrayList<MathEquationOperationCategorytransferObj> oneDByOneDlist = new ArrayList<MathEquationOperationCategorytransferObj>();
		ArrayList<MathEquationOperationCategorytransferObj> twoDByOneDlist = new ArrayList<MathEquationOperationCategorytransferObj>();
		ArrayList<MathEquationOperationCategorytransferObj> twoDByTwoDlist = new ArrayList<MathEquationOperationCategorytransferObj>();
		ArrayList<MathEquationOperationCategorytransferObj> threeDByTwoDlist = new ArrayList<MathEquationOperationCategorytransferObj>();
		ArrayList<MathEquationOperationCategorytransferObj> threeDByThreeDlist = new ArrayList<MathEquationOperationCategorytransferObj>();

		newMathOperationList = new ArrayList<MathEquationOperationCategorytransferObj>();
		randomGenerator = new Random();
		int randomNumber = 0;

		int maxEquation = mathOperationList.size() > 100 ? 100:mathOperationList.size();

		for( int i = 0 ; i < maxEquation ; i++)
		{
			randomNumber = randomGenerator.nextInt(mathOperationList.size());

			if(onebyoneDigitList.contains(mathOperationList.get(randomNumber).getCategoryId()))
			{
				oneDByOneDlist.add(mathOperationList.get(randomNumber));
			}
			else if(twobyoneDigitList.contains(mathOperationList.get(randomNumber).getCategoryId()))
			{
				twoDByOneDlist.add(mathOperationList.get(randomNumber));
			}
			else if(twobytwoDigitList.contains(mathOperationList.get(randomNumber).getCategoryId()))
			{
				twoDByTwoDlist.add(mathOperationList.get(randomNumber));
			}
			else if(threebytwoDigitList.contains(mathOperationList.get(randomNumber).getCategoryId()))
			{
				threeDByTwoDlist.add(mathOperationList.get(randomNumber));
			}
			else if(threebythreeDigitList.contains(mathOperationList.get(randomNumber).getCategoryId()))
			{
				threeDByThreeDlist.add(mathOperationList.get(randomNumber));
			}

			mathOperationList.remove(randomNumber);
		}

		for(int i = 0 ; i < 3 ; i++)
		{
			if(oneDByOneDlist.size() > i)
			{
				randomNumber = randomGenerator.nextInt(oneDByOneDlist.size());
				newMathOperationList.add(oneDByOneDlist.get(randomNumber));
				oneDByOneDlist.remove(randomNumber);
			}
		}

		for(int i = 0 ; i < 3 ; i++)
		{
			if(twoDByOneDlist.size() > i)
			{
				randomNumber = randomGenerator.nextInt(twoDByOneDlist.size());
				newMathOperationList.add(twoDByOneDlist.get(randomNumber));
				twoDByOneDlist.remove(randomNumber);
			}
		}

		for(int i = 0 ; i < 2 ; i++)
		{
			if(twoDByTwoDlist.size() > i)
			{
				randomNumber = randomGenerator.nextInt(twoDByTwoDlist.size());
				newMathOperationList.add(twoDByTwoDlist.get(randomNumber));
				twoDByTwoDlist.remove(randomNumber);
			}
		}

		for(int i = 0 ; i < 1 ; i++)
		{
			if(threeDByTwoDlist.size() > i)
			{
				randomNumber = randomGenerator.nextInt(threeDByTwoDlist.size());
				newMathOperationList.add(threeDByTwoDlist.get(randomNumber));
				threeDByTwoDlist.remove(randomNumber);
			}
		}

		for(int i = 0 ; i < 1 ; i++)
		{
			if(threeDByThreeDlist.size() > i)
			{
				randomNumber = randomGenerator.nextInt(threeDByThreeDlist.size());
				newMathOperationList.add(threeDByThreeDlist.get(randomNumber));
				threeDByThreeDlist.remove(randomNumber);
			}
		}

		int newListSize = newMathOperationList.size();
		for(int i = 0 ; i < maxEquation - newListSize ; i ++ )
		{
			if(oneDByOneDlist.size() > 0)
			{
				randomNumber = randomGenerator.nextInt(oneDByOneDlist.size());
				newMathOperationList.add(oneDByOneDlist.get(randomNumber));
				oneDByOneDlist.remove(randomNumber);
			}

			if(twoDByOneDlist.size() > 0)
			{
				randomNumber = randomGenerator.nextInt(twoDByOneDlist.size());
				newMathOperationList.add(twoDByOneDlist.get(randomNumber));
				twoDByOneDlist.remove(randomNumber);
			}

			if(twoDByTwoDlist.size() > 0)
			{
				randomNumber = randomGenerator.nextInt(twoDByTwoDlist.size());
				newMathOperationList.add(twoDByTwoDlist.get(randomNumber));
				twoDByTwoDlist.remove(randomNumber);
			}

			if(threeDByTwoDlist.size() > 0)
			{
				randomNumber = randomGenerator.nextInt(threeDByTwoDlist.size());
				newMathOperationList.add(threeDByTwoDlist.get(randomNumber));
				threeDByTwoDlist.remove(randomNumber);
			}

			if(threeDByThreeDlist.size() > 0)
			{
				randomNumber = randomGenerator.nextInt(threeDByThreeDlist.size());
				newMathOperationList.add(threeDByThreeDlist.get(randomNumber));
				threeDByThreeDlist.remove(randomNumber);
			}
		}

	}



	@Override
	protected void setSign() 
	{
		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "inside setSign()");

		if(isTab){
			if(operator.equals("+"))
				imgSign.setImageResource(R.drawable.ml_plus_tab);
			else if(operator.equals("-"))
				imgSign.setImageResource(R.drawable.ml_minus_tab);
			else if(operator.equals("x"))
				imgSign.setImageResource(R.drawable.ml_multiplication_tab);
			else if(operator.equals("/"))
				imgSign.setImageResource(R.drawable.ml_div_tab);
		}
		else{
			if(operator.equals("+"))
				imgSign.setImageResource(R.drawable.ml_plus_sign);
			else if(operator.equals("-"))
				imgSign.setImageResource(R.drawable.ml_minus_sign);
			else if(operator.equals("x"))
				imgSign.setImageResource(R.drawable.ml_mul_sign);
			else if(operator.equals("/"))
				imgSign.setImageResource(R.drawable.ml_div_sign);
		}

		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "outside setSign()");

	}

	@Override
	protected void createResultLayout(ArrayList<String> listOfDigits,LinearLayout layout) 
	{
		/**
		 * Set the width of linear layout
		 */
		int width = ((noOfDigitsInFisrtNumber + 1) * maxWidth) + 10 ;
		RelativeLayout.LayoutParams lp1 = new RelativeLayout.LayoutParams(width,LayoutParams.WRAP_CONTENT);
		lp1.addRule(RelativeLayout.BELOW,R.id.imgResultLine);
		layout.setLayoutParams(lp1);

		for(i = 0 ; i < listOfDigits.size() ; i ++ )
		{		
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);

			lp.setMargins(2, 0, 0, 0);
			TextView txtView = new TextView(this);
			if(i == listOfDigits.size() - 1)
			{
				//changes for tab
				if(isTab)
					txtView.setBackgroundResource(R.drawable.tab_yellow_ipad);
				else
					txtView.setBackgroundResource(R.drawable.yellow);
			}
			else
			{
				//changes for tab
				if(isTab)
					txtView.setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
				else
					txtView.setBackgroundResource(R.drawable.ml_no1box);
			}

			txtView.setGravity(Gravity.CENTER);

			if(isTab)
				txtView.setTextSize(45);
			else
				txtView.setTextSize(32);

			txtView.setTypeface(null, Typeface.BOLD);
			txtView.setTextColor(Color.parseColor("#3CB3FF"));
			resultTextViewList.add(txtView);//add text view to arrayList
			layout.addView(txtView,lp);

			txtView.setOnClickListener(new OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					for( int  i = 0 ; i < resultTextViewList.size() ; i ++ )
					{
						if(v == resultTextViewList.get(i))
						{
							selectedIndex = i;
							index = i;
							//Log.e(TAG, resultTextViewList.get(i).getHeight() + " Hight");
							//changes for tab
							if(isTab)
								resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
							else
								resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow);
						}
						else
						{
							//changes for tab
							if(isTab)
								resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
							else
								resultTextViewList.get(i).setBackgroundResource(R.drawable.ml_no1box);
						}
					}
				}
			});
		}
	}

	protected void setWidgetsValuesFromtranselation() 
	{	
		if(LEARNING_CENTER_EQUATION_SOLVE_WITH_TIMER)
			Log.e(TAG, "inside setWidgetsValuesFromtranselation()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		txtTopBarText.setText(transeletion.getTranselationTextByTextIdentifier("playTitle"));
		txtPoints.setText(transeletion.getTranselationTextByTextIdentifier("mfLblPts") + ": " + points);
		txtLap.setText(transeletion.getTranselationTextByTextIdentifier("mfLblLap") + ": " + lap);

		warningMessageOnBack = transeletion.getTranselationTextByTextIdentifier("lblYouMustCompleteAllProblems");
		yesTextMsg           = transeletion.getTranselationTextByTextIdentifier("btnTitleYes");
		noTextMsg            = transeletion.getTranselationTextByTextIdentifier("lblNo");

		transeletion.closeConnection();

		if(LEARNING_CENTER_EQUATION_SOLVE_WITH_TIMER)
			Log.e(TAG, "outside setWidgetsValuesFromtranselation()");
	}


	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.btnGo:
			endTime = new Date();
			isClickOnGo = true;		
			if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION)//fraction
			{
				this.clickonGoForFraction();
			}
			else if(operationId == MULTIPLICATION || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x")) 
					|| (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
			{
				this.clickOnGoForMultiplication();
			}
			else if(operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
					||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))
			{
				this.clickOnGoForDivision();
			}
			else
			{
				this.clickOnGo();
			}
			break;
		case R.id.btn1 :
			if(isCarrySelected)
				this.setCarryText("1");
			else
				this.setTextResultValue("1");
			break;
		case R.id.btn2 :
			if(isCarrySelected)
				this.setCarryText("2");
			else
				this.setTextResultValue("2");
			break;
		case R.id.btn3 :
			if(isCarrySelected)
				this.setCarryText("3");
			else
				this.setTextResultValue("3");
			break;
		case R.id.btn4 :
			if(isCarrySelected)
				this.setCarryText("4");
			else
				this.setTextResultValue("4");
			break;
		case R.id.btn5 :
			if(isCarrySelected)
				this.setCarryText("5");
			else
				this.setTextResultValue("5");
			break;
		case R.id.btn6 :
			if(isCarrySelected)
				this.setCarryText("6");
			else
				this.setTextResultValue("6");
			break;
		case R.id.btn7 :
			if(isCarrySelected)
				this.setCarryText("7");
			else
				this.setTextResultValue("7");
			break;
		case R.id.btn8 :
			if(isCarrySelected)
				this.setCarryText("8");
			else
				this.setTextResultValue("8");
			break;
		case R.id.btn9 :
			if(isCarrySelected)
				this.setCarryText("9");
			else
				this.setTextResultValue("9");
			break;
		case R.id.btn0 :
			if(isCarrySelected)
				this.setCarryText("0");
			else
				this.setTextResultValue("0");
			break;
		case R.id.btnDot :
			this.clickonDot();
			break;
		case R.id.btnMinus :
			this.clickOnMinus();
			break;
		case R.id.btnArrowBack :
			this.clickOnBackArrow();
			break;
		case R.id.txtCarry :
			this.clickOnCarry();
			break;
		case R.id.btnRoughWork :
			Intent intent = new Intent(this,WorkAreaActivity.class);
			intent.putExtra("callingActivity", TAG);
			intent.putExtra("startTime", startTimerForRoughArea);
			intent.putExtra("isForHomeWork", isForHomeWork);
			startActivityForResult(intent, 1);
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == 1)
		{
			if(resultCode == RESULT_OK)
			{
				START_TIME = data.getLongExtra("timerStartTime", 0);
				//this.countDawnTimerStart();
			}
		}
	}


	/**
	 * This method is called when the click on Go Button and operation perform on fraction number
	 */
	private void clickonGoForFraction()
	{
		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "inside clickonGoForFraction()");

		index = 0;

		StringBuilder resultValue = new StringBuilder("");
		for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
		{
			resultValue = resultValue.append(resultTextViewList.get(i).getText().toString());
		}

		userAnswer = resultValue.toString();

		if(userAnswer.endsWith("."))
			resultValue.deleteCharAt(userAnswer.length() - 1);

		if((result.replace("/", "").replace(" ", "")).equals(resultValue.toString()))
		{
			playsound.playSoundForRight(CommonUtils.LANGUAGE_CODE, this);

			this.setProgress();

			rightAnsCounter ++ ;
			isCorrectAnsByUser = 1;
			this.savePlayData();
			/*isYellowboxForFraction = false;
			resultTextViewList.clear();
			linearLayoutQuotient.removeAllViews();
			linearLayoutNumerator.removeAllViews();
			linearLayoutDenominator.removeAllViews();
			this.setIndexForShowEquation();
			noOfAttempts = 0;*/
			
			
			//change
			this.keyBoardDisable();
            this.pauseTimer();
			this.showCorrectAns(new OnAnswerGivenComplete() {
				@Override
				public void onComplete() {
					isYellowboxForFraction = false;
					resultTextViewList.clear();
					linearLayoutQuotient.removeAllViews();
					linearLayoutNumerator.removeAllViews();
					linearLayoutDenominator.removeAllViews();
					setIndexForShowEquation();
					noOfAttempts = 0;
					keyBoardEnable();
                    startTimer();
				}
			} , resultTextViewList , FRACTION_DESIGN);
		}
		else
		{
			isCorrectAnsByUser = 0;
			playsound.playSoundForWrong(CommonUtils.LANGUAGE_CODE, this);
			noOfAttempts ++;
			this.setWrongAttemptsImage(noOfAttempts);
            this.keyBoardDisable();
			this.pauseTimer();
            this.visibleWrongImageForFraction();
			this.setHandler();
		}
		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "outside clickonGoForFraction()");
	}


	/**
	 * This method is called when the click on Go Button
	 */
	private void clickOnGo()
	{
		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "inside clickOnGo()");

		index         = noOfDigitsInResult - 1;
		selectedIndex = noOfDigitsInResult - 1;

		StringBuilder resultValue = new StringBuilder("");
		for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
		{
			resultValue = resultValue.append(resultTextViewList.get(i).getText().toString());
		}

		userAnswer = resultValue.toString();

		if(userAnswer.endsWith("."))
			resultValue.deleteCharAt(userAnswer.length() - 1);

		if(resultValue.toString().equals(resultFromDatabase))
		{
			playsound.playSoundForRight(CommonUtils.LANGUAGE_CODE, this);

			this.setProgress();
			rightAnsCounter ++ ;
			isCorrectAnsByUser = 1;
			this.savePlayData();

			//change 22 jan 2015
			this.keyBoardDisable();
            this.pauseTimer();
			this.showCorrectAns(new OnAnswerGivenComplete() {

				@Override
				public void onComplete() {
					resultTextViewList.clear();
					linearLayoutResultRow.removeViews(0, noOfDigitsInResult);
					setIndexForShowEquation();
					noOfAttempts = 0;
					keyBoardEnable();
                    startTimer();
				}
			} , resultTextViewList);
			//end changes

			/*resultTextViewList.clear();
			linearLayoutResultRow.removeViews(0, noOfDigitsInResult);
			this.setIndexForShowEquation();
			noOfAttempts = 0;*/
		}
		else
		{	
			playsound.playSoundForWrong(CommonUtils.LANGUAGE_CODE, this);

			/**
			 * check for reverse result
			 */

			if((resultValue.reverse().toString()).equals(resultFromDatabase))
			{
				txtCarry.setText("");
				txtCarry.setBackgroundResource(R.drawable.ml_no1box);

				for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
				{
					resultTextViewList.get(i).setText("");

					if(i == index)
					{
						//changes for tab
						if(isTab)
							resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
						else
							resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow);
					}
					else
					{
						//changes for tab
						if(isTab)
							resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
						else
							resultTextViewList.get(i).setBackgroundResource(R.drawable.ml_no1box);
					}
				}

				this.setTimerForReverseResult();


			}
			else
			{
				isCorrectAnsByUser = 0;
				noOfAttempts ++;
				this.setWrongAttemptsImage(noOfAttempts);
				this.keyBoardDisable();
                this.pauseTimer();
				visibleWrongImage();
				this.setHandler();
			}
		}

		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "outside clickOnGo()");
	}



	/**
	 * This method call when click on go for multiplication
	 */
	private void clickOnGoForMultiplication() 
	{
		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "inside clickOnGoForMultiplication()");


		StringBuilder resultValue = new StringBuilder("");
		if(this.getDigit(number2).size() > 1)
			startIndex = this.getDigit(number2).size() * numberOfBoxesForMultiplication;

		for( int i = startIndex ; i < resultTextViewList.size() ; i ++ )
		{
			resultValue = resultValue.append(resultTextViewList.get(i).getText().toString());
		}

		userAnswer = resultValue.toString();

		if(userAnswer.endsWith("."))
			resultValue.deleteCharAt(userAnswer.length() - 1);

		if(resultValue.toString().equals(resultFromDatabase))
		{
			playsound.playSoundForRight(CommonUtils.LANGUAGE_CODE, this);

			this.setProgress();

			rightAnsCounter ++ ;
			isCorrectAnsByUser = 1;
			this.savePlayData();
			/*isYellowboxForFraction = false;
			resultTextViewList.clear();
			linearLayoutMultiple.removeAllViews();
			linearLayoutResult.removeAllViews();
			this.setIndexForShowEquation();
			noOfAttempts = 0;*/


			//change
			this.keyBoardDisable();
            this.pauseTimer();
			this.showCorrectAns(new OnAnswerGivenComplete() {
				@Override
				public void onComplete() {
					isYellowboxForFraction = false;
					resultTextViewList.clear();
					linearLayoutMultiple.removeAllViews();
					linearLayoutResult.removeAllViews();
					setIndexForShowEquation();
					noOfAttempts = 0;
					keyBoardEnable();
                    startTimer();
				}
			} , resultTextViewList , MULTIPLICATION_DESIGN);
		}
		else
		{	
			/**
			 * check for reverse result
			 */
			if(resultValue.reverse().toString().equals(resultFromDatabase))
			{
				txtCarry.setText("");
				txtCarry.setBackgroundResource(R.drawable.whitebox_small);

				if(getDigit(number2).size() > 1)
					startIndex = getDigit(number2).size() * numberOfBoxesForMultiplication;

				for( int i = startIndex ; i < resultTextViewList.size() ; i ++ )
				{
					resultTextViewList.get(i).setText("");

					if( i == resultTextViewList.size() - 1)
					{
						//changes for tab
						if(isTab)
							resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
						else
							resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow_small);
					}
					else
					{
						//changes for tab
						if(isTab)
							resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
						else
							resultTextViewList.get(i).setBackgroundResource(R.drawable.whitebox_small);
					}
				}
				index = resultTextViewList.size() - 1;

				this.setTimerForReverseResult(MULTIPLICATION_DESIGN);

			}
			else
			{
				playsound.playSoundForWrong(CommonUtils.LANGUAGE_CODE, this);

				isCorrectAnsByUser = 0;
				noOfAttempts ++;
				this.setWrongAttemptsImage(noOfAttempts);
				this.visibleWrongImageForMultiplication();
				this.keyBoardDisable();
                this.pauseTimer();
				this.setHandler();
			}
		}

		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "outside clickOnGoForMultiplication()");

	}


	/**
	 * This method method call when click on go for division
	 */
	private void clickOnGoForDivision()
	{
		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "inside clickOnGoForDivision()");

		StringBuilder resultValue = new StringBuilder("");

		boolean isStartFromZero = false;
		for( int i = 0 ; i < numberOfBoxesForDivision ; i ++ )
		{
			if(!(resultTextViewList.get(i).getText().toString().equals("0") && isStartFromZero == false))
			{	
				resultValue = resultValue.append(resultTextViewList.get(i).getText().toString());
				isStartFromZero = true;
			}
		}

		userAnswer = resultValue.toString();

		if(userAnswer.endsWith("."))
			resultValue.deleteCharAt(userAnswer.length() - 1);

		if(resultValue.toString().equals(resultFromDatabase))
		{
			playsound.playSoundForRight(CommonUtils.LANGUAGE_CODE, this);

			this.setProgress();

			rightAnsCounter ++ ;
			isCorrectAnsByUser = 1;
			this.savePlayData();
			/*isYellowboxForFraction = false;
			resultTextViewList.clear();
			linearLayoutResult.removeAllViews();
			linearLayoutRowsForDivision.removeAllViews();
			this.setIndexForShowEquation();
			noOfAttempts = 0;*/


			//change
			this.keyBoardDisable();
            this.pauseTimer();
			this.showCorrectAns(new OnAnswerGivenComplete() {
				@Override
				public void onComplete() {
					isYellowboxForFraction = false;
					resultTextViewList.clear();
					linearLayoutResult.removeAllViews();
					linearLayoutRowsForDivision.removeAllViews();
					setIndexForShowEquation();
					noOfAttempts = 0;
					keyBoardEnable();
                    startTimer();
				}
			} , resultTextViewList , DEVISION_DESIGN);

		}
		else
		{			
			playsound.playSoundForWrong(CommonUtils.LANGUAGE_CODE, this);
			isCorrectAnsByUser = 0;
			noOfAttempts ++;
			this.setWrongAttemptsImage(noOfAttempts);
			this.visibleWrongImageForDivision();
			this.keyBoardDisable();
            this.pauseTimer();
			this.setHandler();
		}

		if(LEARNING_CENTER_EQUATION_SOLVE)
			Log.e(TAG, "outsideside clickOnGoForDivision()");
	}



	/**
	 * This method for displaying cross image for wrong question and invisible it
	 */
	private void setHandler()
	{
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() 
		{
			@Override
			public void run() 
			{
				imgWrong.setVisibility(ImageView.INVISIBLE);

				if(operationId == MULTIPLICATION || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
						||(operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
				{
					if(getDigit(number2).size() > 1)
						startIndex = getDigit(number2).size() * numberOfBoxesForMultiplication;

					for( int i = startIndex ; i < resultTextViewList.size() ; i ++ )
					{
						resultTextViewList.get(i).setText("");
						resultTextViewList.get(i).setTextColor(Color.parseColor("#3CB3FF"));
					}
				}
				else if(operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
						||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))
				{
					for( int i = 0; i < numberOfBoxesForDivision ; i ++ )
					{
						resultTextViewList.get(i).setText("");
						resultTextViewList.get(i).setTextColor(Color.parseColor("#3CB3FF"));
					}
				}
				else
				{
					for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
					{		
						resultTextViewList.get(i).setText("");
						resultTextViewList.get(i).setTextColor(Color.parseColor("#3CB3FF"));
					}
				}

				keyBoardEnable();
                startTimer();

				if(noOfAttempts == MAX_ATTEMPTS)
				{						
					//savePlayData();
					if(operationId == MULTIPLICATION || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
							||(operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
					{
						if(getDigit(number2).size() > 1)
							startIndex = getDigit(number2).size() * numberOfBoxesForMultiplication;

						for( int i = startIndex ; i < resultTextViewList.size() ; i ++ )
						{
							try{
								//changes on 22 Jan 2015
								setGreenBackGroundToTextViewForDivision(resultTextViewList.get(i));
								//end changes
								resultTextViewList.get(i).setText(resultDigitsList.get(i - startIndex));
							}catch(IndexOutOfBoundsException ee)
							{
								break;
							}
						}
					}
					else if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION)//fraction
					{
						String compareResult = result.replace("/", "").replace(" ", "");
						for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
						{
							//changes on 22 Jan 2015
							setGreenBackGroundToTextViewForDivision(resultTextViewList.get(i));
							//end changes
							resultTextViewList.get(i).setText(compareResult.charAt(i) + "");
						}
					}
					else if(operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
							||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))
					{
						for( int i = 0 ; i < numberOfBoxesForDivision ; i ++ )
						{
							try{
								//changes on 22 Jan 2015
								setGreenBackGroundToTextViewForDivision(resultTextViewList.get(i));
								//end changes
								resultTextViewList.get(i).setText(resultDigitsList.get(i));
							}catch(IndexOutOfBoundsException ee)
							{
								break;
							}
						}
					}
					else
					{
						for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
						{
							//changes on 22 Jan 2015
							setGreenBackGroundToTextView(resultTextViewList.get(i));
							//end changes
							resultTextViewList.get(i).setText(resultDigitsList.get(i));
						}
					}

					keyBoardDisable();
                    pauseTimer();
					Handler handlerSetResult = new Handler();
					handlerSetResult.postDelayed(new Runnable() 
					{
						@Override
						public void run() 
						{	
							//changes for timer on 11 Mar 2014
							savePlayData();
							//end chages

							if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION)//fraction
							{
								isYellowboxForFraction = false;
								resultTextViewList.clear();
								linearLayoutQuotient.removeAllViews();
								linearLayoutNumerator.removeAllViews();
								linearLayoutDenominator.removeAllViews();
								setIndexForShowEquation();
								noOfAttempts = 0;
								keyBoardEnable();
                                startTimer();
							}
							else if(operationId == MULTIPLICATION 
									|| (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
									|| (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
							{
								isYellowboxForFraction = false;
								resultTextViewList.clear();
								linearLayoutMultiple.removeAllViews();
								linearLayoutResult.removeAllViews();
								txtCarry.setText("");
								txtCarry.setBackgroundResource(R.drawable.ml_no1box);
								setIndexForShowEquation();
								noOfAttempts = 0;
								keyBoardEnable();
                                startTimer();
							}
							else if(operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
									||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))
							{
								isYellowboxForFraction = false;
								resultTextViewList.clear();
								linearLayoutResult.removeAllViews();
								linearLayoutRowsForDivision.removeAllViews();
								txtCarry.setText("");
								txtCarry.setBackgroundResource(R.drawable.ml_no1box);
								setIndexForShowEquation();
								noOfAttempts = 0;
								keyBoardEnable();
                                startTimer();
							}
							else
							{
								resultTextViewList.clear();
								linearLayoutResultRow.removeViews(0, noOfDigitsInResult);
								txtCarry.setText("");
								txtCarry.setBackgroundResource(R.drawable.ml_no1box);
								setIndexForShowEquation();
								noOfAttempts = 0;
								keyBoardEnable();
                                startTimer();
							}
						}
					}, SLEEP_TIME);
				}
				else
				{
					if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION)//fraction
					{
						for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
						{
							if(i == index)
							{							
								//changes for tab
								if(isTab)
									resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
								else
									resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow_small);
							}
							else
							{
								//changes for tab
								if(isTab)
									resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
								else
									resultTextViewList.get(i).setBackgroundResource(R.drawable.whitebox_small);
							}
						}
					}
					else if(operationId == MULTIPLICATION 
							||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
							|| (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
					{
						txtCarry.setText("");
						txtCarry.setBackgroundResource(R.drawable.whitebox_small);

						if(getDigit(number2).size() > 1)
							startIndex = getDigit(number2).size() * numberOfBoxesForMultiplication;

						for( int i = startIndex ; i < resultTextViewList.size() ; i ++ )
						{
							if( i == resultTextViewList.size() - 1)
							{
								//changes for tab
								if(isTab)
									resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
								else
									resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow_small);
							}
							else
							{
								//changes for tab
								if(isTab)
									resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
								else
									resultTextViewList.get(i).setBackgroundResource(R.drawable.whitebox_small);
							}
						}
						index = resultTextViewList.size() - 1;
					}
					else if(operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
							||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))
					{
						txtCarry.setText("");
						txtCarry.setBackgroundResource(R.drawable.ml_no1box);

						for( int i = 0 ; i <  resultTextViewList.size() ; i ++ )
						{
							if(i == index)
							{
								//changes for tab
								if(isTab)
									resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
								else
									resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow_small);
							}
							else
							{
								//changes for tab
								if(isTab)
									resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
								else
									resultTextViewList.get(i).setBackgroundResource(R.drawable.whitebox_small);
							}
						}
					}
					else
					{
						txtCarry.setText("");
						txtCarry.setBackgroundResource(R.drawable.ml_no1box);

						for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
						{
							if(i == index)
							{
								//changes for tab
								if(isTab)
									resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
								else
									resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow);
							}
							else
							{
								//changes for tab
								if(isTab)
									resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
								else
									resultTextViewList.get(i).setBackgroundResource(R.drawable.ml_no1box);
							}
						}
					}
				}
			}
		//}, SLEEP_TIME / 4);
		}, this.getWrongAnswerShowTime(noOfAttempts , SLEEP_TIME));
	}


	/**
	 * This method add the start and end time to solve equation and all the information related to the equation
	 * to the arrayList
	 */
	private void savePlayData()
	{	
		//changes for timer on 11 Mar
		endTime = new Date();
		//end chages

		MathScoreTransferObj mathObj = new MathScoreTransferObj();
		mathObj.setMathOperationID(operationId);
		mathObj.setEquationId(equationId);
		mathObj.setLap(lap);
		mathObj.setStartDateTimeStr(CommonUtils.formateDate(startTime));
		mathObj.setEndDateTimeStr(CommonUtils.formateDate(endTime));
		mathObj.setPoints(pointsForEachQuestion);

		if(userAnswer.length() > 0)
			mathObj.setAnswer(userAnswer);
		else
			mathObj.setAnswer("1");

		mathObj.setAnswerCorrect(isCorrectAnsByUser);

		startTimeValue = startTime.getSeconds();
		endTimeVlaue   = endTime.getSeconds();

		if(endTimeVlaue < startTimeValue)
			endTimeVlaue = endTimeVlaue + 60;

		/*Log.e(TAG, "time taken end time ,starttime, diff " +  endTimeVlaue + "," 
				+ startTimeValue + "," +(endTimeVlaue - startTimeValue));*/

		mathObj.setTimeTakenToAnswer(endTimeVlaue - startTimeValue);
		mathObj.setEquation(learnignCenterobj);

		pointsForEachQuestion = 0;

		playDatalist.add(mathObj);
		//userAnswerList.add(userAnswer);

		//changes for answerScreen
		if(isCorrectAnsByUser == 1)
			userAnswerList.add(learnignCenterobj.getProductStr());
		else
			userAnswerList.add(userAnswer);
		//end changes

		//added by shilpi for friendzy
		totalTimeTaken = totalTimeTaken + (endTimeVlaue - startTimeValue);
	}


	/**
	 * This method convert the data for solving the equation into xml format
	 */
	private String getEquationSolveXml(ArrayList<MathScoreTransferObj> playDatalist)
	{

		StringBuilder xml = new StringBuilder("");

		for( int i = 0 ;  i < playDatalist.size() ; i++)
		{
			//Log.e(TAG, "inside getEquationSolveXml time taken to solve " + playDatalist.get(i).getTimeTakenToAnswer());
			xml.append("<equation>" +
					"<equationId>"+playDatalist.get(i).getEquationId()+"</equationId>"+
					"<start_date_time>"+playDatalist.get(i).getStartDateTimeStr()+"</start_date_time>"+
					"<end_date_time>"+playDatalist.get(i).getEndDateTimeStr()+"</end_date_time>"+
					"<lap>"+playDatalist.get(i).getLap()+"</lap>"+
					"<time_taken_to_answer>"+playDatalist.get(i).getTimeTakenToAnswer()+"</time_taken_to_answer>"+
					"<math_operation_id>"+playDatalist.get(i).getMathOperationID()+"</math_operation_id>"+
					"<points>"+playDatalist.get(i).getPoints()+"</points>"+
					"<isAnswerCorrect>" + playDatalist.get(i).getAnswerCorrect() + "</isAnswerCorrect>"+
					"<user_answer>"+playDatalist.get(i).getAnswer()+"</user_answer>"+
					"</equation>");
		}

		return xml.toString();
	}


	@Override
	public void onBackPressed(){

		if(!isForHomeWork){
			timer.cancel();
			isClickOnBackPressed = false;
			this.callOnBackPressed();
			super.onBackPressed();
		}else{//for home work
			if(showEquationIndex > 0){
				MathFriendzyHelper.yesNoConfirmationDialog
				(this, warningMessageOnBack, yesTextMsg, noTextMsg, new YesNoListenerInterface() {
					@Override
					public void onYes() {

					}

					@Override
					public void onNo() {
						isClickOnBackPressed = false;
						callOnBackPressed();
						callSuperOnBackPressed();
					}
				});
			}else{
				super.onBackPressed();
			}
		}
	}

	/**
	 * Call super back pressed method
	 */
	private void callSuperOnBackPressed(){
		super.onBackPressed();
	}

	/**
	 * This method call when user click on back pressed and also after the completion of time counter
	 */
	private void callOnBackPressed()
	{	
		//changs for correct time on result screen , on 07 Mar 2014
		if(!isClickOnBackPressed){
			totalTime = this.getNewMathPlayedData((TOTAL_TIME_TO_PLAY - (int)(startTimerForRoughArea/1000)) , playDatalist);
		}
		//end changes

		numberOfCoins = (int) (points * ICommonUtils.COINS_PER_POINT);

		SharedPreferences sheredPreference = this.getSharedPreferences(LOGIN_SHARED_PREFF, 0);	
		if(!sheredPreference.getBoolean(IS_LOGIN, false))
		{			
			if(isClickOnGo)
			{
				this.inserPlayDataIntodatabase(savePlayDataToDataBase());
				Intent intent = new Intent();
				setResult(RESULT_OK,intent);
			}
		}
		else
		{			
			//changes for update player points in local when Internet is not connected
			//updated on 26 11 2014 
			MathResultTransferObj mathResultObj = savePlayDataToDataBase();
			mathResultObj.setLevel(MathFriendzyHelper.getPlayerCompleteLavel
					(mathResultObj.getPlayerId(), this));
			MathFriendzyHelper.insertPlayerPlayedDataIntoLocalEarnedScore(mathResultObj, this);
			//end updation on 26 11 2014

			if(CommonUtils.isInternetConnectionAvailable(this))
			{
				//startActivity(new Intent(this , NewLearnignCenter.class));

				if(!isClickOnBackPressed)
				{
					this.insertIntoPlayerTotalPoints(savePlayDataToDataBase());
					new SaveTempPlayerScoreOnServer(savePlayDataToDataBase())
					.execute(null,null,null);
				}
				else
				{
					this.insertIntoPlayerTotalPoints(savePlayDataToDataBase());
				}

				//added by shilpi for friendzy
				if(CommonUtils.isActivePlayer){
					/*new SaveTimePointsForFriendzyChallenge(totalTimeTaken, points, this)
					.execute(null, null, null);*/
					new SaveTimePointsForFriendzyChallenge(totalTime, points, this)
					.execute(null, null, null);
				}	
			}
			else
			{				
				this.inserPlayDataIntodatabase(savePlayDataToDataBase());
				/*DialogGenerator dg = new DialogGenerator(this);
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
				transeletion.closeConnection();*/
			}
		}
	}


	/**
	 * This method save play data to the database(Math_Result table)
	 */
	private MathResultTransferObj savePlayDataToDataBase()
	{
		MathResultTransferObj mathResultObj = new MathResultTransferObj();
		mathResultObj.setRoundId(0);
		mathResultObj.setIsFirstRound("0");
		mathResultObj.setGameType("Play");
		mathResultObj.setIsWin(0);

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		if(sharedPreffPlayerInfo.getString("userId", "").equals("0"))
			mathResultObj.setUserId("0");
		else
			mathResultObj.setUserId(sharedPreffPlayerInfo.getString("userId", ""));

		if(sharedPreffPlayerInfo.getString("playerId", "").equals("0"))
			mathResultObj.setPlayerId("0");
		else
			mathResultObj.setPlayerId(sharedPreffPlayerInfo.getString("playerId", ""));

		mathResultObj.setIsFakePlayer(0);
		mathResultObj.setProblems(this.getEquationSolveXml(playDatalist));

		mathResultObj.setTotalScore(points);

		mathResultObj.setCoins(numberOfCoins);

		return mathResultObj;
	}


	/**
	 * This method store data into database for Math_result
	 * @param mathResultObj
	 */
	private void inserPlayDataIntodatabase(MathResultTransferObj mathResultObj )
	{
		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();
		learningCenterObj.insertIntoMathResult(mathResultObj);
		learningCenterObj.closeConn();
		this.insertIntoPlayerTotalPoints(mathResultObj);
	}


	/**
	 * This method insert the data into total player points table
	 * @param mathResultObj
	 */
	private void insertIntoPlayerTotalPoints(MathResultTransferObj mathResultObj)
	{
		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();
		PlayerTotalPointsObj playerPoints = this.getPlayerPoints(mathResultObj);

		if(learningCenterObj.isPlayerTotalPointsExist(playerPoints.getPlayerId()))
		{
			int points = playerPoints.getTotalPoints();
			int coins  = playerPoints.getCoins();
			playerPoints.setTotalPoints(learningCenterObj.getDataFromPlayerTotalPoints(playerPoints.getPlayerId()).getTotalPoints()
					+ points);

			playerPoints.setCoins(learningCenterObj.getDataFromPlayerTotalPoints(playerPoints.getPlayerId()).getCoins()
					+ coins);

			learningCenterObj.deleteFromPlayerTotalPoints(playerPoints.getPlayerId());
		}

		learningCenterObj.insertIntoPlayerTotalPoints(playerPoints);


		learningCenterObj.closeConn();
	}


	/**
	 * This method return the player points with its user id and player id and also coins
	 * @return
	 */
	private PlayerTotalPointsObj getPlayerPoints(MathResultTransferObj mathResultObj)
	{
		PlayerTotalPointsObj playerPoints = new PlayerTotalPointsObj();
		playerPoints.setUserId(mathResultObj.getUserId());
		playerPoints.setPlayerId(mathResultObj.getPlayerId());

		playerPoints.setTotalPoints(points);


		//numberOfCoins = (int) (points * 0.05);

		playerPoints.setCoins(numberOfCoins);
		playerPoints.setCompleteLevel(1);
		playerPoints.setPurchaseCoins(0);

		return playerPoints;
	}

	/**
	 * Save Temp Player result on server
	 * @author Yashwant Singh
	 *
	 */
	class SaveTempPlayerScoreOnServer extends AsyncTask<Void, Void, Void>
	{

		private MathResultTransferObj mathobj = null;

		SaveTempPlayerScoreOnServer(MathResultTransferObj mathObj)
		{			
			this.mathobj = mathObj;
		}

		@Override
		protected void onPreExecute() 
		{			
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			Register register = new Register(LearningCenterEquationSolveWithTimer.this);
			register.savePlayerScoreOnServerForloginuser(mathobj);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			super.onPostExecute(result);
			new AddCoinAndPointsForLoginUser(getPlayerPoints(mathobj)).execute(null,null,null);
		}
	}

	/**
	 * This asyncTask Add points and coins on server
	 * @author Yashwant Singh
	 *
	 */
	class AddCoinAndPointsForLoginUser extends AsyncTask<Void, Void, Void> 
	{		
		private PlayerTotalPointsObj playerObj = null;
		AddCoinAndPointsForLoginUser(PlayerTotalPointsObj playerObj)
		{
			this.playerObj = playerObj;

			LearningCenterimpl learningCenterimpl = new LearningCenterimpl(LearningCenterEquationSolveWithTimer.this);
			learningCenterimpl.openConn();
			playerObj = learningCenterimpl.getDataFromPlayerTotalPoints(playerObj.getPlayerId());
			learningCenterimpl.closeConn();	

			this.playerObj.setTotalPoints(playerObj.getTotalPoints());
			this.playerObj.setCoins(playerObj.getCoins());
		}

		@Override
		protected void onPreExecute() 
		{			
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			Register register = new Register(LearningCenterEquationSolveWithTimer.this);
			register.addPointsAndCoinsOnServerForloginUser(playerObj);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			super.onPostExecute(result);

		}
	}

	/**
	 * Save Temp Player result on server
	 * @author Yashwant Singh
	 *
	 */
	class AddMathPlayerLavelScoreOnServer extends AsyncTask<Void, Void, Void>
	{
		private MathResultTransferObj mathobj = null;

		AddMathPlayerLavelScoreOnServer(MathResultTransferObj mathObj)
		{
			PlayerTotalPointsObj playerObj = null;
			LearningCenterimpl learningCenterimpl = new LearningCenterimpl(LearningCenterEquationSolveWithTimer.this);
			learningCenterimpl.openConn();
			playerObj = learningCenterimpl.getDataFromPlayerTotalPoints(mathObj.getPlayerId());
			learningCenterimpl.closeConn();	

			this.mathobj = mathObj;
			this.mathobj.setCoins(playerObj.getCoins());
			this.mathobj.setTotalPoints(playerObj.getTotalPoints());
		}

		@Override
		protected void onPreExecute() 
		{			
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			Register register = new Register(LearningCenterEquationSolveWithTimer.this);
			register.addMathPlayLevelScore(mathobj);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			super.onPostExecute(result);
		}
	}
}

