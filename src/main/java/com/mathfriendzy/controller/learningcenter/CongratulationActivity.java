package com.mathfriendzy.controller.learningcenter;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore.Images;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.controller.learningcenter.schoolcurriculum.SeeAnswerActivityForSchoolCurriculum;
import com.mathfriendzy.controller.top100.Top100Activity;
import com.mathfriendzy.facebookconnect.ShareActivity;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.MathEquationTransferObj;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ICommonUtils;
import com.mathfriendzy.utils.MathVersions;
import com.mathfriendzy.utils.PlaySound;

import java.util.ArrayList;

import static com.mathfriendzy.utils.ICommonUtils.BTN_COM_LINK_URL;

public class CongratulationActivity extends ActBaseClass implements OnClickListener
{
	private TextView txtTileScreen				= null;
	private TextView txtGoodEffort				= null;
	private TextView txtEarned					= null;
	private TextView txtEarnedPoints			= null;
	private TextView txtPlayAgain				= null;
	private Button	 btnAnswer					= null;
	private Button	 btnPlay					= null;
	private Button	 btnCom						= null;
	private Button	 btnShare					= null;
	private String   lblPts						= null;
	
	private Button btnCloseShareTool		= null;
	private RelativeLayout layoutShare		= null;
	private Button btnScreenShot			= null;
	private Button btnEmail					= null;
	private Button btnTwitter				= null;
	private Button btnFbShare				= null;
	
	private String subject					= null;
	private String body						= null;
	private String screenText				= null;
	private int numberOfStarsYouGet         = 0;
	private int ponits                      = 0;
	
	private boolean isFromLearnignCenterSchoolCurriculum = false;

	//changes for Spanish
	private int selectedPlayGrade = 1;
	
	PlaySound playsound = null;

    //For MathVersion Change
    private ImageView imgFriendzy = null;
    private TextView lblTheGameOfMath = null;
    private String lblTheGameOfMathMsg = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_congratulation);
		
		playsound = new PlaySound(this);
		
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);  
		  
		if(metrics.heightPixels == ICommonUtils.TAB_HEIGHT && metrics.widthPixels == ICommonUtils.TAB_WIDTH && metrics.densityDpi == ICommonUtils.TAB_DENISITY)
		{
			setContentView(R.layout.activity_congratulation_tab_low_denisity);
		}
		
		this.getWidgetId();
		this.getIntentValue();
		this.setWidgetText();
        this.setLogoTextBasedOnMathVersion();

	}//END onCreate Method
	
	private void getIntentValue() 
	{	
		ponits              = this.getIntent().getIntExtra("points", 0);
		numberOfStarsYouGet = this.getIntent().getIntExtra("stars", 0);
		isFromLearnignCenterSchoolCurriculum = this.getIntent().
				getBooleanExtra("isFromLearnignCenterSchoolCurriculum", false);
		selectedPlayGrade = this.getIntent().getIntExtra("selectedGrade", 1);
		
		if(isFromLearnignCenterSchoolCurriculum)
			playsound.playSoundForSchoolSurriculumForResultScreen(this);
		
	}

	private void setWidgetText() 
	{
		String text;
		Translation translate = new Translation(this);
		translate.openConnection();

		text = translate.getTranselationTextByTextIdentifier("mfTitleHomeScreen");
		txtTileScreen.setText(text);
		
		if(numberOfStarsYouGet > 0)
			text = translate.getTranselationTextByTextIdentifier("titleGreatJob");
		else
			text = translate.getTranselationTextByTextIdentifier("lblGoodEffort");
		
		txtGoodEffort.setText(text + "!");
		
		text = translate.getTranselationTextByTextIdentifier("lblYouEraned");
		txtEarned.setText(text);
		
		lblPts = translate.getTranselationTextByTextIdentifier("lblPts");
		txtEarnedPoints.setText(CommonUtils.setNumberString(ponits + "") + " " + lblPts);
		
		//txtEarnedPoints.setText(this.getIntent().getIntExtra("points", 0) + " " + lblPts);
		
		if(numberOfStarsYouGet > 0){
			text = translate.getTranselationTextByTextIdentifier("lblCongratulations");
			if(isFromLearnignCenterSchoolCurriculum)
				text = text + ", " + translate.getTranselationTextByTextIdentifier("lblYouCanNowPlayTheNextSubCategory");
			else
				text = text + ", " + translate.getTranselationTextByTextIdentifier("lblYouCanNowPlayTheNextLevel");
		}
		else{
			if(isFromLearnignCenterSchoolCurriculum)
				text = translate.getTranselationTextByTextIdentifier("lblPlayAgainToMoveToNextSubCategory");
			else
				text = translate.getTranselationTextByTextIdentifier("lblPlayAgainToMoveToNextLevel");
		}
		
		txtPlayAgain.setText(text);
		
		text = translate.getTranselationTextByTextIdentifier("mfLblAnswers");
		btnAnswer.setText(text);
		
		text = translate.getTranselationTextByTextIdentifier("btnTitleCom");
		btnCom.setText(text);
		
		text = translate.getTranselationTextByTextIdentifier("playTitle");
		btnPlay.setText(text);
		
		text = translate.getTranselationTextByTextIdentifier("btnTitleShare");
		btnShare.setText(text);
		
		screenText 	= translate.getTranselationTextByTextIdentifier(
				"alertMsgAScreenShotHasBeenSavedToTheCameraRoll");
		subject		= translate.getTranselationTextByTextIdentifier("infoEmailSubject");
		body		= translate.getTranselationTextByTextIdentifier("shareFBEmailMessage");

        lblTheGameOfMathMsg = translate.getTranselationTextByTextIdentifier("lblTheGameOfMath");
		translate.closeConnection();
		
	}//END setWidgetText method


	private void getWidgetId() 
	{
		txtTileScreen		= (TextView) findViewById(R.id.txtTitleScreen);		
		txtGoodEffort		= (TextView) findViewById(R.id.txtGoodEffort);
		txtEarned			= (TextView) findViewById(R.id.txtEarned);
		txtEarnedPoints		= (TextView) findViewById(R.id.txtEarnedPoints);
		txtPlayAgain        = (TextView) findViewById(R.id.txtPlayAgain);
		
		btnAnswer			= (Button) findViewById(R.id.btnAnswer);
		btnPlay				= (Button) findViewById(R.id.btnPlay);
		btnCom				= (Button) findViewById(R.id.btnCom);
		btnShare			= (Button) findViewById(R.id.btnShare);
		
		btnCloseShareTool	= (Button) findViewById(R.id.btnCloseShareToolbar);
		layoutShare			= (RelativeLayout) findViewById(R.id.layoutShare);
		btnEmail			= (Button) findViewById(R.id.btnMail);
		btnFbShare			= (Button) findViewById(R.id.btnFbSahre);
		btnScreenShot		= (Button) findViewById(R.id.btnScreenShot);
		btnTwitter			= (Button) findViewById(R.id.btnTwitterShare);
		
		btnAnswer.setOnClickListener(this);
		btnCom.setOnClickListener(this);
		btnPlay.setOnClickListener(this);
		btnShare.setOnClickListener(this);
		
		btnCloseShareTool.setOnClickListener(this);
		btnEmail.setOnClickListener(this);
		btnFbShare.setOnClickListener(this);
		btnScreenShot.setOnClickListener(this);
		btnTwitter.setOnClickListener(this);

        imgFriendzy = (ImageView) findViewById(R.id.imgFriendzy);
        lblTheGameOfMath = (TextView) findViewById(R.id.lblTheGameOfMath);
	}//END getWidgetId method

	

	@SuppressWarnings("unchecked")
	@Override
	public void onClick(View v) 
	{
		Intent intent = null;	
		Translation transeletion = null;
		/*View view = getWindow().getDecorView().getRootView();
		view.setDrawingCacheEnabled(true);
		layoutShare.setVisibility(View.GONE);
		btnCloseShareTool.setVisibility(View.GONE);
		Bitmap b = view.getDrawingCache();//capture image
		 */		
		Bitmap b = null;
		switch (v.getId())
		{
		case R.id.btnAnswer:
			if(isFromLearnignCenterSchoolCurriculum){
				/*DialogGenerator dg = new DialogGenerator(this);
				dg.generateWarningDialog("Under Developement!");*/
				ArrayList<MathEquationTransferObj> playDataList = (ArrayList<MathEquationTransferObj>) this.getIntent()
						.getSerializableExtra("playDatalist");
				Intent intent1 = new Intent(this , SeeAnswerActivityForSchoolCurriculum.class);
				intent1.putExtra("playDatalist", playDataList);
				intent1.putExtra("isFromLearnignCenterSchoolCurriculum", true);
				intent1.putExtra("selectedGrade", selectedPlayGrade);
				startActivity(intent1);
			}
			else
				startActivity(new Intent(this,SeeAnswerActivity.class));
			break;
		
		case R.id.btnPlay:
			this.clickOnPlay();
			break;
			
		case R.id.btnCom:
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(BTN_COM_LINK_URL)));
			break;
			
		case R.id.btnShare:
			layoutShare.setVisibility(View.VISIBLE);
			btnCloseShareTool.setVisibility(View.VISIBLE);
			break;

		case R.id.btnCloseShareToolbar:
			layoutShare.setVisibility(View.GONE);
			btnCloseShareTool.setVisibility(View.GONE);
			break;

		case R.id.btnScreenShot:
			
			b = this.captureImage();
			CommonUtils.saveBitmap(b, "DCIM/Camera", "screen");//save image into file manager

			DialogGenerator generator = new DialogGenerator(this);
			transeletion = new Translation(this);
			transeletion.openConnection();
			generator.generateWarningDialog(screenText);
			transeletion.closeConnection();

			break;

		case R.id.btnMail:
			//*******For Sharing ScreenShot ******************//
			b = this.captureImage();
			String path = Images.Media.insertImage(getContentResolver(), b,"ScreenShot.jpg", null);
			Uri screenshotUri = Uri.parse(path);
			Intent emailIntent = new Intent(     android.content.Intent.ACTION_SEND);

			emailIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
			emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
			emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, body+" "+ MathFriendzyHelper.getAppRateUrl(this));
			emailIntent.setType("image/png");
			startActivity(Intent.createChooser(emailIntent, "Send email using"));

			break;

		case R.id.btnFbSahre:	
			Top100Activity.b = this.captureImage();
			intent = new Intent(this, ShareActivity.class);
			intent.putExtra("message", body+" "+MathFriendzyHelper.getAppRateUrl(this));
			//intent.putExtra("bitmap", b);
			//intent.putExtra("bitmap", CommonUtils.scaleDownBitmap(b, 50, this));
			intent.putExtra("flag", false);
			intent.putExtra("id", R.id.btnFbSahre);
			startActivity(intent);
			break;

		case R.id.btnTwitterShare:	
			Top100Activity.b = this.captureImage();
			intent = new Intent(this, ShareActivity.class);
			intent.putExtra("message", body);
			intent.putExtra("flag", false);
			//intent.putExtra("bitmap", b);
			//intent.putExtra("bitmap", CommonUtils.scaleDownBitmap(b, 50, this));
			intent.putExtra("id", R.id.btnTwitterShare);
			startActivity(intent);
			break;
		}
		
	}//END onClick Method


	private Bitmap captureImage()
	{	
		View view = getWindow().getDecorView().getRootView();
		view.setDrawingCacheEnabled(true);
		layoutShare.setVisibility(View.GONE);
		btnCloseShareTool.setVisibility(View.GONE);
		Bitmap b = view.getDrawingCache();
		
		return b;
	}
	
	
	private void clickOnPlay() 
	{
		startActivity(new Intent(this,NewLearnignCenter.class));
		/*SharedPreferences sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
		int operationId = sharedPreferences.getInt("operationId", 0);
		
		switch(operationId)
		{
		case 1:
			startActivity(new Intent(this,Addition.class));
			break;
		case 2:
			startActivity(new Intent(this,Subtraction.class));
			break;
		case 3:
			startActivity(new Intent(this,Multiplication.class));
			break;
		case 4:
			startActivity(new Intent(this,Division.class));
			break;
		case 5:
			startActivity(new Intent(this,AdditionSubtractionOfFraction.class));
			break;
		case 6:
			startActivity(new Intent(this,MultiplicationDivisionOfFraction.class));
			break;
		case 7:
			startActivity(new Intent(this,AdditionSubtractionOfDecimal.class));
			break;
		case 8:
			startActivity(new Intent(this,MultiplicationDivisionOfDecimal.class));
			break;
		case 9:
			startActivity(new Intent(this,AdditionSubtractionOfNegativeNumber.class));
			break;
		case 10:
			startActivity(new Intent(this,MultiplicationDivisionOfNegativeNumber.class));
			break;
		}*/
		
	}

	@Override
	public void onBackPressed() 
	{
		Intent intent = new Intent(this,NewLearnignCenter.class);
		startActivity(intent);
		super.onBackPressed();
	}
	
	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}

    private void setLogoTextBasedOnMathVersion() {
        if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_SIMPLE){
            imgFriendzy.setBackgroundResource(R.drawable.tab_mf_maths_friendzy_ipad);
            lblTheGameOfMath.setText(lblTheGameOfMathMsg);
        }else if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS){
            imgFriendzy.setBackgroundResource(R.drawable.mathfriendzy__plus_logo);
            lblTheGameOfMath.setText(lblTheGameOfMathMsg);
        }else if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_TUTOR_PLUS){
            imgFriendzy.setBackgroundResource(R.drawable.mathfriendzy_tutor_plus_logo);
            lblTheGameOfMath.setText(lblTheGameOfMathMsg);
        }
    }
}
