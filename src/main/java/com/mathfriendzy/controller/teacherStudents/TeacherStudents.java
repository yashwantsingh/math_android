package com.mathfriendzy.controller.teacherStudents;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.assessmenttest.report.ReportForPracticeAndAssessment;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.controller.friendzy.TeacherChallengeActivity;
import com.mathfriendzy.controller.player.CreateTeacherPlayerActivity;
import com.mathfriendzy.controller.player.TeacherPlayer;
import com.mathfriendzy.controller.result.ResultActivity;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.model.result.JsonAsyncTaskForScore;
import com.mathfriendzy.model.teacherStudents.GetTeacherStudents;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import static com.mathfriendzy.utils.ICommonUtils.TAECHER_STUDENT_FLAG;

/**
 * This Activity Shows the teacher user player when user login as a teacher
 * @author Yashwant Singh
 *
 */
public class TeacherStudents extends ActBaseClass implements OnClickListener
{
	private TextView resultTitleMyStudents = null;
	private Button   btnTitleBack          = null;
	//private ListView lstTeacherStudents    = null;
	private String callingActivity         = null;

	private ProgressDialog progressDialog  = null;

	private TextView txtStudentdName   = null;
	private ImageView imgStudentDetail = null;
	private TextView  txtLine          = null;
	private ArrayList<RelativeLayout> layoutList = null;
	private final String TAG = this.getClass().getSimpleName();
	private int offSet = 0;

	//for show by Grade , on Apr 08 2014
	private TextView txtGrade 		= null;
	private Spinner  spinnerGrade 	= null;
	private Button   btnShowMore  	= null;
	private LinearLayout students   = null;
	private ArrayList<UserPlayerDto> finalStudentList = null;
	private String gradeSelectedValue = "All";//by default All is selected changes in setGradeAdapterData() method

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_teacher_student_layout);

		if(TAECHER_STUDENT_FLAG)	
			Log.e(TAG, "inside onCreate()");

		finalStudentList = new ArrayList<UserPlayerDto>();

		callingActivity = this.getIntent().getStringExtra("callingActivity");

		this.setWidgetsReferences();
		this.setTextValues();
		this.setListenerOnWidgets();

		layoutList = new ArrayList<RelativeLayout>();

		UserRegistrationOperation userObj = new UserRegistrationOperation(this);
		RegistereUserDto regUserObj = userObj.getUserData();

		if(CommonUtils.isInternetConnectionAvailable(this))
			new GetTeacherStudentsAsyncTask(regUserObj.getUserId(),offSet).execute(null,null,null);
		else
		{
			DialogGenerator dg = new DialogGenerator(this);
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
			transeletion.closeConnection();
		}

		if(TAECHER_STUDENT_FLAG)	
			Log.e(TAG, "outside onCreate()");
	}

	/**
	 * This method set the widgets references from the layout to the widgets objects
	 */
	private void setWidgetsReferences() 
	{
		if(TAECHER_STUDENT_FLAG)	
			Log.e(TAG, "inside setWidgetsReferences()");

		resultTitleMyStudents = (TextView) findViewById(R.id.resultTitleMyStudents);
		btnTitleBack          = (Button)   findViewById(R.id.btnTitleBack);

		//for show by Grade , on Apr 08 2014
		txtGrade              = (TextView) findViewById(R.id.txtGrade);
		spinnerGrade          = (Spinner) findViewById(R.id.spinnerGrade);
		btnShowMore           = (Button) findViewById(R.id.btnShowMore);
		students 			  = (LinearLayout) findViewById(R.id.lstTeacherStudents);

		if(TAECHER_STUDENT_FLAG)	
			Log.e(TAG, "outside setWidgetsReferences()");
	}

	/**
	 * This method set the widgets text values from the Translation
	 */
	private void setTextValues()
	{
		if(TAECHER_STUDENT_FLAG)	
			Log.e(TAG, "inside setTextValues()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		resultTitleMyStudents.setText(transeletion.getTranselationTextByTextIdentifier("resultTitleMyStudents"));
		btnTitleBack.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBack"));
		txtGrade.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade") + ":");
		btnShowMore.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleShowMore"));
		transeletion.closeConnection();

		if(TAECHER_STUDENT_FLAG)	
			Log.e(TAG, "outside setTextValues()");
	}

	/**
	 * this method set the listener on widgets
	 */
	private void setListenerOnWidgets() 
	{
		if(TAECHER_STUDENT_FLAG)	
			Log.e(TAG, "inside setListenerOnWidgets()");

		btnTitleBack.setOnClickListener(this);	

		//for show by Grade , on Apr 08 2014
		btnShowMore.setOnClickListener(this);
		//end changes

		if(TAECHER_STUDENT_FLAG)	
			Log.e(TAG, "outside setListenerOnWidgets()");
	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.btnTitleBack :
			if(callingActivity.equals("TeacherPlayer"))
			{
				Intent intent = new Intent(this,TeacherPlayer.class);
				startActivity(intent);
			}
			else if(callingActivity.equals("CreateTeacherPlayerActivity"))
			{
				Intent intent = new Intent(this,CreateTeacherPlayerActivity.class);
				startActivity(intent);
			}
			break;
			//for show by Grade , on Apr 08 2014
		case R.id.btnShowMore :
			UserRegistrationOperation userObj = new UserRegistrationOperation(TeacherStudents.this);
			RegistereUserDto regUserObj = userObj.getUserData();
			if(CommonUtils.isInternetConnectionAvailable(TeacherStudents.this)){					
				offSet = offSet + 30;
				new GetTeacherStudentsAsyncTask(regUserObj.getUserId(),offSet).execute(null,null,null);
			}
			else{
				DialogGenerator dg = new DialogGenerator(TeacherStudents.this);
				Translation transeletion = new Translation(TeacherStudents.this);
				transeletion.openConnection();
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
				transeletion.closeConnection();
			}
			break;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
		{
			if(callingActivity.equals("TeacherPlayer"))
			{
				Intent intent = new Intent(this,TeacherPlayer.class);
				startActivity(intent);
			}
			else if(callingActivity.equals("CreateTeacherPlayerActivity"))
			{
				Intent intent = new Intent(this,CreateTeacherPlayerActivity.class);
				startActivity(intent);
			}
			else if(callingActivity.equals("TeacherChallengeActivity"))
			{ 
				Intent intent = new Intent(this,TeacherChallengeActivity.class);
				startActivity(intent);
			}else if(callingActivity.equals("ReportForPracticeAndAssessment")){
				Intent intent = new Intent(this,ReportForPracticeAndAssessment.class);
				startActivity(intent);
			}
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}



	/**
	 * This AsyncTask Get Teacher Student from the server 
	 * @author Yashwant Singh
	 *
	 */
	class GetTeacherStudentsAsyncTask extends AsyncTask<Void, Void, Void>
	{
		private String userId = null;
		private int offset    = 0;
		private ArrayList<UserPlayerDto> studentList = null;

		GetTeacherStudentsAsyncTask(String userId,int offset)
		{
			this.userId = userId;
			this.offset = offset;
		}

		@Override
		protected void onPreExecute() 
		{
			progressDialog = CommonUtils.getProgressDialog(TeacherStudents.this);
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			GetTeacherStudents teacherStudentObj = new GetTeacherStudents(TeacherStudents.this);
			studentList = teacherStudentObj.getStudents(userId, offset);

			finalStudentList.addAll(studentList);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			progressDialog.cancel();

			//for show by Grade , on Apr 08 2014
			if(studentList.size() >= 30)
				btnShowMore.setVisibility(Button.VISIBLE);
			else
				btnShowMore.setVisibility(Button.GONE);

			ArrayList<String> gradeList = new ArrayList<String>();
			gradeList.add("All");
			
			ArrayList<Integer> integerGradeList = new ArrayList<Integer>();
			ArrayList<String> studentnameList = new ArrayList<String>();
			for(int i = 0 ; i < finalStudentList.size() ; i ++ ){

				try{
					int grade = Integer.parseInt(finalStudentList.get(i).getGrade());
					if(!integerGradeList.contains(grade))
						integerGradeList.add(grade);
				}catch(Exception e){
					Log.e(TAG, "inside onPost() GetTeacherStudentsAsyncTask : " +
							"Error while parsing grade from String to Integr " + e.toString());
				}

				//old code
				studentnameList.add(finalStudentList.get(i).getFirstname() + " " + finalStudentList.get(i).getLastname());
			}

			Collections.sort(integerGradeList);
			for(int i = 0 ; i < integerGradeList.size() ; i ++ )
				gradeList.add(integerGradeList.get(i) + "");

			/*StudentAdapter adapter = new StudentAdapter(TeacherStudents.this,R.layout.studentslistlayout,studentnameList);
			lstTeacherStudents.setAdapter(adapter);*/
			//createDynamicLayoutForStudents(studentnameList , finalStudentList);
			
			setGradeAdapterData(gradeList , studentnameList , finalStudentList , gradeSelectedValue);
			//end changes

			super.onPostExecute(result);
		}
	}

	//for show by Grade , on Apr 08 2014
	/**
	 * This method set the adapter data
	 * @param gradeList
	 * @param studentNameList
	 * @param studentList
	 * @param gradeSelectedValueByUser
	 */
	private void setGradeAdapterData(ArrayList<String> gradeList,
			final ArrayList<String> studentNameList ,  final ArrayList<UserPlayerDto> studentList
			, String gradeSelectedValueByUser){

		ArrayAdapter<String> gradeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,gradeList);
		gradeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerGrade.setAdapter(gradeAdapter);
		spinnerGrade.setSelection(gradeAdapter.getPosition(gradeSelectedValueByUser));

		spinnerGrade.setOnItemSelectedListener(new OnItemSelectedListener(){	
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
				((TextView) parent.getChildAt(0)).setTextSize(15);

				String selectedGrade = spinnerGrade.getSelectedItem().toString();

				if(selectedGrade.equals("All")){
					createDynamicLayoutForStudents(studentNameList , studentList);
				}else{
					ArrayList<String> newStudentnameList = new ArrayList<String>();
					ArrayList<UserPlayerDto> newStudentList = new ArrayList<UserPlayerDto>();

					for(int i = 0 ; i < studentList.size() ; i ++ ){
						if(studentList.get(i).getGrade().equals(selectedGrade)){
							newStudentList.add(studentList.get(i));
							newStudentnameList.add(studentList.get(i).getFirstname() + " " + studentList.get(i).getLastname());
						}
					}
					createDynamicLayoutForStudents(newStudentnameList , newStudentList);
				}
				gradeSelectedValue = selectedGrade;//changes default selected value
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
	}
	//end changes

	/**
	 * This method creates the dyanmic layout to shoe the teacher user
	 * @param studentNameList
	 */
	private  void createDynamicLayoutForStudents(ArrayList<String> studentNameList , final ArrayList<UserPlayerDto> studentList)
	{
		students.removeAllViews();
		layoutList = new ArrayList<RelativeLayout>();

		if(studentNameList.size() > 0){
			for(int i = 0 ; i < studentNameList.size() ; i++)
			{
				LayoutInflater inflater1 = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
				RelativeLayout layout = (RelativeLayout) inflater1.inflate(R.layout.studentslistlayout, null);
				txtStudentdName = (TextView) layout.findViewById(R.id.txtStudentName);
				txtStudentdName.setText(studentNameList.get(i));

				layout.setOnClickListener(new OnClickListener() 
				{
					@Override
					public void onClick(View v) 
					{		
						for( int  j = 0 ; j < layoutList.size() ; j ++ )
						{
							if(v == layoutList.get(j))
							{
								/*//added for new assessment report
							if(callingActivity.equals("ReportForPracticeAndAssessment"))*/
								ResultActivity.isCallFromPractice = true;

								int playerid = Integer.parseInt(studentList.get(j).getPlayerid());
								int userid   = Integer.parseInt(studentList.get(j).getParentUserId());
								String playerName  = studentList.get(j).getFirstname()+" "
										+studentList.get(j).getLastname();
								String imageName = studentList.get(j).getImageName();

								int points = 0; 
								try{
									points = Integer.parseInt(studentList.get(j).getPoints());
								}catch(Exception e){
									points = 0;
								}

								int grade = 1;
								try{
									grade = Integer.parseInt(studentList.get(j).getGrade());
								}
								catch(Exception e){
									grade = 1;
								}

								Date date  = new Date();
								String zero1 = "";
								String zero2 = "";
								if(date.getMonth() < 9)
									zero1 = "0";
								if(date.getDate() <= 9)
									zero2 = "0";
								String playDate  = (date.getYear()+1900) + "-" + zero1 + (date.getMonth() + 1)
										+ "-"+ zero2 +date.getDate();

								new JsonAsyncTaskForScore(TeacherStudents.this, playDate, userid, playerid, playerName, false
										,points , grade , imageName)

								.execute(null,null,null);
							}
						}
					}
				});

				layoutList.add(layout);
				students.addView(layout);
			}

			/*if(studentNameList.size() >= 30)//login for is student greater then 30 then show show more button
			{
				RelativeLayout.LayoutParams viewMore = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
						RelativeLayout.LayoutParams.WRAP_CONTENT);
				viewMore.addRule(RelativeLayout.CENTER_HORIZONTAL,RelativeLayout.TRUE);

				RelativeLayout layout = new RelativeLayout(this);
				Button viewMoreButton = new Button(this);
				viewMoreButton.setText("Show More");
				viewMoreButton.setBackgroundResource(R.drawable.buttonalert);
				layout.addView(viewMoreButton);
				students.addView(layout);

				viewMoreButton.setOnClickListener(new OnClickListener() 
				{
					@Override
					public void onClick(View v) 
					{
						UserRegistrationOperation userObj = new UserRegistrationOperation(TeacherStudents.this);
						RegistereUserDto regUserObj = userObj.getUserData();

						if(CommonUtils.isInternetConnectionAvailable(TeacherStudents.this))
						{					
							offSet = offSet + 30;
							new GetTeacherStudentsAsyncTask(regUserObj.getUserId(),offSet).execute(null,null,null);
						}
						else
						{
							DialogGenerator dg = new DialogGenerator(TeacherStudents.this);
							Translation transeletion = new Translation(TeacherStudents.this);
							transeletion.openConnection();
							dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
							transeletion.closeConnection();
						}

					}
				});
			}*/
		}else{
			DialogGenerator dg = new DialogGenerator(this);
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			dg.generateWarningDialogForthumbImages(transeletion.
					getTranselationTextByTextIdentifier("lblYouDoNotHaveAnyStudents"));
			transeletion.closeConnection();
		}
	}

	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}
}
