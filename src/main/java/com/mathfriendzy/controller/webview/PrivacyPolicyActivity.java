package com.mathfriendzy.controller.webview;

import com.mathfriendzy.R;
import android.os.Bundle;
import android.webkit.WebView;
import android.app.Activity;

/**
 * This Activity open the given url in WebView
 * @author Yashwant Singh
 *
 */
public class PrivacyPolicyActivity extends Activity{
	private WebView webView = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_privacy_policy);
		
		webView = (WebView) findViewById(R.id.privacypolicyWebView);
		String url = getIntent().getStringExtra("url");
		webView.loadUrl(url);
	}
}
