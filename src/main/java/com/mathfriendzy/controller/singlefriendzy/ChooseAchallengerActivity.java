package com.mathfriendzy.controller.singlefriendzy;

import static com.mathfriendzy.utils.ICommonUtils.CHOOSE_A_CHALLENGER;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;

import java.util.Date;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.schoolcurriculum.singlefriendzy.SingleFriendzyServerOperationForWordProblem;
import com.mathfriendzy.model.singleFriendzy.ChallengerDataFromServerTranseferObj;
import com.mathfriendzy.model.singleFriendzy.SingleFriendzyServerOperation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.PlaySound;

public class ChooseAchallengerActivity extends ActBaseClass
{
	private TextView mfLblChooseAChallenger = null;
	private final String TAG = this.getClass().getSimpleName();

	private Date startProcessTime = null;
	private Date endProcessTime   = null;
	private final int MAX_TIME_FOR_PROCESS = 8 ;// maximum time for finding challenger from server

	private MyTimer timer = null;
	public static ChallengerDataFromServerTranseferObj challengerDate = null;//this will use on the ChooseAChallengerListActivity

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_choose_achallenger);

		if(CHOOSE_A_CHALLENGER)
			Log.e(TAG,  "inside onCreate()");

		this.setWidgetsReferences();
		this.setTextFromTranselation();
		this.checkForLoginOrTempPlayer();		

		//startActivity(new Intent(this,ChooseAChallengerListActivity.class));

		if(CHOOSE_A_CHALLENGER)
			Log.e(TAG,  "outside onCreate()");
	}

	/**
	 * This method set translation text from translate table
	 */
	private void setTextFromTranselation() 
	{

		if(CHOOSE_A_CHALLENGER)
			Log.e(TAG,  " inside setTextFromTranselation()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		mfLblChooseAChallenger.setText(transeletion.getTranselationTextByTextIdentifier("mfLblChooseAChallenger"));
		transeletion.closeConnection();

		if(CHOOSE_A_CHALLENGER)
			Log.e(TAG,  " outside setTextFromTranselation()");
	}

	/**
	 * This method set the widgets referenes from the layout to the References object
	 */
	private void setWidgetsReferences() 
	{
		if(CHOOSE_A_CHALLENGER)
			Log.e(TAG,  " inside setWidgetsReferences()");

		mfLblChooseAChallenger = (TextView) findViewById(R.id.mfLblChooseAChallenger);

		if(CHOOSE_A_CHALLENGER)
			Log.e(TAG,  " outside setWidgetsReferences()");
	} 

	/**
	 * this method check for user templrary or login
	 */
	private void checkForLoginOrTempPlayer() 
	{
		if(CHOOSE_A_CHALLENGER)
			Log.e(TAG,  " inside checkForLoginOrTempPlayer()");

		SharedPreferences sharedPrefPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);	 
		if(!sheredPreference.getBoolean(IS_LOGIN, false))
		{
			if(CommonUtils.isInternetConnectionAvailable(this))
			{		
				//changes for word problem
				if(CommonUtils.isClickedSolveEquation){
					new FindChallengerForTempraryPlayer(sharedPrefPlayerInfo.getInt("grade", 0),
							sharedPrefPlayerInfo.getInt("completeLevel", 0)).execute(null,null,null);
				}else{
					new GetWordProblemChallenger("", "", sharedPrefPlayerInfo.getInt("grade", 0), true)
					.execute(null,null,null);
				}
			}
			else
			{
				this.generateIntenetWarningDialog();
			}
		}
		else
		{
			if(CommonUtils.isInternetConnectionAvailable(this))
			{
				//changes for word problem
				if(CommonUtils.isClickedSolveEquation){
					new FindChallengerForLoginPlayer(sharedPrefPlayerInfo.getString("userId", ""), 
							sharedPrefPlayerInfo.getString("playerId", "")).execute(null,null,null);
				}else{
					new GetWordProblemChallenger(sharedPrefPlayerInfo.getString("userId", ""), 
							sharedPrefPlayerInfo.getString("playerId", ""), 
							sharedPrefPlayerInfo.getInt("grade", 0), false).execute(null,null,null);
				}
			}
			else
			{
				this.generateIntenetWarningDialog();
			}
		}

		if(CHOOSE_A_CHALLENGER)
			Log.e(TAG,  " outside checkForLoginOrTempPlayer()");
	}

	/**This method generate warming dialog when intenet connection is not available
	 * 
	 */
	private void generateIntenetWarningDialog()
	{
		DialogGenerator dg = new DialogGenerator(this);
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
		transeletion.closeConnection();
	}


	/**
	 * This method call when the data from server is loaded,
	 * That means call from onPost From the FindChallengerForTempraryPlayer and 
	 * FindChallengerForLoginPlayer asynckTask
	 */
	private void goToChallengerPlayerActivity(ChallengerDataFromServerTranseferObj challengerData)
	{
		Intent intent = new Intent(this , ChooseAChallengerListActivity.class);
		startActivity(intent);
	}


	/**
	 * Timer for loading challenger player
	 * @author Yashwant Singh
	 *
	 */
	class MyTimer extends CountDownTimer
	{
		private  ProgressDialog pd = null;
		private PlaySound playSound = null;
		private ChallengerDataFromServerTranseferObj challengerDate = null;

		public MyTimer(long millisInFuture, long countDownInterval , ProgressDialog pd , PlaySound playSound , 
				ChallengerDataFromServerTranseferObj challengerDate) 
		{
			super(millisInFuture, countDownInterval);

			this.pd = pd;
			this.playSound = playSound;
			this.challengerDate = challengerDate;
		}

		@Override
		public void onFinish() 
		{
			timer.cancel();
			pd.cancel();
			playSound.stopPlayer();
			goToChallengerPlayerActivity(challengerDate);

		}

		@Override
		public void onTick(long millisUntilFinished) 
		{

		}
	}

	/**
	 * This asynckTask get the challenger from the server
	 * @author Yashwant Singh
	 *
	 */
	class FindChallengerForTempraryPlayer extends AsyncTask<Void, Void, ChallengerDataFromServerTranseferObj>
	{
		private int grade;
		private int completeLevel;
		private ProgressDialog pd;
		private PlaySound playSound = null;

		FindChallengerForTempraryPlayer(int grade , int completeLevel)
		{
			this.grade 			= grade;
			this.completeLevel 	= completeLevel;
		}

		@Override
		protected void onPreExecute() 
		{
			startProcessTime = new Date();
			playSound = new PlaySound(ChooseAchallengerActivity.this);
			playSound.playSoundForFindChallenger(ChooseAchallengerActivity.this);

			pd = CommonUtils.getProgressDialog(ChooseAchallengerActivity.this);
			pd.show();

			super.onPreExecute();
		}

		@Override
		protected ChallengerDataFromServerTranseferObj doInBackground(Void... params) 
		{
			SingleFriendzyServerOperation serverObj = new SingleFriendzyServerOperation();
			ChallengerDataFromServerTranseferObj challengerData = serverObj.findChallengerForTempPlayer
					(grade, completeLevel);
			return challengerData;
		}

		@Override
		protected void onPostExecute(ChallengerDataFromServerTranseferObj challengerData) 
		{		
			if(challengerData != null){
				challengerDate = challengerData;
				endProcessTime = new Date();

				if((endProcessTime.getSeconds() - startProcessTime.getSeconds()) > MAX_TIME_FOR_PROCESS)
				{
					pd.cancel();
					playSound.stopPlayer();
					goToChallengerPlayerActivity(challengerData);
				}
				else
				{
					//Log.e(TAG, "time diff " + (endProcessTime.getSeconds() - startProcessTime.getSeconds()));

					int startTime = MAX_TIME_FOR_PROCESS - ((endProcessTime.getSeconds() - startProcessTime.getSeconds()));

					timer = new MyTimer(startTime * 1000 ,1, pd, playSound, challengerData);
					timer.start();
				}
			}else{
				pd.cancel();
				playSound.stopPlayer();
				CommonUtils.showInternetDialog(ChooseAchallengerActivity.this);
				finish();
			}
			super.onPostExecute(challengerData);
		}
	}


	/**
	 * This asynckTask get the challenger from the server
	 * @author Yashwant Singh
	 *
	 */
	class FindChallengerForLoginPlayer extends AsyncTask<Void, Void, ChallengerDataFromServerTranseferObj>
	{
		private String userId 	= null;
		private String playerId = null;
		private ProgressDialog pd;
		private PlaySound playSound = null;

		FindChallengerForLoginPlayer(String userId , String playerId)
		{
			this.userId 	= userId;
			this.playerId 	= playerId;
		}

		@Override
		protected void onPreExecute() 
		{
			startProcessTime = new Date();
			playSound = new PlaySound(ChooseAchallengerActivity.this);
			playSound.playSoundForFindChallenger(ChooseAchallengerActivity.this);

			pd = CommonUtils.getProgressDialog(ChooseAchallengerActivity.this);
			pd.show();

			super.onPreExecute();
		}

		@Override
		protected ChallengerDataFromServerTranseferObj doInBackground(Void... params) 
		{
			SingleFriendzyServerOperation serverObj = new SingleFriendzyServerOperation();
			ChallengerDataFromServerTranseferObj challengerData = serverObj.findChallangerForLoginPlayer
					(userId, playerId);
			return challengerData;
		}

		@Override
		protected void onPostExecute(ChallengerDataFromServerTranseferObj challengerData) 
		{	
			if(challengerData != null){
				challengerDate = challengerData;
				endProcessTime = new Date();

				if((endProcessTime.getSeconds() - startProcessTime.getSeconds()) > MAX_TIME_FOR_PROCESS)
				{
					pd.cancel();
					playSound.stopPlayer();
					goToChallengerPlayerActivity(challengerData);
				}
				else
				{			
					int startTime = MAX_TIME_FOR_PROCESS - ((endProcessTime.getSeconds() - startProcessTime.getSeconds()));

					timer = new MyTimer(startTime * 1000 ,1, pd, playSound, challengerData);
					timer.start();
				}
			}else{
				pd.cancel();
				playSound.stopPlayer();
				CommonUtils.showInternetDialog(ChooseAchallengerActivity.this);
				finish();
			}

			super.onPostExecute(challengerData);
		}
	}


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
		{	
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * This class get word problem challenger from server
	 * @author Yashwant Singh
	 *
	 */
	private class GetWordProblemChallenger extends AsyncTask<Void, Void, ChallengerDataFromServerTranseferObj>{

		private String userId;
		private String playerId;
		private int grade;
		private boolean isTempPlayer;

		private ProgressDialog pd;
		private PlaySound playSound = null;

		GetWordProblemChallenger(String userId , String playerId, int grade , boolean isTempPlayer){
			this.userId 		= userId;
			this.playerId 		= playerId;
			this.grade 			= grade;
			this.isTempPlayer 	= isTempPlayer;
		}

		@Override
		protected void onPreExecute() {
			startProcessTime = new Date();
			playSound = new PlaySound(ChooseAchallengerActivity.this);
			playSound.playSoundForFindChallenger(ChooseAchallengerActivity.this);

			pd = CommonUtils.getProgressDialog(ChooseAchallengerActivity.this);
			pd.show();

			super.onPreExecute();
		}

		@Override
		protected ChallengerDataFromServerTranseferObj doInBackground(Void... params) {
			SingleFriendzyServerOperationForWordProblem serverObj = 
					new SingleFriendzyServerOperationForWordProblem(); 
			ChallengerDataFromServerTranseferObj challengerData = serverObj
					.getWordProblemChallenger(userId, playerId, grade, isTempPlayer);
			return challengerData;
		}

		@Override
		protected void onPostExecute(ChallengerDataFromServerTranseferObj challengerData) {

			if(challengerData != null){
				challengerDate = challengerData;
				endProcessTime = new Date();

				if((endProcessTime.getSeconds() - startProcessTime.getSeconds()) > MAX_TIME_FOR_PROCESS)
				{
					pd.cancel();
					playSound.stopPlayer();
					goToChallengerPlayerActivity(challengerData);
				}
				else
				{				
					int startTime = MAX_TIME_FOR_PROCESS - ((endProcessTime.getSeconds() - startProcessTime.getSeconds()));

					timer = new MyTimer(startTime * 1000 ,1, pd, playSound, challengerData);
					timer.start();
				}
			}else{
				pd.cancel();
				playSound.stopPlayer();
				CommonUtils.showInternetDialog(ChooseAchallengerActivity.this);
				finish();
			}
			super.onPostExecute(challengerData);
		}
	}
	
	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}
}
