package com.mathfriendzy.controller.singlefriendzy.schoolcurriculum;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources.NotFoundException;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.learningcenter.schoolcurriculum.OptionSelectDataObj;
import com.mathfriendzy.controller.learningcenter.schoolcurriculum.SchoolCurriculumEquationSolveBase;
import com.mathfriendzy.controller.singlefriendzy.ChooseChallengerAdapter;
import com.mathfriendzy.controller.singlefriendzy.CongratulationScreenForSingleFriendzy;
import com.mathfriendzy.controller.singlefriendzy.SingleFriendzyEquationSolve;
import com.mathfriendzy.controller.singlefriendzy.SingleFriendzyMain;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.MathResultTransferObj;
import com.mathfriendzy.model.learningcenter.PlayerEquationLevelObj;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.MathEquationTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.MathScoreForSchoolCurriculumTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SchoolCurriculumLearnignCenterimpl;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.WordProblemQuestionTransferObj;
import com.mathfriendzy.model.schoolcurriculum.singlefriendzy.SigleFriendzyQuestionFromServer;
import com.mathfriendzy.model.schoolcurriculum.singlefriendzy.SingleFriendzySchoolCurriculumImpl;
import com.mathfriendzy.model.schoolcurriculum.singlefriendzy.SingleFriendzyServerOperationForWordProblem;
import com.mathfriendzy.model.singleFriendzy.ChallengerTransferObj;
import com.mathfriendzy.model.singleFriendzy.SingleFriendzyImpl;
import com.mathfriendzy.model.singleFriendzy.SingleFriendzyServerOperation;
import com.mathfriendzy.model.spanishchanges.SpanishChangesImpl;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ICommonUtils;
import com.mathfriendzy.utils.PlaySound;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Random;

import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;

public class SingleFriendzySchoolCurriculumEquationSolve extends SchoolCurriculumEquationSolveBase {

    private TextView txtPlayerName1     	= null;
    private TextView txtPoints1         	= null;
    private ImageView imgFlag1          	= null;
    private LinearLayout progressBarLayout1 = null;
    private ImageView getReady              = null;

    //Challenger data which are set into the challenger list adapter
    private ChallengerTransferObj challengerDataObj = null;

    private final String TAG = this.getClass().getSimpleName();
    //get ready timer
    private CountDownTimer getReadyTimer = null;
    private long START_TIME_FOR_GET_READY = 7 * 1000;

    private StringBuilder answerValue     = new StringBuilder();
    //final question either from server or local
    ArrayList<SigleFriendzyQuestionFromServer> questionListFinal =
            new ArrayList<SigleFriendzyQuestionFromServer>();

    private final int MAX_QUESTION = 10;//maxmum quation to be displayed
    //private Random random = null;//random class for generating random number
    //level which play at current and set into the single friendzy main screen in
    private int completeLevel = 0;
    //question obj which contain the current question which is to be display
    //private WordProblemQuestionTransferObj questionObj = null;

    private int stars              = 0;
    private int numberOfCoins      = 0;
    private int isCorrectAnsByUser = 0;
    private String answerByUser 	= "";

    private final int SLEEP_TIME = 1000;
    private final int SLEEP_TIME_FOR_WRONG_ANS_THIRD_ATTEMP = 500;
    //max seconds after second player progress will set
    private final int MAX_SECONDS = 5;

    //for second player progress
    private int indexForSecodPlayerProgress = 0;
    private int points1                     = 0;//second player points
    CountDownTimer secondPlayerProgressSetTimer = null;
    private final int MAX_TIME_FOR_SOLVE    = 30 * 1000;

    //set winner
    private int isWin = 0;
    private final int EQUAITON_TYPE = 11;
    //for sound
    //private PlaySound playsound  = null;

    //for complete level
    private boolean isCompleteLevelChange = false;

    //for sound
    private boolean isFromOnCreate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_friendzy_school_curriculum_equation_solve);


        //for sound
        isFromOnCreate = true;

        //for tts
        tts = new TextToSpeech(this, this);

        challengerDataObj = ChooseChallengerAdapter.challengerDataObj;
        random            = new Random();
        completeLevel     = SingleFriendzyEquationSolve.completeLevel;//set in single friendzy main for cuurent lavel

        isTab = getResources().getBoolean(R.bool.isTablet);

        playDataList = new ArrayList<MathEquationTransferObj>();

        playSound = new PlaySound(this);

        //changes for Spanish
        this.getIntentValue();

        this.setWidgetsReferences();
        this.setRemainingWidgetsReferencesFromBase();
        this.setTextFromTranslation();
        this.setPlayerDetail();
        this.setListenerOnWidgets();

        txtTimer.setTextColor(Color.BLACK);
        //get learn center time
        SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
        int grade       =  sharedPreffPlayerInfo.getInt("grade", 0);
        startTimeForTimerAtFirstTime = this.getSingleFriendzyTime(grade);

        this.showTimeAtStarting();

        this.getEquations();
    }


    private void getIntentValue(){
        selectedPlayGrade = this.getIntent().getIntExtra("playerSelectedGrade", 1);
    }

    /**
     * This method show timer
     * Changes when required mail on 15 oct
     */
    private void showTimeAtStarting(){
        int mins = (int) (startTimeForTimerAtFirstTime / 60);
        int secs = (int) (startTimeForTimerAtFirstTime % 60);
        txtTimer.setText(mins + ":"+ String.format("%02d", secs));
    }

    /**
     * This method get single friendzy time from database
     * @param grade
     * @return
     */
    private long getSingleFriendzyTime(int grade){
        long time = 0l;
        SingleFriendzySchoolCurriculumImpl schoolImpl = new SingleFriendzySchoolCurriculumImpl(this);
        schoolImpl.openConnection();
        time = schoolImpl.getSingleFriendzyTime(grade);
        schoolImpl.closeConnection();
        return time;
    }


    /**
     * This method change the image on each seconds for get ready
     */
    private void getReadyTimer()
    {
        getReadyTimer = new CountDownTimer(START_TIME_FOR_GET_READY,1)
        {
            @Override
            public void onTick(long millisUntilFinished){
                int value = (int) ((millisUntilFinished/1000) % 60) ;
                setBacKGroundForGetReadyImage(value);
            }

            @Override
            public void onFinish(){
                questionAnswerLayout.setVisibility(RelativeLayout.VISIBLE);
                getReady.setVisibility(ImageView.INVISIBLE);

                playSound.playSoundForSingleFriendzyEquationSolve
                        (SingleFriendzySchoolCurriculumEquationSolve.this);

                if(questionListFinal.size() > 0){
                    getQuestionFromDatabase();//get question from database by question id
                    // question id is either from server or from local database
                }else{
                    notSufficientQuestionDailog();
                }
            }
        };
        getReadyTimer.start();
    }

    /**
     * This method open dialog when user does not have sufficient question to be played
     */
    private void notSufficientQuestionDailog(){
        DialogGenerator dg = new DialogGenerator(SingleFriendzySchoolCurriculumEquationSolve.this);
        Translation transeletion = new Translation(SingleFriendzySchoolCurriculumEquationSolve.this);
        transeletion.openConnection();
        dg.generateWarningDialogForNoSufficientQuestionForSingleFriendzy(transeletion.
                getTranselationTextByTextIdentifier("lblSorryYouDoNotHaveQuestions") , this , true);
        transeletion.closeConnection();
    }

    /**
     * This method set the Get Ready Image back ground
     * @param value
     */
    private void setBacKGroundForGetReadyImage(int value)
    {
        switch(value)
        {
            case 6 :
                getReady.setBackgroundResource(R.drawable.mcg_get_ready);
                break;
            case 5 :
                getReady.setBackgroundResource(R.drawable.mcg_5);
                break;
            case 4 :
                getReady.setBackgroundResource(R.drawable.mcg_4);
                break;
            case 3 :
                getReady.setBackgroundResource(R.drawable.mcg_3);
                break;
            case 2 :
                getReady.setBackgroundResource(R.drawable.mcg_2);
                break;
            case 1 :
                getReady.setBackgroundResource(R.drawable.mcg_1);
                break;
            case 0 :
                getReady.setBackgroundResource(R.drawable.mcg_go);
                break;
        }
    }

    /**
     * This method set the widgets references which are newly added for this screen
     */
    private void setRemainingWidgetsReferencesFromBase() {
        txtPlayerName1 			= (TextView) 	findViewById(R.id.txtPlayerName1);
        txtPoints1     			= (TextView) 	findViewById(R.id.txtPoints1);
        imgFlag1       			= (ImageView) 	findViewById(R.id.imgFlag1);
        progressBarLayout1 		= (LinearLayout)findViewById(R.id.progressBarLayout1);
        getReady                = (ImageView) 	findViewById(R.id.getReady);
    }

    /**
     * This method set the player detail
     */
    @SuppressWarnings("deprecation")
    private void setPlayerDetail()
    {
        SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
        String fullName = sharedPreffPlayerInfo.getString("playerName", "");
        String playerName = fullName.substring(0,(fullName.indexOf(" ") + 2));
        txtPlayerName.setText(playerName + ".");

        Country country = new Country();
        String coutryIso = country.getCountryIsoByCountryName(sharedPreffPlayerInfo.getString("countryName", ""), this);

        try{
            if(!(coutryIso.equals("-")))
                imgFlag.setBackgroundDrawable(Drawable.createFromStream(getAssets().open(getResources().
                        getString(R.string.countryImageFolder) +"/"
                        + coutryIso + ".png"), null));
        }
        catch (IOException e){
            Log.e(TAG, "Inside set player detail Error No Image Found " + e.toString());
        }

        //set second user detail
        coutryIso = challengerDataObj.getCountryIso();

        txtPlayerName1.setText(challengerDataObj.getFirst() + " "
                + challengerDataObj.getLast().charAt(0) + ".");
        try{
            if(!(coutryIso.equals("-")))
                imgFlag1.setBackgroundDrawable(Drawable.createFromStream(getAssets().open(getResources().
                        getString(R.string.countryImageFolder) +"/"
                        + challengerDataObj.getCountryIso() + ".png"), null));
        }
        catch (IOException e){
            Country country1 = new Country();
            try{
                imgFlag1.setBackgroundDrawable(Drawable.createFromStream(getAssets().open(getResources().
                        getString(R.string.countryImageFolder) +"/"
                        + country1.getCountryIsoByCountryId(challengerDataObj.getCountryIso(), this) + ".png"), null));
            }
            catch (NotFoundException e1){
                Log.e(TAG, "Inside set player detail Error No Image Found " + e.toString());
            }
            catch (IOException e1){
                Log.e(TAG, "Inside set player detail Error No Image Found " + e.toString());
            }
        }
    }

    /**
     * This method get equations from server of from local database
     */
    private void getEquations() {
        if(challengerDataObj.getSum().length() > 0 && !challengerDataObj.getSum().equals("0")
                && CommonUtils.isInternetConnectionAvailable(this)){
            this.getEquationsFromServer();
        }else{
            this.getEquatonsFromLocalDatabase();
        }
    }

    /**
     * This method get Equations from server
     */
    private void getEquationsFromServer(){
        new GetWordProblemQuestionInFriendzy(challengerDataObj.getUserId(),
                challengerDataObj.getPlayerId()).execute(null,null,null);
		/*new GetWordProblemQuestionInFriendzy("1199", 
				"1792").execute(null,null,null);*/
    }

    /**
     * This method get Equations from local database
     */
    private void getEquatonsFromLocalDatabase(){
        SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
        String userId   = sharedPreffPlayerInfo.getString("userId", "0");
        String playerId = sharedPreffPlayerInfo.getString("playerId", "0");
        int grade       =  sharedPreffPlayerInfo.getInt("grade", 0);

        //changes for Spanish
        ArrayList<Integer> questionIds = new ArrayList<Integer>();
        if(CommonUtils.getUserLanguageCode(this) == CommonUtils.ENGLISH){
            SingleFriendzySchoolCurriculumImpl schoolImpl = new SingleFriendzySchoolCurriculumImpl(this);
            schoolImpl.openConnection();
            questionIds = schoolImpl.getQuestionIdsFromDatabase(userId, playerId, grade);
            schoolImpl.closeConnection();
        }else{
            SpanishChangesImpl schoolImpl = new SpanishChangesImpl(this);
            schoolImpl.openConn();
            questionIds = schoolImpl.getQuestionIdsFromDatabase(userId, playerId, grade);
            schoolImpl.closeConn();
        }

        //create final question list
        if(questionIds.size() > 0){
            this.createFinalQuaetionListFromDatabase(questionIds);
        }else{
            this.notSufficientQuestionDailog();
        }
    }

    /**
     * Create final question list which is to be display for player
     * @param questionIds
     */
    private void createFinalQuaetionListFromDatabase(ArrayList<Integer> questionIds){
        if(questionIds.size() >= MAX_QUESTION){
            int index = 0;
            for(int i = 0 ; i < MAX_QUESTION ; i ++ ){
                index = random.nextInt(questionIds.size());
                SigleFriendzyQuestionFromServer questionObj = new SigleFriendzyQuestionFromServer();
                questionObj.setTimeTaken(0);
                questionObj.setIsCorrect(this.getIsAnswerCorrectValue());
                questionObj.setQuestionId(questionIds.get(index));
                questionObj.setPoints(0);
                questionObj.setPlayerId(challengerDataObj.getPlayerId());
                questionObj.setUserId(challengerDataObj.getUserId());
                questionListFinal.add(questionObj);
                questionIds.remove(index);
            }
            this.getReadyTimer();
        }else{
            SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
            String userId   = sharedPreffPlayerInfo.getString("userId", "0");
            String playerId = sharedPreffPlayerInfo.getString("playerId", "0");
            SingleFriendzySchoolCurriculumImpl dataObj = new SingleFriendzySchoolCurriculumImpl(this);
            dataObj.openConnection();
            dataObj.deleteFromWordQuestionsPlayed(userId, playerId);
            dataObj.closeConnection();
            this.getEquatonsFromLocalDatabase();
        }
    }

    /**
     * This method return answer correct or not for opponent player when question
     * fatch from database
     * @return
     */
    private int getIsAnswerCorrectValue(){
        int probebilityOfCorerctAnswer = 30 + 3 * (completeLevel - 1);
        Random random = new Random();
        int value = random.nextInt(100);
        value ++ ;
        if(value > probebilityOfCorerctAnswer)
            return 0 ;
        else
            return 1;
    }

    /**
     * This method get question with its answer from database
     * using quation id
     * @param
     */
    private void getQuestionFromDatabase(){

        if(numberOfQuestion < MAX_QUESTION){
            int questionId = questionListFinal.get(numberOfQuestion).getQuestionId();
            //int questionId = 5679;
			/*SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
			schoolImpl.openConnection();
			//get question from database into questionObj object
			questionObj  = schoolImpl.getQuestionByQuestionId(questionId);
			schoolImpl.closeConnection();*/

            //changes for Spanish
            if(languageCode == SPANISH){
                SpanishChangesImpl schoolImpl = new SpanishChangesImpl(this);
                schoolImpl.openConn();
                //get question from database into questionObj object
                questionObj  = schoolImpl.getQuestionByQuestionId(questionId);
                schoolImpl.closeConn();
            }else if(languageCode == ENGLISH){
                SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
                schoolImpl.openConnection();
                //get question from database into questionObj object
                questionObj  = schoolImpl.getQuestionByQuestionId(questionId);
                schoolImpl.closeConnection();
            }

            this.setIsSpanishBooleanValue(); // changes after
            //end changes

            //insert played question into WordQuestionsPlayed table
            SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
            String userId   = sharedPreffPlayerInfo.getString("userId", "0");
            String playerId = sharedPreffPlayerInfo.getString("playerId", "0");
            SingleFriendzySchoolCurriculumImpl dataObj = new SingleFriendzySchoolCurriculumImpl(this);
            dataObj.openConnection();
            dataObj.insertIntoWordQuestionsPlayed(userId, playerId, questionId);
            dataObj.closeConnection();

            this.showQuestion();
        }else{

            //set it on the CongratulationScreenForSingleFriendzy Screen for Alex sheet
            //playsound.playSoundForSchoolSurriculumForResultScreen(this);

            if(playSound != null)
                playSound.stopPlayer();

            this.callAfterCompletion();
        }
    }

    //changes for update player points in local when Internet is not connected
    //updated on 26 11 2014
    private void savePlayerDataWhenNoInternetConnected(){
        try{
            SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
            String userId;
            String playerId;
            if(sharedPreffPlayerInfo.getString("userId", "").equals("0"))
                userId = "0";
            else
                userId = sharedPreffPlayerInfo.getString("userId", "");

            if(sharedPreffPlayerInfo.getString("playerId", "").equals("0"))
                playerId = "0";
            else
                playerId = sharedPreffPlayerInfo.getString("playerId", "");

            MathResultTransferObj mathResultObj = new MathResultTransferObj();
            mathResultObj.setUserId(userId);
            mathResultObj.setPlayerId(playerId);
            mathResultObj.setTotalScore(points);
            mathResultObj.setCoins((int) (points * ICommonUtils.COINS_PER_POINT));
            mathResultObj.setLevel(completeLevel);
            MathFriendzyHelper.insertPlayerPlayedDataIntoLocalEarnedScore(mathResultObj, this);
        }catch(Exception e){
            e.printStackTrace();
        }
    }//end updation on 26 11 2014

    /**
     * This method call after when all question are played
     */
    private void callAfterCompletion(){

        //check current level with the database stored level
        SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
        String userId = sharedPreffPlayerInfo.getString("userId", "") ;
        String playerId = sharedPreffPlayerInfo.getString("playerId", "");

        int completeLevelBeforeChanges = completeLevel;//for checking play level is change or not
        if(completeLevel == sharedPreffPlayerInfo.getInt("completeLevel", 0))
        {
            if(points > points1)
            {
                isWin = 1 ;
                completeLevel ++ ;
            }
            else
            {
                isWin = 0 ;
                if(completeLevel > 1)
                    completeLevel -- ;
            }

            isCompleteLevelChange = true;

            SharedPreferences.Editor editor = sharedPreffPlayerInfo.edit();
            editor.putInt("completeLevel", completeLevel);
            editor.commit();

            insertOrUpdatePlayerEquationTable();
        }

        SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
        if(sheredPreference.getBoolean(IS_LOGIN, false))
        {
            if(CommonUtils.isInternetConnectionAvailable(this))
            {
                new AddFriendzyForCompleteLevelForBonus(userId, playerId, completeLevel, stars,
                        CommonUtils.APP_ID).execute(null,null,null);

                if(completeLevelBeforeChanges == completeLevel){
                    new AddWordProblemMathScore("Play", userId, playerId,
                            this.getEquationSolveXmlForInterNetNotConnected(playDataList),
                            points).execute(null,null,null);
                }else{
                    new AddWordProblemFriendzyScore(userId, playerId, this.getEquationSolveXml(playDataList),
                            points, challengerDataObj.getIsFakePlayer(), challengerDataObj.getPlayerId(),
                            isWin, "Compete").execute(null,null,null);
                }
            }
            else{
                this.insertPlayEquationIntoLocalDatabase(userId, playerId);
            }
        }
        else{
            this.insertPlayEquationIntoLocalDatabase(userId, playerId);
        }

        updateStarAndLevel(userId , playerId);
        updatePlayerTotalPointsTable(playerId , userId);

        this.savePlayerDataWhenNoInternetConnected();

        Intent intent = new Intent(this,CongratulationScreenForSingleFriendzy.class);
        intent.putExtra("points", points);
        intent.putExtra("isWin", isWin);
        intent.putExtra("isFromLearnignCenterSchoolCurriculum", true);
        intent.putExtra("playDatalist", playDataList);
        intent.putExtra("selectedGrade", selectedPlayGrade);
        startActivity(intent);
    }

    /**
     * This method insert the play equation into local database when not is not
     * connected for login player and also for temp player
     * @param userId
     * @param playerId
     */
    private void insertPlayEquationIntoLocalDatabase(String userId , String playerId){
        MathScoreForSchoolCurriculumTransferObj mathScoreObj = new MathScoreForSchoolCurriculumTransferObj();
        mathScoreObj.setUserId(userId);
        mathScoreObj.setPlayerId(playerId);
        mathScoreObj.setProblems(this.getEquationSolveXml(playDataList));
        mathScoreObj.setProblems(this.getEquationSolveXmlForInterNetNotConnected(playDataList));
        SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
        schoolImpl.openConnection();
        schoolImpl.insertIntoWordProblemResults(mathScoreObj);
        schoolImpl.closeConnection();
    }

    /**
     * This method update star and level of the player
     * @param userId
     * @param playerId
     */
    private void updateStarAndLevel(String userId , String playerId)
    {
        SingleFriendzyImpl singleimpl = new SingleFriendzyImpl(this);
        singleimpl.openConn();
        PlayerEquationLevelObj playrEquationObj = singleimpl.getEquaitonDataFromPlayerEquationTable
                (userId, playerId, EQUAITON_TYPE);
        singleimpl.closeConn();

        LearningCenterimpl learningCenterimpl = new LearningCenterimpl(this);
        learningCenterimpl.openConn();
        PlayerTotalPointsObj playerPoints =  learningCenterimpl.getDataFromPlayerTotalPoints(playrEquationObj.getPlayerId());

        int level = playrEquationObj.getLevel();

        if(level != 1 && level == completeLevel && ((level-1) % 10 == 0))
        {
            if(playrEquationObj.getStars() == 0)
            {
                stars = 1;
                SingleFriendzyImpl singleFriendzy = new SingleFriendzyImpl(this);
                singleFriendzy.openConn();

                singleFriendzy.updateEquaitonPlayerData(playrEquationObj.getUserId(),
                        playrEquationObj.getPlayerId(), EQUAITON_TYPE, playrEquationObj.getLevel(), 1);
                singleFriendzy.closeConn();

                playerPoints.setCoins(playerPoints.getCoins() + 2000);//add 2000 coins

                learningCenterimpl.deleteFromPlayerTotalPoints(playrEquationObj.getPlayerId());

                learningCenterimpl.insertIntoPlayerTotalPoints(playerPoints);

                SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
                if(sheredPreference.getBoolean(IS_LOGIN, false))
                {
                    if(CommonUtils.isInternetConnectionAvailable(this))
                    {
                        new AddCoinAndPointsForLoginUser(playerPoints, this).execute(null,null,null);
                        new AddFriendzyForCompleteLevelForBonus(userId, playerId, completeLevel, stars, CommonUtils.APP_ID)
                                .execute(null,null,null);
                    }
                }
            }
        }
        learningCenterimpl.closeConn();
    }

    /**
     * Update player total points table
     * @param playerId
     */
    private void updatePlayerTotalPointsTable(String playerId , String userId)
    {
        numberOfCoins = (int) (points * ICommonUtils.COINS_PER_POINT);

        LearningCenterimpl learningCenterimpl = new LearningCenterimpl(this);
        learningCenterimpl.openConn();
        PlayerTotalPointsObj playerPoints =  learningCenterimpl.getDataFromPlayerTotalPoints(playerId);

        playerPoints.setTotalPoints(playerPoints.getTotalPoints() + points);
        playerPoints.setCoins(playerPoints.getCoins() + numberOfCoins);

        SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
        if(isCompleteLevelChange)
            playerPoints.setCompleteLevel(completeLevel);
        else
            playerPoints.setCompleteLevel(sharedPreffPlayerInfo.getInt("completeLevel", 0));

        playerPoints.setUserId(userId);
        playerPoints.setPlayerId(playerId);

        learningCenterimpl.deleteFromPlayerTotalPoints(playerId);
        learningCenterimpl.insertIntoPlayerTotalPoints(playerPoints);

        learningCenterimpl.closeConn();

        SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
        if(sheredPreference.getBoolean(IS_LOGIN, false))
        {
            if(CommonUtils.isInternetConnectionAvailable(this))
            {
                new AddCoinAndPointsForLoginUser(playerPoints , this).execute(null,null,null);
            }
        }
    }

    /**
     * This method update table is record already exist pther wise insert the new  record
     */
    private void insertOrUpdatePlayerEquationTable()
    {
        SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
        String userId = sharedPreffPlayerInfo.getString("userId", "") ;
        String playerId = sharedPreffPlayerInfo.getString("playerId", "");

        SingleFriendzyImpl singleFriendzy = new SingleFriendzyImpl(this);
        singleFriendzy.openConn();
        if(singleFriendzy.isEquationPlayerExist( userId , playerId , EQUAITON_TYPE))
        {
            singleFriendzy.updateEquaitonPlayerData(userId, playerId, EQUAITON_TYPE, completeLevel, 0);
        }
        else
        {
            PlayerEquationLevelObj playerEquationObj = new PlayerEquationLevelObj();

            playerEquationObj.setUserId(userId);
            playerEquationObj.setPlayerId(playerId);
            playerEquationObj.setLevel(completeLevel);
            playerEquationObj.setStars(0);
            playerEquationObj.setEquationType(EQUAITON_TYPE);

            singleFriendzy.insertIntoEquationPlayerTable(playerEquationObj);
        }
        singleFriendzy.closeConn();
    }

    /**
     * This method show question
     */
    private void showQuestion(){

        try{
            numberOfQuestion ++ ;

            if(numberOfQuestion > 1 && numberOfQuestion <= MAX_QUESTION){
                animation = AnimationUtils.loadAnimation(this, R.anim.splash_animation_right);
                questionAnswerLayout.setAnimation(animation);
            }

            this.setKeyBoardVisivility(questionObj.getFillIn());
            this.setLayoutForShowQuetionAndAnswer(questionObj);
        }
        catch (Exception e) {
            Log.e(TAG, "No Question Found " + e.toString());
        }
    }


    /**
     * This method inflate the layout for show question and answer
     * @param questionObj
     */
    private void setLayoutForShowQuetionAndAnswer(WordProblemQuestionTransferObj questionObj){

        ArrayList<String> imageNameList        = this.getImageArrayList(questionObj);
        ArrayList<String> unAvailableImageList = this.checkImageAvalability(imageNameList);

        if(unAvailableImageList.size() > 0){
            if(CommonUtils.isInternetConnectionAvailable(this)){
                //changes for timer when download images from server
                if(isCountDawnTimer){
                    if(timer != null){
                        timer.cancel();
                        timer = new MyTimer(startTimerForRoughArea, 1 * 1000);//second argument is the interval
                    }
                }else{
                    if(customHandler != null)
                        customHandler.removeCallbacks(updateTimerThread);
                }
                //end changes

                new DawnloadImageForSchooCurriculum(unAvailableImageList, this, questionObj)
                        .execute(null,null,null);
            }else{
                //this.showQuetion();
                this.startTimeForSolveQuestionTime();
                this.setQuestionAnswerLayout(questionObj);
            }
        }
        else{
            this.startTimeForSolveQuestionTime();
            this.setQuestionAnswerLayout(questionObj);
        }

    }

    /**
     * This method start the time for solve question
     * if its greater than or equal to 30 seconds then
     * set the second player progress
     */
    private void startTimeForSolveQuestionTime(){
        if(secondPlayerProgressSetTimer != null)
            secondPlayerProgressSetTimer.cancel();

        secondPlayerProgressSetTimer = new CountDownTimer(MAX_TIME_FOR_SOLVE,1)
        {
            @Override
            public void onTick(long millisUntilFinished){
            }
            @Override
            public void onFinish(){
                setProgressAndPointsOfSecondPlayer();
                secondPlayerProgressSetTimer.start();
            }
        };
        secondPlayerProgressSetTimer.start();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnGoForWithoutFillIn:
                endTime = new Date();
                isClickOnGo = true;
                setClickableGoButton(false);
                this.clickOnbtnGoForWithoutFillIn();
                break;
            case R.id.btnGoForFillIn:
                endTime = new Date();
                isClickOnGo = true;
                setClickableGoButton(false);
                this.clickOnbtnGoForFillIn();
                break;
            case R.id.btn1:
                this.clickOnButton("1");
                break;
            case R.id.btn2:
                this.clickOnButton("2");
                break;
            case R.id.btn3:
                this.clickOnButton("3");
                break;
            case R.id.btn4:
                this.clickOnButton("4");
                break;
            case R.id.btn5:
                this.clickOnButton("5");
                break;
            case R.id.btn6:
                this.clickOnButton("6");
                break;
            case R.id.btn7:
                this.clickOnButton("7");
                break;
            case R.id.btn8:
                this.clickOnButton("8");
                break;
            case R.id.btn9:
                this.clickOnButton("9");
                break;
            case R.id.btn0:
                this.clickOnButton("0");
                break;
            case R.id.btnSpace:
                this.clickOnButton(" ");
                break;
            case R.id.btnSynchronized :
                this.clearAnsBox();
                break;
            case R.id.btnsecondryKeyBoard :
                primaryKeyboard.setVisibility(LinearLayout.GONE);
                secondryKeyboard.setVisibility(LinearLayout.VISIBLE);
                btnSecandryDelete.setVisibility(Button.VISIBLE);
                break;
            case R.id.btnNumber :
                primaryKeyboard.setVisibility(LinearLayout.VISIBLE);
                secondryKeyboard.setVisibility(LinearLayout.GONE);
                btnSecandryDelete.setVisibility(Button.GONE);
                break;
            case R.id.btnDot:
                this.clickOnButton(".");
                break;
            case R.id.btnMinus:
                this.clickOnButton("-");
                break;
            case R.id.btnComma:
                this.clickOnButton(",");
                break;
            case R.id.btnPlus:
                this.clickOnButton("+");
                break;
            case R.id.btnDevide:
                this.clickOnButton("/");
                break;
            case R.id.btnMultiply:
                this.clickOnButton("*");
                break;
            case R.id.btnCollon:
                this.clickOnButton(":");
                break;
            case R.id.btnLessThan:
                this.clickOnButton("<");
                break;
            case R.id.btnGreaterThan:
                this.clickOnButton(">");
                break;
            case R.id.btnEqual:
                this.clickOnButton("=");
                break;
            case R.id.btnRoot:
                this.clickOnButton("√");
                break;
            case R.id.btnDevider:
                this.clickOnButton("|");
                break;
            case R.id.btnOpeningBracket:
                this.clickOnButton("(");
                break;
            case R.id.btnClosingBracket:
                this.clickOnButton(")");
                break;
            case R.id.btnRoughWork:
                this.clickOnWorkArea(false);
                break;
            case R.id.btnRoughWorkWithourFillIn:
                this.clickOnWorkArea(false);
                break;
            case R.id.imgTimerImage:
                timer.cancel();
                timer = new MyTimer(startTimerForRoughArea, 1 * 1000);//second argument is the interval
                DialogGenerator dg = new DialogGenerator(this);
                Translation transeletion = new Translation(this);
                transeletion.openConnection();
                String text = transeletion.getTranselationTextByTextIdentifier("lblTheCountDownRepresents");
                transeletion.closeConnection();
                dg.generateDialogForSchoolCurriculumTimer(text, timer);
                break;
            case R.id.btnEmailSendForWrongQuestion1:
                this.sendEmailFroWrongQuestion(questionObj.getQuestionId() , this , this);
                break;
            case R.id.btnEmailSendForWrongQuestion2:
                this.sendEmailFroWrongQuestion(questionObj.getQuestionId() , this , this);
                break;
            case R.id.btnSecandryDelete:
                this.clearAnsBox();
                break;
        }
    }

	/*@Override
	public void cancelCurrentTimerAndSetTheResumeTime() {
		if(timer != null){
			timer.cancel();
			timer = new MyTimer(startTimerForRoughArea, 1 * 1000);//second argument is the interval
		}
	}

	@Override
	public void resumeTimerFromTimeSetAtCancelTime() {
		if(timer != null)
			timer.start();
	}*/

    /**
     * This method call when a user click on number button
     * @param text
     */
    private void clickOnButton(String text){
        //answerValue = answerValue + text;
        answerValue.append(text);
        this.setAnswertext(answerValue.toString());
    }

    /**
     * This method clear the ans box
     */
    private void clearAnsBox() {
        if(answerValue.length() > 0){
            answerValue.deleteCharAt(answerValue.length() - 1);
            this.setAnswertext(answerValue.toString());
        }
    }

    /**
     * This method set the string to ans box
     * @param ansStringValue
     */
    private void setAnswertext(String ansStringValue){
        if(isAnswerConatin == 0)
            ansBox.setText(answerValue);
        else if(isAnswerConatin == 1){
            ansBox.setText(ansPrefixValue + " " +  answerValue);
        }
        else if(isAnswerConatin == 2){
            ansBox.setText(answerValue + " " + ansPostFixValue);
        }
        else if(isAnswerConatin == 3){
            ansBox.setText(ansPrefixValue + " " + answerValue + " " + ansPostFixValue);
        }
    }

    /**
     * This method call when click on btnGoForWithoutFillIn button
     */
    private void clickOnbtnGoForWithoutFillIn(){

        ArrayList<Integer> selectedIntegerOptionList = new ArrayList<Integer>();

        String userAnswer = "";
        for( int i = 0 ; i < selectedOptionList.size() ; i ++ ){
            for(int j = 0 ; j < questionObj.getOptionList().size() ; j ++ ){
                if(selectedOptionList.get(i).getSelectedOptionTextValue()
                        .equals(questionObj.getOptionList().get(j)))
                {
                    selectedIntegerOptionList.add(j + 1);
                }
            }
        }

        Collections.sort(selectedIntegerOptionList);
        for(int i = 0 ; i < selectedIntegerOptionList.size() ; i ++ ){
            userAnswer = userAnswer + selectedIntegerOptionList.get(i) + ",";
        }

        if(userAnswer.length() > 0){
            answerByUser = userAnswer.substring(0, userAnswer.length() - 1);
        }

        if(userAnswer.equals(questionObj.getAns().replaceAll(" ", "") + ",")){
            for(int i = 0 ; i < selectedOptionList.size() ; i ++ ){
                if(isTab){
                    selectedOptionList.get(i).getOpationSeletedLayout()
                            .setBackgroundResource(R.drawable.right_ipad);
                }
                else{
                    selectedOptionList.get(i).getOpationSeletedLayout()
                            .setBackgroundResource(R.drawable.right);
                }
            }
            playSound.playSoundForSchoolSurriculumEquationSolveForRightAnswer(this);
            isCorrectAnsByUser = 1;
            setProgress(true);
            this.setSecondPlayerProgress();
            this.setPoints();
            this.setHandlerForRightAnswer();
        }
        else{
            playSound.playSoundForSchoolSurriculumEquationSolveForWrongAnswer(this);
            isCorrectAnsByUser = 0;
            setProgress(false);
            this.setSecondPlayerProgress();
            imgForGroundImage.setVisibility(ImageView.VISIBLE);
            this.setHandlerForWrongAnswerForWithoutFillIn();
        }
    }

    /**
     * This method call when user btnGoForFillIn
     */
    private void clickOnbtnGoForFillIn() {

        if(!questionObj.getOptionList().get(0).contains(" ") && answerValue.toString().contains(" ")){
            answerValue = new StringBuilder(answerValue.toString().replaceAll(" ", ""));
        }

        answerByUser = answerValue.toString();

        String userAns = answerValue.toString().replaceAll(" ", "").replace(",", "");
        String dbAns = questionObj.getOptionList().get(0).replaceAll(" ", "").replace(",", "");

        //if(!questionObj.getOptionList().get(0).contains(".")){
        if(this.getIsFloatValue(questionObj.getOptionList().get(0))){
            if(userAns.equals(dbAns)){//changes for space and ask to deepak about this
                playSound.playSoundForSchoolSurriculumEquationSolveForRightAnswer(this);
                isCorrectAnsByUser = 1;
                answerValue = new StringBuilder("");
                ansBox.setBackgroundResource(R.drawable.right_bar_ipad);
                setProgress(true);
                this.setSecondPlayerProgress();
                this.setPoints();
                this.setHandlerForRightAnswer();
            }
            else{
                playSound.playSoundForSchoolSurriculumEquationSolveForWrongAnswer(this);
                isCorrectAnsByUser = 0;
                answerValue = new StringBuilder("");
                setProgress(false);
                this.setSecondPlayerProgress();
                ansBox.setBackgroundResource(R.drawable.incorrect_bar_ipad);
                imgForGroundImage.setVisibility(ImageView.VISIBLE);
                this.setHandlerForWrongAnswerForFillIn();
            }
        }else{//changes for float ans
            if(this.compareFloatAns(answerByUser, questionObj.getOptionList().get(0))){
                playSound.playSoundForSchoolSurriculumEquationSolveForRightAnswer(this);
                isCorrectAnsByUser = 1;
                answerValue = new StringBuilder("");
                ansBox.setBackgroundResource(R.drawable.right_bar_ipad);
                setProgress(true);
                this.setSecondPlayerProgress();
                this.setPoints();
                this.setHandlerForRightAnswer();
            }else{
                playSound.playSoundForSchoolSurriculumEquationSolveForWrongAnswer(this);
                isCorrectAnsByUser = 0;
                answerValue = new StringBuilder("");
                setProgress(false);
                this.setSecondPlayerProgress();
                ansBox.setBackgroundResource(R.drawable.incorrect_bar_ipad);
                imgForGroundImage.setVisibility(ImageView.VISIBLE);
                this.setHandlerForWrongAnswerForFillIn();
            }
        }
    }

    /**
     * This method set points
     */
    private void setPoints(){
        points = points + this.getPointsForSolvingEachEquation();
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtPoints.setText(transeletion.getTranselationTextByTextIdentifier("mfLblPts") + ": " + points);
        transeletion.closeConnection();
    }

    /**
     * This method wait for 1 second after giving right answer and show the next question
     */
    private void setHandlerForRightAnswer()
    {
        //this.setplayDataToArrayList();
        this.pauseTimer();
        disableKeyboard();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {

                resumeTimer();
                enableKeyboard();

                //changes for time on 11 Mar
                setplayDataToArrayList();
                //end changes

                playSound.playSoundForSchoolSurriculumPagePeel
                        (SingleFriendzySchoolCurriculumEquationSolve.this);
                setClickableGoButton(true);
                getQuestionFromDatabase();
            }
        }, SLEEP_TIME);
    }

    /**
     * This method call when user give wrong answer for fill in
     */
    private void setHandlerForWrongAnswerForFillIn(){
        this.pauseTimer();
        disableKeyboard();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                //setplayDataToArrayList();
                resumeTimer();
                enableKeyboard();

                imgForGroundImage.setVisibility(ImageView.INVISIBLE);
                if(!questionObj.getLeftAns().equals("") && !questionObj.getRightAns().equals(""))
                    ansBox.setText(questionObj.getLeftAns() + " " + questionObj.getRightAns());
                else if(!questionObj.getLeftAns().equals(""))
                    ansBox.setText(questionObj.getLeftAns());
                else if(!questionObj.getRightAns().equals(""))
                    ansBox.setText(questionObj.getRightAns());
                else
                    ansBox.setText("");
                ansBox.setBackgroundResource(+ R.layout.edittext);

                ansBox.setText(questionObj.getLeftAns() + " " + questionObj.getOptionList().get(0)
                        + " " + questionObj.getRightAns());

                pauseTimer();
                disableKeyboard();

                Handler handler1 = new Handler();
                handler1.postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        resumeTimer();
                        enableKeyboard();
                        //changes for timer on 11 Mar
                        setplayDataToArrayList();
                        //end changes

                        playSound.playSoundForSchoolSurriculumPagePeel
                                (SingleFriendzySchoolCurriculumEquationSolve.this);
                        setClickableGoButton(true);
                        getQuestionFromDatabase();
                    }
                }, SLEEP_TIME_FOR_WRONG_ANS_THIRD_ATTEMP);
            }
        }, SLEEP_TIME);
    }

    /**
     * This method call when user give the wrong answer
     */
    private void setHandlerForWrongAnswerForWithoutFillIn(){

        this.pauseTimer();
        disableKeyboard();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                //setplayDataToArrayList();
                resumeTimer();
                enableKeyboard();

                imgForGroundImage.setVisibility(ImageView.INVISIBLE);
                selectedOptionList = new ArrayList<OptionSelectDataObj>();// for refreshing the old object
                //from list
                for(int i = 0 ; i < optionLayoutList.size() ; i ++ ){
                    optionNumberList.get(i).setText(alphabet.charAt(i) + "");//set the option text like
                    //A,B,c etc
                    if(isTab)
                        optionLayoutList.get(i).setBackgroundResource(getResources().getIdentifier
                                ("option" + (i+1) + "_ipad", "drawable",getPackageName()));
                    else
                        optionLayoutList.get(i).setBackgroundResource(getResources().getIdentifier
                                ("option" + (i + 1) + "", "drawable",getPackageName()));
                }

                ArrayList<Integer> listWithoutComma = getCommaSepratedValueFromString(questionObj.getAns());
                for(int i = 0 ; i < listWithoutComma.size() ; i ++ ){
                    for(int j = 0 ; j < selectedOptionValue.size() ; j ++ ){
                        if(questionObj.getOptionList().get(listWithoutComma.get(i) - 1)//index in array
                                // start from 0 to 7
                                .equals(selectedOptionValue.get(j))){
                            optionNumberList.get(j).setText("");
                            if(isTab)
                                optionLayoutList.get(j).setBackgroundResource(R.drawable.right_ipad);
                            else
                                optionLayoutList.get(j).setBackgroundResource(R.drawable.right);
                        }
                    }
                }

                pauseTimer();
                disableKeyboard();
                Handler handler1 = new Handler();
                handler1.postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        resumeTimer();
                        enableKeyboard();
                        //changes for timer on 11 Mar
                        setplayDataToArrayList();
                        //end changes

                        playSound.playSoundForSchoolSurriculumPagePeel
                                (SingleFriendzySchoolCurriculumEquationSolve.this);
                        setClickableGoButton(true);
                        getQuestionFromDatabase();
                    }
                }, SLEEP_TIME_FOR_WRONG_ANS_THIRD_ATTEMP);
            }
        }, SLEEP_TIME);
    }

    /**
     * This method set second player progress
     */
    private void setSecondPlayerProgress(){

        //check for 30 seconds , if user given ans with in 30 then set answer other not from this method
        @SuppressWarnings("deprecation")
        int startTimeValue = startTime.getSeconds();
        @SuppressWarnings("deprecation")
        int endTimeValue   = endTime.getSeconds();

        if((endTimeValue - startTimeValue) < 0)
            endTimeValue = endTimeValue + 60;

        if(endTimeValue - startTimeValue < 30){
            int timeForSetProgress =  this.getRandonSecond();
            Handler handler1 = new Handler();
            handler1.postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    setProgressAndPointsOfSecondPlayer();
                }
            }, timeForSetProgress * 1000);
        }
    }

    /**
     * This method set the points and progress of second player
     */
    private void setProgressAndPointsOfSecondPlayer(){
        if(indexForSecodPlayerProgress >= 0 && indexForSecodPlayerProgress < MAX_QUESTION){
            if(questionListFinal.get(indexForSecodPlayerProgress).getIsCorrect() == 1){
                setSecondPlayerProgress(true);
                setSecondPlayerPoints();
            }else{
                setSecondPlayerProgress(false);
            }
        }
        indexForSecodPlayerProgress ++ ;
    }

    /**
     * This method return the seconds after second player progress will set
     * @return
     */
    private int getRandonSecond(){
        return (random.nextInt(MAX_SECONDS) + 1);
    }

    /**
     * This method set the progress
     */
    protected void setSecondPlayerProgress(boolean isRightAnswer){
        ImageView imgProgressBar = new ImageView(this);
        if(isRightAnswer){
            if(isTab){
                imgProgressBar.setBackgroundResource(R.drawable.green_ipad);
            }
            else {
                imgProgressBar.setBackgroundResource(R.drawable.green);
            }
        }
        else{
            if(isTab){
                imgProgressBar.setBackgroundResource(R.drawable.red_ipad);
            }
            else{
                imgProgressBar.setBackgroundResource(R.drawable.red);
            }
        }
        progressBarLayout1.addView(imgProgressBar);
    }

    /**
     * This method set the second player points
     */
    private void setSecondPlayerPoints(){
        points1 = points1 + this.getPointsForSolvingEachEquationForSecondPlayer();
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtPoints1.setText(transeletion.getTranselationTextByTextIdentifier("mfLblPts") + ": " + points1);
        transeletion.closeConnection();
    }

    /**
     * Get second player points to solve question
     * @return
     */
    @SuppressWarnings("deprecation")
    private int getPointsForSolvingEachEquationForSecondPlayer() {
        int startTimeValue = startTime.getSeconds();
        int endTimeValue   = new Date().getSeconds();

        int pointsForEachEquation = 0;
        if((endTimeValue - startTimeValue) < 0)
            endTimeValue = endTimeValue + 60;
        pointsForEachEquation = (MAX_POINTS -  (5 * (endTimeValue - startTimeValue)));
        if(pointsForEachEquation < 200)
            return 200;
        return pointsForEachEquation;
    }

    @Override
    public void onBackPressed() {

        startActivity(new Intent(this,SingleFriendzyMain.class));

        if(playSound != null)
            playSound.stopPlayer();

        if(getReadyTimer != null)
            getReadyTimer.cancel();

        if(isClickOnGo)
            this.clickOnBackPress();

        super.onBackPressed();
    }

    /**
     * This method call when user click on back press
     */
    private void clickOnBackPress() {

        this.savePlayerDataWhenNoInternetConnected();

        this.insertIntoPlayerTotalPoints();

        SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
        String userId;
        String playerId;
        if(sharedPreffPlayerInfo.getString("userId", "").equals("0"))
            userId = "0";
        else
            userId = sharedPreffPlayerInfo.getString("userId", "");

        if(sharedPreffPlayerInfo.getString("playerId", "").equals("0"))
            playerId = "0";
        else
            playerId = sharedPreffPlayerInfo.getString("playerId", "");

        MathScoreForSchoolCurriculumTransferObj mathScoreObj = new MathScoreForSchoolCurriculumTransferObj();
        mathScoreObj.setUserId(userId);
        mathScoreObj.setPlayerId(playerId);
        mathScoreObj.setProblems(this.getEquationSolveXmlForInterNetNotConnected(playDataList));

        SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(this);
        schoolImpl.openConnection();
        schoolImpl.insertIntoWordProblemResults(mathScoreObj);
        schoolImpl.closeConnection();

        if(CommonUtils.isInternetConnectionAvailable(this)){
            PlayerTotalPointsObj playerObj = null;
            LearningCenterimpl learningCenterimpl = new LearningCenterimpl(SingleFriendzySchoolCurriculumEquationSolve.this);
            learningCenterimpl.openConn();
            playerObj = learningCenterimpl.getDataFromPlayerTotalPoints(playerId);
            learningCenterimpl.closeConn();

            playerObj.setPlayerId(playerId);
            playerObj.setUserId(userId);
            new AddCoinAndPointsForLoginUser(playerObj , SingleFriendzySchoolCurriculumEquationSolve.this)
                    .execute(null,null,null);
        }
    }

    /**
     * This method set the play equation data to the list
     * for each equation which is to be save after completion of the
     * play all equations
     */
    @SuppressWarnings("deprecation")
    private void setplayDataToArrayList(){

        //changes for timer on 11 Mar
        endTime = new Date();
        //end changes

        MathEquationTransferObj mathObjData = new MathEquationTransferObj();
        mathObjData.setEqautionId(questionObj.getQuestionId());
        mathObjData.setStartDate(CommonUtils.formateDate(startTime));
        mathObjData.setEndData(CommonUtils.formateDate(endTime));

        int startTimeValue = startTime.getSeconds() ;
        int endTimeVlaue   = endTime.getSeconds();

        if(endTimeVlaue < startTimeValue)
            endTimeVlaue = endTimeVlaue + 60;

        mathObjData.setTimeTakenToAnswer(endTimeVlaue - startTimeValue);
        mathObjData.setCatId(questionObj.getCatId());//changes when deepak will come
        mathObjData.setSubCatId(questionObj.getSubCatId());//changes when deepak will come

        if(isCorrectAnsByUser == 0)
            mathObjData.setPoints(0);
        else
            mathObjData.setPoints(this.getPointsForSolvingEachEquation());

        mathObjData.setIsAnswerCorrect(isCorrectAnsByUser);

        if(answerByUser.length() > 0)
            mathObjData.setUserAnswer(answerByUser);
        else
            mathObjData.setUserAnswer("1");

        mathObjData.setQuestionObj(questionObj);

        playDataList.add(mathObjData);
    }

    /**
     * This method insert the data into total player points table
     * and also save data on server
     * @param
     */
    private void insertIntoPlayerTotalPoints()
    {
        SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
        String userId;
        String playerId;

        if(sharedPreffPlayerInfo.getString("userId", "").equals("0"))
            userId = "0";
        else
            userId = sharedPreffPlayerInfo.getString("userId", "");

        if(sharedPreffPlayerInfo.getString("playerId", "").equals("0"))
            playerId = "0";
        else
            playerId = sharedPreffPlayerInfo.getString("playerId", "");

        PlayerTotalPointsObj playerPoints = new PlayerTotalPointsObj();
        playerPoints.setUserId(userId);
        playerPoints.setPlayerId(playerId);
        playerPoints.setTotalPoints(points);

        numberOfCoins = (int) (points * ICommonUtils.COINS_PER_POINT);

        playerPoints.setCoins(numberOfCoins);
        playerPoints.setCompleteLevel(1);
        playerPoints.setPurchaseCoins(0);

        LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
        learningCenterObj.openConn();

        if(learningCenterObj.isPlayerTotalPointsExist(playerPoints.getPlayerId()))
        {
            int points = playerPoints.getTotalPoints();
            int coins  = playerPoints.getCoins();
            playerPoints.setTotalPoints(learningCenterObj.getDataFromPlayerTotalPoints
                    (playerPoints.getPlayerId()).getTotalPoints() + points);

            playerPoints.setCoins(learningCenterObj.getDataFromPlayerTotalPoints(playerPoints.getPlayerId()).getCoins()
                    + coins);

            learningCenterObj.deleteFromPlayerTotalPoints(playerPoints.getPlayerId());
        }

        learningCenterObj.insertIntoPlayerTotalPoints(playerPoints);

        learningCenterObj.closeConn();
    }

    /**
     * This class get equation fron server
     * @author Yashwant Singh
     *
     */
    private class GetWordProblemQuestionInFriendzy extends AsyncTask<Void, Void, ArrayList<SigleFriendzyQuestionFromServer>>{
        private String userId;
        private String playerId;
        ProgressDialog pd;
        GetWordProblemQuestionInFriendzy(String userId , String playerId){
            this.userId = userId;
            this.playerId = playerId;
        }

        @Override
        protected void onPreExecute() {
            pd = CommonUtils.getProgressDialog(SingleFriendzySchoolCurriculumEquationSolve.this);
            pd.show();
            super.onPreExecute();
        }

        @Override
        protected ArrayList<SigleFriendzyQuestionFromServer> doInBackground(Void... params) {
            SingleFriendzyServerOperationForWordProblem serverObj =
                    new SingleFriendzyServerOperationForWordProblem();
            ArrayList<SigleFriendzyQuestionFromServer> questionList =
                    serverObj.getWordProblemQuestionInFriendzy(userId, playerId);
            return questionList;
        }

        @Override
        protected void onPostExecute(ArrayList<SigleFriendzyQuestionFromServer> questionList) {
            pd.cancel();

            if(questionList != null && questionList.size() > 0){
                questionListFinal = questionList;
                getReadyTimer();
            }else{
                //Log.e(TAG, "try from server otherwise from database");
                getEquatonsFromLocalDatabase();
            }
            super.onPostExecute(questionList);
        }
    }

    /**
     * This asyncktask update the level and stars on server for bonus
     * @author Yashwant Singh
     *
     */
    class AddFriendzyForCompleteLevelForBonus extends AsyncTask<Void, Void, Void>
    {
        private String userId ;
        private String playerId;
        int level;
        int stars;
        String appId;

        AddFriendzyForCompleteLevelForBonus(String userId , String playerId , int level ,
                                            int stars , String appId)
        {
            this.userId = userId;
            this.playerId = playerId;
            this.level = level;
            this.stars = stars;
            this.appId = appId;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            SingleFriendzyServerOperation singleServerObj = new SingleFriendzyServerOperation();
            singleServerObj.addMathComplteLevelForBonus(userId, playerId, level, stars, appId);
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            super.onPostExecute(result);
        }
    }


    /**
     * Add word problem score on server
     * @author Yashwant Singh
     *
     */
    private class AddWordProblemFriendzyScore extends AsyncTask<Void, Void, Void>{

        private String userId;
        private String playerId;
        private String problems;
        private int totalscore;
        private int isFake;
        private String challangerPlayerId;
        private int isWin;
        private String gameType;

        AddWordProblemFriendzyScore(String userId , String playerId, String problems , int totalscore
                , int isFake , String challangerPlayerId , int isWin, String gameType){
            this.userId = userId;
            this.playerId = playerId;
            this.problems = problems;
            this.totalscore = totalscore;
            this.isFake = isFake;
            this.isWin = isWin;
            this.gameType = gameType;

            PlayerTotalPointsObj playerObj = null;
            LearningCenterimpl learningCenterimpl = new LearningCenterimpl
                    (SingleFriendzySchoolCurriculumEquationSolve.this);
            learningCenterimpl.openConn();
            playerObj = learningCenterimpl.getDataFromPlayerTotalPoints(playerId);
            learningCenterimpl.closeConn();

            this.totalscore = playerObj.getTotalPoints();
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            SingleFriendzyServerOperationForWordProblem serverObj =
                    new SingleFriendzyServerOperationForWordProblem();
            serverObj.addWordProblemFriendzyScore(userId, playerId, problems,
                    totalscore, isFake, challangerPlayerId, isWin, gameType);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
        }
    }

    /**
     * Add math score on server when user play
     * the previous level to the highest level
     * @author Yashwant Singh
     *
     */
    private class AddWordProblemMathScore extends AsyncTask<Void, Void, Void>{

        private String gameType;
        private String userId;
        private String playerId;
        private String problems;
        private int totalscore;

        AddWordProblemMathScore(String gemeType , String userId , String playerId ,
                                String problems , int totalscore){
            this.gameType = gemeType;
            this.userId = userId;
            this.playerId = playerId;
            this.problems = problems;
            this.totalscore = totalscore;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            SingleFriendzyServerOperationForWordProblem serverObj =
                    new SingleFriendzyServerOperationForWordProblem();
            serverObj.addMAthScore(gameType, userId, playerId, problems, totalscore);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
        }
    }

    @Override
    public void onInit(int status) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void onResume() {
        if(!isClickOnWorkArea){
            if(!isFromOnCreate){
                if(playSound != null)
                    playSound.playSoundForSingleFriendzyEquationSolve
                            (SingleFriendzySchoolCurriculumEquationSolve.this);
            }else{
                isFromOnCreate = false;
            }
        }else
            isClickOnWorkArea = false;
        super.onResume();
    }

    @Override
    protected void onPause() {
        if(!isClickOnWorkArea){
            if(playSound != null)
                playSound.stopPlayer();
        }
        super.onPause();
    }
}
