package com.mathfriendzy.controller.singlefriendzy;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.controller.learningcenter.SeeAnswerActivity;
import com.mathfriendzy.controller.learningcenter.schoolcurriculum.SeeAnswerActivityForSchoolCurriculum;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.MathEquationTransferObj;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;
import com.mathfriendzy.utils.MathVersions;
import com.mathfriendzy.utils.PlaySound;

import java.util.ArrayList;


public class CongratulationScreenForSingleFriendzy extends ActBaseClass implements OnClickListener
{
	private TextView txtTileScreen				= null;
	private TextView txtGoodEffort				= null;
	private TextView txtEarned					= null;
	private TextView txtEarnedPoints			= null;
	private TextView txtPlayAgain				= null;
	private Button	 btnAnswer					= null;
	private Button	 btnPlay					= null;
	private String   lblPts						= null;
	
	private int isWin = 0;
	private int points = 0;
	
	private boolean isFromSingleFriendzySchoolCurriculum = false;
	
	//changes for Spanish
	private int selectedPlayGrade = 1;
	PlaySound playsound = null;


    //For MathVersion Change
    private ImageView imgFriendzy = null;
    private TextView lblTheGameOfMath = null;
    private String lblTheGameOfMathMsg = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_congratulation_screen_for_single_friendzy);
		
		playsound = new PlaySound(this);
		
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);  
		  
		if(metrics.heightPixels == ICommonUtils.TAB_HEIGHT &&
                metrics.widthPixels == ICommonUtils.TAB_WIDTH && metrics.densityDpi == ICommonUtils.TAB_DENISITY)
		{
			setContentView(R.layout.activity_congratulation_screen_for_single_friendzy_tab_low_denisity);
		}
		
		this.getWidgetId();
		this.getIntentValue();
		this.setWidgetText();
        this.setLogoTextBasedOnMathVersion();
	}

    private void getIntentValue()
	{	
		points = this.getIntent().getIntExtra("points", 0);
		isWin  = this.getIntent().getIntExtra("isWin", 0);
		isFromSingleFriendzySchoolCurriculum = this.getIntent().
		getBooleanExtra("isFromLearnignCenterSchoolCurriculum", false);
		selectedPlayGrade = this.getIntent().getIntExtra("selectedGrade", 1);
		
		if(isFromSingleFriendzySchoolCurriculum)
			playsound.playSoundForSchoolSurriculumForResultScreen(this);
	}
	
	private void setWidgetText() 
	{
		String text;
		Translation translate = new Translation(this);
		translate.openConnection();

		text = translate.getTranselationTextByTextIdentifier("mfTitleHomeScreen");
		txtTileScreen.setText(text);
		
		if(isWin == 0)
			text = translate.getTranselationTextByTextIdentifier("lblGoodEffort");
		else
			text = translate.getTranselationTextByTextIdentifier("lblYouWon");
		
		txtGoodEffort.setText(text + "!");
		
		text = translate.getTranselationTextByTextIdentifier("lblYouEraned");
		txtEarned.setText(text);
		
		lblPts = translate.getTranselationTextByTextIdentifier("lblPts");
		txtEarnedPoints.setText(CommonUtils.setNumberString(points + "") + " " + lblPts);
		
		if(isWin == 1)
			text = translate.getTranselationTextByTextIdentifier("lblYouCanNowPlayTheNextLevel");
		else
			text = translate.getTranselationTextByTextIdentifier("lblYouHaveGoneBackOneLevel");
		
		txtPlayAgain.setText(text);
		
		text = translate.getTranselationTextByTextIdentifier("mfBtnTitleSeeAnswers");
		btnAnswer.setText(text);
		
		text = translate.getTranselationTextByTextIdentifier("mfBtnTitleCompete");
		btnPlay.setText(text);

        lblTheGameOfMathMsg = translate.getTranselationTextByTextIdentifier("lblTheGameOfMath");
		translate.closeConnection();
		
	}//END setWidgetText method
	
	private void getWidgetId() 
	{
		txtTileScreen		= (TextView) findViewById(R.id.txtTitleScreen);		
		txtGoodEffort		= (TextView) findViewById(R.id.txtGoodEffort);
		txtEarned			= (TextView) findViewById(R.id.txtEarned);
		txtEarnedPoints		= (TextView) findViewById(R.id.txtEarnedPoints);
		txtPlayAgain        = (TextView) findViewById(R.id.txtPlayAgain);
		
		btnAnswer			= (Button) findViewById(R.id.btnAnswer);
		btnPlay				= (Button) findViewById(R.id.btnPlay);


        imgFriendzy = (ImageView) findViewById(R.id.imgFriendzy);
        lblTheGameOfMath = (TextView) findViewById(R.id.lblTheGameOfMath);

		btnAnswer.setOnClickListener(this);
		btnPlay.setOnClickListener(this);
	}//END getWidgetId method
	
	@Override
	public void onClick(View v) 
	{
		switch (v.getId())
		{
		case R.id.btnAnswer:	
			if(isFromSingleFriendzySchoolCurriculum){
				@SuppressWarnings("unchecked")
				ArrayList<MathEquationTransferObj> playDataList = (ArrayList<MathEquationTransferObj>) this.getIntent()
						.getSerializableExtra("playDatalist");
				Intent intent1 = new Intent(this , SeeAnswerActivityForSchoolCurriculum.class);
				intent1.putExtra("playDatalist", playDataList);
				intent1.putExtra("isFromLearnignCenterSchoolCurriculum", false);
				intent1.putExtra("selectedGrade", selectedPlayGrade);
				startActivity(intent1);
			}else{
			startActivity(new Intent(this,SeeAnswerActivity.class));
			}
			break;
		
		case R.id.btnPlay:
			startActivity(new Intent(this,SingleFriendzyMain.class));
			break;
		}
		
	}//END onClick Method

	@Override
	public void onBackPressed() 
	{
		startActivity(new Intent(this,SingleFriendzyMain.class));
		super.onBackPressed();
	}
	
	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}

    private void setLogoTextBasedOnMathVersion() {
        if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_SIMPLE){
            imgFriendzy.setBackgroundResource(R.drawable.tab_mf_maths_friendzy_ipad);
            lblTheGameOfMath.setText(lblTheGameOfMathMsg);
        }else if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS){
            imgFriendzy.setBackgroundResource(R.drawable.mathfriendzy__plus_logo);
            lblTheGameOfMath.setText(lblTheGameOfMathMsg);
        }else if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_TUTOR_PLUS){
            imgFriendzy.setBackgroundResource(R.drawable.mathfriendzy_tutor_plus_logo);
            lblTheGameOfMath.setText(lblTheGameOfMathMsg);
        }
    }
}
