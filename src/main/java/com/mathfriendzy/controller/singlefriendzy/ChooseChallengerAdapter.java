package com.mathfriendzy.controller.singlefriendzy;

import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;

import java.io.IOException;
import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources.NotFoundException;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.singlefriendzy.schoolcurriculum.SingleFriendzySchoolCurriculumEquationSolve;
import com.mathfriendzy.dawnloadimagesfromserver.DawnloadImagesFromserver;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.LearnignCenterSchoolCurriculumServerOperation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.QuestionLoadedTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SchoolCurriculumLearnignCenterimpl;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.UpdatedInfoTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.WordProblemQuestionTransferObj;
import com.mathfriendzy.model.schoolcurriculum.singlefriendzy.SingleFriendzyServerOperationForWordProblem;
import com.mathfriendzy.model.singleFriendzy.ChallengerTransferObj;
import com.mathfriendzy.model.spanishchanges.SpanishChangesImpl;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;


public class ChooseChallengerAdapter extends BaseAdapter 
{
	private LayoutInflater mInflater   = null;
	private ViewHolder vholder 		   = null;
	private Context context 		   = null;
	private int resource 			   = 0;
	private int completeLevel          = 0;

	private ArrayList<ChallengerTransferObj> challengerList = null;

	private ArrayList<TextView> txtPointsList 		= null;
	private ArrayList<RelativeLayout> layoutList 	= null;
	private ArrayList<TextView> txtLevelList        = null;

	public static ChallengerTransferObj challengerDataObj = null;// to transfer value to the next screen

	//changes for Spanish
	private final String TAG = this.getClass().getSimpleName();

	ChooseChallengerAdapter(Context context ,  int resource ,
                            ArrayList<ChallengerTransferObj> challengerList){

		this.resource 			= resource;
		mInflater 				= LayoutInflater.from(context);
		this.context 			= context;
		this.challengerList   	= challengerList;

		txtPointsList 	= new ArrayList<TextView>();
		layoutList 		= new ArrayList<RelativeLayout>();
		txtLevelList    = new ArrayList<TextView>();

		for(int i = 0 ; i < challengerList.size() ; i ++ )
		{
			txtPointsList.add(null);
			layoutList.add(null);
			txtLevelList.add(null);
		}

		SharedPreferences sharedPreffPlayerInfo = context.getSharedPreferences(PLAYER_INFO, 0);
		completeLevel = sharedPreffPlayerInfo.getInt("completeLevel", 0);//get played complete level
	}

	@Override
	public int getCount() 
	{
		return challengerList.size();
	}

	@Override
	public Object getItem(int position) 
	{
		return null;
	}

	@Override
	public long getItemId(int position) 
	{

		return 0;
	}

	@Override
	public View getView(final int index, View view, ViewGroup parent) 
	{
		if(view == null)
		{
			vholder 			   	= new ViewHolder();
			view 				  	= mInflater.inflate(resource, null);
			vholder.imgFlag         = (ImageView)  view.findViewById(R.id.imgFlag);
			vholder.txtName   		= (TextView)   view.findViewById(R.id.txtName);
			vholder.txtPoints   	= (TextView)   view.findViewById(R.id.txtpoints);
			vholder.txtLevel		= (TextView)   view.findViewById(R.id.txtLevel);
			vholder.problemLayout   = (RelativeLayout) view.findViewById(R.id.reletiveAttributes);

			view.setTag(vholder);
		}
		else
		{
			vholder = (ViewHolder) view.getTag();
		}

		txtPointsList.set(index, vholder.txtPoints);
		layoutList.set(index, vholder.problemLayout);
		txtLevelList.set(index, vholder.txtLevel);

		this.setCountryImage(index, vholder.imgFlag);
		vholder.txtName.setText(MathFriendzyHelper.getStudentName(challengerList.get(index).getFirst() ,
                challengerList.get(index).getLast()));
		vholder.txtPoints.setText(CommonUtils.setNumberString(challengerList.get(index).getSum()));
		vholder.txtLevel.setText(completeLevel + "");

		vholder.problemLayout.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				layoutList.get(index).setBackgroundResource(R.drawable.mcg_green_bar);
				Country country = new Country();
				setCountryName(challengerList.get(index).getCountryIso() , index);
				txtLevelList.get(index).setText("");
				callForNextActivity(challengerList.get(index));

			}
		});
		return view;
	}

	/**
	 * Country iso may be country id
	 * @param countryIsoFromServer
	 */
	private void setCountryName(String countryIsoFromServer , int index){
		Country country = new Country();
		try{
			Integer.parseInt(countryIsoFromServer);
			txtPointsList.get(index).setText(country.getCountryNameByCountryId
					(countryIsoFromServer, context));
		}catch(Exception e){
			txtPointsList.get(index).setText(country.getCountryNameByCountryIso
					(countryIsoFromServer, context));
		}
	}


	/**
	 * This method set countryImage
	 */
	@SuppressWarnings("deprecation")
	private void setCountryImage(int index , ImageView imgFlag)
	{
		try 
		{

			imgFlag.setBackgroundDrawable(Drawable.createFromStream(context.getAssets().open(context.getResources().
					getString(R.string.countryImageFolder) +"/" 
					+ challengerList.get(index).getCountryIso() + ".png"), null));
		} 
		catch (IOException e) 
		{
			Country country = new Country();
			try 
			{
				imgFlag.setBackgroundDrawable(Drawable.createFromStream(context.getAssets().open(context.getResources().
						getString(R.string.countryImageFolder) +"/" 
						+ country.getCountryIsoByCountryId(challengerList.get(index).getCountryIso(), context) + ".png"), null));
			}
			catch (NotFoundException e1) 
			{
				Log.e("ChooseChallengerAdapter", "Inside setCountryImage Error No Image Found");
				//e1.printStackTrace();
			} 
			catch (IOException e1) 
			{
				Log.e("ChooseChallengerAdapter", "Inside setCountryImage Error No Image Found");
				//e1.printStackTrace();
			}

			//e.printStackTrace();
		}
	}


	/**
	 * This method when user click on challenger player
	 * @param challengerDataObj
	 */
	private void callForNextActivity(ChallengerTransferObj challengerDataObj)
	{
		this.challengerDataObj = challengerDataObj;

		//changes for word problem
		if(CommonUtils.isClickedSolveEquation)
			context.startActivity(new Intent(context , SingleFriendzyEquationSolve.class));
		else{
			this.clickForSchoolCurriculum();
		}
	}

	/**
	 * This method call when user play single school curriculum
	 */
	private void clickForSchoolCurriculum(){

		SharedPreferences sharedPreffPlayerInfo = context.getSharedPreferences(PLAYER_INFO, 0);
		int grade = sharedPreffPlayerInfo.getInt("grade" , 1);

		if(CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH){
			SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl = 
					new SchoolCurriculumLearnignCenterimpl(context);
			schooloCurriculumImpl.openConnection();
			boolean isQuestionLoaded = schooloCurriculumImpl.isQuestionLeaded(grade);
			schooloCurriculumImpl.closeConnection();
			
			if(isQuestionLoaded){
				if(CommonUtils.isInternetConnectionAvailable(context)){
					new GetWordProblemQuestionUpdatedDate(grade , false).execute(null,null,null);
				}else{
					/*Intent intent = new Intent(context , SingleFriendzySchoolCurriculumEquationSolve.class);
					intent.putExtra("playerSelectedGrade", grade);
					//context.startActivity(new Intent(context , SingleFriendzySchoolCurriculumEquationSolve.class));
					context.startActivity(intent);*/
					this.goForSchoolCurriculumPlayScreen(grade);
				}
			}
			else{
				if(CommonUtils.isInternetConnectionAvailable(context)){
					new GetWordProblemQuestion(grade ,context)
					.execute(null,null,null);
					new GetWordProblemQuestionUpdatedDate(grade , true).execute(null,null,null);
				}else{
					DialogGenerator dg = new DialogGenerator(context);
					Translation transeletion = new Translation(context);
					transeletion.openConnection();
					dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
					transeletion.closeConnection();	
				}
			}
		}else{
			callWhenUserLanguageIsSpanish(context , CommonUtils.SPANISH , grade);
		}
	}

	/**
	 * static class for view which hole the static objects of views
	 * @author Yashwant Singh
	 *
	 */
	static class ViewHolder
	{
		ImageView   imgFlag;
		TextView    txtName ;
		TextView    txtPoints;
		TextView    txtLevel;
		RelativeLayout problemLayout;
	}

	/**
	 * This class get word problem updated dates from server
	 * @author Yashwant Singh
	 *
	 */
	private class GetWordProblemQuestionUpdatedDate extends AsyncTask<Void, Void, ArrayList<UpdatedInfoTransferObj>>{
		private int grade;
		private ProgressDialog pd;
		private boolean isFisrtTimeCallForGrade;

		GetWordProblemQuestionUpdatedDate(int grade , boolean isFisrtTimeCallForGrade){
			this.grade = grade;
			this.isFisrtTimeCallForGrade = isFisrtTimeCallForGrade;
		}

		@Override
		protected void onPreExecute() {
			if(isFisrtTimeCallForGrade == false){
				pd = CommonUtils.getProgressDialog(context);
				pd.show();
			}
			super.onPreExecute();
		}

		@Override
		protected ArrayList<UpdatedInfoTransferObj>  doInBackground(Void... params) {
			SingleFriendzyServerOperationForWordProblem serverObj = 
					new SingleFriendzyServerOperationForWordProblem();
			//changes for Spanish
			ArrayList<UpdatedInfoTransferObj>  updatedList = new ArrayList<UpdatedInfoTransferObj>();
			if(CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH)
				updatedList = serverObj.getWordProbleQuestionUpdatedData();
			else//for Spanish
				updatedList = serverObj.getWordProbleQuestionUpdatedDateForSpanish(CommonUtils.SPANISH);
			return updatedList;
		}

		@Override
		protected void onPostExecute(ArrayList<UpdatedInfoTransferObj>  updatedList) {
			if(isFisrtTimeCallForGrade == false){
				pd.cancel();
			}

			if(updatedList != null){
				SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl = 
						new SchoolCurriculumLearnignCenterimpl(context);
				schooloCurriculumImpl.openConnection();

				//changes for Spanish
				SpanishChangesImpl implObj = new SpanishChangesImpl(context);
				implObj.openConn();

				if(isFisrtTimeCallForGrade){
					//changes for Spanish
					if(CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH){
						if(!schooloCurriculumImpl.isWordProblemUpdateDetailsTableDataExist()){
							schooloCurriculumImpl.insertIntoWordProblemUpdateDetails(updatedList);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
						}else{
							String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
							schooloCurriculumImpl.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
						}
					}else{//for Spanish
						if(!implObj.isWordProblemUpdateDetailsTableDataExist()){
							implObj.insertIntoWordProblemUpdateDetails(updatedList);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
						}else{
							String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
							implObj.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
						}
					}
				}
				else{
					if(CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH){
						String updatedDateFromDatabase = schooloCurriculumImpl.getUpdatedDate(grade);
						if(CommonUtils.isUpdateDateChange(updatedDateFromDatabase , grade , updatedList))
						{
							String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
							schooloCurriculumImpl.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();

							if(CommonUtils.isInternetConnectionAvailable(context)){
								new GetWordProblemQuestion(grade , context)
								.execute(null,null,null);
							}else{
								/*Intent intent = new Intent(context , SingleFriendzySchoolCurriculumEquationSolve.class);
							intent.putExtra("playerSelectedGrade", grade);
							//context.startActivity(new Intent(context , SingleFriendzySchoolCurriculumEquationSolve.class));
							context.startActivity(intent);*/
								//context.startActivity(new Intent(context , SingleFriendzySchoolCurriculumEquationSolve.class));
								goForSchoolCurriculumPlayScreen(grade);
							}
						}
						else{
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
							/*Intent intent = new Intent(context , SingleFriendzySchoolCurriculumEquationSolve.class);
						intent.putExtra("playerSelectedGrade", grade);
						//context.startActivity(new Intent(context , SingleFriendzySchoolCurriculumEquationSolve.class));
						context.startActivity(intent);*/
							//context.startActivity(new Intent(context , SingleFriendzySchoolCurriculumEquationSolve.class));
							goForSchoolCurriculumPlayScreen(grade);
						}

					}else{//for Spanish
						String updatedDateFromDatabase = implObj.getUpdatedDate(grade);
						if(CommonUtils.isUpdateDateChange(updatedDateFromDatabase , grade , updatedList))
						{
							String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
							implObj.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();

							if(CommonUtils.isInternetConnectionAvailable(context)){
								new GetWordProblemQuestion(grade , context)
								.execute(null,null,null);
							}else{
								/*Intent intent = new Intent(context , SingleFriendzySchoolCurriculumEquationSolve.class);
							intent.putExtra("playerSelectedGrade", grade);
							//context.startActivity(new Intent(context , SingleFriendzySchoolCurriculumEquationSolve.class));
							context.startActivity(intent);*/
								//context.startActivity(new Intent(context , SingleFriendzySchoolCurriculumEquationSolve.class));
								goForSchoolCurriculumPlayScreen(grade);
							}
						}
						else{
							schooloCurriculumImpl.closeConnection();
							implObj.closeConn();
							/*Intent intent = new Intent(context , SingleFriendzySchoolCurriculumEquationSolve.class);
						intent.putExtra("playerSelectedGrade", grade);
						//context.startActivity(new Intent(context , SingleFriendzySchoolCurriculumEquationSolve.class));
						context.startActivity(intent);*/
							//context.startActivity(new Intent(context , SingleFriendzySchoolCurriculumEquationSolve.class));
							goForSchoolCurriculumPlayScreen(grade);
						}
					}
				}
			}else{
				if(isFisrtTimeCallForGrade == false){
					CommonUtils.showInternetDialog(context);
				}
			}
			super.onPostExecute(updatedList);
		}
	}

	/**
	 * This asynctask get questions from server according to grade if not loaded in database
	 * @author Yashwant Singh
	 *
	 */
	public class GetWordProblemQuestion extends AsyncTask<Void, Void, QuestionLoadedTransferObj>{

		private int grade;
		private ProgressDialog pd;
		private Context context;
		ArrayList<String> imageNameList = new ArrayList<String>();

		public GetWordProblemQuestion(int grade , Context context){
			this.grade 		= grade;
			this.context 	= context;
		}

		@Override
		protected void onPreExecute() {

			Translation translation = new Translation(context);
			translation.openConnection();
			pd = CommonUtils.getProgressDialog(context, translation.getTranselationTextByTextIdentifier
					("alertPleaseWaitWeAreDownloading") , true);
			translation.closeConnection();
			pd.show();

			super.onPreExecute();
		}

		@Override
		protected QuestionLoadedTransferObj doInBackground(Void... params) {
			LearnignCenterSchoolCurriculumServerOperation serverObj = new LearnignCenterSchoolCurriculumServerOperation();
			//QuestionLoadedTransferObj questionData = serverObj.getWordProblemQuestions(grade);
			//changes for Spanish Changes
			QuestionLoadedTransferObj questionData = new QuestionLoadedTransferObj();
			if(CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH)
				questionData = serverObj.getWordProblemQuestions(grade);
			else
				questionData = serverObj.getWordProblemQuestionsForSpanish(grade, CommonUtils.SPANISH);

			//changes for dialog loading wheel
			/*SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl = new SchoolCurriculumLearnignCenterimpl(context);
			schooloCurriculumImpl.openConnection();
			schooloCurriculumImpl.deleteFromWordProblemCategoriesbyGrade(grade);
			schooloCurriculumImpl.deleteFromWordProblemsSubCategoriesByCategoryId(questionData.getCategoryList());
			schooloCurriculumImpl.deleteFromWordProblemsQuestionsCategoryId(questionData.getCategoryList());

			schooloCurriculumImpl.insertIntoWordProblemCategories(questionData.getCategoryList(), grade);
			schooloCurriculumImpl.insertIntoWordProblemsSubCategories(questionData.getCategoryList());
			schooloCurriculumImpl.insertIntoWordProblemsQuestions(questionData.getQuestionList());*/

			if(questionData != null){
				SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl = new SchoolCurriculumLearnignCenterimpl(context);
				schooloCurriculumImpl.openConnection();
				if(CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH){
					//changes for dialog loading wheel
					schooloCurriculumImpl.deleteFromWordProblemCategoriesbyGrade(grade);
					schooloCurriculumImpl.deleteFromWordProblemsSubCategoriesByCategoryId(questionData.getCategoryList());
					schooloCurriculumImpl.deleteFromWordProblemsQuestionsCategoryId(questionData.getCategoryList());

					schooloCurriculumImpl.insertIntoWordProblemCategories(questionData.getCategoryList(), grade);
					schooloCurriculumImpl.insertIntoWordProblemsSubCategories(questionData.getCategoryList());
					schooloCurriculumImpl.insertIntoWordProblemsQuestions(questionData.getQuestionList());
				}else{
					SpanishChangesImpl implObj = new SpanishChangesImpl(context);
					implObj.openConn();

					implObj.deleteFromWordProblemCategoriesbyGrade(grade);
					implObj.deleteFromWordProblemsSubCategoriesByCategoryId(questionData.getCategoryList());
					implObj.deleteFromWordProblemsQuestionsCategoryId(questionData.getCategoryList());

					implObj.insertIntoWordProblemCategories(questionData.getCategoryList(), grade);
					implObj.insertIntoWordProblemsSubCategories(questionData.getCategoryList());
					implObj.insertIntoWordProblemsQuestions(questionData.getQuestionList());
					implObj.closeConn();
				}

				for(int i = 0 ; i < questionData.getQuestionList().size() ; i ++ ){

					WordProblemQuestionTransferObj questionObj = questionData.getQuestionList().get(i);

					if(questionObj.getQuestion().contains(".png"))
						imageNameList.add(questionObj.getQuestion());
					if(questionObj.getOpt1().contains(".png"))
						imageNameList.add(questionObj.getOpt1());
					if(questionObj.getOpt2().contains(".png"))
						imageNameList.add(questionObj.getOpt2());
					if(questionObj.getOpt3().contains(".png"))
						imageNameList.add(questionObj.getOpt3());
					if(questionObj.getOpt4().contains(".png"))
						imageNameList.add(questionObj.getOpt4());
					if(questionObj.getOpt5().contains(".png"))
						imageNameList.add(questionObj.getOpt5());
					if(questionObj.getOpt6().contains(".png"))
						imageNameList.add(questionObj.getOpt6());
					if(questionObj.getOpt7().contains(".png"))
						imageNameList.add(questionObj.getOpt7());
					if(questionObj.getOpt8().contains(".png"))
						imageNameList.add(questionObj.getOpt8());
					if(!questionObj.getImage().equals(""))
						imageNameList.add(questionObj.getImage());
				}

				if(!schooloCurriculumImpl.isImageTableExist()){
					schooloCurriculumImpl.createSchoolCurriculumImageTable();
				}
				schooloCurriculumImpl.closeConnection();
				//end changes
			}
			return questionData;
		}

		@Override
		protected void onPostExecute(QuestionLoadedTransferObj questionData) {

			pd.cancel();

			if(questionData != null){
				DawnloadImagesFromserver serverObj = new DawnloadImagesFromserver(imageNameList, context);
				Thread imageDawnLoadThrad = new Thread(serverObj);
				imageDawnLoadThrad.start();

				/*Intent intent = new Intent(context , SingleFriendzySchoolCurriculumEquationSolve.class);
			intent.putExtra("playerSelectedGrade", grade);
			//context.startActivity(new Intent(context , SingleFriendzySchoolCurriculumEquationSolve.class));
			context.startActivity(intent);*/
				goForSchoolCurriculumPlayScreen(grade);
			}else{
				CommonUtils.showInternetDialog(context);
			}

			//context.startActivity(new Intent(context , SingleFriendzySchoolCurriculumEquationSolve.class));
			super.onPostExecute(questionData);
		}
	}

	//changes for Spanish
	/**
	 * This method call when user language is Spanish
	 * This method for downloading question from server for Spanish Language
	 */
	private void callWhenUserLanguageIsSpanish(Context context , int lang , int grade){
		try {
			SpanishChangesImpl spanishImplObj = new SpanishChangesImpl(context);
			spanishImplObj.openConn();
			//CommonUtils.CheckWordProblemSpanishTable(context);
			boolean isSpanishQuestionLoaded = spanishImplObj.isQuestionLeaded(grade);

			if(isSpanishQuestionLoaded){
				if(CommonUtils.isInternetConnectionAvailable(context)){
					new GetWordProblemQuestionUpdatedDate(grade , false).execute(null,null,null);
				}else{

					/*Intent intent = new Intent(context , SingleFriendzySchoolCurriculumEquationSolve.class);
					intent.putExtra("playerSelectedGrade", grade);
					//context.startActivity(new Intent(context , SingleFriendzySchoolCurriculumEquationSolve.class));
					context.startActivity(intent);*/
					//context.startActivity(new Intent(context , SingleFriendzySchoolCurriculumEquationSolve.class));
					this.goForSchoolCurriculumPlayScreen(grade);
				}
			}
			else{
				if(CommonUtils.isInternetConnectionAvailable(context)){
					new GetWordProblemQuestion(grade ,context)
					.execute(null,null,null);
					new GetWordProblemQuestionUpdatedDate(grade , true).execute(null,null,null);
				}else{
					DialogGenerator dg = new DialogGenerator(context);
					Translation transeletion = new Translation(context);
					transeletion.openConnection();
					dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
					transeletion.closeConnection();	
				}
			}
			spanishImplObj.closeConn();

		} catch (Exception e) {
			Log.e(TAG, "inside callWhenUserLanguageIsSpanish " + e.toString());
		} 
	}
	//end changes

	/**
	 * This method call when user want to play with scool curriculum
	 * @param grade
	 */
	private void goForSchoolCurriculumPlayScreen(int grade){
		Intent intent = new Intent(context , SingleFriendzySchoolCurriculumEquationSolve.class);
		intent.putExtra("playerSelectedGrade", grade);
		//context.startActivity(new Intent(context , SingleFriendzySchoolCurriculumEquationSolve.class));
		context.startActivity(intent);
	}
}
