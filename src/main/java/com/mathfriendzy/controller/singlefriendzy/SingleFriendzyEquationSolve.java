package com.mathfriendzy.controller.singlefriendzy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources.NotFoundException;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.learningcenter.LearningCenterEquationSolve;
import com.mathfriendzy.controller.learningcenter.OnAnswerGivenComplete;
import com.mathfriendzy.controller.learningcenter.OnReverseDialogClose;
import com.mathfriendzy.controller.learningcenter.SeeAnswerActivity;
import com.mathfriendzy.controller.learningcenter.workarea.WorkAreaActivity;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.friendzy.SaveTimePointsForFriendzyChallenge;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.MathEquationOperationCategorytransferObj;
import com.mathfriendzy.model.learningcenter.MathResultTransferObj;
import com.mathfriendzy.model.learningcenter.MathScoreTransferObj;
import com.mathfriendzy.model.learningcenter.PlayerEquationLevelObj;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.learningcenter.SeeAnswerTransferObj;
import com.mathfriendzy.model.multifriendzy.MultiFriendzyImpl;
import com.mathfriendzy.model.registration.Register;
import com.mathfriendzy.model.singleFriendzy.ChallengerTransferObj;
import com.mathfriendzy.model.singleFriendzy.EquationFromServerTransferObj;
import com.mathfriendzy.model.singleFriendzy.SingleFriendzyEquationObj;
import com.mathfriendzy.model.singleFriendzy.SingleFriendzyImpl;
import com.mathfriendzy.model.singleFriendzy.SingleFriendzyServerOperation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;
import com.mathfriendzy.utils.PlaySound;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import static com.mathfriendzy.controller.learningcenter.IOperationId.ADDITION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.DECIMAL_ADDITION_SUBTRACTION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.DECIMAL_MULTIPLICATION_DIVISION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.DIVISION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.FRACTION_ADDITION_SUBTRACTION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.FRACTION_MULTIPLICATION_DIVISION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.MULTIPLICATION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.NEGATIVE_ADDITION_SUBTRACTION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.NEGATIVE_MULTIPLICATION_DIVISION;
import static com.mathfriendzy.controller.learningcenter.IOperationId.SUBTRACTION;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LEARNING_CENTER_BG_INFO;
import static com.mathfriendzy.utils.ICommonUtils.LEARNING_CENTER_EQUATION_SOLVE_WITH_TIMER;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;
import static com.mathfriendzy.utils.ICommonUtils.SINGLE_FRIENDZY_EQUATION_SOLVE;


/**
 * This activity show and solve single friendzy equations
 * @author Yashwant Singh
 *
 */
public class SingleFriendzyEquationSolve extends LearningCenterEquationSolve
{
	private TextView txtUserName1 		= null;
	private TextView txtLap1        	= null;
	private TextView txtPoints1     	= null;
	private ImageView imgFlag1      	= null;
	private ProgressBar layoutProgress1 = null;

	private TextView txtUserName2 		= null;
	private TextView txtLap2        	= null;
	private TextView txtPoints2     	= null;
	private ImageView imgFlag2     		= null;
	private ProgressBar layoutProgress2	= null;

	private TextView txtTimer 			= null;

	private ImageView getReady = null;

	private long START_TIME_FOR_GET_READY = 7 * 1000;
	//private long START_TIME_FOR_GET_READY = 1 * 1000;

	private final String TAG = this.getClass().getSimpleName();

	private long startTimerForRoughArea = 0;
	private CountDownTimer timer 		= null;
	private long  START_TIME 			= 180 * 1000;//start time for timer
	//private long  START_TIME 			= 30 * 1000;//start time for timer
	private final long END_TIME   		= 1 * 1000;//end time for timer

	private int progress   = 0;
	private int progress1   = 0;
	private Date startTime = null;
	private Date endTime   = null;

	private int startTimeValue = 0;//this variable contains the number of seconds of starttime
	private int endTimeVlaue   = 0;//this variable contain the number of seconds of endtime

	private LearningCenterTransferObj learnignCenterobj		= null;
	private ChallengerTransferObj challengerDataObj 		= null;

	private ArrayList<MathEquationOperationCategorytransferObj> mathOperationList  		= null;
	private ArrayList<MathEquationOperationCategorytransferObj> newMathOperationList  	= null;

	private String resultFromDatabase 			= "";
	private ArrayList<String> resultDigitsList  = null;

	private ArrayList<Integer> onebyoneDigitList 	= null;
	private ArrayList<Integer> twobyoneDigitList 	= null;
	private ArrayList<Integer> twobytwoDigitList 	= null;
	private ArrayList<Integer> threebytwoDigitList 	= null;
	private ArrayList<Integer> threebythreeDigitList= null;
	private ArrayList<Integer> onebytwoDigitList    = null;

	//variable which hold the data to transfer to the answer screen
	private ArrayList<LearningCenterTransferObj> playEquationList 	= null;
	private ArrayList<String> userAnswerList 						= null;

	private boolean isClickOnGo 	= false;
	private String userAnswer 		= "";
	//private PlaySound playsound  = null;
	private int lap     = 1;
	private int lap1    = 1;
	private int rightAnsCounter   = 0;
	private int isCorrectAnsByUser= 0;
	private int showEquationIndex = 0;//this index hold the sequence for shoe equation
	private final int SLEEP_TIME        = 2000; //for delay time for displaying result after 3 attempts wrong
	private int points = 0;
	private int pointsForEachQuestion = 0;
	private boolean isEquationFromServer = false;
	private int secondPlayerpoints = 0;
	//private boolean isStopSecondPlayerProgress = false;

	private int numberOfCoins     = 0;
	//private float coinsPerPoint   = 0.05f;
	private boolean isClickOnBackPressed = false;
	private ArrayList<MathScoreTransferObj> playDatalist 	= null;//this list hold the equation time for solving equation
	public static int completeLevel = 0;//level user play at current which is set in Single FriendzyMain
	private int equationId = 0;
	private int isWin = 0;
	private ArrayList<EquationFromServerTransferObj> equationList = null;//hold the equation data from server
	private final int EQUAITON_TYPE = 11;
	private String gameType = "Play";
	private int starForPlayer = 0;
	public static SeeAnswerTransferObj seeAnswerDataObj = null;

	CountDownTimer getReadyTimer = null;

	//for complete level
	private boolean isCompleteLevelChange = false;

	//for progress
	private boolean vesrionGreaterThan = true;

	//added by shilpi for friendzy
	private long totalTimeTaken = 0l;

	private final int BONUS_COINS = 200;

	//changes for wrong timer , on 21 Mar
	private int totalTime  = 0 ;
	//end changes

	//for sound
	private boolean isFromOnCreate = false;


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_single_friendzy_equation_solve);

		//for sound
		isFromOnCreate = true;

		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "inside onCreate()");

		//changes for version
		if (android.os.Build.VERSION.RELEASE.startsWith("1.") ||
				android.os.Build.VERSION.RELEASE.startsWith("2.0") ||
				android.os.Build.VERSION.RELEASE.startsWith("2.1") ||
				android.os.Build.VERSION.RELEASE.startsWith("2.2")){
			vesrionGreaterThan = false;
		}
		//end changes

		/*DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);  

		boolean tabletSize = getResources().getBoolean(R.bool.isTablet);

		if(tabletSize)
		{
			//Log.e(TAG, "Tab");

			isTab = true;
			if(metrics.heightPixels <= TAB_HEIGHT_1024)
			{
				//Log.e(TAG, "Tab1");
				maxWidth = 80;
				MAX_WIDTH_FOR_FRACTION_TEXT = 35;
				widthForFractionLayout = 80;
				max_width_for_layout_maultiplication = 80;
			}
			else
			{
				if (metrics.densityDpi > 160){
					//Log.e(TAG, "Tab2");
					maxWidth = 110;
					MAX_WIDTH_FOR_FRACTION_TEXT = 50;
					widthForFractionLayout = 110;
					max_width_for_layout_maultiplication = 110;//changes for tab
				}
				else{
					//Log.e(TAG, "Tab3");
					maxWidth = 80;
					MAX_WIDTH_FOR_FRACTION_TEXT = 35;
					widthForFractionLayout = 80;
					max_width_for_layout_maultiplication = 80;
				}
			}

			//changes for tab
			//Log.e(TAG, " max width " + maxWidth);
		}
		else{
			if ((getResources().getConfiguration().screenLayout &
					Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
					metrics.densityDpi > SCREEN_DENISITY)
			{
				maxWidth = 100;
				MAX_WIDTH_FOR_FRACTION_TEXT = 50;
				widthForFractionLayout = 90;
				max_width_for_layout_maultiplication = 80;
			}
			else
			{		
				if ((getResources().getConfiguration().screenLayout &
						Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
						metrics.densityDpi > 160
						&&
						(getResources().getConfiguration().screenLayout &
								Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
								metrics.densityDpi <= SCREEN_DENISITY)
				{
					maxWidth = 75;
					MAX_WIDTH_FOR_FRACTION_TEXT = 38;
					widthForFractionLayout = 67;
					max_width_for_layout_maultiplication = 60;
				}
				else
				{
					maxWidth = 50;
					MAX_WIDTH_FOR_FRACTION_TEXT = 25;
					widthForFractionLayout = 45;
					max_width_for_layout_maultiplication = 40;
				}
			}
		}*/

		startTime = new Date();
		endTime   = new Date();

		playsound = new PlaySound(this);
		playDatalist       = new ArrayList<MathScoreTransferObj>();
		resultTextViewList = new ArrayList<TextView>();
		//playDatalist       = new ArrayList<MathScoreTransferObj>();
		learnignCenterobj  = new LearningCenterTransferObj();

		//Array List Which contain the category id for 1*1,1*2 or etc digits
		onebyoneDigitList 		= new ArrayList<Integer>();
		twobyoneDigitList 		= new ArrayList<Integer>();
		twobytwoDigitList 		= new ArrayList<Integer>();
		threebytwoDigitList 	= new ArrayList<Integer>();
		threebythreeDigitList 	= new ArrayList<Integer>();
		onebytwoDigitList       = new ArrayList<Integer>();

		//for transfer info to the answer screen

		playEquationList = new ArrayList<LearningCenterTransferObj>();
		//equationList     = new ArrayList<EquationFromServerTransferObj>();
		userAnswerList   = new ArrayList<String>();

		seeAnswerDataObj = new SeeAnswerTransferObj();

		//for question UI , Issues list 5 march 2014 , issues no. 1
		this.initilizeHeightAndWidthForDynamicLayoutCreation();
		//end changes

		this.setWidgetsReferences();
		this.setWidgetsReferencesForSinleFriendzy();
		this.setWidgetsValuesFromtranselation();
		this.setTextFromeTranslationForSingleFriendzy();
		this.setPlayerDetail();
		this.getIntentValue();
		this.setlistenerOnWidgets();
		this.getEquations();

		this.keyBoardDisable();//disable kayboard at the starting
		btnRoughWork.setClickable(false);

		this.getReadyTimer();

		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "outside onCreate()");

	}

	private void setTextFromeTranslationForSingleFriendzy()
	{
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		txtPoints1.setText(transeletion.getTranselationTextByTextIdentifier("mfLblPts") + ": " + points);
		txtLap1.setText(transeletion.getTranselationTextByTextIdentifier("mfLblLap") + ": " + lap);
		txtPoints2.setText(transeletion.getTranselationTextByTextIdentifier("mfLblPts") + ": " + secondPlayerpoints);
		txtLap2.setText(transeletion.getTranselationTextByTextIdentifier("mfLblLap") + ": " + lap1);
		transeletion.closeConnection();
	}

	/**
	 * This method set the widgets references from the layout to the reference object
	 */
	private void setWidgetsReferencesForSinleFriendzy()
	{
		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "inside setWidgetsReferences()");


		txtUserName1    = (TextView) findViewById(R.id.txtUserName);
		txtLap1			= (TextView) findViewById(R.id.txtLap);
		txtPoints1 		= (TextView) findViewById(R.id.txtPoints);
		imgFlag1		= (ImageView) findViewById(R.id.imgFlag);
		layoutProgress1	= (ProgressBar) findViewById(R.id.layoutProgress);

		txtUserName2    = (TextView) findViewById(R.id.txtUserName1);
		txtLap2 		= (TextView) findViewById(R.id.txtLap1);
		txtPoints2 		= (TextView) findViewById(R.id.txtPoints1);
		imgFlag2		= (ImageView) findViewById(R.id.imgFlag1);
		layoutProgress2	= (ProgressBar) findViewById(R.id.layoutProgress1);

		txtTimer 		= (TextView) findViewById(R.id.txtTimer);
		getReady = (ImageView) findViewById(R.id.getReady);


		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "outside setWidgetsReferences()");

	}

	/**
	 * This method change the image on each seconds for get ready
	 */
	private void getReadyTimer()
	{
		getReadyTimer = new CountDownTimer(START_TIME_FOR_GET_READY,1)
		{
			@Override
			public void onTick(long millisUntilFinished)
			{
				int value = (int) ((millisUntilFinished/1000) % 60) ;
				setBacKGroundForGetReadyImage(value);
			}

			@Override
			public void onFinish()
			{
				getReady.setVisibility(ImageView.INVISIBLE);
				txtTimer.setVisibility(TextView.VISIBLE);

				timer = new MyTimer(START_TIME, END_TIME);
				timer.start();

				playsound.playSoundForSingleFriendzyEquationSolve(SingleFriendzyEquationSolve.this);

				keyBoardEnable();

				btnRoughWork.setClickable(true);

				if(equationList != null)
				{
					showFirstEquation();
				}
				else
				{
					getEquationsFromLocalDatabase();
					showFirstEquation();
				}
				//showFirstEquation();
			}
		};
		getReadyTimer.start();
	}


	/**
	 * This method show the first Equation and start to set second player progress
	 */
	private void showFirstEquation()
	{
		//Log.e(TAG, "inside showFirstEquation ");

		setSecondPlayerProgress();

		LearningCenterimpl learnignCenterImpl = new LearningCenterimpl(SingleFriendzyEquationSolve.this);
		learnignCenterImpl.openConn();
		showEqautionData(learnignCenterImpl.getEquation(equationsIdList.get(showEquationIndex)));
		learnignCenterImpl.closeConn();
	}


	/**
	 * This class show the timer for player
	 * @author Yashwant Singh
	 *
	 */
	private class MyTimer extends CountDownTimer
	{
		public MyTimer(long millisInFuture, long countDownInterval)
		{
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onTick(long millisUntilFinished)
		{
			startTimerForRoughArea = millisUntilFinished;

			if(((millisUntilFinished/1000) % 60) < 10) // Calculate the second if it less then 10 then add 0 prefix
				txtTimer.setText(((millisUntilFinished/1000) / 60) + ":0" + ((millisUntilFinished/1000) % 60));
			else
				txtTimer.setText(((millisUntilFinished/1000) / 60) + ":" + ((millisUntilFinished/1000) % 60));
		}

		@Override
		public void onFinish()
		{
			//Log.e("", "Stop");
			playsound.stopPlayer();

			isStopSecondPlayerProgress = true;//for stop the second player progress
			/*gameType = "Compete";//will ask to deepak sir when we change game type , if user play previous level
								// then it need to set compete or play
			 */
			//check current level with the database stored level
			SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
			String userId = sharedPreffPlayerInfo.getString("userId", "") ;
			String playerId = sharedPreffPlayerInfo.getString("playerId", "");

			int completeLevelForAnswerAcitiy = completeLevel;
			if(completeLevel == sharedPreffPlayerInfo.getInt("completeLevel", 0))
			{
				gameType = "Compete";//will ask to deepak sir when we change game type , if user play previous level
				// then it need to set compete or play

				if(points > secondPlayerpoints)
				{
					isWin = 1 ;
					completeLevel ++ ;
				}
				else
				{
					isWin = 0 ;
					if(completeLevel > 1)
						completeLevel -- ;
				}

				isCompleteLevelChange = true;
				SharedPreferences.Editor editor = sharedPreffPlayerInfo.edit();
				editor.putInt("completeLevel", completeLevel);
				editor.commit();

				insertOrUpdatePlayerEquationTable();
			}

			/*Log.e(TAG, "Player First points  " + points);
			Log.e(TAG, "Player second points" + secondPlayerpoints);
			Log.e(TAG, "Complete level " + completeLevel);*/

			//changs for correct time on result screen , on 07 Mar 2014
			totalTime = getNewMathPlayedData(playDatalist);
			//end changes

			SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
			if(sheredPreference.getBoolean(IS_LOGIN, false))
			{
				if(CommonUtils.isInternetConnectionAvailable(SingleFriendzyEquationSolve.this))
				{
					new AddFriendzyForCompleteLevelForBonus(userId, playerId, completeLevel, starForPlayer,
							CommonUtils.APP_ID).execute(null,null,null);

					MathResultTransferObj mathObj = savePlayDataToDataBase();
					mathObj.setProblems(mathObj.getProblems() + getEquationUnSolveXml());
					new SaveTempPlayerScoreOnServer(mathObj).execute(null,null,null);

					//added by shilpi for friendzy
					if(CommonUtils.isActivePlayer){
						/*new SaveTimePointsForFriendzyChallenge(totalTimeTaken, points, SingleFriendzyEquationSolve.this)
						.execute(null, null, null);*/
						new SaveTimePointsForFriendzyChallenge(totalTime, points, SingleFriendzyEquationSolve.this)
						.execute(null, null, null);
					}
				}
				else
				{
					LearningCenterimpl learningCenterObj = new LearningCenterimpl(SingleFriendzyEquationSolve.this);
					learningCenterObj.openConn();
					learningCenterObj.insertIntoMathResult(savePlayDataToDataBase());
					learningCenterObj.closeConn();
				}
			}
			else
			{
				LearningCenterimpl learningCenterObj = new LearningCenterimpl(SingleFriendzyEquationSolve.this);
				learningCenterObj.openConn();
				learningCenterObj.insertIntoMathResult(savePlayDataToDataBase());
				learningCenterObj.closeConn();
			}

			updateStarAndLevel(userId , playerId);
			updatePlayerTotalPointsTable(playerId , userId);
			savePlayerDataWhenNoInternetConnected();

			seeAnswerDataObj.setPlayerName(sharedPreffPlayerInfo.getString("playerName", ""));
			Country country = new Country();
			String coutryIso = country.getCountryIsoByCountryName(sharedPreffPlayerInfo.getString("countryName", ""), SingleFriendzyEquationSolve.this);

			seeAnswerDataObj.setNoOfCorrectAnswer(rightAnsCounter);
			seeAnswerDataObj.setLevel(completeLevelForAnswerAcitiy);
			seeAnswerDataObj.setPoints(points);
			seeAnswerDataObj.setCountryISO(coutryIso);
			seeAnswerDataObj.setEquationList(playEquationList);
			seeAnswerDataObj.setUserAnswerList(userAnswerList);

			SeeAnswerActivity.isFromLearnignCenter = false;

			Intent intent = new Intent(SingleFriendzyEquationSolve.this,CongratulationScreenForSingleFriendzy.class);
			intent.putExtra("points", points);
			intent.putExtra("isWin", isWin);
			intent.putExtra("isFromLearnignCenterSchoolCurriculum", false);
			startActivity(intent);
		}
	}


	//changes for update player points in local when Internet is not connected
	//updated on 26 11 2014
	private void savePlayerDataWhenNoInternetConnected(){
		try{
			SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
			String userId;
			String playerId;
			if(sharedPreffPlayerInfo.getString("userId", "").equals("0"))
				userId = "0";
			else
				userId = sharedPreffPlayerInfo.getString("userId", "");

			if(sharedPreffPlayerInfo.getString("playerId", "").equals("0"))
				playerId = "0";
			else
				playerId = sharedPreffPlayerInfo.getString("playerId", "");

			MathResultTransferObj mathResultObj = new MathResultTransferObj();
			mathResultObj.setUserId(userId);
			mathResultObj.setPlayerId(playerId);
			mathResultObj.setTotalScore(points);
			mathResultObj.setCoins((int) (points * ICommonUtils.COINS_PER_POINT));
			mathResultObj.setLevel(completeLevel);
			MathFriendzyHelper.insertPlayerPlayedDataIntoLocalEarnedScore(mathResultObj, this);
		}catch(Exception e){
			e.printStackTrace();
		}
	}//end updation on 26 11 2014

	/**
	 * This method update star and level of the player
	 * @param userId
	 * @param playerId
	 */
	private void updateStarAndLevel(String userId , String playerId)
	{
		//Log.e(TAG, "inside updateStarAndLevel userId , player Id " + userId + " , " + playerId);

		SingleFriendzyImpl singleimpl = new SingleFriendzyImpl(SingleFriendzyEquationSolve.this);
		singleimpl.openConn();
		PlayerEquationLevelObj playrEquationObj = singleimpl.getEquaitonDataFromPlayerEquationTable
				(userId, playerId, EQUAITON_TYPE);
		singleimpl.closeConn();

		LearningCenterimpl learningCenterimpl = new LearningCenterimpl(SingleFriendzyEquationSolve.this);
		learningCenterimpl.openConn();
		PlayerTotalPointsObj playerPoints =  learningCenterimpl.
				getDataFromPlayerTotalPoints(playrEquationObj.getPlayerId());

		int level = playrEquationObj.getLevel();

		if(level != 1 && level == completeLevel && ((level-1) % 10 == 0))
		{
			if(playrEquationObj.getStars() == 0)
			{
				starForPlayer = 1;
				SingleFriendzyImpl singleFriendzy = new SingleFriendzyImpl(this);
				singleFriendzy.openConn();

				singleFriendzy.updateEquaitonPlayerData(playrEquationObj.getUserId(),
						playrEquationObj.getPlayerId(), EQUAITON_TYPE, playrEquationObj.getLevel(), 1);
				singleFriendzy.closeConn();

				playerPoints.setCoins(playerPoints.getCoins() + BONUS_COINS);//add 200 coins

				learningCenterimpl.deleteFromPlayerTotalPoints(playrEquationObj.getPlayerId());

				playerPoints.setUserId(userId);
				learningCenterimpl.insertIntoPlayerTotalPoints(playerPoints);

				//changes when school curriculum
				SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
				if(sheredPreference.getBoolean(IS_LOGIN, false))
				{
					if(CommonUtils.isInternetConnectionAvailable(SingleFriendzyEquationSolve.this))
					{
						new AddFriendzyForCompleteLevelForBonus(userId, playerId, completeLevel, starForPlayer, CommonUtils.APP_ID).execute(null,null,null);
					}
				}
			}
		}

		learningCenterimpl.closeConn();

		/*SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
		if(sheredPreference.getBoolean(IS_LOGIN, false))
		{
			if(CommonUtils.isInternetConnectionAvailable(SingleFriendzyEquationSolve.this))
			{				
				new AddFriendzyForCompleteLevelForBonus(userId, playerId, completeLevel, starForPlayer, CommonUtils.APP_ID).execute(null,null,null);
			}
		}*/
	}

	/**
	 * Update player total points table
	 * @param playerId
	 */
	private void updatePlayerTotalPointsTable(String playerId , String userId)
	{
		numberOfCoins = (int) (points * ICommonUtils.COINS_PER_POINT);

		LearningCenterimpl learningCenterimpl = new LearningCenterimpl(SingleFriendzyEquationSolve.this);
		learningCenterimpl.openConn();
		PlayerTotalPointsObj playerPoints =  learningCenterimpl.getDataFromPlayerTotalPoints(playerId);

		playerPoints.setTotalPoints(playerPoints.getTotalPoints() + points);
		playerPoints.setCoins(playerPoints.getCoins() + numberOfCoins);

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		if(isCompleteLevelChange)
			playerPoints.setCompleteLevel(completeLevel);
		else
			playerPoints.setCompleteLevel(sharedPreffPlayerInfo.getInt("completeLevel", 0));

		playerPoints.setUserId(userId);
		playerPoints.setPlayerId(playerId);

		learningCenterimpl.deleteFromPlayerTotalPoints(playerId);
		learningCenterimpl.insertIntoPlayerTotalPoints(playerPoints);

		learningCenterimpl.closeConn();

		SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
		if(sheredPreference.getBoolean(IS_LOGIN, false))
		{
			if(CommonUtils.isInternetConnectionAvailable(SingleFriendzyEquationSolve.this))
			{
				new AddCoinAndPointsForLoginUser(playerPoints).execute(null,null,null);
			}
		}
	}


	/**
	 * This method update table is record already exist pther wise insert the new  record
	 */
	private void insertOrUpdatePlayerEquationTable()
	{
		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		String userId = sharedPreffPlayerInfo.getString("userId", "") ;
		String playerId = sharedPreffPlayerInfo.getString("playerId", "");

		SingleFriendzyImpl singleFriendzy = new SingleFriendzyImpl(this);
		singleFriendzy.openConn();
		if(singleFriendzy.isEquationPlayerExist( userId , playerId , EQUAITON_TYPE))
		{
			singleFriendzy.updateEquaitonPlayerData(userId, playerId, EQUAITON_TYPE, completeLevel, 0);
		}
		else
		{
			PlayerEquationLevelObj playerEquationObj = new PlayerEquationLevelObj();

			playerEquationObj.setUserId(userId);
			playerEquationObj.setPlayerId(playerId);
			playerEquationObj.setLevel(completeLevel);
			playerEquationObj.setStars(0);
			playerEquationObj.setEquationType(EQUAITON_TYPE);

			singleFriendzy.insertIntoEquationPlayerTable(playerEquationObj);
		}

		singleFriendzy.closeConn();
	}


	/**
	 * This method set the Get Ready Image back ground
	 * @param value
	 */
	private void setBacKGroundForGetReadyImage(int value)
	{
		switch(value)
		{
		case 6 :
			getReady.setBackgroundResource(R.drawable.mcg_get_ready);
			break;
		case 5 :
			getReady.setBackgroundResource(R.drawable.mcg_5);
			break;
		case 4 :
			getReady.setBackgroundResource(R.drawable.mcg_4);
			break;
		case 3 :
			getReady.setBackgroundResource(R.drawable.mcg_3);
			break;
		case 2 :
			getReady.setBackgroundResource(R.drawable.mcg_2);
			break;
		case 1 :
			getReady.setBackgroundResource(R.drawable.mcg_1);
			break;
		case 0 :
			getReady.setBackgroundResource(R.drawable.mcg_go);
			break;
		}
	}

	@SuppressWarnings("deprecation")
	private void setPlayerDetail()
	{
		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "inside setPlayerDetail()");

		challengerDataObj = ChooseChallengerAdapter.challengerDataObj;

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);

		//completeLevel = sharedPreffPlayerInfo.getInt("completeLevel", 0);//get played complete level

		String fullName = sharedPreffPlayerInfo.getString("playerName", "");
		String playerName = fullName.substring(0,(fullName.indexOf(" ") + 2));
		//txtUserName1.setText(sharedPreffPlayerInfo.getString("playerName", ""));//change for last name first character
		txtUserName1.setText(playerName + ".");

		Country country = new Country();
		String coutryIso = country.getCountryIsoByCountryName(sharedPreffPlayerInfo.getString("countryName", ""), this);

		try
		{
			if(!(coutryIso.equals("-")))
				imgFlag1.setBackgroundDrawable(Drawable.createFromStream(getAssets().open(getResources().
						getString(R.string.countryImageFolder) +"/"
						+ coutryIso + ".png"), null));
		}
		catch (IOException e)
		{
			Log.e(TAG, "Inside set player detail Error No Image Found");

			//e.printStackTrace();
		}

		//set second user detail
		coutryIso = challengerDataObj.getCountryIso();
		txtUserName2.setText(challengerDataObj.getFirst() + " "
				+ challengerDataObj.getLast().charAt(0) + ".");
		try
		{
			if(!(coutryIso.equals("-")))
				imgFlag2.setBackgroundDrawable(Drawable.createFromStream(getAssets().open(getResources().
						getString(R.string.countryImageFolder) +"/"
						+ challengerDataObj.getCountryIso() + ".png"), null));
		}
		catch (IOException e)
		{
			Country country1 = new Country();
			try
			{
				imgFlag2.setBackgroundDrawable(Drawable.createFromStream(getAssets().open(getResources().
						getString(R.string.countryImageFolder) +"/"
						+ country1.getCountryIsoByCountryId(challengerDataObj.getCountryIso(), this) + ".png"), null));
			}
			catch (NotFoundException e1)
			{
				Log.e(TAG, "Inside set player detail Error No Image Found");
				//e1.printStackTrace();
			}
			catch (IOException e1)
			{
				Log.e(TAG, "Inside set player detail Error No Image Found");
				//e1.printStackTrace();
			}

			//e.printStackTrace();
		}

		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "outside setPlayerDetail()");
	}


	/**
	 * This method set the listener on widgets
	 */
	private void setlistenerOnWidgets()
	{
		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "inside setlistenerOnWidgets()");

		if(txtCarry != null)
			txtCarry.setOnClickListener(this);

		btnDot.setOnClickListener(this);
		btnMinus.setOnClickListener(this);
		btn1.setOnClickListener(this);
		btn2.setOnClickListener(this);
		btn3.setOnClickListener(this);
		btn4.setOnClickListener(this);
		btn5.setOnClickListener(this);
		btn6.setOnClickListener(this);
		btn7.setOnClickListener(this);
		btn8.setOnClickListener(this);
		btn9.setOnClickListener(this);
		btn0.setOnClickListener(this);
		btnArrowBack.setOnClickListener(this);

		btnGo.setOnClickListener(this);
		btnRoughWork.setOnClickListener(this);

		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "outside setlistenerOnWidgets()");

	}


	/**
	 * This method getEquation from server or database
	 */
	private void getEquations()
	{
		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "inside getEquations()");

		if(challengerDataObj.getIsFakePlayer() != 1 && (challengerDataObj.getSum().length() > 0
				&& (!challengerDataObj.getSum().equals("0"))) && CommonUtils.isInternetConnectionAvailable(this))
		{
			isEquationFromServer = true;

			//Log.e(TAG, "Equations from server");

			new FindCompleteEquationsForPlayer(challengerDataObj.getUserId() , challengerDataObj.getPlayerId())
			.execute(null,null,null);
		}
		else
		{
			/*isEquationFromServer = true ; // for testing 
			new FindCompleteEquationsForPlayer("111" , "220").execute(null,null,null);*/

			this.getEquationsFromLocalDatabase();

		}

		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "outside getEquations()");
	}


	/**
	 * This method get equations from local database
	 */
	private void getEquationsFromLocalDatabase()
	{
		//Log.e(TAG, "Equation from local database");

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);

		/*SingleFriendzyImpl singleFriendzyImpl = new SingleFriendzyImpl(this);
		singleFriendzyImpl.openConn();
		ArrayList<Integer> mathOperationCategoryIdList = singleFriendzyImpl.
														 getMathOperationCategoryIdByGradeLevelId
														 (this.setGradeForPlayerSingleFriendzyGrade
														 (sharedPreffPlayerInfo.getInt("grade", 0)));
		singleFriendzyImpl.closeConn();*/

		//changes for friendzy challenge
		ArrayList<Integer> mathOperationCategoryIdList = new ArrayList<Integer>();
		SingleFriendzyImpl singleFriendzyImpl = new SingleFriendzyImpl(this);
		singleFriendzyImpl.openConn();

		if(CommonUtils.isActivePlayer){
			String equationString = MainActivity.equationsObj.getEquationIds();
			MultiFriendzyImpl multifrinedzyImpl = new MultiFriendzyImpl(this);
			multifrinedzyImpl.openConn();
			mathOperationCategoryIdList = multifrinedzyImpl.getEquationCategoryId
					(equationString.substring(equationString.indexOf('&') + 1 , equationString.length()),
							equationString.substring(0 , equationString.indexOf('&')));

		}else{

			mathOperationCategoryIdList = singleFriendzyImpl.
					getMathOperationCategoryIdByGradeLevelId
					(this.setGradeForPlayerSingleFriendzyGrade
							(sharedPreffPlayerInfo.getInt("grade", 0)));
		}
		singleFriendzyImpl.closeConn();
		//end changes

		LearningCenterimpl learnignCenterImpl = new LearningCenterimpl(this);
		learnignCenterImpl.openConn();
		mathOperationList = learnignCenterImpl.getMathEquationDataByCategories(mathOperationCategoryIdList);

		this.getCategoriyIdListByDigits();

		equationsIdList = new ArrayList<Integer>();

		for( int i = 0 ; i < newMathOperationList.size() ; i ++ )
		{
			equationsIdList.add(newMathOperationList.get(i).getEquationId());
		}

		learnignCenterImpl.closeConn();

		this.craeteChallengerDataListFromDataBase();
	}

	/**
	 * This method create challenger data list from database
	 */
	private void craeteChallengerDataListFromDataBase()
	{
		int lapCounter = 3;
		int timeTaken = 0 ;
		int lap = 1;

		equationList = new ArrayList<EquationFromServerTransferObj>();

		//Log.e(TAG, "inside craeteChallengerDataListFromDataBase size of new Math List " + newMathOperationList.size());

		for( int i = 0 ; i < newMathOperationList.size() ; i ++)
		{
			EquationFromServerTransferObj equationObj = new EquationFromServerTransferObj();

			timeTaken = this.calculateTimeForSecondPlayer(newMathOperationList.get(i)
					.getCategoryId() , newMathOperationList.get(i).getOperationId());

			equationObj.setTimeTakenToAnswer(timeTaken);

			equationObj.setPoints(this.setPoints(newMathOperationList.get(i).getOperationId(),
					newMathOperationList.get(i).getCategoryId(), timeTaken));
			if(i < 3)
			{
				equationObj.setIsAnswerCorrect(1);
				equationObj.setLap(lap);
			}
			else
			{
				int isAnsCorrect = this.getIsAnswerCorrectValue() ;

				equationObj.setIsAnswerCorrect(isAnsCorrect);

				/*Log.e(TAG, "inside craeteChallengerDataListFromDataBase isAnswerCorrect " + isAnsCorrect
							+ " time taken " + timeTaken );
				 */
				if(isAnsCorrect == 1)
				{
					lapCounter = (lapCounter + 1 ) % 4;

					if(lapCounter == 1)
						lap ++ ;
				}

				equationObj.setLap(lap);
			}

			//Log.e(TAG, "inside craeteChallengerDataListFromDataBase lap " + lap);
			equationList.add(equationObj);
		}
	}


	/**
	 * This method return the value of isAnser correct
	 */
	private int getIsAnswerCorrectValue()
	{
		int probebilityOfCorerctAnswer = 30 + 3 * (completeLevel - 1);

		Random random = new Random();
		int value = random.nextInt(100);
		value ++ ;

		if(value > probebilityOfCorerctAnswer)
			return 0 ;
		else
			return 1;
	}


	/**
	 * This method calculate the time taken by other user when equations from database
	 * @return
	 */
	private int calculateTimeForSecondPlayer(int categoryId , int operationId)
	{
		int calculatedTime = 0;//initially 0

		if(operationId == ADDITION)
		{
			if(onebyoneDigitList.contains(categoryId))
			{
				calculatedTime = this.calculateTime(2 , 4);
			}
			else if(twobyoneDigitList.contains(categoryId))
			{
				calculatedTime = this.calculateTime(5 , 9);
			}
			else if(twobytwoDigitList.contains(categoryId))
			{
				calculatedTime = this.calculateTime(8 , 12);
			}
			else if(threebythreeDigitList.contains(categoryId))
			{
				calculatedTime = this.calculateTime(11 , 15);
			}
		}
		else if(operationId == SUBTRACTION)
		{
			if(onebyoneDigitList.contains(categoryId))
			{
				calculatedTime = this.calculateTime(2 , 4);
			}
			else if(twobyoneDigitList.contains(categoryId))
			{
				calculatedTime = this.calculateTime(8 , 12);
			}
			else if(twobytwoDigitList.contains(categoryId))
			{
				calculatedTime = this.calculateTime(11 , 15);
			}
			else if(threebythreeDigitList.contains(categoryId))
			{
				calculatedTime = this.calculateTime(14 , 18);
			}
		}
		else if(operationId == MULTIPLICATION)
		{
			if(onebyoneDigitList.contains(categoryId))
			{
				calculatedTime = this.calculateTime(2 , 4);
			}
			else if(twobyoneDigitList.contains(categoryId))
			{
				calculatedTime = this.calculateTime(13 , 17);
			}
			else if(twobytwoDigitList.contains(categoryId))
			{
				calculatedTime = this.calculateTime(25 , 31);
			}
			else if(threebytwoDigitList.contains(categoryId))
			{
				calculatedTime = this.calculateTime(30 , 40);
			}
		}
		else if(operationId == DIVISION)
		{
			if(onebyoneDigitList.contains(categoryId))
			{
				calculatedTime = this.calculateTime(3 , 7);
			}
			else if(twobyoneDigitList.contains(categoryId))
			{
				calculatedTime = this.calculateTime(15 , 19);
			}
			else if(twobytwoDigitList.contains(categoryId))
			{
				calculatedTime =this.calculateTime(25 , 31);
			}
			else if(threebytwoDigitList.contains(categoryId))
			{
				calculatedTime = this.calculateTime(40 , 50);
			}

		}
		else if(operationId == FRACTION_ADDITION_SUBTRACTION)
		{
			if(onebyoneDigitList.contains(categoryId))
			{
				calculatedTime = this.calculateTime(10 , 15);
			}
			else if(twobyoneDigitList.contains(categoryId))
			{
				calculatedTime = this.calculateTime(20 , 30);
			}
			else if(twobytwoDigitList.contains(categoryId))
			{
				calculatedTime = this.calculateTime(30 , 40);
			}
		}
		else if(operationId == FRACTION_MULTIPLICATION_DIVISION)
		{
			if(onebyoneDigitList.contains(categoryId))
			{
				calculatedTime =  this.calculateTime(14 , 20);
			}
			else if(twobyoneDigitList.contains(categoryId))
			{
				calculatedTime =  this.calculateTime(25 , 33);
			}
			else if(twobytwoDigitList.contains(categoryId))
			{
				calculatedTime =  this.calculateTime(40 , 50);
			}
		}
		else if(operationId == DECIMAL_ADDITION_SUBTRACTION || (operationId == DECIMAL_MULTIPLICATION_DIVISION))
		{
			if(onebyoneDigitList.contains(categoryId))
			{
				calculatedTime =  this.calculateTime(6 , 10);
			}
			else if(twobyoneDigitList.contains(categoryId))
			{
				calculatedTime =  this.calculateTime(10 , 17);
			}
			else if(twobytwoDigitList.contains(categoryId))
			{
				calculatedTime =  this.calculateTime(14 , 20);
			}
			else if(threebythreeDigitList.contains(categoryId))
			{
				calculatedTime =  this.calculateTime(17 , 23);
			}
		}
		else if(operationId == NEGATIVE_ADDITION_SUBTRACTION)
		{
			if(onebyoneDigitList.contains(categoryId))
			{
				calculatedTime = this.calculateTime(4 , 9);
			}
			else if(twobyoneDigitList.contains(categoryId))
			{
				calculatedTime = this.calculateTime(7 , 12);
			}
			else if(twobytwoDigitList.contains(categoryId))
			{
				calculatedTime = this.calculateTime(10 , 16);
			}
			else if(threebythreeDigitList.contains(categoryId))
			{
				calculatedTime = this.calculateTime(14 , 20);
			}
		}
		else if(operationId == NEGATIVE_MULTIPLICATION_DIVISION)
		{
			if(onebyoneDigitList.contains(categoryId))
			{
				calculatedTime = this.calculateTime(4 , 9);
			}
			else if(twobyoneDigitList.contains(categoryId))
			{
				calculatedTime = this.calculateTime(14 , 20);
			}
			else if(twobytwoDigitList.contains(categoryId))
			{
				calculatedTime = this.calculateTime(25 , 33);
			}
			else if(threebytwoDigitList.contains(categoryId))
			{
				calculatedTime = this.calculateTime(45 , 55);
			}
		}

		return calculatedTime ;
	}

	/**
	 * This method calculate time for the second player
	 * @param min
	 * @param max
	 * @return
	 */
	private int calculateTime(int min , int max)
	{
		Random random = new Random();

		int timeTaken = random.nextInt(max - min + 1);

		return timeTaken + min;
	}

	/**
	 * Return grade for get categories
	 * @param grade
	 * @return
	 */
	int setGradeForPlayerSingleFriendzyGrade(int grade)
	{
		int gradeIncreaseAfterLevels = 1;
		if(grade < 5)                           //For players with grade 1 to 4. Difference of increase the grade will be after every 6 levels.
			gradeIncreaseAfterLevels = 6;
		else if(grade > 4)
			gradeIncreaseAfterLevels = 3;

		//Return the grade according to the grade and levels.
		if(completeLevel > gradeIncreaseAfterLevels * (grade - 1))          //If the level has reached to the limit after which it grade can't increased then return the grade as it is.
			return grade;
		else if(completeLevel % gradeIncreaseAfterLevels == 0)          //If current level is 1st,7th, 13th etc(when grade increase after 6 levels) and 1st, 4th, 7th etc (when grade increase after every 3 levels) then increase the grade.
			return (completeLevel / gradeIncreaseAfterLevels);
		else
			return (completeLevel / gradeIncreaseAfterLevels + 1);
	}


	/**
	 * This method get Category Id List From Database and set to the arrayList
	 * like 1*1 digit,2*1 and etc
	 */
	private void getCategoriyIdListByDigits()
	{

		//Log.e(TAG, "inside getCategoriyIdListByDigits ");

		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();
		onebyoneDigitList 	= learningCenterObj.getDigitType("1 digit%1 digit");
		twobyoneDigitList 	= learningCenterObj.getDigitType("2 digits%1 digit");
		twobytwoDigitList 	= learningCenterObj.getDigitType("2 digits%2 digits");
		threebytwoDigitList = learningCenterObj.getDigitType("3 digits%2 digits");
		threebythreeDigitList= learningCenterObj.getDigitType("3 digits%3 digits");
		onebytwoDigitList   = learningCenterObj.getDigitType("1 digit%2 digits");

		for( int i = 0 ; i < onebytwoDigitList.size() ; i ++ )
		{
			twobyoneDigitList.add(onebytwoDigitList.get(i));
		}

		learningCenterObj.closeConn();

		this.setSequaence();
	}


	/**
	 * This method set the sequence for displaying equations
	 */
	private void setSequaence()
	{
		//Log.e(TAG, "inside setSequaence " + mathOperationList.size());

		ArrayList<MathEquationOperationCategorytransferObj> oneDByOneDlist = new ArrayList<MathEquationOperationCategorytransferObj>();
		ArrayList<MathEquationOperationCategorytransferObj> twoDByOneDlist = new ArrayList<MathEquationOperationCategorytransferObj>();
		ArrayList<MathEquationOperationCategorytransferObj> twoDByTwoDlist = new ArrayList<MathEquationOperationCategorytransferObj>();
		ArrayList<MathEquationOperationCategorytransferObj> threeDByTwoDlist = new ArrayList<MathEquationOperationCategorytransferObj>();
		ArrayList<MathEquationOperationCategorytransferObj> threeDByThreeDlist = new ArrayList<MathEquationOperationCategorytransferObj>();

		newMathOperationList = new ArrayList<MathEquationOperationCategorytransferObj>();
		randomGenerator = new Random();
		int randomNumber = 0;

		int maxEquation = mathOperationList.size() > 100 ? 100:mathOperationList.size();

		for( int i = 0 ; i < maxEquation ; i++)
		{
			randomNumber = randomGenerator.nextInt(mathOperationList.size());

			if(onebyoneDigitList.contains(mathOperationList.get(randomNumber).getCategoryId()))
			{
				oneDByOneDlist.add(mathOperationList.get(randomNumber));
			}
			else if(twobyoneDigitList.contains(mathOperationList.get(randomNumber).getCategoryId()))
			{
				twoDByOneDlist.add(mathOperationList.get(randomNumber));
			}
			else if(twobytwoDigitList.contains(mathOperationList.get(randomNumber).getCategoryId()))
			{
				twoDByTwoDlist.add(mathOperationList.get(randomNumber));
			}
			else if(threebytwoDigitList.contains(mathOperationList.get(randomNumber).getCategoryId()))
			{
				threeDByTwoDlist.add(mathOperationList.get(randomNumber));
			}
			else if(threebythreeDigitList.contains(mathOperationList.get(randomNumber).getCategoryId()))
			{
				threeDByThreeDlist.add(mathOperationList.get(randomNumber));
			}

			mathOperationList.remove(randomNumber);
		}

		for(int i = 0 ; i < 3 ; i++)
		{
			if(oneDByOneDlist.size() > i)
			{
				randomNumber = randomGenerator.nextInt(oneDByOneDlist.size());
				newMathOperationList.add(oneDByOneDlist.get(randomNumber));
				oneDByOneDlist.remove(randomNumber);
			}
		}

		for(int i = 0 ; i < 3 ; i++)
		{
			if(twoDByOneDlist.size() > i)
			{
				randomNumber = randomGenerator.nextInt(twoDByOneDlist.size());
				newMathOperationList.add(twoDByOneDlist.get(randomNumber));
				twoDByOneDlist.remove(randomNumber);
			}
		}

		for(int i = 0 ; i < 2 ; i++)
		{
			if(twoDByTwoDlist.size() > i)
			{
				randomNumber = randomGenerator.nextInt(twoDByTwoDlist.size());
				newMathOperationList.add(twoDByTwoDlist.get(randomNumber));
				twoDByTwoDlist.remove(randomNumber);
			}
		}

		for(int i = 0 ; i < 1 ; i++)
		{
			if(threeDByTwoDlist.size() > i)
			{
				randomNumber = randomGenerator.nextInt(threeDByTwoDlist.size());
				newMathOperationList.add(threeDByTwoDlist.get(randomNumber));
				threeDByTwoDlist.remove(randomNumber);
			}
		}

		for(int i = 0 ; i < 1 ; i++)
		{
			if(threeDByThreeDlist.size() > i)
			{
				randomNumber = randomGenerator.nextInt(threeDByThreeDlist.size());
				newMathOperationList.add(threeDByThreeDlist.get(randomNumber));
				threeDByThreeDlist.remove(randomNumber);
			}
		}

		int newListSize = newMathOperationList.size();
		for(int i = 0 ; i < maxEquation - newListSize ; i ++ )
		{
			if(oneDByOneDlist.size() > 0)
			{
				randomNumber = randomGenerator.nextInt(oneDByOneDlist.size());
				newMathOperationList.add(oneDByOneDlist.get(randomNumber));
				oneDByOneDlist.remove(randomNumber);
			}

			if(twoDByOneDlist.size() > 0)
			{
				randomNumber = randomGenerator.nextInt(twoDByOneDlist.size());
				newMathOperationList.add(twoDByOneDlist.get(randomNumber));
				twoDByOneDlist.remove(randomNumber);
			}

			if(twoDByTwoDlist.size() > 0)
			{
				randomNumber = randomGenerator.nextInt(twoDByTwoDlist.size());
				newMathOperationList.add(twoDByTwoDlist.get(randomNumber));
				twoDByTwoDlist.remove(randomNumber);
			}

			if(threeDByTwoDlist.size() > 0)
			{
				randomNumber = randomGenerator.nextInt(threeDByTwoDlist.size());
				newMathOperationList.add(threeDByTwoDlist.get(randomNumber));
				threeDByTwoDlist.remove(randomNumber);
			}

			if(threeDByThreeDlist.size() > 0)
			{
				randomNumber = randomGenerator.nextInt(threeDByThreeDlist.size());
				newMathOperationList.add(threeDByThreeDlist.get(randomNumber));
				threeDByThreeDlist.remove(randomNumber);
			}
		}
	}

	@Override
	public void onBackPressed()
	{
		if(timer != null)
			timer.cancel();

		if(getReadyTimer != null)
			getReadyTimer.cancel();

		if(playsound != null)
			playsound.stopPlayer();

		isStopSecondPlayerProgress = true;//for stop the challenger player progresss

		isClickOnBackPressed = false;

		this.clickOnBackPressed();

		super.onBackPressed();
	}


	/**
	 * This method call when user click on back button
	 */
	private void clickOnBackPressed()
	{
		//changs for correct time on result screen , on 07 Mar 2014
		if(!isClickOnBackPressed)
			totalTime = this.getNewMathPlayedData((TOTAL_TIME_TO_PLAY - (int)(startTimerForRoughArea/1000)) , playDatalist);
		//end changes

		numberOfCoins = (int) (points * ICommonUtils.COINS_PER_POINT);

		SharedPreferences sheredPreference = this.getSharedPreferences(LOGIN_SHARED_PREFF, 0);
		if(!sheredPreference.getBoolean(IS_LOGIN, false))
		{
			if(isClickOnGo)
			{
				this.inserPlayDataIntodatabase(savePlayDataToDataBase());
			}
			startActivity(new Intent(this,SingleFriendzyMain.class));
		}
		else
		{
			this.savePlayerDataWhenNoInternetConnected();

			startActivity(new Intent(SingleFriendzyEquationSolve.this,SingleFriendzyMain.class));

			if(CommonUtils.isInternetConnectionAvailable(this))
			{
				if(!isClickOnBackPressed)
				{
					this.insertIntoPlayerTotalPoints(savePlayDataToDataBase());
					new SaveTempPlayerScoreOnServer(savePlayDataToDataBase()).execute(null,null,null);
					new AddCoinAndPointsForLoginUser(getPlayerPoints(savePlayDataToDataBase())).execute(null,null,null);

					//added by shilpi for friendzy
					if(CommonUtils.isActivePlayer){
						/*new SaveTimePointsForFriendzyChallenge(totalTimeTaken, points, this)
						.execute(null, null, null);*/
						new SaveTimePointsForFriendzyChallenge(totalTime, points, this)
						.execute(null, null, null);
					}
				}
				else
				{
					this.insertIntoPlayerTotalPoints(savePlayDataToDataBase());
				}
			}
			else
			{
				this.inserPlayDataIntodatabase(savePlayDataToDataBase());
			}
		}
	}

	/**
	 * This method insert the data into total player points table
	 * @param mathResultObj
	 */
	private void insertIntoPlayerTotalPoints(MathResultTransferObj mathResultObj)
	{
		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();
		PlayerTotalPointsObj playerPoints = this.getPlayerPoints(mathResultObj);

		if(learningCenterObj.isPlayerTotalPointsExist(playerPoints.getPlayerId()))
		{
			int points = playerPoints.getTotalPoints();
			int coins  = playerPoints.getCoins();
			playerPoints.setTotalPoints(learningCenterObj.getDataFromPlayerTotalPoints(playerPoints.getPlayerId()).getTotalPoints()
					+ points);

			playerPoints.setCoins(learningCenterObj.getDataFromPlayerTotalPoints(playerPoints.getPlayerId()).getCoins()
					+ coins);

			learningCenterObj.deleteFromPlayerTotalPoints(playerPoints.getPlayerId());
		}

		learningCenterObj.insertIntoPlayerTotalPoints(playerPoints);

		learningCenterObj.closeConn();
	}

	/**
	 * This method return the player points with its user id and player id and also coins
	 * @return
	 */
	private PlayerTotalPointsObj getPlayerPoints(MathResultTransferObj mathResultObj)
	{
		PlayerTotalPointsObj playerPoints = new PlayerTotalPointsObj();
		playerPoints.setUserId(mathResultObj.getUserId());
		playerPoints.setPlayerId(mathResultObj.getPlayerId());

		playerPoints.setTotalPoints(points);

		//numberOfCoins = (int) (points * 0.05);

		playerPoints.setCoins(numberOfCoins);

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		playerPoints.setCompleteLevel(sharedPreffPlayerInfo.getInt("completeLevel", 0));//set complete level

		playerPoints.setPurchaseCoins(0);

		return playerPoints;
	}

	/**
	 * This method add the start and end time to solve equation and all the information related to the equation
	 * to the arrayList
	 */
	private void savePlayData()
	{
		endTime = new Date();

		MathScoreTransferObj mathObj = new MathScoreTransferObj();
		mathObj.setMathOperationID(operationId);
		mathObj.setEquationId(equationId);
		mathObj.setLap(lap);
		mathObj.setStartDateTimeStr(CommonUtils.formateDate(startTime));
		mathObj.setEndDateTimeStr(CommonUtils.formateDate(endTime));
		mathObj.setPoints(pointsForEachQuestion);

		if(userAnswer.length() > 0)
			mathObj.setAnswer(userAnswer);
		else
			mathObj.setAnswer("1");

		mathObj.setAnswerCorrect(isCorrectAnsByUser);


		startTimeValue = startTime.getSeconds() ;
		endTimeVlaue   = endTime.getSeconds();

		if(endTimeVlaue < startTimeValue)
			endTimeVlaue = endTimeVlaue + 60;

		/*Log.e(TAG, "time taken end time ,starttime, diff " +  endTimeVlaue + "," 
				+ startTimeValue + "," +(endTimeVlaue - startTimeValue));*/

		mathObj.setTimeTakenToAnswer(endTimeVlaue - startTimeValue);

		//mathObj.setTimeTakenToAnswer(endTime.getSeconds() - startTime.getSeconds());
		mathObj.setEquation(learnignCenterobj);


		pointsForEachQuestion = 0;
		playDatalist.add(mathObj);

		//userAnswerList.add(userAnswer);
		//changes for answerScreen
		if(isCorrectAnsByUser == 1)
			userAnswerList.add(learnignCenterobj.getProductStr());
		else
			userAnswerList.add(userAnswer);
		//end changes

		//added by shilpi for friendzy
		totalTimeTaken = totalTimeTaken + (endTimeVlaue - startTimeValue);
	}

	/**
	 * This method save play data to the database(Math_Result table)
	 */
	private MathResultTransferObj savePlayDataToDataBase()
	{
		MathResultTransferObj mathResultObj = new MathResultTransferObj();
		mathResultObj.setRoundId(0);
		mathResultObj.setIsFirstRound("0");
		mathResultObj.setGameType(gameType);
		mathResultObj.setIsWin(isWin);

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		if(sharedPreffPlayerInfo.getString("userId", "").equals("0"))
			mathResultObj.setUserId("0");
		else
			mathResultObj.setUserId(sharedPreffPlayerInfo.getString("userId", ""));

		if(sharedPreffPlayerInfo.getString("playerId", "").equals("0"))
			mathResultObj.setPlayerId("0");
		else
			mathResultObj.setPlayerId(sharedPreffPlayerInfo.getString("playerId", ""));

		mathResultObj.setChallengerId(challengerDataObj.getPlayerId());

		mathResultObj.setIsFakePlayer(0);
		mathResultObj.setProblems(this.getEquationSolveXml(playDatalist));

		mathResultObj.setTotalScore(points);

		mathResultObj.setCoins(numberOfCoins);

		return mathResultObj;
	}


	/**
	 * This method store data into database for Math_result
	 * @param mathResultObj
	 */
	private void inserPlayDataIntodatabase(MathResultTransferObj mathResultObj )
	{
		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();
		learningCenterObj.insertIntoMathResult(mathResultObj);
		learningCenterObj.closeConn();
		this.insertIntoPlayerTotalPoints(mathResultObj);
	}

	/**
	 * This method convert the data for solving the equation into xml format
	 */
	private String getEquationSolveXml(ArrayList<MathScoreTransferObj> playDatalist)
	{
		StringBuilder xml = new StringBuilder("");

		for( int i = 0 ;  i < playDatalist.size() ; i++)
		{
			xml.append("<equation>" +
					"<equationId>"+playDatalist.get(i).getEquationId()+"</equationId>"+
					"<start_date_time>"+playDatalist.get(i).getStartDateTimeStr()+"</start_date_time>"+
					"<end_date_time>"+playDatalist.get(i).getEndDateTimeStr()+"</end_date_time>"+
					"<lap>"+playDatalist.get(i).getLap()+"</lap>"+
					"<time_taken_to_answer>"+playDatalist.get(i).getTimeTakenToAnswer()+"</time_taken_to_answer>"+
					"<math_operation_id>"+playDatalist.get(i).getMathOperationID()+"</math_operation_id>"+
					"<points>"+playDatalist.get(i).getPoints()+"</points>"+
					"<isAnswerCorrect>" + playDatalist.get(i).getAnswerCorrect() + "</isAnswerCorrect>"+
					"<user_answer>"+playDatalist.get(i).getAnswer()+"</user_answer>"+
					"</equation>");
		}

		return xml.toString();
	}

	/**
	 * This methdod make xml for unsove eqaurions
	 * @return
	 */
	private String getEquationUnSolveXml()
	{
		StringBuilder xml = new StringBuilder("");

		for( int i = playDatalist.size() ;  i < equationList.size() - playDatalist.size() ; i++)
		{
			int pointsForUnsolveEquation = (int) (equationList.get(i).getTimeTakenToAnswer() + equationList.get(i).getPoints());

			xml.append("<equation>" +
					"<equationId>"+equationList.get(i).getMathEquationId()+"</equationId>"+
					"<start_date_time>"+CommonUtils.formateDate(endTime)+"</start_date_time>"+
					"<end_date_time>"+CommonUtils.formateDate(endTime)+"</end_date_time>"+
					"<lap>"+lap1+"</lap>"+
					"<time_taken_to_answer>"+equationList.get(i).getTimeTakenToAnswer()+"</time_taken_to_answer>"+
					"<math_operation_id>"+ 0 +"</math_operation_id>"+
					"<points>"+pointsForUnsolveEquation+"</points>"+
					"<isAnswerCorrect>" + 0 + "</isAnswerCorrect>"+
					"<user_answer>"+ 1 +"</user_answer>"+
					"</equation>");
		}

		return xml.toString();
	}


	@Override
	protected void getIntentValue()
	{
	}

	@Override
	protected void showEqautionData(LearningCenterTransferObj learningObj)
	{
		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "inside showEqautionData()");

		/*Log.e(TAG, "equationId" + learningObj.getEquationsId() + " number1 " + learningObj.getNumber1Str()
		+ " number2 " + learningObj.getNumber2Str() + " operator " + learningObj.getOperator() 
		+ " " + learningObj.getProductStr());*/


		SingleFriendzyImpl singleFriendzy = new SingleFriendzyImpl(this);
		singleFriendzy.openConn();
		SingleFriendzyEquationObj singleFriendzyEquation
		= singleFriendzy.getEquationData(learningObj.getEquationsId());

		singleFriendzy.closeConn();

		playEquationList.add(learningObj);

		equationId = learningObj.getEquationsId();

		startTime = new Date();

		operationId =  singleFriendzyEquation.getMathOperationId();

		//send equation for workArea

		SharedPreferences sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putInt("operationId", operationId);
		editor.commit();

		WorkAreaActivity.learningObj = learningObj;

		//set default progress at the starting
		layoutProgress1.setProgress(progress);

		//equationId = learningObj.getEquationsId();
		learnignCenterobj = learningObj;

		number1 = learningObj.getNumber1Str();
		number2 = learningObj.getNumber2Str();
		result  = learningObj.getProductStr();

		resultFromDatabase = learningObj.getProductStr();
		resultDigitsList   = this.getDigit(learningObj.getProductStr());
		operator           = learningObj.getOperator();

		noOfDigitsInFisrtNumber  = this.getDigit(learningObj.getNumber1Str()).size();
		noOfDigitsInSecondNumber = this.getDigit(learningObj.getNumber2Str()).size();
		noOfDigitsInResult       = this.getDigit(learningObj.getProductStr()).size();

		/**
		 * Changing the layout
		 */

		if(operationId == ADDITION || operationId == SUBTRACTION
				|| operationId == DECIMAL_ADDITION_SUBTRACTION || operationId == NEGATIVE_ADDITION_SUBTRACTION)//1,2,7,9 are the constants which show the operation id's
		{
			this.operationPerformForAdditionSubtraction(learningObj);
		}
		else if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION)// 5,6 for operation on fraction number
		{
			this.operatioPerformForFraction();
		}
		else if(operationId == MULTIPLICATION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
		{
			numberOfBoxesForMultiplication = noOfDigitsInResult;
			this.operationPerformForMultiplication();
		}
		else if(operationId == DECIMAL_MULTIPLICATION_DIVISION)
		{
			if(operator.equals("x"))
			{
				this.setnumberOfBoxesForMultiplicationForDecimal();
				this.operationPerformForMultiplication();
			}
			else if(operator.equals("/"))
			{
				this.setNumberOfRowsAndboxForDivisionForDecimal();
				this.operationPerfomrmForDivision();
			}

		}
		else if(operationId ==  DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/")))
		{
			if(noOfDigitsInFisrtNumber == 1 && noOfDigitsInSecondNumber == 1)
				numberOfBoxesForDivision = 1;
			else if((noOfDigitsInFisrtNumber == 2 && noOfDigitsInSecondNumber == 1) || (noOfDigitsInFisrtNumber == 2 && noOfDigitsInSecondNumber == 2))
				numberOfBoxesForDivision = 2;
			else if((noOfDigitsInFisrtNumber == 3 && noOfDigitsInSecondNumber == 2))
				numberOfBoxesForDivision = 3;

			numberOfRowsforDivision = 2 * noOfDigitsInResult;
			this.setNumberOfRowsAndboxForDivisionForBoth();
			this.operationPerfomrmForDivision();
		}

		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "outside showEqautionData()");

	}

	@Override
	protected void setSign()
	{
		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "inside setSign()");

		if(isTab){
			if(operator.equals("+"))
				imgSign.setImageResource(R.drawable.ml_plus_tab);
			else if(operator.equals("-"))
				imgSign.setImageResource(R.drawable.ml_minus_tab);
			else if(operator.equals("x"))
				imgSign.setImageResource(R.drawable.ml_multiplication_tab);
			else if(operator.equals("/"))
				imgSign.setImageResource(R.drawable.ml_div_tab);
		}
		else{
			if(operator.equals("+"))
				imgSign.setImageResource(R.drawable.ml_plus_sign);
			else if(operator.equals("-"))
				imgSign.setImageResource(R.drawable.ml_minus_sign);
			else if(operator.equals("x"))
				imgSign.setImageResource(R.drawable.ml_mul_sign);
			else if(operator.equals("/"))
				imgSign.setImageResource(R.drawable.ml_div_sign);
		}
		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "outside setSign()");
	}

	@Override
	protected void createResultLayout(ArrayList<String> listOfDigits, LinearLayout layout)
	{
		/**
		 * Set the width of linear layout
		 */
		int width = ((noOfDigitsInFisrtNumber + 1) * maxWidth) + 10 ;
		RelativeLayout.LayoutParams lp1 = new RelativeLayout.LayoutParams(width,LayoutParams.WRAP_CONTENT);
		lp1.addRule(RelativeLayout.BELOW,R.id.imgResultLine);
		layout.setLayoutParams(lp1);

		for(i = 0 ; i < listOfDigits.size() ; i ++ )
		{
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);

			lp.setMargins(2, 0, 0, 0);
			TextView txtView = new TextView(this);
			if(i == listOfDigits.size() - 1)
			{
				//changes for tab
				if(isTab)
					txtView.setBackgroundResource(R.drawable.tab_yellow_ipad);
				else
					txtView.setBackgroundResource(R.drawable.yellow);
			}
			else
			{
				//changes for tab
				if(isTab)
					txtView.setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
				else
					txtView.setBackgroundResource(R.drawable.ml_no1box);
			}

			txtView.setGravity(Gravity.CENTER);
			//changes for tab
			if(isTab)
				txtView.setTextSize(45);
			else
				txtView.setTextSize(32);
			txtView.setTypeface(null, Typeface.BOLD);
			txtView.setTextColor(Color.parseColor("#3CB3FF"));
			resultTextViewList.add(txtView);//add text view to arrayList
			layout.addView(txtView,lp);

			txtView.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					for( int  i = 0 ; i < resultTextViewList.size() ; i ++ )
					{
						if(v == resultTextViewList.get(i))
						{
							selectedIndex = i;
							index = i;
							//changes for tab
							if(isTab)
								resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
							else
								resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow);
						}
						else
						{
							//changes for tab
							if(isTab)
								resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
							else
								resultTextViewList.get(i).setBackgroundResource(R.drawable.ml_no1box);
						}
					}
				}
			});
		}
	}

	@Override
	public void onClick(View v)
	{
		switch(v.getId())
		{
		case R.id.btnGo:
			endTime = new Date();
			isClickOnGo = true;
			if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION)//fraction
			{
				this.clickonGoForFraction();
			}
			else if(operationId == MULTIPLICATION || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
					|| (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
			{
				this.clickOnGoForMultiplication();
			}
			else if(operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
					||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))
			{
				this.clickOnGoForDivision();
			}
			else
			{
				this.clickOnGo();
			}
			break;
		case R.id.btn1 :
			if(isCarrySelected)
				this.setCarryText("1");
			else
				this.setTextResultValue("1");
			break;
		case R.id.btn2 :
			if(isCarrySelected)
				this.setCarryText("2");
			else
				this.setTextResultValue("2");
			break;
		case R.id.btn3 :
			if(isCarrySelected)
				this.setCarryText("3");
			else
				this.setTextResultValue("3");
			break;
		case R.id.btn4 :
			if(isCarrySelected)
				this.setCarryText("4");
			else
				this.setTextResultValue("4");
			break;
		case R.id.btn5 :
			if(isCarrySelected)
				this.setCarryText("5");
			else
				this.setTextResultValue("5");
			break;
		case R.id.btn6 :
			if(isCarrySelected)
				this.setCarryText("6");
			else
				this.setTextResultValue("6");
			break;
		case R.id.btn7 :
			if(isCarrySelected)
				this.setCarryText("7");
			else
				this.setTextResultValue("7");
			break;
		case R.id.btn8 :
			if(isCarrySelected)
				this.setCarryText("8");
			else
				this.setTextResultValue("8");
			break;
		case R.id.btn9 :
			if(isCarrySelected)
				this.setCarryText("9");
			else
				this.setTextResultValue("9");
			break;
		case R.id.btn0 :
			if(isCarrySelected)
				this.setCarryText("0");
			else
				this.setTextResultValue("0");
			break;
		case R.id.btnDot :
			this.clickonDot();
			break;
		case R.id.btnMinus :
			this.clickOnMinus();
			break;
		case R.id.btnArrowBack :
			this.clickOnBackArrow();
			break;
		case R.id.txtCarry :
			this.clickOnCarry();
			break;
		case R.id.btnRoughWork :
			isClickOnWorkArea = true;
			Intent intent = new Intent(this,WorkAreaActivity.class);
			intent.putExtra("callingActivity", TAG);
			intent.putExtra("startTime", startTimerForRoughArea);
			startActivityForResult(intent, 1);
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == 1)
		{
			if(resultCode == RESULT_OK)
			{
				START_TIME = data.getLongExtra("timerStartTime", 0);
				//this.countDawnTimerStart();
			}
		}
	}

	/**
	 * This method is called when the click on Go Button and operation perform on fraction number
	 */
	private void clickonGoForFraction()
	{

		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "inside clickonGoForFraction()");

		index = 0;

		StringBuilder resultValue = new StringBuilder("");
		for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
		{
			resultValue = resultValue.append(resultTextViewList.get(i).getText().toString());
		}

		userAnswer = resultValue.toString();

		if(userAnswer.endsWith("."))
			resultValue.deleteCharAt(userAnswer.length() - 1);

		if((result.replace("/", "").replace(" ", "")).equals(resultValue.toString()))
		{
			playsound.playSoundForRightForSingleFriendzy(this);

			this.setProgress();

			rightAnsCounter ++ ;
			isCorrectAnsByUser = 1;
			this.savePlayData();
			/*isYellowboxForFraction = false;
			resultTextViewList.clear();
			linearLayoutQuotient.removeAllViews();
			linearLayoutNumerator.removeAllViews();
			linearLayoutDenominator.removeAllViews();
			this.setIndexForShowEquation();*/


			//change
			this.keyBoardDisable();
            pauseTimer();
			this.showCorrectAns(new OnAnswerGivenComplete() {
				@Override
				public void onComplete() {
					isYellowboxForFraction = false;
					resultTextViewList.clear();
					linearLayoutQuotient.removeAllViews();
					linearLayoutNumerator.removeAllViews();
					linearLayoutDenominator.removeAllViews();
					setIndexForShowEquation();
					keyBoardEnable();
                    startTimer();
				}
			} , resultTextViewList , FRACTION_DESIGN);
		}
		else
		{
			isCorrectAnsByUser = 0;
			playsound.playSoundForWrongForSingleFriendzy(this);
			this.keyBoardDisable();
            pauseTimer();
			this.visibleWrongImageForFraction();
			this.setHandler();
		}

		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "outside clickonGoForFraction()");
	}

	/**
	 * This method call when click on go for multiplication
	 */
	private void clickOnGoForMultiplication()
	{
		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "inside clickOnGoForMultiplication()");


		StringBuilder resultValue = new StringBuilder("");
		if(this.getDigit(number2).size() > 1)
			startIndex = this.getDigit(number2).size() * numberOfBoxesForMultiplication;

		for( int i = startIndex ; i < resultTextViewList.size() ; i ++ )
		{
			resultValue = resultValue.append(resultTextViewList.get(i).getText().toString());
		}

		userAnswer = resultValue.toString();

		if(userAnswer.endsWith("."))
			resultValue.deleteCharAt(userAnswer.length() - 1);

		if(resultValue.toString().equals(resultFromDatabase))
		{
			playsound.playSoundForRightForSingleFriendzy(this);

			this.setProgress();

			rightAnsCounter ++ ;
			isCorrectAnsByUser = 1;
			this.savePlayData();
			/*isYellowboxForFraction = false;
			resultTextViewList.clear();
			linearLayoutMultiple.removeAllViews();
			linearLayoutResult.removeAllViews();
			this.setIndexForShowEquation();*/


			//change
			this.keyBoardDisable();
            pauseTimer();
			this.showCorrectAns(new OnAnswerGivenComplete() {
				@Override
				public void onComplete() {
					isYellowboxForFraction = false;
					resultTextViewList.clear();
					linearLayoutMultiple.removeAllViews();
					linearLayoutResult.removeAllViews();
					setIndexForShowEquation();
					keyBoardEnable();
                    startTimer();
				}
			} , resultTextViewList , MULTIPLICATION_DESIGN);
		}
		else
		{
			/**
			 * check for reverse result
			 */
			if(resultValue.reverse().toString().equals(resultFromDatabase))
			{
				txtCarry.setText("");
				txtCarry.setBackgroundResource(R.drawable.whitebox_small);

				if(getDigit(number2).size() > 1)
					startIndex = getDigit(number2).size() * numberOfBoxesForMultiplication;

				for( int i = startIndex ; i < resultTextViewList.size() ; i ++ )
				{
					resultTextViewList.get(i).setText("");

					if( i == resultTextViewList.size() - 1)
					{
						//changes for tab
						if(isTab)
							resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
						else
							resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow_small);
					}
					else
					{
						//changes for tab
						if(isTab)
							resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
						else
							resultTextViewList.get(i).setBackgroundResource(R.drawable.whitebox_small);
					}
				}
				index = resultTextViewList.size() - 1;

				this.setTimerForReverseResult(MULTIPLICATION_DESIGN);

			}
			else
			{
				playsound.playSoundForWrongForSingleFriendzy(this);

				isCorrectAnsByUser = 0;
				this.visibleWrongImageForMultiplication();
				this.keyBoardDisable();
                pauseTimer();
				this.setHandler();
			}
		}

		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "outside clickOnGoForMultiplication()");

	}


	/**
	 * This method method call when click on go for division
	 */
	private void clickOnGoForDivision()
	{
		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "inside clickOnGoForDivision()");

		StringBuilder resultValue = new StringBuilder("");

		boolean isStartFromZero = false;
		for( int i = 0 ; i < numberOfBoxesForDivision ; i ++ )
		{
			if(!(resultTextViewList.get(i).getText().toString().equals("0") && isStartFromZero == false))
			{
				resultValue = resultValue.append(resultTextViewList.get(i).getText().toString());
				isStartFromZero = true;
			}
		}

		userAnswer = resultValue.toString();

		if(userAnswer.endsWith("."))
			resultValue.deleteCharAt(userAnswer.length() - 1);

		if(resultValue.toString().equals(resultFromDatabase))
		{
			playsound.playSoundForRightForSingleFriendzy(this);

			this.setProgress();

			rightAnsCounter ++ ;
			isCorrectAnsByUser = 1;
			this.savePlayData();
			/*isYellowboxForFraction = false;
			resultTextViewList.clear();
			linearLayoutResult.removeAllViews();
			linearLayoutRowsForDivision.removeAllViews();
			this.setIndexForShowEquation();*/


			//change
			this.keyBoardDisable();
            pauseTimer();
			this.showCorrectAns(new OnAnswerGivenComplete() {
				@Override
				public void onComplete() {
					isYellowboxForFraction = false;
					resultTextViewList.clear();
					linearLayoutResult.removeAllViews();
					linearLayoutRowsForDivision.removeAllViews();
					setIndexForShowEquation();
					keyBoardEnable();
                    startTimer();
				}
			} , resultTextViewList , DEVISION_DESIGN);

		}
		else
		{
			playsound.playSoundForWrongForSingleFriendzy(this);
			isCorrectAnsByUser = 0;
			this.visibleWrongImageForDivision();
			this.keyBoardDisable();
            pauseTimer();
			this.setHandler();
		}

		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "outsideside clickOnGoForDivision()");
	}

	/**
	 * This method is called when the click on Go Button
	 */
	private void clickOnGo()
	{

		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "inside clickOnGo()");

		index         = noOfDigitsInResult - 1;
		selectedIndex = noOfDigitsInResult - 1;

		StringBuilder resultValue = new StringBuilder("");
		for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
		{
			resultValue = resultValue.append(resultTextViewList.get(i).getText().toString());
		}

		userAnswer = resultValue.toString();

		if(userAnswer.endsWith("."))
			resultValue.deleteCharAt(userAnswer.length() - 1);

		if(resultValue.toString().equals(resultFromDatabase))
		{
			playsound.playSoundForRightForSingleFriendzy(this);

			this.setProgress();
			rightAnsCounter ++ ;
			isCorrectAnsByUser = 1;
			this.savePlayData();

			//change 22 jan 2015
			this.keyBoardDisable();
            pauseTimer();
			this.showCorrectAns(new OnAnswerGivenComplete() {

				@Override
				public void onComplete() {
					resultTextViewList.clear();
					linearLayoutResultRow.removeViews(0, noOfDigitsInResult);
					setIndexForShowEquation();
					keyBoardEnable();
                    startTimer();
				}
			} , resultTextViewList);
			//end changes

			/*resultTextViewList.clear();
			linearLayoutResultRow.removeViews(0, noOfDigitsInResult);
			this.setIndexForShowEquation();*/

		}
		else
		{
			playsound.playSoundForWrongForSingleFriendzy(this);

			/**
			 * check for reverse result
			 */

			if((resultValue.reverse().toString()).equals(resultFromDatabase))
			{
				txtCarry.setText("");
				txtCarry.setBackgroundResource(R.drawable.ml_no1box);

				for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
				{
					resultTextViewList.get(i).setText("");

					if(i == index)
					{
						//changes for tab
						if(isTab)
							resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_yellow_ipad);
						else
							resultTextViewList.get(i).setBackgroundResource(R.drawable.yellow);
					}
					else
					{
						//changes for tab
						if(isTab)
							resultTextViewList.get(i).setBackgroundResource(R.drawable.tab_ml_no_box_ipad);
						else
							resultTextViewList.get(i).setBackgroundResource(R.drawable.ml_no1box);
					}
				}

				this.setTimerForReverseResult();
			}
			else
			{
				isCorrectAnsByUser = 0;
				this.keyBoardDisable();
                pauseTimer();
				visibleWrongImage();
				this.setHandler();
			}
		}

		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "outside clickOnGo()");
	}


	/**
	 * This method set the progress and also set points and lap
	 */
	@SuppressWarnings("deprecation")
	private void setProgress()
	{
		int timeTaken = endTime.getSeconds() - startTime.getSeconds();

		int pointForChallenger 		= equationList.get(showEquationIndex).getPoints();
		int timeTakenForChallenger 	= (int) equationList.get(showEquationIndex).getTimeTakenToAnswer();

		pointsForEachQuestion = (pointForChallenger + timeTakenForChallenger - timeTaken);
		points = points + (pointForChallenger + timeTakenForChallenger - timeTaken);

		if(progress == 100) // if progress is 100 then increment lap and set progress 25
		{
			lap = lap + 1; // Increment lap when user progress is 100
			progress = 25; // set progress for progress bar 25

		}
		else
		{
			progress = progress + 25;//update progress bar progress
		}

		setProgressColor(points , secondPlayerpoints);
		layoutProgress1.setProgress(progress);
		//layoutProgress2.setProgress(progress1);//changes

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		txtLap1.setText(transeletion.getTranselationTextByTextIdentifier("mfLblLap") + ": " + lap);
		txtPoints1.setText(transeletion.getTranselationTextByTextIdentifier("mfLblPts") + ": " + points);
		transeletion.closeConnection();
	}


	/**
	 * This method set the second player progress
	 */
	private void setSecondPlayerProgress()
	{
        try {
            if (equationList.get(0).getTimeTakenToAnswer() >
                    0 && equationList.get(0).getIsAnswerCorrect() == 1) {
                setsecondPlayerDataProgress(0, equationList.get(0).getTimeTakenToAnswer());
            }
        }catch(Exception e){
            e.printStackTrace();
        }
	}


	/**
	 * This method set the second player points data
	 * @param index
	 * @param timeTakenToAnswer
	 */
	private void setsecondPlayerDataProgress(final int index , double timeTakenToAnswer)
	{
		int sleepTime = (int) (timeTakenToAnswer * 1000);

		Handler handlerSetResult = new Handler();
		handlerSetResult.postDelayed(new Runnable()
		{
			@Override
			public void run()
			{
				if(progress1 == 100) // if progress is 100 then increment lap and set progress 25
				{
					progress1 = 25; // set progress for progress bar 25
				}
				else
				{
					if(equationList.get(index).getIsAnswerCorrect() == 1)
						progress1 = progress1 + 25;//update progress bar progress
				}

				secondPlayerpoints = secondPlayerpoints + equationList.get(index).getPoints();

				setProgressColor(points , secondPlayerpoints);
				layoutProgress2.setProgress(progress1);
				//layoutProgress1.setProgress(progress);//changes

				Translation transeletion = new Translation(SingleFriendzyEquationSolve.this);
				transeletion.openConnection();
				txtLap2.setText(transeletion.getTranselationTextByTextIdentifier("mfLblLap") + ": " +
						equationList.get(index).getLap());
				txtPoints2.setText(transeletion.getTranselationTextByTextIdentifier("mfLblPts")
                        + ": " + secondPlayerpoints);
				transeletion.closeConnection();

				playsound.playSoundForRightForSingleFriendzyForChallenger(SingleFriendzyEquationSolve.this);

				equationIndex(index);
			}
		}, sleepTime);
	}

	/**
	 * This method set the progress color
	 * @param playerpoints
	 * @param opponentPlayerpoints
	 */
	private void setProgressColor(int playerpoints , int opponentPlayerpoints){
		Drawable greenProgress = getResources().getDrawable(R.drawable.greenprogress);
		Drawable redProgress   = getResources().getDrawable(R.drawable.redprogress);

		if(vesrionGreaterThan){
			if(playerpoints > opponentPlayerpoints){
				layoutProgress1.setProgressDrawable(greenProgress);
				layoutProgress2.setProgressDrawable(redProgress);
			}
			else{
				layoutProgress2.setProgressDrawable(greenProgress);
				layoutProgress1.setProgressDrawable(redProgress);
			}
		}
	}


	/**
	 * Get the data from the equation list for setting progress according to i index
	 * @param index
	 */
	private void equationIndex(int index)
	{
		if(!isStopSecondPlayerProgress)
		{
			if(index + 1 < equationList.size())
			{
				if(equationList.get(index + 1).getTimeTakenToAnswer() > 0 && equationList.get(index + 1).getIsAnswerCorrect() == 1)
				{
					setsecondPlayerDataProgress(index + 1, equationList.get(index + 1).getTimeTakenToAnswer());
				}
				else
				{
					if(!isEquationFromServer)// if equation loaded from server and the if condition is fail then
						// stop the second player progresss
					{
						if(index < equationList.size() - 2)//changes when work on multi friendzy
							equationIndex(index + 1);
					}
				}
			}
		}
	}

	/**
	 * This method set the points
	 */
	private int setPoints(int operationId , int categoryId , int timeTaken)
	{
		if(LEARNING_CENTER_EQUATION_SOLVE_WITH_TIMER)
			Log.e(TAG, "inside setPoints()");

		int calculatedpoints = 0;//initially 0
		if(operationId == ADDITION || operationId == SUBTRACTION)
		{
			if(onebyoneDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(50 , timeTaken);
			}
			else if(twobyoneDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(100 , timeTaken);
			}
			else if(twobytwoDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(125 , timeTaken);
			}
			else if(threebythreeDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(150 , timeTaken);
			}
		}
		else if(operationId == MULTIPLICATION)
		{
			if(onebyoneDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(50 , timeTaken);
			}
			else if(twobyoneDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(150 ,timeTaken);
			}
			else if(twobytwoDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(250 , timeTaken);
			}
			else if(threebytwoDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(300 , timeTaken);
			}


		}
		else if(operationId == DIVISION)
		{
			if(onebyoneDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(50 , timeTaken);
			}
			else if(twobyoneDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(150 , timeTaken);
			}
			else if(twobytwoDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(250 , timeTaken);
			}
			else if(threebytwoDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(400 , timeTaken);
			}

		}
		else if(operationId == FRACTION_ADDITION_SUBTRACTION)
		{
			if(onebyoneDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(150 , timeTaken);
			}
			else if(twobyoneDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(200 , timeTaken);
			}
			else if(twobytwoDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(300 , timeTaken);
			}


		}
		else if(operationId == FRACTION_MULTIPLICATION_DIVISION)
		{
			if(onebyoneDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(150 , timeTaken);
			}
			else if(twobyoneDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(250 , timeTaken);
			}
			else if(twobytwoDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(400 , timeTaken);
			}


		}
		else if(operationId == DECIMAL_ADDITION_SUBTRACTION)
		{
			if(onebyoneDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(75 , timeTaken);
			}
			else if(twobyoneDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(100 , timeTaken);
			}
			else if(twobytwoDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(150 , timeTaken);
			}
			else if(threebythreeDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(200 , timeTaken);
			}


		}
		else if(operationId == DECIMAL_MULTIPLICATION_DIVISION)
		{
			if(onebyoneDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(75 , timeTaken);
			}
			else if(twobyoneDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(125 , timeTaken);
			}
			else if(twobytwoDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(200 , timeTaken);
			}
			else if(threebytwoDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(250 , timeTaken);
			}


		}
		else if(operationId == NEGATIVE_ADDITION_SUBTRACTION)
		{
			if(onebyoneDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(100 , timeTaken);
			}
			else if(twobyoneDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(150 , timeTaken);
			}
			else if(twobytwoDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(200 , timeTaken);
			}
			else if(threebythreeDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(250 , timeTaken);
			}
		}
		else if(operationId == NEGATIVE_MULTIPLICATION_DIVISION)
		{
			if(onebyoneDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(100 , timeTaken);
			}
			else if(twobyoneDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(200 , timeTaken);
			}
			else if(twobytwoDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(300 , timeTaken);
			}
			else if(threebytwoDigitList.contains(categoryId))
			{
				calculatedpoints = this.calculatePoints(500 , timeTaken);
			}
		}

		if(LEARNING_CENTER_EQUATION_SOLVE_WITH_TIMER)
			Log.e(TAG, "outside setPoints()");

		return calculatedpoints ;
	}


	/**
	 * This method calculate points and return it;
	 * @param maxPoints
	 */
	private int calculatePoints(int maxPoints , int timeTaken)
	{
		return maxPoints - timeTaken;
	}

	/**
	 * This method for displaying cross image for wrong question and invisible it
	 */
	private void setHandler()
	{
		Handler handler = new Handler();
		handler.postDelayed(new Runnable()
		{
			@Override
			public void run()
			{
				imgWrong.setVisibility(ImageView.INVISIBLE);

				if(operationId == MULTIPLICATION || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
						||(operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
				{
					if(getDigit(number2).size() > 1)
						startIndex = getDigit(number2).size() * numberOfBoxesForMultiplication;

					for( int i = startIndex ; i < resultTextViewList.size() ; i ++ )
					{
						resultTextViewList.get(i).setText("");
						resultTextViewList.get(i).setTextColor(Color.parseColor("#3CB3FF"));
					}
				}
				else if(operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
						||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))
				{
					for( int i = 0; i < numberOfBoxesForDivision ; i ++ )
					{
						resultTextViewList.get(i).setText("");
						resultTextViewList.get(i).setTextColor(Color.parseColor("#3CB3FF"));
					}
				}
				else
				{
					for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
					{
						resultTextViewList.get(i).setText("");
						resultTextViewList.get(i).setTextColor(Color.parseColor("#3CB3FF"));
					}
				}

				keyBoardEnable();
                startTimer();

				//savePlayData();

				if(operationId == MULTIPLICATION || (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
						||(operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
				{
					if(getDigit(number2).size() > 1)
						startIndex = getDigit(number2).size() * numberOfBoxesForMultiplication;

					for( int i = startIndex ; i < resultTextViewList.size() ; i ++ )
					{
						try{
							//changes on 22 Jan 2015
							setGreenBackGroundToTextViewForDivision(resultTextViewList.get(i));
							//end changes
							resultTextViewList.get(i).setText(resultDigitsList.get(i - startIndex));
						}catch(IndexOutOfBoundsException ee)
						{
							break;
						}
					}
				}
				else if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION)//fraction
				{
					String compareResult = result.replace("/", "").replace(" ", "");
					for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
					{
						//changes on 22 Jan 2015
						setGreenBackGroundToTextViewForDivision(resultTextViewList.get(i));
						//end changes
						resultTextViewList.get(i).setText(compareResult.charAt(i) + "");
					}
				}
				else if(operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
						||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))
				{
					for( int i = 0 ; i < numberOfBoxesForDivision ; i ++ )
					{
						try{
							//changes on 22 Jan 2015
							setGreenBackGroundToTextViewForDivision(resultTextViewList.get(i));
							//end changes

							resultTextViewList.get(i).setText(resultDigitsList.get(i));
						}catch(IndexOutOfBoundsException ee)
						{
							break;
						}
					}
				}
				else
				{
					for( int i = 0 ; i < resultTextViewList.size() ; i ++ )
					{
						//changes on 22 Jan 2015
						setGreenBackGroundToTextView(resultTextViewList.get(i));
						//end changes
						resultTextViewList.get(i).setText(resultDigitsList.get(i));
					}
				}

				keyBoardDisable();
                pauseTimer();
				Handler handlerSetResult = new Handler();
				handlerSetResult.postDelayed(new Runnable()
				{
					@Override
					public void run()
					{
						//changes for timer on 11 Mar 2014
						savePlayData();
						//end chages

						if(operationId == FRACTION_ADDITION_SUBTRACTION || operationId == FRACTION_MULTIPLICATION_DIVISION)//fraction
						{
							isYellowboxForFraction = false;
							resultTextViewList.clear();
							linearLayoutQuotient.removeAllViews();
							linearLayoutNumerator.removeAllViews();
							linearLayoutDenominator.removeAllViews();
							setIndexForShowEquation();
							keyBoardEnable();
                            startTimer();
						}
						else if(operationId == MULTIPLICATION
								|| (operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("x"))
								|| (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("x")))
						{
							isYellowboxForFraction = false;
							resultTextViewList.clear();
							linearLayoutMultiple.removeAllViews();
							linearLayoutResult.removeAllViews();
							txtCarry.setText("");
							txtCarry.setBackgroundResource(R.drawable.ml_no1box);
							setIndexForShowEquation();
							keyBoardEnable();
                            startTimer();
						}
						else if(operationId == DIVISION || (operationId == NEGATIVE_MULTIPLICATION_DIVISION && operator.equals("/"))
								||(operationId == DECIMAL_MULTIPLICATION_DIVISION && operator.equals("/")))
						{
							isYellowboxForFraction = false;
							resultTextViewList.clear();
							linearLayoutResult.removeAllViews();
							linearLayoutRowsForDivision.removeAllViews();
							txtCarry.setText("");
							txtCarry.setBackgroundResource(R.drawable.ml_no1box);
							setIndexForShowEquation();
							keyBoardEnable();
                            startTimer();
						}
						else
						{
							resultTextViewList.clear();
							linearLayoutResultRow.removeViews(0, noOfDigitsInResult);
							txtCarry.setText("");
							txtCarry.setBackgroundResource(R.drawable.ml_no1box);
							setIndexForShowEquation();
							keyBoardEnable();
                            startTimer();
						}
					}
				}, SLEEP_TIME);
			}
		//}, SLEEP_TIME / 4);
		}, this.getWrongAnswerShowTime(3 , SLEEP_TIME));
	}

	/**
	 * This method call when user give the reverse answer
	 */
	private void setTimerForReverseResult(int DESIGN_FOR){

		START_TIME = startTimerForRoughArea;
		timer.cancel();
		timer = new MyTimer(START_TIME, END_TIME);

		if(DESIGN_FOR == MULTIPLICATION_DESIGN){
			try{
                //for the change on the 25 jun 2015  ,treat reverse answer as right, according to ryan
                this.setProgress();
                rightAnsCounter ++ ;
                isCorrectAnsByUser = 1;
                //end change , comment the following line isCorrectAnsByUser = 0;
                //isCorrectAnsByUser = 0;
                this.savePlayData();

				this.keyBoardDisable();
				this.showReverseCorrectAnsDialog(this, timer, new OnReverseDialogClose() {
					@Override
					public void onClose() {
						isYellowboxForFraction = false;
						resultTextViewList.clear();
						linearLayoutMultiple.removeAllViews();
						linearLayoutResult.removeAllViews();
						setIndexForShowEquation();
						keyBoardEnable();
                        startTimer();
					}
				},resultTextViewList , resultDigitsList , MULTIPLICATION_DESIGN);
			}catch(Exception e){
				e.printStackTrace();
			}
			//end changes
		}
	}

    /**
     * pause timer
     */
    private void pauseTimer(){
        try {
            START_TIME = startTimerForRoughArea;
            timer.cancel();
            timer = new MyTimer(START_TIME, END_TIME);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * start timer
     */
    private void startTimer(){
        try {
            timer.start();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

	/**
	 * This method call when user give the reverse answer
	 */
	private void setTimerForReverseResult()
	{
		START_TIME = startTimerForRoughArea;
		timer.cancel();
		timer = new MyTimer(START_TIME, END_TIME);
		/*DialogGenerator dg = new DialogGenerator(this);
		dg.generateDialogForSolvingProblem(timer);*/

		//changes on 22 Jan 2015
		try{
            //for the change on the 25 jun 2015  ,treat reverse answer as right, according to ryan
            this.setProgress();
            rightAnsCounter ++ ;
            isCorrectAnsByUser = 1;
            //end change , comment the following line isCorrectAnsByUser = 0;
            //isCorrectAnsByUser = 0;
            this.savePlayData();

			this.keyBoardDisable();
			this.showReverseCorrectAnsDialog(this, timer, new OnReverseDialogClose() {
				@Override
				public void onClose() {
					resultTextViewList.clear();
					linearLayoutResultRow.removeViews(0, noOfDigitsInResult);
					setIndexForShowEquation();
					keyBoardEnable();
                    startTimer();
				}
			},resultTextViewList , resultDigitsList);
		}catch(Exception e){
			e.printStackTrace();
		}
		//end changes

	}


	/**
	 * This methos set the index for show equation and also calles showEqiuation method which
	 * show the equation on this selected index
	 */
	private void setIndexForShowEquation()
	{
		if(showEquationIndex < equationsIdList.size() - 1)
		{
			this.showEqautionData(this.getEquation(++showEquationIndex));
		}
		else
		{
			showEquationIndex = 0 ;
			this.showEqautionData(this.getEquation(++showEquationIndex));
		}
	}


	/**
	 * This class find equations from server
	 * @author Yashwant Singh
	 *
	 */
	class FindCompleteEquationsForPlayer extends AsyncTask<Void, Void, ArrayList<EquationFromServerTransferObj>>
	{
		private String userId;
		private String playerId;

		FindCompleteEquationsForPlayer(String userId , String playerId)
		{
			this.userId 	= userId;
			this.playerId 	= playerId;
		}

		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
		}

		@Override
		protected ArrayList<EquationFromServerTransferObj> doInBackground(Void... params)
		{
			//Log.e(TAG, "inside FindCompleteEquationsForPlayer inside do in background ");

			ArrayList<EquationFromServerTransferObj> equatiolListFromServer = new ArrayList<EquationFromServerTransferObj>();

			SingleFriendzyServerOperation serverObj = new SingleFriendzyServerOperation();
			equatiolListFromServer = serverObj.getEquations(userId, playerId);

			//Log.e(TAG, "outside FindCompleteEquationsForPlayer inside do in background ");

			return equatiolListFromServer;
		}

		@Override
		protected void onPostExecute(ArrayList<EquationFromServerTransferObj> result)
		{
			//Log.e(TAG, "inside FindCompleteEquationsForPlayer inside do in onPostExecute ");

			if(equationList == null  && result != null)
			{
				equationList = result;

				/*if(equationList != null)
			{*/
				equationsIdList = new ArrayList<Integer>();

				for( int i = 0 ; i < equationList.size() ; i ++ )
				{
					equationsIdList.add(equationList.get(i).getMathEquationId());
				}
			}

			//Log.e(TAG, "outside FindCompleteEquationsForPlayer inside do in onPostExecute ");

			super.onPostExecute(result);
		}
	}


	/**
	 * Save Temp Player result on server
	 * @author Yashwant Singh
	 *
	 */
	class SaveTempPlayerScoreOnServer extends AsyncTask<Void, Void, Void>
	{
		private MathResultTransferObj mathobj = null;

		SaveTempPlayerScoreOnServer(MathResultTransferObj mathObj)
		{
			this.mathobj = mathObj;
		}

		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params)
		{
			Register register = new Register(SingleFriendzyEquationSolve.this);
			if(gameType.equals("Compete"))
			{
				register.savePlayerScoreOnServerForloginuserForSingleFriendzy(mathobj);
			}
			else
			{
				register.savePlayerScoreOnServerForloginuser(mathobj);
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result)
		{
			super.onPostExecute(result);
			//new AddCoinAndPointsForLoginUser(getPlayerPoints(mathobj)).execute(null,null,null);
		}
	}

	/**
	 * This asyncTask Add points and coins on server
	 * @author Yashwant Singh
	 *
	 */
	class AddCoinAndPointsForLoginUser extends AsyncTask<Void, Void, Void>
	{
		private PlayerTotalPointsObj playerObj = null;

		AddCoinAndPointsForLoginUser(PlayerTotalPointsObj playerObj)
		{
			this.playerObj = playerObj;

			LearningCenterimpl learningCenterimpl = new LearningCenterimpl(SingleFriendzyEquationSolve.this);
			learningCenterimpl.openConn();
			playerObj = learningCenterimpl.getDataFromPlayerTotalPoints(playerObj.getPlayerId());
			learningCenterimpl.closeConn();

			this.playerObj.setTotalPoints(playerObj.getTotalPoints());
			this.playerObj.setCoins(playerObj.getCoins());
		}

		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params)
		{
			Register register = new Register(SingleFriendzyEquationSolve.this);
			register.addPointsAndCoinsOnServerForloginUser(playerObj);

			return null;
		}

		@Override
		protected void onPostExecute(Void result)
		{
			super.onPostExecute(result);
		}
	}


	/**
	 * This asyncktask update the level and stars on server for bonus
	 * @author Yashwant Singh
	 *
	 */
	class AddFriendzyForCompleteLevelForBonus extends AsyncTask<Void, Void, Void>
	{
		private String userId ;
		private String playerId;
		int level;
		int stars;
		String appId;

		AddFriendzyForCompleteLevelForBonus(String userId , String playerId , int level , int stars , String appId)
		{
			this.userId = userId;
			this.playerId = playerId;
			this.level = level;
			this.stars = stars;
			this.appId = appId;
		}

		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params)
		{
			SingleFriendzyServerOperation singleServerObj = new SingleFriendzyServerOperation();
			singleServerObj.addMathComplteLevelForBonus(userId, playerId, level, stars, appId);
			return null;
		}

		@Override
		protected void onPostExecute(Void result)
		{
			super.onPostExecute(result);
		}
	}

	@Override
	protected void onResume() {
		if(!isClickOnWorkArea){
			isStopSecondPlayerProgress = false;//for stop the challenger player progresss
			if(!isFromOnCreate){
				if(playsound != null)
					playsound.playSoundForSingleFriendzyEquationSolve(SingleFriendzyEquationSolve.this);
			}else{
				isFromOnCreate = false;
			}
		}else
			isClickOnWorkArea = false;
		super.onResume();
	}

	@Override
	protected void onPause() {
		if(!isClickOnWorkArea){
			isStopSecondPlayerProgress = true;//for stop the challenger player progresss
			if(playsound != null)
				playsound.stopPlayer();
		}
		super.onPause();
	}
}
