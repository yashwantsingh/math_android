package com.mathfriendzy.controller.singlefriendzy;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.base.ActBaseClass;
import com.mathfriendzy.controller.inapp.GetMoreCoins;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.singlefriendzy.schoolcurriculum.SingleFriendzySchoolCurriculumEquationSolve;
import com.mathfriendzy.controller.top100.Top100Activity;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.helper.YesNoListenerInterface;
import com.mathfriendzy.listener.OnPurchaseDone;
import com.mathfriendzy.model.appunlock.GetAppUnlockStatusResponse;
import com.mathfriendzy.model.chooseAvtar.ChooseAvtarOpration;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.CoinsFromServerObj;
import com.mathfriendzy.model.learningcenter.LearningCenteServerOperation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.learningcenter.PurchaseItemObj;
import com.mathfriendzy.model.player.temp.TempPlayerOperation;
import com.mathfriendzy.model.schoolcurriculum.singlefriendzy.SingleFriendzySchoolCurriculumImpl;
import com.mathfriendzy.model.schoolcurriculum.singlefriendzy.SingleFriendzyServerOperationForWordProblem;
import com.mathfriendzy.model.schoolcurriculum.singlefriendzy.SingleFriendzyTimeTransObj;
import com.mathfriendzy.model.singleFriendzy.ChallengerTransferObj;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DateTimeOperation;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ICommonUtils;
import com.mathfriendzy.utils.MathVersions;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;

import static com.mathfriendzy.utils.ICommonUtils.FACEBOOK_HOST_NAME;
import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;
import static com.mathfriendzy.utils.ICommonUtils.SINGLE_FRIENDZY_MAIN;

public class SingleFriendzyMain extends ActBaseClass implements OnClickListener
{
	private TextView lblSingleFriendzy 				= null;
	private TextView  mfLblTextCompeteWithOthers 	= null;
	private TextView txtPlayerName  				= null;
	private TextView txtPlayerAddress				= null;
	private TextView txtPlayerPoints 				= null;		
	private ImageView imgPlayer     				= null;
	private Button  btnTop100 						= null;
	private RelativeLayout levelLayout              = null;
	private Button btnLevelPlay                     = null;
	private Button btnBonous1                     	= null;
	private Button btnBonous2                     	= null;
	private Button btnBonous3                     	= null;

	private Bitmap profileImageBitmap = null;
	private int completeLevel = 0;
	private int playLevelbuttonWidth = 150;
	private int playLevelButtonMarginHieght = 90;//set the top margin
	private int marginGap = 50;
	private int levelStartingIndex = 0 ;//this will contain the value of starting index which is divisivle by 30
	private int MAX_LEVEL_SHOW = 30 ; // this is used for maximum 30 level on the screen
	private int appStatus    = 0;
	private int UNLOCK_ITEM_ID = 11 ; // unlock item id for single friendzy
	private boolean isUnlockSingleFriendzy = false;//hold the lock/unlock status of single friendzy

	private String bonusCoins = " 200 ";

	//private final int MAX_CONIS = 10000;
	private final int MAX_CONIS = 100;
	private ArrayList<Button> btnList = null;

	private  int SCREEN_DENISITY   = 240;

	private final String TAG = this.getClass().getSimpleName();

	private int max_width_for_level_layout = 90;

	private boolean isTab = false;

	private final int TAB_HEIGHT = 800;
	private final int TAB_WIDTH = 480;
	private final int TAB_DENISITY = 160;

	private final int TAB_HEIGHT_1024 = 1024; // tab with 1024 height
	private int textSize = 0; // text size for level

	//for word problem changes
	private TextView lblSolveEquation 		= null;
	private TextView lblSchoolCurriculum 	= null;
	private ImageView imgSolveEquation      = null;
	private ImageView imgSchoolCurriculum   = null;


	//for updated on 24 11 2014
	private String internetNoConnectedMsg = null;
	private String playWithComputerMsg = null;
	private String noThanksText = null;
	private String okText = null;
	private int playerGrade = 1;

    private LinearLayout lnlevelLayout = null;
    private String bonusCoinsText = "";
    private String levelText = "";
    private String playAgainLevel = "";

    //for New App Change
    private RelativeLayout layoutCheckBox = null;

    //NewInApp changes
    private ProgressDialog progressDialogForNewInApp = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_single_friendzy_main);

		if(SINGLE_FRIENDZY_MAIN)
			Log.e(TAG, "inside onCreate()");

        this.init();
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);  

		boolean tabletSize = getResources().getBoolean(R.bool.isTablet);

		if(tabletSize)
		{
			//Log.e(TAG, "Tab");
			isTab = true;
			textSize = 30;

			if(metrics.heightPixels <= TAB_HEIGHT_1024)
			{
				//Log.e(TAG, "Tab1");
				max_width_for_level_layout = 150;
				playLevelButtonMarginHieght = 110;
				marginGap = 110;
				playLevelbuttonWidth  = 150;
			}
			else
			{
				if(metrics.densityDpi >= 320 && metrics.heightPixels >= 1900
						&& metrics.widthPixels >= 1500){
					//Log.e(TAG, "tab4");
					max_width_for_level_layout = 225;
					playLevelButtonMarginHieght = 220;
					marginGap = 220;
					playLevelbuttonWidth  = 250;
				}else{
					if (metrics.densityDpi > 160){
						//Log.e(TAG, "tab2");
						max_width_for_level_layout = 180;
						playLevelButtonMarginHieght = 150;
						marginGap = 146;
						playLevelbuttonWidth  = 200;
					}
					else{
						//Log.e(TAG, "tab3");
						max_width_for_level_layout = 150;
						playLevelButtonMarginHieght = 110;
						marginGap = 110;
						playLevelbuttonWidth  = 150;
					}
				}
			}
		}
		else
		{
			//Log.e(TAG, "Phone");
			isTab = false;

			textSize = 15;
			if(metrics.heightPixels <= TAB_HEIGHT && metrics.widthPixels >= TAB_WIDTH && metrics.densityDpi == TAB_DENISITY)
			{
				setContentView(R.layout.activity_single_friendzy_main_tab_low_denisity);

				max_width_for_level_layout = 90;
				playLevelbuttonWidth       = 150;
				playLevelButtonMarginHieght = 90;
				marginGap = 55;
			}
			else if ((getResources().getConfiguration().screenLayout &
					Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
					metrics.densityDpi > SCREEN_DENISITY)
			{
				max_width_for_level_layout = 180;
				playLevelbuttonWidth = 300;
				playLevelButtonMarginHieght  = 180;
				marginGap = 110;
			}
			else
			{
				if ((getResources().getConfiguration().screenLayout &
						Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
						metrics.densityDpi > 160
						&&
						(getResources().getConfiguration().screenLayout &
								Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL &&
								metrics.densityDpi <= SCREEN_DENISITY)
				{
					max_width_for_level_layout = 135;
					playLevelbuttonWidth       = 225;
					playLevelButtonMarginHieght= 135;
					marginGap = 83;
				}
				else
				{
					max_width_for_level_layout = 90;
					playLevelbuttonWidth       = 150;
					playLevelButtonMarginHieght = 90;
					marginGap = 55;
				}
			}
		}

		this.setWidgetsReferences();
		this.setTextFromTranselation();
		this.setListenerOnWidgets();
		this.setPlayerData();
		this.createLevelLayout();
		//this.setPlayLevelPosition();

		//changes for word problem
		this.setChakedImage();
		//end changes for word problem

		/*SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);	 
		if(sheredPreference.getBoolean(IS_LOGIN, false))
		{
			if(!CommonUtils.isInternetConnectionAvailable(this))
			{
				DialogGenerator dg = new DialogGenerator(this);
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				//dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
				//changes when testing by client
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnected"));
				transeletion.closeConnection();
			}
		}*/

		if(CommonUtils.isInternetConnectionAvailable(this)){
			SharedPreferences learnCenterTimePreff = getSharedPreferences(ICommonUtils.SINGLE_FRIENDZY_TIME_PREFF, 0);
			if(learnCenterTimePreff.getLong(ICommonUtils.SINGLE_FRIENDZY_TIME, 0) == 0){
				new GetTimeDefinedForSingleFriendzy().execute(null,null,null);
			}else{
				long remainTime = learnCenterTimePreff.getLong(ICommonUtils.SINGLE_FRIENDZY_TIME, 0);
				long callTime   = learnCenterTimePreff.getLong(ICommonUtils.SINGLE_FRIENDZY_TIME_API_CALL_TIME, 0);
				Date date = new Date();
				if(((date.getTime() - callTime) / 1000) > remainTime){
					new GetTimeDefinedForSingleFriendzy().execute(null,null,null);
				}
			}
		}

        this.setVisibilityOfLayoutBasedOnAppVersion();

		if(SINGLE_FRIENDZY_MAIN)
			Log.e(TAG, "outside onCreate()");
	}

    //New InAppp Changes
    private void init(){
        try{
            progressDialogForNewInApp = MathFriendzyHelper.getProgressDialog(this , "");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

	/**
	 * This method set the chacked 
	 * and unchacked image
	 * this will add for word problem
	 */
	private void setChakedImage(){
		if(CommonUtils.isClickedSolveEquation){ 
			imgSolveEquation.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
			imgSchoolCurriculum.setBackgroundResource(R.drawable.mf_check_box_ipad);
		}else{
			imgSchoolCurriculum.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
			imgSolveEquation.setBackgroundResource(R.drawable.mf_check_box_ipad);
		}
	}

	/**
	 * This method set player data
	 */
	private void setPlayerData() 
	{		
		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);

		//updated on 24 11 2014
		playerGrade = sharedPreffPlayerInfo.getInt("grade" , 1);
		//end 24 11 2014 changes

		completeLevel = sharedPreffPlayerInfo.getInt("completeLevel", 0);//get played complete level
        if(completeLevel <= 0)
            completeLevel = 1;

		/*Log.e(TAG, "complete level " + completeLevel + " playerid " + sharedPreffPlayerInfo.getString("playerId", "0")
				+ " userid " + sharedPreffPlayerInfo.getString("userId", "0"));*/
		
		this.invibleBonusImageButton(this.getInvisibleBonousButton(completeLevel));

		levelStartingIndex = completeLevel / MAX_LEVEL_SHOW ; 

		PlayerTotalPointsObj playerObj = null;
		LearningCenterimpl learningCenterimpl = new LearningCenterimpl(this);
		learningCenterimpl.openConn();
		if(sharedPreffPlayerInfo.getString("playerId", "0").equals(""))
		{
			playerObj = learningCenterimpl.getDataFromPlayerTotalPoints("0");
		}
		else
		{
			playerObj = learningCenterimpl.getDataFromPlayerTotalPoints
					(sharedPreffPlayerInfo.getString("playerId", "0"));
		}
		learningCenterimpl.closeConn();			

		txtPlayerName.setText(sharedPreffPlayerInfo.getString("playerName", ""));

		if(sharedPreffPlayerInfo.getString("state", "").equals(""))
		{
			txtPlayerAddress.setText(sharedPreffPlayerInfo.getString("city", ""));
		}
		else
		{
			txtPlayerAddress.setText(sharedPreffPlayerInfo.getString("city", "")
					+ ", " + sharedPreffPlayerInfo.getString("state", ""));
		}

		Translation transeletion1 = new Translation(this);
		transeletion1.openConnection();

		DateTimeOperation numberformat = new DateTimeOperation();
		txtPlayerPoints.setText(numberformat.setNumberString(playerObj.getTotalPoints() + "")
				+ " " + transeletion1.getTranselationTextByTextIdentifier("btnTitlePoints"));

		transeletion1.closeConnection();

		String imageName = sharedPreffPlayerInfo.getString("imageName",null);
		try
		{
			Long.parseLong(imageName);
			//changes for Internet Connection
			if(CommonUtils.isInternetConnectionAvailable(this))
			{
				String strUrl = FACEBOOK_HOST_NAME + imageName + "/picture?type=large";
				new FacebookImageLoaderTask(strUrl).execute(null,null,null);
			}
			else
			{
				DialogGenerator dg = new DialogGenerator(this);
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
				transeletion.closeConnection();
			}
		}
		catch(NumberFormatException ee)
		{
			ChooseAvtarOpration chooseAvtarObj = new ChooseAvtarOpration();
			chooseAvtarObj.openConn(this);
			if(chooseAvtarObj.getAvtarImageByName(imageName) != null)
			{
				//profileImageBitmap = CommonUtils.getBitmapFromByte(chooseAvtarObj.getAvtarImageByName(imageName) , SingleFriendzyMain.this);
				profileImageBitmap = CommonUtils.getBitmapFromByte(chooseAvtarObj.getAvtarImageByName(imageName));
				imgPlayer.setImageBitmap(profileImageBitmap);
			}
			chooseAvtarObj.closeConn();
		}
	}


	/**
	 * This method return the index on which the bonus button is invisivle
	 * @param level
	 * @return
	 */
	private int getInvisibleBonousButton(int level)
	{		
		/*if(level % 10 == 0)
		{			
			return (level % MAX_LEVEL_SHOW) / 10;

		}*/

		//return MAX_LEVEL_SHOW / 10 + 1;
		int retValue = 0;
		if(level % MAX_LEVEL_SHOW >= 10 && level % MAX_LEVEL_SHOW < 20)
			retValue = 1;
		else if(level % MAX_LEVEL_SHOW >= 20 && level % MAX_LEVEL_SHOW < 30)
			retValue = 2;
		else if(level % MAX_LEVEL_SHOW == 0 )
			retValue = 0;
		else
			retValue = 4;

		return retValue;

	} 

	/**
	 * This method invisible the bonus images
	 */
	private void invibleBonusImageButton(int index)
	{
		switch(index)
		{
		case 1:
			btnBonous1.setVisibility(Button.INVISIBLE);
			break;
		case 2:
			btnBonous1.setVisibility(Button.INVISIBLE);
			btnBonous2.setVisibility(Button.INVISIBLE);
			break;
		case 0:
			btnBonous1.setVisibility(Button.INVISIBLE);
			btnBonous2.setVisibility(Button.INVISIBLE);
			btnBonous3.setVisibility(Button.INVISIBLE);
			break;
		}
	}


	/**
	 * This method set the widgets references
	 */
	private void setWidgetsReferences() 
	{
		if(SINGLE_FRIENDZY_MAIN)
			Log.e(TAG, "inside setWidgetsReferences()");

		lblSingleFriendzy 			= (TextView) findViewById(R.id.lblSingleFriendzy);
		mfLblTextCompeteWithOthers 	= (TextView) findViewById(R.id.mfLblTextCompeteWithOthers);
		txtPlayerName 				= (TextView) findViewById(R.id.txtPlayerName);
		txtPlayerAddress 			= (TextView) findViewById(R.id.txtPlayerAddress);
		txtPlayerPoints 			= (TextView) findViewById(R.id.txtPlayerCoins);
		imgPlayer 					= (ImageView) findViewById(R.id.imgPlayer);
		btnTop100 					= (Button) findViewById(R.id.btnTop100);
		levelLayout 				= (RelativeLayout) findViewById(R.id.levelLayout);

		btnLevelPlay                = (Button) findViewById(R.id.btnLevelPlay);
		btnBonous1               	= (Button) findViewById(R.id.btnBonous1);
		btnBonous2                	= (Button) findViewById(R.id.btnBonous2);
		btnBonous3                	= (Button) findViewById(R.id.btnBonous3);

		//for word problrm changes
		lblSolveEquation 	= (TextView) findViewById(R.id.lblSolveEquation);
		lblSchoolCurriculum = (TextView) findViewById(R.id.lblSchoolCurriculum);
		imgSolveEquation 	= (ImageView) findViewById(R.id.imgSolveEquation);
		imgSchoolCurriculum = (ImageView) findViewById(R.id.imgSchoolCurriculum);
        lnlevelLayout = (LinearLayout) findViewById(R.id.lnlevelLayout);

        this.setWidgetsReferenceForNewApps();

		if(SINGLE_FRIENDZY_MAIN)
			Log.e(TAG, "outside setWidgetsReferences()");
	}


	/**
	 * This method set the text value from translation
	 */
	private void setTextFromTranselation() 
	{
		if(SINGLE_FRIENDZY_MAIN)
			Log.e(TAG, "inside setTextFromTranselation()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		lblSingleFriendzy.setText(transeletion.getTranselationTextByTextIdentifier("lblSingleFriendzy"));
		mfLblTextCompeteWithOthers.setText(transeletion.getTranselationTextByTextIdentifier("mfLblTextCompeteWithOthers") + "!");
		btnTop100.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleTop100"));
		txtPlayerPoints.setText("0"
				+ " " + transeletion.getTranselationTextByTextIdentifier("btnTitlePoints"));

		btnLevelPlay.setText(transeletion.getTranselationTextByTextIdentifier("playTitle") + " "
				+ transeletion.getTranselationTextByTextIdentifier("mfLblLevel"));

		btnBonous1.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBonus")
				+ bonusCoins + transeletion.getTranselationTextByTextIdentifier("lblCoins"));
		btnBonous2.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBonus")
				+ bonusCoins + transeletion.getTranselationTextByTextIdentifier("lblCoins"));
		btnBonous3.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBonus")
				+ bonusCoins + transeletion.getTranselationTextByTextIdentifier("lblCoins"));

		//for word problem changes
		lblSolveEquation.setText(transeletion.getTranselationTextByTextIdentifier("lblSolveEquations"));
		lblSchoolCurriculum.setText(transeletion.getTranselationTextByTextIdentifier("lblSchoolsCurriculum"));

		internetNoConnectedMsg = transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet");
		playWithComputerMsg = transeletion.getTranselationTextByTextIdentifier("mfAlertMsgWouldYouLikeToPlayWithComputer");
		noThanksText = transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks");
		okText = transeletion.getTranselationTextByTextIdentifier("btnTitleOK");
        bonusCoinsText = transeletion.getTranselationTextByTextIdentifier("btnTitleBonus")
                + bonusCoins + transeletion.getTranselationTextByTextIdentifier("lblCoins");
        playAgainLevel = transeletion.getTranselationTextByTextIdentifier("playTitle") + " "
                + transeletion.getTranselationTextByTextIdentifier("mfLblLevel");
        levelText = transeletion.getTranselationTextByTextIdentifier("mfLblLevel");
		transeletion.closeConnection();

		if(SINGLE_FRIENDZY_MAIN)
			Log.e(TAG, "outside setTextFromTranselation()");
	}


	/** 
	 * This method set listener on widgets
	 */
	private void setListenerOnWidgets() 
	{
		btnTop100.setOnClickListener(this);
		btnLevelPlay.setOnClickListener(this);
		imgSolveEquation.setOnClickListener(this);
		imgSchoolCurriculum.setOnClickListener(this);
	}


	/**
	 * This method check for application is unlock or not
	 * @param itemId
	 * @param userId
	 * @return
	 */
	private int getApplicationUnLockStatus(int itemId,String userId)
	{	
		LearningCenterimpl learnignCenterImpl = new LearningCenterimpl(this);
		learnignCenterImpl.openConn();
		appStatus = learnignCenterImpl.getAppUnlockStatus(itemId,userId);
		learnignCenterImpl.closeConn();
		return appStatus;
	}


	/**
	 * This method check application unlock status for unlock the application
	 * @return
	 */
	private boolean checkUnlockStatus(){
        if(MathFriendzyHelper.isAppUnlockCategoryPurchased(this)){
            appStatus = 1;
            return true;
        }
		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		if(this.getApplicationUnLockStatus(100 , sharedPreffPlayerInfo.getString("userId", "")) == 1)
			return true;
		else if(this.getApplicationUnLockStatus(UNLOCK_ITEM_ID , sharedPreffPlayerInfo.getString("userId", "")) == 1)
			return true;
		else
			return false;
	}

    private boolean isUnlockForSchoolSubscription(int itemId){
        if(MathFriendzyHelper.isAppUnlockCategoryPurchased(this)){
            appStatus = 1;
            return true;
        }
        SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
        if(this.getApplicationUnLockStatus(itemId , sharedPreffPlayerInfo.getString("userId", "")) == 1)
            return true;
        else
            return false;
    }

	/**
	 * This method create level layout 
	 */
	private void createLevelLayout() 
	{
		if(SINGLE_FRIENDZY_MAIN)
			Log.e(TAG, "inside createLevelLayout()");


		/*btnList = new ArrayList<Button>();

		isUnlockSingleFriendzy = this.checkUnlockStatus();

		RelativeLayout top3Layout = new RelativeLayout(this);
		top3Layout.setId(2000);//set id to the layout

		RelativeLayout otherLevelLayout   = new RelativeLayout(this);

		if(!isUnlockSingleFriendzy)
			otherLevelLayout.setBackgroundResource(R.drawable.buttons_orange_bck);


		for( int i = 0 ; i <= 30 ; i ++ )
		{	
			Button btnLevel = new Button(this);
			if(isTab)
				btnLevel.setTextSize(textSize);

			RelativeLayout.LayoutParams layoutParam = new RelativeLayout.LayoutParams(max_width_for_level_layout,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			layoutParam.addRule(RelativeLayout.CENTER_HORIZONTAL , RelativeLayout.TRUE);

			if(i == 0)
			{				
				btnLevel.setText("Level");
				btnLevel.setId( i + 1);
				btnLevel.setTextColor(Color.WHITE);
				btnLevel.setTypeface(null,Typeface.BOLD);

				*//*if(isTab)
					btnLevel.setBackgroundResource(R.drawable.tab_sf_level_ipad);
				else*//*
				btnLevel.setBackgroundResource(R.drawable.sf_level);
				btnLevel.setLayoutParams(layoutParam);
				top3Layout.addView(btnLevel);
			}
			else if( i > 0 && i < 4)
			{
				btnLevel.setText("" + (((MAX_LEVEL_SHOW * levelStartingIndex) + (i + 1)) - 1));
				btnLevel.setId( i + 1);
				btnLevel.setTextColor(Color.WHITE);
				btnLevel.setTypeface(null,Typeface.BOLD);
				if( i == 1 )
				{
					if(isTab)
						btnLevel.setBackgroundResource(R.drawable.tab_sf_green_ipad);
					else
						btnLevel.setBackgroundResource(R.drawable.sf_green);

				}
				else
				{
					if(isTab)
						btnLevel.setBackgroundResource(R.drawable.tab_sf_gray_ipad);
					else
						btnLevel.setBackgroundResource(R.drawable.sf_gray);
				}

				layoutParam.addRule(RelativeLayout.BELOW , i + 1 - 1);

				btnLevel.setLayoutParams(layoutParam);

				btnList.add(btnLevel);

				top3Layout.addView(btnLevel);
			}
			else
			{
				btnLevel.setText("" + (((MAX_LEVEL_SHOW * levelStartingIndex) + (i + 1)) - 1));
				btnLevel.setId( i + 1);
				btnLevel.setTextColor(Color.WHITE);
				btnLevel.setTypeface(null,Typeface.BOLD);
				//btnLevel.setBackgroundResource(R.drawable.sf_gray);

				if(isTab)
					btnLevel.setBackgroundResource(R.drawable.tab_sf_gray_ipad);
				else
					btnLevel.setBackgroundResource(R.drawable.sf_gray);

				if( i > 4) // if i < 4 then does not set the below property , because it is the first element in this layout 
					layoutParam.addRule(RelativeLayout.BELOW , i + 1 - 1);

				btnLevel.setLayoutParams(layoutParam);

				otherLevelLayout.addView(btnLevel);

				if( i == 4 && (!isUnlockSingleFriendzy)) //  if i == 4 then add lock to the layout
				{
					layoutParam.setMargins(0, 10, 0, 0);
					RelativeLayout.LayoutParams layoutParamLock = new RelativeLayout.LayoutParams(max_width_for_level_layout / 3 , max_width_for_level_layout / 3);
					layoutParamLock.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,RelativeLayout.TRUE);
					layoutParamLock.addRule(RelativeLayout.ALIGN_PARENT_TOP,RelativeLayout.TRUE);

					Button btnLock = new Button(this);
					btnLock.setBackgroundResource(R.drawable.sf_lock);
					btnLock.setLayoutParams(layoutParamLock);

					btnLock.setOnClickListener(new OnClickListener() 
					{
						@Override
						public void onClick(View v) 
						{						
							clickOnLevel(4);
						}
					});

					otherLevelLayout.addView(btnLock);
				}

				btnList.add(btnLevel);
			}

			btnLevel.setOnClickListener(new OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					for(int i = 0 ; i < btnList.size() ; i ++ )
					{
						if(v == btnList.get(i))
						{
							//Log.e(TAG, btnList.get(i).getHeight() + "");
							clickOnLevel(i + 1);
						}
					}
				}
			});
		}

		RelativeLayout.LayoutParams layoutParamForTop = new RelativeLayout.LayoutParams(max_width_for_level_layout + 10,
				RelativeLayout.LayoutParams.WRAP_CONTENT);

		top3Layout.setLayoutParams(layoutParamForTop);
		levelLayout.addView(top3Layout);

		RelativeLayout.LayoutParams layoutParamForOther = new RelativeLayout.LayoutParams(max_width_for_level_layout + 10,
				RelativeLayout.LayoutParams.WRAP_CONTENT);

		layoutParamForOther.addRule(RelativeLayout.BELOW ,top3Layout.getId());
		otherLevelLayout.setLayoutParams(layoutParamForOther);

		levelLayout.addView(otherLevelLayout);*/

        //change for the level layout
        int numberOfCompleleve = completeLevel - (levelStartingIndex * MAX_LEVEL_SHOW);
        Log.e(TAG, "completeLevel "+ completeLevel);
        isUnlockSingleFriendzy = this.checkUnlockStatus();
        //numberOfCompleleve = 5;
        final LinearLayout mainLayout = new LinearLayout(this);
        mainLayout.setOrientation(LinearLayout.HORIZONTAL);
        final LinearLayout levelLayout = new LinearLayout(this);
        levelLayout.setOrientation(LinearLayout.VERTICAL);
        levelLayout.setBackgroundResource(R.drawable.buttons_bck);
        final LinearLayout topLayout = new LinearLayout(this);
        topLayout.setOrientation(LinearLayout.VERTICAL);
        final LinearLayout belowTopLayout = new LinearLayout(this);
        belowTopLayout.setOrientation(LinearLayout.VERTICAL);
        final LinearLayout playAgainBonusLayout = new LinearLayout(this);
        playAgainBonusLayout.setOrientation(LinearLayout.VERTICAL);
        if(!isUnlockSingleFriendzy)
            belowTopLayout.setBackgroundResource(R.drawable.buttons_orange_bck);

        for(int levelIndex = 0 ; levelIndex <= 30 ; levelIndex ++ ){
            View view = LayoutInflater.from(this).inflate(R.layout.single_friendzy_level_layout,
                    null);
            Button btnPlayLevel = (Button) view.findViewById(R.id.btnPlayLevel);
            Button btnLevel = (Button) view.findViewById(R.id.btnLevel);
            ImageView imgLock = (ImageView) view.findViewById(R.id.imgLock);

            if(levelIndex == 0){
                btnLevel.setText(levelText);
                btnLevel.setBackgroundResource(R.drawable.sf_level);
                btnLevel.getLayoutParams().height = (int) getResources()
                        .getDimension(R.dimen.play_level_first_index_button_height);
                btnLevel.getLayoutParams().width = (int) getResources()
                        .getDimension(R.dimen.play_level_first_index_button_width);
                btnLevel.requestLayout();
                topLayout.addView(view);
            }else if( levelIndex > 0 && levelIndex < 4){
                btnLevel.setText("" + (((MAX_LEVEL_SHOW * levelStartingIndex) + (levelIndex + 1)) - 1));
                topLayout.addView(view);
            }else{
                if( levelIndex == 4 && (!isUnlockSingleFriendzy)){
                    imgLock.setVisibility(ImageView.VISIBLE);
                }
                btnLevel.setText("" + (((MAX_LEVEL_SHOW * levelStartingIndex) + (levelIndex + 1)) - 1));
                belowTopLayout.addView(view);
            }

            if(numberOfCompleleve == 1 && levelIndex == 1){
                btnLevel.setBackgroundResource(R.drawable.tab_sf_green_ipad);
            }else{
                if( levelIndex != 0  && levelIndex <= numberOfCompleleve){
                    btnLevel.setBackgroundResource(R.drawable.tab_sf_green_ipad);
                }
            }

            btnPlayLevel.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    clickOnLevel(completeLevel);
                }
            });

            btnLevel.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    for(int i = 0 ; i < topLayout.getChildCount() ; i ++ ){
                        View view = topLayout.getChildAt(i);
                        if((Button)v == ((Button)view.
                                findViewById(R.id.btnLevel))){
                            clickOnLevel(i);
                            return;
                        }
                    }

                    for(int i = 0 ; i < belowTopLayout.getChildCount() ; i ++ ){
                        View view = belowTopLayout.getChildAt(i);
                        if((Button)v == ((Button)view.
                                findViewById(R.id.btnLevel))){
                            clickOnLevel(topLayout.getChildCount() + i);
                            return ;
                        }
                    }
                }
            });
        }


        for(int i = 0 ; i <= 30 ; i ++ ){
            View view = LayoutInflater.from(this)
                    .inflate(R.layout.single_friendzy_play_again_bonus_layout, null);
            Button btnPlayLevel = (Button) view.findViewById(R.id.btnPlayLevel);
            if(i  == 0){
                btnPlayLevel.getLayoutParams().height = (int) getResources()
                        .getDimension(R.dimen.play_level_first_index_button_height);
                btnPlayLevel.getLayoutParams().width = (int) getResources()
                        .getDimension(R.dimen.play_level_first_index_button_width);
                btnPlayLevel.requestLayout();
            }

            if(i == numberOfCompleleve){
                btnPlayLevel.setText(playAgainLevel);
                btnPlayLevel.setVisibility(Button.VISIBLE);
                btnPlayLevel.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        clickOnLevel(completeLevel);
                    }
                });
            }else if( i > 0 && i % 10 == 0){
                btnPlayLevel.setBackgroundResource(R.drawable.tab_sf_bonus_ipad);
                btnPlayLevel.setText(bonusCoinsText);
                btnPlayLevel.setVisibility(Button.VISIBLE);
            }
            playAgainBonusLayout.addView(view);

        }

        levelLayout.addView(topLayout);
        levelLayout.addView(belowTopLayout);


        mainLayout.addView(playAgainBonusLayout);
        mainLayout.addView(levelLayout);
        lnlevelLayout.addView(mainLayout);

		if(SINGLE_FRIENDZY_MAIN)
			Log.e(TAG, "outside createLevelLayout()");
	}


	/**
	 * This method call when click on level in single friendzy
	 * @param level
	 */
	private void clickOnLevel(final int level)
	{
		//set level for playing at single friendzy equation solve screen
		SingleFriendzyEquationSolve.completeLevel = level;
		//changes for school curriculum pop up in Dec29 sheet.
		SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
		if(!CommonUtils.isClickedSolveEquation){
			if(!sheredPreference.getBoolean(IS_LOGIN, false)){
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				DialogGenerator dg = new DialogGenerator(this);
				dg.generateRegisterOrLogInDialog(transeletion
                        .getTranselationTextByTextIdentifier("alertMsgYouMustRegisterLoginToAccessThisFeature"));
				transeletion.closeConnection();	
			}else{
				/*SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
				if(CommonUtils.isInternetConnectionAvailable(this)){
					new GetRequiredCoinsForPurchaseItemForSchoolSubscription(level).execute(null,null,null);
				}else{
					//changes on 24 Nov 2014
					if(this.getApplicationUnLockStatus
							(100, sharedPreffPlayerInfo.getString("userId", "")) == 1){
						this.showPopUpForWarningForPlayWrongLevel(level);
					}else{ 
						CommonUtils.showInternetDialog(this);
					}
				}*/
                this.clickOnSchoolCurriculumLockLevel(level);
			}
		}else{
			if( level < 4){
				this.showPopUpForWarningForPlayWrongLevel(level);
			}
			else{
				if(isUnlockSingleFriendzy){
					this.showPopUpForWarningForPlayWrongLevel(level);
				}
				else{
					//this.getRequiredCoinsForPurchase();
                    this.clickOnPracticeSkillLockLevel(level);
				}
			}
		}
	}


	/**
	 * This method call when user click on the level, which is not open for him
	 */
	private void showPopUpForWarningForPlayWrongLevel(int level)
	{
		if(level <= completeLevel)
		{
			if(CommonUtils.isInternetConnectionAvailable(this)){		
				startActivity(new Intent(this,ChooseAchallengerActivity.class));
			}
			else{
				if(CommonUtils.isClickedSolveEquation)
					this.generateIntenetWarningDialog();
				else{
					//Log.e(TAG, "inside showPopUpForWarningForPlayWrongLevel not Internet connected!");
					//only this line open before 24 11 2014 CommonUtils.showInternetDialog(this);

					//updated on 24 11 2014
					MathFriendzyHelper.generateWarningDialogToPlayWithComputer
					(this, internetNoConnectedMsg, playWithComputerMsg, noThanksText, okText, 
							new YesNoListenerInterface() {

						@Override
						public void onYes() {

							ChallengerTransferObj challengerObj = new ChallengerTransferObj();
							challengerObj.setFirst("Computer");
							challengerObj.setLast("Player");
							challengerObj.setIsFakePlayer(1);
							challengerObj.setPlayerId("");
							challengerObj.setUserId("");
							challengerObj.setSum("");
							challengerObj.setCountryIso("");
							ChooseChallengerAdapter.challengerDataObj = challengerObj;

							if(CommonUtils.getUserLanguageCode(SingleFriendzyMain.this)
									== CommonUtils.ENGLISH){
								if(MathFriendzyHelper.isWordQuestionLoaded
										(SingleFriendzyMain.this, playerGrade)){
									startWordPlayActivityWhenPlayWithComputer();
								}else{
									CommonUtils.showInternetDialog(SingleFriendzyMain.this);
								}
							}else if(CommonUtils.getUserLanguageCode(SingleFriendzyMain.this)
									== CommonUtils.SPANISH){
								if(MathFriendzyHelper.isWordQuestionForSpanishLoaded
										(SingleFriendzyMain.this, playerGrade)){
									startWordPlayActivityWhenPlayWithComputer();
								}else{
									CommonUtils.showInternetDialog(SingleFriendzyMain.this);
								}
							}
						}

						@Override
						public void onNo() {

						}
					});
				}
			}
		}
		else
		{
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			DialogGenerator dg = new DialogGenerator(this);
			dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgCompeteThePreviousLevelToUnlockThis"));
			transeletion.closeConnection();
		}
	}

	/**
	 * Start the School Curriculum Play Activity when play with computer
	 */
	private void startWordPlayActivityWhenPlayWithComputer(){
		Intent intent = new Intent(this , 
				SingleFriendzySchoolCurriculumEquationSolve.class);
		intent.putExtra("playerSelectedGrade", playerGrade);
		startActivity(intent);
	}


	/**
	 * This method get coins from server
	 */
	private void getRequiredCoinsForPurchase()
	{
		String api = null;
		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);

		TempPlayerOperation tempPlayer = new TempPlayerOperation(this);
		if(tempPlayer.isTempPlayerDeleted())
		{
			api = "itemId=" + UNLOCK_ITEM_ID + "&userId="+sharedPreffPlayerInfo.getString("userId", "")
					+"&playerId="+sharedPreffPlayerInfo.getString("playerId", "");
		}
		else
		{
			api = "itemId=" + UNLOCK_ITEM_ID;
		}

		if(CommonUtils.isInternetConnectionAvailable(this))
		{
			new GetRequiredCoinsForPurchaseItem(api).execute(null,null,null);
		}
		else
		{
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			DialogGenerator dg = new DialogGenerator(this);
			dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
			transeletion.closeConnection();
		}
	}

	/**This method generate warming dialog when intenet connection is not available
	 * 
	 */
	private void generateIntenetWarningDialog()
	{
		DialogGenerator dg = new DialogGenerator(this);
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		dg.generateWarningDialogForSingleFriendzy(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
		transeletion.closeConnection();
	}

	/**
	 * This method set the play level Button position based on the complete level
	 */
	private void setPlayLevelPosition()
	{           
		for( int i = 0 ; i < completeLevel - (levelStartingIndex * MAX_LEVEL_SHOW); i ++ )
		{
			if(isTab)
				btnList.get(i).setBackgroundResource(R.drawable.tab_sf_green_ipad);
			else
				btnList.get(i).setBackgroundResource(R.drawable.sf_green);
		}

		RelativeLayout.LayoutParams layoutParamForPlayLevelButton = new RelativeLayout.LayoutParams(playLevelbuttonWidth,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		layoutParamForPlayLevelButton.addRule(RelativeLayout.BELOW , R.id.layoutNowPlaying);
		layoutParamForPlayLevelButton.addRule(RelativeLayout.LEFT_OF , R.id.levelLayout);

		int margintop = playLevelButtonMarginHieght + ((completeLevel % MAX_LEVEL_SHOW - 1) * marginGap);
		layoutParamForPlayLevelButton.setMargins(0, margintop, 0, 0);

		btnLevelPlay.setLayoutParams(layoutParamForPlayLevelButton);
	}


	@Override
	public void onClick(View v) 
	{	
		switch(v.getId())
		{
		case R.id.btnTop100 :
			SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
			if(!sheredPreference.getBoolean(IS_LOGIN, false))
			{
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				DialogGenerator dg = new DialogGenerator(this);
				dg.generateRegisterOrLogInDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouMustLoginOrRegisterToViewAndParticipate"));
				transeletion.closeConnection();	
			}
			else
			{
				startActivity(new Intent(this,Top100Activity.class));
			}
			break;
		case R.id.btnLevelPlay :
			this.clickOnLevel(completeLevel);
			break;
		case R.id.imgSolveEquation :
			CommonUtils.isClickedSolveEquation = true; 
			this.setChakedImage();
			break;
		case R.id.imgSchoolCurriculum :
			CommonUtils.isClickedSolveEquation = false;
			this.setChakedImage();
			/*DialogGenerator dg = new DialogGenerator(this);
			dg.generateWarningDialog("Coming Soon!");*/
			break;
		}
	}

	/**
	 * This asyncTask set image from facebook url to the Button 
	 * @author Yashwant Singh
	 *
	 */
	class FacebookImageLoaderTask extends AsyncTask<Void, Void, Void>
	{
		private String strUrl = null;
		public FacebookImageLoaderTask(String strUrl)
		{
			this.strUrl = strUrl;
		}

		@Override
		protected Void doInBackground(Void... params) 
		{				
			URL img_value;
			try 
			{
				img_value = new URL(strUrl);
				profileImageBitmap = BitmapFactory.decodeStream(img_value.openConnection().getInputStream());
			} 
			catch (MalformedURLException e) 
			{			
				e.printStackTrace();
				Log.e("EditActivity", "Problem in setting image" + e);
			}
			catch(Exception ee)
			{
				Log.e("EditActivity", "Problem in setting image" + ee);
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			imgPlayer.setImageBitmap(profileImageBitmap);
			imgPlayer.invalidate();
			super.onPostExecute(result);
		}
	}

	@Override
	public void onBackPressed() 
	{
		startActivity(new Intent(this,MainActivity.class));
		super.onBackPressed();
	}

	/**
	 * This class get coins from server
	 * @author Yashwant Singh
	 *
	 */
	class GetRequiredCoinsForPurchaseItem extends AsyncTask<Void, Void, Void>
	{
		private String apiString = null;
		private CoinsFromServerObj coindFromServer;
		private ProgressDialog pg = null;

		GetRequiredCoinsForPurchaseItem(String apiValue)
		{
			this.apiString = apiValue;
		}

		@Override
		protected void onPreExecute() 
		{
			pg = CommonUtils.getProgressDialog(SingleFriendzyMain.this);
			pg.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			LearningCenteServerOperation learnignCenterOpr = new LearningCenteServerOperation();
			coindFromServer = learnignCenterOpr.getRequiredCoisForPurchase(apiString);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{			
			pg.cancel();

			if(coindFromServer != null){
				/*if(coindFromServer.getCoinsEarned() == -1 || coindFromServer.getCoinsEarned() >= coindFromServer.getCoinsRequired() 
					|| coindFromServer.getCoinsPurchase() == 0)
			{*/
				if(coindFromServer.getCoinsEarned() == -1)
				{
					LearningCenterimpl learnignCenterObj = new LearningCenterimpl(SingleFriendzyMain.this);
					learnignCenterObj.openConn();
					DialogGenerator dg = new DialogGenerator(SingleFriendzyMain.this);
					dg.generateLevelWarningDialogForLearnignCenter(learnignCenterObj.getDataFromPlayerTotalPoints("0").getCoins() ,
							coindFromServer , UNLOCK_ITEM_ID , null);
					learnignCenterObj.closeConn();
				}
				else
				{

					DialogGenerator dg = new DialogGenerator(SingleFriendzyMain.this);
					dg.generateLevelWarningDialogForLearnignCenter(coindFromServer.getCoinsEarned() ,
							coindFromServer , UNLOCK_ITEM_ID , null);
				}
				/*}
			else
			{
				DialogGenerator dg = new DialogGenerator(SingleFriendzyMain.this);
				dg.generateDialogForNeedCoinsForUlock(coindFromServer.getCoinsEarned() ,
						coindFromServer.getCoinsRequired());
			}*/
			}else{
				CommonUtils.showInternetDialog(SingleFriendzyMain.this);
			}
			super.onPostExecute(result);
		}
	}

	/**
	 * This class get single friendzy define time for word problem  
	 * @author Yashwant Singh
	 *
	 */
	private class GetTimeDefinedForSingleFriendzy extends AsyncTask<Void, Void, SingleFriendzyTimeTransObj>{
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected SingleFriendzyTimeTransObj doInBackground(Void... params) {
			SingleFriendzyServerOperationForWordProblem serverObj = new SingleFriendzyServerOperationForWordProblem();
			SingleFriendzyTimeTransObj singleFriemdzyTime =  serverObj.getSingleFriendzyDefineTime();
			return singleFriemdzyTime;
		}

		@Override
		protected void onPostExecute(SingleFriendzyTimeTransObj singleFriemdzyTime) {

			if(singleFriemdzyTime != null){
				Date date = new Date();
				SharedPreferences learnCenterTimePreff = getSharedPreferences(ICommonUtils.SINGLE_FRIENDZY_TIME_PREFF, 0);
				SharedPreferences.Editor editor = learnCenterTimePreff.edit();
				editor.putLong(ICommonUtils.SINGLE_FRIENDZY_TIME, singleFriemdzyTime.getRemainingTime());
				editor.putLong(ICommonUtils.SINGLE_FRIENDZY_TIME_API_CALL_TIME, date.getTime());
				editor.commit();

				SingleFriendzySchoolCurriculumImpl singleFrirnedzyImpl = 
						new SingleFriendzySchoolCurriculumImpl(SingleFriendzyMain.this);
				singleFrirnedzyImpl.openConnection();
				singleFrirnedzyImpl.deleteFromWordSingleFriendzyTime();
				singleFrirnedzyImpl.insertIntoWordSingleFriendzyTime(singleFriemdzyTime.getTimeList());
				singleFrirnedzyImpl.closeConnection();
			}
			super.onPostExecute(singleFriemdzyTime);
		}
	}

	/**
	 * This class get coins from server
	 * @author Shilpi
	 *
	 */
	class GetRequiredCoinsForPurchaseItemForSchoolSubscription extends AsyncTask<Void, Void, Void>
	{
		private String apiString = null;
		private CoinsFromServerObj coindFromServer;
		private ProgressDialog pg = null;
		String userId;
		String playerId;
		private int level;

		GetRequiredCoinsForPurchaseItemForSchoolSubscription(int level)
		{   
			this.level = level;
			SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(IS_CHECKED_PREFF, 0);
			userId   = sharedPreffPlayerInfo.getString("userId", "");
			playerId = sharedPreffPlayerInfo.getString("playerId", "");

			apiString = "userId="+userId+"&playerId="+playerId;
		}

		@Override
		protected void onPreExecute() 
		{
			pg = CommonUtils.getProgressDialog(SingleFriendzyMain.this);
			pg.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			LearningCenteServerOperation learnignCenterOpr = new LearningCenteServerOperation();
			coindFromServer = learnignCenterOpr.getSubscriptionInfoForUser(apiString);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{   
			pg.cancel(); 

			if(coindFromServer != null){
				//update app status in local database
				if(coindFromServer.getAppUnlock() != -1){
					ArrayList<PurchaseItemObj> purchaseItem = new ArrayList<PurchaseItemObj>();
					PurchaseItemObj purchseObj = new PurchaseItemObj();
					purchseObj.setUserId(userId);
					purchseObj.setItemId(100);
					purchseObj.setStatus(coindFromServer.getAppUnlock());
					purchaseItem.add(purchseObj);

					LearningCenterimpl learningCenter = new LearningCenterimpl(SingleFriendzyMain.this);
					learningCenter.openConn();

					learningCenter.deleteFromPurchaseItem(userId , 100);
					learningCenter.insertIntoPurchaseItem(purchaseItem);		
					learningCenter.closeConn();
				}

				if(coindFromServer.getAppUnlock() == 1)	{					
					showPopUpForWarningForPlayWrongLevel(level);
				}
				else{				
					DialogGenerator dg = new DialogGenerator(SingleFriendzyMain.this);
					dg.generateDailogForSchoolCurriculumUnlock(coindFromServer , userId , playerId
							, coindFromServer.getAppUnlock());
				} 
			}else{
				CommonUtils.showInternetDialog(SingleFriendzyMain.this);
			}
			super.onPostExecute(result);
		}
	}

	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}

    private void setWidgetsReferenceForNewApps(){
        layoutCheckBox = (RelativeLayout) findViewById(R.id.layoutCheckBox);
    }

    private void setVisibilityOfLayoutBasedOnAppVersion() {
        if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS){
            layoutCheckBox.setVisibility(RelativeLayout.VISIBLE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            switch (requestCode){
                case GetMoreCoins.START_GET_MORE_COIN_ACTIVITY_REQUEST:
                    this.updateUIAfterPurchaseSuccess();
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void updateUIAfterPurchaseSuccess() {
        this.onCreate(null);
    }

    private Activity getCurrentActivityObj(){
        return this;
    }


    //New InApp Changes
    private void clickOnPracticeSkillLockLevel(final int level){
        if(this.isUnlockForSchoolSubscription(MathFriendzyHelper.APP_UNLOCK_ID)){
            updateUIAfterPurchaseSuccess();
            return ;
        }

        if(!CommonUtils.isInternetConnectionAvailable(this)){
            CommonUtils.showInternetDialog(this);
            return ;
        }

        MathFriendzyHelper.showProgressDialog(progressDialogForNewInApp);
        MathFriendzyHelper.getAppUnlockStatusFromServerAndSaveIntoLocal(this ,
                CommonUtils.getUserId(this) , false , new HttpResponseInterface() {
                    @Override
                    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                        if(requestCode == MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE){
                            MathFriendzyHelper.hideProgressDialog(progressDialogForNewInApp);
                            return ;
                        }
                        GetAppUnlockStatusResponse response =
                                (GetAppUnlockStatusResponse) httpResponseBase;
                        if(response.getResult()
                                .equalsIgnoreCase(MathFriendzyHelper.SUCCESS)
                                && response.getAppUnlockStatus() == MathFriendzyHelper.APP_UNLOCK){
                            MathFriendzyHelper.hideProgressDialog(progressDialogForNewInApp);
                            //showPopUpForWarningForPlayWrongLevel(level);
                            updateUIAfterPurchaseSuccess();
                            return ;
                        }

                        MathFriendzyHelper.getInAppStatusAndShowDialog(getCurrentActivityObj(),
                                MathFriendzyHelper.SHOW_UNLOCK_FOR_SINGLE_FRIENDZY ,
                                new HttpResponseInterface() {//interface call when save true from the server otherwise open dialog
                                    @Override
                                    public void serverResponse(HttpResponseBase httpResponseBase,
                                                               int requestCode) {
                                        MathFriendzyHelper.hideProgressDialog(progressDialogForNewInApp);
                                        if(requestCode ==
                                                MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE){
                                            return ;
                                        }
                                        //showPopUpForWarningForPlayWrongLevel(level);
                                        updateUIAfterPurchaseSuccess();
                                    }
                                } , false , response , new OnPurchaseDone() {
                                    @Override
                                    public void onPurchaseDone(boolean isDone) {
                                        if(isDone) {
                                            updateUIAfterPurchaseSuccess();
                                        }
                                    }
                                });
                    }
                });

        /*MathFriendzyHelper.showProgressDialog(progressDialogForNewInApp);
        MathFriendzyHelper.getInAppStatusAndShowDialog(getCurrentActivityObj(),
                MathFriendzyHelper.SHOW_UNLOCK_FOR_SINGLE_FRIENDZY ,
                new HttpResponseInterface() {//interface call when save true from the server otherwise open dialog
                    @Override
                    public void serverResponse(HttpResponseBase httpResponseBase,
                                               int requestCode) {
                        MathFriendzyHelper.hideProgressDialog(progressDialogForNewInApp);
                        if(requestCode ==
                                MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE){
                            return ;
                        }
                        updateUIAfterPurchaseSuccess();
                    }
                } , false);*/
    }

    private void clickOnSchoolCurriculumLockLevel(final int level){
        if(this.isUnlockForSchoolSubscription(MathFriendzyHelper.APP_UNLOCK_ID)){
            this.showPopUpForWarningForPlayWrongLevel(level);
            return ;
        }

        if(!CommonUtils.isInternetConnectionAvailable(this)){
            CommonUtils.showInternetDialog(this);
            return ;
        }

        MathFriendzyHelper.showProgressDialog(progressDialogForNewInApp);
        MathFriendzyHelper.getAppUnlockStatusFromServerAndSaveIntoLocal(this ,
                CommonUtils.getUserId(this) , false , new HttpResponseInterface() {
                    @Override
                    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                        if(requestCode == MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE){
                            MathFriendzyHelper.hideProgressDialog(progressDialogForNewInApp);
                            return ;
                        }
                        GetAppUnlockStatusResponse response =
                                (GetAppUnlockStatusResponse) httpResponseBase;
                        if(response.getResult()
                                .equalsIgnoreCase(MathFriendzyHelper.SUCCESS)
                                && response.getAppUnlockStatus() == MathFriendzyHelper.APP_UNLOCK){
                            MathFriendzyHelper.hideProgressDialog(progressDialogForNewInApp);
                            //showPopUpForWarningForPlayWrongLevel(level);
                            updateUIAfterPurchaseSuccess();
                            return ;
                        }

                        MathFriendzyHelper.getInAppStatusAndShowDialog(getCurrentActivityObj(),
                                MathFriendzyHelper.SHOW_UNLOCK_FOR_SINGLE_FRIENDZY ,
                                new HttpResponseInterface() {//interface call when save true from the server otherwise open dialog
                                    @Override
                                    public void serverResponse(HttpResponseBase httpResponseBase,
                                                               int requestCode) {
                                        MathFriendzyHelper.hideProgressDialog(progressDialogForNewInApp);
                                        if(requestCode ==
                                                MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE){
                                            return ;
                                        }
                                        //showPopUpForWarningForPlayWrongLevel(level);
                                        updateUIAfterPurchaseSuccess();
                                    }
                                } , false , response , new OnPurchaseDone() {
                            @Override
                            public void onPurchaseDone(boolean isDone) {
                                if(isDone) {
                                    updateUIAfterPurchaseSuccess();
                                }
                            }
                        });
                    }
                });
    }
}