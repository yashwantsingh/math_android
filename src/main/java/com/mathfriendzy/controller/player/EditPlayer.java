package com.mathfriendzy.controller.player;

import static com.mathfriendzy.utils.ICommonUtils.PLAYER_ACTIVITY_LOG;
import static com.mathfriendzy.utils.ICommonUtils.IS_FACEBOOK_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN_FROM_FACEBOOK;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.mathfriendzy.R;
import com.mathfriendzy.controller.chooseavtar.ChooseAvtars;
import com.mathfriendzy.controller.school.SearchTeacherActivity;
import com.mathfriendzy.controller.school.SearchYourSchoolActivity;
import com.mathfriendzy.facebookconnect.FacebookConnect;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.player.temp.TempPlayer;
import com.mathfriendzy.model.player.temp.TempPlayerOperation;
import com.mathfriendzy.model.states.States;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.Validation;

/**
 * This Activity is open when user want to edit temp player
 * @author Yashwant Singh
 *
 */
public class EditPlayer extends BasePlayer implements OnClickListener
{	
	private Button btnSavePlayer  = null;
	private Button btnDeletePlayer= null; 
	private Button   btnTitleBack = null;
	private TextView lblTransferYourInfoFromFacebookAndFindFriends = null;

	private ArrayList<TempPlayer> tempPlayerList= null;
	public static String schoolName				= null;

	//public static Bitmap profileImageBitmap    	= null;
	//public static String imageName				= null;

	private SharedPreferences sharedPreference 	= null;
	private SharedPreferences facebookPreference= null;

	private ProgressDialog progressDialog 		= null;
	private String schoolIdFromFind  			= null;

	private  ArrayList<String> teacherInfo 		= null;
	private String teacherFirstName 			= null;
	private String teacherLastName  			= null;
	private String teacherId        			= null;

	private boolean isChekedUser    = false;
	private boolean isSetState      = false;
	private boolean isClickOnSchool = false;

	public static boolean IS_REGISTER_SCHOOL	= false;

	private String TAG = this.getClass().getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_or_edit_player);

		if(PLAYER_ACTIVITY_LOG)
			Log.e("EditPlayer", "inside onCreate()");

		facebookPreference = getSharedPreferences(IS_FACEBOOK_LOGIN, 0);

		this.setWidgetsReferences();
		this.setWidgetsTextValue();
		this.getTempPlayerData();
		this.setWidgetsValues();
		this.getCountries(this,tempPlayerList.get(0).getCountry());
		this.getGrade(this,String.valueOf(tempPlayerList.get(0).getGrade()));
		this.setListenerOnWidgets();

		if(PLAYER_ACTIVITY_LOG)
			Log.e("EditPlayer", "outside onCreate()");
	}

	/**
	 * This method set the widgets references from the layout to the widgets objects
	 */
	private void setWidgetsReferences()
	{
		if(PLAYER_ACTIVITY_LOG)
			Log.e("EditPlayer", "inside setWidgetsReferences()");

		edtFirstName 	= (EditText) findViewById(R.id.edtFirstName);
		edtLastName  	= (EditText) findViewById(R.id.edtLastName);
		edtUserName  	= (EditText) findViewById(R.id.edtUserName);
		edtCity 	 	= (EditText) findViewById(R.id.edtCity);
		edtZipCode 	 	= (EditText) findViewById(R.id.edtZipCode);
		spinnerCountry	= (Spinner) findViewById(R.id.spinnerCountry);
		spinnerState 	= (Spinner) findViewById(R.id.spinnerState);
		spinnerGrade 	= (Spinner) findViewById(R.id.spinnerGrade);
		edtSchool 		= (EditText) findViewById(R.id.spinnerSchool);//changes
		edtTeacher      = (EditText) findViewById(R.id.spinnerTeacher);//changes

		lblRegState     = (TextView)findViewById(R.id.lblRegState);
		btnSavePlayer   = (Button) findViewById(R.id.btnTitleSave);
		btnDeletePlayer = (Button) findViewById(R.id.btnTitleDelete);
		imgAvtar        = (ImageView) findViewById(R.id.imgAvtar);

		mfTitleHomeScreen 	= (TextView) findViewById(R.id.mfTitleHomeScreen);
		lblEditPlayerTitle 	= (TextView) findViewById(R.id.lblEditPlayerTitle);
		lblFbConnect 		= (Button) findViewById(R.id.lblFbConnect);
		lblFirstName 		=  (TextView) findViewById(R.id.lblFirstName);
		lblLastName 		= (TextView) findViewById(R.id.lblLastName);
		lblUserName 		= (TextView) findViewById(R.id.lblUserName);
		lblRegCountry 		= (TextView) findViewById(R.id.lblRegCountry);
		lblRegCity 			= (TextView) findViewById(R.id.lblRegCity);
		lblRegZip 			= (TextView) findViewById(R.id.lblRegZip);
		pickerTitleSchool   = (TextView) findViewById(R.id.pickerTitleSchool);
		lblAddPlayerGrade 	= (TextView) findViewById(R.id.lblAddPlayerGrade);
		lblRegTeacher 		= (TextView) findViewById(R.id.lblRegTeacher);
		lblRegState			= (TextView) findViewById(R.id.lblRegState);
		lblTransferYourInfoFromFacebookAndFindFriends = (TextView) findViewById(R.id.lblTransferYourInfoFromFacebookAndFindFriends);

		lblChooseAnAvatar 	= (TextView) findViewById(R.id.lblChooseAnAvatar);
		avtarLayout         = (RelativeLayout) findViewById(R.id.avtarLayout);

		btnTitleBack        = (Button)   findViewById(R.id.btnTitleBack);

		//set Done button to keyboard
		CommonUtils.setDoneButtonToEditText(edtFirstName);
		CommonUtils.setDoneButtonToEditText(edtLastName);
		CommonUtils.setDoneButtonToEditText(edtUserName);
		CommonUtils.setDoneButtonToEditText(edtCity);
		CommonUtils.setDoneButtonToEditText(edtZipCode);
		//end changes

		if(PLAYER_ACTIVITY_LOG)
			Log.e("EditPlayer", "outside setWidgetsReferences()");
	}

	/**
	 * This method set the widgets text values from the translation
	 */
	private void setWidgetsTextValue()
	{
		if(PLAYER_ACTIVITY_LOG)
			Log.e("EditPlayer", "inside setWidgetsTextValue()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		mfTitleHomeScreen.setText(transeletion.getTranselationTextByTextIdentifier("mfTitleHomeScreen"));
		lblEditPlayerTitle.setText(transeletion.getTranselationTextByTextIdentifier("lblEditPlayerTitle") + " :");
		lblFirstName.setText(transeletion.getTranselationTextByTextIdentifier("lblFirstName") + " :");
		lblLastName.setText(transeletion.getTranselationTextByTextIdentifier("lblLastName") + " :");
		lblUserName.setText(transeletion.getTranselationTextByTextIdentifier("lblUserName") + " :");
		lblRegCountry.setText(transeletion.getTranselationTextByTextIdentifier("lblRegCountry") + " :");
		lblRegState.setText(transeletion.getTranselationTextByTextIdentifier("lblRegState") + " :");
		lblRegCity.setText(transeletion.getTranselationTextByTextIdentifier("lblRegCity") + " :");
		lblRegZip.setText(transeletion.getTranselationTextByTextIdentifier("lblRegZip") + " :");
		pickerTitleSchool.setText(transeletion.getTranselationTextByTextIdentifier("pickerTitleSchool") + " :");
		lblAddPlayerGrade.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade") + " :");
		lblRegTeacher.setText(transeletion.getTranselationTextByTextIdentifier("lblRegTeacher") + " :");
		lblChooseAnAvatar.setText(transeletion.getTranselationTextByTextIdentifier("lblChooseAnAvatar"));
		btnSavePlayer.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleSave"));
		btnDeletePlayer.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleDelete"));

		if(!facebookPreference.getBoolean(IS_LOGIN_FROM_FACEBOOK, false))
		{
			lblFbConnect.setText(transeletion.getTranselationTextByTextIdentifier("lblFbConnect"));
			lblTransferYourInfoFromFacebookAndFindFriends.setText(transeletion.getTranselationTextByTextIdentifier
					("lblTransferYourInfoFromFacebookAndFindFriends"));
		}
		else
		{
			lblFbConnect.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleLogOut"));
			lblTransferYourInfoFromFacebookAndFindFriends.setText(transeletion.getTranselationTextByTextIdentifier
					("lblDisconnectFromFb"));
		}


		btnTitleBack.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBack"));
		transeletion.closeConnection();

		if(PLAYER_ACTIVITY_LOG)
			Log.e("EditPlayer", "outside setWidgetsTextValue()");
	}

	/**
	 * This method get temp player data from database
	 */
	private void getTempPlayerData()
	{
		if(PLAYER_ACTIVITY_LOG)
			Log.e("EditPlayer", "inside getTempPlayerData()");

		TempPlayerOperation tempPlayerObj = new TempPlayerOperation(this);
		tempPlayerList = tempPlayerObj.getTempPlayerData();

		if(PLAYER_ACTIVITY_LOG)
			Log.e("EditPlayer", "outside getTempPlayerData()");
	}

	/**
	 * This method set the widgets value to the widgets from database
	 */
	private void setWidgetsValues()
	{

		if(PLAYER_ACTIVITY_LOG)
			Log.e("EditPlayer", "inside setWidgetsValues()");

		edtFirstName.setText(tempPlayerList.get(0).getFirstName());
		edtLastName.setText(tempPlayerList.get(0).getLastName());
		edtUserName.setText(tempPlayerList.get(0).getUserName());
		edtCity.setText(tempPlayerList.get(0).getCity());
		edtZipCode.setText(tempPlayerList.get(0).getZipCode());
		edtSchool.setText(tempPlayerList.get(0).getSchoolName());

		if(tempPlayerList.get(0).getSchoolName().equals("Home School"))
		{
			edtTeacher.setEnabled(false);
		}

		edtTeacher.setText(tempPlayerList.get(0).getTeacherFirstName() + " " + tempPlayerList.get(0).getTeacherLastName());
		schoolIdFromFind = String.valueOf(tempPlayerList.get(0).getSchoolId());

		teacherFirstName = tempPlayerList.get(0).getTeacherFirstName();
		teacherLastName  = tempPlayerList.get(0).getTeacherLastName();
		teacherId        = String.valueOf(tempPlayerList.get(0).getTeacherUserId());

		imageName = tempPlayerList.get(0).getProfileImageName();

		sharedPreference = getSharedPreferences("myPreff", 0);
		SharedPreferences.Editor editor = sharedPreference.edit();
		editor.putString("userName", tempPlayerList.get(0).getUserName());
		editor.putString("firstName",tempPlayerList.get(0).getFirstName());
		editor.putString("lastName",tempPlayerList.get(0).getLastName());
		editor.commit();

		byte[] blob = tempPlayerList.get(0).getProfileImage();
		//profileImageBitmap = BitmapFactory.decodeByteArray(blob,0,blob.length);
		//profileImageBitmap = CommonUtils.getBitmapFromByte(blob, this);
		profileImageBitmap = CommonUtils.getBitmapFromByte(blob);
		imgAvtar.setImageBitmap(profileImageBitmap);

		if(PLAYER_ACTIVITY_LOG)
			Log.e("EditPlayer", "outside setWidgetsValues()");
	}


	/**
	 * This method set the listener on widgets
	 */
	private void setListenerOnWidgets()
	{
		if(PLAYER_ACTIVITY_LOG)
			Log.e(TAG, "inside setListenetOnWidgets()");

		spinnerCountry.setOnItemSelectedListener(new OnItemSelectedListener() 
		{
			@Override
			public void onItemSelected(AdapterView<?> arg0, View view,int arg2, long arg3) 
			{				
				if(spinnerCountry.getSelectedItem().toString().equals("United States"))
				{
					spinnerState.setVisibility(Spinner.VISIBLE);
					lblRegState.setVisibility(TextView.VISIBLE);
					lblRegZip.setTextColor(Color.BLACK);
					edtZipCode.setEnabled(true);

					if(!isSetState)
					{
						setStates("United States",tempPlayerList.get(0).getState());
						isSetState = true;
					}
					else
					{
						setStates("United States","");
					}

				}
				else if(spinnerCountry.getSelectedItem().toString().equals("Canada"))
				{
					edtZipCode.setEnabled(true);
					spinnerState.setVisibility(Spinner.VISIBLE);
					lblRegState.setVisibility(TextView.VISIBLE);
					lblRegZip.setTextColor(Color.BLACK);
					if(!isSetState)
					{
						setStates("United States",tempPlayerList.get(0).getState());
						isSetState = true;
					}
					else
					{
						setStates("United States","");
					}

				}
				else
				{
					edtZipCode.setText("");
					edtZipCode.setEnabled(false);
					lblRegZip.setTextColor(Color.GRAY);
					spinnerState.setVisibility(Spinner.GONE);
					lblRegState.setVisibility(TextView.GONE);
					/*getSchoolList(spinnerCountry.getSelectedItem().toString(), "", 
								edtCity.getText().toString(), edtZipCode.getText().toString());*/
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) 
			{				
			}
		});


		btnSavePlayer.setOnClickListener(this);
		btnDeletePlayer.setOnClickListener(this);
		lblFbConnect.setOnClickListener(this);
		avtarLayout.setOnClickListener(this);

		edtSchool.setOnClickListener(this);//changes
		edtTeacher.setOnClickListener(this);//changes

		btnTitleBack.setOnClickListener(this);//changes

		if(PLAYER_ACTIVITY_LOG)
			Log.e(TAG, "outside setListenetOnWidgets()");
	}

	/**
	 * This method set states when user select country either United State or Canada
	 * @param countryName
	 * @param stateName
	 */
	private void setStates(String countryName,String stateName)
	{
		if(PLAYER_ACTIVITY_LOG)
			Log.e(TAG, "inside setStates()");

		ArrayList<String> satateList = new ArrayList<String>();
		States stateObj = new States();
		if(countryName.equals("United States"))
			satateList = stateObj.getUSStates(this);
		else if(countryName.equals("Canada"))
			satateList = stateObj.getCanadaStates(this);

		//Log.e(TAG, "stateName inside setStates" + stateName);
		if(satateList != null)
		{
			ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(EditPlayer.this, android.R.layout.simple_list_item_1,satateList);
			stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinnerState.setAdapter(stateAdapter);
			spinnerState.setSelection(stateAdapter.getPosition(stateName));
			stateAdapter.notifyDataSetChanged();
		}

		if(PLAYER_ACTIVITY_LOG)
			Log.e(TAG, "outside setStates()");
	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.btnTitleSave : 
			this.getDataFromWidgets();
			break;

		case R.id.btnTitleDelete:
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			DialogGenerator dg = new DialogGenerator(this);
			dg.generateDeleteConfirmationDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgAreYouSureToDeletePlayer"));
			transeletion.closeConnection();
			break;

		case R.id.lblFbConnect:
			this.facebookConnect();
			break;
		case R.id.avtarLayout:
			Intent chooseAvtar  = new Intent(this,ChooseAvtars.class);
			startActivity(chooseAvtar);
			break;
		case R.id.spinnerSchool:
			isClickOnSchool = true;
			this.setListenerOnEdtSchool();
			break;
		case R.id.spinnerTeacher:
			Intent teacherIntent = new Intent(this,SearchTeacherActivity.class);
			teacherIntent.putExtra("schoolId", schoolIdFromFind);
			startActivityForResult(teacherIntent, 2);
			break;
		case R.id.btnTitleBack:
			Intent intentMain = new Intent(this,PlayersActivity.class);
			startActivity(intentMain);
			break;		
		}
	}


	/**
	 * This method connect with facebook and if already connect to facebook then disconnect from faebook
	 */
	private void facebookConnect()
	{
		Translation transeletion1 = new Translation(this);
		transeletion1.openConnection();		

		SharedPreferences.Editor editor = facebookPreference.edit();

		if(facebookPreference.getBoolean(IS_LOGIN_FROM_FACEBOOK, false))
		{
			FacebookConnect.logoutFromFacebook();
			editor.putBoolean(IS_LOGIN_FROM_FACEBOOK, false);
			editor.commit();
			lblFbConnect.setText(transeletion1.getTranselationTextByTextIdentifier("lblFbConnect"));
			lblTransferYourInfoFromFacebookAndFindFriends.setText(transeletion1.getTranselationTextByTextIdentifier
					("lblTransferYourInfoFromFacebookAndFindFriends"));
		}
		else
		{			
			//changes for Internet Connection
			if(CommonUtils.isInternetConnectionAvailable(this))
			{					
				Intent intent = new Intent(this,FacebookConnect.class);
				startActivity(intent);
				editor.putBoolean(IS_LOGIN_FROM_FACEBOOK, true);
				editor.commit();
				lblFbConnect.setText(transeletion1.getTranselationTextByTextIdentifier("btnTitleLogOut"));
				lblTransferYourInfoFromFacebookAndFindFriends.setText(transeletion1.getTranselationTextByTextIdentifier
						("lblDisconnectFromFb"));
			}
			else
			{
				DialogGenerator dg1 = new DialogGenerator(this);
				dg1.generateWarningDialog(transeletion1.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
			}
		}
		transeletion1.closeConnection();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
		{
			Intent intentMain = new Intent(this,PlayersActivity.class);
			startActivity(intentMain);
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * This method call when user click on the school edit text
	 */
	private void setListenerOnEdtSchool()
	{
		if(PLAYER_ACTIVITY_LOG)
			Log.e(TAG, "inside setListenerOnEdtSchool()");

		Intent schoolIntent = new Intent(this,SearchYourSchoolActivity.class);

		if(spinnerCountry.getSelectedItem().toString().equals("United States") || 
				spinnerCountry.getSelectedItem().toString().equals("Canada"))
		{	
			if(edtCity.getText().toString().equals("") ||  edtZipCode.getText().toString().equals(""))
			{
				DialogGenerator dg1 = new DialogGenerator(this);
				Translation transeletion1 = new Translation(this);
				transeletion1.openConnection();
				dg1.generateWarningDialog(transeletion1.getTranselationTextByTextIdentifier("alertMsgEnterYourLocationToContinue"));
				transeletion1.closeConnection();
			}
			else
			{	
				Country countryObj = new Country();
				String countryId = countryObj.getCountryIdByCountryName(spinnerCountry.getSelectedItem().toString(), this);

				States stateObj = new States();
				String stateId  = null;

				if(spinnerCountry.getSelectedItem().toString().equals("Canada"))
					stateId  = stateObj.getCanadaStateIdByName(spinnerState.getSelectedItem().toString(), this);
				else if(spinnerCountry.getSelectedItem().toString().equals("United States"))
					stateId  = stateObj.getUSStateIdByName(spinnerState.getSelectedItem().toString(), this);


				//changes for Internet Connection
				if(CommonUtils.isInternetConnectionAvailable(this))
				{
					new CheckValidCityAsynTask(countryId,stateId,edtCity.getText().toString(), edtZipCode.getText().toString()).execute(null,null,null);
					//execute Asynctask for check city is city and zip valid or not
				}
				else
				{
					DialogGenerator dg = new DialogGenerator(this);
					Translation transeletion = new Translation(this);
					transeletion.openConnection();
					dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
					transeletion.closeConnection();
				}

				/*schoolIntent.putExtra("country", spinnerCountry.getSelectedItem().toString());
				schoolIntent.putExtra("state", spinnerState.getSelectedItem().toString());
				schoolIntent.putExtra("city", edtCity.getText().toString());
				schoolIntent.putExtra("zip", edtZipCode.getText().toString());

				startActivityForResult(schoolIntent, 1);*/
			}
		}
		else
		{	
			schoolIntent.putExtra("country", spinnerCountry.getSelectedItem().toString());
			schoolIntent.putExtra("state", "");
			schoolIntent.putExtra("city", edtCity.getText().toString());
			schoolIntent.putExtra("zip", edtZipCode.getText().toString());

			startActivityForResult(schoolIntent, 1);
		}


		if(PLAYER_ACTIVITY_LOG)
			Log.e(TAG, "outside setListenerOnEdtSchool()");
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		if(PLAYER_ACTIVITY_LOG)
			Log.e(TAG, "inside onActivityResult()");

		if (requestCode == 1) 
		{
			if(resultCode == RESULT_OK)
			{      
				ArrayList<String> schoolInfo = data.getStringArrayListExtra("schoolInfo");
				edtSchool.setText(schoolInfo.get(0));
				schoolIdFromFind = schoolInfo.get(1);

				if(schoolInfo.get(0).equals("Home School"))
				{
					edtTeacher.setEnabled(false);
					edtTeacher.setText("N/A");
					teacherFirstName = "N/A";
					teacherLastName  = "";
					teacherId        = "-2";
				}
				else
				{
					edtTeacher.setEnabled(true);
				}
			}
		}

		if (requestCode == 2) 
		{
			if(resultCode == RESULT_OK)
			{      
				teacherInfo = data.getStringArrayListExtra("schoolInfo");
				edtTeacher.setText(teacherInfo.get(0) + " " + teacherInfo.get(1));

				teacherFirstName = teacherInfo.get(0);
				teacherLastName  = teacherInfo.get(1);
				teacherId        = teacherInfo.get(2);
			}
		}

		if(PLAYER_ACTIVITY_LOG)
			Log.e(TAG, "outside onActivityResult()");

		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * This method get the data from the widgets which is edit by the user 
	 */
	private void getDataFromWidgets()
	{
		if(PLAYER_ACTIVITY_LOG)
			Log.e(TAG, "inside getDataFromWidgets()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();

		if(spinnerCountry.getSelectedItem().toString().equals("United States") || spinnerCountry.getSelectedItem().toString().equals("Canada"))
		{
			if(edtCity.getText().toString().equals("") || edtZipCode.getText().toString().equals("") 
					|| edtFirstName.getText().toString().equals("") || edtLastName.getText().toString().equals("")
					)
			{
				DialogGenerator dg = new DialogGenerator(this);
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgPleaseEnterAllInfo"));
			}
			else
			{
				if(tempPlayerList.get(0).getUserName().equals(edtUserName.getText().toString()))
				{
					checkValidCity();
				}
				else
				{
					isChekedUser = true;
					checkValidUser(edtUserName.getText().toString());
				}
			}
		}
		else
		{
			if(edtCity.getText().toString().equals("")/* || edtZipCode.getText().toString().equals("") */
					|| edtFirstName.getText().toString().equals("") || edtLastName.getText().toString().equals("")
					)
			{
				DialogGenerator dg = new DialogGenerator(this);
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgPleaseEnterAllInfo"));
			}
			else
			{
				if(tempPlayerList.get(0).getUserName().equals(edtUserName.getText().toString()))
				{
					checkValidCity();
				}
				else
				{
					checkValidUser(edtUserName.getText().toString());
				}
			}
		}
		transeletion.closeConnection();	

		if(PLAYER_ACTIVITY_LOG)
			Log.e(TAG, "outside getDataFromWidgets()");
	}

	/**
	 * If user want to edit your user name then it will be check for valid or not 
	 * @param userName
	 */
	private void checkValidUser(String userName)
	{
		//Changes for Internet Connection
		if(CommonUtils.isInternetConnectionAvailable(this))
		{
			progressDialog = CommonUtils.getProgressDialog(EditPlayer.this);
			new CkeckValidUserAsyncTask(userName).execute(null,null,null);
		}
		else
		{
			DialogGenerator dg = new DialogGenerator(this);
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
			transeletion.closeConnection();
		}
	}

	/**
	 * This methos check city is valid or not
	 */
	private void checkValidCity()
	{
		if(PLAYER_ACTIVITY_LOG)
			Log.e(TAG, "inside checkValidCity()");

		if(spinnerCountry.getSelectedItem().toString().equals("United States") || spinnerCountry.getSelectedItem().toString().equals("Canada"))
		{
			Country countryObj = new Country();
			String countryId = countryObj.getCountryIdByCountryName(spinnerCountry.getSelectedItem().toString(), this);

			States stateObj = new States();
			String stateId  = null;

			if(spinnerCountry.getSelectedItem().toString().equals("Canada"))
				stateId  = stateObj.getCanadaStateIdByName(spinnerState.getSelectedItem().toString(), this);
			else if(spinnerCountry.getSelectedItem().toString().equals("United States"))
				stateId  = stateObj.getUSStateIdByName(spinnerState.getSelectedItem().toString(), this);
			//Changes for Internet Connection
			if(CommonUtils.isInternetConnectionAvailable(this))
			{
				new CheckValidCityAsynTask(countryId,stateId,edtCity.getText().toString(), edtZipCode.getText().toString()).execute(null,null,null);
			}
			else
			{
				DialogGenerator dg = new DialogGenerator(this);
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
				transeletion.closeConnection();
			}
		}
		else
		{
			if(progressDialog != null)
				progressDialog.cancel();

			if(saveTempPlayer())
			{
				Intent intent = new Intent(EditPlayer.this,PlayersActivity.class);
				startActivity(intent);
			}
			else
				Log.e("", "Problem in editing!");
		}

		if(PLAYER_ACTIVITY_LOG)
			Log.e(TAG, "outside checkValidCity()");
	}


	@Override
	protected void onRestart() 
	{
		this.setTextFromFacebook();// set text value and image from facebooks profile information
		/*if(IS_REGISTER_SCHOOL)
			{
				if(schoolName != null)
				{
					schoolNameList.add(schoolName);
					schoolAdapter = new ArrayAdapter<String>(EditPlayer.this, android.R.layout.simple_list_item_1, schoolNameList);
					schoolAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spinnerSchool.setAdapter(schoolAdapter);
					spinnerSchool.setSelection(schoolAdapter.getPosition(schoolName));
				}
			}*/

		super.onRestart();
	}

	/**
	 * This method seva the player data to the database
	 * @return
	 */
	private boolean saveTempPlayer()
	{
		if(PLAYER_ACTIVITY_LOG)
			Log.e(TAG, "inside saveTempPlayerData()");

		TempPlayer tempPlayerObj = new TempPlayer();

		tempPlayerObj.setCity(edtCity.getText().toString());
		tempPlayerObj.setCoins(0);
		tempPlayerObj.setCompeteLevel(1);
		tempPlayerObj.setFirstName(edtFirstName.getText().toString());
		tempPlayerObj.setGrade(Integer.parseInt(spinnerGrade.getSelectedItem().toString()));
		tempPlayerObj.setLastName(edtLastName.getText().toString());
		tempPlayerObj.setParentUserId(0);
		tempPlayerObj.setPlayerId(0);
		tempPlayerObj.setPoints(0);

		if(profileImageBitmap != null)
		{
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			profileImageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
			byte[] bitMapData = stream.toByteArray();
			tempPlayerObj.setProfileImage(bitMapData);
		}

		tempPlayerObj.setProfileImageName(imageName);
		//Log.e(TAG, "image name in save" + imageName);

		tempPlayerObj.setSchoolId(Integer.parseInt(schoolIdFromFind));

		tempPlayerObj.setSchoolName(edtSchool.getText().toString());

		tempPlayerObj.setTeacherFirstName(teacherFirstName);

		tempPlayerObj.setTeacherLastName(teacherLastName);
		tempPlayerObj.setTeacherUserId(Integer.parseInt(teacherId));
		tempPlayerObj.setUserName(edtUserName.getText().toString());
		tempPlayerObj.setZipCode(edtZipCode.getText().toString());


		if(spinnerCountry.getSelectedItem().toString().equals("United States") 
				|| spinnerCountry.getSelectedItem().toString().equals("Canada"))
		{
			tempPlayerObj.setState(spinnerState.getSelectedItem().toString());
		}
		else
			tempPlayerObj.setState("");

		tempPlayerObj.setCountry(spinnerCountry.getSelectedItem().toString());

		TempPlayerOperation tempObj = new TempPlayerOperation(this);
		tempObj.deleteFromTempPlayer();

		if(PLAYER_ACTIVITY_LOG)
			Log.e(TAG, "outside saveTempPlayerData()");

		return (tempObj.insertTempPlayer(tempPlayerObj));
	}

	/**
	 * @descritopn check for user already registered or not on server
	 * @author Yashwant Singh
	 *
	 */
	class CkeckValidUserAsyncTask extends AsyncTask<Void, Void, Void>
	{
		private String userName = null;
		private String resultValue   = null;

		public CkeckValidUserAsyncTask(String userName)
		{
			this.userName = userName;
		}

		@Override
		protected void onPreExecute()
		{
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			Validation validObj = new Validation();
			resultValue = validObj.validUser(userName);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			if(resultValue != null){
				if(resultValue.equals("0"))//O for valid ,not registered
				{	
					checkValidCity();
				}
				else if(resultValue.equals("1"))//1 for already registered
				{
					progressDialog.cancel();

					Translation transeletion = new Translation(EditPlayer.this);
					transeletion.openConnection();
					DialogGenerator dg = new DialogGenerator(EditPlayer.this);
					dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgThisUserNameAlreadyExist"));
					transeletion.closeConnection();	
				}
			}else{
				CommonUtils.showInternetDialog(EditPlayer.this);
			}
			super.onPostExecute(result);
		}
	}

	/**
	 * @dexcription check for valid city and zipcode
	 * @author Yashwant Singh
	 *
	 */
	class CheckValidCityAsynTask extends AsyncTask<Void, Void, Void>
	{
		String countryId 	= null;
		String stateId 		= null;
		String city 		= null;
		String zip 			= null;
		String resultValue  = null;

		CheckValidCityAsynTask(String countryId,String stateId,String city,String zip)
		{
			this.countryId = countryId;
			this.stateId   = stateId;
			this.city      = city;
			this.zip       = zip;
		}

		@Override
		protected void onPreExecute() 
		{
			if(!isChekedUser)
			{
				progressDialog = CommonUtils.getProgressDialog(EditPlayer.this);
				progressDialog.show();
			}
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			Validation validObj = new Validation();
			resultValue = validObj.validateCity(countryId, stateId, city, zip);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			progressDialog.cancel();


			if(resultValue != null){
				Translation transeletion = new Translation(EditPlayer.this);
				transeletion.openConnection();

				if(resultValue.equals("-9001"))//invalid city
				{
					DialogGenerator dg = new DialogGenerator(EditPlayer.this);
					dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgInvalidCity"));
				}
				else if(resultValue.equals("-9002"))//invalid zipcode
				{
					DialogGenerator dg = new DialogGenerator(EditPlayer.this);
					dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgInvalidZipCode"));
				}
				else
				{
					if(isClickOnSchool)
					{
						isClickOnSchool = false;

						Intent schoolIntent = new Intent(EditPlayer.this,SearchYourSchoolActivity.class);
						schoolIntent.putExtra("country", spinnerCountry.getSelectedItem().toString());
						schoolIntent.putExtra("state", spinnerState.getSelectedItem().toString());
						schoolIntent.putExtra("city", edtCity.getText().toString());
						schoolIntent.putExtra("zip", edtZipCode.getText().toString());

						startActivityForResult(schoolIntent, 1);
					}
					else
					{
						if(saveTempPlayer())
						{
							Intent intent = new Intent(EditPlayer.this,PlayersActivity.class);
							startActivity(intent);
						}
						else
							Log.e("", "Problem in editing!");
					}
				}
				transeletion.closeConnection();	
			}else{
				CommonUtils.showInternetDialog(EditPlayer.this);
			}
			super.onPostExecute(result);
		}
	}
	
	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}
}
