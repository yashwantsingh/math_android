package com.mathfriendzy.controller.player;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.chooseavtar.ChooseAvtars;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.registration.ModifyRegistration;
import com.mathfriendzy.controller.school.SearchYourSchoolActivity;
import com.mathfriendzy.facebookconnect.FacebookConnect;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.EditTextFocusChangeListener;
import com.mathfriendzy.model.chooseAvtar.ChooseAvtarOpration;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.Register;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.model.states.States;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.Validation;

import java.util.ArrayList;

import static com.mathfriendzy.utils.ICommonUtils.EDIT_REG_USER_PLAYER_FLAG;
import static com.mathfriendzy.utils.ICommonUtils.FACEBOOK_HOST_NAME;
import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.IS_FACEBOOK_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN_FROM_FACEBOOK;
import static com.mathfriendzy.utils.ICommonUtils.MODIFY_USER_FLAG;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_ID;
/**
 * This Activity is open when user want to edit the player if it is registered
 * or login
 * @author Yashwant Singh
 *
 */
public class EditRegisteredUserPlayer extends BasePlayer implements OnClickListener, EditTextFocusChangeListener
{
    private Button   btnSavePlayer  	= null;
    private Button   btnDeletePlayer	= null;
    private TextView lblTransferYourInfoFromFacebookAndFindFriends  = null;

    public static String schoolName		= null;

    public static String playerId 		= null;
    private String userId               = null;
    private UserPlayerDto playerData 	= null;
    private String country 				= null;
    private RegistereUserDto regUserObj = null;

    public static boolean IS_REGISTER_SCHOOL = false;
    public static Bitmap profileImageBitmap  = null;

    private String schoolIdFromFind  			= null;
    private  ArrayList<String> teacherInfo 		= null;
    private String teacherFirstName 			= null;
    private String teacherLastName  			= null;
    private String teacherId        			= null;

    ProgressDialog progressDialog  = null;
    private String callingActivity = null;

    private Button   btnTitleBack  = null;
    private final String TAG = this.getClass().getSimpleName();

    private SharedPreferences facebookPreference= null;

    private TextView lblAvatar = null;
    private TextView lblGrade = null;
    private String studentInfoModificationNotice = null;
    private boolean onFocusLostIsValidString = true;
    private String alertOnlyTeacherCanChange = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_registered_user_player);

        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "inside onCreate()");

        facebookPreference = getSharedPreferences(IS_FACEBOOK_LOGIN, 0);

        this.init();
        this.getPlayerIdFromIntent();
        this.setWidgetsReferences();
        this.setWidgetsTextValue();
        this.getPlayerDataById();
        this.setDataFromDatabase();
        //this.getGrade(this,playerData.getGrade());
        //this.setSchools();
        this.setGrade(playerData.getGrade());
        this.setListenerOnWidgets();

        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "outside onCreate()");
    }

    private void init(){
        gradeList = MathFriendzyHelper.getUpdatedGradeList(this);
    }

    private void setGrade(String grade){
        MathFriendzyHelper.setAdapterToSpinner(this,
                MathFriendzyHelper.getGradeForSelection(grade)
                , gradeList, spinnerGrade);
    }

    /**
     * This method set the listener on widgets
     */
    private void setListenerOnWidgets()
    {
        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "inside setListenerOnWidgets()");

        btnSavePlayer.setOnClickListener(this);
        lblFbConnect.setOnClickListener(this);
        avtarLayout.setOnClickListener(this);
        btnDeletePlayer.setOnClickListener(this);

        edtSchool.setOnClickListener(this);//changes
        edtTeacher.setOnClickListener(this);//changes

        btnTitleBack.setOnClickListener(this);

        //changes according deepak mail
        spinnerGrade.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {

                int selectedGrade = MathFriendzyHelper
                        .getGrade(spinnerGrade.getSelectedItem().toString());

                if((selectedGrade + "").equals("13"))
                {
                    //edtSchool.setVisibility(EditText.GONE);
                    //edtTeacher.setVisibility(EditText.GONE);
                    //lblRegTeacher.setVisibility(TextView.GONE);
                    //pickerTitleSchool.setVisibility(TextView.GONE);
                }else{
                    //edtSchool.setVisibility(EditText.VISIBLE);
                    //edtTeacher.setVisibility(EditText.VISIBLE);
                    //lblRegTeacher.setVisibility(TextView.VISIBLE);
                    //pickerTitleSchool.setVisibility(TextView.VISIBLE);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });
        //end changes

        MathFriendzyHelper.setFocusChangeListener(this , edtFirstName , this);
        MathFriendzyHelper.setFocusChangeListener(this , edtLastName , this);
        MathFriendzyHelper.setFocusChangeListener(this , edtUserName , this);

        MathFriendzyHelper.setEditTextWatcherToEditTextForLastInitialName(this , edtLastName );

        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "outside setListenerOnWidgets()");
    }


    @Override
    protected void onRestart()
    {
        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "inside onRestart()");

        this.setTextFromFacebook();
		/*if(IS_REGISTER_SCHOOL)
		{
			if(schoolName != null)
			{
				schoolNameList.add(schoolName);
				schoolAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, schoolNameList);
				schoolAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				spinnerSchool.setAdapter(schoolAdapter);
				spinnerSchool.setSelection(schoolAdapter.getPosition(schoolName));
			}
		}*/

        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "outsideside onRestart()");

        super.onRestart();
    }

    /**
     * This method get the player id from intent which is set in previous activity
     */
    private void getPlayerIdFromIntent()
    {
        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "inside getPlayerIdFromIntent()");

        callingActivity = this.getIntent().getStringExtra("callingActivity");
        playerId  = this.getIntent().getStringExtra("playerId");
        userId    = this.getIntent().getStringExtra("userId");
        //Log.e(TAG, "playerId" + playerId);
        imageName = this.getIntent().getStringExtra("imageName");

        if(this.getIntent().getBooleanExtra("isCallFromRegistration", false)){
            CommonUtils.showDialogAfterRegistrationSuccess(this);
        }

        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "outside getPlayerIdFromIntent()");
    }

    private void getPlayerDataById()
    {
        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "inside getPlayerDataById()");

        playerData = new UserPlayerDto();
        UserPlayerOperation userOprObj = new UserPlayerOperation(this);
        playerData = userOprObj.getUserPlayerDataById(playerId);

        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "outside getPlayerDataById()");
    }

    /**
     * New registration change on 22 jun 2015
     */
    private void setSchoolVisibility(){
        try{
            if(MathFriendzyHelper.parseInt(playerData.getSchoolId()) > 0
                    && !MathFriendzyHelper.isEmpty(playerData.getSchoolName())){
                edtSchool.setVisibility(EditText.VISIBLE);
                pickerTitleSchool.setVisibility(TextView.VISIBLE);
            }else{
                edtSchool.setVisibility(EditText.GONE);
                pickerTitleSchool.setVisibility(TextView.GONE);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * New registration change on 22 jun 2015
     */
    private void setTeacherVisibility(){
        try{
            if(MathFriendzyHelper.parseInt(playerData.getTeacherUserId()) > 0
                    && (!MathFriendzyHelper.isEmpty(playerData.getTeacherFirstName())
                    || !MathFriendzyHelper.isEmpty(playerData.getTeacheLastName()))){
                edtTeacher.setVisibility(EditText.VISIBLE);
                lblRegTeacher.setVisibility(TextView.VISIBLE);
            }else{
                edtTeacher.setVisibility(EditText.GONE);
                lblRegTeacher.setVisibility(TextView.GONE);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * This method set the data from the database which is already in database and user want to edit it
     */
    private void setDataFromDatabase()
    {
        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "inside setDataFromDatabase()");

        edtFirstName.setText(playerData.getFirstname());
        edtLastName.setText(MathFriendzyHelper.getLastInitialName(playerData.getLastname()));
        edtUserName.setText(playerData.getUsername());

        if(playerData.getSchoolName().equals(""))
            edtSchool.setText(SCHOOL_TEXT);
        else
            edtSchool.setText(playerData.getSchoolName());
        schoolIdFromFind = playerData.getSchoolId();

        if(playerData.getTeacherFirstName().equals(""))
            edtTeacher.setText(TEACHAER_TEXT);
        else
            edtTeacher.setText(playerData.getTeacherFirstName() + " " +
                    playerData.getTeacheLastName());

        teacherFirstName = playerData.getTeacherFirstName();
        teacherLastName  = playerData.getTeacheLastName();
        teacherId  		 = playerData.getTeacherUserId();
        imageName        = playerData.getImageName();

        this.setSchoolVisibility();
        this.setTeacherVisibility();

        //Log.e(TAG, "imageName" + imageName);
        try
        {
            Long.parseLong(imageName);
            //changes for Internet Connection
            if(CommonUtils.isInternetConnectionAvailable(this))
            {
                String strUrl = FACEBOOK_HOST_NAME + imageName + "/picture?type=large";
                new FacebookImageLoaderTask(strUrl).execute(null,null,null);
            }
            else
            {
                DialogGenerator dg = new DialogGenerator(this);
                Translation transeletion = new Translation(this);
                transeletion.openConnection();
                dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
                transeletion.closeConnection();
            }
        }
        catch(NumberFormatException ee)
        {
            ChooseAvtarOpration chooseAvtarObj = new ChooseAvtarOpration();
            chooseAvtarObj.openConn(this);
            if(chooseAvtarObj.getAvtarImageByName(playerData.getImageName()) != null)
            {
                //profileImageBitmap = CommonUtils.getBitmapFromByte(chooseAvtarObj.getAvtarImageByName(playerData.getImageName()) , this);
                profileImageBitmap = CommonUtils.getBitmapFromByte(chooseAvtarObj
                        .getAvtarImageByName(playerData.getImageName()));
                imgAvtar.setImageBitmap(profileImageBitmap);
            }
            chooseAvtarObj.closeConn();
        }

        this.checkForStudentCreatedByTeacher(playerData);

        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "outside setDataFromDatabase()");

    }

    /**
     * This method set the widgets references from the layout to the widgets objects
     */
    private void setWidgetsReferences()
    {
        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "inside setWidgetsReferences()");

        edtFirstName 	= (EditText) findViewById(R.id.edtFirstName);
        edtLastName  	= (EditText) findViewById(R.id.edtLastName);
        edtUserName  	= (EditText) findViewById(R.id.edtUserName);

        spinnerGrade 	= (Spinner) findViewById(R.id.spinnerGrade);
        edtSchool 		= (EditText) findViewById(R.id.spinnerSchool);//changes
        edtTeacher      = (EditText) findViewById(R.id.spinnerTeacher);//changes
        btnSavePlayer   = (Button) findViewById(R.id.btnTitleSave);
        btnDeletePlayer = (Button) findViewById(R.id.btnTitleDelete);
        imgAvtar        = (ImageView) findViewById(R.id.imgAvtar);

        mfTitleHomeScreen 	= (TextView) findViewById(R.id.mfTitleHomeScreen);
        lblEditPlayerTitle 	= (TextView) findViewById(R.id.lblEditPlayerTitle);
        lblFbConnect 		= (Button) findViewById(R.id.lblFbConnect);
        lblFirstName 		=  (TextView) findViewById(R.id.lblFirstName);
        lblLastName 		= (TextView) findViewById(R.id.lblLastName);
        lblUserName 		= (TextView) findViewById(R.id.lblUserName);
        pickerTitleSchool   = (TextView) findViewById(R.id.pickerTitleSchool);
        lblAddPlayerGrade 	= (TextView) findViewById(R.id.lblAddPlayerGrade);
        lblRegTeacher 		= (TextView) findViewById(R.id.lblRegTeacher);
        lblChooseAnAvatar 	= (TextView) findViewById(R.id.lblChooseAnAvatar);
        avtarLayout         = (RelativeLayout) findViewById(R.id.avtarLayout);

        btnTitleBack        = (Button)   findViewById(R.id.btnTitleBack);
        lblTransferYourInfoFromFacebookAndFindFriends = (TextView) findViewById(R.id.lblTransferYourInfoFromFacebookAndFindFriends);
        lblAvatar           = (TextView) findViewById(R.id.lblAvatar);
        lblGrade            = (TextView) findViewById(R.id.lblGrade);

        //set Done button to keyboard
        CommonUtils.setDoneButtonToEditText(edtFirstName);
        CommonUtils.setDoneButtonToEditText(edtLastName);
        CommonUtils.setDoneButtonToEditText(edtUserName);
        //end changes

        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "outside setWidgetsReferences()");
    }

    /**
     * this method set the wisgets text values from the transelation
     */
    private void setWidgetsTextValue()
    {
        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "inside setWidgetsTextValue()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        mfTitleHomeScreen.setText(transeletion.getTranselationTextByTextIdentifier("mfTitleHomeScreen"));
        lblEditPlayerTitle.setText(transeletion.getTranselationTextByTextIdentifier("lblEditPlayerTitle"));
        lblFirstName.setText(transeletion.getTranselationTextByTextIdentifier("lblFirstName"));
        lblLastName.setText(transeletion.getTranselationTextByTextIdentifier("lblLastInitial"));
        lblUserName.setText(transeletion.getTranselationTextByTextIdentifier("lblUserName"));
        pickerTitleSchool.setText(transeletion.getTranselationTextByTextIdentifier("pickerTitleSchool"));
        lblAddPlayerGrade.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade"));
        lblRegTeacher.setText(transeletion.getTranselationTextByTextIdentifier("lblRegTeacher"));
        lblChooseAnAvatar.setText(transeletion.getTranselationTextByTextIdentifier("lblAvatar"));
        btnSavePlayer.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleSubmit"));
        btnDeletePlayer.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleDelete"));
        btnTitleBack.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBack"));

        if(!facebookPreference.getBoolean(IS_LOGIN_FROM_FACEBOOK, false))
        {
            lblFbConnect.setText(transeletion.getTranselationTextByTextIdentifier("lblFbConnect"));
            lblTransferYourInfoFromFacebookAndFindFriends.setText(transeletion.getTranselationTextByTextIdentifier
                    ("lblTransferYourInfoFromFacebookAndFindFriends"));
        }
        else
        {
            lblFbConnect.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleLogOut"));
            lblTransferYourInfoFromFacebookAndFindFriends.setText(transeletion.getTranselationTextByTextIdentifier
                    ("lblDisconnectFromFb"));
        }
        lblGrade.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade"));
        lblAvatar.setText(transeletion.getTranselationTextByTextIdentifier("lblAvatar"));
        studentInfoModificationNotice = transeletion.getTranselationTextByTextIdentifier("studentInfoModificationNotice");
        alertOnlyTeacherCanChange = transeletion
                .getTranselationTextByTextIdentifier("alertOnlyTeacherCanChange");
        this.setHintToEditText(transeletion.getTranselationTextByTextIdentifier("lblFirstName"), edtFirstName);
        this.setHintToEditText(transeletion.getTranselationTextByTextIdentifier("lblLastInitial"), edtLastName);
        this.setHintToEditText(transeletion.getTranselationTextByTextIdentifier("lblUserName"), edtUserName);
        transeletion.closeConnection();

        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "outside setWidgetsTextValue()");
    }

    protected void setHintToEditText(String text ,  EditText edtText){
        edtText.setHint(text);
    }

    @Override
    public void onClick(View v)
    {
        MathFriendzyHelper.clearFocus(this);
        if(onFocusLostIsValidString) {
            switch (v.getId()) {
                case R.id.avtarLayout:
                    Intent chooseAvtar = new Intent(this, ChooseAvtars.class);
                    startActivity(chooseAvtar);
                    break;
                case R.id.btnTitleSave:
                    this.checkEmptyValidate();
                    break;
                case R.id.lblFbConnect:
                    this.facebookConnect();
                    break;
                case R.id.btnTitleDelete:
                    this.deletePlayer();
                    break;
                case R.id.spinnerSchool:
                    MathFriendzyHelper.showWarningDialog(this, studentInfoModificationNotice);
                    //this.setListenerOnEdtSchool();
                    break;
                case R.id.spinnerTeacher:
                    MathFriendzyHelper.showWarningDialog(this, studentInfoModificationNotice);
                    /*Intent teacherIntent = new Intent(this,SearchTeacherActivity.class);
                    teacherIntent.putExtra("schoolId", schoolIdFromFind);
                    startActivityForResult(teacherIntent, 2);*/
                    break;
                case R.id.btnTitleBack:
                    if (callingActivity.equals("LoginUserPlayerActivity"))
                        EditRegisteredUserPlayer.this.startActivity(new Intent(EditRegisteredUserPlayer.this, LoginUserPlayerActivity.class));
                    else if (callingActivity.equals("ModifyRegistration") || callingActivity.equals("RegisterationStep2"))
                        EditRegisteredUserPlayer.this.startActivity(new Intent(EditRegisteredUserPlayer.this, ModifyRegistration.class));
                    else if (callingActivity.equals("TeacherPlayer"))
                        EditRegisteredUserPlayer.this.startActivity(new Intent(EditRegisteredUserPlayer.this, TeacherPlayer.class));
                    /*Intent intentMain = new Intent(this,MainActivity.class);
                    startActivity(intentMain);*/
                    break;
            }
        }
    }

    /**
     * This method connect with facebook and if already connect to facebook then disconnect from faebook
     */
    private void facebookConnect()
    {
        Translation transeletion1 = new Translation(this);
        transeletion1.openConnection();

        SharedPreferences.Editor editor = facebookPreference.edit();

        if(facebookPreference.getBoolean(IS_LOGIN_FROM_FACEBOOK, false))
        {
            FacebookConnect.logoutFromFacebook();
            editor.putBoolean(IS_LOGIN_FROM_FACEBOOK, false);
            editor.commit();
            lblFbConnect.setText(transeletion1.getTranselationTextByTextIdentifier("lblFbConnect"));
            lblTransferYourInfoFromFacebookAndFindFriends.setText(transeletion1.getTranselationTextByTextIdentifier
                    ("lblTransferYourInfoFromFacebookAndFindFriends"));
        }
        else
        {
            //changes for Internet Connection
            if(CommonUtils.isInternetConnectionAvailable(this))
            {
                Intent intent = new Intent(this,FacebookConnect.class);
                startActivity(intent);
                editor.putBoolean(IS_LOGIN_FROM_FACEBOOK, true);
                editor.commit();
                lblFbConnect.setText(transeletion1.getTranselationTextByTextIdentifier("btnTitleLogOut"));
                lblTransferYourInfoFromFacebookAndFindFriends.setText(transeletion1.getTranselationTextByTextIdentifier
                        ("lblDisconnectFromFb"));
            }
            else
            {
                DialogGenerator dg1 = new DialogGenerator(this);
                dg1.generateWarningDialog(transeletion1.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
            }
        }
        transeletion1.closeConnection();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(keyCode == KeyEvent.KEYCODE_BACK)
        {
            if(callingActivity.equals("LoginUserPlayerActivity"))
                EditRegisteredUserPlayer.this.startActivity(new Intent(EditRegisteredUserPlayer.this,LoginUserPlayerActivity.class));
            else if(callingActivity.equals("ModifyRegistration") || callingActivity.equals("RegisterationStep2"))
                EditRegisteredUserPlayer.this.startActivity(new Intent(EditRegisteredUserPlayer.this,ModifyRegistration.class));
            else if(callingActivity.equals("TeacherPlayer"))
                EditRegisteredUserPlayer.this.startActivity(new Intent(EditRegisteredUserPlayer.this,TeacherPlayer.class));
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    private String getCountryName()
    {
        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "inside getCountryName()");

        UserRegistrationOperation regObj = new UserRegistrationOperation(EditRegisteredUserPlayer.this);
        regUserObj = regObj.getUserData();
        Country countryObj = new Country();
        country = countryObj.getCountryNameByCountryId(regUserObj.getCountryId(), EditRegisteredUserPlayer.this);

        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "outside getCountryName()");

        return country;
    }

    /**
     * This methos is called when user click on school edit text
     */
    private void setListenerOnEdtSchool()
    {
        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "inside setListenerOnEdtSchool()");

        String country = getCountryName();
        Intent schoolIntent = new Intent(this,SearchYourSchoolActivity.class);

        //for getting state name
        States stateObj  = new States();
        String stateName = null;
        if(country.equals("United States"))
        {
            stateName = stateObj.getUSStateNameById(regUserObj.getStateId(), this);
        }
        else if(country.equals("Canada"))
        {
            stateName = stateObj.getCanadaStateNameById(regUserObj.getStateId(), this);
        }


        if(country.equals("United States")
                ||country.equals("Canada"))
        {
            schoolIntent.putExtra("country", country);
            schoolIntent.putExtra("state", stateName);
            schoolIntent.putExtra("city", regUserObj.getCity());
            schoolIntent.putExtra("zip", regUserObj.getZip());
        }
        else
        {
            schoolIntent.putExtra("country", country);
            schoolIntent.putExtra("state", "");
            schoolIntent.putExtra("city", regUserObj.getCity());
            schoolIntent.putExtra("zip", regUserObj.getZip());
        }
        startActivityForResult(schoolIntent, 1);


        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "outside setListenerOnEdtSchool()");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "inside onActivityResult()");

		/*if (requestCode == 1) 
		{
		     if(resultCode == RESULT_OK)
		     {      
		         ArrayList<String> schoolInfo = data.getStringArrayListExtra("schoolInfo");
		         edtSchool.setText(schoolInfo.get(0));
		         schoolIdFromFind = schoolInfo.get(1);
		     }
		}
		 */

        if (requestCode == 1)
        {
            if(resultCode == RESULT_OK)
            {
                ArrayList<String> schoolInfo = data.getStringArrayListExtra("schoolInfo");
                edtSchool.setText(schoolInfo.get(0));
                schoolIdFromFind = schoolInfo.get(1);

                if(schoolInfo.get(0).equals("Home School")){
                    edtTeacher.setEnabled(false);
                    edtTeacher.setText("N/A");
                    teacherFirstName = "N/A";
                    teacherLastName  = "";
                    teacherId        = "-2";
                }else{
                    edtTeacher.setEnabled(true);

                    //chages for school change on 11-03-2015
                    if(schoolInfo.get(0).equalsIgnoreCase(playerData.getSchoolName())){
                        if(playerData.getTeacherFirstName().equals(""))
                            edtTeacher.setText(TEACHAER_TEXT);
                        else
                            edtTeacher.setText(playerData.getTeacherFirstName() + " " +
                                    playerData.getTeacheLastName());
                    }else{
                        edtTeacher.setText("");
                    }
                }
            }
        }

        if (requestCode == 2)
        {
            if(resultCode == RESULT_OK)
            {

                teacherInfo = data.getStringArrayListExtra("schoolInfo");
                edtTeacher.setText(teacherInfo.get(0) + " " + teacherInfo.get(1));

                teacherFirstName = teacherInfo.get(0);
                teacherLastName  = teacherInfo.get(1);
                teacherId        = teacherInfo.get(2);

            }
        }

        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "outside onActivityResult()");

        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * This method delete the user player from the database as well as from server
     */
    private void deletePlayer()
    {
        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "inside deletePlayer()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        DialogGenerator dg = new DialogGenerator(this);
        dg.generateDeleteConfirmationUserPlayerDialog(transeletion.getTranselationTextByTextIdentifier
                ("alertMsgAreYouSureToDeletePlayer"),playerId,userId,callingActivity);
        transeletion.closeConnection();

        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "outside deletePlayer()");
    }

    /**
     * When user want to save edit information then check for empty validation
     */
    private void checkEmptyValidate()
    {
        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "inside checkEmptyValidate()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        DialogGenerator dg = new DialogGenerator(this);
        int grade = MathFriendzyHelper.getGrade(spinnerGrade.getSelectedItem().toString());

        if(edtFirstName.getText().toString().equals("")
                ||edtLastName.getText().toString().equals("")
                ||edtUserName.getText().toString().equals("")//condition changes according deepak mail
                || (grade == MathFriendzyHelper.NO_GRADE_SELETED)
                || (!(grade + "").equals("13")) &&
                edtSchool.getText().toString().equals("")
                || edtTeacher.getText().toString().equals("")){
            dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier
                    ("alertMsgPleaseEnterAllInfo"));
        }
        else
        {
            this.checkValidUser(edtUserName.getText().toString());
        }
        transeletion.closeConnection();

        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "outside checkEmptyValidate()");
    }

    /**
     * Check fro valid user or not if it is edited by the user
     * @param userName
     */
    private void checkValidUser(String userName)
    {
        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "inside checkValidUser()");

        if(edtUserName.getText().toString().equals(playerData.getUsername()))
        {
            savePlayerData();
			/*if(callingActivity.equals("LoginUserPlayerActivity"))
				EditRegisteredUserPlayer.this.startActivity(new Intent(EditRegisteredUserPlayer.this,LoginUserPlayerActivity.class));
			else if(callingActivity.equals("ModifyRegistration"))
				EditRegisteredUserPlayer.this.startActivity(new Intent(EditRegisteredUserPlayer.this,ModifyRegistration.class));
			else if(callingActivity.equals("TeacherPlayer"))
				EditRegisteredUserPlayer.this.startActivity(new Intent(EditRegisteredUserPlayer.this,TeacherPlayer.class));
			else	
				EditRegisteredUserPlayer.this.startActivity(new Intent(EditRegisteredUserPlayer.this,MainActivity.class));*/
        }
        else
        {
            //changes for Internet Connection
            if(CommonUtils.isInternetConnectionAvailable(this))
            {
                progressDialog = CommonUtils.getProgressDialog(this);
                new CkeckValidUserAsyncTask(userName).execute(null,null,null);
                //execute asyncktask for check user is already registered or not
            }
            else
            {
                DialogGenerator dg = new DialogGenerator(this);
                Translation transeletion = new Translation(this);
                transeletion.openConnection();
                dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
                transeletion.closeConnection();
            }
        }

        if(EDIT_REG_USER_PLAYER_FLAG)
            Log.e(TAG, "outside checkValidUser()");
    }

    /**
     * This method save the data from the screen to the database
     */
    private void savePlayerData()
    {
        ArrayList<UserPlayerDto> userPlayerList = new ArrayList<UserPlayerDto>();

        UserPlayerOperation userPlayoprObj = new UserPlayerOperation(this);
        UserPlayerDto userPlayerObj = userPlayoprObj.getUserPlayerDataById(playerId);

        userPlayerObj.setFirstname(edtFirstName.getText().toString());
        userPlayerObj.setLastname(edtLastName.getText().toString());
        userPlayerObj.setUsername(edtUserName.getText().toString());

        userPlayerObj.setSchoolId(schoolIdFromFind);
        userPlayerObj.setSchoolName(edtSchool.getText().toString());

        int selectedGrade = MathFriendzyHelper.getGrade(spinnerGrade
                .getSelectedItem().toString());
		/*Log.e(TAG, "selected grade " + selectedGrade);*/
        userPlayerObj.setGrade(selectedGrade + "");
        userPlayerObj.setTeacherFirstName(teacherFirstName);
        userPlayerObj.setTeacheLastName(teacherLastName);
        userPlayerObj.setTeacherUserId(teacherId);

        userPlayerObj.setImageName(imageName);

        if(profileImageBitmap != null)
            userPlayerObj.setProfileImage(CommonUtils.getByteFromBitmap(profileImageBitmap));
        userPlayerList.add(userPlayerObj);

        userPlayoprObj = new UserPlayerOperation(this);
        userPlayoprObj.deleteFromUserPlayerByPlayerId(playerId);
        userPlayoprObj.insertUserPlayerData(userPlayerList);

        UserRegistrationOperation regObj = new UserRegistrationOperation(EditRegisteredUserPlayer.this);
        regUserObj = regObj.getUserData();

        UserPlayerOperation userObj = new UserPlayerOperation(this);
        if(userObj.isUserPlayersExist())
        {
            regUserObj.setPlayers("<AllPlayers></AllPlayers>");
        }
        else
        {
            UserPlayerOperation userObj1 = new UserPlayerOperation(this);
            ArrayList<UserPlayerDto> userPlayerObj1 = userObj1.getUserPlayerData();
            regUserObj.setPlayers("<AllPlayers>"+this.getXmlPlayer(userPlayerObj1)+"</AllPlayers>");
        }

        //changes for Internet Connection
        if(CommonUtils.isInternetConnectionAvailable(this))
        {
            new UpdateAsynTask(regUserObj).execute(null,null,null);
        }
        else
        {
            DialogGenerator dg = new DialogGenerator(this);
            Translation transeletion = new Translation(this);
            transeletion.openConnection();
            dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
            transeletion.closeConnection();
        }

		/*userPlayoprObj = new UserPlayerOperation(this);
		userPlayoprObj.deleteFromUserPlayerByPlayerId(playerId);
		userPlayoprObj.insertUserPlayerData(userPlayerList);*/
    }

    /**
     * This method return the xml format of the given user player data for updation on server
     * @param userPlayerObj
     * @return
     */
    private String getXmlPlayer(ArrayList<UserPlayerDto> userPlayerObj)
    {
        if(MODIFY_USER_FLAG)
            Log.e(TAG, "inside getXmlPlayer()");

        String xml = "";

        for( int i = 0 ;  i < userPlayerObj.size() ; i++)
        {
            xml = xml +"<player>" +
                    "<playerId>"+userPlayerObj.get(i).getPlayerid()+"</playerId>"+
                    "<fName>"+userPlayerObj.get(i).getFirstname()+"</fName>"+
                    "<lName>"+userPlayerObj.get(i).getLastname()+"</lName>"+
                    "<grade>"+userPlayerObj.get(i).getGrade()+"</grade>"+
                    "<schoolId>"+userPlayerObj.get(i).getSchoolId()+"</schoolId>"+
                    "<teacherId>"+userPlayerObj.get(i).getTeacherUserId()+"</teacherId>"+
                    "<indexOfAppearance>-1</indexOfAppearance>"+
                    "<profileImageId>"+userPlayerObj.get(i).getImageName()+"</profileImageId>"+
                    "<coins>"+userPlayerObj.get(i).getCoin()+"</coins>"+
                    "<points>"+userPlayerObj.get(i).getPoints()+"</points>"+
                    "<userName>"+userPlayerObj.get(i).getUsername()+"</userName>"+
                    "<competeLevel>1</competeLevel>"+
                    "</player>";
        }

        if(MODIFY_USER_FLAG)
            Log.e(TAG, "outside getXmlPlayer()");

        return xml;
    }
    /**
     * @description check for user already registered or not
     * @author Yashwant Singh
     *
     */
    class CkeckValidUserAsyncTask extends AsyncTask<Void, Void, Void>
    {
        private String userName = null;
        private String resultValue   = null;

        public CkeckValidUserAsyncTask(String userName)
        {
            this.userName = userName;
        }

        @Override
        protected void onPreExecute()
        {
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            if(EDIT_REG_USER_PLAYER_FLAG)
                Log.e(TAG, "inside CkeckValidUserAsyncTask doInBackground()");

            Validation validObj = new Validation();
            resultValue = validObj.validUser(userName);

            if(EDIT_REG_USER_PLAYER_FLAG)
                Log.e(TAG, "outside CkeckValidUserAsyncTask doInBackground()");
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            if(EDIT_REG_USER_PLAYER_FLAG)
                Log.e(TAG, "inside CkeckValidUserAsyncTask onPostExecute()");

            progressDialog.cancel();

            if(resultValue != null){
                if(resultValue.equals("1"))//already registered
                {
                    Translation transeletion = new Translation(EditRegisteredUserPlayer.this);
                    transeletion.openConnection();
                    DialogGenerator dg = new DialogGenerator(EditRegisteredUserPlayer.this);
                    dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgThisUserNameAlreadyExist"));
                    transeletion.closeConnection();
                }
                else
                {
                    savePlayerData();
					/*if(callingActivity.equals("LoginUserPlayerActivity"))
					EditRegisteredUserPlayer.this.startActivity(new Intent(EditRegisteredUserPlayer.this,LoginUserPlayerActivity.class));
				else if(callingActivity.equals("ModifyRegistration") || callingActivity.equals("RegisterationStep2"))
					EditRegisteredUserPlayer.this.startActivity(new Intent(EditRegisteredUserPlayer.this,ModifyRegistration.class));
				else if(callingActivity.equals("TeacherPlayer"))
					EditRegisteredUserPlayer.this.startActivity(new Intent(EditRegisteredUserPlayer.this,TeacherPlayer.class));
				else
					EditRegisteredUserPlayer.this.startActivity(new Intent(EditRegisteredUserPlayer.this,MainActivity.class));*/
                }
            }else{
                CommonUtils.showInternetDialog(EditRegisteredUserPlayer.this);
            }
            if(EDIT_REG_USER_PLAYER_FLAG)
                Log.e(TAG, "outside CkeckValidUserAsyncTask onPostExecute()");

            super.onPostExecute(result);
        }
    }

    /**
     * Update user information on server
     * @author Yashwant Singh
     *
     */
    class UpdateAsynTask extends AsyncTask<Void, Void, Void>
    {
        private RegistereUserDto regObj = null;
        private int registreationResult = 0;

        UpdateAsynTask(RegistereUserDto regObj)
        {
            this.regObj = regObj;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            if(MODIFY_USER_FLAG)
                Log.e(TAG, "inside UpdateAsynTask onPreExecute()");

            progressDialog = CommonUtils.getProgressDialog(EditRegisteredUserPlayer.this);
            progressDialog.show();

            if(MODIFY_USER_FLAG)
                Log.e(TAG, "outside UpdateAsynTask onPreExecute()");
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            if(MODIFY_USER_FLAG)
                Log.e(TAG, "inside UpdateAsynTask doInBackground()");

            Register registerObj = new Register(EditRegisteredUserPlayer.this);
            registreationResult = registerObj.updateUserOnserver(regObj);

            if(MODIFY_USER_FLAG)
                Log.e(TAG, "outside UpdateAsynTask doInBackground()");

            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            //progressDialog.cancel();

            if(MODIFY_USER_FLAG)
                Log.e(TAG, "inside UpdateAsynTask onPostExecute()");

            Translation transeletion = new Translation(EditRegisteredUserPlayer.this);
            transeletion.openConnection();
            DialogGenerator dg = new DialogGenerator(EditRegisteredUserPlayer.this);

            if(registreationResult == Register.SUCCESS)
            {
                //progressDialog.cancel();
				/*Intent intent = new Intent(ModifyRegistration.this,MainActivity.class);
				startActivity(intent);*/
                //changes when facing problem in dialog not dismiss
                progressDialog.cancel();
                if(callingActivity.equals("LoginUserPlayerActivity"))
                    EditRegisteredUserPlayer.this.startActivity(new Intent(EditRegisteredUserPlayer.this,LoginUserPlayerActivity.class));
                else if(callingActivity.equals("ModifyRegistration"))
                    EditRegisteredUserPlayer.this.startActivity(new Intent(EditRegisteredUserPlayer.this,ModifyRegistration.class));
                else if(callingActivity.equals("TeacherPlayer"))
                    EditRegisteredUserPlayer.this.startActivity(new Intent(EditRegisteredUserPlayer.this,TeacherPlayer.class));
                else{
                    try{
                        //changes for selected single player (by deepak mail)
                        UserPlayerOperation userObj = new UserPlayerOperation(EditRegisteredUserPlayer.this);
                        ArrayList<UserPlayerDto> userPlayerObj = userObj.getUserPlayerData();

                        if(userPlayerObj.size() == 1){
                            SharedPreferences sheredPreference2 = getSharedPreferences(IS_CHECKED_PREFF, 0);
                            SharedPreferences.Editor editor2 = sheredPreference2.edit();
                            editor2.putString(PLAYER_ID,userPlayerObj.get(0).getPlayerid());
                            //changes according to shilpi
                            editor2.putString("userId",userPlayerObj.get(0).getParentUserId());
                            //end changes
                            editor2.commit();
                        }
                    }catch(Exception e){
                        Log.e(TAG, "Exception occure when user player table not exist " + e.toString());
                    }
                    EditRegisteredUserPlayer.this.startActivity(new Intent(EditRegisteredUserPlayer.this,MainActivity.class));
                }
                //end changes
            }
            else if(registreationResult == Register.INVALID_CITY)
            {
                progressDialog.cancel();
                dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgInvalidCity"));
            }
            else if(registreationResult == Register.INVALID_ZIP)
            {
                progressDialog.cancel();
                dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgInvalidZipCode"));
            }else if(registreationResult == 0){
                CommonUtils.showInternetDialog(EditRegisteredUserPlayer.this);
            }

            transeletion.closeConnection();

            if(MODIFY_USER_FLAG)
                Log.e(TAG, "outside UpdateAsynTask onPostExecute()");

            super.onPostExecute(result);
        }
    }

    @Override
    protected void onResume() {
        //for show ad dialog
        CommonUtils.showAdDialog(this);
        //end ad dialog
        super.onResume();
    }

    //check only for the last edittext which has current focus
    @Override
    public void onFocusLost(boolean isValidString) {
        onFocusLostIsValidString = isValidString;
    }

    @Override
    public void onFocusHas(boolean isValidString) {

    }


    /**
     *
     */
    private void checkForStudentCreatedByTeacher(UserPlayerDto playerData){
        try{
            /*if(!MathFriendzyHelper.isEmpty(playerData.getStudentIdByTeacher())){
                spinnerGrade.setOnTouchListener(Spinner_OnTouch);
            }*/
            if(MathFriendzyHelper.isStudentRegisterByTeacher(playerData)){
                spinnerGrade.setOnTouchListener(Spinner_OnTouch);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private View.OnTouchListener Spinner_OnTouch = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                MathFriendzyHelper.showWarningDialog(EditRegisteredUserPlayer.this ,
                        alertOnlyTeacherCanChange);
            }
            return true;
        }
    };
}
