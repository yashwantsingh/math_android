package com.mathfriendzy.controller.player;

import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_USER_PLAYER_FLAG;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_ID;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.friendzy.TeacherChallengeActivity;
import com.mathfriendzy.controller.learningcenter.NewLearnignCenter;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.multifriendzy.MultiFriendzyMain;
import com.mathfriendzy.controller.singlefriendzy.SingleFriendzyMain;
import com.mathfriendzy.controller.top100.Top100Activity;
import com.mathfriendzy.customview.DynamicLayout;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenteServerOperation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.PlayerDataFromServerObj;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.learningcenter.PurchaseItemObj;
import com.mathfriendzy.model.player.temp.TempPlayerOperation;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.model.singleFriendzy.SingleFriendzyImpl;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;

/**
 * This shows the no. of player exits under the login user
 * @author Yashwant Singh
 *
 */
public class LoginUserPlayerActivity extends Activity implements OnClickListener
{
	private TextView mfTitleHomeScreen 		= null;
	private TextView lblPleaseSelectPlayer 	= null;
	private TextView textResult  			= null;
	private TextView btnTitleEdit 			= null;
	private Button   lblAddPlayer 			= null;
	private Button   playTitle 				= null;
	private Button   btnTitleTop100         = null;
	private TextView btnTitlePlayers        = null;
	//private ListView playerListView 		= null;
	private ArrayList<UserPlayerDto> userPlayerList   = null;
	private LinearLayout linearLayout = null;

	private final String TAG = this.getClass().getSimpleName();
	private Button   btnTitleBack      = null;

	private final int MAX_ITEM_ID = 11;// if item id is 11 the unlock the level

	//added for friendzy
	private String callingActivity  = " ";

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_user_player);

		if(LOGIN_USER_PLAYER_FLAG)
			Log.e(TAG, " inside onCreate()");

		callingActivity = getIntent().getStringExtra("calling");
		if(callingActivity == null)
		{
			callingActivity = "Main";
		}

		this.setWidgetsReferences();
		this.setWidgetsTextValue();
		this.getUserPlayersData();
		this.setListenerOnWidgets();

		/*UserPlayersAdapter adapter = new UserPlayersAdapter(this,R.layout.list_players,userPlayerList);
		playerListView.setAdapter(adapter);*/
		//this.createDyanamicLayoutForDisplayUserPlayer();

		DynamicLayout dynamicLayout = new DynamicLayout(this);
		dynamicLayout.createDyanamicLayoutForDisplayUserPlayer(userPlayerList, linearLayout , false);

		if(LOGIN_USER_PLAYER_FLAG)
			Log.e(TAG, " outside onCreate()");
	}

	/**
	 * This method set the references from the layout to the widgets objects
	 */
	private void setWidgetsReferences()
	{
		if(LOGIN_USER_PLAYER_FLAG)
			Log.e(TAG, " inside setWidgetsReferences()");

		//playerListView 			= (ListView) findViewById(R.id.playerListView);
		mfTitleHomeScreen 		= (TextView) findViewById(R.id.mfTitleHomeScreen);
		lblPleaseSelectPlayer 	= (TextView) findViewById(R.id.lblPleaseSelectPlayer);
		textResult 				= (TextView) findViewById(R.id.textResult);
		btnTitleEdit 			= (TextView) findViewById(R.id.btnTitleEdit);
		lblAddPlayer 			= (Button)   findViewById(R.id.lblAddPlayer);
		playTitle    			= (Button)   findViewById(R.id.playTitle);
		btnTitleTop100          = (Button)   findViewById(R.id.btnTitleTop100);
		btnTitlePlayers         = (TextView) findViewById(R.id.btnTitlePlayers);
		linearLayout            = (LinearLayout) findViewById(R.id.userPlayerLayout);
		btnTitleBack            = (Button)   findViewById(R.id.btnTitleBack);

		if(LOGIN_USER_PLAYER_FLAG)
			Log.e(TAG, " outside setWidgetsReferences()");
	}

	/**
	 * this method set the text Widgets text values from the translation
	 */
	private void setWidgetsTextValue()
	{
		if(LOGIN_USER_PLAYER_FLAG)
			Log.e(TAG, " inside setWidgetsTextValue()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		mfTitleHomeScreen.setText(transeletion.getTranselationTextByTextIdentifier("mfTitleHomeScreen"));
		lblPleaseSelectPlayer.setText(transeletion.getTranselationTextByTextIdentifier("lblSelectAPlayer"));
		textResult.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleResults"));
		btnTitleEdit.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleEdit"));
		lblAddPlayer.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayer"));
		playTitle.setText(transeletion.getTranselationTextByTextIdentifier("playTitle")
				+ " " + transeletion.getTranselationTextByTextIdentifier("lblNow") + "!");
		btnTitleTop100.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleTop100"));
		btnTitlePlayers.setText(transeletion.getTranselationTextByTextIdentifier("btnTitlePlayers"));
		btnTitleBack.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBack"));
		transeletion.closeConnection();

		if(LOGIN_USER_PLAYER_FLAG)
			Log.e(TAG, " outside setWidgetsTextValue()");
	}

	/**
	 * this method get player data from the database
	 */
	private void getUserPlayersData() 
	{
		if(LOGIN_USER_PLAYER_FLAG)
			Log.e(TAG, " inside getUserPlayersData()");

		UserPlayerOperation userPlayerObj = new UserPlayerOperation(this);
		userPlayerList = userPlayerObj.getUserPlayerData();

		if(LOGIN_USER_PLAYER_FLAG)
			Log.e(TAG, " outside getUserPlayersData()");
	}

	/**
	 * This method set listener on widgets
	 */
	private void setListenerOnWidgets()
	{
		lblAddPlayer.setOnClickListener(this);
		playTitle.setOnClickListener(this);
		btnTitleTop100.setOnClickListener(this);
		btnTitleBack.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.lblAddPlayer:
			if(!CommonUtils.isInternetConnectionAvailable(this)){
				DialogGenerator dg = new DialogGenerator(this);
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
				transeletion.closeConnection();
			}
			else
				startActivity(new Intent(this,AddPlayer.class).putExtra("callingActivity", "LoginUserPlayerActivity"));
			break;
		case R.id.playTitle:
			if(callingActivity.equals("TeacherChallengeActivity"))
			{
				Intent intentMain = new Intent(this,TeacherChallengeActivity.class);
				startActivity(intentMain);
			}
			else
				this.clickOnPlayTitle();
			break;
		case R.id.btnTitleBack:
			Intent intentMain = new Intent(this,MainActivity.class);
			startActivity(intentMain);
			break;
		case R.id.btnTitleTop100 :
			SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
			if(!sheredPreference.getBoolean(IS_LOGIN, false))
			{
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				DialogGenerator dg = new DialogGenerator(this);
				dg.generateRegisterOrLogInDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouMustLoginOrRegisterToViewAndParticipate"));
				transeletion.closeConnection();	
			}
			else
			{
				startActivity(new Intent(this,Top100Activity.class));
			}
			break;
		}
	}


	/**
	 * This method call when user click on play now button
	 */
	private void clickOnPlayTitle()
	{
		if(MainActivity.IS_CALL_FROM_LEARNING_CENTER == 1)
		{
			SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0); 

			if(sheredPreferencePlayer.getString(PLAYER_ID, "").equals(""))
			{	
				DialogGenerator dg = new DialogGenerator(this);
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("lblPleaseSelectPlayer"));
				transeletion.closeConnection();
			}
			else
			{	
				MainActivity.IS_CALL_FROM_LEARNING_CENTER = 0;
				SharedPreferences sharedPrefPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
				SharedPreferences.Editor editor = sharedPrefPlayerInfo.edit();

				UserPlayerOperation userPlayerOpr = new UserPlayerOperation(this);
				UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(sheredPreferencePlayer.getString(PLAYER_ID, ""));

				UserRegistrationOperation userObj = new UserRegistrationOperation(this);
				RegistereUserDto regUserObj = userObj.getUserData();

				/*Intent intent = new Intent(this,LearningCenterMain.class);*/

				editor.clear();
				/*editor.putString("playerName", userPlayerData.getFirstname() + " " + userPlayerData.getLastname());
				editor.putString("city", regUserObj.getCity());
				editor.putString("state", regUserObj.getState());
				editor.putString("imageName",  userPlayerData.getImageName());
				editor.putString("coins",  userPlayerData.getCoin());
				editor.putInt("grade", Integer.parseInt(userPlayerData.getGrade()));*/

				editor.putString("playerName", userPlayerData.getFirstname() + " " + userPlayerData.getLastname());
				editor.putString("city", regUserObj.getCity());
				editor.putString("state", regUserObj.getState());
				editor.putString("imageName",  userPlayerData.getImageName());
				editor.putString("coins",  userPlayerData.getCoin());
				editor.putInt("grade", Integer.parseInt(userPlayerData.getGrade()));
				editor.putString("userId",  userPlayerData.getParentUserId());
				editor.putString("playerId",  userPlayerData.getPlayerid());
				Country country = new Country();
				editor.putString("countryName", country.getCountryNameByCountryId(regUserObj.getCountryId(), this));
				editor.commit();

				this.clickOnNewLearnignCenter();
				//startActivity(intent);
			}

		}
		else if(MainActivity.IS_CALL_FROM_SINGLE_FRIENDZY == 1)
		{
			SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0); 

			if(sheredPreferencePlayer.getString(PLAYER_ID, "").equals(""))
			{
				DialogGenerator dg = new DialogGenerator(this);
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("lblPleaseSelectPlayer"));
				transeletion.closeConnection();
			}
			else
			{

				MainActivity.IS_CALL_FROM_SINGLE_FRIENDZY = 0;
				UserPlayerOperation userPlayerOpr = new UserPlayerOperation(this);
				UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(sheredPreferencePlayer.getString(PLAYER_ID, ""));

				SingleFriendzyImpl singleimpl = new SingleFriendzyImpl(this);
				singleimpl.openConn();
				int itemId = singleimpl.getItemId(userPlayerData.getParentUserId());
				singleimpl.closeConn();

				MainActivity x = new MainActivity();

				if(itemId != MAX_ITEM_ID)
				{
					if(CommonUtils.isInternetConnectionAvailable(this))
					{
						x.new GetSingleFriendzyDetail(userPlayerData.getParentUserId() , userPlayerData.getPlayerid()
								,this).execute(null,null,null);
					}
					else
					{
						/*DialogGenerator dg = new DialogGenerator(this);
							Translation transeletion = new Translation(this);
							transeletion.openConnection();
							dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
							transeletion.closeConnection();*/
						setUserPlayerDataForSingleFriendzyAndGotoSingleFriendzyMainActivity();
					}
				}
				else
				{
					if(CommonUtils.isInternetConnectionAvailable(this))
					{
						x.new GetpointsLevelDetail(userPlayerData.getParentUserId(), 
								userPlayerData.getPlayerid() , this).execute(null,null,null);
					}
					else
					{
						/*DialogGenerator dg = new DialogGenerator(this);
							Translation transeletion = new Translation(this);
							transeletion.openConnection();
							dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
							transeletion.closeConnection();*/
						setUserPlayerDataForSingleFriendzyAndGotoSingleFriendzyMainActivity();
					}

				}

			}
		}
		else if(MainActivity.IS_CALL_FROM_MULTIFRIENDZY == 1)
		{
			SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0); 

			if(sheredPreferencePlayer.getString(PLAYER_ID, "").equals(""))
			{
				DialogGenerator dg = new DialogGenerator(this);
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("lblPleaseSelectPlayer"));
				transeletion.closeConnection();
			}
			else
			{
				MainActivity.IS_CALL_FROM_MULTIFRIENDZY = 0;

				SharedPreferences sharedPrefPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
				SharedPreferences.Editor editor = sharedPrefPlayerInfo.edit();

				UserPlayerOperation userPlayerOpr = new UserPlayerOperation(this);
				UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(sheredPreferencePlayer.getString(PLAYER_ID, ""));

				UserRegistrationOperation userObj = new UserRegistrationOperation(this);
				RegistereUserDto regUserObj = userObj.getUserData();

				Intent intent = new Intent(this,MultiFriendzyMain.class);

				editor.clear();

				editor.putString("playerName", userPlayerData.getFirstname() + " " + userPlayerData.getLastname());
				editor.putString("city", regUserObj.getCity());
				editor.putString("state", regUserObj.getState());
				editor.putString("imageName",  userPlayerData.getImageName());
				editor.putString("coins",  userPlayerData.getCoin());
				editor.putInt("grade", Integer.parseInt(userPlayerData.getGrade()));
				editor.putString("userId",  userPlayerData.getParentUserId());
				editor.putString("playerId",  userPlayerData.getPlayerid());
				Country country = new Country();
				editor.putString("countryName", country.getCountryNameByCountryId(regUserObj.getCountryId(), this));
				editor.commit();

				startActivity(intent);
			}
		}
		else
		{
			//Log.e(TAG, "inside else block" + MainActivity.IS_CALL_FROM_LEARNING_CENTER);
			Intent intentMain1 = new Intent(this,MainActivity.class);
			startActivity(intentMain1);
		}
	}

	/**
	 * This method call when click on learnign center and created when new learning center is added
	 */
	private void clickOnNewLearnignCenter()
	{
		NewLearnignCenter.isCallFromMainActivity = true;
		TempPlayerOperation tempPlayer = new TempPlayerOperation(LoginUserPlayerActivity.this);

		if(tempPlayer.isTempPlayerDeleted())
		{				
			SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
			if(CommonUtils.isInternetConnectionAvailable(LoginUserPlayerActivity.this))
			{
				new PlayerDataFromServer(sharedPreffPlayerInfo.getString("userId", ""), 
						sharedPreffPlayerInfo.getString("playerId", "")).execute(null,null,null);
			}
			else
			{
				startActivity(new Intent(this , NewLearnignCenter.class));
				/*DialogGenerator dg = new DialogGenerator(LoginUserPlayerActivity.this);
				Translation transeletion = new Translation(LoginUserPlayerActivity.this);
				transeletion.openConnection();
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
				transeletion.closeConnection();*/
			}
		}
		else
		{
			startActivity(new Intent(this , NewLearnignCenter.class));	
		}
	}


	/**
	 * This method call after the data from server is loaded for single friendzy
	 */
	private void setUserPlayerDataForSingleFriendzyAndGotoSingleFriendzyMainActivity()
	{
		SharedPreferences sheredPreferencePlayer = getSharedPreferences(IS_CHECKED_PREFF, 0);
		UserPlayerOperation userPlayerOpr = new UserPlayerOperation(this);
		UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(sheredPreferencePlayer.getString(PLAYER_ID, ""));

		SharedPreferences sharedPrefPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		SharedPreferences.Editor editor = sharedPrefPlayerInfo.edit();

		UserRegistrationOperation userObj = new UserRegistrationOperation(this);
		RegistereUserDto regUserObj = userObj.getUserData();

		Intent intent = new Intent(this,SingleFriendzyMain.class);

		editor.clear();
		editor.putString("playerName", userPlayerData.getFirstname() + " " + userPlayerData.getLastname());
		editor.putString("city", regUserObj.getCity());
		editor.putString("state", regUserObj.getState());
		editor.putString("imageName",  userPlayerData.getImageName());
		editor.putString("coins",  userPlayerData.getCoin());
		editor.putInt("grade", Integer.parseInt(userPlayerData.getGrade()));
		editor.putString("userId",  userPlayerData.getParentUserId());
		editor.putString("playerId",  userPlayerData.getPlayerid());
		Country country = new Country();
		editor.putString("countryName", country.getCountryNameByCountryId(regUserObj.getCountryId(), this));

		LearningCenterimpl learningCenter = new LearningCenterimpl(this);
		learningCenter.openConn();

		if(learningCenter.getDataFromPlayerTotalPoints(userPlayerData.getPlayerid())
				.getCompleteLevel() != 0)
			editor.putInt("completeLevel", learningCenter.getDataFromPlayerTotalPoints(userPlayerData.getPlayerid())
					.getCompleteLevel());
		else
			editor.putInt("completeLevel", Integer.parseInt(userPlayerData.getCompletelavel()));

		learningCenter.closeConn();
		//for score update
		editor.commit();

		startActivity(intent);

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
		{
			/*Intent intentMain = new Intent(this,MainActivity.class);
			startActivity(intentMain);
			return false;*/
			//added by shilpi for friendzy

			if(callingActivity.equals("TeacherChallengeActivity"))
			{
				Intent intentMain = new Intent(this,TeacherChallengeActivity.class);
				startActivity(intentMain);
			}
			else
			{
				Intent intentMain = new Intent(this,MainActivity.class);
				startActivity(intentMain);
			}
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * This class get player data from server for learning center
	 * @author Yashwant Singh
	 *
	 */
	class PlayerDataFromServer extends AsyncTask<Void, Void, PlayerDataFromServerObj>
	{
		private ProgressDialog pd;
		private String userId;
		private String playerId;

		PlayerDataFromServer(String userId , String playeId)
		{
			this.playerId = playeId;
			this.userId   = userId;
		}

		@Override
		protected void onPreExecute() 
		{
			pd = CommonUtils.getProgressDialog(LoginUserPlayerActivity.this);
			pd.show();
			super.onPreExecute();
		}

		@Override
		protected PlayerDataFromServerObj doInBackground(Void... params) 
		{
			LearningCenteServerOperation learnignCenter = new LearningCenteServerOperation();
			PlayerDataFromServerObj playerDataFromServer = learnignCenter.getPlayerDetailFromServer(userId, playerId);
			return playerDataFromServer;
		}

		@Override
		protected void onPostExecute(PlayerDataFromServerObj result) 
		{
			//pd.cancel();

			if(result != null){
				String itemIds = "";

				ArrayList<String> itemList = new ArrayList<String>();

				if(result.getItems().length() > 0)
				{	
					//itemIds = result.getItems().replace(",", "");
					//get item value which is comma separated , add (,) at the last for finish	
					String newItemList = result.getItems() + ",";

					for(int i = 0 ; i < newItemList.length() ; i ++ )
					{
						if(newItemList.charAt(i) == ',' )
						{
							itemList.add(itemIds);
							itemIds = "";
						}
						else
						{
							itemIds = itemIds +  newItemList.charAt(i);
						}
					}
				}

				ArrayList<PurchaseItemObj> purchaseItem = new ArrayList<PurchaseItemObj>();


				for( int i = 0 ; i < itemList.size() ; i ++ )
				{
					PurchaseItemObj purchseObj = new PurchaseItemObj();
					purchseObj.setUserId(userId);
					purchseObj.setItemId(Integer.parseInt(itemList.get(i)));
					purchseObj.setStatus(1);
					purchaseItem.add(purchseObj);
				}

				if(result.getLockStatus() != -1)
				{
					PurchaseItemObj purchseObj = new PurchaseItemObj();
					purchseObj.setUserId(userId);
					purchseObj.setItemId(100);
					purchseObj.setStatus(result.getLockStatus());
					purchaseItem.add(purchseObj);
				}

				LearningCenterimpl learningCenter = new LearningCenterimpl(LoginUserPlayerActivity.this);
				learningCenter.openConn();
				learningCenter.deleteFromPlayerEruationLevel();
				for( int i = 0 ; i < result.getPlayerLevelList().size() ; i ++ )
					learningCenter.insertIntoPlayerEquationLevel(result.getPlayerLevelList().get(i));

				if(learningCenter.isPlayerRecordExits(playerId))
				{
					learningCenter.updatePlayerPoints(result.getPoints(), playerId);
				}
				else
				{
					PlayerTotalPointsObj playerTotalPoints = new PlayerTotalPointsObj();
					playerTotalPoints.setUserId(userId);
					playerTotalPoints.setPlayerId(playerId);
					playerTotalPoints.setCoins(0);
					playerTotalPoints.setCompleteLevel(0);
					playerTotalPoints.setPurchaseCoins(0);
					playerTotalPoints.setTotalPoints(result.getPoints());
					learningCenter.insertIntoPlayerTotalPoints(playerTotalPoints);
				}

				learningCenter.deleteFromPurchaseItem();
				learningCenter.insertIntoPurchaseItem(purchaseItem);		
				learningCenter.closeConn();

				MainActivity x = new MainActivity();
				x.new GetWordProblemLevelDetail(userId, playerId , pd , LoginUserPlayerActivity.this).execute(null,null,null);
			}else{
				pd.cancel();
				CommonUtils.showInternetDialog(LoginUserPlayerActivity.this);
			}
			//startActivity(new Intent(LoginUserPlayerActivity.this , NewLearnignCenter.class));	

			super.onPostExecute(result);
		}
	}
	
	@Override
	protected void onResume() {
		//for show ad dialog
		CommonUtils.showAdDialog(this);
		//end ad dialog
		super.onResume();
	}
}
