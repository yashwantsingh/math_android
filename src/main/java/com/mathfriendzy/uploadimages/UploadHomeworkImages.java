package com.mathfriendzy.uploadimages;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;


public class UploadHomeworkImages extends Service{
	private Thread threadObj = null;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		//Log.e("", "onCreate()");
		super.onCreate();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		//Log.e("", "start service");
		threadObj = UploadImageOnServerThread.getUploadThreadObj(this);
		if(!threadObj.isAlive()){
			threadObj.start();
		}
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onDestroy() {
		//Log.e("", "onDestroye()" + threadObj.getState());
		/*try{
			if(threadObj != null && threadObj.isAlive()){
				//Log.e("", "inside on destroyed");
				threadObj.stop();
			}
		}catch(Exception e){
			e.printStackTrace();
		}*/
		super.onDestroy();
	}
}
