package com.mathfriendzy.uploadimages;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.util.Log;

import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.utils.ICommonUtils;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;

@SuppressWarnings("deprecation")
public class UploadImage {

	private final static String TAG = "UploadImage";

	/**
	 * This method upload the images on server
	 * @param bm
	 * @param extraInfo 
	 * @throws Exception
	 */
	public static String executeMultipartPost(Bitmap bm , 
			String imageName , String extraInfo , UploadImageRequest request) throws Exception {
		String responseString = null;
		try {

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			bm.compress(CompressFormat.PNG, 90, bos);

			/*if(bm != null){
				bm.recycle();
				bm = null;
			}*/

			byte[] data = bos.toByteArray();
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost(ICommonUtils.UPLOAD_HOMW_WORK_IMAGE_URL 
					+ "action=uploadWorkImage");

			imageName = MathFriendzyHelper.getPNGFormateImageName(imageName);

			ByteArrayBody bab = new ByteArrayBody(data, imageName);
			MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
			reqEntity.addPart("uploadedfile", bab);
            reqEntity.addPart("uploadOnS3", new StringBody("1"));
			/*reqEntity.addPart("fileName", new StringBody(imageName));
			reqEntity.addPart("mimeType", new StringBody("images/jpeg"));*/
			//reqEntity.addPart("extraInfo", new StringBody(extraInfo));

			postRequest.setEntity(reqEntity);
			HttpResponse response = httpClient.execute(postRequest);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent(), "UTF-8"));
			String sResponse;
			StringBuilder uploadResponse = new StringBuilder();

			while ((sResponse = reader.readLine()) != null) {
				uploadResponse = uploadResponse.append(sResponse);
			}

			responseString = uploadResponse.toString();
			request.onComplete(responseString);
			return uploadResponse.toString();
		} catch (Exception e) {
			Log.e(TAG, "Error while uploading " + e.getMessage());
			responseString = null;
			request.onComplete(responseString);
			return null;
		}
	}

	/**
	 * This method upload the images on server
	 * @param bm
	 * @param extraInfo 
	 * @throws Exception
	 */
	public static String executeMultipartPost(Bitmap bm , 
			String imageName , String extraInfo , UploadImageRequest request
			, String uploadUrl) throws Exception {
		String responseString = null;
		try {

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			bm.compress(CompressFormat.PNG, 90, bos);

			/*if(bm != null){
				bm.recycle();
				bm = null;
			}*/

			byte[] data = bos.toByteArray();
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost(uploadUrl);
			imageName = MathFriendzyHelper.getPNGFormateImageName(imageName);
            //Log.e(TAG , "url " + uploadUrl + " name " + imageName);
			ByteArrayBody bab = new ByteArrayBody(data, imageName);
			MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
			reqEntity.addPart("uploadedfile", bab);
            reqEntity.addPart("uploadOnS3", new StringBody("1"));
			/*reqEntity.addPart("fileName", new StringBody(imageName));
			reqEntity.addPart("mimeType", new StringBody("images/jpeg"));*/
			//reqEntity.addPart("extraInfo", new StringBody(extraInfo));

			postRequest.setEntity(reqEntity);
			HttpResponse response = httpClient.execute(postRequest);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent(), "UTF-8"));
			String sResponse;
			StringBuilder uploadResponse = new StringBuilder();

			while ((sResponse = reader.readLine()) != null) {
				uploadResponse = uploadResponse.append(sResponse);
			}

			responseString = uploadResponse.toString();
			request.onComplete(responseString);
			return uploadResponse.toString();
		} catch (Exception e) {
            e.printStackTrace();
			Log.e(TAG, "Error while uploading " + e.getMessage());
			responseString = null;
			request.onComplete(responseString);
			return null;
		}
	}


    /**
     * This method upload the images on server
     * @param bm
     * @param extraInfo
     * @throws Exception
     */
    public static String executeMultipartPost(Bitmap bm ,
                                              String imageName ,
                                              String extraInfo , UploadImageRequest request
            , String uploadUrl
            , int compressPercentage , boolean isTab) throws Exception {
        String responseString = null;
        try {
            /*if(isTab) {
                bm = ScalingUtilities.createScaledBitmap(bm, (bm.getWidth() * 2) / 3,
                        (bm.getHeight() * 2) / 3,
                        ScalingUtilities.ScalingLogic.FIT);
            }else{
                bm = ScalingUtilities.createScaledBitmap(bm, (bm.getWidth() * 2) / 3,
                        (bm.getHeight() * 2) / 3,
                        ScalingUtilities.ScalingLogic.FIT);
            }*/
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bm.compress(CompressFormat.PNG, compressPercentage, bos);

            byte[] data = bos.toByteArray();
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost postRequest = new HttpPost(uploadUrl);
            imageName = MathFriendzyHelper.getPNGFormateImageName(imageName);
            //Log.e(TAG , "url " + uploadUrl + " name " + imageName);
            ByteArrayBody bab = new ByteArrayBody(data, imageName);
            MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            reqEntity.addPart("uploadedfile", bab);
            reqEntity.addPart("uploadOnS3", new StringBody("1"));
			/*reqEntity.addPart("fileName", new StringBody(imageName));
			reqEntity.addPart("mimeType", new StringBody("images/jpeg"));*/
            //reqEntity.addPart("extraInfo", new StringBody(extraInfo));

            postRequest.setEntity(reqEntity);
            HttpResponse response = httpClient.execute(postRequest);
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    response.getEntity().getContent(), "UTF-8"));
            String sResponse;
            StringBuilder uploadResponse = new StringBuilder();

            while ((sResponse = reader.readLine()) != null) {
                uploadResponse = uploadResponse.append(sResponse);
            }


            if(bm != null){
                bm.recycle();
                bm = null;
            }

            responseString = uploadResponse.toString();
            request.onComplete(responseString);
            return uploadResponse.toString();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Error while uploading " + e.getMessage());
            responseString = null;
            request.onComplete(responseString);
            return null;
        }
    }
}
