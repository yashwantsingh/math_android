package com.mathfriendzy.uploadimages;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.util.Log;

import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.homework.HomeWorkImpl;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;

@SuppressWarnings("deprecation")
public class UploadImageOnServerThread implements Runnable{

	private static Thread threadObj = null;
	private final String TAG = this.getClass().getName();
	private Context context = null;

	public UploadImageOnServerThread(Context context){
		this.context = context;
	}

	public static Thread getUploadThreadObj(Context context){
		if(threadObj == null || threadObj.getState() == Thread.State.TERMINATED)
			threadObj = new Thread(new UploadImageOnServerThread(context));
		return threadObj;
	}

	@Override
	public synchronized void run() {
		//Log.e(TAG, "inside run");
		try{
			if(CommonUtils.isInternetConnectionAvailable(context)){
				final HomeWorkImpl implObj = new HomeWorkImpl(context);
				implObj.openConnection();
				boolean isHomeworkImage = false;
				final String mimageName = implObj.fetchFirstIndexImageName();
				if(mimageName != null && mimageName.length() > 0){

					if(mimageName.startsWith(MathFriendzyHelper.HOME_WORK_IMAGE_PREFF)){
						isHomeworkImage = true;
					}else if(mimageName.startsWith(MathFriendzyHelper.HOME_WORK_QUE_IMAGE_PREFF)){
						isHomeworkImage = false;
					}

					String imageName = mimageName;
					if(isHomeworkImage){
						imageName = mimageName.replaceAll(MathFriendzyHelper
								.HOME_WORK_IMAGE_PREFF, "");
					}else{
						imageName = mimageName.replaceAll(MathFriendzyHelper
								.HOME_WORK_QUE_IMAGE_PREFF, "");
					}

					Bitmap bitmap = null;
					if(MathFriendzyHelper.checkForExistenceOfFile(imageName)){
						bitmap = MathFriendzyHelper.getFileByFileName(context , imageName);
						if(bitmap != null){
							this.executeMultipartPost(bitmap, imageName, "", new UploadImageRequest() {
								@Override
								public void onComplete(String response) {
									if(CommonUtils.LOG_ON)
										Log.e(TAG, "Response " + response);
									if(response != null){
										deleteUploadedImage(implObj , mimageName);
									}else{
										stopService();
										/*context.stopService(new Intent
												(context,UploadImageServiceInBackground.class));*/
									}
								}
							} , isHomeworkImage);
						}else{
							this.deleteUploadedImage(implObj , mimageName);
						}
					}else{
						this.deleteUploadedImage(implObj , mimageName);
					}
				}else{
					this.stopService();
					//context.stopService(new Intent(context,UploadImageServiceInBackground.class));
				}
				implObj.closeConnection();
			}else{
				this.stopService();
				//context.stopService(new Intent(context,UploadImageServiceInBackground.class));
			}
		}catch(Exception e){
			Log.e(TAG, "Error while uploading image outside " + e.toString());
		}
	}

	/**
	 * Stop the service
	 */
	private void stopService(){
		//Log.e(TAG, "service stop");
		context.stopService(new Intent
				(context,UploadHomeworkImages.class));
	}
	
	
	/**
	 * Delete uploaded image and restart thread
	 * @param implObj
	 * @param imageName
	 */
	private void deleteUploadedImage(HomeWorkImpl implObj, String imageName){
		implObj.deleteUploadedImageFromDB(imageName);
		this.run();
	}

	/**
	 * This method upload the images on server
	 * @param bm
	 * @param extraInfo 
	 * @param isHomeworkImage 
	 * @throws Exception
	 */
	public String executeMultipartPost(Bitmap bm , 
			String imageName , String extraInfo , UploadImageRequest request,
			boolean isHomeworkImage) throws Exception {
		String responseString = null;
		try {

			String url = ICommonUtils.UPLOAD_HOMW_WORK_IMAGE_URL;
			if(isHomeworkImage){
				url = url + "action=uploadWorkImage";
			}else{
				url = url + "action=uploadQuestionImage";
			}

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			bm.compress(CompressFormat.PNG, 90, bos);

			if(bm != null){
				bm.recycle();
				bm = null;
			}

			byte[] data = bos.toByteArray();
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost(url);

			imageName = MathFriendzyHelper.getPNGFormateImageName(imageName);

			ByteArrayBody bab = new ByteArrayBody(data, imageName);
			MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
			reqEntity.addPart("uploadedfile", bab);
            reqEntity.addPart("uploadOnS3", new StringBody("1"));
			/*reqEntity.addPart("fileName", new StringBody(imageName));
			reqEntity.addPart("mimeType", new StringBody("images/jpeg"));*/
			//reqEntity.addPart("extraInfo", new StringBody(extraInfo));

			postRequest.setEntity(reqEntity);
			HttpResponse response = httpClient.execute(postRequest);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent(), "UTF-8"));
			String sResponse;
			StringBuilder uploadResponse = new StringBuilder();

			while ((sResponse = reader.readLine()) != null) {
				uploadResponse = uploadResponse.append(sResponse);
			}

			responseString = uploadResponse.toString();
			request.onComplete(responseString);
			return uploadResponse.toString();
		} catch (Exception e) {
			Log.e(TAG, "Error while uploading " + e.getMessage());
			responseString = null;
			request.onComplete(responseString);
			return null;
		}
	}
}