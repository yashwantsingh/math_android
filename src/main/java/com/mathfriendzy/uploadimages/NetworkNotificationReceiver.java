package com.mathfriendzy.uploadimages;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import com.mathfriendzy.controller.homework.homwworkworkarea.HomeWorkWorkArea;
import com.mathfriendzy.controller.tutor.ActTutorSession;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.utils.CommonUtils;

public class NetworkNotificationReceiver extends BroadcastReceiver{

	//private final String TAG = this.getClass().getSimpleName();
	
	@Override
	public void onReceive(Context context, Intent intent) {
		/*Log.e(TAG,"inside network change");
		Log.e(TAG,intent.getExtras().toString());*/

		@SuppressWarnings("static-access")
		ConnectivityManager conMgr = (ConnectivityManager)
                context.getSystemService(context.CONNECTIVITY_SERVICE);
		NetworkInfo netInf = conMgr.getActiveNetworkInfo();
		if(netInf != null){
			/*Log.e("WHAT TYPE OF NETWORK IS THERE",netInf.getTypeName());
			Log.e("IS CONNECTED",netInf.isConnected()+"");*/
			
			if(netInf.getTypeName().equals("WIFI") || netInf.getTypeName().equals("mobile")){
				if(netInf.isConnected() || netInf.isConnectedOrConnecting()){
					if(isAirplaneModeOn(context) == false){
						//Log.e(TAG,"start service background upload");
                        performTutoringSessionOperationOnInternetConnection(true , context);
                        updateOfflineDataOnServer(context);
						context.startService(new Intent(context,UploadImageServiceInBackground.class));
					}
				}
				else{
					//Log.e(TAG, "Network Connection Lost");
                    performTutoringSessionOperationOnInternetConnection(false , context);
					context.stopService(new Intent(context,UploadImageServiceInBackground.class));
				}
			}
		}
		else{
			//Log.e(TAG, "No Network connection");
            performTutoringSessionOperationOnInternetConnection(false , context);
			context.stopService(new Intent(context,UploadImageServiceInBackground.class));
		}
	}

	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	private boolean isAirplaneModeOn(Context context) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
			return Settings.System.getInt(context.getContentResolver(), 
					Settings.System.AIRPLANE_MODE_ON, 0) != 0;          
		} else {
			return Settings.Global.getInt(context.getContentResolver(), 
					Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
		}       
	}

    /**
     * Check for internet connection and perform operation for the live drawing
     * @param isConnect
     */
    private void performTutoringSessionOperationOnInternetConnection
            (boolean isConnect , Context context){
        String myPackageName = context.getPackageName();
        String currentPackage = MathFriendzyHelper.getCurrectPackageName(context);
        if(myPackageName.equals(currentPackage)){
            String currentOpenActivity  = MathFriendzyHelper.getCurrentOpenActivityName(context);
            if(currentOpenActivity.equalsIgnoreCase(MathFriendzyHelper.HomeWorkWorkArea_Name)){
                HomeWorkWorkArea.getCurrentObj()
                        .updateTutorSessionOnInternetConnectionChange(isConnect);
            }else if(currentOpenActivity.equalsIgnoreCase(MathFriendzyHelper.ActTutorSession_Name)){
                ActTutorSession.getCurrentObj().updateTutorSessionOnInternetConnectionChange(isConnect);
            }else{
                if(CommonUtils.LOG_ON)
                    Log.e("NetworkReceiver" , "inside else in NetworkNotificationReceiver");
            }
        }else{
            if(CommonUtils.LOG_ON)
                Log.e("NetworkReceiver" , "inside else in NetworkNotificationReceiver");
        }
    }

    private void updateOfflineDataOnServer(Context context){
        MathFriendzyHelper.saveOfflineHWDataOnServer(context, new HttpServerRequest() {
            @Override
            public void onRequestComplete() {

            }
        });
    }
}
