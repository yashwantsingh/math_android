package com.mathfriendzy.uploadimages;

import android.util.Log;

import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

@SuppressWarnings("deprecation")
public class UploadPdf {

	private final static String TAG = "UploadPdf";

	public static void uploadPdf(ArrayList<String> filesUri){
		for(int i = 0 ; i < filesUri.size() ; i ++ ){
			Thread thread = new Thread(new UploadPdfThread(filesUri.get(i)));
			thread.start();
		}
	}

	/**
	 * Upload pdf
	 * @param fileUri
	 * @return
	 */
	public static String  uploadPdf(String fileUri){
		try{
			
			/*File file = new File(fileUri);
			InputStream is = new FileInputStream(file);
			HttpClient httpClient = new DefaultHttpClient();

			HttpPost postRequest = new HttpPost(ICommonUtils.UPLOAD_HW_WORK_PDF_URL 
					+ "action=uploadPdfFile");

			byte[] data = MathFriendzyHelper.convertInputStreamToByteArray(is);
			InputStreamBody isb= new InputStreamBody(
					new ByteArrayInputStream(data), file.getName());

			MultipartEntity multipartContent = new MultipartEntity();
			multipartContent.addPart("uploadedfile", isb);

			postRequest.setEntity(multipartContent);
			HttpResponse response = httpClient.execute(postRequest);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent(), "UTF-8"));
			String sResponse;
			StringBuilder uploadResponse = new StringBuilder();

			while ((sResponse = reader.readLine()) != null) {
				uploadResponse = uploadResponse.append(sResponse);
			}

			String responseString = uploadResponse.toString();
			
			if(CommonUtils.LOG_ON)
				Log.e(TAG, responseString);
            return responseString;
		 */

            File file = new File(fileUri);
            InputStream is = new FileInputStream(file);

            byte[] data = MathFriendzyHelper.convertInputStreamToByteArray(is);
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost postRequest = new HttpPost(ICommonUtils.UPLOAD_HW_WORK_PDF_URL
                    + "action=uploadPdfFile");
            ByteArrayBody bab = new ByteArrayBody(data, "application/pdf" , file.getName());
            MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            reqEntity.addPart("uploadedfile", bab);
            reqEntity.addPart("uploadOnS3", new StringBody("1"));
            postRequest.setEntity(reqEntity);
            HttpResponse response = httpClient.execute(postRequest);
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    response.getEntity().getContent(), "UTF-8"));
            String sResponse;
            StringBuilder uploadResponse = new StringBuilder();

            while ((sResponse = reader.readLine()) != null) {
                uploadResponse = uploadResponse.append(sResponse);
            }

            if(CommonUtils.LOG_ON)
                Log.e(TAG, uploadResponse.toString());

            return uploadResponse.toString();

		}catch(Exception e){
			e.printStackTrace();
			return "";
		}
	}
}
