package com.mathfriendzy.uploadimages;

public class UploadPdfThread implements Runnable{
		
	private String fileUri = null;
	
	public UploadPdfThread(String fileUri){
		this.fileUri = fileUri;
	}

	@Override
	public void run() {
		UploadPdf.uploadPdf(fileUri);
	}
}
