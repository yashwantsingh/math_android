package com.mathfriendzy.uploadimages;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;

public class UploadImageServiceInBackground extends Service{

	//private Thread threadObj = null;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		//Log.e("", "onCreate()");
		super.onCreate();
	}


	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		/*threadObj = UploadImageOnServerThread.getUploadThreadObj(this);
		//Log.e("", "isAlive " + threadObj.isAlive() + " " + threadObj.getState());
		if(!threadObj.isAlive()){
			//Log.e("", "onStartCommand()");
			threadObj.start();
		}*/

		//Log.e("", "start service");
		MathFriendzyHelper.updatepointsAndCoinsFromLocalEarnedScore
		(this, new HttpServerRequest() {
			@Override
			public void onRequestComplete() {
				MathFriendzyHelper.saveOfflineHWDataOnServer(UploadImageServiceInBackground.this,
						new HttpServerRequest() {
					@Override
					public void onRequestComplete() {
						//Log.e("", "Service completed");
						UploadImageServiceInBackground.this.stopSelf();	
					}
				});
			}
		});
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onDestroy() {
		//Log.e("", "onDestroye()" + threadObj.getState());
		/*if(threadObj != null && threadObj.isAlive()){
			Log.e("", "inside on destroyed");
			threadObj.stop();
		}*/
		super.onDestroy();
	}
}