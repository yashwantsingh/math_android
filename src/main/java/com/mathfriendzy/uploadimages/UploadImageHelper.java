package com.mathfriendzy.uploadimages;

import java.util.ArrayList;

import com.google.gson.Gson;

import android.content.Context;
import android.content.SharedPreferences;

public class UploadImageHelper {
	
	private static final String UPLOAD_IMAGE_PREFF = "com.mathfriendzy.uploadImage";
	private static final String UPLOAD_IMAGE_LIST_KEY = "uploadImageList";
	
	public static void saveImageListToUpload(Context context , ArrayList<String>  imageList){
		SharedPreferences preff = context.getSharedPreferences(UPLOAD_IMAGE_PREFF, 0);
		SharedPreferences.Editor edit = preff.edit();
		Gson gson = new Gson();
		String json = gson.toJson(imageList);
		edit.putString(UPLOAD_IMAGE_LIST_KEY, json);
		edit.commit();
	}
		
	@SuppressWarnings("unchecked")
	public static ArrayList<String> getImageListToUpload(Context context){
		SharedPreferences loginUserDataPreff = context.getSharedPreferences(UPLOAD_IMAGE_PREFF, 0);
		Gson gson = new Gson();
		String json = loginUserDataPreff.getString(UPLOAD_IMAGE_LIST_KEY, "");
		ArrayList<String>  imageList = gson.fromJson(json, ArrayList.class);
		return imageList;
	}
}
