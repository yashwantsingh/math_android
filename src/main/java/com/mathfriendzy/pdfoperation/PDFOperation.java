package com.mathfriendzy.pdfoperation;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.OnRequestSuccess;
import com.mathfriendzy.serveroperation.ServerDialogs;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;
import com.mathfriendzy.utils.MyApplication;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;

public class PDFOperation {

    private static Bitmap bitmap = null;
    public static final String ANS_SHEET_PREFF = "questions_";

    public static int OUT_OF_MEMORY = 100;

    /**
     * Get the path where file to be stored
     *
     * @param context
     * @return
     */
    public static String getPDFSavePath(Context context) {
        return Environment.getExternalStorageDirectory()
                + "/MathFriendzy/pdf";
    }

    /**
     * PDF formatted file name
     *
     * @param fileName
     * @return
     */
    public static String getPdfFormattedFileName(String fileName) {
        if (fileName.contains(".pdf"))
            return fileName;
        return fileName + ".pdf";
    }

    /**
     * Create the pdf for the given image list
     *
     * @param context
     * @param imageUrlList
     */
    public static int createPdfForImages(Context context
            , ArrayList<String> imageUrlList, String pdfName) {
        try {
            Document document = new Document();
            File file = createFile(context, pdfName);
            FileOutputStream fos = new FileOutputStream(file);
            PdfWriter.getInstance(document, fos);
            document.open();

            Rectangle rectangle = document.getPageSize();
            for (int i = 0; i < imageUrlList.size(); i++) {
                if (bitmap != null) {
                    bitmap.recycle();
                    bitmap = null;
                }

                bitmap = BitmapFactory.decodeFile(imageUrlList.get(i)
                        .replace(MathFriendzyHelper.IMAGE_FILE_PREFF, ""));
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);

                //Image image = Image.getInstance(imageUrlList.get(i));
                Image image = Image.getInstance(stream.toByteArray());
                image.scaleToFit(rectangle);
                //image.scaleToFit(rectangle.getWidth() , rectangle.getHeight());
                //image.setIndentationRight(50);
                image.setAlignment(Element.ALIGN_CENTER);
                document.add(image);
            }
            document.close();
        } catch (BadElementException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            return OUT_OF_MEMORY;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * Create file
     *
     * @param context
     * @param fileName
     * @return
     */
    private static File createFile(Context context, String fileName) {
        File fileDirectiry = new File(getPDFSavePath(context));
        if (!fileDirectiry.isDirectory())
            fileDirectiry.mkdirs();
        File file = new File(fileDirectiry, fileName);
        if (file.exists())
            file.delete();
        return file;
    }

    /**
     * Rename all pdf files
     *
     * @param context
     * @param hwId
     */
    public static void renamePdfFilesAfterGettingHWID(Context context, String hwId) {

        String filePath = getPDFSavePath(context) + "/";
        File file = new File(filePath);
        File allFiles[] = file.listFiles();
        for (int i = 0; i < allFiles.length; i++) {
            allFiles[i].renameTo(new File(filePath +
                    getChangesFileName(allFiles[i].getName(), hwId)));
        }
    }

    /**
     * Get Changes file name
     *
     * @param oldFileName
     * @param hwId
     * @return
     */
    public static String getChangesFileName(String oldFileName, String hwId) {
        if (oldFileName.startsWith(ANS_SHEET_PREFF)) {
            return hwId + "_" + oldFileName;
        }
        return oldFileName;
    }

    /**
     * Return the all pdf file list start with given HWID
     *
     * @param context
     * @param hwId
     * @return
     */
    public static ArrayList<String> getAllPdfPathStartWithGivenHWID(Context context,
                                                                    String hwId) {
        ArrayList<String> pdfList = new ArrayList<String>();
        String filePath = getPDFSavePath(context) + "/";
        File file = new File(filePath);
        File allFiles[] = file.listFiles();
        for (int i = 0; i < allFiles.length; i++) {
            if (allFiles[i].getName().startsWith(hwId)) {
                pdfList.add(allFiles[i].getAbsolutePath());
            }
        }
        return pdfList;
    }

    /**
     * Delete the unused files
     *
     * @param context
     */
    public static void deleteUnusedPdf(Context context) {
        try {
            String filePath = getPDFSavePath(context) + "/";
            File file = new File(filePath);
            File allFiles[] = file.listFiles();
            for (int i = 0; i < allFiles.length; i++) {
                if (allFiles[i].getName().startsWith(ANS_SHEET_PREFF)) {
                    allFiles[i].delete();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Check for existence of pdf file
     *
     * @param context
     * @param pdfFileName
     * @return
     */
    public static boolean checkForExistanceOfPdf(Context context, String pdfFileName) {
        if (pdfFileName != null && pdfFileName.length() > 0) {
            String pdfName = getPdfFormattedFileName(pdfFileName);
            String filePath = getPDFSavePath(context);
            File file = new File(filePath, pdfName);
            if (file.exists())
                return true;
            return false;
        }
        return false;
    }

    /**
     * Save downloaded PDF
     *
     * @param inputStream
     * @param pdfFileName
     */
    public static void saveDownloadedPdfFile(InputStream inputStream, String pdfFileName) {
        try {
            String pdfName = getPdfFormattedFileName(pdfFileName);
            String pdfDirPath = getPDFSavePath(null);

            File fileDirectiry = new File(pdfDirPath);
            if (!fileDirectiry.isDirectory())
                fileDirectiry.mkdirs();

            File file = new File(fileDirectiry, pdfName);

            if (file.exists())
                file.delete();

            FileOutputStream out = new FileOutputStream(file);

            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = inputStream.read(buffer)) > 0) {
                out.write(buffer, 0, len1);
            }
            out.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Download PDF
     *
     * @param context
     * @param pdfFileUri
     * @param pdfFileName
     * @param request
     */
    public static void downloadPdf(final Context context, final String pdfFileUri
            , final String pdfFileName, final HttpServerRequest request) {

        final String pdfName = getPdfFormattedFileName(pdfFileName);
        new AsyncTask<Void, Void, Bitmap>() {

            private ProgressDialog pd = null;

            @Override
            protected void onPreExecute() {
                pd = ServerDialogs.getProgressDialog(context, "", 1);
                pd.show();
                super.onPreExecute();
            }

            @Override
            protected Bitmap doInBackground(Void... params) {
                try {
                    InputStream inputStream = CommonUtils.getInputStream(pdfFileUri + pdfName);
                    if (inputStream != null) {
                        saveDownloadedPdfFile(inputStream, pdfName);
                    }
                    return bitmap;
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Bitmap result) {
                if (pd != null && pd.isShowing()) {
                    pd.cancel();
                }
                request.onRequestComplete();
                super.onPostExecute(result);
            }
        }.execute();
    }

    /**
     * Download PDF
     *
     * @param context
     * @param pdfFileUri
     * @param pdfFileName
     * @param request
     */
    public static void downloadPdf(final Context context, final String pdfFileUri
            , final String pdfFileName, final OnRequestSuccess request) {

        final String pdfName = getPdfFormattedFileName(pdfFileName);
        new AsyncTask<Void, Void, Bitmap>() {

            private ProgressDialog pd = null;
            private boolean isSuccess = false;

            @Override
            protected void onPreExecute() {
                pd = ServerDialogs.getProgressDialog(context, "", 1);
                pd.show();
                super.onPreExecute();
            }

            @Override
            protected Bitmap doInBackground(Void... params) {
                try {
                    InputStream inputStream = CommonUtils.getInputStream(pdfFileUri + pdfName);
                    if (inputStream != null) {
                        isSuccess = true;
                        saveDownloadedPdfFile(inputStream, pdfName);
                    }
                    return bitmap;
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Bitmap result) {
                if (pd != null && pd.isShowing()) {
                    pd.cancel();
                }
                request.onRequestSuccess(isSuccess);
                super.onPostExecute(result);
            }
        }.execute();
    }


    /**
     * Download the pdf
     *
     * @param pdfFileName
     */
    public static void downloadPdf(String pdfFileName) {
        try {
            if (CommonUtils.isInternetConnectionAvailable(MyApplication.getAppContext())) {
                if (pdfFileName != null && pdfFileName.length() > 0) {
                    final String pdfName = getPdfFormattedFileName(pdfFileName);
                    if (!PDFOperation.checkForExistanceOfPdf(null, pdfName)) {
                        final String pdfFileUri = ICommonUtils.DOWNLOAD_WORK_PDF_URL;

                        if (CommonUtils.LOG_ON)
                            Log.e("PDF Operation", "Downloading Start");

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    if (!PDFOperation.checkForExistanceOfPdf(null, pdfName)) {
                                        InputStream inputStream = CommonUtils
                                                .getInputStream(pdfFileUri + pdfName);
                                        if (inputStream != null) {
                                            if (CommonUtils.LOG_ON)
                                                Log.e("PDF Operation", "Downloading Done");
                                            saveDownloadedPdfFile(inputStream, pdfName);

											/*//for service
                                            try{
												ArrayList<String> list = PDFDownloadService
														.getSheredPreffList(MyApplication.getAppContext());
												list.remove(pdfName);
												PDFDownloadService.saveAfterRemove
												(MyApplication.getAppContext(), list);
											}catch(Exception e){
												e.printStackTrace();
											}*/
                                        }
                                    }
                                } catch (MalformedURLException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }).start();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Start downloading pdf service
     *
     * @param
     */
    public static void startPdfDownloadingService(ArrayList<String> pdfNameList) {
        try {
            if (pdfNameList != null && pdfNameList.size() > 0) {
                PDFDownloadService.startService(MyApplication.getAppContext(), pdfNameList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public static boolean isFileCorrupted(String pdfFileName , Context context){
        ProgressDialog pd = MathFriendzyHelper.getProgressDialog(context , "");
        MathFriendzyHelper.showProgressDialog(pd);
        try {
            String pdfName = getPdfFormattedFileName(pdfFileName);
            String filePath = getPDFSavePath(context);
            File file = new File(filePath, pdfName);
            CommonUtils.printLog("PDFOperation" ,
                    "File Corrupted check file path " + file.getAbsolutePath()
            + " " + file.exists() + " start " + System.currentTimeMillis());
            PdfReader pdfReader = new PdfReader(file.getAbsolutePath());
            PdfTextExtractor.getTextFromPage(pdfReader,1);
            MathFriendzyHelper.hideProgressDialog(pd);
            CommonUtils.printLog("PDFOperation" ,
                    "isFileCorrupted  outside " + System.currentTimeMillis());
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            MathFriendzyHelper.hideProgressDialog(pd);
            CommonUtils.printLog("PDFOperation" ,
                    "isFileCorrupted  outside * " + System.currentTimeMillis());
            return true;
        }
    }

       public static void isFileCorrupted(final String pdfFileName , final Context context
               , final OnRequestSuccess listener){
           new AsyncTask<Void,Void,Boolean>(){
               ProgressDialog pd = null;
               @Override
               protected void onPreExecute() {
                   pd = MathFriendzyHelper.getProgressDialog(context , "");
                   MathFriendzyHelper.showProgressDialog(pd);
                   super.onPreExecute();
               }


    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            String pdfName = getPdfFormattedFileName(pdfFileName);
            String filePath = getPDFSavePath(context);
            File file = new File(filePath, pdfName);
            CommonUtils.printLog("PDFOperation",
                    "File Corrupted check file path " + file.getAbsolutePath()
                            + " " + file.exists() + " " + System.currentTimeMillis());
            PdfReader pdfReader = new PdfReader(file.getAbsolutePath());
            PdfTextExtractor.getTextFromPage(pdfReader, 1);
            CommonUtils.printLog("PDFOperation", "isFileCorrupted outside " + System.currentTimeMillis());
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            CommonUtils.printLog("PDFOperation", "isFileCorrupted outside * " + System.currentTimeMillis());
            return true;
        }
    }

    @Override
    protected void onPostExecute(Boolean aVoid) {
        MathFriendzyHelper.hideProgressDialog(pd);
        listener.onRequestSuccess(aVoid.booleanValue());
        super.onPostExecute(aVoid);
    }
}.execute();
        }
        }
