package com.mathfriendzy.pdfoperation;

import java.util.ArrayList;

import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.utils.MyApplication;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.util.Log;

public class PDFDownloadService extends Service{

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		//ArrayList<String> pdfNameList = intent.getStringArrayListExtra("pdfNameList");
		ArrayList<String> pdfNameList = getSheredPreffList(MyApplication.getAppContext());
		Log.e("PDFDownloadService", "onStartCommand Service " + pdfNameList.size());
		for(int i = 0 ; i < pdfNameList.size() ; i ++ ){
			Log.e("PDFDownloadService", "inside loop " + i);
			PDFOperation.downloadPdf(pdfNameList.get(i));
		}
		Log.e("PDFDownloadService", "outside");
		//this.stopSelf();
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	/**
	 * Start Service
	 * @param context
	 * @param pdfNameList 
	 */
	public static void startService(Context context, ArrayList<String> pdfNameList){
		Log.e("", "inside service started " + pdfNameList.size());
		if(pdfNameList != null && pdfNameList.size() > 0){
			//savePdfListToSharedPreff(context, pdfNameList);
			Intent intent = new Intent(context , PDFDownloadService.class);
			//intent.putExtra("pdfNameList", pdfNameList);
			context.startService(intent);
		}
	}

	/**
	 * Stop Service
	 * @param context
	 */
	public static void stopService(Context context){
		context.stopService(new Intent(context , PDFDownloadService.class));
	}

	@SuppressLint("CommitPrefEdits")
	public static void savePdfListToSharedPreff(Context context , ArrayList<String> pdfList){
		SharedPreferences preff = context.getSharedPreferences
				(MathFriendzyHelper.MATH_FRENDZY_PREFF, 0);
		SharedPreferences.Editor edit = preff.edit();
		ArrayList<String> list = getSheredPreffList(context);
		if(list == null)
			list = new ArrayList<String>();
		list.addAll(pdfList);
		edit.putString("pdfList", MathFriendzyHelper.convertStringArrayListIntoStringWithDelimeter
				(list, ","));
		edit.commit();
	}

	/**
	 * Get the arraylist of pdf
	 * @param context
	 * @return
	 */
	public static ArrayList<String> getSheredPreffList(Context context){
		try{
			SharedPreferences preff = context.getSharedPreferences
					(MathFriendzyHelper.MATH_FRENDZY_PREFF, 0);
			return MathFriendzyHelper.getCommaSepratedOptionInArrayList
					(preff.getString("pdfList", "") , ",");
		}catch(Exception e){
			return null;
		}
	}
	
	/**
	 * Save after removing
	 * @param context
	 * @param pdfList
	 */
	public static void saveAfterRemove(Context context , ArrayList<String> pdfList){
		SharedPreferences preff = context.getSharedPreferences
				(MathFriendzyHelper.MATH_FRENDZY_PREFF, 0);
		SharedPreferences.Editor edit = preff.edit();
		edit.putString("pdfList", MathFriendzyHelper.convertStringArrayListIntoStringWithDelimeter
				(pdfList, ","));
		edit.commit();
	}
}
