package com.mathfriendzy.soringclasses;

import com.mathfriendzy.model.homework.CustomeAns;

import java.util.Comparator;

/**
 * Created by root on 24/2/16.
 */
public class SortByCorrectAnsForTeacher implements Comparator<CustomeAns> {

    @Override
    public int compare(CustomeAns lhs, CustomeAns rhs) {
        return rhs.getCorrectAnsCounter() - lhs.getCorrectAnsCounter();
    }
}
