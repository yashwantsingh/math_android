package com.mathfriendzy.soringclasses;

import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.homework.checkhomework.GetDetailOfHomeworkWithCustomeResponse;

import java.util.Comparator;

/**
 * Created by root on 10/8/16.
 */
public class GetStudentByGradeResponseSortByAvgSc implements Comparator<GetDetailOfHomeworkWithCustomeResponse> {
    private boolean isAscending = true;
    public GetStudentByGradeResponseSortByAvgSc(boolean isAscending){
        this.isAscending = isAscending;
    }

    @Override
    public int compare(GetDetailOfHomeworkWithCustomeResponse lhs, GetDetailOfHomeworkWithCustomeResponse rhs) {
        if(isAscending) {
            return (MathFriendzyHelper.parseInt(lhs.getAvgScore())) - (MathFriendzyHelper.parseInt(rhs.getAvgScore()));
        }else{
            return (MathFriendzyHelper.parseInt(rhs.getAvgScore())) - (MathFriendzyHelper.parseInt(lhs.getAvgScore()));
        }
    }
}
