package com.mathfriendzy.soringclasses;

import com.mathfriendzy.model.homework.checkhomework.GetDetailOfHomeworkWithCustomeResponse;

import java.util.Comparator;

/**
 * Created by root on 10/8/16.
 */
public class GetStudentByGradeResponseSortByName implements Comparator<GetDetailOfHomeworkWithCustomeResponse> {
    private boolean isAscending = true;

    public GetStudentByGradeResponseSortByName(boolean isAscending){
        this.isAscending = isAscending;
    }

    @Override
    public int compare(GetDetailOfHomeworkWithCustomeResponse lhs, GetDetailOfHomeworkWithCustomeResponse rhs) {
        if(isAscending) {
            return (lhs.getfName() + " " + lhs.getlName()).compareTo(rhs.getfName() + " " + rhs.getlName());
        }else{
            return (rhs.getfName() + " " + rhs.getlName()).compareTo(lhs.getfName() + " " + lhs.getlName());
        }
    }
}
