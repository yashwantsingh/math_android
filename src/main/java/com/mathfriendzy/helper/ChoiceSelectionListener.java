package com.mathfriendzy.helper;

/**
 * Created by root on 9/4/15.
 */
public interface ChoiceSelectionListener {
    void onCancel(int selectionIndex);
    void onOk(int selectionIndex);
}
