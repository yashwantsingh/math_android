package com.mathfriendzy.helper;

/**
 * Created by root on 19/8/15.
 */
public interface YesNoListenerInterfaceWithCross {
    void onYes();
    void onNo();
    void onCross();
}
