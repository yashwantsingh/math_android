package com.mathfriendzy.helper;

import java.util.ArrayList;

/**
 * Created by root on 4/7/16.
 */
public interface DownloadCitiesForSelectedCountryListener {
    void onCities(ArrayList<String> cities);
}
