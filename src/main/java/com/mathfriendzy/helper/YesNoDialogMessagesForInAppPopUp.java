package com.mathfriendzy.helper;

/**
 * Created by root on 3/2/16.
 */
public class YesNoDialogMessagesForInAppPopUp {
    private String mainText;
    private String limitedTimeOffer;
    private String upgradeText;


    public String getMainText() {
        return mainText;
    }

    public void setMainText(String mainText) {
        this.mainText = mainText;
    }

    public String getLimitedTimeOffer() {
        return limitedTimeOffer;
    }

    public void setLimitedTimeOffer(String limitedTimeOffer) {
        this.limitedTimeOffer = limitedTimeOffer;
    }

    public String getUpgradeText() {
        return upgradeText;
    }

    public void setUpgradeText(String upgradeText) {
        this.upgradeText = upgradeText;
    }
}
