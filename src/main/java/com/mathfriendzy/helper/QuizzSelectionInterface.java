package com.mathfriendzy.helper;

public interface QuizzSelectionInterface {
	void onPracticeSkillSelected();
	void onWordProblemSelected();
	void onCustomeHomeworkSelected();
}
