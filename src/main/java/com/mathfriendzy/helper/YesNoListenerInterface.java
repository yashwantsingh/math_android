package com.mathfriendzy.helper;

public interface YesNoListenerInterface {
	void onYes();
	void onNo();
}
