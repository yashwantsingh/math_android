package com.mathfriendzy.helper;

import com.mathfriendzy.model.homework.assignhomework.AssignHomeworkParam;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by root on 7/6/16.
 */
public class JsonCreator {

    private static final String TAG = "JsonCreator";

    /**
     * Create the json string for save homework on server
     * @param param
     * @return
     */
    public static String getSavedHomeworkJson(AssignHomeworkParam param){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("date" , param.getDate());
            jsonObject.put("grade" , param.getGrade());
            jsonObject.put("students" , new JSONArray(param.getStudentJson()));
            jsonObject.put("practiceCategories" , new JSONArray(param.getPracticeCatJson()));
            jsonObject.put("wordCategories" , new JSONArray(param.getWordCatJson()));
            jsonObject.put("customData" , new JSONArray(param.getCustomeDataJson()));
            jsonObject.put("message" , param.getMessage());
            return jsonObject.toString().replaceAll("\\\\", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
