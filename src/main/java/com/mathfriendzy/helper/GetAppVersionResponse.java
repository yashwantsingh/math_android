package com.mathfriendzy.helper;

import com.mathfriendzy.serveroperation.HttpResponseBase;

/**
 * Created by root on 13/4/16.
 */
public class GetAppVersionResponse extends HttpResponseBase{

    private String result;
    private int frequency;
    private String androidVersion;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public String getAndroidVersion() {
        return androidVersion;
    }

    public void setAndroidVersion(String androidVersion) {
        this.androidVersion = androidVersion;
    }
}
