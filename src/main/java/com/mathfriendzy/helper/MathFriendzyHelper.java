package com.mathfriendzy.helper;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.os.Debug;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.gson.Gson;
import com.mathfriendzy.R;
import com.mathfriendzy.controller.chooseavtar.ChooseAvtars;
import com.mathfriendzy.controller.helpastudent.ActHelpAStudent;
import com.mathfriendzy.controller.homework.ActCheckHomeWork;
import com.mathfriendzy.controller.homework.ActHomeWork;
import com.mathfriendzy.controller.homework.HomeWorkListAdapter;
import com.mathfriendzy.controller.homework.OffLineHomeworkResponse;
import com.mathfriendzy.controller.homework.assignhomeworkquiz.AdapterListenerForNewAssignHW;
import com.mathfriendzy.controller.homework.assignhomeworkquiz.ClassAdapterForNewAssignHW;
import com.mathfriendzy.controller.homework.checkhomeworkquiz.TeacherDetailToExport;
import com.mathfriendzy.controller.homework.homwworkworkarea.AddUrlToWorkArea;
import com.mathfriendzy.controller.homework.workimageoption.TakeScreenShotAndSaveToMemoryCallback;
import com.mathfriendzy.controller.inapp.GetMoreCoins;
import com.mathfriendzy.controller.login.LoginActivity;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.moreapp.AppDetail;
import com.mathfriendzy.controller.moreapp.MoreAppFileParser;
import com.mathfriendzy.controller.moreapp.MoreAppListener;
import com.mathfriendzy.controller.player.EditRegisteredUserPlayer;
import com.mathfriendzy.controller.player.LoginUserCreatePlayer;
import com.mathfriendzy.controller.player.LoginUserPlayerActivity;
import com.mathfriendzy.controller.player.TeacherPlayer;
import com.mathfriendzy.controller.registration.ModifyRegistration;
import com.mathfriendzy.controller.registration.RegistrationStep1;
import com.mathfriendzy.controller.resources.ActResourceHome;
import com.mathfriendzy.controller.resources.GetKhanVideoLinkParam;
import com.mathfriendzy.controller.resources.ResourceCategory;
import com.mathfriendzy.controller.resources.ResourceSearchTermParam;
import com.mathfriendzy.controller.school.SearchTeacherActivity;
import com.mathfriendzy.controller.school.SearchYourSchoolActivity;
import com.mathfriendzy.controller.signinwithgoogle.ActSignInWithGoogle;
import com.mathfriendzy.controller.tutor.ActTutorSession;
import com.mathfriendzy.controller.tutor.ChatMessageCallback;
import com.mathfriendzy.controller.tutor.TutorSessionBundleArgument;
import com.mathfriendzy.controller.webview.MyWebView;
import com.mathfriendzy.dawnloadimagesfromserver.DawnloadImagesFromserver;
import com.mathfriendzy.dawnloadimagesfromserver.DownloadImageCallback;
import com.mathfriendzy.googleanalytics.TrackerName;
import com.mathfriendzy.imageoperation.ImageOperation;
import com.mathfriendzy.listener.CheckTempPlayerListener;
import com.mathfriendzy.listener.CheckUnlockStatusCallback;
import com.mathfriendzy.listener.ClassSelectedListener;
import com.mathfriendzy.listener.EditTextFocusChangeListener;
import com.mathfriendzy.listener.GetTempPlayerUserName;
import com.mathfriendzy.listener.LoginRegisterPopUpListener;
import com.mathfriendzy.listener.OnPurchaseDone;
import com.mathfriendzy.listener.OnRequestComplete;
import com.mathfriendzy.listener.OnRequestCompleteWithStringResult;
import com.mathfriendzy.listener.OnScrollChange;
import com.mathfriendzy.listener.PhraseListListener;
import com.mathfriendzy.listener.RateUsCallback;
import com.mathfriendzy.listener.TeacherFunctionDialogListener;
import com.mathfriendzy.model.appunlock.GetAppUnlockStatusParam;
import com.mathfriendzy.model.appunlock.GetAppUnlockStatusResponse;
import com.mathfriendzy.model.assessmenttest.AssessmentStandardDto;
import com.mathfriendzy.model.assessmenttest.AssessmentTestImpl;
import com.mathfriendzy.model.assessmenttest.AssessmentTestServerOperation;
import com.mathfriendzy.model.chooseAvtar.AvtarServerOperation;
import com.mathfriendzy.model.chooseAvtar.ChooseAvtarOpration;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.country.CountryCityResponse;
import com.mathfriendzy.model.country.GetCountryCityParam;
import com.mathfriendzy.model.devicedimention.DrawDeviceDimention;
import com.mathfriendzy.model.grade.Grade;
import com.mathfriendzy.model.helpastudent.CheckForTutorParam;
import com.mathfriendzy.model.helpastudent.CheckForTutorResponse;
import com.mathfriendzy.model.homework.AddCustomeAnswerByStudentParam;
import com.mathfriendzy.model.homework.CustomeAns;
import com.mathfriendzy.model.homework.CustomePlayerAns;
import com.mathfriendzy.model.homework.CustomeResult;
import com.mathfriendzy.model.homework.GetAnswerDetailForStudentHWParam;
import com.mathfriendzy.model.homework.GetHomeWorkForStudentWithCustomeResponse;
import com.mathfriendzy.model.homework.GetWorkAreaChatMsgParam;
import com.mathfriendzy.model.homework.HWImage;
import com.mathfriendzy.model.homework.HomeWorkImpl;
import com.mathfriendzy.model.homework.HomeworkData;
import com.mathfriendzy.model.homework.PracticeResult;
import com.mathfriendzy.model.homework.PracticeResultSubCat;
import com.mathfriendzy.model.homework.SaveGDocLinkOnServerParam;
import com.mathfriendzy.model.homework.SaveHomeWorkScoreParam;
import com.mathfriendzy.model.homework.SaveHomeWorkServerResponse;
import com.mathfriendzy.model.homework.SaveHomeWorkWordPlayedDataParam;
import com.mathfriendzy.model.homework.SaveWorkAreaChatParam;
import com.mathfriendzy.model.homework.SelectAssignHomeworkClassParam;
import com.mathfriendzy.model.homework.UpdateInputStatusForWorkAreaParam;
import com.mathfriendzy.model.homework.WordResult;
import com.mathfriendzy.model.homework.WordSubCatResult;
import com.mathfriendzy.model.homework.assignhomework.AssignHomeworkParam;
import com.mathfriendzy.model.homework.assignhomework.GetStudentForSchoolClassesParam;
import com.mathfriendzy.model.homework.checkhomework.GetDetailOfHomeworkWithCustomeResponse;
import com.mathfriendzy.model.homework.checkhomework.SaveCheckTeacherChangesParam;
import com.mathfriendzy.model.homework.checkhomework.UpdateCreditByTeacherForStudentQuestionParam;
import com.mathfriendzy.model.homework.savehomework.SaveHomeworkParam;
import com.mathfriendzy.model.inapp.UpdateUserCoinsParam;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.CoinsFromServerObj;
import com.mathfriendzy.model.learningcenter.LearningCenteServerOperation;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.MathResultTransferObj;
import com.mathfriendzy.model.learningcenter.PlayerDataFromServerObj;
import com.mathfriendzy.model.learningcenter.PlayerEquationLevelObj;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.learningcenter.PurchaseItemObj;
import com.mathfriendzy.model.login.Login;
import com.mathfriendzy.model.login.PurchasedAvtarDto;
import com.mathfriendzy.model.multifriendzy.MultiFriendzyServerOperation;
import com.mathfriendzy.model.player.temp.GetUserName;
import com.mathfriendzy.model.player.temp.TempPlayer;
import com.mathfriendzy.model.player.temp.TempPlayerOperation;
import com.mathfriendzy.model.professionaltutoring.AddPurchaseTimeParam;
import com.mathfriendzy.model.professionaltutoring.GetPlayerPurchaseTimeParam;
import com.mathfriendzy.model.professionaltutoring.PurchaseTimeCallback;
import com.mathfriendzy.model.professionaltutoring.ShouldPaisTutorResponse;
import com.mathfriendzy.model.professionaltutoring.UpdatePurchaseTimeResponse;
import com.mathfriendzy.model.ratetutorsession.RateTutorSessionCallback;
import com.mathfriendzy.model.ratetutorsession.RateTutorSessionParam;
import com.mathfriendzy.model.registration.OnRegistration;
import com.mathfriendzy.model.registration.Register;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.Registration;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.model.registration.classes.ClassWithName;
import com.mathfriendzy.model.registration.classes.GetSchoolClassResponse;
import com.mathfriendzy.model.registration.classes.GetSchoolClassesParam;
import com.mathfriendzy.model.resource.GetResourceCategoriesParam;
import com.mathfriendzy.model.resource.GetResourceVideoInAppResult;
import com.mathfriendzy.model.resource.GetResourceVideoInAppStatusParam;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.CategoryListTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.DownloadCategoriesForGradesParam;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.DownloadCategoriesForGradesResponse;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.LearnignCenterSchoolCurriculumServerOperation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.QuestionLoadedTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SchoolCurriculumLearnignCenterimpl;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SubCatergoryTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.UpdatedInfoTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.WordProblemLevelTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.WordProblemQuestionTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.wordProblemLevelDetailTransferObj;
import com.mathfriendzy.model.schoolcurriculum.singlefriendzy.SingleFriendzyServerOperationForWordProblem;
import com.mathfriendzy.model.schools.SchoolDTO;
import com.mathfriendzy.model.schools.TeacherDTO;
import com.mathfriendzy.model.singleFriendzy.SingleFriendzyServerOperation;
import com.mathfriendzy.model.spanishchanges.SpanishChangesImpl;
import com.mathfriendzy.model.states.States;
import com.mathfriendzy.model.tutor.AddTimeSpentInTutorSessionParam;
import com.mathfriendzy.model.tutor.ChatMessageModel;
import com.mathfriendzy.model.tutor.GetChatRequestParam;
import com.mathfriendzy.model.tutor.GetChatUserNameCallback;
import com.mathfriendzy.model.tutor.GetDrawingPointForChatOnSuccess;
import com.mathfriendzy.model.tutor.GetDrawingPointForChatParam;
import com.mathfriendzy.model.tutor.GetDrawingPointForChatResponse;
import com.mathfriendzy.model.tutor.GetMessagesForTutorRequestParam;
import com.mathfriendzy.model.tutor.GetNewChatUserNameResponse;
import com.mathfriendzy.model.tutor.PhraseCatagory;
import com.mathfriendzy.model.tutor.QBLoginSuccessCallback;
import com.mathfriendzy.model.tutor.RegisterUserOnQBCallback;
import com.mathfriendzy.model.tutor.RegisterUserToQB;
import com.mathfriendzy.model.tutor.SaveMessageForSingleChatCallback;
import com.mathfriendzy.model.tutor.SaveMessageForSingleChatParam;
import com.mathfriendzy.model.tutor.SaveMessageForSingleChatResponse;
import com.mathfriendzy.model.tutor.UpdateChatIdParam;
import com.mathfriendzy.model.tutor.UpdateDrawPointsForPlayerParam;
import com.mathfriendzy.notification.AddUserWithAndroidDevice;
import com.mathfriendzy.pdfoperation.PDFOperation;
import com.mathfriendzy.quickblox.QBCreateSession;
import com.mathfriendzy.quickblox.qbcallbacks.QBCreateSessionCallback;
import com.mathfriendzy.quickblox.qbcallbacks.QBOnLoginSuccess;
import com.mathfriendzy.serveroperation.FileParser;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.serveroperation.MyAsyckTask;
import com.mathfriendzy.serveroperation.ServerDialogs;
import com.mathfriendzy.serveroperation.ServerOperation;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.serveroperation.serveroperationparamresponseclasses.GetActiveInActiveStatusOfStudentForTutorResponse;
import com.mathfriendzy.socketprograming.ClientCallback;
import com.mathfriendzy.socketprograming.MyClientTask;
import com.mathfriendzy.socketprograming.MyGlobalWebSocket;
import com.mathfriendzy.socketprograming.MyWebSocket;
import com.mathfriendzy.uploadimages.UploadHomeworkImages;
import com.mathfriendzy.uploadimages.UploadImage;
import com.mathfriendzy.uploadimages.UploadImageRequest;
import com.mathfriendzy.utils.BuildApp;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DateObj;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ICommonUtils;
import com.mathfriendzy.utils.ITextIds;
import com.mathfriendzy.utils.MathVersions;
import com.mathfriendzy.utils.MyApplication;
import com.mathfriendzy.utils.RegisterDeviceOnServer;
import com.mathfriendzy.utils.Validation;
import com.mathfriendzy.utils.VedioUrls;
import com.mathfriendzy.youtubeplayer.ActYouTubePlaye;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.qb.gson.reflect.TypeToken;
import com.quickblox.auth.model.QBSession;
import com.quickblox.users.model.QBUser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;
import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_ID;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;

public class MathFriendzyHelper {

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    public static final String MATH_FRENDZY_PREFF = "mathFriendzyPreff";
    private static final String TAG = "MathFriendzyHelper";

    public static final String STUDENT_MSG_PREFF = "Student";
    public static final String TEACHER_MSG_PREFF = "Teacher";
    public static final String FULL_MESSAGE_DELIMETER = "\\$\\$\\$";
    public static final String STUDENT_TEACHER_MSG_DELIMETER = "&&&";
    public static final String MSG_DELIMETER = "$$$";
    public static final String COLON_DELIMETER = ":";

    private static final int KITKAT = 19;

    //for Animation 
    private static Animation animation = null;

    public static String IMAGE_FILE_PREFF = "file://";

    private static final int HALF_CREDIT = 3;
    private static final int NONE_CREDITT = 1;
    private static final int FULL_CREDIT = 4;
    //private static final int ZERO_CREDIT = 2;

    //for upload offline homework images on server when net is connected
    public static String HOME_WORK_IMAGE_PREFF = "HW_";
    public static String HOME_WORK_QUE_IMAGE_PREFF = "QUE_";

    public static final String YYYY_MM_DD = "yyyy-MM-dd";

    //fonts Uri;
    public static final String DK_COLL_FONT_URI_IN_ASSETS = "font/dkcoolcryan.ttf";

    //pop up dialog constans
    //for teacher function
    public static final String DIALOG_PRACTICE_REPORT = "dialogPracticeReport";
    public static final String DIALOG_ASSESSMENT_REPORT = "dialogAssessmentReport";
    public static final String DIALOG_ASSESSMENT_REPORT_BY_CLASS = "dialogAssessmentReportByClass";
    public static final String DIALOG_ASSIGN_HOMEWORK = "dialogAssignHomework";
    public static final String DIALOG_CHECK_HOMEWORK = "dialogCheckHomework";
    public static final String DIALOG_CREATE_CHALLENGE = "dialogCreateChallenge";
    public static final String DIALOG_STUDENT_ACCOUNT = "dialogStudentAccount";
    public static final String DIALOG_LEARNING_CENTER = "dialogLearningCenter";
    public static final String DIALOG_SINGLE_FRIENDZY = "dialogSingleFriendzy";
    public static final String DIALOG_MULTI_FRIENDZY = "dialogMultiFriendzy";
    public static final String DIALOG_SCHOOL_FUNCTION = "dialogSchoolFunction";
    public static final String DIALOG_MANAGE_TUTOR = "dialogManageTutor";
    //for school function
    public static final String DIALOG_SCHOOL_ASSESSMENT_TEST = "dialogSchoolAssessmentTest";
    public static final String DIALOG_SCHOOL_SCHOOL_CHALLENGE = "dialogSchoolSchoolChallenge";
    public static final String DIALOG_SCHOOL_HOMEWORK_QUIZZ = "dialogSchoolHomeworkQuizz";
    public static final String DIALOG_SCHOOL_STUDY_GROUP = "dialogSchoolStudyGroup";
    public static final String DIALOG_SCHOOL_HELP_STUDENT = "dialogSchoolHelpStudemt";
    public static final String DIALOG_TUTORING_SESSION = "dialogTutoringSessions";
    public static final String DIALOG_RESOURCES = "dialogResources";

    //for Main Screen
    public static final String DIALOG_PROFESSIONAL_TUTORING = "dialogProfesssionalTutoring";
    //end constants

    //for new registration constants
    public static final String UNITED_STATE = "United States";
    public static final String CANADA = "Canada";
    public static final String PUERTO_RICO = "Puerto Rico";
    public static final int SELECT_SCHOOL_REQUEST_CODE = 11001;
    public static final int SELECT_TEACHER_REQUEST_CODE = 11002;
    public static final String HOME_SCHOOL = "Home School";
    public static final String ADULT = "Adult";
    public static final String ENG_LANGUAGE_ID = "1";
    public static final String ENG_LANGuAGE_CODE = "EN";
    public static final String GRADE_TEXT = "Select a Grade";
    //public static final String GRADE_TEXT = "1";
    public static final int ADULT_GRADE = 13;
    public static final int NO_GRADE_SELETED = -1;
    public static final String US_DEFAULT_STATE = "California";
    public static final String CANADA_DEFAULT_STATE = "Alberta";
    public static final String DEFAULT_PROFILE_IMAGE = "Smiley";
    public static final String DEFAULT_US_CITY = "San Diego";
    public static final String DEFAULT_US_ZIP = "92101";
    public static final String DEFAULT_CANADA_CITY = "Corner Brook";
    public static final String DEFAULT_CITY = "Delhi";
    private static ProgressDialog pd = null;
    public static final int MAX_NUMBER_OF_PLAYER = 4;

    //for new unlock status
    public static final int APP_UNLOCK_ID = 100;//id for full app unlock
    public static final int APP_UNLOCK = 1;

    //for scrolling
    private final static long INITAIL_DELAY_TIME = 0;
    private final static long REPEAT_DELAY_TIME = 100;
    private final static int SCROLL_UP_DWON = 80;
    public static final int SCROLL_UP_COMPLETE = 1;
    public static final int SCROLL_DOWN_COMPLETE = 2;
    public static final int ON_SCROLLING = 3;

    //for youtube vedio change format to embad
    private static String REPLACE_TEXT = "watch?v=";
    private static String REPLACE_BY_TEXT = "embed/";

    //for start find tutor activity from fragment
    public static final int FIND_TUTOR_REQUEST_FROM_FRAGMENT = 1004;

    public static final int ACTIVE_INACTIVE_UPDATE_STATUE_TIME = 10 * 1000;

    public static final int YES = 1;
    public static final int NO = 0;
    public static final String SUCCESS = "success";
    public static final String ALL = "All";

    //for account type
    public static final int TEACHER = 1;
    public static final int STUDENT = 2;
    public static final int PARENT = 3;

    //professional tutoring change
    public static final int START_PURCHASE_SCREEN_REQUEST_CODE = 90001;
    //end professional tutoring change

    public static final String HOUSE_ADS_IMAGE_NAME = "House_Ads_Image_Name";
    public static final String HOUSE_ADS_LINK_URL = "House_Ads_Link_Url";
    private static final String DEFAULT_HOUSE_ADS_IMAGE_NAME = "768 x 1024.png";
    private static final String DEFAULT_HOUSE_ADS_LINK_URL = "www.MathTutorPlus.com";

    /**
     * Grade text which add at the top of the grade list in the new registraion
     *
     * @param contex
     * @return
     */
    public static String getGradeText(Context contex) {
        return MathFriendzyHelper.GRADE_TEXT;
    }

    //activity name
    public static String ActHelpAStudent_Name = "com.mathfriendzy.controller.helpastudent.ActHelpAStudent";
    public static String HomeWorkWorkArea_Name =
            "com.mathfriendzy.controller.homework.homwworkworkarea.HomeWorkWorkArea";
    public static String ActTutorSession_Name = "com.mathfriendzy.controller.tutor.ActTutorSession";
    public static String ActCheckHomework = "com.mathfriendzy.controller.homework.ActCheckHomeWork";
    public static String ActTutoringSession = "com.mathfriendzy.controller.tutoringsession.ActTutoringSession";

    private static Handler handler = null;

    public static int DRAW_LINE_THICKNESS = 5;//motai
    private static Bitmap createdBitmapForDrawing = null;

    public static int drawViewHeight = 0;
    public static int drawViewWidth = 0;
    public static final int MAX_DRAW_VIEW_WIDTH = 900;

    /*public static final String PROFESSIONAL_TUTORING_WATCH_VEDIO_URL =
            "https://www.youtube.com/watch?v=ZPm3Vufh4u4";*/
    public static final String PROFESSIONAL_TUTORING_WATCH_VEDIO_URL =
            "https://www.youtube.com/watch?v=H90FAVFsSMM";

    public static final String HOUSE_ADS_FREQUENCY_KEY = "HOUSE_ADS_FREQUENCY_KEY";
    public static final String RATE_ADS_FREQUENCY_KEY = "RATE_ADS_FREQUENCY_KEY";
    private static boolean isSubscriptionPurchase = false;
    public static final String SHOW_RATE_US_POP_UP_KEY = "rateUsPopUp";

    public static final String MATH_FRIENDZY_WEB_URL = ICommonUtils.HTTP + "://www.MathFriendzy.com";
    public static final String HOMEWORK_FRIENDZY_WEB_URL = ICommonUtils.HTTP +"://www.HomeworkFriendzy.com";
    public static final String MATH_TUTOR_PLUS_WEB_URL = ICommonUtils.HTTP + "://www.MathTutorPlus.com";

    public static final int MAX_ASSUME_SCHOOL_ID = 100;
    //for create pattern
    /**
     * (			# Start of group
     * (?=.*\d)		#   must contains one digit from 0-9
     * (?=.*[a-z])	#   must contains one lowercase characters
     * (?=.*[A-Z])	#   must contains one uppercase characters
     * (?=.*[@#$%])	#   must contains one special symbols in the list "@#$%"
     * .		#   match anything with previous condition checking
     * {6,20}	    #   length at least 6 characters and maximum of 20
     * )			# End of group
     * <p/>
     * <p/>
     * ((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})
     * <p/>
     * ?= – means apply the assertion condition, meaningless by itself,
     * always work with other combination
     */
    private static final String REGISTRATION_STRING_PATTERN = "(?=.*[\\\\<>\"&'])";
    private static ArrayList<String> profanityWordList = CommonUtils.getProfanityWordList();

    public static boolean isDeviceSizeGreaterThanDraViewLimit = false;


    //professional tutoring changes
    public static final String IS_ALREADY_PAID_TUTOR_SHOW_DIALOG_KEY = "isPaideTutorDialogShow";
    public static final String LAST_TIME_TO_SHOW_FULL_SCREEN_ADS = "lastTimeToShowFullScreenAds";
    //end professional tutoring changes


    public static String MATH_TITLE_IDENTIFIER = "mfTitleHomeScreen";
    public static String MATH_PLUS_TITLE_IDENTIFIER = "lblMFPTitle";
    public static String MATH_TUTOR_PLUS_TITLE_IDENTIFIER = "lblMTPTitle";

    private static String MATH_FRIENDZY = "MathFriendzy";
    private static String MATH_PLUS = "School Friendzy";
    private static String MATH_TUTOR_PLUS = "MathTutorPlus";

    //for phrase list
    private static String PHRASE_LIST_SHARED_PREFF_KEY_FOR_TUTOR = "phraseListKey";
    private static String PHRASE_LIST_SHARED_PREFF_KEY_FOR_STUDENT = "phraseListKeyForStudent";
    private static String PHRASE_LIST_SAVE_DATE_KEY_FOR_TUTOR = "phraseListSaveDateKey";
    private static String PHRASE_LAST_SAVE_DATE_KEY_FOR_TUTOR = "phraseLastSaveDateKey";
    private static String PHRASE_LIST_SAVE_DATE_KEY_FOR_STUDENT = "phraseListSaveDateKeyForStudent";
    private static String PHRASE_LAST_SAVE_DATE_KEY_FOR_STUDENT = "phraseLastSaveDateKeyForStudent";

    public static final int COMPRESS_IMAGE_PERCENTAGE = 90;


    public static final String MATH_SIMPLE_PACKAGE_NAME = "com.mathfriendzy";
    public static final String MATH_PLUS_PACKAGE_NAME = "com.mathfriendzy_plus";
    public static final String MATH_TUTOR_PLUS_PACKAGE_NAME = "com.math_tutor_plus";
    public static final String CURRENT_APP_VERSION_KEY = "com.mathfriendzy.currentappversionkey";
    public static final String RESOURCE_PURCHASE_STATUS_KEY = "com.mathfriendzy.resource.purchase.key";
    public static final String UNLOCK_CATEGORY_PURCHASE_STATUS_KEY = "com.mathfriendzy.unlock_category.purchase.key";


    //New InApp Changes
    public static final int NO_RESPONSE_FROM_SERVER_REQUEST_CODE = -1;
    public static String GET_APP_UNLOCK_STATUS_DOWNLOAD_KEY =
            "com.mathfriendzy.app_unlock_status_download_date";
    public static final int MAX_USER_COINS_TO_UNLOCK_APP_CATEGORY = 10000;

    public static void saveBooleanInSharedPreff(Context context , String key , boolean bValue){
        SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
        SharedPreferences.Editor editor = sharedPreff.edit();
        editor.putBoolean(key, bValue);
        editor.commit();
    }

    public static boolean getBooleanValueFromSharedPreff(Context context , String key){
        return context.getSharedPreferences(MATH_FRENDZY_PREFF, 0)
                .getBoolean(key, false);
    }

    public static void saveStringInSharedPreff(Context context , String key , String bValue){
        SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
        SharedPreferences.Editor editor = sharedPreff.edit();
        editor.putString(key, bValue);
        editor.commit();
    }

    public static String getStringValueFromSharedPreff(Context context , String key){
        return context.getSharedPreferences(MATH_FRENDZY_PREFF, 0)
                .getString(key, null);
    }

    /**
     * Return the App Version
     * @param context
     * @return
     */
    public static String getAppVersionTextForTesting(Context context){
        //return "000" + "062416" + ICommonUtils.LIVE_API;//000MMDDYY
        return "";
    }

    /**
     * Check is free subscription active
     *
     * @param context
     * @return
     */
    public static boolean isFreeSubscriptionDialogShown(Context context) {
        return context.getSharedPreferences(MATH_FRENDZY_PREFF, 0)
                .getBoolean("freeSubscription", false);
    }

    /**
     * Save free subscription values
     *
     * @param context
     * @param bValue
     */
    public static void saveFreeSubscriptionStatus(Context context, boolean bValue) {
        SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
        SharedPreferences.Editor editor = sharedPreff.edit();
        editor.putBoolean("freeSubscription", bValue);
        editor.commit();
    }

    /**
     * Open the Url
     *
     * @param context
     * @param url
     */
    public static void openUrl(Context context, String url) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            if(isAnyAppAvailableToHandleIntent(context , intent)) {
                context.startActivity(intent);
            }else{
                MathFriendzyHelper.showWarningDialog(context ,
                        "No browser install in your device to open this url , Please install any browser.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Play youtube vedios into embadded format vedios
     *
     * @param context
     * @param url
     */
    public static void converUrlIntoEmbaddedAndPlay(Context context, String url) {
        try {
            if(url.contains("&")){
                url = url.substring(0 , url.indexOf("&"));
            }
            url = url.replace(REPLACE_TEXT, REPLACE_BY_TEXT);
            url = url + "?rel=0&controls=0&showinfo=0&modestbranding=1&frameborder=0&allowfullscreen";
            Intent intent = new Intent(context, ActYouTubePlaye.class);
            intent.putExtra("url", url);
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * OPen url in webview
     * @param context
     * @param url
     */
    public static void openUrlInWebView(Context context, String url){
        try {
            if(url.contains("youtube")){
                converUrlIntoEmbaddedAndPlay(context , url);
                return ;
            }
            Intent intent = new Intent(context, MyWebView.class);
            intent.putExtra("url", url);
            context.startActivity(intent);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * OPen url in webview
     * @param context
     * @param url
     */
    public static void openUrlInWebViewForResult(Context context, String url
            , int requestCode , boolean isForPayment){
        try {
            url = url.replace(REPLACE_TEXT, REPLACE_BY_TEXT);
            Intent intent = new Intent(context, MyWebView.class);
            intent.putExtra("url", url);
            intent.putExtra("isForPayment" , isForPayment);
            ((Activity)context).startActivityForResult(intent ,requestCode);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Refresh the cache
     *
     * @param context
     */
    public static void refreshCache(Context context) {
        ((Activity) context).sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
                Uri.parse("file://" + Environment.getExternalStorageDirectory())));
    }

    /**
     * This method return the selected player id , if exist otherwise return blank id
     *
     * @param context
     * @return
     */
    public static String getSelectedPlayerID(Context context) {
        return context.getSharedPreferences(IS_CHECKED_PREFF, 0)
                .getString(ICommonUtils.PLAYER_ID, "");
    }

    /**
     * Check for work expire date to the current date
     *
     * @param dueDate
     * @return
     */
    public static boolean isDateExpire(String dueDate) {
        Date dueWorkDate = MathFriendzyHelper.getValidateDataForHomeWork(dueDate);
        if (dueWorkDate.compareTo(MathFriendzyHelper.getCurrentDateForHomwWork()) >= 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * This method return the Seletced player data
     *
     * @param context
     * @param playerId
     * @return
     */
    public static UserPlayerDto getSelectedPlayerDataById(Context context, String playerId) {
        UserPlayerOperation userPlayerOpr = new UserPlayerOperation(context);
        UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(playerId);
        return userPlayerData;
    }

    /**
     * Show login registration dialog
     *
     * @param context
     */
    public static void showLoginRegistrationDialog(Context context, String msg) {
        DialogGenerator dg = new DialogGenerator(context);
        dg.generateRegisterOrLogInDialog(msg);
    }

    /**
     * Return the current data
     *
     * @return
     */
    @SuppressLint("SimpleDateFormat")
    public static Date getCurrentDateForHomwWork() {
        Date date = new Date();
        DateFormat df = new SimpleDateFormat("MM-dd-yy");
        try {
            return df.parse(df.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the string into int
     *
     * @param stringValue
     * @return
     */
    public static int parseInt(String stringValue) {
        try {
            return Integer.parseInt(stringValue);
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * This method check for user is login or not
     *
     * @param context
     * @return
     */
    public static boolean isUserLogin(Context context) {
        return context.getSharedPreferences(LOGIN_SHARED_PREFF, 0)
                .getBoolean(IS_LOGIN, false);
    }

    /**
     * Validate the date
     *
     * @param date
     * @return
     */
    @SuppressLint("SimpleDateFormat")
    public static Date getValidateDataForHomeWork(String date) {
        try {
            return new SimpleDateFormat("MM-dd-yy").parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Validate the date in given formate
     *
     * @param date
     * @param format
     * @return
     */
    @SuppressLint("SimpleDateFormat")
    public static Date getValidateDate(String date, String format) {
        try {
            if(date != null)
                return new SimpleDateFormat(format).parse(date);
            return null;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Validate the date in given formate
     *
     * @param date
     * @param format
     * @return
     */
    @SuppressLint("SimpleDateFormat")
    private static Date getValidateDateForTimeZone(String date, String format) {
        try {
            SimpleDateFormat formate = new SimpleDateFormat(format);
            formate.setTimeZone(TimeZone.getTimeZone("PDT"));
            return formate.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Format Date into YYYY-MM-DD Format , given date into MM-DD-YYYY
     */
    @SuppressLint("SimpleDateFormat")
    public static String formatDataInYYYYMMDD(String date) {
        try {
            SimpleDateFormat givenFormat = new SimpleDateFormat("MM-dd-yyyy");
            SimpleDateFormat convertedFomate = new SimpleDateFormat("yyyy-MM-dd");
            return convertedFomate.format(givenFormat.parse(date));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param date           - date
     * @param currentFormate - current date formate
     * @param requiredFormat - new required date formate
     * @return
     */
    @SuppressLint("SimpleDateFormat")
    public static String formatDataInGivenFormat(String date,
                                                 String currentFormate, String requiredFormat) {
        try {
            SimpleDateFormat givenFormat = new SimpleDateFormat(currentFormate);
            SimpleDateFormat convertedFomate = new SimpleDateFormat(requiredFormat);
            return convertedFomate.format(givenFormat.parse(date));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param date           - date
     * @param currentFormate - current date formate
     * @param requiredFormat - new required date formate
     * @return
     */
    @SuppressLint("SimpleDateFormat")
    public static String formatDataInGivenFormatWithDefaultTimeZone(String date,
                                                                    String currentFormate, String requiredFormat) {
        try {
            SimpleDateFormat givenFormat = new SimpleDateFormat(currentFormate);
            givenFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            SimpleDateFormat convertedFomate = new SimpleDateFormat(requiredFormat);
            convertedFomate.setTimeZone(TimeZone.getDefault());
            return convertedFomate.format(givenFormat.parse(date));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Return the string in the given formate
     *
     * @param formate
     * @return
     */
    public static String getCurrentDateInGiveGformateDate(String formate) {
        try {
            SimpleDateFormat convertedFomate = new SimpleDateFormat(formate);
            return convertedFomate.format(new Date());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Return the string in the given formate
     *
     * @param formate - Formath of the date which you want
     * @param - numberOfMonts -  number of months which you want to add + for next month and - for prev month
     * @param - numberOfYear -  number of year which you want to add + for next month and - for prev month
     * @return
     */
    public static String getBackAndForthDateInGiveGformateDate(String formate ,
                                                               int numberOfMonths , int numberOfYear) {
        try {
            Calendar calender = MathFriendzyHelper.getCurrentCalender();
            calender.add(Calendar.MONTH, numberOfMonths);
            calender.add(Calendar.YEAR, numberOfYear);
            SimpleDateFormat convertedFomate = new SimpleDateFormat(formate);
            return convertedFomate.format(calender.getTime());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Return the string in the given formate
     *
     * @param format - Format of the date which you want
     * @param - numberOfMonts -  number of months which you want to add + for next month and - for prev month
     * @param - numberOfYear -  number of year which you want to add + for next month and - for prev month
     * @return
     */
    public static String getBackAndForthDateInGivenFormatDate(String format,
                                                              int numberOfMonths,
                                                              int numberOfYear,
                                                              int numberOfDays) {
        try {
            Calendar calender = MathFriendzyHelper.getCurrentCalender();
            calender.add(Calendar.MONTH, numberOfMonths);
            calender.add(Calendar.YEAR, numberOfYear);
            calender.add(Calendar.DATE, numberOfDays);
            SimpleDateFormat convertedFomate = new SimpleDateFormat(format);
            return convertedFomate.format(calender.getTime());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Return the string in the given formate
     *
     * @param formate
     * @return
     */
    public static String getCurrentDateInGiveGformateDateWithDefaultTimeZone(String formate) {
        try {
            SimpleDateFormat convertedFomate = new SimpleDateFormat(formate);
            convertedFomate.setTimeZone(TimeZone.getTimeZone("GMT"));
            return convertedFomate.format(new Date());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * Return the current date in given format
     *
     * @param formate
     * @return
     */
    @SuppressLint("SimpleDateFormat")
    private static Date getCurrentDate(String formate) {
        Date date = new Date();
        DateFormat df = new SimpleDateFormat(formate);
        try {
            df.setTimeZone(TimeZone.getTimeZone("PDT"));
            return df.parse(df.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * This method save the number of active home work
     *
     * @param context
     * @param userId
     * @param playerId
     * @param numberOfActiveHomeWork
     */
    public static void saveNumberOfActiveHomeWork(Context context, String userId, String playerId
            , int numberOfActiveHomeWork) {
        SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
        SharedPreferences.Editor editor = sharedPreff.edit();
        editor.putString("ActiveHomeWorkDetail", userId + "-" + playerId + "-"
                + numberOfActiveHomeWork);
        editor.commit();
    }

    /**
     * This method save the number of active home work
     *
     * @param context
     * @param userId
     * @param playerId
     * @param numberOfActiveHomeWork
     */
    public static void saveNumberOfActiveHomeWorkFromServer(Context context,
                                                            String userId, String playerId
            , int numberOfActiveHomeWork) {
        SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
        SharedPreferences.Editor editor = sharedPreff.edit();
        editor.putString("activeHomeworkFromServer", userId + "-" + playerId + "-"
                + numberOfActiveHomeWork);
        editor.commit();
    }

    /**
     * This method return the number of active player
     *
     * @param context
     * @param userId
     * @param playerId
     * @return
     */
    public static int getNumberOfActiveHomeWorkFromServer(Context context, String userId, String playerId) {
        String activeWorkDetail = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0)
                .getString("activeHomeworkFromServer", null);
        if (activeWorkDetail != null && activeWorkDetail.length() > 0) {
            if (userId.equals(activeWorkDetail.split("-")[0])
                    && playerId.equals(activeWorkDetail.split("-")[1]))
                return Integer.parseInt(activeWorkDetail.split("-")[2]);
            return 0;
        } else {
            return 0;
        }
    }


    /**
     * This method return the number of active player
     *
     * @param context
     * @param userId
     * @param playerId
     * @return
     */
    public static int getNumberOfActiveHomeWork(Context context, String userId, String playerId) {
        String activeWorkDetail = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0)
                .getString("ActiveHomeWorkDetail", null);
        if (activeWorkDetail != null && activeWorkDetail.length() > 0) {
            if (userId.equals(activeWorkDetail.split("-")[0])
                    && playerId.equals(activeWorkDetail.split("-")[1]))
                return Integer.parseInt(activeWorkDetail.split("-")[2]);
            return 0;
        } else {
            return 0;
        }
    }

    /**
     * This method get categories detail from database according to the user grade
     *
     * @param grade
     */
    public static ArrayList<CategoryListTransferObj> getCategories(int grade, Context context) {
        ArrayList<CategoryListTransferObj> categoryList = new ArrayList<CategoryListTransferObj>();
        //changes for Spanish
        if (CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH) {
            SchoolCurriculumLearnignCenterimpl schoolImpl =
                    new SchoolCurriculumLearnignCenterimpl(context);
            schoolImpl.openConnection();
            categoryList = schoolImpl.getCategoriesForSchoolCurriculum(grade);
            schoolImpl.closeConnection();
        } else {
            SpanishChangesImpl schoolImpl = new SpanishChangesImpl(context);
            schoolImpl.openConn();
            categoryList = schoolImpl.getCategoriesForSchoolCurriculum(grade);
            schoolImpl.closeConn();
        }
        return categoryList;
    }

    /**
     * This method download the school categories
     *
     * @param grade
     * @param lang
     * @param context
     * @return
     */
    public static QuestionLoadedTransferObj downLoadSchoolCategories(int grade, int lang,
                                                                     Context context) {

        LearnignCenterSchoolCurriculumServerOperation serverObj =
                new LearnignCenterSchoolCurriculumServerOperation();
        QuestionLoadedTransferObj questionData = serverObj.getCategoriesList(grade, lang);

        if (questionData != null) {
            SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl =
                    new SchoolCurriculumLearnignCenterimpl(context);
            schooloCurriculumImpl.openConnection();

            if (lang == CommonUtils.ENGLISH) {
                //changes for dialog loading wheel
                schooloCurriculumImpl.deleteFromWordProblemCategoriesbyGrade(grade);
                schooloCurriculumImpl.deleteFromWordProblemsSubCategoriesByCategoryId
                        (questionData.getCategoryList());
                schooloCurriculumImpl.insertIntoWordProblemCategories(questionData.getCategoryList(), grade);
                schooloCurriculumImpl.insertIntoWordProblemsSubCategories
                        (questionData.getCategoryList());
            } else {
                SpanishChangesImpl implObj = new SpanishChangesImpl(context);
                implObj.openConn();
                implObj.deleteFromWordProblemCategoriesbyGrade(grade);
                implObj.deleteFromWordProblemsSubCategoriesByCategoryId
                        (questionData.getCategoryList());
                implObj.insertIntoWordProblemCategories(questionData.getCategoryList(), grade);
                implObj.insertIntoWordProblemsSubCategories(questionData.getCategoryList());
                implObj.closeConn();
            }
            schooloCurriculumImpl.closeConnection();
        }
        return questionData;
    }

    /**
     * This method return the categories list
     *
     * @param context
     * @return
     */
    public static ArrayList<LearningCenterTransferObj> getPracticeSkillCategories(Context context) {
        LearningCenterimpl laerningCenter = new LearningCenterimpl(context);
        laerningCenter.openConn();
        ArrayList<LearningCenterTransferObj> laernignCenterFunctionsList =
                laerningCenter.getLearningCenterFunctions();
        laerningCenter.closeConn();
        return laernignCenterFunctionsList;
    }

    /**
     * Return the learning center practice data
     *
     * @param catId
     * @return
     */
    public static LearningCenterTransferObj getPracticeSkillCatDataByCatId
    (Context context, String catId) {
        LearningCenterTransferObj laerningCenterObj = new LearningCenterTransferObj();
        LearningCenterimpl laerningCenter = new LearningCenterimpl(context);
        laerningCenter.openConn();
        laerningCenterObj = laerningCenter.getLearningCenterFunctionsByCatId(catId);
        laerningCenter.closeConn();
        return laerningCenterObj;
    }

    /**
     * This method update the Cat name and return the updated List
     *
     * @param laernignCenterFunctionsList
     * @param context
     * @return
     */
    public static ArrayList<LearningCenterTransferObj>
    getLearningCenterCategoryListWithUpdatedCategoryName
    (ArrayList<LearningCenterTransferObj> laernignCenterFunctionsList, Context context) {
        ArrayList<LearningCenterTransferObj> laernignCenterFunctionsListUpdated
                = new ArrayList<LearningCenterTransferObj>();
        Translation transeletion = new Translation(context);
        transeletion.openConnection();
        for (int i = 0; i < laernignCenterFunctionsList.size(); i++) {
            //LearningCenterTransferObj lc = laernignCenterFunctionsList.get(i);
            LearningCenterTransferObj lc = new LearningCenterTransferObj();
            if (laernignCenterFunctionsList.get(i)
                    .getLearningCenterOperation().contains("Fractions"))
                lc.setLearningCenterOperation(transeletion
                        .getTranselationTextByTextIdentifier("btnTitleFractions"));
            else if (laernignCenterFunctionsList.get(i)
                    .getLearningCenterOperation().contains("Decimals"))
                lc.setLearningCenterOperation(transeletion
                        .getTranselationTextByTextIdentifier("btnTitleDecimals"));
            else if (laernignCenterFunctionsList.get(i)
                    .getLearningCenterOperation().contains("Negative"))
                lc.setLearningCenterOperation(transeletion
                        .getTranselationTextByTextIdentifier("btnTitleNegatives"));
            else if (laernignCenterFunctionsList.get(i)
                    .getLearningCenterOperation().contains("Addition"))
                lc.setLearningCenterOperation(transeletion
                        .getTranselationTextByTextIdentifier("btnTitleAddition"));
            else if (laernignCenterFunctionsList.get(i)
                    .getLearningCenterOperation().contains("Subtraction"))
                lc.setLearningCenterOperation(transeletion
                        .getTranselationTextByTextIdentifier("btnTitleSubtraction"));
            else if (laernignCenterFunctionsList.get(i)
                    .getLearningCenterOperation().contains("Multiplication"))
                lc.setLearningCenterOperation(transeletion
                        .getTranselationTextByTextIdentifier("btnTitleMutiplication"));
            else if (laernignCenterFunctionsList.get(i)
                    .getLearningCenterOperation().contains("Division"))
                lc.setLearningCenterOperation(transeletion
                        .getTranselationTextByTextIdentifier("btnTitleDivision"));
            laernignCenterFunctionsListUpdated.add(lc);
        }
        transeletion.closeConnection();
        return laernignCenterFunctionsListUpdated;
    }

    /**
     * Return the updated cat name
     *
     * @param context
     * @param catName
     * @return
     */
    public static String getUpdatedCatNameByCatName(Context context, String catName) {
        String updatedCatName = null;
        Translation transeletion = new Translation(context);
        transeletion.openConnection();
        if (catName.contains("Fractions"))
            updatedCatName = transeletion.getTranselationTextByTextIdentifier("btnTitleFractions");
        else if (catName.contains("Decimals"))
            updatedCatName = transeletion.getTranselationTextByTextIdentifier("btnTitleDecimals");
        else if (catName.contains("Negative"))
            updatedCatName = transeletion.getTranselationTextByTextIdentifier("btnTitleNegatives");
        else if (catName.contains("Addition"))
            updatedCatName = transeletion.getTranselationTextByTextIdentifier("btnTitleAddition");
        else if (catName.contains("Subtraction"))
            updatedCatName = transeletion.getTranselationTextByTextIdentifier("btnTitleSubtraction");
        else if (catName.contains("Multiplication"))
            updatedCatName = transeletion.getTranselationTextByTextIdentifier("btnTitleMutiplication");
        else if (catName.contains("Division"))
            updatedCatName = transeletion.getTranselationTextByTextIdentifier("btnTitleDivision");
        transeletion.closeConnection();
        return updatedCatName;
    }

    /**
     * Return the updated cat name
     *
     * @param context
     * @param oldText
     * @return
     */
    public static String getUpdatedCatNameText(Context context, String oldText) {
        String updatedText = null;
        Translation transeletion = new Translation(context);
        transeletion.openConnection();
        if (oldText.contains("Fractions"))
            updatedText = transeletion.getTranselationTextByTextIdentifier("btnTitleFractions");
        else if (oldText.contains("Decimals"))
            updatedText = transeletion.getTranselationTextByTextIdentifier("btnTitleDecimals");
        else if (oldText.contains("Negative"))
            updatedText = transeletion.getTranselationTextByTextIdentifier("btnTitleNegatives");
        else if (oldText.contains("Addition"))
            updatedText = transeletion.getTranselationTextByTextIdentifier("btnTitleAddition");
        else if (oldText.contains("Subtraction"))
            updatedText = transeletion.getTranselationTextByTextIdentifier("btnTitleSubtraction");
        else if (oldText.contains("Multiplication"))
            updatedText = transeletion.getTranselationTextByTextIdentifier("btnTitleMutiplication");
        else if (oldText.contains("Division"))
            updatedText = transeletion.getTranselationTextByTextIdentifier("btnTitleDivision");
        transeletion.closeConnection();
        return updatedText;
    }

    /**
     * Return the updated cat name
     *
     * @param laernignCenterFunctionsList
     * @param context
     * @return
     */
    public static ArrayList<String> getUpdatedPracticeSkillCatNameList
    (ArrayList<LearningCenterTransferObj> laernignCenterFunctionsList, Context context) {
        ArrayList<String> catNameList = new ArrayList<String>();
        Translation transeletion = new Translation(context);
        transeletion.openConnection();
        for (int i = 0; i < laernignCenterFunctionsList.size(); i++) {
            if (laernignCenterFunctionsList.get(i)
                    .getLearningCenterOperation().contains("Fractions"))
                catNameList.add(transeletion
                        .getTranselationTextByTextIdentifier("btnTitleFractions"));
            else if (laernignCenterFunctionsList.get(i)
                    .getLearningCenterOperation().contains("Decimals"))
                catNameList.add(transeletion
                        .getTranselationTextByTextIdentifier("btnTitleDecimals"));
            else if (laernignCenterFunctionsList.get(i)
                    .getLearningCenterOperation().contains("Negative"))
                catNameList.add(transeletion
                        .getTranselationTextByTextIdentifier("btnTitleNegatives"));
            else if (laernignCenterFunctionsList.get(i)
                    .getLearningCenterOperation().contains("Addition"))
                catNameList.add(transeletion
                        .getTranselationTextByTextIdentifier("btnTitleAddition"));
            else if (laernignCenterFunctionsList.get(i)
                    .getLearningCenterOperation().contains("Subtraction"))
                catNameList.add(transeletion
                        .getTranselationTextByTextIdentifier("btnTitleSubtraction"));
            else if (laernignCenterFunctionsList.get(i)
                    .getLearningCenterOperation().contains("Multiplication"))
                catNameList.add(transeletion
                        .getTranselationTextByTextIdentifier("btnTitleMutiplication"));
            else if (laernignCenterFunctionsList.get(i)
                    .getLearningCenterOperation().contains("Division"))
                catNameList.add(transeletion
                        .getTranselationTextByTextIdentifier("btnTitleDivision"));

        }
        transeletion.closeConnection();
        return catNameList;
    }

    /**
     * This method get categories detail from database according to the user grade
     */
    public static String getSchoolCurriculumCategoryNameByCatId(Context context, String catId) {
        String catName = null;
        if (CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH) {
            SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl
                    (context);
            schoolImpl.openConnection();
            catName = schoolImpl.getCategorieNameByCatIdForSchoolCurriculum(catId);
            schoolImpl.closeConnection();
        } else {
            SpanishChangesImpl schoolImpl = new SpanishChangesImpl(context);
            schoolImpl.openConn();
            catName = schoolImpl.getCategorieNameByCatIdForSchoolCurriculum(catId);
            schoolImpl.closeConn();
        }
        return catName;
    }

    /**
     * Return the subcat name for school curriculum
     *
     * @param context
     * @param catId
     * @param subCatId
     * @return
     */
    public static String getSchoolCurriculumSubCatNameByCatIdAndSubCatId(Context context,
                                                                         String catId, String subCatId) {
        String subCatName = null;
        if (CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH) {
            SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl
                    (context);
            schoolImpl.openConnection();
            subCatName = schoolImpl.getSchoolCurriculumSubCatNameByCatIdAndSubCatId(catId,
                    subCatId);
            schoolImpl.closeConnection();
        } else {
            SpanishChangesImpl schoolImpl = new SpanishChangesImpl(context);
            schoolImpl.openConn();
            subCatName = schoolImpl.getSchoolCurriculumSubCatNameByCatIdAndSubCatId(catId,
                    subCatId);
            schoolImpl.closeConn();
        }
        return subCatName;
    }

    /**
     * Return the subchild cat list
     *
     * @param context
     * @param catId
     * @return
     */
    public static ArrayList<SubCatergoryTransferObj>
    getWorkSubCatListByCatId(Context context, int catId) {
        ArrayList<SubCatergoryTransferObj> subCatList = new ArrayList<SubCatergoryTransferObj>();
        if (CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH) {
            SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl
                    (context);
            schoolImpl.openConnection();
            subCatList = schoolImpl.getSubChildCategories(catId);
            schoolImpl.closeConnection();
        } else {
            SpanishChangesImpl schoolImpl = new SpanishChangesImpl(context);
            schoolImpl.openConn();
            subCatList = schoolImpl.getSubChildCategories(catId);
            schoolImpl.closeConn();
        }
        return subCatList;
    }

    /**
     * This method show the warning dialog
     *
     * @param context
     * @param message
     */
    public static void showWarningDialog(Context context, String message) {
        DialogGenerator dg = new DialogGenerator(context);
        dg.generateWarningDialog(message);
    }

    public static void showWarningDialog(Context context, String message,
                                         HttpServerRequest request) {
        DialogGenerator dg = new DialogGenerator(context);
        dg.generateCommonWarningDialog(message, request);
    }

    /**
     * This method show the warning dialog
     *
     * @param context
     * @param message
     */
    public static void showWarningDialogForSomeLongMessage(Context context, String message) {
        DialogGenerator dg = new DialogGenerator(context);
        dg.generateWarningDialogForthumbImages(message);
    }

    /**
     * This method show the warning dialog
     *
     * @param context
     * @param message
     */
    public static void showWarningDialogForSomeLongMessage(Context context, String message
            , HttpServerRequest request) {
        DialogGenerator dg = new DialogGenerator(context);
        dg.generateWarningDialogForthumbImages(message , request);
    }

    /**
     * This method check for question loaded or not
     *
     * @param context
     * @param grade
     * @return
     */
    public static boolean isWordQuestionLoaded(Context context, int grade) {
        SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl =
                new SchoolCurriculumLearnignCenterimpl(context);
        schooloCurriculumImpl.openConnection();
        boolean isQuestionLoaded = schooloCurriculumImpl.isQuestionLeaded(grade);
        schooloCurriculumImpl.closeConnection();

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "isWordQuestionLoaded " + isQuestionLoaded);
        return isQuestionLoaded;
    }

    /**
     * Check for question loaded in spanish for word problem
     *
     * @param context
     * @param grade
     * @return
     */
    public static boolean isWordQuestionForSpanishLoaded(Context context, int grade) {
        SpanishChangesImpl spanishImplObj = new SpanishChangesImpl(context);
        spanishImplObj.openConn();
        //CommonUtils.CheckWordProblemSpanishTable(context);
        boolean isSpanishQuestionLoaded = spanishImplObj.isQuestionLeaded(grade);
        spanishImplObj.closeConn();
        return isSpanishQuestionLoaded;
    }

    /**
     * This method save the login player data into shared preff
     *
     * @param context
     */
    public static void saveLoginPlayerDataIntoSharedPreff(Context context) {

        SharedPreferences sheredPreferencePlayer = context.getSharedPreferences(IS_CHECKED_PREFF, 0);
        UserPlayerOperation userPlayerOpr = new UserPlayerOperation(context);
        UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(sheredPreferencePlayer
                .getString(PLAYER_ID, ""));

        SharedPreferences sharedPrefPlayerInfo = context.getSharedPreferences(PLAYER_INFO, 0);
        SharedPreferences.Editor editor = sharedPrefPlayerInfo.edit();

        UserRegistrationOperation userObj = new UserRegistrationOperation(context);
        RegistereUserDto regUserObj = userObj.getUserData();

        editor.clear();
        editor.putString("playerName", userPlayerData.getFirstname() + " " + userPlayerData
                .getLastname());
        editor.putString("userName", userPlayerData.getUsername());
        editor.putString("city", regUserObj.getCity());
        editor.putString("state", regUserObj.getState());
        editor.putString("imageName", userPlayerData.getImageName());
        editor.putString("coins", userPlayerData.getCoin());
        editor.putInt("grade", Integer.parseInt(userPlayerData.getGrade()));

        editor.putString("userId", userPlayerData.getParentUserId());
        editor.putString("playerId", userPlayerData.getPlayerid());

        Country country = new Country();
        editor.putString("countryName", country.getCountryNameByCountryId(regUserObj.getCountryId()
                , context));

        LearningCenterimpl learningCenter = new LearningCenterimpl(context);
        learningCenter.openConn();

        if (learningCenter.getDataFromPlayerTotalPoints(userPlayerData.getPlayerid())
                .getCompleteLevel() != 0)
            editor.putInt("completeLevel", learningCenter.getDataFromPlayerTotalPoints
                    (userPlayerData.getPlayerid())
                    .getCompleteLevel());
        else
            editor.putInt("completeLevel", Integer.parseInt(userPlayerData.getCompletelavel()));

        learningCenter.closeConn();
        editor.commit();
    }

    /**
     * Convert the string into delimiter seprated string
     *
     * @param string
     * @param delimiter
     * @return
     */
    public static String convertStringIntoSperatedStringWithDelimeter(String string,
                                                                      String delimiter) {
        try {
            string = string.replace(delimiter, "");

            char[] cArray = string.toCharArray();
            ArrayList<String> list = new ArrayList<String>(cArray.length);
            for (char c : cArray) {
                list.add(String.valueOf(c));
            }
            return join(list, delimiter);
        } catch (Exception e) {
            return string;
        }
    }

    /**
     * Join the string with given delimeter
     *
     * @param s
     * @param delimiter
     * @return
     */
    public static String join(Iterable<? extends CharSequence> s, String delimiter) {
        Iterator<? extends CharSequence> iter = s.iterator();
        if (!iter.hasNext()) return "";
        StringBuilder buffer = new StringBuilder(iter.next());
        while (iter.hasNext()) buffer.append(delimiter).append(iter.next());
        return buffer.toString();
    }


    /**
     * Separate the string with commas and return the string array
     *
     * @return
     */
    public static String[] getCommaSepratedOption(String stringWithCommas, String delimiter) {
        try {
            return stringWithCommas.split(delimiter);
        } catch (Exception e) {
            e.printStackTrace();
            return new String[0];//return blank array
        }
    }

    /**
     * Convert the string array list into seprated string with delimiter
     *
     * @param list
     * @param delimiter
     * @return
     */
    public static String convertStringArrayListIntoStringWithDelimeter(ArrayList<String> list
            , String delimiter) {
        try {
            StringBuilder sb = new StringBuilder();
            for (String s : list) {
                sb.append(delimiter).append(s);
            }
            return sb.substring(delimiter.length());// remove leading separator
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getCommaSeparatedStringForIntegerList(ArrayList<Integer> integerArrayList
            , String delimiter){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0 ; i < integerArrayList.size() ; i ++ ){
            stringBuilder.append(stringBuilder.length() > 0 ? delimiter + integerArrayList.get(i)
                    : integerArrayList.get(i));
        }
        return stringBuilder.toString();
    }


    public static String removeDelimeterFromString(String stringwithDelimert
            , String delimiter){
        try{
            return stringwithDelimert.replaceAll(delimiter , "");
        }catch (Exception e){
            e.printStackTrace();
            return "";
        }
    }

    /**
     * Separate the string with commas and return the string array
     *
     * @return
     */
    public static ArrayList<String> getCommaSepratedOptionInArrayList
    (String stringWithCommas, String delimiter) {
        ArrayList<String> list = new ArrayList<String>();
        try {
            String array[] = stringWithCommas.split(delimiter);
            for (int i = 0; i < array.length; i++) {
                list.add(array[i]);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return list;//return blank array
        }
    }

    /**
     * Generate the yes no interface dialog
     *
     * @param context
     * @param message
     * @param yesText
     * @param noText
     * @param listenerInterface
     */
    public static void yesNoConfirmationDialog(Context context, String message,
                                               String yesText, String noText,
                                               final YesNoListenerInterface listenerInterface) {
        final Dialog dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.yes_no_confirmation_dialog_layout);
        dialog.show();

        Button btnNo = (Button) dialog.findViewById(R.id.btnNoThanks);
        Button btnYes = (Button) dialog.findViewById(R.id.btnYes);
        TextView txtMsg = (TextView) dialog.findViewById(R.id.txtTimeSpentMessage);

        btnNo.setText(noText);
        btnYes.setText(yesText);
        txtMsg.setText(message);

        btnNo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                listenerInterface.onNo();
            }
        });

        btnYes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                listenerInterface.onYes();
            }
        });
    }

    /**
     * Generate the yes no interface dialog
     *
     * @param context
     * @param message
     * @param yesText
     * @param noText
     * @param listenerInterface
     */
    public static void yesNoConfirmationDialogWithCancelButton(Context context, String message,
                                                               String yesText, String noText,
                                                               final YesNoListenerInterfaceWithCross listenerInterface) {
        final Dialog dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.yes_no_confirmation_dialog_layout_with_cancel_button);
        dialog.show();

        Button btnNo = (Button) dialog.findViewById(R.id.btnNoThanks);
        Button btnYes = (Button) dialog.findViewById(R.id.btnYes);
        Button btnCross = (Button) dialog.findViewById(R.id.btnCross);
        TextView txtMsg = (TextView) dialog.findViewById(R.id.txtTimeSpentMessage);

        btnNo.setText(noText);
        btnYes.setText(yesText);
        txtMsg.setText(message);

        btnNo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                listenerInterface.onNo();
            }
        });

        btnYes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                listenerInterface.onYes();
            }
        });

        btnCross.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                listenerInterface.onCross();
            }
        });
    }

    /**
     * Generate the yes no interface dialog
     *
     * @param context
     * @param message
     * @param yesText
     * @param noText
     * @param listenerInterface
     */
    public static void yesNoConfirmationDialogForLongMessage(Context context, String message,
                                                             String yesText, String noText,
                                                             final YesNoListenerInterface listenerInterface) {
        final Dialog dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.yes_no_confirmation_dialog_for_long_message);
        dialog.show();

        Button btnNo = (Button) dialog.findViewById(R.id.btnNoThanks);
        Button btnYes = (Button) dialog.findViewById(R.id.btnYes);
        TextView txtMsg = (TextView) dialog.findViewById(R.id.txtTimeSpentMessage);

        btnNo.setText(noText);
        btnYes.setText(yesText);
        txtMsg.setText(message);

        btnNo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                listenerInterface.onNo();
            }
        });

        btnYes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                listenerInterface.onYes();
            }
        });
    }

    /**
     * Generate the yes no interface dialog
     *
     * @param context
     * @param message
     * @param yesText
     * @param noText
     * @param listenerInterface
     */
    public static void yesNoConfirmationDialogForLongMessageForUnlockInAppPopUp(Context context,
                                                                                String message,
                                                                                String yesText, String noText,
                                                                                final YesNoListenerInterface listenerInterface
            , YesNoDialogMessagesForInAppPopUp messages) {
        final Dialog dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.yes_no_confirmation_dialog_for_in_app_purchase);
        dialog.show();

        Button btnNo = (Button) dialog.findViewById(R.id.btnNoThanks);
        Button btnYes = (Button) dialog.findViewById(R.id.btnYes);
        TextView txtMsg = (TextView) dialog.findViewById(R.id.txtTimeSpentMessage);
        TextView txtLimitedTimeOffer = (TextView) dialog.findViewById(R.id.txtLimitedTimeOffer);
        TextView txtUpgradeAmount = (TextView) dialog.findViewById(R.id.txtUpgradeAmount);

        btnNo.setText(noText);
        btnYes.setText(yesText);
        txtMsg.setText(messages.getMainText());
        txtLimitedTimeOffer.setText("\n" + messages.getLimitedTimeOffer());
        txtUpgradeAmount.setText(messages.getUpgradeText());

        btnNo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                listenerInterface.onNo();
            }
        });

        btnYes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                listenerInterface.onYes();
            }
        });
    }


    /**
     * Generate warning dialog
     *
     * @param context
     * @param msg
     */
    public static void warningDialog(Context context, String msg) {
        DialogGenerator dg = new DialogGenerator(context);
        dg.generateCommonWarningDialog(msg);
    }

    /**
     * If the text in the message is very big
     *
     * @param context
     * @param msg
     */
    public static void warningDialogForLongMsg(Context context, String msg) {
        DialogGenerator dg = new DialogGenerator(context);
        dg.generateCommonWarningDialogForLongMsg(msg);
    }

    /**
     * If the text in the message is very big
     *
     * @param context
     * @param msg
     */
    public static void warningDialogForLongMsg(Context context, String msg , HttpServerRequest request) {
        DialogGenerator dg = new DialogGenerator(context);
        dg.generateCommonWarningDialogForLongMsgWithSameTextSizeForTabPhone(msg, request);
    }

    /**
     * Generate warning dialog
     *
     * @param context
     * @param msg
     */
    public static void warningDialog(Context context, String msg,
                                     final HttpServerRequest request) {
        DialogGenerator dg = new DialogGenerator(context);
        dg.generateCommonWarningDialog(msg, request);
    }

    /**
     * This method take screen shot of the screen
     * and retun the bitmap
     *
     * @param context
     * @return
     */
    public static Bitmap takeScreenShot(Context context, View view) {
		/*view.setDrawingCacheEnabled(true);
		view.buildDrawingCache(true);
		Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
		view.setDrawingCacheEnabled(false);
		view.destroyDrawingCache();*/
        Bitmap screenshot = Bitmap.createBitmap(view.getWidth(),
                view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(screenshot);
        view.draw(c);
        return screenshot;
    }

    /*public static Bitmap takeScreenShotUsingDrawingCache(Context context, View view) {
		view.setDrawingCacheEnabled(true);
		view.buildDrawingCache(true);
		Bitmap bitmap = view.getDrawingCache();
		view.setDrawingCacheEnabled(false);
		view.destroyDrawingCache();
        return bitmap;
    }*/

    /**
     * Update the image with .png formate
     *
     * @param imageName
     * @return
     */
    public static String getPNGFormateImageName(String imageName) {
        if (imageName.contains(".png")) {
            return imageName;
        } else {
            return imageName + ".png";
        }
    }

    /**
     * This method check for file existance
     *
     * @param fileName
     * @return
     */
    public static boolean checkForExistenceOfFile(String fileName) {
        if (fileName != null && fileName.length() > 0) {
            fileName = getPNGFormateImageName(fileName);

            String settingDirPath = Environment.getExternalStorageDirectory()
                    + "/MathFriendzy";
            File file = new File(settingDirPath, fileName);
            if (file.exists())
                return true;
            return false;
        } else {
            return false;
        }
    }

    /**
     * Delete All images
     *
     * @return
     */
    public static boolean deleteAllImages() {
        String settingDirPath = Environment.getExternalStorageDirectory()
                + "/MathFriendzy";
        File path = new File(settingDirPath);

        if (path.exists()) {
            File[] files = path.listFiles();
            if (files == null) {
                return true;
            }
            for (int i = 0; i < files.length; i++) {
                //if(!files[i].getName().contains("q"))
                files[i].delete();
            }
        }
        return (path.delete());
    }

    /**
     * Delete image from given uri
     *
     * @param uri
     */
    public static void deleteImageFromGivenUri(String uri) {
        try {
            File path = new File(uri);
            path.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Return the file bitmap
     *
     * @param fileName
     * @return
     */
    @SuppressLint("NewApi")
    public static Bitmap getFileByFileName(Context context, String fileName) {
        try {
            fileName = getPNGFormateImageName(fileName);
            String settingDirPath = Environment.getExternalStorageDirectory() + "/MathFriendzy";
            File fileDirectiry = new File(settingDirPath);
            File file = new File(fileDirectiry, fileName);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            if (VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB)
                options.inMutable = true;
            Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options);
            if (!bitmap.isMutable())
                bitmap = convertToMutable(context, bitmap);
            return bitmap;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Return the file bitmap
     *
     * @param fileName
     * @return
     */
    @SuppressLint("NewApi")
    public static Bitmap getFileByFileNameForHW(Context context, String fileName) {
        try {
            fileName = getPNGFormateImageName(fileName);
            String settingDirPath = Environment.getExternalStorageDirectory() + "/MathFriendzy";
            File fileDirectiry = new File(settingDirPath);
            File file = new File(fileDirectiry, fileName);
            BitmapFactory.Options options = new BitmapFactory.Options();
            /*if(MathFriendzyHelper.isChromeBookRelease){
                options.inSampleSize = 2;
            }*/
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            if (VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB)
                options.inMutable = true;
            Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options);
            if (!bitmap.isMutable())
                bitmap = convertToMutable(context, bitmap);
            return bitmap;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Return the file bitmap
     *
     * @param fileUri
     * @return
     */
    @SuppressLint("NewApi")
    public static Bitmap getFileByFileUri(Context context, String fileUri) {
        try {
            File file = new File(fileUri);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;

            if (VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB)
                options.inMutable = true;

            Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options);

            if (!bitmap.isMutable())
                bitmap = convertToMutable(context, bitmap);

            return bitmap;
        } catch (Exception e) {
            return null;
        }
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public static Bitmap convertToMutable(final Context context, final Bitmap imgIn) {
        final int width = imgIn.getWidth(), height = imgIn.getHeight();
        final Config type = imgIn.getConfig();
        File outputFile = null;
        final File outputDir = context.getCacheDir();
        try {
            outputFile = File.createTempFile(Long.toString(System.currentTimeMillis()), null,
                    outputDir);
            outputFile.deleteOnExit();
            final RandomAccessFile randomAccessFile = new RandomAccessFile(outputFile, "rw");
            final FileChannel channel = randomAccessFile.getChannel();
            final MappedByteBuffer map = channel.map(MapMode.READ_WRITE, 0, imgIn.getRowBytes()
                    * height);
            imgIn.copyPixelsToBuffer(map);
            imgIn.recycle();
            final Bitmap result = Bitmap.createBitmap(width, height, type);
            map.position(0);
            result.copyPixelsFromBuffer(map);
            channel.close();
            randomAccessFile.close();
            outputFile.delete();
            return result;
        } catch (final Exception e) {
        } finally {
            if (outputFile != null)
                outputFile.delete();
        }
        return null;
    }

    /**
     * Save the image file into SDCARD
     *
     * @param bitMap
     * @param fileName
     * @return
     */
    public static boolean saveFileToSDCard(Bitmap bitMap, String fileName) {
        try {
            fileName = getPNGFormateImageName(fileName);

            String settingDirPath = Environment.getExternalStorageDirectory() + "/MathFriendzy";

            File fileDirectiry = new File(settingDirPath);
            if (!fileDirectiry.isDirectory())
                fileDirectiry.mkdirs();

            File file = new File(fileDirectiry, fileName);

            if (file.exists())
                file.delete();

            FileOutputStream out = new FileOutputStream(file);
            bitMap.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();
            return true;

        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * Return the unique image name
     *
     * @return
     */
    public static String getUniqueScreenShotName() {
        return "CAPTURE_" + System.currentTimeMillis();
    }

    /**
     * Save  the screen shot
     *
     * @param bitMap
     * @param fileName
     * @return
     */
    public static File saveScreenShot(Bitmap bitMap, String fileName) {
        try {
            fileName = getPNGFormateImageName(fileName);

			/*String settingDirPath = Environment.getExternalStorageDirectory() 
					+ "/MathFriendzy/ScreenShot";*/
            String settingDirPath = Environment.getExternalStorageDirectory()
                    + "/MathFriendzy";

            File fileDirectiry = new File(settingDirPath);
            if (!fileDirectiry.isDirectory())
                fileDirectiry.mkdirs();

            File file = new File(fileDirectiry, fileName);

            if (file.exists())
                file.delete();

            FileOutputStream out = new FileOutputStream(file);
            bitMap.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();
            return file;

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Return math folder file path
     *
     * @return
     */
    public static String getMathFilePath() {
        return Environment.getExternalStorageDirectory()
                + "/MathFriendzy";
    }

    /**
     * Rename file name
     *
     * @param uri
     * @param newFileName
     */
    public static void renameFileNameFromUri(String uri, String newFileName) {
        try {
            File renameFilePath = new File(getMathFilePath() + "/");
            File file = new File(uri);
            file.renameTo(new File(renameFilePath + "/" + newFileName));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Convert the question image name into local uri
     *
     * @param fileName
     * @return
     */
    public static String getQuestionImageUri(String fileName) {
        fileName = getPNGFormateImageName(fileName);
        return getMathFilePath() + "/" + fileName;
    }

    /**
     * Upload the image from SD card to server
     *
     * @param context
     * @param fileName
     */
    public static void uploadImageOnserverFromSDCard
    (final Context context, final String fileName,
     final HttpServerRequest request) {
        new AsyncTask<Void, Void, Void>() {

            private ProgressDialog pd = null;

            @Override
            protected void onPreExecute() {
                pd = CommonUtils.getProgressDialog(context);
                pd.show();
            }

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    UploadImage.executeMultipartPost(ImageOperation.getBitmapByFilePath
                                    (context, getQuestionImageUri(fileName)),
                            MathFriendzyHelper.getPNGFormateImageName(fileName), "",
                            new UploadImageRequest() {
                                @Override
                                public void onComplete(String response) {
                                    if (response != null) {
                                        if (CommonUtils.LOG_ON)
                                            Log.e(TAG, "Response " + response);
                                    }
                                }
                            }, ICommonUtils.UPLOAD_QUESTION_IMAGE_URL
                                    + "action=uploadQuestionImage");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {

                if (pd != null && pd.isShowing()) {
                    pd.cancel();
                }

                request.onRequestComplete();
                super.onPostExecute(result);
            }
        }.execute();
    }

    /**
     * Upload the image from SD card to server
     *
     * @param context
     * @param fileName
     */
    public static void uploadImageOnserverFromSDCardWithoutLoader
    (final Context context, final String fileName,
     final HttpServerRequest request) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
            }

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    UploadImage.executeMultipartPost(ImageOperation.getBitmapByFilePath
                                    (context, getQuestionImageUri(fileName)),
                            MathFriendzyHelper.getPNGFormateImageName(fileName), "",
                            new UploadImageRequest() {
                                @Override
                                public void onComplete(String response) {
                                    if (response != null) {
                                        if (CommonUtils.LOG_ON)
                                            Log.e(TAG, "Response " + response);
                                    }
                                }
                            }, ICommonUtils.UPLOAD_QUESTION_IMAGE_URL
                                    + "action=uploadQuestionImage");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                request.onRequestComplete();
                super.onPostExecute(result);
            }
        }.execute();
    }


    /**
     * This method download the image from server and save into SD card
     *
     * @param context
     * @param imageUrl
     * @param imageName
     * @param request
     */
    public static void downLoadImageFromUrl(final Context context, final String imageUrl
            , final String imageName, final HttpServerRequest request) {

        final String newimageName = getPNGFormateImageName(imageName);

        new AsyncTask<Void, Void, Bitmap>() {

            private ProgressDialog pd = null;

            @Override
            protected void onPreExecute() {
                pd = ServerDialogs.getProgressDialog(context, "", 1);
                pd.show();
                super.onPreExecute();
            }

            @Override
            protected Bitmap doInBackground(Void... params) {
                try {
                    Bitmap bitmap = CommonUtils.getBitmapWithOption(imageUrl + newimageName);
                    if(bitmap != null) {
                        if (!bitmap.isMutable())
                            bitmap = MathFriendzyHelper.convertToMutable(context, bitmap);
                    }
                    saveFileToSDCard(bitmap, newimageName);
                    return bitmap;
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Bitmap result) {
                if (pd != null && pd.isShowing()) {
                    pd.cancel();
                }

                request.onRequestComplete();
                super.onPostExecute(result);
            }
        }.execute();
    }

    /**
     * Download the question for the word problem
     *
     * @param context
     * @param grade
     * @param httprequest
     */
    public static void downLoadQuestionForWordProblem(Context context,
                                                      int grade, HttpServerRequest httprequest) {

        if (CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH) {
            if (MathFriendzyHelper.isWordQuestionLoaded(context, grade)) {
                if (CommonUtils.isInternetConnectionAvailable(context)) {
                    new GetWordProblemQuestionUpdatedDate(grade, false, httprequest
                            , context).execute(null, null, null);

                } else {
                    httprequest.onRequestComplete();
                }
            } else {
                if (CommonUtils.isInternetConnectionAvailable(context)) {
                    new GetWordProblemQuestion(grade, context, httprequest)
                            .execute(null, null, null);
                    new GetWordProblemQuestionUpdatedDate(grade, true, httprequest
                            , context).execute(null, null, null);
                } else {
                    CommonUtils.showInternetDialog(context);
                }
            }
        } else {
            if (MathFriendzyHelper.isWordQuestionForSpanishLoaded(context, grade)) {
                if (CommonUtils.isInternetConnectionAvailable(context)) {
                    new GetWordProblemQuestionUpdatedDate(grade, false, httprequest
                            , context).execute(null, null, null);
                } else {
                    httprequest.onRequestComplete();
                }
            } else {
                if (CommonUtils.isInternetConnectionAvailable(context)) {
                    new GetWordProblemQuestion(grade, context, httprequest)
                            .execute(null, null, null);
                    new GetWordProblemQuestionUpdatedDate(grade, true, httprequest
                            , context).execute(null, null, null);
                } else {
                    CommonUtils.showInternetDialog(context);
                }
            }
        }
    }

    /**
     * Download the question for the word problem
     *
     * @param context
     * @param grade
     * @param httprequest
     */
    public static void downLoadQuestionForWordProblemForHW(Context context,
                                                           int grade, HttpServerRequest httprequest) {

        if (CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH) {
            if (MathFriendzyHelper.isWordQuestionLoaded(context, grade)) {
                if (CommonUtils.isInternetConnectionAvailable(context)) {
                    new GetWordProblemQuestionUpdatedDate(grade, false, httprequest
                            , context).execute(null, null, null);

                } else {
                    httprequest.onRequestComplete();
                }
            } else {
                if (CommonUtils.isInternetConnectionAvailable(context)) {
                    new GetWordProblemQuestion(grade, context, httprequest)
                            .execute(null, null, null);
                    new GetWordProblemQuestionUpdatedDate(grade, true, httprequest
                            , context).execute(null, null, null);
                } else {
                    //CommonUtils.showInternetDialog(context);
                    httprequest.onRequestComplete();
                }
            }
        } else {
            if (MathFriendzyHelper.isWordQuestionForSpanishLoaded(context, grade)) {
                if (CommonUtils.isInternetConnectionAvailable(context)) {
                    new GetWordProblemQuestionUpdatedDate(grade, false, httprequest
                            , context).execute(null, null, null);
                } else {
                    httprequest.onRequestComplete();
                }
            } else {
                if (CommonUtils.isInternetConnectionAvailable(context)) {
                    new GetWordProblemQuestion(grade, context, httprequest)
                            .execute(null, null, null);
                    new GetWordProblemQuestionUpdatedDate(grade, true, httprequest
                            , context).execute(null, null, null);
                } else {
                    //CommonUtils.showInternetDialog(context);
                    httprequest.onRequestComplete();
                }
            }
        }
    }

    /**
     * Check question is downloaded or not
     *
     * @param context
     * @param grade
     * @return
     */
    public static boolean checkForDownload(Context context, int grade) {
        if (CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH) {
            return MathFriendzyHelper.isWordQuestionLoaded(context, grade);
        } else {
            return MathFriendzyHelper.isWordQuestionForSpanishLoaded(context, grade);
        }
    }

    /**
     * Check for word question downloaded or not
     *
     * @param context
     * @param grade
     * @return
     */
    public static void isWordQuestionDownloaded(Context context, int grade,
                                                HttpServerRequest httprequest) {
        if (checkForDownload(context, grade)) {
            httprequest.onRequestComplete();
        } else {
            if (CommonUtils.isInternetConnectionAvailable(context)) {
                new GetWordProblemQuestion(grade, context, httprequest)
                        .execute(null, null, null);
                new GetWordProblemQuestionUpdatedDate(grade, true, httprequest
                        , context).execute(null, null, null);
            } else {
                CommonUtils.showInternetDialog(context);
            }
        }
    }

    /**
     * This class get word problem updated dates from server
     *
     * @author Yashwant Singh
     */
    public static class GetWordProblemQuestionUpdatedDate extends
            AsyncTask<Void, Void, ArrayList<UpdatedInfoTransferObj>> {
        private int grade;
        private ProgressDialog pd;
        private boolean isFisrtTimeCallForGrade;
        private HttpServerRequest httprequest = null;
        private Context context = null;

        GetWordProblemQuestionUpdatedDate(int grade, boolean isFisrtTimeCallForGrade,
                                          HttpServerRequest httprequest, Context context) {
            this.grade = grade;
            this.isFisrtTimeCallForGrade = isFisrtTimeCallForGrade;
            this.httprequest = httprequest;
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            if (isFisrtTimeCallForGrade == false) {
                pd = CommonUtils.getProgressDialog(context);
                pd.show();
            }
            super.onPreExecute();
        }

        @Override
        protected ArrayList<UpdatedInfoTransferObj> doInBackground(Void... params) {
            SingleFriendzyServerOperationForWordProblem serverObj =
                    new SingleFriendzyServerOperationForWordProblem();
            //changes for Spanish
            ArrayList<UpdatedInfoTransferObj> updatedList = new ArrayList<UpdatedInfoTransferObj>();
            if (CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH)
                updatedList = serverObj.getWordProbleQuestionUpdatedData();
            else//for Spanish
                updatedList = serverObj.getWordProbleQuestionUpdatedDateForSpanish
                        (CommonUtils.SPANISH);
            return updatedList;
        }

        @Override
        protected void onPostExecute(ArrayList<UpdatedInfoTransferObj> updatedList) {
            if (isFisrtTimeCallForGrade == false) {
                pd.cancel();
            }

            if (updatedList != null) {
                SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl =
                        new SchoolCurriculumLearnignCenterimpl(context);
                schooloCurriculumImpl.openConnection();

                //changes for Spanish
                SpanishChangesImpl implObj = new SpanishChangesImpl(context);
                implObj.openConn();

                if (isFisrtTimeCallForGrade) {
                    //changes for Spanish
                    if (CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH) {
                        if (!schooloCurriculumImpl.isWordProblemUpdateDetailsTableDataExist()) {
                            schooloCurriculumImpl.insertIntoWordProblemUpdateDetails(updatedList);
                            schooloCurriculumImpl.closeConnection();
                            implObj.closeConn();
                        } else {
                            String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
                            schooloCurriculumImpl.updateNewUpdatedDateFromServer(grade,
                                    serverUpdatedDate);
                            schooloCurriculumImpl.closeConnection();
                            implObj.closeConn();
                        }
                    } else {//for Spanish
                        if (!implObj.isWordProblemUpdateDetailsTableDataExist()) {
                            implObj.insertIntoWordProblemUpdateDetails(updatedList);
                            schooloCurriculumImpl.closeConnection();
                            implObj.closeConn();
                        } else {
                            String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
                            implObj.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
                            schooloCurriculumImpl.closeConnection();
                            implObj.closeConn();
                        }
                    }
                } else {
                    if (CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH) {
                        String updatedDateFromDatabase = schooloCurriculumImpl.getUpdatedDate(grade);
                        if (CommonUtils.isUpdateDateChange(updatedDateFromDatabase, grade,
                                updatedList)) {
                            String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
                            schooloCurriculumImpl.updateNewUpdatedDateFromServer(grade,
                                    serverUpdatedDate);
                            schooloCurriculumImpl.closeConnection();
                            implObj.closeConn();

                            if (CommonUtils.isInternetConnectionAvailable(context)) {
                                new GetWordProblemQuestion(grade, context, httprequest)
                                        .execute(null, null, null);
                            } else {
                                httprequest.onRequestComplete();
                            }
                        } else {
                            schooloCurriculumImpl.closeConnection();
                            implObj.closeConn();
                            httprequest.onRequestComplete();
                        }

                    } else {//for Spanish
                        String updatedDateFromDatabase = implObj.getUpdatedDate(grade);
                        if (CommonUtils.isUpdateDateChange(updatedDateFromDatabase, grade,
                                updatedList)) {
                            String serverUpdatedDate = CommonUtils.getServerDate(grade, updatedList);
                            implObj.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);
                            schooloCurriculumImpl.closeConnection();
                            implObj.closeConn();

                            if (CommonUtils.isInternetConnectionAvailable(context)) {
                                new GetWordProblemQuestion(grade, context, httprequest)
                                        .execute(null, null, null);
                            } else {
                                httprequest.onRequestComplete();
                            }
                        } else {
                            schooloCurriculumImpl.closeConnection();
                            implObj.closeConn();
                            httprequest.onRequestComplete();
                        }
                    }
                }
            } else {
                if (isFisrtTimeCallForGrade == false) {
                    CommonUtils.showInternetDialog(context);
                }
            }
            super.onPostExecute(updatedList);
        }
    }

    /**
     * This asynctask get questions from server according to grade if not loaded in database
     *
     * @author Yashwant Singh
     */
    public static class GetWordProblemQuestion extends
            AsyncTask<Void, Void, QuestionLoadedTransferObj> {

        private int grade;
        private ProgressDialog pd;
        private Context context;
        ArrayList<String> imageNameList = new ArrayList<String>();
        private HttpServerRequest httprequest = null;

        public GetWordProblemQuestion(int grade, Context context,
                                      HttpServerRequest httprequest) {
            this.grade = grade;
            this.context = context;
            this.httprequest = httprequest;
        }

        @Override
        protected void onPreExecute() {

            Translation translation = new Translation(context);
            translation.openConnection();
            pd = CommonUtils.getProgressDialog(context, translation
                    .getTranselationTextByTextIdentifier
                            ("alertPleaseWaitWeAreDownloading"), true);
            translation.closeConnection();
            pd.show();

            super.onPreExecute();
        }

        @Override
        protected QuestionLoadedTransferObj doInBackground(Void... params) {
            LearnignCenterSchoolCurriculumServerOperation serverObj =
                    new LearnignCenterSchoolCurriculumServerOperation();
            //QuestionLoadedTransferObj questionData = serverObj.getWordProblemQuestions(grade);
            //changes for Spanish Changes
            QuestionLoadedTransferObj questionData = new QuestionLoadedTransferObj();
            if (CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH)
                questionData = serverObj.getWordProblemQuestions(grade);
            else
                questionData = serverObj.getWordProblemQuestionsForSpanish(grade,
                        CommonUtils.SPANISH);

            if (questionData != null) {
                SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl =
                        new SchoolCurriculumLearnignCenterimpl(context);
                schooloCurriculumImpl.openConnection();
                if (CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH) {
                    //changes for dialog loading wheel
                    schooloCurriculumImpl.deleteFromWordProblemCategoriesbyGrade(grade);
                    schooloCurriculumImpl.deleteFromWordProblemsSubCategoriesByCategoryId
                            (questionData.getCategoryList());
                    schooloCurriculumImpl.deleteFromWordProblemsQuestionsCategoryId
                            (questionData.getCategoryList());

                    schooloCurriculumImpl.insertIntoWordProblemCategories
                            (questionData.getCategoryList(), grade);
                    schooloCurriculumImpl.insertIntoWordProblemsSubCategories
                            (questionData.getCategoryList());
                    schooloCurriculumImpl.insertIntoWordProblemsQuestions
                            (questionData.getQuestionList());
                } else {
                    SpanishChangesImpl implObj = new SpanishChangesImpl(context);
                    implObj.openConn();

                    implObj.deleteFromWordProblemCategoriesbyGrade(grade);
                    implObj.deleteFromWordProblemsSubCategoriesByCategoryId
                            (questionData.getCategoryList());
                    implObj.deleteFromWordProblemsQuestionsCategoryId
                            (questionData.getCategoryList());

                    implObj.insertIntoWordProblemCategories
                            (questionData.getCategoryList(), grade);
                    implObj.insertIntoWordProblemsSubCategories(questionData.getCategoryList());
                    implObj.insertIntoWordProblemsQuestions(questionData.getQuestionList());
                    implObj.closeConn();
                }

                for (int i = 0; i < questionData.getQuestionList().size(); i++) {

                    WordProblemQuestionTransferObj questionObj = questionData.getQuestionList()
                            .get(i);

                    if (questionObj.getQuestion().contains(".png"))
                        imageNameList.add(questionObj.getQuestion());
                    if (questionObj.getOpt1().contains(".png"))
                        imageNameList.add(questionObj.getOpt1());
                    if (questionObj.getOpt2().contains(".png"))
                        imageNameList.add(questionObj.getOpt2());
                    if (questionObj.getOpt3().contains(".png"))
                        imageNameList.add(questionObj.getOpt3());
                    if (questionObj.getOpt4().contains(".png"))
                        imageNameList.add(questionObj.getOpt4());
                    if (questionObj.getOpt5().contains(".png"))
                        imageNameList.add(questionObj.getOpt5());
                    if (questionObj.getOpt6().contains(".png"))
                        imageNameList.add(questionObj.getOpt6());
                    if (questionObj.getOpt7().contains(".png"))
                        imageNameList.add(questionObj.getOpt7());
                    if (questionObj.getOpt8().contains(".png"))
                        imageNameList.add(questionObj.getOpt8());
                    if (!questionObj.getImage().equals(""))
                        imageNameList.add(questionObj.getImage());
                }

                if (!schooloCurriculumImpl.isImageTableExist()) {
                    schooloCurriculumImpl.createSchoolCurriculumImageTable();
                }
                schooloCurriculumImpl.closeConnection();
                //end changes
            }
            return questionData;
        }

        @Override
        protected void onPostExecute(QuestionLoadedTransferObj questionData) {

            pd.cancel();

            if (questionData != null) {
                DawnloadImagesFromserver serverObj = new DawnloadImagesFromserver
                        (imageNameList, context);
                Thread imageDawnLoadThrad = new Thread(serverObj);
                imageDawnLoadThrad.start();
                httprequest.onRequestComplete();
            } else {
                CommonUtils.showInternetDialog(context);
            }

            super.onPostExecute(questionData);
        }
    }


    /**
     * Return the current date calender
     *
     * @return
     */
    public static Calendar getCurrentCalender() {
        return Calendar.getInstance();
    }

    /**
     * Show the date picker dialog
     *
     * @param context
     * @param currentYear
     * @param currentMonth
     * @param currentDate
     * @param dateSetListener
     * @param calender
     */
    @SuppressLint("NewApi")
    public static void showDatePickerDialog(Context context, int currentYear, int currentMonth
            , int currentDate, OnDateSetListener dateSetListener, Calendar calender) {
        DatePickerDialog dialog = new DatePickerDialog(context, dateSetListener, currentYear,
                currentMonth, currentDate);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {//setMinDate Added in API Level 11
            dialog.getDatePicker().setCalendarViewShown(false);
            dialog.getDatePicker().setMinDate(calender.getTimeInMillis());
        }
        dialog.show();
    }

    /**
     * Show the date picker dialog
     *
     * @param context
     * @param currentYear
     * @param currentMonth
     * @param currentDate
     * @param dateSetListener
     * @param calender
     */
    @SuppressLint("NewApi")
    public static void showDatePickerDialogPrevDateSelected(Context context, int currentYear, int currentMonth
            , int currentDate, OnDateSetListener dateSetListener, Calendar calender) {
        DatePickerDialog dialog = new DatePickerDialog(context, dateSetListener, currentYear,
                currentMonth, currentDate);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {//setMinDate Added in API Level 11
            dialog.getDatePicker().setCalendarViewShown(false);
            //dialog.getDatePicker().setMinDate(calender.getTimeInMillis());
        }
        dialog.show();
    }

    /**
     * set the double digit format
     *
     * @param number
     * @return
     */
    public static String getDoubleDigitFormatNumber(int number) {
        return new DecimalFormat("00").format(number);
    }

    /**
     * set the double digit format
     *
     * @param number
     * @return
     */
    public static String getFourDigitFormatNumber(int number) {
        return new DecimalFormat("00").format(number);
    }

    /**
     * Show the assign home work quizz seleciton dialog
     *
     * @param context
     * @param selection
     * @param customeHomeworkTxt
     * @param wordProblemTxt
     * @param practiceSkillTxt
     */
    public static void showHomeworkQuizzSelectionDialog(Context context
            , final QuizzSelectionInterface selection,
                                                        String practiceSkillTxt, String wordProblemTxt,
                                                        String customeHomeworkTxt) {

        final Dialog dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_assign_homework_selection_quiztype);
        dialog.show();

        Button btnPracticeSkill = (Button) dialog.findViewById(R.id.btnPracticeSkill);
        Button btnSchoolCurriculum = (Button) dialog.findViewById(R.id.btnSchoolCurriculum);
        Button btnCustomeHomework = (Button) dialog.findViewById(R.id.btnCustomeHomework);

        RelativeLayout rlPracticeLayout = (RelativeLayout) dialog.findViewById(R.id.rlPracticeLayout);
        RelativeLayout rlWordProblem = (RelativeLayout) dialog.findViewById(R.id.rlWordProblem);
        RelativeLayout rlCustomeHomework = (RelativeLayout) dialog.findViewById(R.id.rlCustomeHomework);

        Button btncancel = (Button) dialog.findViewById(R.id.btncancel);

        btnPracticeSkill.setText(practiceSkillTxt);
        btnSchoolCurriculum.setText(wordProblemTxt);
        btnCustomeHomework.setText(customeHomeworkTxt);

        //for open for picture selection in the AddHomwwork Screen
        if (customeHomeworkTxt.length() == 0) {
            btnCustomeHomework.setVisibility(Button.GONE);
            rlCustomeHomework.setVisibility(RelativeLayout.GONE);
        }

        btnPracticeSkill.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                selection.onPracticeSkillSelected();
            }
        });

        btnSchoolCurriculum.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                selection.onWordProblemSelected();
            }
        });

        btnCustomeHomework.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                selection.onCustomeHomeworkSelected();
            }
        });


        rlPracticeLayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                selection.onPracticeSkillSelected();
            }
        });

        rlWordProblem.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                selection.onWordProblemSelected();
            }
        });

        rlCustomeHomework.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                selection.onCustomeHomeworkSelected();
            }
        });

        btncancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
    }

    /**
     * Check isShowByCCSS is downloaded or not
     *
     * @param context
     * @return
     */
    public static boolean isShowByCCSSDownLoaded(Context context, int grade) {
        AssessmentTestImpl implObj = new AssessmentTestImpl(context);
        implObj.openConnection();
        boolean isStandardLoaded = implObj.isStandard(grade);
        implObj.closeConnection();
        return isStandardLoaded;
    }

    /**
     * Download the CCSS standards
     *
     * @param context
     * @param request
     */
    public static void downloadCCSSStandards(Context context, final HttpServerRequest request) {
        new GetUpdatedDateAssessment(context, request).execute();
    }

    /**
     * Get Assessment date from server for standards
     *
     * @author Yashwant Singh
     */
    public static class GetUpdatedDateAssessment extends AsyncTask<Void, Void, String> {

        private ProgressDialog pd;
        private int lang = 1;
        private Context context = null;
        private HttpServerRequest request = null;

        GetUpdatedDateAssessment(Context context, HttpServerRequest request) {
            this.context = context;
            this.request = request;
            if (CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH)
                lang = CommonUtils.ENGLISH;
            else
                lang = CommonUtils.SPANISH;
        }

        @Override
        protected void onPreExecute() {
            pd = CommonUtils.getProgressDialog(context);
            pd.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            AssessmentTestServerOperation serverObj = new AssessmentTestServerOperation();
            String date = serverObj.getUpdatedDateAssessment(lang);
            return date;
        }

        @Override
        protected void onPostExecute(String date) {
            pd.cancel();

            if (date != null) {
                SharedPreferences sharedPrefForData = context.getSharedPreferences
                        (ICommonUtils.DATE_ASSESSMENT_STANDARD_DOWNLOAD, 0);
                downLoadStandars(sharedPrefForData, date, lang, context, request);
            } else {
                CommonUtils.showInternetDialog(context);
            }
            super.onPostExecute(date);
        }
    }

    /**
     * This method call when ready to download standards
     *
     * @param sharedPrefForData
     * @param date
     * @param lang
     * @param request
     */
    public static void downLoadStandars(SharedPreferences sharedPrefForData,
                                        String date, int lang, Context context, HttpServerRequest request) {

        SharedPreferences.Editor editor = sharedPrefForData.edit();
        editor.putString("assessmentStandardDate", date);
        editor.commit();

        new GetAssessmentStandards(lang, context, request).execute(null, null, null);
    }

    /**
     * Get Assessment Standards
     *
     * @author Yashwant Singh
     */
    public static class GetAssessmentStandards extends AsyncTask<Void, Void, AssessmentStandardDto> {

        private ProgressDialog pd;
        private int lang;
        private Context context = null;
        private HttpServerRequest request = null;

        GetAssessmentStandards(int lang, Context context, HttpServerRequest request) {
            this.request = request;
            this.lang = lang;
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            pd = CommonUtils.getProgressDialog(context);
            pd.show();
            super.onPreExecute();
        }

        @Override
        protected AssessmentStandardDto doInBackground(Void... params) {
            AssessmentTestServerOperation serverObj = new AssessmentTestServerOperation();
            AssessmentStandardDto assessmentStandard = serverObj.getAssessmentStandards(lang);
            if (assessmentStandard != null) {
                try {
                    AssessmentTestImpl implObj = new AssessmentTestImpl(context);
                    implObj.openConnection();
                    implObj.deleteFromWordAssessmentStandards();
                    implObj.deleteFromWordAssessmentSubStandards();
                    implObj.deleteFromWordAssessmentSubCategoriesInfo();
                    implObj.insertIntoWordAssessmentStandards
                            (assessmentStandard.getStandardDataList());
                    implObj.insertIntoWordAssessmentSubStandards
                            (assessmentStandard.getStandardDataList());
                    implObj.insertIntoWordAssessmentSubCategoriesInfo
                            (assessmentStandard.getCategoriesList());
                    implObj.closeConnection();
                } catch (Exception e) {
                    Log.e("AssessmentClass", "No Data on Server For Stabdards " + e.toString());
                }
            }
            return assessmentStandard;

        }

        @Override
        protected void onPostExecute(AssessmentStandardDto assessmentStandard) {
            pd.cancel();
            if (assessmentStandard != null)
                request.onRequestComplete();
            else
                CommonUtils.showInternetDialog(context);

            super.onPostExecute(assessmentStandard);
        }
    }

    /**
     * @param context
     * @param view
     */
    public static void showViewFromRightToLeftAnimation(Context context, View view) {
        animation = AnimationUtils.loadAnimation(context, R.anim.sliding_right_to_left_animation);
        view.setAnimation(animation);
    }

    //updated for new version updated on 24 Nov 2014

    /**
     * Generate warning dialog to play with computer for word problem
     *
     * @param context
     * @param internetNoTConnectedMsg
     * @param msg
     * @param noThankstext
     * @param okText
     * @param yesNoInterface
     */
    public static void generateWarningDialogToPlayWithComputer
    (Context context, String internetNoTConnectedMsg,
     String msg, String noThankstext, String okText,
     final YesNoListenerInterface yesNoInterface) {
        final Dialog dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_internet_warning_for_single_friendzy);
        dialog.show();

        TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById
                (R.id.txtAlertMsgRegisterLogIn);
        TextView mfAlertMsgWouldYouLikeToPlayWithComputer = (TextView)
                dialog.findViewById(R.id.mfAlertMsgWouldYouLikeToPlayWithComputer);
        Button btnOk = (Button) dialog.findViewById(R.id.dialogBtnOkForSingleFriendzy);
        Button btnTahnks = (Button) dialog.findViewById(R.id.btnNoThanks);

        btnOk.setText(okText);
        mfAlertMsgWouldYouLikeToPlayWithComputer.setText(msg);
        btnTahnks.setText(noThankstext);
        txtAlertMsgRegisterLogIn.setText(internetNoTConnectedMsg);

        btnOk.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }

                yesNoInterface.onYes();
            }
        });

        btnTahnks.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }

                yesNoInterface.onNo();
            }
        });
    }


    //for update player points for the player when internet is not connected
    //updated on 26 11 2014
    public static void insertPlayerPlayedDataIntoLocalEarnedScore
    (MathResultTransferObj mathResultObj, Context context) {
		/*Log.e(TAG, "Earned points " + mathResultObj.getTotalScore()
				+ " level " + mathResultObj.getLevel()
				+ " coins " + mathResultObj.getCoins());*/
        try {
            if (!CommonUtils.isInternetConnectionAvailable(context)) {
                String userId = mathResultObj.getUserId();
                String playerId = mathResultObj.getPlayerId();
                LearningCenterimpl learningCenterObj = new LearningCenterimpl(context);
                learningCenterObj.openConn();
                if (learningCenterObj.isPlayerDataExistInLocalEarnedScore(
                        userId, playerId)) {
                    MathResultTransferObj dbData = learningCenterObj.
                            getLocalEarnedScoreDataByUserIdAndPlayerId(userId, playerId);
                    mathResultObj.setTotalScore
                            (mathResultObj.getTotalScore() + dbData.getTotalScore());
                    mathResultObj.setCoins(mathResultObj.getCoins() + dbData.getCoins());
                }
                learningCenterObj.deleteFromLocalEarnedScore(userId, playerId);
                learningCenterObj.insertIntoLocalEarnedScore(mathResultObj);
                learningCenterObj.closeConn();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get player complete level
     *
     * @param playerId
     * @param context
     * @return
     */
    public static int getPlayerCompleteLavel(String playerId, Context context) {
        try {
            int completeLevel = 0;
            LearningCenterimpl learningCenter = new LearningCenterimpl(context);
            learningCenter.openConn();
            if (learningCenter.getDataFromPlayerTotalPoints(playerId)
                    .getCompleteLevel() != 0)
                completeLevel = learningCenter.getDataFromPlayerTotalPoints(playerId)
                        .getCompleteLevel();
            else
                completeLevel = Integer.parseInt(getSelectedPlayerDataById
                        (context, playerId).getCompletelavel());
            learningCenter.closeConn();
            return completeLevel;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }


    /**
     * Create the Json for the play data at the time of Internet not connected
     *
     * @param playerList
     * @return
     */
    private static String getStudentDataFromLocalEarnedScoreTableList
    (ArrayList<MathResultTransferObj> playerList) {
        try {
            JSONArray array = new JSONArray();
            for (int i = 0; i < playerList.size(); i++) {
                MathResultTransferObj obj = playerList.get(i);
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("userId", obj.getUserId());
                jsonObj.put("playerId", obj.getPlayerId());
                jsonObj.put("points", obj.getTotalScore());
                jsonObj.put("coins", obj.getCoins());
                array.put(jsonObj);
            }
            return array.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * Update the points and coins data from the LocalEarnedScore table ,
     * That are get at the time of play when Internet is not connected
     *
     * @param context
     * @param request
     */
    public static void updatepointsAndCoinsFromLocalEarnedScore
    (final Context context, final HttpServerRequest request) {
        try {
            LearningCenterimpl learningCenterObj = new LearningCenterimpl(context);
            learningCenterObj.openConn();

            ArrayList<MathResultTransferObj> playerList = learningCenterObj
                    .getPlayedDataListFromLocalEarnedScore();
            if (playerList.size() > 0) {
                HttpResponseInterface responseInterface = new HttpResponseInterface() {

                    @Override
                    public void serverResponse(HttpResponseBase httpResponseBase,
                                               int requestCode) {
                        LearningCenterimpl learningCenterObj = new LearningCenterimpl(context);
                        learningCenterObj.openConn();
                        learningCenterObj.deleteFromLocalEarnedScore();
                        learningCenterObj.closeConn();
                        request.onRequestComplete();
                    }
                };

                SaveCheckTeacherChangesParam param = new SaveCheckTeacherChangesParam();
                param.setAction("updatePointsAndCoins");
                param.setData(getStudentDataFromLocalEarnedScoreTableList(playerList));
				/*Log.e(TAG, "jsonString " + getStudentDataFromLocalEarnedScoreTableList
						(playerList));*/
                //request.onRequestComplete();

                new MyAsyckTask(ServerOperation.createPostRequestForAddLocalPlayData(param)
                        , null, ServerOperationUtil.SAVE_PLAY_SCORE_WHEN_NO_INTERNET, context,
                        responseInterface, ServerOperationUtil.SIMPLE_DIALOG, false,
                        context.getString(R.string.please_wait_dialog_msg))
                        .execute();
            } else {
                request.onRequestComplete();
            }
            learningCenterObj.closeConn();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Check Capture image bug
     *
     * @return
     */
    public static boolean hasImageCaptureBug() {
        // list of known devices that have the bug
        ArrayList<String> devices = new ArrayList<String>();
        devices.add("android-devphone1/dream_devphone/dream");
        devices.add("generic/sdk/generic");
        devices.add("vodafone/vfpioneer/sapphire");
        devices.add("tmobile/kila/dream");
        devices.add("verizon/voles/sholes");
        devices.add("google_ion/google_ion/sapphire");

        return devices.contains(android.os.Build.BRAND + "/" + android.os.Build.PRODUCT + "/"
                + android.os.Build.DEVICE);
    }

    /**
     * Convert IS into Bytearray
     *
     * @param inputStream
     * @return
     */
    public static byte[] convertInputStreamToByteArray(InputStream inputStream) {
        byte[] bytes = null;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            byte data[] = new byte[1024];
            int count;

            while ((count = inputStream.read(data)) != -1) {
                bos.write(data, 0, count);
            }

            bos.flush();
            bos.close();
            inputStream.close();

            bytes = bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bytes;
    }

    /**
     * Create a file Uri for saving an image or video
     */
    public static Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * Create a File for saving an image or video
     */
    @SuppressLint("SimpleDateFormat")
    private static File getOutputMediaFile(int type) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

		/*File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
	              Environment.DIRECTORY_PICTURES), "MyCameraApp");*/
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/MathFriendzy", "Camera");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }
        return mediaFile;
    }

    /**
     * Return the image
     *
     * @param uri
     * @return
     */
    public static String getImagePathUsingUri(Context context, Uri uri) {
        try {
            return uri.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //for getting path

    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri     The Uri to query.
     * @author paulburke
     */
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }
    //end get path

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }


    //for offlile homework

    /**
     * Save the homework json response string into LocalDB
     * First delete the existing record and then insert the new one
     *
     * @param context
     * @param userId
     * @param playerId
     * @param jsonString
     */
    public static void saveHomeworkJsonIntoLocalDB(Context context,
                                                   String userId, String playerId, String jsonString) {
        try {
            HomeWorkImpl implObj = new HomeWorkImpl(context);
            implObj.openConnection();
            implObj.deleteFromHomeworkByUserIDAndPlayerId(userId, playerId);
            implObj.insertIntoHomeTable(userId, playerId, jsonString);
            implObj.closeConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Return the Homework json String from LocalDB by user id and player id
     *
     * @param context
     * @param userId
     * @param playerId
     * @return
     */
    public static String getHomeworkJsonByUserIdAndPlayerId(Context context,
                                                            String userId, String playerId) {
        String responseJson = null;
        try {
            HomeWorkImpl implObj = new HomeWorkImpl(context);
            implObj.openConnection();
            responseJson = implObj.getHomeworkJsonByUserIdAndPlayerId(userId, playerId);
            implObj.closeConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseJson;
    }

    /**
     * Create the json string for the given home work list data
     *
     * @param homeWorkresult
     * @return
     */
    public static String getJsonStringFromGivenHWData
    (ArrayList<GetHomeWorkForStudentWithCustomeResponse> homeWorkresult) {
        try {

            JSONObject json = new JSONObject();
            json.put("result", "success");

            JSONArray jsonDataArray = new JSONArray();
            for (int i = 0; i < homeWorkresult.size(); i++) {
                GetHomeWorkForStudentWithCustomeResponse
                        jsonData = homeWorkresult.get(i);
                JSONObject jsonDataObj = new JSONObject();
                jsonDataObj.put("homeworkId", jsonData.getHomeWorkId());
                jsonDataObj.put("date", jsonData.getDate());
                jsonDataObj.put("grade", jsonData.getGrade());
                jsonDataObj.put("finished", jsonData.getFinished());
                jsonDataObj.put("practiceTime", jsonData.getPracticeTime());
                jsonDataObj.put("practiceScore", jsonData.getPracticeScore());
                jsonDataObj.put("wordTime", jsonData.getWordTime());
                jsonDataObj.put("wordScore", jsonData.getWordScore());
                jsonDataObj.put("customScore", jsonData.getCustomeScore());
                jsonDataObj.put("avgScore", jsonData.getAvgScore());
                jsonDataObj.put("zeroCredit", jsonData.getZeroCredit());
                jsonDataObj.put("halfCredit", jsonData.getHalfCredit());
                jsonDataObj.put("fullCredit", jsonData.getFullCredit());
                jsonDataObj.put("totalQuestions", jsonData.getTotalQuestion());
                jsonDataObj.put("message", jsonData.getMessage());
                jsonDataObj.put("className" , jsonData.getClassName());
                jsonDataObj.put("subjectName" , jsonData.getSubjectName());
                jsonDataObj.put("subjectId" , jsonData.getSubId());

                JSONArray jsonCustomeArray = new JSONArray();
                ArrayList<CustomeResult> customeResultList = jsonData.getCustomeresultList();
                for (int j = 0; j < customeResultList.size(); j++) {
                    CustomeResult customeResult = customeResultList.get(j);
                    JSONObject jsonCustomeObj = new JSONObject();
                    jsonCustomeObj.put("customHwId", customeResult.getCustomHwId());
                    jsonCustomeObj.put("title", customeResult.getTitle());
                    jsonCustomeObj.put("problems", customeResult.getProblem());
                    jsonCustomeObj.put("allowChanges", customeResult.getAllowChanges());
                    jsonCustomeObj.put("showAnswers", customeResult.getShowAns());
                    jsonCustomeObj.put("score", customeResult.getScore());
                    jsonCustomeObj.put("questionSheet", customeResult.getSheetName());
                    jsonCustomeObj.put("resourceAdded" , customeResult.getResourceAdded());


                    try {
                        //for link changes
                        ArrayList<AddUrlToWorkArea> links = customeResult.getLinks();
                        JSONArray jsonLinkArray = new JSONArray();
                        if (links != null && links.size() > 0) {
                            for (int l = 0; l < links.size(); l++) {
                                AddUrlToWorkArea link = links.get(l);
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("title", link.getTitle());
                                jsonObject.put("url", link.getUrl());
                                jsonLinkArray.put(jsonObject);
                            }
                        }
                        jsonCustomeObj.put("links", jsonLinkArray);
                        //end link changes
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    ArrayList<CustomeAns> customeAnsList = customeResult.getCustomeAnsList();
                    JSONArray jsonAnsArray = new JSONArray();
                    if(customeAnsList != null && customeAnsList.size() > 0) {
                        for (int k = 0; k < customeAnsList.size(); k++) {
                            CustomeAns customeAns = customeAnsList.get(k);
                            JSONObject customeAnsObj = new JSONObject();
                            customeAnsObj.put("quesNum", customeAns.getQueNo());
                            customeAnsObj.put("options", customeAns.getOptions());
                            customeAnsObj.put("correctAns", customeAns.getCorrectAns());
                            customeAnsObj.put("ansSuffix", customeAns.getAnsSuffix());
                            customeAnsObj.put("isLeftUnit", customeAns.getIsLeftUnit());
                            customeAnsObj.put("fillInType", customeAns.getFillInType());
                            customeAnsObj.put("answerType", customeAns.getAnswerType());
                            customeAnsObj.put("ansAvailable", customeAns.getIsAnswerAvailable());
                            customeAnsObj.put("questionStr" , customeAns.getQuestionString());
                            jsonAnsArray.put(customeAnsObj);
                        }
                    }
                    jsonCustomeObj.put("answers", jsonAnsArray);

                    ArrayList<CustomePlayerAns> customePlayerAnsList = customeResult
                            .getCustomePlayerAnsList();
                    JSONArray jsonCustomePlayerAnsArray = new JSONArray();
                    if(customePlayerAnsList != null && customePlayerAnsList.size() > 0) {
                        for (int l = 0; l < customePlayerAnsList.size(); l++) {
                            CustomePlayerAns playerAns = customePlayerAnsList.get(l);
                            JSONObject playerAnsObj = new JSONObject();
                            playerAnsObj.put("quesNum", playerAns.getQueNo());
                            playerAnsObj.put("answer", playerAns.getAns());
                            playerAnsObj.put("isCorrect", playerAns.getIsCorrect());
                            playerAnsObj.put("workImage", playerAns.getWorkImage());
                            playerAnsObj.put("firstTimeWrong", playerAns.getFirstTimeWrong());
                            playerAnsObj.put("teacher_credit", playerAns.getTeacherCredit());
                            playerAnsObj.put("message", playerAns.getMessage());
                            playerAnsObj.put("workAreaPublic", playerAns.getIsWorkAreaPublic());
                            playerAnsObj.put("getHelp", playerAns.getIsGetHelp());
                            playerAnsObj.put("questionImage", playerAns.getQuestionImage());
                            playerAnsObj.put("chatRequestId", playerAns.getChatRequestedId());
                            playerAnsObj.put("fixedWorkImage", playerAns.getFixedWorkImage());
                            playerAnsObj.put("fixedMessage", playerAns.getFixedMessage());
                            playerAnsObj.put("fixedQuestionImage", playerAns.getFixedQuestionImage());
                            playerAnsObj.put("workInput", playerAns.getWorkInput());
                            playerAnsObj.put("opponentInput", playerAns.getOpponentInput());

                            try {
                                //for player ans link changes
                                ArrayList<AddUrlToWorkArea> playerAnsLinks = playerAns.getUrlLinks();
                                JSONArray jsonLinkForPlAnsArray = new JSONArray();
                                if (playerAnsLinks != null && playerAnsLinks.size() > 0) {
                                    for (int m = 0; m < playerAnsLinks.size(); m++) {
                                        AddUrlToWorkArea link = playerAnsLinks.get(m);
                                        JSONObject jsonObject = new JSONObject();
                                        jsonObject.put("title", link.getTitle());
                                        jsonObject.put("url", link.getUrl());
                                        jsonLinkForPlAnsArray.put(jsonObject);
                                    }
                                }
                                playerAnsObj.put("links", jsonLinkForPlAnsArray);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            //end changes
                            jsonCustomePlayerAnsArray.put(playerAnsObj);
                        }
                    }
                    jsonCustomeObj.put("playerAns", jsonCustomePlayerAnsArray);
                    jsonCustomeArray.put(jsonCustomeObj);
                }
                jsonDataObj.put("customResult", jsonCustomeArray);

                ArrayList<PracticeResult> practiceresultList = jsonData.getPractiseResultList();
                if (practiceresultList.size() > 0) {
                    JSONObject practiceJsonObj = new JSONObject();
                    for (int m = 0; m < practiceresultList.size(); m++) {
                        PracticeResult practiceResult = practiceresultList.get(m);
                        JSONArray practiceResultJsonArray = new JSONArray();
                        ArrayList<PracticeResultSubCat> practiceResultSubCatList =
                                practiceResult.getPracticeResultSubCatList();
                        for (int o = 0; o < practiceResultSubCatList.size(); o++) {
                            PracticeResultSubCat subCat = practiceResultSubCatList.get(o);
                            JSONObject practiceResultObj = new JSONObject();
                            practiceResultObj.put("subCategId", subCat.getSubCatId());
                            practiceResultObj.put("problems", subCat.getProblems());
                            practiceResultObj.put("time", subCat.getTime());
                            practiceResultObj.put("score", subCat.getScore());
                            practiceResultJsonArray.put(practiceResultObj);
                        }
                        practiceJsonObj.put(practiceResult.getCatId(),
                                practiceResultJsonArray);
                    }
                    jsonDataObj.put("practiceResult", practiceJsonObj);
                } else {
                    JSONArray practiceResultJsonArray = new JSONArray();
                    jsonDataObj.put("practiceResult", practiceResultJsonArray);
                }

                ArrayList<WordResult> wordResultList = jsonData.getWordResultList();
                if (wordResultList.size() > 0) {
                    JSONObject wordJsonObj = new JSONObject();
                    for (int n = 0; n < wordResultList.size(); n++) {
                        WordResult wordResult = wordResultList.get(n);
                        ArrayList<WordSubCatResult> subCatResultList = wordResult
                                .getSubCatResultList();
                        JSONArray wordResultJsonArray = new JSONArray();
                        for (int p = 0; p < subCatResultList.size(); p++) {
                            WordSubCatResult subCat = subCatResultList.get(p);
                            JSONObject wordResultObj = new JSONObject();
                            wordResultObj.put("subCategId", subCat.getSubCatId());
                            wordResultObj.put("time", subCat.getTime());
                            wordResultObj.put("score", subCat.getScore());
                            wordResultJsonArray.put(wordResultObj);
                        }
                        wordJsonObj.put(wordResult.getCatIg(), wordResultJsonArray);
                    }
                    jsonDataObj.put("wordResult", wordJsonObj);
                } else {
                    JSONArray wordResultJsonArray = new JSONArray();
                    jsonDataObj.put("wordResult", wordResultJsonArray);
                }

                jsonDataArray.put(jsonDataObj);
            }
            json.put("data", jsonDataArray);
            return json.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Update the main homework list
     *
     * @param response
     */
    public static void updateMainHomeworkList(SaveHomeWorkServerResponse response) {
        try {
            if (response != null) {
                HomeWorkListAdapter.homeWorkresult.get
                        (ActHomeWork.selectedPosition).
                        setFinished(response.getFinished());
                HomeWorkListAdapter.homeWorkresult.get
                        (ActHomeWork.selectedPosition).
                        setPracticeTime(response.getPracticeTime());
                HomeWorkListAdapter.homeWorkresult.get
                        (ActHomeWork.selectedPosition).
                        setPracticeScore(response.getPracticeScore());
                HomeWorkListAdapter.homeWorkresult.get
                        (ActHomeWork.selectedPosition).
                        setWordTime(response.getWordTime());
                HomeWorkListAdapter.homeWorkresult.get
                        (ActHomeWork.selectedPosition).
                        setWordScore(response.getWordScore());
                HomeWorkListAdapter.homeWorkresult.get
                        (ActHomeWork.selectedPosition).
                        setCustomeScore(response.getCustomeScore());
                HomeWorkListAdapter.homeWorkresult.get
                        (ActHomeWork.selectedPosition).
                        setAvgScore(response.getAvgScore());
                HomeWorkListAdapter.homeWorkresult.get
                        (ActHomeWork.selectedPosition)
                        .setHalfCredit(response.getHalfCredit());
                HomeWorkListAdapter.homeWorkresult.get
                        (ActHomeWork.selectedPosition)
                        .setFullCredit(response.getFullCredit());
                HomeWorkListAdapter.homeWorkresult.get
                        (ActHomeWork.selectedPosition)
                        .setZeroCredit(response.getZeroCredit());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Create and save json into local
     *
     * @param context
     * @param homeWorkresult
     * @param userId
     * @param playerId
     */
    public static void createJsonForHomeworkAndSaveIntoLocal(final Context context,
                                                             final ArrayList<GetHomeWorkForStudentWithCustomeResponse> homeWorkresult
            , final String userId, final String playerId, final HttpServerRequest response) {
        new AsyncTask<Void, Void, Void>() {
            //private ProgressDialog pd;
            @Override
            protected void onPreExecute() {
				/*pd = ServerDialogs.getProgressDialog(ActCheckHomeWork.this, 
						"Please wait" , 1);
				pd.show();*/
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                String jsonStringAfterPlayed = MathFriendzyHelper
                        .getJsonStringFromGivenHWData(homeWorkresult);
                if (CommonUtils.LOG_ON)
                    Log.e(TAG, "inside createJsonForHomeworkAndSaveIntoLocal json String "
                            + jsonStringAfterPlayed);
                MathFriendzyHelper.saveHomeworkJsonIntoLocalDB
                        (context, userId, playerId, jsonStringAfterPlayed);
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                //pd.cancel();
                response.onRequestComplete();
                super.onPostExecute(result);
            }
        }.execute();
    }


    /**
     * Get updated homework response after playing practice skill Homework
     *
     * @param catId
     * @param subCatId
     * @param subCatScore
     * @param totalTimeTaken
     * @return
     */
    public static SaveHomeWorkServerResponse getUpdatedResultForPracticeSkillForOfflineHomework(
            String catId, int subCatId, String subCatScore, long totalTimeTaken) {
        try {
            //global update
            for (int i = 0; i < HomeWorkListAdapter.homeWorkresult.
                    get(ActHomeWork.selectedPosition).getPractiseResultList().size(); i++) {
                if (catId.equals(HomeWorkListAdapter.homeWorkresult
                        .get(ActHomeWork.selectedPosition).getPractiseResultList()
                        .get(i).getCatId())) {
                    for (int j = 0; j < HomeWorkListAdapter.homeWorkresult
                            .get(ActHomeWork.selectedPosition).getPractiseResultList()
                            .get(i).getPracticeResultSubCatList().size(); j++) {
                        if (subCatId == HomeWorkListAdapter.homeWorkresult
                                .get(ActHomeWork.selectedPosition).getPractiseResultList()
                                .get(i).getPracticeResultSubCatList().get(j).getSubCatId()) {
                            HomeWorkListAdapter.homeWorkresult
                                    .get(ActHomeWork.selectedPosition).getPractiseResultList()
                                    .get(i).getPracticeResultSubCatList().get(j)
                                    .setScore(subCatScore);
                            HomeWorkListAdapter.homeWorkresult
                                    .get(ActHomeWork.selectedPosition).getPractiseResultList()
                                    .get(i).getPracticeResultSubCatList().get(j)
                                    .setTime(totalTimeTaken + "");
                        }
                    }
                }
            }
            return getSaveHomeworkResponse(getCalculatedScore
                    (HomeWorkListAdapter.homeWorkresult.get
                            (ActHomeWork.selectedPosition)), subCatScore);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Get updated homework response after playing word problem homework Homework
     *
     * @param catId
     * @param subCatId
     * @param subCatScore
     * @param totalTimeTaken
     * @return
     */
    public static SaveHomeWorkServerResponse getUpdatedResultForWordProblemForOfflineHomework(
            String catId, int subCatId, String subCatScore, int totalTimeTaken) {
        try {
            //global update
            for (int i = 0; i < HomeWorkListAdapter.homeWorkresult
                    .get(ActHomeWork.selectedPosition)
                    .getWordResultList().size(); i++) {
                if (catId.equals(HomeWorkListAdapter.homeWorkresult
                        .get(ActHomeWork.selectedPosition)
                        .getWordResultList()
                        .get(i).getCatIg())) {
                    for (int j = 0; j < HomeWorkListAdapter.homeWorkresult
                            .get(ActHomeWork.selectedPosition)
                            .getWordResultList()
                            .get(i).getSubCatResultList().size(); j++) {
                        if (subCatId == HomeWorkListAdapter.homeWorkresult
                                .get(ActHomeWork.selectedPosition)
                                .getWordResultList()
                                .get(i).getSubCatResultList().get(j).getSubCatId()) {
                            HomeWorkListAdapter.homeWorkresult.get(ActHomeWork.selectedPosition)
                                    .getWordResultList()
                                    .get(i).getSubCatResultList().get(j)
                                    .setScore(subCatScore);
                            HomeWorkListAdapter.homeWorkresult.get(ActHomeWork.selectedPosition)
                                    .getWordResultList()
                                    .get(i).getSubCatResultList().get(j)
                                    .setTime(totalTimeTaken + "");
                        }
                    }
                }
            }
            return getSaveHomeworkResponse(getCalculatedScore
                    (HomeWorkListAdapter.homeWorkresult.get
                            (ActHomeWork.selectedPosition)), subCatScore);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Get updated homework response after playing custome homework Homework
     *
     * @param subCatScore
     * @param customePlayListByStudent
     * @return
     */
    public static SaveHomeWorkServerResponse getUpdatedResultForCustomeForOfflineHomework(
            int selectedCustomeHWId, String subCatScore,
            ArrayList<CustomePlayerAns> customePlayListByStudent) {
        try {

            if (CommonUtils.LOG_ON)
                Log.e(TAG, "SaveHomeWorkServerResponse customePlayListByStudent size "
                        + customePlayListByStudent.size());

            //global update
            for (int i = 0; i < HomeWorkListAdapter.homeWorkresult
                    .get(ActHomeWork.selectedPosition)
                    .getCustomeresultList().size(); i++) {
                if (selectedCustomeHWId ==
                        HomeWorkListAdapter.homeWorkresult.get
                                (ActHomeWork.selectedPosition).
                                getCustomeresultList().get(i)
                                .getCustomHwId()) {
                    HomeWorkListAdapter.homeWorkresult.get
                            (ActHomeWork.selectedPosition).
                            getCustomeresultList().get(i)
                            .setScore(subCatScore);
                    HomeWorkListAdapter.homeWorkresult.get
                            (ActHomeWork.selectedPosition).
                            getCustomeresultList().get(i)
                            .setCustomePlayerAnsList(customePlayListByStudent);
                    HomeWorkListAdapter.homeWorkresult.get
                            (ActHomeWork.selectedPosition).
                            getCustomeresultList().get(i).setAlreadyPlayed(true);
                }
            }
            return getSaveHomeworkResponse(getCalculatedScore
                    (HomeWorkListAdapter.homeWorkresult.get
                            (ActHomeWork.selectedPosition)), subCatScore);
        } catch (Exception e) {
            e.printStackTrace();
            MathFriendzyHelper.WriteLog("MathHelper" ,
                    "Inside getUpdatedResultForCustomeForOfflineHomework" , e);
            return null;
        }
    }

    /**
     * Convert the string value into integer
     *
     * @param strValue
     * @return
     */
    private static int convertStringToInt(String strValue) {
        try {
            return Integer.parseInt(strValue);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * Calculate the avg score
     *
     * @param offlineResponse
     * @return
     */
    private static int getAvgScore(OffLineHomeworkResponse offlineResponse) {
        try {
            return Math.round((((offlineResponse.getFullCredit()
                    + ((float) offlineResponse.getHalfCredit() / 2)) * 100)
                    / offlineResponse.getTotalProblem()));
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * Convert the devident value into round off figure
     */
    private static int getRoundOfValues(int devident, int devisor) {
        try {
            if (devident == -1)
                return devident;
            return Math.round((devident / (float) devisor));
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * Save the offline homework to play practice skill into local Database
     *
     * @param context
     * @param param
     */
    public static void savePracticeSkillForOfflineHW(Context context,
                                                     SaveHomeWorkScoreParam param) {
        HomeWorkImpl homeworkImpl = new HomeWorkImpl(context);
        homeworkImpl.openConnection();
        homeworkImpl.deleteFromPracticeSkillHW(param);
        homeworkImpl.insertIntoPracticeSkillHW(param);
        homeworkImpl.closeConnection();
    }

    /**
     * Save the offline homework to play word problem into local Database
     *
     * @param context
     * @param param
     */
    public static void saveWordForOfflineHW(Context context,
                                            SaveHomeWorkWordPlayedDataParam param) {
        HomeWorkImpl homeworkImpl = new HomeWorkImpl(context);
        homeworkImpl.openConnection();
        homeworkImpl.deleteFromWordProblemHW(param);
        homeworkImpl.insertIntoWordProblemHW(param);
        homeworkImpl.closeConnection();
    }

    /**
     * Save the offline homework to play custome homework into local Database
     *
     * @param context
     * @param param
     */
    public static void saveCustomeForOfflineHW(Context context,
                                               AddCustomeAnswerByStudentParam param) {
        HomeWorkImpl homeworkImpl = new HomeWorkImpl(context);
        homeworkImpl.openConnection();
        homeworkImpl.deleteFromCustomeHW(param);
        homeworkImpl.insertIntoCustomeHW(param);
        homeworkImpl.closeConnection();
    }

    /**
     * Return the calculated credit for each practice, word and custome play
     *
     * @param getHomeWorkForStudentWithCustomeResponse
     * @return
     */
    private static OffLineHomeworkResponse getCalculatedScore
    (GetHomeWorkForStudentWithCustomeResponse getHomeWorkForStudentWithCustomeResponse) {

        OffLineHomeworkResponse offlineResponse = new OffLineHomeworkResponse();
        int totalProblem = 0;
        int fullCredit = 0;
        int halfCredit = 0;
        int zeroCredit = 0;

        //for practice time and score
        int practiceTime = 0;
        int practiceScore = 0;
        int noOfPracticePlayed = 0;//number of practice skill are played
        int practiceTimeForEachCat = 0;

        //for word time and score
        int wordTime = 0;
        int wordScore = 0;
        int noOfWordPlayed = 0;//number of word problem are played
        int wordTimeForEachCat = 0;

        //for custome score
        int customeScore = 0;
        int noOfCustomePlayed = 0;//number of custome problem are played

        //calculate for practice skill
        ArrayList<PracticeResult> practiseResultList = HomeWorkListAdapter.homeWorkresult.
                get(ActHomeWork.selectedPosition).getPractiseResultList();

        if (practiseResultList.size() > 0) {
            for (int i = 0; i < practiseResultList.size(); i++) {
                ArrayList<PracticeResultSubCat> practiceResultSubCatList = practiseResultList
                        .get(i).getPracticeResultSubCatList();
                for (int j = 0; j < practiceResultSubCatList.size(); j++) {
                    totalProblem = totalProblem + practiceResultSubCatList.get(j).getProblems();
                    OffLineHomeworkResponse teacherCredit = calculateCredtitForPracticeAndWord
                            (practiceResultSubCatList.get(j).getScore()
                                    , practiceResultSubCatList.get(j).getProblems());

                    if (teacherCredit != null) {
                        fullCredit = fullCredit + teacherCredit.getFullCredit();
                        zeroCredit = zeroCredit + teacherCredit.getZeroCredit();
                    }

                    //for practime and practice score
                    practiceTimeForEachCat = convertStringToInt(practiceResultSubCatList
                            .get(j).getTime());
                    practiceTime = practiceTime + practiceTimeForEachCat;
                    if (practiceTimeForEachCat > 0) {
                        noOfPracticePlayed++;
                        practiceScore = practiceScore + convertStringToInt(practiceResultSubCatList
                                .get(j).getScore());
                    }
                }
            }

            if (CommonUtils.LOG_ON)
                Log.e(TAG, "practice time " + practiceTime
                        + " score " + getRoundOfValues(practiceScore, noOfPracticePlayed));

        } else {
            practiceScore = -1;
        }

        //calculate for word problem
        ArrayList<WordResult> wordResultList = HomeWorkListAdapter.homeWorkresult.
                get(ActHomeWork.selectedPosition).getWordResultList();
        if (wordResultList.size() > 0) {
            for (int i = 0; i < wordResultList.size(); i++) {
                ArrayList<WordSubCatResult> subCatResultList = wordResultList
                        .get(i).getSubCatResultList();
                for (int j = 0; j < subCatResultList.size(); j++) {
                    totalProblem = totalProblem + 10;
                    OffLineHomeworkResponse teacherCredit = calculateCredtitForPracticeAndWord
                            (subCatResultList.get(j).getScore(), 10);
                    if (teacherCredit != null) {
                        fullCredit = fullCredit + teacherCredit.getFullCredit();
                        zeroCredit = zeroCredit + teacherCredit.getZeroCredit();
                    }

                    //for wordtime and word score
                    wordTimeForEachCat = convertStringToInt(subCatResultList.get(j).getTime());
                    wordTime = wordTime + wordTimeForEachCat;
                    if (wordTimeForEachCat > 0) {
                        noOfWordPlayed++;
                        wordScore = wordScore + convertStringToInt(subCatResultList.get(j)
                                .getScore());
                    }
                }
            }

            if (CommonUtils.LOG_ON)
                Log.e(TAG, "word time " + wordTime
                        + " score " + getRoundOfValues(wordScore, noOfWordPlayed));
        } else {
            wordScore = -1;
        }

        //calculate for custome
        ArrayList<CustomeResult> customeresultList = HomeWorkListAdapter.homeWorkresult.
                get(ActHomeWork.selectedPosition).getCustomeresultList();

        if (customeresultList.size() > 0) {
            for (int i = 0; i < customeresultList.size(); i++) {
                totalProblem = totalProblem + customeresultList.get(i).getProblem();
                OffLineHomeworkResponse teacherCredit = calculateCreditForCustome
                        (customeresultList.get(i));
                if (teacherCredit != null) {
                    fullCredit = fullCredit + teacherCredit.getFullCredit();
                    zeroCredit = zeroCredit + teacherCredit.getZeroCredit();
                    halfCredit = halfCredit + teacherCredit.getHalfCredit();
                }

                //for custome score
                customeScore = customeScore + convertStringToInt(customeresultList.get(i).getScore());
                if (customeresultList.get(i).isAlreadyPlayed()) {
                    noOfCustomePlayed++;
                }
            }

            if (CommonUtils.LOG_ON)
                Log.e(TAG, " custome score " +
                        getRoundOfValues(customeScore, noOfCustomePlayed));
        } else {
            customeScore = -1;
        }

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "total problem " + totalProblem
                    + " total full " + fullCredit + " total half " + halfCredit + " total zero "
                    + zeroCredit);

        offlineResponse.setPracticeTime(practiceTime);
        offlineResponse.setPracticeScore(getRoundOfValues(practiceScore, noOfPracticePlayed));
        offlineResponse.setWordTime(wordTime);
        offlineResponse.setWordScore(getRoundOfValues(wordScore, noOfWordPlayed));
        offlineResponse.setCustomeScore(getRoundOfValues(customeScore, noOfCustomePlayed));
        offlineResponse.setFullCredit(fullCredit);
        offlineResponse.setHalfCredit(halfCredit);
        offlineResponse.setZeroCredit(zeroCredit);
        offlineResponse.setTotalProblem(totalProblem);
        if (CommonUtils.LOG_ON)
            Log.e(TAG, "avg score " + getAvgScore(offlineResponse));
        offlineResponse.setAvgScore(getAvgScore(offlineResponse));
        return offlineResponse;
    }

    /**
     * Calculate the teacher credit for work ana practice skill , based on its score for
     * each category
     *
     * @param score
     * @param totlaProblem
     * @return
     */
    private static OffLineHomeworkResponse calculateCredtitForPracticeAndWord(String score,
                                                                              int totlaProblem) {
        int intScore = 0;
        try {
            OffLineHomeworkResponse teacherCredit = new OffLineHomeworkResponse();
            intScore = Integer.parseInt(score);
            int fullCredit = (intScore * totlaProblem) / 100;
            int zeroCredit = totlaProblem - fullCredit;
            teacherCredit.setFullCredit(fullCredit);
            teacherCredit.setZeroCredit(zeroCredit);
            teacherCredit.setHalfCredit(0);

            if (CommonUtils.LOG_ON)
                Log.e(TAG, "score " + intScore + " total " + totlaProblem
                        + " full credit " + fullCredit + " zero credit "
                        + zeroCredit);

            return teacherCredit;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Calculate the credit for custome ans
     *
     * @param customeResult
     * @return
     */
    private static OffLineHomeworkResponse calculateCreditForCustome(CustomeResult customeResult) {
        try {
            OffLineHomeworkResponse teacherCredit = new OffLineHomeworkResponse();
            int halfCredit = 0;
            int fullCredit = 0;
            int zeroCredit = 0;

            ArrayList<CustomePlayerAns> customePlayerAnsList = customeResult
                    .getCustomePlayerAnsList();
            if (customePlayerAnsList.size() > 0) {
                for (int i = 0; i < customePlayerAnsList.size(); i++) {
                    String credit = customePlayerAnsList.get(i).getTeacherCredit();
                    if (credit.equalsIgnoreCase(FULL_CREDIT + "")) {
                        fullCredit++;
                    } else if (credit.equalsIgnoreCase(HALF_CREDIT + "")) {
                        halfCredit++;
                    } else if (credit.equalsIgnoreCase(NONE_CREDITT + "")) {
                        if (customePlayerAnsList.get(i).getIsCorrect() == 0) {
                            zeroCredit++;
                        } else {
                            fullCredit++;
                        }
                    } else {
                        zeroCredit++;
                    }
                }
            } else {
                zeroCredit = customeResult.getProblem();
            }

            teacherCredit.setFullCredit(fullCredit);
            teacherCredit.setHalfCredit(halfCredit);
            teacherCredit.setZeroCredit(zeroCredit);

            if (CommonUtils.LOG_ON)
                Log.e(TAG, "half credit " + halfCredit + " full "
                        + fullCredit + " zero " + zeroCredit);

            return teacherCredit;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Return the save home resposne when play offline
     *
     * @param teacherCreditFinal
     * @param currentCatPlayScore
     * @return
     */
    private static SaveHomeWorkServerResponse getSaveHomeworkResponse(OffLineHomeworkResponse
                                                                              teacherCreditFinal, String currentCatPlayScore) {
        try {
            SaveHomeWorkServerResponse response = new SaveHomeWorkServerResponse();
            response.setCurrentPlaySubCatScore(currentCatPlayScore);
            response.setFullCredit(teacherCreditFinal.getFullCredit() + "");
            response.setHalfCredit(teacherCreditFinal.getHalfCredit() + "");
            response.setZeroCredit(teacherCreditFinal.getZeroCredit() + "");
            response.setTotalQuestion(teacherCreditFinal.getTotalProblem() + "");
            response.setFinished("0");
            response.setPracticeTime(teacherCreditFinal.getPracticeTime() + "");
            if (teacherCreditFinal.getPracticeScore() == -1)
                response.setPracticeScore("");
            else
                response.setPracticeScore(teacherCreditFinal.getPracticeScore() + "");
            response.setWordTime(teacherCreditFinal.getWordTime() + "");

            if (teacherCreditFinal.getWordScore() == -1)
                response.setWordScore("");
            else
                response.setWordScore(teacherCreditFinal.getWordScore() + "");

            if (teacherCreditFinal.getCustomeScore() == -1)
                response.setCustomeScore("");
            else
                response.setCustomeScore(teacherCreditFinal.getCustomeScore() + "");

            response.setAvgScore(teacherCreditFinal.getAvgScore() + "");
            response.setResult("success");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            MathFriendzyHelper.WriteLog("MathHelper" , "Inside getSaveHomeworkResponse" , e);
            return null;
        }
    }


    /**
     * Return the homework image list which are saved at the time of offline play
     * @param customeList
     * @return
     */
	/*private static ArrayList<HWImage> getHWImages
	(ArrayList<AddCustomeAnswerByStudentParam> customeList){
		ArrayList<HWImage> imageList = new ArrayList<HWImage>();
		for(int i = 0 ; i < customeList.size() ; i ++ ){
			AddCustomeAnswerByStudentParam param = customeList.get(i);
			try {
				JSONArray questionArray = new JSONArray(param.getQuestionJson());
				for(int j = 0 ; j < questionArray.length() ; j ++ ){
					JSONObject jsonObj = questionArray.getJSONObject(j);
					HWImage image = new HWImage();
					image.setQuestionImage(jsonObj.getString("questionImage"));
					image.setWorkAreaImage(jsonObj.getString("workImageName"));
					imageList.add(image);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return imageList;
	}*/

    /**
     * Save the offline homework data from local to server when internet is connected
     *
     * @param context
     * @param request
     */
    public static void saveOfflineHWDataOnServer(final Context context,
                                                 final HttpServerRequest request) {

        try {
            new AsyncTask<Void, Void, ArrayList<HWImage>>() {

                @Override
                protected void onPreExecute() {

                };

                @Override
                protected ArrayList<HWImage> doInBackground(Void... params) {

                    //String pref = "";
                    //ArrayList<String> urlList = new ArrayList<String>();

                    HomeWorkImpl homeworkImpl = new HomeWorkImpl(context);
                    homeworkImpl.openConnection();
                    ArrayList<SaveHomeWorkScoreParam> practiceList = homeworkImpl
                            .getPracticeSkillHW();
                    ArrayList<SaveHomeWorkWordPlayedDataParam> wordList = homeworkImpl
                            .getWordProblemHW();
                    ArrayList<AddCustomeAnswerByStudentParam> customeList = homeworkImpl
                            .getCustomeHW();
                    ArrayList<SaveGDocLinkOnServerParam> userLinks = homeworkImpl.getUserLinks();

                    for (int i = 0; i < practiceList.size(); i++) {

						/*urlList.add(pref + URLEncoder.encode(ServerOperation.
								getUrl(ServerOperation.createPostRequestForSaveHomeWork
										(practiceList.get(i)) , 
										ServerOperation.getUrl
										(ServerOperationUtil.SAVE_HOMEWORK_SCORE))//)
										+ pref);*/

                        String response = ServerOperation.readFromURL
                                (ServerOperation.createPostRequestForSaveHomeWork
                                                (practiceList.get(i)),
                                        ServerOperation.getUrl
                                                (ServerOperationUtil.SAVE_HOMEWORK_SCORE));

                        if (response != null) {
                            HttpResponseBase httpResponseBase = new FileParser()
                                    .ParseJsonString(response, ServerOperationUtil.SAVE_HOMEWORK_SCORE);
                            SaveHomeWorkServerResponse saveHomeWorkResponse =
                                    (SaveHomeWorkServerResponse) httpResponseBase;
                            if (saveHomeWorkResponse.getResult().equalsIgnoreCase("success")) {
                                homeworkImpl.deleteFromPracticeSkillHW(practiceList.get(i));
                            }
                        }

                        if (CommonUtils.LOG_ON)
                            Log.e(TAG, "practice skill response " + response);
                    }

                    for (int i = 0; i < wordList.size(); i++) {

						/*urlList.add(pref + URLEncoder.encode(ServerOperation.
								getUrl(ServerOperation.createPostRequestForSaveHomeWorkForWordProblem
										(wordList.get(i)) , 
										ServerOperation.getUrl
										(ServerOperationUtil.SAVE_HOME_WORK_WORD_PLAY_REQUEST))//)
										+ pref);*/

                        String response = ServerOperation.readFromURL
                                (ServerOperation.createPostRequestForSaveHomeWorkForWordProblem
                                                (wordList.get(i)),
                                        ServerOperation.
                                                getUrl(ServerOperationUtil.SAVE_HOME_WORK_WORD_PLAY_REQUEST));

                        if (response != null) {
                            HttpResponseBase httpResponseBase = new FileParser()
                                    .ParseJsonString(response, ServerOperationUtil
                                            .SAVE_HOME_WORK_WORD_PLAY_REQUEST);
                            SaveHomeWorkServerResponse saveHomeWorkResponse =
                                    (SaveHomeWorkServerResponse) httpResponseBase;
                            if (saveHomeWorkResponse.getResult().equalsIgnoreCase("success")) {
                                homeworkImpl.deleteFromWordProblemHW(wordList.get(i));
                            }
                        }

                        if (CommonUtils.LOG_ON)
                            Log.e(TAG, "word problem response " + response);
                    }

                    //ArrayList<HWImage> imageList = getHWImages(customeList);

                    for (int i = 0; i < customeList.size(); i++) {

						/*urlList.add(pref + URLEncoder.encode(ServerOperation.
								getUrl(ServerOperation.createPostRequestForSaveCustomeAnsBuStudents
										(customeList.get(i)) , 
										ServerOperation.getUrl
										(ServerOperationUtil.SAVE_HW_CUSTOME_ANS_BY_STUDENTS))//)
										+ pref);*/

                        String response = ServerOperation.readFromURL
                                (ServerOperation.createPostRequestForSaveCustomeAnsBuStudents
                                                (customeList.get(i)),
                                        ServerOperation.
                                                getUrl(ServerOperationUtil.SAVE_HW_CUSTOME_ANS_BY_STUDENTS));

                        if (response != null) {
                            HttpResponseBase httpResponseBase = new FileParser()
                                    .ParseJsonString(response, ServerOperationUtil
                                            .SAVE_HW_CUSTOME_ANS_BY_STUDENTS);
                            SaveHomeWorkServerResponse saveHomeWorkResponse =
                                    (SaveHomeWorkServerResponse) httpResponseBase;
                            if (saveHomeWorkResponse.getResult().equalsIgnoreCase("success")) {
                                homeworkImpl.deleteFromCustomeHW(customeList.get(i));
                            }
                        }

                        if (CommonUtils.LOG_ON)
                            Log.e(TAG, "custome response " + response);
                    }

                    for(int i = 0 ; i < userLinks.size() ; i ++ ){
                        String response = ServerOperation.readFromURL
                                (ServerOperation.createPostToSaveGDocOrWebLinks
                                                (userLinks.get(i)),
                                        ServerOperation.
                                                getUrl(ServerOperationUtil.SAVE_GDOC_WEB_LINK_REQUEST));
                        if (response != null) {
                            if (CommonUtils.LOG_ON)
                                Log.e(TAG, "save links response " + response);
                            homeworkImpl.deleteFromUserLinks(userLinks.get(i));
                        }
                    }

					/*if(urlList.size() > 0){
						String response = ServerOperation.readFromURL
								(ServerOperation.createPostRequestForAddLocalPlayDataForHomework
										(convertStringArrayListIntoStringWithDelimeter
												(urlList , "$$$$")),
												ICommonUtils.UPLOAD_HW_OFFLINE_PLAY_DATA_URL);

						homeworkImpl.deleteAllFromPracticeSkillHW();
						homeworkImpl.deleteAllFromWordProblemHW();
						homeworkImpl.deleteAllFromCustomeHW();

						if(CommonUtils.LOG_ON)
							Log.e(TAG, "response " + response);
					}*/

                    homeworkImpl.closeConnection();
                    //return imageList;
                    return null;
                }

                @Override
                protected void onPostExecute(ArrayList<HWImage> result) {
                    request.onRequestComplete();
                    //uploadingHomeworkImages(context , result);
                }

            }.execute();
            MathFriendzyHelper.updatedMarkInputAndWorkInputOnServer(context);
            uploadingHomeworkImages(context, null);
        } catch (Exception e) {
            e.printStackTrace();
            request.onRequestComplete();
        }
    }

    /**
     * Upload the homework images which are saved in local at the time play with offline mode
     *
     * @param context
     * @param imageList
     */
    private static void uploadingHomeworkImages(Context context, ArrayList<HWImage> imageList) {
		/*HomeWorkImpl implObj = new HomeWorkImpl(context);
		implObj.openConnection();
		for(int i = 0 ; i < imageList.size() ; i ++ ){
			Log.e(TAG, "work image " + imageList.get(i).getWorkAreaImage());
			Log.e(TAG, "question image " + imageList.get(i).getQuestionImage());

			String homeworkImage = imageList.get(i).getWorkAreaImage();
			String questionImage = imageList.get(i).getQuestionImage();

			if(homeworkImage != null && homeworkImage.length() > 0 &&
					MathFriendzyHelper.checkForExistenceOfFile(homeworkImage))
				implObj.insertIntoHomeWorkImageTable(HOME_WORK_IMAGE_PREFF + homeworkImage);

			if(questionImage != null && questionImage.length() > 0 && 
					MathFriendzyHelper.checkForExistenceOfFile(questionImage))
				implObj.insertIntoHomeWorkImageTable(HOME_WORK_QUE_IMAGE_PREFF + questionImage);
		}
		implObj.closeConnection();*/
        context.startService(new Intent(context, UploadHomeworkImages.class));
    }


    public static void showFreeSubscriptionRegisterDialog(Context context,
                                                          YesNoListenerInterface listener) {
        /*DialogGenerator dg = new DialogGenerator(context);
        dg.freeSubscriptionDialog(listener);*/
    }


    /**
     * Save the subscription expire date
     *
     * @param context
     * @param date
     */
    public static void saveSubscriptionExpireDate(Context context, String date) {

        if (CommonUtils.LOG_ON)
            Log.e(TAG, "Expire Date " + date);

        SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
        SharedPreferences.Editor editor = sharedPreff.edit();
        editor.putString("subscriptionExpireDate", date);
        editor.commit();
    }

    /**
     * Save the purchase subscription date
     *
     * @param context
     * @param date
     */
    public static void savePurchaseSubscriptionDate(Context context, String date) {
        if (CommonUtils.LOG_ON)
            Log.e(TAG, "subscriptionDate Date " + date);
        SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
        SharedPreferences.Editor editor = sharedPreff.edit();
        editor.putString("purchaseSubscriptionDate", date);
        editor.commit();
    }

    /**
     * Return the purchase subscription date
     *
     * @param context
     * @return
     */
    public static String getPurchaseSubscriptionDate(Context context) {
        return context.getSharedPreferences(MATH_FRENDZY_PREFF, 0)
                .getString("purchaseSubscriptionDate", null);
    }

    /**
     * Get a diff between two dates
     *
     * @param date1    the oldest date
     * @param date2    the newest date
     * @param timeUnit the unit in which you want the diff
     * @return the diff value, in the provided unit
     */
    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        //diffInMillies -= getTimeOffset(diffInMillies);
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
        //return diffInMillies / (24 * 60 * 60 * 1000);
    }

    /**
     * Return the subscription expire date
     *
     * @param context
     * @return
     */
    public static String getSubscriptionExpireDate(Context context) {
        return context.getSharedPreferences(MATH_FRENDZY_PREFF, 0)
                .getString("subscriptionExpireDate", null);
    }

    public static int getTimeOffset(long time) {
        TimeZone tz = TimeZone.getDefault();
        return tz.getOffset(time);
    }

    @SuppressLint({"InlinedApi", "NewApi"})
    public static int getFreeSubscriptionRemainingDays(Context context) {
        try {
            String expireDateString = getSubscriptionExpireDate(context);
            Date currentDate = getCurrentDate(YYYY_MM_DD);
            Date expireDate = getValidateDateForTimeZone(expireDateString, YYYY_MM_DD);
            return (int) getDateDiff(currentDate, expireDate, TimeUnit.DAYS);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static int getFreeSubscriptionRemainingDays(Context context, String dateString
            , String format) {
        try {
            Date currentDate = getCurrentDate(YYYY_MM_DD);
            Date expireDate = getValidateDateForTimeZone(dateString, format);
            return (int) getDateDiff(currentDate, expireDate, TimeUnit.DAYS);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * Set font to textview
     *
     * @param context
     * @param txtView
     * @param fontFileUri
     */
    public static void setFont(Context context, TextView txtView, String fontFileUri) {
        try {
            Typeface myTypeface = Typeface.createFromAsset(context.getAssets(), fontFileUri);
            txtView.setTypeface(myTypeface);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * This method call when user click on btnInfoPractice and btnInfoAssessment
     *
     * @param messageFor
     */
    public static String getPopUpMessage(Context context,
                                         String messageFor) {//1 for assessment and 2 for practice
        String msg = null;
        Translation transeletion = new Translation(context);
        transeletion.openConnection();

        //for teacher function
        if (messageFor.equals(DIALOG_PRACTICE_REPORT)) {//Practice Report
            msg = transeletion.
                    getTranselationTextByTextIdentifier("lblThisButtonWillGenerateReport");
        } else if (messageFor.equals(DIALOG_ASSESSMENT_REPORT)) {//Assement report
            msg = transeletion.
                    getTranselationTextByTextIdentifier("lblThisViewAllowToSee");
        } else if (messageFor.equals(DIALOG_CREATE_CHALLENGE)) {//Create Challenge
            msg = transeletion.
                    getTranselationTextByTextIdentifier("lblThisButtonAllowToCreateFriendzyChallenge");
        } else if (messageFor.equals(DIALOG_STUDENT_ACCOUNT)) {//Student Account
            msg = transeletion.
                    getTranselationTextByTextIdentifier("lblThisButtonWillAllowYou");
        } else if (messageFor.equals(DIALOG_ASSESSMENT_REPORT_BY_CLASS)) {//Assessment report by class
            msg = transeletion.
                    getTranselationTextByTextIdentifier("lblThisButtonWillGenerateSeveralReports");
        } else if (messageFor.equals(DIALOG_ASSIGN_HOMEWORK)) {//Assign homework quizzes
            msg = transeletion.
                    getTranselationTextByTextIdentifier("lblHereYouCanSelect");
        } else if (messageFor.equals(DIALOG_CHECK_HOMEWORK)) {//check homework quizzes
            msg = transeletion.
                    getTranselationTextByTextIdentifier("lblOnceYouHaveAssigned");
        } else if (messageFor.equals(DIALOG_LEARNING_CENTER)) {
            msg = transeletion.
                    getTranselationTextByTextIdentifier("lblHereYouCanEarnPointsCoins");
            //msg = "Here you earn points, coins and trophies by answering tens of thousands of practice skills and critical thinking word problems.";
        } else if (messageFor.equals(DIALOG_SINGLE_FRIENDZY)) {
            msg = transeletion.
                    getTranselationTextByTextIdentifier("lblHereYouCompeteByAnswering");
            //msg = "Here you compete by answering Practice Skills or Word Problems with other students from around the world in the same grade and level.  As you win, you move up in level.";
        } else if (messageFor.equals(DIALOG_MULTI_FRIENDZY)) {
            msg = transeletion.
                    getTranselationTextByTextIdentifier("lblHereYouChallenge");
            //msg = "Here you challenge your classmates to a three-round competition of Practice Skills or Word Problems. Find your friends by username, or invite them to play via Facebook or email.";
        } else if (messageFor.equals(DIALOG_SCHOOL_FUNCTION)) {
            msg = transeletion.
                    getTranselationTextByTextIdentifier("lblHereYouTakeAssessment");
            //msg = "Here you take Assessment Tests, participate in school challenges, do your homework, find a tutor, create a study group and much more.";
        } else if (messageFor.equals(DIALOG_SCHOOL_ASSESSMENT_TEST)) {
            msg = transeletion.
                    getTranselationTextByTextIdentifier("lblHereYouCanTakeFourRound");
            //msg = "Here you can take up to four rounds of Assessment Tests based on each Common Core State Standard.  This allows your teacher to assess the class’ understanding of each standard.";
        } else if (messageFor.equals(DIALOG_SCHOOL_SCHOOL_CHALLENGE)) {
            msg = transeletion.
                    getTranselationTextByTextIdentifier("lblHereParticipateInChallenege");
            //msg = "Here you participate in School Challenges created by your teacher for your class, grade, or the entire school. Check the leader board to see who is winning.";
        } else if (messageFor.equals(DIALOG_SCHOOL_HOMEWORK_QUIZZ)) {
            msg = transeletion.
                    getTranselationTextByTextIdentifier("lblHereYouCanDoHomework");
            //msg = "Here you can do homework and quizzes created by your teachers.  You get instant feedback if you answer the problems right or wrong, and you can even get help from your classmates.";
        } else if (messageFor.equals(DIALOG_SCHOOL_STUDY_GROUP)) {
            msg = transeletion.
                    getTranselationTextByTextIdentifier("lblHereYouCanCreateStudyGroup");
            //msg = "Here you can create or join a study group. This will allow you to collaborate with your classmates in a shared chat and drawing area to help each other with your class work";
        } else if (messageFor.equals(DIALOG_SCHOOL_HELP_STUDENT)) {
            msg = transeletion.
                    getTranselationTextByTextIdentifier("lblHereYouCanFindStudentToHelp");
            //msg = "Here you can find students that have requested help from a tutor.  But first, ask your teacher to register you as a tutor for your school.";
        }else if (messageFor.equals(DIALOG_MANAGE_TUTOR)) {
            msg = transeletion.
                    getTranselationTextByTextIdentifier("lblHereYouCanAssignStudents");
            //msg = "Here you can find students that have requested help from a tutor.  But first, ask your teacher to register you as a tutor for your school.";
        }else if (messageFor.equals(DIALOG_TUTORING_SESSION)) {
            msg = transeletion.
                    getTranselationTextByTextIdentifier("lblHereYouCanSeeSessions");
            //msg = "Here you can find students that have requested help from a tutor.  But first, ask your teacher to register you as a tutor for your school.";
        }
        transeletion.closeConnection();
        return msg;
    }


    /**
     * Check for dialog is to show or not
     *
     * @param context
     * @param dialogKey
     * @return
     */
    public static boolean checkDialogShow(Context context, String dialogKey) {
        return !context.getSharedPreferences(MATH_FRENDZY_PREFF, 0)
                .getBoolean(dialogKey, false);
        //return true;
    }

    /**
     * Save  the dialog shown or not
     *
     * @param context
     * @param dialogKey
     * @param isShow
     */
    public static void saveCheckedDialog(Context context,
                                         String dialogKey, boolean isShow) {
        SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
        SharedPreferences.Editor editor = sharedPreff.edit();
        editor.putBoolean(dialogKey, isShow);
        editor.commit();
    }

    /**
     * Return the vedio url
     *
     * @param vedioFor
     * @return
     */
    public static String getVedioUrl(String vedioFor) {
        String vedioUrl = "https://www.google.co.in";
        if (vedioFor.equals(DIALOG_PRACTICE_REPORT)) {//Practice Report
            vedioUrl = VedioUrls.TEACHER_PRACTICE_REPORT_VEDIO;
        } else if (vedioFor.equals(DIALOG_ASSESSMENT_REPORT)) {//Assement report
            vedioUrl = VedioUrls.TEACHER_ASSESSMENT_REPORT_VEDIO;
        } else if (vedioFor.equals(DIALOG_CREATE_CHALLENGE)) {//Create Challenge
            vedioUrl = VedioUrls.TEACHER_CREATE_CHALLENGE_VEDIO;
        } else if (vedioFor.equals(DIALOG_STUDENT_ACCOUNT)) {//Student Account
            vedioUrl = VedioUrls.TEACHER_STUDENT_ACCOUNT_VEDIO;
        } else if (vedioFor.equals(DIALOG_ASSESSMENT_REPORT_BY_CLASS)) {//Assessment report by class
            vedioUrl = VedioUrls.TEACHER_ASSESSMENT_REPORT_BY_CLASS_VEDIO;
        } else if (vedioFor.equals(DIALOG_ASSIGN_HOMEWORK)) {//Assign homework quizzes
            vedioUrl = VedioUrls.TEACHER_ASSIGN_HW_QUIZZ_VEDIO;
        } else if (vedioFor.equals(DIALOG_CHECK_HOMEWORK)) {//check homework quizzes
            vedioUrl = VedioUrls.TEACHER_CHECK_HW_QUIZZ_VEDIO;
        } else if (vedioFor.equals(DIALOG_LEARNING_CENTER)) {
            vedioUrl = VedioUrls.LEARNING_CENTER_VEDIO;
        } else if (vedioFor.equals(DIALOG_SINGLE_FRIENDZY)) {
            vedioUrl = VedioUrls.SINGLE_FRIENDZY_VEDIO;
        } else if (vedioFor.equals(DIALOG_MULTI_FRIENDZY)) {
            vedioUrl = VedioUrls.MULTI_FRIESNDZY_VEDIO;
        } else if (vedioFor.equals(DIALOG_SCHOOL_FUNCTION)) {
            vedioUrl = VedioUrls.SCHOO_FUNCTION_VEDIO;
        } else if (vedioFor.equals(DIALOG_SCHOOL_ASSESSMENT_TEST)) {
            vedioUrl = VedioUrls.SCHOOL_ASSESSMENT_TEST_VEDIO;
        } else if (vedioFor.equals(DIALOG_SCHOOL_SCHOOL_CHALLENGE)) {
            vedioUrl = VedioUrls.SCHOOL_SCHOOL_CHALLENGE_VEDIO;
        } else if (vedioFor.equals(DIALOG_SCHOOL_HOMEWORK_QUIZZ)) {
            vedioUrl = VedioUrls.SCHOOL_HOMEWROK_QUIZZ_VEDIO;
        } else if (vedioFor.equals(DIALOG_SCHOOL_STUDY_GROUP)) {
            vedioUrl = VedioUrls.SCHOOL_STUDY_GROUP_VEDIO;
        } else if (vedioFor.equals(DIALOG_SCHOOL_HELP_STUDENT)) {
            vedioUrl = VedioUrls.SCHOOL_HELP_STUDENT_VEDIO;
        }else if (vedioFor.equals(DIALOG_MANAGE_TUTOR)) {
            vedioUrl = VedioUrls.TEACHER_MANAGE_TUTOR_VEDIO;
        }
        return vedioUrl;
    }

    /**
     * Show the teacher function
     */
    public static boolean showTeacherFunctionPopup(final Context context,
                                                   final String showPopUpFor,
                                                   final TeacherFunctionDialogListener listener
            , String txtTitle , boolean isShowWatchButton) {

        if (!MathFriendzyHelper.checkDialogShow(context, showPopUpFor))
            return false;

        MathFriendzyHelper.showTacherFunctionDialog(context,
                new TeacherFunctionDialogListener() {

                    @Override
                    public void clickOnWatchVedio(boolean isShow) {
                        String vedioUrl = getVedioUrl(showPopUpFor);
                        if (vedioUrl != null) {
                            //openUrl(context, vedioUrl);
                            if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS){
                                vedioUrl = MathFriendzyHelper.updateMathPlusUrl(vedioUrl);
                            }
                            MathFriendzyHelper.converUrlIntoEmbaddedAndPlay(context,
                                    vedioUrl);
                        } else {
                            //MathFriendzyHelper.showWarningDialog(context, "No vedio exist.");
                            CommonUtils.comingSoonPopUp(context);
                        }
                        listener.clickOnWatchVedio(isShow);
                    }

                    @Override
                    public void clickOnDontShow(boolean isShow) {
                        listener.clickOnDontShow(isShow);
                    }

                    @Override
                    public void clickOnClose(boolean isShow) {
                        listener.clickOnClose(isShow);
                    }
                }, showPopUpFor, "", "" , txtTitle , isShowWatchButton);
        return true;
    }

    /**
     * Show the treacher function dialog
     *
     * @param context
     * @param listener
     */
    public static void showTacherFunctionDialog(final Context context,
                                                final TeacherFunctionDialogListener listener,
                                                final String openDialogFor, String watchTheVedioText,
                                                String dontShowAgainMsg
            ,String txtTitle , boolean isShowWatchButton) {

        DialogGenerator dg = new DialogGenerator(context);
        dg.showTeacherFunctionsDialog(new TeacherFunctionDialogListener() {

                                          @Override
                                          public void clickOnWatchVedio(boolean isShow) {
                                              saveCheckedDialog(context, openDialogFor, isShow);
                                              listener.clickOnWatchVedio(isShow);
                                          }

                                          @Override
                                          public void clickOnDontShow(boolean isShow) {
                                              saveCheckedDialog(context, openDialogFor, isShow);
                                              listener.clickOnDontShow(isShow);
                                          }

                                          @Override
                                          public void clickOnClose(boolean isShow) {
                                              saveCheckedDialog(context, openDialogFor, isShow);
                                              listener.clickOnClose(isShow);
                                          }
                                      }, getPopUpMessage(context, openDialogFor),
                watchTheVedioText, dontShowAgainMsg , txtTitle , isShowWatchButton);
    }


    /**
     * Get the temp player data
     *
     * @param context
     * @return
     */
    public static TempPlayer getTempPlayer(Context context) {
        try {
            TempPlayerOperation tempPlayerOperationObj = new TempPlayerOperation(context);
            if (tempPlayerOperationObj.isTemparyPlayerExist()) {
                if (!tempPlayerOperationObj.isTempPlayerDeleted()) {
                    TempPlayerOperation tempPlayerOperationObj1 = new TempPlayerOperation(context);
                    ArrayList<TempPlayer> tempPlayer = tempPlayerOperationObj1.getTempPlayerData();
                    return tempPlayer.get(0);

                } else {
                    return null;
                }
            } else {
                tempPlayerOperationObj.closeConn();
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Check for temp player is created or not atleast one time in the app
     *
     * @param context
     * @return
     */
    public static boolean isTempraroryPlayerCreated(Context context) {
        TempPlayerOperation tempObj = new TempPlayerOperation(context);
        boolean isCreated = tempObj.isTemparyPlayerExist();
        tempObj.closeConn();
        return isCreated;
    }

    /**
     * Get the country list
     *
     * @param context
     * @return
     */
    public static ArrayList<String> getCountryList(Context context) {
        Country countryObj = new Country();
        ArrayList<String> countryList = countryObj.getCountryName(context);
        return countryList;
    }

    /**
     * Set country adapter
     *
     * @param context
     * @param selectedCountry
     * @param countryList
     * @param spinner
     */
    public static void setCountryAdapter(Context context, String selectedCountry,
                                         ArrayList<String> countryList, Spinner spinner) {
        if (countryList != null) {
            ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>
                    (context, R.layout.spinner_country_item, countryList);
            countryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(countryAdapter);
            spinner.setSelection(countryAdapter.getPosition(selectedCountry));
        }
    }

    /**
     * Select school
     *
     * @param selectedCountry
     * @param city
     * @param zipCode
     * @param state
     */
    public static void selectSchool(Context context,
                                    String selectedCountry, String city,
                                    String zipCode, String state) {

        Intent schoolIntent = new Intent(context, SearchYourSchoolActivity.class);
        if (selectedCountry.equals(UNITED_STATE) ||
                selectedCountry.toString().equals(CANADA)) {
            if (/*city.toString().equals("") ||*/
                    zipCode.toString().equals("")) {
                DialogGenerator dg1 = new DialogGenerator(context);
                Translation transeletion1 = new Translation(context);
                transeletion1.openConnection();
                dg1.generateWarningDialog(transeletion1
                        .getTranselationTextByTextIdentifier
                                ("alertMsgEnterYourLocationToContinue"));
                transeletion1.closeConnection();
            } else {
                schoolIntent.putExtra("country", selectedCountry);
                schoolIntent.putExtra("state", state);
                schoolIntent.putExtra("city", city);
                schoolIntent.putExtra("zip", zipCode);
                ((Activity) context).startActivityForResult(schoolIntent,
                        SELECT_SCHOOL_REQUEST_CODE);
            }
        } else {
            schoolIntent.putExtra("country", selectedCountry);
            schoolIntent.putExtra("state", "");
            schoolIntent.putExtra("city", city);
            schoolIntent.putExtra("zip", zipCode);
            ((Activity) context).startActivityForResult
                    (schoolIntent, SELECT_SCHOOL_REQUEST_CODE);
        }
    }


    /**
     * Select teacher
     *
     * @param context
     * @param schoolId
     */
    public static void selectTeacher(Context context, String schoolId) {
        Intent teacherIntent = new Intent(context, SearchTeacherActivity.class);
        teacherIntent.putExtra("schoolId", schoolId);
        ((Activity) context).startActivityForResult(teacherIntent, SELECT_TEACHER_REQUEST_CODE);
    }

    /**
     * Get grade list
     *
     * @param context
     * @return
     */
    public static ArrayList<String> getGradeList(Context context) {
        Grade gradeObj = new Grade();
        return gradeObj.getGradeList(context);
    }

    public static ArrayList<String> getUpdatedGradeList(Context context) {
        Grade gradeObj = new Grade();
        ArrayList<String> gradeList = gradeObj.getGradeList(context);
        gradeList.add(0, MathFriendzyHelper.getGradeText(context));
        gradeList.set(gradeList.size() - 1, MathFriendzyHelper.ADULT);
        return gradeList;
    }

    /**
     * Set country adapter
     *
     * @param context
     * @param spinner
     */
    public static void setAdapterToSpinner(Context context, String selectionValue,
                                           ArrayList<String> list, Spinner spinner) {
        if (list != null) {
            ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>
                    (context, R.layout.spinner_country_item, list);
            countryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(countryAdapter);
            spinner.setSelection(countryAdapter.getPosition(selectionValue));
        }
    }

    /**
     * This method check for empty fields
     *
     * @param fields
     * @return true , if any field is empty otherwise return false;
     */
    public static boolean checkForEmptyFields(String... fields) {
        for (int i = 0; i < fields.length; i++) {
            if (fields[i].length() == 0)
                return true;
        }
        return false;
    }


    /**
     * Get Country id by country name
     *
     * @param countryName
     * @param context
     * @return
     */
    public static String getCountryIdByCountryName(String countryName, Context context) {
        Country countryObj = new Country();
        return countryObj.getCountryIdByCountryName(countryName, context);
    }

    /**
     * Get country ISO by country name
     *
     * @param countryName
     * @param context
     * @return
     */
    public static String getCountryIsoByCountryName(String countryName, Context context) {
        Country countryObj = new Country();
        return countryObj.getCountryIsoByCountryName(countryName, context);
    }


    /**
     * register user on server
     *
     * @param context
     * @param regObj
     * @param listener
     */
    public static void registerUser(final Context context, final RegistereUserDto regObj,
                                    final OnRegistration listener) {

        new AsyncTask<Void, Void, Void>() {
            private int registreationResult = 0;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                Register registerObj = new Register(context);
                registreationResult = registerObj.registerUserOnserver(regObj);
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                listener.onRegistrationComplete(registreationResult);
            }
        }.execute();
    }


    /**
     * Get user name
     *
     * @param context
     * @param listener
     */
    public static void getUserName(final Context context, final GetUserName listener) {
        new AsyncTask<Void, Void, String>() {

            TempPlayerOperation tempPlayerObj = null;

            @Override
            protected void onPreExecute() {
                tempPlayerObj = new TempPlayerOperation(context);
            }
            ;

            @Override
            protected String doInBackground(Void... params) {
                String userName = tempPlayerObj.getUserNameFromServer();
                return userName;
            }

            @Override
            protected void onPostExecute(String result) {
                tempPlayerObj.closeConn();
                listener.onComplete(result);
            }

            ;
        }.execute();
    }

    /**
     * Create temp player
     *
     * @param userName
     * @param context
     */
    public static void createTempPlayer(String userName, Context context,
                                        OnRequestComplete listener) {
        TempPlayerOperation tempPlayerObj = new TempPlayerOperation(context);
        TempPlayer tempPlayer = new TempPlayer();
        tempPlayer.setCity("");
        tempPlayer.setCoins(0);
        tempPlayer.setCompeteLevel(1);
        tempPlayer.setFirstName("Temp");
        tempPlayer.setGrade(1);

        tempPlayer.setLastName("Player");
        tempPlayer.setParentUserId(0);
        tempPlayer.setPlayerId(0);
        tempPlayer.setPoints(0);

        tempPlayer.setProfileImageName("Smiley");//changes

        Resources res = context.getResources();

        Drawable drawable = res.getDrawable(R.drawable.smiley_ipad);
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();

        byte[] bitMapData = CommonUtils.getByteFromBitmap(bitmap);
        tempPlayer.setProfileImage(bitMapData);

        tempPlayer.setSchoolId(1);
        tempPlayer.setSchoolName("Home School");
        tempPlayer.setTeacherFirstName("NA");
        tempPlayer.setTeacherLastName("");
        tempPlayer.setTeacherUserId(-2);

        tempPlayer.setUserName(userName);
        tempPlayer.setZipCode("");
        tempPlayer.setState("");
        tempPlayer.setCountry("United States");
        tempPlayerObj.createTempPlayerTable(tempPlayer);
        listener.onComplete();
    }


    /**
     * Save user Login
     *
     * @param context
     * @param bValues
     */
    public static void saveUserLogin(Context context, boolean bValues) {
        SharedPreferences sheredPreference =
                context.getSharedPreferences(LOGIN_SHARED_PREFF, 0);
        SharedPreferences.Editor editor = sheredPreference.edit();
        editor.putBoolean(IS_LOGIN, bValues);
        editor.commit();
    }


    /**
     * Add user on server with the login device
     *
     * @param conetx
     */
    public static void addUserOnServerWithAndroidDevice(Context conetx) {
        UserRegistrationOperation userObj = new UserRegistrationOperation(conetx);
        new AddUserWithAndroidDevice(userObj.getUserId(), conetx).execute(null, null, null);
    }

    /**
     * Get the player id , which is recenlty added into the user account at the time
     * of registration
     *
     * @param contex
     * @param tempPlayer
     * @return
     */
    public static String getPlayerIdOfTempPlayerWhichIsNowRegister
    (Context contex, TempPlayer tempPlayer) {
        UserPlayerOperation userOpr = new UserPlayerOperation(contex);
        String playerId = userOpr.getPlayerIdbyUserName(tempPlayer.getUserName());
        userOpr.closeConn();
        return playerId;
    }

    /**
     * Get user id of temp player which is recently added into the user account at the time
     * of registration
     *
     * @param contex
     * @param tempPlayer
     * @return
     */
    public static String getUserIdOfTempPlayerWhichIsNowRegister(Context contex, TempPlayer tempPlayer) {
        UserPlayerOperation userOpr = new UserPlayerOperation(contex);
        String userId = userOpr.getUserIdbyUserName(tempPlayer.getUserName());
        userOpr.closeConn();
        return userId;
    }

    /**
     * Update temp player which is recently added into user account at the time of registration
     *
     * @param userId
     * @param playerId
     * @param context
     */
    public static void updateTempPlayerDataWhichIsNotRegisteredPlayed(String userId,
                                                                      String playerId, Context context) {
        LearningCenterimpl learnignCenter = new LearningCenterimpl(context);
        learnignCenter.openConn();
        learnignCenter.updatePlayerTotalPointsForUserIdandPlayerId(userId, playerId);
        learnignCenter.updatePlayerEquationTabelForUserIdAndPlayerId(userId, playerId);
        learnignCenter.updateMathResultForUserIdAndPlayerId(userId, playerId);
        learnignCenter.closeConn();
    }


    /**
     * Update the player avatar and get avatar list
     *
     * @param userId
     * @param playerId
     * @param context
     * @return
     */
    public static ArrayList<String> updatePlayerAvterAndGetList(String userId, String playerId
            , Context context) {
        ChooseAvtarOpration opr = new ChooseAvtarOpration();
        opr.openConn(context);
        opr.updateplayerAvtarStatusForTempPlayer(userId, playerId);
        ArrayList<String> avtarList = opr.getAvtarIds(userId, playerId);
        opr.closeConn();
        return avtarList;
    }


    /**
     * Save purchased avatar which is purchased at the time of temp player
     *
     * @param userId
     * @param playerId
     * @param avtarIdList
     */
    public static final void saveAvatar(final String userId, final String playerId,
                                        final ArrayList<String> avtarIdList) {

        new AsyncTask<Void, Void, Void>() {
            private String avtarIds;

            @Override
            protected void onPreExecute() {
                avtarIds = "";
                for (int i = 0; i < avtarIdList.size(); i++) {
                    avtarIds = avtarIds + avtarIdList.get(i);
                    if (i < avtarIdList.size() - 1)
                        avtarIds = avtarIds + ",";
                }
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                AvtarServerOperation serverObj = new AvtarServerOperation();
                serverObj.saveAvtar(userId, playerId, avtarIds);
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                // TODO Auto-generated method stub
                super.onPostExecute(result);
            }
        }.execute();
    }

    /**
     * Get the purrchased item list
     *
     * @param context
     * @param userId
     * @return
     */
    public static ArrayList<String> getUserPurchaseItemList(Context context, String userId) {
        LearningCenterimpl lrarningImpl = new LearningCenterimpl(context);
        lrarningImpl.openConn();
        lrarningImpl.updatePurchasedItemTable(userId);
        ArrayList<String> purchaseItemList = lrarningImpl.getPurchaseItemIdsByUserId(userId);
        lrarningImpl.closeConn();
        return purchaseItemList;
    }

    /**
     * Save purchased item on server
     *
     * @param context
     * @param userId
     * @param purchasedItemList
     */
    public static void savePurchasedItem(final Context context, final String userId,
                                         final ArrayList<String> purchasedItemList) {

        new AsyncTask<Void, Void, Void>() {
            private String itemsId;

            @Override
            protected void onPreExecute() {
                itemsId = "";
                for (int i = 0; i < purchasedItemList.size(); i++) {
                    itemsId = itemsId + purchasedItemList.get(i);
                    if (i < purchasedItemList.size() - 1)
                        itemsId = itemsId + ",";
                }
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                Login login = new Login(context);
                login.savePurchasedItem(userId, itemsId);
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
            }
        }.execute();
    }


    /**
     * Get math resut list
     *
     * @param context
     * @return
     */
    public static ArrayList<MathResultTransferObj> getResultMathList(Context context) {
        LearningCenterimpl learningCenterObj = new LearningCenterimpl(context);
        learningCenterObj.openConn();
        ArrayList<MathResultTransferObj> mathResultList = learningCenterObj.getMathResultData();
        learningCenterObj.closeConn();
        return mathResultList;
    }

    /**
     * Save temp player score on server which is recently added into user account at the time of
     * registration or login
     *
     * @param context
     * @param mathobj
     * @param listener
     * @param tempPlayerUserName
     */
    public static void saveTempPlayerScoreOnServer(final Context context,
                                                   final ArrayList<MathResultTransferObj> mathobj, final OnRequestComplete listener
            , final String tempPlayerUserName) {

        new AsyncTask<Void, Void, Void>() {

            private String playerId = null;
            private String UserId = null;

            @Override
            protected void onPreExecute() {

                UserPlayerOperation userPlayerObj = new UserPlayerOperation(context);
                playerId = userPlayerObj.getPlayerIdbyUserName(tempPlayerUserName);
                userPlayerObj.closeConn();

                UserRegistrationOperation userOperation = new UserRegistrationOperation(context);
                UserId = userOperation.getUserData().getUserId();
                userOperation.closeConn();

                for (int i = 0; i < mathobj.size(); i++) {
                    mathobj.get(i).setPlayerId(playerId);
                    mathobj.get(i).setUserId(UserId);
                }

                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                Register register = new Register(context);
                register.savePlayerScoreOnServer(mathobj);
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                listener.onComplete();
            }
        }.execute();
    }

    /**
     * Delete the result data from MathResult after it uploaded on the server
     *
     * @param context
     */
    public static void deleteFromMath(Context context) {
        LearningCenterimpl learnignCenter = new LearningCenterimpl(context);
        learnignCenter.openConn();
        learnignCenter.deleteFromMathResult();
        learnignCenter.closeConn();
    }

    /**
     * Get the player equation data
     *
     * @param context
     * @param playerId
     * @return
     */
    public static ArrayList<PlayerEquationLevelObj>
    getPlayerEquationData(Context context, String playerId) {
        LearningCenterimpl learnignCenter1 = new LearningCenterimpl(context);
        learnignCenter1.openConn();
        ArrayList<PlayerEquationLevelObj> playerquationData = learnignCenter1
                .getPlayerEquationLevelDataByPlayerId(playerId);
        learnignCenter1.closeConn();
        return playerquationData;
    }

    /**
     * Add player level on server
     *
     * @param playerquationData
     * @param listener
     */
    public static void addLevelAsynckTask(final ArrayList<PlayerEquationLevelObj> playerquationData
            , final OnRequestComplete listener) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                LearningCenteServerOperation serverOpr = new LearningCenteServerOperation();
                serverOpr.addAllLevel("<levels>" + getLevelXml(playerquationData) + "</levels>");
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                listener.onComplete();
            }
        }.execute();
    }

    /**
     * Get the player xml at the time of registration
     *
     * @param playerquationData
     * @return
     */
    private static String getLevelXml(ArrayList<PlayerEquationLevelObj> playerquationData) {
        StringBuilder xml = new StringBuilder("");
        for (int i = 0; i < playerquationData.size(); i++) {
            xml.append("<userLevel>" +
                    "<uid>" + playerquationData.get(i).getUserId() + "</uid>" +
                    "<pid>" + playerquationData.get(i).getPlayerId() + "</pid>" +
                    "<eqnId>" + playerquationData.get(i).getEquationType() + "</eqnId>" +
                    "<level>" + playerquationData.get(i).getLevel() + "</level>" +
                    "<stars>" + playerquationData.get(i).getStars() + "</stars>" +
                    "</userLevel>");
        }
        return xml.toString();
    }

    /**
     * OPen the edit registered player screen after registration success with the temp player
     *
     * @param context
     * @param email
     */
    public static void openEditRegisteredUserScreenAfterRegistration(Context context,
                                                                     String email) {
        UserPlayerOperation userPlayerObj = new UserPlayerOperation(context);
        ArrayList<UserPlayerDto> userPlayerList = userPlayerObj.getUserPlayerData();

        SharedPreferences sheredPreferences = context.getSharedPreferences(IS_CHECKED_PREFF, 0);
        SharedPreferences.Editor editor = sheredPreferences.edit();
        editor.clear();
        editor.putString(PLAYER_ID, userPlayerList.get(0).getPlayerid());
        editor.commit();

        Intent intent = new Intent(context, EditRegisteredUserPlayer.class);
        if (email.equals(""))//do not show registration pop up for student
            intent.putExtra("isCallFromRegistration", false);
        else
            intent.putExtra("isCallFromRegistration", true);

        intent.putExtra("playerId", userPlayerList.get(0).getPlayerid());
        intent.putExtra("userId", userPlayerList.get(0).getParentUserId());
        intent.putExtra("imageName", userPlayerList.get(0).getImageName());
        intent.putExtra("callingActivity", "RegisterationStep2");
        context.startActivity(intent);
    }

    /**
     * Open the Main Screen after registration success
     *
     * @param contex
     * @param email
     */
    public static void openMainScreenAfterRegistration(Context contex, String email) {
        try {
            //Need to do for disable ads after registration
            MathFriendzyHelper.getPurchasedItemDetail(contex,
                    CommonUtils.getUserId(contex));
        }catch(Exception e){
            e.printStackTrace();
        }

        Intent intent = new Intent(contex, MainActivity.class);
        if (email.equals(""))//do not show registration pop up for student
            intent.putExtra("isCallFromRegistration", false);
        else
            intent.putExtra("isCallFromRegistration", true);
        contex.startActivity(intent);
    }

    /**
     * Open the Main Screen after registration success
     *
     * @param contex
     * @param email
     */
    public static void openMainScreenAfterRegistration(Context contex, String email,
                                                       String isParent) {
        try {
            //Need to do for disable ads after registration
            MathFriendzyHelper.getPurchasedItemDetail(contex,
                    CommonUtils.getUserId(contex));
        }catch(Exception e){
            e.printStackTrace();
        }

        if (isParent.equals("1")) {//1 for parent
            Intent intent = new Intent(contex, ModifyRegistration.class);
            contex.startActivity(intent);
        } else {
            Intent intent = new Intent(contex, MainActivity.class);
            if (email.equals(""))//do not show registration pop up for student
                intent.putExtra("isCallFromRegistration", false);
            else
                intent.putExtra("isCallFromRegistration", true);
            contex.startActivity(intent);
        }
    }

    /**
     * Get us stateid by name
     *
     * @param stateName
     * @param context
     * @return
     */
    public static String getUsStateIdByName(String stateName, Context context) {
        States stateobj = new States();
        String stateId = stateobj.getUSStateIdByName(stateName, context);
        return stateId;
    }

    /**
     * Get US state code by state name
     *
     * @param stateName
     * @param context
     * @return
     */
    public static String getUsStateCodeByName(String stateName, Context context) {
        States stateobj = new States();
        String stateCode = stateobj.getStateCodeNameByStateNameFromUS(stateName, context);
        return stateCode;
    }

    /**
     * Get canada state id by state name
     *
     * @param stateName
     * @param context
     * @return
     */
    public static String getCanadaStateIdByName(String stateName, Context context) {
        States stateobj = new States();
        String stateId = stateobj.getCanadaStateIdByName(stateName, context);
        return stateId;
    }

    /**
     * Get canada state code by state name
     *
     * @param stateName
     * @param contex
     * @return
     */
    public static String getCanadaStateCodeByName(String stateName, Context contex) {
        States stateobj = new States();
        String stateCode = stateobj.getStateCodeNameByStateNameFromCanada(stateName, contex);
        return stateCode;
    }


    /**
     * Create temp player
     *
     * @param userName
     * @param context
     */
    public static TempPlayer createDefaultPlayer(Context context, String fName, String lName,
                                                 String city, int grade, SchoolDTO school, TeacherDTO teacher, String userName
            , String zipCode, String state, String country, String profileImageName) {

        TempPlayer tempPlayer = new TempPlayer();
        tempPlayer.setFirstName(fName);
        tempPlayer.setLastName(lName);
        tempPlayer.setCity(city);
        tempPlayer.setCoins(0);
        tempPlayer.setCompeteLevel(1);
        tempPlayer.setGrade(grade);
        tempPlayer.setParentUserId(0);//
        tempPlayer.setPlayerId(0);//
        tempPlayer.setPoints(0);

        tempPlayer.setProfileImageName(profileImageName);
        tempPlayer.setSchoolId(Integer.parseInt(school.getSchoolId()));
        tempPlayer.setSchoolIdString(school.getSchoolId());
        tempPlayer.setSchoolName(school.getSchoolName());

        tempPlayer.setTeacherFirstName(teacher.getfName());
        tempPlayer.setTeacherLastName(teacher.getlName());
        tempPlayer.setTeacherUserId(Integer.parseInt(teacher.getTeacherUserId()));
        tempPlayer.setUserName(userName);
        tempPlayer.setZipCode(zipCode);
        tempPlayer.setState(state);
        tempPlayer.setCountry(country);
        return tempPlayer;
    }

    /**
     * Return avtar image bitmap by name
     *
     * @param context
     * @param imageName
     * @return
     */
    public static Bitmap getAvatarImageByName(Context context, String imageName) {
        try {
            ChooseAvtarOpration chooseAvtarObj = new ChooseAvtarOpration();
            chooseAvtarObj.openConn(context);
            Bitmap profileImageBitmap = null;
            if (chooseAvtarObj.getAvtarImageByName(imageName) != null) {
                profileImageBitmap = CommonUtils.getBitmapFromByte
                        (chooseAvtarObj.getAvtarImageByName(imageName));
            }
            chooseAvtarObj.closeConn();
            return profileImageBitmap;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Return the selected grade
     *
     * @param selectedGrade
     * @return
     */
    public static int getGrade(String selectedGrade) {
        try {
            if (selectedGrade.equalsIgnoreCase(GRADE_TEXT)) {
                return NO_GRADE_SELETED;
            } else if (selectedGrade.equalsIgnoreCase(ADULT)) {
                return ADULT_GRADE;
            } else {
                return Integer.parseInt(selectedGrade);
            }
        } catch (Exception e) {
            return -1;
        }
    }

    /**
     * Get grade for selection at the time of edit grade
     *
     * @param grade
     * @return
     */
    public static String getGradeForSelection(String grade) {
        try {
            int intGrade = Integer.parseInt(grade);
            if (intGrade == ADULT_GRADE)
                return ADULT;
            return grade + "";
        } catch (Exception e) {
            return 1 + "";
        }
    }

    /**
     * Check for valid user name
     *
     * @param userName
     * @param listener
     */
    public static void checkForValidUserName(final String userName,
                                             final OnRequestCompleteWithStringResult listener) {

        new AsyncTask<Void, Void, Void>() {
            private String resultValue = null;

            @Override
            protected void onPreExecute() {
                //progressDialog.show();
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                Validation validObj = new Validation();
                resultValue = validObj.validUser(userName);
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                listener.onComplete(resultValue);
                super.onPostExecute(result);
            }
        }.execute();
    }

    /**
     * Show dialog when user is already exist
     *
     * @param context
     */
    public static void showAlreadyExistUserNameDialog(Context context) {
        Translation transeletion = new Translation(context);
        transeletion.openConnection();
        DialogGenerator dg = new DialogGenerator(context);
        dg.generateWarningDialog(transeletion.
                getTranselationTextByTextIdentifier("alertMsgThisUserNameAlreadyExist"));
        transeletion.closeConnection();
    }

    /**
     * Initialize the progress dialog which will show
     * at the time of login
     *
     * @param context
     */
    public static void initializeProcessDialog(Context context) {
        //if(pd == null){
        pd = new ProgressDialog(context);
        pd.setIndeterminate(false);//back button does not work
        pd.setCancelable(false);
        pd.setMessage("Please Wait...");
        //}
    }

    /**
     * Show the progress dialog
     */
    public static void showDialog() {
        if (pd != null)
            pd.show();
    }

    /**
     * Dismiss progress dialog
     */
    public static void dismissDialog() {
        if (pd != null && pd.isShowing())
            pd.cancel();
    }

    /**
     * Create a blank temp player table
     *
     * @param contex
     */
    public static void createBlankTempTable(Context contex) {
        try {
            TempPlayerOperation tempObj = new TempPlayerOperation(contex);
            tempObj.createTempPlayerTable();
            tempObj.closeConn();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Checked , Set check player if only one player exist in his account
     *
     * @param contxt
     */
    public static void setCheckPlayer(Context contxt) {
        SharedPreferences sheredPreference1 = contxt.getSharedPreferences(IS_CHECKED_PREFF, 0);
        SharedPreferences.Editor editor = sheredPreference1.edit();
        editor.clear();

        UserPlayerOperation userObj = new UserPlayerOperation(contxt);
        ArrayList<UserPlayerDto> userPlayerObj = userObj.getUserPlayerData();
        if (userPlayerObj.size() == 1) {
            editor.putString(PLAYER_ID, userPlayerObj.get(0).getPlayerid());
            editor.putString("userId", userPlayerObj.get(0).getParentUserId());

        }
        editor.commit();
    }

    /**
     * Select Avatar
     *
     * @param context
     * @param requestCode
     */
    public static void selectAvatar(Context context, int requestCode) {
        Intent chooseAvtar = new Intent(context, ChooseAvtars.class);
        chooseAvtar.putExtra("isForNewRegistration", true);
        ((Activity) context).startActivityForResult(chooseAvtar, requestCode);
    }

    /**
     * Select Avatar
     *
     * @param context
     * @param requestCode
     */
    public static void selectAvatar(Context context, int requestCode, boolean isEditAvatar) {
        Intent chooseAvtar = new Intent(context, ChooseAvtars.class);
        chooseAvtar.putExtra("isForNewRegistrationSelectAvatar", true);
        ((Activity) context).startActivityForResult(chooseAvtar, requestCode);
    }

    /**
     * Update teacher player avtar and grade
     *
     * @param context
     * @param avatarName
     * @param grade
     * @param playerId
     */
    public static void updateTeacherplayer(Context context,
                                           String avatarName, String grade, String playerId, String firstName, String lastName) {
        if (CommonUtils.LOG_ON)
            Log.e(TAG, "inside updateTeacherplayer player id " + playerId);
        UserPlayerOperation userPlayoprObj = new UserPlayerOperation(context);
        userPlayoprObj.updatePlayerGradeAvatar(playerId, grade, avatarName, firstName, lastName);
        userPlayoprObj.closeConn();
    }

    /**
     * Sign in with Google
     */
    public static void signInWithGoogle(Context context) {
        context.startActivity(new Intent(context, ActSignInWithGoogle.class));
    }


    /**
     * Login with google
     *
     * @param context
     * @param email
     * @param listener
     */
    public static void loginWithGoogle(final Context context,
                                       final String email,
                                       final OnRequestCompleteWithStringResult listener) {

        new AsyncTask<Void, Void, Void>() {
            private int resultValue = 0;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                Login loginobj = new Login(context);
                //default password for sign in with google 'google'
                resultValue = loginobj.getLoginDataByEmail(email, "google");
                //resultValue    = loginobj.getLoginDataByEmail("parent@gmail.com", "google");
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                listener.onComplete(resultValue + "");
            }
        }.execute();
    }

    /**
     * Get purchased item detail
     *
     * @param context
     * @param userId
     */
    public static void getPurchasedItemDetail(final Context context, final String userId) {
        new AsyncTask<Void, Void, Void>() {
            private String resultString = "";

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                MultiFriendzyServerOperation serverObj = new MultiFriendzyServerOperation();
                resultString = serverObj.getPurchasedItemDetailByUserId(userId);
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                try {
                    if (resultString != null) {
                        String itemIds = "";
                        ArrayList<String> itemList = new ArrayList<String>();

                        if (resultString != null) {//start if

                            //got app unlock status , changes on 5 May 2014
                            String[] resultValue = resultString.split("&");
                            resultString = resultValue[0].replace("&", "");
                            String appUnlockStatus = resultValue[1].replace("&", "");

                            if (resultString.length() > 0) {
                                //itemIds = result.getItems().replace(",", "");
                                //get item value which is comma separated , add (,) at the last for finish	
                                String newItemList = resultString + ",";

                                for (int i = 0; i < newItemList.length(); i++) {
                                    if (newItemList.charAt(i) == ',') {
                                        itemList.add(itemIds);
                                        itemIds = "";
                                    } else {
                                        itemIds = itemIds + newItemList.charAt(i);
                                    }
                                }
                            }

                            ArrayList<PurchaseItemObj> purchaseItem = new ArrayList<PurchaseItemObj>();


                            for (int i = 0; i < itemList.size(); i++) {
                                PurchaseItemObj purchseObj = new PurchaseItemObj();
                                purchseObj.setUserId(userId);
                                purchseObj.setItemId(Integer.parseInt(itemList.get(i)));
                                purchseObj.setStatus(1);
                                purchaseItem.add(purchseObj);
                            }

                            //add app unlick statuc , changes on 5 May 2014
                            try {
                                if (!appUnlockStatus.equals("-1")) {
                                    PurchaseItemObj purchseObj = new PurchaseItemObj();
                                    purchseObj.setUserId(userId);
                                    purchseObj.setItemId(100);
                                    purchseObj.setStatus(Integer.parseInt(appUnlockStatus));
                                    purchaseItem.add(purchseObj);
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "Error while inserting app unlock status " + e.toString());
                            }

                            LearningCenterimpl learningCenter = new LearningCenterimpl(context);
                            learningCenter.openConn();
                            learningCenter.deleteFromPurchaseItem();
                            learningCenter.insertIntoPurchaseItem(purchaseItem);
                            learningCenter.closeConn();
                        }//end if
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Error while getting purchase response " + e.toString());
                }
                super.onPostExecute(result);
            }
        }.execute();
    }


    /**
     * Get purchased avatar
     *
     * @param userId
     * @param context
     */
    public static void getPurchasedAvater(final String userId, final Context context) {

        new AsyncTask<Void, Void, Void>() {
            private ArrayList<PurchasedAvtarDto> purchasedAvterList;

            @Override
            protected void onPreExecute() {
                purchasedAvterList = new ArrayList<PurchasedAvtarDto>();
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                Login loginObj = new Login(context);
                purchasedAvterList = loginObj.getPurchasedAvtar(userId);
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                if (purchasedAvterList != null) {
                    ChooseAvtarOpration opr = new ChooseAvtarOpration();
                    opr.openConn(context);

                    for (int i = 0; i < purchasedAvterList.size(); i++) {
                        opr.insertIntoPlayerActarStatus(purchasedAvterList.get(i).getUserId(),
                                purchasedAvterList.get(i).getPlayerId()
                                , purchasedAvterList.get(i).getActerId(),
                                purchasedAvterList.get(i).getStatus());
                    }
                    opr.closeConn();
                }
                super.onPostExecute(result);
            }
        }.execute();
    }

    /**
     * Return the login user player list
     *
     * @param context
     * @return
     */
    public static ArrayList<UserPlayerDto> getUserPlayers(Context context) {
        UserPlayerOperation userPlayerObjOperation = new UserPlayerOperation(context);
        ArrayList<UserPlayerDto> userPlayerObj = userPlayerObjOperation.getUserPlayerData();
        return userPlayerObj;
    }

    /**
     * Delete the temp player record from the database
     *
     * @param context
     */
    public static void deleteTempPlayerRecord(Context context) {
        TempPlayerOperation tempPlayerObj1 = new TempPlayerOperation(context);
        tempPlayerObj1.deleteFromTempPlayer();
        tempPlayerObj1.closeConn();

		/*Delete Temp player records*/
        LearningCenterimpl learnignCenter = new LearningCenterimpl(context);
        learnignCenter.openConn();
        learnignCenter.deleteFromPlayerTotalPoints("0");
        learnignCenter.deleteFromPlayerEruationLevel("0");
        learnignCenter.deleteFromMathResult("0", "0");
        learnignCenter.deleteFromLocalPlayerLevels("0", "0");
        learnignCenter.deleteFromPurchaseItem("0");
        learnignCenter.closeConn();
    }

    /**
     * Return the app unlock status
     *
     * @param itemId
     * @param userId
     * @param context
     * @return
     */
    public static int getAppUnlockStatus(int itemId, String userId, Context context) {
        int appStatus = 0;
        LearningCenterimpl learnignCenterImpl = new LearningCenterimpl(context);
        learnignCenterImpl.openConn();
        appStatus = learnignCenterImpl.getAppUnlockStatus(itemId, userId);
        learnignCenterImpl.closeConn();
        return appStatus;
    }

    /**
     * Check for app unlock status
     *
     * @param userId
     * @param playerId
     * @param context
     */
    public static void checkAppUnlockStatus(final String userId, final String playerId,
                                            final Context context,
                                            final CheckUnlockStatusCallback listener) {

        new AsyncTask<Void, Void, CoinsFromServerObj>() {
            private String apiString = null;
            @Override
            protected void onPreExecute() {
                apiString = "userId=" + userId + "&playerId=" + playerId;
                super.onPreExecute();
            }

            @Override
            protected CoinsFromServerObj doInBackground(Void... params) {
                LearningCenteServerOperation learnignCenterOpr = new LearningCenteServerOperation();
                return learnignCenterOpr.getSubscriptionInfoForUser(apiString);
            }

            @Override
            protected void onPostExecute(CoinsFromServerObj coinsFromServer) {
                if (coinsFromServer != null) {
                    //update app status in local database
                    if (coinsFromServer.getAppUnlock() != -1) {
                        ArrayList<PurchaseItemObj> purchaseItem = new ArrayList<PurchaseItemObj>();
                        PurchaseItemObj purchseObj = new PurchaseItemObj();
                        purchseObj.setUserId(userId);
                        purchseObj.setItemId(APP_UNLOCK_ID);
                        purchseObj.setStatus(coinsFromServer.getAppUnlock());
                        purchaseItem.add(purchseObj);

                        LearningCenterimpl learningCenter = new LearningCenterimpl(context);
                        learningCenter.openConn();

                        learningCenter.deleteFromPurchaseItem(userId, APP_UNLOCK_ID);
                        learningCenter.insertIntoPurchaseItem(purchaseItem);
                        learningCenter.closeConn();
                    }
                }
                listener.onRequestComplete(coinsFromServer);
            }
        }.execute();
    }


    /**
     * Return the default schook with Can't find my school
     *
     * @return
     */
    public static SchoolDTO getDefaultSchool() {
        SchoolDTO school = new SchoolDTO();
        school.setSchoolId("0000");
        school.setSchoolName("Cannot find my school");
        return school;
    }

    /**
     * Return the default teacher with can not find my teacher
     *
     * @return
     */
    public static TeacherDTO getDefaultTeacher() {
        TeacherDTO teacher = new TeacherDTO();
        teacher.setfName("Cannot find my teacher");
        teacher.setlName("");
        teacher.setTeacherUserId("-1");
        return teacher;
    }

    /**
     * Register user
     *
     * @param context
     * @param country
     * @param grade
     * @param cityOrZip
     * @param school
     * @param teacher
     * @param isParent
     * @param tempPlayer
     * @param registration
     * @param profileImageName
     */
    public static void registereUser(Context context, String country, int grade, String cityOrZip,
                                     SchoolDTO school, TeacherDTO teacher, String isParent, TempPlayer tempPlayer,
                                     Registration registration, String profileImageName) {

        initializeProcessDialog(context);

        String selectedCountry = country;
        String countryId = MathFriendzyHelper.getCountryIdByCountryName(selectedCountry, context);
        String countryIso = MathFriendzyHelper.getCountryIsoByCountryName(selectedCountry, context);
        int selectedGrade = MathFriendzyHelper.getGrade(grade + "");

        String stateId = "0";//default state id
        String stateCode = "";//default state code
        String state = "";
        String city = "";//default city
        String zipCode = "";

        if (selectedCountry.equalsIgnoreCase(MathFriendzyHelper.UNITED_STATE)) {
            zipCode = cityOrZip;
        } else if (selectedCountry.equalsIgnoreCase(MathFriendzyHelper.CANADA)) {
            zipCode = cityOrZip;
        } else {
            city = cityOrZip;
        }

        String languageId = MathFriendzyHelper.ENG_LANGUAGE_ID;//default language id
        //String languageCode = MathFriendzyHelper.ENG_LANGuAGE_CODE;//default language code
        String isParentValue = isParent;
        String schoolIdFromFind = school.getSchoolId();
        String schoolNameFromFind = school.getSchoolName();
        String volume = "0";
        String coins = "0";
        String players = "<AllPlayers></AllPlayers>";

        //String tempPlayerUserName = null;
        if (!MathFriendzyHelper.isTempraroryPlayerCreated(context)) {
            MathFriendzyHelper.createBlankTempTable(context);
        }

        //if(MathFriendzyHelper.isTempraroryPlayerCreated(this)){
        if (tempPlayer != null) {
            tempPlayer.setUserName(registration.getUserName());
            ArrayList<TempPlayer> tempPlayerObj = new ArrayList<TempPlayer>();
            tempPlayerObj.add(tempPlayer);
            players = "<AllPlayers>" + getXmlPlayer(tempPlayerObj, context) + "</AllPlayers>";
        } else {
            TempPlayer defaultPlayer = MathFriendzyHelper.createDefaultPlayer
                    (context, registration.getfName(),
                            registration.getlName(), city, selectedGrade,
                            school, teacher, registration.getUserName(),
                            zipCode, state, selectedCountry, profileImageName);
            ArrayList<TempPlayer> tempPlayerObj = new ArrayList<TempPlayer>();
            tempPlayerObj.add(defaultPlayer);
            players = "<AllPlayers>" + getXmlPlayer(tempPlayerObj, context) + "</AllPlayers>";
        }

        RegistereUserDto regObj = new RegistereUserDto();
        regObj.setFirstName(registration.getfName());
        regObj.setLastName(registration.getlName());
        regObj.setEmail(registration.getEmail());
        regObj.setPass(registration.getPassword());
        regObj.setCountryId(countryId);
        regObj.setCountryIso(countryIso);
        regObj.setStateId(stateId);
        regObj.setStateCode(stateCode);
        regObj.setCity(city);
        regObj.setPreferedLanguageId(languageId);
        regObj.setSchoolId(schoolIdFromFind);
        regObj.setSchoolName(schoolNameFromFind);
        regObj.setIsParent(isParentValue);
        regObj.setIsStudent("0");
        regObj.setIsTeacher("0");
        regObj.setVolume(volume);
        regObj.setZip(zipCode);
        regObj.setPlayers(players);
        regObj.setCoins(coins);

        if (CommonUtils.isInternetConnectionAvailable(context)) {
            showDialog();
            registerUser(regObj, registration, context, tempPlayer);
        } else {
            CommonUtils.showInternetDialog(context);
        }
    }

    /**
     * This method return the xml format of the temp player data
     *
     * @param tempPlayerObj
     * @return
     */
    private static String getXmlPlayer(ArrayList<TempPlayer> tempPlayerObj, Context context) {
        StringBuilder xml = new StringBuilder("");
        for (int i = 0; i < tempPlayerObj.size(); i++) {

            PlayerTotalPointsObj playerObj = null;
            LearningCenterimpl learningCenterimpl = new LearningCenterimpl(context);
            learningCenterimpl.openConn();
            playerObj = learningCenterimpl.getDataFromPlayerTotalPoints
                    (tempPlayerObj.get(i).getPlayerId() + "");
            learningCenterimpl.closeConn();
            xml.append("<player>" +
                    "<playerId>" + tempPlayerObj.get(i).getPlayerId() + "</playerId>" +
                    "<fName>" + tempPlayerObj.get(i).getFirstName() + "</fName>" +
                    "<lName>" + tempPlayerObj.get(i).getLastName() + "</lName>" +
                    "<grade>" + tempPlayerObj.get(i).getGrade() + "</grade>" +
                    "<schoolId>" + tempPlayerObj.get(i).getSchoolId() + "</schoolId>" +
                    "<teacherId>" + tempPlayerObj.get(i).getTeacherUserId() + "</teacherId>" +
                    "<indexOfAppearance>-1</indexOfAppearance>" +
                    "<profileImageId>" + tempPlayerObj.get(i).getProfileImageName() + "</profileImageId>" +
                    "<coins>" + playerObj.getCoins() + "</coins>" +
                    "<points>" + playerObj.getTotalPoints() + "</points>" +
                    "<userName>" + tempPlayerObj.get(i).getUserName() + "</userName>" +
                    "<competeLevel>" + playerObj.getCompleteLevel() + "</competeLevel>" +
                    "</player>");
        }
        return xml.toString();
    }


    /**
     * Register user
     *
     * @param regObj
     */
    private static void registerUser(final RegistereUserDto regObj,
                                     final Registration registration, final Context context,
                                     final TempPlayer tempPlayer) {
        MathFriendzyHelper.registerUser(context, regObj, new OnRegistration() {
            @Override
            public void onRegistrationComplete(int result) {
                if (result == Register.SUCCESS) {
                    afterRegisterSuccess(context, tempPlayer, registration, regObj);
                } else if (result == Register.INVALID_CITY) {
                    dismissDialog();
                    //MathFriendzyHelper.showWarningDialog(getCurrentObj(), alertMsgInvalidCity);
                    MathFriendzyHelper.showWarningDialog(context, "Invalid zip code");
                } else if (result == Register.INVALID_ZIP) {
                    dismissDialog();
                    MathFriendzyHelper.showWarningDialog(context, "invalid zip code");
                } else if (result == 0) {
                    dismissDialog();
                    CommonUtils.showInternetDialog(context);
                }
            }
        });
    }

    /**
     * Do the thing after registration
     *
     * @param regObj
     */
    private static void afterRegisterSuccess(final Context context, final TempPlayer tempPlayer
            , final Registration registration, final RegistereUserDto regObj) {
        MathFriendzyHelper.saveUserLogin(context, true);
        MathFriendzyHelper.addUserOnServerWithAndroidDevice(context);

        if (tempPlayer != null) {//If temp player exist then do the following
            String playerId = MathFriendzyHelper.getPlayerIdOfTempPlayerWhichIsNowRegister
                    (context, tempPlayer);
            String userId = MathFriendzyHelper.getUserIdOfTempPlayerWhichIsNowRegister
                    (context, tempPlayer);
            MathFriendzyHelper.updateTempPlayerDataWhichIsNotRegisteredPlayed(userId, playerId, context);

            //for avatar purchased by temp player
            ArrayList<String> avatarList = MathFriendzyHelper.updatePlayerAvterAndGetList
                    (userId, playerId, context);
            if (avatarList.size() > 0) {
                if (CommonUtils.isInternetConnectionAvailable(context)) {
                    MathFriendzyHelper.saveAvatar(userId, playerId, avatarList);
                }
            }

            //item puchased by temmp player
            ArrayList<String> purchaseItemList = MathFriendzyHelper.
                    getUserPurchaseItemList(context, userId);
            if (purchaseItemList.size() > 0) {
                if (CommonUtils.isInternetConnectionAvailable(context)) {
                    MathFriendzyHelper.savePurchasedItem(context, userId, purchaseItemList);
                }
            }
        }
        //for language
        if (tempPlayer != null) {
            ArrayList<MathResultTransferObj> mathResultList = MathFriendzyHelper
                    .getResultMathList(context);
            if (mathResultList.size() > 0) {
                if (CommonUtils.isInternetConnectionAvailable(context)) {
                    MathFriendzyHelper.saveTempPlayerScoreOnServer(context, mathResultList, new OnRequestComplete() {
                        @Override
                        public void onComplete() {
                            MathFriendzyHelper.deleteFromMath(context);
                            ArrayList<PlayerEquationLevelObj> playerquationData = MathFriendzyHelper
                                    .getPlayerEquationData(context,
                                            MathFriendzyHelper
                                                    .getPlayerIdOfTempPlayerWhichIsNowRegister
                                                            (context, tempPlayer));

                            if (playerquationData.size() > 0) {
                                if (CommonUtils.isInternetConnectionAvailable(context)) {
                                    MathFriendzyHelper.addLevelAsynckTask(playerquationData,
                                            new OnRequestComplete() {
                                                @Override
                                                public void onComplete() {
                                                    dismissDialog();
                                                    doneRegistration(context, tempPlayer,
                                                            registration, regObj);
                                                }
                                            });
                                } else {
                                    dismissDialog();
                                    CommonUtils.showInternetDialog(context);
                                }
                            } else {
                                dismissDialog();
                                doneRegistration(context, tempPlayer,
                                        registration, regObj);
                            }
                        }
                    }, tempPlayer.getUserName());
                } else {
                    dismissDialog();
                    CommonUtils.showInternetDialog(context);
                }
            } else {
                dismissDialog();
                doneRegistration(context, tempPlayer,
                        registration, regObj);
            }
        } else {
            dismissDialog();
            doneRegistration(context, tempPlayer,
                    registration, regObj);
        }
    }

    /**
     * Open the screen after registration process id done
     */
    private static void doneRegistration(Context context, TempPlayer tempPlayer,
                                         Registration registration, RegistereUserDto regObj) {
        MathFriendzyHelper.setCheckPlayer(context);
        if (tempPlayer != null) {
            MathFriendzyHelper.openEditRegisteredUserScreenAfterRegistration(context,
                    registration.getEmail());
        } else {
            MathFriendzyHelper.openMainScreenAfterRegistration(context,
                    registration.getEmail(), regObj.getIsParent());
        }
    }


    /**
     * Convert the string into underline formate
     *
     * @param string
     * @return
     */
    public static String convertStringToUnderLineString(String string) {
        return "<html>" + "<u>" + string + "</u></html>";
    }

    /**
     * Scroll Up the scroll view on button click
     *
     * @param btnScrollUp
     * @param scrollView
     */
    public static void scrollUp(Button btnScrollUp, final ScrollView scrollView
            , final OnScrollChange onScrollListener) {

        btnScrollUp.setOnTouchListener(new View.OnTouchListener() {

            private Handler mHandler;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (mHandler != null)
                            return true;
                        mHandler = new Handler();
                        mHandler.postDelayed(mAction, INITAIL_DELAY_TIME);
                        break;
                    case MotionEvent.ACTION_UP:
                        if (mHandler == null)
                            return true;
                        mHandler.removeCallbacks(mAction);
                        mHandler = null;
                        break;
                }
                return false;
            }

            Runnable mAction = new Runnable() {
                @Override
                public void run() {
                    scrollView.scrollTo((int) scrollView.getScrollX(),
                            (int) scrollView.getScrollY() - SCROLL_UP_DWON);
                    mHandler.postDelayed(mAction, REPEAT_DELAY_TIME);
                    if (scrollView.getScrollY() <= 0) {
                        onScrollListener.onScrolling(SCROLL_UP_COMPLETE);
                    } else {
                        onScrollListener.onScrolling(ON_SCROLLING);
                    }
                }
            };
        });
    }

    /**
     * Scroll Down
     *
     * @param btnScrollDown
     * @param scrollView
     */
    public static void scrollDown(Button btnScrollDown, final ScrollView scrollView
            , final OnScrollChange onScrollListener) {
        btnScrollDown.setOnTouchListener(new View.OnTouchListener() {

            private Handler mHandler;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (mHandler != null)
                            return true;
                        mHandler = new Handler();
                        mHandler.postDelayed(mAction, INITAIL_DELAY_TIME);
                        break;
                    case MotionEvent.ACTION_UP:
                        if (mHandler == null)
                            return true;
                        mHandler.removeCallbacks(mAction);
                        mHandler = null;
                        break;
                }
                return false;
            }

            Runnable mAction = new Runnable() {
                @Override
                public void run() {
                    scrollView.scrollTo((int) scrollView.getScrollX(),
                            (int) scrollView.getScrollY() + SCROLL_UP_DWON);
                    mHandler.postDelayed(mAction, REPEAT_DELAY_TIME);
                    View view = (View) scrollView.getChildAt(0);
                    int diff = (view.getBottom() - (scrollView.getHeight() +
                            scrollView.getScrollY()));
                    if (diff == 0) {//on scroll down complete
                        onScrollListener.onScrolling(SCROLL_DOWN_COMPLETE);
                    } else {
                        onScrollListener.onScrolling(ON_SCROLLING);
                    }
                }
            };
        });
    }

    /**
     * Get chat work area message
     *
     * @param param
     * @param context
     */
    public static void getWorkAreaChatMessage(GetWorkAreaChatMsgParam param, Context context) {
        new MyAsyckTask(ServerOperation.createPostRequestToGetChatWorkAreaMsg(param)
                , null, ServerOperationUtil.GET_WORK_AREA_CHAT_MSG_REQUEST, context,
                (HttpResponseInterface) context, ServerOperationUtil.SIMPLE_DIALOG, false,
                context.getString(R.string.please_wait_dialog_msg))
                .execute();
    }

    /**
     * Save homework area chat
     *
     * @param param
     * @param context
     */
    public static void saveWorkAreaChatMessage(SaveWorkAreaChatParam param, Context context) {
        new MyAsyckTask(ServerOperation.createPostRequestToSaveChatWorkAreaMsg(param)
                , null, ServerOperationUtil.SAVE_WORK_AREA_CHAT_MSG_REQUEST, context,
                (HttpResponseInterface) context, ServerOperationUtil.SIMPLE_DIALOG, false,
                context.getString(R.string.please_wait_dialog_msg))
                .execute();
    }

    /**
     * Return the fragment manager
     *
     * @param context
     * @return
     */
    public static FragmentManager getFragmentManager(Context context) {
        return ((Activity) context).getFragmentManager();
    }

    /**
     * Return the fragment transaction
     *
     * @param fragmentManager
     * @return
     */
    public static FragmentTransaction getFragmentTransaction
    (FragmentManager fragmentManager) {
        return fragmentManager.beginTransaction();
    }

    /**
     * OPen the tutor session screen
     *
     * @param context
     * @param argument
     */
    public static void openTutorSessionActivity(Context context,
                                                TutorSessionBundleArgument argument) {
        Intent intent = new Intent(context, ActTutorSession.class);
        intent.putExtra("argument", argument);
        context.startActivity(new Intent(context, ActTutorSession.class));
    }


    /**
     * Check selected player register on quick blox or not
     *
     * @param selectedPlayer
     * @return
     */
    public static boolean isUserAlreadyRegisterOnQuickBlox(UserPlayerDto selectedPlayer) {
        try {
            if (selectedPlayer.getChatId() != null &&
                    !selectedPlayer.getChatId().equalsIgnoreCase("0")
                    && selectedPlayer.getChatId().length() > 0
                    && selectedPlayer.getChatUserName() != null &&
                    !selectedPlayer.getChatUserName().equalsIgnoreCase("0")
                    && selectedPlayer.getChatUserName().length() > 0) {
                return true;
            }
            return false;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }


    /**
     * Register user on quick blox
     *
     * @param selectedPlayer
     * @param context
     */
    public static void registerUserToQuickBlox(final UserPlayerDto selectedPlayer,
                                               final Context context,
                                               final RegisterUserOnQBCallback callback) {
        getNewChatUserName(context, new GetChatUserNameCallback() {
            @Override
            public void onSuccess(String chatUserName) {
                final QBCreateSession qbsession = QBCreateSession.getInstance(context);
                qbsession.registerUserOnQuickBlox(getRegisterUser
                        (selectedPlayer, chatUserName), new RegisterUserOnQBCallback() {
                    @Override
                    public void onSuccess(final QBUser qbUser, final Bundle bundle) {
                        updateChatDetailToPlayer(context, selectedPlayer, qbUser);
                        qbsession.logoutFromChat(new OnRequestComplete() {
                            @Override
                            public void onComplete() {
                                ((Activity)context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onSuccess(qbUser, bundle);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    /**
     * Register user on quick blox
     *
     * @param selectedPlayer
     * @param context
     */
    public static void registerUserToQuickBloxWithSignIn(final UserPlayerDto selectedPlayer,
                                                         final Context context,
                                                         final RegisterUserOnQBCallback callback) {
        getNewChatUserName(context, new GetChatUserNameCallback() {
            @Override
            public void onSuccess(String chatUserName) {
                final QBCreateSession qbsession = QBCreateSession.getInstance(context);
                qbsession.registerUserOnQuickBloxWithSignIn(getRegisterUser
                        (selectedPlayer, chatUserName), new RegisterUserOnQBCallback() {
                    @Override
                    public void onSuccess(final QBUser qbUser, final Bundle bundle) {
                        updateChatDetailToPlayer(context, selectedPlayer, qbUser);
                        callback.onSuccess(qbUser, bundle);
                        //qbsession.deleteSession();
                        /*qbsession.logoutFromChat(new OnRequestComplete() {
                            @Override
                            public void onComplete() {
                                ((Activity)context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onSuccess(qbUser, bundle);
                                    }
                                });
                            }
                        });*/

                    }
                });
            }
        });
    }

    private static RegisterUserToQB getRegisterUser(UserPlayerDto selectedPlayer,
                                                    String chatUserName) {
        RegisterUserToQB registerUser = new RegisterUserToQB();
        registerUser.setLogin(chatUserName);
        registerUser.setPass(chatUserName);
        registerUser.setFullUserName(selectedPlayer.getFirstname() + " "
                + selectedPlayer.getLastname());
        return registerUser;
    }

    /**
     * Return the client socket task
     *
     * @param clientCallback
     * @return
     */
    public static MyClientTask getClientSocketTask(ClientCallback clientCallback) {
        MyClientTask task = new MyClientTask(ICommonUtils.SERVER_IP,
                ICommonUtils.SERVER_PORT, clientCallback);
        task.execute();
        return task;
    }

    /**
     * Return the client socket task
     *@param  chatRequestId - chat request id to create the room based on that id
     * @param clientCallback
     * @return
     */
    public static MyWebSocket getClientWebSocketTask(Context context
            , ClientCallback clientCallback , int chatRequestId , String playerId) {
        MyWebSocket task = new MyWebSocket(context, clientCallback  , chatRequestId , playerId);
        task.execute();
        return task;
    }

    /**
     * login user to quick blox
     *
     * @param seleUserPlayerDto
     * @param context
     */
    public static void loginUserToQuickBlox(UserPlayerDto seleUserPlayerDto, Context context
            , final QBLoginSuccessCallback callback) {
        final QBCreateSession qbsession = QBCreateSession.getInstance(context);
        qbsession.createSession(seleUserPlayerDto.getChatUserName(),
                seleUserPlayerDto.getChatUserName(), new QBCreateSessionCallback() {
                    @Override
                    public void onSuccess(QBSession session, Bundle args, QBUser user) {
                        qbsession.loginToChat(user, new QBOnLoginSuccess() {
                            @Override
                            public void onloginSuccess() {
                                callback.onLoginSuccess();
                            }
                        });
                    }
                });
    }


    /**
     * Get new chat user name
     *
     * @param context
     * @param getChatUserNameCallback
     */
    public static void getNewChatUserName(final Context context,
                                          final GetChatUserNameCallback getChatUserNameCallback) {
        String url = ICommonUtils.COMPLETE_URL + "action=getNewChatUserName";
        new MyAsyckTask(null, url, ServerOperationUtil.GET_NEW_CHAT_USER_NAME, context,
                new HttpResponseInterface() {
                    @Override
                    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                        GetNewChatUserNameResponse response = (GetNewChatUserNameResponse) httpResponseBase;
                        if (response.getResult().equalsIgnoreCase("success")) {
                            getChatUserNameCallback.onSuccess(response.getChatUserName());
                        } else {
                            CommonUtils.showInternetDialog(context);
                        }
                    }
                }, ServerOperationUtil.SIMPLE_DIALOG, false, context.getString
                (R.string.please_wait_dialog_msg)).execute();
    }

    /**
     * Update the chat data to the selected user
     *
     * @param context
     * @param selectedPlayerDto
     * @param qbUser
     */
    public static void updateChatDetailToPlayer(Context context,
                                                UserPlayerDto selectedPlayerDto, QBUser qbUser) {
        UserPlayerOperation playerOperation = new UserPlayerOperation(context);
        playerOperation.updateChatPlayerDetail(selectedPlayerDto, qbUser);
        playerOperation.closeConn();

        UpdateChatIdParam param = new UpdateChatIdParam();
        param.setAction("updateChatIdForPlayer");
        param.setUserId(selectedPlayerDto.getParentUserId());
        param.setPlayerId(selectedPlayerDto.getPlayerid());
        param.setChatUserName(qbUser.getLogin());
        param.setChatId(qbUser.getId() + "");

        new MyAsyckTask(ServerOperation.createPostRequestToupdateChatIdForPlayer(param)
                , null, ServerOperationUtil.UPDATE_CHAT_ID_TO_SERVER_REQUEST, context,
                new HttpResponseInterface() {
                    @Override
                    public void serverResponse(HttpResponseBase httpResponseBase,
                                               int requestCode) {

                    }
                }, ServerOperationUtil.SIMPLE_DIALOG, false,
                context.getString(R.string.please_wait_dialog_msg)).execute();
    }

    /**
     * Show email chooser account
     *
     * @param context
     * @param okButtontext
     * @param cencelButtonText
     * @param title
     * @param message
     * @param choices
     * @param listener
     */
    public static void showEmailChooserDialog(Context context, String okButtontext,
                                              String cencelButtonText, String title, String message,
                                              CharSequence[] choices, final ChoiceSelectionListener listener) {
        int selectedChoice = 0;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setSingleChoiceItems(choices, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.onOk(which);
                dialog.cancel();
            }
        });

        builder.setNegativeButton(cencelButtonText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                //Log.e(TAG , " cancel " + id);
                dialog.cancel();
                listener.onCancel(0);
            }
        });

        builder.setPositiveButton(okButtontext, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                //Log.e(TAG , " ok " + id);
                dialog.cancel();
                listener.onOk(0);
            }
        });

        builder.create();
        builder.show();
    }


    /**
     * Return the Display matrix
     *
     * @param context
     * @return
     */
    public static DisplayMetrics getDeviceDimention(Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        /*int width = metrics.widthPixels;
        int height = metrics.heightPixels;*/
        return metrics;
    }


    /**
     * Get drawing points
     * @param pointsString
     * @return
     */
    public static PointF getPoints(String pointsString , DrawDeviceDimention dimention) {
        try {
            String BEGIN = "begin";
            String ERASE = "er";
            String END = "end";
            String START_BRACES = "\\{";
            String END_BRACES = "\\}";
            String POINTS = "p:";
            String newString = pointsString.replaceAll(BEGIN, "").replaceAll(ERASE, "")
                    .replaceAll(START_BRACES, "").replaceAll(END_BRACES, "").replaceAll(END, "")
                    .replaceAll(POINTS , "");
            CommonUtils.printLog(TAG , "new string " + newString);
            float x = Float.parseFloat(newString.split(",")[0]);
            float y = Float.parseFloat(newString.split(",")[1]);
            PointF points = new PointF();
            if(dimention != null) {
                points.x = (x * dimention.getCurrentDeviceWidth()) / dimention.getOtherDeviceWidth();//new line
                points.y = (y * dimention.getCurrentDeviceHeight()) / dimention.getOtherDeviceHeight();//new line
            }else{
                points.x = x;
                points.y = y;
            }
            CommonUtils.printLog(TAG , "point " + points.x + " " + points.y);
            return points;
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtils.printLog(TAG , "getPoints() Exception  " + e.getMessage());
            return null;
        }
    }

    /**
     * Get drawing points
     *
     * @param pointsString
     * @return
     */
    public static PointF getDeviceScalePoints(String pointsString, DrawDeviceDimention dimention) {
        try {
            String BEGIN = "begin";
            String ERASE = "er";
            String END = "end";
            String START_BRACES = "\\{";
            String END_BRACES = "\\}";
            String POINTS_PREFF = "p:";
            String newString = pointsString.replaceAll(BEGIN, "").replaceAll(ERASE, "")
                    .replaceAll(START_BRACES, "").replaceAll(END_BRACES, "").replaceAll(END, "")
                    .replaceAll(POINTS_PREFF, "");

            /*if (CommonUtils.LOG_ON)
                Log.e(TAG, "new string " + newString);*/

            float x = Float.parseFloat(newString.split(",")[0]);
            float y = Float.parseFloat(newString.split(",")[1]);
            PointF points = new PointF();
            points.x = (x * dimention.getCurrentDeviceWidth()) / dimention.getOtherDeviceWidth();
            points.y = (y * dimention.getCurrentDeviceHeight()) / dimention.getOtherDeviceHeight();
            return points;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Return the device dimention
     *
     * @param currentDeviceMatrix
     * @param otherDeviceSize
     * @return
     */
    public static DrawDeviceDimention getDrawDeviceDimention(DisplayMetrics currentDeviceMatrix,
                                                             String otherDeviceSize) {
        try {
            DrawDeviceDimention dimention = new DrawDeviceDimention();
            dimention.setCurrentDeviceWidth(currentDeviceMatrix.widthPixels);
            dimention.setCurrentDeviceHeight(currentDeviceMatrix.heightPixels);
            if (otherDeviceSize.length() > 0) {
                dimention.setOtherDeviceWidth(Float.parseFloat(otherDeviceSize.split(",")[0]));
                dimention.setOtherDeviceHeight(Float.parseFloat(otherDeviceSize.split(",")[1]));
            }
            return dimention;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Save message for single chat
     *
     * @param context
     * @param param
     * @param callback
     */
    public static void saveMessageForSingleChat(Context context, SaveMessageForSingleChatParam param,
                                                final SaveMessageForSingleChatCallback callback) {

        new MyAsyckTask(ServerOperation.createPostRequestForSaveMessageForSingleChat(param)
                , null, ServerOperationUtil.SAVE_SINGLE_CHAT_MESSAGE_REQUEST,
                context, new HttpResponseInterface() {
            @Override
            public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                callback.onSuccess((SaveMessageForSingleChatResponse) httpResponseBase);
            }
        }, ServerOperationUtil.SIMPLE_DIALOG, false,
                context.getString(R.string.please_wait_dialog_msg)).execute();
    }

    /**
     * Update draw points for player
     *
     * @param context
     * @param param
     */
    public static void updateDrawPonitsForPlayer(Context context, UpdateDrawPointsForPlayerParam param) {
        if(CommonUtils.isInternetConnectionAvailable(context)) {
            new MyAsyckTask(ServerOperation.createPostRequestForUpdateDrawPonitsForPlayer(param)
                    , null, ServerOperationUtil.UPDATE_DRAW_POINT_FOR_PLAYER_REQUEST,
                    context, new HttpResponseInterface() {
                @Override
                public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {

                }
            }, ServerOperationUtil.SIMPLE_DIALOG, false,
                    context.getString(R.string.please_wait_dialog_msg)).execute();
        }
    }


    /**
     * Get drawing point from server for drawing chat
     *
     * @param context
     * @param param
     */
    public static void getDrawingPointsFromServer(Context context, GetDrawingPointForChatParam param
            , final GetDrawingPointForChatOnSuccess callback) {
        new MyAsyckTask(ServerOperation.createPostRequestForGetDrawingPointForChat(param)
                , null, ServerOperationUtil.GET_DRAWING_POINT_REQUEST,
                context, new HttpResponseInterface() {
            @Override
            public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                callback.onSuccess((GetDrawingPointForChatResponse) httpResponseBase);
            }
        }, ServerOperationUtil.SIMPLE_DIALOG, false,
                "Please Wait...").execute();
    }

    /**
     * Add time spent in tutor session.
     *
     * @param context
     * @param param
     */
    public static void addTimeSpentInTutorSession(Context context, AddTimeSpentInTutorSessionParam param
            , final HttpResponseInterface responseIntefrface) {
        new MyAsyckTask(ServerOperation.createPostRequestForAddTimeSpentInTutorSession(param)
                , null, ServerOperationUtil.ADD_TIME_SPENT_IN_TUTOR_SESSION_REQUEST,
                context, new HttpResponseInterface() {
            @Override
            public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                responseIntefrface.serverResponse(httpResponseBase, requestCode);
            }
        }, ServerOperationUtil.SIMPLE_DIALOG, param.isShowDialog(),
                context.getString(R.string.please_wait_dialog_msg)).execute();
    }

    /**
     * Show the rate us dialog
     *
     * @param context
     * @param message
     * @param ok
     * @param noThanks
     * @param callback
     */
    public static void showRateUsDialog(Context context, String message, String ok, String noThanks,
                                        RateUsCallback callback) {
        DialogGenerator dg = new DialogGenerator(context);
        dg.rateDialog(message, ok, noThanks, callback);
    }

    /**
     * Rete tutor session
     *
     * @param context
     * @param param
     */
    public static void rateTutorSession(Context context, RateTutorSessionParam param,
                                        final RateTutorSessionCallback callback) {
        new MyAsyckTask(ServerOperation.createPostRequestForRateTutorSession(param)
                , null, ServerOperationUtil.RATE_TUTOR_TUTOR_SESSION_REQUEST,
                context, new HttpResponseInterface() {
            @Override
            public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                callback.onServerResponse(httpResponseBase, requestCode);
            }
        }, ServerOperationUtil.SIMPLE_DIALOG, true,
                "Please Wait...").execute();
    }


    /**
     * Return time spent text
     *
     * @param totalTimeInSeconds
     * @return
     */
    public static String getTimeSpentText(int totalTimeInSeconds) {
        int hours = totalTimeInSeconds / (60 * 60);
        int minutes = (totalTimeInSeconds % (60 * 60)) / 60;
        int seconds = (totalTimeInSeconds % (60 * 60)) % 60;
        String returnString = "";
        if (hours > 0)
            returnString = String.format("%02d", hours)
                    + ":" + String.format("%02d", minutes)
                    + ":" + String.format("%02d", seconds);
        else
            returnString = String.format("%02d", minutes)
                    + ":" + String.format("%02d", seconds);
        return returnString;
    }

    /**
     * Return time spent text
     *
     * @param totalTimeInSeconds
     * @return
     */
    public static String getTimeSpentTextInHHMM(int totalTimeInSeconds) {
        int hours = totalTimeInSeconds / (60 * 60);
        int minutes = (totalTimeInSeconds % (60 * 60)) / 60;
        int seconds = (totalTimeInSeconds % (60 * 60)) % 60;
        String returnString = "";
        //if (hours > 0)
        returnString = String.format("%02d", hours)
                + ":" + String.format("%02d", minutes);
        //+ ":" + String.format("%02d", seconds);
        /*else
            returnString = String.format("%02d", minutes)
                    + ":" + String.format("%02d", seconds);*/
        return returnString;
    }

    public static String getConvertedTimeStringText
            (int totalTimeInSeconds , String lblHour , String lblMinutes , String lblSeconds){
        if(totalTimeInSeconds < 0)
            totalTimeInSeconds = 0;

        //When i have 0 minutes left, instead of displaying 00 minutes and 00 seconds.
        // please just say "You currenlty have 0 minutes available.
        if(totalTimeInSeconds == 0)
            return 0 + " " + lblMinutes;

        int hours = totalTimeInSeconds / (60 * 60);
        int minutes = (totalTimeInSeconds % (60 * 60)) / 60;
        int seconds = (totalTimeInSeconds % (60 * 60)) % 60;
        String returnString = "";
        if (hours > 0)
            returnString = String.format("%02d", hours)
                    + " " + lblHour + " " +
                    String.format("%02d", minutes)
                    + " " + lblMinutes + " " +
                    String.format("%02d", seconds)
                    + " " + lblSeconds;
        else
            returnString = String.format("%02d", minutes)
                    + " " + lblMinutes + " " +
                    String.format("%02d", seconds)
                    + " " + lblSeconds;
        return returnString;
    }

    /**
     * Return time spent text
     *
     * @param totalTimeInSeconds
     * @return
     */
    public static String getTutorTimeSpentText(long totalTimeInSeconds) {
        long hours = totalTimeInSeconds / (60 * 60);
        long minutes = (totalTimeInSeconds % (60 * 60)) / 60;
        long seconds = (totalTimeInSeconds % (60 * 60)) % 60;
        return String.format("%02d", hours)
                + ":" + String.format("%02d", minutes);
    }

    /**
     * Check for time in between
     *
     * @param currentTime
     * @param startTime
     * @param endTime
     * @return
     */
    public static boolean isTimeInBetween(String currentTime, String startTime, String endTime) {
        Calendar currentCalender = getFormattedCalender(currentTime);
        Calendar startCalender = getFormattedCalender(startTime);
        Calendar endCalender = getFormattedCalender(endTime);
        Date currentDate = currentCalender.getTime();
        Date startDate = startCalender.getTime();
        Date endDate = endCalender.getTime();
        if (currentDate.after(startDate) && currentDate.before(endDate))
            return true;
        return false;
    }

    public static Calendar getFormattedCalender(String time) {
        Date currentDate = MathFriendzyHelper.getValidateDate(time, "HH:mm:ss");
        Calendar calender = MathFriendzyHelper.getCurrentCalender();
        calender.setTime(currentDate);
        calender.add(Calendar.DATE, 1);
        return calender;
    }

    /**
     * Check for notification
     *
     * @param message
     * @param key
     * @return
     */
    public static boolean checkNotificationForGivenKey(String message, String key) {
        try {
            JSONObject jsonObj = new JSONObject(message);
            jsonObj.getString(key);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Check for app in background
     *
     * @param context
     * @return
     */
    public static boolean isAppInBackGround(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Update the active inactive status
     */
    public static void updateAppActiveInActiveStatus(final Context context) {
       /* if (handler != null) {
            handler.removeCallbacksAndMessages(null);
        } else {
            handler = new Handler();
        }
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (CommonUtils.LOG_ON)
                    Log.e(TAG, "inside update status");

                if (CommonUtils.isInternetConnectionAvailable(context)) {
                    if (MathFriendzyHelper.isUserLogin(context)) {
                        try {
                            UserPlayerDto playerData = MathFriendzyHelper.getSelectedPlayerDataById
                                    (context, MathFriendzyHelper.getSelectedPlayerID(context));
                            if (playerData != null && !isAppInBackGround(context)) {
                                PpdateLastActivityTimeForPlayerParam param =
                                        new PpdateLastActivityTimeForPlayerParam();
                                param.setAction("updateLastActivityTimeForPlayer");
                                param.setPlayerId(playerData.getPlayerid());
                                param.setUserId(playerData.getParentUserId());
                                AsyncTask<Void, Void, HttpResponseBase> task = new MyAsyckTask(ServerOperation.
                                        createPostRequestForUpdateLastActivityTimeForPlayer(param)
                                        , null, ServerOperationUtil.
                                        UPDATE_LAST_ACTIVITY_TIME_FOR_PLAYER_REQUEST, context,
                                        new HttpResponseInterface() {
                                            @Override
                                            public void serverResponse(HttpResponseBase httpResponseBase,
                                                                       int requestCode) {

                                            }
                                        }, ServerOperationUtil.SIMPLE_DIALOG, false,
                                        context.getString(R.string.please_wait_dialog_msg));
                                MathFriendzyHelper.executeCommonTask(task);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    }
                }
                handler.postDelayed(this, ACTIVE_INACTIVE_UPDATE_STATUE_TIME);
            }
        }, ACTIVE_INACTIVE_UPDATE_STATUE_TIME);*/
    }

    /**
     * Update the isTutor value
     *
     * @param context
     * @param selectedPlayer
     * @param isTutor
     */
    public static void updateIsTutor(Context context, UserPlayerDto selectedPlayer, int isTutor) {
        UserPlayerOperation userPlayerOpr = new UserPlayerOperation(context);
        userPlayerOpr.updateIsTutorValue(selectedPlayer, isTutor);
        userPlayerOpr.closeConn();
    }

    /**
     * Return the device id
     *
     * @param context
     * @return
     */
    public static String getDeviceId(Context context) {
        try {
            String myAndroidDeviceId = "";
            TelephonyManager mTelephony = (TelephonyManager) context.
                    getSystemService(Context.TELEPHONY_SERVICE);
            if (mTelephony.getDeviceId() != null) {
                myAndroidDeviceId = mTelephony.getDeviceId();
            } else {
                myAndroidDeviceId = Settings.Secure.
                        getString(context.getApplicationContext().getContentResolver(),
                                Settings.Secure.ANDROID_ID);
            }

            if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS){
                myAndroidDeviceId = myAndroidDeviceId + "_" + MathFriendzyHelper.getPackageName(context);//"_mathplus";
            }else if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_TUTOR_PLUS){
                myAndroidDeviceId = myAndroidDeviceId + "_mathtutorplus";
            }

             /*if(AppVersion.BUILD_APP_TYPE == AppVersion.AMAZON){
                myAndroidDeviceId = myAndroidDeviceId + "_paid";
            }*/

            return myAndroidDeviceId;
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * Take screen shot and save into memory
     *
     * @param callback
     */
    public static void takeScreenShotAndSaveIntoMemory(final Context context, final View view, final
    TakeScreenShotAndSaveToMemoryCallback callback) {
        new AsyncTask<Void, Void, Void>() {
            private ProgressDialog pd = null;
            private File file = null;
            private Bitmap bitmap = null;

            @Override
            protected void onPreExecute() {
                pd = CommonUtils.getProgressDialog(context);
                pd.show();
            }

            @Override
            protected Void doInBackground(Void... params) {
                bitmap = MathFriendzyHelper.
                        takeScreenShot(context, view);
                file = MathFriendzyHelper.saveScreenShot(bitmap,
                        MathFriendzyHelper.getUniqueScreenShotName());
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                if (pd != null && pd.isShowing()) {
                    pd.cancel();
                }
                callback.onComplete(file, bitmap);
                super.onPostExecute(result);
            }
        }.execute();
    }

    /**
     * Take screen shot
     *
     * @param callback
     */
    public static void takeScreenShot(final Context context, final View view, final
    TakeScreenShotAndSaveToMemoryCallback callback) {
        try {
            new AsyncTask<Void, Void, Void>() {
                private ProgressDialog pd = null;
                private File file = null;
                private Bitmap bitmap = null;

                @Override
                protected void onPreExecute() {
                    pd = CommonUtils.getProgressDialog(context);
                    pd.show();
                }

                @Override
                protected Void doInBackground(Void... params) {
                    try {
                        bitmap = MathFriendzyHelper.
                                takeScreenShot(context, view);
                    } catch (Exception e) {
                        e.printStackTrace();
                        bitmap = null;
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void result) {
                    if (pd != null && pd.isShowing()) {
                        pd.cancel();
                    }
                    callback.onComplete(file, bitmap);
                    super.onPostExecute(result);
                }
            }.execute();
        } catch (Exception e) {
            callback.onComplete(null, null);
        }
    }

    /**
     * Download bitmap from url
     *
     * @param imageUrl
     * @param imageName
     * @param context
     */
    public static void downloadImageFromGivenUrl(final String imageUrl,
                                                 final String imageName,
                                                 final Context context,
                                                 final DownloadImageCallback callback) {
        final String newImageName = getPNGFormateImageName(imageName);
        AsyncTask<Void, Void, Bitmap> imageDownloadTask = new AsyncTask<Void, Void, Bitmap>() {
            private ProgressDialog pd = null;
            private Bitmap bitmap = null;
            private File file = null;

            @Override
            protected void onPreExecute() {
               /* pd = ServerDialogs.getProgressDialog(context, "Please wait...", 1);
                pd.show();*/
                super.onPreExecute();
            }

            @Override
            protected Bitmap doInBackground(Void... params) {
                try {
                    bitmap = CommonUtils.getBitmapWithOption(imageUrl + newImageName);
                    if(bitmap != null) {
                        if (!bitmap.isMutable())
                            bitmap = MathFriendzyHelper.convertToMutable(context, bitmap);
                    }
                    //MathFriendzyHelper.saveFileToSDCard(bitmap , imageName);
                    return bitmap;
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Bitmap result) {
                /*if (pd != null && pd.isShowing()) {
                    pd.cancel();
                }*/
                callback.onDownloadComplete(file, bitmap);
                super.onPostExecute(result);
            }
        };

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            imageDownloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            imageDownloadTask.execute();
    }

    /**
     * Download bitmap from url
     *
     * @param imageUrl
     * @param imageName
     * @param context
     */
    public static void downloadImageFromGivenUrlWithLoadingWheel(final String imageUrl,
                                                                 final String imageName,
                                                                 final Context context,
                                                                 final DownloadImageCallback callback) {
        final String newImageName = getPNGFormateImageName(imageName);
        AsyncTask<Void, Void, Bitmap> imageDownloadTask = new AsyncTask<Void, Void, Bitmap>() {
            private ProgressDialog pd = null;
            private Bitmap bitmap = null;
            private File file = null;

            @Override
            protected void onPreExecute() {
                pd = ServerDialogs.getProgressDialog(context, "", 1);
                pd.show();
                super.onPreExecute();
            }

            @Override
            protected Bitmap doInBackground(Void... params) {
                try {
                    bitmap = CommonUtils.getBitmapWithOption(imageUrl + newImageName);
                    if(bitmap != null) {
                        if (!bitmap.isMutable())
                            bitmap = MathFriendzyHelper.convertToMutable(context, bitmap);
                    }
                    //MathFriendzyHelper.saveFileToSDCard(bitmap , imageName);
                    return bitmap;
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Bitmap result) {
                if (pd != null && pd.isShowing()) {
                    pd.cancel();
                }
                callback.onDownloadComplete(file, bitmap);
                super.onPostExecute(result);
            }
        };

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            imageDownloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            imageDownloadTask.execute();
    }


    public static void executeTask(AsyncTask<Void, Void, Void> task){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            task.execute();
    }

    public static void executeCommonTask(AsyncTask<Void, Void, HttpResponseBase> task){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            task.execute();
    }

    /**
     * Return the correct message after removing the delimeter
     *
     * @param message
     * @return
     */
    public static String getCopyTextMessageAfterReplacement(String message) {
        return message.replaceAll(MathFriendzyHelper.FULL_MESSAGE_DELIMETER, "\n")
                .replaceAll(MathFriendzyHelper.STUDENT_MSG_PREFF, "")
                .replaceAll(MathFriendzyHelper.STUDENT_TEACHER_MSG_DELIMETER, "")
                .replaceAll(MathFriendzyHelper.TEACHER_MSG_PREFF, "")
                .replaceAll(MathFriendzyHelper.COLON_DELIMETER, "")
                .replaceAll(" ", "");
    }

    /**
     * Check for empty string+
     *
     * @param string
     * @return
     */
    public static boolean isEmpty(String string) {
        if (string != null && string.length() > 0)
            return false;
        return true;
    }

    /**
     * Add the duplicate work area content for the student
     *
     * @param context
     * @param paramFor - For GET_HELP , or SEE_ANSWER
     */
    public static void addDuplicateWorkAreaContent(Context context, int paramFor) {
        ActCheckHomeWork.getCurrentObj().addDuplicateWork(context, paramFor);
    }

    /**
     * Return the currect login user account type
     *
     * @param context
     * @return
     */
    public static int getUserAccountType(Context context) {
        try {
            UserRegistrationOperation userObj = new UserRegistrationOperation(context);
            RegistereUserDto regUserObj = userObj.getUserData();
            if (regUserObj.getIsParent().equals("2")) {//2 for student
                return STUDENT;
            } else if (regUserObj.getIsParent().equals("1")) {// 1 for parent
                return PARENT;
            } else if (regUserObj.getIsParent().equals("0")) {//0 for teacher
                return TEACHER;
            } else {
                return TEACHER;
            }
        } catch (Exception e) {
            return PARENT;
        }
    }

    /**
     * Check for valid string
     *
     * @param string , return true is a string does not contain any of the given special character ":\\<>"&'"
     * @return otherwise false
     */
    public static boolean isValidString(String string) {
        Pattern p = Pattern.compile(REGISTRATION_STRING_PATTERN,
                Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(string);
        return !m.find();
    }

    /**
     * Clear focus
     *
     * @param context
     */
    public static void clearFocus(Context context) {
        try {
            ((Activity) context).getCurrentFocus().clearFocus();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Retutn the special character message
     *
     * @param context
     * @return
     */
    private static String getSpecialCharacterNotAllowedMessage(Context context) {
        String lblSomeSpecialCharAreNotAllowed = null;
        Translation transeletion = new Translation(context);
        transeletion.openConnection();
        lblSomeSpecialCharAreNotAllowed = transeletion
                .getTranselationTextByTextIdentifier("lblSomeSpecialCharAreNotAllowed");
        transeletion.closeConnection();
        return lblSomeSpecialCharAreNotAllowed;
    }

    /**
     * Set in focus change listener
     *
     * @param context
     * @param edtText
     */
    public static void setFocusChangeListener(final Context context, final EditText edtText,
                                              final EditTextFocusChangeListener listener) {
        edtText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!isValidString(edtText.getText().toString())) {
                        edtText.setText("");
                        listener.onFocusLost(false);
                        MathFriendzyHelper.showWarningDialog(context,
                                getSpecialCharacterNotAllowedMessage(context));
                    } else {
                        listener.onFocusLost(true);
                    }
                }
            }
        });
    }

    /**
     * check the profanity word in the message
     * @param message
     * @return
     */
    public static boolean isProfinityWordExistInString(String message){
        try {
            if (profanityWordList != null) {
                String[] listOfWord = message.split(" ");
                for(String word : listOfWord) {
                    if (profanityWordList.contains
                            (word.trim().toLowerCase()))
                        return true;
                }
                return false;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Save the downloaded language at the time of downloading
     * @param context
     * @param langId
     */
    public static void saveDownloadedLanguage(Context context , String langId){
        SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
        SharedPreferences.Editor editor = sharedPreff.edit();
        editor.putString("langId", langId);
        editor.commit();
    }

    /**
     * get the downloaded language at the time of downloading
     * @param context
     * @return
     */
    public static String getDownloadedLangId(Context context){
        return context.getSharedPreferences(MATH_FRENDZY_PREFF, 0)
                .getString("langId", "");
    }

    /**
     * Get laguage translation
     * @param context
     * @param langId
     * @param langCode
     * @param appId
     * @param responseIntefrface
     */
    public static void getLanguageTranslation(final Context context ,
                                              final String langId , final String langCode ,
                                              final String appId ,
                                              final HttpServerRequest responseIntefrface){

        if(langId.equals(getDownloadedLangId(context))){
            responseIntefrface.onRequestComplete();
            return ;
        }else{
            //changes for New Assessment
            AssessmentTestImpl implObj = new AssessmentTestImpl(context);
            implObj.openConnection();
            implObj.deleteFromWordAssessmentStandards();
            implObj.deleteFromWordAssessmentSubStandards();
            implObj.deleteFromWordAssessmentSubCategoriesInfo();
            implObj.closeConnection();
            //end changes
            CommonUtils.LANGUAGE_CODE = langCode;
            CommonUtils.LANGUAGE_ID   = langId;
            saveDownloadedLanguage(context , langId);
        }

        new AsyncTask<Void,Void,Void>(){
            private ProgressDialog progressDialog = null;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = CommonUtils.getProgressDialog(context);
                progressDialog.show();
            }

            @Override
            protected Void doInBackground(Void... params) {
                Translation traneselation = new Translation(context);
                traneselation.getTransalateTextFromServer(langCode, langId, appId);
                traneselation.saveTranslationText();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if(progressDialog != null
                        && progressDialog.isShowing())
                    progressDialog.cancel();
                responseIntefrface.onRequestComplete();
                super.onPostExecute(aVoid);
            }
        }.execute();
    }


    /**
     * Return the current open screen name
     * @param context
     * @return
     */
    public static String getCurrentOpenActivityName(Context context){
        ActivityManager am = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
        String currentOpenScreenName = am.getRunningTasks(1).get(0).topActivity.getClassName();
        return  currentOpenScreenName;
    }

    /**
     * Save email subscription and email valid for the weekly report on the account page
     * @param context
     * @param emailSubscription
     * @param emailValid
     */
    public static void saveEmailSubscriptionAndValid(Context context ,
                                                     String emailSubscription ,
                                                     String emailValid){
        SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
        SharedPreferences.Editor editor = sharedPreff.edit();
        editor.putString("emailSubscription", emailSubscription);
        editor.putString("emailValid", emailValid);
        editor.commit();
    }

    /**
     * Get email valid value
     * @param context
     * @return
     */
    public static String getEmailValid(Context context){
        return context.getSharedPreferences(MATH_FRENDZY_PREFF, 0)
                .getString("emailValid", "");
    }

    /**
     * Get email subscription
     * @param context
     * @return
     */
    public static String getEmailSubscription(Context context){
        return context.getSharedPreferences(MATH_FRENDZY_PREFF, 0)
                .getString("emailSubscription", "");
    }

    /**
     * Save Tutor Area Responses
     * @param context
     * @param tutorAreaResponse
     */
    public static void saveTutorAreaResponse(Context context , String userId, String playerId
            , int tutorAreaResponse){
        SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
        SharedPreferences.Editor editor = sharedPreff.edit();
        editor.putString("tutorAreaResponse", userId + "-" + playerId + "-" + tutorAreaResponse);
        editor.commit();
    }

    /**
     * Get Tutor Area Responses
     * @param context
     * @return
     */
    public static int getTutorAreaResponses(Context context , String userId, String playerId){
        String activeTutorResponse = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0)
                .getString("tutorAreaResponse", null);
        if (activeTutorResponse != null && activeTutorResponse.length() > 0) {
            if (userId.equals(activeTutorResponse.split("-")[0])
                    && playerId.equals(activeTutorResponse.split("-")[1]))
                return Integer.parseInt(activeTutorResponse.split("-")[2]);
            return 0;
        } else {
            return 0;
        }
    }

    /**
     * Save Tutor Area Responses
     * @param context
     * @param tutorSessionResponse
     */
    public static void saveTutorSessionResponse(Context context , String userId, String playerId
            , int tutorSessionResponse){
        SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
        SharedPreferences.Editor editor = sharedPreff.edit();
        editor.putString("tutorSessionResponse", userId + "-" + playerId + "-" + tutorSessionResponse);
        editor.commit();
    }

    /**
     * Get Tutor Area Responses
     * @param context
     * @return
     */
    public static int getTutorSessionResponses(Context context , String userId, String playerId){
        String activeTutorResponse = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0)
                .getString("tutorSessionResponse", null);
        if (activeTutorResponse != null && activeTutorResponse.length() > 0) {
            if (userId.equals(activeTutorResponse.split("-")[0])
                    && playerId.equals(activeTutorResponse.split("-")[1]))
                return Integer.parseInt(activeTutorResponse.split("-")[2]);
            return 0;
        } else {
            return 0;
        }
    }


    /**
     * Return the App Version
     * @param context
     * @return
     */
    public static String getAppVersion(Context context){
        PackageInfo pInfo = null;
        String version = "";
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }



    /**
     * Get currect app open package name
     * @param context
     * @return
     */
    public static String getCurrectPackageName(Context context){
        ActivityManager am = (ActivityManager)
                context.getSystemService(Activity.ACTIVITY_SERVICE);
        String packageName = am.getRunningTasks(1).get(0).topActivity.getPackageName();
        return packageName;
    }


    /**
     * Check for given key in the json object
     * @param jsonObj
     * @param key
     * @return
     */
    public static boolean checkForKeyExistInJsonObj(JSONObject jsonObj , String key){
        try{
            return jsonObj.has(key);
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public static boolean checkPlayServices(Context context) {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            return false;
        }
        return true;
    }


    public static ProgressDialog getProgressDialog(Context context){
        ProgressDialog pd = new ProgressDialog(context);
        pd.setIndeterminate(false);//back button does not work
        pd.setCancelable(false);
        pd.setMessage(context.getString(R.string.please_wait_dialog_msg));
        return pd;
    }

    public static ProgressDialog getProgressDialog(Context context , String message){
        ProgressDialog pd = new ProgressDialog(context);
        pd.setIndeterminate(false);//back button does not work
        pd.setCancelable(false);
        pd.setMessage(message);
        return pd;
    }

    public static void showProgressDialog(ProgressDialog pd){
        if(pd != null && !pd.isShowing()){
            pd.show();
        }
    }

    public static void hideProgressDialog(ProgressDialog pd){
        if(pd != null && pd.isShowing()){
            pd.cancel();
        }
    }

    public static ImageLoader getImageLoaderInstance(){
        return ImageLoader.getInstance();
    }

    /**
     * Get display option
     * @return
     */
    public static DisplayImageOptions getDisplayOption(){
        return new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.loading_images)
                .showImageForEmptyUri(R.drawable.loading_images)
                .showImageOnFail(R.drawable.loading_images)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    /**
     * Get display option
     * @return
     */
    public static DisplayImageOptions getDisplayOptionForTutoringSession(){
        return new DisplayImageOptions.Builder()
                .showImageOnLoading(null)
                .showImageForEmptyUri(R.drawable.resources)
                .showImageOnFail(null)
                .considerExifParams(true)
                .bitmapConfig(Config.ARGB_8888)
                .imageScaleType(ImageScaleType.NONE)
                .build();
    }

    public static void clearBitmap(final Bitmap... bitmaps){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                MathFriendzyHelper.invokGC();
                for(int i = 0 ; i < bitmaps.length ; i ++ ) {
                    if (bitmaps[i] != null) {
                        bitmaps[i].recycle();
                        bitmaps[i] = null;
                    }
                }
                MathFriendzyHelper.invokGC();
            }
        });
    }

    public static void invokGC(){
        try {
            System.gc();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /*
    * Checks if a bitmap with the specified size fits in memory
    * @param bmpwidth Bitmap width
    * @param bmpheight Bitmap height
    * @param bmpdensity Bitmap bpp (use 2 as default)
    * @return true if the bitmap fits in memory false otherwise
    */
    public static boolean checkBitmapFitsInMemory(long bmpwidth,long bmpheight, int bmpdensity
            , int numberOfBitmap){
        long reqsize=bmpwidth*bmpheight*bmpdensity*numberOfBitmap;
        long allocNativeHeap = Debug.getNativeHeapAllocatedSize();
        final long heapPad=(long) Math.max(4*1024*1024,Runtime.getRuntime().maxMemory()*0.1);
        if ((reqsize + allocNativeHeap + heapPad) >= Runtime.getRuntime().maxMemory()){
            return false;
        }
        return true;
    }

    public static Bitmap createBitmap(int w , int h){
        /*if(createdBitmapForDrawing != null) {
            createdBitmapForDrawing = Bitmap
                    .createBitmap(w, h, Bitmap.Config.ARGB_8888);
        }*/
        MathFriendzyHelper.invokGC();
        return Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
    }

    public static String getMemoryInfo(){
        Debug.MemoryInfo memoryInfo = new Debug.MemoryInfo();
        Debug.getMemoryInfo(memoryInfo);
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int freeMemory = (int) (Runtime.getRuntime().freeMemory() / 1024);
        String memMessage = String.format(
                "Free=%d kB,\nFree=%d MB,\nMaxMem=%d kB,\nMaxMem=%d MB " +
                        ",\n Memory: Pss=%.2f MB, Private=%.2f MB, Shared=%.2f MB",
                freeMemory,freeMemory/1024 ,
                maxMemory,maxMemory/1024,
                memoryInfo.getTotalPss() / 1024.0,
                memoryInfo.getTotalPrivateDirty() / 1024.0,
                memoryInfo.getTotalSharedDirty() / 1024.0);
        return memMessage;
    }

    public static ViewGroup getViewGroup(Context context){
        /*return (ViewGroup) ((ViewGroup) ((Activity) context).
                findViewById(android.R.id.content)).getChildAt(0);*/
        return (ViewGroup) (((Activity) context).
                findViewById(android.R.id.content)).getRootView();
    }

    /* *//*
    * remove views from fragment on stop
    */
    public static void removeView(ViewGroup contentView) {
        if(contentView != null) {
            contentView.destroyDrawingCache();
            contentView.removeAllViews();
            contentView = null;
        }
    }

    /*public static void removeView(RelativeLayout contentView) {
        if(contentView != null) {
            contentView.destroyDrawingCache();
            contentView.removeAllViews();
            contentView = null;
        }
    }*/


    public static void recursiveLoopChildren(ViewGroup parent) {
        for (int i = parent.getChildCount() - 1; i >= 0; i--) {
            View child = parent.getChildAt(i);
            if (child instanceof ViewGroup) {
                recursiveLoopChildren((ViewGroup) child);
            } else {
                if (child != null) {
                    child = null;
                }
            }
        }
    }


    public static void initializeHeightAndWidth(Context context , boolean isTab){
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        drawViewHeight = metrics.heightPixels;
        drawViewWidth  = metrics.widthPixels;
        /*if(metrics.widthPixels > MAX_DRAW_VIEW_WIDTH){
            isDeviceSizeGreaterThanDraViewLimit = true;
            drawViewWidth = MAX_DRAW_VIEW_WIDTH;
        }*/
    }


    /**
     * Tracking screen
     * @param path
     */
    public static void TrackingScreen(Context context , String path){
        CommonUtils.printLog(TAG , "Screen Name " + path);
        // Get tracker.
        Tracker t = ((MyApplication) ((Activity)context).getApplication())
                .getTracker(TrackerName.APP_TRACKER);
        //Set screen name.
        //Where path is a String representing the screen name.
        t.setScreenName(path);
        //Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    /**
     * Convert the float into two digit after decimal
     * @param floatValue
     * @return
     */
    public static String getFloatWithTwoDigitAfterDecimalPoint(float floatValue){
        try {
            return String.format("%.2f", floatValue);
        }catch(Exception e){
            e.printStackTrace();
            return floatValue + "";
        }
    }


    /**
     * Show the first dialog for professional tutoring
     * @param context
     * @param msg
     */
    public static void showProfessionalTutoringPaidTutorDialog(final Context context ,
                                                               String msg){
        DialogGenerator dg = new DialogGenerator(context);
        dg.showPaidTutorProfessionalTutoringDialog(new TeacherFunctionDialogListener() {
            @Override
            public void clickOnClose(boolean isShow) {
                saveBooleanInSharedPreff(context ,
                        MathFriendzyHelper.IS_ALREADY_PAID_TUTOR_SHOW_DIALOG_KEY , isShow);
            }

            @Override
            public void clickOnWatchVedio(boolean isShow) {
                saveBooleanInSharedPreff(context ,
                        MathFriendzyHelper.IS_ALREADY_PAID_TUTOR_SHOW_DIALOG_KEY , isShow);
            }

            @Override
            public void clickOnDontShow(boolean isShow) {
                saveBooleanInSharedPreff(context ,
                        MathFriendzyHelper.IS_ALREADY_PAID_TUTOR_SHOW_DIALOG_KEY , isShow);
            }
        } , msg);
    }

    /**
     * Add purchase time for student
     * @param context
     * @param callback
     */
    public static void updatePurchaseTimeForStudent(final Context context,
                                                    AddPurchaseTimeParam param,
                                                    final PurchaseTimeCallback callback){
        if(CommonUtils.isInternetConnectionAvailable(context)) {
            new MyAsyckTask(ServerOperation.createPostRequestForPurchaseTime(param)
                    , null, ServerOperationUtil.UPDATE_PURCHASE_TIME_REQUEST, context,
                    new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase,
                                                   int requestCode) {
                            UpdatePurchaseTimeResponse response =
                                    (UpdatePurchaseTimeResponse) httpResponseBase;
                            if(response.getResult()
                                    .equalsIgnoreCase(MathFriendzyHelper.SUCCESS)){
                                callback.onPurchase(response);
                            }
                        }
                    }, ServerOperationUtil.SIMPLE_DIALOG, true,
                    context.getString(R.string.please_wait_dialog_msg))
                    .execute();
        }else{
            CommonUtils.showInternetDialog(context);
        }
    }

    /**
     * Add purchase time for student
     * @param action
     * @param context
     * @param userId
     * @param playerId
     * @param purchaseTime
     * @param callback
     */
    public static void updatePurchaseTimeForStudent(String action, final Context context,
                                                    String userId
            , String playerId, int purchaseTime, final PurchaseTimeCallback callback
            , boolean isShowDialog){
        if(CommonUtils.isInternetConnectionAvailable(context)) {
            AddPurchaseTimeParam param = new AddPurchaseTimeParam();
            param.setAction(action);
            param.setUserId(userId);
            param.setPlayerId(playerId);
            param.setPurchaseTime(purchaseTime);
            new MyAsyckTask(ServerOperation.createPostRequestForPurchaseTime(param)
                    , null, ServerOperationUtil.UPDATE_PURCHASE_TIME_REQUEST, context,
                    new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase,
                                                   int requestCode) {
                            UpdatePurchaseTimeResponse response =
                                    (UpdatePurchaseTimeResponse) httpResponseBase;
                            if(response.getResult()
                                    .equalsIgnoreCase(MathFriendzyHelper.SUCCESS)){
                                callback.onPurchase(response);
                            }
                        }
                    }, ServerOperationUtil.SIMPLE_DIALOG, isShowDialog,
                    context.getString(R.string.please_wait_dialog_msg))
                    .execute();
        }else{
            CommonUtils.showInternetDialog(context);
        }
    }


    /**
     * Send Email Using email intent
     * @param context
     * @param receiver
     * @param subject
     * @param body
     * @param createChooserMsh
     */
    public static void sendEmailUsingEmailChooser(Context context , String receiver ,
                                                  String subject , String body , String createChooserMsh){
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", receiver, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT,subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, body);
        context.startActivity(Intent.createChooser(emailIntent, createChooserMsh));
    }


    /**
     * Get purchase time for student
     * @param context
     * @param userId
     * @param playerId
     * @param responseInterface
     * @param isShowDialog
     */
    public static void getPlayerPurchaseTimeInfo(Context context , String userId ,
                                                 String playerId ,
                                                 HttpResponseInterface responseInterface ,
                                                 boolean isShowDialog){
        GetPlayerPurchaseTimeParam param = new GetPlayerPurchaseTimeParam();
        param.setAction("getTimePurchaseDetails");
        param.setpId(playerId);
        param.setuId(userId);
        new MyAsyckTask(ServerOperation
                .createPostRequestForGetPlayersPurchasedTime(param)
                , null, ServerOperationUtil.GET_PLAYER_PURCHASE_TIME_INFO, context,
                responseInterface, ServerOperationUtil.SIMPLE_DIALOG , isShowDialog ,
                context.getString(R.string.please_wait_dialog_msg))
                .execute();
    }

    public static boolean isShowFullScreenAds(){
        String lastTimeToShowAds = MathFriendzyHelper.
                getStringValueFromSharedPreff(MyApplication.getAppContext() ,
                        MathFriendzyHelper.LAST_TIME_TO_SHOW_FULL_SCREEN_ADS);
        if(lastTimeToShowAds != null){
            Date lastDate = MathFriendzyHelper.
                    getValidateDate(lastTimeToShowAds, "yyyy-MM-dd HH:mm:ss");
            DateObj dateObj = CommonUtils.getDateTimeDiff(new Date() , lastDate);
            if(dateObj.getDiffInDay() < 0){
                MathFriendzyHelper.saveStringInSharedPreff(MyApplication.getAppContext()
                        , MathFriendzyHelper.LAST_TIME_TO_SHOW_FULL_SCREEN_ADS ,
                        CommonUtils.formateDate(new Date()));
                return false;
            }
            long diffSeconds = MathFriendzyHelper.
                    getDateDiff(lastDate, new Date(), TimeUnit.SECONDS);
            int timeInterval = getTimeIntervalForFullScreenAds();
            if(timeInterval == 0)
                timeInterval = 86400;
            if(diffSeconds >= timeInterval){
                MathFriendzyHelper.saveStringInSharedPreff(MyApplication.getAppContext()
                        , MathFriendzyHelper.LAST_TIME_TO_SHOW_FULL_SCREEN_ADS ,
                        CommonUtils.formateDate(new Date()));
                return true;
            }
        }else {
            MathFriendzyHelper.saveStringInSharedPreff(MyApplication.getAppContext()
                    , MathFriendzyHelper.LAST_TIME_TO_SHOW_FULL_SCREEN_ADS ,
                    CommonUtils.formateDate(new Date()));
            return true;
        }
        return false;
    }

    private static int getTimeIntervalForFullScreenAds(){
        SharedPreferences sharedPreff = MyApplication.getAppContext().
                getSharedPreferences(ICommonUtils.ADS_FREQUENCIES_PREFF, 0);
        return sharedPreff.getInt(ICommonUtils.ADS_timeIntervalFullAd , 0);
    }

    public static String getStudentName(String firstName , String lastName){
        try{
            if(!MathFriendzyHelper.isEmpty(lastName)){
                return firstName + " " + lastName.charAt(0);
            }
            return firstName;
        }catch(Exception e){
            e.printStackTrace();
            return firstName + " " + lastName;
        }
    }

    public static void saveIsAdDisble(Context context , int value){
        SharedPreferences sharedPreff = context.getSharedPreferences
                (ICommonUtils.ADS_FREQUENCIES_PREFF, 0);
        SharedPreferences.Editor editor = sharedPreff.edit();
        editor.putInt(ICommonUtils.ADS_IS_ADS_DISABLE,value);
        editor.commit();
    }

    public static void checkForTempPlayerAndCreteNotExist(final Context context ,
                                                          final CheckTempPlayerListener listener){
        TempPlayerOperation tempObj2 = new TempPlayerOperation(context);
        if(tempObj2.isTemparyPlayerExist()){
            listener.onDone();
        }else{
            MathFriendzyHelper.getUserName(context , new GetTempPlayerUserName() {
                @Override
                public void onDone(String userName) {
                    if(userName != null) {
                        MathFriendzyHelper.createTempPlayer(userName, context,
                                new OnRequestComplete() {
                                    @Override
                                    public void onComplete() {
                                        MathFriendzyHelper.
                                                setTempPlayerAsCurrentPlayer(context);
                                        listener.onDone();
                                    }
                                });
                    }else{
                        CommonUtils.showInternetDialog(context);
                    }
                }
            });
        }
        tempObj2.closeConn();
    }

    private static void getUserName(final Context context ,
                                    final GetTempPlayerUserName listener) {
        new AsyncTask<Void, Void, String>() {
            private ProgressDialog pd = null;
            @Override
            protected void onPreExecute() {
                pd = ServerDialogs.getProgressDialog(context,
                        context.getString(R.string.please_wait_dialog_msg) , 1);
                pd.show();
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(Void... params) {
                String userName = "";
                String jsonString = CommonUtils.readFromURL(COMPLETE_URL
                        + "action=getTempUserName");
                if(jsonString != null){
                    try {
                        JSONObject jObject = new JSONObject(jsonString);
                        userName = jObject.getString("data");
                    }catch (JSONException e){
                        e.printStackTrace();
                    }
                    return userName;
                }else{
                    return null;
                }
            }

            @Override
            protected void onPostExecute(String result) {
                pd.cancel();
                listener.onDone(result);
                super.onPostExecute(result);
            }
        }.execute();
    }

    public static void setTempPlayerAsCurrentPlayer(Context context){
        SharedPreferences sharedPrefPlayerInfo = context.getSharedPreferences(PLAYER_INFO, 0);
        SharedPreferences.Editor editor =        sharedPrefPlayerInfo.edit();
        TempPlayerOperation tempPlayer1 = new TempPlayerOperation(context);
        ArrayList<TempPlayer> tempPlayerData = tempPlayer1.getTempPlayerData();
        editor.clear();
        editor.putString("playerName", tempPlayerData.get(0).getFirstName() + " " + tempPlayerData.get(0).getLastName());
        editor.putString("city", tempPlayerData.get(0).getCity());
        editor.putString("state", tempPlayerData.get(0).getState());
        editor.putString("imageName",  tempPlayerData.get(0).getProfileImageName());
        editor.putString("coins",  String.valueOf(tempPlayerData.get(0).getCoins()));
        editor.putInt("grade", tempPlayerData.get(0).getGrade());
        //for score update
        editor.putString("userId",  "0");
        editor.putString("playerId",  "0");
        editor.putString("countryName",tempPlayerData.get(0).getCountry());
        editor.putInt("completeLevel", tempPlayerData.get(0).getCompeteLevel());
        editor.commit();
    }

    public static boolean isTempPlayerDeleted(Context context){
        TempPlayerOperation tempPlayer = new TempPlayerOperation(context);
        boolean isTempPlayerDeleted = tempPlayer.isTempPlayerDeleted();
        return isTempPlayerDeleted;
    }

    /**
     * Return false is user player exist
     * other wise return true if not exist
     * @param context
     * @return
     */
    public static boolean isUserPlayerExist(Context context){
        UserPlayerOperation userPlayer = new UserPlayerOperation(context);
        boolean isTableExist = userPlayer.isUserPlayersExist();
        return !isTableExist;
    }

    public static void getSingleFriendzyDetail(final Context context ,
                                               final String userId , final String playerId ,
                                               final OnRequestComplete listener){
        new AsyncTask<Void, Void, PlayerDataFromServerObj>(){
            @Override
            protected void onPreExecute(){
                super.onPreExecute();
            }

            @Override
            protected PlayerDataFromServerObj doInBackground(Void... params){
                SingleFriendzyServerOperation singleServerObj = new SingleFriendzyServerOperation();
                PlayerDataFromServerObj playerDetaill = singleServerObj
                        .getSingleFriendzyDetail(userId, playerId);
                return playerDetaill;
            }

            @Override
            protected void onPostExecute(PlayerDataFromServerObj result){
                if(result != null){
                    String itemIds = "";
                    ArrayList<String> itemList = new ArrayList<String>();
                    if(result != null){//start if
                        if(result.getItems().length() > 0){
                            String newItemList = result.getItems() + ",";
                            for(int i = 0 ; i < newItemList.length() ; i ++ ){
                                if(newItemList.charAt(i) == ',' ){
                                    itemList.add(itemIds);
                                    itemIds = "";
                                }else{
                                    itemIds = itemIds +  newItemList.charAt(i);
                                }
                            }
                        }
                        ArrayList<PurchaseItemObj> purchaseItem = new ArrayList<PurchaseItemObj>();
                        for( int i = 0 ; i < itemList.size() ; i ++ ){
                            PurchaseItemObj purchseObj = new PurchaseItemObj();
                            purchseObj.setUserId(userId);
                            purchseObj.setItemId(Integer.parseInt(itemList.get(i)));
                            purchseObj.setStatus(1);
                            purchaseItem.add(purchseObj);
                        }
                        if(result.getLockStatus() != -1){
                            PurchaseItemObj purchseObj = new PurchaseItemObj();
                            purchseObj.setUserId(userId);
                            purchseObj.setItemId(100);
                            purchseObj.setStatus(result.getLockStatus());
                            purchaseItem.add(purchseObj);
                        }

                        LearningCenterimpl learningCenter = new LearningCenterimpl(context);
                        learningCenter.openConn();

                        if(learningCenter.isPlayerRecordExits(playerId)){
                            learningCenter.updatePlayerPointsAndCmpleteLevel(result.getPoints(),
                                    result.getCompleteLevel(), playerId);
                        }else{
                            PlayerTotalPointsObj playerTotalPoints = new PlayerTotalPointsObj();
                            playerTotalPoints.setUserId(userId);
                            playerTotalPoints.setPlayerId(playerId);
                            playerTotalPoints.setCoins(0);
                            playerTotalPoints.setCompleteLevel(result.getCompleteLevel());
                            playerTotalPoints.setPurchaseCoins(0);
                            playerTotalPoints.setTotalPoints(result.getPoints());
                            learningCenter.insertIntoPlayerTotalPoints(playerTotalPoints);
                        }
                        learningCenter.deleteFromPurchaseItem();
                        learningCenter.insertIntoPurchaseItem(purchaseItem);
                        learningCenter.closeConn();
                    }//end if
                    listener.onComplete();
                }else{
                    CommonUtils.showInternetDialog(context);
                }
                super.onPostExecute(result);
            }
        }.execute();
    }

    public static void getPointsLevelDetail(final Context context ,
                                            final String userId , final String playerId ,
                                            final OnRequestComplete listener){
        new AsyncTask<Void, Void, PlayerDataFromServerObj>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected PlayerDataFromServerObj doInBackground(Void... params) {
                SingleFriendzyServerOperation singleServerObj = new SingleFriendzyServerOperation();
                PlayerDataFromServerObj playerDetaill = singleServerObj
                        .getPointsLevelDetail(userId, playerId);
                return playerDetaill;
            }

            @Override
            protected void onPostExecute(PlayerDataFromServerObj result) {
                if (result != null) {//start if
                    LearningCenterimpl learningCenter = new LearningCenterimpl(context);
                    learningCenter.openConn();
                    ArrayList<PurchaseItemObj> purchaseItem = new ArrayList<PurchaseItemObj>();
                    if (result.getLockStatus() != -1) {
                        PurchaseItemObj purchseObj = new PurchaseItemObj();
                        purchseObj.setUserId(userId);
                        purchseObj.setItemId(100);
                        purchseObj.setStatus(result.getLockStatus());
                        purchaseItem.add(purchseObj);
                        learningCenter.deleteFromPurchaseItem(100);
                        learningCenter.insertIntoPurchaseItem(purchaseItem);
                    }

                    if (learningCenter.isPlayerRecordExits(playerId)) {
                        learningCenter.updatePlayerPointsAndCmpleteLevel(result.getPoints(), result.getCompleteLevel(), playerId);
                    } else {
                        PlayerTotalPointsObj playerTotalPoints = new PlayerTotalPointsObj();
                        playerTotalPoints.setUserId(userId);
                        playerTotalPoints.setPlayerId(playerId);
                        playerTotalPoints.setCoins(0);
                        playerTotalPoints.setCompleteLevel(result.getCompleteLevel());
                        playerTotalPoints.setPurchaseCoins(0);
                        playerTotalPoints.setTotalPoints(result.getPoints());
                        learningCenter.insertIntoPlayerTotalPoints(playerTotalPoints);
                    }

                    learningCenter.closeConn();
                    listener.onComplete();
                }//end if
                else {
                    CommonUtils.showInternetDialog(context);
                }
                super.onPostExecute(result);
            }
        }.execute();
    }

    public static boolean isLocalPlayerDataExist(Context context ,
                                                 String userId , String playerId){
        LearningCenterimpl impl = new LearningCenterimpl(context);
        impl.openConn();
        boolean isLoacalPlayerDataExist = impl.isPlayerDataExistInLocalPlayerLevels(
                userId, playerId);
        impl.closeConn();
        return isLoacalPlayerDataExist;
    }

    public static ArrayList<PlayerEquationLevelObj> getPlayerLevelEquationDataById(Context context ,
                                                                                   String playerId){
        LearningCenterimpl learnignCenter = new LearningCenterimpl(context);
        learnignCenter.openConn();
        ArrayList<PlayerEquationLevelObj> playerquationData = learnignCenter.
                getPlayerEquationLevelDataByPlayerId(playerId);
        learnignCenter.closeConn();
        return playerquationData;
    }

    public static void deleteFromLocalPlayerLevel(Context context , String userId ,
                                                  String playerId){
        LearningCenterimpl impl = new LearningCenterimpl(context);
        impl.openConn();
        impl.deleteFromLocalPlayerLevels(userId , playerId);
        impl.closeConn();
    }

    /**
     * This method check if data exist in this table or not
     * Corresponding to the user and playerId
     * @return
     */
    public static boolean isWordProblemDataExistForLearnignCenter(Context context ,
                                                                  String userId , String playerId){
        boolean isDataExist = false;
        SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(context);
        schoolImpl.openConnection();
        isDataExist = schoolImpl.isDataExistInLocalWordProblemLevels(userId, playerId);
        schoolImpl.closeConnection();
        return isDataExist;
    }


    public static void addAllWordProblemLevel(final Context context , String userId , String playerId ,
                                              final OnRequestComplete listener){
        class AddAllWordProblemLevel extends AsyncTask<Void, Void, Void>{
            private String levelXml;
            AddAllWordProblemLevel(String userId , String playerId){
                SchoolCurriculumLearnignCenterimpl schoolImpl =
                        new SchoolCurriculumLearnignCenterimpl(context);
                schoolImpl.openConnection();
                ArrayList<WordProblemLevelTransferObj> wordProblemLevelList
                        = schoolImpl.getWordProblemLevelData(userId, playerId);
                schoolImpl.closeConnection();
                ArrayList<PlayerEquationLevelObj> playerquationData =
                        new ArrayList<PlayerEquationLevelObj>();

                for(int i = 0 ; i < wordProblemLevelList.size() ; i ++ ){
                    PlayerEquationLevelObj playerData = new PlayerEquationLevelObj();
                    playerData.setEquationType(wordProblemLevelList.get(i).getCategoryId());
                    playerData.setLevel(wordProblemLevelList.get(i).getSubCategoryId());
                    playerData.setPlayerId(playerId);
                    playerData.setUserId(userId);
                    playerData.setStars(wordProblemLevelList.get(i).getStars());
                    playerquationData.add(playerData);
                }
                levelXml = getLevelXml(playerquationData);
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                LearnignCenterSchoolCurriculumServerOperation serverObj
                        = new LearnignCenterSchoolCurriculumServerOperation();
                serverObj.addAllWordProblemLevels("<levels>" + levelXml + "</levels>");
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                listener.onComplete();
            }
        }
        new AddAllWordProblemLevel(userId , playerId).execute();
    }

    public static void deleteFromLocalWordProblemLevel(Context context ,
                                                       String userId , String playerId){
        SchoolCurriculumLearnignCenterimpl schoolImpl =
                new SchoolCurriculumLearnignCenterimpl(context);
        schoolImpl.openConnection();
        schoolImpl.deleteFromLocalWordProblemLevels(userId, playerId);
        schoolImpl.closeConnection();
    }

    public static void getPlayerDataFromServer(final Context context , final String userId ,
                                               final String playerId ,
                                               final OnRequestComplete listener){
        new AsyncTask<Void, Void, PlayerDataFromServerObj>() {

            @Override
            protected void onPreExecute(){
                super.onPreExecute();
            }

            @Override
            protected PlayerDataFromServerObj doInBackground(Void... params){
                LearningCenteServerOperation learnignCenter = new LearningCenteServerOperation();
                PlayerDataFromServerObj playerDataFromServer = learnignCenter.
                        getPlayerDetailFromServer(userId, playerId);
                return playerDataFromServer;
            }

            @Override
            protected void onPostExecute(PlayerDataFromServerObj result){
                if(result != null){
                    try{
                        String itemIds = "";
                        ArrayList<String> itemList = new ArrayList<String>();
                        if(result != null){//start if
                            if(result.getItems().length() > 0)
                            {
                                //itemIds = result.getItems().replace(",", "");
                                //get item value which is comma separated , add (,) at the last for finish
                                String newItemList = result.getItems() + ",";

                                for(int i = 0 ; i < newItemList.length() ; i ++ )
                                {
                                    if(newItemList.charAt(i) == ',' )
                                    {
                                        itemList.add(itemIds);
                                        itemIds = "";
                                    }
                                    else
                                    {
                                        itemIds = itemIds +  newItemList.charAt(i);
                                    }
                                }
                            }

                            ArrayList<PurchaseItemObj> purchaseItem = new ArrayList<PurchaseItemObj>();


                            for( int i = 0 ; i < itemList.size() ; i ++ )
                            {
                                PurchaseItemObj purchseObj = new PurchaseItemObj();
                                purchseObj.setUserId(userId);
                                purchseObj.setItemId(Integer.parseInt(itemList.get(i)));
                                purchseObj.setStatus(1);
                                purchaseItem.add(purchseObj);
                            }

                            if(result.getLockStatus() != -1)
                            {
                                PurchaseItemObj purchseObj = new PurchaseItemObj();
                                purchseObj.setUserId(userId);
                                purchseObj.setItemId(100);
                                purchseObj.setStatus(result.getLockStatus());
                                purchaseItem.add(purchseObj);
                            }

                            LearningCenterimpl learningCenter = new LearningCenterimpl(context);
                            learningCenter.openConn();
                            learningCenter.deleteFromPlayerEruationLevel();
                            for( int i = 0 ; i < result.getPlayerLevelList().size() ; i ++ )
                                learningCenter.insertIntoPlayerEquationLevel(result.getPlayerLevelList().get(i));

                            if(learningCenter.isPlayerRecordExits(playerId))
                            {
                                learningCenter.updatePlayerPoints(result.getPoints(), playerId);
                            }
                            else
                            {
                                PlayerTotalPointsObj playerTotalPoints = new PlayerTotalPointsObj();
                                playerTotalPoints.setUserId(userId);
                                playerTotalPoints.setPlayerId(playerId);
                                playerTotalPoints.setCoins(0);
                                playerTotalPoints.setCompleteLevel(0);
                                playerTotalPoints.setPurchaseCoins(0);
                                playerTotalPoints.setTotalPoints(result.getPoints());
                                learningCenter.insertIntoPlayerTotalPoints(playerTotalPoints);
                            }

                            learningCenter.deleteFromPurchaseItem();
                            learningCenter.insertIntoPurchaseItem(purchaseItem);
                            learningCenter.closeConn();
                        }//end if
                    }catch (Exception e) {
                        Log.e(TAG, "inside PlayerDataFromServer onPost()" +
                                "Error while loading unlock categorieds from Internet " + e.toString());

                    }
                }
                listener.onComplete();
                //new GetWordProblemLevelDetail(userId, playerId , pd , MainActivity.this).execute(null,null,null);
                super.onPostExecute(result);
            }
        }.execute();
    }


    public static void getWordProblemLevelDetail(final Context context , final String userId ,
                                                 final String playerId , final OnRequestComplete listener){
        new AsyncTask<Void, Void, wordProblemLevelDetailTransferObj>(){
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected wordProblemLevelDetailTransferObj doInBackground(Void... params) {
                LearnignCenterSchoolCurriculumServerOperation learnignCenterServerOperation
                        = new LearnignCenterSchoolCurriculumServerOperation();
                wordProblemLevelDetailTransferObj wordProblemLevelDetail = learnignCenterServerOperation
                        .getWordProblemDetail(userId, playerId);
                //changes for UI hang when we do it onPostExecute()
                if(wordProblemLevelDetail != null){
                    SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl =
                            new SchoolCurriculumLearnignCenterimpl(context);
                    schooloCurriculumImpl.openConnection();
                    schooloCurriculumImpl.deleteFromWordProblemLevelStar();
                    schooloCurriculumImpl.insertIntoWordProblemLevelStar
                            (wordProblemLevelDetail.getLevelData());
                    schooloCurriculumImpl.closeConnection();
                }
                return wordProblemLevelDetail;
            }

            @Override
            protected void onPostExecute(wordProblemLevelDetailTransferObj wordProblemLevelDetail) {
                if(wordProblemLevelDetail != null){
                    MainActivity.worlProblemLevelDetail = wordProblemLevelDetail.getUpdatedData();
                    //added for assessment test details
                    MainActivity.testDeatilList = wordProblemLevelDetail.getTestDeatilList();
                    //end changes
                    listener.onComplete();
                }else{
                    CommonUtils.showInternetDialog(context);
                }
                super.onPostExecute(wordProblemLevelDetail);
            }
        }.execute();
    }

    public static int getNonZeroValue(int value){
        if(value == 0)
            return 1;
        return value;
    }

    public static void initializeIsSuscriptionPurchase(boolean bValue){
        isSubscriptionPurchase = bValue;
    }

    public static boolean isSubscriptionPurchase(){
        return isSubscriptionPurchase;
    }

    public static int getIntValueFromPreff(Context context , String key){
        return context.getSharedPreferences(MATH_FRENDZY_PREFF, 0).getInt(key, 0);
    }

    public static void saveIntegerValue(Context context ,
                                        String key , int value){
        SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
        SharedPreferences.Editor editor = sharedPreff.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static void saveBooleanValueInSharedPreff(Context context ,
                                                     String key , boolean value){
        SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
        SharedPreferences.Editor editor = sharedPreff.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static boolean getBooleanValueFromPreff(Context context , String key){
        return context.getSharedPreferences(MATH_FRENDZY_PREFF, 0)
                .getBoolean(key, false);
    }

    public static void sendFeedback(Context context ,
                                    final String fromEmail , final String feebback , final String appName){
        new AsyncTask<Void, Void, Void>(){

            @Override
            protected void onPreExecute() {

            }

            @Override
            protected Void doInBackground(Void... params) {
                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                try{
                    nameValuePairs.add(new BasicNameValuePair("action", "sendAppFeedback"));
                    nameValuePairs.add(new BasicNameValuePair("from", fromEmail));
                    nameValuePairs.add(new BasicNameValuePair("message", feebback));
                    nameValuePairs.add(new BasicNameValuePair("appName", appName));
                    CommonUtils.readFromURL(nameValuePairs);
                }catch(Exception e){
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {

            }
        }.execute();
    }


    //change for not to create temp player
    public static void openCreateTempPlayerActivity(Context context){
		/*Intent intent = new Intent(context,CreateTempPlayerActivity.class);
    	context.startActivity(intent);*/
        if(!MathFriendzyHelper.isUserLogin(context)){
            MathFriendzyHelper.showLoginRegistrationDialog(context);
        }
    }

    //change for not to create temp player
    public static void openCreateTempPlayerActivity(Context context , String message){
		/*Intent intent = new Intent(context,CreateTempPlayerActivity.class);
    	context.startActivity(intent);*/
        if(!MathFriendzyHelper.isUserLogin(context)){
            MathFriendzyHelper.showLoginRegistrationDialog(context , message);
        }
    }
    /**
     * Show login registration dialog
     *
     * @param context
     */
    public static void showLoginRegistrationDialog(Context context) {
        DialogGenerator dg = new DialogGenerator(context);
        Translation transeletion = new Translation(context);
        transeletion.openConnection();
        String alertMsgYouMustRegisterOrLogin = transeletion
                .getTranselationTextByTextIdentifier("alertMsgYouMustRegisterOrLogin");
        dg.generateRegisterOrLogInDialog(alertMsgYouMustRegisterOrLogin);
        transeletion.closeConnection();
    }


    public static String getAppName(Translation transeletion) {
        String appName = transeletion.
                getTranselationTextByTextIdentifier(ITextIds.MF_HOMESCREEN);
        return appName;
    }

    public static void downloadMoreApps(final Context context ,
                                        final MoreAppListener listener , final String appId){
        new AsyncTask<Void,Void,ArrayList<AppDetail>>(){
            private ProgressDialog progressDialog = null;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = CommonUtils.getProgressDialog(context);
                progressDialog.show();
            }

            @Override
            protected ArrayList<AppDetail> doInBackground(Void... params) {
                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                try{
                    nameValuePairs.add(new BasicNameValuePair("action", "getApps"));
                    nameValuePairs.add(new BasicNameValuePair("udid", ""));
                    nameValuePairs.add(new BasicNameValuePair("android", "1"));
                    return MoreAppFileParser.parseAppData(CommonUtils
                            .readFromURL(nameValuePairs), appId);
                }catch(Exception e){
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(ArrayList<AppDetail> appDetailList) {
                if(progressDialog != null
                        && progressDialog.isShowing())
                    progressDialog.cancel();
                listener.onSuccess(appDetailList);
                super.onPostExecute(appDetailList);
            }
        }.execute();
    }

    /**
     * Get display option
     * @return
     */
    public static DisplayImageOptions getDisplayOptionWithoutLoadingFail(){
        return new DisplayImageOptions.Builder()
                //.showImageOnLoading(R.drawable.resources)
                //.showImageForEmptyUri(R.drawable.resources)
                //.showImageOnFail(R.drawable.resources)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    /** Open another app.
     * @param context current Context, like Activity, App, or Service
     * @param packageName the full package name of the app to open
     * @return true if likely successful, false if unsuccessful
     */
    public static boolean openApp(Context context, String packageName) {
        PackageManager manager = context.getPackageManager();
        try {
            Intent i = manager.getLaunchIntentForPackage(packageName);
            if (i == null) {
                return false;
            }
            i.addCategory(Intent.CATEGORY_LAUNCHER);
            context.startActivity(i);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Return the installed application package list
     * @param context
     * @return
     */
    public static ArrayList<String> getInstalledAppInAndroid(Context context){
        final PackageManager pm = context.getPackageManager();
        ArrayList<String> pachageList = new ArrayList<String>();
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        for (ApplicationInfo packageInfo : packages) {
		    /*Log.d("TAG", "Installed package :" + packageInfo.packageName);
		    Log.d("TAG", "Source dir : " + packageInfo.sourceDir);
		    Log.d("TAG", "Launch Activity :" + pm.getLaunchIntentForPackage(packageInfo.packageName));*/
            pachageList.add(packageInfo.packageName);
        }
        return pachageList;
    }

    /**
     * This method return the user player data
     * @return
     */
    public static UserPlayerDto getPlayerData(Context context){
        try{
            return MathFriendzyHelper.getSelectedPlayerDataById
                    (context, MathFriendzyHelper.getSelectedPlayerID(context));
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public static boolean isStudentRegisterByTeacher(UserPlayerDto selectedPlayer){
        try{
            if(selectedPlayer != null
                    && !MathFriendzyHelper
                    .isEmpty(selectedPlayer.getStudentIdByTeacher())){
                if(MathFriendzyHelper
                        .parseInt(selectedPlayer.getStudentIdByTeacher()) > 0){
                    return true;
                }
            }
            return false;
        }catch(Exception e){
            return false;
        }
    }


    public static void saveStringValueToPreff(Context context ,
                                              String key , String value){
        SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
        SharedPreferences.Editor editor = sharedPreff.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getStringValueFromPreff(Context context , String key){
        return context.getSharedPreferences(MATH_FRENDZY_PREFF, 0).getString(key, null);
    }

    public static void openUrlWithoutHttps(Context context , String url){
        if(!url.contains("http")){
            url = "http://" + url;
        }
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        context.startActivity(intent);
    }

    public static String getHouseAdsImageName(Context context){
        String imageName = MathFriendzyHelper.getStringValueFromPreff(context,
                MathFriendzyHelper.HOUSE_ADS_IMAGE_NAME);
        if(MathFriendzyHelper.isEmpty(imageName))
            return DEFAULT_HOUSE_ADS_IMAGE_NAME;
        return imageName;
    }

    public static String getHouseAdsUrlLink(Context context){
        String link = MathFriendzyHelper.getStringValueFromPreff(context,
                MathFriendzyHelper.HOUSE_ADS_LINK_URL);
        if(MathFriendzyHelper.isEmpty(link))
            return DEFAULT_HOUSE_ADS_LINK_URL;
        return link;
    }

    /**
     * Get display option
     * @return
     */
    public static DisplayImageOptions getDisplayOptionWithoutCaching(){
        return new DisplayImageOptions.Builder()
                .showImageOnLoading(null)
                .showImageForEmptyUri(null)
                .showImageOnFail(null)
                .considerExifParams(true)
                        //.cacheInMemory(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }


    public static String getAppNameForNotificationBar(){
        String appName = MATH_FRIENDZY;
        if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_SIMPLE){
            appName = MATH_FRIENDZY;
        }else if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS){
            appName = MATH_PLUS;
        }else if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_TUTOR_PLUS){
            appName = MATH_TUTOR_PLUS;
        }

        /*if(AppVersion.BUILD_APP_TYPE == AppVersion.AMAZON){
            return "1st Grade Unlocked";
        }*/
        return appName;
    }

    public static void savePhraseList(Context context ,
                                      ArrayList<PhraseCatagory> list , boolean isTutor ){
        SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
        SharedPreferences.Editor editor = sharedPreff.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        if(isTutor) {
            editor.putString(PHRASE_LIST_SHARED_PREFF_KEY_FOR_TUTOR, json);
        }else{
            editor.putString(PHRASE_LIST_SHARED_PREFF_KEY_FOR_STUDENT, json);
        }
        editor.commit();
    }

    public static ArrayList<PhraseCatagory> getPhraseList(Context context , boolean isTutor){
        try {
            SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
            Gson gson = new Gson();
            String json = null;
            if(isTutor){
                json = sharedPreff.getString(PHRASE_LIST_SHARED_PREFF_KEY_FOR_TUTOR, "");
            }else{
                json = sharedPreff.getString(PHRASE_LIST_SHARED_PREFF_KEY_FOR_STUDENT, "");
            }
            if(MathFriendzyHelper.isEmpty(json)){
                return null;
            }
            Type type = new TypeToken<ArrayList<PhraseCatagory>>() {}.getType();
            ArrayList<PhraseCatagory> phraseList = gson.fromJson(json, type);
            return phraseList;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public static void getPhraseList(final Context context , final PhraseListListener listener
            , final boolean isTutor){
        ArrayList<PhraseCatagory> phraseCatList = MathFriendzyHelper.getPhraseList(context , isTutor);
        if(phraseCatList != null && phraseCatList.size() > 0
                && !isPhraseDateSaveDateOver(context , isTutor)){
            listener.onPhraseList(phraseCatList);
        }else{
            DownloadPhraseList(context , new PhraseListListener() {
                @Override
                public void onPhraseList(ArrayList<PhraseCatagory> phraseList) {
                    /*if(phraseList != null && phraseList.size() > 0) {
                        MathFriendzyHelper.savePhraseList(context, phraseList , isTutor);
                    }*/
                    listener.onPhraseList(MathFriendzyHelper.getPhraseList(context , isTutor));
                }
            } , isTutor);
        }
    }

    private static boolean isPhraseDateSaveDateOver(Context context , boolean isTutor){
        String lastPhraseSaveDate = null;
        if(isTutor){
            lastPhraseSaveDate = MathFriendzyHelper
                    .getStringValueFromPreff(context , PHRASE_LAST_SAVE_DATE_KEY_FOR_TUTOR);
        }else{
            lastPhraseSaveDate = MathFriendzyHelper
                    .getStringValueFromPreff(context , PHRASE_LAST_SAVE_DATE_KEY_FOR_STUDENT);
        }

        boolean isDateOver = false;
        if(!MathFriendzyHelper.isEmpty(lastPhraseSaveDate)){
            DateObj dateObj = CommonUtils.getDateTimeDiff(new Date() ,
                    MathFriendzyHelper.getValidateDate(lastPhraseSaveDate ,
                            "yyyy-MM-dd HH:mm:ss"));
            if(dateObj.getDiffInDay() >= 7){
                isDateOver = true;
            }else{
                isDateOver = false;
            }
        }else{
            isDateOver = true;
        }
        return isDateOver;
    }

    private static void DownloadPhraseList(final Context context ,
                                           final PhraseListListener listener ,
                                           final boolean isTutor){
        if(CommonUtils.isInternetConnectionAvailable(context)) {
            new AsyncTask<Void, Void, PhraseCatagory>() {
                private ProgressDialog progressDialog = null;

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    progressDialog = CommonUtils.getProgressDialog(context);
                    progressDialog.show();
                }

                @Override
                protected PhraseCatagory doInBackground(Void... params) {
                    ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                    try {
                        String date = null;
                        String action = null;
                        if(isTutor) {
                            action = "getTutorPhrasesWithSelection";
                            /*date = MathFriendzyHelper.getStringValueFromPreff(context,
                                    PHRASE_LIST_SAVE_DATE_KEY_FOR_TUTOR);*/
                        }else{
                            action = "getTutorPhrasesWithSelection";
                            /*date = MathFriendzyHelper.getStringValueFromPreff(context,
                                    PHRASE_LIST_SAVE_DATE_KEY_FOR_STUDENT);*/
                        }
                        if (date == null)
                            date = "";
                        nameValuePairs.add(new BasicNameValuePair("action", action));
                        nameValuePairs.add(new BasicNameValuePair("date", date));
                        return new FileParser().parsePhraseTutorResponse(CommonUtils
                                .readFromURL(nameValuePairs));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(PhraseCatagory phraseList) {
                    if (progressDialog != null
                            && progressDialog.isShowing())
                        progressDialog.cancel();
                    if (phraseList != null) {
                        if (phraseList.getResponse()
                                .equalsIgnoreCase(MathFriendzyHelper.SUCCESS)) {
                            if(phraseList.getCatList() != null && phraseList.getCatList().size() > 0) {
                                MathFriendzyHelper.saveStringValueToPreff(context,
                                        PHRASE_LIST_SAVE_DATE_KEY_FOR_TUTOR, phraseList.getDate());
                                MathFriendzyHelper.saveStringValueToPreff(context,
                                        PHRASE_LAST_SAVE_DATE_KEY_FOR_TUTOR,
                                        CommonUtils.formateDate(new Date()));
                                MathFriendzyHelper.savePhraseList(context, phraseList.getCatList() , true);
                            }
                            if(phraseList.getStudentCatList() != null && phraseList.getStudentCatList().size() > 0) {
                                MathFriendzyHelper.saveStringValueToPreff(context,
                                        PHRASE_LIST_SAVE_DATE_KEY_FOR_STUDENT, phraseList.getDate());
                                MathFriendzyHelper.saveStringValueToPreff(context,
                                        PHRASE_LAST_SAVE_DATE_KEY_FOR_STUDENT,
                                        CommonUtils.formateDate(new Date()));
                                MathFriendzyHelper.savePhraseList(context, phraseList.getStudentCatList() ,
                                        false);
                            }
                            listener.onPhraseList(phraseList.getCatList());
                        }else{
                            listener.onPhraseList(null);
                        }
                    }else{
                        listener.onPhraseList(null);
                    }
                    super.onPostExecute(phraseList);
                }
            }.execute();
        }else{
            listener.onPhraseList(null);
        }
    }

    public static void clickOnHelpAStudent(Context context){
        checkForStudentSelection(MathFriendzyHelper.DIALOG_SCHOOL_HELP_STUDENT , context);
    }

    /**
     * This method call when user click on HomeWork
     */
    public static void checkForStudentSelection(String checkFor , Context context){
        SharedPreferences sheredPreference = context.getSharedPreferences(LOGIN_SHARED_PREFF, 0);
        if(sheredPreference.getBoolean(IS_LOGIN, false))
        {
            UserPlayerOperation userPlayer = new UserPlayerOperation(context);
            if(!userPlayer.isUserPlayersExist())
            {
                SharedPreferences sheredPreferencePlayer = context.getSharedPreferences(IS_CHECKED_PREFF, 0);
                if(sheredPreferencePlayer.getString(PLAYER_ID, "").equals("")){
                    UserRegistrationOperation userObj = new UserRegistrationOperation(context);
                    if(userObj.getUserData().getIsParent().equals("0")){//0 for teacher
                        Intent intent = new Intent(context,TeacherPlayer.class);
                        context.startActivity(intent);
                    }
                    else{
                        Intent intent = new Intent(context,LoginUserPlayerActivity.class);
                        context.startActivity(intent);
                    }
                }
                else{
                    if(checkFor.equalsIgnoreCase(MathFriendzyHelper.DIALOG_SCHOOL_HELP_STUDENT)){
                        checkForTutorForHelpAStudent(context);
                    }else{
                        //this.checkForUnlock(checkFor);
                    }
                }
            }else{
                Intent intent = new Intent(context,LoginUserCreatePlayer.class);
                context.startActivity(intent);
            }
        }
        else{
            MathFriendzyHelper.showLoginRegistrationDialog(context , getTreanslationTextById(context ,
                    "lblYouMustRegisterLoginToAccess"));
        }
    }

    /**
     * Check for tutor
     */
    private static void checkForTutorForHelpAStudent(final Context context){
        final RegistereUserDto loginUser = CommonUtils.getLoginUser(context);
        final UserPlayerDto selectedPlayer = MathFriendzyHelper.getPlayerData(context);
        if(loginUser != null){
            if(MathFriendzyHelper.getUserAccountType(context) == TEACHER){
                goForHelpStudentScreen(context);
            }else{
                if(CommonUtils.isInternetConnectionAvailable(context)){
                    CheckForTutorParam param = new CheckForTutorParam();
                    param.setAction("checkIfStudnetATutor");
                    param.setUserId(selectedPlayer.getParentUserId());
                    param.setPlayerId(selectedPlayer.getPlayerid());
                    new MyAsyckTask(ServerOperation
                            .createPostRequestForCheckIsTutor(param)
                            , null, ServerOperationUtil.CHECK_FOR_TUTOR_REQUEST, context,
                            new HttpResponseInterface() {
                                @Override
                                public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                                    CheckForTutorResponse response = (CheckForTutorResponse) httpResponseBase;
                                    if(response.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)){
                                        MathFriendzyHelper.updateIsTutor(context , selectedPlayer ,
                                                response.getIsTutor());
                                        if(response.getIsTutor() == MathFriendzyHelper.YES){
                                            goForHelpStudentScreen(context);
                                        }else{
                                            MathFriendzyHelper.warningDialog(context , getTreanslationTextById(context ,
                                                    "studentNotATutorAlertTitle"));
                                        }
                                    }
                                }
                            }, ServerOperationUtil.SIMPLE_DIALOG , true ,
                            context.getString(R.string.please_wait_dialog_msg))
                            .execute();
                }else{
                    CommonUtils.showInternetDialog(context);
                }
            }
        }else{
            goForHelpStudentScreen(context);//never execute but for the rare case
        }
    }

    private static void goForHelpStudentScreen(Context context){
        context.startActivity(new Intent(context, ActHelpAStudent.class));
    }

    public static String getTreanslationTextById(Context context , String translationId){
        Translation transeletion = new Translation(context);
        transeletion.openConnection();
        String text = transeletion.getTranselationTextByTextIdentifier(translationId);
        transeletion.closeConnection();
        return text;
    }

    public static String[] getTreanslationTextById(Context context , String ...args){
        String[] textArray = new String[args.length];
        Translation transeletion = new Translation(context);
        transeletion.openConnection();
        for(int i = 0 ; i < args.length ; i ++ ) {
            textArray[i] =  transeletion.getTranselationTextByTextIdentifier(args[i]);
        }
        transeletion.closeConnection();
        return textArray;
    }

    public static String getCurrentActivityName(Context context){
        ActivityManager am = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
        String topActivityName =  am.getRunningTasks(1).get(0).topActivity.getClassName();
        return topActivityName;
    }

    public static String getAppPackageName(Context context) {
        try {
            //return MyApplication.getAppContext().getPackageName();
            return "com.mathfriendzy";
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * @param screenName - screen name with package name like - controller.main.MainActivity
     * @return
     */
    public static boolean isGivenScreenOpen(String screenName , Context context){
        boolean isOpen = false;
        String currentScreenName = getCurrentActivityName(context);
        String packageName = getAppPackageName(context);
        if(currentScreenName.equalsIgnoreCase(packageName + screenName)){
            isOpen = true;
        }
        return isOpen;
    }

    public static boolean isTab(Context context){
        return context.getResources().getBoolean(R.bool.isTablet);
    }

    /**
     * Upload the image from SD card to server
     *
     * @param context
     * @param fileName
     */
    public static void uploadImageOnserverFromSDCard
    (final Context context, final String fileName,
     final HttpServerRequest request , final int compressPercentage) {
        new AsyncTask<Void, Void, Void>() {

            private ProgressDialog pd = null;

            @Override
            protected void onPreExecute() {
                pd = CommonUtils.getProgressDialog(context);
                pd.show();
            }

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    UploadImage.executeMultipartPost(ImageOperation.getBitmapByFilePath
                                    (context, getQuestionImageUri(fileName)),
                            MathFriendzyHelper.getPNGFormateImageName(fileName), "",
                            new UploadImageRequest() {
                                @Override
                                public void onComplete(String response) {
                                    if (response != null) {
                                        if (CommonUtils.LOG_ON)
                                            Log.e(TAG, "Response " + response);
                                    }
                                }
                            }, ICommonUtils.UPLOAD_QUESTION_IMAGE_URL
                                    + "action=uploadQuestionImage" , compressPercentage ,
                            MathFriendzyHelper.isTab(context));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {

                if (pd != null && pd.isShowing()) {
                    pd.cancel();
                }

                request.onRequestComplete();
                super.onPostExecute(result);
            }
        }.execute();
    }

    public static void uploadBitmapImage(final Bitmap bitmap, final String fileName
            , final String uploadImageUrl, final UploadImageRequest callback ,
                                         final boolean isTab) {
        try {
            new AsyncTask<Void, Void, Void>() {
                private String serverResponse = null;

                @Override
                protected Void doInBackground(Void... params) {
                    try {
                        UploadImage.executeMultipartPost(bitmap,
                                MathFriendzyHelper.getPNGFormateImageName(fileName), "",
                                new UploadImageRequest() {
                                    @Override
                                    public void onComplete(String response) {
                                        serverResponse = response;
                                    }
                                }, uploadImageUrl ,
                                MathFriendzyHelper.COMPRESS_IMAGE_PERCENTAGE , isTab);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    callback.onComplete(serverResponse);
                }
            }.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static final String MODEL_FIRST_GEN = "Kindle Fire";
    public static final int GOOGLE_DEVICE = 1;
    public static final int AMAZON_DEVICE = 2;
    public static boolean isAmazonDevice(){
        if(android.os.Build.MANUFACTURER.equals("Amazon")){
            try {
                Class.forName("com.amazon.device.messaging.ADM");
                return true;
            } catch (ClassNotFoundException e) {
                return false;
            }
        }else{
            return false;
        }
    }

    public static boolean isFirstGenKindleFireDevice(){
        return android.os.Build.MODEL.equals(MODEL_FIRST_GEN);
    }

    public static boolean isADMSupportedForAmazonDevice(){
        return isAmazonDevice() && !isFirstGenKindleFireDevice();
    }

    public static void registerDeviceOnServer(Context context , String registrationId
            , int deviceType){
        if(deviceType == MathFriendzyHelper.AMAZON_DEVICE){
            registrationId = registrationId + "_amazon";
        }
        new RegisterDeviceOnServer(registrationId , context).execute(null,null,null);
    }

    public static ArrayList<WordProblemQuestionTransferObj> getWordProblemQuestions
            (Context context , int categoryId , int subCategoryId) {
        ArrayList<WordProblemQuestionTransferObj> questionAnswerList = null;
        if(CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH){
            SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(context);
            schoolImpl.openConnection();
            questionAnswerList = schoolImpl.getQuestionAnswer(categoryId, subCategoryId);
            schoolImpl.closeConnection();
        }else{
            SpanishChangesImpl schoolImpl = new SpanishChangesImpl(context);
            schoolImpl.openConn();
            questionAnswerList = schoolImpl.getQuestionAnswer(categoryId, subCategoryId);
            schoolImpl.closeConn();
        }
        return questionAnswerList;
    }

    //quick blox removal
    public static final String MESSAGE_PREFIX = "msg:";
    public static final String SERVER_PREFIX = "server:";
    public static final String PING = "ping";
    public static final String PONG = "pong";
    public static final String START = "start";
    public static final String STOP = "stop";
    public static final String AUDIO_PREFIX = "audio:";

    public static void getChatMessageForTutorRequest(Context context
            , GetMessagesForTutorRequestParam param , final ChatMessageCallback callback){
        new MyAsyckTask(ServerOperation
                .createPostRequestForGetMessagesForTutorRequest(param)
                , null, ServerOperationUtil.GET_CHAT_MESSAGES_REQUEST, context,
                new HttpResponseInterface() {
                    @Override
                    public void serverResponse(HttpResponseBase httpResponseBase,
                                               int requestCode) {
                        ChatMessageModel messageModel = (ChatMessageModel) httpResponseBase;
                        callback.onMessages(messageModel.getMessageList());
                    }
                }, ServerOperationUtil.SIMPLE_DIALOG , false ,
                context.getString(R.string.please_wait_dialog_msg))
                .execute();
    }

    public static String getDBPath(Context context){
        return "/data/data/" + MathFriendzyHelper.getPackageName(context) +"/databases/";
    }

    public static String getPackageName(Context context){
        return context.getPackageName();
    }

    public static String getBase64EncodedPublicKey(){
        if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_SIMPLE){
            return ICommonUtils.base64EncodedPublicKeyMathSimple;
        }else if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS){
            return ICommonUtils.base64EncodedPublicKeyMathPlus;
        }else if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_TUTOR_PLUS){
            return ICommonUtils.base64EncodedPublicKeyMathTutorPlus;
        }else{
            return ICommonUtils.base64EncodedPublicKeyMathSimple;
        }
    }

    /*public static void initializeCurrentAppVrsion(Context context){
        int currentAppVersion = MathFriendzyHelper.getIntValueFromPreff(context ,
                MathFriendzyHelper.CURRENT_APP_VERSION_KEY);
        if(currentAppVersion > 0){
            MathVersions.CURRENT_MATH_VERSION =  currentAppVersion;
            return ;
        }
        String currentAppPackageName = MathFriendzyHelper.getPackageName(context);
        if(currentAppPackageName.equalsIgnoreCase(MathFriendzyHelper.MATH_SIMPLE_PACKAGE_NAME)){
            MathVersions.CURRENT_MATH_VERSION = MathVersions.MATH_SIMPLE;
        }else if(currentAppPackageName.equalsIgnoreCase(MathFriendzyHelper.MATH_PLUS_PACKAGE_NAME)){
            MathVersions.CURRENT_MATH_VERSION = MathVersions.MATH_PLUS;
        }else if(currentAppPackageName.equalsIgnoreCase(MathFriendzyHelper.MATH_TUTOR_PLUS_PACKAGE_NAME)){
            MathVersions.CURRENT_MATH_VERSION = MathVersions.MATH_TUTOR_PLUS;
        }else{
            MathVersions.CURRENT_MATH_VERSION = MathVersions.MATH_SIMPLE;
        }

        MathFriendzyHelper.saveIntegerValue(context ,
                MathFriendzyHelper.CURRENT_APP_VERSION_KEY , MathVersions.CURRENT_MATH_VERSION);
    }*/

    public static String getAppRateUrl(Context context){
        return "https://play.google.com/store/apps/details?id="
                + MathFriendzyHelper.getPackageName(context);
    }


    public static int getRandomNumber(){
        return new Random().nextInt();
    }

    public static String getTimeForTutorServiceTime(Context context){
        //tz.getDisplayName()
        //tz.getDisplayName(false , TimeZone.SHORT)
        //tz.getDisplayName(false , TimeZone.LONG)
        //tz.getID()
        TimeZone tz = TimeZone.getDefault();
        try{
            String stringTimeZone = tz.getDisplayName(false, TimeZone.SHORT);
            stringTimeZone = stringTimeZone.replaceAll("GMT" , "").replace("gmt" , "");
            String timeZoneHours = stringTimeZone.split(":")[0];//like +05 in GMT+5:30
            int timeZoneMinutes = Integer.parseInt(stringTimeZone.split(":")[1]);
            //convert 0.5 into .5
            String finalTimeZoneMinutes = new DecimalFormat(".##")
                    .format(((float) timeZoneMinutes) / 60);
            return timeZoneHours + finalTimeZoneMinutes;
        }catch(Exception e){
            e.printStackTrace();
            return "-08.0";//default US time zone
        }
    }

    public static void openResourceScreen(Context context , ResourceSearchTermParam searchTerm){
        Intent intent = new Intent(context , ActResourceHome.class);
        intent.putExtra("searchParam" , searchTerm);
        context.startActivity(intent);
    }

    public static void getResourceCategories(final Context context ,
                                             final HttpResponseInterface responseInterface){
        ResourceCategory resourceCategory = getSavedResourceCategories(context);
        //String date = "";
        if(resourceCategory != null){
            DateObj dateObj = CommonUtils.getDateTimeDiff(new Date() ,
                    MathFriendzyHelper.getValidateDate(resourceCategory.getDate(),
                            "yyyy-MM-dd HH:mm:ss"));
            //date = resourceCategory.getDate();
            if(!(dateObj.getDiffInDay() >= 7)){
                responseInterface.serverResponse(resourceCategory ,
                        ServerOperationUtil.GET_RESOURCE_CATEGORIES_REQUEST);
                return ;
            }
        }

        if(CommonUtils.isInternetConnectionAvailable(context)){
            GetResourceCategoriesParam param = new GetResourceCategoriesParam();
            param.setAction("getGooruCategories");
            param.setDate("");//get the fresh record when date is blank
            new MyAsyckTask(ServerOperation.CreatePostRequestForGetResourcesCategories(param)
                    , null, ServerOperationUtil.GET_RESOURCE_CATEGORIES_REQUEST, context,
                    new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                            ResourceCategory resourceCategory = (ResourceCategory) httpResponseBase;
                            if(resourceCategory.getResult()
                                    .equalsIgnoreCase(MathFriendzyHelper.SUCCESS)) {
                                if(resourceCategory.getCategoriesList().size() > 0) {
                                    saveResourceCategories(context, resourceCategory);
                                }
                            }
                            responseInterface.serverResponse(httpResponseBase, requestCode);
                        }
                    }, ServerOperationUtil.SIMPLE_DIALOG, true,
                    context.getString(R.string.please_wait_dialog_msg))
                    .execute();
        }else{
            CommonUtils.showInternetDialog(context);
        }
    }

    private static final String RESOURCE_CATEGORIES_LIST = "com.mathfriendzy_resource_categories";
    public static void saveResourceCategories(Context context , ResourceCategory resources){
        SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
        SharedPreferences.Editor editor = sharedPreff.edit();
        Gson gson = new Gson();
        String json = gson.toJson(resources);
        editor.putString(RESOURCE_CATEGORIES_LIST, json);
        editor.commit();
    }

    public static ResourceCategory getSavedResourceCategories(Context context){
        try {
            SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
            Gson gson = new Gson();
            String json = null;
            json = sharedPreff.getString(RESOURCE_CATEGORIES_LIST, "");
            if (MathFriendzyHelper.isEmpty(json)) {
                return null;
            }
            Type type = new TypeToken<ResourceCategory>() {
            }.getType();
            ResourceCategory resources = gson.fromJson(json, type);
            return resources;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Get the Resource vedio In-App Status
     * @param context
     * @param userId
     */
    public static void getVideoInAppStatusSaveIntoSharedPreff(final Context context
            , String userId , final HttpResponseInterface responseInterface){
        if(CommonUtils.isInternetConnectionAvailable(context)) {
            GetResourceVideoInAppStatusParam param = new GetResourceVideoInAppStatusParam();
            param.setAction("getVideoInAppStatus");
            param.setUserId(userId);
            param.setAppId(CommonUtils.APP_ID);
            new MyAsyckTask(ServerOperation.CreatePostRequestForGetResourcesVedioInAppStatus(param)
                    , null, ServerOperationUtil.GET_RESOURCE_VIDEO_IN_APP_STATUS_REQUEST, context,
                    new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                            GetResourceVideoInAppResult response =
                                    (GetResourceVideoInAppResult) httpResponseBase;
                            if(response.getResult().equalsIgnoreCase
                                    (MathFriendzyHelper.SUCCESS)){
                                boolean status = false;
                                boolean unlockCategoryStatus = false;
                                if(response.getStatus() == MathFriendzyHelper.YES){
                                    status = true;
                                }
                                if(response.getUnlockCategoryStatus() == MathFriendzyHelper.YES){
                                    unlockCategoryStatus = true;
                                }
                                MathFriendzyHelper.saveBooleanInSharedPreff(context ,
                                        MathFriendzyHelper.RESOURCE_PURCHASE_STATUS_KEY , status);
                                MathFriendzyHelper.saveBooleanInSharedPreff(context ,
                                        MathFriendzyHelper.UNLOCK_CATEGORY_PURCHASE_STATUS_KEY ,
                                        unlockCategoryStatus);
                            }
                            responseInterface.serverResponse(httpResponseBase , requestCode);
                        }
                    }, ServerOperationUtil.SIMPLE_DIALOG, false,
                    context.getString(R.string.please_wait_dialog_msg))
                    .execute();
        }else{
            //CommonUtils.showInternetDialog(context);
        }
    }

    /**
     * Get the Resource vedio In-App Status
     * @param context
     * @param userId
     */
    public static void getVideoInAppStatusSaveIntoSharedPreff(final Context context
            , String userId , final HttpResponseInterface responseInterface , boolean isShowDialog){
        if(CommonUtils.isInternetConnectionAvailable(context)) {
            GetResourceVideoInAppStatusParam param = new GetResourceVideoInAppStatusParam();
            param.setAction("getVideoInAppStatus");
            param.setUserId(userId);
            param.setAppId(CommonUtils.APP_ID);
            new MyAsyckTask(ServerOperation.CreatePostRequestForGetResourcesVedioInAppStatus(param)
                    , null, ServerOperationUtil.GET_RESOURCE_VIDEO_IN_APP_STATUS_REQUEST, context,
                    new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                            GetResourceVideoInAppResult response =
                                    (GetResourceVideoInAppResult) httpResponseBase;
                            if(response.getResult().equalsIgnoreCase
                                    (MathFriendzyHelper.SUCCESS)){
                                boolean status = false;
                                boolean unlockCategoryStatus = false;
                                if(response.getStatus() == MathFriendzyHelper.YES){
                                    status = true;
                                }
                                if(response.getUnlockCategoryStatus() == MathFriendzyHelper.YES){
                                    unlockCategoryStatus = true;
                                }
                                MathFriendzyHelper.saveBooleanInSharedPreff(context ,
                                        MathFriendzyHelper.RESOURCE_PURCHASE_STATUS_KEY , status);
                                MathFriendzyHelper.saveBooleanInSharedPreff(context ,
                                        MathFriendzyHelper.UNLOCK_CATEGORY_PURCHASE_STATUS_KEY ,
                                        unlockCategoryStatus);
                            }
                            responseInterface.serverResponse(httpResponseBase , requestCode);
                        }
                    }, ServerOperationUtil.SIMPLE_DIALOG, isShowDialog,
                    context.getString(R.string.please_wait_dialog_msg))
                    .execute();
        }else{
            if(isShowDialog) {
                CommonUtils.showInternetDialog(context);
            }
        }
    }
    public static void saveVideoInAppStatus(final Context context, String userId ,
                                            final HttpResponseInterface responseInterface){
        MathFriendzyHelper.saveBooleanInSharedPreff(context ,
                MathFriendzyHelper.RESOURCE_PURCHASE_STATUS_KEY , true);
        if(CommonUtils.isInternetConnectionAvailable(context)) {
            GetResourceVideoInAppStatusParam param = new GetResourceVideoInAppStatusParam();
            param.setAction("saveVideoInAppStatus");
            param.setUserId(userId);
            param.setAppId(CommonUtils.APP_ID);
            new MyAsyckTask(ServerOperation.CreatePostRequestForGetResourcesVedioInAppStatus(param)
                    , null, ServerOperationUtil.SAVE_RESOURCE_VIDEO_IN_APP_STATUS_REQUEST, context,
                    new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                            responseInterface.serverResponse(httpResponseBase , requestCode);
                        }
                    }, ServerOperationUtil.SIMPLE_DIALOG, false,
                    context.getString(R.string.please_wait_dialog_msg))
                    .execute();
        }else{
            CommonUtils.showInternetDialog(context);
        }
    }


    public static void openInAppScreen(Context context ,
                                       int requestFor , int requestCode){
        if(CommonUtils.isInternetConnectionAvailable(context)) {
            Intent intent = new Intent(context, GetMoreCoins.class);
            intent.putExtra(GetMoreCoins.IN_APP_REQUEST, requestFor);
            ((Activity) context).startActivityForResult(intent, requestCode);
        }else{
            CommonUtils.showInternetDialog(context);
        }
    }
    public static final int SHOW_UNLOCK_FOR_LEARNING_CENTER = 1;
    public static final int SHOW_UNLOCK_FOR_SINGLE_FRIENDZY = 2;
    public static final int SHOW_UNLOCK_FOR_MULTI_FRIENDZY = 3;
    public static final int SHOW_PURCHASE_RESOURCE_VIDEO = 4;

    private static void openGetMoreCoinsScreenForUnlockCategory(Context context){
        MathFriendzyHelper.openInAppScreen(context,
                GetMoreCoins.UNLOCK_MATH_APP_CATEGORY_PURCHASED,
                GetMoreCoins.START_GET_MORE_COIN_ACTIVITY_REQUEST);
    }

    public static void updateCategoryPurchasedStatus(Context context
            , String userId
            ,final HttpResponseInterface responseInterface , int spentCoin){
        MathFriendzyHelper.saveBooleanInSharedPreff(context ,
                MathFriendzyHelper.UNLOCK_CATEGORY_PURCHASE_STATUS_KEY , true);
        MathFriendzyHelper.saveIsAdDisble(context , 1);
        if(CommonUtils.isInternetConnectionAvailable(context)) {
            GetResourceVideoInAppStatusParam param = new GetResourceVideoInAppStatusParam();
            param.setAction("saveUnlockCategoriesInAppStatus");
            param.setUserId(userId);
            param.setSpentCoins(spentCoin);
            param.setAppId(CommonUtils.APP_ID);
            new MyAsyckTask(ServerOperation.CreatePostRequestForGetResourcesVedioInAppStatus(param)
                    , null, ServerOperationUtil.SAVE_RESOURCE_VIDEO_IN_APP_STATUS_REQUEST, context,
                    new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                            responseInterface.serverResponse(httpResponseBase , requestCode);
                        }
                    }, ServerOperationUtil.SIMPLE_DIALOG, false,
                    context.getString(R.string.please_wait_dialog_msg))
                    .execute();
        }else{
            CommonUtils.showInternetDialog(context);
        }
    }

    public static boolean isNeedToGetPurchaseStatus(Context context){
        boolean isVideoPurchase = MathFriendzyHelper.getBooleanValueFromPreff(context ,
                MathFriendzyHelper.RESOURCE_PURCHASE_STATUS_KEY);
        boolean isUnlockCategory = MathFriendzyHelper.getBooleanValueFromPreff(context ,
                MathFriendzyHelper.UNLOCK_CATEGORY_PURCHASE_STATUS_KEY);
        if(!(isVideoPurchase && isUnlockCategory))
            return true;
        return false;
    }

    public static void showUnlockCategoryDialogWhenUserHaveMaxCoins(final Context context
            , final OnPurchaseDone purchaseListener
            , final GetAppUnlockStatusResponse appUnlockResponse){
        String[] textArray = MathFriendzyHelper.getTreanslationTextById(context ,
                "lblUnlockAllCategoriesWithCoins" ,
                "btnTitleNoThanks" , "lblUpgrade");
        MathFriendzyHelper.yesNoConfirmationDialog(context , textArray[0] ,
                textArray[2] , textArray[1] , new YesNoListenerInterface() {
                    @Override
                    public void onYes() {
                        MathFriendzyHelper.updateCategoryPurchasedStatus(context,
                                CommonUtils.getUserId(context), new HttpResponseInterface() {
                                    @Override
                                    public void serverResponse(HttpResponseBase httpResponseBase,
                                                               int requestCode) {

                                    }
                                }, MathFriendzyHelper.YES);
                        MathFriendzyHelper.updateUserCoins(context ,
                                CommonUtils.getUserId(context) , appUnlockResponse.getCoins()
                                        - MAX_USER_COINS_TO_UNLOCK_APP_CATEGORY ,
                                false , new HttpResponseInterface() {
                                    @Override
                                    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {

                                    }
                                });
                        purchaseListener.onPurchaseDone(true);
                    }

                    @Override
                    public void onNo() {
                        purchaseListener.onPurchaseDone(false);
                    }
                });
    }

    public static void getInAppStatusAndShowDialog(final Context context ,final int requestFrom ,
                                                   final HttpResponseInterface responseInterface ,
                                                   boolean ishowDialog ,
                                                   final GetAppUnlockStatusResponse appUnlockResponse ,
                                                   final OnPurchaseDone onPurchaseDone){
        if(!CommonUtils.isInternetConnectionAvailable(context)){
            if(ishowDialog) {
                CommonUtils.showInternetDialog(context);
            }
            responseInterface.serverResponse(null ,
                    MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE);
            return ;
        }

        if(MathFriendzyHelper.isUserLogin(context)){
            MathFriendzyHelper.getVideoInAppStatusSaveIntoSharedPreff(context ,
                    CommonUtils.getUserId(context) , new HttpResponseInterface() {
                        @Override
                        public void serverResponse(final HttpResponseBase httpResponseBase,
                                                   final int requestCode) {
                            if(requestFrom != SHOW_PURCHASE_RESOURCE_VIDEO){
                                if(MathFriendzyHelper
                                        .isAppUnlockCategoryPurchased(context)){
                                    responseInterface.serverResponse(httpResponseBase , requestCode);
                                }else{
                                    responseInterface.serverResponse(null ,
                                            MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE);
                                    if(appUnlockResponse.getCoins() >= MAX_USER_COINS_TO_UNLOCK_APP_CATEGORY){
                                        MathFriendzyHelper.showUnlockCategoryDialogWhenUserHaveMaxCoins(
                                                context , onPurchaseDone , appUnlockResponse);
                                    }else {
                                        MathFriendzyHelper.showUnlockMathCategoryDialog(context, requestFrom);
                                    }
                                }
                            }else if(requestFrom == SHOW_PURCHASE_RESOURCE_VIDEO){
                                if(MathFriendzyHelper
                                        .isAppUnlockOrResourcePurchased(context)){
                                    responseInterface.serverResponse(httpResponseBase , requestCode);
                                }else{
                                    responseInterface.serverResponse(null ,
                                            MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE);
                                }
                            }
                        }
                    } , ishowDialog);
        }else{
            if(requestFrom != SHOW_PURCHASE_RESOURCE_VIDEO) {
                responseInterface.serverResponse(null,
                        MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE);
                if(appUnlockResponse.getCoins() >= MAX_USER_COINS_TO_UNLOCK_APP_CATEGORY){
                    MathFriendzyHelper.showUnlockCategoryDialogWhenUserHaveMaxCoins(
                            context , onPurchaseDone , appUnlockResponse);
                }else {
                    MathFriendzyHelper.showUnlockMathCategoryDialog(context, requestFrom);
                }
            }else if(requestFrom == SHOW_PURCHASE_RESOURCE_VIDEO){
                responseInterface.serverResponse(null ,
                        MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE);
            }
        }
    }

    public static void getAppUnlockStatusFromServerAndSaveIntoLocal(final Context context
            , final String userId , boolean isShowDialog , final HttpResponseInterface responseInterface){
        if(CommonUtils.isInternetConnectionAvailable(context)) {
            GetAppUnlockStatusParam param = new GetAppUnlockStatusParam();
            param.setAction("getStatusOfSubscriptionForUser");
            param.setUserId(userId);
            new MyAsyckTask(ServerOperation.CreatePostRequestForGetAppUnlockStatus(param)
                    , null, ServerOperationUtil.GET_APP_UNLOCK_STATUS_REQUEST, context,
                    new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                            GetAppUnlockStatusResponse response =
                                    (GetAppUnlockStatusResponse) httpResponseBase;
                            if(response.getResult()
                                    .equalsIgnoreCase(MathFriendzyHelper.SUCCESS)
                                    && response.getAppUnlockStatus() != -1){
                                ArrayList<PurchaseItemObj> purchaseItem = new ArrayList<PurchaseItemObj>();
                                PurchaseItemObj purchseObj = new PurchaseItemObj();
                                purchseObj.setUserId(userId);
                                purchseObj.setItemId(MathFriendzyHelper.APP_UNLOCK_ID);
                                purchseObj.setStatus(response.getAppUnlockStatus());
                                purchaseItem.add(purchseObj);
                                LearningCenterimpl learningCenter = new LearningCenterimpl(context);
                                learningCenter.openConn();
                                learningCenter.deleteFromPurchaseItem(userId ,
                                        MathFriendzyHelper.APP_UNLOCK_ID);
                                learningCenter.insertIntoPurchaseItem(purchaseItem);
                                learningCenter.closeConn();
                            }
                            responseInterface.serverResponse(httpResponseBase , requestCode);
                        }
                    }, ServerOperationUtil.SIMPLE_DIALOG, isShowDialog,
                    context.getString(R.string.please_wait_dialog_msg))
                    .execute();
        }else{
            if(isShowDialog) {
                CommonUtils.showInternetDialog(context);
            }
            responseInterface.serverResponse(null ,
                    MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE);
        }
    }

    public static void openSubscriptionDialog(Context context , String openDialogFor){
        String[] textArray = MathFriendzyHelper.getTreanslationTextById(context ,
                "lblSubscriptionFeaturePopUp" , "lblHomeworkInfoPopUp");
        if(openDialogFor.equalsIgnoreCase(MathFriendzyHelper.DIALOG_SCHOOL_HOMEWORK_QUIZZ)){
            MathFriendzyHelper.warningDialogForLongMsg(context, textArray[1], new HttpServerRequest() {
                @Override
                public void onRequestComplete() {

                }
            });
        }else{
            MathFriendzyHelper.warningDialogForLongMsg(context, textArray[0], new HttpServerRequest() {
                @Override
                public void onRequestComplete() {

                }
            });
            //MathFriendzyHelper.showWarningDialogForSomeLongMessage(context, textArray[0]);
        }
    }

    public static boolean isNeedToDownloadAppUnlockStatus(Context context) {
        if(BuildApp.BUILD_APP_TYPE == BuildApp.AMAZON){
            return false;
        }
        String downloadDate = MathFriendzyHelper.getStringValueFromPreff(context,
                GET_APP_UNLOCK_STATUS_DOWNLOAD_KEY);
        if (MathFriendzyHelper.isEmpty(downloadDate)) {
            MathFriendzyHelper.saveStringValueToPreff(context,
                    GET_APP_UNLOCK_STATUS_DOWNLOAD_KEY,
                    MathFriendzyHelper.getCurrentDateInGiveGformateDate("yyyy-MM-dd HH:mm:ss"));
            return true;
        }

        DateObj dateObj = CommonUtils.getDateTimeDiff(new Date() ,
                MathFriendzyHelper.getValidateDate(downloadDate ,
                        "yyyy-MM-dd HH:mm:ss"));
        if(dateObj.getDiffInDay() >= 1){
            MathFriendzyHelper.saveStringValueToPreff(context,
                    GET_APP_UNLOCK_STATUS_DOWNLOAD_KEY,
                    MathFriendzyHelper.getCurrentDateInGiveGformateDate("yyyy-MM-dd HH:mm:ss"));
            return true;
        }else{
            return false;
        }
    }

    /**
     * Update Teacher credit on server for student answer
     * @param context
     * @param param
     * @param responseInterface
     * @param isShowDialog
     */
    public static void updateCreditByTeacherForStudentQuestion(Context context
            , UpdateCreditByTeacherForStudentQuestionParam param
            , final HttpResponseInterface responseInterface , boolean isShowDialog){
        new MyAsyckTask(ServerOperation.CreatePostRequestForUpdateTeacherCreditGivenToStudentAnswer(param)
                , null, ServerOperationUtil.UPDATE_TEACHER_CREDIT_FOR_STUDENT_ANSWER, context,
                new HttpResponseInterface() {
                    @Override
                    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                        responseInterface.serverResponse(httpResponseBase , requestCode);
                    }
                }, ServerOperationUtil.SIMPLE_DIALOG, isShowDialog,
                context.getString(R.string.please_wait_dialog_msg))
                .execute();
    }


    public static void updateUserCoins(Context context , String userId , int coins ,
                                       boolean isShowDialog ,
                                       final HttpResponseInterface responseInterface){
        UserRegistrationOperation userRegOpr = new UserRegistrationOperation(context);
        userRegOpr.updaUserCoins(coins, userId);
        userRegOpr.closeConn();

        UpdateUserCoinsParam param = new UpdateUserCoinsParam();
        param.setAction("saveCoinsForUser");
        param.setUserId(userId);
        param.setCoins(coins);

        if(CommonUtils.isInternetConnectionAvailable(context)) {
            new MyAsyckTask(ServerOperation.CreatePostRequestForUpdateUserCoins(param)
                    , null, ServerOperationUtil.UPDATE_USER_COINS_ON_SERVER, context,
                    new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                            responseInterface.serverResponse(httpResponseBase, requestCode);
                        }
                    }, ServerOperationUtil.SIMPLE_DIALOG, isShowDialog,
                    context.getString(R.string.please_wait_dialog_msg))
                    .execute();
        }
    }

    public static String getTextFromTextView(TextView txtView){
        try{
            return txtView.getText().toString();
        }catch(Exception e){
            return "";
        }
    }

    public static String getTextFromButton(Button btnView){
        try{
            return btnView.getText().toString();
        }catch(Exception e){
            return "";
        }
    }

    public static String getLastInitialName(String lastName){
        try{
            if(!MathFriendzyHelper.isEmpty(lastName)){
                return lastName.substring(0, 1);
            }
            return "";
        }catch(Exception e){
            return "";
        }
    }

    public static void setEditTextWatcherToEditTextForLastInitialName
            (final Context context , final EditText edtText){
        final String message = MathFriendzyHelper.getTreanslationTextById(context ,
                "lblOnlyLastInitial");
        edtText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = String.valueOf(s);
                if(text.length() > 1){
                    edtText.setText(MathFriendzyHelper.getLastInitialName(text));
                    MathFriendzyHelper.showWarningDialog(context , message);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public static String replaceDoubleQuotesBySingleQuote(String stringWithDoubleQuotes){
        try{
            if(stringWithDoubleQuotes.contains("''"))
                return stringWithDoubleQuotes.replaceAll("''" ,"'");
            return stringWithDoubleQuotes;
        }catch(Exception e){
            return "";
        }
    }

    public static void updateSchoolAllowPaidTutor(Context context
            , ShouldPaisTutorResponse response , UserPlayerDto selectedPlayer){
        UserPlayerOperation userPlayerOpr = new UserPlayerOperation(context);
        userPlayerOpr.updateAllowPaidTutor(selectedPlayer,
                MathFriendzyHelper.parseInt(response.getSchoolAllowPaidTutor()));
        userPlayerOpr.closeConn();
    }

    public static void getAnswerDetailForStudentHW(Context context
            , GetAnswerDetailForStudentHWParam param
            , final HttpResponseInterface responseInterface , boolean isShowDialog){
        if(CommonUtils.isInternetConnectionAvailable(context)) {
            new MyAsyckTask(ServerOperation.CreatePostRequestForStudentAnswerDetailForHW(param)
                    , null, ServerOperationUtil.GET_STUDENT_ANSWER_DETAIL_REQUEST, context,
                    new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                            responseInterface.serverResponse(httpResponseBase, requestCode);
                        }
                    }, ServerOperationUtil.SIMPLE_DIALOG, isShowDialog,
                    context.getString(R.string.please_wait_dialog_msg))
                    .execute();
        }
    }

    /**
     * Makes a substring of a string bold.
     * @param text          Full text
     * @param textToBold    Text you want to make bold
     * @return              String with bold substring
     */
    /*public static SpannableStringBuilder makeSectionOfTextBold(String text, String textToBold) {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        if (textToBold.length() > 0 && !textToBold.trim().equals("")) {
            //for counting start/end indexes
            String testText = text.toLowerCase(Locale.US);
            String testTextToBold = textToBold.toLowerCase(Locale.US);
            int startingIndex = testText.indexOf(testTextToBold);
            int endingIndex = startingIndex + testTextToBold.length();
            //for counting start/end indexes
            if (startingIndex < 0 || endingIndex < 0) {
                return builder.append(text);
            } else if (startingIndex >= 0 && endingIndex >= 0) {
                builder.append(text);
                builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
            }
        } else {
            return builder.append(text);
        }
        return builder;
    }*/

    public static final String SAVE_WORK_AREA_INPUT_FOR_OFFLINE_KEY = "com.mathfriendzy_work_area_input_list_key";

    /**
     * Save the work input and mark input status
     * @param context
     * @param param
     */
    public static void saveWorkAreaInputForOffline(Context context ,
                                                   UpdateInputStatusForWorkAreaParam param){
        ArrayList<UpdateInputStatusForWorkAreaParam> workInputList =
                getSaveWorkAreaInputForOffline(context);
        if(workInputList == null){
            workInputList = new ArrayList<UpdateInputStatusForWorkAreaParam>();
        }
        workInputList.add(param);
        SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
        SharedPreferences.Editor editor = sharedPreff.edit();
        Gson gson = new Gson();
        String json = gson.toJson(workInputList);
        editor.putString(SAVE_WORK_AREA_INPUT_FOR_OFFLINE_KEY, json);
        editor.commit();
    }

    public static void clearSavedWorkInputAndMarkInoutListInSharedPreff(Context context){
        SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
        SharedPreferences.Editor editor = sharedPreff.edit();
        editor.putString(SAVE_WORK_AREA_INPUT_FOR_OFFLINE_KEY, null);
        editor.commit();
    }

    public static ArrayList<UpdateInputStatusForWorkAreaParam>  getSaveWorkAreaInputForOffline
            (Context context){
        try {
            SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
            Gson gson = new Gson();
            String json = null;
            json = sharedPreff.getString(SAVE_WORK_AREA_INPUT_FOR_OFFLINE_KEY, "");
            if (MathFriendzyHelper.isEmpty(json)) {
                return null;
            }
            Type type = new TypeToken<ArrayList<UpdateInputStatusForWorkAreaParam>>() {
            }.getType();
            ArrayList<UpdateInputStatusForWorkAreaParam> list = gson.fromJson(json, type);
            return list;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }


    public static void updatedMarkInputAndWorkInputOnServer(final Context context){
        try {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected Void doInBackground(Void... params) {
                    ArrayList<UpdateInputStatusForWorkAreaParam> workAndMarkInputList
                            = getSaveWorkAreaInputForOffline(context);
                    if (workAndMarkInputList != null && workAndMarkInputList.size() > 0) {
                        for (int i = 0; i < workAndMarkInputList.size(); i++) {
                            String response = ServerOperation.readFromURL(ServerOperation
                                    .createPostRequestForUpdateInputStatusForWorkArea
                                            (workAndMarkInputList.get(i))
                                    , ServerOperation.getUrl(ServerOperationUtil.UPDATE_INPUT_STATUS_FOR_WORK_AREA));
                            if(response != null){
                                HttpResponseBase httpResponseBase = new FileParser()
                                        .ParseJsonString(response,
                                                ServerOperationUtil.UPDATE_INPUT_STATUS_FOR_WORK_AREA);
                            }
                        }
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    clearSavedWorkInputAndMarkInoutListInSharedPreff(context);
                    super.onPostExecute(aVoid);
                }
            }.execute();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static final String SAVE_USER_IN_PREFF_KEY = "com.math.saveuser.preff.key";
    public static void saveUserInSharedPreff(Context context, RegistereUserDto regUserObj) {
        if(regUserObj != null) {
            SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
            SharedPreferences.Editor editor = sharedPreff.edit();
            Gson gson = new Gson();
            String json = gson.toJson(regUserObj);
            editor.putString(SAVE_USER_IN_PREFF_KEY, json);
            editor.commit();
        }
    }

    public static RegistereUserDto getUserFromPreff(Context context){
        SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
        Gson gson = new Gson();
        String json = null;
        json = sharedPreff.getString(SAVE_USER_IN_PREFF_KEY, "");
        if (MathFriendzyHelper.isEmpty(json)) {
            return null;
        }
        Type type = new TypeToken<RegistereUserDto>() {
        }.getType();
        RegistereUserDto userDto = gson.fromJson(json, type);
        return userDto;
    }

    public static String getUserSchoolYear(Context context){
        try {
            if (MathFriendzyHelper.isUserLogin(context)) {
                return MathFriendzyHelper.getUserFromPreff(context).getSchoolYear();
            } else {
                return "";
            }
        }catch (Exception e){
            e.printStackTrace();
            return "";
        }
    }

    public static void getSchoolClasses(final Context context , final HttpResponseInterface responseInterface
            , boolean isShowDialog){
        try {
            if (CommonUtils.isInternetConnectionAvailable(context)) {
                final RegistereUserDto registereUserDto = MathFriendzyHelper.getUserFromPreff(context);
                GetSchoolClassesParam param = new GetSchoolClassesParam();
                //param.setAction("getSchoolClasses");
                param.setAction("getSchoolClassesForTeacher");
                param.setUserId(registereUserDto.getUserId());
                param.setSchoolYear(registereUserDto.getSchoolYear());
                new MyAsyckTask(ServerOperation
                        .createPostRequestForGetSchoolClasses(param)
                        , null, ServerOperationUtil.GET_SCHOOL_CLASSES_REQUEST, context,
                        new HttpResponseInterface() {
                            @Override
                            public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                                GetSchoolClassResponse response = (GetSchoolClassResponse) httpResponseBase;
                                if(response.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)){
                                    registereUserDto.setUserClasses(response.getUserClasseses());
                                    MathFriendzyHelper.saveUserInSharedPreff(context , registereUserDto);
                                }
                                responseInterface.serverResponse(httpResponseBase , requestCode);
                            }
                        }, ServerOperationUtil.SIMPLE_DIALOG, isShowDialog,
                        context.getString(R.string.please_wait_dialog_msg))
                        .execute();
            } else {
                CommonUtils.showInternetDialog(context);
                //responseInterface.serverResponse(null, MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE);
            }
        }catch (Exception e){
            e.printStackTrace();
            responseInterface.serverResponse(null, MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE);
        }
    }

    //not login user resource purchase change
    public static final String RESOURCE_PURCHASE_STATUS_KEY_FOR_DEVICE
            = "com.mathfriendzy.resource.purchase.key.for.device";
    public static final String UNLOCK_CATEGORY_PURCHASE_STATUS_KEY_FOR_DEVICE
            = "com.mathfriendzy.unlock_category.purchase.key.for.device";
    public static final int OPEN_LOGIN_SCREEN_REQUEST_CODE = 1000001;
    public static final int OPEN_REGISTRATION_SCREEN_REQUEST_CODE = 1000002;

    public static boolean isAppUnlockOrResourcePurchased(Context context){
        //Save this when user make purchase without register or login
        if(MathFriendzyHelper.getBooleanValueFromSharedPreff(context ,
                MathFriendzyHelper.RESOURCE_PURCHASE_STATUS_KEY_FOR_DEVICE)){
            return true;
        }

        if(MathFriendzyHelper.getAppUnlockStatus(APP_UNLOCK_ID ,
                CommonUtils.getUserId(context) , context)
                == MathFriendzyHelper.APP_UNLOCK){
            return true;
        }
        //save this value when user login
        return MathFriendzyHelper.getBooleanValueFromPreff(context ,
                MathFriendzyHelper.RESOURCE_PURCHASE_STATUS_KEY);
    }


    /**
     * Show login registration dialog
     *
     * @param context
     */
    public static void showLoginRegistrationDialog(Context context, String msg ,
                                                   LoginRegisterPopUpListener listener) {
        DialogGenerator dg = new DialogGenerator(context);
        dg.generateRegisterOrLogInDialog(msg , listener);
    }

    public static void openLogin(Context context , boolean isOpenForInAppPurchase , int requestCode){
        Intent intent = new Intent(context, LoginActivity.class);
        intent.putExtra("callingActivity", ((Activity)context).getClass().getSimpleName());
        intent.putExtra("isOpenForInAppPurchase", isOpenForInAppPurchase);
        ((Activity)context).startActivityForResult(intent, requestCode);
    }

    public static void openRegistration(Context context , boolean isOpenForInAppPurchase , int requestCode){
        Intent intent = new Intent(context, RegistrationStep1.class);
        intent.putExtra("isOpenForInAppPurchase", isOpenForInAppPurchase);
        ((Activity)context).startActivityForResult(intent , requestCode);
    }


    public static boolean isAppUnlockCategoryPurchased(Context context){
        //Save this when user make purchase without register or login
        if(MathFriendzyHelper.getBooleanValueFromSharedPreff(context ,
                MathFriendzyHelper.UNLOCK_CATEGORY_PURCHASE_STATUS_KEY_FOR_DEVICE)){
            return true;
        }
        //save this value when user login
        return MathFriendzyHelper.getBooleanValueFromPreff(context ,
                MathFriendzyHelper.UNLOCK_CATEGORY_PURCHASE_STATUS_KEY);
    }

    public static void showUnlockMathCategoryDialog(final Context context , int requestFrom){
        final String[] textArray = MathFriendzyHelper.getTreanslationTextById(context , "lblUnlockAllCategories" ,
                "btnTitleNoThanks" , "btnTitleOK" , "lblLimitedTimeOffer" ,
                "lblUpgradeUnlockCategory" , "lblUpgradeMathCategoryOnly" , "lblWouldLikeToLoginBeforePurchase");
        YesNoDialogMessagesForInAppPopUp message = new YesNoDialogMessagesForInAppPopUp();
        message.setMainText(textArray[4]);
        message.setLimitedTimeOffer(textArray[3]);
        message.setUpgradeText(textArray[5]);
        MathFriendzyHelper.yesNoConfirmationDialogForLongMessageForUnlockInAppPopUp(context, textArray[0]
                , textArray[2], textArray[1], new YesNoListenerInterface() {
            @Override
            public void onYes() {
                if (!MathFriendzyHelper.isUserLogin(context)) {
                    //MathFriendzyHelper.showLoginRegistrationDialog(context);
                    MathFriendzyHelper.showLoginRegistrationDialog(context
                            , textArray[6] , new LoginRegisterPopUpListener() {
                        @Override
                        public void onRegister() {
                            MathFriendzyHelper.openRegistration(context , true ,
                                    MathFriendzyHelper.OPEN_REGISTRATION_SCREEN_REQUEST_CODE);
                        }

                        @Override
                        public void onLogin() {
                            MathFriendzyHelper.openLogin(context , true ,
                                    MathFriendzyHelper.OPEN_LOGIN_SCREEN_REQUEST_CODE);
                        }

                        @Override
                        public void onNoThanks() {
                            openGetMoreCoinsScreenForUnlockCategory(context);
                        }
                    });
                    return;
                }
                openGetMoreCoinsScreenForUnlockCategory(context);
            }

            @Override
            public void onNo() {
                //nothing to do
            }
        } , message);
    }

    public static int getWebViewTextSizeForPaypalPayment(Context context){
        try {
            return (int)context.getResources().getDimension(R.dimen.web_view_txtSize);
        }catch(Exception e){
            e.printStackTrace();
            return -1;
        }
    }


    public static final int MATH_APP_ID = 6;
    public static final int HW_APP_ID = 32;
    public static final int MTP_APP_ID = 33;
    public static final String APP_LIVE_VERSION_DATE_KEY = "save.app.live.version.date";
    public static int HOME_SCREEN_VISIT_COUNTER = 0;

    public static void getLiveAppVersionAndCheckForUpdate(final Context context , boolean isDialogShow){
        GetAppVersionResponse appData = MathFriendzyHelper.getLiveAppVersionData(context);
        if(appData != null && !MathFriendzyHelper.isLiveAppDateSaveOver(context)){
            MathFriendzyHelper.HOME_SCREEN_VISIT_COUNTER ++ ;
            if(MathFriendzyHelper.HOME_SCREEN_VISIT_COUNTER >= appData.getFrequency()){
                MathFriendzyHelper.HOME_SCREEN_VISIT_COUNTER = 0;
                checkForUpdateVersion(context , appData);
            }
            return ;
        }

        if(CommonUtils.isInternetConnectionAvailable(context)) {
            GetAppVersionParam param = new GetAppVersionParam();
            param.setAction("getAppUpdateVersion");
            if (MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_SIMPLE) {
                param.setAppId(MATH_APP_ID);
            } else if (MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS) {
                param.setAppId(HW_APP_ID);
            } else if (MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_TUTOR_PLUS) {
                param.setAppId(MTP_APP_ID);
            }
            new MyAsyckTask(ServerOperation
                    .createPostRequestForGetLiveAppVersion(param)
                    , null, ServerOperationUtil.GET_LIVE_APP_VERSION, context,
                    new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                            GetAppVersionResponse response = (GetAppVersionResponse) httpResponseBase;
                            if (response.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)) {
                                MathFriendzyHelper.saveLiveAppVersionInPreff(context, response);
                                MathFriendzyHelper.saveStringValueToPreff(context,
                                        MathFriendzyHelper.APP_LIVE_VERSION_DATE_KEY, MathFriendzyHelper
                                                .getCurrentDateInGiveGformateDate("yyyy-MM-dd HH:mm:ss"));
                                checkForUpdateVersion(context , response);
                            }
                        }
                    }, ServerOperationUtil.SIMPLE_DIALOG, isDialogShow,
                    context.getString(R.string.please_wait_dialog_msg))
                    .execute();
        }
    }

    public static void checkForUpdateVersion(final Context context , GetAppVersionResponse appData){
        String currentVersion = MathFriendzyHelper.getAppVersion(context);
        if(!appData.getAndroidVersion().equalsIgnoreCase(currentVersion)){
            MathFriendzyHelper.showWarningDialog(context ,
                    MathFriendzyHelper.getTreanslationTextById(context ,
                            "lblWeHaveMadeChangeForAndroid") , new HttpServerRequest() {
                        @Override
                        public void onRequestComplete() {
                            context.startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse(MathFriendzyHelper.getAppRateUrl(context))));
                        }
                    });
        }
    }

    public static final String LIVE_APP_VERSION_KEY = "com.math.live.app.version.key";
    public static void saveLiveAppVersionInPreff(Context context, GetAppVersionResponse response) {
        if(response != null) {
            SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
            SharedPreferences.Editor editor = sharedPreff.edit();
            Gson gson = new Gson();
            String json = gson.toJson(response);
            editor.putString(LIVE_APP_VERSION_KEY, json);
            editor.commit();
        }
    }

    public static GetAppVersionResponse getLiveAppVersionData(Context context){
        SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
        Gson gson = new Gson();
        String json = null;
        json = sharedPreff.getString(LIVE_APP_VERSION_KEY, "");
        if (MathFriendzyHelper.isEmpty(json)) {
            return null;
        }
        Type type = new TypeToken<GetAppVersionResponse>() {
        }.getType();
        GetAppVersionResponse appData = gson.fromJson(json, type);
        return appData;
    }

    private static boolean isLiveAppDateSaveOver(Context context){
        String liveDataSaveLastDate = null;
        liveDataSaveLastDate = MathFriendzyHelper
                .getStringValueFromPreff(context , MathFriendzyHelper.APP_LIVE_VERSION_DATE_KEY);
        boolean isDateOver = false;
        if(!MathFriendzyHelper.isEmpty(liveDataSaveLastDate)){
            DateObj dateObj = CommonUtils.getDateTimeDiff(new Date() ,
                    MathFriendzyHelper.getValidateDate(liveDataSaveLastDate ,
                            "yyyy-MM-dd HH:mm:ss"));
            if(dateObj.getDiffInDay() >= 1){
                isDateOver = true;
            }else{
                isDateOver = false;
            }
        }else{
            isDateOver = true;
        }
        return isDateOver;
    }

    public static UserPlayerDto getSelectedPlayer(Context context){
        if(MathFriendzyHelper.isUserLogin(context)){
            return MathFriendzyHelper.getPlayerData(context);
        }
        return null;
    }

    public static void disconnectFromGlobalChatRoom(){
        try {
            if (MyGlobalWebSocket.getInstance() != null) {
                MyGlobalWebSocket.getInstance().disconnect();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public static GetActiveInActiveStatusOfStudentForTutorResponse getActiveInActiveResponse(String jsonStringFromRoom){
        try{
            if (MathFriendzyHelper.isEmpty(jsonStringFromRoom))
                return null;
            jsonStringFromRoom = jsonStringFromRoom.replaceAll(MathFriendzyHelper.SERVER_PREFIX , "");
            CommonUtils.printLog(TAG , "jsonStringFromRoom " + jsonStringFromRoom);
            GetActiveInActiveStatusOfStudentForTutorResponse response =
                    new GetActiveInActiveStatusOfStudentForTutorResponse();
            ArrayList<GetActiveInActiveStatusOfStudentForTutorResponse> list
                    = new ArrayList<GetActiveInActiveStatusOfStudentForTutorResponse>();
            JSONObject jsonObject = new JSONObject(jsonStringFromRoom);
            if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject , "id")){
                String id = jsonObject.getString("id");
                if(id.equalsIgnoreCase(MyGlobalWebSocket.GET_ONLINE_USER_STATUS)){
                    if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject , "users")){
                        try {//try catch added because users may be array or json
                            JSONObject jsonUser = jsonObject.getJSONObject("users");
                            Iterator<?> iterator = jsonUser.keys();
                            while (iterator.hasNext()) {
                                GetActiveInActiveStatusOfStudentForTutorResponse userStatus
                                        = new GetActiveInActiveStatusOfStudentForTutorResponse();
                                String key = (String) iterator.next();
                                userStatus.setPlayerId(key);
                                JSONArray onlineReqId = jsonUser.getJSONArray(key);
                                if (onlineReqId.length() > 0) {
                                    for(int  i = 0 ; i < onlineReqId.length() ; i ++ ) {
                                        String requestId = onlineReqId
                                                .getString(i).replaceAll("/room/", "");
                                        if(requestId.contains("global"))//If user in global room then leave it
                                            continue;
                                        userStatus.setOnlineRequestId
                                                (MathFriendzyHelper.parseInt(requestId));
                                        break;
                                    }
                                }
                                list.add(userStatus);
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        response.setList(list);
                        return response;
                    }
                }
            }
            return null;
        }catch (JSONException e){
            e.printStackTrace();
            return null;
        }
    }


    /**
     * Get Chat Request Data
     * @param chatRequestId
     * @param context
     * @param responseInterface
     * @param isDialogShow
     */
    public static void getChatRequestData(int chatRequestId
            , Context context , HttpResponseInterface responseInterface , boolean isDialogShow){
        if(CommonUtils.isInternetConnectionAvailable(context)) {
            GetChatRequestParam param = new GetChatRequestParam();
            param.setAction("getChatRequest");
            param.setChatRequestId(chatRequestId);
            new MyAsyckTask(ServerOperation.createPostTOGetChatRequest(param)
                    , null, ServerOperationUtil.GET_CHAT_REQUEST_SERVER_REQUEST,
                    context, responseInterface, ServerOperationUtil.SIMPLE_DIALOG, isDialogShow,
                    context.getString(R.string.please_wait_dialog_msg))
                    .execute();
        }
    }

    public static final double MAX_IMAGE_BYTE_SIZE_IN_MB = 8.0;
    public static final double MAX_PDF_FILE_SIZE_IN_MB = 8.0;
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    public static double getMaxImageByteSizeInMb(Bitmap data) {
        double byteSize = 0;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
            byteSize =  data.getRowBytes() * data.getHeight();
        } else {
            byteSize =  data.getByteCount();
        }
        return Math.ceil(byteSize / (1024.0 * 1024.0));
    }

    public static double getPDFFileSizeInMB(Context context , String pdfFileName){
        double fileSize = 0;
        if(pdfFileName != null && pdfFileName.length() > 0){
            String pdfName = PDFOperation.getPdfFormattedFileName(pdfFileName);
            String filePath = PDFOperation.getPDFSavePath(context);
            File file = new File(filePath, pdfName);
            if(file.exists()){
                fileSize = Math.ceil(file.length() / (1024.0 * 1024.0));
            }
        }
        return fileSize;
    }

    public static int getStringLength(String string){
        try{
            if(MathFriendzyHelper.isEmpty(string))
                return 0;
            return string.length();
        }catch (Exception e){
            return 0;
        }
    }

    /**
     * Check for work expire date
     *
     * @param dueDate
     * @return
     */
    public static boolean isExpireHomwWork(String dueDate) {
        Date dueWorkDate = MathFriendzyHelper.getValidateDataForHomeWork(dueDate);
        if (dueWorkDate.compareTo(MathFriendzyHelper.getCurrentDateForHomwWork()) >= 0) {
            return false;
        } else {
            return true;
        }
    }

    //hide the dialog
    public static void hideDialog(Dialog dialog){
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public static final int MATH_SUBJECT_ID = 1;
    public static final String SAVE_LAST_CLASS_SELECTED_KEY = "com.mathfriendzy.last.class.selected";

    /**
     * Show the class selection dialog
     * @param context
     * @param classes
     * @param listener
     */
    public static void showClassSelectionDialog(final Context context , ArrayList<ClassWithName> classes ,
                                                final ClassSelectedListener listener ,
                                                SelectAssignHomeworkClassParam param){
        final Dialog dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.class_title_list_assign_homework_layout);
        dialog.show();

        final Button btnClassListDone = (Button) dialog.findViewById(R.id.btnClassListDone);
        ListView lstClassesList = (ListView) dialog.findViewById(R.id.lstClassesList);

        //add header view
        View view = LayoutInflater.from(context).inflate(R.layout.class_title_assign_homework_item , null);
        LinearLayout rlClassTitleItemLayout = (LinearLayout) view.findViewById(R.id.rlClassTitleItemLayout);
        rlClassTitleItemLayout.setBackgroundColor(context.getResources().getColor(R.color.LIGHT_GREEN));
        RelativeLayout rlStartDate = (RelativeLayout) view.findViewById(R.id.rlStartDate);
        TextView txtStartDate = (TextView) view.findViewById(R.id.txtStartDate);
        TextView txtSubject = (TextView) view.findViewById(R.id.txtSubject);
        TextView txtClass = (TextView) view.findViewById(R.id.txtClassName);
        String[] texts = MathFriendzyHelper.getTreanslationTextById(context , "lblStart" , "mfLblDate" ,
                "lblSubjectHeader" , "lblClass");
        txtStartDate.setText(texts[0] + " " + texts[1]);
        txtSubject.setText(texts[2]);
        txtClass.setText(texts[3]);

        if(param.isShowForPrevSaveHomework()){
            rlStartDate.setVisibility(RelativeLayout.GONE);
            btnClassListDone.setVisibility(Button.GONE);
        }

        lstClassesList.addHeaderView(view);
        //end add header view

        if(classes != null && classes.size() > 0) {
            final ClassAdapterForNewAssignHW adapter = new ClassAdapterForNewAssignHW(context, classes , param ,
                    new AdapterListenerForNewAssignHW() {
                        @Override
                        public void onSingleSelectionDone(ClassWithName selectedClass) {
                            hideDialog(dialog);
                            MathFriendzyHelper.saveLastClassSelected(context, selectedClass);
                            ArrayList<ClassWithName> selectedClassList = new ArrayList<ClassWithName>();
                            selectedClassList.add(selectedClass);
                            listener.onClassSelectionDone(selectedClassList);
                        }
                    });
            lstClassesList.setAdapter(adapter);

            btnClassListDone.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideDialog(dialog);
                    listener.onClassSelectionDone(adapter.getSelectedList());
                    listener.onUpdatedClasses(adapter.getUpdateClassesList());
                }
            });
        }else{
            btnClassListDone.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideDialog(dialog);
                }
            });
        }
    }

    /**
     * Return the last selected
     * @param context
     * @return
     */
    public static ClassWithName getLastSelectedClass(Context context ,
                                                     ArrayList<ClassWithName> classWithNames){
        SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
        Gson gson = new Gson();
        String json = null;
        json = sharedPreff.getString(SAVE_LAST_CLASS_SELECTED_KEY, "");
        if (MathFriendzyHelper.isEmpty(json)) {
            return classWithNames.get(0);
        }
        Type type = new TypeToken<ClassWithName>() {
        }.getType();
        ClassWithName selectedClass = gson.fromJson(json, type);
        if(selectedClass == null){
            return classWithNames.get(0);
        }
        return selectedClass;
    }

    /**
     * Save the last selected class
     * @param context
     * @param selectedClass - selected class is null when user logout
     */
    public static void saveLastClassSelected(Context context , ClassWithName selectedClass){
        SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
        SharedPreferences.Editor editor = sharedPreff.edit();
        Gson gson = new Gson();
        String json = gson.toJson(selectedClass);
        editor.putString(SAVE_LAST_CLASS_SELECTED_KEY, json);
        editor.commit();
    }

    //save homework changes
    public static final String HOME_WORK_SAVE_KEY = "com.saved.homeworks";

    /**
     * Return the index of saved homework in the list
     * @param context
     * @param homeworkData
     * @return
     */
    public static String saveHomework(Context context , HomeworkData homeworkData){
        ArrayList<HomeworkData> homeworkList = getSavedHomework(context , homeworkData.getTeacherId());
        if(homeworkList == null){
            homeworkList = new ArrayList<HomeworkData>();
        }
        homeworkList.add(homeworkData);

        SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
        SharedPreferences.Editor editor = sharedPreff.edit();
        Gson gson = new Gson();
        String json = gson.toJson(homeworkList);
        editor.putString(HOME_WORK_SAVE_KEY + homeworkData.getTeacherId(), json);
        editor.commit();
        return homeworkList.size() + "_" + homeworkData.getTeacherId();
    }

    public static ArrayList<HomeworkData> getSavedHomework(Context context , String userId){
        try {
            SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
            Gson gson = new Gson();
            String json = sharedPreff.getString(HOME_WORK_SAVE_KEY + userId, "");
            if(MathFriendzyHelper.isEmpty(json)){
                return null;
            }
            Type type = new TypeToken<ArrayList<HomeworkData>>() {}.getType();
            ArrayList<HomeworkData> homeworkList = gson.fromJson(json, type);
            return homeworkList;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    //end save homework changes


    public static void getStudentForSchoolClasses(Context context , GetStudentForSchoolClassesParam param
            , HttpResponseInterface responseInterface){
        if(!CommonUtils.isInternetConnectionAvailable(context)){
            if(param.isDialogShow()){
                CommonUtils.showInternetDialog(context);
            }
        }else{
            new MyAsyckTask(ServerOperation.createPostRequestForGetStudentForSchoolClasses(param)
                    , null, ServerOperationUtil.GET_STUDENTS_BY_GRADE_REQUEST, context,
                    responseInterface, ServerOperationUtil.SIMPLE_DIALOG , param.isDialogShow() ,
                    /*context.getString(R.string.please_wait_dialog_msg)*/ "")
                    .execute();
        }
    }

    /**
     * Save homewrk on server
     * @param context
     * @param param
     * @param responseInterface
     */
    public static void saveMathHomework(Context context , AssignHomeworkParam param
            , HttpResponseInterface responseInterface){
        SaveHomeworkParam saveHomeworkParam = new SaveHomeworkParam();
        saveHomeworkParam.setAction("saveMathHomework");
        saveHomeworkParam.setUserId(param.getUserId());
        saveHomeworkParam.setData(JsonCreator.getSavedHomeworkJson(param));
        saveHomeworkParam.setHwId(param.getHwId());
        saveHomeworkParam.setClasses(param.getSelectedClasses());
        /*CommonUtils.printLog(TAG , "userId " + saveHomeworkParam.getUserId() + " data = " + saveHomeworkParam.getData()
          + " hwId " + saveHomeworkParam.getHwId());*/

        if(!CommonUtils.isInternetConnectionAvailable(context)){
            if(param.isShowDialog()){
                CommonUtils.showInternetDialog(context);
            }
        }else{
            new MyAsyckTask(ServerOperation.createPostRequestForSaveHomework(saveHomeworkParam)
                    , null, ServerOperationUtil.SAVE_HOMEWORK_ON_SERVER_REQUEST, context,
                    responseInterface, ServerOperationUtil.SIMPLE_DIALOG , param.isShowDialog() ,
                    context.getString(R.string.please_wait_dialog_msg))
                    .execute();
        }
    }


    /**
     * This method return the list of grade for which categories are downloaded
     */
    public static ArrayList<Integer> getGradeListForWhichCategoriesDownloaded(Context context) {
        ArrayList<Integer> gradeList = new ArrayList<Integer>();
        //changes for Spanish
        if (CommonUtils.getUserLanguageCode(context) == CommonUtils.ENGLISH) {
            SchoolCurriculumLearnignCenterimpl schoolImpl =
                    new SchoolCurriculumLearnignCenterimpl(context);
            schoolImpl.openConnection();
            gradeList = schoolImpl.getGradeListForWhichCategoriesDownloaded();
            schoolImpl.closeConnection();
        } else {
            SpanishChangesImpl schoolImpl = new SpanishChangesImpl(context);
            schoolImpl.openConn();
            gradeList = schoolImpl.getGradeListForWhichCategoriesDownloaded();
            schoolImpl.closeConn();
        }
        return gradeList;
    }

    /**
     * Return the all grade list for which we need to download categories
     * @return
     */
    public static ArrayList<Integer> getAllGradeList(){
        ArrayList<Integer> gradeList = new ArrayList<Integer>();
        for(int i = 1; i <= 8 ; i ++ ){
            gradeList.add(i);
        }
        return gradeList;
    }

    public static void downloadAllGradeWordCategories(final Context context ,
                                                      final HttpResponseInterface responseInterface,
                                                      final boolean isShowDialog){
        ArrayList<Integer> gradeListForDownloadedCategories = getGradeListForWhichCategoriesDownloaded(context);
        ArrayList<Integer> allGradeList = getAllGradeList();
        allGradeList.removeAll(gradeListForDownloadedCategories);
        if(!(allGradeList.size() > 0)){
            responseInterface.serverResponse(null,MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE);
            return ;
        }

        if(!CommonUtils.isInternetConnectionAvailable(context)){
            if(isShowDialog){
                CommonUtils.showInternetDialog(context);
            }
        }else{
            final ProgressDialog pd = MathFriendzyHelper.getProgressDialog(context);
            if(isShowDialog)
                MathFriendzyHelper.showProgressDialog(pd);
            DownloadCategoriesForGradesParam param = new DownloadCategoriesForGradesParam();
            param.setAction("getWordCategoriesForGrades");
            param.setGrades(MathFriendzyHelper.getCommaSeparatedStringForIntegerList(allGradeList, ","));
            param.setLang(CommonUtils.getUserLanguageCode(context));
            new MyAsyckTask(ServerOperation.createPostRequestForDownloadCategoriesForGrades(param)
                    , null, ServerOperationUtil.GET_WORD_CATEGORIES_FOR_GRADEDS_REQUEST, context,
                    new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                            final DownloadCategoriesForGradesResponse response =
                                    (DownloadCategoriesForGradesResponse) httpResponseBase;
                            new AsyncTask<Void,Void,Void>(){
                                @Override
                                protected Void doInBackground(Void... params) {
                                    if(response.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)){
                                        insertCategoriesDataIntoTable(context , response.getCategoriesForGradesResponses());
                                    }
                                    return null;
                                }

                                @Override
                                protected void onPostExecute(Void aVoid) {
                                    super.onPostExecute(aVoid);
                                    if(isShowDialog)
                                        MathFriendzyHelper.hideDialog(pd);
                                    responseInterface.serverResponse(null,
                                            MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE);
                                }
                            }.execute();
                        }
                    }, ServerOperationUtil.SIMPLE_DIALOG , false ,
                    context.getString(R.string.please_wait_dialog_msg))
                    .execute();
        }
    }

    public static void downloadAllGradeWordCategories(final Context context ,
                                                      final HttpResponseInterface responseInterface,
                                                      final boolean isShowDialog ,
                                                      ArrayList<Integer> allGradeList){
        ArrayList<Integer> gradeListForDownloadedCategories = getGradeListForWhichCategoriesDownloaded(context);
        //ArrayList<Integer> allGradeList = getAllGradeList();
        /*CommonUtils.printLog("All Grade List before remove " + allGradeList
                + " downloaded grades " + gradeListForDownloadedCategories);*/
        allGradeList.removeAll(gradeListForDownloadedCategories);
        //CommonUtils.printLog("All Grade List After remove " + allGradeList);
        if(!(allGradeList.size() > 0)) {
            responseInterface.serverResponse(null, MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE);
            return;
        }

        if(!CommonUtils.isInternetConnectionAvailable(context)){
            if(isShowDialog){
                CommonUtils.showInternetDialog(context);
            }
        }else{
            final ProgressDialog pd = MathFriendzyHelper.getProgressDialog(context);
            if(isShowDialog)
                MathFriendzyHelper.showProgressDialog(pd);
            DownloadCategoriesForGradesParam param = new DownloadCategoriesForGradesParam();
            param.setAction("getWordCategoriesForGrades");
            param.setGrades(MathFriendzyHelper.getCommaSeparatedStringForIntegerList(allGradeList, ","));
            param.setLang(CommonUtils.getUserLanguageCode(context));
            new MyAsyckTask(ServerOperation.createPostRequestForDownloadCategoriesForGrades(param)
                    , null, ServerOperationUtil.GET_WORD_CATEGORIES_FOR_GRADEDS_REQUEST, context,
                    new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                            final DownloadCategoriesForGradesResponse response =
                                    (DownloadCategoriesForGradesResponse) httpResponseBase;
                            new AsyncTask<Void,Void,Void>(){
                                @Override
                                protected Void doInBackground(Void... params) {
                                    if(response.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)){
                                        insertCategoriesDataIntoTable(context , response.getCategoriesForGradesResponses());
                                    }
                                    return null;
                                }

                                @Override
                                protected void onPostExecute(Void aVoid) {
                                    super.onPostExecute(aVoid);
                                    if(isShowDialog)
                                        MathFriendzyHelper.hideDialog(pd);
                                    responseInterface.serverResponse(null,
                                            MathFriendzyHelper.NO_RESPONSE_FROM_SERVER_REQUEST_CODE);
                                }
                            }.execute();
                        }
                    }, ServerOperationUtil.SIMPLE_DIALOG , false ,
                    context.getString(R.string.please_wait_dialog_msg))
                    .execute();
        }
    }

    private static void insertCategoriesDataIntoTable(Context context ,
                                                      ArrayList<DownloadCategoriesForGradesResponse> downloadedCateories){
        if (CommonUtils.getUserLanguageCode(context)  == CommonUtils.ENGLISH) {
            SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl =
                    new SchoolCurriculumLearnignCenterimpl(context);
            schooloCurriculumImpl.openConnection();
            for(int i = 0 ; i < downloadedCateories.size() ; i ++ ){
                DownloadCategoriesForGradesResponse downloadedCateory = downloadedCateories.get(i);
                ArrayList<CategoryListTransferObj> categories = downloadedCateory.getCategories();
                int grade = downloadedCateory.getGrade();
                schooloCurriculumImpl.deleteFromWordProblemCategoriesbyGrade(grade);
                schooloCurriculumImpl.deleteFromWordProblemsSubCategoriesByCategoryId(categories);
                schooloCurriculumImpl.insertIntoWordProblemCategories(categories, grade);
                schooloCurriculumImpl.insertIntoWordProblemsSubCategories(categories);
            }
            schooloCurriculumImpl.closeConnection();
        } else {
            SpanishChangesImpl implObj = new SpanishChangesImpl(context);
            implObj.openConn();
            for(int i = 0 ; i < downloadedCateories.size() ; i ++ ){
                DownloadCategoriesForGradesResponse downloadedCateory = downloadedCateories.get(i);
                ArrayList<CategoryListTransferObj> categories = downloadedCateory.getCategories();
                int grade = downloadedCateory.getGrade();
                implObj.deleteFromWordProblemCategoriesbyGrade(grade);
                implObj.deleteFromWordProblemsSubCategoriesByCategoryId(categories);
                implObj.insertIntoWordProblemCategories(categories, grade);
                implObj.insertIntoWordProblemsSubCategories(categories);
            }
            implObj.closeConn();
        }
    }

    //For lead the city from server for selected country
    private static final String CITY_KEY = "com.mathfriendzy.cities";
    public static void saveCitiesForTheSelectedCountry(Context context ,
                                                       ArrayList<String> cities , String country){
        SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
        SharedPreferences.Editor editor = sharedPreff.edit();
        Gson gson = new Gson();
        String json = gson.toJson(cities);
        editor.putString(CITY_KEY + "_" + country, json);
        editor.commit();
    }

    public static ArrayList<String> getCityForSelectedCountry(Context context , String country){
        try {
            SharedPreferences sharedPreff = context.getSharedPreferences(MATH_FRENDZY_PREFF, 0);
            Gson gson = new Gson();
            String json = sharedPreff.getString(CITY_KEY + "_" + country, "");
            if(MathFriendzyHelper.isEmpty(json)){
                return null;
            }
            Type type = new TypeToken<ArrayList<String>>() {}.getType();
            ArrayList<String> cityList = gson.fromJson(json, type);
            return cityList;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public static void downloadCitiesForTheSelectedCountry(final Context context
            , final String selectedCountry , final DownloadCitiesForSelectedCountryListener listener){
        ArrayList<String> cities = getCityForSelectedCountry(context , selectedCountry);
        if(cities != null && cities.size() > 0){
            listener.onCities(cities);
            return ;
        }

        if(CommonUtils.isInternetConnectionAvailable(context)){
            String countyIOS = MathFriendzyHelper.getCountryIsoByCountryName(selectedCountry , context);
            GetCountryCityParam param = new GetCountryCityParam();
            param.setAction("getCities");
            param.setCountryISO(countyIOS);
            new MyAsyckTask(ServerOperation.createPostRequestForGetCities(param)
                    , null, ServerOperationUtil.GET_CITIES_FROM_SERVER_REQUEST, context,
                    new HttpResponseInterface() {
                        @Override
                        public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
                            CountryCityResponse response = (CountryCityResponse) httpResponseBase;
                            if(response != null){
                                if(response.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)){
                                    saveCitiesForTheSelectedCountry(context ,
                                            response.getCities() , selectedCountry);
                                    listener.onCities(response.getCities());
                                }
                            }
                        }
                    }, ServerOperationUtil.SIMPLE_DIALOG ,true,
                    context.getString(R.string.please_wait_dialog_msg))
                    .execute();

        }else{
            CommonUtils.showInternetDialog(context);
            listener.onCities(null);
        }
    }

    /**
     * Check for two list are equal or not
     * @param a
     * @param b
     * @return
     */
    public static boolean isEqualLists(List<String> a, List<String> b){
        try {
            CommonUtils.printLog("a.size() " + a.size() + " b.size() " + b.size() );
            if(a.size() == 0 && b.size() == 0) return true;

            // Check for sizes and nulls
            if ((a.size() != b.size()) || (a == null && b != null) || (a != null && b == null)) {
                return false;
            }

            if (a == null && b == null) return true;

            // Sort and compare the two lists
            Collections.sort(a);
            Collections.sort(b);
            return a.equals(b);
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Disable Enable Controls
     * @param enable
     * @param vg
     */
    public static void enableDisableControls(boolean enable, ViewGroup vg){
        for (int i = 0; i < vg.getChildCount(); i++){
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup){
                enableDisableControls(enable, (ViewGroup)child);
            }
        }
    }

    /**
     * Get Resource Link From khan video
     * @param context
     * @param link
     * @param responseInterface
     */
    public static void getResourceUrlFromLink(Context context , String link ,
                                              HttpResponseInterface responseInterface){
        if(CommonUtils.isInternetConnectionAvailable(context)){
            GetKhanVideoLinkParam param = new GetKhanVideoLinkParam();
            param.setUrl(link);
            new MyAsyckTask(ServerOperation
                    .createPostRequestForGetKhanVideoLink(param)
                    , null, ServerOperationUtil.GET_KHAN_VIDEO_LINK, context,
                    responseInterface, ServerOperationUtil.SIMPLE_DIALOG , true ,
                    context.getString(R.string.please_wait_dialog_msg))
                    .execute();
        }else{
            CommonUtils.showInternetDialog(context);
        }
    }

    /**
     * Return the updated url for math plus
     * @param vedioUrl
     * @return
     */
    private static String updateMathPlusUrl(String vedioUrl) {
        if(vedioUrl.equalsIgnoreCase(VedioUrls.SCHOOL_HOMEWROK_QUIZZ_VEDIO)){
            return VedioUrls.SCHOOL_HOMEWROK_QUIZZ_VEDIO_SF;
        }else if(vedioUrl.equalsIgnoreCase(VedioUrls.TEACHER_ASSIGN_HW_QUIZZ_VEDIO)){
            return VedioUrls.TEACHER_ASSIGN_HW_QUIZZ_VEDIO_SF;
        }else if(vedioUrl.equalsIgnoreCase(VedioUrls.TEACHER_CHECK_HW_QUIZZ_VEDIO)){
            return VedioUrls.TEACHER_CHECK_HW_QUIZZ_VEDIO_SF;
        }else if(vedioUrl.equalsIgnoreCase(VedioUrls.TEACHER_STUDENT_ACCOUNT_VEDIO)){
            return VedioUrls.TEACHER_STUDENT_ACCOUNT_VEDIO_SF;
        }else if(vedioUrl.equalsIgnoreCase(VedioUrls.TEACHER_MANAGE_TUTOR_VEDIO)){
            return VedioUrls.TEACHER_MANAGE_TUTOR_VEDIO_SF;
        }
        return vedioUrl;
    }

    private static final String SELECTED_RESOURCE_LANG = "com.math.selected.resource.lang";
    public static void saveResorceSelectedLanguage(Context context , int selectedLang){
        MathFriendzyHelper.saveIntegerValue(context , SELECTED_RESOURCE_LANG , selectedLang);
    }

    public static int getResourceSelectedLanguage(Context context){
        return MathFriendzyHelper.getIntValueFromPreff(context , SELECTED_RESOURCE_LANG);
    }

    public static int getAppIdForRequest(){
        try{
            return MyApplication.getAppContext().getResources().getInteger(R.integer.app_id);
        }catch (Exception e){
            e.printStackTrace();
            return 6;
        }
    }

    /**
     * Return the json links array
     * @param links
     * @return
     */
    public static String getLinkString(ArrayList<AddUrlToWorkArea> links){
        JSONArray jsonArray = new JSONArray();
        try {
            if (links != null && links.size() > 0) {
                if(links.size() == 1){//if size is one and both title and link empty
                    AddUrlToWorkArea url = links.get(0);
                    if(MathFriendzyHelper.isEmpty(url.getTitle())
                            && MathFriendzyHelper.isEmpty(url.getUrl())){
                        return jsonArray.toString();
                    }
                }

                for (int i = 0; i < links.size(); i++) {
                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("title", links.get(i).getTitle());
                    jsonObj.put("url", links.get(i).getUrl());
                    jsonArray.put(jsonObj);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return jsonArray.toString();
    }

    //CSV file changes
    public static String STUDENT_CSV_FILE_NAME = "math_report.csv";
    public static void createStudentsCSVFile(String csvFileName
            , final OnRequestComplete onCompeteListener
            , final ArrayList<GetDetailOfHomeworkWithCustomeResponse> studentList
            , final TeacherDetailToExport teacherDetailToExport) throws IOException  {

        File folder = new File(getMathFilePath() + "/CSV");
        if (!folder.exists())
            folder.mkdir();
        final String filename = folder.toString() + "/" + csvFileName;
        new AsyncTask<Void, Void , Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    createStudentsCSVFile(filename, studentList, teacherDetailToExport);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                onCompeteListener.onComplete();
                super.onPostExecute(aVoid);
            }
        }.execute();
    }

    //append the data into csv file
    public static void createStudentsCSVFile(String fileName
            , ArrayList<GetDetailOfHomeworkWithCustomeResponse> studentList
            , TeacherDetailToExport teacherDetailToExport) throws IOException {
        FileWriter fw = new FileWriter(fileName);
        fw.append("Teacher's Name").append(",").append(teacherDetailToExport.getTeacherName()).append("\n");
        fw.append("Subject").append(",").append(teacherDetailToExport.getSubjects()).append("\n");
        fw.append("Class Title").append(",").append(teacherDetailToExport.getClasses()).append("\n");
        fw.append("Due Date").append(",").append(teacherDetailToExport.getDueDate()).append("\n\n");
        fw.append("First Name").append(",").append("Last Name");
        if(teacherDetailToExport.isPractice())
            fw.append(",").append("Practice Skills");
        if(teacherDetailToExport.isWordProblem())
            fw.append(",").append("Word Problems");
        if(teacherDetailToExport.isCustom())
            fw.append(",").append("Assignments");
        fw.append(",").append("Average Score").append("\n");
        if(studentList != null && studentList.size() > 0) {
            for (int i = 0; i < studentList.size(); i++) {
                GetDetailOfHomeworkWithCustomeResponse student = studentList.get(i);
                fw.append(student.getfName()).append(",").append(student.getlName());
                if(teacherDetailToExport.isPractice())
                    fw.append(",").append(getScoreWithPercentage(student.getPracticeScore()));
                if(teacherDetailToExport.isWordProblem())
                    fw.append(",").append(getScoreWithPercentage(student.getWordScore()));
                if(teacherDetailToExport.isCustom())
                    fw.append(",").append(getScoreWithPercentage(student.getCustomeScore()));
                fw.append(",").append(getScoreWithPercentage(student.getAvgScore())).append("\n");
            }
        }
        fw.close();
    }

    private static String getScoreWithPercentage(String score){
        if(MathFriendzyHelper.isEmpty(score))
            return "0%";
        return score + "%";
    }

    public static String getFullPathOfCSVFile(String fileName){
        return getMathFilePath() + "/CSV" + "/" + fileName;
    }

    //check app availability before start internal intent
    public static boolean isAnyAppAvailableToHandleIntent(Context context , Intent intent){
        PackageManager packageManager = context.getPackageManager();
        if (intent.resolveActivity(packageManager) != null) {
            return true;
        } else {
            return false;
        }
    }
    //End CSV File changes


    /**
     * Save the links for the offline play
     * First delete the existing record and then insert the new one
     * @param context
     */
    public static void saveUserLinksIntoLocalDB(Context context, SaveGDocLinkOnServerParam param) {
        try {
            HomeWorkImpl implObj = new HomeWorkImpl(context);
            implObj.openConnection();
            implObj.deleteFromUserLinks(param);
            implObj.insertIntoUserLinks(param);
            implObj.closeConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static final String LOG_FILE_NAME = "MathLog.txt";
    public static String getFullLogFilePath(String fileName){
        return getMathFilePath() + "/MathLog" + "/" + fileName;
    }

    public static void WriteLog(String Tag, String Message, Exception e) {
        try {
            String settingDirPath = getMathFilePath() + "/MathLog" ;
            File wlDir = new File(settingDirPath);
            if (!wlDir.isDirectory()) {
                wlDir.mkdirs();
            }
            File logFile = new File(settingDirPath + "/" + LOG_FILE_NAME);
            FileWriter fw = new FileWriter(logFile, true);
            if (e != null) {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                e.printStackTrace(pw);
                fw.append(System.getProperty("line.separator"));
                fw.append(" Exeption Log at "
                        + MathFriendzyHelper.GetCurrentDateInTimeWEuropeSTD());
                fw.append(System.getProperty("line.separator"));
                fw.append(" Tag=" + Tag);
                fw.append(System.getProperty("line.separator"));
                fw.append(" Message=" + sw.toString());
            } else {
                fw.append(System.getProperty("line.separator"));
                fw.append(" Log at " + MathFriendzyHelper.GetCurrentDateInTimeWEuropeSTD());
                fw.append(System.getProperty("line.separator"));
                fw.append(" Tag=" + Tag);
                fw.append(System.getProperty("line.separator"));
                fw.append(" Message=" + Message);
            }
            fw.append(System.getProperty("line.separator")
                    + "================================================================"
                    + System.getProperty("line.separator"));
            fw.close();
        } catch (Exception ex) {
        }
    }

    public static String GetCurrentDateInTimeWEuropeSTD() {
        Date date = new Date();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        df.setTimeZone(TimeZone.getTimeZone("Europe/Berlin"));
        return df.format(date);
    }

    public static boolean isFileExist(String fileUrl){
        try {
            File file = new File(fileUrl);
            if (file.exists())
                return true;
            return false;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public static void sendEmailWithLogFile(Context context , String fileUrl){
        if(!MathFriendzyHelper.isFileExist(fileUrl)){
            MathFriendzyHelper.showToast(context , "No Log File Found. Please do the test.");
            return;
        }
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_EMAIL , new String[]{"yashwants@chromeinfotech.com"});
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Log File");
        sendIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(fileUrl)));
        sendIntent.setType("text/html");
        final PackageManager pm = context.getPackageManager();
        final List<ResolveInfo> matches = pm.queryIntentActivities(sendIntent, 0);
        ResolveInfo best = null;
        for(final ResolveInfo info : matches)
            if (info.activityInfo.packageName.endsWith(".gm")
                    || info.activityInfo.name.toLowerCase().contains("gmail"))
                best = info;
        if (best != null)
            sendIntent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
        context.startActivity(sendIntent);
    }
}
