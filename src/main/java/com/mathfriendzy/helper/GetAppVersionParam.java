package com.mathfriendzy.helper;

/**
 * Created by root on 13/4/16.
 */
public class GetAppVersionParam {
    private String action;
    private int appId;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getAppId() {
        return appId;
    }

    public void setAppId(int appId) {
        this.appId = appId;
    }
}
