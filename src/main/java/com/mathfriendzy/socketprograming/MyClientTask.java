package com.mathfriendzy.socketprograming;

import android.os.AsyncTask;
import android.util.Log;

import com.mathfriendzy.utils.CommonUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class MyClientTask extends AsyncTask<Void, Void, String>{

    private String  destinationAddress = null;
    private int     destinationport = 0;
    private boolean isConnected = false;
    private Socket socket = null;
    private OutputStream outputStream = null;
    private ClientCallback callback = null;
    private Thread recievedMessageThread = null;
    private boolean isStopThread = false;

    public MyClientTask(String destinationAddress , int detinationPost ,
                        ClientCallback callback){
        this.destinationAddress = destinationAddress;
        this.destinationport = detinationPost;
        this.callback = callback;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... params) {
        try {
            socket = new Socket(destinationAddress, destinationport);
            isConnected = true;
            outputStream = socket.getOutputStream();
            //inputStream  = socket.getInputStream();
        } catch (UnknownHostException e) {
            isConnected = false;
            e.printStackTrace();
        } catch (IOException e) {
            isConnected = false;
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        if(isConnected){
            callback.onConnected();
            recievedMessageThread = new Thread(new RecieverThread());
            recievedMessageThread.start();
        }else{
            callback.onConnectionFail();
        }
        super.onPostExecute(result);
    }

    /**
     * Send message to the server
     * @param message
     */
    public void sendMessage(String message){
        try{
            //message = message + "\n";
            if(CommonUtils.LOG_ON)
                Log.e("MyClientTask" , "sending message " + message);
            if(isConnected){
                PrintWriter out = new PrintWriter(new BufferedWriter(
                        new OutputStreamWriter(outputStream)) , true);
                out.print(message);
                out.flush();
                callback.onMessageSent(true);
            }else{
                callback.onMessageSent(false);
            }
        }catch(Exception e){
            e.printStackTrace();
            callback.onMessageSent(false);
        }
    }

    /**
     * Recieved message thread
     * @author yashwant
     *
     */
    class RecieverThread implements Runnable{
        @Override
        public void run() {
            try {
                while(!isStopThread){
                    BufferedReader in = new BufferedReader
                            (new InputStreamReader(socket.getInputStream()));
                    String line = in.readLine();
                   while (line != null && !line.equals("null")){
                        callback.onMessageRecieved(line);
                        line = in.readLine();
                    }
               }
            } catch (Exception e) {
                e.printStackTrace();
                callback.onConnectionFail();
            }
        }
    }

    /**
     * Disconnect from chat
     */
    public void disconnect(){
        if(socket != null){
            try {
                isStopThread = true;
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
