package com.mathfriendzy.socketprograming;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import com.mathfriendzy.utils.CommonUtils;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by root on 27/7/15.
 */
public class MyWebSocket extends AsyncTask<Void , Void , Void>{

    //private String WEB_SOCKET_URL = "ws://calypso.link:8080/room/1";
    //private String WEB_SOCKET_URL = "ws://calypso.link:8080/room/";
    //private String WEB_SOCKET_URL = "ws://letsleapahead.com:8080/room/";
    private String WEB_SOCKET_URL = "ws://letsleapahead.com:2197/room/";
    //private String WEB_SOCKET_URL = "ws://192.168.0.122:8080/room/";//for local
    private Context context = null;
    private WebSocketClient mWebSocketClient = null;
    private ClientCallback callback = null;
    private final String TAG = this.getClass().getSimpleName();
    private int chatRequestId = 0;
    private String playerId = null;

    public MyWebSocket(Context context , ClientCallback callback ,
                       int chatRequestId , String playerId){
        this.context = context;
        this.callback = callback;
        this.chatRequestId = chatRequestId;
        this.playerId = playerId;
    }

    @Override
    protected Void doInBackground(Void... params) {
        this.connectWebSocket(callback);
        return null;
    }

    /**
     * Connect with the web socket
     * @param callback - callback
     */
    private void connectWebSocket(final ClientCallback callback) {
        URI uri;
        try {
            String url = WEB_SOCKET_URL + this.chatRequestId + "?user_id=" + this.playerId;
            CommonUtils.printLog(TAG , "socket url " + url);
            uri = new URI(url);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            CommonUtils.printLog(TAG , e.getMessage() + " " + e.toString());
            return;
        }

        mWebSocketClient = new WebSocketClient(uri) {
            @Override
            public void onOpen(ServerHandshake serverHandshake) {
                CommonUtils.printLog(TAG , "onOpen");
                //mWebSocketClient.send("Hello from " + Build.MANUFACTURER + " " + Build.MODEL);
                callback.onConnected();
            }

            @Override
            public void onMessage(String s) {
                final String message = s;
                ((Activity)context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        CommonUtils.printLog(TAG, "onMessage " + message);
                        callback.onMessageRecieved(message);
                    }
                });
            }
            @Override
            public void onClose(int i, String s, boolean b) {
                CommonUtils.printLog(TAG, "onClose " + s);
            }
            @Override
            public void onError(Exception e) {
                CommonUtils.printLog(TAG , "onError " + e.getMessage());
                callback.onConnectionFail();
            }
        };
        mWebSocketClient.connect();
    }

    /**
     * Send message to the connected receiver
     * @param message
     */
    public void sendMessage(String message) {
        try {
            CommonUtils.printLog(TAG , "message sent " + message);
            if (mWebSocketClient != null) {
                mWebSocketClient.send(message);
                callback.onMessageSent(true);
            }else{
                callback.onMessageSent(false);
            }
        }catch(Exception e){
            e.printStackTrace();
            callback.onMessageSent(false);
        }
    }

    /**
     * Disconnect with the socket connection
     */
    public void disconnect(){
        try {
            mWebSocketClient.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void getAllOnlineUserInRoom(String prefixKey , String currentPlayerId){
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("cmd" , "get_members");
            jsonObject.put("id" , currentPlayerId);
            this.sendMessage(prefixKey + jsonObject.toString());
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void getRoomHistory(String prefixKey , String currentPlayerId){
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("cmd" , "get_room_history");
            jsonObject.put("id" , currentPlayerId);
            this.sendMessage(prefixKey + jsonObject.toString());
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
