package com.mathfriendzy.socketprograming;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;

import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.utils.CommonUtils;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by root on 14/4/16.
 */
public class MyGlobalWebSocket extends AsyncTask<Void , Void , Void> {

    private String WEB_SOCKET_URL = "ws://letsleapahead.com:2197/room/";
    private Context context = null;
    private WebSocketClient mWebSocketClient = null;
    private static ClientCallback callback = null;
    private final String TAG = this.getClass().getSimpleName();
    private int chatRequestId = 0;
    private String playerId = null;

    private static MyGlobalWebSocket myGlobalWebSocket = null;
    private static boolean isSocketConnected = false;

    public static final String GET_ONLINE_USER_STATUS = "get_online_user_status";

    public MyGlobalWebSocket(Context context , ClientCallback callback ,
                             int chatRequestId , String playerId){
        this.context = context;
        this.callback = callback;
        this.chatRequestId = chatRequestId;
        this.playerId = playerId;
    }

    public static MyGlobalWebSocket getInstance(Context context , ClientCallback callback ,
                                                int chatRequestId , String playerId){
        if(myGlobalWebSocket == null){
            myGlobalWebSocket = new MyGlobalWebSocket(context , callback , chatRequestId , playerId);
            myGlobalWebSocket.execute();
        }
        return myGlobalWebSocket;
    }

    public static MyGlobalWebSocket getInstance(){
        return myGlobalWebSocket;
    }

    public static void initializeCallback(ClientCallback callback){
        MyGlobalWebSocket.callback = callback;
    }

    public static boolean isSocketConnected(){
        return isSocketConnected;
    }

    @Override
    protected Void doInBackground(Void... params) {
        this.connectWebSocket();
        return null;
    }

    private String getSocketUrl(){
        return WEB_SOCKET_URL + "global" + "?user_id=" + this.playerId;
    }

    /**
     * Connect with the web socket
     */
    private void connectWebSocket() {
        URI uri;
        try {
            String url = this.getSocketUrl();
            CommonUtils.printLog(TAG, "socket url " + url);
            uri = new URI(url);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            CommonUtils.printLog(TAG , e.getMessage() + " " + e.toString());
            return;
        }

        mWebSocketClient = new WebSocketClient(uri) {
            @Override
            public void onOpen(ServerHandshake serverHandshake) {
                CommonUtils.printLog(TAG , "onOpen");
                //mWebSocketClient.send("Hello from " + Build.MANUFACTURER + " " + Build.MODEL);
                isSocketConnected = true;
                callback.onConnected();
            }

            @Override
            public void onMessage(String s) {
                final String message = s;
                ((Activity)context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        CommonUtils.printLog(TAG, "onMessage " + message);
                        processServerMessage(message);
                        //callback.onMessageRecieved(message);
                    }
                });
            }
            @Override
            public void onClose(int i, String s, boolean b) {
                isSocketConnected = false;
                CommonUtils.printLog(TAG, "onClose " + s);
            }
            @Override
            public void onError(Exception e) {
                CommonUtils.printLog(TAG , "onError " + e.getMessage());
                callback.onConnectionFail();
            }
        };
        mWebSocketClient.connect();
    }

    /**
     * Send message to the connected receiver
     * @param message
     */
    public void sendMessage(String message) {
        try {
            CommonUtils.printLog(TAG , "message sent " + message);
            if (mWebSocketClient != null) {
                mWebSocketClient.send(message);
                callback.onMessageSent(true);
            }else{
                callback.onMessageSent(false);
            }
        }catch(Exception e){
            e.printStackTrace();
            callback.onMessageSent(false);
        }
    }

    /**
     * Disconnect with the socket connection
     */
    public void disconnect(){
        try {
            mWebSocketClient.close();
            myGlobalWebSocket = null;
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void processServerMessage(String message){
        try {
            message = message.replaceAll(MathFriendzyHelper.SERVER_PREFIX, "");
            if (message.equalsIgnoreCase(MathFriendzyHelper.PING)) {
                this.sendMessage(MathFriendzyHelper.SERVER_PREFIX + MathFriendzyHelper.PONG);
                return;
            }
            callback.onMessageRecieved(message);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void getOnlineStatus(String prefixKey , JSONArray studentArray){
        try{
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("cmd" , "get_rooms");
            jsonObject.put("id" ,  GET_ONLINE_USER_STATUS);
            jsonObject.put("users" ,  studentArray);
            this.sendMessage(prefixKey + jsonObject.toString());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
