package com.mathfriendzy.socketprograming;

public interface ClientCallback {
	void onConnected();
	void onConnectionFail();
	void onMessageSent(boolean isSuccess);
	void onMessageRecieved(String message);
}
