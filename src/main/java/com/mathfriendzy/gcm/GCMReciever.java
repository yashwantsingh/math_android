package com.mathfriendzy.gcm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.mathfriendzy.utils.CommonUtils;

import java.util.Random;

import static com.mathfriendzy.utils.ICommonUtils.PROPERTY_REG_ID;
import static com.mathfriendzy.utils.ICommonUtils.REG_ID_PREFF;

public class GCMReciever extends BroadcastReceiver
{
    private static final String TOKEN = Long.toBinaryString(new Random()
            .nextLong());
    private SharedPreferences prefs;

    @Override
    public void onReceive(Context context, Intent intent){

        if(CommonUtils.LOG_ON)
            Log.e("GCMReciever", "inside onRecieve()");

        try {
            prefs = context.getSharedPreferences(REG_ID_PREFF, 0);
            SharedPreferences.Editor prefsEditor = prefs.edit();

            String action = intent.getAction();
            GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
            // Log.e("","inside on receive");
            String messageType = gcm.getMessageType(intent);
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                Log.e("GCMReciever", messageType + "");
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
                    .equals(messageType)) {
                Log.e("GCMReciever", messageType + "");
            } else if (action.equals("com.google.android.c2dm.intent.REGISTRATION")) {

                String error = intent.getExtras().getString("error");
                if (error != null) {
                    if ("SERVICE_NOT_AVAILABLE".equals(error)) {
                        long backoffTimeMs = prefs.getLong("GCM_BACK_OFF_TIME",
                                2000); // get back-off time from shared
                        // preferences
                        long nextAttempt = SystemClock.elapsedRealtime()
                                + backoffTimeMs;
                        Intent retryIntent = new Intent(
                                "com.example.gcm.intent.RETRY");
                        retryIntent.putExtra("token", TOKEN);
                        PendingIntent retryPendingIntent = PendingIntent
                                .getBroadcast(context, 0, retryIntent, 0);
                        AlarmManager am = (AlarmManager) context
                                .getSystemService(Context.ALARM_SERVICE);
                        am.set(AlarmManager.ELAPSED_REALTIME, nextAttempt,
                                retryPendingIntent);
                        backoffTimeMs *= 2; // Next retry should wait longer.
                        prefsEditor.putLong("GCM_BACK_OFF_TIME", backoffTimeMs);
                        prefsEditor.commit();
                    } else {
                        // Unrecoverable error, log it
                        // Log.i("", "Received error: " + error);
                    }
                }

            } else if (action.equals("com.google.android.c2dm.intent.RECEIVE")) {
                // CommonFunctions.displayToast(context, "Received: " +
                // intent.getExtras().toString());

                if (CommonUtils.LOG_ON)
                    Log.d("", "inside on notification received");

                /*for (String key : intent.getExtras().keySet()) {
                    Object value = intent.getExtras().get(key);
                    Log.d("", String.format("%s %s (%s)", key,
                            value.toString(), value.getClass().getName()));*/
                    String message = intent.getExtras().getString("message");
                    Intent i = new Intent(context, ProcessNotification.class);
                    i.putExtra("Message", message);
                    context.startService(i);
                //}
            } else if (action.equals("com.example.gcm.intent.RETRY")) {
                Log.e("", "indside on retry getting Reg ID");
                String token = intent.getStringExtra("token");
                if (TOKEN.equals(token)) {
                    String registrationId = prefs.getString(
                            PROPERTY_REG_ID, "");
                    if (registrationId.equals("")) {
                        new GCMRegistration(context, true);
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
