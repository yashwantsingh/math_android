package com.mathfriendzy.gcm;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.mathfriendzy.utils.RegisterDeviceOnServer;

import java.io.IOException;
import java.sql.Timestamp;

import static com.mathfriendzy.utils.ICommonUtils.PROPERTY_REG_ID;
import static com.mathfriendzy.utils.ICommonUtils.REG_ID_PREFF;

public class GCMRegistration
{
    public static final String EXTRA_MESSAGE = "message";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private static final String PROPERTY_ON_SERVER_EXPIRATION_TIME =
            "onServerExpirationTimeMs";
    /**
     * Default lifespan (7 days) of a reservation until it is considered expired.
     */
    public static final long REGISTRATION_EXPIRY_TIME_MS = 1000 * 3600 * 24 * 7;
    private GoogleCloudMessaging gcm;
    private String regid;

    private static final String TAG = "GSM Registeration";
    private static final String SENDER_ID = "102009597507";//application id from google url
    //console account url : https://accounts.google.com/ServiceLogin?service=cloudconsole
    //&passive=true&continue=https%3A%2F%2Fcloud.google.com%2Fconsole&ltmpl=cloudconsole
    //or get it from http://developer.android.com/google/gcm/gs.html -> GoogleCloudConsole
    private boolean isUpdateRegIdOnServer = false;

    public GCMRegistration(Context context , boolean updateId) {
        isUpdateRegIdOnServer = updateId;
        gcm   = GoogleCloudMessaging.getInstance(context);
        regid = getRegistrationId(context);

        /*MathFriendzyHelper.showToast(context.getApplicationContext() ,
                "regid " + regid);*/

        if (regid.length() == 0){
            registerBackground(context);
        }
        //code added by me
        else{
            new RegisterDeviceOnServer(regid , context).execute(null,null,null);
        }
    }

    /**
     * Gets the current registration id for application on GCM service.
     * <p>
     * If result is empty, the registration has failed.
     *
     * @return registration id, or empty string if the registration is not
     *         complete.
     */
    private String getRegistrationId(Context context)
    {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");

        if (registrationId.length() == 0) {
            Log.v(TAG, "Registration not found.");
            return "";
        }
        // check if app was updated; if so, it must clear registration id to
        // avoid a race condition if GCM sends a message
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion || isRegistrationExpired(context))
        {
            Log.v(TAG, "App version changed or registration expired.");
            return "";
        }
        return registrationId;
    }


    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getGCMPreferences(Context context)
    {
        SharedPreferences sharedPref = context.getSharedPreferences(REG_ID_PREFF, 0);
        return sharedPref;
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context)
    {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * Checks if the registration has expired.
     *
     * <p>To avoid the scenario where the device sends the registration to the
     * server but the server loses it, the app developer may choose to re-register
     * after REGISTRATION_EXPIRY_TIME_MS.
     *
     * @return true if the registration has expired.
     */
    private boolean isRegistrationExpired(Context context)
    {
        final SharedPreferences prefs = getGCMPreferences(context);
        // checks if the information is not stale
        long expirationTime =
                prefs.getLong(PROPERTY_ON_SERVER_EXPIRATION_TIME, -1);
        return System.currentTimeMillis() > expirationTime;
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration id, app versionCode, and expiration time in the
     * application's shared preferences.
     */
    private void registerBackground(final Context context)
    {
        new AsyncTask<Void,Void,String>()
        {
            @Override
            protected String doInBackground(Void... params)
            {
                String msg = "";
                try {
                    //Log.e("GCM Registeration","inside register background");
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regid = gcm.register(SENDER_ID);
                    msg = "Device registered, registration id=" + regid;

                    // You should send the registration ID to your server over HTTP,
                    // so it can use GCM/HTTP or CCS to send messages to your app.

                    // For this demo: we don't need to send it because the device
                    // will send upstream messages to a server that echo back the message
                    // using the 'from' address in the message.

                    // Save the regid - no need to register again.

                    setRegistrationId(context, regid);
                }
                catch (IOException ex)
                {
                    msg = "Error :" + ex.getMessage();
                }

                return msg;
            }

            @Override
            protected void onPostExecute(String msg){
                //Log.e("registerBackground","inside on post execute");
                /*MathFriendzyHelper.showToast(context.getApplicationContext() ,
                        "After register on google regid " + regid + " " + msg);*/
                //code added by me
                if(regid.length() > 0)
                    new RegisterDeviceOnServer(regid , context).execute(null,null,null);
                else
                    registerBackground(context);

                // Log.d("",msg);
            }
        }.execute(null, null, null);
    }

    /**
     * Stores the registration id, app versionCode, and expiration time in the
     * application's {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId registration id
     */
    private void setRegistrationId(Context context, String regId)
    {
        //Log.e(TAG,"inside on set registration id");
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);

        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);

        long expirationTime = System.currentTimeMillis() + REGISTRATION_EXPIRY_TIME_MS;

        Log.v(TAG, "Setting registration expiry time to " +
                new Timestamp(expirationTime));
        editor.putLong(PROPERTY_ON_SERVER_EXPIRATION_TIME, expirationTime);
        editor.commit();

        if(isUpdateRegIdOnServer)
        {
            Intent i = new Intent(context, UpdateRegistrationID.class);
            context.startService(i);
        }
    }
}