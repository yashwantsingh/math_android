package com.mathfriendzy.gcm;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.helpastudent.ActHelpAStudent;
import com.mathfriendzy.controller.homework.ActCheckHomeWork;
import com.mathfriendzy.controller.homework.ActHomeWork;
import com.mathfriendzy.controller.homework.homwworkworkarea.HomeWorkWorkArea;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.multifriendzy.MultiFriendzyMain;
import com.mathfriendzy.controller.multifriendzy.MultiFriendzyRound;
import com.mathfriendzy.controller.multifriendzy.MultiFriendzyWinnerScreen;
import com.mathfriendzy.controller.tutor.ActTutorSession;
import com.mathfriendzy.controller.tutoringsession.ActTutoringSession;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.friendzy.FriendzyDTO;
import com.mathfriendzy.model.homework.HomeWorkNotification;
import com.mathfriendzy.model.multifriendzy.MathFriendzysRoundDTO;
import com.mathfriendzy.model.multifriendzy.MultiFriendzyServerOperation;
import com.mathfriendzy.model.multifriendzy.MultiFriendzysFromServerDTO;
import com.mathfriendzy.notification.DisconnectStudentNotif;
import com.mathfriendzy.notification.DisconnectWithTutorNotif;
import com.mathfriendzy.notification.HelpSetudentResponse;
import com.mathfriendzy.notification.PopUpActivityForNotification;
import com.mathfriendzy.notification.PopUpForFriendzyChallenge;
import com.mathfriendzy.notification.RequireHelpResponse;
import com.mathfriendzy.notification.SendNotificationTransferObj;
import com.mathfriendzy.notification.StudentInputNotif;
import com.mathfriendzy.notification.TutorCanthelpRightNowNotifResponse;
import com.mathfriendzy.notification.TutorImageUpdateNotif;
import com.mathfriendzy.notification.TutorWorkAreaImageUpdateNotif;
import com.mathfriendzy.utils.CommonUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * This class process notification
 * @author Yashwant Singh
 *
 */
public class ProcessNotification extends IntentService
{
    private final String TAG = this.getClass().getSimpleName();
    public static SendNotificationTransferObj sentNotificationData = null;
    public static boolean isFromNotification = false;
    private boolean isParseJson   = false;
    private boolean isShowMessage = false;

    private boolean isChallenge	  = false;
    public static FriendzyDTO dto = null;

    private final String CHAT_REQ_KEY = "chatReq";
    private final String HELP_STUDENT_KEY = "helpStud";
    private final String STUDENT_INPUT_KEY = "studInput";
    private final String DISCONNECT_STUDENT_KEY = "disStud";
    private final String DISCONNECT_WITH_TUTOR_KEY = "disTutor";
    private final String TUTOR_IMAGE_UPDATE_KEY = "tutorImgUpdate";
    private final String TUTOR_WORK_IMAGE_UPDATE_KEY = "tutorWorkImgUpdate";
    private final String TUTOR_CANT_HELP_NOW = "cantHelpNow";

    public ProcessNotification(){
        super("Process Notification");
    }
    private int SERVER_DATA_RECEIVED = 2;
    private int REQUEST_CODE_FOR_PENDING_INTENT = 1001;

    @Override
    protected void onHandleIntent(Intent intent)
    {
        try {

            CharSequence messageFromNotification = intent.getExtras().getString("Message");//message from notification
            if (CommonUtils.LOG_ON)
                Log.e(TAG, "notification msg " + messageFromNotification);
            //parse notification data
            if (!isParseJson) {
                isParseJson = true;
                if(!MathFriendzyHelper.isUserLogin(this))
                    return ;

                String message = messageFromNotification.toString();
                if (MathFriendzyHelper.checkNotificationForGivenKey(message, CHAT_REQ_KEY)) {
                    this.processChatRequest(message);
                } else if (MathFriendzyHelper.checkNotificationForGivenKey(message, HELP_STUDENT_KEY)) {
                    this.processConnectWithStudent(message);
                } else if (MathFriendzyHelper.checkNotificationForGivenKey(message, STUDENT_INPUT_KEY)) {
                    this.processStudentInputRequest(message);
                } else if (MathFriendzyHelper.checkNotificationForGivenKey(message, DISCONNECT_STUDENT_KEY)) {
                    this.processDisconnectWithStudent(message);
                } else if (MathFriendzyHelper.checkNotificationForGivenKey(message, DISCONNECT_WITH_TUTOR_KEY)) {
                    this.processDisconnectWithTutor(message);
                } else if (MathFriendzyHelper.checkNotificationForGivenKey(message, TUTOR_IMAGE_UPDATE_KEY)) {
                    this.processTutorAreaImageUpdate(message);
                } else if (MathFriendzyHelper.checkNotificationForGivenKey(message, TUTOR_WORK_IMAGE_UPDATE_KEY)) {
                    this.processTutorWorkAreaImageUpdate(message);
                } else if (MathFriendzyHelper.checkNotificationForGivenKey(message, TUTOR_CANT_HELP_NOW)) {
                    this.processTutorCantHelpNow(message);
                } else {
                    //updated for home work notification , null not for homework
                    HomeWorkNotification homwWorkNotifData = this.getHomeWorkNotificationData
                            (messageFromNotification.toString());
                    if (homwWorkNotifData != null) {//check for homework data , null if this notification not for HomeWork
                        this.showHomeWorkNotification(homwWorkNotifData);
                    } else {
                        sentNotificationData = this.parsingNotificationData(messageFromNotification.toString());
                        /********Edit by Shilpi ***************/
                        if (!isChallenge) {
                            isFromNotification = true;
                            new FindMultiFriendzyRound(ProcessNotification.sentNotificationData.getOppUserId(),
                                    ProcessNotification.sentNotificationData.getOppPlayerId(),
                                    ProcessNotification.sentNotificationData.getFriendzyId())
                                    .execute(null, null, null);
                        }
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * This method parse notification messagejson String
     * @param jsonStringMessage
     */
    private SendNotificationTransferObj parsingNotificationData(String jsonStringMessage)
    {
        //Log.e(TAG, "inside parsingNotificationData jsonString : " + jsonStringMessage);

        SendNotificationTransferObj notificationData = new SendNotificationTransferObj();

        try
        {
            JSONObject jObject = new JSONObject(jsonStringMessage);
            notificationData.setMessage(jObject.getString("msg"));
            notificationData.setUserId(jObject.getString("uid"));
            notificationData.setPlayerId(jObject.getString("pid"));
            notificationData.setCountry(jObject.getString("iso"));
            notificationData.setOppUserId(jObject.getString("oppUserId"));
            notificationData.setOppPlayerId(jObject.getString("oppPlrId"));
            notificationData.setFriendzyId(jObject.getString("fid"));
            notificationData.setProfileImageId(jObject.getString("img"));
            notificationData.setShareMessage(jObject.getString("optMsg"));
            notificationData.setSenderDeviceId(jObject.getString("oppPid"));//sender device pid
            notificationData.setForWord(jObject.getInt("worldProblem"));
            notificationData.setIsNotoficationFromIOS(jObject.getInt("fromIOs"));
        }
        catch (JSONException e)
        {
            Log.e(TAG, "inside parsingNotificationData Error while parsing : " + e.toString());
            /********Edit by Shilpi ***************/
            isChallenge = true;
            //Log.e(TAG, "inside parsingNotificationData Error while parsing : " + e.toString());
            dto = setChallengerNotification(jsonStringMessage);

            if(dto != null)
                displayNotification();
        }

        return notificationData;
    }

    /**
     * This class find multi friendzy rounds when user from notification
     * @author Yashwant Singh
     *
     */
    class FindMultiFriendzyRound extends AsyncTask<Void, Void, Void>
    {
        private String userId ;
        private String playerId;
        private String friendzyId;
        MultiFriendzysFromServerDTO multifriendzyDataFromSerevr = null;
        //private ProgressDialog pd = null;

        FindMultiFriendzyRound(String userId ,String playerId , String friendzyId)
        {
            MultiFriendzyRound.roundList = new ArrayList<MathFriendzysRoundDTO>();
            multifriendzyDataFromSerevr  = new MultiFriendzysFromServerDTO();

            this.userId 	= userId;
            this.playerId 	= playerId;
            this.friendzyId = friendzyId;
        }

        @Override
        protected void onPreExecute()
        {
            //pd = CommonUtils.getProgressDialog(this);
            //pd.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            MultiFriendzyServerOperation serverObj = new MultiFriendzyServerOperation();
            multifriendzyDataFromSerevr = serverObj.getRoundList(userId, playerId, friendzyId);
            return null;
        }

        @SuppressWarnings("deprecation")
        @Override
        protected void onPostExecute(Void result)
        {
            //pd.cancel();


            //MultiFriendzyRound.roundList = multifriendzyDataFromSerevr.getRoundList();

            MultiFriendzyMain.isClickOnYourTurn = true;
            String myPackageName = getPackageName();

            ActivityManager am = (ActivityManager) getSystemService(Activity.ACTIVITY_SERVICE);
            String packageName = am.getRunningTasks(1).get(0).topActivity.getPackageName();

            if(myPackageName.equals(packageName))
            {
                if(!isShowMessage)
                {
                    isShowMessage = true;

                    //ProcessNotification.isFromNotification = false;

                    if(multifriendzyDataFromSerevr.getWinner() == -1)
                    {
                        sentNotificationData.setWinnerId("-1");
                        MultiFriendzyRound.roundList = multifriendzyDataFromSerevr.getRoundList();
                    }
                    else
                    {
                        sentNotificationData.setWinnerId(multifriendzyDataFromSerevr.getWinner() + "");
                        MultiFriendzyWinnerScreen.roundList = multifriendzyDataFromSerevr.getRoundList();
                    }

                    Intent newIntent =  new Intent(ProcessNotification.this ,
                            PopUpActivityForNotification.class);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(newIntent);
                }
            }
            else
            {
                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                int icon = R.drawable.ic_launcher;
                CharSequence notiText = MathFriendzyHelper.getAppNameForNotificationBar();
                long meow = System.currentTimeMillis();

                @SuppressWarnings("deprecation")
                Notification notification = new Notification(icon, notiText, meow);
                notification.flags     |= Notification.FLAG_AUTO_CANCEL;
                notification.defaults  |= Notification.DEFAULT_LIGHTS;
                notification.defaults  |= Notification.DEFAULT_SOUND;
                notification.defaults  |= Notification.DEFAULT_VIBRATE;

                CharSequence contentTitle = MathFriendzyHelper.getAppNameForNotificationBar();
                Intent notificationIntent = null;

                if(multifriendzyDataFromSerevr.getWinner() == -1)
                {
                    sentNotificationData.setWinnerId("-1");
                    MultiFriendzyRound.roundList = multifriendzyDataFromSerevr.getRoundList();
                    notificationIntent = new Intent(ProcessNotification.this, MultiFriendzyRound.class);//sending notification activity
                }
                else
                {
                    sentNotificationData.setWinnerId(multifriendzyDataFromSerevr.getWinner() + "");
                    MultiFriendzyWinnerScreen.roundList = multifriendzyDataFromSerevr.getRoundList();
                    notificationIntent = new Intent(ProcessNotification.this, MultiFriendzyWinnerScreen.class);//sending notification activity
                }

				/*notificationIntent.putExtra("message", sentNotificationData.getMessage());*/
                PendingIntent contentIntent = PendingIntent.getActivity(ProcessNotification.this,
                        MathFriendzyHelper.getRandomNumber(), notificationIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
                notification.setLatestEventInfo(ProcessNotification.this, contentTitle,
                        sentNotificationData.getMessage(), contentIntent);
                notificationManager.notify(SERVER_DATA_RECEIVED, notification);
            }
            super.onPostExecute(result);
        }
    }


    /********Edit by Shilpi ***************/
    private FriendzyDTO setChallengerNotification(String jsonStringMessage)
    {
        FriendzyDTO dto = new FriendzyDTO();
        try
        {
            JSONObject jObject = new JSONObject(jsonStringMessage);
            dto.setRewards(jObject.getString("msg"));
            //if(dto.getRewards().contains("New Challenge") || dto.getRewards().contains("Friendzy Challenge"))
            if(!dto.getRewards().contains("has been removed"))
            {
                dto.setTeacherId(jObject.getString("teacherId"));
                dto.setSchoolId(jObject.getString("schoolId"));
                dto.setSchoolName(jObject.getString("school"));
                dto.setStatus(jObject.getString("status"));
                dto.setType(jObject.getString("type"));
                dto.setGrade(jObject.getString("grade"));
            }
            else
            {
                PopUpForFriendzyChallenge.isDelete = true;
                dto.setUid(jObject.getString("uId"));
                dto.setPid(jObject.getString("pid"));
            }
            dto.setChallengerId(jObject.getString("chId"));
            dto.setTeacherName(jObject.getString("teacherName"));
            dto.setStartDate(jObject.getString("startDate"));
            dto.setEndDate(jObject.getString("endDate"));
            dto.setClassName(jObject.getString("class"));

        }
        catch (JSONException e)
        {
            Log.e(TAG, "Inside setChallengerNotification "+e.toString());
            return null;
        }

        return dto;
    }


    @SuppressWarnings("deprecation")
    private void displayNotification()
    {
        String myPackageName = getPackageName();

        ActivityManager am = (ActivityManager) getSystemService(Activity.ACTIVITY_SERVICE);
        String packageName = am.getRunningTasks(1).get(0).topActivity.getPackageName();

        if(myPackageName.equals(packageName))
        {
            Intent newIntent =  new Intent(ProcessNotification.this ,
                    PopUpForFriendzyChallenge.class);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(newIntent);
        }
        else
        {
            NotificationManager notificationManager = (NotificationManager)
                    getSystemService(NOTIFICATION_SERVICE);
            int icon = R.drawable.ic_launcher;
            CharSequence notiText = MathFriendzyHelper.getAppNameForNotificationBar();
            long meow = System.currentTimeMillis();

            Notification notification = new Notification(icon, notiText, meow);
            notification.flags     |= Notification.FLAG_AUTO_CANCEL;
            notification.defaults  |= Notification.DEFAULT_LIGHTS;
            notification.defaults  |= Notification.DEFAULT_SOUND;
            notification.defaults  |= Notification.DEFAULT_VIBRATE;

            CharSequence contentTitle = MathFriendzyHelper.getAppNameForNotificationBar();
            Intent notificationIntent = new Intent(ProcessNotification.this,
                    PopUpForFriendzyChallenge.class);
            PendingIntent contentIntent = PendingIntent.getActivity(ProcessNotification.this,
                    MathFriendzyHelper.getRandomNumber(), notificationIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            notification.setLatestEventInfo(ProcessNotification.this, contentTitle,
                    dto.getClassName(), contentIntent);
            notificationManager.notify(SERVER_DATA_RECEIVED, notification);

        }//End if else

    }//END displayNotification method


    //edit for Home work
    /**
     * Check for homework , If not for homework then return null , otherwise home work data
     * @param notificationJson
     * @return
     */
    private HomeWorkNotification getHomeWorkNotificationData(String notificationJson){
        try {
            HomeWorkNotification homeWorkNotificationData = new HomeWorkNotification();
            JSONObject jsonObj = new JSONObject(notificationJson);
            homeWorkNotificationData.setHomeWork(jsonObj.getInt("homework"));
            homeWorkNotificationData.setMsg(jsonObj.getString("msg"));
            return homeWorkNotificationData;
        } catch (JSONException e) {
            return null;
        }
    }

    /**
     * Show the HomeWork Notificaiton
     */
    @SuppressWarnings("deprecation")
    private void showHomeWorkNotification(HomeWorkNotification homeWorkNotificationData){

        String myPackageName = getPackageName();
        ActivityManager am = (ActivityManager) getSystemService(Activity.ACTIVITY_SERVICE);
        String packageName = am.getRunningTasks(1).get(0).topActivity.getPackageName();

        if(myPackageName.equals(packageName)){
            Intent newIntent =  new Intent(ProcessNotification.this , PopUpActivityForNotification.class);
            newIntent.putExtra(PopUpActivityForNotification.ACT_CALL_FOR,
                    PopUpActivityForNotification.CALL_FOR_HOME_WORK);
            newIntent.putExtra("notifData", homeWorkNotificationData);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(newIntent);
        }
        else{
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            int icon = R.drawable.ic_launcher;
            CharSequence notiText = MathFriendzyHelper.getAppNameForNotificationBar();
            long meow = System.currentTimeMillis();

            Notification notification = new Notification(icon, notiText, meow);
            notification.flags     |= Notification.FLAG_AUTO_CANCEL;
            notification.defaults  |= Notification.DEFAULT_LIGHTS;
            notification.defaults  |= Notification.DEFAULT_SOUND;
            notification.defaults  |= Notification.DEFAULT_VIBRATE;

            CharSequence contentTitle = MathFriendzyHelper.getAppNameForNotificationBar();
            Intent notificationIntent = new Intent(ProcessNotification.this, ActHomeWork.class);
			/*notificationIntent.putExtra("message", sentNotificationData.getMessage());*/
            PendingIntent contentIntent = PendingIntent.getActivity(ProcessNotification.this,
                    MathFriendzyHelper.getRandomNumber(), notificationIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            notification.setLatestEventInfo(ProcessNotification.this, contentTitle,
                    homeWorkNotificationData.getMsg(), contentIntent);
            notificationManager.notify(SERVER_DATA_RECEIVED, notification);
        }
    }


    /**
     * Process chat request
     * @param message
     */
    private void processChatRequest(String message) {
        RequireHelpResponse response = this.parseChatRequestNotif(message);
        if(response != null){
            String myPackageName = getPackageName();
            ActivityManager am = (ActivityManager) getSystemService(Activity.ACTIVITY_SERVICE);
            String packageName = am.getRunningTasks(1).get(0).topActivity.getPackageName();
            if(myPackageName.equals(packageName)){
                String currentOpenScreenName = am.getRunningTasks(1).get(0).
                        topActivity.getClassName();
                //Log.e(TAG , "currentOpenScreenName " + currentOpenScreenName);
                if(currentOpenScreenName.equalsIgnoreCase(MathFriendzyHelper.ActHelpAStudent_Name)){
                    //ActHelpAStudent.getCurrentObj().updateUIFromNotofication();
                    //for popup also
                    Intent newIntent = new Intent(ProcessNotification.this,
                            PopUpActivityForNotification.class);
                    newIntent.putExtra(PopUpActivityForNotification.ACT_CALL_FOR,
                            PopUpActivityForNotification.CALL_FOR_CHAT_REQUEST);
                    newIntent.putExtra("notifData", response);
                    newIntent.putExtra("isHelpStudentScreenOpen" , true);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(newIntent);
                }else {
                    Intent newIntent = new Intent(ProcessNotification.this,
                            PopUpActivityForNotification.class);
                    newIntent.putExtra(PopUpActivityForNotification.ACT_CALL_FOR,
                            PopUpActivityForNotification.CALL_FOR_CHAT_REQUEST);
                    newIntent.putExtra("notifData", response);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(newIntent);
                }
            }else{
                this.showNotification(response.getMessage());
            }
        }
    }

    private RequireHelpResponse parseChatRequestNotif(String message){
        try {
            RequireHelpResponse response = new RequireHelpResponse();
            JSONObject jsonObject = new JSONObject(message);
            response.setMessage(jsonObject.getString("msg"));
            try {
                response.setChatReqId(jsonObject.getInt("chatReqId"));
            }catch(Exception e){
                response.setChatReqId(0);
            }
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Process help student or connect with student
     * @param message
     */
    private void processConnectWithStudent(String message) {
        HelpSetudentResponse response = this.parseHelpStudentNoti(message);
        if(response != null){
            String myPackageName = getPackageName();
            ActivityManager am = (ActivityManager) getSystemService(Activity.ACTIVITY_SERVICE);
            String packageName = am.getRunningTasks(1).get(0).topActivity.getPackageName();
            if(myPackageName.equals(packageName)){
                String currentOpenScreenName = am.getRunningTasks(1).get(0).
                        topActivity.getClassName();
                if(currentOpenScreenName.equalsIgnoreCase(MathFriendzyHelper.HomeWorkWorkArea_Name)){
                    HomeWorkWorkArea.getCurrentObj().
                            updateUIFromNotoficationForConnectWithStudent(response);
                }else if(currentOpenScreenName.equalsIgnoreCase(MathFriendzyHelper.ActCheckHomework)){
                    ActCheckHomeWork.getCurrentObj().updateUIAfterTutorInput(response);
                    Intent newIntent = new Intent(ProcessNotification.this,
                            PopUpActivityForNotification.class);
                    newIntent.putExtra(PopUpActivityForNotification.ACT_CALL_FOR,
                            PopUpActivityForNotification.CALL_FOR_HELP_STUDENT);
                    newIntent.putExtra("notifData", response);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(newIntent);
                }else if(currentOpenScreenName.equalsIgnoreCase(MathFriendzyHelper.ActTutorSession_Name)){
                    ActTutorSession.getCurrentObj().updateUIFromNotoficationForConnectWithStudent(response);
                }else if(currentOpenScreenName.equalsIgnoreCase(MathFriendzyHelper.ActTutoringSession)){
                    ActTutoringSession.getCurrentObj().updateUIAfterTutorInput(response);
                    Intent newIntent = new Intent(ProcessNotification.this,
                            PopUpActivityForNotification.class);
                    newIntent.putExtra(PopUpActivityForNotification.ACT_CALL_FOR,
                            PopUpActivityForNotification.CALL_FOR_HELP_STUDENT);
                    newIntent.putExtra("notifData", response);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(newIntent);
                }else{
                    Intent newIntent = new Intent(ProcessNotification.this,
                            PopUpActivityForNotification.class);
                    newIntent.putExtra(PopUpActivityForNotification.ACT_CALL_FOR,
                            PopUpActivityForNotification.CALL_FOR_HELP_STUDENT);
                    newIntent.putExtra("notifData", response);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(newIntent);
                }
            }
            else{
                this.showNotification(response.getMessage());
            }
        }
    }

    private HelpSetudentResponse parseHelpStudentNoti(String message){
        try {
            HelpSetudentResponse response = new HelpSetudentResponse();
            JSONObject jsonObject = new JSONObject(message);
            response.setMessage(jsonObject.getString("msg"));
            response.setChatReqId(jsonObject.getString("chatReqId"));
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Process response from student or student input
     * @param message
     */
    private void processStudentInputRequest(String message) {
        StudentInputNotif response = this.parseStudentInoutNotif(message);
        if(response != null){
            String myPackageName = getPackageName();
            ActivityManager am = (ActivityManager) getSystemService(Activity.ACTIVITY_SERVICE);
            String packageName = am.getRunningTasks(1).get(0).topActivity.getPackageName();
            if(myPackageName.equals(packageName)){
                String currentOpenScreenName = am.getRunningTasks(1).get(0).
                        topActivity.getClassName();
                if(currentOpenScreenName.equalsIgnoreCase(MathFriendzyHelper.ActHelpAStudent_Name)){
                    ActHelpAStudent.getCurrentObj().updateUIFromNotofication();
                }
                Intent newIntent =  new Intent(ProcessNotification.this ,
                        PopUpActivityForNotification.class);
                newIntent.putExtra(PopUpActivityForNotification.ACT_CALL_FOR,
                        PopUpActivityForNotification.CALL_FOR_STUDENT_INPUT);
                newIntent.putExtra("notifData", response);
                newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(newIntent);
            }
            else{
                this.showNotification(response.getMessage());
            }
        }
    }

    private StudentInputNotif parseStudentInoutNotif(String message){
        try {
            StudentInputNotif response = new StudentInputNotif();
            JSONObject jsonObject = new JSONObject(message);
            response.setMessage(jsonObject.getString("msg"));
            response.setChatReqId(jsonObject.getString("chatReqId"));
            response.setStudentInput(jsonObject.getString("studInput"));
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Process disconnect with student
     * @param message
     */
    private void processDisconnectWithStudent(String message) {
        DisconnectStudentNotif response = this.parseDisconnectWithStudentNotif(message);
        if(response != null){
            String myPackageName = getPackageName();
            ActivityManager am = (ActivityManager) getSystemService(Activity.ACTIVITY_SERVICE);
            String packageName = am.getRunningTasks(1).get(0).topActivity.getPackageName();
            if(myPackageName.equals(packageName)){
                String currentOpenScreenName = am.getRunningTasks(1).get(0).
                        topActivity.getClassName();
                //Log.e(TAG , "currentOpenScreenName " + currentOpenScreenName);
                if(currentOpenScreenName.equalsIgnoreCase(MathFriendzyHelper.HomeWorkWorkArea_Name)) {
                    HomeWorkWorkArea.getCurrentObj()
                            .updateUIFromNotoficationForDisConnectWithStudent(response);
                }else if(currentOpenScreenName.equalsIgnoreCase(MathFriendzyHelper.ActTutorSession_Name)) {
                    ActTutorSession.getCurrentObj()
                            .updateUIFromNotoficationForDisConnectWithStudent(response);
                }else{
                    Intent newIntent = new Intent(ProcessNotification.this, PopUpActivityForNotification.class);
                    newIntent.putExtra(PopUpActivityForNotification.ACT_CALL_FOR,
                            PopUpActivityForNotification.CALL_FOR_DISCONNECT_STUDENT);
                    newIntent.putExtra("notifData", response);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(newIntent);
                }
            }else{
                this.showNotification(response.getMessage());
            }
        }
    }

    private DisconnectStudentNotif parseDisconnectWithStudentNotif(String message){
        try {
            DisconnectStudentNotif response = new DisconnectStudentNotif();
            JSONObject jsonObject = new JSONObject(message);
            response.setMessage(jsonObject.getString("msg"));
            response.setDisconnectStudent(jsonObject.getString("disStud"));
            response.setChatRequestId(jsonObject.getString("chatReqId"));
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Process disconnect with tutor
     * @param message
     */
    private void processDisconnectWithTutor(String message) {
        DisconnectWithTutorNotif response = this.parseDisconnectWithTutorNotif(message);
        if(response != null){
            String myPackageName = getPackageName();
            ActivityManager am = (ActivityManager) getSystemService(Activity.ACTIVITY_SERVICE);
            String packageName = am.getRunningTasks(1).get(0).topActivity.getPackageName();
            if(myPackageName.equals(packageName)){
                String currentOpenScreenName = am.getRunningTasks(1).get(0).
                        topActivity.getClassName();
                if(currentOpenScreenName.equalsIgnoreCase(MathFriendzyHelper.ActHelpAStudent_Name)){
                    ActHelpAStudent.getCurrentObj().updateUIFromNotofication();
                }else if(currentOpenScreenName.equalsIgnoreCase(MathFriendzyHelper.ActTutorSession_Name)) {
                    ActTutorSession.getCurrentObj().updateUIFromNotoficationForDisConnectWithTutor(response);
                    return ;
                }/*else{*/
                Intent newIntent = new Intent(ProcessNotification.this, PopUpActivityForNotification.class);
                newIntent.putExtra(PopUpActivityForNotification.ACT_CALL_FOR,
                        PopUpActivityForNotification.CALL_FOR_DISCONNECT_TUTOR);
                newIntent.putExtra("notifData", response);
                newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(newIntent);
                //}
            }
            else{
                this.showNotification(response.getMessage());
            }
        }
    }

    private DisconnectWithTutorNotif parseDisconnectWithTutorNotif(String message){
        try {
            DisconnectWithTutorNotif response = new DisconnectWithTutorNotif();
            JSONObject jsonObject = new JSONObject(message);
            response.setMessage(jsonObject.getString("msg"));
            response.setDisconnectTutor(jsonObject.getString("disTutor"));
            response.setChatRequestId(jsonObject.getString("chatReqId"));
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Process the tutor area image update  notification
     * @param message
     */
    private void processTutorAreaImageUpdate(String message) {
        TutorImageUpdateNotif response = this.parseTutorImageUpdaterNotif(message);
        if(response != null){
            String myPackageName = getPackageName();
            ActivityManager am = (ActivityManager) getSystemService(Activity.ACTIVITY_SERVICE);
            String packageName = am.getRunningTasks(1).get(0).topActivity.getPackageName();
            if(myPackageName.equals(packageName)){
                String currentOpenScreenName = am.getRunningTasks(1).get(0).
                        topActivity.getClassName();
                if(currentOpenScreenName.equalsIgnoreCase(MathFriendzyHelper.ActHelpAStudent_Name)){
                    ActHelpAStudent.getCurrentObj().updateUIFromNotofication();
                    Intent newIntent = new Intent(ProcessNotification.this, PopUpActivityForNotification.class);
                    newIntent.putExtra(PopUpActivityForNotification.ACT_CALL_FOR,
                            PopUpActivityForNotification.CALL_FOR_TUTOR_AREA_IMAGE_UPDATE);
                    newIntent.putExtra("notifData", response);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(newIntent);
                }else if(currentOpenScreenName.equalsIgnoreCase(MathFriendzyHelper.ActCheckHomework)){
                    ActCheckHomeWork.getCurrentObj().updateUIAfterTutorInputForImageUpdate(response);
                    Intent newIntent = new Intent(ProcessNotification.this,
                            PopUpActivityForNotification.class);
                    newIntent.putExtra(PopUpActivityForNotification.ACT_CALL_FOR,
                            PopUpActivityForNotification.CALL_FOR_TUTOR_AREA_IMAGE_UPDATE);
                    newIntent.putExtra("notifData", response);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(newIntent);
                }else if(currentOpenScreenName.equalsIgnoreCase(MathFriendzyHelper.ActTutorSession_Name)) {
                    ActTutorSession.getCurrentObj().updateUIFromNotoficationForTutorImageUpdate(response);
                }else if(currentOpenScreenName.equalsIgnoreCase(MathFriendzyHelper.HomeWorkWorkArea_Name)) {
                    HomeWorkWorkArea.getCurrentObj().updateUIFromNotoficationForTutorImageUpdate(response);
                }else if(currentOpenScreenName.equalsIgnoreCase(MathFriendzyHelper.ActTutoringSession)) {
                    ActTutoringSession.getCurrentObj().updateUIAfterTutorInputForImageUpdate(response);
                    Intent newIntent = new Intent(ProcessNotification.this,
                            PopUpActivityForNotification.class);
                    newIntent.putExtra(PopUpActivityForNotification.ACT_CALL_FOR,
                            PopUpActivityForNotification.CALL_FOR_TUTOR_AREA_IMAGE_UPDATE);
                    newIntent.putExtra("notifData", response);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(newIntent);
                }else {
                    Intent newIntent = new Intent(ProcessNotification.this, PopUpActivityForNotification.class);
                    newIntent.putExtra(PopUpActivityForNotification.ACT_CALL_FOR,
                            PopUpActivityForNotification.CALL_FOR_TUTOR_AREA_IMAGE_UPDATE);
                    newIntent.putExtra("notifData", response);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(newIntent);
                }
            }
            else{
                this.showNotification(response.getMessage());
            }
        }
    }

    private TutorImageUpdateNotif parseTutorImageUpdaterNotif(String message){
        try {
            TutorImageUpdateNotif response = new TutorImageUpdateNotif();
            JSONObject jsonObject = new JSONObject(message);
            response.setMessage(jsonObject.getString("msg"));
            response.setTutorImageUpdate(jsonObject.getString("tutorImgUpdate"));
            response.setChatRequestId(jsonObject.getString("chatReqId"));
            response.setImage(jsonObject.getString("image"));
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Process tutor work area image update
     * @param message
     */
    private void processTutorWorkAreaImageUpdate(String message) {
        TutorWorkAreaImageUpdateNotif response = this.parseTutorWorkAreaImageUpdate(message);
        if(response != null){
            String myPackageName = getPackageName();
            ActivityManager am = (ActivityManager) getSystemService(Activity.ACTIVITY_SERVICE);
            String packageName = am.getRunningTasks(1).get(0).topActivity.getPackageName();
            if(myPackageName.equals(packageName)){

                String currentOpenScreenName = am.getRunningTasks(1).get(0).
                        topActivity.getClassName();
                if(currentOpenScreenName.equalsIgnoreCase(MathFriendzyHelper.ActHelpAStudent_Name)){
                    ActHelpAStudent.getCurrentObj().updateUIFromNotofication();
                    Intent newIntent = new Intent(ProcessNotification.this, PopUpActivityForNotification.class);
                    newIntent.putExtra(PopUpActivityForNotification.ACT_CALL_FOR,
                            PopUpActivityForNotification.CALL_FOR_TUTOR_WORK_AREA_IMAGE_UPDATE);
                    newIntent.putExtra("notifData", response);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(newIntent);
                }else if(currentOpenScreenName.equalsIgnoreCase(MathFriendzyHelper.ActTutorSession_Name)) {
                    ActTutorSession.getCurrentObj().updateUIFromNotoficationForTutorWorkImageUpdate(response);
                }else if(currentOpenScreenName.equalsIgnoreCase(MathFriendzyHelper.HomeWorkWorkArea_Name)) {
                    HomeWorkWorkArea.getCurrentObj().updateUIFromNotoficationForTutorWorkImageUpdate(response);
                }else {
                    Intent newIntent = new Intent(ProcessNotification.this, PopUpActivityForNotification.class);
                    newIntent.putExtra(PopUpActivityForNotification.ACT_CALL_FOR,
                            PopUpActivityForNotification.CALL_FOR_TUTOR_WORK_AREA_IMAGE_UPDATE);
                    newIntent.putExtra("notifData", response);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(newIntent);
                }
            }
            else{
                this.showNotification(response.getMessage());
            }
        }
    }

    private TutorWorkAreaImageUpdateNotif parseTutorWorkAreaImageUpdate(String message){
        try {
            TutorWorkAreaImageUpdateNotif response = new TutorWorkAreaImageUpdateNotif();
            JSONObject jsonObject = new JSONObject(message);
            response.setMessage(jsonObject.getString("msg"));
            response.setTutorWorkImgUpdate(jsonObject.getString("tutorWorkImgUpdate"));
            response.setChatRequestId(jsonObject.getString("chatReqId"));
            response.setImage(jsonObject.getString("image"));
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Process Notifiactrion when tutor cant right now
     * @param message
     */
    private void processTutorCantHelpNow(String message) {
        TutorCanthelpRightNowNotifResponse response = this.parseTutorCanthelpNotifResponse(message);
        if(response != null){
            String myPackageName = getPackageName();
            ActivityManager am = (ActivityManager) getSystemService(Activity.ACTIVITY_SERVICE);
            String packageName = am.getRunningTasks(1).get(0).topActivity.getPackageName();
            if(myPackageName.equals(packageName)){
                Intent newIntent =  new Intent(ProcessNotification.this , PopUpActivityForNotification.class);
                newIntent.putExtra(PopUpActivityForNotification.ACT_CALL_FOR,
                        PopUpActivityForNotification.CALL_FOR_TUTOR_CANT_HELP_RIGHT_NOW);
                newIntent.putExtra("notifData", response);
                newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(newIntent);
            }
            else{
                this.showNotification(response.getMessage());
            }
        }
    }

    private TutorCanthelpRightNowNotifResponse parseTutorCanthelpNotifResponse(String message) {
        try {
            TutorCanthelpRightNowNotifResponse response = new TutorCanthelpRightNowNotifResponse();
            JSONObject jsonObject = new JSONObject(message);
            response.setMessage(jsonObject.getString("msg"));
            response.setCantHelpNow(jsonObject.getInt("cantHelpNow"));
            response.setChatReqId(jsonObject.getInt("chatReqId"));
            response.setName(jsonObject.getString("name"));
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Intent getPendingIntentToOpenHomeScreen(){
        return new Intent(ProcessNotification.this , MainActivity.class);
    }

    private void showNotification(String message){
        NotificationManager notificationManager = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);
        int icon = R.drawable.ic_launcher;
        CharSequence notiText = MathFriendzyHelper.getAppNameForNotificationBar();
        long meow = System.currentTimeMillis();

        Notification notification = new Notification(icon, notiText, meow);
        notification.flags     |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults  |= Notification.DEFAULT_LIGHTS;
        notification.defaults  |= Notification.DEFAULT_SOUND;
        notification.defaults  |= Notification.DEFAULT_VIBRATE;

        CharSequence contentTitle = MathFriendzyHelper.getAppNameForNotificationBar();
        PendingIntent contentIntent = PendingIntent.getActivity(ProcessNotification.this,
                MathFriendzyHelper.getRandomNumber(),
                this.getPendingIntentToOpenHomeScreen(), PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setLatestEventInfo(ProcessNotification.this, contentTitle,
                message, contentIntent);
        notificationManager.notify(SERVER_DATA_RECEIVED, notification);
    }

}