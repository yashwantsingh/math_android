package com.mathfriendzy.serveroperation;

import android.util.Log;

import com.mathfriendzy.controller.homework.homwworkworkarea.AddUrlToWorkArea;
import com.mathfriendzy.controller.managetutor.settutor.ClassSubjects;
import com.mathfriendzy.controller.managetutor.settutor.MainSubject;
import com.mathfriendzy.controller.resources.GetKhanVideoLinkResponse;
import com.mathfriendzy.controller.resources.ResourceCategory;
import com.mathfriendzy.controller.resources.ResourceSubCat;
import com.mathfriendzy.helper.GetAppVersionResponse;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.appactiveinactivestatus.ActiveInactiveUserResponse;
import com.mathfriendzy.model.appunlock.GetAppUnlockStatusResponse;
import com.mathfriendzy.model.country.CountryCityResponse;
import com.mathfriendzy.model.grade.GetWorkAreaChatResponse;
import com.mathfriendzy.model.helpastudent.CheckForTutorResponse;
import com.mathfriendzy.model.helpastudent.ConnectWithTutorResponse;
import com.mathfriendzy.model.helpastudent.GetPlayerNeedHelpForTutorResponse;
import com.mathfriendzy.model.helpastudent.LoadServiceTimeResponse;
import com.mathfriendzy.model.helpastudent.TutorAreaResponse;
import com.mathfriendzy.model.helpastudent.UpdateChatWithDialogResponse;
import com.mathfriendzy.model.homework.CustomeAns;
import com.mathfriendzy.model.homework.CustomePlayerAns;
import com.mathfriendzy.model.homework.CustomeResult;
import com.mathfriendzy.model.homework.GetHelpStudentListResponse;
import com.mathfriendzy.model.homework.GetHomeWorkForStudentWithCustomeResponse;
import com.mathfriendzy.model.homework.GetStudentDetailAnswerResponse;
import com.mathfriendzy.model.homework.HomeworkData;
import com.mathfriendzy.model.homework.PracticeResult;
import com.mathfriendzy.model.homework.PracticeResultSubCat;
import com.mathfriendzy.model.homework.SaveHomeWorkServerResponse;
import com.mathfriendzy.model.homework.SaveStudentRatingResponse;
import com.mathfriendzy.model.homework.WordResult;
import com.mathfriendzy.model.homework.WordSubCatResult;
import com.mathfriendzy.model.homework.assignhomework.AssignHomeworkResponse;
import com.mathfriendzy.model.homework.assignhomework.GetStudentByGradeResponse;
import com.mathfriendzy.model.homework.checkhomework.GetDetailOfHomeworkWithCustomeResponse;
import com.mathfriendzy.model.homework.checkhomework.GetHomeworksWithCustomForTeacherResponse;
import com.mathfriendzy.model.homework.checkhomework.GetQuestionDetailForTeacherResponse;
import com.mathfriendzy.model.managetutor.mystudents.GetHomeworkDetailsForChatRequestIdResponse;
import com.mathfriendzy.model.managetutor.mystudents.GetMyStuentWithPasswordResponse;
import com.mathfriendzy.model.managetutor.mystudents.GetStidentsTutoringDetailForTeacherResponse;
import com.mathfriendzy.model.professionaltutoring.CancelProfileResponse;
import com.mathfriendzy.model.professionaltutoring.CreditCardPaymentResponse;
import com.mathfriendzy.model.professionaltutoring.GetPlayerPurchaseTimeResponse;
import com.mathfriendzy.model.professionaltutoring.GetPurchagePackagesResponse;
import com.mathfriendzy.model.professionaltutoring.GetPurchaseTimeInfoResponse;
import com.mathfriendzy.model.professionaltutoring.ShouldPaisTutorResponse;
import com.mathfriendzy.model.professionaltutoring.UpdatePurchaseTimeResponse;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.classes.ClassWithName;
import com.mathfriendzy.model.registration.classes.GetSchoolClassResponse;
import com.mathfriendzy.model.registration.classes.UserClasses;
import com.mathfriendzy.model.resource.GetGooruResourceForHWResponse;
import com.mathfriendzy.model.resource.GetResourceVideoInAppResult;
import com.mathfriendzy.model.resource.ResourceResponse;
import com.mathfriendzy.model.resource.SearchResourceResponse;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.CategoryListTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.DownloadCategoriesForGradesResponse;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SubCatergoryTransferObj;
import com.mathfriendzy.model.tutor.ChatMessageModel;
import com.mathfriendzy.model.tutor.FindTutorResponse;
import com.mathfriendzy.model.tutor.GetActiveTutorResponse;
import com.mathfriendzy.model.tutor.GetAnnonymousTutorResponse;
import com.mathfriendzy.model.tutor.GetChatRequestIdForProblemResponse;
import com.mathfriendzy.model.tutor.GetChatRequestResponse;
import com.mathfriendzy.model.tutor.GetDrawingPointForChatResponse;
import com.mathfriendzy.model.tutor.GetNewChatUserNameResponse;
import com.mathfriendzy.model.tutor.PhraseCatagory;
import com.mathfriendzy.model.tutor.PhraseSubCatagory;
import com.mathfriendzy.model.tutor.SaveMessageForSingleChatResponse;
import com.mathfriendzy.model.tutor.SendMessageToTutorResponse;
import com.mathfriendzy.model.tutoringsessions.TutoringSessionsForStudentResponse;
import com.mathfriendzy.pdfoperation.PDFOperation;
import com.mathfriendzy.serveroperation.serveroperationparamresponseclasses.GetActiveInActiveStatusOfStudentForTutorResponse;
import com.mathfriendzy.utils.CommonUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * This is the File Parser class 
 * Which Parse either json or XMl
 * @author Yashwant Singh
 *
 */
public class FileParser {

    private static final String TAG = "FileParser";

    //for download pdf at the time of homework download
    //private ArrayList<String> pdfNameList = null;


    public HttpResponseBase ParseJsonString(String response, int requestCode) {
        if(requestCode == ServerOperationUtil.GETHOMEWORKSFORSTUDENTSWITHCUSTOME)
            return this.parseGetHomeworksForStudentWithCustom(response);
        else if(requestCode == ServerOperationUtil.SAVE_HOMEWORK_SCORE)
            return this.parseSaveHomeWorkScoreJsonResponse(response);
        else if(requestCode == ServerOperationUtil.SAVE_HOME_WORK_WORD_PLAY_REQUEST)
            return this.parseSaveHomeWorkScoreJsonResponse(response);
        else if(requestCode == ServerOperationUtil.SAVE_HW_CUSTOME_ANS_BY_STUDENTS)
            return this.parseSaveHomeWorkScoreJsonResponse(response);
        else if(requestCode == ServerOperationUtil.GET_HELP_STUDENT_REQUEST)
            return this.parseGetHelpStudentList(response);
        else if(requestCode == ServerOperationUtil.SAVE_STUDENT_WORK_AREA_RATING_REQUEST)
            return this.parseStudentSaveRatingResponse(response);
        else if(requestCode == ServerOperationUtil.GET_STUDENTS_BY_GRADE_REQUEST)
            return this.parseGetStudentByGradeResponse(response);
        else if(requestCode == ServerOperationUtil.ASSIGN_HOMEWORK_REQUEST)
            return this.parseAssignHomeworkResponse(response);
        else if(requestCode == ServerOperationUtil.GETHOMEWORK_WITH_CUSTOME_TEACHER_REQUEST)
            return this.parseGetHomeworksWithCustomForTeacherResponse(response);
        else if(requestCode == ServerOperationUtil.GET_DETAIL_STUDENT_HOME_WORK_REQUEST)
            return this.parseGetDetailOfHomeworkWithCustomeResponseResponse(response);
        else if(requestCode == ServerOperationUtil.SAVE_TEACHER_CHECK_HW_CHANGES_REQUEST)
            return this.parseSaveHomeWorkScoreJsonResponse(response);
        else if(requestCode == ServerOperationUtil.SAVE_PLAY_SCORE_WHEN_NO_INTERNET)
            return this.parseSavePlayDataWhenNoInternet(response);
        else if(requestCode == ServerOperationUtil.GET_WORK_AREA_CHAT_MSG_REQUEST)
            return this.parseWorkAreaChatMessage(response);
        else if(requestCode == ServerOperationUtil.SAVE_WORK_AREA_CHAT_MSG_REQUEST)
            return this.parseSaveWorkAreaChatMessage(response);
        else if(requestCode == ServerOperationUtil.GET_TUTOR_BY_USERNAME_REQUEST)
            return this.parseGetTutorByUserNameJsonString(response);
        else if(requestCode == ServerOperationUtil.GET_ANNONYMOUS_TUTOR_REQUEST)
            return this.parseAnnonymousTutorResponse(response);
        else if(requestCode == ServerOperationUtil.GET_NEW_CHAT_USER_NAME)
            return this.parseGetNewChatUserName(response);
        else if(requestCode == ServerOperationUtil.UPDATE_CHAT_ID_TO_SERVER_REQUEST)
            return this.parseSaveWorkAreaChatMessage(response);
        else if(requestCode == ServerOperationUtil.GET_CHAT_REQUEST_SERVER_REQUEST)
            return this.parseGetChatRequestResponse(response);
        else if(requestCode == ServerOperationUtil.SEND_MESSAGE_TO_ANONYMOUS_TUTOR_REQUEST)
            return this.parseSendMessageToAnonymousTutorResponse(response);
        else if(requestCode == ServerOperationUtil.GET_PLAYER_NEED_HELP_FOR_TUTOR_REQUEST)
            return this.parseGetPlayerNeedHelpForTutorResponse(response);
        else if(requestCode == ServerOperationUtil.CONNECT_WITH_STUDENT_REQUEST)
            return this.parseConnectWithTutorResponse(response);
        else if(requestCode == ServerOperationUtil.GET_CHAT_REQUEST_ID_FOR_PROBLEM_REQUEST)
            return this.parseGetChatRequestIdForProblem(response);
        else if(requestCode == ServerOperationUtil.UPDATE_CHAT_REQUEST_WITH_DIALOG_REQUEST)
            return this.parseUpdateChatRequestWithDialogResponse(response);
        else if(requestCode == ServerOperationUtil.SAVE_SINGLE_CHAT_MESSAGE_REQUEST)
            return this.parseSingleChatResponse(response);
        else if(requestCode == ServerOperationUtil.UPDATE_DRAW_POINT_FOR_PLAYER_REQUEST)
            return this.parseTheUpdateDrawPointsForPlayerResponse(response);
        else if(requestCode == ServerOperationUtil.GET_DRAWING_POINT_REQUEST)
            return this.parseGetDrawingPointForChat(response);
        else if(requestCode == ServerOperationUtil.ADD_TIME_SPENT_IN_TUTOR_SESSION_REQUEST)
            return this.parseAddTimeSpentInTutorSessionResponse(response);
        else if(requestCode == ServerOperationUtil.RATE_TUTOR_TUTOR_SESSION_REQUEST)
            return this.parseRateTutorSessionResponse(response);
        else if(requestCode == ServerOperationUtil.GET_MY_STUDENTS_WITH_PASSWORD)
            return this.parseTheGetMyStudentWithPasswordResponse(response);
        else if(requestCode == ServerOperationUtil.MARK_AS_TUTOR_REQUEST)
            return this.parseTheMarkAsTutorResponse(response);
        else if(requestCode == ServerOperationUtil.GET_STUDENT_TUTORING_DETAIL_FOR_TEACHER_REQUEST)
            return this.parseTheGetStudentsTutoringDetailsForTeacher(response);
        else if(requestCode == ServerOperationUtil.GET_HOMEWORK_DETAIL_BY_CHAT_ID_REQUEST)
            return this.parseTheGetHomeworkDetailsForChatRequestId(response);
        else if(requestCode == ServerOperationUtil.SEND_EMAIL_TO_OTHER_TEACHER_REQUEST)
            return this.parseTheSendEmailToOtherTeacherResponse(response);
        else if(requestCode == ServerOperationUtil.GET_SERVICE_TIME_FOR_TUTOR_REQUEST)
            return this.parseTheGetServiceTimeForTutorResponse(response);
        else if(requestCode == ServerOperationUtil.TUTOR_SESSION_DESCONNECT_BY_TUTOR_REQUEST)
            return this.parseTheTutorSessionDisconnectByTutor(response);
        else if(requestCode == ServerOperationUtil.ADD_SERVICE_TIME_FOR_TUTOR_REQUEST)
            return this.parseTheTutorSessionDisconnectByTutor(response);
        else if(requestCode == ServerOperationUtil.STOP_WAITING_FOR_TUTOR_REQUEST)
            return this.parseTheTutorSessionDisconnectByTutor(response);
        else if(requestCode == ServerOperationUtil.EDIT_TUTOR_AREA_BY_STUDENT_OR_TUTOR)
            return this.parseTheEditTutorAreaResponse(response);
        else if(requestCode == ServerOperationUtil.REPORT_THIS_TUTOR_REQUEST)
            return this.parseTheResponseWhichDoesNotHaveNothingToShow(response);
        else if(requestCode == ServerOperationUtil.INPUT_SEEN_BY_PLAYER_REQUEST)
            return this.parseTheResponseWhichDoesNotHaveNothingToShow(response);
        else if(requestCode == ServerOperationUtil.UPDATE_LAST_ACTIVITY_TIME_FOR_PLAYER_REQUEST)
            return this.parseTheResponseWhichDoesNotHaveNothingToShow(response);
        else if(requestCode == ServerOperationUtil.GET_USER_ACTIVE_STATUS_REQUEST)
            return this.parseUserActiveInactiveStatusResponse(response);
        else if(requestCode == ServerOperationUtil.CHECK_FOR_TUTOR_REQUEST)
            return this.parseCheckForTutorResponse(response);
        else if(requestCode == ServerOperationUtil.UPDATE_TUTOR_AREA_IMAGE_REQUEST)
            return this.parseTheResponseWhichDoesNotHaveNothingToShow(response);
        else if(requestCode == ServerOperationUtil.ADD_DUPLICATE_WORK_FOR_STUDENT)
            return this.parseTheResponseWhichDoesNotHaveNothingToShow(response);
        else if(requestCode == ServerOperationUtil.GET_QUESTION_DETAIL_FOR_TEACHER_REQUEST)
            return this.pareseGetQuestionDetailForTeacherResponse(response);
        else if(requestCode == ServerOperationUtil.UPDATE_INPUT_STATUS_FOR_WORK_AREA)
            return this.parseTheResponseWhichDoesNotHaveNothingToShow(response);
        else if(requestCode == ServerOperationUtil.CANCEL_CHAT_REQUEST_BY_TUTOR_REQUEST)
            return this.parseTheResponseWhichDoesNotHaveNothingToShow(response);
        else if(requestCode == ServerOperationUtil.GET_ACTIVE_TUTOR_REQUEST)
            return this.parseActiveTutorsResponse(response);
        else if(requestCode == ServerOperationUtil.GET_TUTOR_AREA_RESPONSES)
            return this.parseTutorAreaResponses(response);
        else if(requestCode == ServerOperationUtil.GET_TUTOR_SESSION_FOR_STUDENT_REQUEST)
            return this.parseTutorSessionsForStudentResponse(response);
        else if(requestCode == ServerOperationUtil.GET_SEARCH_RESOURCES_REQUEST)
            return this.parseSearchResultResponse(response);
        else if(requestCode == ServerOperationUtil.GET_GOORU_RESOURCES_FOR_HW)
            return this.parseGooruResourceForHWResponse(response);
        else if(requestCode == ServerOperationUtil.GET_PLAYER_PURCHASE_TIME)
            return this.parseGetPurchaseTimeForPlayerResponse(response);
        else if(requestCode == ServerOperationUtil.GET_TUTOR_PACKAGES)
            return this.parseGetPurchagePackageResponse(response);
        else if(requestCode == ServerOperationUtil.UPDATE_PURCHASE_TIME_REQUEST)
            return this.parseUpdatePurchaseTime(response);
        else if(requestCode == ServerOperationUtil.GET_PLAYER_PURCHASE_TIME_INFO)
            return this.parseStudentPurchaseInfo(response);
        else if(requestCode == ServerOperationUtil.CANCEL_PROFILE)
            return this.parseCancelProfileResponse(response);
        else if(requestCode == ServerOperationUtil.CREDIT_CARD_PAYMENT_REQUEST)
            return this.parseCreditCardPaymentResponse(response);
        else if(requestCode == ServerOperationUtil.GET_TUTORING_SESSION_WITH_TITLE_REQUEST)
            return this.parseTutorSessionsForStudentResponse(response);
        else if(requestCode == ServerOperationUtil.SHOULD_PAID_TUTOR_ALLOW_REQUEST)
            return this.parseShouldPaidTutorResponse(response);
        else if(requestCode == ServerOperationUtil.GET_CHAT_MESSAGES_REQUEST)
            return this.parseChatMessgaesResponse(response);
        else if(requestCode == ServerOperationUtil.GET_RESOURCE_CATEGORIES_REQUEST)
            return this.parseResourceCategories(response);
        else if(requestCode == ServerOperationUtil.GET_RESOURCE_VIDEO_IN_APP_STATUS_REQUEST)
            return this.parseResourceVideoInAppStatusResponse(response);
        else if(requestCode == ServerOperationUtil.SAVE_RESOURCE_VIDEO_IN_APP_STATUS_REQUEST)
            return this.parseTheResponseWhichDoesNotHaveNothingToShow(response);
        else if(requestCode == ServerOperationUtil.GET_APP_UNLOCK_STATUS_REQUEST)
            return this.parseGetAppUnlockStatusResponse(response);
        else if(requestCode == ServerOperationUtil.UPDATE_TEACHER_CREDIT_FOR_STUDENT_ANSWER)
            return this.parseTheResponseWhichDoesNotHaveNothingToShow(response);
        else if(requestCode == ServerOperationUtil.UPDATE_USER_COINS_ON_SERVER)
            return this.parseTheResponseWhichDoesNotHaveNothingToShow(response);
        else if(requestCode == ServerOperationUtil.GET_STUDENT_ANSWER_DETAIL_REQUEST)
            return this.parseStudentAnswerDetailResponse(response);
        else if(requestCode == ServerOperationUtil.GET_INACTIVE_STATUS_OF_PLAYER_FOR_TUTOR)
            return this.parseTheActiveInActiveStatusOfStudentForTutor(response);
        else if(requestCode == ServerOperationUtil.SET_TUTOR_AREA_STATUS_REQUEST)
            return this.parseTheResponseWhichDoesNotHaveNothingToShow(response);
        else if(requestCode == ServerOperationUtil.GET_SCHOOL_CLASSES_REQUEST)
            return this.parseTheSchoolClassessResponse(response);
        else if(requestCode == ServerOperationUtil.GET_LIVE_APP_VERSION)
            return this.parseTheGetLiveAppVersion(response);
        else if(requestCode == ServerOperationUtil.SAVE_HOMEWORK_ON_SERVER_REQUEST)
            return this.parseSaveHomeworkResponse(response);
        else if(requestCode == ServerOperationUtil.GET_SAVED_HOMEWORK_FROM_SERVER_REQUEST)
            return this.parseSavedHomeworkDataFromServer(response);
        else if(requestCode == ServerOperationUtil.GET_WORD_CATEGORIES_FOR_GRADEDS_REQUEST)
            return this.parseGetCategoriesForGradesResponse(response);
        else if(requestCode == ServerOperationUtil.GET_PREV_HOMEWORK_FROM_SERVER_REQUEST)
            return this.parsePrevAssignedHomeworkDataFromServer(response);
        else if(requestCode == ServerOperationUtil.GET_CITIES_FROM_SERVER_REQUEST)
            return this.parseCitiesResponses(response);
        else if(requestCode == ServerOperationUtil.GET_SUBJECTS_FROM_SERVER_REQUEST)
            return this.parseSubjectResponse(response);
        else if(requestCode == ServerOperationUtil.GET_KHAN_VIDEO_LINK)
            return this.paserKhanVideoLinkResponse(response);
        else if(requestCode == ServerOperationUtil.SAVE_GDOC_WEB_LINK_REQUEST)
            return this.parseTheResponseWhichDoesNotHaveNothingToShow(response);
        return null;
    }

    /**
     * This method parse the GetHomeWorkForStudentWithCustomeResponse response
     * @param response
     * @return
     */
    private GetHomeWorkForStudentWithCustomeResponse parseGetHomeworksForStudentWithCustom(String response){
        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside parseGetHomeworksForStudentWithCustom response " + response);

        GetHomeWorkForStudentWithCustomeResponse responseData = new GetHomeWorkForStudentWithCustomeResponse();
        responseData.setJsonString(response);
        ArrayList<GetHomeWorkForStudentWithCustomeResponse> dataList =
                new ArrayList<GetHomeWorkForStudentWithCustomeResponse>();
        try {
            JSONObject jsonObj = new JSONObject(response);
            responseData.setResult(jsonObj.getString("result"));

            //new homework changes
            try {
                if (MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObj, "lastClassAssignedForHw")) {
                    responseData.setLastClassAssignedForHw(jsonObj.getInt("lastClassAssignedForHw"));
                }
                if (MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObj, "classes")) {
                    responseData.setClassArrayExist(true);
                    responseData.setClassWithNames(
                            this.parseClassesResponse(jsonObj.getJSONArray("classes")));
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            //end new homework changes

            JSONArray dataArray = jsonObj.getJSONArray("data");
            for(int i = 0 ; i < dataArray.length() ; i ++ ){
                JSONObject jsonDataObj = dataArray.getJSONObject(i);

                GetHomeWorkForStudentWithCustomeResponse dataObj = new GetHomeWorkForStudentWithCustomeResponse();
                dataObj.setHomeWorkId(jsonDataObj.getString("homeworkId"));
                dataObj.setDate(jsonDataObj.getString("date"));
                dataObj.setGrade(jsonDataObj.getString("grade"));
                dataObj.setFinished(jsonDataObj.getString("finished"));
                dataObj.setPracticeTime(jsonDataObj.getString("practiceTime"));
                dataObj.setPracticeScore(jsonDataObj.getString("practiceScore"));
                dataObj.setWordTime(jsonDataObj.getString("wordTime"));
                dataObj.setWordScore(jsonDataObj.getString("wordScore"));
                dataObj.setCustomeScore(jsonDataObj.getString("customScore"));
                dataObj.setAvgScore(jsonDataObj.getString("avgScore"));
                dataObj.setZeroCredit(jsonDataObj.getString("zeroCredit"));
                dataObj.setHalfCredit(jsonDataObj.getString("halfCredit"));
                dataObj.setFullCredit(jsonDataObj.getString("fullCredit"));
                dataObj.setTotalQuestion(jsonDataObj.getString("totalQuestions"));
                dataObj.setMessage(jsonDataObj.getString("message"));

                if (MathFriendzyHelper.checkForKeyExistInJsonObj(jsonDataObj, "className")) {
                    dataObj.setClassName(jsonDataObj.getString("className"));
                }

                if (MathFriendzyHelper.checkForKeyExistInJsonObj(jsonDataObj, "subjectName")) {
                    dataObj.setSubjectName(jsonDataObj.getString("subjectName"));
                }

                if (MathFriendzyHelper.checkForKeyExistInJsonObj(jsonDataObj, "subjectId")) {
                    dataObj.setSubId(jsonDataObj.getInt("subjectId"));
                }

                if(this.isJsonArray(jsonDataObj.get("practiceResult"))){
					/*JSONArray practiceArray = jsonDataObj.getJSONArray("practiceResult");
					for(int j = 0 ; j < practiceArray.length() ; j ++ ){
						practiceResultList.add(this.parsePracticeResult(practiceArray.getJSONObject(j)));
					}*/
                    ArrayList<PracticeResult> practiceResultList = new ArrayList<PracticeResult>();
                    //set blank list
                    dataObj.setPractiseResultList(practiceResultList);
                }else{
                    dataObj.setPractiseResultList(this.parsePracticeResult(jsonDataObj.getJSONObject("practiceResult")));
                    //practiceResultList.add(this.parsePracticeResult(jsonDataObj.getJSONObject("practiceResult")))	;
                }


                if(this.isJsonArray(jsonDataObj.get("wordResult"))){
                    ArrayList<WordResult> wordResultList = new ArrayList<WordResult>();
                    //set blank list
                    dataObj.setWordResultList(wordResultList);
                }else{
                    dataObj.setWordResultList(this.parseWordResult(jsonDataObj.getJSONObject("wordResult")));
                }

                ArrayList<CustomeResult> customeResultList = new ArrayList<CustomeResult>();
                if(this.isJsonArray(jsonDataObj.get("customResult"))){
                    JSONArray customeArray = jsonDataObj.getJSONArray("customResult");
                    for(int j = 0 ; j < customeArray.length() ; j ++ ){
                        customeResultList.add(this.parseCustomeResult(customeArray.getJSONObject(j)));
                    }
                }else{
                    customeResultList.add(this.parseCustomeResult(jsonDataObj.getJSONObject("customResult")));
                }

                dataObj.setCustomeresultList(customeResultList);
                dataList.add(dataObj);
            }

            //changes for download pdf at the first time
            //PDFOperation.startPdfDownloadingService(getPdfNameList());
            //end chages

            responseData.setFinalDataList(dataList);
        } catch (JSONException e) {
            responseData.setFinalDataList(dataList);
            e.printStackTrace();
        }
        return responseData;
    }

    /**
     * Parse the class response
     * @param classJsonArray
     * @return
     */
    private ArrayList<ClassWithName> parseClassesResponse(JSONArray classJsonArray){
        try {
            ArrayList<ClassWithName> classWithNames = new ArrayList<ClassWithName>();
            for (int j = 0; j < classJsonArray.length(); j++) {
                JSONObject jsonClassObjWithClassName = classJsonArray.getJSONObject(j);
                ClassWithName classWithName = new ClassWithName();
                classWithName.setClassId(jsonClassObjWithClassName.getInt("classId"));
                classWithName.setClassName(jsonClassObjWithClassName.getString("className"));
                classWithName.setSubject(jsonClassObjWithClassName.getString("subject"));
                classWithName.setStartDate(jsonClassObjWithClassName.getString("startDate"));
                classWithName.setGrade(MathFriendzyHelper.parseInt
                        (jsonClassObjWithClassName.getString("grade")));
                classWithName.setEndDate(jsonClassObjWithClassName.getString("endDate"));
                classWithNames.add(classWithName);
            }
            return classWithNames;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Add pdf name to list
     * @param pdfName
     */
	/*private void addPdfNameToList(String pdfName){
		if(pdfName != null && pdfName.length() > 0){
			if(pdfNameList == null)
				pdfNameList = new ArrayList<String>();
			pdfNameList.add(PDFOperation.getPdfFormattedFileName(pdfName));
		}
	}
	
	*//**
     * getPdfNameList
     *//*
	private ArrayList<String> getPdfNameList(){
		return pdfNameList;
	}*/

    /**
     * This method parse the practice Result
     * @param jsonObj
     * @return
     */
    private ArrayList<PracticeResult> parsePracticeResult(JSONObject jsonObj){

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside parsePracticeResult response " + jsonObj.toString());

        ArrayList<PracticeResult> practiceResultList = new ArrayList<PracticeResult>();
        Iterator<?> iterator = jsonObj.keys();
        while(iterator.hasNext()){
            PracticeResult result = new PracticeResult();
            String key = (String) iterator.next();
            result.setCatId(key);
            result.setIntCatId(Integer.parseInt(key));

            try {
                JSONArray  subCatJsonArray = jsonObj.getJSONArray(key);
                ArrayList<PracticeResultSubCat> practiceSubCatList = new ArrayList<PracticeResultSubCat>();
                for(int i = 0 ; i < subCatJsonArray.length() ; i ++ ){
                    JSONObject subCatJson = subCatJsonArray.getJSONObject(i);
                    PracticeResultSubCat subCat = new PracticeResultSubCat();
                    subCat.setSubCatId(subCatJson.getInt("subCategId"));
                    subCat.setProblems(subCatJson.getInt("problems"));
                    subCat.setScore(subCatJson.getString("score"));
                    subCat.setTime(subCatJson.getString("time"));
                    practiceSubCatList.add(subCat);
                }
                result.setPracticeResultSubCatList(practiceSubCatList);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            practiceResultList.add(result);
        }
        return practiceResultList;
    }

    /**
     * This method parse the word result
     * @param jsonObj
     * @return
     */
    private ArrayList<WordResult> parseWordResult(JSONObject jsonObj){

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside parseWordResult response " + jsonObj.toString());

        ArrayList<WordResult> wordResultList = new ArrayList<WordResult>();
        Iterator<?> iterator = jsonObj.keys();
        while(iterator.hasNext()){
            WordResult wordResult = new WordResult();
            String key = (String) iterator.next();
            wordResult.setCatIg(key);
            JSONArray subCatJsonArray;
            try {
                subCatJsonArray = jsonObj.getJSONArray(key);
                ArrayList<WordSubCatResult> wordSubCatList = new ArrayList<WordSubCatResult>();
                for(int i = 0 ; i < subCatJsonArray.length() ; i ++ ){
                    JSONObject subCatJson = subCatJsonArray.getJSONObject(i);
                    WordSubCatResult subCat = new WordSubCatResult();
                    subCat.setSubCatId(subCatJson.getInt("subCategId"));
                    subCat.setTime(subCatJson.getString("time"));
                    subCat.setScore(subCatJson.getString("score"));
                    wordSubCatList.add(subCat);
                }
                wordResult.setSubCatResultList(wordSubCatList);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            wordResultList.add(wordResult);
        }
        return wordResultList;
    }

    /**
     * This method parse the custome result
     * @param jsonObj
     * @return
     */
    private CustomeResult parseCustomeResult(JSONObject jsonObj){

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside parseCustomeResult response " + jsonObj.toString());

        CustomeResult customeResult = new CustomeResult();
        try{
            int numberOfOpponentInput = 0;
            customeResult.setCustomHwId(jsonObj.getInt("customHwId"));
            customeResult.setTitle(jsonObj.getString("title"));
            customeResult.setProblem(jsonObj.getInt("problems"));
            customeResult.setAllowChanges(jsonObj.getInt("allowChanges"));
            customeResult.setShowAns(jsonObj.getInt("showAnswers"));
            customeResult.setScore(jsonObj.getString("score"));
            customeResult.setSheetName(jsonObj.getString("questionSheet"));
            customeResult.setResourceAdded(jsonObj.getString("resourceAdded"));

            if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObj , "links")){
                customeResult.setLinks(this.getUrlLinks(jsonObj.getString("links")));
            }

            //this.addPdfNameToList(jsonObj.getString("questionSheet"));
            PDFOperation.downloadPdf(jsonObj.getString("questionSheet"));

            ArrayList<CustomeAns> customeAnsList = new ArrayList<CustomeAns>();
            JSONArray cutomeJsonAnsArray = jsonObj.getJSONArray("answers");
            for(int i = 0 ; i < cutomeJsonAnsArray.length() ; i ++ ){
                JSONObject customeAnsJson = cutomeJsonAnsArray.getJSONObject(i);
                CustomeAns customeAns = new CustomeAns();
                customeAns.setQueNo(customeAnsJson.getString("quesNum"));
                customeAns.setOptions(customeAnsJson.getString("options"));
                customeAns.setCorrectAns(customeAnsJson.getString("correctAns"));
                customeAns.setAnsSuffix(customeAnsJson.getString("ansSuffix"));
                customeAns.setIsLeftUnit(customeAnsJson.getInt("isLeftUnit"));
                customeAns.setFillInType(customeAnsJson.getInt("fillInType"));
                customeAns.setAnswerType(customeAnsJson.getInt("answerType"));
                try {
                    if (MathFriendzyHelper.checkForKeyExistInJsonObj
                            (customeAnsJson, "ansAvailable")) {
                        customeAns.setIsAnswerAvailable(customeAnsJson.getInt("ansAvailable"));
                    } else {
                        customeAns.setIsAnswerAvailable(MathFriendzyHelper.YES);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                    customeAns.setIsAnswerAvailable(MathFriendzyHelper.YES);
                }
                customeAns.setQuestionString(customeAnsJson.getString("questionStr"));
                customeAnsList.add(customeAns);
            }
            customeResult.setCustomeAnsList(customeAnsList);

            ArrayList<CustomePlayerAns> playerAnsList = new ArrayList<CustomePlayerAns>();
            JSONArray customePlayerAnsArray = jsonObj.getJSONArray("playerAns");
            for(int i = 0 ; i < customePlayerAnsArray.length() ; i ++ ){
                JSONObject customePlayerAnsJson = customePlayerAnsArray.getJSONObject(i);
                CustomePlayerAns playerAns = new CustomePlayerAns();
                playerAns.setQueNo(customePlayerAnsJson.getString("quesNum"));
                playerAns.setAns(customePlayerAnsJson.getString("answer"));
                playerAns.setIsCorrect(customePlayerAnsJson.getInt("isCorrect"));
                playerAns.setWorkImage(customePlayerAnsJson.getString("workImage"));
                playerAns.setFirstTimeWrong(customePlayerAnsJson.getInt("firstTimeWrong"));
                playerAns.setTeacherCredit(customePlayerAnsJson.getString("teacher_credit"));
                playerAns.setMessage(customePlayerAnsJson.getString("message"));
                playerAns.setIsGetHelp(customePlayerAnsJson.getInt("getHelp"));
                playerAns.setIsWorkAreaPublic(customePlayerAnsJson.getInt("workAreaPublic"));
                playerAns.setQuestionImage(customePlayerAnsJson.getString("questionImage"));
                playerAns.setChatRequestedId(customePlayerAnsJson.getInt("chatRequestId"));
                playerAns.setFixedWorkImage(customePlayerAnsJson.getString("fixedWorkImage"));
                playerAns.setFixedMessage(customePlayerAnsJson.getString("fixedMessage"));
                playerAns.setFixedQuestionImage(customePlayerAnsJson.getString("fixedQuestionImage"));
                playerAns.setWorkInput(customePlayerAnsJson.getString("workInput"));
                playerAns.setOpponentInput(customePlayerAnsJson.getString("opponentInput"));
                if(MathFriendzyHelper.checkForKeyExistInJsonObj(customePlayerAnsJson , "links"))
                    playerAns.setUrlLinks(this.getUrlLinks(customePlayerAnsJson.getString("links")));
                //for active inactive counter
                if(playerAns.getWorkInput().equalsIgnoreCase(MathFriendzyHelper.YES + ""))
                    numberOfOpponentInput ++;
                if(playerAns.getOpponentInput().equalsIgnoreCase(MathFriendzyHelper.YES + ""))
                    numberOfOpponentInput ++;
                playerAnsList.add(playerAns);
            }

            if(playerAnsList.size() > 0){
                customeResult.setAlreadyPlayed(true);
            }else{
                customeResult.setAlreadyPlayed(false);
            }
            customeResult.setNumberOfActiveInput(numberOfOpponentInput);
            customeResult.setCustomePlayerAnsList(playerAnsList);
        }catch(Exception e){
            e.printStackTrace();
        }
        return customeResult;
    }

    /**
     * This method check for the resultant data is in the form of Array and Object
     * @param obj
     * @return
     */
    private boolean isJsonArray(Object obj){
        if(obj instanceof JSONObject){
            return false;
        }else if(obj instanceof JSONArray){
            return true;
        }
        return false;
    }

    /**
     * This method parse the save homework score on server result
     * @param jsonString
     * @return
     */
    private SaveHomeWorkServerResponse parseSaveHomeWorkScoreJsonResponse(String jsonString){

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside parseSaveHomeWorkScoreJsonResponse " + jsonString);

        SaveHomeWorkServerResponse saveHomeWorkResponse = new SaveHomeWorkServerResponse();
        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            saveHomeWorkResponse.setResult(jsonObj.getString("result"));
            JSONObject jsonData = jsonObj.getJSONObject("data");
            saveHomeWorkResponse.setFinished(jsonData.getString("finished"));
            saveHomeWorkResponse.setPracticeTime(jsonData.getString("practiceTime"));
            saveHomeWorkResponse.setPracticeScore(jsonData.getString("practiceScore"));
            saveHomeWorkResponse.setWordTime(jsonData.getString("wordTime"));
            saveHomeWorkResponse.setWordScore(jsonData.getString("wordScore"));
            saveHomeWorkResponse.setCustomeScore(jsonData.getString("customScore"));
            saveHomeWorkResponse.setAvgScore(jsonData.getString("avgScore"));
            saveHomeWorkResponse.setZeroCredit(jsonData.getString("zeroCredit"));
            saveHomeWorkResponse.setHalfCredit(jsonData.getString("halfCredit"));
            saveHomeWorkResponse.setFullCredit(jsonData.getString("fullCredit"));
            saveHomeWorkResponse.setTotalQuestion(jsonData.getString("totalQuestions"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return saveHomeWorkResponse;
    }

    /**
     * This method parse the SaveHomeWorkForWordPlayScoreJsonResponse
     * @param jsonString
     * @return
     */
	/*private SaveHomeWorkServerResponse parseSaveHomeWorkForWordPlayScoreJsonResponse(String jsonString){
		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside parseSaveHomeWorkForWordPlayScoreJsonResponse " + jsonString);
		SaveHomeWorkServerResponse saveHomeWorkResponse = new SaveHomeWorkServerResponse();
		return saveHomeWorkResponse;
	}*/

	/*private void saveCustomeHWAnsByStudentResponse(String jsonString){
		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside saveCustomeHWAnsByStudentResponse " + jsonString);
	}*/

    /**
     * Parse the student help response
     * @param jsonString
     * @return
     */
    private GetHelpStudentListResponse parseGetHelpStudentList(String jsonString){
        GetHelpStudentListResponse response = new GetHelpStudentListResponse();
        ArrayList<GetHelpStudentListResponse> studentList = new ArrayList<GetHelpStudentListResponse>();

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside parseGetHelpStudentList response " + jsonString);

        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            JSONArray dataArray = jsonObj.getJSONArray("data");
            for(int i = 0 ; i < dataArray.length() ; i ++ ){
                JSONObject object = dataArray.getJSONObject(i);
                GetHelpStudentListResponse studentObj = new GetHelpStudentListResponse();
                studentObj.setUserId(object.getString("uid"));
                studentObj.setPlayerId(object.getString("pid"));
                studentObj.setfName(object.getString("fName"));
                studentObj.setlName(object.getString("lName"));
                studentObj.setRating(object.getInt("rating"));
                studentObj.setWorkImageName(object.getString("workImage"));
                studentObj.setStudentTeacherMsg(object.getString("message"));
                studentObj.setYourRating(object.getInt("yourRating"));
                studentObj.setQuestionImage(object.getString("questionImage"));
                studentList.add(studentObj);
            }
            response.setStudentList(studentList);
            return response;
        } catch (JSONException e) {
            return null;
        }
    }

    /**
     * Parse the student rating json response
     * @param jsonString
     * @return
     */
    private SaveStudentRatingResponse parseStudentSaveRatingResponse(String jsonString){

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside parseStudentSaveRatingResponse " + jsonString);

        SaveStudentRatingResponse response = new SaveStudentRatingResponse();

        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));
            response.setAvgRatingStar(jsonObj.getInt("rating"));
            return response;
        } catch (JSONException e) {
            return null;
        }
    }

    /**
     * Parse the get student by grade response
     * @param jsonString
     * @return
     */
    private GetStudentByGradeResponse parseGetStudentByGradeResponse(String jsonString){

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside parseGetStudentByGradeResponse " + jsonString);

        GetStudentByGradeResponse response = new GetStudentByGradeResponse();
        ArrayList<GetStudentByGradeResponse> studentList = new ArrayList<GetStudentByGradeResponse>();
        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            JSONArray array = jsonObj.getJSONArray("data");
            for(int i = 0 ; i < array.length() ; i ++ ){
                GetStudentByGradeResponse obj = new GetStudentByGradeResponse();
                JSONObject jsonDataObj = array.getJSONObject(i);
                obj.setfName(jsonDataObj.getString("fName"));
                obj.setlName(jsonDataObj.getString("lName"));
                obj.setParentUserId(jsonDataObj.getString("parentUserId"));
                obj.setPlayerId(jsonDataObj.getString("playerId"));
                obj.setProfileImageId(jsonDataObj.getString("profileImageId"));

                if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonDataObj , "classId")){
                    obj.setClassId(jsonDataObj.getInt("classId"));
                }
                obj.setSelected(true);//by default selected at the starting
                studentList.add(obj);
            }
            response.setStudentList(studentList);
            return response;
        } catch (JSONException e) {
            return null;
        }
    }

    /**
     * Parse the assign homework response
     * @param jsonString
     * @return
     */
    private AssignHomeworkResponse parseAssignHomeworkResponse(String jsonString){
        AssignHomeworkResponse response = new AssignHomeworkResponse();

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside parseAssignHomeworkResponse " + jsonString);

        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));
            response.setData(jsonObj.getString("data"));
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * 	Parse the GetHomeworksWithCustomForTeacherResponse response string
     * @param jsonString
     * @return
     */
    private GetHomeworksWithCustomForTeacherResponse
    parseGetHomeworksWithCustomForTeacherResponse(String jsonString){

        GetHomeworksWithCustomForTeacherResponse response =
                new GetHomeworksWithCustomForTeacherResponse();

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "parseGetHomeworksWithCustomForTeacherResponse " + jsonString);

        ArrayList<GetHomeworksWithCustomForTeacherResponse> homwworkList =
                new ArrayList<GetHomeworksWithCustomForTeacherResponse>();

        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));
            //new homework changes
            try {
                if (MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObj, "lastClassAssignedForHw")) {
                    response.setLastClassAssignedForHw(jsonObj.getInt("lastClassAssignedForHw"));
                }
                if (MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObj, "classes")) {
                    response.setClassArrayExist(true);
                    response.setClassWithNames(
                            this.parseClassesResponse(jsonObj.getJSONArray("classes")));
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            //end new homework changes

            JSONArray jsonArray = jsonObj.getJSONArray("data");
            for(int i = 0 ; i < jsonArray.length() ; i ++ ){
                JSONObject obj = jsonArray.getJSONObject(i);
                GetHomeworksWithCustomForTeacherResponse hwObj =
                        new GetHomeworksWithCustomForTeacherResponse();
                hwObj.setHomeworkId(obj.getString("homeworkId"));
                hwObj.setDate(obj.getString("date"));
                hwObj.setGrade(obj.getString("grade"));
                hwObj.setMessage(obj.getString("message"));

                try {
                    if (MathFriendzyHelper.checkForKeyExistInJsonObj(obj, "classes")) {
                        hwObj.setClassName(obj.getString("classes"));//classes list with comma separated for check homework like 1,2,3
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    hwObj.setClassName("");
                }

                /*hwObj.setPracticeScore(obj.getString("practiceScore"));
                hwObj.setWordScore(obj.getString("wordScore"));
                hwObj.setCustomScore(obj.getString("customScore"));
                hwObj.setAvgScore(obj.getString("avgScore"));*/

                if(this.isJsonArray(obj.get("practiceResult"))){
                    ArrayList<PracticeResult> practiceResultList = new ArrayList<PracticeResult>();
                    //set blank list
                    hwObj.setPractiseResultList(practiceResultList);
                }else{
                    hwObj.setPractiseResultList(this.parsePracticeResultForPrevHW
                            (obj.getJSONObject("practiceResult")));
                }

                if(this.isJsonArray(obj.get("wordResult"))){
                    ArrayList<WordResult> wordResultList = new ArrayList<WordResult>();
                    //set blank list
                    hwObj.setWordResultList(wordResultList);
                }else{
                    hwObj.setWordResultList(this.parseWordResultForPrevHW
                            (obj.getJSONObject("wordResult")));
                }

                hwObj.setCustomeresultList(this.parseCustomeResultForPrevHW
                        (obj.getJSONArray("customResult")));

                /*ArrayList<CustomeResult> customeResults = new ArrayList<>();
                if(this.isJsonArray(obj.get("customResult"))){
                    JSONArray cusJsonArray = obj.getJSONArray("customResult");
                    for(int j = 0 ; j < cusJsonArray.length() ; j ++ ){
                        JSONObject jsonObject = cusJsonArray.getJSONObject(j);
                        CustomeResult customeResult = new CustomeResult();
                        customeResult.setCustomHwId(jsonObject.getInt("customHwId"));
                        customeResult.setTitle(jsonObject.getString("title"));
                        customeResult.setProblem(jsonObject.getInt("problems"));
                        *//*customeResult.setAllowChanges(jsonObject.getInt("allowChanges"));
                        customeResult.setShowAns(jsonObject.getInt("showAnswers"));
                        customeResult.setSheetName(jsonObject.getString("questionSheet"));
                        customeResult.setResourceAdded(jsonObject.getString("resourceAdded"));*//*

                        ArrayList<CustomeAns> customeAnsList = new ArrayList<CustomeAns>();
                        JSONArray cutomeJsonAnsArray = jsonObject.getJSONArray("answers");
                        for(int k = 0 ; k < cutomeJsonAnsArray.length() ; k ++ ){
                            JSONObject customeAnsJson = cutomeJsonAnsArray.getJSONObject(k);
                            CustomeAns customeAns = new CustomeAns();
                            customeAns.setQueNo(customeAnsJson.getString("quesNum"));
                            customeAns.setOptions(customeAnsJson.getString("options"));
                            customeAns.setCorrectAns(customeAnsJson.getString("correctAns"));
                            customeAns.setAnsSuffix(customeAnsJson.getString("ansSuffix"));
                            customeAns.setIsLeftUnit(customeAnsJson.getInt("isLeftUnit"));
                            customeAns.setFillInType(customeAnsJson.getInt("fillInType"));
                            customeAns.setAnswerType(customeAnsJson.getInt("answerType"));
                            try {
                                if (MathFriendzyHelper.checkForKeyExistInJsonObj
                                        (customeAnsJson, "ansAvailable")) {
                                    customeAns.setIsAnswerAvailable(customeAnsJson.getInt("ansAvailable"));
                                } else {
                                    customeAns.setIsAnswerAvailable(MathFriendzyHelper.YES);
                                }
                            }catch(Exception e){
                                e.printStackTrace();
                                customeAns.setIsAnswerAvailable(MathFriendzyHelper.YES);
                            }
                            customeAnsList.add(customeAns);
                        }
                        customeResult.setCustomeAnsList(customeAnsList);
                        customeResults.add(customeResult);
                    }
                }
                hwObj.setCustomeresultList(customeResults);*/
                homwworkList.add(hwObj);
            }
            response.setHomeworkWithCustomeList(homwworkList);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * This method parse the custome result
     * @param jsonObj
     * @param customeresultCatgoriesList
     * @return
     */
    private CustomeResult parseCustomeResultForCheckHW(JSONObject jsonObj,
                                                       ArrayList<CustomeResult> customeresultCatgoriesList){

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside parseCustomeResult response " + jsonObj.toString());

        CustomeResult customeResult = new CustomeResult();
        try{

            customeResult.setCustomHwId(jsonObj.getInt("customHwId"));
            customeResult.setScore(jsonObj.getString("score"));

            ArrayList<CustomePlayerAns> playerAnsList = new ArrayList<CustomePlayerAns>();
            JSONArray customePlayerAnsArray = jsonObj.getJSONArray("playerAns");
            for(int i = 0 ; i < customePlayerAnsArray.length() ; i ++ ){
                JSONObject customePlayerAnsJson = customePlayerAnsArray.getJSONObject(i);
                CustomePlayerAns playerAns = new CustomePlayerAns();
                playerAns.setQueNo(customePlayerAnsJson.getString("quesNum"));
                playerAns.setAns(customePlayerAnsJson.getString("answer"));
                playerAns.setIsCorrect(customePlayerAnsJson.getInt("isCorrect"));
                playerAns.setWorkImage(customePlayerAnsJson.getString("workImage"));
                playerAns.setFirstTimeWrong(customePlayerAnsJson.getInt("firstTimeWrong"));
                playerAns.setTeacherCredit(customePlayerAnsJson.getString("teacher_credit"));
                playerAns.setMessage(customePlayerAnsJson.getString("message"));
                playerAns.setIsGetHelp(customePlayerAnsJson.getInt("getHelp"));
                playerAns.setIsWorkAreaPublic(customePlayerAnsJson.getInt("workAreaPublic"));
                playerAns.setQuestionImage(customePlayerAnsJson.getString("questionImage"));

                //for new changes , for second work area tab
                playerAns.setChatRequestedId(customePlayerAnsJson.getInt("chatRequestId"));
                playerAns.setFixedWorkImage(customePlayerAnsJson.getString("fixedWorkImage"));
                playerAns.setFixedMessage(customePlayerAnsJson.getString("fixedMessage"));
                playerAns.setFixedQuestionImage(customePlayerAnsJson.getString("fixedQuestionImage"));
                playerAns.setWorkInput(customePlayerAnsJson.getString("workInput"));
                playerAnsList.add(playerAns);
            }

            CustomeResult resultById = this.getCustomeResultObjByHWID(customeResult.getCustomHwId(),
                    customeresultCatgoriesList);
            if(resultById != null){
                customeResult.setTitle(resultById.getTitle());
                customeResult.setProblem(resultById.getProblem());
                customeResult.setAllowChanges(resultById.getAllowChanges());
                customeResult.setShowAns(resultById.getShowAns());
                customeResult.setSheetName(resultById.getSheetName());
                customeResult.setCustomeAnsList(resultById.getCustomeAnsList());
                customeResult.setLinks(resultById.getLinks());
                customeResult.setResourceAdded(resultById.getResourceAdded());
            }

            if(playerAnsList.size() > 0){
                customeResult.setAlreadyPlayed(true);
            }else{
                customeResult.setAlreadyPlayed(false);
            }
            customeResult.setCustomePlayerAnsList(playerAnsList);
        }catch(Exception e){
            e.printStackTrace();
        }
        return customeResult;
    }

    /**
     * Return the custom homework result obj by customeHW Id
     * @param hwId
     * @param customeresultCatgoriesList
     * @return
     */
    private CustomeResult getCustomeResultObjByHWID(int hwId ,
                                                    ArrayList<CustomeResult> customeresultCatgoriesList){
        if(customeresultCatgoriesList != null && customeresultCatgoriesList.size() > 0){
            for(int i = 0 ; i < customeresultCatgoriesList.size() ; i ++ ){
                if(hwId == customeresultCatgoriesList.get(i).getCustomHwId()){
                    return customeresultCatgoriesList.get(i);
                }
            }
            return null;
        }else{
            return null;
        }
    }

    /**
     * This method parse the custome result
     * @param jsonObj
     * @return
     */
    private CustomeResult parseCustomeResultForCheckHomwworkForCat(JSONObject jsonObj){

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside parseCustomeResult response " + jsonObj.toString());

        CustomeResult customeResult = new CustomeResult();
        try{

            customeResult.setCustomHwId(jsonObj.getInt("customHwId"));
            customeResult.setTitle(jsonObj.getString("title"));
            customeResult.setProblem(jsonObj.getInt("problems"));
            customeResult.setAllowChanges(jsonObj.getInt("allowChanges"));
            customeResult.setShowAns(jsonObj.getInt("showAnswers"));
            customeResult.setSheetName(jsonObj.getString("questionSheet"));

            if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObj , "links")){
                customeResult.setLinks(this.getUrlLinks(jsonObj.getString("links")));
            }

            if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObj , "resourceAdded")){
                customeResult.setResourceAdded(jsonObj.getString("resourceAdded"));
            }

            ArrayList<CustomeAns> customeAnsList = new ArrayList<CustomeAns>();
            JSONArray cutomeJsonAnsArray = jsonObj.getJSONArray("answers");
            for(int i = 0 ; i < cutomeJsonAnsArray.length() ; i ++ ){
                JSONObject customeAnsJson = cutomeJsonAnsArray.getJSONObject(i);
                CustomeAns customeAns = new CustomeAns();
                customeAns.setQueNo(customeAnsJson.getString("quesNum"));
                customeAns.setOptions(customeAnsJson.getString("options"));
                customeAns.setCorrectAns(customeAnsJson.getString("correctAns"));
                customeAns.setAnsSuffix(customeAnsJson.getString("ansSuffix"));
                customeAns.setIsLeftUnit(customeAnsJson.getInt("isLeftUnit"));
                customeAns.setFillInType(customeAnsJson.getInt("fillInType"));
                customeAns.setAnswerType(customeAnsJson.getInt("answerType"));

                try {
                    if (MathFriendzyHelper.checkForKeyExistInJsonObj
                            (customeAnsJson, "ansAvailable")) {
                        customeAns.setIsAnswerAvailable(customeAnsJson.getInt("ansAvailable"));
                    } else {
                        customeAns.setIsAnswerAvailable(MathFriendzyHelper.YES);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                    customeAns.setIsAnswerAvailable(MathFriendzyHelper.YES);
                }

                customeAns.setQuestionString(customeAnsJson.getString("questionStr"));
                customeAnsList.add(customeAns);
            }
            customeResult.setCustomeAnsList(customeAnsList);
        }catch(Exception e){
            e.printStackTrace();
        }
        return customeResult;
    }

    /**
     * parse GetDetailOfHomeworkWithCustomeResponse Response
     * @param jsonString
     * @return
     */
    private GetDetailOfHomeworkWithCustomeResponse
    parseGetDetailOfHomeworkWithCustomeResponseResponse(String jsonString){
        GetDetailOfHomeworkWithCustomeResponse response = new GetDetailOfHomeworkWithCustomeResponse();

        ArrayList<GetDetailOfHomeworkWithCustomeResponse> responseList =
                new ArrayList<GetDetailOfHomeworkWithCustomeResponse>();
        ArrayList<CustomeResult>  customeresultCatgoriesList = new ArrayList<CustomeResult>();

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside parseGetDetailOfHomeworkWithCustomeResponseResponse " + jsonString);

        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            JSONArray jsonCustomeCatArray = jsonObj.getJSONArray("customCategories");
            for(int i = 0 ; i < jsonCustomeCatArray.length() ; i ++ ){
                customeresultCatgoriesList.add
                        (this.parseCustomeResultForCheckHomwworkForCat
                                (jsonCustomeCatArray.getJSONObject(i)));
            }

            JSONArray jsonStudentArray = jsonObj.getJSONArray("students");
            for(int i = 0 ; i < jsonStudentArray.length() ; i ++ ){
                JSONObject jsonDataObj = jsonStudentArray.getJSONObject(i);
                GetDetailOfHomeworkWithCustomeResponse detailObj =
                        new GetDetailOfHomeworkWithCustomeResponse();
                detailObj.setParentId(jsonDataObj.getString("parentId"));
                detailObj.setPlayerId(jsonDataObj.getString("playerId"));
                detailObj.setfName(jsonDataObj.getString("fName"));
                detailObj.setlName(jsonDataObj.getString("lName"));
                detailObj.setChatId(jsonDataObj.getString("chatId"));
                detailObj.setChatUserName(jsonDataObj.getString("chatUserName"));
                detailObj.setPracticeTime(jsonDataObj.getString("practiceTime"));
                detailObj.setPracticeScore(jsonDataObj.getString("practiceScore"));
                detailObj.setWordTime(jsonDataObj.getString("wordTime"));
                detailObj.setWordScore(jsonDataObj.getString("wordScore"));
                detailObj.setCustomeScore(jsonDataObj.getString("customScore"));
                detailObj.setAvgScore(jsonDataObj.getString("avgScore"));
                detailObj.setZeroCredit(jsonDataObj.getString("zeroCredit"));
                detailObj.setHalfCredit(jsonDataObj.getString("halfCredit"));
                detailObj.setFullCredit(jsonDataObj.getString("fullCredit"));
                detailObj.setTotalQuestions(jsonDataObj.getString("totalQuestions"));

                if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonDataObj , "classId")){
                    try{
                        detailObj.setClassId(jsonDataObj.getInt("classId"));
                    }catch (Exception e){
                        detailObj.setClassId(0);
                        e.printStackTrace();
                    }
                }

                if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonDataObj , "className")){
                    try{
                        detailObj.setClassName(jsonDataObj.getString("className"));
                    }catch (Exception e){
                        detailObj.setClassName("");
                        e.printStackTrace();
                    }
                }

                if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonDataObj , "subjectId")){
                    try{
                        detailObj.setSubjectId(jsonDataObj.getInt("subjectId"));
                    }catch (Exception e){
                        detailObj.setSubjectId(MathFriendzyHelper.MATH_SUBJECT_ID);
                        e.printStackTrace();
                    }
                }

                if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonDataObj , "subjectName")){
                    try{
                        detailObj.setSubjectName(jsonDataObj.getString("subjectName"));
                    }catch (Exception e){
                        detailObj.setSubjectName("");
                        e.printStackTrace();
                    }
                }

                if(this.isJsonArray(jsonDataObj.get("practiceResult"))){
					/*JSONArray practiceArray = jsonDataObj.getJSONArray("practiceResult");
					for(int j = 0 ; j < practiceArray.length() ; j ++ ){
						practiceResultList.add(this.parsePracticeResult(practiceArray.getJSONObject(j)));
					}*/
                    ArrayList<PracticeResult> practiceResultList = new ArrayList<PracticeResult>();
                    //set blank list
                    detailObj.setPractiseResultList(practiceResultList);
                }else{
                    detailObj.setPractiseResultList(this.parsePracticeResult(jsonDataObj.getJSONObject("practiceResult")));
                    //practiceResultList.add(this.parsePracticeResult(jsonDataObj.getJSONObject("practiceResult")))	;
                }


                if(this.isJsonArray(jsonDataObj.get("wordResult"))){
                    ArrayList<WordResult> wordResultList = new ArrayList<WordResult>();
                    //set blank list
                    detailObj.setWordResultList(wordResultList);
                }else{
                    detailObj.setWordResultList(this.parseWordResult(jsonDataObj.getJSONObject("wordResult")));
                }

                ArrayList<CustomeResult> customeResultList = new ArrayList<CustomeResult>();
                if(this.isJsonArray(jsonDataObj.get("customResult"))){
                    JSONArray customeArray = jsonDataObj.getJSONArray("customResult");
                    for(int j = 0 ; j < customeArray.length() ; j ++ ){
                        customeResultList.add
                                (this.parseCustomeResultForCheckHW
                                        (customeArray.getJSONObject(j) , customeresultCatgoriesList));
                    }
                }else{
                    customeResultList.add
                            (this.parseCustomeResultForCheckHW
                                    (jsonDataObj.getJSONObject("customResult") , customeresultCatgoriesList));
                }

                detailObj.setCustomeresultList(customeResultList);
                responseList.add(detailObj);
            }

            response.setResposeList(responseList);
            //response.setCustomeresultCatgoriesList(customeresultCatgoriesList);
            return response;
        } catch (JSONException e) {
            return null;
        }
    }

    /**
     * Parse the save teacher changes for homework
     * @param jsonString
     * @return
     */
	/*private SaveCheckTeacherChangesResponse
	parseSaveCheckTeacherChangesResponse(String jsonString){
		SaveCheckTeacherChangesResponse response = new SaveCheckTeacherChangesResponse();
		if(CommonUtils.LOG_ON)
			Log.e(TAG, "inside parseSaveCheckTeacherChangesResponse response " + jsonString);
		return response;
	}*/

    /**
     * Parse the Josn response for SavePlayDataWhenNoInternet
     * @param jsonString
     * @return
     */
    private HttpResponseBase parseSavePlayDataWhenNoInternet(String jsonString){
        HttpResponseBase httpResponseBase = new HttpResponseBase();
        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside parseSavePlayDataWhenNoInternet response " + jsonString);
        return httpResponseBase;
    }

    /**
     * Parse the get work area chat message response
     * @param jsonString
     * @return
     */
    private GetWorkAreaChatResponse parseWorkAreaChatMessage(String jsonString){
        GetWorkAreaChatResponse response = new GetWorkAreaChatResponse();

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside parseWorkAreaChatMessage response " + jsonString);

        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));
            response.setData(jsonObj.getString("data"));
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the save work area chat message response
     * @param jsonString
     * @return
     */
    private HttpResponseBase parseSaveWorkAreaChatMessage(String jsonString){
        HttpResponseBase httpResponseBase = new HttpResponseBase();
        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside parseSaveWorkAreaChatMessage response " + jsonString);
        return httpResponseBase;
    }

    /**
     * Parse the find tutor response json string and return it
     * @param jsonString
     * @return
     */
    private FindTutorResponse parseGetTutorByUserNameJsonString(String jsonString){
        FindTutorResponse response = new FindTutorResponse();

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside parseGetTutorByUserNameJsonString response " + jsonString);

        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));
            JSONObject jsonData = jsonObj.getJSONObject("data");
            if(this.isTutorFound(jsonData)){
                response.setIsTutorFind(true);
                response.setChatId(jsonData.getString("chatId"));
                response.setChatUserName(jsonData.getString("chatUserName"));
                response.setCity(jsonData.getString("city"));
                response.setCoins(jsonData.getString("coins"));
                response.setCompleteLevel(jsonData.getString("competeLevel"));
                response.setfName(jsonData.getString("fName"));
                response.setGrade(jsonData.getString("grade"));
                response.setIndexOfAppereance(jsonData.getString("indexOfAppearance"));
                response.setIsTutor(jsonData.getInt("isTutor"));
                response.setLname(jsonData.getString("lName"));
                response.setParentUserId(jsonData.getString("parentUserId"));
                response.setPlayerId(jsonData.getString("playerId"));
                response.setPoints(jsonData.getString("points"));
                response.setSchoolId(jsonData.getString("schoolId"));
                response.setSchoolName(jsonData.getString("schoolName"));
                response.setState(jsonData.getString("state"));
                response.setTeacherFirstName(jsonData.getString("teacherFirstName"));
                response.setTeacherLastName(jsonData.getString("teacherLastName"));
                response.setTeacherUserId(jsonData.getString("teacherUserId"));
                response.setUserName(jsonData.getString("userName"));
            }else{
                response.setIsTutorFind(false);
            }
            response.setDeviceIds(jsonObj.getString("deviceIds"));
            response.setAndroidDeviceIds(jsonObj.getString("androidIds"));
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }


    @SuppressWarnings("unchecked")
    private boolean isTutorFound(JSONObject jsonObj){
        Iterator<Object> keys = jsonObj.keys();
        if(keys.hasNext())
            return true;
        return false;
    }

    /**
     * Parse the annonymous tutor response
     * @param jsonString
     * @return
     */
    private GetAnnonymousTutorResponse parseAnnonymousTutorResponse(String jsonString){
        GetAnnonymousTutorResponse response = new GetAnnonymousTutorResponse();

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside parseAnnonymousTutorResponse response " + jsonString);

        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));
            response.setMessage(jsonObj.getString("msg"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return response;
    }

    /**
     * Parse the get new chat user name response
     * @param jsonString
     */
    private GetNewChatUserNameResponse parseGetNewChatUserName(String jsonString){

        GetNewChatUserNameResponse response = new GetNewChatUserNameResponse();
        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside parseGetNewChatUserName response " + jsonString);

        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));
            response.setChatUserName(jsonObj.getString("data"));
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * Parse the get chat response
     * @param jsonString
     * @return
     */
    private GetChatRequestResponse parseGetChatRequestResponse(String jsonString) {
        GetChatRequestResponse chatResponse = new GetChatRequestResponse();

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseGetChatRequestResponse response " + jsonString);

        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            chatResponse.setResult(jsonObj.getString("result"));
            JSONArray jsonArray = jsonObj.getJSONArray("data");
            if(jsonArray.length() > 0){
                JSONObject jsonData = jsonArray.getJSONObject(0);
                chatResponse.setChatDialogId(jsonData.getString("chatDialogId"));
                chatResponse.setIsAnonymous(jsonData.getInt("isAnonymous"));
                chatResponse.setIsConnected(jsonData.getInt("isConnected"));
                chatResponse.setMessage(jsonData.getString("message"));
                chatResponse.setPlayerId(jsonData.getString("playerId"));
                chatResponse.setRating(jsonData.getInt("rating"));
                chatResponse.setReqDate(jsonData.getString("reqDate"));
                chatResponse.setRequestId(jsonData.getInt("requestId"));
                chatResponse.setTimeSpent(jsonData.getString("timeSpent"));
                chatResponse.setTutorName(jsonData.getString("tutorName"));
                chatResponse.setTutorPid(jsonData.getInt("tutorPid"));
                chatResponse.setTuturUid(jsonData.getInt("tutorUid"));
                chatResponse.setUserId(jsonData.getString("userId"));
                chatResponse.setDisconnected(jsonData.getInt("disconnected"));

                //new param
                chatResponse.setStudentImage(jsonData.getString("studentImage"));
                chatResponse.setTutorImage(jsonData.getString("tutorImage"));
                chatResponse.setWorkAreaImage(jsonData.getString("workAreaImage"));
                chatResponse.setLastUpdateByTutor(jsonData.getInt("lastUpdateByTutor"));
                chatResponse.setOpponentInput(jsonData.getInt("opponentInput"));
            }
            return chatResponse;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the send message to anonymous tutor
     * @param jsonString
     * @return
     */
    private SendMessageToTutorResponse parseSendMessageToAnonymousTutorResponse(String jsonString) {

        SendMessageToTutorResponse response = new SendMessageToTutorResponse();
        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside parseSendMessageToAnonymousTutorResponse response " + jsonString);

        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));
            response.setRequestId(jsonObj.getInt("requestId"));
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the get player need help for tutor
     * @param jsonString
     * @return
     */
    private GetPlayerNeedHelpForTutorResponse parseGetPlayerNeedHelpForTutorResponse(String jsonString) {
        GetPlayerNeedHelpForTutorResponse response = new GetPlayerNeedHelpForTutorResponse();
        ArrayList<GetPlayerNeedHelpForTutorResponse> dataList = new ArrayList<GetPlayerNeedHelpForTutorResponse>();
        ArrayList<GetPlayerNeedHelpForTutorResponse> connectedTutorList = new ArrayList<GetPlayerNeedHelpForTutorResponse>();
        ArrayList<GetPlayerNeedHelpForTutorResponse> notConnectedTutorList = new ArrayList<GetPlayerNeedHelpForTutorResponse>();


        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseGetPlayerNeedHelpForTutorResponse response " + jsonString);

        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResponse(jsonObj.getString("result"));
            JSONArray jsonDataArray =  jsonObj.getJSONArray("data");
            for(int i = 0 ; i < jsonDataArray.length() ; i ++ ){
                GetPlayerNeedHelpForTutorResponse data = new GetPlayerNeedHelpForTutorResponse();
                JSONObject jsonData = jsonDataArray.getJSONObject(i);
                data.setRequestId(jsonData.getInt("requestId"));
                data.setUserId(jsonData.getString("userId"));
                data.setPlayerId(jsonData.getString("playerId"));
                data.setPlayerFName(jsonData.getString("playerFname"));
                data.setPlayerLName(jsonData.getString("playerLname"));
                data.setGrade(jsonData.getInt("grade"));
                data.setChatId(jsonData.getString("chatId"));
                data.setChatUserName(jsonData.getString("chatUname"));
                data.setReqDate(jsonData.getString("reqDate"));
                data.setMessage(jsonData.getString("message"));
                data.setIsConnected(jsonData.getInt("isConnected"));
                data.setTutorUid(jsonData.getString("tutorUid"));
                data.setTutorPid(jsonData.getString("tutorPid"));
                data.setChatDialogId(jsonData.getString("chatDialogId"));
                data.setIsAnonymous(jsonData.getInt("isAnonymous"));
                data.setTimeSpent(jsonData.getInt("timeSpent"));
                data.setStars(jsonData.getInt("rating"));

                data.setInActive(jsonData.getInt("inactive"));
                data.setStudentImage(jsonData.getString("studentImage"));
                data.setTutorImage(jsonData.getString("tutorImage"));
                data.setWorkAreaImage(jsonData.getString("workAreaImage"));
                data.setLastUpdatedByTutor(jsonData.getString("lastUpdateByTutor"));
                data.setDisconnected(jsonData.getInt("disconnected"));
                data.setOpponentInput(jsonData.getInt("opponentInput"));

                if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonData , "paidSession")){
                    data.setPaidSession(jsonData.getString("paidSession"));
                }else{
                    data.setPaidSession("0");
                }

                try {
                    if (MathFriendzyHelper.checkForKeyExistInJsonObj(jsonData, "studentOnline")) {
                        data.setStudentOnline(jsonData.getInt("studentOnline"));
                    } else {
                        data.setStudentOnline(0);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                    data.setStudentOnline(0);
                }

                if(data.getIsConnected() == 1){//1 for connected tutor
                    connectedTutorList.add(data);
                }else{
                    notConnectedTutorList.add(data);
                }
                dataList.add(data);
            }
            response.setConnectedTutorList(connectedTutorList);
            response.setNotConnectedTutorList(notConnectedTutorList);
            response.setTutorList(dataList);
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the connect with tutor response
     * @param jsonString
     * @return
     */
    private ConnectWithTutorResponse parseConnectWithTutorResponse(String jsonString) {
        ConnectWithTutorResponse response = new ConnectWithTutorResponse();
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseConnectWithTutorResponse response " + jsonString);
        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the get chat request id for problem
     * @param jsonString
     * @return
     */
    private GetChatRequestIdForProblemResponse parseGetChatRequestIdForProblem(String jsonString) {
        GetChatRequestIdForProblemResponse response = new GetChatRequestIdForProblemResponse();

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseGetChatRequestIdForProblem response " + jsonString);
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            response.setResult(jsonObject.getString("result"));
            response.setChatId(jsonObject.getInt("chatId"));
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the update chat request with dialog response
     * @param jsonString
     * @return
     */
    private UpdateChatWithDialogResponse parseUpdateChatRequestWithDialogResponse(String jsonString) {
        UpdateChatWithDialogResponse response  = new UpdateChatWithDialogResponse();
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseUpdateChatRequestWithDialogResponse response " + jsonString);
        return response;
    }

    /**
     * Parse the save single chat message response
     * @param jsonString
     * @return
     */
    private SaveMessageForSingleChatResponse parseSingleChatResponse(String jsonString) {
        SaveMessageForSingleChatResponse response = new SaveMessageForSingleChatResponse();
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseSingleChatResponse response " + jsonString);
        return response;
    }


    /**
     * Parse the update draw points for player response
     * @param jsonString
     * @return
     */
    private HttpResponseBase parseTheUpdateDrawPointsForPlayerResponse(String jsonString) {
        HttpResponseBase response = new HttpResponseBase();
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseTheUpdateDrawPointsForPlayerResponse response " + jsonString);
        return  response;
    }


    /**
     * parse the get drawing points for chat from server
     * @param jsonString
     * @return
     */
    private GetDrawingPointForChatResponse parseGetDrawingPointForChat(String jsonString) {
        GetDrawingPointForChatResponse response = new GetDrawingPointForChatResponse();
        ArrayList<GetDrawingPointForChatResponse> dataList = new ArrayList<GetDrawingPointForChatResponse>();

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseGetDrawingPointForChat response " + jsonString);

        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));
            JSONArray jsonArray = jsonObj.getJSONArray("data");
            for(int i = 0 ; i < jsonArray.length() ; i ++ ){
                JSONObject jsonDataObj = jsonArray.getJSONObject(i);
                GetDrawingPointForChatResponse data = new GetDrawingPointForChatResponse();
                data.setPlayerId(jsonDataObj.getString("playerId"));
                data.setDrawPoints(jsonDataObj.getString("drawnPoints"));
                data.setDeviceSize(jsonDataObj.getString("deviceSize"));
                dataList.add(data);
            }
            response.setDrawingList(dataList);
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * Parse the add time spent in tutor session response
     * @param jsonString
     * @return
     */
    private HttpResponseBase parseAddTimeSpentInTutorSessionResponse(String jsonString) {
        HttpResponseBase response = new HttpResponseBase();
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseAddTimeSpentInTutorSessionResponse response " + jsonString);
        return  response;
    }


    /**
     * Parse the rate tutor session response
     * @param jsonString
     * @return
     */
    private HttpResponseBase parseRateTutorSessionResponse(String jsonString) {
        HttpResponseBase response = new HttpResponseBase();
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseRateTutorSessionResponse response " + jsonString);
        return  response;
    }


    /**
     * Parse the get my students with password
     * @param jsonString
     * @return
     */
    private GetMyStuentWithPasswordResponse parseTheGetMyStudentWithPasswordResponse(String jsonString) {
        GetMyStuentWithPasswordResponse response  = new GetMyStuentWithPasswordResponse();
        ArrayList<String> gradeList = new ArrayList<String>();
        ArrayList<Integer> intGradeList = new ArrayList<>();
        gradeList.add(MathFriendzyHelper.ALL);

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseTheGetMyStudentWithPasswordResponse response " + jsonString);

        try{
            JSONObject jObject = new JSONObject(jsonString);
            response.setResponse(jObject.getString("result"));
            response.setStartDate(jObject.getString("startDate"));
            response.setEndDate(jObject.getString("endDate"));

            JSONArray jsonArray = jObject.getJSONArray("data");
            ArrayList<UserPlayerDto> userPlayer = new ArrayList<UserPlayerDto>();
            for( int i = 0 ; i < jsonArray.length() ; i ++ ) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                UserPlayerDto userPlayerDto = new UserPlayerDto();
                userPlayerDto.setPlayerid(jsonObject.getString("playerId"));
                userPlayerDto.setFirstname(jsonObject.getString("fName"));
                userPlayerDto.setLastname(jsonObject.getString("lName"));
                userPlayerDto.setGrade(jsonObject.getString("grade"));
                userPlayerDto.setTeacherUserId(jsonObject.getString("teacherUserId"));
                userPlayerDto.setParentUserId(jsonObject.getString("parentUserId"));
                userPlayerDto.setUsername(jsonObject.getString("userName"));
                userPlayerDto.setPassword(jsonObject.getString("password"));

                if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject , "isTutor"))
                    userPlayerDto.setIsTutor(jsonObject.getString("isTutor"));
                if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject , "chatId"))
                    userPlayerDto.setChatId(jsonObject.getString("chatId"));
                if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject , "chatUserName"))
                    userPlayerDto.setChatUserName(jsonObject.getString("chatUserName"));
                if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject , "tutoringTime"))
                    userPlayerDto.setTutoringTime(jsonObject.getInt("tutoringTime"));
                if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject , "rating"))
                    userPlayerDto.setRating(jsonObject.getInt("rating"));

                if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject , "subjects")) {
                    userPlayerDto.setSubjects(jsonObject.getString("subjects"));
                    userPlayerDto.setSubjectList(MathFriendzyHelper
                            .getCommaSepratedOptionInArrayList(userPlayerDto.getSubjects() , ","));
                }else{
                    userPlayerDto.setSubjects("");
                    userPlayerDto.setSubjectList(MathFriendzyHelper
                            .getCommaSepratedOptionInArrayList(userPlayerDto.getSubjects() , ","));
                }

                if(jsonObject.has("inactive"))
                    userPlayerDto.setInActive(jsonObject.getInt("inactive"));
                if(!intGradeList.contains(MathFriendzyHelper
                        .parseInt(userPlayerDto.getGrade()))){
                    intGradeList.add(MathFriendzyHelper.parseInt
                            (userPlayerDto.getGrade()));
                }

                if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject , "classId")){
                    try{
                        userPlayerDto.setClassId(jsonObject.getInt("classId"));
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                userPlayer.add(userPlayerDto);
            }
            Collections.sort(intGradeList);
            for(int i = 0 ; i < intGradeList.size() ; i ++){
                gradeList.add(intGradeList.get(i) + "");
            }
            response.setIntGradeList(intGradeList);
            response.setGradeList(gradeList);
            response.setUserPlayer(userPlayer);
            return response;
        }
        catch (JSONException e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the mark as tutor response
     * @param jsonString
     * @return
     */
    private HttpResponseBase parseTheMarkAsTutorResponse(String jsonString) {
        HttpResponseBase response = new HttpResponseBase();
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseTheMarkAsTutorResponse response " + jsonString);
        return  response;
    }

    /**
     *Parse the getStudentsTutoringDetailsForTeacher response
     * @param jsonString
     * @return
     */
    private GetStidentsTutoringDetailForTeacherResponse
    parseTheGetStudentsTutoringDetailsForTeacher(String jsonString) {

        GetStidentsTutoringDetailForTeacherResponse response  =
                new GetStidentsTutoringDetailForTeacherResponse();
        ArrayList<GetStidentsTutoringDetailForTeacherResponse> tutors
                = new ArrayList<GetStidentsTutoringDetailForTeacherResponse>();
        ArrayList<GetStidentsTutoringDetailForTeacherResponse> students
                = new ArrayList<GetStidentsTutoringDetailForTeacherResponse>();

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseTheMarkAsTutorResponse response " + jsonString);

        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            response.setResponse(jsonObject.getString("result"));
            JSONObject jsonDataObj = jsonObject.getJSONObject("data");
            JSONArray jsonTutorArray = jsonDataObj.getJSONArray("tutors");
            JSONArray jsonStudentArray = jsonDataObj.getJSONArray("students");

            for(int i = 0 ; i < jsonTutorArray.length() ; i ++ ){
                GetStidentsTutoringDetailForTeacherResponse tutor =
                        new GetStidentsTutoringDetailForTeacherResponse();
                JSONObject jsonTutor = jsonTutorArray.getJSONObject(i);
                tutor.setRequestId(jsonTutor.getInt("requestId"));
                tutor.setPlayerFName(jsonTutor.getString("playerFname"));
                tutor.setPlayerLName(jsonTutor.getString("playerLname"));
                tutor.setGrade(jsonTutor.getInt("grade"));
                tutor.setChatId(jsonTutor.getString("chatId"));
                tutor.setChatUserName(jsonTutor.getString("chatUname"));
                tutor.setReqDate(jsonTutor.getString("reqDate"));
                tutor.setMessage(jsonTutor.getString("message"));
                tutor.setIsConnected(jsonTutor.getInt("isConnected"));
                tutor.setTutorUid(jsonTutor.getString("tutorUid"));
                tutor.setTutorPid(jsonTutor.getString("tutorPid"));
                tutor.setChatDialogId(jsonTutor.getString("chatDialogId"));
                tutor.setIsAnonymous(jsonTutor.getInt("isAnonymous"));
                tutor.setRating(jsonTutor.getInt("rating"));
                tutor.setTimeSpent(jsonTutor.getInt("timeSpent"));
                tutor.setTeacherName(jsonTutor.getString("teacherName"));
                tutor.setSchoolName(jsonTutor.getString("schoolName"));
                tutor.setStudentImage(jsonTutor.getString("studentImage"));
                tutor.setTutorImage(jsonTutor.getString("tutorImage"));
                tutor.setTeacherEmail(jsonTutor.getString("teacherEmail"));
                tutor.setWorkAreaImageName(jsonTutor.getString("workAreaImage"));
                tutor.setLastUpdatedByTutor(jsonTutor.getString("lastUpdateByTutor"));
                tutors.add(tutor);
            }

            for(int i = 0 ; i < jsonStudentArray.length() ; i ++ ){
                GetStidentsTutoringDetailForTeacherResponse student =
                        new GetStidentsTutoringDetailForTeacherResponse();
                JSONObject jsonStudent = jsonStudentArray.getJSONObject(i);
                student.setRequestId(jsonStudent.getInt("requestId"));
                student.setPlayerFName(jsonStudent.getString("playerFname"));
                student.setPlayerLName(jsonStudent.getString("playerLname"));
                student.setGrade(jsonStudent.getInt("grade"));
                student.setChatId(jsonStudent.getString("chatId"));
                student.setChatUserName(jsonStudent.getString("chatUname"));
                student.setReqDate(jsonStudent.getString("reqDate"));
                student.setMessage(jsonStudent.getString("message"));
                student.setIsConnected(jsonStudent.getInt("isConnected"));
                student.setChatDialogId(jsonStudent.getString("chatDialogId"));
                student.setIsAnonymous(jsonStudent.getInt("isAnonymous"));
                student.setRating(jsonStudent.getInt("rating"));
                student.setTimeSpent(jsonStudent.getInt("timeSpent"));
                student.setTeacherName(jsonStudent.getString("teacherName"));
                student.setSchoolName(jsonStudent.getString("schoolName"));
                student.setStudentImage(jsonStudent.getString("studentImage"));
                student.setTutorImage(jsonStudent.getString("tutorImage"));
                student.setTeacherEmail(jsonStudent.getString("teacherEmail"));
                student.setWorkAreaImageName(jsonStudent.getString("workAreaImage"));
                student.setLastUpdatedByTutor(jsonStudent.getString("lastUpdateByTutor"));
                students.add(student);
            }
            response.setTutorList(tutors);
            response.setStudentList(students);
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * Parse the GetHomeworkDetailsForChatRequestId response
     * @param jsonString
     * @return
     */
    private GetHomeworkDetailsForChatRequestIdResponse
    parseTheGetHomeworkDetailsForChatRequestId(String jsonString) {
        GetHomeworkDetailsForChatRequestIdResponse response = new
                GetHomeworkDetailsForChatRequestIdResponse();
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside GetHomeworkDetailsForChatRequestIdResponse response " + jsonString);

        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));

            JSONObject jsonDataObj = jsonObj.getJSONObject("data");
            response.setHwDate(jsonDataObj.getString("hwDate"));
            response.setHwTitle(jsonDataObj.getString("hwTitle"));
            response.setProblem(jsonDataObj.getString("quesNum"));
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            response.setResult("fail");
            return response;
        }
    }

    /**
     * Parse the TheSendEmailToOtherTeacherResponse
     * @param jsonString
     * @return
     */
    private HttpResponseBase parseTheSendEmailToOtherTeacherResponse(String jsonString) {
        HttpResponseBase response = new HttpResponseBase();
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseTheSendEmailToOtherTeacherResponse response " + jsonString);
        return response;
    }


    /**
     * parse the load service time response
     * @param jsonString
     * @return
     */
    private LoadServiceTimeResponse parseTheGetServiceTimeForTutorResponse(String jsonString) {
        LoadServiceTimeResponse response = new LoadServiceTimeResponse();
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseTheSendEmailToOtherTeacherResponse response " + jsonString);

        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));

            JSONObject jsonDataObj = jsonObj.getJSONObject("time");
            response.setStartDate(jsonDataObj.getString("startDate"));
            response.setEndData(jsonDataObj.getString("endDate"));
            response.setTutoringTime(jsonDataObj.getString("tutoringTime"));
            response.setIdleTime(jsonDataObj.getString("idleTime"));
            response.setRating(jsonDataObj.getInt("rating"));
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the tutor session disconnect by the tutor response
     * @param jsonString
     * @return
     */
    private HttpResponseBase parseTheTutorSessionDisconnectByTutor(String jsonString) {
        HttpResponseBase response = new HttpResponseBase();
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseTheTutorSessionDisconnectByTutor response " + jsonString);
        return response;
    }

    /**
     * Parse the tutor area edit tutor or student response session response
     * @param jsonString
     * @return
     */
    private HttpResponseBase parseTheEditTutorAreaResponse(String jsonString) {
        HttpResponseBase response = new HttpResponseBase();
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseTheEditTutorAreaResponse response " + jsonString);
        return response;
    }

    /**
     * Parse the response which doen not have nothing to parse
     * @param jsonString
     * @return
     */
    private HttpResponseBase parseTheResponseWhichDoesNotHaveNothingToShow(String jsonString) {
        HttpResponseBase response = new HttpResponseBase();
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseTheResponseWhichDoesNotHaveNothingToShow response " + jsonString);
        return response;
    }

    /**
     * Parse user active inactive user status
     * @param jsonString
     * @return
     */
    private ActiveInactiveUserResponse parseUserActiveInactiveStatusResponse(String jsonString) {
        ActiveInactiveUserResponse response = new ActiveInactiveUserResponse();
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseUserActiveInactiveStatusResponse response " + jsonString);
        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));
            response.setInActive(jsonObj.getInt("inactive"));
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the check for tutor response
     * @param jsonString
     * @return
     */
    private CheckForTutorResponse parseCheckForTutorResponse(String jsonString) {
        CheckForTutorResponse response = new CheckForTutorResponse();
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseUserActiveInactiveStatusResponse response " + jsonString);
        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));
            response.setIsTutor(jsonObj.getInt("isTutor"));
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the question detail for teacher response
     * @param jsonString
     * @return
     */
    private GetQuestionDetailForTeacherResponse pareseGetQuestionDetailForTeacherResponse(String jsonString) {
        GetQuestionDetailForTeacherResponse response = new GetQuestionDetailForTeacherResponse();
        CustomePlayerAns playerAns = new CustomePlayerAns();
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside pareseGetQuestionDetailForTeacherResponse response " + jsonString);
        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));
            JSONObject jsonDataObj = jsonObj.getJSONObject("data");
            playerAns.setQueNo(jsonDataObj.getString("quesNum"));
            playerAns.setAns(jsonDataObj.getString("answer"));
            playerAns.setIsCorrect(jsonDataObj.getInt("isCorrect"));
            playerAns.setWorkImage(jsonDataObj.getString("workImage"));
            playerAns.setFirstTimeWrong(jsonDataObj.getInt("firstTimeWrong"));
            playerAns.setTeacherCredit(jsonDataObj.getString("teacher_credit"));
            playerAns.setMessage(jsonDataObj.getString("message"));
            playerAns.setIsWorkAreaPublic(jsonDataObj.getInt("workAreaPublic"));
            playerAns.setIsGetHelp(jsonDataObj.getInt("getHelp"));
            playerAns.setQuestionImage(jsonDataObj.getString("questionImage"));
            playerAns.setChatRequestedId(jsonDataObj.getInt("chatRequestId"));
            playerAns.setFixedWorkImage(jsonDataObj.getString("fixedWorkImage"));
            playerAns.setFixedMessage(jsonDataObj.getString("fixedMessage"));
            playerAns.setFixedQuestionImage(jsonDataObj.getString("fixedQuestionImage"));

            if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonDataObj , "links"))
                playerAns.setUrlLinks(this.getUrlLinks(jsonDataObj.getString("links")));
            response.setPlayerAns(playerAns);
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the active tutors response
     * @param jsonString
     * @return
     */
    private GetActiveTutorResponse parseActiveTutorsResponse(String jsonString) {
        GetActiveTutorResponse response = new GetActiveTutorResponse();
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseActiveTutorsResponse response " + jsonString);
        try {
            if(MathFriendzyHelper.checkNotificationForGivenKey(jsonString , "msg")){
                JSONObject jsonObj = new JSONObject(jsonString);
                response.setResult(jsonObj.getString("result"));
                response.setMessage(jsonObj.getString("msg"));
                response.setTutors(jsonObj.getInt("tutors"));
            }else{
                JSONObject jsonObj = new JSONObject(jsonString);
                response.setResult(jsonObj.getString("result"));
                response.setMessage(jsonObj.getString("error"));
                response.setTutors(0);
            }
            return response;
        }catch(Exception e){
            return null;
        }
    }

    /**
     * Parse the tutor area responses for help a student counter
     * @param jsonString
     * @return
     */
    private TutorAreaResponse parseTutorAreaResponses(String jsonString) {
        TutorAreaResponse response = new TutorAreaResponse();
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseTutorAreaResponses response " + jsonString);
        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));
            response.setTutorResponses(jsonObj.getInt("tutorResponses"));
            response.setHwInput(jsonObj.getInt("homeworkInputs"));
            response.setTutorSessionResponses(jsonObj.getInt("tutorSessionResponses"));
            return response;
        }catch(Exception e){
            return null;
        }
    }


    /**
     * Parse the student tutoring sessions for student response
     * @param jsonString
     * @return
     */
    private TutoringSessionsForStudentResponse parseTutorSessionsForStudentResponse
    (String jsonString) {
        TutoringSessionsForStudentResponse response = new TutoringSessionsForStudentResponse();
        ArrayList<TutoringSessionsForStudentResponse> dataList  =
                new ArrayList<TutoringSessionsForStudentResponse>();

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseTutorSessionsForStudentResponse response " + jsonString);

        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));

            JSONArray jsonDataArray = jsonObj.getJSONArray("data");
            for(int i = 0 ; i < jsonDataArray.length() ; i ++ ){
                JSONObject jsonDataObj = jsonDataArray.getJSONObject(i);
                TutoringSessionsForStudentResponse data = new TutoringSessionsForStudentResponse();
                data.setRequestId(jsonDataObj.getInt("requestId"));
                data.setUserId(jsonDataObj.getString("userId"));
                data.setPlayerId(jsonDataObj.getString("playerId"));
                data.setReqDate(jsonDataObj.getString("reqDate"));
                data.setMessage(jsonDataObj.getString("message"));
                data.setIsConnected(jsonDataObj.getInt("isConnected"));
                data.setTutorUid(jsonDataObj.getString("tutorUid"));
                data.setTutorPid(jsonDataObj.getString("tutorPid"));
                data.setChatDialogId(jsonDataObj.getString("chatDialogId"));
                data.setIsAnonymous(jsonDataObj.getInt("isAnonymous"));
                data.setRating(jsonDataObj.getInt("rating"));
                data.setTimeSpent(jsonDataObj.getString("timeSpent"));
                data.setDisconnected(jsonDataObj.getInt("disconnected"));
                data.setStudentImage(jsonDataObj.getString("studentImage"));
                data.setTutorImage(jsonDataObj.getString("tutorImage"));
                data.setWorkAreaImage(jsonDataObj.getString("workAreaImage"));
                data.setLastUpdatedByTutor(jsonDataObj.getString("lastUpdateByTutor"));
                data.setOpponentInput(jsonDataObj.getInt("opponentInput"));
                data.setTutorName(jsonDataObj.getString("tutorName"));
                data.setInActive(jsonDataObj.getInt("inactive"));

                if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonDataObj , "timeSpentStudent")) {
                    data.setTimeSpentStudent(jsonDataObj.getInt("timeSpentStudent"));
                }

                if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonDataObj , "hwDate")){
                    data.setHwDate(jsonDataObj.getString("hwDate"));
                }else{
                    data.setHwDate("");
                }

                if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonDataObj , "hwTitle")){
                    data.setHwTitle(jsonDataObj.getString("hwTitle"));
                }else{
                    data.setHwTitle("");
                }

                if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonDataObj , "quesNum")){
                    data.setQueNo(jsonDataObj.getString("quesNum"));
                }else{
                    data.setQueNo("");
                }

                if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonDataObj , "paidSession")){
                    data.setPaidSession(jsonDataObj.getString("paidSession"));
                }else{
                    data.setPaidSession("0");
                }

                if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonDataObj , "paidSessionTitle")){
                    data.setPaidSessionTitle(jsonDataObj.getString("paidSessionTitle"));
                }else{
                    data.setPaidSessionTitle("");
                }

                try {
                    if (MathFriendzyHelper.checkForKeyExistInJsonObj(jsonDataObj, "tutorOnline")) {
                        data.setTutorOnline(jsonDataObj.getInt("tutorOnline"));
                    } else {
                        data.setTutorOnline(0);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                    data.setTutorOnline(0);
                }

                dataList.add(data);
            }
            response.setList(dataList);
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }


    private SearchResourceResponse parseSearchResultResponse(String jsonString) {
        SearchResourceResponse resources = new SearchResourceResponse();
        JSONObject data = null;
        JSONArray results = null;
        try{
            CommonUtils.printLog(TAG , "inside parseSearchResultResponse response " + jsonString);
            data =  new JSONObject(jsonString);
            if(data.has("results") && data.getJSONArray("results").length() > 0){
                results = data.getJSONArray("results");
                resources.setTotalHitCount(data.getJSONObject("stats")
                        .get("totalHitCount").toString());
                final int lenth = results.length();
                for(int i = 0; i < lenth; i++){
                    ResourceResponse resource = new ResourceResponse();
                    JSONObject result = results.getJSONObject(i);
                    resource.setTitle(result.get("title").toString());
                    resource.setMediaType(result.get("mediaType").toString());
                    resource.setDescription(result.get("description").toString());
                    resource.setValue(result.getJSONObject("resourceFormat").get("value").toString());
                    resource.setUrl(result.getJSONObject("thumbnails").get("url").toString());
                    resource.setResourceUrl(result.getJSONObject("thumbnails")
                            .get("resourceUrl").toString());
                    resource.setScollectionCount(result.get("scollectionCount").toString());
                    JSONArray curriculumCodeArray = result.getJSONArray("curriculumCode");
                    final int curriculumCodeLength = curriculumCodeArray.length();
                    for(int j = 0; j < curriculumCodeLength; j++)
                        resource.getCurriculumCode().add(curriculumCodeArray.getString(j));
                    resources.getListOfresource().add(resource);
                }
                resources.setResult(MathFriendzyHelper.SUCCESS);
            }else{
                resources.setResult("Fail");
            }
        }catch(JSONException e){
            e.printStackTrace();
            resources.setResult("Fail");
        }
        return resources;
    }

    /**
     * Parse the get gooru resources for homework
     * @param jsonString
     * @return
     */
    private GetGooruResourceForHWResponse parseGooruResourceForHWResponse(String jsonString) {
        GetGooruResourceForHWResponse response = new GetGooruResourceForHWResponse();
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseGooruResourceForHWResponse response " + jsonString);
        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));
            ArrayList<ResourceResponse> selectedResourceList = new ArrayList<ResourceResponse>();
            JSONArray jsonDataArray = jsonObj.getJSONArray("data");
            for(int  i = 0 ; i < jsonDataArray.length() ; i ++ ){
                JSONObject jsonDataObj = jsonDataArray.getJSONObject(i);
                ResourceResponse resourceData = new ResourceResponse();
                resourceData.setTitle(this.getValueFromJsonObject(jsonDataObj , "title"));
                resourceData.setValue(this.getValueFromJsonObject(jsonDataObj , "resourceFormat"));
                resourceData.setDescription(this.getValueFromJsonObject(jsonDataObj , "description"));
                resourceData.setResourceUrl(this.getValueFromJsonObject(jsonDataObj , "resourceUrl"));
                resourceData.setUrl(this.getValueFromJsonObject(jsonDataObj , "url"));
                ArrayList<String> curriculumString = new ArrayList<String>();

                try {
                    if (this.isJsonArray(jsonDataObj.get("curriculumCode"))) {
                        JSONArray jsonCurriculumCodeArray = jsonDataObj.getJSONArray("curriculumCode");
                        for (int j = 0; j < jsonCurriculumCodeArray.length(); j++) {
                            curriculumString.add(jsonCurriculumCodeArray.getString(j));
                        }
                    } else {
                        String curriculumCode = jsonDataObj.getString("curriculumCode");
                        if(!MathFriendzyHelper.isEmpty(curriculumCode)){
                            String[] codeArrStrings = curriculumCode.split("\\|");// | is the literal so we need to ad \\
                            List<String> wordList = Arrays.asList(codeArrStrings);
                            for (String code : wordList){
                                curriculumString.add(code);
                            }
                        }
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }

                resourceData.setCurriculumCode(curriculumString);
                resourceData.setMediaType(this.getValueFromJsonObject(jsonDataObj ,
                        "isMobileFriendliness"));
                resourceData.setScollectionCount(this.getValueFromJsonObject(jsonDataObj ,
                        "scollectionCount"));
                //resourceData.setScollectionCount(curriculumString.size() + "");
                selectedResourceList.add(resourceData);
            }
            response.setSelectedResourceList(selectedResourceList);
            return response;
        }catch(Exception e){
            e.printStackTrace();
            Log.e(TAG , "Error in parsing " + e.toString());
            return null;
        }
    }


    /**
     * Parse the purchase time for player response
     * @param jsonString
     * @return
     */
    private GetPlayerPurchaseTimeResponse parseGetPurchaseTimeForPlayerResponse(String jsonString) {
        GetPlayerPurchaseTimeResponse response = new GetPlayerPurchaseTimeResponse();
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseGetPurchaseTimeForPlayerResponse response " + jsonString);
        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));
            response.setTime(jsonObj.getInt("time"));
            return response;
        }catch(Exception e){
            return null;
        }
    }

    /**
     * Parse the tutor package response
     * @param jsonString
     * @return
     */
    private GetPurchagePackagesResponse parseGetPurchagePackageResponse(String jsonString) {
        GetPurchagePackagesResponse response = new GetPurchagePackagesResponse();
        ArrayList<GetPurchagePackagesResponse> singleTypePackageList =
                new ArrayList<GetPurchagePackagesResponse>();
        ArrayList<GetPurchagePackagesResponse> recurringTypePackageList =
                new ArrayList<GetPurchagePackagesResponse>();
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseGetPurchagePackageResponse response " + jsonString);
        try{
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));
            JSONArray dataArray = jsonObj.getJSONArray("data");
            for(int i = 0 ; i < dataArray.length() ; i ++ ){
                GetPurchagePackagesResponse data = new GetPurchagePackagesResponse();
                JSONObject dataObj = dataArray.getJSONObject(i);
                data.setId(dataObj.getInt("id"));
                data.setTermPrice(dataObj.getInt("term_price"));
                data.setTotalPrice(dataObj.getInt("total_price"));
                data.setTimePurchased(dataObj.getInt("time_purchased"));
                data.setDescription(dataObj.getString("description"));
                data.setType(dataObj.getString("type"));
                if(data.getType().equalsIgnoreCase(GetPurchagePackagesResponse.SINGLE_TYPE)){
                    singleTypePackageList.add(data);
                }else if(data.getType().equalsIgnoreCase(GetPurchagePackagesResponse.RECURRING_TYPE)){
                    recurringTypePackageList.add(data);
                }
            }
            response.setSingleTypePackageList(singleTypePackageList);
            response.setRecurringTypePackageList(recurringTypePackageList);
            return response;
        }catch(Exception e){
            return null;
        }
    }

    /**
     * Get update purchase time response
     * @param jsonString
     * @return
     */
    private UpdatePurchaseTimeResponse parseUpdatePurchaseTime(String jsonString) {
        UpdatePurchaseTimeResponse response = new UpdatePurchaseTimeResponse();
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseUpdatePurchaseTime response " + jsonString);
        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));
            response.setTime(jsonObj.getInt("time"));
            return response;
        }catch(Exception e){
            return null;
        }
    }

    /**
     * Parse the student purchase time info
     * @param jsonString
     * @return
     */
    private GetPurchaseTimeInfoResponse parseStudentPurchaseInfo(String jsonString) {
        GetPurchaseTimeInfoResponse response = new GetPurchaseTimeInfoResponse();
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseStudentPurchaseInfo response " + jsonString);
        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));
            JSONObject jsonData = jsonObj.getJSONObject("data");
            response.setTime(jsonData.getInt("time"));
            response.setIsTimePurchase(jsonData.getInt("isTimePurchased"));
            response.setProfileId(jsonData.getString("profileId"));
            response.setPackageId(jsonData.getString("packageId"));
            response.setNextDate(jsonData.getString("nextDate"));
            response.setIsCancel(jsonData.getString("status"));
            response.setPackageName(jsonData.getString("packName"));
            return response;
        }catch(Exception e){
            return null;
        }
    }

    private String getValueFromJsonObject(JSONObject jsonObj , String key){
        try {
            if (MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObj, key)) {
                return jsonObj.getString(key);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Parse the cncel profile response
     * @param jsonString
     * @return
     */
    private CancelProfileResponse parseCancelProfileResponse(String jsonString) {
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseCancelProfileResponse response " + jsonString);
        CancelProfileResponse response = new CancelProfileResponse();
        try{
            JSONObject jsonObject = new JSONObject(jsonString);
            response.setResponse(jsonObject.getString("result"));
            if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject ,
                    "error")){
                response.setError(jsonObject.getString("error"));
            }
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
        return response;
    }

    private CreditCardPaymentResponse parseCreditCardPaymentResponse(String jsonString) {
        CreditCardPaymentResponse response = new CreditCardPaymentResponse();
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseCreditCardPaymentResponse response " + jsonString);
        try{
            JSONObject jsonObject = new JSONObject(jsonString);
            response.setResult(jsonObject.getString("result"));
            if(!response.getResult().equalsIgnoreCase(MathFriendzyHelper.SUCCESS)){
                response.setMessage(jsonObject.getString("error"));
            }
        }catch (Exception e){
            response.setResult("failed");
            response.setMessage("Error in payment.");
        }
        return response;
    }

    private ShouldPaisTutorResponse parseShouldPaidTutorResponse(String jsonString) {
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parseShouldPaidTutorResponse response " + jsonString);
        ShouldPaisTutorResponse response = new ShouldPaisTutorResponse();
        try{
            JSONObject jsonObject = new JSONObject(jsonString);
            response.setResult(jsonObject.getString("result"));
            response.setPaidTutorAllow(jsonObject.getString("paidTutorAllow"));
            response.setSchoolAllowPaidTutor(jsonObject.getString("schoolAllowPaidTutor"));
            response.setStudentWithNoSchoolAllowed(jsonObject.getString("studentWithNoSchoolAllowed"));
            response.setTime(jsonObject.getString("time"));
            return response;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public PhraseCatagory parsePhraseTutorResponse(String jsonString){
        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside parsePhraseTutorResponse response " + jsonString);
        PhraseCatagory response = new PhraseCatagory();
        try{
            JSONObject jsonObject = new JSONObject(jsonString);
            response.setResponse(jsonObject.getString("result"));
            /*if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject , "date" )){
                response.setDate(jsonObject.getString("date"));
            }else{
                response.setDate("");
            }*/
            response.setDate(MathFriendzyHelper.getCurrentDateInGiveGformateDate("yyyy-MM-dd HH:mm:ss"));
            if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject , "data" )){
                ArrayList<PhraseCatagory> catList = new ArrayList<PhraseCatagory>();
                ArrayList<PhraseCatagory> studentCatList = new ArrayList<PhraseCatagory>();
                JSONArray jsonDataArray = jsonObject.getJSONArray("data");
                for(int i = 0 ; i < jsonDataArray.length() ; i ++ ){
                    JSONObject jsonCatObj = jsonDataArray.getJSONObject(i);
                    PhraseCatagory category = new PhraseCatagory();
                    category.setCatId(jsonCatObj.getInt("id"));
                    category.setCatName(jsonCatObj.getString("title"));
                    if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonCatObj , "for_tutor")){
                        category.setForTutor(jsonCatObj.getInt("for_tutor"));
                    }
                    ArrayList<PhraseSubCatagory> subCatList = new ArrayList<PhraseSubCatagory>();
                    JSONArray jsonSubCatArray = jsonCatObj.getJSONArray("messages");
                    for(int j = 0 ; j < jsonSubCatArray.length() ; j ++ ){
                        JSONObject jsonSubCatObj = jsonSubCatArray.getJSONObject(j);
                        PhraseSubCatagory subCat = new PhraseSubCatagory();
                        subCat.setSubCatId(j + 1);
                        subCat.setSubCatName(jsonSubCatObj.getString("message"));
                        subCat.setIsSelectable(jsonSubCatObj.getInt("selectable"));
                        subCat.setForFreeTutor(jsonSubCatObj.getInt("forFreeTutor"));
                        subCatList.add(subCat);
                    }
                    category.setSubCatList(subCatList);

                    if(category.getForTutor() == MathFriendzyHelper.YES){
                        catList.add(category);
                    }else{
                        studentCatList.add(category);
                    }
                }
                response.setCatList(catList);
                response.setStudentCatList(studentCatList);
            }else{
                response.setCatList(null);
                response.setStudentCatList(null);
            }
            return response;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    private ChatMessageModel parseChatMessgaesResponse(String jsonString) {
        CommonUtils.printLog(TAG , "inside parseChatMessgaesResponse response " + jsonString);
        try {
            ChatMessageModel modelObj = new ChatMessageModel();
            ArrayList<ChatMessageModel> messages = new ArrayList<ChatMessageModel>();
            JSONObject jsonObj = new JSONObject(jsonString);
            JSONArray jsonArray = jsonObj.getJSONArray("data");
            for(int i = 0 ; i < jsonArray.length() ; i ++ ){
                JSONObject jsonData = jsonArray.getJSONObject(i);
                ChatMessageModel dataModel = new ChatMessageModel();
                dataModel.setSenderId(jsonData.getString("sender"));
                dataModel.setMessage(jsonData.getString("txt"));
                if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonData ,
                        "params")){
                    String paramString = jsonData.getString("params");
                    if(!MathFriendzyHelper.isEmpty(paramString)){
                        JSONObject jsonParam = new JSONObject(jsonData.getString("params"));
                        dataModel.setName(jsonParam.getString("name"));
                        if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonParam ,"tutor")){
                            dataModel.setIsTutor(jsonParam.getInt("tutor"));
                        }else{
                            dataModel.setIsTutor(0);
                        }
                    }
                }
                messages.add(dataModel);
            }
            modelObj.setMessageList(messages);
            return modelObj;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public ResourceCategory parseResourceCategories(String jsonString) {
        try {
            CommonUtils.printLog(TAG , "inside parseResourceCategories response " + jsonString);
            ResourceCategory resourceCategory = new ResourceCategory();
            ArrayList<ResourceCategory> resourceCategoryList = new ArrayList<ResourceCategory>();
            JSONObject jsonObject = new JSONObject(jsonString);
            resourceCategory.setResult(jsonObject.getString("result"));

            JSONArray jsonCatArray = jsonObject.getJSONArray("data");
            for(int i = 0 ; i < jsonCatArray.length() ; i ++ ){
                JSONObject jsonCatObj = jsonCatArray.getJSONObject(i);
                ResourceCategory cat = new ResourceCategory();
                cat.setCatId(jsonCatObj.getInt("id"));
                cat.setCatName(MathFriendzyHelper
                        .replaceDoubleQuotesBySingleQuote(jsonCatObj.getString("title")));

                JSONArray jsonSubCatArray = jsonCatObj.getJSONArray("subcategories");
                ArrayList<ResourceSubCat> subCatList = new ArrayList<ResourceSubCat>();
                for(int j = 0 ; j < jsonSubCatArray.length() ; j ++ ){
                    JSONObject jsonSubCatObj = jsonSubCatArray.getJSONObject(j);
                    ResourceSubCat subCat = new ResourceSubCat();
                    subCat.setSubCatId(jsonSubCatObj.getInt("subCategId"));
                    subCat.setSubCatName(MathFriendzyHelper
                            .replaceDoubleQuotesBySingleQuote(jsonSubCatObj.getString("title")));
                    subCatList.add(subCat);
                }
                cat.setSubCatList(subCatList);
                resourceCategoryList.add(cat);
            }
            resourceCategory.setCategoriesList(resourceCategoryList);
            resourceCategory.setDate(MathFriendzyHelper
                    .getCurrentDateInGiveGformateDate("yyyy-MM-dd HH:mm:ss"));
            if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject, "subjects")){
                resourceCategory.setSubjectList(
                        MathFriendzyHelper
                                .getCommaSepratedOption(jsonObject.getString("subjects"),","));
            }
            return resourceCategory;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the Resource video inApp Status response
     * @param jsonString
     * @return
     */
    private GetResourceVideoInAppResult parseResourceVideoInAppStatusResponse(String jsonString) {
        try {
            CommonUtils.printLog(TAG, "inside parseResourceVideoInAppStatusResponse response " + jsonString);
            GetResourceVideoInAppResult response = new GetResourceVideoInAppResult();
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));
            response.setStatus(jsonObj.getInt("videoPurchase"));
            response.setUnlockCategoryStatus(jsonObj.getInt("unlockCategories"));
            return response;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the AppUnlock status from server
     * @param jsonString
     * @return
     */
    private GetAppUnlockStatusResponse parseGetAppUnlockStatusResponse(String jsonString) {
        try {
            CommonUtils.printLog(TAG, "inside parseResourceVideoInAppStatusResponse response " + jsonString);
            GetAppUnlockStatusResponse response = new GetAppUnlockStatusResponse();
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));
            if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObj ,
                    "appsUnlock")) {
                response.setAppUnlockStatus(jsonObj.getInt("appsUnlock"));
            }
            response.setCoins(jsonObj.getInt("coins"));
            return response;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the student answer detail
     * @param jsonString
     * @return
     */
    private GetStudentDetailAnswerResponse parseStudentAnswerDetailResponse(String jsonString) {
        try {
            CommonUtils.printLog(TAG, "inside parseStudentAnswerDetailResponse response " + jsonString);
            GetStudentDetailAnswerResponse response = new GetStudentDetailAnswerResponse();
            JSONObject jsonObject = new JSONObject(jsonString);
            response.setScore(jsonObject.getInt("score"));
            ArrayList<CustomePlayerAns> playerAnsList = new ArrayList<CustomePlayerAns>();
            JSONArray customePlayerAnsArray = jsonObject.getJSONArray("playerAns");
            for(int i = 0 ; i < customePlayerAnsArray.length() ; i ++ ){
                JSONObject customePlayerAnsJson = customePlayerAnsArray.getJSONObject(i);
                CustomePlayerAns playerAns = new CustomePlayerAns();
                playerAns.setQueNo(customePlayerAnsJson.getString("quesNum"));
                playerAns.setAns(customePlayerAnsJson.getString("answer"));
                playerAns.setIsCorrect(customePlayerAnsJson.getInt("isCorrect"));
                playerAns.setWorkImage(customePlayerAnsJson.getString("workImage"));
                playerAns.setFirstTimeWrong(customePlayerAnsJson.getInt("firstTimeWrong"));
                playerAns.setTeacherCredit(customePlayerAnsJson.getString("teacher_credit"));
                playerAns.setMessage(customePlayerAnsJson.getString("message"));
                playerAns.setIsGetHelp(customePlayerAnsJson.getInt("getHelp"));
                playerAns.setIsWorkAreaPublic(customePlayerAnsJson.getInt("workAreaPublic"));
                playerAns.setQuestionImage(customePlayerAnsJson.getString("questionImage"));
                playerAns.setChatRequestedId(customePlayerAnsJson.getInt("chatRequestId"));
                playerAns.setFixedWorkImage(customePlayerAnsJson.getString("fixedWorkImage"));
                playerAns.setFixedMessage(customePlayerAnsJson.getString("fixedMessage"));
                playerAns.setFixedQuestionImage(customePlayerAnsJson.getString("fixedQuestionImage"));
                playerAns.setWorkInput(customePlayerAnsJson.getString("workInput"));
                playerAns.setOpponentInput(customePlayerAnsJson.getString("opponentInput"));
                if(MathFriendzyHelper.checkForKeyExistInJsonObj(customePlayerAnsJson , "links"))
                    playerAns.setUrlLinks(this.getUrlLinks(customePlayerAnsJson.getString("links")));
                playerAnsList.add(playerAns);
            }
            response.setCustomePlayerAnsList(playerAnsList);
            return response;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the active inactive status of student for tutor
     * @param jsonString
     * @return
     */
    private GetActiveInActiveStatusOfStudentForTutorResponse
    parseTheActiveInActiveStatusOfStudentForTutor(String jsonString) {
        GetActiveInActiveStatusOfStudentForTutorResponse response =
                new GetActiveInActiveStatusOfStudentForTutorResponse();
        try {
            CommonUtils.printLog(TAG, "inside parseStudentAnswerDetailResponse response " + jsonString);
            ArrayList<GetActiveInActiveStatusOfStudentForTutorResponse> list
                    = new ArrayList<GetActiveInActiveStatusOfStudentForTutorResponse>();
            JSONObject jsonObject = new JSONObject(jsonString);
            response.setResult(jsonObject.getString("result"));
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            for(int i = 0 ; i < jsonArray.length() ; i ++ ){
                JSONObject jsonDataObj = jsonArray.getJSONObject(i);
                GetActiveInActiveStatusOfStudentForTutorResponse dataObj
                        = new GetActiveInActiveStatusOfStudentForTutorResponse();
                dataObj.setUserId(jsonDataObj.getString("userId"));
                dataObj.setPlayerId(jsonDataObj.getString("playerId"));
                dataObj.setInActive(jsonDataObj.getInt("inactive"));
                if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonDataObj , "onlineRequest")) {
                    dataObj.setOnlineRequestId(jsonDataObj.getInt("onlineRequest"));
                }else{
                    dataObj.setOnlineRequestId(0);
                }
                list.add(dataObj);
            }
            response.setList(list);
            return response;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the user class from server
     * @param jsonString
     * @return
     */
    private GetSchoolClassResponse parseTheSchoolClassessResponse(String jsonString) {
        try {
            CommonUtils.printLog(TAG, "inside parseTheSchoolClassessResponse response " + jsonString);

            GetSchoolClassResponse response = new GetSchoolClassResponse();
            ArrayList<UserClasses> userClasseses = new ArrayList<UserClasses>();
            JSONObject jsonObject = new JSONObject(jsonString);
            response.setResult(jsonObject.getString("result"));
            JSONArray classJsonArray = jsonObject.getJSONArray("data");
            for(int i = 0 ; i < classJsonArray.length() ; i ++ ){
                JSONObject classJsonObject = classJsonArray.getJSONObject(i);
                UserClasses userclass = new UserClasses();
                userclass.setGrade(classJsonObject.getInt("grade"));
                JSONArray jsonArrayWithClassDetail = classJsonObject.getJSONArray("classes");
                ArrayList<ClassWithName> classWithNames = new ArrayList<ClassWithName>();
                for(int j = 0 ; j < jsonArrayWithClassDetail.length() ; j ++ ){
                    JSONObject jsonClassObjWithClassName = jsonArrayWithClassDetail.getJSONObject(j);
                    ClassWithName classWithName = new ClassWithName();
                    classWithName.setClassId(jsonClassObjWithClassName.getInt("classId"));
                    classWithName.setClassName(jsonClassObjWithClassName.getString("className"));
                    classWithName.setSubject(jsonClassObjWithClassName.getString("subject"));
                    classWithName.setStartDate(jsonClassObjWithClassName.getString("startDate"));
                    classWithName.setGrade(MathFriendzyHelper.parseInt
                            (jsonClassObjWithClassName.getString("grade")));
                    classWithName.setEndDate(jsonClassObjWithClassName.getString("endDate"));
                    classWithName.setSubjectId(jsonClassObjWithClassName.getInt("subjectId"));
                    classWithNames.add(classWithName);
                }
                userclass.setClassList(classWithNames);
                userClasseses.add(userclass);
            }
            response.setUserClasseses(userClasseses);
            return response;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the get live app version response
     * @param jsonString
     * @return
     */
    private GetAppVersionResponse parseTheGetLiveAppVersion(String jsonString) {
        try {
            CommonUtils.printLog(TAG, "inside parseTheGetLiveAppVersion response " + jsonString);
            GetAppVersionResponse response = new GetAppVersionResponse();
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));
            response.setFrequency(jsonObj.getInt("frequency"));
            response.setAndroidVersion(jsonObj.getString("android_version"));
            return response;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the saved homework data
     * @param jsonString
     * @return
     */
    private HomeworkData parseSavedHomeworkDataFromServer(String jsonString) {
        try {
            CommonUtils.printLog(TAG, "inside parseSavedHomeworkDataFromServer response "
                    + jsonString.replaceAll("\\\\", ""));
            //jsonString = jsonString.replaceAll("\\\\", "");
            HomeworkData response = new HomeworkData();
            ArrayList<HomeworkData> homeworkDatas = new ArrayList<HomeworkData>();
            JSONObject jsonObject = new JSONObject(jsonString);
            response.setResult(jsonObject.getString("result"));
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            for(int i = 0 ; i < jsonArray.length() ; i ++ ){
                try {
                    JSONObject jsonData = jsonArray.getJSONObject(i);
                    HomeworkData homeworkData = new HomeworkData();
                    homeworkData.setSavedHwId(jsonData.getInt("id"));
                    homeworkData.setCreationDate(jsonData.getString("creationDate"));

                    JSONObject jsonSavedData = jsonData.getJSONObject("data");
                    homeworkData.setMessage(jsonSavedData.getString("message"));
                    homeworkData.setGrade(jsonSavedData.getString("grade"));

                    if (MathFriendzyHelper.checkForKeyExistInJsonObj(jsonSavedData, "date"))
                        homeworkData.setDate(jsonSavedData.getString("date"));
                    //need to add creation date field also
                    homeworkData.setCustomeQuizList(this.parseCustomSavedHomework(jsonSavedData.getJSONArray("customData")));
                    homeworkData.setPracticeResults(this.parsePracticeSavedHomework(jsonSavedData.getJSONArray("practiceCategories")));
                    homeworkData.setWordResults(this.parseWordProblemSavedHomework(jsonSavedData.getJSONArray("wordCategories")));
                    homeworkDatas.add(homeworkData);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            response.setHomeworkDatas(homeworkDatas);
            return response;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the custom HW response from the saved homework
     */
    private final int FILL_IN_TYPE = 1;
    private final int MULTIPLE_CHOICE_ANS_TYPE = 1;
    private final int TRUE_FALSE_ANS_TYPE = 2;
    private final int YES_NO_ANS_TYPE = 3;
    private ArrayList<CustomeResult> parseCustomSavedHomework(JSONArray jsonArray){
        try {
            ArrayList<CustomeResult> customeResultArrayList = new ArrayList<CustomeResult>();
            for (int i  = 0 ; i < jsonArray.length() ; i ++ ){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                CustomeResult customeResult = new CustomeResult();
                customeResult.setShowAns(jsonObject.getInt("showAnswers"));
                customeResult.setTitle(jsonObject.getString("title"));
                customeResult.setAllowChanges(jsonObject.getInt("allowChanges"));
                customeResult.setSheetName(jsonObject.getString("questionSheet"));

                if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject , "gooruResources")){
                    customeResult.setSelectedResourceList(
                            this.parseGooruResources(jsonObject.getJSONArray("gooruResources")));
                }

                if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject , "links")){
                    customeResult.setLinks(this.getUrlLinks(jsonObject.getString("links")));
                }

                JSONArray jsonAnsArray = jsonObject.getJSONArray("answers");
                ArrayList<CustomeAns> customeAnslist = new ArrayList<CustomeAns>();
                for(int j = 0 ; j < jsonAnsArray.length() ; j ++ ){
                    JSONObject jsonCusAns = jsonAnsArray.getJSONObject(j);
                    CustomeAns customeAns = new CustomeAns();
                    customeAns.setQueNo(jsonCusAns.getString("questionNum"));
                    customeAns.setAnsSuffix(jsonCusAns.getString("measurementUnit"));
                    customeAns.setIsLeftUnit(jsonCusAns.getInt("isLeftMeasurement"));
                    customeAns.setFillInType(jsonCusAns.getInt("isFillInBlank"));

                    JSONArray jsonCorrectAnsArray = jsonCusAns.getJSONArray("correctAnswers");
                    JSONArray jsonCusAnsJSONArrayAnsArray = jsonCusAns.getJSONArray("answers");//only for multiple choice
                    if(jsonCusAns.getInt("isFillInBlank") == FILL_IN_TYPE){
                        customeAns.setCorrectAns(jsonCorrectAnsArray.getString(0));
                    }else{
                        int answerType = jsonCusAns.getInt("multipleChoiceQuestionType");
                        if(answerType == MULTIPLE_CHOICE_ANS_TYPE){
                            StringBuilder stringBuilder = new StringBuilder();
                            for(int k = 0 ; k < jsonCorrectAnsArray.length() ; k ++ ){
                                stringBuilder.append(jsonCorrectAnsArray.getString(k));
                            }
                            customeAns.setCorrectAns(stringBuilder.toString());
                            ArrayList<String> options = new ArrayList<String>();
                            for(int l = 0 ; l < jsonCusAnsJSONArrayAnsArray.length() ; l ++ ) {
                                options.add(jsonCusAnsJSONArrayAnsArray.getString(l));
                            }
                            customeAns.setMultipleChoiceOptionList(options);
                        }else if(answerType == YES_NO_ANS_TYPE){
                            customeAns.setCorrectAns(jsonCorrectAnsArray.getString(0));
                        }else if(answerType == TRUE_FALSE_ANS_TYPE){
                            customeAns.setCorrectAns(jsonCorrectAnsArray.getString(0));
                        }
                    }
                    customeAns.setAnswerType(jsonCusAns.getInt("multipleChoiceQuestionType"));
                    customeAns.setIsAnswerAvailable(jsonCusAns.getInt("isAnswerAvailable"));

                    if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonCusAns , "questionStr")){
                        customeAns.setQuestionString(jsonCusAns.getString("questionStr"));
                    }else{
                        customeAns.setQuestionString("");
                    }
                    customeAnslist.add(customeAns);
                }
                customeResult.setCustomeAnsList(customeAnslist);
                customeResultArrayList.add(customeResult);
            }
            return customeResultArrayList;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the practice skill categories from saved homework
     * @param jsonArray
     * @return
     */
    private ArrayList<PracticeResult> parsePracticeSavedHomework(JSONArray jsonArray) {
        try {
            Map<Integer , PracticeResult> map =
                    new LinkedHashMap<Integer , PracticeResult>();
            ArrayList<PracticeResult> practiceResults = new ArrayList<PracticeResult>();
            for(int i = 0 ; i < jsonArray.length() ; i ++ ){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int catId = jsonObject.getInt("categoryId");
                PracticeResult practiceResult = map.get(catId);
                PracticeResultSubCat subCat = new PracticeResultSubCat();
                subCat.setCatId(catId + "");
                subCat.setSubCatId(jsonObject.getInt("subCategId"));
                subCat.setProblems(jsonObject.getInt("problems"));

                if(practiceResult == null) {
                    practiceResult = new PracticeResult();
                    ArrayList<PracticeResultSubCat> subCats = new ArrayList<PracticeResultSubCat>();
                    practiceResult.setCatId(catId + "");
                    practiceResult.setIntCatId(catId);
                    subCats.add(subCat);
                    practiceResult.setPracticeResultSubCatList(subCats);
                }else{
                    practiceResult.getPracticeResultSubCatList().add(subCat);
                }
                map.put(catId , practiceResult);
            }
            practiceResults.addAll(map.values());
            return practiceResults;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Parse the word categories from the saved homework
     * @param jsonArray
     * @return
     */
    private ArrayList<WordResult> parseWordProblemSavedHomework(JSONArray jsonArray) {
        try {
            Map<Integer , WordResult> map =
                    new LinkedHashMap<Integer , WordResult>();
            ArrayList<WordResult> wordResults = new ArrayList<WordResult>();
            for(int i = 0 ; i < jsonArray.length() ; i ++ ){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int catId = jsonObject.getInt("categoryId");
                WordResult wordResult = map.get(catId);
                WordSubCatResult subCat = new WordSubCatResult();
                subCat.setCatId(catId + "");
                subCat.setSubCatId(jsonObject.getInt("subCategId"));

                if(wordResult == null) {
                    wordResult = new WordResult();
                    ArrayList<WordSubCatResult> subCats = new ArrayList<WordSubCatResult>();
                    wordResult.setCatIg(catId + "");
                    subCats.add(subCat);
                    wordResult.setSubCatResultList(subCats);
                }else{
                    wordResult.getSubCatResultList().add(subCat);
                }
                map.put(catId , wordResult);
            }
            wordResults.addAll(map.values());
            return wordResults;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Parse the gooru resources
     * @param jsonDataArray
     * @return
     */
    private ArrayList<ResourceResponse> parseGooruResources(JSONArray jsonDataArray){
        try {
            ArrayList<ResourceResponse> selectedResourceList = new ArrayList<ResourceResponse>();
            for (int i = 0; i < jsonDataArray.length(); i++) {
                JSONObject jsonDataObj = jsonDataArray.getJSONObject(i);
                ResourceResponse resourceData = new ResourceResponse();
                resourceData.setTitle(this.getValueFromJsonObject(jsonDataObj, "title"));
                resourceData.setValue(this.getValueFromJsonObject(jsonDataObj, "resourceFormat"));
                resourceData.setDescription(this.getValueFromJsonObject(jsonDataObj, "description"));
                resourceData.setResourceUrl(this.getValueFromJsonObject(jsonDataObj, "resourceUrl"));
                resourceData.setUrl(this.getValueFromJsonObject(jsonDataObj, "url"));
                ArrayList<String> curriculumString = new ArrayList<String>();

                try {
                    if (this.isJsonArray(jsonDataObj.get("curriculumCode"))) {
                        JSONArray jsonCurriculumCodeArray = jsonDataObj.getJSONArray("curriculumCode");
                        for (int j = 0; j < jsonCurriculumCodeArray.length(); j++) {
                            curriculumString.add(jsonCurriculumCodeArray.getString(j));
                        }
                    } else {
                        String curriculumCode = jsonDataObj.getString("curriculumCode");
                        if (!MathFriendzyHelper.isEmpty(curriculumCode)) {
                            String[] codeArrStrings = curriculumCode.split("\\|");// | is the literal so we need to ad \\
                            List<String> wordList = Arrays.asList(codeArrStrings);
                            for (String code : wordList) {
                                curriculumString.add(code);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                resourceData.setCurriculumCode(curriculumString);
                resourceData.setMediaType(this.getValueFromJsonObject(jsonDataObj,
                        "isMobileFriendliness"));
                resourceData.setScollectionCount(this.getValueFromJsonObject(jsonDataObj,
                        "scollectionCount"));
                //resourceData.setScollectionCount(curriculumString.size() + "");
                selectedResourceList.add(resourceData);
            }
            return selectedResourceList;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the get categories for grades response
     * @param jsonString
     * @return
     */
    private DownloadCategoriesForGradesResponse parseGetCategoriesForGradesResponse(String jsonString) {
        try {
            CommonUtils.printLog(TAG, "inside parseGetCategoriesForGradesResponse response " + jsonString);
            DownloadCategoriesForGradesResponse response = new DownloadCategoriesForGradesResponse();
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));

            ArrayList<DownloadCategoriesForGradesResponse> categories =
                    new ArrayList<DownloadCategoriesForGradesResponse>();
            JSONArray jsonArray = jsonObj.getJSONArray("data");
            for(int i = 0 ; i < jsonArray.length() ;  i ++ ){
                JSONObject jsonGradeDataObj =  jsonArray.getJSONObject(i);
                Iterator<?> keys = jsonGradeDataObj.keys();
                while(keys.hasNext()){
                    DownloadCategoriesForGradesResponse gradeCatData = new DownloadCategoriesForGradesResponse();
                    ArrayList<CategoryListTransferObj> wordCategories = new ArrayList<CategoryListTransferObj>();
                    String grade = (String)keys.next();
                    //CommonUtils.printLog(TAG , "grade " + grade + "i" + i);
                    gradeCatData.setGrade(MathFriendzyHelper.parseInt(grade));
                    JSONArray jsonCatArray = jsonGradeDataObj.getJSONArray(grade);
                    for(int j = 0 ; j < jsonCatArray.length() ; j ++ ){
                        JSONObject jsonCatObj = jsonCatArray.getJSONObject(j);
                        CategoryListTransferObj catObj = new CategoryListTransferObj();
                        catObj.setCid(jsonCatObj.getInt("cId"));
                        catObj.setName(jsonCatObj.getString("name"));

                        ArrayList<SubCatergoryTransferObj> subCategories =
                                new ArrayList<SubCatergoryTransferObj>();
                        JSONArray jsonSubCatArray = jsonCatObj.getJSONArray("subCategories");
                        for(int k = 0 ; k < jsonSubCatArray.length() ; k ++ ){
                            SubCatergoryTransferObj subCat = new SubCatergoryTransferObj();
                            JSONObject jsonSubCatObj = jsonSubCatArray.getJSONObject(k);
                            subCat.setId(jsonSubCatObj.getInt("id"));
                            subCat.setName(jsonSubCatObj.getString("name"));
                            subCat.setUrl(jsonSubCatObj.getString("url"));
                            subCategories.add(subCat);
                        }
                        catObj.setSubCategories(subCategories);
                        wordCategories.add(catObj);
                    }
                    gradeCatData.setCategories(wordCategories);
                    categories.add(gradeCatData);
                }
            }
            response.setCategoriesForGradesResponses(categories);
            return response;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * parse the previous assigned homework from server
     * @param jsonString
     * @return
     */
    private HomeworkData parsePrevAssignedHomeworkDataFromServer(String jsonString) {
        try {
            CommonUtils.printLog(TAG, "inside parsePrevAssignedHomeworkDataFromServer response " + jsonString);
            HomeworkData response = new HomeworkData();
            ArrayList<HomeworkData> homeworkDatas = new ArrayList<HomeworkData>();
            JSONObject jsonObject = new JSONObject(jsonString);
            response.setResult(jsonObject.getString("result"));

            //new homework changes
            try {
                if (MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject, "lastClassAssignedForHw")) {
                    response.setLastClassAssignedForHw(jsonObject.getInt("lastClassAssignedForHw"));
                }
                if (MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject, "classes")) {
                    response.setClassArrayExist(true);
                    response.setClassWithNames(
                            this.parseClassesResponse(jsonObject.getJSONArray("classes")));
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            //end new homework changes

            JSONArray jsonDataArray = jsonObject.getJSONArray("data");
            for(int i = 0 ; i < jsonDataArray.length() ;  i ++ ){
                JSONObject jsonDataObj = jsonDataArray.getJSONObject(i);
                HomeworkData homeworkData = new HomeworkData();
                homeworkData.setSavedHwId(jsonDataObj.getInt("homeworkId"));
                homeworkData.setDate(jsonDataObj.getString("date"));
                homeworkData.setGrade(jsonDataObj.getString("grade"));
                homeworkData.setMessage(jsonDataObj.getString("message"));
                homeworkData.setCreationDate(jsonDataObj.getString("creationDate"));

                if(this.isJsonArray(jsonDataObj.get("practiceCategories"))){
                    ArrayList<PracticeResult> practiceResultList = new ArrayList<PracticeResult>();
                    //set blank list
                    homeworkData.setPracticeResults(practiceResultList);
                }else{
                    homeworkData.setPracticeResults(this.parsePracticeResultForPrevHW
                            (jsonDataObj.getJSONObject("practiceCategories")));
                }

                if(this.isJsonArray(jsonDataObj.get("wordCategories"))){
                    ArrayList<WordResult> wordResultList = new ArrayList<WordResult>();
                    //set blank list
                    homeworkData.setWordResults(wordResultList);
                }else{
                    homeworkData.setWordResults(this.parseWordResultForPrevHW
                            (jsonDataObj.getJSONObject("wordCategories")));
                }
                homeworkData.setCustomeQuizList(this.parseCustomeResultForPrevHW
                        (jsonDataObj.getJSONArray("customData")));
                homeworkDatas.add(homeworkData);
            }
            response.setHomeworkDatas(homeworkDatas);
            return response;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * This method parse the practice Result
     * @param jsonObj
     * @return
     */
    private ArrayList<PracticeResult> parsePracticeResultForPrevHW(JSONObject jsonObj){
        ArrayList<PracticeResult> practiceResultList = new ArrayList<PracticeResult>();
        Iterator<?> iterator = jsonObj.keys();
        while(iterator.hasNext()){
            PracticeResult result = new PracticeResult();
            String key = (String) iterator.next();
            result.setCatId(key);
            result.setIntCatId(Integer.parseInt(key));

            try {
                JSONArray  subCatJsonArray = jsonObj.getJSONArray(key);
                ArrayList<PracticeResultSubCat> practiceSubCatList = new ArrayList<PracticeResultSubCat>();
                for(int i = 0 ; i < subCatJsonArray.length() ; i ++ ){
                    JSONObject subCatJson = subCatJsonArray.getJSONObject(i);
                    PracticeResultSubCat subCat = new PracticeResultSubCat();
                    subCat.setSubCatId(subCatJson.getInt("subCategId"));
                    subCat.setProblems(subCatJson.getInt("problems"));
                    practiceSubCatList.add(subCat);
                }
                result.setPracticeResultSubCatList(practiceSubCatList);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            practiceResultList.add(result);
        }
        return practiceResultList;
    }

    /**
     * This method parse the word result
     * @param jsonObj
     * @return
     */
    private ArrayList<WordResult> parseWordResultForPrevHW(JSONObject jsonObj){

        ArrayList<WordResult> wordResultList = new ArrayList<WordResult>();
        Iterator<?> iterator = jsonObj.keys();
        while(iterator.hasNext()){
            WordResult wordResult = new WordResult();
            String key = (String) iterator.next();
            wordResult.setCatIg(key);
            JSONArray subCatJsonArray;
            try {
                subCatJsonArray = jsonObj.getJSONArray(key);
                ArrayList<WordSubCatResult> wordSubCatList = new ArrayList<WordSubCatResult>();
                for(int i = 0 ; i < subCatJsonArray.length() ; i ++ ){
                    JSONObject subCatJson = subCatJsonArray.getJSONObject(i);
                    WordSubCatResult subCat = new WordSubCatResult();
                    subCat.setSubCatId(subCatJson.getInt("subCategId"));
                    wordSubCatList.add(subCat);
                }
                wordResult.setSubCatResultList(wordSubCatList);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            wordResultList.add(wordResult);
        }
        return wordResultList;
    }

    /**
     * This method parse the custome result
     * @param jsonArray
     * @return
     */
    private ArrayList<CustomeResult>  parseCustomeResultForPrevHW(JSONArray jsonArray){
        try {
            ArrayList<CustomeResult> customeResultArrayList = new ArrayList<CustomeResult>();
            for (int i  = 0 ; i < jsonArray.length() ; i ++ ){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                CustomeResult customeResult = new CustomeResult();
                customeResult.setShowAns(jsonObject.getInt("showAnswers"));
                customeResult.setTitle(jsonObject.getString("title"));
                customeResult.setAllowChanges(jsonObject.getInt("allowChanges"));
                customeResult.setSheetName(jsonObject.getString("questionSheet"));
                customeResult.setCustomHwId(jsonObject.getInt("customHwId"));
                customeResult.setResourceAdded(jsonObject.getString("resourceAdded"));


                //for teacher edit homework from check homework
                if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject , "problems")){
                    customeResult.setProblem(jsonObject.getInt("problems"));
                }

                ArrayList<CustomeAns> customeAnsList = new ArrayList<CustomeAns>();
                JSONArray cutomeJsonAnsArray = jsonObject.getJSONArray("answers");
                for(int j = 0 ; j < cutomeJsonAnsArray.length() ; j ++ ){
                    JSONObject customeAnsJson = cutomeJsonAnsArray.getJSONObject(j);
                    CustomeAns customeAns = new CustomeAns();
                    customeAns.setQueNo(customeAnsJson.getString("quesNum"));
                    customeAns.setAnsSuffix(customeAnsJson.getString("ansSuffix"));
                    customeAns.setIsLeftUnit(customeAnsJson.getInt("isLeftUnit"));
                    customeAns.setFillInType(customeAnsJson.getInt("fillInType"));
                    customeAns.setAnswerType(customeAnsJson.getInt("answerType"));
                    try {
                        if (MathFriendzyHelper.checkForKeyExistInJsonObj
                                (customeAnsJson, "ansAvailable")) {
                            customeAns.setIsAnswerAvailable(customeAnsJson.getInt("ansAvailable"));
                        } else {
                            customeAns.setIsAnswerAvailable(MathFriendzyHelper.YES);
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                        customeAns.setIsAnswerAvailable(MathFriendzyHelper.YES);
                    }

                    if(customeAns.getFillInType() == FILL_IN_TYPE){
                        customeAns.setCorrectAns(customeAnsJson.getString("correctAns"));
                    }else{
                        int answerType = customeAns.getAnswerType();
                        if(customeAns.getAnswerType() == MULTIPLE_CHOICE_ANS_TYPE){
                            String correctAndWithCommas = customeAnsJson.getString("correctAns");
                            customeAns.setCorrectAns(MathFriendzyHelper.
                                    removeDelimeterFromString(correctAndWithCommas , ","));
                            String optionsWithCommas = customeAnsJson.getString("options");
                            ArrayList<String> options = MathFriendzyHelper.
                                    getCommaSepratedOptionInArrayList(optionsWithCommas , ",");
                            customeAns.setMultipleChoiceOptionList(options);
                        }else if(answerType == YES_NO_ANS_TYPE){
                            customeAns.setCorrectAns(customeAnsJson.getString("correctAns"));
                        }else if(answerType == TRUE_FALSE_ANS_TYPE){
                            customeAns.setCorrectAns(customeAnsJson.getString("correctAns"));
                        }
                    }

                    if(MathFriendzyHelper.checkForKeyExistInJsonObj(customeAnsJson , "questionStr")){
                        customeAns.setQuestionString(customeAnsJson.getString("questionStr"));
                    }else{
                        customeAns.setQuestionString("");
                    }
                    customeAnsList.add(customeAns);
                }
                customeResult.setCustomeAnsList(customeAnsList);

                if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject , "resources")){
                    customeResult.setSelectedResourceList(
                            this.parseGooruResources(jsonObject.getJSONArray("resources")));
                }

                if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject , "links")){
                    customeResult.setLinks(this.getUrlLinks(jsonObject.getString("links")));
                }
                customeResultArrayList.add(customeResult);
            }
            return customeResultArrayList;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the country city response
     * @param jsonString
     * @return
     */
    private CountryCityResponse parseCitiesResponses(String jsonString) {
        try {
            CountryCityResponse response = new CountryCityResponse();
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));
            JSONArray jsonDataArray = jsonObj.getJSONArray("data");
            ArrayList<String> cities = new ArrayList<String>();
            for(int i = 0 ; i < jsonDataArray.length() ; i ++ ){
                cities.add(jsonDataArray.getString(i));
            }
            response.setCities(cities);
            return response;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the subjects response
     * @param jsonString
     * @return
     */
    private MainSubject parseSubjectResponse(String jsonString) {
        try {
            MainSubject mainSubject = new MainSubject();
            JSONObject jsonObj = new JSONObject(jsonString);
            mainSubject.setResult(jsonObj.getString("result"));

            ArrayList<MainSubject> mainSubjects = new ArrayList<MainSubject>();
            JSONArray jsonArray = jsonObj.getJSONArray("data");
            for(int i = 0 ; i < jsonArray.length() ; i ++ ){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                MainSubject mainSubData = new MainSubject();
                mainSubData.setSubjectId(jsonObject.getInt("id"));
                mainSubData.setSubjectName(jsonObject.getString("mainSubject"));

                ArrayList<ClassSubjects> classSubjects = new ArrayList<ClassSubjects>();
                JSONArray jsonClassSubject = jsonObject.getJSONArray("subjects");
                for(int j = 0 ; j < jsonClassSubject.length() ; j ++ ){
                    JSONObject jsonClassObj = jsonClassSubject.getJSONObject(j);
                    ClassSubjects classSubject = new ClassSubjects();
                    classSubject.setSubjectId(jsonClassObj.getInt("subjectId"));
                    classSubject.setClassSubjectName(jsonClassObj.getString("name"));
                    classSubjects.add(classSubject);
                }
                mainSubData.setClassSubjects(classSubjects);
                mainSubjects.add(mainSubData);
            }
            mainSubject.setMainSubjects(mainSubjects);
            return mainSubject;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }



    /**
     * Parse the save homework response
     * @param jsonString
     * @return
     */
    private AssignHomeworkResponse parseSaveHomeworkResponse(String jsonString){
        AssignHomeworkResponse response = new AssignHomeworkResponse();

        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside parseAssignHomeworkResponse " + jsonString);

        try {
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setResult(jsonObj.getString("result"));
            response.setData(jsonObj.getString("id"));
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the vedio link from khan academy url
     * @param jsonString
     * @return
     */
    private GetKhanVideoLinkResponse paserKhanVideoLinkResponse(String jsonString) {
        try {
            CommonUtils.printLog(TAG, "inside paserKhanVideoLinkResponse response " + jsonString);
            GetKhanVideoLinkResponse response = new GetKhanVideoLinkResponse();
            JSONObject jsonObj = new JSONObject(jsonString);
            response.setLink(jsonObj.getString("link"));
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the url links for the student and teacher
     * @param links
     * @return
     */
    private ArrayList<AddUrlToWorkArea> getUrlLinks(String links){
        ArrayList<AddUrlToWorkArea> urlLinks = new ArrayList<AddUrlToWorkArea>();
        try{
            if(!MathFriendzyHelper.isEmpty(links)){
                JSONArray jsonArray = new JSONArray(links);
                for(int i = 0 ; i < jsonArray.length() ; i ++ ){
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    AddUrlToWorkArea urlToWorkArea = new AddUrlToWorkArea();
                    urlToWorkArea.setTitle(jsonObject.getString("title"));
                    urlToWorkArea.setUrl(jsonObject.getString("url"));
                    urlLinks.add(urlToWorkArea);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return urlLinks;
    }
}
