package com.mathfriendzy.serveroperation;

import android.util.Log;

import com.mathfriendzy.controller.managetutor.settutor.GetSubjectParam;
import com.mathfriendzy.controller.resources.GetKhanVideoLinkParam;
import com.mathfriendzy.helper.GetAppVersionParam;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.appactiveinactivestatus.GetUserActiveStatusParam;
import com.mathfriendzy.model.appactiveinactivestatus.PpdateLastActivityTimeForPlayerParam;
import com.mathfriendzy.model.appunlock.GetAppUnlockStatusParam;
import com.mathfriendzy.model.country.GetCountryCityParam;
import com.mathfriendzy.model.helpastudent.AddServiceTimeForTutorParam;
import com.mathfriendzy.model.helpastudent.CheckForTutorParam;
import com.mathfriendzy.model.helpastudent.ConnectWithStudentParam;
import com.mathfriendzy.model.helpastudent.GetPlayerNeedHelpForTutorParam;
import com.mathfriendzy.model.helpastudent.LoadServiceTimeParam;
import com.mathfriendzy.model.helpastudent.TutorAreaResponsesParam;
import com.mathfriendzy.model.helpastudent.UpdateChatRequestWithDialogParam;
import com.mathfriendzy.model.homework.AddCustomeAnswerByStudentParam;
import com.mathfriendzy.model.homework.GetAnswerDetailForStudentHWParam;
import com.mathfriendzy.model.homework.GetHomeworksForStudentWithCustomParam;
import com.mathfriendzy.model.homework.GetStudentsForWorkAreaHelpParam;
import com.mathfriendzy.model.homework.GetWorkAreaChatMsgParam;
import com.mathfriendzy.model.homework.SaveGDocLinkOnServerParam;
import com.mathfriendzy.model.homework.SaveHomeWorkScoreParam;
import com.mathfriendzy.model.homework.SaveHomeWorkWordPlayedDataParam;
import com.mathfriendzy.model.homework.SaveRatingStarForHelpWorkAreaParam;
import com.mathfriendzy.model.homework.SaveWorkAreaChatParam;
import com.mathfriendzy.model.homework.UpdateInputStatusForWorkAreaParam;
import com.mathfriendzy.model.homework.assignhomework.AssignHomeworkParam;
import com.mathfriendzy.model.homework.assignhomework.GetPreviousAssignHomeworkParam;
import com.mathfriendzy.model.homework.assignhomework.GetStudentByGradeParam;
import com.mathfriendzy.model.homework.assignhomework.GetStudentForSchoolClassesParam;
import com.mathfriendzy.model.homework.checkhomework.GetDetailsOfHomeworkWithCustomParam;
import com.mathfriendzy.model.homework.checkhomework.GetHomeworksWithCustomForTeacher;
import com.mathfriendzy.model.homework.checkhomework.GetQuestionDetailsForTeacherParam;
import com.mathfriendzy.model.homework.checkhomework.SaveCheckTeacherChangesParam;
import com.mathfriendzy.model.homework.checkhomework.UpdateCreditByTeacherForStudentQuestionParam;
import com.mathfriendzy.model.homework.savehomework.GetSavedHomeworkParam;
import com.mathfriendzy.model.homework.savehomework.SaveHomeworkParam;
import com.mathfriendzy.model.homework.secondworkarea.AddDuplicateWorkAreaParam;
import com.mathfriendzy.model.inapp.UpdateUserCoinsParam;
import com.mathfriendzy.model.managetutor.mystudents.GetHomeworkDetailForChatRequestParam;
import com.mathfriendzy.model.managetutor.mystudents.GetStudentWithPasswordParam;
import com.mathfriendzy.model.managetutor.mystudents.GetTutoringDetailForTeacherParam;
import com.mathfriendzy.model.managetutor.mystudents.MarkAsTutorParam;
import com.mathfriendzy.model.managetutor.mystudents.SendEmailToOtherTeacherParam;
import com.mathfriendzy.model.professionaltutoring.AddPurchaseTimeParam;
import com.mathfriendzy.model.professionaltutoring.CancelProfileParam;
import com.mathfriendzy.model.professionaltutoring.CreditCardPaymentParam;
import com.mathfriendzy.model.professionaltutoring.GetPlayerPurchaseTimeParam;
import com.mathfriendzy.model.professionaltutoring.GetTutorPackageParam;
import com.mathfriendzy.model.professionaltutoring.PurchaseRecurringPackageParam;
import com.mathfriendzy.model.professionaltutoring.ShouldPaidTutorAllowParam;
import com.mathfriendzy.model.ratetutorsession.RateTutorSessionParam;
import com.mathfriendzy.model.registration.classes.GetSchoolClassesParam;
import com.mathfriendzy.model.resource.GetGooruResourcesForHWParam;
import com.mathfriendzy.model.resource.GetResourceCategoriesParam;
import com.mathfriendzy.model.resource.GetResourceVideoInAppStatusParam;
import com.mathfriendzy.model.resource.ResourceParam;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.DownloadCategoriesForGradesParam;
import com.mathfriendzy.model.tutor.AddTimeSpentInTutorSessionParam;
import com.mathfriendzy.model.tutor.EditTutorAreaParam;
import com.mathfriendzy.model.tutor.FindTutorParam;
import com.mathfriendzy.model.tutor.GetActiveTutorParam;
import com.mathfriendzy.model.tutor.GetAnnonymousTutorParam;
import com.mathfriendzy.model.tutor.GetChatRequestIdParam;
import com.mathfriendzy.model.tutor.GetChatRequestParam;
import com.mathfriendzy.model.tutor.GetDrawingPointForChatParam;
import com.mathfriendzy.model.tutor.GetMessagesForTutorRequestParam;
import com.mathfriendzy.model.tutor.InputSeenByPlayerParam;
import com.mathfriendzy.model.tutor.ReportThisTutorParam;
import com.mathfriendzy.model.tutor.SaveMessageForSingleChatParam;
import com.mathfriendzy.model.tutor.SendMessageToTutorParam;
import com.mathfriendzy.model.tutor.StopWaitingForTutorParam;
import com.mathfriendzy.model.tutor.TutorSessionDisconnectedBuTutorParam;
import com.mathfriendzy.model.tutor.UpdateChatIdParam;
import com.mathfriendzy.model.tutor.UpdateDrawPointsForPlayerParam;
import com.mathfriendzy.model.tutor.UpdateTutorAreaImageName;
import com.mathfriendzy.model.tutoringsessions.TutoringSessionsForStudentParam;
import com.mathfriendzy.notification.CancelChatRequestParam;
import com.mathfriendzy.serveroperation.serveroperationparamresponseclasses.GetActiveInActiveStatusForTutorParam;
import com.mathfriendzy.serveroperation.serveroperationparamresponseclasses.SetTutorAreaStatusParam;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;
import com.mathfriendzy.utils.MyApplication;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;



/**
 * This is the class where all the server opeation will be done
 * @author Yashwant Singh
 *
 */
public class ServerOperation {

    private final static String TAG = "ServerOperation";

    /** This method read the data from url and return it into string form */
    public static String readFromURL(String strURL) {

        StringBuffer finalString = new StringBuffer();
        try {
            URL url = new URL(strURL);

            BufferedReader in = new BufferedReader(new
                    InputStreamReader(url.openStream()), 8*1024);

            String str = "";
            while ((str = in.readLine()) != null){
                finalString.append(str);
            }
            in.close();
        }
        catch (Exception e){
            Log.e(TAG, "Error while reading from url " + e.toString());
            return null;
        }
        return finalString.toString();
    }

    /**
     * This method read the data from url and return it into string form
     * @param nameValuePairs
     * @return
     */
    public static String readFromURL(ArrayList<NameValuePair> nameValuePairs , String url){

        if(CommonUtils.LOG_ON){
            Log.e(TAG , " url " + url);
            for(int i = 0 ; i < nameValuePairs.size() ; i ++ ){
                Log.e("requested param " , nameValuePairs.get(i) + "");
            }
        }

        InputStream inputStraem		= null;
        StringBuilder buffer 		= new StringBuilder("");

        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs , "UTF-8"));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            inputStraem = entity.getContent();

            BufferedReader reader = new BufferedReader(new InputStreamReader
                    (inputStraem,"iso-8859-1"),8);
            buffer = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null){
                buffer.append(line);
            }
            inputStraem.close();
        }
        catch (Exception e){
            Log.e(TAG, "Error while reading from url " + e.toString());
            return null;
        }
        return buffer.toString();
    }

    /**
     * Create the string url for the post request
     * @param nameValuePairs
     * @param url
     * @return
     */
    public static String getUrl(ArrayList<NameValuePair> nameValuePairs , String url){
        StringBuilder strBuilder = new StringBuilder(url);
        for(int i = 0 ; i < nameValuePairs.size() ; i ++ ){
            if(i == 0)
                strBuilder.append(nameValuePairs.get(i));
            else
                //strBuilder.append("%26" + nameValuePairs.get(i));
                strBuilder.append("&" + nameValuePairs.get(i));
        }
        //return url + URLEncoder.encode(strBuilder.toString());
        return strBuilder.toString();
    }

    /**
     * When we are sending a post request then we need a request url
     * So We get url by using the request code
     * @param requestCode
     * @return
     */
    public static String getUrl(int requestCode){
        if(requestCode == ServerOperationUtil.GETHOMEWORKSFORSTUDENTSWITHCUSTOME)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.SAVE_HOMEWORK_SCORE)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.SAVE_HOME_WORK_WORD_PLAY_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.SAVE_HW_CUSTOME_ANS_BY_STUDENTS)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_HELP_STUDENT_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.SAVE_STUDENT_WORK_AREA_RATING_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_STUDENTS_BY_GRADE_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.ASSIGN_HOMEWORK_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GETHOMEWORK_WITH_CUSTOME_TEACHER_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_DETAIL_STUDENT_HOME_WORK_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.SAVE_TEACHER_CHECK_HW_CHANGES_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.SAVE_PLAY_SCORE_WHEN_NO_INTERNET)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_WORK_AREA_CHAT_MSG_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.SAVE_WORK_AREA_CHAT_MSG_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_TUTOR_BY_USERNAME_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_ANNONYMOUS_TUTOR_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.UPDATE_CHAT_ID_TO_SERVER_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_CHAT_REQUEST_SERVER_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.SEND_MESSAGE_TO_ANONYMOUS_TUTOR_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_PLAYER_NEED_HELP_FOR_TUTOR_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.CONNECT_WITH_STUDENT_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_CHAT_REQUEST_ID_FOR_PROBLEM_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.UPDATE_CHAT_REQUEST_WITH_DIALOG_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.SAVE_SINGLE_CHAT_MESSAGE_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.UPDATE_DRAW_POINT_FOR_PLAYER_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_DRAWING_POINT_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.ADD_TIME_SPENT_IN_TUTOR_SESSION_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.RATE_TUTOR_TUTOR_SESSION_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_MY_STUDENTS_WITH_PASSWORD)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.MARK_AS_TUTOR_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_STUDENT_TUTORING_DETAIL_FOR_TEACHER_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_HOMEWORK_DETAIL_BY_CHAT_ID_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.SEND_EMAIL_TO_OTHER_TEACHER_REQUEST)
            return ICommonUtils.COMPLETE_URL_TO_SEND_EMAIL;
        if(requestCode == ServerOperationUtil.GET_SERVICE_TIME_FOR_TUTOR_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.TUTOR_SESSION_DESCONNECT_BY_TUTOR_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.ADD_SERVICE_TIME_FOR_TUTOR_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.STOP_WAITING_FOR_TUTOR_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.EDIT_TUTOR_AREA_BY_STUDENT_OR_TUTOR)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.REPORT_THIS_TUTOR_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.INPUT_SEEN_BY_PLAYER_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.UPDATE_LAST_ACTIVITY_TIME_FOR_PLAYER_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_USER_ACTIVE_STATUS_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.CHECK_FOR_TUTOR_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.UPDATE_TUTOR_AREA_IMAGE_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.ADD_DUPLICATE_WORK_FOR_STUDENT)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_QUESTION_DETAIL_FOR_TEACHER_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.UPDATE_INPUT_STATUS_FOR_WORK_AREA)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.CANCEL_CHAT_REQUEST_BY_TUTOR_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_ACTIVE_TUTOR_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_TUTOR_AREA_RESPONSES)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_TUTOR_SESSION_FOR_STUDENT_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_SEARCH_RESOURCES_REQUEST)
            return ICommonUtils.GOORU_SEARCH_RESOURCES_URL;
        if(requestCode == ServerOperationUtil.GET_GOORU_RESOURCES_FOR_HW)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_PLAYER_PURCHASE_TIME)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_TUTOR_PACKAGES)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.UPDATE_PURCHASE_TIME_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_PLAYER_PURCHASE_TIME_INFO)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.CANCEL_PROFILE)
            return ICommonUtils.PAYPAL_CANCEL_PROFILE;
        if(requestCode == ServerOperationUtil.CREDIT_CARD_PAYMENT_REQUEST)
            return ICommonUtils.PAYPAL_CREDIT_CARD_PAYMENT;
        if(requestCode == ServerOperationUtil.GET_TUTORING_SESSION_WITH_TITLE_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.SHOULD_PAID_TUTOR_ALLOW_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_CHAT_MESSAGES_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_RESOURCE_CATEGORIES_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_RESOURCE_VIDEO_IN_APP_STATUS_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.SAVE_RESOURCE_VIDEO_IN_APP_STATUS_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_APP_UNLOCK_STATUS_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.UPDATE_TEACHER_CREDIT_FOR_STUDENT_ANSWER)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.UPDATE_USER_COINS_ON_SERVER)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_STUDENT_ANSWER_DETAIL_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_INACTIVE_STATUS_OF_PLAYER_FOR_TUTOR)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.SET_TUTOR_AREA_STATUS_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_SCHOOL_CLASSES_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_LIVE_APP_VERSION)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.SAVE_HOMEWORK_ON_SERVER_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_SAVED_HOMEWORK_FROM_SERVER_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_WORD_CATEGORIES_FOR_GRADEDS_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_PREV_HOMEWORK_FROM_SERVER_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_CITIES_FROM_SERVER_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_SUBJECTS_FROM_SERVER_REQUEST)
            return ICommonUtils.COMPLETE_URL;
        if(requestCode == ServerOperationUtil.GET_KHAN_VIDEO_LINK)
            return ICommonUtils.GET_KHAN_VIDEO_LINK;
        if(requestCode == ServerOperationUtil.SAVE_GDOC_WEB_LINK_REQUEST)
            return ICommonUtils.COMPLETE_URL;
         return null;
    }

    /**
     * This method create post request for GetHomeworksForStudentWithCustom
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForGetHomeworksForStudentWithCustom
    (GetHomeworksForStudentWithCustomParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
            //nameValuePairs.add(new BasicNameValuePair("grade", param.getGrade()));
            nameValuePairs.add(new BasicNameValuePair("offset", param.getOffset() + ""));
            nameValuePairs.add(new BasicNameValuePair("classId", param.getClassId()));
            nameValuePairs.add(new BasicNameValuePair("limit", param.getLimit() + ""));
            nameValuePairs.add(new BasicNameValuePair("startDate", param.getStartDate()));
            nameValuePairs.add(new BasicNameValuePair("endDate", param.getEndDate()));
            nameValuePairs.add(new BasicNameValuePair("forTeacher", param.getForTeacher() + ""));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * This method save home score on server
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForSaveHomeWork
    (SaveHomeWorkScoreParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("time", param.getTime()));
            nameValuePairs.add(new BasicNameValuePair("score", param.getScore()));
            nameValuePairs.add(new BasicNameValuePair("homeworkId", param.getHWID()));
            //nameValuePairs.add(new BasicNameValuePair("homeworkId", "59"));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("questionsCount", param.getQuesitonCount()
                    + ""));
            nameValuePairs.add(new BasicNameValuePair("problems", param.getProblems()));
            //nameValuePairs.add(new BasicNameValuePair("problems", "<equations></equations>"));
            nameValuePairs.add(new BasicNameValuePair("points", param.getPoints() + ""));
            nameValuePairs.add(new BasicNameValuePair("coins", param.getCoinds() + ""));
            nameValuePairs.add(new BasicNameValuePair("mathCategoryId", param.getMathCatId()));
            nameValuePairs.add(new BasicNameValuePair("mathSubCategoryId", param.getMathSubCatId() + ""));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * This method save home score on server
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForSaveHomeWorkForWordProblem
    (SaveHomeWorkWordPlayedDataParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("problems", param.getProblems()));
            //nameValuePairs.add(new BasicNameValuePair("problems", "<equations></equations>"));
            nameValuePairs.add(new BasicNameValuePair("categoryId", param.getCatId() + ""));
            nameValuePairs.add(new BasicNameValuePair("subCategoryId", param.getSubCatId() + ""));
            nameValuePairs.add(new BasicNameValuePair("time", param.getTime()));
            nameValuePairs.add(new BasicNameValuePair("score", param.getScore() + ""));
            nameValuePairs.add(new BasicNameValuePair("homeworkId", param.getHWId()));
            //nameValuePairs.add(new BasicNameValuePair("homeworkId", "59"));
            nameValuePairs.add(new BasicNameValuePair("points", param.getPints() + ""));
            nameValuePairs.add(new BasicNameValuePair("coins", param.getCoins() + ""));

        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * This method save home score on server
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForSaveCustomeAnsBuStudents
    (AddCustomeAnswerByStudentParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("homeworkId", param.getHWId()));
            //nameValuePairs.add(new BasicNameValuePair("homeworkId", "59"));
            nameValuePairs.add(new BasicNameValuePair("customHwId", param.getCustomHWId()));
            //nameValuePairs.add(new BasicNameValuePair("customHwId", "40"));
            nameValuePairs.add(new BasicNameValuePair("numOfQuestions", param.getNumberOfQuestions() + ""));
            nameValuePairs.add(new BasicNameValuePair("score", param.getScore() + ""));
            nameValuePairs.add(new BasicNameValuePair("questions", param.getQuestionJson()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request to get the student help list
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostToGethelpStudentList
    (GetStudentsForWorkAreaHelpParam param){

        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("homeworkId", param.getHWId()));
            nameValuePairs.add(new BasicNameValuePair("customHwId", param.getCutomeHWId()));
            nameValuePairs.add(new BasicNameValuePair("questionId", param.getQuestionId()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * Create post request to save student rating for work area help
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostToSaveStudentWorkAreaHelp
    (SaveRatingStarForHelpWorkAreaParam param){

        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("homeworkId", param.getHWId()));
            nameValuePairs.add(new BasicNameValuePair("customHwId", param.getCutomeHWId()));
            nameValuePairs.add(new BasicNameValuePair("questionId", param.getQuestionId()));
            nameValuePairs.add(new BasicNameValuePair("stars", param.getRatingStar() + ""));
            nameValuePairs.add(new BasicNameValuePair("senderUser", param.getSenderUserId()));
            nameValuePairs.add(new BasicNameValuePair("senderPlayer", param.getSenderPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("workImage", param.getWorkImageName() + ""));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request for get student by grade
     * @param param
     * @return
     */
    public static  ArrayList<NameValuePair> createPostRequestForGetStudentByGrade
    (GetStudentByGradeParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("teacherId", param.getTeacherId()));
            nameValuePairs.add(new BasicNameValuePair("grade", param.getGrade()));
            nameValuePairs.add(new BasicNameValuePair("year", param.getSchoolYear()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request for Assign Homework
     * @param param
     * @return
     */
    public static  ArrayList<NameValuePair> createPostRequestForAssignHomework
    (AssignHomeworkParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("date", param.getDate()));
            nameValuePairs.add(new BasicNameValuePair("grade", param.getGrade()));
            nameValuePairs.add(new BasicNameValuePair("students", param.getStudentJson()));
            nameValuePairs.add(new BasicNameValuePair("practiceCategories",
                    param.getPracticeCatJson()));
            nameValuePairs.add(new BasicNameValuePair("wordCategories", param.getWordCatJson()));
            nameValuePairs.add(new BasicNameValuePair("customData", param.getCustomeDataJson()));
            nameValuePairs.add(new BasicNameValuePair("message", param.getMessage()));
            //To delete the saved homework if assign from saved homework
            nameValuePairs.add(new BasicNameValuePair("savedHwId", param.getHwId() + ""));

            if(param.isTeacherEditHomework()){
                nameValuePairs.add(new BasicNameValuePair("hwId", param.getHwId() + ""));
            }
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request for Assign Homework
     * @param param
     * @return
     */
    public static  ArrayList<NameValuePair> createPostRequestForGetHomeworksWithCustomForTeacher
    (GetHomeworksWithCustomForTeacher param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getTeacherId()));
            nameValuePairs.add(new BasicNameValuePair("startDate", param.getStartDate()));
            nameValuePairs.add(new BasicNameValuePair("endDate", param.getEndDate()));
            nameValuePairs.add(new BasicNameValuePair("classId", param.getClassId()));
            nameValuePairs.add(new BasicNameValuePair("offset", param.getOffset() + ""));
            nameValuePairs.add(new BasicNameValuePair("limit", param.getLimit() + ""));
            nameValuePairs.add(new BasicNameValuePair("firstVisit", param.getFirstVisit() + ""));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request for GetDetailsOfHomeworkWithCustom
     * @param param
     * @return
     */
    public static  ArrayList<NameValuePair> createPostRequestForGetDetailsOfHomeworkWithCustom
    (GetDetailsOfHomeworkWithCustomParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("homeworkId", param.getHWId()));
            nameValuePairs.add(new BasicNameValuePair("classId", param.getSelectedClassId() + ""));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * This method save home score on server
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForAddNotesByTeacherForStudent
    (SaveCheckTeacherChangesParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("data", param.getData()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * This method save the local play data , saved when no Internet connected at the time
     * of playing
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForAddLocalPlayData
    (SaveCheckTeacherChangesParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("data", param.getData()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }



    /**
     * This method save the local play data , saved when no Internet connected at the time
     * of playing
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForAddLocalPlayDataForHomework
    (String data){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", "server"));
            nameValuePairs.add(new BasicNameValuePair("data", data));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * Create post request to get work area chat message
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestToGetChatWorkAreaMsg
    (GetWorkAreaChatMsgParam param){

        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("homeworkId", param.getHWId()));
            nameValuePairs.add(new BasicNameValuePair("customHwId", param.getCustomeHWId()));
            nameValuePairs.add(new BasicNameValuePair("question", param.getQuestionNo()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * Create post request to save work area chat message
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestToSaveChatWorkAreaMsg
    (SaveWorkAreaChatParam param){

        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("homeworkId", param.getHWId()));
            nameValuePairs.add(new BasicNameValuePair("customHwId", param.getCustomeHWId()));
            nameValuePairs.add(new BasicNameValuePair("question", param.getQuestionNo()));
            nameValuePairs.add(new BasicNameValuePair("message", param.getMessage()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * Create post request to get tutor by username
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostToGetTutorByUserName(FindTutorParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("uname", param.getUserName()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * Create post request to get annonymous tutor
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostToGetAnnonymousTutor(GetAnnonymousTutorParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("schoolId", param.getSchoolId()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request to get annonymous tutor
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestToupdateChatIdForPlayer
    (UpdateChatIdParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("chatId", param.getChatId()));
            nameValuePairs.add(new BasicNameValuePair("chatUserName", param.getChatUserName()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * Create post request to get chat request by request id
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostTOGetChatRequest(GetChatRequestParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("requestId", param.getChatRequestId() + ""));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * Create post request to send message to anonymous tutor
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostTOSendMessageToAnonymousTutor(SendMessageToTutorParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("data", param.getData()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * This method create post request for get player need for tutor
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForGetPlayersNeedHelpTutor
    (GetPlayerNeedHelpForTutorParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("isTutor", param.getIsTutor()));
            nameValuePairs.add(new BasicNameValuePair("grade", param.getGrade()));
            nameValuePairs.add(new BasicNameValuePair("forSubject", param.getForSubject() + ""));

            if(!param.isFirstTime()){
                nameValuePairs.add(new BasicNameValuePair("isConnect", param.getIsConnected() + ""));
                nameValuePairs.add(new BasicNameValuePair("offset", param.getOffSet() + ""));
            }
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * This method create post request for get player need for tutor
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForConnectWithStudent
    (ConnectWithStudentParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("data", param.getData()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * Create post request to get chat request id for problem
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostTOGetChatRequestIdForProblem(GetChatRequestIdParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("hwId", param.getHwId() + ""));
            nameValuePairs.add(new BasicNameValuePair("customHwId", param.getCustomeHwId() + ""));
            nameValuePairs.add(new BasicNameValuePair("uId", param.getUserId() + ""));
            nameValuePairs.add(new BasicNameValuePair("pId", param.getPlayerId() + ""));
            nameValuePairs.add(new BasicNameValuePair("question", param.getQuestion() + ""));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * This method create post request for update chat request with dialog
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForUpdateChatRequestWithDialog
    (UpdateChatRequestWithDialogParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("data", param.getData()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * Create post request to save message for single chat
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForSaveMessageForSingleChat
    (SaveMessageForSingleChatParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("requestId", param.getRequestId() + ""));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("message", param.getMessage()));
            nameValuePairs.add(new BasicNameValuePair("params", param.getParams()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * Create post request to save message for single chat
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForUpdateDrawPonitsForPlayer
    (UpdateDrawPointsForPlayerParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("requestId", param.getRequestId() + ""));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("drawnPoints", param.getDrawPoints()));
            nameValuePairs.add(new BasicNameValuePair("deviceSize", param.getDeviceSize()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request to get drawing points from server for chat
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForGetDrawingPointForChat
    (GetDrawingPointForChatParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("requestId", param.getRequestId() + ""));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request to add time spent in tutor session
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForAddTimeSpentInTutorSession
    (AddTimeSpentInTutorSessionParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("requestId", param.getRequestId() + ""));
            int timeSpent = param.getTimeSpent();
            if(timeSpent == 0)
                timeSpent = 1;
            nameValuePairs.add(new BasicNameValuePair("time", timeSpent + ""));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * Create post request to rate tutor session
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForRateTutorSession
    (RateTutorSessionParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("requestId", param.getRequestId() + ""));
            nameValuePairs.add(new BasicNameValuePair("stars", param.getStars() + ""));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request to get student with password
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForGetStudentsWithPassword
    (GetStudentWithPasswordParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            //nameValuePairs.add(new BasicNameValuePair("offset", param.getOffset() + ""));
            nameValuePairs.add(new BasicNameValuePair("startDate", param.getStartDate()));
            nameValuePairs.add(new BasicNameValuePair("endDate", param.getEndDate()));
            nameValuePairs.add(new BasicNameValuePair("timeZone", param.getTimeZone()));
            nameValuePairs.add(new BasicNameValuePair("year", param.getYear()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request to get student with password
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForGetStudentsWithPasswordFromClass
    (GetStudentWithPasswordParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("classId", param.getClassId()));
            if(!param.isForStudentAccount()){
                //nameValuePairs.add(new BasicNameValuePair("offset", param.getOffset() + ""));
                nameValuePairs.add(new BasicNameValuePair("startDate", param.getStartDate()));
                nameValuePairs.add(new BasicNameValuePair("endDate", param.getEndDate()));
                //nameValuePairs.add(new BasicNameValuePair("limit", param.getLimit() + ""));
                nameValuePairs.add(new BasicNameValuePair("timeZone", param.getTimeZone()));
                nameValuePairs.add(new BasicNameValuePair("getTime", param.getGetTime()));
            }
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request to get student with password
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForGetStudentsWithPasswordOnlyByUserId
    (GetStudentWithPasswordParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            //nameValuePairs.add(new BasicNameValuePair("offset", param.getOffset() + ""));
            nameValuePairs.add(new BasicNameValuePair("year", param.getYear()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request to mark as tutor
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForMarkAsTutor
    (MarkAsTutorParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("data", param.getData()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request to getStudentsTutoringDetailsForTeacher
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForGetStudentsTutoringDetailsForTeacher
    (GetTutoringDetailForTeacherParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
            if(!param.isFirstTime()){
                nameValuePairs.add(new BasicNameValuePair("isTutorList", param.getIsTutorList() + ""));
                nameValuePairs.add(new BasicNameValuePair("offset", param.getOffset() + ""));
            }
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request for getHomeworkDetailsForChatRequestId
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForGetHomeworkDetailsForChatRequestId
    (GetHomeworkDetailForChatRequestParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("chatId", param.getChatId() + ""));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * Create post request for SendEmailToOtherTeacher
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForSendEmailToOtherTeacher
    (SendEmailToOtherTeacherParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("data", param.getData()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * This method create post request for load tutor service time
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForLoadServiceTime
    (LoadServiceTimeParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("startDate", param.getStartDate()));
            nameValuePairs.add(new BasicNameValuePair("endDate", param.getEndData()));
            nameValuePairs.add(new BasicNameValuePair("timeZone", param.getTimeZone()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request to TutorSessionDisconnectedBuTutorParam
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForTutorSessionDisconnectByTutor
    (TutorSessionDisconnectedBuTutorParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("requestId", param.getChatRequestId() + ""));
            nameValuePairs.add(new BasicNameValuePair("name", param.getName()));
            if(!param.isDiconnectTutorWithoutAnswer()) {
                nameValuePairs.add(new BasicNameValuePair("tutorUid", param.getTutorUid()));
                nameValuePairs.add(new BasicNameValuePair("tutorPid", param.getTutorPid()));
                nameValuePairs.add(new BasicNameValuePair("date", param.getDate()));
            }
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * Create post request for AddServiceTimeForTutorParam
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForAddServieTimeForTutor
    (AddServiceTimeForTutorParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("time", param.getTimeInSeconds() + ""));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request for stop waiting for tutor
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForStopWaitingForTutor
    (StopWaitingForTutorParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("requestId", param.getRequestId() + ""));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request for edit tutor area by tutor or by student
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForEditTutorArea(EditTutorAreaParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("requestId", param.getReqId() + ""));

            if(param.getForSubject() > 0){
                nameValuePairs.add(new BasicNameValuePair("forSubject", param.getForSubject() + ""));
            }
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * Report this tutor
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForReportThisTutor
    (ReportThisTutorParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("senderEmail", param.getSenderEmail()));
            nameValuePairs.add(new BasicNameValuePair("studName", param.getStudentName()));
            nameValuePairs.add(new BasicNameValuePair("studSchool", param.getStudentSchool()));
            nameValuePairs.add(new BasicNameValuePair("studTeacher", param.getStudentTeacher()));
            nameValuePairs.add(new BasicNameValuePair("requestId", param.getRequestId() + ""));
            nameValuePairs.add(new BasicNameValuePair("message", param.getMessage()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request for input seen by player
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForInputSeenByPlauer(InputSeenByPlayerParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("requestId", param.getCharRequestId() + ""));
            nameValuePairs.add(new BasicNameValuePair("isStudent", param.getIsStudent() + ""));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * This method create post request for update active inactive app status
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForUpdateLastActivityTimeForPlayer
    (PpdateLastActivityTimeForPlayerParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request for get active inactive user status
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForGetUserActiveStatus
    (GetUserActiveStatusParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Check for is tutor
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForCheckIsTutor
    (CheckForTutorParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Update the tutor area image name
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForUpdateTutorAreaImageName
    (UpdateTutorAreaImageName param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("requestId", param.getRequestId()));
            nameValuePairs.add(new BasicNameValuePair("image", param.getImageName()));
            if(param.getStudentUserId() != null)
                nameValuePairs.add(new BasicNameValuePair("studUserId", param.getStudentUserId()));
            if(param.getShouldSendPN() != -1)
                nameValuePairs.add(new BasicNameValuePair("shouldSendPN", param.getShouldSendPN() + ""));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * Create post request for add duplicate work for student for second work area tab
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForAddDuplicateWorkForStudent
    (AddDuplicateWorkAreaParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("homeworkId", param.getHwId()));
            nameValuePairs.add(new BasicNameValuePair("customHwId", param.getCustomHWId()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("question", param.getQuestion()));
            nameValuePairs.add(new BasicNameValuePair("workImageName", param.getWorkImageName()));
            nameValuePairs.add(new BasicNameValuePair("questionImage", param.getQuestionImageName()));
            nameValuePairs.add(new BasicNameValuePair("fixedWorkImage", param.getFixedWorkImageName()));
            nameValuePairs.add(new BasicNameValuePair("fixedMessage", param.getFixedMessage()));
            nameValuePairs.add(new BasicNameValuePair("fixedQuestionImage", param.getFixedQuestionImage()));
            nameValuePairs.add(new BasicNameValuePair("gotHelp", param.getGothelp() + ""));
            nameValuePairs.add(new BasicNameValuePair("sawAnswer", param.getSawAnswer() + ""));
            nameValuePairs.add(new BasicNameValuePair("uploadOnS3", "1"));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * Create post request for get question detail for teacher
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForGetQuestionDetailsForTeacher
    (GetQuestionDetailsForTeacherParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("homeworkId", param.getHwId()));
            nameValuePairs.add(new BasicNameValuePair("customHwId", param.getCustomHWId()));
            nameValuePairs.add(new BasicNameValuePair("quesNum", param.getQuesNo()));

        }catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Update input status for work area
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForUpdateInputStatusForWorkArea
    (UpdateInputStatusForWorkAreaParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId" , param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("homeworkId", param.getHwId()));
            nameValuePairs.add(new BasicNameValuePair("customHwId", param.getCustomHWId()));
            nameValuePairs.add(new BasicNameValuePair("quesNum", param.getQuestion()));
            nameValuePairs.add(new BasicNameValuePair("isStudent", param.getIsStudent() + ""));

        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * Cancel chat request by the tutor
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForCancelChatRequest
    (CancelChatRequestParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("chatReqId" , param.getChatRequestId() + ""));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     *Create post request to get active anonymous tutor
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostToGetActiveTutor(GetActiveTutorParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("schoolId", param.getSchoolId()));
            nameValuePairs.add(new BasicNameValuePair("grade", param.getGrade() + ""));
            nameValuePairs.add(new BasicNameValuePair("subjectId", param.getSubjectId() + ""));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request for tutor area responses
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForTutorAreaResponses
    (TutorAreaResponsesParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * This method create post request for load tutor session for student
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForLoadServiceTime
    (TutoringSessionsForStudentParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("offset", param.getOffset() + ""));
            nameValuePairs.add(new BasicNameValuePair("limit", param.getLimit() + ""));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * This method create post request for GetHomeworksForStudentWithCustom
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> CreatePostRequestForGetResources(ResourceParam param){

        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("grade", param.getGrade()));
            nameValuePairs.add(new BasicNameValuePair("pageNumber", param.getPageNumber()));
            nameValuePairs.add(new BasicNameValuePair("pageSize", param.getPageSize()));
            nameValuePairs.add(new BasicNameValuePair("query", param.getQuery()));
            nameValuePairs.add(new BasicNameValuePair("resourceFormat", param.getResourceFormat()));
            nameValuePairs.add(new BasicNameValuePair("subjectName", param.getSubjectName()));
            //nameValuePairs.add(new BasicNameValuePair("language", param.getSelectedLang() + ""));
            nameValuePairs.add(new BasicNameValuePair("language", MathFriendzyHelper.
                    getResourceSelectedLanguage(MyApplication.getAppContext()) + ""));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request for get gooru resources for homework
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> CreatePostRequestForGetGooruResourcesForHomework
    (GetGooruResourcesForHWParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("homeworkId", param.getHomeworkId()));
            nameValuePairs.add(new BasicNameValuePair("customHwId", param.getCustomHWId()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * Create post request for get gooru resources for homework
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForGetPlayersPurchasedTime
    (GetPlayerPurchaseTimeParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("uId", param.getuId()));
            nameValuePairs.add(new BasicNameValuePair("pId", param.getpId()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request for get gooru resources for homework
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForGetTutorPackage
    (GetTutorPackageParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * Create post request for purchase time
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForPurchaseTime
    (AddPurchaseTimeParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("uId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("pId", param.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("time", param.getPurchaseTime() + ""));
            if(param.getPurchaseInfo() != null){
                nameValuePairs.add(new BasicNameValuePair("purchaseInfo", param.getPurchaseInfo()));
            }
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    public static ArrayList<NameValuePair> createPostRequestForCancelProfile
            (CancelProfileParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("profileId", param.getProfileId()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    public static ArrayList<NameValuePair> createPostRequestForPurchaseRecurringPackage
            (PurchaseRecurringPackageParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("amount", param.getAmount()));
            nameValuePairs.add(new BasicNameValuePair("itemName", param.getItemName()));
            nameValuePairs.add(new BasicNameValuePair("customParams", param.getCustomParam()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request for credit card payment
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForPayWithCreditCard
    (CreditCardPaymentParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("customParams", param.getCustomParam()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Get tutor session by title
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForGetTutorSessionByTitle
    (TutoringSessionsForStudentParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("title", param.getTitle()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * Create post request for get gooru resources for homework
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForShouldPaidTutorAllow
    (ShouldPaidTutorAllowParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("uId", param.getUid()));
            nameValuePairs.add(new BasicNameValuePair("pId", param.getPid()));
            nameValuePairs.add(new BasicNameValuePair("schoolId", param.getSchoolId()));

        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request to get messages for tutor from server
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForGetMessagesForTutorRequest
    (GetMessagesForTutorRequestParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("requestId", param.getReqId() + ""));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * This method create post request for CreatePostRequestForGetResourcesCategories
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> CreatePostRequestForGetResourcesCategories
    (GetResourceCategoriesParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("date", param.getDate()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * Create the post request to get the vedio resource in-app status
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> CreatePostRequestForGetResourcesVedioInAppStatus
    (GetResourceVideoInAppStatusParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("spentCoins", param.getSpentCoins() + ""));
            nameValuePairs.add(new BasicNameValuePair("appId", param.getAppId()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create Post Request to get the app unlock status from server and save into local device
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> CreatePostRequestForGetAppUnlockStatus
    (GetAppUnlockStatusParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create Post Request to update teacher credit on server for student
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> CreatePostRequestForUpdateTeacherCreditGivenToStudentAnswer
    (UpdateCreditByTeacherForStudentQuestionParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("data", param.getDataJson()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create Post Request to update user coins on server
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> CreatePostRequestForUpdateUserCoins
    (UpdateUserCoinsParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("coins", param.getCoins() + ""));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * Create Post Request to get student answer for student
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> CreatePostRequestForStudentAnswerDetailForHW
    (GetAnswerDetailForStudentHWParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("homeworkId", param.getHwId()));
            nameValuePairs.add(new BasicNameValuePair("customHwId", param.getCusHWId()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request for GetInactiveStatusOfPlayersForTutor
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForGetInactiveStatusOfPlayersForTutor
    (GetActiveInActiveStatusForTutorParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("data", param.getData()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Update the tutor area status
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForSetTutorAreaStatus
    (SetTutorAreaStatusParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("isStudent", param.getIsStudent() + ""));
            nameValuePairs.add(new BasicNameValuePair("online", param.getOnline() + ""));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("requestId", param.getReqId() + ""));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * This method create post request for Get School Classes For User
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForGetSchoolClasses
    (GetSchoolClassesParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("schoolYear", param.getSchoolYear()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * This method create post request for Get live app version Classes For User
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForGetLiveAppVersion
    (GetAppVersionParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("appId", param.getAppId() + ""));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request for get student by grade
     * @param param
     * @return
     */
    public static  ArrayList<NameValuePair> createPostRequestForGetStudentForSchoolClasses
    (GetStudentForSchoolClassesParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("classes", param.getClasses()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request for save homework on server
     * @param param
     * @return
     */
    public static  ArrayList<NameValuePair> createPostRequestForSaveHomework
    (SaveHomeworkParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("hwId", param.getHwId() + ""));
            nameValuePairs.add(new BasicNameValuePair("data", param.getData()));
            nameValuePairs.add(new BasicNameValuePair("classes", param.getClasses()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * Create post request to get saved homework from server
     * @param param
     * @return
     */
    public static  ArrayList<NameValuePair> createPostRequestForGetSavedHomework
    (GetSavedHomeworkParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("offset", param.getOffset() + ""));
            nameValuePairs.add(new BasicNameValuePair("limit", param.getLimit() + ""));
            nameValuePairs.add(new BasicNameValuePair("classId", param.getClassIds()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request for save homework on server
     * @param param
     * @return
     */
    public static  ArrayList<NameValuePair> createPostRequestForDownloadCategoriesForGrades
    (DownloadCategoriesForGradesParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("grade", param.getGrades()));
            nameValuePairs.add(new BasicNameValuePair("lang", param.getLang() + ""));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request to get previous assigned homework from server
     * @param param
     * @return
     */
    public static  ArrayList<NameValuePair> createPostRequestForGetPreviousAssignedHw
    (GetPreviousAssignHomeworkParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("startDate", param.getStartDate()));
            nameValuePairs.add(new BasicNameValuePair("endDate", param.getEndDate()));
            nameValuePairs.add(new BasicNameValuePair("offset", param.getOffset() + ""));
            nameValuePairs.add(new BasicNameValuePair("limit", param.getLimit() + ""));
            nameValuePairs.add(new BasicNameValuePair("classId", param.getClassIds()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * Create post request for get cities for the selected country
     * @param param
     * @return
     */
    public static  ArrayList<NameValuePair> createPostRequestForGetCities
    (GetCountryCityParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("countryCode", param.getCountryISO()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }


    /**
     * Create post request to get subject detail
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForGetSubjectDetail
    (GetSubjectParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * This method create post request for GetHomeworksForStudentWithCustom
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForGetKhanVideoLink
    (GetKhanVideoLinkParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("link", param.getUrl()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }

    /**
     * Create post request to save student gdoc or web link
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostToSaveGDocOrWebLinks
    (SaveGDocLinkOnServerParam param){

        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", param.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("homeworkId", param.getHwId()));
            nameValuePairs.add(new BasicNameValuePair("customHwId", param.getCusHwId()));
            nameValuePairs.add(new BasicNameValuePair("question", param.getQuestionNo()));
            nameValuePairs.add(new BasicNameValuePair("links", param.getLinks()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }
}
