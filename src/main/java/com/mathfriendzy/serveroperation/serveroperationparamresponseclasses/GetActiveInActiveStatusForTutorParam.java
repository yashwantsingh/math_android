package com.mathfriendzy.serveroperation.serveroperationparamresponseclasses;

/**
 * Created by root on 3/3/16.
 */
public class GetActiveInActiveStatusForTutorParam {
    private String action;
    private String data;


    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
