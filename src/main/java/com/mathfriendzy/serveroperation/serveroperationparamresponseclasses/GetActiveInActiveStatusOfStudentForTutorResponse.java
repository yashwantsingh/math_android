package com.mathfriendzy.serveroperation.serveroperationparamresponseclasses;

import com.mathfriendzy.serveroperation.HttpResponseBase;

import java.util.ArrayList;

/**
 * Created by root on 3/3/16.
 */
public class GetActiveInActiveStatusOfStudentForTutorResponse extends HttpResponseBase{
    private String result;
    private String userId;
    private String playerId;
    private int inActive = 1;
    private int onlineRequestId;

    ArrayList<GetActiveInActiveStatusOfStudentForTutorResponse> list
            = new ArrayList<GetActiveInActiveStatusOfStudentForTutorResponse>();

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public int getInActive() {
        return inActive;
    }

    public void setInActive(int inActive) {
        this.inActive = inActive;
    }

    public ArrayList<GetActiveInActiveStatusOfStudentForTutorResponse> getList() {
        return list;
    }

    public void setList(ArrayList<GetActiveInActiveStatusOfStudentForTutorResponse> list) {
        this.list = list;
    }

    public int getOnlineRequestId() {
        return onlineRequestId;
    }

    public void setOnlineRequestId(int onlineRequestId) {
        this.onlineRequestId = onlineRequestId;
    }
}
