package com.mathfriendzy.database;

import com.mathfriendzy.utils.ICommonUtils;

/**
 * Database Constants are defined in this interface
 * @author Yashwant Singh
 *
 */
public interface DatabaseConstant 
{

	 String DATABASE_PATH_ON_DEVICE	 	= "/data/data/com.mathfriendzy/databases";
	 String DATABASE_NAME 				= "LeapAhead.sqlite";
	 String DATABASE_PATH_ON_INTERNET 	= ICommonUtils.HTTP + "://api.letsleapahead.com/AndroidDb/LeapAhead.sqlite";
	 
	 //Language Table Constants
	 String LANGUAGE_TABLE_NAME 	= "Languages";
	 String LANGUAGE_ID 			= "Language_ID";
	 String LANGUAGE_NAME 			= "Language";
	 String LANGUAGE_CODE 			= "Language_Code";
	 
	 //Transelate Table Constants
	 String TRANSELATE_TABLE_NAME 	= "Translations";
	 String TRANSELATE_LANGUAGE_ID 	= "LANGUAGE_ID";
	 String TEXT_IDENTIFIER 		= "TEXT_IDENTIFIER";
	 String TRANSELATION 			= "TRANSLATION";
	 String APPLICATION_ID 			= "APPLICATION_ID";
	 
}
