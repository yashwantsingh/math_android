package com.mathfriendzy.database;

import static com.mathfriendzy.database.DatabaseConstant.DATABASE_NAME;
import static com.mathfriendzy.utils.ICommonUtils.DATABASE_LOG;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * This class communicate with the database and create , close connection with it
 * @author Yashwant Singh
 *
 */
public class Database 
{	
	private SQLiteDatabase database = null;
	private Context context 		= null;
	
	/**
	 * Database Constructor
	 * @param context
	 */
	public Database(Context context)
	{
		this.context = context;
	}
	
	/**
	 * This method open the database connection
	 */
	public void open()
	{
		if(DATABASE_LOG)
			Log.e("Database", " inside open()");
		
		database  = context.openOrCreateDatabase(DATABASE_NAME, 0, null);
		
		if(DATABASE_LOG)
			Log.e("Database", " outside open()");
		
	}
	
	/**
	 * This method close the database connection
	 */
	public void close()
	{
		if(DATABASE_LOG)
			Log.e("Database", " inside close()");
		
		database.close();
		
		if(DATABASE_LOG)
			Log.e("Database", " outside close()");
	}
		
	/**
	 * This method return the database object for performing operation on the database
	 * @return
	 */
	public SQLiteDatabase getConnection()
	{
		return database;
	}
}
