package com.mathfriendzy.facebookconnect;

import com.facebook.Request;
import com.facebook.Request.GraphUserCallback;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.mathfriendzy.controller.player.AddTempPlayerStep1Activity;
import com.mathfriendzy.controller.player.AddPlayer;
import com.mathfriendzy.controller.player.EditPlayer;
import com.mathfriendzy.controller.player.EditRegisteredUserPlayer;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import static com.mathfriendzy.utils.ICommonUtils.FACEBOOK_CONNECT_LOG;
import static com.mathfriendzy.utils.ICommonUtils.FACEBOOK_HOST_NAME;

/**
 * This class connect with tha facebook and get the profile data from facebook
 * @author Yashwant Singh
 *
 */
public class FacebookConnect extends Activity 
{
	private SharedPreferences sharedPreference 	= null;
	//private final String FACEBOOK_HOST_NAME 	= "http://graph.facebook.com/";
	private String TAG = this.getClass().getSimpleName();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		
		if(FACEBOOK_CONNECT_LOG)
			Log.e(TAG, "inside onCreate()");
		
		EditPlayer.IS_IMAGE_FROM_FACEBOOK 				 = true;
		AddTempPlayerStep1Activity.IS_INFO_FROM_FACEBOOK = true;
		EditRegisteredUserPlayer.IS_IMAGE_FROM_FACEBOOK  = true;
		AddPlayer.IS_IMAGE_FROM_FACEBOOK	 			= true;
		
		if (Session.getActiveSession() == null || Session.getActiveSession().isClosed()) 
        {			
            Session.openActiveSession(this, true, new StatusCallback() 
            {            	
				@Override
				public void call(Session session, SessionState state, Exception exception) 
				{
					if(session.isOpened())
					{
						createrequest(session);
					}
					
					if (exception != null) 
					{
	                    Log.e("", "somethimgbad happen");
	                }
				}
			
			});
         }
		
		if(FACEBOOK_CONNECT_LOG)
			Log.e(TAG, "outside onCreate()");
	} 
	
	/**
	 * This method create request to the facebook and get the profile information of the user
	 * @param session
	 */
	private void createrequest(Session session)
	{
		if(FACEBOOK_CONNECT_LOG)
			Log.e(TAG, "inside createrequest()");
		
		 Request myinformation = Request.newMeRequest(session,new GraphUserCallback() 
		 {		
				@Override
				public void onCompleted(GraphUser user, Response response) 
				{
				//
				try 
			    	{						
						sharedPreference = getSharedPreferences("myPreff", 0);
						SharedPreferences.Editor editor = sharedPreference.edit();
						editor.clear();
						if(user.getUsername() != null)
						{
							//Log.e("UserName", user.getUsername().toString());
							editor.putString("userName", user.getUsername().toString());
						}
						if(user.getFirstName() != null)
						{
							//Log.e("FirstName", user.getFirstName().toString());
							editor.putString("firstName",user.getFirstName().toString());
						}
						if(user.getLastName() != null)
						{
							//Log.e("LastName", user.getLastName().toString());
							editor.putString("lastName",user.getLastName().toString());
						}
						if(user.getName() != null)
						{
							//Log.e("name", user.getName().toString());
						}
						if(user.getId() != null)
						{
							AddTempPlayerStep1Activity.imageName 	= user.getId().toString();
							EditRegisteredUserPlayer.imageName 		= user.getId().toString();
							EditPlayer.imageName 					= user.getId().toString();
							AddPlayer.imageName					    = user.getId().toString();
							editor.putString("imgUrl", FACEBOOK_HOST_NAME + user.getId().toString()+"/picture?type=large");
						}
						editor.commit();
						
											
						//values stored into shared prefs, now expire the session and finish this activity
						if(Session.getActiveSession() != null)
							Session.getActiveSession().closeAndClearTokenInformation();
						FacebookConnect.this.finish();
					} 
			    	 catch (Exception e) 
					 {
			    		 Log.e("", "Error " + e.toString());
			    		 FacebookConnect.this.finish();
					 } 
				}

			});
	        myinformation.executeAsync();
	        
	        if(FACEBOOK_CONNECT_LOG)
				Log.e(TAG, "outnside createrequest()");
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{	 
		if(FACEBOOK_CONNECT_LOG)
			Log.e(TAG, "inside onActivityResult()");
	
		switch (requestCode) 
        {
             default:
                Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
                break;
        }
		
		if(FACEBOOK_CONNECT_LOG)
			Log.e(TAG, "outside onActivityResult()");
		
		super.onActivityResult(requestCode, resultCode, data);
	}	
	
	public static void logoutFromFacebook()
	{
		if(Session.getActiveSession() != null)
			Session.getActiveSession().closeAndClearTokenInformation();
	}
}
