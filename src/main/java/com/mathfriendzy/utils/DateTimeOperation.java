package com.mathfriendzy.utils;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.net.ParseException;

public class DateTimeOperation{
	
	public String setTimeFormat(long startTime){
		
		String space = "";
		String space2 = "";
		String space3 = "";

		long hour = ((startTime)/(60*60));
		long min = ((startTime)/(60)- (hour)*60);
		long sec = (startTime - (min + hour*60)*60);

		if(hour < 10){
			space = "0";
		}
		
		if(min < 10){
			space2 = "0";					
		}
		
		if(sec < 10){
			space3 = "0";
		}

		return space+hour+":"+space2+min+":"+space3+sec;
	}//END setTimeFormat method


	/**
	 * Formate the integer to String
	 * ex - 2000 to 2,000
	 * @param str
	 * @return
	 */
	public String setNumberString(String str){
		double amount = Double.parseDouble(str);
		DecimalFormat formatter = new DecimalFormat("###,###,###,###");
		return formatter.format(amount);
	}


	@SuppressLint("SimpleDateFormat")
	@SuppressWarnings("deprecation")
	public String setDate(String strdate){
		Date date = null;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			date = formatter.parse(strdate);
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}
		String month = getMonthInWord(date.getMonth());		
		//String finalDate = date.getDate()+ " "+month+" "+(date.getYear()+1900);
		String finalDate = month + " " + date.getDate() + ", " + (date.getYear()+1900);

		return finalDate;
	}

	public String getMonthInWord(int i){
		String month = "";
		switch(i)
		{
		case 0:
			month = "January";
			break;
		case 1:
			month = "February";
			break;
		case 2:
			month = "March";
			break;
		case 3:
			month = "April";
			break;
		case 4:
			month = "May";
			break;
		case 5:
			month = "June";
			break;
		case 6:
			month = "July";
			break;
		case 7:
			month = "August";
			break;
		case 8:
			month = "September";
			break;
		case 9:
			month = "October";
			break;
		case 10:
			month = "November";
			break;
		case 11:
			month = "December";
		}
		return month;
	}

	//added for friendzy challenge
	@SuppressWarnings("deprecation")
	@SuppressLint("SimpleDateFormat")
	public static long getDuration(String strdate){
		Date date = null;
		Date currentDate = new Date();

		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat fm = new SimpleDateFormat("dd MMM yyyy kk:mm:ss 'GMT'");
		try {
			date = formatter.parse(strdate);   
			currentDate = fm.parse(currentDate.toGMTString());

			//currentDate = formatter.parse(formatter.format(calender.getTime())); 
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}

		//Log.e("Date", ""+date+", currentdate "+currentDate.toGMTString());
		if(date == currentDate)
		{
			return 0;
		}

		//return Math.max(currentDate.getTime(), date.getTime()) - Math.min(currentDate.getTime(), date.getTime());
		return date.getTime() - currentDate.getTime();
	}

	public static String setTimeInMinAndSec(long startTime){    
		String space = "";
		String space2 = "";

		long min = (startTime / 60);
		long sec = (startTime - (min * 60));

		if(min < 10)
		{
			space = "0";     
		}
		if(sec < 10)
		{
			space2 = "0";
		}
		//Log.e("startTime",""+startTime);
		return space + min + ":" + space2+sec;
	}//END setTimeFormat method


	@SuppressLint("SimpleDateFormat")
	@SuppressWarnings("deprecation")
	public static String getOneDayIncreamentedDate(String strDate){
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		try{
			Date date = formatter.parse(strDate);
			date.setDate(date.getDate() + 1);
			return formatter.format(date);
		}catch (Exception e) {
			e.printStackTrace();
			return strDate;
		}
	}
}