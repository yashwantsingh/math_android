package com.mathfriendzy.utils;

/**
 * Created by root on 6/10/15.
 */
public interface MathVersions {
    public static final int MATH_SIMPLE = 1;
    public static final int MATH_PLUS = 2;
    public static final int MATH_TUTOR_PLUS = 3;
    public static final int CURRENT_MATH_VERSION = MATH_PLUS;
}
