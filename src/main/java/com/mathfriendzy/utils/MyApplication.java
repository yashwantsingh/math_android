package com.mathfriendzy.utils;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mathfriendzy.R;
import com.mathfriendzy.googleanalytics.TrackerName;
import com.mathfriendzy.oovoo.OOVOOSDK;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.oovoo.sdk.api.LoggerListener;
import com.oovoo.sdk.api.ooVooClient;
import com.oovoo.sdk.api.sdk_error;
import com.oovoo.sdk.interfaces.ooVooSdkResult;
import com.oovoo.sdk.interfaces.ooVooSdkResultListener;

import java.util.HashMap;

/**
 * This class contains the application context
 * @author Yashwant Singh
 *
 */
public class MyApplication extends MultiDexApplication implements LoggerListener{

    private static Context context;
    public static HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

    public void onCreate() {
        super.onCreate();

        MyApplication.context = getApplicationContext();

        // Create global configuration and initialize ImageLoader with this configuration
        ImageLoaderConfiguration config = new ImageLoaderConfiguration
                .Builder(getApplicationContext())
                .threadPoolSize(5)
                .build();
        ImageLoader.getInstance().init(config);

        this.initOovoo();
    }

    public static Context getAppContext() {
        return MyApplication.context;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    /**
     * Return the tracker id
     * @param trackerId
     * @return
     */
    public synchronized Tracker getTracker(TrackerName trackerId) {
        //ChromeLogger.e("MyApplication", "" + getString(R.string.google_analytics_tracker_id));
        if (!mTrackers.containsKey(trackerId)) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(
                        getString(R.string.google_analytics_tracker_id))
                    : (trackerId == TrackerName.GLOBAL_TRACKER) ?
                      analytics.newTracker(R.xml.global_tracker)
                    : analytics.newTracker(R.xml.ecommerce_tracker);
            mTrackers.put(trackerId, t);
        }
        return mTrackers.get(trackerId);
    }


    private void initOovoo(){
        try{
            ooVooClient.setContext(this);
            ooVooClient.setLogger(this, LoggerListener.LogLevel.Trace);
            ooVooClient.sharedInstance().authorizeClient(OOVOOSDK.APP_TOKEN,
                    new ooVooSdkResultListener() {
                        @Override
                        public void onResult(ooVooSdkResult result) {
                            if (result.getResult() == sdk_error.OK) {
                                Log.e("Application", "Authorized");
                            }else{
                                Log.e("Application" , "Not Authorized " + result.getDescription());
                                //Oops, you are not authorized , you can see reason of error by call result.getDescription()
                            }
                        }
                    });
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void OnLog(LogLevel logLevel, String s, String s2) {

    }
}
