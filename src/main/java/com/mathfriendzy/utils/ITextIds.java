package com.mathfriendzy.utils;

import static com.mathfriendzy.utils.ICommonUtils.HOST_NAME;

public interface ITextIds
{
	String FRIENDZY                 = "preffFriendzy";
	String LBL_RATE 				= "lblRememberToBeKind";
	String BTN_RATE 				= "btnTitleRateUs";
	String LBL_FEEDBACK 			= "lblWeWantToHearFromYouSoWeCanDoOurBest";
	String BTN_FEEDBACK 			= "btnTitleSendUsFeedback";
	String LBL_SHARE 				= "lblGoAheadSpreadTheWorld";
	String BTN_SHARE 				= "btnTitleShareThisApp";
	String LBL_MORE_BOOKS 			= "lblLetsLeapAheadBooksAreHoursOfLearningFun";
	String BTN_MORE_BOOKS 			= "btnTitleViewTheBooks";
	String LBL_COPY_RIGHT 			= "lblCopyrightMessage";
	String LBL_INFO					= "infoTitle";
	String EMAIL_BODY				= "infoEmailMessage";
	String EMAIL_SUB				= "inviteEmailSubject";
	String FEED_SUB					= "mfTitleHomeScreen";
	
	String FEEDBACK_ID				= "info@letsleapahead.com";
	String MORE_BOOKS_URL			= ICommonUtils.HTTP + "://www.letsleapahead.com/books/";
	//String RATE_URL					= "https://play.google.com/store/apps/details?id=com.mathfriendzy";
	
	String MF_TEACHER				= "mfBtnTitleTeachers";
	String MF_STUDENT				= "mfBtnTitleStudents";
	String MF_SCHOOL				= "mfBtnTitleSchools";
	String MF_TOP100				= "btnTitleTop100";
	String LBL_COUNTDOWN			= "lblCountdownToReset";
	String LBL_POS					= "mfLblPos";
	String LBL_POINTS				= "btnTitlePoints";
	String LBL_STUDENT_NAME			= "mfLblStudentsName";
	String LBL_TEACHER_NAME			= "mfLblTeacherName";
	String LBL_SCHOOL_NAME			= "mfLblSchoolsName";
	String MF_HOMESCREEN			= "mfTitleHomeScreen";
	String MF_RESULT				= "btnTitleResults";
	String LBL_LIFETIME				= "lblLifetimeTotal";
	String LBL_TIME					= "lblTime";
	String LBL_SELECTDATE			= "btnTitleSelectDate";
	String LBL_SCORE				= "mfBtnTitleScore";
	String LBL_ADDITION				= "btnTitleAddition";
	String LBL_SUBTRACT				= "btnTitleSubtraction";
	String LBL_MULTI				= "btnTitleMutiplication";
	String LBL_DIVISION				= "btnTitleDivision";
	String LBL_FRACTION				= "btnTitleFractions";
	String LBL_DECIMAL				= "btnTitleDecimals";
	String LBL_NEGATIVE				= "btnTitleNegatives";
	
	
	String JSON_URL					= HOST_NAME + "LeapAheadMultiFreindzy/index.php?action=top100";
	
	//updated text ids for multifriendzy
	
	String LBL_FIND_INVITE   = "lblChooseYourOpponent";
	 String LBL_FIND_FRIEND   = "lblFindPlayers";
	 String LBL_INVITE    = "lblInvite";
	 String LBL_FRIEND    = "lblFriends";
	 String LBL_MESSAGE    = "lblTextMessage";
	 String LBL_EMAIL    = "lblInviteFriendsViaEmail";
	 String LBL_TOP100    = "btnTitleTop100";
	 String LBL_FB     = "lblFacebookFriends";
	 String LBL_USER     = "lblPlayerSearch";
	 
	 String ALERT_LOGIN_REGISTER  = "alertMsgYouMustLoginOrRegisterToViewAndParticipate";
	 String ALERT_NO_NUMBER   = "alertMsgNoPhoneNumberAssociated";
	 /* Message to invite Friend*/
	 String MSG_INVITE_1    = "txtDownloadMathFriendzyOnItunes";
	 //String MSG_INVITE_2    = "lblToVisitiTunesAndDownload";
	 String MSG_INVITE_2_FOR_EMIAL  = "lblToVisitiTunesAndDownloadAndroid";
	 String MSG_INVITE_2    = "lblToiTunesAndDownloadAndroid";
	 String MSG_INVITE_3    = "msgBodyDownloadMathFriendzyOnItunes";
	 String MSG_INVITE_4    = "txtAndInviteMeToStartFriendzy"; 
	 String MSG_INVITE_5    = "txtLikeUsOnOfficialPage";
	 
	 String MSG_CLICK    = "lblClickHere";
	 String MSG_LIKE     = "lblLike";
	 String LIKE_URL     = "https://www.facebook.com/pages/Friendzy-Apps/619174838097567";
	 
	 //String EMAIL_SUBJECT   = "lblCheckOut";
	 String EMAIL_SUBJECT   = "inviteEmailSubject";
	 String LBL_SEARCH_USER   = "lblEnterYourOpponents";
	 String LBL_USERNAME    = "lblUserName";
	 String BTN_GO     = "btnTitleGo";
	 String LBL_FOUND    = "lblFound";
	 String LBL_ROUND    = "lblRound";
	 String LBL_PLAYERS    = "btnTitlePlayers";

	 String LBL_MEMBER    = "lblMembers";
	 
	 //for new Learnign center
	 String FLASH_CARD 	= "btnTitleFlashCards";
	 String BEGINNER 	= "lblMfBeginner";
	 String NOVICE 		= "lblMfNovice";
	 String ROOKIE 		= "lblMfRookie";
	 String INTERMEDIATE= "lblMfIntermediate";
	 String PROFESSIONAL= "lblMfProfessional";
	 String WHIZ 		= "lblMfWhiz";
	 String EXPERT 		= "lblMfExpert";
	 String MASTER 		= "lblMfMaster";
	 String PRODIGY 	= "lblMfProdigy";
	 String BRAINIAC 	= "lblMfBrainiac";

	 String URL_ICON_IMG = ICommonUtils.HTTP + "://api.letsleapahead.com/images/icon_mathfriendzy.png";
}
