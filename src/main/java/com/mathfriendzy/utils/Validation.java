package com.mathfriendzy.utils;

import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class Validation 
{


	/**
	 * This method check the user is valid or not
	 * @param userName
	 * @return
	 */
	public String validUser(String userName)
	{
		String strUrl = COMPLETE_URL + "action=checkUserNameExistence&userName=" + userName;
		return this.parseJsonForValiUser(CommonUtils.readFromURL(strUrl));
	}

	private String parseJsonForValiUser(String jsonString)
	{
		if(jsonString != null){
			String userResult = "";
			try 
			{
				JSONObject jObject = new JSONObject(jsonString);
				userResult = jObject.getString("data");
			}
			catch (JSONException e) 
			{
				e.printStackTrace();
			}
			return userResult;
		}else{
			return null;
		}
	}

	/**
	 * This method check for city is valid or not
	 * @param countryId
	 * @param stateId
	 * @param city
	 * @param zip
	 * @return
	 */
	public String validateCity(String countryId,String stateId,String city,String zip)
	{
		String action = "validateZipCityStateCountry";
		String encodedCity = null;
		try 
		{
			encodedCity = URLEncoder.encode(city, "UTF-8");
		} 
		catch (UnsupportedEncodingException e) 
		{

		}


		String strUrl = COMPLETE_URL  + "action=" 	+ 	action 			+ "&" 
				+ "countryId=" 	+ 	countryId	+ "&"
				+ "stateId="	+	stateId		+ "&"
				+ "city="		+	encodedCity	+ "&"
				+ "zip="		+	zip;

		return this.parseJsonForValidCity(CommonUtils.readFromURL(strUrl));
	}

	/**
	 * Parse json String for valid city
	 * @param jsonString
	 * @return
	 */
	private String parseJsonForValidCity(String jsonString)
	{
		if(jsonString != null){
			String userResult = "";
			try 
			{
				JSONObject jObject = new JSONObject(jsonString);
				userResult = jObject.getString("code");
			}
			catch (JSONException e) 
			{
				e.printStackTrace();
			}
			return userResult;
		}else{
			return null;
		}
	}
}
