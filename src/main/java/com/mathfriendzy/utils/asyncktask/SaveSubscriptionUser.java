package com.mathfriendzy.utils.asyncktask;

import java.util.ArrayList;

import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.PurchaseItemObj;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ICommonUtils;

//class created by shilpi
public class SaveSubscriptionUser extends AsyncTask<Void, Void, Void>
{
	Context context;
	int playerCoins;
	String userId;
	String playerId;
	int duration;
	String date;
	ProgressDialog pd;
	String rs;

	public SaveSubscriptionUser(Context context, int playerCoins, int duration)
	{
		pd = CommonUtils.getProgressDialog(context);
		pd.show();
		this.context		= context;
		this.playerCoins	= playerCoins;
		this.duration		= duration;

		SharedPreferences sharedPreffPlayerInfo = context.getSharedPreferences(ICommonUtils.IS_CHECKED_PREFF, 0);

		userId   = sharedPreffPlayerInfo.getString("userId", "");
		playerId = sharedPreffPlayerInfo.getString("playerId", "");
	}


	@Override
	protected Void doInBackground(Void... params) {

		String strUrl = ICommonUtils.COMPLETE_URL+"action=saveSubscriptionStatus" +
				"&userId="+userId+"&playerId="+playerId+
				"&duration="+duration+"&coins="+playerCoins+
				"&appId="+CommonUtils.APP_ID;

		//Log.e("", "url : "+strUrl);
		date = parseSubscriptionData(CommonUtils.readFromURL(strUrl));
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		pd.cancel();
		if(date != null && rs.equals("success"))
		{
			updateDatabase();			
			//Log.e("", "data : "+date);
			DialogGenerator dg = new DialogGenerator(context);
			Translation transeletion = new Translation(context);
			transeletion.openConnection();
			//lblAllOfTheAppsFeaturesAreUnlocked
			dg.generateSubsriptionDialog(transeletion
					.getTranselationTextByTextIdentifier("lblThankYouForPurchasingSchoolSubscription")+
					" " +duration+" "+transeletion.getTranselationTextByTextIdentifier("lblMonth")+
					"\n\n"+transeletion.getTranselationTextByTextIdentifier("lblAllOfTheAppsFeaturesAreUnlocked")
					+" "+date.substring(0,10));
			transeletion.closeConnection();
		}else{
			CommonUtils.showInternetDialog(context);
		}
		super.onPostExecute(result);
	}

	private String parseSubscriptionData(String jsonString)
	{
		//Log.e("", "jsonString : "+jsonString);
		try 
		{
			JSONObject jObject 		= new JSONObject(jsonString);
			rs					= jObject.getString("result");
			String date   			= jObject.getString("data");
			return date;
		}
		catch(Exception e)
		{
			return null;
		}
	}

	private void updateDatabase()
	{
		LearningCenterimpl learnObj = new LearningCenterimpl(context);
		learnObj.openConn();
		learnObj.updateCoinsForPlayer(playerCoins, userId, playerId);

		ArrayList<PurchaseItemObj> purchaseItem	= new ArrayList<PurchaseItemObj>();
		PurchaseItemObj obj						= new PurchaseItemObj();
		obj.setItemId(100);
		obj.setStatus(1);
		obj.setUserId(userId);
		purchaseItem.add(obj);

		learnObj.insertIntoPurchaseItem(purchaseItem);

		learnObj.closeConn();		
	}
}