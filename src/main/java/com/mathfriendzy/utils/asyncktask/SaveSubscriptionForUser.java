package com.mathfriendzy.utils.asyncktask;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.PurchaseItemObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.LearnignCenterSchoolCurriculumServerOperation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;

public class SaveSubscriptionForUser  extends AsyncTask<Void, Void, String>{

	private String userId;
	private String playerId;
	private int duration;
	private ProgressDialog pd;
	private int coins;
	private Context context;
	
	public SaveSubscriptionForUser(String userId , String playerId , int duration , 
			int coins ,Context context){
		
		this.userId = userId;
		this.playerId = playerId;
		this.duration = duration;
		this.coins = coins;
		this.context = context;
	}
	
	@Override
	protected void onPreExecute() {
		pd = CommonUtils.getProgressDialog(context);
		pd.show();
		super.onPreExecute();
	}
	
	@Override
	protected String doInBackground(Void... params) {
		LearnignCenterSchoolCurriculumServerOperation serverObj = 
				new LearnignCenterSchoolCurriculumServerOperation();
		String result = serverObj.saveSubscriptionForUser(userId, playerId, coins, duration);
		return result;
	}

	@Override
	protected void onPostExecute(String result) {
		pd.cancel();
		if(result != null)
		{
			this.updateCoinsInLocalDatabase(context, userId, playerId, coins);
			
			DialogGenerator dg = new DialogGenerator(context);
			Translation transeletion = new Translation(context);
			transeletion.openConnection();
			//dg.generateWarningDialog(transeletion
		     //.getTranselationTextByTextIdentifier("lblThankYouForPurchasingSchoolSubscription")+" "+ result.substring(0 , 10));
			 dg.generateWarningDialog(transeletion
				     .getTranselationTextByTextIdentifier("lblThankYouForPurchasingSchoolSubscription")+
				     " " + duration + " " + transeletion.getTranselationTextByTextIdentifier("lblMonth")+
				     "\n\n"+transeletion.getTranselationTextByTextIdentifier("lblAllOfTheAppsFeaturesAreUnlocked")
				     +" "+result.substring(0,10));
			transeletion.closeConnection();
		 }else{
			 CommonUtils.showInternetDialog(context);
		 }
		super.onPostExecute(result);
	}
	
	/**
	 * This method update the coins into local database
	 * @param context
	 * @param userId
	 * @param playerId
	 */
	private void updateCoinsInLocalDatabase(Context context , String userId , String playerId
			, int coins){
		
		LearningCenterimpl learnImpl = new LearningCenterimpl(context);
		learnImpl.openConn();
		learnImpl.updateCoinsForPlayer(coins, userId, playerId);
		learnImpl.closeConn();
		
		ArrayList<PurchaseItemObj> purchaseItem = new ArrayList<PurchaseItemObj>();
		PurchaseItemObj purchseObj = new PurchaseItemObj();
		purchseObj.setUserId(userId);
		purchseObj.setItemId(100);
		purchseObj.setStatus(1);
		purchaseItem.add(purchseObj);
		
		LearningCenterimpl learningCenter = new LearningCenterimpl(context);
		learningCenter.openConn();
		learningCenter.deleteFromPurchaseItem(userId , 100);
		learningCenter.insertIntoPurchaseItem(purchaseItem);		
		learningCenter.closeConn();
	}
}
