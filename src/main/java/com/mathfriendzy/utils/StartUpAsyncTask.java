package com.mathfriendzy.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.Language;
import com.mathfriendzy.model.language.translation.Translation;

/**
 * This AsyncTask execute at the starting of the project in which the translation , languages from the server loaded 
 * into the database
 * @author Yashwant Singh
 *
 */
public class StartUpAsyncTask extends AsyncTask<Void, Void, Void> 
{
	private static boolean isGetLanguageFromServer 	= false;
	private static boolean isTranselation 			= false; 
	
	//private final String TAG = this.getClass().getSimpleName();
	private Context context = null;
	private ProgressDialog pd;
	
	public StartUpAsyncTask(Context context)
	{
		MainActivity.isAvtarLoaded = false;
		this.context = context;
	}
	
	@Override
	protected void onPreExecute()
	{
		pd = CommonUtils.getProgressDialog(context ,
                "Please be patient. This could take several minutes." , false);
		pd.show();
		super.onPreExecute();
	}
		
	@Override
	protected Void doInBackground(Void... params) 
	{
		Log.e("StartUpAsyncTask", "inside doInBackGround()");
			
		SharedPreferences sheredPreference = context.getSharedPreferences("SHARED_FLAG", 0);	
		isGetLanguageFromServer = sheredPreference.getBoolean("isGetLanguageFromServer", false);
		isTranselation			= sheredPreference.getBoolean("isTranselation", false);

		SharedPreferences.Editor editor = sheredPreference.edit();

		/*
		 * use to return because no need to go inside
		 */
		if(isGetLanguageFromServer && isTranselation)
		{
			return null;
		}

		if(CommonUtils.isInternetConnectionAvailable(context))
		{
			if(!isGetLanguageFromServer)
			{
				Language languageFromServer = new Language(context);
				languageFromServer.getLanguagesFromServer();
				languageFromServer.saveLanguages();
				editor.putBoolean("isGetLanguageFromServer", true);
			}

			if(!isTranselation){
				Translation traneselation = new Translation(context);
				traneselation.getTransalateTextFromServer(CommonUtils.LANGUAGE_CODE,
                        CommonUtils.LANGUAGE_ID, CommonUtils.APP_ID);
				traneselation.saveTranslationText();
				editor.putBoolean("isTranselation", true);
                MathFriendzyHelper.saveDownloadedLanguage(context , CommonUtils.LANGUAGE_ID);
			}
			editor.commit();
		}
		else{
			DialogGenerator dg = new DialogGenerator(context);
			dg.generateWarningDialog("You are not connected to the internet.\nPlease try again later.");
			((Activity) context).finish();
		}	
		
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) 
	{	
		new Handler().postDelayed(new Runnable() 
		{
			@Override
			public void run() 
			{
				pd.cancel();
				
				//changes for Spanish
				CommonUtils.CheckWordProblemSpanishTable(context);
				//end changes
				
				Intent intent = new Intent(context,MainActivity.class);
				context.startActivity(intent);
				((Activity)context).finish();
			}
		}, 1000);
				
		super.onPostExecute(result);
	}
}
