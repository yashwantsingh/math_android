package com.mathfriendzy.utils;

import java.io.InputStream;
import java.util.ArrayList;

import com.mathfriendzy.controller.multifriendzy.contacts.ContactBeans;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;

public class ContactOperation 
{
	ContentResolver cr;

	public ContactOperation(ContentResolver cr)
	{
		this.cr = cr;
	}


	/**
	 * use to fetch contacts from phone memory
	 * @return list of contacts
	 */
	public ArrayList<ContactBeans> addContactToList()
	{
		ArrayList<ContactBeans> record = new ArrayList<ContactBeans>();

		Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

		if (cur.getCount() > 0) 	
		{			
			while (cur.moveToNext()) 
			{
				ContactBeans obj				= new ContactBeans();
				String id 		= cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
				String name 	= cur.getString(cur
						.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));		        

				/*-----------------------------------*
				 * Checking Phone Numbers in contacts*
				 *-----------------------------------*/	
				if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) 
				{
					String phone = getPhoneNumber(id);	                	
					obj.setPhone(phone);
				}	   		        


				Bitmap bitmap = loadContactPhoto(cr, id);
				obj.setPhoto(bitmap);
				obj.setId(id);
				obj.setName(name);   		        

				record.add(obj);
			}//End outer While Loop


		}//End if

		cur.close();

		return record;		
	}//END addContactandCheckBday method



	private Bitmap loadContactPhoto(ContentResolver cr, String id)
	{		
		Uri uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, 
				Long.parseLong(id));
		InputStream input	= ContactsContract.Contacts
				.openContactPhotoInputStream(cr, uri);			
		if(input == null)
			return null;

		Bitmap bitmap  = BitmapFactory.decodeStream(input);

		return bitmap;
	}//END loadContactPhoto method



	private String getPhoneNumber(String id)
	{
		Cursor pCur = cr.query(Phone.CONTENT_URI,null,Phone.CONTACT_ID +" = ?", new String[]{id}, null);
		if(pCur != null)
		{
			while(pCur.moveToNext())
			{
				String phone = pCur.getString(pCur.getColumnIndex(Phone.NUMBER));	                	
				return phone;
			}
		}
		return null;
	}
}
