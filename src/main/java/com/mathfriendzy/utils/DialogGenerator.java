package com.mathfriendzy.utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.assessmenttest.SelectGradeAndStandardActivity;
import com.mathfriendzy.controller.coinsdistribution.CoinsDistributionActivity;
import com.mathfriendzy.controller.inapp.GetMoreCoins;
import com.mathfriendzy.controller.learningcenter.LearningCenterEquationSolveWithTimer;
import com.mathfriendzy.controller.learningcenter.OnReverseDialogClose;
import com.mathfriendzy.controller.learningcenter.schoolcurriculum.SchoolCurriculumEquationSolveBase;
import com.mathfriendzy.controller.learningcenter.schoolcurriculum.ShowTranslateQuestionAfterDownloading;
import com.mathfriendzy.controller.login.LoginActivity;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.multifriendzy.MultiFriendzyMain;
import com.mathfriendzy.controller.player.CreateTempPlayerActivity;
import com.mathfriendzy.controller.player.LoginUserCreatePlayer;
import com.mathfriendzy.controller.player.LoginUserPlayerActivity;
import com.mathfriendzy.controller.player.TeacherPlayer;
import com.mathfriendzy.controller.registration.ModifyRegistration;
import com.mathfriendzy.controller.registration.RegistrationStep1;
import com.mathfriendzy.controller.reportaproblem.ReportProblemActivity;
import com.mathfriendzy.controller.singlefriendzy.ChooseChallengerAdapter;
import com.mathfriendzy.controller.singlefriendzy.SingleFriendzyEquationSolve;
import com.mathfriendzy.controller.singlefriendzy.SingleFriendzyMain;
import com.mathfriendzy.controller.singlefriendzy.schoolcurriculum.SingleFriendzySchoolCurriculumEquationSolve;
import com.mathfriendzy.dawnloadimagesfromserver.DawnloadImagesFromserver;
import com.mathfriendzy.dialogs.FreeSubsriptionListener;
import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.helper.YesNoListenerInterface;
import com.mathfriendzy.listener.LoginRegisterPopUpListener;
import com.mathfriendzy.listener.RateUsCallback;
import com.mathfriendzy.listener.TeacherFunctionDialogListener;
import com.mathfriendzy.model.coinsdistribution.CoinsDistributionServerOperation;
import com.mathfriendzy.model.friendzy.FriendzyDTO;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.CoinsFromServerObj;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.PurchaseItemObj;
import com.mathfriendzy.model.login.Login;
import com.mathfriendzy.model.multifriendzy.MultiFriendzyServerOperation;
import com.mathfriendzy.model.player.temp.TempPlayerOperation;
import com.mathfriendzy.model.registration.Register;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.LearnignCenterSchoolCurriculumServerOperation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.QuestionLoadedTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SchoolCurriculumLearnignCenterimpl;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.WordProblemQuestionTransferObj;
import com.mathfriendzy.model.schools.TeacherDTO;
import com.mathfriendzy.model.singleFriendzy.ChallengerTransferObj;
import com.mathfriendzy.model.spanishchanges.SpanishChangesImpl;
import com.mathfriendzy.utils.asyncktask.SaveSubscriptionForUser;
import com.mathfriendzy.utils.asyncktask.SaveSubscriptionUser;

import java.util.ArrayList;
import java.util.Date;

import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;

public class DialogGenerator implements OnClickListener
{
    Context context;
    Dialog dialog;
    Translation transeletion = null;
    String playerId = null;
    String userId   = null;
    String callingActivity = null;

    //for rate us dialg
    private int numberOfStarSelected = 0;

    public DialogGenerator(Context context)
    {
        this.context = context;
        transeletion = new Translation(context);
    }

    /**
     * Generate Register or login Dialog
     * @param msg
     */
    public void generateRegisterOrLogInDialog(String msg)
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_register_login);
        dialog.show();

        Button btnNoThanks = (Button)dialog.findViewById(R.id.btnAlertBtnTitleNoThanks);
        Button btnRegister = (Button)dialog.findViewById(R.id.btnAlertBtnTitleRegister);
        Button btnLogin    = (Button)dialog.findViewById(R.id.btnAlertBtnTitleLogIn);
        TextView txtRegisterLogIn = (TextView)dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
        Button btnSignInWithGoogle = (Button) dialog.findViewById(R.id.btnSignInWithGoogle);

        transeletion.openConnection();
        btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
        btnRegister.setText(transeletion.getTranselationTextByTextIdentifier("alertBtnTitleRegister"));
        btnLogin.setText(transeletion.getTranselationTextByTextIdentifier("alertBtnLogin"));
        txtRegisterLogIn.setText(msg);
        transeletion.closeConnection();
        btnNoThanks.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
        btnLogin.setOnClickListener(this);

        btnSignInWithGoogle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                MathFriendzyHelper.signInWithGoogle(context);
            }
        });
    }

    /**
     * Generate Warning Dialog
     * @param msg
     */
    public void generateWarningDialog(String msg){
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_warning);
        dialog.show();

        TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
        Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
        transeletion.openConnection();
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        transeletion.closeConnection();
        txtAlertMsgRegisterLogIn.setText(msg);
        btnOk.setOnClickListener(this);
    }

    /**
     * Generate Warning Dialog
     * @param msg
     */
    public void generateCommonWarningDialog(String msg){
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.common_warning_dialog);
        dialog.show();

        TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
        Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
        transeletion.openConnection();
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        transeletion.closeConnection();
        txtAlertMsgRegisterLogIn.setText(msg);
        btnOk.setOnClickListener(this);
    }

    /**
     * Generate Warning Dialog
     * @param msg
     */
    public void generateCommonWarningDialog(String msg , final HttpServerRequest request){
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.common_warning_dialog);
        dialog.show();

        TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
        Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
        transeletion.openConnection();
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        transeletion.closeConnection();
        txtAlertMsgRegisterLogIn.setText(msg);

        btnOk.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                request.onRequestComplete();
                dialog.dismiss();
            }
        });

        dialog.setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                request.onRequestComplete();
            }
        });
    }

    /**
     * Generate Warning Dialog
     * @param msg
     */
    public void generateCommonWarningDialogForLongMsg(String msg){
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_warning_for_long_message);
        dialog.show();

        TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
        Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
        transeletion.openConnection();
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        transeletion.closeConnection();
        txtAlertMsgRegisterLogIn.setText(msg);
        btnOk.setOnClickListener(this);
    }

    /**
     * Generate Warning Dialog
     * @param msg
     */
    public void generateCommonWarningDialogForLongMsgWithSameTextSizeForTabPhone(String msg
            , final HttpServerRequest request){
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_warning_for_long_message_with_same_text_size_for_tab_phone);
        dialog.show();

        TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
        Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
        TextView txtTitle = (TextView) dialog.findViewById(R.id.txtTitle);
        transeletion.openConnection();
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        txtTitle.setText(transeletion.getTranselationTextByTextIdentifier("lblThisIsSchoolFeature"));
        transeletion.closeConnection();
        txtAlertMsgRegisterLogIn.setText(msg);
        btnOk.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                request.onRequestComplete();
                dialog.dismiss();
            }
        });

        dialog.setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                request.onRequestComplete();
            }
        });
    }

    /**
     * Generate Warning Dialog
     * @param msg
     */
    public void generateWarningDialogForthumbImages(String msg , final HttpServerRequest request)
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dailog_for_thumb_image);
        dialog.show();

        TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
        Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
        transeletion.openConnection();
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        transeletion.closeConnection();
        txtAlertMsgRegisterLogIn.setText(msg);
        btnOk.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                request.onRequestComplete();
                dialog.dismiss();
            }
        });

        dialog.setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                request.onRequestComplete();
            }
        });
    }

    /**
     * Generate Warning Dialog
     * @param msg
     */
    public void generateWarningDialogForthumbImages(String msg)
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dailog_for_thumb_image);
        dialog.show();

        TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
        Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
        transeletion.openConnection();
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        transeletion.closeConnection();
        txtAlertMsgRegisterLogIn.setText(msg);
        btnOk.setOnClickListener(this);
    }

    /**
     * This dailog open when user click to play with school curriculum for
     * learning center or for the assessment test and also for friendzy chaalange
     */
    public void generateDailogForSchoolCurriculumUnlock(final CoinsFromServerObj coindFromServer,
                                                        final String userId ,final String playerId , int appUnlockStatus){

        //method info changes for new subscription info at the time of TextToSpeach , On 21 Apr 2014
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_for_lock_level_for_learnign_center_school_curriculum);
        dialog.show();

        TextView txtScoolCurriculumTop = (TextView) dialog.findViewById(R.id.txtScoolCurriculumTop);
        TextView txtIncludeTheFollowing = (TextView) dialog.findViewById(R.id.txtIncludeTheFollowing);
        TextView txtA = (TextView) dialog.findViewById(R.id.txtA);
        TextView txtB = (TextView) dialog.findViewById(R.id.txtB);
        TextView txtC = (TextView) dialog.findViewById(R.id.txtC);
        TextView txtAssignCoinsYouHave = (TextView) dialog.findViewById(R.id.txtYourCurrentlyCoins);
        TextView txtUnAssignCoinsYouHave = (TextView) dialog.findViewById(R.id.txtUserCoins);
        TextView txtSpecialPrice = (TextView) dialog.findViewById(R.id.txtSpecialPrice);

        if(appUnlockStatus == -1)
            txtIncludeTheFollowing.setVisibility(TextView.GONE);

        //set translation text
        transeletion.openConnection();
        txtScoolCurriculumTop.setText(transeletion.getTranselationTextByTextIdentifier("lblSchoolsCurriculum"));
        txtIncludeTheFollowing.setText(transeletion.getTranselationTextByTextIdentifier("lblYourSubscriptionExpired"));
        txtA.setText(transeletion.getTranselationTextByTextIdentifier("lblYouCanPurchaseSubscriptionToUnlock"));
        txtB.setText("b. " + transeletion.getTranselationTextByTextIdentifier("lblAssessmentTest") + ".");
        txtC.setText("c. " + transeletion.getTranselationTextByTextIdentifier("lblSchoolChallenge") + ".");
        txtSpecialPrice.setText(transeletion.getTranselationTextByTextIdentifier("lblWeHaveSpecialPricesForSchools")
                + ".");

        Button btnAssignCons 	= (Button) dialog.findViewById(R.id.btnAssignCons);
        Button btnPurchaseCoins = (Button) dialog.findViewById(R.id.btnPurchaseCoins);

        TextView txtMonth = (TextView) dialog.findViewById(R.id.txtMonth);
        TextView txtYear = (TextView) dialog.findViewById(R.id.txtYear);

        Button btnGoForMonth = (Button) dialog.findViewById(R.id.btnGoForMonth);
        Button btnGoForYear = (Button) dialog.findViewById(R.id.btnGoForYear);

        Button btncancel = (Button) dialog.findViewById(R.id.btncancel);

        btnAssignCons.setText(transeletion.getTranselationTextByTextIdentifier("lblAssignCoins"));
        btnPurchaseCoins.setText(transeletion.getTranselationTextByTextIdentifier("lblPurchaseCoins"));
        btnGoForMonth.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGo") + "!");
        btnGoForYear.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGo") + "!");

        txtAssignCoinsYouHave.setText(transeletion.getTranselationTextByTextIdentifier("lblYouHave") + " " +
                CommonUtils.setNumberString(coindFromServer.getCoinsEarned() + "") + " " + transeletion.getTranselationTextByTextIdentifier("lblCoins")+
                "  assigned to your account.");

        txtUnAssignCoinsYouHave.setText(transeletion.getTranselationTextByTextIdentifier("lblYouAlsoHave") + " " +
                CommonUtils.setNumberString(coindFromServer.getCoinsPurchase() + " ")
                + " unassigned coins.");

        txtMonth.setText("1 " + transeletion.getTranselationTextByTextIdentifier("lblMonth") + " = "
                + CommonUtils.setNumberString(coindFromServer.getMonthlyCoins() + "")
                + " " + transeletion.getTranselationTextByTextIdentifier("lblCoins"));
        txtYear.setText("1 " + transeletion.getTranselationTextByTextIdentifier("lblYear") + " = "
                + CommonUtils.setNumberString(coindFromServer.getYearlyCoins() + "")
                + " " + transeletion.getTranselationTextByTextIdentifier("lblCoins"));

        transeletion.closeConnection();
        //end translation text

        //set visibility for view
        if(coindFromServer.getCoinsPurchase() > 0){
            txtUnAssignCoinsYouHave.setVisibility(TextView.VISIBLE);
            btnAssignCons.setVisibility(Button.VISIBLE);
        }

		/*//set text value
		txtAssignCoinsYouHave.setText("You have " +  CommonUtils.setNumberString
				(coindFromServer.getCoinsEarned() + "") 
				+ " coins assigned to your account.");
		txtUnAssignCoinsYouHave.setText("You also have " + CommonUtils.setNumberString
				(coindFromServer.getCoinsPurchase() + "") 
				+ " unassigned coins.");*/

		/*txtMonth.setText("1 Month = " + CommonUtils.setNumberString(coindFromServer.getMonthlyCoins() + "") 
				+ " Coins");
		txtYear.setText("1 Year = " + CommonUtils.setNumberString(coindFromServer.getYearlyCoins() + "") 
				+ " Coins");
		 */
        btncancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnAssignCons.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getUserDataFromServer();
                //context.startActivity(new Intent(context, CoinsDistributionActivity.class));
            }
        });

        btnPurchaseCoins.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                context.startActivity(new Intent(context , GetMoreCoins.class));
            }
        });

        btnGoForMonth.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                clickOnGoForSubscription(coindFromServer , context , userId , playerId , 1);
            }
        });

        btnGoForYear.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                clickOnGoForSubscription(coindFromServer , context , userId , playerId , 12);
            }
        });
    }

    /**
     * This dailog open when user click to play with school curriculum for
     * learning center or for the assessment test and also for friendzy chaalange
     */
    public void generateDailogAfterRegistration(String msg){
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_for_confirmation_email_after_registration);
        dialog.show();

        TextView txtLastStepOfRegistration = (TextView) dialog.findViewById(R.id.txtLastStepOfRegistration);
        Button btncancel = (Button) dialog.findViewById(R.id.btncancel);

        txtLastStepOfRegistration.setText(msg);

        btncancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }


    /**
     * This method call when user click on subscription button
     * @param coindFromServer
     * @param context
     * @param userId
     * @param playerId
     * @param month
     */
    private void clickOnGoForSubscription(CoinsFromServerObj coindFromServer , Context context ,
                                          String userId , String playerId , int month){

        int coins = 0;
        if(month == 1){
            if(coindFromServer.getCoinsEarned() >= coindFromServer.getMonthlyCoins()){
                if(CommonUtils.isInternetConnectionAvailable(context)){

                    coins = coindFromServer.getCoinsEarned() - coindFromServer.getMonthlyCoins();
                    //this.updateCoinsInLocalDatabase(context, userId, playerId, coins);

                    new SaveSubscriptionForUser(userId , playerId , month
                            ,coins, context).execute(null,null,null);
                }
                else{
                    DialogGenerator dg = new DialogGenerator(context);
                    Translation transeletion = new Translation(context);
                    transeletion.openConnection();
                    dg.generateWarningDialog(transeletion.
                            getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
                    transeletion.closeConnection();
                }
            }else{
                this.generateDialogForNeedCoinsForUlock(coindFromServer.getCoinsEarned(),
                        coindFromServer.getMonthlyCoins() , 1);
            }
        }else{

            if(coindFromServer.getCoinsEarned() >= coindFromServer.getYearlyCoins()){
                if(CommonUtils.isInternetConnectionAvailable(context)){

                    coins = coindFromServer.getCoinsEarned() - coindFromServer.getYearlyCoins();
                    //this.updateCoinsInLocalDatabase(context, userId, playerId, coins);

                    new SaveSubscriptionForUser(userId , playerId , month
                            ,coins, context).execute(null,null,null);
                }else{
                    DialogGenerator dg = new DialogGenerator(context);
                    Translation transeletion = new Translation(context);
                    transeletion.openConnection();
                    dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
                    transeletion.closeConnection();
                }
            }else{
                this.generateDialogForNeedCoinsForUlock(coindFromServer.getCoinsEarned(),
                        coindFromServer.getYearlyCoins() , 1);
            }
        }
    }

    /**
     * This method update the coins into local database
     * @param context
     * @param userId
     * @param playerId
     *//*
	private void updateCoinsInLocalDatabase(Context context , String userId , String playerId
			, int coins){

		LearningCenterimpl learnImpl = new LearningCenterimpl(context);
		learnImpl.openConn();
		learnImpl.updateCoinsForPlayer(coins, userId, playerId);
		learnImpl.closeConn();

		ArrayList<PurchaseItemObj> purchaseItem = new ArrayList<PurchaseItemObj>();
		PurchaseItemObj purchseObj = new PurchaseItemObj();
		purchseObj.setUserId(userId);
		purchseObj.setItemId(100);
		purchseObj.setStatus(1);
		purchaseItem.add(purchseObj);

		LearningCenterimpl learningCenter = new LearningCenterimpl(context);
		learningCenter.openConn();
		learningCenter.deleteFromPurchaseItem(userId , 100);
		learningCenter.insertIntoPurchaseItem(purchaseItem);		
		learningCenter.closeConn();
	}*/

    /**
     * Generate Warning Dialog
     * @param msg
     */
    public void generateWarningDialogForNoSufficientQuestionForSingleFriendzy
    (String msg , final Context context , final boolean isSingleFriendzy)
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_warning);
        dialog.show();

        TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
        Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
        transeletion.openConnection();
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        transeletion.closeConnection();
        txtAlertMsgRegisterLogIn.setText(msg);
        btnOk.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if(isSingleFriendzy)
                    context.startActivity(new Intent(context , SingleFriendzyMain.class));
                else
                    context.startActivity(new Intent(context , MultiFriendzyMain.class));
            }
        });
    }

    /**
     * Generate Warning Dialog
     * @param msg
     */
    public void generateWarningDialogForNoSufficientQuestionForSingleFriendzy
    (String msg , final Context context , final int callingActivity)
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_warning);
        dialog.show();

        TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
        Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
        transeletion.openConnection();
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        transeletion.closeConnection();
        txtAlertMsgRegisterLogIn.setText(msg);
        btnOk.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if(callingActivity == 1)
                    context.startActivity(new Intent(context , SelectGradeAndStandardActivity.class));
                else if(callingActivity == 2)
                    context.startActivity(new Intent(context , LearningCenterEquationSolveWithTimer.class));
            }
        });
    }

    /**
     * Generate Warning Dialog
     * @param msg
     */
    public void generateDialogForSchoolCurriculumTimer(String msg , final CountDownTimer timer)
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_warning_for_school_curriculum_timer);
        dialog.show();

        TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
        Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
        transeletion.openConnection();
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        transeletion.closeConnection();
        txtAlertMsgRegisterLogIn.setText(msg);
        btnOk.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if(timer != null)
                    timer.start();
            }
        });

        dialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dialog.dismiss();
                    if(timer != null)
                        timer.start();
                }
                return true;
            }
        });
    }

    /**
     * Generate Warning Dialog
     * @param msg
     */
    public void generateWarningDialogForEmptyCoisTransfer(String msg)
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_empty_coinstransfer_warning);
        dialog.show();

        TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
        Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
        transeletion.openConnection();
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        transeletion.closeConnection();
        txtAlertMsgRegisterLogIn.setText(msg);

        btnOk.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
            }
        });
    }

    /**
     * Generate Warning Dialog
     * @param msg
     */
    public void generateWarningDialogForSendEmailSuccessfully(String msg , String buttontext)
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_empty_coinstransfer_warning);
        dialog.show();

        TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
        Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
        btnOk.setText(buttontext);
        txtAlertMsgRegisterLogIn.setText(msg);

        btnOk.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
            }
        });
    }
    /**
     * Generate Warning Dialog
     * @param msg
     */
    public void generateWarningDialogForSingleFriendzy(String msg)
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_internet_warning_for_single_friendzy);
        dialog.show();

        TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
        TextView mfAlertMsgWouldYouLikeToPlayWithComputer = (TextView) dialog.findViewById(R.id.mfAlertMsgWouldYouLikeToPlayWithComputer);
        Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOkForSingleFriendzy);
        Button btnTahnks = (Button) dialog.findViewById(R.id.btnNoThanks);

        transeletion.openConnection();
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        mfAlertMsgWouldYouLikeToPlayWithComputer.setText(transeletion.getTranselationTextByTextIdentifier("mfAlertMsgWouldYouLikeToPlayWithComputer"));
        btnTahnks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
        transeletion.closeConnection();
        txtAlertMsgRegisterLogIn.setText(msg);

        btnOk.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                ChallengerTransferObj challengerObj = new ChallengerTransferObj();

                challengerObj.setFirst("Computer");
                challengerObj.setLast("Player");
                challengerObj.setIsFakePlayer(1);
                challengerObj.setPlayerId("");
                challengerObj.setUserId("");
                challengerObj.setSum("");
                challengerObj.setCountryIso("");

                ChooseChallengerAdapter.challengerDataObj = challengerObj;
                //changes for word problem
                if(CommonUtils.isClickedSolveEquation)
                    context.startActivity(new Intent(context , SingleFriendzyEquationSolve.class));
                else{
                    //generateWarningDialog("School Curriculum Coming Soon!!!");
                    context.startActivity(new Intent(context , SingleFriendzySchoolCurriculumEquationSolve.class));
                }
            }
        });

        btnTahnks.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
            }
        });
    }

    /**
     * Generate Warning dialog
     * @param msg
     * @param okMsg
     */
    public void generateWarningDialog(String msg , String okMsg)
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_warning);
        dialog.show();

        TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
        Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
        btnOk.setText(okMsg);
        txtAlertMsgRegisterLogIn.setText(msg);
        btnOk.setOnClickListener( new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
            }
        });
    }

    /**
     * Genarate School Dialog
     * @param msg
     * @param schoolInfo
     * @param result
     */
    public void generateSchoolDialog(String msg,final ArrayList<String> schoolInfo,final int result)
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialogschoolfromusorcanada);
        dialog.show();

        Button btnNoThanks 	= (Button)dialog.findViewById(R.id.btnSchoolNoThanks);
        Button btnOk 		= (Button)dialog.findViewById(R.id.btnSchoolOk);
        TextView txtMsg     = (TextView)dialog.findViewById(R.id.txtschoolAlertMessage);

        transeletion.openConnection();
        btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        transeletion.closeConnection();
        txtMsg.setText(msg);

        //btnNoThanks.setOnClickListener(this);
        btnNoThanks.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                Intent returnIntent = new Intent();
                returnIntent.putExtra("schoolInfo",schoolInfo);
                ((Activity)context).setResult(result,returnIntent);
                ((Activity)context).finish();
            }
        });

        btnOk.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                emailIntent.setType("plain/text");
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,new String[]{"info@letsleapahead.com"});
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Please add my school.");
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Enter your school's name,city,state,and zip code here...");
                context.startActivity(emailIntent);

                Intent returnIntent = new Intent();
                returnIntent.putExtra("schoolInfo",schoolInfo);
                ((Activity)context).setResult(result,returnIntent);
                ((Activity)context).finish();
				/*Intent intent = context.getPackageManager().getLaunchIntentForPackage("com.android.email");
				context.startActivity(intent);*/
            }
        });
    }


    /**
     * This dalog open when user play with application and back without saving your data
     * @param
     */
    public void generatetimeSpentDialog()
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_time_spent_on_this_application);
        dialog.show();

        Button btnNoThanks 	= (Button)dialog.findViewById(R.id.btnNoThanks);
        Button btnYes 		= (Button)dialog.findViewById(R.id.btnYes);
        TextView txtMsg     = (TextView)dialog.findViewById(R.id.txtTimeSpentMessage);

        transeletion.openConnection();
        btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
        btnYes.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleYes"));
        txtMsg.setText(transeletion.getTranselationTextByTextIdentifier("alertMsgWouldYouLikeToRecordYourTimeSpentAndPointsEarnedOnThisApp"));
        transeletion.closeConnection();

        btnNoThanks.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
            }
        });

        btnYes.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                transeletion.openConnection();
                String msg = transeletion.getTranselationTextByTextIdentifier("alertMsgYouMustRegisterOrLogin");
                transeletion.closeConnection();
                //generateRegisterOrLogInDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouMustRegisterOrLogin"));
                generateRegisterOrLogInDialog(msg);
            }
        });
    }

    /**
     * This dalog open when user play with application and back without saving your data
     * @param msg
     */
    public void generateDialogUserEnterGraterCoinsThenAvalableCoins(String msg , final int userCoins)
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_enter_transfercoins_greater_available_coins);
        dialog.show();

        Button btnGetMoreCoins 	= (Button)dialog.findViewById(R.id.btnNoThanks);
        Button btnOk 		= (Button)dialog.findViewById(R.id.btnYes);
        TextView txtMsg     = (TextView)dialog.findViewById(R.id.txtTimeSpentMessage);

        transeletion.openConnection();
        btnGetMoreCoins.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGetMoreCoins"));
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        //txtMsg.setText(transeletion.getTranselationTextByTextIdentifier("alertMsgSorryYouDoNotHaveThatManyCoins"));
        txtMsg.setText(msg);
        transeletion.closeConnection();

        btnGetMoreCoins.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                context.startActivity(new Intent(context , GetMoreCoins.class));//chANGES
            }
        });

        btnOk.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();

                if(userCoins == 0)
                    context.startActivity(new Intent(context , MainActivity.class));
            }
        });
    }


    /**
     * Dialog when user click on proceed button , when user coins is less then the required coins
     * @param
     */
    public void generateDialogWhenUserClickOnProceedButtonForCoins(int earnCoins , int requiredCoins)
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_click_on_proceed_for_coins);
        dialog.show();

        Button btnNoThanks 	= (Button)dialog.findViewById(R.id.btnNoThanks);
        Button btnOk 		= (Button)dialog.findViewById(R.id.btnYes);
        TextView txtMsg     = (TextView)dialog.findViewById(R.id.txtYouneedMoreCoins);

        transeletion.openConnection();
        btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        txtMsg.setText(transeletion.getTranselationTextByTextIdentifier("alertMsgYouNeedMoreCoinsForThatItem"));
        transeletion.closeConnection();

        btnNoThanks.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
            }
        });

        btnOk.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                context.startActivity(new Intent(context , GetMoreCoins.class));//CHANGES
            }
        });
    }


    /**
     * Generate Delete confirmation dialog
     * @param msg
     */
    public void generateDeleteConfirmationDialog(String msg)
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialogdeleteconfirmation);
        dialog.show();

        Button btnNoThanks 	= (Button)dialog.findViewById(R.id.btnDeleteNoThanks);
        Button btnOk 		= (Button)dialog.findViewById(R.id.btnDeletelOk);
        TextView txtMsg     = (TextView)dialog.findViewById(R.id.txtschoolAlertMessage);

        transeletion.openConnection();
        btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        transeletion.closeConnection();
        txtMsg.setText(msg);

        btnNoThanks.setOnClickListener(this);
        btnOk.setOnClickListener(this);
    }


    /**
     * Generate Delete confirmation dialog
     * @param
     */
    public void generateResignDialogForMultifriendzy(final String friendzyId , final String winnerId)
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_resign_for_multifriendzy);
        dialog.show();

        Button btnNoThanks 	= (Button)dialog.findViewById(R.id.btnDeleteNoThanks);
        Button btnOk 		= (Button)dialog.findViewById(R.id.btnDeletelOk);
        TextView txtMsg     = (TextView)dialog.findViewById(R.id.txtIfYouResign);

        transeletion.openConnection();
        btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        txtMsg.setText(transeletion.getTranselationTextByTextIdentifier("alertMsgIfYouResignYouWillLose"));
        transeletion.closeConnection();
        btnNoThanks.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
            }
        });

        btnOk.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                new ResignAndSaveFriendzy(friendzyId , winnerId , context).execute(null,null,null);
                dialog.dismiss();
            }
        });
    }

    /**
     * De;ete Confirmation User Player dialog
     * @param msg
     * @param playerId
     * @param userId
     * @param callingActivity
     */
    public void generateDeleteConfirmationUserPlayerDialog(String msg,String playerId,String userId,String callingActivity)
    {
        this.playerId = playerId;
        this.userId   = userId;
        this.callingActivity = callingActivity;

        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.deleteuserplayerdialog);
        dialog.show();

        Button btnNoThanks 	= (Button)dialog.findViewById(R.id.btnDeleteNoThanks);
        Button btnOk 		= (Button)dialog.findViewById(R.id.btnDeletelUserPlayerOk);
        TextView txtMsg     = (TextView)dialog.findViewById(R.id.txtschoolAlertMessage);

        transeletion.openConnection();
        btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        transeletion.closeConnection();
        txtMsg.setText(msg);

        btnNoThanks.setOnClickListener(this);
        btnOk.setOnClickListener(this);
    }

    /**
     * Generate email existence dialog
     * @param msg
     */
    public void generateDialogEmailExistDialog(String msg)
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialogalreadyemailexist);
        dialog.show();

        Button btnNoThanks 	= (Button)dialog.findViewById(R.id.btnemailAlreadyExistThanks);
        Button btnLogin 	= (Button)dialog.findViewById(R.id.btnLogin);
        TextView txtMsg     = (TextView)dialog.findViewById(R.id.txtemailAlreadyExist);

        transeletion.openConnection();
        btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
        btnLogin.setText(transeletion.getTranselationTextByTextIdentifier("alertBtnLogin"));
        txtMsg.setText(msg);
        transeletion.closeConnection();

        btnNoThanks.setOnClickListener(this);
        btnLogin.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(context,LoginActivity.class);
                intent.putExtra("callingActivity", "RegistrationStep1");
                context.startActivity(intent);
            }
        });
    }


    /**
     * Generate dialog for the user when user type answer reverse
     */
    public void generateDialogForSolvingProblem(final CountDownTimer cdt)
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_reverse_result);
        dialog.show();

        TextView txtSolvingproblem = (TextView) dialog.findViewById(R.id.txtSolvingProblem);
        TextView txtMessageRight   = (TextView) dialog.findViewById(R.id.txtMessageForRightToLeft);
        TextView txtMessageLeft    = (TextView) dialog.findViewById(R.id.txtMessageFromLeftToRight);
        Button   btnCancel         = (Button)   dialog.findViewById(R.id.dialogBtnCancel);

        transeletion.openConnection();
        txtSolvingproblem.setText(transeletion.getTranselationTextByTextIdentifier("lblSolvingProblems"));
        txtMessageRight.setText(transeletion.getTranselationTextByTextIdentifier("lblWriteRightToLeft"));
        txtMessageLeft.setText(transeletion.getTranselationTextByTextIdentifier("lblTapOnTheLeftField"));
        btnCancel.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleCancel"));
        transeletion.closeConnection();

        btnCancel.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(cdt != null)
                {
                    cdt.start();
                }
                dialog.dismiss();
            }
        });
    }

    /**
     * Generate dialog for the user when user type answer reverse
     */
    public void generateDialogForSolvingProblem(final CountDownTimer cdt ,
                                                final OnReverseDialogClose onClose){
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_reverse_result);
        dialog.show();

        TextView txtSolvingproblem = (TextView) dialog.findViewById(R.id.txtSolvingProblem);
        TextView txtMessageRight   = (TextView) dialog.findViewById(R.id.txtMessageForRightToLeft);
        TextView txtMessageLeft    = (TextView) dialog.findViewById(R.id.txtMessageFromLeftToRight);
        Button   btnCancel         = (Button)   dialog.findViewById(R.id.dialogBtnCancel);

        transeletion.openConnection();
        txtSolvingproblem.setText(transeletion.getTranselationTextByTextIdentifier("lblSolvingProblems"));
        txtMessageRight.setText(transeletion.getTranselationTextByTextIdentifier("lblWriteRightToLeft"));
        txtMessageLeft.setText(transeletion.getTranselationTextByTextIdentifier("lblTapOnTheLeftField"));
        btnCancel.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleCancel"));
        transeletion.closeConnection();

        btnCancel.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                /*if(cdt != null){
                    cdt.start();
                }*/
                dialog.dismiss();
                onClose.onClose();
            }
        });
    }



    /**
     * Generate Forgate password dialog
     *
     */
    public void generateDialogForgatePassword()
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialogforgatepasswordlayout);
        dialog.show();

        TextView txtMsg 	= (TextView)dialog.findViewById(R.id.txtAlertMsgForgatePass);
        TextView txtEmail 	= (TextView) dialog.findViewById(R.id.txtEmail);
        final EditText edtEmail 	= (EditText) dialog.findViewById(R.id.edtEnterEmail);
        Button btnOk      	= (Button) dialog.findViewById(R.id.dialogBtnOk);

        transeletion.openConnection();
        txtMsg.setText(transeletion.getTranselationTextByTextIdentifier("alertMsgEnterYourEmailUsedDuringRegistrationSoWeCanSendYouYourPassword"));
        txtEmail.setText(transeletion.getTranselationTextByTextIdentifier("lblRegEmail") + ":");
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        transeletion.closeConnection();

        btnOk.setOnClickListener( new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(edtEmail.getText().toString().equals(""))
                {
                    dialog.dismiss();
                }
                else
                {
                    if(CommonUtils.isEmailValid(edtEmail.getText().toString()))
                    {
                        if(CommonUtils.isInternetConnectionAvailable(context))
                        {
                            new ForgetPass(edtEmail.getText().toString()).execute(null,null,null);
                        }
                        else
                        {
                            transeletion.openConnection();
                            generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
                            transeletion.closeConnection();
                        }
                    }
                    else
                    {
                        dialog.dismiss();
                    }
                }
            }
        });
    }

    /**
     * Generate Warning Dialog
     * @param msg
     */
    public void generateDateWarningDialog(String msg)
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_warning);
        dialog.show();

        TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
        Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
        transeletion.openConnection();
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        transeletion.closeConnection();
        txtAlertMsgRegisterLogIn.setText(msg);
        btnOk.setOnClickListener( new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                ((Activity)context).finish();
            }
        });
    }


    /**
     * This dialog open when user get coins from server
     * @param currentCoins
     * @param requiredCoins
     */
	/*public void generateLevelWarningDialogForLearnignCenter(final int currentCoins,final int requiredCoins , final int itemId)
	{
		dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.dialog_for_lock_level_for_learnign_center);
		dialog.show();

		TextView txtUnlock 			= (TextView) dialog.findViewById(R.id.txtUlockAllRemaining);
		TextView txtCurrentCoins 	= (TextView) dialog.findViewById(R.id.txtYourCurrentlyCoins);
		Button btnTahnks 			= (Button) dialog.findViewById(R.id.btnNoThanks);
		Button btnProceeds			= (Button) dialog.findViewById(R.id.btnProceeds);
		Button btnGetMoreCoins 		= (Button) dialog.findViewById(R.id.btnGetMoreCoins);


		DateTimeOperation numberformat = new DateTimeOperation();
		String reqcoins = numberformat.setNumberString(requiredCoins + "");
		String currCoins= numberformat.setNumberString(currentCoins + "");


		transeletion.openConnection();
		txtUnlock.setText(transeletion.getTranselationTextByTextIdentifier("alertMsgUnlockAllLevelsForOnly") + " " +
				reqcoins + " " + transeletion.getTranselationTextByTextIdentifier("lblCoins"));
		txtCurrentCoins.setText(transeletion.getTranselationTextByTextIdentifier("alertMsgYouCurrentlyHave") + " " +
				currCoins + " " + transeletion.getTranselationTextByTextIdentifier("alertMsgPointsInYourAccount"));

		btnTahnks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
		btnProceeds.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleProceed"));
		btnGetMoreCoins.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGetMoreCoins"));
		transeletion.closeConnection();

		btnTahnks.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				dialog.dismiss();			
			}
		});

		btnProceeds.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				dialog.dismiss();

				if(currentCoins < requiredCoins)
				{
					generateDialogWhenUserClickOnProceedButtonForCoins(currentCoins , requiredCoins);
				}
				else
				{
					//changes when exception occure in shilpi code add if condition only
					SharedPreferences sharedPreffPlayerInfo = context.getSharedPreferences(PLAYER_INFO, 0);
					String userId   = sharedPreffPlayerInfo.getString("userId", "") ; 
					String playerId = sharedPreffPlayerInfo.getString("playerId", "");

					if(userId.equals("0") && playerId.equals("0"))
					{
						int playerCoins = currentCoins - requiredCoins ;

						ArrayList<PurchaseItemObj> purchseItems = new ArrayList<PurchaseItemObj>();

						PurchaseItemObj itemObj = new PurchaseItemObj();
						itemObj.setUserId(userId);
						itemObj.setItemId(itemId);
						itemObj.setStatus(1);
						purchseItems.add(itemObj);

						LearningCenterimpl lrarningImpl = new LearningCenterimpl(context);
						lrarningImpl.openConn();
						lrarningImpl.updateCoinsForPlayer(playerCoins , userId , playerId);
						lrarningImpl.insertIntoPurchaseItem(purchseItems);
						lrarningImpl.closeConn();

						Intent intent = new Intent(context,context.getClass());
						context.startActivity(intent);
					}
					else
					{
					UserRegistrationOperation userObj = new UserRegistrationOperation(context);
					RegistereUserDto regUserObj = userObj.getUserData();

					int userCoins  = 0 ;
					int playerCoins = currentCoins - requiredCoins ;

					if(regUserObj.getCoins().length() > 0)
						userCoins = Integer.parseInt(regUserObj.getCoins());

					if(CommonUtils.isInternetConnectionAvailable(context))
					{
						new SaveCoinsAndItemStatus(itemId , userId , userCoins , playerId , playerCoins , requiredCoins).execute(null , null,null);
					}
					else
					{
						DialogGenerator dg = new DialogGenerator(context);
						Translation transeletion = new Translation(context);
						transeletion.openConnection();
						dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
						transeletion.closeConnection();
					}
				}
				}
				Intent intent = new Intent(context,CoinsDistributionActivity.class);
				context.startActivity(intent);
			}
		});

		btnGetMoreCoins.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				dialog.dismiss();
				context.startActivity(new Intent(context , GetMoreCoins.class));//CHANGES
			}
		});

	}*/

    /**
     * This dialog open when user get coins from server
     * @param currentCoins
     * @param requiredCoins
     *//*
	public void generateLevelWarningDialogForLearnignCenter(final int currentCoins,final int requiredCoins , final int itemId
			,String msg)
	{
		dialog = new Dialog(context, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.dialog_for_lock_level_for_learnign_center);
		dialog.show();

		TextView txtUnlock 			= (TextView) dialog.findViewById(R.id.txtUlockAllRemaining);
		TextView txtCurrentCoins 	= (TextView) dialog.findViewById(R.id.txtYourCurrentlyCoins);
		Button btnTahnks 			= (Button) dialog.findViewById(R.id.btnNoThanks);
		Button btnProceeds			= (Button) dialog.findViewById(R.id.btnProceeds);
		Button btnGetMoreCoins 		= (Button) dialog.findViewById(R.id.btnGetMoreCoins);


		DateTimeOperation numberformat = new DateTimeOperation();
		String reqcoins = numberformat.setNumberString(requiredCoins + "");
		String currCoins= numberformat.setNumberString(currentCoins + "");


		transeletion.openConnection();
		txtUnlock.setText(transeletion.getTranselationTextByTextIdentifier("alertMsgUnlockAllLevelsForOnly") + " " +
				reqcoins + " " + transeletion.getTranselationTextByTextIdentifier("lblCoins"));
		txtUnlock.setText(msg);
		txtCurrentCoins.setText(transeletion.getTranselationTextByTextIdentifier("alertMsgYouCurrentlyHave") + " " +
				currCoins + " " + transeletion.getTranselationTextByTextIdentifier("alertMsgPointsInYourAccount"));

		btnTahnks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
		btnProceeds.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleProceed"));
		btnGetMoreCoins.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGetMoreCoins"));
		transeletion.closeConnection();

		btnTahnks.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				dialog.dismiss();			
			}
		});

		btnProceeds.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				dialog.dismiss();

				if(currentCoins < requiredCoins)
				{
					generateDialogWhenUserClickOnProceedButtonForCoins(currentCoins , requiredCoins);
				}
				else
				{
					//changes for exception from shilpi add if condition only
					SharedPreferences sharedPreffPlayerInfo = context.getSharedPreferences(PLAYER_INFO, 0);
					String userId   = sharedPreffPlayerInfo.getString("userId", "") ; 
					String playerId = sharedPreffPlayerInfo.getString("playerId", "");

					if(userId.equals("0") && playerId.equals("0"))
					{
						int playerCoins = currentCoins - requiredCoins ;

						ArrayList<PurchaseItemObj> purchseItems = new ArrayList<PurchaseItemObj>();

						PurchaseItemObj itemObj = new PurchaseItemObj();
						itemObj.setUserId(userId);
						itemObj.setItemId(itemId);
						itemObj.setStatus(1);
						purchseItems.add(itemObj);

						LearningCenterimpl lrarningImpl = new LearningCenterimpl(context);
						lrarningImpl.openConn();
						lrarningImpl.updateCoinsForPlayer(playerCoins , userId , playerId);
						lrarningImpl.insertIntoPurchaseItem(purchseItems);
						lrarningImpl.closeConn();

						Intent intent = new Intent(context,context.getClass());
						context.startActivity(intent);
					}
					else
					{
					UserRegistrationOperation userObj = new UserRegistrationOperation(context);
					RegistereUserDto regUserObj = userObj.getUserData();

					int userCoins  = 0 ;
					int playerCoins = currentCoins - requiredCoins ;

					if(regUserObj.getCoins().length() > 0)
						userCoins = Integer.parseInt(regUserObj.getCoins());

					if(CommonUtils.isInternetConnectionAvailable(context))
					{
						new SaveCoinsAndItemStatus(itemId , userId , userCoins , playerId , playerCoins , requiredCoins).execute(null , null,null);
					}
					else
					{
						DialogGenerator dg = new DialogGenerator(context);
						Translation transeletion = new Translation(context);
						transeletion.openConnection();
						dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
						transeletion.closeConnection();
					}
				}
				}
				Intent intent = new Intent(context,CoinsDistributionActivity.class);
				context.startActivity(intent);
			}
		});

		btnGetMoreCoins.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				dialog.dismiss();
				context.startActivity(new Intent(context , GetMoreCoins.class));//changes
			}
		});
	}*/

    /**
     * Generate Warning Dialog
     * @param
     */
    public void generateDialogForNeedCoinsForUlock(int earnCoins,int requiredCoins , int forSuscription)
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_need_coins_for_unlock);
        dialog.show();

        TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
        Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
        transeletion.openConnection();
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        if(forSuscription == 1){
            txtAlertMsgRegisterLogIn.setText(transeletion.getTranselationTextByTextIdentifier("lblYouNeed")
                    + " " + CommonUtils.setNumberString((requiredCoins - earnCoins) + "") + " "
                    + transeletion.getTranselationTextByTextIdentifier("lblCoinsToUnlockAllFeaturesOfApp"));
        }else{
            txtAlertMsgRegisterLogIn.setText(transeletion.getTranselationTextByTextIdentifier("lblYouNeed")
                    + " " + CommonUtils.setNumberString((requiredCoins - earnCoins) + "") + " "
                    + transeletion.getTranselationTextByTextIdentifier("lblMoreCoinsToPurchaseTheItem"));
        }
        transeletion.closeConnection();

        btnOk.setOnClickListener( new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();

                getUserDataFromServer();

				/*Intent intent = new Intent(context,CoinsDistributionActivity.class);
				context.startActivity(intent);		*/
            }
        });
    }

    /**
     * Icon defination dialog on the help screen
     */
    public void helpStudentIconDefinationDialog(){
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.help_a_student_icon_defination_dialog);
        dialog.show();

        Button btncancel = (Button) dialog.findViewById(R.id.btncancel);
        TextView txtStudentWorkArea = (TextView) dialog.findViewById(R.id.txtStudentWorkArea);
        TextView txtStudentInput = (TextView) dialog.findViewById(R.id.txtStudentInput);
        TextView txtStudentStatus = (TextView) dialog.findViewById(R.id.txtStudentStatus);
        TextView txtStudentOnTheWorkArea = (TextView) dialog.findViewById(R.id.txtStudentOnTheWorkArea);
        TextView txtStudentActiveOnTheApp = (TextView) dialog.findViewById(R.id.txtStudentActiveOnTheApp);
        TextView txtStudentNotActiveOnTheApp = (TextView) dialog.findViewById(R.id.txtStudentNotActiveOnTheApp);
        TextView txtRatingGivenByStudent = (TextView) dialog.findViewById(R.id.txtRatingGivenByStudent);
        TextView txtOnceTheStudentHasunderstand = (TextView) dialog.findViewById(R.id.txtOnceTheStudentHasunderstand);
        transeletion.openConnection();
        txtStudentWorkArea.setText(" - " + transeletion.getTranselationTextByTextIdentifier("lblNoInputFromTheStudent"));
        txtStudentInput.setText(" - " + transeletion.getTranselationTextByTextIdentifier("lblInputFromStudent"));
        txtStudentStatus.setText(transeletion.getTranselationTextByTextIdentifier("lblWorkAreaStatus"));
        txtStudentOnTheWorkArea.setText(" - " + transeletion.getTranselationTextByTextIdentifier("lblTabToHelpStudent"));
        txtStudentActiveOnTheApp.setText(" - " + transeletion.getTranselationTextByTextIdentifier("lblActiveOnTheApp"));
        txtStudentNotActiveOnTheApp.setText(" - " + transeletion.getTranselationTextByTextIdentifier("lblNotActiveOnTheApp"));
        txtRatingGivenByStudent.setText(" - " + transeletion.getTranselationTextByTextIdentifier("lblRatingGivenByStudent"));
        txtOnceTheStudentHasunderstand.setText(transeletion.getTranselationTextByTextIdentifier("lblOnceTheStudentUnderstudTheProblem"));
        transeletion.closeConnection();
        btncancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    /**
     * Generate Warning Dialog
     * @param
     */
    public void generateFreeSubscriptionDialog(final FreeSubsriptionListener listener){
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_free_experience_our_school_features);
        dialog.show();

        Button btbCencel = (Button) dialog.findViewById(R.id.btncancel);
        TextView txtNowExperinceOurSchoolFeature = (TextView) dialog.
                findViewById(R.id.txtNowExperinceOurSchoolFeature);
        TextView txtFree = (TextView) dialog.findViewById(R.id.txtFree);
        TextView txtForSevenDays = (TextView) dialog.findViewById(R.id.txtForSevenDays);
        TextView txtWatchOurVedios = (TextView) dialog.findViewById(R.id.txtWatchOurVedios);
        Button btnIntroduction = (Button) dialog.findViewById(R.id.btnIntroduction);
        Button btnStudentFeature = (Button) dialog.findViewById(R.id.btnStudentFeature);
        Button btnTeacherFeature = (Button) dialog.findViewById(R.id.btnTeacherFeature);

        btnIntroduction.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //dialog.dismiss();
                listener.clickOnViewIntroduction();
            }
        });

        btnStudentFeature.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //dialog.dismiss();
                listener.clickOnViewStudentFeature();
            }
        });

        btnTeacherFeature.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //dialog.dismiss();
                listener.clickOnViewTeacherFeature();
            }
        });

        btbCencel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }


    /**
     * Show the free subscription dialog after going through the tutorial
     * @param listener
     */
    public void freeSubscriptionDialog(final YesNoListenerInterface listener){
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.free_subscription_dialog);
        dialog.show();

        transeletion.openConnection();
        TextView txtLimitedTimeOnly = (TextView) dialog.findViewById(R.id.txtLimitedTimeOnly);
        TextView txtExperienceAllTheStudent = (TextView)
                dialog.findViewById(R.id.txtExperienceAllTheStudent);
        TextView txtFree = (TextView) dialog.findViewById(R.id.txtFree);
        TextView txtFor30Days = (TextView) dialog.findViewById(R.id.txtFor30Days);
        Button btnNothanks = (Button) dialog.findViewById(R.id.btnNothanks);
        Button btnRegister = (Button) dialog.findViewById(R.id.btnRegister);

        txtLimitedTimeOnly.setText(transeletion
                .getTranselationTextByTextIdentifier("lblLimitedTimeOnly"));

        MathFriendzyHelper.setFont(context, txtLimitedTimeOnly,
                MathFriendzyHelper.DK_COLL_FONT_URI_IN_ASSETS);

        txtExperienceAllTheStudent.setText(transeletion
                .getTranselationTextByTextIdentifier("lblExperienceAllFunctions"));
        txtFree.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleTryFree"));
        txtFor30Days.setText(transeletion.getTranselationTextByTextIdentifier("lblFor30days"));
        btnNothanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
        btnRegister.setText(transeletion.getTranselationTextByTextIdentifier("regTitleRegistration"));
        transeletion.closeConnection();

        btnNothanks.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                listener.onNo();
            }
        });

        btnRegister.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                listener.onYes();
            }
        });
    }


    /**
     * Show the teacher function dialog
     * @param listener
     * @param dontShowAgainMsg
     * @param watchTheVedioText
     * @param msg
     */
    public void showTeacherFunctionsDialog(final TeacherFunctionDialogListener listener,
                                           String msg, String watchTheVedioText, String dontShowAgainMsg
            ,String popUpTitle , boolean isShowWatchButton){
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.teacher_function_layotu);
        dialog.show();

        transeletion.openConnection();
        Button btncancel = (Button) dialog.findViewById(R.id.btncancel);
        TextView txtThisbuttonwillGenerate = (TextView) dialog.
                findViewById(R.id.txtThisbuttonwillGenerate);
        final CheckBox chkShowAgain = (CheckBox) dialog.findViewById(R.id.chkShowAgain);
        TextView txtDontShowMsg = (TextView) dialog.findViewById(R.id.txtDontShowMsg);
        Button btnWathTheVedios = (Button) dialog.findViewById(R.id.btnWathTheVedios);
        TextView txtTitle = (TextView) dialog.findViewById(R.id.txtTitle);

        txtDontShowMsg.setText(transeletion.getTranselationTextByTextIdentifier("lblDontShowAgain"));
        btnWathTheVedios.setText(transeletion.getTranselationTextByTextIdentifier("lblWatchOurVideo"));
        btncancel.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        txtTitle.setText(popUpTitle);
        if(isShowWatchButton){
            btnWathTheVedios.setVisibility(Button.VISIBLE);
        }else{
            btnWathTheVedios.setVisibility(Button.INVISIBLE);
        }
		/*txtDontShowMsg.setText("Don't Show Again");
		btnWathTheVedios.setText("Watch the vedio");*/

        txtThisbuttonwillGenerate.setText(msg);
        transeletion.closeConnection();

        btncancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                listener.clickOnClose(chkShowAgain.isChecked());
                dialog.dismiss();
            }
        });

        btnWathTheVedios.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                listener.clickOnWatchVedio(chkShowAgain.isChecked());
                dialog.dismiss();
            }
        });


        dialog.setOnCancelListener(new OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                listener.clickOnDontShow(chkShowAgain.isChecked());
            }
        });
    }


    public void showPaidTutorProfessionalTutoringDialog(final TeacherFunctionDialogListener listener,
                                                        String msg){
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.paid_tutor_professional_tutoring_first_message_layout);
        dialog.show();
        transeletion.openConnection();
        Button btncancel = (Button) dialog.findViewById(R.id.btncancel);
        TextView txtThisbuttonwillGenerate = (TextView) dialog.
                findViewById(R.id.txtThisbuttonwillGenerate);
        final CheckBox chkShowAgain = (CheckBox) dialog.findViewById(R.id.chkShowAgain);
        TextView txtDontShowMsg = (TextView) dialog.findViewById(R.id.txtDontShowMsg);
        Button btnWathTheVedios = (Button) dialog.findViewById(R.id.btnWathTheVedios);
        txtDontShowMsg.setText(transeletion.getTranselationTextByTextIdentifier("lblDontShowAgain"));
        btnWathTheVedios.setText(transeletion.getTranselationTextByTextIdentifier("lblWatchVideo"));
        btncancel.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        txtThisbuttonwillGenerate.setText(msg);
        transeletion.closeConnection();

        btncancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.clickOnClose(chkShowAgain.isChecked());
                dialog.dismiss();
            }
        });

        btnWathTheVedios.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.clickOnWatchVedio(chkShowAgain.isChecked());
                dialog.dismiss();
            }
        });


        dialog.setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                listener.clickOnDontShow(chkShowAgain.isChecked());
            }
        });
    }

    /**
     * This method get data from server
     */
    private void getUserDataFromServer()
    {
        SharedPreferences sheredPreference = context.getSharedPreferences(LOGIN_SHARED_PREFF, 0);
        if(!sheredPreference.getBoolean(IS_LOGIN, false))
        {
            Translation transeletion = new Translation(context);
            transeletion.openConnection();
            DialogGenerator dg = new DialogGenerator(context);
            dg.generateRegisterOrLogInDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouMustRegisterOrLogin"));
            transeletion.closeConnection();
        }
        else
        {
            if(CommonUtils.isInternetConnectionAvailable(context)){
                UserRegistrationOperation userOprObj = new UserRegistrationOperation(context);
                RegistereUserDto regUserObj = userOprObj.getUserData();

                if(regUserObj.getEmail().length() > 1)
                {
                    new GetUserDetailByEmail(regUserObj.getEmail(), regUserObj.getPass()).execute(null,null,null);
                }
                else
                {
                    new GetUserDetailByUserId(regUserObj.getUserId(), regUserObj.getPass()).execute(null,null,null);
                }
            }
            else{
                UserRegistrationOperation userOprObj = new UserRegistrationOperation(context);
                RegistereUserDto regUserObj = userOprObj.getUserData();
                if(regUserObj.getCoins().equals("0"))
                    context.startActivity(new Intent(context , GetMoreCoins.class));//CHANGES
                else{
                    Intent intent = new Intent(context, CoinsDistributionActivity.class);
                    context.startActivity(intent);
                }
            }
        }
    }


    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnAlertBtnTitleNoThanks:
                dialog.dismiss();
                break;
            case R.id.dialogBtnOk:
                dialog.dismiss();
                break;

            case R.id.btnAlertBtnTitleRegister:
                dialog.dismiss();
                context.startActivity(new Intent(context, RegistrationStep1.class));
                break;

            case R.id.btnAlertBtnTitleLogIn:
                dialog.dismiss();
                Intent intent = new Intent(context, LoginActivity.class);
                intent.putExtra("callingActivity", ((Activity)context).getClass().getSimpleName());
                ((Activity)context).startActivity(intent);
                break;
            case R.id.btnSchoolNoThanks:
                dialog.dismiss();

                break;
            case R.id.btnemailAlreadyExistThanks:
                dialog.dismiss();
                break;
            case R.id.btnLogin:
			/*Intent intentLogin = new Intent(context,LoginActivity.class);
			context.startActivity(intentLogin);*/
                break;
            case R.id.btnDeletelOk:
                TempPlayerOperation tempObj = new TempPlayerOperation(context);
                tempObj.deleteFromTempPlayer();
                tempObj.closeConn();

                LearningCenterimpl learningCenterImpl = new LearningCenterimpl(context);
                learningCenterImpl.openConn();
                learningCenterImpl.deleteFromMathResult();
                learningCenterImpl.deleteFromPlayerEruationLevel();
                learningCenterImpl.deleteFromPlayerTotalPoints();
                learningCenterImpl.closeConn();

                SharedPreferences sheredPreference = context.getSharedPreferences(IS_CHECKED_PREFF, 0);
                SharedPreferences.Editor editor = sheredPreference.edit();
                editor.clear();
                editor.commit();
                Intent intentCreate = new Intent(context,CreateTempPlayerActivity.class);
                context.startActivity(intentCreate);
                break;
            case R.id.btnDeleteNoThanks:
                dialog.dismiss();
                break;
            case R.id.btnDeletelUserPlayerOk:
                dialog.dismiss();
                this.deleteUserPlayer();
			/*UserPlayerOperation userOprObj = new UserPlayerOperation(context);	
			userOprObj.deleteFromUserPlayerByPlayerId(playerId);
			userOprObj.closeConn();
			Intent intentCreateUserPlayer = new Intent(context,MainActivity.class);
			context.startActivity(intentCreateUserPlayer);*/
                break;
        }
    }


    /**
     * Generate Dialog for cannot find my Teacher
     * @param msg
     * @param teacherobj
     * @param result
     */
    public void canNotFindTeacherDialog(String msg , final TeacherDTO teacherobj , final int result)
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_warning);
        dialog.show();

        TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
        Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
        transeletion.openConnection();
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        transeletion.closeConnection();
        txtAlertMsgRegisterLogIn.setText(msg);
        btnOk.setOnClickListener( new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                ArrayList<String> teacherIdAndName = new ArrayList<String>();
                teacherIdAndName.add(teacherobj.getfName());//add teacher fname at 0th position
                teacherIdAndName.add(teacherobj.getlName());//add teacher lname at 1st position
                teacherIdAndName.add(teacherobj.getTeacherUserId());//add teacher id at 2nd position

                Intent returnIntent = new Intent();
                returnIntent.putExtra("schoolInfo",teacherIdAndName);
                ((Activity)context).setResult(result,returnIntent);
                ((Activity)context).finish();
            }
        });
    }

    /**
     * This method delete user player
     */
    private void deleteUserPlayer()
    {
        if(CommonUtils.isInternetConnectionAvailable(context))
            new DeletePlayerAsynkTask().execute(null,null,null);
        else
        {
            DialogGenerator dg = new DialogGenerator(context);
            Translation transeletion = new Translation(context);
            transeletion.openConnection();
            dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
            transeletion.closeConnection();
        }
    }

    /**
     * AsynckTask For delete user player from server
     * @author Yashwant Singh
     *
     */
    class DeletePlayerAsynkTask extends AsyncTask<Void, Void, Void>
    {
        String resultValue = null;
        ProgressDialog progressDialog = CommonUtils.getProgressDialog(context);

        @Override
        protected void onPreExecute()
        {
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            Register registerObj = new Register(context);
            resultValue     	 = registerObj.deleteRegisteredUser(userId, playerId);
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            progressDialog.cancel();

            if(resultValue != null){
                if(resultValue.equals("success"))
                {
                    int noOfUserPlayereRemaining = 0;

                    UserPlayerOperation userOprObj = new UserPlayerOperation(context);
                    userOprObj.deleteFromUserPlayerByPlayerId(playerId);
                    noOfUserPlayereRemaining = userOprObj.getUserPlayerData().size();
                    userOprObj.closeConn();

                    SharedPreferences sheredPreference = context.getSharedPreferences(IS_CHECKED_PREFF, 0);
                    SharedPreferences.Editor editor = sheredPreference.edit();
                    editor.clear();
                    editor.commit();

                    if(noOfUserPlayereRemaining > 0)
                    {
                        if(callingActivity.equals("LoginUserPlayerActivity"))
                        {
                            Intent intentCreateUserPlayer = new Intent(context,LoginUserPlayerActivity.class);
                            context.startActivity(intentCreateUserPlayer);
                        }
                        else if(callingActivity.equals("ModifyRegistration") || callingActivity.equals("RegisterationStep2"))
                        {
                            Intent intent = new Intent(context,ModifyRegistration.class);
                            context.startActivity(intent);
                        }
                        else if(callingActivity.equals("TeacherPlayer"))
                        {
                            Intent intent = new Intent(context,TeacherPlayer.class);
                            context.startActivity(intent);
                        }
                    }
                    else
                    {
                        Intent intentCreateUserPlayer = new Intent(context,LoginUserCreatePlayer.class);
                        context.startActivity(intentCreateUserPlayer);
                    }
                }
                else
                {
                    DialogGenerator dg = new DialogGenerator(context);
                    dg.generateWarningDialog("Proble In Deletion");
                }
            }else{
                CommonUtils.showInternetDialog(context);
            }
            super.onPostExecute(result);
        }
    }

    /**
     * This aynckTask send the pass. to user email id
     * @author Yashwant Singh
     *
     */
    class ForgetPass extends AsyncTask<Void, Void, Void>
    {
        private String email 		= null;
        private String resultValue  = null;

        ForgetPass(String email)
        {
            this.email = email;
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            Login login = new Login(context);
            resultValue = login.forgetPassword(email);
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            dialog.dismiss();
            if(resultValue != null){
                DialogGenerator dg = new DialogGenerator(context);
                Translation transeletion = new Translation(context);
                transeletion.openConnection();

                if(resultValue.equals("1"))
                {
                    dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYourPasswordHasBeenSentToYourEmail"));
                }
                else if(resultValue.equals("-9001"))
                {
                    dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgEmailUsernameNotInSystem"));
                }
            }else{
                CommonUtils.showInternetDialog(context);
            }
            transeletion.closeConnection();
        }
    }

    /**
     * This Asyncktask get User Detail from server
     * @author Yashwant Singh
     *
     */
    class GetUserDetailByEmail extends AsyncTask<Void, Void, Void>
    {
        private ProgressDialog pd 	= null;
        private String email 		= null;
        private String pass 		= null;

        GetUserDetailByEmail(String email, String pass)
        {
            this.email = email;
            this.pass  = pass;
        }

        @Override
        protected void onPreExecute()
        {
            pd = CommonUtils.getProgressDialog(context);
            pd.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            Login login = new Login(context);
            login.getUserDetailByEmail(email, pass);

            return null;

        }

        @Override
        protected void onPostExecute(Void result)
        {
            pd.cancel();

            UserRegistrationOperation userOprObj = new UserRegistrationOperation(context);
            RegistereUserDto regUserObj = userOprObj.getUserData();
            if(regUserObj.getCoins().equals("0"))
                context.startActivity(new Intent(context , GetMoreCoins.class));//CHANGES
            else{
                Intent intent = new Intent(context, CoinsDistributionActivity.class);
                context.startActivity(intent);
            }

            super.onPostExecute(result);
        }
    }


    /**
     * This Asyncktask get User Detail from server
     * @author Yashwant Singh
     *
     */
    class GetUserDetailByUserId extends AsyncTask<Void, Void, Void>
    {
        private String userId 		= null;
        private String pass     	= null;
        private ProgressDialog pd 	= null;

        GetUserDetailByUserId(String userId , String pass)
        {
            this.userId = userId;
            this.pass     =  pass;
        }

        @Override
        protected void onPreExecute()
        {
            pd = CommonUtils.getProgressDialog(context);
            pd.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            Login login = new Login(context);
            login.getUserDetailByUserId(userId);
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            pd.cancel();

			/*Intent intent = new Intent(context,CoinsDistributionActivity.class);
			context.startActivity(intent);*/
            UserRegistrationOperation userOprObj = new UserRegistrationOperation(context);
            RegistereUserDto regUserObj = userOprObj.getUserData();
            if(regUserObj.getCoins().equals("0"))
                context.startActivity(new Intent(context , GetMoreCoins.class));//CHANGES
            else{
                Intent intent = new Intent(context, CoinsDistributionActivity.class);
                context.startActivity(intent);
            }

            super.onPostExecute(result);
        }
    }


    /**
     * This class save itm status on server
     * @author Yashwant Singh
     *
     */
    class SaveCoinsAndItemStatus extends AsyncTask<Void, Void, Void>
    {
        private int itemId 		= 0;
        private String userId 	= null;
        private int userCoins 	= 0;
        private String playerId = null;
        private int playerCoins = 0;

        private ProgressDialog pd = null;

        SaveCoinsAndItemStatus(int itemId , String userId , int userCoins , String playerId , int playerCoins ,int requiredCoins)
        {
            this.itemId 	= itemId;
            this.userId 	= userId;
            this.userCoins  = userCoins;
            this.playerId 	= playerId;
            this.playerCoins = playerCoins;
        }

        @Override
        protected void onPreExecute()
        {
            pd = CommonUtils.getProgressDialog(context);
            pd.show();

            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            CoinsDistributionServerOperation coinsOperation = new CoinsDistributionServerOperation();
            coinsOperation.saveCoinsAndItemStatus(itemId, userId, userCoins, playerId, playerCoins);

            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            pd.cancel();

            ArrayList<PurchaseItemObj> purchseItems = new ArrayList<PurchaseItemObj>();

            PurchaseItemObj itemObj = new PurchaseItemObj();
            itemObj.setUserId(userId);
            itemObj.setItemId(itemId);
            itemObj.setStatus(1);
            purchseItems.add(itemObj);

            LearningCenterimpl lrarningImpl = new LearningCenterimpl(context);
            lrarningImpl.openConn();
            lrarningImpl.updateCoinsForPlayer(playerCoins , userId , playerId);
            lrarningImpl.insertIntoPurchaseItem(purchseItems);
            lrarningImpl.closeConn();

            Intent intent = new Intent(context,context.getClass());
            context.startActivity(intent);

            super.onPostExecute(result);
        }
    }

    /**
     * This method save and resign friendzy
     * @author Yashwant Singh
     *
     */
    class ResignAndSaveFriendzy extends AsyncTask<Void, Void, Void>
    {
        private String friendzyId;
        private String winnerId;
        private Context context;

        ResignAndSaveFriendzy(String friendzyId , String winnerId , Context context)
        {
            this.friendzyId = friendzyId ;
            this.winnerId   = winnerId;
            this.context 	= context;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            MultiFriendzyServerOperation serverObj = new MultiFriendzyServerOperation();
            serverObj.resignAndSaveFriendzy(friendzyId, winnerId);
            return null;
        }


        @Override
        protected void onPostExecute(Void result)
        {
            context.startActivity(new Intent(context , MultiFriendzyMain.class));
            super.onPostExecute(result);
        }

    }


    //dialog added by shilpi

    /**
     * This dialog open when user get coins from server
     * @param currentCoins
     * @param
     */
    public void generateLevelWarningDialogForLearnignCenter(final int currentCoins,CoinsFromServerObj coinsFromServer,
                                                            final int itemId,String msg){

        boolean isTrue = false;
        SharedPreferences sheredPreference = context.getSharedPreferences(LOGIN_SHARED_PREFF, 0);
        if(!sheredPreference.getBoolean(IS_LOGIN, false))
        {
            isTrue= true;
        }

        final boolean isTemp = isTrue;

        final int requiredCoins 	= coinsFromServer.getCoinsRequired();
        final int monthlyCoins		= coinsFromServer.getMonthlyCoins();//Change them in string format like 30,000
        final int yearlyCoins		= coinsFromServer.getYearlyCoins();
        String userCoins			= coinsFromServer.getCoinsPurchase() + "";

        dialog = new Dialog(context, R.style.CustomDialogTheme);
        if(isTemp)
        {
            userCoins = "0";
            dialog.setContentView(R.layout.dialog_for_lock_level_for_temp_player);
        }
        else
        {
            dialog.setContentView(R.layout.dialog_for_lock_level_for_learnign_center);
        }

        final String coins = userCoins;

        dialog.show();

        TextView txtUnlock 			= (TextView) dialog.findViewById(R.id.txtUlockAllRemaining);
        TextView txtCurrentCoins 	= (TextView) dialog.findViewById(R.id.txtYourCurrentlyCoins);
        Button btnAssign 			= (Button) dialog.findViewById(R.id.btnNoThanks);
        Button btnProceeds			= (Button) dialog.findViewById(R.id.btnProceeds);
        Button btnPurchaseCoins 	= (Button) dialog.findViewById(R.id.btnGetMoreCoins);

        transeletion.openConnection();
        DateTimeOperation numberformat = new DateTimeOperation();
        String reqcoins = numberformat.setNumberString(requiredCoins + "");
        String currCoins= numberformat.setNumberString(currentCoins + "");

        //Changes For Subscription
        if(!isTemp)
        {
            String msgForUserCoins	= "";
            if(userCoins.equals("0"))
            {
                btnAssign.setVisibility(View.GONE);
                if(currentCoins < requiredCoins)
                    btnProceeds.setVisibility(View.GONE);
            }
            else
            {
                msgForUserCoins = "\n\nYou also have "+CommonUtils.setNumberString(coins)
                        +" unassigned coins.";
            }
            TextView txtSubscription	= (TextView) dialog.findViewById(R.id.txtSubscription);
            Button	 btnCancel			= (Button) dialog.findViewById(R.id.btncancel);
            Button	 btnGoForYear		= (Button) dialog.findViewById(R.id.btnGoForYear);
            Button	 btnGoForMonth		= (Button) dialog.findViewById(R.id.btnGoForMonth);

            TextView txtMonthly			= (TextView) dialog.findViewById(R.id.txtMonthly);
            TextView txtYearly			= (TextView) dialog.findViewById(R.id.txtYearly);

			/*if(MainActivity.isTab)
			{
				btnCancel.setBackgroundResource(R.drawable.cross_ipad);
			}*/

            btnCancel.setOnClickListener(new OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    dialog.dismiss();
                }
            });

            btnGoForMonth.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    goForSubscription(true, currentCoins, monthlyCoins, coins, 1);
                }
            });
            btnGoForYear.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    goForSubscription(false, currentCoins, yearlyCoins, coins, 12);
                }
            });

            if(msg == null){
                txtUnlock.setText(transeletion.getTranselationTextByTextIdentifier("alertMsgUnlockAllLevelsForOnly") + " " +
                        reqcoins + " " + transeletion.getTranselationTextByTextIdentifier("lblCoins"));
            }
            else{

                txtUnlock.setText(msg);
            }

            txtCurrentCoins.setText(transeletion.getTranselationTextByTextIdentifier("lblYouHave") + " " +
                    currCoins + " " + transeletion.getTranselationTextByTextIdentifier("lblCoins")+
                    " assigned to your account."+msgForUserCoins);

            btnAssign.setText("Assign Coins");
            btnProceeds.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleProceed"));
            btnPurchaseCoins.setText("Purchase Coins");
            btnGoForMonth.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGo")+"!");
            btnGoForYear.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGo")+"!");
            txtMonthly.setText("1 Month = "+CommonUtils.setNumberString(monthlyCoins +"")+" coins");
            txtYearly.setText("1 Year = "+CommonUtils.setNumberString(yearlyCoins +"")+" coins");
            txtSubscription.setText(transeletion.getTranselationTextByTextIdentifier("lblOrYouCanPurchaseSchoolSubscription"));
        }
        else
        {
            txtUnlock.setText(transeletion.getTranselationTextByTextIdentifier("alertMsgUnlockAllLevelsForOnly") + " " +
                    reqcoins + " " + transeletion.getTranselationTextByTextIdentifier("lblCoins"));
            txtCurrentCoins.setText(transeletion.getTranselationTextByTextIdentifier("alertMsgYouCurrentlyHave") + " " +
                    currCoins + " " + transeletion.getTranselationTextByTextIdentifier("alertMsgPointsInYourAccount"));

            btnAssign.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
            btnProceeds.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleProceed"));
            btnPurchaseCoins.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGetMoreCoins"));
        }

        transeletion.closeConnection();

        btnAssign.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                dialog.dismiss();
                if(!isTemp)
                {
                    getUserDataFromServer();
					/*Intent intent = new Intent(context,CoinsDistributionActivity.class);
					context.startActivity(intent);*/
                }
            }
        });

        btnProceeds.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();

                if(currentCoins < requiredCoins)//player coins and required coins
                {
                    generateDialogForNeedCoinsForUlock(currentCoins , requiredCoins , 0);
                }
                else
                {
                    SharedPreferences sharedPreffPlayerInfo = context.getSharedPreferences(PLAYER_INFO, 0);
                    String userId   = sharedPreffPlayerInfo.getString("userId", "") ;
                    String playerId = sharedPreffPlayerInfo.getString("playerId", "");

                    if(userId.equals("0") && playerId.equals("0"))
                    {
                        int playerCoins = currentCoins - requiredCoins ;

                        ArrayList<PurchaseItemObj> purchseItems = new ArrayList<PurchaseItemObj>();

                        PurchaseItemObj itemObj = new PurchaseItemObj();
                        itemObj.setUserId(userId);
                        itemObj.setItemId(itemId);
                        itemObj.setStatus(1);
                        purchseItems.add(itemObj);

                        LearningCenterimpl lrarningImpl = new LearningCenterimpl(context);
                        lrarningImpl.openConn();
                        lrarningImpl.updateCoinsForPlayer(playerCoins , userId , playerId);
                        lrarningImpl.insertIntoPurchaseItem(purchseItems);
                        lrarningImpl.closeConn();

                        Intent intent = new Intent(context,context.getClass());
                        context.startActivity(intent);
                    }
                    else
                    {
                        UserRegistrationOperation userObj = new UserRegistrationOperation(context);
                        RegistereUserDto regUserObj = userObj.getUserData();

                        int userCoins  = 0 ;
                        int playerCoins = currentCoins - requiredCoins ;

                        if(regUserObj.getCoins().length() > 0)
                            userCoins = Integer.parseInt(regUserObj.getCoins());

                        if(CommonUtils.isInternetConnectionAvailable(context))
                        {
                            new SaveCoinsAndItemStatus(itemId , userId , userCoins , playerId , playerCoins , requiredCoins).execute(null , null,null);
                        }
                        else
                        {
                            DialogGenerator dg = new DialogGenerator(context);
                            Translation transeletion = new Translation(context);
                            transeletion.openConnection();
                            dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
                            transeletion.closeConnection();
                        }
                    }
                }
				/*Intent intent = new Intent(context,CoinsDistributionActivity.class);
				context.startActivity(intent);*/
            }
        });

        btnPurchaseCoins.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                context.startActivity(new Intent(context , GetMoreCoins.class));//CHANGES
            }
        });

    }

    private void goForSubscription(boolean isMonth, int currentCoins, int requiredCoins, String userCoins, int duration)
    {
        if(currentCoins < requiredCoins)
        {
            generateDialogForNeedCoinsForUlock(currentCoins , requiredCoins , 1);
        }
        else
        {
            if(CommonUtils.isInternetConnectionAvailable(context))
            {
                new SaveSubscriptionUser(context, (currentCoins-requiredCoins), duration).execute(null,null,null);
            }
            else
            {
                CommonUtils.showInternetDialog(context);
            }
        }
    }


    /**
     * Generate Warning Dialog
     * @param msg
     */
    public void generateSubsriptionDialog(String msg)
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_friendzy);
        dialog.show();

        TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
        Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
        transeletion.openConnection();
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        transeletion.closeConnection();
        txtAlertMsgRegisterLogIn.setText(msg);
        btnOk.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(context, context.getClass());
                context.startActivity(intent);

            }
        });
    }

    //code added by shilpi for friendzy challenge
    /**
     * Generate Warning Dialog
     * @param msg
     */
    public void generateFriendzyWarningDialog(String msg)
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_friendzy);
        dialog.show();

        TextView txtAlertMsgRegisterLogIn = (TextView) dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
        Button btnOk = (Button)dialog.findViewById(R.id.dialogBtnOk);
        transeletion.openConnection();
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        transeletion.closeConnection();
        txtAlertMsgRegisterLogIn.setText(msg);
        btnOk.setOnClickListener(this);
    }

    /**
     * Generate Dialog for check ChallengerId
     * @param
     */
    public void generateFriendzyCheckDialog(final ImageButton img, final String challengerId ,
                                            final FriendzyDTO friendzyDTO)
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialogdeleteconfirmation);
        dialog.show();

        Button btnNoThanks  = (Button)dialog.findViewById(R.id.btnDeleteNoThanks);
        Button btnOk   = (Button)dialog.findViewById(R.id.btnDeletelOk);
        TextView txtMsg     = (TextView)dialog.findViewById(R.id.txtschoolAlertMessage);

        transeletion.openConnection();
        btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        txtMsg.setText(transeletion.getTranselationTextByTextIdentifier("lblFriendzyChallengeWeek")+
                "\n\n"+transeletion.getTranselationTextByTextIdentifier("lblSpellAppYouAreNowActivelyParticipating"));

        transeletion.closeConnection();

        btnOk.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                SharedPreferences sharedPreffPlayerInfo  = context.getSharedPreferences(IS_CHECKED_PREFF, 0);
                String playerId   = sharedPreffPlayerInfo.getString("playerId", "");
                //String userId     = sharedPreffPlayerInfo.getString("userId", "");

                CommonUtils.remeoveChallenge(playerId);

                FriendzyDTO friendzyDto = new FriendzyDTO();
                friendzyDto = friendzyDTO;
                friendzyDto.setChallengerId(challengerId);
                friendzyDto.setPid(playerId);
                CommonUtils.activeFriendzyPlayerList.add(friendzyDto);

                if(img != null)
                {
                    if(MainActivity.isTab)
                    {
                        img.setBackgroundResource(R.drawable.tab_checkbox_checked_ipad);
                    }
                    else
                    {
                        img.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
                    }
                }
                //img.setTag("1");
                context.startActivity(new Intent(context, MainActivity.class));
                ((Activity)context).finish();
                dialog.dismiss();
            }
        });
        btnNoThanks.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v)
            {
                //img.setTag("0");
                dialog.dismiss();
            }
        });
    }

    /**
     * This dialog open when user get coins from server
     * @param currentCoins
     * @param
     */
    public void generateDialogForFriendzyUnlock(final int currentCoins,CoinsFromServerObj coinsFromServer)
    {
        final int monthlyCoins  = coinsFromServer.getMonthlyCoins();
        final int yearlyCoins  = coinsFromServer.getYearlyCoins();
        String userCoins   = coinsFromServer.getCoinsPurchase()+"";

        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_for_lock_level_for_friendzy);

        final String coins = userCoins;
        dialog.show();

        RelativeLayout lay   = (RelativeLayout) dialog.findViewById(R.id.ShowMessage);
        LinearLayout lin   = (LinearLayout) dialog.findViewById(R.id.linearLayout);
        Button btnProceeds   = (Button) dialog.findViewById(R.id.btnProceeds);
        /**********Hide Buttons For Friendzy Subscription *************/
        lay.setVisibility(View.GONE);
        lin.setVisibility(View.GONE);
        btnProceeds.setVisibility(View.GONE);

        //Changes For Freindzy Subscription
        TextView txtSubscription = (TextView) dialog.findViewById(R.id.txtSubscription);
        Button  btnCancel   = (Button) dialog.findViewById(R.id.btncancel);
        Button  btnGoForYear  = (Button) dialog.findViewById(R.id.btnGoForYear);
        Button  btnGoForMonth  = (Button) dialog.findViewById(R.id.btnGoForMonth);
        Button   btnPurchaseCoins  = (Button) dialog.findViewById(R.id.btnPurchaseCoins);

        TextView txtMonthly   = (TextView) dialog.findViewById(R.id.txtMonthly);
        TextView txtYearly   = (TextView) dialog.findViewById(R.id.txtYearly);
        TextView txtContactDetails = (TextView) dialog.findViewById(R.id.txtContactDetails);

        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        lp.setMargins(10, 30, 30, 10);
        txtSubscription.setLayoutParams(lp);

        btnPurchaseCoins.setVisibility(View.VISIBLE);

        if(MainActivity.isTab)
        {
            btnCancel.setBackgroundResource(R.drawable.cross_ipad);
        }

        btnCancel.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
            }
        });

        btnGoForMonth.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                goForSubscription(true, currentCoins, monthlyCoins, coins, 1);
            }
        });
        btnGoForYear.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                goForSubscription(false, currentCoins, yearlyCoins, coins, 12);
            }
        });

        transeletion.openConnection();
        btnPurchaseCoins.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGetMoreCoins"));

        btnGoForMonth.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGo")+"!");
        btnGoForYear.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGo")+"!");
        txtMonthly.setText("1 Month = "+CommonUtils.setNumberString(monthlyCoins +"")+" coins");
        txtYearly.setText("1 Year = "+CommonUtils.setNumberString(yearlyCoins +"")+" coins");
        txtSubscription.setText(transeletion.getTranselationTextByTextIdentifier("lblThisFeatureIsPartOfSchoolSubscription"));
        txtContactDetails.setText(transeletion.getTranselationTextByTextIdentifier("lblWeHaveSpecialPricesForSchools"));

        transeletion.closeConnection();

        btnPurchaseCoins.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                context.startActivity(new Intent(context , GetMoreCoins.class));//CHANGES
            }
        });
    }//END generateDialogForFriendzyUnlock method

    //added for send email for wrong question
    /**
     * Generate Delete confirmation dialog
     * @param
     */
    public void generateDialogForSendEmailForWrongQuestion(final int questionId
            , final com.mathfriendzy.controller.timer.MyTimerInrerface myTimer)
    {

        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_resign_for_multifriendzy);
        dialog.show();

        Button btnNoThanks 	= (Button)dialog.findViewById(R.id.btnDeleteNoThanks);
        Button btnOk 		= (Button)dialog.findViewById(R.id.btnDeletelOk);
        TextView txtMsg     = (TextView)dialog.findViewById(R.id.txtIfYouResign);

        transeletion.openConnection();
        btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleCancel"));
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleSubmit"));
        txtMsg.setText(transeletion.getTranselationTextByTextIdentifier("lblIfYouFeelThereIsProblem"));
        transeletion.closeConnection();

        if(myTimer != null)
            myTimer.cancelCurrentTimerAndSetTheResumeTime();
        ReportProblemActivity.timer = myTimer;

        btnNoThanks.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                if(myTimer != null)
                    myTimer.resumeTimerFromTimeSetAtCancelTime();
            }
        });

        btnOk.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();

				/*if(CommonUtils.isInternetConnectionAvailable(context)){
					new SendEmailForWrongQuestion(questionId).execute(null,null,null);
				}*/

				/*if(myTimer != null)
					myTimer.cancelCurrentTimerAndSetTheResumeTime();
				ReportProblemActivity.timer = myTimer;*/

                Intent intent = new Intent(context , ReportProblemActivity.class);
                intent.putExtra("questionId", questionId);
                //intent.putExtra("timer", myTimer);
                context.startActivity(intent);
            }
        });

        dialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dialog.dismiss();

                    if(myTimer != null)
                        myTimer.resumeTimerFromTimeSetAtCancelTime();

                }
                return true;
            }
        });
    }

    /**
     * This class send email
     * @author Yashwant Singh
     *
     */
    class SendEmailForWrongQuestion extends AsyncTask<Void, Void, Void>{

        private int questionId = 0;
        private ProgressDialog pd;

        SendEmailForWrongQuestion(int questionId){
            this.questionId = questionId;
        }

        @Override
        protected void onPreExecute() {
            pd = CommonUtils.getProgressDialog(context);
            pd.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            LearnignCenterSchoolCurriculumServerOperation serverObj =
                    new LearnignCenterSchoolCurriculumServerOperation();
            serverObj.sendEmailForWorngQuestion(questionId , "");
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            pd.cancel();
            transeletion.openConnection();
            generateWarningDialogForSendEmailSuccessfully(
                    transeletion.getTranselationTextByTextIdentifier("lblThisQuestionHasBeenSubmitted") ,
                    transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
            transeletion.closeConnection();
            super.onPostExecute(result);
        }
    }

    //changes for Spanish
    /**
     * Generate Delete confirmation dialog
     * @param msg
     */
    public void generateDialogSpanishChagesQuestionDownload(String msg , final int langCode ,
                                                            final int grade , final boolean isAssessment , final boolean isCountDownTimer ,
                                                            final CountDownTimer timer , final Handler customHandler ,
                                                            final long startTimeForTimer , final Runnable updateTimerThread
                                                            //for question translation , Issues list 5 march 2014 , issues no. 5
            ,final ShowTranslateQuestionAfterDownloading showQuestionAfterTranslation)
    //end changes
    {
        final Date dawnloadStartTime   = new Date();

        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dailog_for_spanish_question_download_or_not);
        dialog.show();

        Button btnNoThanks 	= (Button)dialog.findViewById(R.id.btnDeleteNoThanks);
        Button btnOk 		= (Button)dialog.findViewById(R.id.btnDeletelOk);
        TextView txtMsg     = (TextView)dialog.findViewById(R.id.txtschoolAlertMessage);

        transeletion.openConnection();
        btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleYes"));
        transeletion.closeConnection();
        txtMsg.setText(msg);

        btnNoThanks.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if(!isAssessment){
                    if(isCountDownTimer){
                        if(timer != null)
                            timer.start();
                    }else{
                        if(customHandler != null){
                            Date dawnloadEndTime = new Date();
                            SchoolCurriculumEquationSolveBase.startTimeForTimer =
                                    SchoolCurriculumEquationSolveBase.startTimeForTimer + (dawnloadEndTime.getTime() -
                                            dawnloadStartTime.getTime());
                            customHandler.postDelayed(updateTimerThread, 0);
                        }
                    }
                }
            }
        });


        btnOk.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                if(CommonUtils.isInternetConnectionAvailable(context)){
                    CommonUtils.isOneTimeClick = false;
                    new GetWordProblemQuestion(grade, context, langCode
                            ,isAssessment , isCountDownTimer , timer , customHandler
                            ,startTimeForTimer,updateTimerThread , dawnloadStartTime
                            //for question translation , Issues list 5 march 2014 , issues no. 5
                            , showQuestionAfterTranslation)
                            //end changes
                            .execute(null,null,null);
                }else{
                    transeletion.openConnection();
                    generateWarningDialog
                            (transeletion.getTranselationTextByTextIdentifier
                                    ("alertMsgYouAreNotConnectedToTheInternet"));
                    transeletion.closeConnection();
                }

            }
        });
    }

    /**
     * This asynctask get questions from server according to grade if not loaded in database
     * @author Yashwant Singh
     *
     */
    public class GetWordProblemQuestion extends AsyncTask<Void, Void, QuestionLoadedTransferObj>{

        private int grade;
        private ProgressDialog pd;
        private Context context;
        ArrayList<String> imageNameList = new ArrayList<String>();
        private int langCode = 1;

        //for timer
        private boolean isAssessment;
        private  boolean isCountDownTimer;
        private CountDownTimer timer;
        private Handler customHandler;
        //private long startTimeForTimer;
        private Runnable updateTimerThread;
        private Date dawnloadStartTime = null;

        //for question translation , Issues list 5 march 2014 , issues no. 5
        private ShowTranslateQuestionAfterDownloading showQuestionAfterTranslation = null;
        //end chnages

        public GetWordProblemQuestion(int grade , Context context , int langCode , boolean isAssesssment
                ,  boolean isCountDownTimer , CountDownTimer timer ,  Handler customHandler
                , long startTimeForTimer , Runnable updateTimerThread , Date dawnloadStartTime
                                      //for question translation , Issues list 5 march 2014 , issues no. 5
                , ShowTranslateQuestionAfterDownloading showQuestionAfterTranslation){
            //end changes

            this.grade 		= grade;
            this.context 	= context;
            this.langCode 	= langCode;
            this.isAssessment = isAssesssment;
            this.isCountDownTimer = isCountDownTimer;
            this.timer = timer;
            this.customHandler = customHandler;
            //this.startTimeForTimer = startTimeForTimer;
            this.updateTimerThread = updateTimerThread;
            this.dawnloadStartTime = dawnloadStartTime;

            //for question translation , Issues list 5 march 2014 , issues no. 5
            this.showQuestionAfterTranslation = showQuestionAfterTranslation;
            //end changes
        }

        @Override
        protected void onPreExecute() {

            Translation translation = new Translation(context);
            translation.openConnection();
            pd = CommonUtils.getProgressDialog(context, translation.getTranselationTextByTextIdentifier
                    ("alertPleaseWaitWeAreDownloading")
                    + "\n\n" + translation.getTranselationTextByTextIdentifier
                    ("lblThisCouldTakeSeveralMinutes") , false);
            translation.closeConnection();
            pd.show();

            super.onPreExecute();
        }

        @Override
        protected QuestionLoadedTransferObj doInBackground(Void... params) {
            LearnignCenterSchoolCurriculumServerOperation serverObj = new LearnignCenterSchoolCurriculumServerOperation();
            //QuestionLoadedTransferObj questionData = serverObj.getWordProblemQuestions(grade);

            //changes for Spanish Changes
            QuestionLoadedTransferObj questionData = new QuestionLoadedTransferObj();
            if(langCode == CommonUtils.ENGLISH)
                questionData = serverObj.getWordProblemQuestions(grade);
            else
                questionData= serverObj.getWordProblemQuestionsForSpanish(grade, CommonUtils.SPANISH);

            if(questionData != null){
                SchoolCurriculumLearnignCenterimpl schooloCurriculumImpl = new SchoolCurriculumLearnignCenterimpl(context);
                schooloCurriculumImpl.openConnection();

                if(langCode == CommonUtils.ENGLISH){
                    //changes for dialog loading wheel
                    schooloCurriculumImpl.deleteFromWordProblemCategoriesbyGrade(grade);
                    schooloCurriculumImpl.deleteFromWordProblemsSubCategoriesByCategoryId(questionData.getCategoryList());
                    schooloCurriculumImpl.deleteFromWordProblemsQuestionsCategoryId(questionData.getCategoryList());

                    schooloCurriculumImpl.insertIntoWordProblemCategories(questionData.getCategoryList(), grade);
                    schooloCurriculumImpl.insertIntoWordProblemsSubCategories(questionData.getCategoryList());
                    schooloCurriculumImpl.insertIntoWordProblemsQuestions(questionData.getQuestionList());
                }else{
                    SpanishChangesImpl implObj = new SpanishChangesImpl(context);
                    implObj.openConn();

                    implObj.deleteFromWordProblemCategoriesbyGrade(grade);
                    implObj.deleteFromWordProblemsSubCategoriesByCategoryId(questionData.getCategoryList());
                    implObj.deleteFromWordProblemsQuestionsCategoryId(questionData.getCategoryList());

                    implObj.insertIntoWordProblemCategories(questionData.getCategoryList(), grade);
                    implObj.insertIntoWordProblemsSubCategories(questionData.getCategoryList());
                    implObj.insertIntoWordProblemsQuestions(questionData.getQuestionList());
                    implObj.closeConn();
                }

                for(int i = 0 ; i < questionData.getQuestionList().size() ; i ++ ){

                    WordProblemQuestionTransferObj questionObj = questionData.getQuestionList().get(i);

                    if(questionObj.getQuestion().contains(".png"))
                        imageNameList.add(questionObj.getQuestion());
                    if(questionObj.getOpt1().contains(".png"))
                        imageNameList.add(questionObj.getOpt1());
                    if(questionObj.getOpt2().contains(".png"))
                        imageNameList.add(questionObj.getOpt2());
                    if(questionObj.getOpt3().contains(".png"))
                        imageNameList.add(questionObj.getOpt3());
                    if(questionObj.getOpt4().contains(".png"))
                        imageNameList.add(questionObj.getOpt4());
                    if(questionObj.getOpt5().contains(".png"))
                        imageNameList.add(questionObj.getOpt5());
                    if(questionObj.getOpt6().contains(".png"))
                        imageNameList.add(questionObj.getOpt6());
                    if(questionObj.getOpt7().contains(".png"))
                        imageNameList.add(questionObj.getOpt7());
                    if(questionObj.getOpt8().contains(".png"))
                        imageNameList.add(questionObj.getOpt8());
                    if(!questionObj.getImage().equals(""))
                        imageNameList.add(questionObj.getImage());
                }

                if(!schooloCurriculumImpl.isImageTableExist()){
                    schooloCurriculumImpl.createSchoolCurriculumImageTable();
                }
                schooloCurriculumImpl.closeConnection();
            }
            //end changes
            return questionData;
        }

        @Override
        protected void onPostExecute(QuestionLoadedTransferObj questionData) {

            pd.cancel();

            if(questionData != null){
                DawnloadImagesFromserver serverObj = new DawnloadImagesFromserver(imageNameList, context);
                Thread imageDawnLoadThrad = new Thread(serverObj);
                imageDawnLoadThrad.start();

                if(questionData.getCategoryList().size() == 0 ){
					/*DialogGenerator dg = new DialogGenerator(context);
				dg.generateWarningDialog("Coming Soon!!!");*/
                    CommonUtils.comingSoonPopUp(context);
                }else{//for question translation , Issues list 5 march 2014 , issues no. 5
                    if(showQuestionAfterTranslation != null){
                        showQuestionAfterTranslation.
                                showQuestionAfterTranslationDownload(null, isAssessment);
                    }
                }
                //end changes
            }else{
                CommonUtils.showInternetDialog(context);
            }

            if(!isAssessment){
                if(isCountDownTimer){
                    if(timer != null)
                        timer.start();
                }else{
                    if(customHandler != null){
                        Date dawnloadEndTime = new Date();
                        SchoolCurriculumEquationSolveBase.startTimeForTimer =
                                SchoolCurriculumEquationSolveBase.startTimeForTimer +
                                        (dawnloadEndTime.getTime() - dawnloadStartTime.getTime());
                        customHandler.postDelayed(updateTimerThread, 0);
                    }
                }
            }

            super.onPostExecute(questionData);
        }
    }

    /**
     * Rate us dialog
     * @param message
     * @param okText
     * @param noThanksText
     * @param callback
     */
    public void rateDialog(String message , String okText , String noThanksText ,
                           final RateUsCallback callback){
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.rate_us_dialog_layout);
        dialog.show();

        TextView txtMessage = (TextView) dialog.findViewById(R.id.txtMessage);
        final ImageView imgStar1  = (ImageView) dialog.findViewById(R.id.imgStar1);
        final ImageView imgStar2  = (ImageView) dialog.findViewById(R.id.imgStar2);
        final ImageView imgStar3  = (ImageView) dialog.findViewById(R.id.imgStar3);
        final ImageView imgStar4  = (ImageView) dialog.findViewById(R.id.imgStar4);
        final ImageView imgStar5  = (ImageView) dialog.findViewById(R.id.imgStar5);
        final Button btnOk        = (Button) dialog.findViewById(R.id.btnOk);
        final Button btnNoThanks  = (Button) dialog.findViewById(R.id.btnNoThanks);
        final Button btncancel    = (Button) dialog.findViewById(R.id.btncancel);

        txtMessage.setText(message);
        btnOk.setText(okText);
        btnNoThanks.setText(Html.fromHtml(noThanksText));

        OnClickListener MyOnClickListener = new OnClickListener(){
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                   case R.id.imgStar1:
                        numberOfStarSelected = 1;
                        imgStar1.setBackgroundResource(R.drawable.star_selected);
                        imgStar2.setBackgroundResource(R.drawable.start_unselected);
                        imgStar3.setBackgroundResource(R.drawable.start_unselected);
                        imgStar4.setBackgroundResource(R.drawable.start_unselected);
                        imgStar5.setBackgroundResource(R.drawable.start_unselected);
                        break;
                    case R.id.imgStar2:
                        numberOfStarSelected = 2;
                        imgStar1.setBackgroundResource(R.drawable.star_selected);
                        imgStar2.setBackgroundResource(R.drawable.star_selected);
                        imgStar3.setBackgroundResource(R.drawable.start_unselected);
                        imgStar4.setBackgroundResource(R.drawable.start_unselected);
                        imgStar5.setBackgroundResource(R.drawable.start_unselected);
                        break;
                    case R.id.imgStar3:
                        numberOfStarSelected = 3;
                        imgStar1.setBackgroundResource(R.drawable.star_selected);
                        imgStar2.setBackgroundResource(R.drawable.star_selected);
                        imgStar3.setBackgroundResource(R.drawable.star_selected);
                        imgStar4.setBackgroundResource(R.drawable.start_unselected);
                        imgStar5.setBackgroundResource(R.drawable.start_unselected);
                        break;
                    case R.id.imgStar4:
                        numberOfStarSelected = 4;
                        imgStar1.setBackgroundResource(R.drawable.star_selected);
                        imgStar2.setBackgroundResource(R.drawable.star_selected);
                        imgStar3.setBackgroundResource(R.drawable.star_selected);
                        imgStar4.setBackgroundResource(R.drawable.star_selected);
                        imgStar5.setBackgroundResource(R.drawable.start_unselected);
                        break;
                    case R.id.imgStar5:
                        numberOfStarSelected = 5;
                        imgStar1.setBackgroundResource(R.drawable.star_selected);
                        imgStar2.setBackgroundResource(R.drawable.star_selected);
                        imgStar3.setBackgroundResource(R.drawable.star_selected);
                        imgStar4.setBackgroundResource(R.drawable.star_selected);
                        imgStar5.setBackgroundResource(R.drawable.star_selected);
                        break;
                    case R.id.btnOk:
                        dialog.dismiss();
                        callback.numberOfStar(numberOfStarSelected);
                        break;
                    case R.id.btnNoThanks:
                        //dialog.dismiss();
                        callback.clickOnRateThisTutor();
                        break;
                    case R.id.btncancel:
                        dialog.dismiss();
                        break;
                }
            }
        };
        imgStar1.setOnClickListener(MyOnClickListener);
        imgStar2.setOnClickListener(MyOnClickListener);
        imgStar3.setOnClickListener(MyOnClickListener);
        imgStar4.setOnClickListener(MyOnClickListener);
        imgStar5.setOnClickListener(MyOnClickListener);
        btnOk.setOnClickListener(MyOnClickListener);
        btnNoThanks.setOnClickListener(MyOnClickListener);
        btncancel.setOnClickListener(MyOnClickListener);
    }


    /**
     * Generate Register or login Dialog
     * @param msg
     */
    public void generateRegisterOrLogInDialog(String msg , final LoginRegisterPopUpListener listener)
    {
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.dialog_register_login);
        dialog.show();

        Button btnNoThanks = (Button)dialog.findViewById(R.id.btnAlertBtnTitleNoThanks);
        Button btnRegister = (Button)dialog.findViewById(R.id.btnAlertBtnTitleRegister);
        Button btnLogin    = (Button)dialog.findViewById(R.id.btnAlertBtnTitleLogIn);
        TextView txtRegisterLogIn = (TextView)dialog.findViewById(R.id.txtAlertMsgRegisterLogIn);
        Button btnSignInWithGoogle = (Button) dialog.findViewById(R.id.btnSignInWithGoogle);

        transeletion.openConnection();
        btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
        btnRegister.setText(transeletion.getTranselationTextByTextIdentifier("alertBtnTitleRegister"));
        btnLogin.setText(transeletion.getTranselationTextByTextIdentifier("alertBtnLogin"));
        txtRegisterLogIn.setText(msg);
        transeletion.closeConnection();

        btnSignInWithGoogle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                MathFriendzyHelper.signInWithGoogle(context);
            }
        });

        btnNoThanks.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                listener.onNoThanks();
            }
        });

        btnRegister.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                listener.onRegister();
            }
        });

        btnLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                listener.onLogin();
            }
        });
    }
}
