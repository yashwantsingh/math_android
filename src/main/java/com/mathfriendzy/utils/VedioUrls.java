package com.mathfriendzy.utils;

public class VedioUrls {

	public static final String LEARNING_CENTER_VEDIO = "https://www.youtube.com/watch?v=G5mpa1H3Pkg";
	public static final String SINGLE_FRIENDZY_VEDIO = "https://www.youtube.com/watch?v=y3uVliszBqM";
	public static final String MULTI_FRIESNDZY_VEDIO = "https://www.youtube.com/watch?v=RmknLMlg-4s";
	
	
	//for school function
	public static final String SCHOO_FUNCTION_VEDIO = null;
	public static final String SCHOOL_ASSESSMENT_TEST_VEDIO = "https://www.youtube.com/watch?v=kzk2VdsnSWc";
	public static final String SCHOOL_SCHOOL_CHALLENGE_VEDIO = "https://www.youtube.com/watch?v=YOSqqVPJOPA";
	public static final String SCHOOL_HOMEWROK_QUIZZ_VEDIO = "https://www.youtube.com/watch?v=PhWkzrUsC9A";
	public static final String SCHOOL_STUDY_GROUP_VEDIO = null;
	public static final String SCHOOL_HELP_STUDENT_VEDIO = "https://www.youtube.com/watch?v=vUYFdK4RlGI";

	//for teacher function
	public static final String TEACHER_PRACTICE_REPORT_VEDIO = "https://www.youtube.com/watch?v=0Tn8ir2W6JY";
	public static final String TEACHER_ASSESSMENT_REPORT_VEDIO = "https://www.youtube.com/watch?v=8j7A0S1xcO8";
	public static final String TEACHER_ASSESSMENT_REPORT_BY_CLASS_VEDIO = "https://www.youtube.com/watch?v=8j7A0S1xcO8";
	public static final String TEACHER_ASSIGN_HW_QUIZZ_VEDIO = "https://www.youtube.com/watch?v=PFoNjkXtMU8";
	public static final String TEACHER_CHECK_HW_QUIZZ_VEDIO = "https://www.youtube.com/watch?v=SlOyEkHtru4";
	public static final String TEACHER_CREATE_CHALLENGE_VEDIO = "https://www.youtube.com/watch?v=pvPrby1xQxU";
	public static final String TEACHER_STUDENT_ACCOUNT_VEDIO = "https://www.youtube.com/watch?v=8a-dTnmkbNg";
    public static final String TEACHER_MANAGE_TUTOR_VEDIO = "https://www.youtube.com/watch?v=-Y4XMWz8Cs0";


    public static final String SCHOOL_HOMEWROK_QUIZZ_VEDIO_SF = "https://www.youtube.com/watch?v=0lVOlNic_sU";
    public static final String TEACHER_ASSIGN_HW_QUIZZ_VEDIO_SF = "https://www.youtube.com/watch?v=qSL47-cRcMM";
    public static final String TEACHER_CHECK_HW_QUIZZ_VEDIO_SF = "https://www.youtube.com/watch?v=P4-EWjTZWRM";
    public static final String TEACHER_STUDENT_ACCOUNT_VEDIO_SF = "https://www.youtube.com/watch?v=VdD30AxmGWI";
    public static final String TEACHER_MANAGE_TUTOR_VEDIO_SF = "https://www.youtube.com/watch?v=dek1G8Qv3os";
}
