package com.mathfriendzy.utils;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Session;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;
import com.google.android.gms.ads.AdListener;
import com.mathfriendzy.R;
import com.mathfriendzy.controller.inapp.GetMoreCoins;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.friendzy.FriendzyDTO;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.UpdatedInfoTransferObj;
import com.mathfriendzy.model.spanishchanges.SpanishChangesImpl;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.TEMP_PLAYER_OPERATION_FLAG;
import static com.mathfriendzy.utils.ITextIds.LIKE_URL;
import static com.mathfriendzy.utils.ITextIds.MF_HOMESCREEN;
import static com.mathfriendzy.utils.ITextIds.MSG_INVITE_1;
import static com.mathfriendzy.utils.ITextIds.MSG_INVITE_2;
import static com.mathfriendzy.utils.ITextIds.MSG_INVITE_3;
import static com.mathfriendzy.utils.ITextIds.MSG_INVITE_4;
import static com.mathfriendzy.utils.ITextIds.MSG_INVITE_5;

public class CommonUtils extends android.app.Application
{
    //for question downloaded
    public static String newversion     = "1.53";
    public static String oldversion     = "1.32";
    //end changes

    public static boolean isClickedSolveEquation = true;
    //only for newLearning center
    public static int selectedGrade = 1;
    //changes for open selected categories in New Learnign center
    public static int selectedIndexForOpenCategory = 0;
    public static int openCategoryId = 0;

    //changes for friendzy challenge
    public static boolean isActivePlayer = false;
    public static ArrayList<FriendzyDTO> activeFriendzyPlayerList = new ArrayList<FriendzyDTO>();
    //end for friendzy

    //Changes for Spanish
    //this flag is used when user click on language conversion , its only for one time click
    public static boolean isOneTimeClick = true;

    private static final String TAG = "class CommonUtils";
    private static final int RESPONCE_CODE 	= 404;
    public static String LANGUAGE_CODE 		= "EN";
    public static String LANGUAGE_ID 		= "1";
    public static String APP_ID 			= "6";

    //changes for Spanish
    public static final int ENGLISH = 1;
    public static final int SPANISH = 2;
    //end changes

    public static int MAX_ATTEMPT_FOR_PLAY_SAME_STANDARD = 4;//for new assessment

    //for ads
    private static WindowManager wm = null;
    private static View view        = null;
    private static final int MAX_SCREEN   = 5;//number of screen after add will display
    private static int  numberOfScreeCounter = 0;//number of screen visited
    private static int  numberOfAdsDisplayed = 0;//contains the how many ads are displayed
    private static WindowManager wmDialog    = null;//wm dialog which will diaplyed after max number of ads displayed
    private static View dialogView			 = null;
    private static final int MAX_NUMBER_DIALOG = 10;//this is the maximum number of dialog after which purchase dialog will display
    public static boolean LOG_ON = true;
    //for show timer
    public static boolean isShowTimer = false;
    public static boolean isActiveHomeSchoolNotificationDownloaded = false;

    /** This method read the data from url and return it into string form */
    public static String readFromURL(String strURL){
        CommonUtils.printLog(TAG , "Str url " + strURL);
        StringBuffer finalString = new StringBuffer();
        try{
            URL url = new URL(strURL);
            BufferedReader in = new BufferedReader(new
                    InputStreamReader(url.openStream()), 8*1024);
            String str = "";
            while ((str = in.readLine()) != null){
                finalString.append(str);
            }
            in.close();
        }catch (Exception e){
            Log.e(TAG, "Error while reading from url " + e.toString());
            return null;
            //System.exit(0);
        }
        return finalString.toString();
    }



    /**
     * This method read the data from url and return it into string form
     * @param nameValuePairs
     * @return
     */
    public static String readFromURL(ArrayList<NameValuePair> nameValuePairs){

        if(CommonUtils.LOG_ON){
            for(int i = 0 ; i < nameValuePairs.size() ; i ++ ){
                Log.e("requested param " , nameValuePairs.get(i) + "");
            }
        }

        InputStream inputStraem		= null;
        StringBuilder buffer 		= new StringBuilder("");

        try{
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(COMPLETE_URL);
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            inputStraem = entity.getContent();

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStraem,"iso-8859-1"),8);
            buffer = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null){
                buffer.append(line);
            }
            inputStraem.close();
        }catch (Exception e){
            Log.e(TAG, "Error while reading from url " + e.toString());
            return null;
            //System.exit(0);
        }
        return buffer.toString();
    }


    /**
     * This method return array object from bitmap object
     *
     * @param bitmap
     * @return
     */
    public static byte[] getByteFromBitmap(Bitmap bitmap) {
        final int MAX_BYTE = 100;// for max byte
        byte[] byteMap = null;
        if(bitmap != null)
        {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, MAX_BYTE, stream);
            byteMap = stream.toByteArray();
        }
        return byteMap;
    }


    /**
     * This method return bitmap from byte array
     *
     * @param byteImage
     * @return
     */
    public static Bitmap getBitmapFromByte(byte[] byteImage) {
        final int MIN_BYTE = 0;// for min byte
        Bitmap bmp = BitmapFactory.decodeByteArray(byteImage, MIN_BYTE,
                byteImage.length);
        return bmp;
    }

    /**
     * Check for url is valid or not
     *
     * @param urlString
     * @return
     * @throws IOException
     */
    public static boolean isValidUrl(String urlString) throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection urlconnection = (HttpURLConnection) url
                .openConnection();

        if (urlconnection.getResponseCode() != RESPONCE_CODE)
            return true;
        else
            return false;
    }

    /**
     * This method return the bitmap object from image url from server
     *
     * @param urlString
     * @return
     * @throws MalformedURLException
     * @throws IOException
     *//*
	public static Bitmap getBitmap(String urlString)
			throws MalformedURLException, IOException {
		if (CommonUtils.isValidUrl(urlString)) {
			InputStream inputStream = (InputStream) new URL(urlString)
			.getContent();
			Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
			return bitmap;
		} else
			return null;
	}*/

    /**
     * This method return the bitmap object from image url from server
     *
     * @param urlString
     * @return
     * @throws MalformedURLException
     * @throws IOException
     */
    public static Bitmap getBitmap(String urlString)
            throws MalformedURLException, IOException {
        if (CommonUtils.isValidUrl(urlString)) {
            InputStream inputStream = (InputStream) new URL(urlString).getContent();
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            return bitmap;
        } else{
            return null;
        }
    }

    /**
     * This method return the bitmap object from image url from server
     *
     * @param urlString
     * @return
     * @throws MalformedURLException
     * @throws IOException
     */
    public static Bitmap getBitmapWithOption(String urlString)
            throws MalformedURLException, IOException {
        if (CommonUtils.isValidUrl(urlString)) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                options.inMutable = true;
            InputStream inputStream = (InputStream) new URL(urlString).getContent();
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream , null , options);
            return bitmap;
        } else{
            return null;
        }
    }

    /**
     * getInputStream from given url
     * @param urlString
     * @return
     * @throws MalformedURLException
     * @throws IOException
     */
    public static InputStream getInputStream(String urlString)
            throws MalformedURLException, IOException{
        if (CommonUtils.isValidUrl(urlString)) {
            InputStream inputStream = (InputStream)
                    new URL(urlString).getContent();
            return inputStream;
        } else{
            return null;
        }
    }

    /**
     * Generate Progress Dialog
     *
     * @param context
     * @return
     */
    public static ProgressDialog getProgressDialog(Context context) {
        ProgressDialog pd = new ProgressDialog(context);
        pd.setIndeterminate(false);//back button does not work
        pd.setCancelable(false);
        //pd.setCanceledOnTouchOutside(false);
        //pd.setMessage("Please wait.");
        pd.setMessage("Please Wait, This could take several minutes.");
        //pd.setMessage("Please wait. We are downloading thousands of questions and images according to your grade level.");
        return pd;
    }

    /**
     * Generate Progress Dialog
     *
     * @param context
     * @return
     */
    public static ProgressDialog getProgressDialog(Context context , String msg , boolean isForDwonLoading) {
        Translation translation = new Translation(context);
        translation.openConnection();
        ProgressDialog pd = new ProgressDialog(context);
        pd.setIndeterminate(false);//back button does not work
        pd.setCancelable(false);
        if(isForDwonLoading)
            msg = msg + translation.getTranselationTextByTextIdentifier("lblThisCouldTakeTwoToTenMinutes");
        pd.setMessage(msg);
        translation.closeConnection();
        return pd;
    }

    /**
     * @description Perform Validation for email.
     * @param email
     * @return isValid
     */
    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        // String expression = "(?=.*[a-z])(?=.*\\d)+@(?=.*[a-z])(?=.*\\d)";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    /**
     * @description Perform Validation for Password.
     * @param password
     * @return isValid
     */
    public static boolean isPasswordValid(String password) {
        boolean isValid = false;

        String PASSWORD_PATTERN = "(?=.*[a-z])(?=.*\\d).{8,}";
        CharSequence inputStr = password;

        Pattern pattern = Pattern.compile(PASSWORD_PATTERN,
                Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    /**
     * Check for email already exist or not
     *
     * @param email
     * @return
     */
    public boolean isEmailAlreadyExist(String email) {
        String strURL = COMPLETE_URL + "action=checkEmailExistence&email="
                + email;
        String result = this.parseEmailResponceJson(CommonUtils.readFromURL(strURL));
        if( result != null){
            if (result.equals("1"))
                return true;
            else
                return false;
        }else{
            return false;
        }
    }

    /**
     * Parse json for email existence
     *
     * @param jsonString
     * @return
     */
    private String parseEmailResponceJson(String jsonString) {
        String result = null;
        try {
            JSONObject jObject = new JSONObject(jsonString);
            result = jObject.getString("response");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (TEMP_PLAYER_OPERATION_FLAG)
            Log.e("TempPlayerOperation", "outside getUserNameFromServer(");

        return result;
    }

    /**
     * Check for Internet connection
     *
     * @param context
     * @return
     */
    public static boolean isInternetConnectionAvailable(Context context) {
        // Log.e("Common", "inside");
        ConnectivityManager connectivityMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivityMgr.getActiveNetworkInfo() != null
                && connectivityMgr.getActiveNetworkInfo().isAvailable()
                && connectivityMgr.getActiveNetworkInfo().isConnected())
            return true;
        else
            return false;
    }

    /**
     * This method formate the date
     * @param date
     * @return
     */
    @SuppressLint("SimpleDateFormat")
    public static String formateDate(Date date) {
        String displayDate = "";
        if(date != null)
            displayDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
        return displayDate;
    }

    /**
     * This method formate the date
     * @param date
     * @return
     */
    @SuppressLint("SimpleDateFormat")
    public static String formateDateIn24Hours(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
        //return SimpleDateFormat.getTimeInstance(SimpleDateFormat.MEDIUM, Locale.UK).format(date);
        return df.format(date);
    }

    /**
     * This  method set the time zone
     * @param date
     * @return
     */
    public static Date setTimeZoneWithDate(Date date){
        date.setMinutes(date.getMinutes() + date.getTimezoneOffset());
        return date;
    }

    /**
     * Formate the integer to String
     * ex - 2000 to 2,000
     * @param str
     * @return
     */
    public static String setNumberString(String str)
    {
        double amount = Double.parseDouble(str);
        //DecimalFormat formatter = new DecimalFormat("#,##,##,###");
        DecimalFormat formatter = new DecimalFormat("###,###,###,###");
        return formatter.format(amount);
    }

    /**
     * This method save Bitmap
     * @param bitmap
     * @param dir
     * @param baseName
     * @return
     */
    public static String saveBitmap(Bitmap bitmap, String dir, String baseName)
    {
        try {
            File sdcard = Environment.getExternalStorageDirectory();
            File pictureDir = new File(sdcard, dir);
            pictureDir.mkdirs();
            File f = null;
            for (int i = 1; i < 200; ++i) {
                String name = baseName + i + ".jpg";
                f = new File(pictureDir, name);
                if (!f.exists()) {
                    break;
                }
            }
            if (!f.exists())
            {
                String name = f.getAbsolutePath();
                FileOutputStream fos = new FileOutputStream(name);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                fos.flush();
                fos.close();

                return name;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * This method show Internet not connected dialog
     * @param context
     */
    public static void showInternetDialog(Context context){
        DialogGenerator dg = new DialogGenerator(context);
        Translation transeletion = new Translation(context);
        transeletion.openConnection();
        dg.generateWarningDialog(transeletion.
                getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
        transeletion.closeConnection();
    }

    /**
     * This method show the coming soon dialog
     * @param context
     */
    public static void comingSoonPopUp(Context context){
        DialogGenerator dg = new DialogGenerator(context);
        Translation transeletion = new Translation(context);
        transeletion.openConnection();
        dg.generateWarningDialog(transeletion.
                getTranselationTextByTextIdentifier("lblComingSoon") + " !!!");
        transeletion.closeConnection();
    }

    /**
     * This method set the value in sharedPreff.
     * @param context
     */
    public static void setSharedPreferences(Context context)
    {
        SharedPreferences sheredPreference = context.getSharedPreferences("SHARED_FLAG", 0);
        SharedPreferences.Editor editor = sheredPreference.edit();
        editor.putBoolean("isGetLanguageFromServer", false);
        editor.putBoolean("isTranselation", false);
        editor.putString("oldversion", newversion);
        editor.commit();
    }

    /**
     * This method generate the feed dialog to post on Facebook
     * @param user
     * @param context
     * @param isFriend
     */
    public static void publishFeedDialog(final String user, final Context context, boolean isFriend)
    {
        Bundle params = new Bundle();
        params.putString("description",  getFacebookShareMsg(context));
        params.putString("picture", ITextIds.URL_ICON_IMG);
        params.putString("link", LIKE_URL);
        if(isFriend)
            params.putString("to", user);

        WebDialog feedDialog = (new WebDialog.FeedDialogBuilder(context,
                Session.getActiveSession(),params))
                .setOnCompleteListener(new OnCompleteListener() {

                    @Override
                    public void onComplete(Bundle values,FacebookException error)
                    {
                        if (error == null) {
                            // When the story is posted, echo the success
                            // and the post Id.
                            final String postId = values.getString("post_id");
                            if (postId != null)
                            {
                                DialogGenerator generator = new DialogGenerator(context);
                                Translation translate = new Translation(context);
                                translate.openConnection();
                                generator.generateWarningDialog(translate
                                        .getTranselationTextByTextIdentifier("alertMsgYourWallPostCompleted"));
                                translate.closeConnection();

                            } else {
                                // User clicked the Cancel button
                                Toast.makeText(context.getApplicationContext(),
                                        "Publish cancelled",
                                        Toast.LENGTH_SHORT).show();
                            }
                        } else if (error instanceof FacebookOperationCanceledException) {
                            // User clicked the "x" button
                            Toast.makeText(context.getApplicationContext(),
                                    "Publish cancelled",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            // Generic, ex: network error
                            Toast.makeText(context.getApplicationContext(),
                                    "Error posting story",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                })
                .build();
        feedDialog.show();
    }

    /**
     * use to make message for inviting friends
     * @return message
     */
    public static String getFacebookShareMsg(Context context)
    {

        SharedPreferences sharedPreffPlayerInfo = context.getSharedPreferences(ICommonUtils.PLAYER_INFO, 0);
        String playerUserName = sharedPreffPlayerInfo.getString("userName", "");
        Translation translate = new Translation(context);
        translate.openConnection();
        String text = null;

        text =  translate.getTranselationTextByTextIdentifier(MSG_INVITE_1)+
                " "+ MathFriendzyHelper.getAppRateUrl(context)+
                translate.getTranselationTextByTextIdentifier(MSG_INVITE_2)
                + " '"  + translate.getTranselationTextByTextIdentifier(MF_HOMESCREEN)+ "'"
                + " " + translate.getTranselationTextByTextIdentifier(MSG_INVITE_3)
                + " '" + playerUserName + "'."
                + " " + translate.getTranselationTextByTextIdentifier(MSG_INVITE_4)
                +"\n"+ translate.getTranselationTextByTextIdentifier(MSG_INVITE_5);

        translate.closeConnection();
        return text;
    }

    /**
     * This method check the data for word problem
     * This method return true if date is changed other wise false
     * @param dateFromDatabase
     * @param grade
     * @param arrayList
     * @return
     */
    public static boolean isUpdateDateChange(String dateFromDatabase , int grade,
                                             ArrayList<UpdatedInfoTransferObj> arrayList){
        try {
            if (grade > 8)
                grade = 8;
            if (CommonUtils.LOG_ON)
                Log.e(TAG, "from database " + dateFromDatabase);
            boolean isUpdatedDateChange = false;
            for (int i = 0; i < arrayList.size(); i++) {
                if (CommonUtils.LOG_ON)
                    Log.e(TAG, "date from server " + arrayList.get(i).getUpdatedDate());
                if (grade == arrayList.get(i).getGrade() &&
                        !dateFromDatabase.equals(arrayList.get(i).getUpdatedDate())) {
                    isUpdatedDateChange = true;
                    break;
                }
            }
            return isUpdatedDateChange;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * This method return the server date
     * @param grade
     * @param arrayList
     * @return
     */
    public static String getServerDate(int grade, ArrayList<UpdatedInfoTransferObj> arrayList){
        if(grade > 8)
            grade = 8;
        String serverDate = "";
        for( int i = 0 ; i < arrayList.size() ; i ++ ){
            if(grade == arrayList.get(i).getGrade()){
                serverDate  = arrayList.get(i).getUpdatedDate();
                break;
            }
        }
        return serverDate;
    }

    //added for friendzy challenge
    /**
     * This method check for active player for friendzy challenge
     * @param userId
     * @param playerId
     * @return
     */
    public static boolean isActivePlayer(String userId , String playerId){
        for(int i = 0 ; i < activeFriendzyPlayerList.size() ; i ++ ){
            if(activeFriendzyPlayerList.get(i).getPid().equals(playerId)){
                if(DateTimeOperation.getDuration(activeFriendzyPlayerList.get(i).getEndDate()) >= 0){
                    isActivePlayer = true;
                    return true ;
                }else{
                    isActivePlayer = false;
                    activeFriendzyPlayerList.remove(i);
                    return false;
                }
            }
        }
        isActivePlayer = false;
        return false;
    }

    /**
     * This method return the active player challengerId
     * @param userId
     * @param playerId
     * @return
     */
    public static String getActivePlayerChallengerId(String userId , String playerId){
        for(int i = 0 ; i < activeFriendzyPlayerList.size() ; i ++ ){
            if(activeFriendzyPlayerList.get(i).getPid().equals(playerId)){
                return activeFriendzyPlayerList.get(i).getChallengerId();
            }
        }
        return "";
    }

    /**
     * This method check for player active or not friendzy challenge
     * @param challengerId
     * @param playerId
     * @return
     */
    public static boolean isChallengerActiveOrNot(String challengerId , String playerId){
        for(int i = 0 ; i < activeFriendzyPlayerList.size() ; i ++ ){
            if(activeFriendzyPlayerList.get(i).getPid().equals(playerId)
                    && activeFriendzyPlayerList.get(i).getChallengerId().equals(challengerId))
                return true;
        }
        return false;
    }

    /**
     * This method update the new end date for active player
     * @param playerId
     */
    public static void updateEndDateForFriendzyChallenge(String playerId , String endDate , String challengerId){
        for(int i = 0 ; i < activeFriendzyPlayerList.size() ; i ++ ){
            if(activeFriendzyPlayerList.get(i).getPid().equals(playerId)
                    && activeFriendzyPlayerList.get(i).getChallengerId().equals(challengerId))
            {
                activeFriendzyPlayerList.get(i).setEndDate(endDate);
            }
        }
    }

    /**
     * This method retrun the equation for playing when not is not connected
     * @param playerId
     * @param challengerId
     * @return
     */
    public static String getEquationIfInternetNotConnectedForActivePlayer(String playerId , String challengerId){
        for(int i = 0 ; i < activeFriendzyPlayerList.size() ; i ++ ){
            if(activeFriendzyPlayerList.get(i).getPid().equals(playerId)
                    && activeFriendzyPlayerList.get(i).getChallengerId().equals(challengerId))
                return activeFriendzyPlayerList.get(i).getEquations();
        }
        return "";
    }

    /**
     * This method deactivate the challenger
     * @param challengerId
     * @param playerId
     */
    public static void deActivatePlayer(String challengerId , String playerId){
        for(int i = 0 ; i < activeFriendzyPlayerList.size() ; i ++ ){
            if(activeFriendzyPlayerList.get(i).getPid().equals(playerId)
                    && activeFriendzyPlayerList.get(i).getChallengerId().equals(challengerId))
                activeFriendzyPlayerList.remove(i);
        }
    }

    /**
     * This method remove the challenge from the list
     * when user accept the another challenge
     * @param playerId
     */
    public static void remeoveChallenge(String playerId){
        for(int i = 0 ; i < activeFriendzyPlayerList.size() ; i ++ ){
            if(activeFriendzyPlayerList.get(i).getPid().equals(playerId))
                activeFriendzyPlayerList.remove(i);
        }
    }

    //added for assessmentTest
    /**
     * This method return the diff in day
     * @return
     */
    @SuppressLint("SimpleDateFormat")
    public static DateObj getDateTimeDiff(Date playDate , Date currentData )
    {
        DateObj dateObj = new DateObj();

        int diffInDays 		= 0 ;
        int diffInHours     = 0;
        int diffInMinuts    = 0;
        int diffInSeconds	= 0;

        if(playDate.compareTo(currentData) >= 0){
            diffInDays 		= (int)( (playDate.getTime()
                    - currentData.getTime()) / (1000 * 60 * 60 * 24) );
            diffInHours 	= (int)( (playDate.getTime()
                    - currentData.getTime()) / (1000 * 60 * 60 ) )
                    - 24 * diffInDays;
            diffInMinuts 	= (int)( (playDate.getTime() - currentData.getTime()) / (1000  * 60 ) )
                    - (diffInHours + 24 * diffInDays) * 60;
            diffInSeconds 	= (int)( (playDate.getTime() - currentData.getTime()) / (1000))
                    - (diffInMinuts + (diffInHours + 24 * diffInDays) * 60) * 60;

            dateObj.setDiffInDay(diffInDays);
            dateObj.setDiffInHours(diffInHours);
            dateObj.setDiffInMin(diffInMinuts);
            dateObj.setDiffInSec(diffInSeconds);
        }else{
            dateObj.setDiffInDay(-1);
        }

        return dateObj;
    }

    //Spanish changes
    /**
     * This method check for newly created table if not exist then create , at the time starting of the app.
     * @param context
     */
    public static void CheckWordProblemSpanishTable(Context context){
        SpanishChangesImpl spanish = new SpanishChangesImpl(context);
        spanish.openConn();
        if(spanish.isWordProblemsQuestionsSpanishExist() == false)
            spanish.createWordProblemsQuestionsSpanish();
        if(spanish.isWordProblemsUpdateDateDetailSpanishExist() == false)
            spanish.createWordProblemUpdateDetailsSpanishTable();
        if(spanish.isWordProblemCategoriesSpanishTableExist() == false)
            spanish.createWordProblemCategoriesSpanish();
        if(spanish.isWordProblemsSubCategoriesSpanish() == false)
            spanish.createWordProblemsSubCategoriesSpanish();
        if(spanish.isWordAssessmentStandards() == false)
            spanish.createWordAssessmentStandards();
        if(spanish.isWordAssessmentSubCategoriesInfo() == false)
            spanish.createWordAssessmentSubCategoriesInfo();
        if(spanish.isWordAssessmentSubStandards() == false)
            spanish.createWordAssessmentSubStandards();

        //for New Assessment Changes
        if(spanish.isNewWordAssessmentTestResult() == false)
            spanish.createNewWordAssessmentTestResult();
        if(spanish.isWordAssessmentStandardsRoundInfo() == false)
            spanish.createWordAssessmentStandardsRoundInfo();

        //for upload home work images
        if(spanish.isHomeWorkImagesTableExist() == false)
            spanish.createHomeWorkImageTable();

        if(spanish.isLocalEarnedScoreTableExist() == false)
            spanish.createLocalEarnedScoreTable();

        //for homework offline
        if(spanish.isHomeworkTableExist() == false)
            spanish.createHomeworkTable();

        if(spanish.isPracticeSkillHWExist() == false)
            spanish.createPracticeSkillHWTable();
        if(spanish.isWordProblemHWExist() == false)
            spanish.createWordProblemHWTable();
        if(spanish.isCustomeHWExist() == false)
            spanish.createCustomeHWTable();

        if(spanish.iUserLinksExist() == false)
            spanish.createUserLinksTable();

        spanish.closeConn();
    }


    /**
     * This method return user language code
     * @param context
     * @return
     *//*
	public static int getUserLanguageCode(Context context){
		UserRegistrationOperation userObj = new UserRegistrationOperation(context);
		RegistereUserDto userPlayerObj = userObj.getUserData();
		if(userPlayerObj.getPreferedLanguageId().equals("1")){
			return 1;//for English
		}else if(userPlayerObj.getPreferedLanguageId().equals("2")){
			return 2; // for Spanish
		}
		return 1;//default
	}*/

    /**
     * This method return user language code
     * @param context
     * @return
     */
    public static int getUserLanguageCode(Context context){
        //for login player
        SharedPreferences sheredPreference = context.getSharedPreferences(LOGIN_SHARED_PREFF, 0);
        if(sheredPreference.getBoolean(IS_LOGIN, false)){
            UserRegistrationOperation userObj = new UserRegistrationOperation(context);
            String userLanguage = userObj.getUserLanguage();
            if(userLanguage.equals("1"))
                return 1;//for English
            else if((userLanguage.equals("2")))
                return 2;//for Spanish
            return 1;//default
        }else{//for temp player
            return 1;
        }
    }

    /**
     * This method return user language code
     * @param context
     * @return
     */
    public static String getUserVolume(Context context){
        //for login player
        SharedPreferences sheredPreference = context.getSharedPreferences(LOGIN_SHARED_PREFF, 0);
        if(sheredPreference.getBoolean(IS_LOGIN, false)){
            UserRegistrationOperation userObj = new UserRegistrationOperation(context);
            String userVolume = userObj.getUserVolume();
            return userVolume;
        }else{//for temp player
            return "";
        }
    }

    /**
     * This metho return the userId
     * @param context
     * @return
     */
    public static String getUserId(Context context){
        //for login player
        SharedPreferences sheredPreference = context.getSharedPreferences(LOGIN_SHARED_PREFF, 0);
        if(sheredPreference.getBoolean(IS_LOGIN, false)){
            UserRegistrationOperation userObj = new UserRegistrationOperation(context);
            String userId = userObj.getUserId();
            return userId;
        }else{//for temp player
            return "";
        }
    }

    /**
     * Return the login user data
     * @param context
     * @return
     */
    public static RegistereUserDto getLoginUser(Context context){
        SharedPreferences sheredPreference = context.getSharedPreferences(LOGIN_SHARED_PREFF, 0);
        if(sheredPreference.getBoolean(IS_LOGIN, false)){
            UserRegistrationOperation userObj = new UserRegistrationOperation(context);
            RegistereUserDto loginUser = userObj.getUserData();
            return loginUser;
        }else{//for temp player
            return null;
        }
    }

    //for assessment play date test
    /**
     * This method check for play date is valid or not
     * @param playDate
     * @return
     */
    @SuppressLint("SimpleDateFormat")
    @SuppressWarnings("deprecation")
    public static boolean isValidplayDate(String playDate , Context context) {

        //playDate = "2013-11-11 15:58:44";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date lastPlayDate = null;
        try {
            lastPlayDate = sdf.parse(playDate);
            //set next Date
            Calendar cal = Calendar.getInstance(Locale.getDefault());

            // original
            cal.set(lastPlayDate.getYear() + 1900, lastPlayDate.getMonth(), lastPlayDate.getDate()
                    ,lastPlayDate.getHours(),lastPlayDate.getMinutes(),lastPlayDate.getSeconds());
            cal.add(Calendar.DATE, 7);

			/*//for testing
			cal.set(lastPlayDate.getYear() + 1900, lastPlayDate.getMonth(), lastPlayDate.getDate()
					,lastPlayDate.getHours(), lastPlayDate.getMinutes() + 30 , lastPlayDate.getSeconds());
			//end testing
			 */

            DateObj dateObj = CommonUtils.getDateTimeDiff(cal.getTime(), new Date());

            if(dateObj.getDiffInDay() < 0){
                return true;
            }else{
                DialogGenerator dg = new DialogGenerator(context);
                Translation transeletion = new Translation(context);
                transeletion.openConnection();
                dg.generateWarningDialogForEmptyCoisTransfer(transeletion.
                        getTranselationTextByTextIdentifier("lblYouCanTakeThisTest").replace("___", "7")
                        + "\n\n"+ dateObj.getDiffInDay() + " " +
                        transeletion.getTranselationTextByTextIdentifier("lblDays")
                        + ", " + dateObj.getDiffInHours() + " " + transeletion.
                        getTranselationTextByTextIdentifier("lblHours")	+ " & "
                        + dateObj.getDiffInMin() + " " + transeletion.
                        getTranselationTextByTextIdentifier("lblMinutes"));
                transeletion.closeConnection();
            }
            return false;
        } catch (ParseException e) {
            Log.e("AssessmentClass", "inside isValidplayDate Error while parsing Date " + e.toString());
            return true;
        }
    }

    /**
     * This method show the dialog after registeration added for new changes
     */
    public static void showDialogAfterRegistrationSuccess(Context context){
        Translation transeletion = new Translation(context);
        transeletion.openConnection();
        DialogGenerator dg = new DialogGenerator(context);
        dg.generateDailogAfterRegistration(getUpdatedTextMessage(transeletion.
                getTranselationTextByTextIdentifier("lblTheLastStepOfRegistrationIsToConfirm")));
        transeletion.closeConnection();
    }

    private static String getUpdatedTextMessage(String message){
        if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS){
            return message.replace("MathFriendzy.com" , "SchoolFriendzy.com");
        }
        return message;
    }

    /**
     * This method set the done button in keyboard for edit text
     * @param edtText
     */
    public static void setDoneButtonToEditText(EditText edtText){
        try {
            edtText.setImeOptions(EditorInfo.IME_ACTION_DONE);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * This method return the array list from string with comma
     * @param stringWithComma
     * @return
     */
    public static ArrayList<Integer> getArrayListFromStringWithCommas(String stringWithComma){
        String value = "";
        ArrayList<Integer> arrayList = new ArrayList<Integer>();

        if(stringWithComma.length() > 0){
            stringWithComma = stringWithComma + ",";
            for(int i = 0 ; i < stringWithComma.length() ; i ++ ){
                if(stringWithComma.charAt(i) == ',' )
                {
                    arrayList.add(Integer.parseInt(value));
                    value = "";
                }
                else
                {
                    value = value +  stringWithComma.charAt(i);
                }
            }
        }
        return arrayList;
    }

    /**
     * This method check the application status
     * @param context - current activity object
     * @param userId  - userId
     * @param itemId  - itemId
     * @return - 1 for unlock status and 0 for unlock status
     */
    public static int getApplicationStatus(Context context , String userId , int itemId){
        int appStatus = 0;
        LearningCenterimpl learnignCenterImpl = new LearningCenterimpl(context);
        learnignCenterImpl.openConn();
        appStatus = learnignCenterImpl.getAppUnlockStatus(itemId,userId);
        learnignCenterImpl.closeConn();
        return appStatus;
    }

	/*
	 * code for showing ads 
	 */

    /**
     * This method for showing add dialog
     * @param context
     */
    public static void showAdDialog(final Context context){
        /*SharedPreferences loginPreff = context.getSharedPreferences(ICommonUtils.LOGIN_SHARED_PREFF, 0);
        if(loginPreff.getBoolean(IS_LOGIN, false)){//check for login
        //if(false){//stop ads in this verison
            SharedPreferences sharedPreff = context.getSharedPreferences
                    (ICommonUtils.ADS_FREQUENCIES_PREFF, 0);
            UserRegistrationOperation userObj = new UserRegistrationOperation(context);
            String userId = userObj.getUserId();

            if(CommonUtils.getApplicationStatus(context, userId, 100) != 1
                    && sharedPreff.getInt(ICommonUtils.ADS_IS_ADS_DISABLE, 0) != 1){//for locked status 1 for unlock
            //if(true){//for testing , also changes admob id in string for client
                try{
                    CommonUtils.numberOfScreeCounter ++;
                    if(CommonUtils.numberOfScreeCounter ==
                            sharedPreff.getInt(ICommonUtils.ADS_FREQUENCIES, 0)){
                    //if(true){//for testing , also changes admob id in string for client
                        LayoutInflater inflater = (LayoutInflater)context
                                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        view = inflater.inflate(R.layout.add_dialog, null);

                        AdView adView   = (AdView) view.findViewById(R.id.ad);
                        Button btnClose = (Button) view.findViewById(R.id.btnCloseAd);

                        adView.loadAd(new AdRequest.Builder().build());
                        adView.setAdListener(new MyAdListener());

                        btnClose.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(wm != null && view != null){
                                    wm.removeView(view);
                                    CommonUtils.numberOfScreeCounter = 0;
                                }

                                //for showing purchase dialog after max ads to be displayed
                                if(CommonUtils.numberOfAdsDisplayed ==
                                        CommonUtils.MAX_NUMBER_DIALOG){
                                    showDialogAfterMaxAdsDisplayed(context);
                                }
                            }
                        });
                    }
                }catch(Exception e){
                    e.printStackTrace();
                    Log.e(TAG, "inside showAdDialog : Eorror while showing dialog");
                    CommonUtils.numberOfScreeCounter = 0;
                }
            }
        }*/
    }

    /**
     * This method check the frequncy date and compare it with current date if
     * its diff is >= 10 days then ready to get new frequency from server
     * @return - true for getting new frequency and false for not
     */
    @SuppressLint("SimpleDateFormat")
    public static boolean isReadyToGetNewFrequesncyFromSerevr(Context context){
        SharedPreferences sharedPreff =
                context.getSharedPreferences(ICommonUtils.ADS_FREQUENCIES_PREFF, 0);
        if(sharedPreff.getInt(ICommonUtils.ADS_FREQUENCIES, 0) == 0){
            return true;
        }else{
            SimpleDateFormat sdf 	= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date lastFrequencyDate 	= null;
            try {
                lastFrequencyDate = sdf.parse(sharedPreff
                        .getString(ICommonUtils.ADS_FREQUENCIES_DATE, ""));
                DateObj dateObj = CommonUtils.getDateTimeDiff(new Date() , lastFrequencyDate);
                if(dateObj.getDiffInDay() >= 1){
                    return true;
                }else{
                    return false;
                }
            } catch (ParseException e) {
                Log.e(TAG, "inside isReadyToGetNewFrequesncyFromSerevr " +
                        "Error while parsing Date " + e.toString());
                return true;
            }
        }
    }

    /**
     * This is the dialog listener which is load the ad on dialo
     * @author Yashwant Singh
     *
     */
    private static class MyAdListener extends AdListener  {

        MyAdListener(){

        }

		/*@Override
		public void onDismissScreen(Ad ad) {
			//Log.e("", "onDismissScreen");
		}

		@Override
		public void onFailedToReceiveAd(Ad ad, ErrorCode errorCode) {
			//Log.e("", "onFailedToReceiveAd");
			CommonUtils.numberOfScreeCounter --;
		}

		@Override
		public void onLeaveApplication(Ad ad) {
			//Log.e("", "onLeaveApplication");
			if(wm != null && view != null){
				wm.removeView(view);
				CommonUtils.numberOfScreeCounter = 0;
			}
		}

		@Override
		public void onPresentScreen(Ad ad) {
			//Log.e("", "onPresentScreen");
		}

		@Override
		public void onReceiveAd(Ad ad) {
			try{
				//Log.e("", "Ad Loaded");
				showWindowManagerDialog();
			}catch (Exception e) {
				Log.e(TAG, e.toString());
			}
		}*/

        //changes for new API
        @Override
        public void onAdClosed() {
            //Log.e(TAG , "Ad onAdClosed");
            super.onAdClosed();
        }

        @Override
        public void onAdFailedToLoad(int errorCode) {
            //Log.e(TAG , "Ad onAdFailedToLoad");
            CommonUtils.numberOfScreeCounter --;
            super.onAdFailedToLoad(errorCode);
        }

        @Override
        public void onAdLeftApplication() {
            //Log.e(TAG , "Ad onAdLeftApplication");
            if(wm != null && view != null){
                wm.removeView(view);
                CommonUtils.numberOfScreeCounter = 0;
            }
            super.onAdLeftApplication();
        }

        @Override
        public void onAdLoaded() {
            //Log.e(TAG , "Ad onAdLoaded");
            showWindowManagerDialog();
            super.onAdLoaded();
        }

        @Override
        public void onAdOpened() {
            super.onAdOpened();
        }
    }

    /**
     * This method show the ad dialog on window manager
     */
    private static void showWindowManagerDialog(){
        try{
            WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    //WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
                    0,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                            | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                    PixelFormat.TRANSLUCENT);
            params.gravity = Gravity.CENTER;
            wm = (WindowManager) MyApplication.getAppContext().getSystemService(WINDOW_SERVICE);
            wm.addView(view, params);
            //for number of ads displayed
            CommonUtils.numberOfAdsDisplayed ++ ;
        }catch(Exception e){
            e.printStackTrace();
            Log.e(TAG, "inside showWindowManagerDialog : Eorror while showing dialog 1");
            CommonUtils.numberOfScreeCounter = 0;
        }
    }

    /**
     * This method call after the maximun dialog are diaplayes , max_dialog = 10
     * @param context
     */
    private static void showDialogAfterMaxAdsDisplayed(final Context context){
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        dialogView = inflater.inflate(R.layout.dialog_click_on_proceed_for_coins, null);

        TextView txtMsg  = (TextView) dialogView.findViewById(R.id.txtYouneedMoreCoins);
        Button btnCancel = (Button) dialogView.findViewById(R.id.btnNoThanks);
        Button btnOk     = (Button) dialogView.findViewById(R.id.btnYes);

        Translation transeletion = new Translation(context);
        transeletion.openConnection();
        txtMsg.setText(transeletion.getTranselationTextByTextIdentifier("lblMakeAppAdsFree"));
        btnCancel.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleCancel"));
        btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
        transeletion.closeConnection();

        btnCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(wmDialog != null && dialogView != null){
                    wmDialog.removeView(dialogView);
                    CommonUtils.numberOfAdsDisplayed = 0;
                }
            }
        });

        btnOk.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(wmDialog != null && dialogView != null){
                    wmDialog.removeView(dialogView);
                    CommonUtils.numberOfAdsDisplayed = 0;
                    Intent intent = new Intent(context , GetMoreCoins.class);
                    intent.putExtra("isShowScrennAfterAds", true);
                    context.startActivity(intent);
                }
            }
        });

        showWindowManagerDialogAfterMAxAdsDialogDisplayed();
    }

    /**
     * This method show the ad dialog on window manager
     */
    private static void showWindowManagerDialogAfterMAxAdsDialogDisplayed(){

        try{
            WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    //WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
                    0,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                            | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                    PixelFormat.TRANSLUCENT);
            params.gravity = Gravity.CENTER;
            wmDialog = (WindowManager) MyApplication.getAppContext().getSystemService(WINDOW_SERVICE);
            wmDialog.addView(dialogView, params);
        }catch(Exception e){
            e.printStackTrace();
            Log.e(TAG, "inside showWindowManagerDialog : Eorror while showing dialog ");
        }
    }
	/*
	 * End code for showing ads
	 */

    /**
     * Return the Character List
     * @return
     */
    public static ArrayList<String> getCharacterList(){
        ArrayList<String> characterlist = new ArrayList<String>();
        for(int i = 0 ; i < 26 ; i ++ ){
            characterlist.add(String.valueOf(Character.toChars( i + 65)));
        }
        return characterlist;
    }


    /**
     * Return the englist tutorial image list
     * @return
     */
    public static ArrayList<Integer> getEnglistTutorialIdList(){
        ArrayList<Integer> tutorialImageList = new ArrayList<Integer>();
        tutorialImageList.add(R.drawable.welcome_ipad);
        tutorialImageList.add(R.drawable.login_ipad);
        tutorialImageList.add(R.drawable.student_functions_ipad);
        tutorialImageList.add(R.drawable.teacher_functions_ipad);
        tutorialImageList.add(R.drawable.learning_center_ipad);
        tutorialImageList.add(R.drawable.single_friendzy_ipad);
        tutorialImageList.add(R.drawable.multi_friendzy_ipad);
        tutorialImageList.add(R.drawable.help_ipad);
        tutorialImageList.add(R.drawable.rate_ipad);
        return tutorialImageList;
    }

    /**
     * Return thr profanityword list
     * @return
     */
    public static ArrayList<String> getProfanityWordList(){
        ArrayList<String> profanityWordList = new ArrayList<String>();
        profanityWordList.add("4r5e");
        profanityWordList.add("5h1t");
        profanityWordList.add("5hit");
        profanityWordList.add("a55");
        profanityWordList.add("anal");
        profanityWordList.add("anus");
        profanityWordList.add("ar5e");
        profanityWordList.add("arrse");
        profanityWordList.add("arse");
        profanityWordList.add("ass");
        profanityWordList.add("ass-fucker");
        profanityWordList.add("asses");
        profanityWordList.add("assfucker");
        profanityWordList.add("assfukka");
        profanityWordList.add("asshole");
        profanityWordList.add("assholes");
        profanityWordList.add("asswhole");
        profanityWordList.add("a_s_s");
        profanityWordList.add("b!tch");
        profanityWordList.add("b00bs");
        profanityWordList.add("b17ch");
        profanityWordList.add("b1tch");
        profanityWordList.add("ballbag");
        profanityWordList.add("balls");
        profanityWordList.add("ballsack");
        profanityWordList.add("bastard");
        profanityWordList.add("beastial");
        profanityWordList.add("beastiality");
        profanityWordList.add("bellend");
        profanityWordList.add("bestial");
        profanityWordList.add("bestiality");
        profanityWordList.add("bi+ch");
        profanityWordList.add("biatch");
        profanityWordList.add("bitch");
        profanityWordList.add("bitcher");
        profanityWordList.add("bitchers");
        profanityWordList.add("bitches");
        profanityWordList.add("bitchin");
        profanityWordList.add("bitching");
        profanityWordList.add("bloody");
        profanityWordList.add("blow job");
        profanityWordList.add("blowjob");
        profanityWordList.add("blowjobs");
        profanityWordList.add("boiolas");
        profanityWordList.add("bollock");
        profanityWordList.add("bollok");
        profanityWordList.add("boner");
        profanityWordList.add("boob");
        profanityWordList.add("boobs");
        profanityWordList.add("booobs");
        profanityWordList.add("boooobs");
        profanityWordList.add("booooobs");
        profanityWordList.add("booooooobs");
        profanityWordList.add("breasts");
        profanityWordList.add("buceta");
        profanityWordList.add("bugger");
        profanityWordList.add("bum");
        profanityWordList.add("bunny fucker");
        profanityWordList.add("butt");
        profanityWordList.add("butthole");
        profanityWordList.add("buttmuch");
        profanityWordList.add("buttplug");
        profanityWordList.add("c0ck");
        profanityWordList.add("c0cksucker");
        profanityWordList.add("carpet muncher");
        profanityWordList.add("cawk");
        profanityWordList.add("chink");
        profanityWordList.add("cipa");
        profanityWordList.add("cl1t");
        profanityWordList.add("clit");
        profanityWordList.add("clitoris");
        profanityWordList.add("clits");
        profanityWordList.add("cnut");
        profanityWordList.add("cock");
        profanityWordList.add("cock-sucker");
        profanityWordList.add("cockface");
        profanityWordList.add("cockhead");
        profanityWordList.add("cockmunch");
        profanityWordList.add("cockmuncher");
        profanityWordList.add("cocks");
        profanityWordList.add("cocksuck");
        profanityWordList.add("cocksucked");
        profanityWordList.add("cocksucker");
        profanityWordList.add("cocksucking");
        profanityWordList.add("cocksucks");
        profanityWordList.add("cocksuka");
        profanityWordList.add("cocksukka");
        profanityWordList.add("cok");
        profanityWordList.add("cokmuncher");
        profanityWordList.add("coksucka");
        profanityWordList.add("coon");
        profanityWordList.add("cox");
        profanityWordList.add("crap");
        profanityWordList.add("cum");
        profanityWordList.add("cummer");
        profanityWordList.add("cumming");
        profanityWordList.add("cums");
        profanityWordList.add("cumshot");
        profanityWordList.add("cunilingus");
        profanityWordList.add("cunillingus");
        profanityWordList.add("cunnilingus");
        profanityWordList.add("cunt");
        profanityWordList.add("cuntlick");
        profanityWordList.add("cuntlicker");
        profanityWordList.add("cuntlicking");
        profanityWordList.add("cunts");
        profanityWordList.add("cyalis");
        profanityWordList.add("cyberfuc");
        profanityWordList.add("cyberfuck");
        profanityWordList.add("cyberfucked");
        profanityWordList.add("cyberfucker");
        profanityWordList.add("cyberfuckers");
        profanityWordList.add("cyberfucking");
        profanityWordList.add("d1ck");
        profanityWordList.add("damn");
        profanityWordList.add("dick");
        profanityWordList.add("dickhead");
        profanityWordList.add("dildo");
        profanityWordList.add("dildos");
        profanityWordList.add("dink");
        profanityWordList.add("dinks");
        profanityWordList.add("dirsa");
        profanityWordList.add("dlck");
        profanityWordList.add("dog-fucker");
        profanityWordList.add("doggin");
        profanityWordList.add("dogging");
        profanityWordList.add("donkeyribber");
        profanityWordList.add("doosh");
        profanityWordList.add("duche");
        profanityWordList.add("dyke");
        profanityWordList.add("ejaculate");
        profanityWordList.add("ejaculated");
        profanityWordList.add("ejaculates");
        profanityWordList.add("ejaculating");
        profanityWordList.add("ejaculatings");
        profanityWordList.add("ejaculation");
        profanityWordList.add("ejakulate");
        profanityWordList.add("f u c k");
        profanityWordList.add("f u c k e r");
        profanityWordList.add("f4nny");
        profanityWordList.add("fag");
        profanityWordList.add("fagging");
        profanityWordList.add("faggitt");
        profanityWordList.add("faggot");
        profanityWordList.add("faggs");
        profanityWordList.add("fagot");
        profanityWordList.add("fagots");
        profanityWordList.add("fags");
        profanityWordList.add("fanny");
        profanityWordList.add("fannyflaps");
        profanityWordList.add("fannyfucker");
        profanityWordList.add("fanyy");
        profanityWordList.add("fatass");
        profanityWordList.add("fcuk");
        profanityWordList.add("fcuker");
        profanityWordList.add("fcuking");
        profanityWordList.add("feck");
        profanityWordList.add("fecker");
        profanityWordList.add("felching");
        profanityWordList.add("fellate");
        profanityWordList.add("fellatio");
        profanityWordList.add("fingerfuck");
        profanityWordList.add("fingerfucked");
        profanityWordList.add("fingerfucker");
        profanityWordList.add("fingerfuckers");
        profanityWordList.add("fingerfucking");
        profanityWordList.add("fingerfucks");
        profanityWordList.add("fistfuck");
        profanityWordList.add("fistfucked");
        profanityWordList.add("fistfucker");
        profanityWordList.add("fistfuckers");
        profanityWordList.add("fistfucking");
        profanityWordList.add("fistfuckings");
        profanityWordList.add("fistfucks");
        profanityWordList.add("flange");
        profanityWordList.add("fook");
        profanityWordList.add("fooker");
        profanityWordList.add("fuck");
        profanityWordList.add("fucka");
        profanityWordList.add("fucked");
        profanityWordList.add("fucker");
        profanityWordList.add("fuckers");
        profanityWordList.add("fuckhead");
        profanityWordList.add("fuckheads");
        profanityWordList.add("fuckin");
        profanityWordList.add("fucking");
        profanityWordList.add("fuckings");
        profanityWordList.add("fuckingshitmotherfucker");
        profanityWordList.add("fuckme");
        profanityWordList.add("fucks");
        profanityWordList.add("fuckwhit");
        profanityWordList.add("fuckwit");
        profanityWordList.add("fudge packer");
        profanityWordList.add("fudgepacker");
        profanityWordList.add("fuk");
        profanityWordList.add("fuker");
        profanityWordList.add("fukker");
        profanityWordList.add("fukkin");
        profanityWordList.add("fuks");
        profanityWordList.add("fukwhit");
        profanityWordList.add("fukwit");
        profanityWordList.add("fux");
        profanityWordList.add("fux0r");
        profanityWordList.add("f_u_c_k");
        profanityWordList.add("gangbang");
        profanityWordList.add("gangbanged");
        profanityWordList.add("gangbangs");
        profanityWordList.add("gaylord");
        profanityWordList.add("gaysex");
        profanityWordList.add("goatse");
        profanityWordList.add("God");
        profanityWordList.add("god-dam");
        profanityWordList.add("god-damned");
        profanityWordList.add("goddamn");
        profanityWordList.add("goddamned");
        profanityWordList.add("hardcoresex");
        profanityWordList.add("hell");
        profanityWordList.add("heshe");
        profanityWordList.add("hoar");
        profanityWordList.add("hoare");
        profanityWordList.add("hoer");
        profanityWordList.add("homo");
        profanityWordList.add("hore");
        profanityWordList.add("horniest");
        profanityWordList.add("horny");
        profanityWordList.add("hotsex");
        profanityWordList.add("jack-off");
        profanityWordList.add("jackoff");
        profanityWordList.add("jap");
        profanityWordList.add("jerk-off");
        profanityWordList.add("jism");
        profanityWordList.add("jiz");
        profanityWordList.add("jizm");
        profanityWordList.add("jizz");
        profanityWordList.add("kawk");
        profanityWordList.add("knob");
        profanityWordList.add("knobead");
        profanityWordList.add("knobed");
        profanityWordList.add("knobend");
        profanityWordList.add("knobhead");
        profanityWordList.add("knobjocky");
        profanityWordList.add("knobjokey");
        profanityWordList.add("kock");
        profanityWordList.add("kondum");
        profanityWordList.add("kondums");
        profanityWordList.add("kum");
        profanityWordList.add("kummer");
        profanityWordList.add("kumming");
        profanityWordList.add("kums");
        profanityWordList.add("kunilingus");
        profanityWordList.add("l3i+ch");
        profanityWordList.add("l3itch");
        profanityWordList.add("labia");
        profanityWordList.add("lmfao");
        profanityWordList.add("lust");
        profanityWordList.add("lusting");
        profanityWordList.add("m0f0");
        profanityWordList.add("m0fo");
        profanityWordList.add("m45terbate");
        profanityWordList.add("ma5terb8");
        profanityWordList.add("ma5terbate");
        profanityWordList.add("masochist");
        profanityWordList.add("master-bate");
        profanityWordList.add("masterb8");
        profanityWordList.add("masterbat*");
        profanityWordList.add("masterbat3");
        profanityWordList.add("masterbate");
        profanityWordList.add("masterbation");
        profanityWordList.add("masterbations");
        profanityWordList.add("masturbate");
        profanityWordList.add("mo-fo");
        profanityWordList.add("mof0");
        profanityWordList.add("mofo");
        profanityWordList.add("mothafuck");
        profanityWordList.add("mothafucka");
        profanityWordList.add("mothafuckas");
        profanityWordList.add("mothafuckaz");
        profanityWordList.add("mothafucked");
        profanityWordList.add("mothafucker");
        profanityWordList.add("mothafuckers");
        profanityWordList.add("mothafuckin");
        profanityWordList.add("mothafucking");
        profanityWordList.add("mothafuckings");
        profanityWordList.add("mothafucks");
        profanityWordList.add("mother fucker");
        profanityWordList.add("motherfuck");
        profanityWordList.add("motherfucked");
        profanityWordList.add("motherfucker");
        profanityWordList.add("motherfuckers");
        profanityWordList.add("motherfuckin");
        profanityWordList.add("motherfucking");
        profanityWordList.add("motherfuckings");
        profanityWordList.add("motherfuckka");
        profanityWordList.add("motherfucks");
        profanityWordList.add("muff");
        profanityWordList.add("mutha");
        profanityWordList.add("muthafecker");
        profanityWordList.add("muthafuckker");
        profanityWordList.add("muther");
        profanityWordList.add("mutherfucker");
        profanityWordList.add("n1gga");
        profanityWordList.add("n1gger");
        profanityWordList.add("nazi");
        profanityWordList.add("nigg3r");
        profanityWordList.add("nigg4h");
        profanityWordList.add("nigga");
        profanityWordList.add("niggah");
        profanityWordList.add("niggas");
        profanityWordList.add("niggaz");
        profanityWordList.add("nigger");
        profanityWordList.add("niggers");
        profanityWordList.add("nob");
        profanityWordList.add("nob jokey");
        profanityWordList.add("nobhead");
        profanityWordList.add("nobjocky");
        profanityWordList.add("nobjokey");
        profanityWordList.add("numbnuts");
        profanityWordList.add("nutsack");
        profanityWordList.add("orgasim");
        profanityWordList.add("orgasims");
        profanityWordList.add("orgasm");
        profanityWordList.add("orgasms");
        profanityWordList.add("p0rn");
        profanityWordList.add("pawn");
        profanityWordList.add("pecker");
        profanityWordList.add("penis");
        profanityWordList.add("penisfucker");
        profanityWordList.add("phonesex");
        profanityWordList.add("phuck");
        profanityWordList.add("phuk");
        profanityWordList.add("phuked");
        profanityWordList.add("phuking");
        profanityWordList.add("phukked");
        profanityWordList.add("phukking");
        profanityWordList.add("phuks");
        profanityWordList.add("phuq");
        profanityWordList.add("pigfucker");
        profanityWordList.add("pimpis");
        profanityWordList.add("piss");
        profanityWordList.add("pissed");
        profanityWordList.add("pisser");
        profanityWordList.add("pissers");
        profanityWordList.add("pisses");
        profanityWordList.add("pissflaps");
        profanityWordList.add("pissin");
        profanityWordList.add("pissing");
        profanityWordList.add("pissoff");
        profanityWordList.add("poop");
        profanityWordList.add("porn");
        profanityWordList.add("porno");
        profanityWordList.add("pornography");
        profanityWordList.add("pornos");
        profanityWordList.add("prick");
        profanityWordList.add("pricks");
        profanityWordList.add("pron");
        profanityWordList.add("pube");
        profanityWordList.add("pusse");
        profanityWordList.add("pussi");
        profanityWordList.add("pussies");
        profanityWordList.add("pussy");
        profanityWordList.add("pussys");
        profanityWordList.add("rectum");
        profanityWordList.add("retard");
        profanityWordList.add("rimjaw");
        profanityWordList.add("rimming");
        profanityWordList.add("s hit");
        profanityWordList.add("s.o.b.");
        profanityWordList.add("sadist");
        profanityWordList.add("schlong");
        profanityWordList.add("screwing");
        profanityWordList.add("scroat");
        profanityWordList.add("scrote");
        profanityWordList.add("scrotum");
        profanityWordList.add("semen");
        profanityWordList.add("sex");
        profanityWordList.add("sh!+");
        profanityWordList.add("sh!t");
        profanityWordList.add("sh1t");
        profanityWordList.add("shag");
        profanityWordList.add("shagger");
        profanityWordList.add("shaggin");
        profanityWordList.add("shagging");
        profanityWordList.add("shemale");
        profanityWordList.add("shi+");
        profanityWordList.add("shit");
        profanityWordList.add("shitdick");
        profanityWordList.add("shite");
        profanityWordList.add("shited");
        profanityWordList.add("shitey");
        profanityWordList.add("shitfuck");
        profanityWordList.add("shitfull");
        profanityWordList.add("shithead");
        profanityWordList.add("shiting");
        profanityWordList.add("shitings");
        profanityWordList.add("shits");
        profanityWordList.add("shitted");
        profanityWordList.add("shitter");
        profanityWordList.add("shitters");
        profanityWordList.add("shitting");
        profanityWordList.add("shittings");
        profanityWordList.add("shitty");
        profanityWordList.add("skank");
        profanityWordList.add("slut");
        profanityWordList.add("sluts");
        profanityWordList.add("smegma");
        profanityWordList.add("smut");
        profanityWordList.add("snatch");
        profanityWordList.add("son-of-a-bitch");
        profanityWordList.add("spac");
        profanityWordList.add("spunk");
        profanityWordList.add("s_h_i_t");
        profanityWordList.add("t1tt1e5");
        profanityWordList.add("t1tties");
        profanityWordList.add("teets");
        profanityWordList.add("teez");
        profanityWordList.add("testical");
        profanityWordList.add("testicle");
        profanityWordList.add("tit");
        profanityWordList.add("titfuck");
        profanityWordList.add("tits");
        profanityWordList.add("titt");
        profanityWordList.add("tittie5");
        profanityWordList.add("tittiefucker");
        profanityWordList.add("titties");
        profanityWordList.add("tittyfuck");
        profanityWordList.add("tittywank");
        profanityWordList.add("titwank");
        profanityWordList.add("tosser");
        profanityWordList.add("turd");
        profanityWordList.add("tw4t");
        profanityWordList.add("twat");
        profanityWordList.add("twathead");
        profanityWordList.add("twatty");
        profanityWordList.add("twunt");
        profanityWordList.add("twunter");
        profanityWordList.add("v14gra");
        profanityWordList.add("v1gra");
        profanityWordList.add("vagina");
        profanityWordList.add("viagra");
        profanityWordList.add("vulva");
        profanityWordList.add("w00se");
        profanityWordList.add("wang");
        profanityWordList.add("wank");
        profanityWordList.add("wanker");
        profanityWordList.add("wanky");
        profanityWordList.add("whoar");
        profanityWordList.add("whore");
        profanityWordList.add("willies");
        profanityWordList.add("willy");
        profanityWordList.add("xrated");
        profanityWordList.add("xxx");
        return profanityWordList;
    }

    /**
     * print the log
     * @param tag
     * @param message
     * @return
     */
    public static void printLog(String tag , String message){
        if(LOG_ON)
            Log.e(tag , message);
    }


    /**
     * print the log
     * @param
     * @param message
     * @return
     */
    public static void printLog(String message){
        if(LOG_ON) {
            Log.e(Thread.currentThread().getStackTrace()[3].getClassName() +
                    " : "+Thread.currentThread().getStackTrace()[3].getMethodName() , message);

        }
    }
}
