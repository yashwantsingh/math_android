package com.mathfriendzy.utils;

public interface ICommonUtils
{
    int TAB_HEIGHT = 752;
    int TAB_WIDTH = 480;
    int TAB_DENISITY = 160;

    float COINS_PER_POINT = 0.05f;

    boolean MAIN_ACTIVITY_LOG	= false;
    boolean SPLASH_ACTIVITY_LOG = false;
    boolean LANGUAGE_LOG		= false;
    boolean DATABASE_LOG		= false;
    boolean PLAYER_ACTIVITY_LOG	= false;
    boolean CHOOSE_AVTAR_LOG	= false;

    boolean CREATE_TEMP_PLAYER_FLAG 	= false;
    boolean ADD_TEMP_PLAYER_STEP1_FLAG 	= false;
    boolean ADD_TEMP_PLAYER_STEP2_FLAG 	= false;
    boolean SCHOOL_INFO_SUB_FLAG		= false;
    boolean LOGIN_USER_PLAYER_FLAG		= false;
    boolean REGISTRATION_STEP_1_FALG	= false;
    boolean REGISTRATION_STEP_2_FALG    = false;
    boolean EDIT_REG_USER_PLAYER_FLAG   = false;
    boolean LOGIN_USER_CREATE_PLAYER    = false;
    boolean MODIFY_USER_FLAG			= false;
    boolean ADD_OR_CREATE_USER_PLAYER_FLAG = false;
    boolean SEARCH_YOUR_SCHOOL_FLAG		= false;
    boolean SEACRCH_YOUR_TEACHER		= false;
    boolean TAECHER_STUDENT_FLAG        = false;
    boolean LOGIN_ACTIVITY_FLAG         = false;
    boolean SEE_ANSWER_FLAG             = false;

    boolean LEARNING_CENTER_EQUATION_SOLVE_WITH_TIMER = false;


    //single friendzy

    boolean SINGLE_FRIENDZY_MAIN        = false;
    boolean CHOOSE_A_CHALLENGER         = false;
    boolean CHOOSE_A_CHALLENGER_LIST    = false;
    boolean SINGLE_FRIENDZY_EQUATION_SOLVE = false;


    boolean TEMP_PLAYER_OPERATION_FLAG = false;
    boolean FACEBOOK_CONNECT_LOG       = false;
    boolean CHHOSEAVTAR_OPERATION_LOG  = false;
    boolean COUNTRY_FLAG			   = false;
    boolean GRADE_FLAG				   = false;

    boolean IS_DATABASE_LOAD           = false;
    boolean IS_LANGUAGE_LOAD		   = false;
    boolean IS_TRANSELATION_LOAD	   = false;
    //boolean isAvtarLoaded 			   = true;

    //learning center flag
    boolean LEARNING_CENTER_NEW_MAIN_FLAG   = false;
    boolean LEARNING_CENTER_MAIN_FLAG 		= false;
    boolean LEARNING_CENTER_OPR       		= false;
    boolean LEARNING_CENTER_CHOOSE_EQUATION = false;
    boolean LEARNING_CENTER_EQUATION_SOLVE  = false;
    boolean LEARNIGN_CENTER_SCHOOL_CURRICULUM_SOLVE_EQUATION_FLAG = false;

    //multifriendzy flag

    boolean MULTI_FRIENDZY_MAIN_FLAG        = false;
    boolean MULTIFRIENDZY_ROUND_FLAG        = false;
    boolean MULTIFRIENDZY_PROBLEM_TYPE      = false;
    boolean MULTIFRIENDZY_WINNER_SCREEN     = false;

    //coins distribution flag
    boolean COINS_DISTRIBUTION_FLAG 		= false;

    //inn app
    boolean GET_MORE_COINS_FLAG             = false;

    // school curriculum
    boolean MULTI_FRIENDZY_SCHOOL_EQUATION_SOLVE = false;

    //assessment test
    boolean ASSESSMENT_MAIN					= false;
    boolean ASSESSMENT_PLAY_SCREEN			= false;

    //for report problem with question
    boolean REPORT_PROBLEM                  = false;
    boolean STUDENT_ACCOUNT					= false;
    boolean STUDENT_REPORT					= false;

    String LOGIN_SHARED_PREFF          = "loginPreff";
    String  IS_LOGIN			       = "isLogin";
    String PLAYER_ID				   = "playerId";
    String PLAYER_INFO                 = "playerInfo";
    String REG_USER_INFO_PREFF		   = "reguserinfoPreff";
    String IS_CHECKED_PREFF            = "isCheckedPreff";
    String LEARNING_CENTER_BG_INFO     = "backgroundInfo";
    String IS_FACEBOOK_LOGIN		   = "facebookLogin";//name of the shared preference
    String IS_LOGIN_FROM_FACEBOOK	   = "isLoginFromFacebook";//for holding true or false
    //for holding the assessment date
    String DATE_ASSESSMENT_STANDARD_DOWNLOAD = "assessmentStandardDate";
    //for wordProblem
    //for learn center
    String LEARN_CENTER_TIME_PREFF     = "learnignCenterTimePreff";
    String LEARN_CENTER_TIME           = "learnignCenterTime";
    String LEARN_CENTER_TIME_API_CALL_TIME = "apiCallingTimeForLearnignCenter";
    //for single friendzy
    String SINGLE_FRIENDZY_TIME_PREFF  = "singleFriendzyTimePreff";
    String SINGLE_FRIENDZY_TIME		   = "singleFriendzyTime";
    String SINGLE_FRIENDZY_TIME_API_CALL_TIME = "appCallingTimeForSingleFriendzy";

    String LOCAL_HOST_NAME      	= "http://172.16.5.140/~deepak/";
    String LOCAL_FILE_PATH_ON_HOST 	= "LeapAhead/LeapAhead/index.php?";

    //String LOCAL_FILE_PATH_ON_HOST 	= "LeapAhead/LeapAhead/tempIndex.php?";
    //String HOST_NAME 			= "http://www.chromeinfotech.com/";

    String HTTP = "http";
    String LIVE_API = "API3";//Need to change everytime when send the release
    String HOST_NAME 			= HTTP + "://api.letsleapahead.com/";//for live
    //String HOST_NAME 			= HTTP + "://api3.letsleapahead.com/";//for live
    String FILE_PATH_ON_HOST 	= "LeapAheadMultiFreindzy/index.php?";//for live
    //change1
    String COMPLETE_URL         =  HOST_NAME + FILE_PATH_ON_HOST;//for live
    //String COMPLETE_URL       =  LOCAL_HOST_NAME + LOCAL_FILE_PATH_ON_HOST;//for local

    //for sending email
    String LOCAL_FILE_PATH_TO_SEND_EMAIL = "LeapAhead/LeapAhead/inc/sendTutorEmails.php?";//for local
    String FILE_PATH_ON_HOST_TO_SEND_EMAIL = "LeapAheadMultiFreindzy/inc/sendTutorEmails.php?";//for live
    //change2
    //String COMPLETE_URL_TO_SEND_EMAIL = LOCAL_HOST_NAME + LOCAL_FILE_PATH_TO_SEND_EMAIL;//for local
    String COMPLETE_URL_TO_SEND_EMAIL = HOST_NAME + FILE_PATH_ON_HOST_TO_SEND_EMAIL;//for live

    //for drawing chat , change3
    //public static final String SERVER_IP = "172.16.5.140";//"192.168.1.32";//for local
    public static final String SERVER_IP = "letsleapahead.com";//for live
    public static final int SERVER_PORT  = 1099;

    String IMG_AVTAR_URL			=  HOST_NAME + "LeapAheadMultiFreindzy/images/avatars/";
    //String IPHONE_NOTIFICATION_URL 	= "http://api.letsleapahead.com/LeapAheadMultiFreindzy/inc/samples.php?";
    String IPHONE_NOTIFICATION_URL 	= HOST_NAME + "LeapAheadMultiFreindzy/inc/androidToIOsNotif.php?";

    //api for testing notification , change4
    /*String COMPLETE_URL_FOR_REG_DEVICE_NITFICATION = LOCAL_HOST_NAME + "LeapAhead/LeapAhead/androidNotification/" +
            "register.php?";*/
    String COMPLETE_URL_FOR_REG_DEVICE_NITFICATION = HOST_NAME + "LeapAheadMultiFreindzy/androidNotification/" +
            "register.php?";

    //String CONPLETE_URL_FOR_NOTIFICATION  = LOCAL_HOST_NAME + "LeapAhead/LeapAhead/index.php?";
    String CONPLETE_URL_FOR_NOTIFICATION  = COMPLETE_URL;
    //end

    String PRIVACY_POLICY_URL   = HTTP + "://letsleapahead.com/privacy-policy-app";
    String FACEBOOK_HOST_NAME 	= "https://graph.facebook.com/";
    String BTN_COM_LINK_URL     = HTTP + "://www.letsleapahead.com/";

    String FACEBOOK_GETMORE_COINS = "https://www.facebook.com/pages/Math-Friendzy/418520078185325?ref=ts";
    String IMAGE_DAWNLOAD_URL = HOST_NAME + "MathCurriculumImages/";

    String PROPERTY_REG_ID      = "GCM_REG_ID";
    String REG_ID_PREFF         = "Reg_Id";
    String DEVICE_ID_PREFF      = "deviceIdPreff";
    String DEVICE_ID            = "deviceId";

    //for friendzy Challenge
    String FRIENDZY_CHALL_ID            = "challengerIdPreff";

    //for ads
    String ADS_FREQUENCIES_PREFF  	     = "adsFrequenciesPreff";
    String ADS_FREQUENCIES_DATE   	     = "getFrequenciesDate";
    String ADS_FREQUENCIES			     = "adsFrequencies";
    String ADS_timeIntervalFullAd	     = "adstimeIntervalFullAd";
    String ADS_timeIntervalFullAdForPaid = "adstimeIntervalFullAdForPaid";
    //set in parseLoginEmailJson() of Login class or updated in updateUserCoins() of GetMoreCoins Activity
    String ADS_IS_ADS_DISABLE       = "isAdsDisable";

    //for new s3 bucket url for images
    String S3_BUCKET_URL = "http://s3-us-west-2.amazonaws.com/letsleap-image/images/";
    //for download images
    String DOWNLOAD_WORK_PDF_URL = S3_BUCKET_URL + "customQuesImages/";
    String DOWNLOAD_QUESTION_IMAGE_URL = S3_BUCKET_URL + "customStudQuesImg/";
    String DOWNLOD_WORK_IMAGE_URL = S3_BUCKET_URL + "customWorkImages/";
    String DOWNLOAD_CUSTOM_TUTOR_IMAGE_URL = S3_BUCKET_URL + "customTutorImages/";

    //change 5
    //for live , homework image url
    //for download
    /*String DOWNLOAD_CUSTOM_TUTOR_IMAGE_URL = HOST_NAME + "LeapAheadMultiFreindzy/customTutorImages/";
    String DOWNLOAD_WORK_PDF_URL = HOST_NAME + "LeapAheadMultiFreindzy/customQuesImages/";
    String DOWNLOAD_QUESTION_IMAGE_URL = HOST_NAME + "LeapAheadMultiFreindzy/customStudQuesImg/";
    String DOWNLOD_WORK_IMAGE_URL = HOST_NAME + "LeapAheadMultiFreindzy/customWorkImages/";*/

    //for upload
    String UPLOAD_HOMW_WORK_IMAGE_URL = HOST_NAME + "LeapAheadMultiFreindzy/upload.php?";
    String UPLOAD_HW_WORK_PDF_URL = HOST_NAME + "LeapAheadMultiFreindzy/upload.php?";
    String UPLOAD_QUESTION_IMAGE_URL = HOST_NAME + "LeapAheadMultiFreindzy/upload.php?";
    String UPLOAD_CUSTOM_TUTOR_IMAGE_URL = HOST_NAME + "LeapAheadMultiFreindzy/upload.php?";
    //end homework images urls

    //String GOORU_SEARCH_RESOURCES_URL = HOST_NAME + "LeapAheadMultiFreindzy/gooruapi/index.php/gooru/search?";//for live
    String GOORU_SEARCH_RESOURCES_URL = HOST_NAME + "LeapAheadMultiFreindzy/khanapi/?";//for live
    String  GET_KHAN_VIDEO_LINK = HOST_NAME + "LeapAheadMultiFreindzy/khanapi/video_link.php?";

    String PAYPAL_HOST = "https://api.letsleapahead.com/LeapAheadMultiFreindzy/PayPalRecurring";//for live
    String PAYPAL_CHECKOUT = PAYPAL_HOST + "/SetExpressCheckout.php?";
    String PAYPAL_CANCEL_PROFILE = PAYPAL_HOST +  "/CancelProfile.php?";
    String PAYPAL_CREDIT_CARD_PAYMENT = PAYPAL_HOST + "/CreditCardPayment.php?";

    String HOUSE_AD_ID = "ca-app-pub-4866053470899641/3084913413";
    String MORE_APP_URL = "https://play.google.com/store/apps/developer?id=WS+Publishing+Group";

    String DOWNLOAD_APP_ICON_URL = HOST_NAME + "LeapAheadMultiFreindzy/images/";
    String HOUSE_ADS_IMAGE_URL = HOST_NAME + "LeapAheadMultiFreindzy/AdImage/";

    //client in app API key
    String base64EncodedPublicKeyMathSimple = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtNSUwOs3ktfT" +
            "lvVxTbjCbHHS2cbdlkhl2PSFaincmIHGo+Y8UKLcN60pIzLBPaeBAym5v5g1nt1G50eOJS0mvMJJpXI" +
            "TO4TjpczUbgo3AUerb6L3u2OJ7HI7sSGO1vldlVqjwEqOGUfcH+E3Z/JuV9pJ4X2j6pmSkq6U8Elj7WayR" +
            "jvFHtwc0cyCZVzh2kEjfNXKwOflAPwRHMIsLBlM0tQUNHQ9uhC8IR/9zihaXy22df+Jfa5NHk+2NkB6LxJ7/" +
            "hkUelBaExOJeHnlizjuw/a3IotsQo/rvZCVmV2Fuknf5Ue0tTNNQwQACosxR8RS0X03MfJWWZVaVVwMCtvuswIDAQAB";

    /*String base64EncodedPublicKeyMathPlus = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA6AO5O" +
            "N8ADOY3C4Y3E5qsGsyfsf+Qvp5tG61exhGTDmuMreyjmM8uCZ5LTvmJVY2aUT0z3h74bX9F/fi2fI" +
            "yS6lwqRASzcIRmVlFOd1xAJQbDMj4vyXO2kNW6iCDnL75hHsok6AUctY/zVaq5BcJVXzfzNdlZGN4GVp" +
            "Lbp4x7dfKXTUkDdm4df7XF3I5v2RCXDQxFVXPnUPPgLexz7NzFDw37LfSqoWjJHYNKz/5ivfmLg2i1XyERN" +
            "wRM4+SIl1rnx8NQsHEvtTuqa0sV14cwQdXBLaufAHDxCCYRdeFmHB5MjwNbvqQVUrnsvpC9gsrbnZD9VKS7" +
            "25ogECE9sHU2uwIDAQAB";*/

    String base64EncodedPublicKeyMathPlus = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhntWv3inyZGw2Z" +
            "N18Gyh3SBvgGS6U0Kug859IL7jukNpzLJ3wi1JcRIelcTB3K3/aw9rdLF8fExOuw2m53ZyCpIMnWF4QiYeTjpHXMs" +
            "UzggmaxJVVUD6f9CUVtwe4K9g1TlAovqb+gYpsa7yOnFPbgfGQAvmx39zCCjG3/glU4F3Kk4Sy7wdo+L/JGoHnGAuue" +
            "QTqg7FTO/gOvZ9OeDtRRaej2EeHTTKrj7fmbsy0RnhfnFNED87w8qsUGgYmKWO/SeG7+MuwIit7vwTSOhTSqjiN2DT6y+3NZr" +
            "H5EWAt2992jn4gaRAA27TZKFkzaK31jDdpOoY8cU16XQVyY5vQwIDAQAB";

    String base64EncodedPublicKeyMathTutorPlus = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmQxzTNwWq" +
            "MiN0AH20pXAsWwTFx8bepsc39RVgPNttfeyRGgP9LSs5f3x+W8NIfHWzmqGfV9PERqgjBkWvOuWiMvMGm+A9+PyU5" +
            "xMo7Rsri4yJlWANKm8X1qEIiLSBC8er18diGWfM4uV0+5pAKTeE2w2n+ocMtP2ztfA2Vw8fDPcyjLkNkDIDe4dWsPj" +
            "hIpZ7GsxQEVM1jSxpHCivA1WXauZPuWPZdYa86cg7mkKGzqj05PUfgjsy8grqtSpSpXYOmnqbgMHfCtUB15" +
            "GaeA/jC02fl1dxMmMnPTPTP/E8hqzSO/3w10oKkJwCYMDHSVsSgxjdQO/JVLDEgJq3aCvLQIDAQAB";
}
