package com.mathfriendzy.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.util.Log;

import java.util.Random;

import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;

/**
 * This class play sound
 * @author Yashwant Singh
 *
 */
public class PlaySound 
{
	private MediaPlayer mp ;

	//for set default device volume when sound is off
	private int deviceDefaultVolume = 0;
	private Context context = null;
	private int userVolume = 50;

	private MediaPlayer curlPagePlayer;

	private final static int MAX_VOLUME = 100;

	public PlaySound(Context context)
	{
		this.context = context;
		this.setVolume(context);
	}

	//playsound
	/**
	 * this method play sound when user give wrong answer
	 * @param languageCode
	 * @param context
	 */
	public void playSoundForWrong(String languageCode,final Context context)
	{	
		Random random = new Random(); 
		int randomInteger = random.nextInt(4);
		randomInteger ++ ;
		//Log.e("Play Sund", randomInteger + "");
		try 
		{
			//this.setUserVolume(userVolume);

			MediaPlayer mp = MediaPlayer.create(context, Uri.parse("android.resource://com.mathfriendzy/raw/try_again_" + languageCode.toLowerCase() + "_" + randomInteger));
			this.setVolumeByGivenValue(mp , userVolume);
			if(mp != null)
				mp.start();

			mp.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					mp.release();
					//setDefaultVolumeWhenSoundClose(context);
				}
			});
		} 
		catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		} 
	}

	//playsound
	/**
	 * This method play sound when user give right answer
	 * @param languageCode
	 * @param context
	 */
	public void playSoundForRight(String languageCode,final Context context)
	{	
		Random random = new Random(); 
		int randomInteger = random.nextInt(9);
		randomInteger ++ ;
		//Log.e("Play Sund", randomInteger + "");
		try 
		{
			//this.setUserVolume(userVolume);
			MediaPlayer mp = MediaPlayer.create(context, Uri.parse("android.resource://com.mathfriendzy/raw/great_job_" + languageCode.toLowerCase() + "_" + randomInteger));
			this.setVolumeByGivenValue(mp , userVolume);
			if(mp != null)
				mp.start();

			mp.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					mp.release();
					//setDefaultVolumeWhenSoundClose(context);
				}
			});
		} 
		catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		} 
	}

	/**
	 * this method play sound for find challenger
	 * @param context
	 */
	public void playSoundForFindChallenger(Context context)
	{
		try 
		{
			//this.setUserVolume(userVolume);

			mp = MediaPlayer.create(context, Uri.parse("android.resource://com.mathfriendzy/raw/loop_chordal_synth_pattern_16"));
			this.setVolumeByGivenValue(mp , userVolume);
			mp.setLooping(true);
			mp.start();

			mp.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					mp.release();
				}
			});
		} 
		catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		} 
	}

	/**
	 * This method play sound when user play single friendzy
	 * @param context
	 */
	public void playSoundForSingleFriendzyEquationSolve(Context context)
	{
		Random random = new Random(); 
		int randomInteger = random.nextInt(3);
		randomInteger ++ ;

		try 
		{
			//this.setUserVolume(userVolume);
			mp = MediaPlayer.create(context, Uri.parse("android.resource://com.mathfriendzy/raw/loop_timely" + randomInteger));
			this.setVolumeByGivenValue(mp , userVolume);
			mp.setLooping(true);
			mp.start();

			mp.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					mp.release();
				}
			});
		} 
		catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		} 
	}

	//playsound
	/**
	 * this method play sound when user give wrong answer
	 * @param
	 * @param context
	 */
	public void playSoundForWrongForSingleFriendzy(final Context context)
	{		
		try 
		{
			MediaPlayer mp = MediaPlayer.create(context, Uri.parse("android.resource://com.mathfriendzy/raw/effect_comedy_wap"));
			this.setVolumeByGivenValue(mp , userVolume);
			if(mp != null)
				mp.start();

			mp.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					mp.release();
				}
			});
		} 
		catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		} 
	}

	//playsound
	/**
	 * this method play sound when user give wrong answer
	 * @param
	 * @param context
	 */
	public void playSoundForRightForSingleFriendzy(final Context context)
	{		
		try 
		{
			MediaPlayer mp = MediaPlayer.create(context, Uri.parse("android.resource://com.mathfriendzy/raw/effect_cartoon_space_boing"));
			this.setVolumeByGivenValue(mp , userVolume);
			if(mp != null)
				mp.start();

			mp.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					mp.release();
				}
			});
		} 
		catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		} 
	}

	/**
	 * this method play sound when another user give right answer in single friendzy
	 * @param
	 * @param context
	 */
	public void playSoundForRightForSingleFriendzyForChallenger(final Context context)
	{		
		try 
		{
			MediaPlayer mp = MediaPlayer.create(context, Uri.parse("android.resource://com.mathfriendzy/raw/effect_comedy_boing"));
			this.setVolumeByGivenValue(mp , userVolume);
			if(mp != null)
				mp.start();

			mp.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					mp.release();
				}
			});
		} 
		catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * This method play sound when user play learning center school curriculum
	 * @param context
	 */
	public void playSoundForSchoolSurriculumEquationSolve(Context context)
	{
		try 
		{		
			//this.setUserVolume(userVolume);
			mp = MediaPlayer.create(context, Uri.parse("android.resource://com.mathfriendzy/raw/trivia_background_music"));
			this.setVolumeByGivenValue(mp , userVolume);
			mp.setLooping(true);
			mp.start();

			mp.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					mp.release();
				}
			});
		} 
		catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		} 
	}

	/**
	 * This method play sound when user play learning center school curriculum
	 * and give right answer
	 * @param context
	 */
	public void playSoundForSchoolSurriculumEquationSolveForRightAnswer(final Context context)
	{
		try 
		{
			MediaPlayer mp = MediaPlayer.create(context, Uri.parse("android.resource://com.mathfriendzy/raw/correct_answer"));
			this.setVolumeByGivenValue(mp , userVolume);
			if(mp != null)
				mp.start();

			mp.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					mp.release();
				}
			});
		} 
		catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		} 
	}

	/**
	 * This method play sound when user play learning center school curriculum
	 * and give right answer
	 * @param context
	 */
	public void playSoundForSchoolSurriculumEquationSolveForWrongAnswer(final Context context)
	{
		try 
		{
			MediaPlayer mp = MediaPlayer.create(context, Uri.parse("android.resource://com.mathfriendzy/raw/incorrect_answer"));
			this.setVolumeByGivenValue(mp , userVolume);
			if(mp != null)
				mp.start();

			mp.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					mp.release();
				}
			});
		} 
		catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		} 
	}

	/**
	 * This method play sound when user play learning center school curriculum
	 * for page peel
	 * @param context
	 */
	public void playSoundForSchoolSurriculumPagePeel(final Context context)
	{
		try 
		{
			/*MediaPlayer mp = MediaPlayer.create(context, Uri.parse("android.resource://com.mathfriendzy/raw/page_peel"));
			if(mp != null)
				mp.start();

			mp.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					mp.release();
				}
			});*/
			curlPagePlayer = MediaPlayer.create(context, Uri.parse("android.resource://com.mathfriendzy/raw/page_peel"));
			this.setVolumeByGivenValue(curlPagePlayer , userVolume);
			if(curlPagePlayer != null)
				curlPagePlayer.start();

			curlPagePlayer.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					curlPagePlayer.release();
				}
			});
		} 
		catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		} 
	}

	/**
	 * This method play sound when user play learning center school curriculum
	 * for page peel
	 * @param context
	 */
	public void playSoundForSchoolSurriculumForResultScreen(final Context context)
	{
		try 
		{
			//this.setUserVolume(userVolume);
			MediaPlayer mp = MediaPlayer.create(context, Uri.parse("android.resource://com.mathfriendzy/raw/results_screen"));
			this.setVolumeByGivenValue(mp , userVolume);
			if(mp != null)
				mp.start();

			mp.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					mp.release();
					//setDefaultVolumeWhenSoundClose(context);
				}
			});
		} 
		catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		} 
	}

	/**
	 * This method stop player
	 */
	public void stopPlayer()
	{
		try{
			if(mp != null)
				mp.stop();

			if(curlPagePlayer != null)
				curlPagePlayer.release();
		}catch(Exception e){
			Log.e("PlaySound", "Error while stop player " + e.toString());
		}
		//this.setDefaultVolumeWhenSoundClose(context);
	}

	/**
	 * This method set the default device volume
	 * @param context
	 */
	public void setDefaultVolumeWhenSoundClose(Context context){
		/*// Get the AudioManager
		AudioManager audioManager =(AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
		// Set the volume of played media to your choice.
		audioManager.setStreamVolume (AudioManager.STREAM_MUSIC,deviceDefaultVolume ,0);*/
	}

	/**
	 * This method set the volume to the local variable
	 * @param volume
	 */
	/*public void setUserVolume(int volume){
		// Get the AudioManager
		AudioManager audioManager =(AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
		//deviceDefaultVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		deviceDefaultVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		// Set the volume of played media to your choice.
		audioManager.setStreamVolume (AudioManager.STREAM_MUSIC,
				(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) * volume) / 100 ,0);
	}*/

	/**
	 * This method call from see answer activity
	 */
	public void setUserVolumeFromSeeAnswer(){
		/*// Get the AudioManager
		AudioManager audioManager =(AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
		deviceDefaultVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		// Set the volume of played media to your choice.
		audioManager.setStreamVolume (AudioManager.STREAM_MUSIC,
				(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) * userVolume) / 100 ,0);*/
	}

	/**
	 * This method set the volume
	 */
	private void setVolume(Context context){
		SharedPreferences sheredPreference = context.getSharedPreferences(LOGIN_SHARED_PREFF, 0);
		if(sheredPreference.getBoolean(IS_LOGIN, false)){
			userVolume = this.getVolumeProgressForUser(context);
            /*// Get the AudioManager
			AudioManager audioManager =(AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
			//deviceDefaultVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
			deviceDefaultVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
			// Set the volume of played media to your choice.
			audioManager.setStreamVolume (AudioManager.STREAM_MUSIC,
					(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) * userVolume) / 100 ,0);*/
		}
	}

	/**
	 * This method return the sound value in integer , volume value lie between 0 to 1 so need to
     * multiply by 100
	 * @return
	 */
	private int getVolumeProgressForUser(Context context){
		try{
            Log.e("TAG" , "str volume " + CommonUtils.getUserVolume(context));
            return (int) (Float.parseFloat(CommonUtils.getUserVolume(context)) * 100);
			//return Integer.parseInt(CommonUtils.getUserVolume(context));
		}catch(Exception e){
			Log.e("PlaySound", " error while parsing volume " + e.toString());
			return 50;
		}
	}

	/**
	 * This method set the sound volume to the MediaPlayer
	 * @param mp
	 * @param soundVolume
	 */
	private void setVolumeByGivenValue(MediaPlayer mp , int soundVolume){
		if(mp != null){
			final float volume = (float) (1 - (Math.log(MAX_VOLUME - soundVolume) / Math.log(MAX_VOLUME)));
			mp.setVolume(volume, volume);
			//mp.setVolume(1 , volume);
		}
	}
}
