package com.mathfriendzy.adm;

import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.amazon.device.messaging.ADMMessageHandlerBase;
import com.amazon.device.messaging.ADMMessageReceiver;
import com.mathfriendzy.gcm.ProcessNotification;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.MyApplication;

public class SampleADMMessageHandler extends ADMMessageHandlerBase {

    private final static String TAG = "ADMSampleMessenger";

    public static class MessageAlertReceiver extends ADMMessageReceiver {
        public MessageAlertReceiver() {
            super(SampleADMMessageHandler.class);
        }
    }

    public SampleADMMessageHandler(){
        super(SampleADMMessageHandler.class.getName());
    }

    public SampleADMMessageHandler(final String className){
        super(className);
    }

    @Override
    protected void onMessage(final Intent intent) {
        //this.showToast(" Notification Message: " + intent.getExtras().getString("message"));
        String message = intent.getExtras().getString("message");
        Intent i = new Intent(this, ProcessNotification.class);
        i.putExtra("Message", message);
        this.startService(i);
    }

    @Override
    protected void onRegistrationError(final String string){
        Log.e(TAG, "SampleADMMessageHandler:onRegistrationError " + string);
        this.showToast(" onRegistrationError : " + string);
    }

    @Override
    protected void onRegistered(final String registrationId){
        CommonUtils.printLog(TAG , "AMD Registration ID " + registrationId);
        //this.showToast("On Register Success  : Registration Id " + registrationId);
        MathFriendzyHelper.registerDeviceOnServer(this, registrationId,
                MathFriendzyHelper.AMAZON_DEVICE);
    }

    @Override
    protected void onUnregistered(final String registrationId){
        CommonUtils.printLog(TAG , "on UnRegister AMD Registration ID " + registrationId);
       //this.showToast("On UnRegister : AMD Registration ID " + registrationId);
    }

    private void showToast(final String message) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MyApplication.getAppContext(),
                        message, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
