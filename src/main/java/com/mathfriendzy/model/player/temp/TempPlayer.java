package com.mathfriendzy.model.player.temp;

import com.mathfriendzy.model.player.base.Player;

public class TempPlayer extends Player 
{
	private String zipCode;
	private int countryId; 
	private int stateId; 
	private String country;
	private String state;
		
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getCountryId() 
	{
		return countryId;
	}

	public void setCountryId(int countryId) 
	{
		this.countryId = countryId;
	}

	public int getStateId() 
	{
		return stateId;
	}

	public void setStateId(int stateId) 
	{
		this.stateId = stateId;
	}

	public String getZipCode() 
	{
		return zipCode;
	}

	public void setZipCode(String zipCode) 
	{
		this.zipCode = zipCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}
