package com.mathfriendzy.model.player.temp;

public interface GetUserName {
	void onComplete(String userName);
}
