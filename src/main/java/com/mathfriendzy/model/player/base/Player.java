package com.mathfriendzy.model.player.base;

public class Player 
{
    protected String firstName;
	protected String lastName;
	protected String userName;
	protected int schoolId;
	protected String schoolName;
	protected int grade;
	protected int teacherUserId;
	protected String teacherFirstName;
	protected String teacherLastName;
	protected int coins;
	protected int points;
	protected String city;
	protected String state;
	protected int competeLevel;
	protected String profileImageName;
	protected int indexOfAppearance;
	
	protected String androidPids;
	protected String iponPids;
	
	protected String schoolIdString;
	
	public String getAndroidPids() {
		return androidPids;
	}

	public void setAndroidPids(String androidPids) {
		this.androidPids = androidPids;
	}

	public String getIponPids() {
		return iponPids;
	}

	public void setIponPids(String iponPids) {
		this.iponPids = iponPids;
	}

	public int getIndexOfAppearance() {
		return indexOfAppearance;
	}

	public void setIndexOfAppearance(int indexOfAppearance) {
		this.indexOfAppearance = indexOfAppearance;
	}

	public String getProfileImageName() {
		return profileImageName;
	}

	public void setProfileImageName(String profileImageName) {
		this.profileImageName = profileImageName;
	}

	protected byte[] profileImage;
	protected int parentUserId;
	protected int playerId;
	
	public int getTeacherUserId() 
	{
		return teacherUserId;
	}

	public void setTeacherUserId(int teacherUserId) 
	{
		this.teacherUserId = teacherUserId;
	}

	public String getTeacherFirstName() 
	{
		return teacherFirstName;
	}

	public void setTeacherFirstName(String teacherFirstName) 
	{
		this.teacherFirstName = teacherFirstName;
	}

	public String getTeacherLastName() 
	{
		return teacherLastName;
	}

	public void setTeacherLastName(String teacherLastName) 
	{
		this.teacherLastName = teacherLastName;
	}

	public int getCoins() 
	{
		return coins;
	}

	public void setCoins(int coins) 
	{
		this.coins = coins;
	}

	public int getPoints() 
	{
		return points;
	}

	public void setPoints(int points) 
	{
		this.points = points;
	}

	public String getCity() 
	{
		return city;
	}

	public void setCity(String city) 
	{
		this.city = city;
	}

	public String getState() 
	{
		return state;
	}

	public void setState(String state) 
	{
		this.state = state;
	}

	public int getCompeteLevel() 
	{
		return competeLevel;
	}

	public void setCompeteLevel(int competeLevel) 
	{
		this.competeLevel = competeLevel;
	}

	public byte[] getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(byte[] profileImage) {
		this.profileImage = profileImage;
	}

	/*public String getProfileImage() 
	{
		return profileImage;
	}

	public void setProfileImage(String profileImageId) 
	{
		this.profileImage = profileImageId;
	}
*/
	public String getFirstName() 
	{
		return firstName;
	}
	
	public void setFirstName(String firstName) 
	{
		this.firstName = firstName;
	}

	public String getSchoolName() 
	{
		return schoolName;
	}

	public void setSchoolName(String schoolName) 
	{
		this.schoolName = schoolName;
	}
	
	public int getSchoolId() 
	{
		return schoolId;
	}

	public void setSchoolId(int schoolId) 
	{
		this.schoolId = schoolId;
	}

	public int getGrade() 
	{
		return grade;
	}

	public void setGrade(int grade) 
	{
		this.grade = grade;
	}
	
	public String getLastName() 
	{
		return lastName;
	}

	public void setLastName(String lastName) 
	{
		this.lastName = lastName;
	}
	
	public String getUserName() 
	{
		return userName;
	}
	
	public void setUserName(String userName) 
	{
		this.userName = userName;
	}
	public int getParentUserId() 
	{
		return parentUserId;
	}
	
	public void setParentUserId(int parentUserId) 
	{
		this.parentUserId = parentUserId;
	}
	
	public int getPlayerId() 
	{
		return playerId;
	}
	
	public void setPlayerId(int playerId) 
	{
		this.playerId = playerId;
	}

	public String getSchoolIdString() {
		return schoolIdString;
	}

	public void setSchoolIdString(String schoolIdString) {
		this.schoolIdString = schoolIdString;
	}
	
}
