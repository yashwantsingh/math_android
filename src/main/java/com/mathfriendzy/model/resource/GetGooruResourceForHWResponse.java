package com.mathfriendzy.model.resource;

import com.mathfriendzy.serveroperation.HttpResponseBase;

import java.util.ArrayList;

/**
 * Created by root on 20/8/15.
 */
public class GetGooruResourceForHWResponse extends HttpResponseBase{
    private String result;
    private ArrayList<ResourceResponse> selectedResourceList;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public ArrayList<ResourceResponse> getSelectedResourceList() {
        return selectedResourceList;
    }

    public void setSelectedResourceList(ArrayList<ResourceResponse> selectedResourceList) {
        this.selectedResourceList = selectedResourceList;
    }
}
