package com.mathfriendzy.model.resource;

import com.mathfriendzy.serveroperation.HttpResponseBase;

/**
 * Created by root on 13/1/16.
 */
public class GetResourceVideoInAppResult extends HttpResponseBase{
    private String result;
    private int status;
    private int unlockCategoryStatus;
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getUnlockCategoryStatus() {
        return unlockCategoryStatus;
    }

    public void setUnlockCategoryStatus(int unlockCategoryStatus) {
        this.unlockCategoryStatus = unlockCategoryStatus;
    }
}
