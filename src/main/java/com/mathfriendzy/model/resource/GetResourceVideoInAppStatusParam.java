package com.mathfriendzy.model.resource;

/**
 * Created by root on 13/1/16.
 */
public class GetResourceVideoInAppStatusParam {
    private String action;
    private String userId;
    private String appId;
    private int spentCoins;
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public int getSpentCoins() {
        return spentCoins;
    }

    public void setSpentCoins(int spentCoins) {
        this.spentCoins = spentCoins;
    }
}
