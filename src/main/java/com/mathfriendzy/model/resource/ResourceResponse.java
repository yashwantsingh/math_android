package com.mathfriendzy.model.resource;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by root on 6/8/15.
 */
public class ResourceResponse implements Serializable {

    private String title;
    private String description;
    private String mediaType;
    private String url ;
    private String value;
    private String resourceUrl;
    private String scollectionCount;

    private boolean isAddedToHomework = false;

    private int pageNumber = 0;

    public String getScollectionCount() {
        return scollectionCount;
    }
    public void setScollectionCount(String scollectionCount) {
        this.scollectionCount = scollectionCount;
    }
    public String getResourceUrl() {
        return resourceUrl;
    }
    public void setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl;
    }
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
    private ArrayList<String> curriculumCode = new ArrayList<String>();

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getMediaType() {
        return mediaType;
    }
    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public ArrayList<String> getCurriculumCode() {
        return curriculumCode;
    }
    public void setCurriculumCode(ArrayList<String> curriculumCode) {
        this.curriculumCode = curriculumCode;
    }

    public boolean isAddedToHomework() {
        return isAddedToHomework;
    }

    public void setAddedToHomework(boolean isAddedToHomework) {
        this.isAddedToHomework = isAddedToHomework;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }
}
