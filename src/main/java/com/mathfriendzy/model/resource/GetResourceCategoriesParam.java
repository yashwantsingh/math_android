package com.mathfriendzy.model.resource;

/**
 * Created by root on 13/1/16.
 */
public class GetResourceCategoriesParam {
    private String action;
    private String date;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
