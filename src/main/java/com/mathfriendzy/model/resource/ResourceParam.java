package com.mathfriendzy.model.resource;

import java.io.Serializable;

/**
 * Created by root on 30/7/15.
 */
public class ResourceParam  implements Serializable{

    private String grade;
    private String pageNumber;
    private String pageSize;
    private String query;
    private String resourceFormat;
    private String subjectName ;
    private int resourceFormateType;
    private int selectedLang;

    public String getGrade() {
        return grade;
    }
    public void setGrade(String grade) {
        this.grade = grade;
    }
    public String getPageNumber() {
        return pageNumber;
    }
    public void setPageNumber(String pageNumber) {
        this.pageNumber = pageNumber;
    }
    public String getPageSize() {
        return pageSize;
    }
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }
    public String getQuery() {
        return query;
    }
    public void setQuery(String query) {
        this.query = query;
    }
    public String getResourceFormat() {
        return resourceFormat;
    }
    public void setResourceFormat(String resourceFormat) {
        this.resourceFormat = resourceFormat;
    }
    public String getSubjectName() {
        return subjectName;
    }
    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }
    public int getResourceFormateType() {
        return resourceFormateType;
    }

    public void setResourceFormateType(int resourceFormateType) {
        this.resourceFormateType = resourceFormateType;
    }

    public int getSelectedLang() {
        return selectedLang;
    }

    public void setSelectedLang(int selectedLang) {
        this.selectedLang = selectedLang;
    }
}
