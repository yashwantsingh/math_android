package com.mathfriendzy.model.resource;

/**
 * Created by root on 20/8/15.
 */
public class GetGooruResourcesForHWParam {
    private String action;
    private String homeworkId;
    private String customHWId;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getHomeworkId() {
        return homeworkId;
    }

    public void setHomeworkId(String homeworkId) {
        this.homeworkId = homeworkId;
    }

    public String getCustomHWId() {
        return customHWId;
    }

    public void setCustomHWId(String customHWId) {
        this.customHWId = customHWId;
    }
}
