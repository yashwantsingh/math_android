package com.mathfriendzy.model.devicedimention;

/**
 * Created by root on 16/4/15.
 */
public class DrawDeviceDimention {

    private float currentDeviceWidth;
    private float currentDeviceHeight;
    private float otherDeviceWidth;
    private float otherDeviceHeight;

    public float getCurrentDeviceWidth() {
        return currentDeviceWidth;
    }

    public void setCurrentDeviceWidth(float currentDeviceWidth) {
        this.currentDeviceWidth = currentDeviceWidth;
    }

    public float getCurrentDeviceHeight() {
        return currentDeviceHeight;
    }

    public void setCurrentDeviceHeight(float currentDeviceHeight) {
        this.currentDeviceHeight = currentDeviceHeight;
    }

    public float getOtherDeviceWidth() {
        return otherDeviceWidth;
    }

    public void setOtherDeviceWidth(float otherDeviceWidth) {
        this.otherDeviceWidth = otherDeviceWidth;
    }

    public float getOtherDeviceHeight() {
        return otherDeviceHeight;
    }

    public void setOtherDeviceHeight(float otherDeviceHeight) {
        this.otherDeviceHeight = otherDeviceHeight;
    }
}
