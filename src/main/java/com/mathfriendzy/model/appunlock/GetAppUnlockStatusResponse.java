package com.mathfriendzy.model.appunlock;

import com.mathfriendzy.serveroperation.HttpResponseBase;

/**
 * Created by root on 19/1/16.
 */
public class GetAppUnlockStatusResponse extends HttpResponseBase{
    private String result;
    private int appUnlockStatus;
    private int coins;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getAppUnlockStatus() {
        return appUnlockStatus;
    }

    public void setAppUnlockStatus(int appUnlockStatus) {
        this.appUnlockStatus = appUnlockStatus;
    }

    public int getCoins() {
        return coins;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }
}
