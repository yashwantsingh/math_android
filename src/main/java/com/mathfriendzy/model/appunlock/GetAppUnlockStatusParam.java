package com.mathfriendzy.model.appunlock;

/**
 * Created by root on 19/1/16.
 */
public class GetAppUnlockStatusParam {
    private String action;
    private String userId;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
