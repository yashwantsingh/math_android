package com.mathfriendzy.model.schools;

import java.util.Comparator;

public class SortByScoolName implements Comparator<SchoolDTO>
{
	@Override
	public int compare(SchoolDTO school1, SchoolDTO school2) 
	{
		return school1.getSchoolName().compareTo(school2.getSchoolName());
	}
}
