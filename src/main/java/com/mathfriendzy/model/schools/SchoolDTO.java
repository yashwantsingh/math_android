package com.mathfriendzy.model.schools;

public class SchoolDTO 
{
	private String schoolName = null;
	private String schoolId   = null;
	
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public String getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
	}
	
}
