package com.mathfriendzy.model.friendzy;

public class GetEquationIdFromServerForFeiendzy {
	private String endDate;
	private String equationIds;
	
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getEquationIds() {
		return equationIds;
	}
	public void setEquationIds(String equationIds) {
		this.equationIds = equationIds;
	}
	
	
}
