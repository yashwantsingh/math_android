package com.mathfriendzy.model.friendzy;

import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;

import com.mathfriendzy.utils.CommonUtils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

public class SaveTimePointsForFriendzyChallenge extends AsyncTask<Void, Void, Void>
{
	long time;
	int points;
	Context context;
	SharedPreferences sharedPreffPlayerInfo;

	SharedPreferences sharedPreffriendzy;
	String playerId	;
	String userId	;
	String freindPlayer;
	String friendUser;

	public SaveTimePointsForFriendzyChallenge(long time, int points, Context context)
	{
		this.points = points;
		this.time	= time;
		this.context	= context;
		
		sharedPreffPlayerInfo	 	= context.getSharedPreferences(IS_CHECKED_PREFF, 0);
		//sharedPreffriendzy	 		= context.getSharedPreferences(ITextIds.FRIENDZY, 0);

		playerId					= sharedPreffPlayerInfo.getString("playerId", "");
		userId						= sharedPreffPlayerInfo.getString("userId", "");

		/*freindPlayer				= sharedPreffriendzy.getString("playerId", "");
		friendUser					= sharedPreffriendzy.getString("userId", "");	*/	
	}

	@Override
	protected Void doInBackground(Void... params) 
	{	
		FriendzyServerOperation frndzy = new FriendzyServerOperation();
		frndzy.saveTimeForFriendzy(userId, playerId,
				CommonUtils.getActivePlayerChallengerId(userId , playerId), time, points);
		
		return null;
	}

	@Override
	protected void onPostExecute(Void result) 
	{		
		super.onPostExecute(result);
	}
}
