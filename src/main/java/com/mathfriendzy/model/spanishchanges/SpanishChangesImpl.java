package com.mathfriendzy.model.spanishchanges;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.mathfriendzy.database.Database;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.CategoryListTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SubCatergoryTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.UpdatedInfoTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.WordProblemQuestionTransferObj;

import java.util.ArrayList;

public class SpanishChangesImpl {

	//private Context context 				= null;
	private SQLiteDatabase dbConn 	  	    = null;
	private Database database               = null;

	/**
	 * Constructor
	 * @param context
	 */
	public SpanishChangesImpl(Context context)
	{
		database = new Database(context);
		database.open();		
	}

	/**
	 * This method open connection with database
	 */
	public void openConn()
	{
		dbConn = database.getConnection();
	}

	/**
	 * This method close the database connection
	 */
	public void closeConn()
	{
		if(dbConn != null)
			dbConn.close();
	}

	/**
	 * This method check for this table is exist or not
	 * @return
	 */
	public boolean isWordProblemsQuestionsSpanishExist(){

		Cursor cursor = dbConn.rawQuery("SELECT * FROM sqlite_master WHERE type = ? AND name = ?", 
				new String[] {"table", "WordProblemsQuestionsSpanish"});
		if(cursor.moveToFirst())
		{
			cursor.close();
			return true;
		}
		else
		{
			cursor.close();
			return false;
		}
	}

	/**
	 * This method check for this table is exist or not
	 * @return
	 */
	public boolean isWordProblemsUpdateDateDetailSpanishExist(){
		Cursor cursor = dbConn.rawQuery("SELECT * FROM sqlite_master WHERE type = ? AND name = ?", 
				new String[] {"table", "WordProblemUpdateDetailsSpanish"});
		if(cursor.moveToFirst())
		{
			cursor.close();
			return true;
		}
		else
		{
			cursor.close();
			return false;
		}
	}

	/**
	 * This method check for this table is exist or not
	 * @return
	 */
	public boolean isWordProblemCategoriesSpanishTableExist(){

		Cursor cursor = dbConn.rawQuery("SELECT * FROM sqlite_master WHERE type = ? AND name = ?", 
				new String[] {"table", "WordProblemCategoriesSpanish"});
		if(cursor.moveToFirst())
		{
			cursor.close();
			return true;
		}
		else
		{
			cursor.close();
			return false;
		}
	}


	/**
	 * This method check for this table is exist or not
	 * @return
	 */
	public boolean isWordProblemsSubCategoriesSpanish(){

		Cursor cursor = dbConn.rawQuery("SELECT * FROM sqlite_master WHERE type = ? AND name = ?", 
				new String[] {"table", "WordProblemsSubCategoriesSpanish"});
		if(cursor.moveToFirst())
		{
			cursor.close();
			return true;
		}
		else
		{
			cursor.close();
			return false;
		}
	}

	/**
	 * This method create the WordProblemsSubCategoriesSpanish table
	 */
	public void createWordProblemsSubCategoriesSpanish(){
		String query = "CREATE TABLE WordProblemsSubCategoriesSpanish " +
				"(category_id INTEGER, sub_category_id INTEGER, " +
				"sub_category_name VARCHAR(200), video_url VARCHAR(250))";
		dbConn.execSQL(query);
	}

	/**
	 * This method create the WordProblemCategoriesSpanish table
	 */
	public void createWordProblemCategoriesSpanish(){
		String query = "CREATE TABLE WordProblemCategoriesSpanish " +
				"(word_category_id INTEGER PRIMARY KEY  AUTOINCREMENT " +
				"NOT NULL , category_name VARCHAR(60), grade_level INTEGER(2))";
		dbConn.execSQL(query);
	}



	/**
	 * This method create table
	 */
	public void createWordProblemsQuestionsSpanish(){
		String query = "CREATE TABLE WordProblemsQuestionsSpanish " +
				"(category_id INTEGER , subcategory_id INTEGER , question_id " +
				"INTEGER PRIMARY KEY  NOT NULL , question VARCHAR(350) , " +
				"option1 VARCHAR(200) DEFAULT (null) , option2 VARCHAR(200) DEFAULT (null) " +
				", option3 VARCHAR(200) DEFAULT (null) ,option4 VARCHAR(200) DEFAULT (null) ," +
				"option5 VARCHAR(200) DEFAULT (null) ,option6 VARCHAR(200) DEFAULT (null) ," +
				"option7 VARCHAR(200) DEFAULT (null) ,option8 VARCHAR(200),ansPrefix VARCHAR(25) DEFAULT (null) ," +
				"ansSuffix VARCHAR(25) DEFAULT (null) ,image VARCHAR(50),fill_in_blank_option BOOL DEFAULT (0) ," +
				"avialableForTest BOOL DEFAULT (0) ,correctAnswers VARCHAR(50))";
		dbConn.execSQL(query);
	}

	/**
	 * This method delete the data from the WordProblemsQuestions ByCategoryId
	 * @param categoryList
	 */
	public void deleteFromWordProblemsQuestionsCategoryId(ArrayList<CategoryListTransferObj> categoryList){
		String categories = "";

		for(  int  i = 0 ; i < categoryList.size() ; i ++)
		{
			if(i == categoryList.size() - 1)
				categories = categories + categoryList.get(i).getCid();
			else
				categories = categories + categoryList.get(i).getCid() + ",";
		}

		/*String query = "delete from WordProblemsQuestionsSpanish where category_id in (" + categories + ")";
		Cursor cursor =  dbConn.rawQuery(query, null);*/
		String where = "category_id in (" + categories + ")";
		dbConn.delete("WordProblemsQuestionsSpanish" , where, null);
		//this.closeCursor(cursor);
	}

	/**
	 * This method get equation from data base accordign to the category and sub categiry id
	 * @param categoryId
	 * @param subCategoryId
	 */
	public ArrayList<WordProblemQuestionTransferObj> getQuestionAnswer(int categoryId , int subCategoryId){

		ArrayList<WordProblemQuestionTransferObj> questionAnswerList = 
				new ArrayList<WordProblemQuestionTransferObj>();

		String query = "select * from WordProblemsQuestionsSpanish where category_id = '" + categoryId 
				+"' and subcategory_id = '" + subCategoryId +"'";

		Cursor cursor =  dbConn.rawQuery(query, null);

		while(cursor.moveToNext()){

			ArrayList<String> optionList = new ArrayList<String>();

			WordProblemQuestionTransferObj quetionObj = new WordProblemQuestionTransferObj();
			quetionObj.setCatId(cursor.getInt(cursor.getColumnIndex("category_id")));
			quetionObj.setSubCatId(cursor.getInt(cursor.getColumnIndex("subcategory_id")));
			quetionObj.setQuestionId(cursor.getInt(cursor.getColumnIndex("question_id")));
			quetionObj.setQuestion(cursor.getString(cursor.getColumnIndex("question")));

			optionList.add(cursor.getString(cursor.getColumnIndex("option1")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option2")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option3")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option4")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option5")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option6")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option7")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option8")));
			quetionObj.setOptionList(optionList);

			quetionObj.setLeftAns(cursor.getString(cursor.getColumnIndex("ansPrefix")));
			quetionObj.setRightAns(cursor.getString(cursor.getColumnIndex("ansSuffix")));
			quetionObj.setImage(cursor.getString(cursor.getColumnIndex("image")));
			quetionObj.setFillIn(cursor.getInt(cursor.getColumnIndex("fill_in_blank_option")));
			quetionObj.setForTest(cursor.getInt(cursor.getColumnIndex("avialableForTest")));
			quetionObj.setAns(cursor.getString(cursor.getColumnIndex("correctAnswers")));

			questionAnswerList.add(quetionObj);
		}

		this.closeCursor(cursor);

		return questionAnswerList;
	}

	/**
	 * Get question by question id
	 * @param questionId
	 * @return
	 */
	public WordProblemQuestionTransferObj getQuestionByQuestionId(int questionId){
		WordProblemQuestionTransferObj quetionObj = new WordProblemQuestionTransferObj();
		String query = "select * from WordProblemsQuestionsSpanish where question_id = '" + questionId  +"'";
		Cursor cursor =  dbConn.rawQuery(query, null);

		if(cursor.moveToNext()){
			ArrayList<String> optionList = new ArrayList<String>();

			quetionObj.setCatId(cursor.getInt(cursor.getColumnIndex("category_id")));
			quetionObj.setSubCatId(cursor.getInt(cursor.getColumnIndex("subcategory_id")));
			quetionObj.setQuestionId(cursor.getInt(cursor.getColumnIndex("question_id")));
			quetionObj.setQuestion(cursor.getString(cursor.getColumnIndex("question")));

			optionList.add(cursor.getString(cursor.getColumnIndex("option1")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option2")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option3")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option4")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option5")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option6")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option7")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option8")));
			quetionObj.setOptionList(optionList);

			quetionObj.setLeftAns(cursor.getString(cursor.getColumnIndex("ansPrefix")));
			quetionObj.setRightAns(cursor.getString(cursor.getColumnIndex("ansSuffix")));
			quetionObj.setImage(cursor.getString(cursor.getColumnIndex("image")));
			quetionObj.setFillIn(cursor.getInt(cursor.getColumnIndex("fill_in_blank_option")));
			quetionObj.setForTest(cursor.getInt(cursor.getColumnIndex("avialableForTest")));
			quetionObj.setAns(cursor.getString(cursor.getColumnIndex("correctAnswers")));
		}

		this.closeCursor(cursor);
		return quetionObj;
	}

	/**
	 * This method return the fill in value 
	 * @param questionId
	 * @return
	 */
	public int getFillIn(int questionId){
		int fill = 0;
		String query = "select fill_in_blank_option from WordProblemsQuestionsSpanish " +
				"where question_id = '" 
				+ questionId  +"'";
		Cursor cursor =  dbConn.rawQuery(query, null);
		if(cursor.moveToNext()){
			fill = cursor.getInt(cursor.getColumnIndex("fill_in_blank_option"));
		}
		this.closeCursor(cursor);
		return fill;
	}

	/**
	 * This method close cursor
	 * @param cursor
	 */
	private void closeCursor(Cursor cursor){
		if(cursor != null)
			cursor.close();
	}

	/**
	 * This method create the table for word problem question update date details
	 */
	public void createWordProblemUpdateDetailsSpanishTable(){
		String query = "CREATE TABLE WordProblemUpdateDetailsSpanish (grade INTEGER, date DATETIME)";
		dbConn.execSQL(query);
	}

	/**
	 * This method updated the new server updated date from the old one
	 * @param grade
	 * @param updatedDate
	 */
	public void updateNewUpdatedDateFromServer(int grade , String updatedDate){
		if(grade > 8)
			grade = 8;
		String where = "grade = '" + grade +"'";
		ContentValues cv = new ContentValues();
		cv.put("date", updatedDate);
		dbConn.update("WordProblemUpdateDetailsSpanish", cv, where, null);
	}

	/**
	 * This method insert the data into the WordProblemUpdateDetails table
	 * @param updatedList
	 */
	public void insertIntoWordProblemUpdateDetails(ArrayList<UpdatedInfoTransferObj> updatedList){
		for(int i = 0 ; i < updatedList.size() ; i ++ ){
			ContentValues contentValues = new ContentValues();
			contentValues.put("grade", updatedList.get(i).getGrade());
			contentValues.put("date", updatedList.get(i).getUpdatedDate());

			dbConn.insert("WordProblemUpdateDetailsSpanish", null, contentValues);
		}
	}

	/**
	 * This method check equation is loaded in database or not corresponding to database
	 * @param grade
	 * @return
	 */
	public boolean isQuestionLeaded(int grade){
		if(grade > 8)
			grade = 8;
		String query = "select * from WordProblemsQuestionsSpanish where category_id in " +
				"(select word_category_id from " +
				" WordProblemCategoriesSpanish where grade_level = '" + grade + "')";
		Cursor cursor =  dbConn.rawQuery(query, null);

		if(cursor.moveToNext()){
			this.closeCursor(cursor);
			return true;
		}
		else{
			this.closeCursor(cursor);
			return false;
		}
	}

	/**
	 * This method return the updated date according to the grade
	 * @param grade
	 * @return
	 */
	public String getUpdatedDate(int grade){

		if(grade > 8)
			grade = 8;

		String date = "";
		String query = "select * from WordProblemUpdateDetailsSpanish where grade = '" + grade + "'";
		Cursor cursor =  dbConn.rawQuery(query, null);

		if(cursor.moveToNext()){
			date =  cursor.getString(cursor.getColumnIndex("date"));
			//date =  cursor.getString(0);
		}
		this.closeCursor(cursor);

		return date;
	}

	/**
	 * This method delete the category id according to grade
	 * @param grade
	 */
	public void deleteFromWordProblemCategoriesbyGrade(int grade){
		if(grade > 8)
			grade = 8;
		dbConn.delete("WordProblemCategoriesSpanish" , "grade_level=?" , new String[]{grade + ""});
	}

	/**
	 * This method delete the data from the WordProblemsSubCategoriesByCategoryId
	 * @param categoryList
	 */
	public void deleteFromWordProblemsSubCategoriesByCategoryId(ArrayList<CategoryListTransferObj> categoryList){
		String categories = "";

		for(  int  i = 0 ; i < categoryList.size() ; i ++)
		{
			if(i == categoryList.size() - 1)
				categories = categories + categoryList.get(i).getCid();
			else
				categories = categories + categoryList.get(i).getCid() + ",";
		}

		String where = "category_id in (" + categories + ")";
		dbConn.delete("WordProblemsSubCategoriesSpanish" , where, null);
	}

	/**
	 * This method insert the data into WordProblemCategories table from server
	 */
	public void insertIntoWordProblemCategories(ArrayList<CategoryListTransferObj> categoryList , int grade){

		if(grade > 8)
			grade = 8;
		for(int i = 0 ; i < categoryList.size() ; i ++ ){
			ContentValues contentValues = new ContentValues();
			contentValues.put("word_category_id", categoryList.get(i).getCid());
			contentValues.put("category_name", categoryList.get(i).getName());
			contentValues.put("grade_level", grade);

			if(dbConn.insert("WordProblemCategoriesSpanish", null, contentValues) > 0 ){
				//Log.e("SchoolCurriculumLearnignCenterimpl", " WordProblemLevelStar insert successfully!");
			}
		}
	}

	/**
	 * This method insert the data into WordProblemsSubCategories table
	 * @param categoryList
	 */
	public void insertIntoWordProblemsSubCategories(ArrayList<CategoryListTransferObj> categoryList){

		for(int i = 0 ; i < categoryList.size() ; i ++ ){
			for( int j = 0 ; j < categoryList.get(i).getSubCategories().size() ; j ++ ){
				ContentValues contentValues = new ContentValues();
				contentValues.put("category_id", categoryList.get(i).getCid());
				contentValues.put("sub_category_id", categoryList.get(i).getSubCategories().get(j).getId());
				contentValues.put("sub_category_name", categoryList.get(i).getSubCategories().get(j).getName());
				contentValues.put("video_url", categoryList.get(i).getSubCategories().get(j).getUrl());

				if(dbConn.insert("WordProblemsSubCategoriesSpanish", null, contentValues) > 0 ){
					//Log.e("SchoolCurriculumLearnignCenterimpl", " WordProblemLevelStar insert successfully!");
				}
			}
		}
	}

	/**
	 * This method insert the data into WordProblemsSubCategories table
	 * @param questionList
	 */
	public void insertIntoWordProblemsQuestions(ArrayList<WordProblemQuestionTransferObj> questionList){

		for(int i = 0 ; i < questionList.size() ; i ++ ){
			ContentValues contentValues = new ContentValues();
			contentValues.put("category_id", questionList.get(i).getCatId());
			contentValues.put("subcategory_id", questionList.get(i).getSubCatId());
			contentValues.put("question_id", questionList.get(i).getQuestionId());
			contentValues.put("question", questionList.get(i).getQuestion());
			contentValues.put("option1", questionList.get(i).getOpt1());
			contentValues.put("option2", questionList.get(i).getOpt2());
			contentValues.put("option3", questionList.get(i).getOpt3());
			contentValues.put("option4", questionList.get(i).getOpt4());
			contentValues.put("option5", questionList.get(i).getOpt5());
			contentValues.put("option6", questionList.get(i).getOpt6());
			contentValues.put("option7", questionList.get(i).getOpt7());
			contentValues.put("option8", questionList.get(i).getOpt8());

			contentValues.put("ansPrefix", questionList.get(i).getLeftAns());
			contentValues.put("ansSuffix", questionList.get(i).getRightAns());
			contentValues.put("image", questionList.get(i).getImage());
			contentValues.put("fill_in_blank_option", questionList.get(i).getFillIn());
			contentValues.put("avialableForTest", questionList.get(i).getForTest());
			contentValues.put("correctAnswers", questionList.get(i).getAns());

			if(dbConn.insert("WordProblemsQuestionsSpanish", null, contentValues) > 0 ){
				//Log.e("SchoolCurriculumLearnignCenterimpl", " WordProblemLevelStar insert successfully!");
			}
		}
	}

	/**
	 * This method get categories from database and return it corresponding to the player grade
	 * @param grade
	 */
	public ArrayList<CategoryListTransferObj>  getCategoriesForSchoolCurriculum(int grade){

		if(grade > 8)
			grade = 8;

		ArrayList<CategoryListTransferObj> categoryList = new ArrayList<CategoryListTransferObj>();

		String query = "select * from WordProblemCategoriesSpanish where grade_level = '" + grade +"' order by category_name";
		Cursor cursor =  dbConn.rawQuery(query, null);

		while(cursor.moveToNext()){
			CategoryListTransferObj categoryData = new CategoryListTransferObj();
			categoryData.setCid(cursor.getInt(cursor.getColumnIndex("word_category_id")));
			categoryData.setName(cursor.getString(cursor.getColumnIndex("category_name")));
			/*categoryData.setCid(cursor.getInt(0));
			categoryData.setName(cursor.getString(1));*/
			categoryList.add(categoryData);
		}

		this.closeCursor(cursor);
		return categoryList;
	}

    /**
     * Return the grade list for which categories downloaded
     * @return
     */
    public ArrayList<Integer> getGradeListForWhichCategoriesDownloaded(){
        ArrayList<Integer> gradeList = new ArrayList<Integer>();
        String query = "SELECT DISTINCT grade_level from WordProblemCategoriesSpanish";
        Cursor cursor =  dbConn.rawQuery(query, null);
        while(cursor.moveToNext()){
            gradeList.add(cursor.getInt(cursor.getColumnIndex("grade_level")));
        }
        this.closeCursor(cursor);
        return gradeList;
    }

	/**
	 * This method get categories from database and return it corresponding to the player grade
	 * @param
	 */
	public String getCategorieNameByCatIdForSchoolCurriculum(String catId){
		String catName = null;
		String query = "select category_name from WordProblemCategoriesSpanish " +
				"where word_category_id = '"
				+ catId +"'";

		Cursor cursor =  dbConn.rawQuery(query, null);
		if(cursor.moveToNext()){
			catName = cursor.getString(cursor.getColumnIndex("category_name"));
		}
		this.closeCursor(cursor);
		return catName;
	}

	/**
	 * this method return the sub category data according to the category id
	 * @param cetegoryId
	 * @return
	 */
	public ArrayList<SubCatergoryTransferObj> getSubChildCategories(int cetegoryId){

		ArrayList<SubCatergoryTransferObj> subCategoryList = new ArrayList<SubCatergoryTransferObj>();
		//String query = "select distinct * from WordProblemsSubCategoriesSpanish where category_id = '" + cetegoryId +"'";
		String query = "select distinct * from WordProblemsSubCategoriesSpanish where category_id = '" 
				+ cetegoryId +"' order by sub_category_name";
		Cursor cursor =  dbConn.rawQuery(query, null);

		while(cursor.moveToNext()){
			SubCatergoryTransferObj subcategoryData = new SubCatergoryTransferObj();
			subcategoryData.setId(cursor.getInt(cursor.getColumnIndex("sub_category_id")));
			subcategoryData.setName(cursor.getString(cursor.getColumnIndex("sub_category_name")));
			subcategoryData.setUrl(cursor.getString(cursor.getColumnIndex("video_url")));
			subcategoryData.setMainCatId(cetegoryId);
			subCategoryList.add(subcategoryData);
		}

		this.closeCursor(cursor);
		return subCategoryList;
	}

	public String getSchoolCurriculumSubCatNameByCatIdAndSubCatId(String catId , String subCatId){
		String subCatName = null;
		String query = "select sub_category_name from WordProblemsSubCategoriesSpanish where category_id = '" 
				+ catId +"' and sub_category_id = '" + subCatId +"'";
		Cursor cursor =  dbConn.rawQuery(query, null);

		if(cursor.moveToNext()){
			subCatName = cursor.getString(cursor.getColumnIndex("sub_category_name"));
		}

		this.closeCursor(cursor);
		return subCatName;
	}

	/**
	 * This method return the subcat name by id
	 * @param id
	 * @return
	 */
	public String getSubChildCatNameById(int id){
		String subCatName = "";
		String query = "select sub_category_name from WordProblemsSubCategoriesSpanish where sub_category_id = '" 
				+ id +"'";
		Cursor cursor =  dbConn.rawQuery(query, null);
		if(cursor.moveToNext()){
			subCatName = cursor.getString(0);
		}
		return subCatName;
	}

	/**
	 * This will check initially , if data into table exist or not means for blank table
	 */
	public boolean isWordProblemUpdateDetailsTableDataExist(){
		String query = "select * from WordProblemUpdateDetailsSpanish";
		Cursor cursor =  dbConn.rawQuery(query, null);

		if(cursor.moveToNext()){
			this.closeCursor(cursor);
			return true;
		}
		else{
			this.closeCursor(cursor);
			return false;
		}
	}

	/**
	 * This method get the question ids from database for displaying the quaeion for
	 * single friendzy
	 * @param userId
	 * @param playerId
	 * @param grade
	 */
	public ArrayList<Integer> getQuestionIdsFromDatabase(String userId , String playerId , int grade){

		if(grade > 8)
			grade = 8;

		ArrayList<Integer> questionIds = new ArrayList<Integer>();
		String query = "SELECT question_id, max(random()) AS r " +
				"FROM WordProblemsQuestionsSpanish where category_id IN " +
				"(Select word_category_id From WordProblemCategoriesSpanish " +
				"where grade_level = '" + grade + "') AND avialableForTest = 0 AND " +
				"question_id NOT IN (Select QuestionId FROM WordQuestionsPlayed WHERE " +
				"User_id = '" + userId + "' AND Player_id = '" + playerId + "') " +
				"GROUP BY category_id, subcategory_id";
		Cursor cursor = dbConn.rawQuery(query, null);
		while(cursor.moveToNext()){
			questionIds.add(cursor.getInt(cursor.getColumnIndex("question_id")));
		}
		this.closeCursor(cursor);

		return questionIds;
	}

	//changes for New Assessment
	/**
	 * This method check for this table is exist or not
	 * @return
	 */
	public boolean isWordAssessmentStandards(){
		Cursor cursor = dbConn.rawQuery("SELECT * FROM sqlite_master WHERE type = ? AND name = ?", 
				new String[] {"table", "WordAssessmentStandards"});
		if(cursor.moveToFirst())
		{
			cursor.close();
			return true;
		}
		else
		{
			cursor.close();
			return false;
		}
	}

	/**
	 * This method check for this table is exist or not
	 * @return
	 */
	public boolean isWordAssessmentSubCategoriesInfo(){
		Cursor cursor = dbConn.rawQuery("SELECT * FROM sqlite_master WHERE type = ? AND name = ?", 
				new String[] {"table", "WordAssessmentSubCategoriesInfo"});
		if(cursor.moveToFirst())
		{
			cursor.close();
			return true;
		}
		else
		{
			cursor.close();
			return false;
		}
	}

	/**
	 * This method check for this table is exist or not
	 * @return
	 */
	public boolean isWordAssessmentSubStandards(){
		Cursor cursor = dbConn.rawQuery("SELECT * FROM sqlite_master WHERE type = ? AND name = ?", 
				new String[] {"table", "WordAssessmentSubStandards"});
		if(cursor.moveToFirst())
		{
			cursor.close();
			return true;
		}
		else
		{
			cursor.close();
			return false;
		}
	}

	/**
	 * This method check for this table is exist or not
	 * @return
	 */
	public boolean isNewWordAssessmentTestResult(){
		Cursor cursor = dbConn.rawQuery("SELECT * FROM sqlite_master WHERE type = ? AND name = ?", 
				new String[] {"table", "NewWordAssessmentTestResult"});
		if(cursor.moveToFirst())
		{
			cursor.close();
			return true;
		}
		else
		{
			cursor.close();
			return false;
		}
	}

	/**
	 * This method check for this table is exist or not
	 * @return
	 */
	public boolean isWordAssessmentStandardsRoundInfo(){
		Cursor cursor = dbConn.rawQuery("SELECT * FROM sqlite_master WHERE type = ? AND name = ?", 
				new String[] {"table", "WordAssessmentStandardsRoundInfo"});
		if(cursor.moveToFirst())
		{
			cursor.close();
			return true;
		}
		else
		{
			cursor.close();
			return false;
		}
	}

	/**
	 * Check for homwwork image table
	 * @return
	 */
	public boolean isHomeWorkImagesTableExist(){
		Cursor cursor = dbConn.rawQuery("SELECT * FROM sqlite_master WHERE type = ? AND name = ?", 
				new String[] {"table", "homeworkimage"});
		if(cursor.moveToFirst())
		{
			cursor.close();
			return true;
		}
		else
		{
			cursor.close();
			return false;
		}
	}

	/**
	 * Create home work image table
	 */
	public void createHomeWorkImageTable(){
		String query = "CREATE TABLE homeworkimage (imagename VARCHAR(50) PRIMARY KEY)";
		dbConn.execSQL(query);
	}

	/**
	 * Check for LocalEarnedScore table
	 * @return
	 */
	public boolean isLocalEarnedScoreTableExist(){
		Cursor cursor = dbConn.rawQuery("SELECT * FROM sqlite_master WHERE type = ? AND name = ?", 
				new String[] {"table", "LocalEarnedScore"});
		if(cursor.moveToFirst())
		{
			cursor.close();
			return true;
		}
		else
		{
			cursor.close();
			return false;
		}
	}

	/**
	 * Check for homework table exist
	 * @return
	 */
	public boolean isHomeworkTableExist(){
		Cursor cursor = dbConn.rawQuery("SELECT * FROM sqlite_master WHERE type = ? AND name = ?", 
				new String[] {"table", "Homework"});
		if(cursor.moveToFirst())
		{
			cursor.close();
			return true;
		}
		else
		{
			cursor.close();
			return false;
		}
	}

	/**
	 * Check for practiceSkillHW table exist
	 * @return
	 */
	public boolean isPracticeSkillHWExist(){
		Cursor cursor = dbConn.rawQuery("SELECT * FROM sqlite_master WHERE type = ? AND name = ?", 
				new String[] {"table", "PracticeSkillHW"});
		if(cursor.moveToFirst())
		{
			cursor.close();
			return true;
		}
		else
		{
			cursor.close();
			return false;
		}
	}

	/**
	 * Check for WordProblemHW table exist
	 * @return
	 */
	public boolean isWordProblemHWExist(){
		Cursor cursor = dbConn.rawQuery("SELECT * FROM sqlite_master WHERE type = ? AND name = ?", 
				new String[] {"table", "WordProblemHW"});
		if(cursor.moveToFirst())
		{
			cursor.close();
			return true;
		}
		else
		{
			cursor.close();
			return false;
		}
	}

	/**
	 * Check for CustomeHW table exist
	 * @return
	 */
	public boolean isCustomeHWExist(){
		Cursor cursor = dbConn.rawQuery("SELECT * FROM sqlite_master WHERE type = ? AND name = ?", 
				new String[] {"table", "CustomeHW"});
		if(cursor.moveToFirst())
		{
			cursor.close();
			return true;
		}
		else
		{
			cursor.close();
			return false;
		}
	}

	/**
	 * Create LocalEarnedScore table
	 */
	public void createLocalEarnedScoreTable(){
		String query = "CREATE TABLE LocalEarnedScore (user_id INTEGER NOT NULL , " +
				"player_id INTEGER NOT NULL ,points INTEGER NOT NULL ," +
				"coins INTEGER NOT NULL ," +
				"level INTEGER NOT NULL  DEFAULT (0))";
		dbConn.execSQL(query);
	}

	/**
	 * Create homework table
	 */
	public void createHomeworkTable(){
		String query = "CREATE TABLE Homework (user_id INTEGER NOT NULL , " +
				"player_id INTEGER NOT NULL , homework_json TEXT , " +
				"CONSTRAINT pk PRIMARY KEY (user_id , player_id))";
		dbConn.execSQL(query); 
	}

	/**
	 * Create homework table
	 */
	public void createPracticeSkillHWTable(){
		String query = "CREATE TABLE PracticeSkillHW (time VARCHAR(10), score VARCHAR(10), " +
				"homeworkId VARCHAR(10), userId VARCHAR(10), playerId VARCHAR(10), questionsCount" +
				" VARCHAR(5), problems TEXT, points VARCHAR(10), coins VARCHAR(10), mathCategoryId" +
				" VARCHAR(10), mathSubCategoryId VARCHAR(10))";
		dbConn.execSQL(query); 
	}

	/**
	 * Create homework table
	 */
	public void createWordProblemHWTable(){
		String query = "CREATE TABLE WordProblemHW (userId VARCHAR(10), playerId VARCHAR(10), " +
				"problems TEXT, categoryId VARCHAR(10), subCategoryId VARCHAR(10), time" +
				" VARCHAR(10), score VARCHAR(10), homeworkId VARCHAR(10), points VARCHAR(10)," +
				"coins VARCHAR(10))";
		dbConn.execSQL(query); 
	}

	/**
	 * Create homework table
	 */
	public void createCustomeHWTable(){
		String query = "CREATE TABLE CustomeHW (userId VARCHAR(10), playerId VARCHAR(10)," +
				" homeworkId VARCHAR(10), customHwId VARCHAR(10), numOfQuestions VARCHAR(5), " +
				"score VARCHAR(10), questions TEXT)";
		dbConn.execSQL(query); 
	}
	
	/**
	 * This method create the table for word problem question update date details
	 */
	public void createWordAssessmentStandards(){
		String query = "CREATE TABLE WordAssessmentStandards (id INTEGER , name VARCHAR(400) DEFAULT (null) , grade INTEGER(3))";
		dbConn.execSQL(query);
	}

	/**
	 * This method create the table for word problem question update date details
	 */
	public void createWordAssessmentSubCategoriesInfo(){
		String query = "CREATE TABLE WordAssessmentSubCategoriesInfo (standard_id INTEGER(4) , sub_standard_id INTEGER(3), word_categ_id INTEGER(5), sub_categories VARCHAR)";
		dbConn.execSQL(query);
	}

	/**
	 * This method create the table for word problem question update date details
	 */
	public void createWordAssessmentSubStandards(){
		String query = "CREATE TABLE WordAssessmentSubStandards (stdId INTEGER(5), subStdId INTEGER(3), name VARCHAR)";
		dbConn.execSQL(query);
	}

	/**
	 * This method create the table for word problem question update date details
	 */
	public void createNewWordAssessmentTestResult(){
		String query = "CREATE TABLE NewWordAssessmentTestResult (user_id INTEGER,player_id INTEGER,grade INTEGER(2),problems VARCHAR(1000) DEFAULT (null) ,isCompleted BOOL,date DATETIME DEFAULT (null) ,questionsRemainToPlay VARCHAR(500), standardId INTEGER, allQuestionIds VARCHAR ,correctScore INTEGER)";
		dbConn.execSQL(query);
	}

	/**
	 * This method create the table for word problem question update date details
	 */
	public void createWordAssessmentStandardsRoundInfo(){
		String query = "CREATE TABLE WordAssessmentStandardsRoundInfo (userId INTEGER, playerId INTEGER, standardId INTEGER, round INTEGER , last_played_date DATETIME)";
		dbConn.execSQL(query);
	}
	//end changes for New Assessment

	/**
	 * This method return the questions ids from database for New Assessment test
	 * @param catId
	 * @param subCatIds
	 * @param aleadyFatchQuationIds
	 * @return
	 */
	public ArrayList<Integer> getQuestionId(int catId , String subCatIds ,
			ArrayList<Integer> aleadyFatchQuationIds){
		ArrayList<Integer> questionIds = new ArrayList<Integer>();

		String stringFromArray = "";

		for(  int  i = 0 ; i < aleadyFatchQuationIds.size() ; i ++)
		{
			if(i == aleadyFatchQuationIds.size() - 1)
				stringFromArray = stringFromArray + aleadyFatchQuationIds.get(i);
			else
				stringFromArray = stringFromArray + aleadyFatchQuationIds.get(i) + ",";
		}

		String query = "SELECT question_id, max(random()) AS r FROM WordProblemsQuestionsSpanish " +
				"where (category_id = '" + catId + 
				"'And subcategory_id IN (" + subCatIds + 
				")) AND question_id NOT in (" + stringFromArray + 
				") GROUP BY category_id, subcategory_id";
		Cursor cursor = dbConn.rawQuery(query, null);
		while(cursor.moveToNext()){
			questionIds.add(cursor.getInt(cursor.getColumnIndex("question_id")));
		}
		this.closeCursor(cursor);
		return questionIds;
	}

    /**
     * Create homework table
     */
    public void createUserLinksTable(){
        String query = "CREATE TABLE UserLinks (userId VARCHAR(10), playerId VARCHAR(10)," +
                " homeworkId VARCHAR(10), customHwId VARCHAR(10), questionNo VARCHAR(10), " +
                "linkString TEXT)";
        dbConn.execSQL(query);
    }

    /**
     * Check for practiceSkillHW table exist
     * @return
     */
    public boolean iUserLinksExist(){
        Cursor cursor = dbConn.rawQuery("SELECT * FROM sqlite_master WHERE type = ? AND name = ?",
                new String[] {"table", "UserLinks"});
        if(cursor.moveToFirst())
        {
            cursor.close();
            return true;
        }
        else
        {
            cursor.close();
            return false;
        }
    }
}
