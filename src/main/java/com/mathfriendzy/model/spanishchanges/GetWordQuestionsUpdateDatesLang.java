package com.mathfriendzy.model.spanishchanges;

import java.util.ArrayList;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.mathfriendzy.controller.learningcenter.NewLearnignCenter;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.UpdatedInfoTransferObj;
import com.mathfriendzy.utils.CommonUtils;

/**
 * This asyncktask get date details for Spanish word problem questions
 * @author Yashwant Singh
 *
 */
public class GetWordQuestionsUpdateDatesLang extends AsyncTask<Void, Void, ArrayList<UpdatedInfoTransferObj>>{

	private int lang  = 1;
	private ProgressDialog pd;
	private Context context;
	private int grade;
	private boolean isFirstTimeLoaded;
	private int callingActivity = 0;
	public GetWordQuestionsUpdateDatesLang(int lang , Context context , boolean isFirstTimeLoaded
			,int grade , int callingActivity){
		this.lang = lang;
		this.context = context;
		this.isFirstTimeLoaded = isFirstTimeLoaded;
		this.grade = grade;
		this.callingActivity = callingActivity;
	}

	@Override
	protected void onPreExecute() {
		if(!isFirstTimeLoaded){
			pd = CommonUtils.getProgressDialog(context);
			pd.show();
		}
		super.onPreExecute();
	}

	@Override
	protected ArrayList<UpdatedInfoTransferObj> doInBackground(Void... params) {
		SpanishServerOperation serverObj = new SpanishServerOperation();
		ArrayList<UpdatedInfoTransferObj> updatedList = serverObj.getUpdateDateDetaile(lang);
		return updatedList;
	}

	@Override
	protected void onPostExecute(ArrayList<UpdatedInfoTransferObj> updatedList) {
		if(!isFirstTimeLoaded){
			pd.cancel();

			if(updatedList != null){
				SpanishChangesImpl spanishImplObj = new SpanishChangesImpl(context);
				spanishImplObj.openConn();
				String updatedDateFromDatabase = spanishImplObj.getUpdatedDate(grade);
				if(callingActivity == 0){//for main Activity
					if(CommonUtils.isUpdateDateChange(updatedDateFromDatabase,grade,updatedList))
					{
						String serverUpdatedDate = CommonUtils.getServerDate(grade , updatedList);
						spanishImplObj.updateNewUpdatedDateFromServer(grade, serverUpdatedDate);

						MainActivity x = new MainActivity();
						x.new GetWordProblemQuestion(grade, context).execute(null,null,null);

					}else{
						context.startActivity(new Intent(context , NewLearnignCenter.class));
					}
				}
				spanishImplObj.closeConn();
			}else{
				CommonUtils.showInternetDialog(context);
			}
		}

		super.onPostExecute(updatedList);
	}
}
