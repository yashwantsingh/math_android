package com.mathfriendzy.model.spanishchanges;

import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mathfriendzy.model.schoolcurriculum.learnigncenter.UpdatedInfoTransferObj;
import com.mathfriendzy.utils.CommonUtils;

import android.util.Log;

public class SpanishServerOperation {

	private final String TAG = this.getClass().getSimpleName();

	/**
	 * This method getUpdated date details from server for Spanish Language
	 * @param lang
	 */
	public ArrayList<UpdatedInfoTransferObj> getUpdateDateDetaile(int lang){
		String action = "getWordQuestionsUpdateDatesLang";
		String strURL =  COMPLETE_URL  + "action=" 	+ 	action 	+ "&"
				+ "lang="   + lang ;
		return this.parseUpdateDateDetails(CommonUtils.readFromURL(strURL));
	}

	/**
	 * This method parse the jsonString
	 * @param jsonString
	 */
	private ArrayList<UpdatedInfoTransferObj> parseUpdateDateDetails(String jsonString){
		//Log.e(TAG, "inside parseUpdateDateDetails josnString " + jsonString);

		if(jsonString != null){
			ArrayList<UpdatedInfoTransferObj> updatedList  = new ArrayList<UpdatedInfoTransferObj>();
			try 
			{
				JSONObject jsonObj = new JSONObject(jsonString);
				JSONArray updatedArray = jsonObj.getJSONArray("data");
				for(int j = 0 ; j < updatedArray.length() ; j ++ ){
					JSONObject jsonjUpdatedObj = updatedArray.getJSONObject(j);
					Iterator<String> iterator = jsonjUpdatedObj.keys();

					UpdatedInfoTransferObj updatedData = new UpdatedInfoTransferObj();
					int grade = Integer.parseInt(iterator.next());
					updatedData.setGrade(grade);
					updatedData.setUpdatedDate(jsonjUpdatedObj.getString(grade + ""));

					updatedList.add(updatedData);
				}
			} catch (JSONException e) {
				Log.e(TAG, "inside Error while parsing : " + e.toString());
			}

			return updatedList;
		}else{
			return null;
		}
	}
}
