package com.mathfriendzy.model.teacherStudents;

import android.content.Context;

import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.utils.CommonUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;

/**
 * This Get Teacher Student From the server
 * @author Yashwant Singh
 *
 */
public class GetTeacherStudents 
{

	private Context context = null;

	/**
	 *Constructor 
	 * @param context
	 */
	public GetTeacherStudents(Context context)
	{
		this.context = context;
	}

	/**
	 * This method return the students from the server
	 * @param userId
	 * @param offset
	 * @return
	 */
	public ArrayList<UserPlayerDto> getStudents(String userId , int offset)
	{
		String action = "getStudents";

		String strURL = COMPLETE_URL + "action=" + action + "&"
				+ "userId=" + userId + "&"
				+ "offset=" + offset;
		//Log.e("USRL", strURL);
		return this.parseJson(CommonUtils.readFromURL(strURL));

	}

	/**
	 * This method parse json String for student data
	 * @param jsonString
	 * @return
	 */
	private ArrayList<UserPlayerDto> parseJson(String jsonString)
	{
		ArrayList<UserPlayerDto> userPlayer = new ArrayList<UserPlayerDto>();

		//Log.e("Json", jsonString);

		try
		{
			JSONObject jObject = new JSONObject(jsonString);
			try
			{
				JSONArray jsonArray = jObject.getJSONArray("data");

				for( int i = 0 ; i < jsonArray.length() ; i ++ )
				{
					JSONObject jsonObject = jsonArray.getJSONObject(i);

					UserPlayerDto userPlayerDto = new UserPlayerDto();
					userPlayerDto.setFirstname(jsonObject.getString("fName"));
					userPlayerDto.setLastname(jsonObject.getString("lName"));
					userPlayerDto.setSchoolId(jsonObject.getString("schoolId"));
					userPlayerDto.setSchoolName(jsonObject.getString("schoolName"));
					userPlayerDto.setGrade(jsonObject.getString("grade"));
					userPlayerDto.setTeacherUserId(jsonObject.getString("teacherUserId"));
					userPlayerDto.setTeacherFirstName(jsonObject.getString("teacherFirstName"));
					userPlayerDto.setTeacheLastName(jsonObject.getString("teacherLastName"));
					userPlayerDto.setIndexOfAppearance(jsonObject.getString("indexOfAppearance"));
					userPlayerDto.setParentUserId(jsonObject.getString("parentUserId"));
					userPlayerDto.setPlayerid(jsonObject.getString("playerId"));
					userPlayerDto.setCompletelavel(jsonObject.getString("competeLevel"));
					userPlayerDto.setImageName(jsonObject.getString("profileImageId"));
					userPlayerDto.setCoin(jsonObject.getString("coins"));
					userPlayerDto.setPoints(jsonObject.getString("points"));
					userPlayerDto.setCity(jsonObject.getString("city"));
					userPlayerDto.setStateName(jsonObject.getString("state"));
					userPlayerDto.setUsername(jsonObject.getString("userName"));
					userPlayer.add(userPlayerDto);
				}
			}
			catch(JSONException e1)
			{
				return userPlayer;
			}
		}
		catch (JSONException e) 
		{
			e.printStackTrace();
		}

		return userPlayer;
	}

	/**
	 * This method get the teacher students details with password
	 * @param userId
	 * @param offset
	 */
	public ArrayList<UserPlayerDto> getStudentsWithPassword(String userId , int offset){
		String action = "getStudentsWithPassword";

		String strURL = COMPLETE_URL + "action=" + action + "&"
				+ "userId=" + userId; /*+ "&"
				+ "offset=" + offset;*/
		return this.parseStudentsWithPasswordJsonString(CommonUtils.readFromURL(strURL));
	}

    /**
     * This method get the teacher students details with password
     * @param userId
     * @param offset
     */
    public ArrayList<UserPlayerDto> getStudentsWithPassword(String userId , int offset , String year){
        String action = "getStudentsWithPassword";

        String strURL = COMPLETE_URL + "action=" + action + "&"
                + "userId=" + userId + "&"
				+ "year=" + year;
        return this.parseStudentsWithPasswordJsonString(CommonUtils.readFromURL(strURL));
    }

	/**
	 * This method parse the json string for teacher player
	 * @param jsonString
	 * @return
	 */
	private ArrayList<UserPlayerDto> parseStudentsWithPasswordJsonString(String jsonString){
			
		//Log.e("GetTeacherStudents", "response " + jsonString);
		
		ArrayList<UserPlayerDto> userPlayer = new ArrayList<UserPlayerDto>();
	
		try
		{
			JSONObject jObject = new JSONObject(jsonString);
			try
			{
				JSONArray jsonArray = jObject.getJSONArray("data");

				for( int i = 0 ; i < jsonArray.length() ; i ++ )
				{
					JSONObject jsonObject = jsonArray.getJSONObject(i);
					
					UserPlayerDto userPlayerDto = new UserPlayerDto();
					
					userPlayerDto.setPlayerid(jsonObject.getString("playerId"));
					userPlayerDto.setFirstname(jsonObject.getString("fName"));
					userPlayerDto.setLastname(jsonObject.getString("lName"));
					userPlayerDto.setGrade(jsonObject.getString("grade"));
					userPlayerDto.setTeacherUserId(jsonObject.getString("teacherUserId"));
					userPlayerDto.setParentUserId(jsonObject.getString("parentUserId"));
					userPlayerDto.setUsername(jsonObject.getString("userName"));
					userPlayerDto.setPassword(jsonObject.getString("password"));

                    if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject , "classId")){
                        try{
                            userPlayerDto.setClassId(jsonObject.getInt("classId"));
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
					userPlayer.add(userPlayerDto);
				}
			}
			catch(JSONException e1)
			{
				return userPlayer;
			}
		}
		catch (JSONException e) 
		{
			e.printStackTrace();
		}

		return userPlayer;
	}
}
