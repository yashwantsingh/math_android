package com.mathfriendzy.model.managetutor.mystudents;

/**
 * Created by root on 23/4/15.
 */
public class GetTutoringDetailForTeacherParam {
    private String action;
    private String userId;
    private String playerId;

    private boolean isFirstTime;
    private int isTutorList;
    private int offset;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public boolean isFirstTime() {
        return isFirstTime;
    }

    public void setFirstTime(boolean isFirstTime) {
        this.isFirstTime = isFirstTime;
    }

    public int getIsTutorList() {
        return isTutorList;
    }

    public void setIsTutorList(int isTutorList) {
        this.isTutorList = isTutorList;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }
}
