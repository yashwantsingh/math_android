package com.mathfriendzy.model.managetutor.mystudents;

import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.serveroperation.HttpResponseBase;

import java.util.ArrayList;

/**
 * Created by root on 21/4/15.
 */
public class GetMyStuentWithPasswordResponse extends HttpResponseBase{
    private String response;
    private String startDate;
    private String endDate;
    private ArrayList<String> gradeList;
    private ArrayList<UserPlayerDto> userPlayer;
    private ArrayList<Integer> intGradeList;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public ArrayList<UserPlayerDto> getUserPlayer() {
        return userPlayer;
    }

    public void setUserPlayer(ArrayList<UserPlayerDto> userPlayer) {
        this.userPlayer = userPlayer;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public ArrayList<String> getGradeList() {
        return gradeList;
    }

    public void setGradeList(ArrayList<String> gradeList) {
        this.gradeList = gradeList;
    }

    public ArrayList<Integer> getIntGradeList() {
        return intGradeList;
    }

    public void setIntGradeList(ArrayList<Integer> intGradeList) {
        this.intGradeList = intGradeList;
    }
}
