package com.mathfriendzy.model.managetutor.mystudents;

/**
 * Created by root on 28/4/15.
 */
public class SendEmailToOtherTeacherParam {
    private String action;
    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
