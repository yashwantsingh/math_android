package com.mathfriendzy.model.managetutor.mystudents;

/**
 * Created by root on 21/4/15.
 */
public class GetStudentWithPasswordParam {

    private String action;
    private String userId;
    private int offset;
    private String startDate;
    private String endDate;
    private String timeZone;
    private String getTime;

    //class title changes
    private String year;

    private int limit;
    private String classId;
    private boolean isForStudentAccount;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public boolean isForStudentAccount() {
        return isForStudentAccount;
    }

    public void setForStudentAccount(boolean isForStudentAccount) {
        this.isForStudentAccount = isForStudentAccount;
    }

    public String getGetTime() {
        return getTime;
    }

    public void setGetTime(String getTime) {
        this.getTime = getTime;
    }
}
