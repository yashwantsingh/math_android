package com.mathfriendzy.model.managetutor.mystudents;

import com.mathfriendzy.serveroperation.HttpResponseBase;

/**
 * Created by root on 28/4/15.
 */
public class GetHomeworkDetailsForChatRequestIdResponse extends HttpResponseBase {

    private String result;
    private String hwDate;
    private String hwTitle;
    private String problem;

    public String getHwDate() {
        return hwDate;
    }

    public void setHwDate(String hwData) {
        this.hwDate = hwData;
    }

    public String getHwTitle() {
        return hwTitle;
    }

    public void setHwTitle(String hwTitle) {
        this.hwTitle = hwTitle;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
