package com.mathfriendzy.model.managetutor.mystudents;

import com.mathfriendzy.model.tutor.GetDrawingPointForChatResponse;
import com.mathfriendzy.serveroperation.HttpResponseBase;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by root on 23/4/15.
 */
public class GetStidentsTutoringDetailForTeacherResponse extends HttpResponseBase implements Serializable{

    private String response;

    private int requestId;
    private String userId;
    private String playerId;
    private String playerFName;
    private String playerLName;
    private int grade;
    private String chatId;
    private String chatUserName;
    private String reqDate;
    private String message;
    private int isConnected;
    private String tutorUid;
    private String tutorPid;
    private String chatDialogId;
    private int isAnonymous;
    private int timeSpent;
    private int rating;
    private String teacherName;
    private String schoolName;
    private String studentImage;
    private String tutorImage;

    private String teacherEmail;
    private String workAreaImageName;
    private String lastUpdatedByTutor;

    private ArrayList<GetStidentsTutoringDetailForTeacherResponse> tutorList;
    private ArrayList<GetStidentsTutoringDetailForTeacherResponse> studentList;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getPlayerFName() {
        return playerFName;
    }

    public void setPlayerFName(String playerFName) {
        this.playerFName = playerFName;
    }

    public String getPlayerLName() {
        return playerLName;
    }

    public void setPlayerLName(String playerLName) {
        this.playerLName = playerLName;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getChatUserName() {
        return chatUserName;
    }

    public void setChatUserName(String chatUserName) {
        this.chatUserName = chatUserName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getReqDate() {
        return reqDate;
    }

    public void setReqDate(String reqDate) {
        this.reqDate = reqDate;
    }

    public int getIsConnected() {
        return isConnected;
    }

    public void setIsConnected(int isConnected) {
        this.isConnected = isConnected;
    }

    public String getTutorUid() {
        return tutorUid;
    }

    public void setTutorUid(String tutorUid) {
        this.tutorUid = tutorUid;
    }

    public String getTutorPid() {
        return tutorPid;
    }

    public void setTutorPid(String tutorPid) {
        this.tutorPid = tutorPid;
    }

    public String getChatDialogId() {
        return chatDialogId;
    }

    public void setChatDialogId(String chatDialogId) {
        this.chatDialogId = chatDialogId;
    }

    public int getIsAnonymous() {
        return isAnonymous;
    }

    public void setIsAnonymous(int isAnonymous) {
        this.isAnonymous = isAnonymous;
    }

    public int getTimeSpent() {
        return timeSpent;
    }

    public void setTimeSpent(int timeSpent) {
        this.timeSpent = timeSpent;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getStudentImage() {
        return studentImage;
    }

    public void setStudentImage(String studentImage) {
        this.studentImage = studentImage;
    }

    public String getTutorImage() {
        return tutorImage;
    }

    public void setTutorImage(String tutorImage) {
        this.tutorImage = tutorImage;
    }

    public ArrayList<GetStidentsTutoringDetailForTeacherResponse> getTutorList() {
        return tutorList;
    }

    public void setTutorList(ArrayList<GetStidentsTutoringDetailForTeacherResponse> tutorList) {
        this.tutorList = tutorList;
    }

    public ArrayList<GetStidentsTutoringDetailForTeacherResponse> getStudentList() {
        return studentList;
    }

    public void setStudentList(ArrayList<GetStidentsTutoringDetailForTeacherResponse> studentList) {
        this.studentList = studentList;
    }

    public String getTeacherEmail() {
        return teacherEmail;
    }

    public void setTeacherEmail(String teacherEmail) {
        this.teacherEmail = teacherEmail;
    }

    public String getWorkAreaImageName() {
        return workAreaImageName;
    }

    public void setWorkAreaImageName(String workAreaImageName) {
        this.workAreaImageName = workAreaImageName;
    }

    public String getLastUpdatedByTutor() {
        return lastUpdatedByTutor;
    }

    public void setLastUpdatedByTutor(String lastUpdatedByTutor) {
        this.lastUpdatedByTutor = lastUpdatedByTutor;
    }
}
