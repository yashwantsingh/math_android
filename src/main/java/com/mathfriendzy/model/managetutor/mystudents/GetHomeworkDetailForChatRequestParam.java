package com.mathfriendzy.model.managetutor.mystudents;

/**
 * Created by root on 28/4/15.
 */
public class GetHomeworkDetailForChatRequestParam {
    private String action;
    private int chatId;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getChatId() {
        return chatId;
    }

    public void setChatId(int chatId) {
        this.chatId = chatId;
    }
}
