package com.mathfriendzy.model.country;

import com.mathfriendzy.serveroperation.HttpResponseBase;

import java.util.ArrayList;

/**
 * Created by root on 4/7/16.
 */
public class CountryCityResponse extends HttpResponseBase{
    private String result;
    private ArrayList<String> cities;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public ArrayList<String> getCities() {
        return cities;
    }

    public void setCities(ArrayList<String> cities) {
        this.cities = cities;
    }
}
