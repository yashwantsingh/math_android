package com.mathfriendzy.model.country;

/**
 * Created by root on 4/7/16.
 */
public class GetCountryCityParam {
    private String action;
    private String countryISO;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getCountryISO() {
        return countryISO;
    }

    public void setCountryISO(String countryISO) {
        this.countryISO = countryISO;
    }
}
