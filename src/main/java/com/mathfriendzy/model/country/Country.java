package com.mathfriendzy.model.country;

import java.util.ArrayList;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.mathfriendzy.database.Database;
import static com.mathfriendzy.utils.ICommonUtils.COUNTRY_FLAG;

public class Country 
{
	private final String COUNTRY_ID 		= "ID";
	private final String COUNTRY_NAME 		= "COUNTRY_NAME";
	private final String COUNTRY_TABLE_NAME = "World_Countries";
	private final String COUNTRY_ISO		= "ISO_3166";
	private final String TAG  = this.getClass().getSimpleName();
	
	private SQLiteDatabase dbConn = null;
	
	/**
	 * This method return the country list
	 * @param context
	 * @return
	 */
	public ArrayList<String> getCountryName(Context context)
	{
		if(COUNTRY_FLAG)
			Log.e(TAG, "inside getCountryName()");
		
		ArrayList<String> countryList = new ArrayList<String>();
		
		this.openConn(context);		
		
		String query = "select " + COUNTRY_NAME + " from " + COUNTRY_TABLE_NAME;
		Cursor cursor = dbConn.rawQuery(query, null);
		
		while(cursor.moveToNext())
		{		
			countryList.add(cursor.getString(cursor.getColumnIndex(COUNTRY_NAME)));
		}
		if(cursor != null)
			cursor.close();
		this.closeConn();
		
		if(COUNTRY_FLAG)
			Log.e(TAG, "outside getCountryName()");
		
		return countryList;
	}
	
	/**
	 * Return ISO for corresponding country
	 * @param countryName
	 * @param context
	 * @return
	 */
	public String getCountryIsoByCountryName(String countryName ,Context context)
	{
		if(COUNTRY_FLAG)
			Log.e(TAG, "inside getCountryIsoByCountryName()");
		
		String countryIso = "";
		this.openConn(context);		
		String query = "select " + COUNTRY_ISO + " from " + COUNTRY_TABLE_NAME + " where " + COUNTRY_NAME + " = '" + countryName + "'" ;
		Cursor cursor = dbConn.rawQuery(query, null);
		
		if(cursor.moveToNext())
		{		
			countryIso = cursor.getString(cursor.getColumnIndex(COUNTRY_ISO));
		}
		if(cursor != null)
			cursor.close();
		this.closeConn();
		
		if(COUNTRY_FLAG)
			Log.e(TAG, "outside getCountryIsoByCountryName()");
		
		return countryIso;
	}
	
	/**
	 * Return ISO for corresponding country
	 * @param countryName
	 * @param context
	 * @return
	 */
	public String getCountryNameByCountryIso(String countryIso ,Context context)
	{
		if(COUNTRY_FLAG)
			Log.e(TAG, "inside getCountryIsoByCountryName()");
		
		String countryName = "";
		this.openConn(context);		
		String query = "select " + COUNTRY_NAME + " from " + COUNTRY_TABLE_NAME + " where " + COUNTRY_ISO + " = '" + countryIso + "'" ;
		Cursor cursor = dbConn.rawQuery(query, null);
		
		if(cursor.moveToNext())
		{		
			countryName = cursor.getString(cursor.getColumnIndex(COUNTRY_NAME));
		}
		if(cursor != null)
			cursor.close();
		this.closeConn();
		
		if(COUNTRY_FLAG)
			Log.e(TAG, "outside getCountryIsoByCountryName()");
		
		return countryName;
	}
	/**
	 * Return ISO for corresponding country
	 * @param countryId
	 * @param context
	 * @return
	 */
	public String getCountryIsoByCountryId(String countryId ,Context context)
	{
		if(COUNTRY_FLAG)
			Log.e(TAG, "inside getCountryIsoByCountryName()");
		
		String countryIso = "";
		this.openConn(context);	
		
		String query = "select " + COUNTRY_ISO + " from " + COUNTRY_TABLE_NAME + " where " + COUNTRY_ID  + " = '" + countryId + "'" ;
		Cursor cursor = dbConn.rawQuery(query, null);
		
		if(cursor.moveToNext())
		{		
			countryIso = cursor.getString(cursor.getColumnIndex(COUNTRY_ISO));
		}
		if(cursor != null)
			cursor.close();
		this.closeConn();
		
		if(COUNTRY_FLAG)
			Log.e(TAG, "outside getCountryIsoByCountryName()");
		
		return countryIso;
	}
	
	
	/**
	 * This method return the country ID by country Name
	 * @param countryName
	 * @param context
	 * @return
	 */
	public String getCountryIdByCountryName(String countryName ,Context context)
	{
		if(COUNTRY_FLAG)
			Log.e(TAG, "inside getCountryIdByCountryName()");
		
		String countryId = "";
		this.openConn(context);		
		String query = "select " + COUNTRY_ID + " from " + COUNTRY_TABLE_NAME + " where " + COUNTRY_NAME + " = '" + countryName + "'" ;
		Cursor cursor = dbConn.rawQuery(query, null);
		
		if(cursor.moveToNext())
		{		
			countryId = cursor.getString(cursor.getColumnIndex(COUNTRY_ID));
		}
		if(cursor != null)
			cursor.close();
		this.closeConn();
		
		if(COUNTRY_FLAG)
			Log.e(TAG, "outside getCountryIdByCountryName()");
		
		return countryId;
	}
	
	/**
	 * This methos return the country name by country id
	 * @param countryID
	 * @param context
	 * @return
	 */
	public String getCountryNameByCountryId(String countryID, Context context)
	{
		if(COUNTRY_FLAG)
			Log.e(TAG, "inside getCountryIdByCountryName()");
		
		String countryName = "";
		this.openConn(context);		
		String query = "select " + COUNTRY_NAME + " from " + COUNTRY_TABLE_NAME + " where " + COUNTRY_ID + " = '" + countryID + "'" ;
		Cursor cursor = dbConn.rawQuery(query, null);
		
		if(cursor.moveToNext())
		{		
			countryName = cursor.getString(cursor.getColumnIndex(COUNTRY_NAME));
		}
		if(cursor != null)
			cursor.close();
		this.closeConn();
		
		if(COUNTRY_FLAG)
			Log.e(TAG, "outside getCountryIdByCountryName()");
		
		return countryName;
	}
	
	/**
	 * This method open the connection with the database
	 * @param context
	 */
	private void openConn(Context context)
	{
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
	}
	
	/**
	 * This method close the connection with the database
	 */
	private void closeConn()
	{
		if(dbConn != null)
			dbConn.close();
	}
}
