package com.mathfriendzy.model.singleFriendzy;

public class EquationFromServerTransferObj 
{
	private String userId ; 
	private String playerId;
	private int isFakePlayer;
	private int mathEquationId;
	private int lap;
	private int points;
	private int isAnswerCorrect;
	private double timeTakenToAnswer;
	//for multifriendzy
	private String setIsFakePlayer;
	
	
	public double getTimeTakenToAnswer() {
		return timeTakenToAnswer;
	}
	
	public String getSetIsFakePlayer() {
		return setIsFakePlayer;
	}
	public void setSetIsFakePlayer(String setIsFakePlayer) {
		this.setIsFakePlayer = setIsFakePlayer;
	}
	public void setTimeTakenToAnswer(double timeTakenToAnswer) {
		this.timeTakenToAnswer = timeTakenToAnswer;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public int getIsFakePlayer() {
		return isFakePlayer;
	}
	public void setIsFakePlayer(int isFakePlayer) {
		this.isFakePlayer = isFakePlayer;
	}
	public int getMathEquationId() {
		return mathEquationId;
	}
	public void setMathEquationId(int mathEquationId) {
		this.mathEquationId = mathEquationId;
	}
	public int getLap() {
		return lap;
	}
	public void setLap(int lap) {
		this.lap = lap;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	public int getIsAnswerCorrect() {
		return isAnswerCorrect;
	}
	public void setIsAnswerCorrect(int isAnswerCorrect) {
		this.isAnswerCorrect = isAnswerCorrect;
	}
	
	
}
