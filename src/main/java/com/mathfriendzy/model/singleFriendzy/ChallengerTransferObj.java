package com.mathfriendzy.model.singleFriendzy;

public class ChallengerTransferObj 
{
	private String first;
	private String last;
	private String playerId;
	private String userId;
	private String sum;
	private String countryIso;
	private int isFakePlayer;
	
	public String getFirst() {
		return first;
	}
	public void setFirst(String first) {
		this.first = first;
	}
	public String getLast() {
		return last;
	}
	public void setLast(String last) {
		this.last = last;
	}
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getSum() {
		return sum;
	}
	public void setSum(String sum) {
		this.sum = sum;
	}
	public String getCountryIso() {
		return countryIso;
	}
	public void setCountryIso(String countryIso) {
		this.countryIso = countryIso;
	}
	public int getIsFakePlayer() {
		return isFakePlayer;
	}
	public void setIsFakePlayer(int isFakePlayer) {
		this.isFakePlayer = isFakePlayer;
	}
	
}
