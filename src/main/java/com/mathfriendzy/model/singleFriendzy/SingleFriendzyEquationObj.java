package com.mathfriendzy.model.singleFriendzy;

public class SingleFriendzyEquationObj 
{
	private int equationsId   = 0;
	
	private String operator                		        = null;
	private String number1Str                     		= null;
	private String number2Str                     		= null;
	private String productStr                     		= null;
	private int mathOperationId			                = 0;
	private int pointSum							    = 0;
	
	public int getEquationsId() {
		return equationsId;
	}
	public void setEquationsId(int equationsId) {
		this.equationsId = equationsId;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getNumber1Str() {
		return number1Str;
	}
	public void setNumber1Str(String number1Str) {
		this.number1Str = number1Str;
	}
	public String getNumber2Str() {
		return number2Str;
	}
	public void setNumber2Str(String number2Str) {
		this.number2Str = number2Str;
	}
	public String getProductStr() {
		return productStr;
	}
	public void setProductStr(String productStr) {
		this.productStr = productStr;
	}
	public int getMathOperationId() {
		return mathOperationId;
	}
	public void setMathOperationId(int mathOperationId) {
		this.mathOperationId = mathOperationId;
	}
	public int getPointSum() {
		return pointSum;
	}
	public void setPointSum(int pointSum) {
		this.pointSum = pointSum;
	}
	
	
}
