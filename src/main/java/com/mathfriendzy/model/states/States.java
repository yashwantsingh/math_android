package com.mathfriendzy.model.states;

import java.util.ArrayList;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.mathfriendzy.database.Database;

public class States 
{
	//us State table
	private String US_STATE_TABLE           = "world_US_States";
	private String US_STATE_FULL_NAME		= "State_Full_Name";
	
	//canada states table
	private String CANADA_STATE_TABLE       = "world_Canada_Province";
	private String CANADA_STATE_FULL_NAME	= "State_Full_Name";
	private String STATE_CODE_NAME			= "State_Name";
	private String STATE_ID					= "State_ID";

	/**
	 * This method return the US states
	 * @param context
	 * @return
	 */
	public ArrayList<String> getUSStates(Context context)
	{
		ArrayList<String> USStatesList = new ArrayList<String>();

		SQLiteDatabase dbConn = null;
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
		String query = "select " + US_STATE_FULL_NAME + " from " + US_STATE_TABLE;
		Cursor cursor = dbConn.rawQuery(query, null);
		
		while(cursor.moveToNext())
		{		
			USStatesList.add(cursor.getString(cursor.getColumnIndex(US_STATE_FULL_NAME)));
		}
		cursor.close();
		dbConn.close();
		
		return USStatesList;
	}
	
	/**
	 * This method return the canada states
	 * @param context
	 * @return
	 */
	public ArrayList<String> getCanadaStates(Context context)
	{
		ArrayList<String> canadaStatesList = new ArrayList<String>();

		SQLiteDatabase dbConn = null;
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
		String query = "select " + CANADA_STATE_FULL_NAME + " from " + CANADA_STATE_TABLE;
		Cursor cursor = dbConn.rawQuery(query, null);
		
		while(cursor.moveToNext())
		{		
			canadaStatesList.add(cursor.getString(cursor.getColumnIndex(CANADA_STATE_FULL_NAME)));
		}
		cursor.close();
		dbConn.close();
		
		return canadaStatesList;
	}
	
	/**
	 * This method return the stateCodeName From Canada By State Name
	 * @param stateName
	 * @param context
	 * @return
	 */
	public String getStateCodeNameByStateNameFromCanada(String stateName , Context context)
	{
		String stateNameCode = "";
		SQLiteDatabase dbConn = null;
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
		
		String query = "select " + STATE_CODE_NAME + " from " + CANADA_STATE_TABLE + " where " + CANADA_STATE_FULL_NAME + " = '" 
						+ stateName + "'" ;
		Cursor cursor = dbConn.rawQuery(query, null);
		
		if(cursor.moveToNext())
		{		
			stateNameCode = cursor.getString(cursor.getColumnIndex(STATE_CODE_NAME));
		}
		cursor.close();
		dbConn.close();
		
		return stateNameCode;
	}
	
	/**
	 * This method return the stateCodeName From US By State Name
	 * @param stateName
	 * @param context
	 * @return
	 */
	public String getStateCodeNameByStateNameFromUS(String stateName , Context context)
	{
		String stateNameCode = "";
		SQLiteDatabase dbConn = null;
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
		
		String query = "select " + STATE_CODE_NAME + " from " + US_STATE_TABLE + " where " + US_STATE_FULL_NAME + " = '" 
						+ stateName + "'" ;
		Cursor cursor = dbConn.rawQuery(query, null);
		
		if(cursor.moveToNext())
		{		
			stateNameCode = cursor.getString(cursor.getColumnIndex(STATE_CODE_NAME));
		}
		cursor.close();
		dbConn.close();
		
		return stateNameCode;
	}
	
	/**
	 * This method return the US stateCode By ID
	 * @param stateId
	 * @param context
	 * @return
	 */
	public String getUSStateCodeById(String stateId,Context context)
	{
		String stateName = "";
		SQLiteDatabase dbConn = null;
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
		
		String query = "select " + STATE_CODE_NAME + " from " + US_STATE_TABLE + " where " + STATE_ID + " = '" 
						+ stateId + "'" ;
		Cursor cursor = dbConn.rawQuery(query, null);
		
		if(cursor.moveToNext())
		{		
			stateName = cursor.getString(cursor.getColumnIndex(STATE_CODE_NAME));
		}
		cursor.close();
		dbConn.close();
		return stateName;
	}
	
	/**
	 * This method return the state ID by Name from canada
	 * @param stateName
	 * @param context
	 * @return
	 */
	public String getCanadaStateIdByName(String stateName,Context context)
	{
		String stateId = "";
		SQLiteDatabase dbConn = null;
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
		
		String query = "select " + STATE_ID + " from " + CANADA_STATE_TABLE + " where " + CANADA_STATE_FULL_NAME + " = '" 
						+ stateName + "'" ;
		Cursor cursor = dbConn.rawQuery(query, null);
		
		if(cursor.moveToNext())
		{		
			stateId = cursor.getString(cursor.getColumnIndex(STATE_ID));
		}
		cursor.close();
		dbConn.close();
		return stateId;
	}
	
	/**
	 * This method return the Canada stateCode By ID
	 * @param stateId
	 * @param context
	 * @return
	 */
	public String getCanadaStateCodeById(String stateId,Context context)
	{
		String stateName = "";
		SQLiteDatabase dbConn = null;
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
		
		String query = "select " + STATE_CODE_NAME + " from " + CANADA_STATE_TABLE + " where " + STATE_ID + " = '" 
						+ stateId + "'" ;
		Cursor cursor = dbConn.rawQuery(query, null);
		
		if(cursor.moveToNext())
		{		
			stateName = cursor.getString(cursor.getColumnIndex(STATE_CODE_NAME));
		}
		cursor.close();
		dbConn.close();
		return stateName;
	}
	
	/**
	 * This method return the state ID by Name from US
	 * @param stateName
	 * @param context
	 * @return
	 */
	public String getUSStateIdByName(String stateName,Context context)
	{
		String stateId = "";
		SQLiteDatabase dbConn = null;
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
		
		String query = "select " + STATE_ID + " from " + US_STATE_TABLE + " where " + US_STATE_FULL_NAME + " = '" 
						+ stateName + "'" ;
		Cursor cursor = dbConn.rawQuery(query, null);
		
		if(cursor.moveToNext())
		{		
			stateId = cursor.getString(cursor.getColumnIndex(STATE_ID));
		}
		cursor.close();
		dbConn.close();
		return stateId;
	}
	
	/**
	 * This method return the US stateName By ID
	 * @param stateId
	 * @param context
	 * @return
	 */
	public String getUSStateNameById(String stateId,Context context)
	{
		String stateName = "";
		SQLiteDatabase dbConn = null;
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
		
		String query = "select " + US_STATE_FULL_NAME + " from " + US_STATE_TABLE + " where " + STATE_ID + " = '" 
						+ stateId + "'" ;
		Cursor cursor = dbConn.rawQuery(query, null);
		
		if(cursor.moveToNext())
		{		
			stateName = cursor.getString(cursor.getColumnIndex(US_STATE_FULL_NAME));
		}
		cursor.close();
		dbConn.close();
		return stateName;
	}
	
	/**
	 * This method return the Canada stateName By ID
	 * @param stateId
	 * @param context
	 * @return
	 */
	public String getCanadaStateNameById(String stateId,Context context)
	{
		String stateName = "";
		SQLiteDatabase dbConn = null;
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
		
		String query = "select " + US_STATE_FULL_NAME + " from " + CANADA_STATE_TABLE + " where " + STATE_ID + " = '" 
						+ stateId + "'" ;
		Cursor cursor = dbConn.rawQuery(query, null);
		
		if(cursor.moveToNext())
		{		
			stateName = cursor.getString(cursor.getColumnIndex(US_STATE_FULL_NAME));
		}
		cursor.close();
		dbConn.close();
		return stateName;
	}
}
