package com.mathfriendzy.model.ratetutorsession;

/**
 * Created by root on 21/4/15.
 */
public class RateTutorSessionParam {

    private String action;
    private int requestId;
    private int stars;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }
}
