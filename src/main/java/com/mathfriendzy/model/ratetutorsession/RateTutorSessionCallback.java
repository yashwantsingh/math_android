package com.mathfriendzy.model.ratetutorsession;

import com.mathfriendzy.serveroperation.HttpResponseBase;

/**
 * Created by root on 12/5/15.
 */
public interface RateTutorSessionCallback {
    void onServerResponse(HttpResponseBase httpResponseBase, int requestCode);
}
