package com.mathfriendzy.model.language;

import static com.mathfriendzy.database.DatabaseConstant.LANGUAGE_CODE;
import static com.mathfriendzy.database.DatabaseConstant.LANGUAGE_ID;
import static com.mathfriendzy.database.DatabaseConstant.LANGUAGE_NAME;
import static com.mathfriendzy.database.DatabaseConstant.LANGUAGE_TABLE_NAME;
import static com.mathfriendzy.utils.ICommonUtils.LANGUAGE_LOG;
import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.mathfriendzy.database.Database;
import com.mathfriendzy.utils.CommonUtils;

public class Language 
{		
	private final String  GET_LANGUAGE_URL = COMPLETE_URL + "action=getLanguages";
	
	private ArrayList<LanguageDTO> languageList = null;
	private SQLiteDatabase dbConn 	  	  = null;
	private LanguageDTO language 	= null;
	private Context context 		= null;
	
	public Language(Context context)
	{
		this.context = context;
	}
	
	/**
	 * This method getLanguages from server
	 */
	public void getLanguagesFromServer()
	{
		if(LANGUAGE_LOG)
			Log.e("Language", "inside getLanguagesFromServer()");
		
		this.parseJson(CommonUtils.readFromURL(GET_LANGUAGE_URL));
		
		if(LANGUAGE_LOG)
			Log.e("Language", "outside getLanguagesFromServer()");
	}
		
	/**
	 * This method parse Json
	 * @param jsonString
	 */
	public void parseJson(String jsonString)
	{
		if(LANGUAGE_LOG)
			Log.e("Language", "inside parseJson()");
		
		languageList = new ArrayList<LanguageDTO>();
		
		try 
		{
			JSONObject jObject = new JSONObject(jsonString);
			JSONArray jArray = jObject.getJSONArray("data");
			
			for(int i = 0; i < jArray.length() ; i++)
			{
				JSONObject jObject2 = jArray.getJSONObject(i);
						
				language = new LanguageDTO();
				language.setLanguageId(jObject2.get("languageId").toString());
				language.setLanguageName(jObject2.get("name").toString());
				language.setLanguageCode(jObject2.get("langCode").toString());
				languageList.add(language);
			}
		}
		catch (JSONException e) 
		{
			CommonUtils.setSharedPreferences(context);
			e.printStackTrace();
		}
		
		if(LANGUAGE_LOG)
			Log.e("Language", "outside parseJson()");
	}
	
	/**
	 * This method save the language into database
	 */
	public void saveLanguages()
	{
		if(LANGUAGE_LOG)
			Log.e("Language", "inside storeLanguagesIntoDatabase()");
		
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
		this.deleteFromLanguage();
		
		for(int i = 0 ; i < languageList.size() ; i++)
		{
			ContentValues contentValues = new ContentValues();
			contentValues.put(LANGUAGE_ID, languageList.get(i).getLanguageId());
			contentValues.put(LANGUAGE_NAME, languageList.get(i).getLanguageName());
			contentValues.put(LANGUAGE_CODE, languageList.get(i).getLanguageCode());
					
		  dbConn.insert(LANGUAGE_TABLE_NAME, null, contentValues);
		}
		
		database.close();
		
		if(LANGUAGE_LOG)
			Log.e("Language", "outside storeLanguagesIntoDatabase()");
	}
	
	/**
	 * This method return the languages from database
	 * @return
	 */
	public ArrayList<String> getLanguages()
	{
		ArrayList<String> languageList = new ArrayList<String>();
		
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
				
		String query = " select " + LANGUAGE_NAME + " from " + LANGUAGE_TABLE_NAME ;
		
		Cursor cursor = dbConn.rawQuery(query, null);
		while(cursor.moveToNext())
		{
			languageList.add(cursor.getString(cursor.getColumnIndex(LANGUAGE_NAME)));
		}
		cursor.close();
		database.close();
		
	 return languageList;
	}
	
	/**
	 * This method delete the data from the Languages
	 */
	public void deleteFromLanguage()
	{
		dbConn.delete(LANGUAGE_TABLE_NAME , null , null);
	}
	
	/**
	 * Return language id corresponding to language name
	 * @param languageName
	 * @return
	 */
	public String getLanguageIdByName(String languageName)
	{
		String languageId = null;
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
				
		String query = " select " + LANGUAGE_ID + " from " + LANGUAGE_TABLE_NAME + " where "  + LANGUAGE_NAME + " = '" + languageName + "'";
		Cursor cursor = dbConn.rawQuery(query, null);
		if(cursor.moveToNext())
		{
			languageId = cursor.getString(cursor.getColumnIndex(LANGUAGE_ID));
		}
		cursor.close();
		database.close();
		return languageId;
	}
	
	/**
	 * Return language code corresponding to language name
	 * @param languageName
	 * @return
	 */
	public String getLanguageCodeByName(String languageName)
	{
		String languageCode = null;
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
				
		String query = " select " + LANGUAGE_CODE + " from " + LANGUAGE_TABLE_NAME + " where "  + LANGUAGE_NAME + " = '" + languageName + "'";
		Cursor cursor = dbConn.rawQuery(query, null);
		if(cursor.moveToNext())
		{
			languageCode = cursor.getString(cursor.getColumnIndex(LANGUAGE_CODE));
		}
		cursor.close();
		database.close();
		return languageCode;
	}
	
	/**
	 * This method return the language Name by Language Id
	 * @param languageId
	 * @return
	 */
	public String getLanguageNameById(String languageId)
	{
		String languageName = null;
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
				
		String query = " select " + LANGUAGE_NAME + " from " + LANGUAGE_TABLE_NAME + " where "  + LANGUAGE_ID + " = '" + languageId + "'";
		Cursor cursor = dbConn.rawQuery(query, null);
		if(cursor.moveToNext())
		{
			languageName = cursor.getString(cursor.getColumnIndex(LANGUAGE_NAME));
		}
		cursor.close();
		database.close();
		return languageName;
	}
}
