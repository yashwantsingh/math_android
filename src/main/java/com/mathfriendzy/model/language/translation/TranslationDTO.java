package com.mathfriendzy.model.language.translation;

public class TranslationDTO 
{
	private String languageId 		= null;
	private String textIdentifier 	= null;
	private String transelation		= null;
	private String applicationId	= null;
	
	public String getLanguageId() {
		return languageId;
	}
	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}
	public String getTextIdentifier() {
		return textIdentifier;
	}
	public void setTextIdentifier(String textIdentifier) {
		this.textIdentifier = textIdentifier;
	}
	public String getTranselation() {
		return transelation;
	}
	public void setTranselation(String transelation) {
		this.transelation = transelation;
	}
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
}
