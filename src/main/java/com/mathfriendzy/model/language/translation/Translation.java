package com.mathfriendzy.model.language.translation;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mathfriendzy.database.Database;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.MathVersions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.mathfriendzy.database.DatabaseConstant.APPLICATION_ID;
import static com.mathfriendzy.database.DatabaseConstant.TEXT_IDENTIFIER;
import static com.mathfriendzy.database.DatabaseConstant.TRANSELATE_LANGUAGE_ID;
import static com.mathfriendzy.database.DatabaseConstant.TRANSELATE_TABLE_NAME;
import static com.mathfriendzy.database.DatabaseConstant.TRANSELATION;
import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;
import static com.mathfriendzy.utils.ICommonUtils.LANGUAGE_LOG;

;

/**
 * This class for the Translation Text From the server and store into database
 * @author Yashwant Singh
 *
 */
public class Translation 
{
	private ArrayList<TranslationDTO> transelationList = null;
	private SQLiteDatabase dbConn 	  	= null;
	private Context context 			= null;
	private TranslationDTO transelation = null;
	private String languageCode 		= null;
	private String languageId 			= null;
	private String applicationId 		= null;
		
	public Translation(Context context)
	{
		this.context = context;
	}
	
	
	/**
	 * This method get the transelate text from server
	 * @param languageCode
	 * @param languageId
	 * @param applicationId
	 */
	public void getTransalateTextFromServer(String languageCode,String languageId,String applicationId)
	{
		if(LANGUAGE_LOG)
			Log.e("Translation", "inside getTransalateTextFromServer()");
		
		String stringUrl = 	COMPLETE_URL + "action=getLang&langCode=" + languageCode +"&langId="+ languageId 
							+"&appId="+applicationId;
	
		this.languageCode 	= languageCode;
		this.languageId 	= languageId;
		this.applicationId 	= applicationId;
		
		this.parseJson(CommonUtils.readFromURL(stringUrl));
		
		if(LANGUAGE_LOG)
			Log.e("Translation", "outside getTransalateTextFromServer()");
	}
	
	
	/**
	 * This method parse json
	 * @param jsonString
	 */
	private void parseJson(String jsonString)
	{
		if(LANGUAGE_LOG)
			Log.e("Translation", "inside parseJson()");
		
		transelationList = new ArrayList<TranslationDTO>();
		try 
		{
			JSONObject jObject = new JSONObject(jsonString);
			JSONArray jArray = jObject.getJSONArray("data");
			
			for(int i = 0; i < jArray.length() ; i++)
			{
				transelation = new TranslationDTO();
				JSONObject jObject2 = jArray.getJSONObject(i);
				
				transelation.setLanguageId(languageId);
				transelation.setApplicationId(applicationId);
				transelation.setTranselation(jObject2.get("text").toString());
				transelation.setTextIdentifier(jObject2.get("textId").toString());
				transelationList.add(transelation);
			}
		}
		catch (Exception e) 
		{			
			CommonUtils.setSharedPreferences(context);
		}
		
		if(LANGUAGE_LOG)
			Log.e("Translation", "outside parseJson()");
	}
	
	
	/**
	 * This method save the translation text into database
	 */
	public void saveTranslationText()
	{
		if(LANGUAGE_LOG)
			Log.e("Translation", "inside saveTranslationTextIntoDatabase()");
		
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
		this.deleteFromtranslation();
		
		for(int i = 0 ; i < transelationList.size() ; i++)
		{
			ContentValues contentValues = new ContentValues();
			contentValues.put(TRANSELATE_LANGUAGE_ID, transelationList.get(i).getLanguageId());
			contentValues.put(TEXT_IDENTIFIER, transelationList.get(i).getTextIdentifier());
			contentValues.put(TRANSELATION, transelationList.get(i).getTranselation());
			contentValues.put(APPLICATION_ID, transelationList.get(i).getTranselation());
					
			dbConn.insert(TRANSELATE_TABLE_NAME, null, contentValues);
		}
		database.close();
		
		if(LANGUAGE_LOG)
			Log.e("Translation", "outside saveTranslationTextIntoDatabase()");
	}
	
	
	/**
	 * This method delete the data from the Translation
	 */
	public void deleteFromtranslation()
	{
		dbConn.delete(TRANSELATE_TABLE_NAME , null , null);
	}
	
	/**
	 * This method return the text corresponding to the textIdentifier
	 * @param textIdentifier
	 * @return
	 */
	public String getTranselationTextByTextIdentifier(String textIdentifier)
	{

		if(LANGUAGE_LOG)
			Log.e("Translation", "inside getTranselationTextByTextIdentifier()");

        if(textIdentifier.equalsIgnoreCase(MathFriendzyHelper.MATH_TITLE_IDENTIFIER)) {
            if (MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS) {
                textIdentifier = MathFriendzyHelper.MATH_PLUS_TITLE_IDENTIFIER;
            }else if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_TUTOR_PLUS){
                textIdentifier = MathFriendzyHelper.MATH_TUTOR_PLUS_TITLE_IDENTIFIER;
            }
        }

		String textIdentifire = "";
		String query = " select *  from " + TRANSELATE_TABLE_NAME + " where "
						+ TEXT_IDENTIFIER + " = '" + textIdentifier + "'";
		
		Cursor cursor = dbConn.rawQuery(query, null);
		if(cursor.moveToNext())
			textIdentifire =  cursor.getString(cursor.getColumnIndex(TRANSELATION));
		if(cursor != null)
			cursor.close();
		
	 return textIdentifire;
	}
	
	/**
	 * Open the database connection
	 */
	public void openConnection()
	{
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
	}
	
	/**
	 * Close the database connection
	 */
	public void closeConnection()
	{
		if(dbConn != null)
			dbConn.close();
	}
}
