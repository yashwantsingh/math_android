package com.mathfriendzy.model.helpastudent;

/**
 * Created by root on 7/4/15.
 */
public class UpdateChatRequestWithDialogParam {
    private String action;
    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
