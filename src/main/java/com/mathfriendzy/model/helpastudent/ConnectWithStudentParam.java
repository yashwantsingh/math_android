package com.mathfriendzy.model.helpastudent;

/**
 * Created by root on 2/4/15.
 */
public class ConnectWithStudentParam {

    private String action;
    private String data;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
