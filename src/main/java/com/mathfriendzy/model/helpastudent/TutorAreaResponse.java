package com.mathfriendzy.model.helpastudent;

import com.mathfriendzy.serveroperation.HttpResponseBase;

/**
 * Created by root on 29/6/15.
 */
public class TutorAreaResponse extends HttpResponseBase{
    private String result;
    private int tutorResponses;
    private int tutorSessionResponses;
    private int hwInput;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getTutorResponses() {
        return tutorResponses;
    }

    public void setTutorResponses(int tutorResponses) {
        this.tutorResponses = tutorResponses;
    }

    public int getTutorSessionResponses() {
        return tutorSessionResponses;
    }

    public void setTutorSessionResponses(int tutorSessionResponses) {
        this.tutorSessionResponses = tutorSessionResponses;
    }

    public int getHwInput() {
        return hwInput;
    }

    public void setHwInput(int hwInput) {
        this.hwInput = hwInput;
    }
}
