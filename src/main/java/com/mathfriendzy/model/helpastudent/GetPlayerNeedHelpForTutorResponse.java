package com.mathfriendzy.model.helpastudent;

import com.mathfriendzy.serveroperation.HttpResponseBase;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by root on 1/4/15.
 */
public class GetPlayerNeedHelpForTutorResponse extends HttpResponseBase implements Serializable {

    private String response;

    private int requestId;
    private String userId;
    private String playerId;
    private String playerFName;
    private String playerLName;
    private int grade;
    private String chatId;
    private String chatUserName;
    private int inActive;//
    private String reqDate;
    private String message;
    private int isConnected;
    private String tutorUid;
    private String tutorPid;
    private String chatDialogId;
    private int isAnonymous;
    private int timeSpent;
    private String studentImage;//
    private String tutorImage;//
    private String workAreaImage;//
    private String lastUpdatedByTutor;//
    private int stars;
    private int disconnected;//
    private int opponentInput;//
    private String paidSession;
    private int studentOnline;//
    //means newly addedd in API response

    private ArrayList<GetPlayerNeedHelpForTutorResponse> tutorList = null;
    private ArrayList<GetPlayerNeedHelpForTutorResponse> connectedTutorList = null;
    private ArrayList<GetPlayerNeedHelpForTutorResponse> notConnectedTutorList = null;
    private boolean isFromDisconnectedList = false;

    public String getResponse() {
        return response;
    }

    public int getRequestId() {
        return requestId;
    }

    public String getUserId() {
        return userId;
    }

    public String getPlayerId() {
        return playerId;
    }

    public String getPlayerFName() {
        return playerFName;
    }

    public String getPlayerLName() {
        return playerLName;
    }

    public String getChatId() {
        return chatId;
    }

    public int getGrade() {
        return grade;
    }

    public String getChatUserName() {
        return chatUserName;
    }

    public String getMessage() {
        return message;
    }

    public String getReqDate() {
        return reqDate;
    }

    public int getIsConnected() {
        return isConnected;
    }

    public String getTutorUid() {
        return tutorUid;
    }

    public String getChatDialogId() {
        return chatDialogId;
    }

    public String getTutorPid() {
        return tutorPid;
    }

    public int getIsAnonymous() {
        return isAnonymous;
    }

    public int getTimeSpent() {
        return timeSpent;
    }

    public ArrayList<GetPlayerNeedHelpForTutorResponse> getTutorList() {
        return tutorList;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public void setPlayerFName(String playerFName) {
        this.playerFName = playerFName;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public void setPlayerLName(String gplayerLName) {
        this.playerLName = gplayerLName;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public void setChatUserName(String chatUserName) {
        this.chatUserName = chatUserName;
    }

    public void setReqDate(String reqDate) {
        this.reqDate = reqDate;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setIsConnected(int isConnected) {
        this.isConnected = isConnected;
    }

    public void setTutorUid(String tutorUid) {
        this.tutorUid = tutorUid;
    }

    public void setTutorPid(String tutorPid) {
        this.tutorPid = tutorPid;
    }

    public void setChatDialogId(String chatDialogId) {
        this.chatDialogId = chatDialogId;
    }

    public void setIsAnonymous(int isAnonymous) {
        this.isAnonymous = isAnonymous;
    }

    public void setTimeSpent(int timeSpent) {
        this.timeSpent = timeSpent;
    }

    public void setTutorList(ArrayList<GetPlayerNeedHelpForTutorResponse> tutorList) {
        this.tutorList = tutorList;
    }

    public ArrayList<GetPlayerNeedHelpForTutorResponse> getConnectedTutorList() {
        return connectedTutorList;
    }

    public void setConnectedTutorList(ArrayList<GetPlayerNeedHelpForTutorResponse> connectedTutorList) {
        this.connectedTutorList = connectedTutorList;
    }

    public ArrayList<GetPlayerNeedHelpForTutorResponse> getNotConnectedTutorList() {
        return notConnectedTutorList;
    }

    public void setNotConnectedTutorList(ArrayList<GetPlayerNeedHelpForTutorResponse> notConnectedTutorList) {
        this.notConnectedTutorList = notConnectedTutorList;
    }

    public boolean isFromDisconnectedList() {
        return isFromDisconnectedList;
    }

    public void setFromDisconnectedList(boolean isFromDisconnectedList) {
        this.isFromDisconnectedList = isFromDisconnectedList;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public int getInActive() {
        return inActive;
    }

    public void setInActive(int inActive) {
        this.inActive = inActive;
    }

    public String getStudentImage() {
        return studentImage;
    }

    public void setStudentImage(String studentImage) {
        this.studentImage = studentImage;
    }

    public String getTutorImage() {
        return tutorImage;
    }

    public void setTutorImage(String tutorImage) {
        this.tutorImage = tutorImage;
    }

    public String getWorkAreaImage() {
        return workAreaImage;
    }

    public void setWorkAreaImage(String workAreaImage) {
        this.workAreaImage = workAreaImage;
    }

    public String getLastUpdatedByTutor() {
        return lastUpdatedByTutor;
    }

    public void setLastUpdatedByTutor(String lastUpdatedByTutor) {
        this.lastUpdatedByTutor = lastUpdatedByTutor;
    }

    public int getDisconnected() {
        return disconnected;
    }

    public void setDisconnected(int disconnected) {
        this.disconnected = disconnected;
    }

    public int getOpponentInput() {
        return opponentInput;
    }

    public void setOpponentInput(int opponentInput) {
        this.opponentInput = opponentInput;
    }

    public String getPaidSession() {
        return paidSession;
    }

    public void setPaidSession(String paidSession) {
        this.paidSession = paidSession;
    }

    public int getStudentOnline() {
        return studentOnline;
    }

    public void setStudentOnline(int studentOnline) {
        this.studentOnline = studentOnline;
    }
}
