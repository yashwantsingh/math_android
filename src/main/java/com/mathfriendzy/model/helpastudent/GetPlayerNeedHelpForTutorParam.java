package com.mathfriendzy.model.helpastudent;

/**
 * Created by root on 1/4/15.
 */
public class GetPlayerNeedHelpForTutorParam {
    private String action;
    private String userId;
    private String playerId;
    private String isTutor;
    private int isConnected;
    private int offSet;
    private boolean isFirstTime;
    private String grade;
    private int forSubject;

    public String getAction() {
        return action;
    }

    public String getUserId() {
        return userId;
    }

    public String getPlayerId() {
        return playerId;
    }

    public String getIsTutor() {
        return isTutor;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public void setIsTutor(String isTutor) {
        this.isTutor = isTutor;
    }

    public int getIsConnected() {
        return isConnected;
    }

    public void setIsConnected(int isConnected) {
        this.isConnected = isConnected;
    }

    public int getOffSet() {
        return offSet;
    }

    public void setOffSet(int offSet) {
        this.offSet = offSet;
    }

    public boolean isFirstTime() {
        return isFirstTime;
    }

    public void setFirstTime(boolean isFirstTime) {
        this.isFirstTime = isFirstTime;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public int getForSubject() {
        return forSubject;
    }

    public void setForSubject(int forSubject) {
        this.forSubject = forSubject;
    }
}
