package com.mathfriendzy.model.helpastudent;

import com.mathfriendzy.serveroperation.HttpResponseBase;

/**
 * Created by root on 2/4/15.
 */
public class ConnectWithTutorResponse extends HttpResponseBase {
    private String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
