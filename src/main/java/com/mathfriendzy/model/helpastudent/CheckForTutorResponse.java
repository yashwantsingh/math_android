package com.mathfriendzy.model.helpastudent;

import com.mathfriendzy.serveroperation.HttpResponseBase;

/**
 * Created by root on 21/5/15.
 */
public class CheckForTutorResponse extends HttpResponseBase{
    private String result;
    private int isTutor;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getIsTutor() {
        return isTutor;
    }

    public void setIsTutor(int isTutor) {
        this.isTutor = isTutor;
    }
}
