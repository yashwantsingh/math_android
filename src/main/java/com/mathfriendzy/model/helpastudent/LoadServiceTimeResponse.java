package com.mathfriendzy.model.helpastudent;

import com.mathfriendzy.serveroperation.HttpResponseBase;

/**
 * Created by root on 30/4/15.
 */
public class LoadServiceTimeResponse extends HttpResponseBase{

    private String result;
    private String startDate;
    private String endData;
    private String tutoringTime;
    private String idleTime;
    private int rating;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndData() {
        return endData;
    }

    public void setEndData(String endData) {
        this.endData = endData;
    }

    public String getIdleTime() {
        return idleTime;
    }

    public void setIdleTime(String idleTime) {
        this.idleTime = idleTime;
    }

    public String getTutoringTime() {
        return tutoringTime;
    }

    public void setTutoringTime(String tutoringTime) {
        this.tutoringTime = tutoringTime;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
