package com.mathfriendzy.model.multifriendzy;

public class MathFriendzysRoundDTO 
{
	private String roundNumber;
	private String playerScore;
	private String oppScore;
	
	public String getRoundNumber() {
		return roundNumber;
	}
	public void setRoundNumber(String roundNumber) {
		this.roundNumber = roundNumber;
	}
	public String getPlayerScore() {
		return playerScore;
	}
	public void setPlayerScore(String playerScore) {
		this.playerScore = playerScore;
	}
	public String getOppScore() {
		return oppScore;
	}
	public void setOppScore(String oppScore) {
		this.oppScore = oppScore;
	}
	
	
}
