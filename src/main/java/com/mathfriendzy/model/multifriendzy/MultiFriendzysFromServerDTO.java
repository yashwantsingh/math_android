package com.mathfriendzy.model.multifriendzy;

import java.util.ArrayList;

public class MultiFriendzysFromServerDTO /*implements Serializable*/
{
	/**
	 * 
	 *//*
	private static final long serialVersionUID = 1L;*/
	
	private String countryCode ;
	private String notificationDevices;
	private String friendzysId;
	private String type;
	private String initiatorId;
	private String startingFriendzyDate;
	private int isCompleted;
	private int winner;
	private String androidPids;
		
	private OppnentDataDTO opponentData;
	
	private ArrayList<MathFriendzysRoundDTO> roundList;

	//added for word problem
	private int forWordProblem;
			
	public int getForWordProblem() {
		return forWordProblem;
	}

	public void setForWordProblem(int forWordProblem) {
		this.forWordProblem = forWordProblem;
	}

	public String getAndroidPids() {
		return androidPids;
	}

	public void setAndroidPids(String androidPids) {
		this.androidPids = androidPids;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getNotificationDevices() {
		return notificationDevices;
	}

	public void setNotificationDevices(String notificationDevices) {
		this.notificationDevices = notificationDevices;
	}

	public String getFriendzysId() {
		return friendzysId;
	}

	public void setFriendzysId(String friendzysId) {
		this.friendzysId = friendzysId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getInitiatorId() {
		return initiatorId;
	}

	public void setInitiatorId(String initiatorId) {
		this.initiatorId = initiatorId;
	}

	public String getStartingFriendzyDate() {
		return startingFriendzyDate;
	}

	public void setStartingFriendzyDate(String startingFriendzyDate) {
		this.startingFriendzyDate = startingFriendzyDate;
	}

	public int getIsCompleted() {
		return isCompleted;
	}

	public void setIsCompleted(int isCompleted) {
		this.isCompleted = isCompleted;
	}

	public int getWinner() {
		return winner;
	}

	public void setWinner(int winner) {
		this.winner = winner;
	}

	public OppnentDataDTO getOpponentData() {
		return opponentData;
	}

	public void setOpponentData(OppnentDataDTO opponentData) {
		this.opponentData = opponentData;
	}

	public ArrayList<MathFriendzysRoundDTO> getRoundList() {
		return roundList;
	}

	public void setRoundList(ArrayList<MathFriendzysRoundDTO> roundList) {
		this.roundList = roundList;
	}
}
