package com.mathfriendzy.model.multifriendzy.schoolcurriculum;

import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.mathfriendzy.controller.multifriendzy.schoolcurriculum.WordQuestionFromServerObj;
import com.mathfriendzy.model.schoolcurriculum.singlefriendzy.SigleFriendzyQuestionFromServer;
import com.mathfriendzy.utils.CommonUtils;

public class MultiFriendzySchoolCurriculumServerOperation {

	private final String TAG = this.getClass().getSimpleName();

	/**
	 * This  method get equations from server for word problem 
	 * for multifriendzy
	 * @param userId
	 * @param playerId
	 * @param friendzyId
	 */
	public WordQuestionFromServerObj getWordQuestionForPlayer(String userId , String playerId , String friendzyId){
		String action = "findMultiFriendzyWordProblemEquatiosForPlayer";
		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "userId="   + userId  + "&"
				+ "playerId=" + playerId + "&"
				+ "friendzyId=" + friendzyId;
		//Log.e(TAG, "inside getWordQuestionForPlayer url : " + strUrl);
		return this.parseEquationJson(CommonUtils.readFromURL(strUrl));
	}

	private WordQuestionFromServerObj parseEquationJson(String jsonString){
		//Log.e(TAG, "json String " + jsonString);

		if(jsonString != null){
			WordQuestionFromServerObj wordQuestion = new WordQuestionFromServerObj();

			ArrayList<SigleFriendzyQuestionFromServer> questionList	= 
					new ArrayList<SigleFriendzyQuestionFromServer>();
			try{
				JSONObject jsonObj = new JSONObject(jsonString);
				JSONArray jsonDataArray = jsonObj.getJSONArray("data");

				for( int i = 0 ; i < jsonDataArray.length() ; i ++ ){
					SigleFriendzyQuestionFromServer questionObj = new SigleFriendzyQuestionFromServer();
					JSONObject jsonQuestionObj = jsonDataArray.getJSONObject(i);
					questionObj.setcId(jsonQuestionObj.getInt("cId"));
					questionObj.setQuestionId(jsonQuestionObj.getInt("questionId"));
					questionObj.setIsCorrect(jsonQuestionObj.getInt("isCorrect"));
					questionObj.setPoints(jsonQuestionObj.getInt("points"));
					questionObj.setTimeTaken(jsonQuestionObj.getInt("timeTaken"));
					questionList.add(questionObj);
				}

				wordQuestion.setQuestionList(questionList);
				wordQuestion.setGrade(jsonObj.getInt("grade"));

			}catch(JSONException e){
				Log.e(TAG, "Error while parsing getWordQuestionForPlayer json " + e.toString());
			}
			//return questionList;
			return wordQuestion;
		}else{
			return null;
		}
	}
}
