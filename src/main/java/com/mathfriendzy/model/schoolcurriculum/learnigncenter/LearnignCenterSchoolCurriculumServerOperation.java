package com.mathfriendzy.model.schoolcurriculum.learnigncenter;

import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;
//import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL_FOR;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;

public class LearnignCenterSchoolCurriculumServerOperation {

	private final String TAG = this.getClass().getSimpleName();

	/**
	 * This method get wordProblem detail for player for learning center
	 * @param userId
	 * @param playerId
	 */
	public wordProblemLevelDetailTransferObj getWordProblemDetail(String userId , String playerId){

		String action = "getWordProblemLevelsDetails";

		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "userId="   + userId  + "&"
				+ "playerId=" + playerId;

        if(CommonUtils.LOG_ON)
		    Log.e(TAG, "inside getWordProblemDetail url " + strUrl);

		return this.parseSchoolCurriculumDetailJson(CommonUtils.readFromURL(strUrl));
	}

	/**
	 * This method parse wordProblem detail json from server
	 * @param jsonString
	 */
	private wordProblemLevelDetailTransferObj parseSchoolCurriculumDetailJson(String jsonString){

        if(CommonUtils.LOG_ON)
		    Log.e(TAG, "inside parseSchoolCurriculumDetailJson json String " + jsonString);

		if(jsonString != null){
			wordProblemLevelDetailTransferObj wordProblemLevelDetail = new wordProblemLevelDetailTransferObj();

			ArrayList<WordProblemLevelTransferObj> wordProblemLevelList = new ArrayList<WordProblemLevelTransferObj>();
			ArrayList<UpdatedInfoTransferObj>      updatedList          = new ArrayList<UpdatedInfoTransferObj>();
			ArrayList<TestDetails>                 testDetailList       = new ArrayList<TestDetails>();

			try 
			{
				JSONObject json = new JSONObject(jsonString);
				JSONObject jsonObj = json.getJSONObject("data");

				JSONArray jsonLevelArray = jsonObj.getJSONArray("level");

				for(int i = 0 ; i < jsonLevelArray.length() ; i ++ ){

					JSONObject jsonjLevelObj = jsonLevelArray.getJSONObject(i);
					WordProblemLevelTransferObj levelDataObj = new WordProblemLevelTransferObj();

					levelDataObj.setUserId(jsonjLevelObj.getString("uid"));
					levelDataObj.setPlayerId(jsonjLevelObj.getString("pid"));
					levelDataObj.setCategoryId(jsonjLevelObj.getInt("category"));
					levelDataObj.setSubCategoryId(jsonjLevelObj.getInt("subCategory"));
					levelDataObj.setStars(jsonjLevelObj.getInt("star"));

					wordProblemLevelList.add(levelDataObj);
				}

				JSONArray updatedArray = jsonObj.getJSONArray("updateInfo");
				for(int j = 0 ; j < updatedArray.length() ; j ++ ){

					JSONObject jsonjUpdatedObj = updatedArray.getJSONObject(j);
					Iterator<String> iterator = jsonjUpdatedObj.keys();

					UpdatedInfoTransferObj updatedData = new UpdatedInfoTransferObj();
					int grade = Integer.parseInt(iterator.next());
					updatedData.setGrade(grade);
					updatedData.setUpdatedDate(jsonjUpdatedObj.getString(grade + ""));

					updatedList.add(updatedData);
				}

				JSONArray testDetialArray = json.getJSONArray("testDetails");

				for(int i = 0 ; i < testDetialArray.length() ; i ++ ){
					JSONObject testJsonObj = testDetialArray.getJSONObject(i);
					TestDetails testDetail = new TestDetails();
					testDetail.setCatId(testJsonObj.getInt("cid"));
					testDetail.setsCatId(testJsonObj.getInt("sCid"));
					testDetail.setAns(testJsonObj.getInt("ans"));
					testDetailList.add(testDetail);
				}

				wordProblemLevelDetail.setLevelData(wordProblemLevelList);
				wordProblemLevelDetail.setUpdatedData(updatedList);
				wordProblemLevelDetail.setTestDeatilList(testDetailList);

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return wordProblemLevelDetail;
		}else{
			return null;
		}
	}

	/**
	 * This method get the question from server according grade
	 * @param grade
	 */
	public QuestionLoadedTransferObj getWordProblemQuestions(int grade){

		if(grade > 8)
			grade = 8;
		String action = "getWordProblemQuestions";

		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "grade="   + grade;
		//Log.e(TAG, "inside getWordProblemQuestions url : " + strUrl);

		return this.parseWordProblemQuestionJson(CommonUtils.readFromURL(strUrl));
	}

	/**
	 * This method get the question from server according grade
	 * @param grade
	 */
	public QuestionLoadedTransferObj getWordProblemQuestionsForSpanish(int grade , int lang){

		if(grade > 8)
			grade = 8;

		String action = "getWordProblemQuestionsForLang";

		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "grade="   + grade + "&"
				+"lang=" + lang;
		//Log.e(TAG, "inside getWordProblemQuestions url : " + strUrl);

		return this.parseWordProblemQuestionJson(CommonUtils.readFromURL(strUrl));
	}

	/**
	 * This method parse word problem question json 
	 * @param jsonString
	 */
	private QuestionLoadedTransferObj parseWordProblemQuestionJson(String jsonString){

		//Log.e(TAG, "inside parse word problem question json jsonStrign : " + jsonString);
		if(jsonString != null){
			QuestionLoadedTransferObj questionData = new QuestionLoadedTransferObj();

			ArrayList<CategoryListTransferObj> categoryList = new ArrayList<CategoryListTransferObj>();
			ArrayList<SubCatergoryTransferObj> subCategoryList = new ArrayList<SubCatergoryTransferObj>();
			ArrayList<WordProblemQuestionTransferObj> questionList  = new ArrayList<WordProblemQuestionTransferObj>();

			try 
			{
				JSONObject json = new JSONObject(jsonString);

				JSONArray categoryArrayObj = json.getJSONArray("categList");

				for(int i = 0 ; i < categoryArrayObj.length() ; i ++ ){

					subCategoryList = new ArrayList<SubCatergoryTransferObj>();

					JSONObject jsonCategoryObj = categoryArrayObj.getJSONObject(i);

					CategoryListTransferObj categoryObj = new CategoryListTransferObj();
					categoryObj.setCid(jsonCategoryObj.getInt("cId"));
					categoryObj.setName(jsonCategoryObj.getString("name"));


					JSONArray jsonSubCategoryArray = jsonCategoryObj.getJSONArray("subCategories");

					for(int j = 0 ; j < jsonSubCategoryArray.length() ; j ++ ){

						JSONObject subCategiryJsonObj = jsonSubCategoryArray.getJSONObject(j);
						SubCatergoryTransferObj subCategoryObj = new SubCatergoryTransferObj();

						subCategoryObj.setId(subCategiryJsonObj.getInt("id"));
						subCategoryObj.setName(subCategiryJsonObj.getString("name"));
						subCategoryObj.setUrl(subCategiryJsonObj.getString("url"));

						subCategoryList.add(subCategoryObj);
					}

					categoryObj.setSubCategories(subCategoryList);

					categoryList.add(categoryObj);
				}

				JSONArray questionJsonArray = json.getJSONArray("data");

				for( int k = 0 ; k < questionJsonArray.length() ; k ++ ){

					JSONObject jsonQuestionObj = questionJsonArray.getJSONObject(k);

					WordProblemQuestionTransferObj questionObj = new WordProblemQuestionTransferObj();
					questionObj.setCatId(jsonQuestionObj.getInt("categ"));
					questionObj.setSubCatId(jsonQuestionObj.getInt("subCateg"));
					questionObj.setQuestionId(jsonQuestionObj.getInt("quesId"));
					questionObj.setQuestion(jsonQuestionObj.getString("question"));
					questionObj.setOpt1(jsonQuestionObj.getString("opt1"));
					questionObj.setOpt2(jsonQuestionObj.getString("opt2"));
					questionObj.setOpt3(jsonQuestionObj.getString("opt3"));
					questionObj.setOpt4(jsonQuestionObj.getString("opt4"));
					questionObj.setOpt5(jsonQuestionObj.getString("opt5"));
					questionObj.setOpt6(jsonQuestionObj.getString("opt6"));
					questionObj.setOpt7(jsonQuestionObj.getString("opt7"));
					questionObj.setOpt8(jsonQuestionObj.getString("opt8"));
					questionObj.setAns(jsonQuestionObj.getString("ans"));
					questionObj.setImage(jsonQuestionObj.getString("img"));
					questionObj.setLeftAns(jsonQuestionObj.getString("leftAns"));
					questionObj.setRightAns(jsonQuestionObj.getString("rightAns"));
					questionObj.setFillIn(jsonQuestionObj.getInt("fillIn"));
					questionObj.setForTest(jsonQuestionObj.getInt("forTest"));

					questionList.add(questionObj);
				}

				questionData.setCategoryList(categoryList);
				questionData.setQuestionList(questionList);

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return questionData;
		}else{
			return null;
		}
	}

	/**
	 * This method dawanload mage from server for school curriculum
	 * @param imageName
	 * @return
	 */
	public byte[] dawabloadImageFromServer(String imageName){
		byte[] image = null;

		try {
			//Log.e("", "url " + ICommonUtils.IMAGE_DAWNLOAD_URL + imageName);
			image = CommonUtils.getByteFromBitmap(CommonUtils.getBitmap(ICommonUtils.IMAGE_DAWNLOAD_URL + imageName));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return image;
	}

	/**
	 * this method update addMathWordProblemPlayLevelScore on server
	 * @param mathScoreObj
	 */
	public void addMathWordProblemPlayLevelScore(MathScoreForSchoolCurriculumTransferObj mathScoreObj){
		String action = "addMathWordProblemPlayLevelScore";
		//Log.e(TAG, "equation " + mathScoreObj.getProblems());
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		try 
		{
			nameValuePairs.add(new BasicNameValuePair("action", action));
			nameValuePairs.add(new BasicNameValuePair("userId", mathScoreObj.getUserId()));
			nameValuePairs.add(new BasicNameValuePair("playerId", mathScoreObj.getPlayerId()));
			nameValuePairs.add(new BasicNameValuePair("problems", "<equations>" + mathScoreObj.getProblems() 
					+ "</equations>"));
			nameValuePairs.add(new BasicNameValuePair("categoryId", mathScoreObj.getCatId() + ""));
			nameValuePairs.add(new BasicNameValuePair("subCategoryId", mathScoreObj.getSubCatId() + ""));
			nameValuePairs.add(new BasicNameValuePair("stars", mathScoreObj.getStars() + ""));
			nameValuePairs.add(new BasicNameValuePair("totalPoints", mathScoreObj.getPoints() + ""));
			nameValuePairs.add(new BasicNameValuePair("coins", mathScoreObj.getCoins() + ""));
		}
		catch (Exception e1) 
		{			
			Log.e(TAG, "Error in addMathWordProblemPlayLevelScore While parsing" + e1);
		}
		this.parseJson(CommonUtils.readFromURL(nameValuePairs));
	}

	/**
	 * This method parse json
	 * @param jsonString
	 */
	private void parseJson(String jsonString) {
		//Log.e(TAG, "Json String " + jsonString);
	}

	public void addWordProblemScore(MathScoreForSchoolCurriculumTransferObj mathObj , int totalScore){
		String action = "addWordProblemScore";

		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		try 
		{
			nameValuePairs.add(new BasicNameValuePair("action", action));
			nameValuePairs.add(new BasicNameValuePair("gameType", "Play"));
			nameValuePairs.add(new BasicNameValuePair("userId", mathObj.getUserId()));
			nameValuePairs.add(new BasicNameValuePair("playerId", mathObj.getPlayerId()));
			nameValuePairs.add(new BasicNameValuePair("problems", "<equations>" + mathObj.getProblems() 
					+ "</equations>"));
			nameValuePairs.add(new BasicNameValuePair("totalScore", totalScore + ""));

		}
		catch (Exception e1) 
		{			
			Log.e(TAG, "Error in addWordProblemScore " + e1);
		}
		this.parseJson(CommonUtils.readFromURL(nameValuePairs));
	}

	/**
	 * This method get learning center time for word problem
	 */
	public LearnCenterTimeObj getTimeLearnCenter(){
		String action = "getTimeLearnCenter";
		String strUrl = COMPLETE_URL  + "action=" + action;
		return this.parseJsonForGeTTimeLearnCenter(CommonUtils.readFromURL(strUrl));
	}

	/**
	 * This method parse json String for getTimeLearnCenter
	 * @param jsonString
	 */
	private LearnCenterTimeObj parseJsonForGeTTimeLearnCenter(String jsonString){

		if(jsonString != null){
			LearnCenterTimeObj timeObj = new LearnCenterTimeObj();
			ArrayList<LearnCenterTimeForCategoryObj> catTimeList = new ArrayList<LearnCenterTimeForCategoryObj>();
			try {
				JSONObject json = new JSONObject(jsonString);
				timeObj.setRemainingTime(json.getLong("timeRemain"));

				JSONArray catTimeArray = json.getJSONArray("data");

				for(int i = 0 ; i < catTimeArray.length() ; i ++ ){
					JSONObject jsonCatTimeObj = catTimeArray.getJSONObject(i);
					LearnCenterTimeForCategoryObj cetTimeObj = new LearnCenterTimeForCategoryObj();
					cetTimeObj.setCatId(jsonCatTimeObj.getInt("cid"));
					cetTimeObj.setSuBCatId(jsonCatTimeObj.getInt("sCid"));
					cetTimeObj.setTime(jsonCatTimeObj.getLong("time"));
					catTimeList.add(cetTimeObj);
				}
				timeObj.setCategoryTimeList(catTimeList);
			} catch (JSONException e) {
				Log.e(TAG, "inside parseJsonForGeTTimeLearnCenter Error While Parsing : " + e.toString());
			}
			return timeObj;
		}else{
			return null;
		}
	}

	/**
	 * This method add word problem level on server
	 * @param allLevel
	 */
	public void addAllWordProblemLevels(String allLevel){
		String action = "addAllWordProblemLevels";
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		try{
			nameValuePairs.add(new BasicNameValuePair("action", action));
			nameValuePairs.add(new BasicNameValuePair("allLevels", allLevel));
		}catch (Exception e1){			
			Log.e(TAG, "Error in addAllWordProblemLevels" + e1);
		}
		this.parseJson(CommonUtils.readFromURL(nameValuePairs));
	}

	/**
	 * This method update onto the server for subscription
	 * @param userId
	 * @param playerId
	 * @param coins
	 */
	public String saveSubscriptionForUser(String userId , String playerId , int coins , int duration){
		String action = "saveSubscriptionStatus";
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		try 
		{
			nameValuePairs.add(new BasicNameValuePair("action", action));
			nameValuePairs.add(new BasicNameValuePair("userId", userId));
			nameValuePairs.add(new BasicNameValuePair("duration", duration + ""));
			nameValuePairs.add(new BasicNameValuePair("playerId", playerId));
			nameValuePairs.add(new BasicNameValuePair("coins", coins + ""));
		}
		catch (Exception e1) 
		{			
			Log.e(TAG, "Error in addWordProblemScore " + e1);
		}
		return this.parseJsonStringForSubscription(CommonUtils.readFromURL(nameValuePairs));
	}

	/**
	 * This method parse the jsonString for subscription success
	 * @param jsonString
	 * @return
	 */
	private String parseJsonStringForSubscription(String jsonString)
	{
		//Log.e("", "jsonString : "+jsonString);
		try 
		{
			JSONObject jObject   = new JSONObject(jsonString);
			String date      = jObject.getString("data");
			return date;
		}
		catch(Exception e)
		{
			return null;
		}
	}

	//added for sending mail for wrong quesiton 
	/**
	 * This method send email when user feel the is wrong
	 * @param questionId
	 */
	public void sendEmailForWorngQuestion(int questionId , String msg){
		//String action = "problemWithQuestion";
		String action = "submitProblemWithQuestion";//for new
		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "quesId=" + questionId + "&message=" + msg;
		/*String strUrl = "http://192.168.1.17/~deepak/LeapAhead/LeapAhead/index.php?"  + "action=" + action + "&"
				+ "quesId="   + questionId + "&message=" + msg;*/
		//Log.e(TAG, "inside sendEmailForWorngQuestion url : " + strUrl);
		this.parseJsonStringForSendEmailForWrongQuestion(CommonUtils.readFromURL(strUrl));
	}

	/**
	 * This method parse the response string after sending mail
	 * @param jsonString
	 */
	private void parseJsonStringForSendEmailForWrongQuestion(String jsonString){
		//Log.e(TAG, "inside parseJsonStringForSendEmailForWrongQuestion jsonString " + jsonString);
	}

	//changes for downloading wordproblem categories
	/**
	 * This method download the word problem categories
	 * @param grade
	 * @param lang
	 */
	public QuestionLoadedTransferObj getCategoriesList(int grade , int lang){

		if(grade > 8)
			grade = 8;
		String action = "getCategoriesList";
		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "grade="   + grade + "&"
				+"lang=" + lang;
		//Log.e(TAG, "inside getCategoriesList url : " + strUrl);
		return this.parseJsonStringForWordProblemCategories(CommonUtils.readFromURL(strUrl));
	}

	/**
	 * This method parse the jsonString for wordproblem categories
	 * @param jsonString
	 */
	private QuestionLoadedTransferObj parseJsonStringForWordProblemCategories(String jsonString){
		//Log.e(TAG, "json String " + jsonString);

		if(jsonString != null){
			QuestionLoadedTransferObj questionData = new QuestionLoadedTransferObj();
			ArrayList<CategoryListTransferObj> categoryList = new ArrayList<CategoryListTransferObj>();
			ArrayList<SubCatergoryTransferObj> subCategoryList = new ArrayList<SubCatergoryTransferObj>();
			try 
			{
				JSONObject json = new JSONObject(jsonString);
				JSONArray categoryArrayObj = json.getJSONArray("data");

				for(int i = 0 ; i < categoryArrayObj.length() ; i ++ ){

					subCategoryList = new ArrayList<SubCatergoryTransferObj>();

					JSONObject jsonCategoryObj = categoryArrayObj.getJSONObject(i);

					CategoryListTransferObj categoryObj = new CategoryListTransferObj();
					categoryObj.setCid(jsonCategoryObj.getInt("cId"));
					categoryObj.setName(jsonCategoryObj.getString("name"));


					JSONArray jsonSubCategoryArray = jsonCategoryObj.getJSONArray("subCategories");

					for(int j = 0 ; j < jsonSubCategoryArray.length() ; j ++ ){

						JSONObject subCategiryJsonObj = jsonSubCategoryArray.getJSONObject(j);
						SubCatergoryTransferObj subCategoryObj = new SubCatergoryTransferObj();

						subCategoryObj.setId(subCategiryJsonObj.getInt("id"));
						subCategoryObj.setName(subCategiryJsonObj.getString("name"));
						subCategoryObj.setUrl(subCategiryJsonObj.getString("url"));

						subCategoryList.add(subCategoryObj);
					}

					categoryObj.setSubCategories(subCategoryList);
					categoryList.add(categoryObj);
					questionData.setCategoryList(categoryList);
				}
				return questionData;
			}catch (JSONException e) {
				Log.e(TAG, "Error while parsing " + e.toString());
				return null;
			}
		}else{
			return null;
		}
	}

}
