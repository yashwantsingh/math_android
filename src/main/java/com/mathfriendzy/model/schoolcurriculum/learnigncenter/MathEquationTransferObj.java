package com.mathfriendzy.model.schoolcurriculum.learnigncenter;

import java.io.Serializable;

public class MathEquationTransferObj implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int eqautionId;
	private String startDate;
	private String endData;
	private int timeTakenToAnswer;
	private int catId;
	private int subCatId;
	private int points;//points for each equation
	private int isAnswerCorrect;// for answer correct or not
	private String userAnswer;
	
	private WordProblemQuestionTransferObj questionObj;
	
	//added for assessment test
	private boolean isQuestionAttempt;
	private String userAnserForAssessment;//only for showing selected option
	
	public String getUserAnserForAssessment() {
		return userAnserForAssessment;
	}

	public void setUserAnserForAssessment(String userAnserForAssessment) {
		this.userAnserForAssessment = userAnserForAssessment;
	}

	public boolean isQuestionAttempt() {
		return isQuestionAttempt;
	}
	
	public void setQuestionAttempt(boolean isQuestionAttempt) {
		this.isQuestionAttempt = isQuestionAttempt;
	}
	public WordProblemQuestionTransferObj getQuestionObj() {
		return questionObj;
	}
	public void setQuestionObj(WordProblemQuestionTransferObj questionObj) {
		this.questionObj = questionObj;
	}
	public int getEqautionId() {
		return eqautionId;
	}
	public void setEqautionId(int eqautionId) {
		this.eqautionId = eqautionId;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndData() {
		return endData;
	}
	public void setEndData(String endData) {
		this.endData = endData;
	}
	public int getTimeTakenToAnswer() {
		return timeTakenToAnswer;
	}
	public void setTimeTakenToAnswer(int timeTakenToAnswer) {
		this.timeTakenToAnswer = timeTakenToAnswer;
	}
	public int getCatId() {
		return catId;
	}
	public void setCatId(int catId) {
		this.catId = catId;
	}
	public int getSubCatId() {
		return subCatId;
	}
	public void setSubCatId(int subCatId) {
		this.subCatId = subCatId;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	public int getIsAnswerCorrect() {
		return isAnswerCorrect;
	}
	public void setIsAnswerCorrect(int isAnswerCorrect) {
		this.isAnswerCorrect = isAnswerCorrect;
	}
	public String getUserAnswer() {
		return userAnswer;
	}
	public void setUserAnswer(String userAnswer) {
		this.userAnswer = userAnswer;
	}
}
