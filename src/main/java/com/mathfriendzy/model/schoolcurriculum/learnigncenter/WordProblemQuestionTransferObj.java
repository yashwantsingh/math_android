package com.mathfriendzy.model.schoolcurriculum.learnigncenter;

import java.io.Serializable;
import java.util.ArrayList;

public class WordProblemQuestionTransferObj implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int catId;
	private int subCatId;
	private int questionId;
	private String question;
	
	private String opt1;
	private String opt2;
	public ArrayList<String> getOptionList() {
		return optionList;
	}
	public void setOptionList(ArrayList<String> optionList) {
		this.optionList = optionList;
	}
	private String opt3;
	private String opt4;
	private String opt5;
	private String opt6;
	private String opt7;
	private String opt8;
	
	private String ans;
	private String image;
	private String leftAns;
	private String rightAns;
	
	private int fillIn;
	private int forTest;
	
	private ArrayList<String> optionList;
	
	public int getCatId() {
		return catId;
	}
	public void setCatId(int catId) {
		this.catId = catId;
	}
	public int getSubCatId() {
		return subCatId;
	}
	public void setSubCatId(int subCatId) {
		this.subCatId = subCatId;
	}
	public int getQuestionId() {
		return questionId;
	}
	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getOpt1() {
		return opt1;
	}
	public void setOpt1(String opt1) {
		this.opt1 = opt1;
	}
	public String getOpt2() {
		return opt2;
	}
	public void setOpt2(String opt2) {
		this.opt2 = opt2;
	}
	public String getOpt3() {
		return opt3;
	}
	public void setOpt3(String opt3) {
		this.opt3 = opt3;
	}
	public String getOpt4() {
		return opt4;
	}
	public void setOpt4(String opt4) {
		this.opt4 = opt4;
	}
	public String getOpt5() {
		return opt5;
	}
	public void setOpt5(String opt5) {
		this.opt5 = opt5;
	}
	public String getOpt6() {
		return opt6;
	}
	public void setOpt6(String opt6) {
		this.opt6 = opt6;
	}
	public String getOpt7() {
		return opt7;
	}
	public void setOpt7(String opt7) {
		this.opt7 = opt7;
	}
	public String getOpt8() {
		return opt8;
	}
	public void setOpt8(String opt8) {
		this.opt8 = opt8;
	}
	public String getAns() {
		return ans;
	}
	public void setAns(String ans) {
		this.ans = ans;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getLeftAns() {
		return leftAns;
	}
	public void setLeftAns(String leftAns) {
		this.leftAns = leftAns;
	}
	public String getRightAns() {
		return rightAns;
	}
	public void setRightAns(String rightAns) {
		this.rightAns = rightAns;
	}
	public int getFillIn() {
		return fillIn;
	}
	public void setFillIn(int fillIn) {
		this.fillIn = fillIn;
	}
	public int getForTest() {
		return forTest;
	}
	public void setForTest(int forTest) {
		this.forTest = forTest;
	}
}
