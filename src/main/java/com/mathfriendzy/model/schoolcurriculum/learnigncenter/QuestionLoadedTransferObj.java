package com.mathfriendzy.model.schoolcurriculum.learnigncenter;

import java.util.ArrayList;

public class QuestionLoadedTransferObj {
	
	private ArrayList<CategoryListTransferObj> categoryList;
	private ArrayList<WordProblemQuestionTransferObj> questionList;
	
	public ArrayList<CategoryListTransferObj> getCategoryList() {
		return categoryList;
	}
	public void setCategoryList(ArrayList<CategoryListTransferObj> categoryList) {
		this.categoryList = categoryList;
	}
	public ArrayList<WordProblemQuestionTransferObj> getQuestionList() {
		return questionList;
	}
	public void setQuestionList(
			ArrayList<WordProblemQuestionTransferObj> questionList) {
		this.questionList = questionList;
	}
	
}
