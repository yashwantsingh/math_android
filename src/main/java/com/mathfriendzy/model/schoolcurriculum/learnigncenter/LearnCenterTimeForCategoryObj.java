package com.mathfriendzy.model.schoolcurriculum.learnigncenter;

public class LearnCenterTimeForCategoryObj {
	private int catId;
	private int suBCatId;
	private long time;
	
	public int getCatId() {
		return catId;
	}
	public void setCatId(int catId) {
		this.catId = catId;
	}
	public int getSuBCatId() {
		return suBCatId;
	}
	public void setSuBCatId(int suBCatId) {
		this.suBCatId = suBCatId;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	
}
