package com.mathfriendzy.model.schoolcurriculum.learnigncenter;

import java.util.Comparator;

public class SortByCategoryName implements Comparator<CategoryListTransferObj>{

	@Override
	public int compare(CategoryListTransferObj lhs, CategoryListTransferObj rhs) {
		return lhs.getName().compareTo(rhs.getName());
	}
}
