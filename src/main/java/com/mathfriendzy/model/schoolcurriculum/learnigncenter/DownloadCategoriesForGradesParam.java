package com.mathfriendzy.model.schoolcurriculum.learnigncenter;

/**
 * Created by root on 13/6/16.
 */
public class DownloadCategoriesForGradesParam {
    private String action;
    private String grades;
    private int lang;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getLang() {
        return lang;
    }

    public void setLang(int lang) {
        this.lang = lang;
    }

    public String getGrades() {
        return grades;
    }

    public void setGrades(String grades) {
        this.grades = grades;
    }
}
