package com.mathfriendzy.model.schoolcurriculum.learnigncenter;

import java.io.Serializable;

public class SubCatergoryTransferObj implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String name;
	private String url;
	
	//for assign home work
	private boolean isSelected;
	private int mainCatId;
	private int numberOfQuestions = 10;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public boolean isSelected() {
		return isSelected;
	}
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
	public int getMainCatId() {
		return mainCatId;
	}
	public void setMainCatId(int mainCatId) {
		this.mainCatId = mainCatId;
	}
	public int getNumberOfQuestions() {
		return numberOfQuestions;
	}
	public void setNumberOfQuestions(int numberOfQuestions) {
		this.numberOfQuestions = numberOfQuestions;
	}
	
}
