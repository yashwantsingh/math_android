package com.mathfriendzy.model.schoolcurriculum.learnigncenter;

import java.util.ArrayList;

public class CategoryListTransferObj {

	private int cid;
	private String name;
	private ArrayList<SubCatergoryTransferObj> subCategories;
	
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<SubCatergoryTransferObj> getSubCategories() {
		return subCategories;
	}
	public void setSubCategories(ArrayList<SubCatergoryTransferObj> subCategories) {
		this.subCategories = subCategories;
	}
}
