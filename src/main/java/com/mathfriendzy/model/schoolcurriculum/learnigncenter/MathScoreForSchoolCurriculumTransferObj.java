package com.mathfriendzy.model.schoolcurriculum.learnigncenter;

/**
 * For save player math score on server or in database
 * addMathWordProblemPlayLevelScore
 * @author Yashwant Singh
 *
 */
public class MathScoreForSchoolCurriculumTransferObj {
	private String userId;
	private String playerId;
	private String problems;
	private int catId;
	private int subCatId;
	private int stars;
	private int points;
	private int coins;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public String getProblems() {
		return problems;
	}
	public void setProblems(String problems) {
		this.problems = problems;
	}
	public int getCatId() {
		return catId;
	}
	public void setCatId(int catId) {
		this.catId = catId;
	}
	public int getSubCatId() {
		return subCatId;
	}
	public void setSubCatId(int subCatId) {
		this.subCatId = subCatId;
	}
	public int getStars() {
		return stars;
	}
	public void setStars(int stars) {
		this.stars = stars;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	public int getCoins() {
		return coins;
	}
	public void setCoins(int coins) {
		this.coins = coins;
	}
	
}
