package com.mathfriendzy.model.schoolcurriculum.learnigncenter;

import java.util.ArrayList;

public class LearnCenterTimeObj {
	private long remainingTime;
	private ArrayList<LearnCenterTimeForCategoryObj> categoryTimeList;
	
	public long getRemainingTime() {
		return remainingTime;
	}
	public void setRemainingTime(long remainingTime) {
		this.remainingTime = remainingTime;
	}
	public ArrayList<LearnCenterTimeForCategoryObj> getCategoryTimeList() {
		return categoryTimeList;
	}
	public void setCategoryTimeList(
			ArrayList<LearnCenterTimeForCategoryObj> categoryTimeList) {
		this.categoryTimeList = categoryTimeList;
	}
	
}
