package com.mathfriendzy.model.schoolcurriculum.learnigncenter;

public class TestDetails {
	private int catId;
	private int sCatId;
	private int ans;
	
	public int getCatId() {
		return catId;
	}
	public void setCatId(int catId) {
		this.catId = catId;
	}
	public int getsCatId() {
		return sCatId;
	}
	public void setsCatId(int sCatId) {
		this.sCatId = sCatId;
	}
	public int getAns() {
		return ans;
	}
	public void setAns(int ans) {
		this.ans = ans;
	}
	
}
