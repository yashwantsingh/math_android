package com.mathfriendzy.model.schoolcurriculum.learnigncenter;

import com.mathfriendzy.serveroperation.HttpResponseBase;

import java.util.ArrayList;

/**
 * Created by root on 13/6/16.
 */
public class DownloadCategoriesForGradesResponse extends HttpResponseBase{
    private String result;
    private int grade;
    private ArrayList<CategoryListTransferObj> categories;

    //for containing your self list
    private ArrayList<DownloadCategoriesForGradesResponse> categoriesForGradesResponses;


    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public ArrayList<CategoryListTransferObj> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<CategoryListTransferObj> categories) {
        this.categories = categories;
    }

    public ArrayList<DownloadCategoriesForGradesResponse> getCategoriesForGradesResponses() {
        return categoriesForGradesResponses;
    }

    public void setCategoriesForGradesResponses(ArrayList<DownloadCategoriesForGradesResponse> categoriesForGradesResponses) {
        this.categoriesForGradesResponses = categoriesForGradesResponses;
    }
}
