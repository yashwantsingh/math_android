package com.mathfriendzy.model.schoolcurriculum.learnigncenter;

import java.util.ArrayList;

public class wordProblemLevelDetailTransferObj {

	private ArrayList<WordProblemLevelTransferObj> levelData;
	private ArrayList<UpdatedInfoTransferObj>      updatedData;
	
	//added for thumb image in learning center for sub categories
	private ArrayList<TestDetails> testDeatilList;
	
	public ArrayList<TestDetails> getTestDeatilList() {
		return testDeatilList;
	}
	public void setTestDeatilList(ArrayList<TestDetails> testDeatilList) {
		this.testDeatilList = testDeatilList;
	}
	
	public ArrayList<WordProblemLevelTransferObj> getLevelData() {
		return levelData;
	}
	public void setLevelData(ArrayList<WordProblemLevelTransferObj> levelData) {
		this.levelData = levelData;
	}
	public ArrayList<UpdatedInfoTransferObj> getUpdatedData() {
		return updatedData;
	}
	public void setUpdatedData(ArrayList<UpdatedInfoTransferObj> updatedData) {
		this.updatedData = updatedData;
	}
}
