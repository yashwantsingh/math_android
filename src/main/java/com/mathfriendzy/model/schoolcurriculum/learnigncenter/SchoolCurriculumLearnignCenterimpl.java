package com.mathfriendzy.model.schoolcurriculum.learnigncenter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mathfriendzy.database.Database;

import java.util.ArrayList;

import static com.mathfriendzy.utils.ICommonUtils.TEMP_PLAYER_OPERATION_FLAG;

public class SchoolCurriculumLearnignCenterimpl {

	private SQLiteDatabase dbConn = null;
	private Context context 	  = null;

	public SchoolCurriculumLearnignCenterimpl(Context context)
	{
		this.context = context;
	}

	/**
	 * Open the database connection
	 */
	public void openConnection()
	{
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
	}

	/**
	 * Close the database connection
	 */
	public void closeConnection()
	{
		if(dbConn != null)
			dbConn.close();
	}

	/**
	 * This method check equation is loaded in database or not corresponding to database
	 * @param grade
	 * @return
	 */
	public boolean isQuestionLeaded(int grade){

		if(grade > 8)
			grade = 8;
			
		String query = "select * from WordProblemsQuestions where category_id in " +
				"(select word_category_id from " +
				" WordProblemCategories where grade_level = '" + grade + "')";
		Cursor cursor =  dbConn.rawQuery(query, null);

		if(cursor.moveToNext()){
			this.closeCursor(cursor);
			return true;
		}
		else{
			this.closeCursor(cursor);
			return false;
		}
	}

	private void closeCursor(Cursor cursor){
		if(cursor != null)
			cursor.close();
	}

	/**
	 * This method insert the data into the WordProblemLevelStar table
	 * @param wordProblemLevelList
	 */
	public void insertIntoWordProblemLevelStar(ArrayList<WordProblemLevelTransferObj> wordProblemLevelList){
        try {
            if (wordProblemLevelList != null && wordProblemLevelList.size() > 0) {
                for (int i = 0; i < wordProblemLevelList.size(); i++) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("user_id", wordProblemLevelList.get(i).getUserId());
                    contentValues.put("player_id", wordProblemLevelList.get(i).getPlayerId());
                    contentValues.put("category_id", wordProblemLevelList.get(i).getCategoryId());
                    contentValues.put("subCategory_id", wordProblemLevelList.get(i).getSubCategoryId());
                    contentValues.put("Star", wordProblemLevelList.get(i).getStars());
                    if (dbConn.insert("WordProblemLevelStar", null, contentValues) > 0) {
                        //Log.e("SchoolCurriculumLearnignCenterimpl", " WordProblemLevelStar insert successfully!");
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
	}
	
	/**
	 * This method delete the data from the WordProblemLevelStar table
	 */
	public void deleteFromWordProblemLevelStar(){
		dbConn.delete("WordProblemLevelStar" , null , null);
	}

	/**
	 * This method insert the data into the WordProblemUpdateDetails table
	 * @param updatedList
	 */
	public void insertIntoWordProblemUpdateDetails(ArrayList<UpdatedInfoTransferObj> updatedList){

		for(int i = 0 ; i < updatedList.size() ; i ++ ){
			ContentValues contentValues = new ContentValues();
			contentValues.put("grade", updatedList.get(i).getGrade());
			contentValues.put("date", updatedList.get(i).getUpdatedDate());
			if(dbConn.insert("WordProblemUpdateDetails", null, contentValues) > 0 ){
				//Log.e("SchoolCurriculumLearnignCenterimpl", "WordProblemUpdateDetails insert successfully!");
			}
		}
	}

	/**
	 * This will check initially , if data into table exist or not means for blank table
	 */
	public boolean isWordProblemUpdateDetailsTableDataExist(){
		
		String query = "select * from WordProblemUpdateDetails";
		Cursor cursor =  dbConn.rawQuery(query, null);
		
		if(cursor.moveToNext()){
			this.closeCursor(cursor);
			return true;
		}
		else{
			this.closeCursor(cursor);
			return false;
		}
	}
	
	/**
	 * This method return the updated date according to the grade
	 * @param grade
	 * @return
	 */
	public String getUpdatedDate(int grade){
		
		if(grade > 8)
			grade = 8;
		
		String date = "";
		String query = "select * from WordProblemUpdateDetails where grade = '" + grade + "'";
		Cursor cursor =  dbConn.rawQuery(query, null);
		
		if(cursor.moveToNext()){
			date =  cursor.getString(cursor.getColumnIndex("date"));
		}
		this.closeCursor(cursor);
		
		return date;
	}
	
	/**
	 * This method updated the new server updated date from the old one
	 * @param grade
	 * @param updatedDate
	 */
	public void updateNewUpdatedDateFromServer(int grade , String updatedDate){
		
		if(grade > 8)
			grade = 8;
		
		String where = "grade = '" + grade +"'";
		ContentValues cv = new ContentValues();
		cv.put("date", updatedDate);
		dbConn.update("WordProblemUpdateDetails", cv, where, null);
	}
	
	/**
	 * This method insert the data into WordProblemCategories table from server
	 */
	public void insertIntoWordProblemCategories(ArrayList<CategoryListTransferObj> categoryList , int grade){
		
		if(grade > 8)
			grade = 8;
		for(int i = 0 ; i < categoryList.size() ; i ++ ){
			ContentValues contentValues = new ContentValues();
			contentValues.put("word_category_id", categoryList.get(i).getCid());
			contentValues.put("category_name", categoryList.get(i).getName());
			contentValues.put("grade_level", grade);
			
				if(dbConn.insert("WordProblemCategories", null, contentValues) > 0 ){
				//Log.e("SchoolCurriculumLearnignCenterimpl", " WordProblemLevelStar insert successfully!");
			}
		}
	}
	
	/**
	 * This method delete the category id according to grade
	 * @param grade
	 */
	public void deleteFromWordProblemCategoriesbyGrade(int grade){
		if(grade > 8)
			grade = 8;
		dbConn.delete("WordProblemCategories" , "grade_level=?" , new String[]{grade + ""});
	}
	
	/**
	 * This method delete the data from the WordProblemsSubCategoriesByCategoryId
	 * @param categoryList
	 */
	public void deleteFromWordProblemsSubCategoriesByCategoryId(ArrayList<CategoryListTransferObj> categoryList){
		String categories = "";
		
		for(  int  i = 0 ; i < categoryList.size() ; i ++)
		{
			if(i == categoryList.size() - 1)
				categories = categories + categoryList.get(i).getCid();
			else
				categories = categories + categoryList.get(i).getCid() + ",";
		}
		
		String where = "category_id in (" + categories + ")";
		dbConn.delete("WordProblemsSubCategories" , where, null);
	}
	
	/**
	 * This method insert the data into WordProblemsSubCategories table
	 * @param categoryList
	 */
	public void insertIntoWordProblemsSubCategories(ArrayList<CategoryListTransferObj> categoryList){
		
		for(int i = 0 ; i < categoryList.size() ; i ++ ){
			for( int j = 0 ; j < categoryList.get(i).getSubCategories().size() ; j ++ ){
				ContentValues contentValues = new ContentValues();
				contentValues.put("category_id", categoryList.get(i).getCid());
				contentValues.put("sub_category_id", categoryList.get(i).getSubCategories().get(j).getId());
				contentValues.put("sub_category_name", categoryList.get(i).getSubCategories().get(j).getName());
				contentValues.put("video_url", categoryList.get(i).getSubCategories().get(j).getUrl());
				
				if(dbConn.insert("WordProblemsSubCategories", null, contentValues) > 0 ){
					//Log.e("SchoolCurriculumLearnignCenterimpl", " WordProblemLevelStar insert successfully!");
				}
			}
		}
	}
	
	/**
	 * This method delete the data from the WordProblemsQuestions ByCategoryId
	 * @param categoryList
	 */
	public void deleteFromWordProblemsQuestionsCategoryId(ArrayList<CategoryListTransferObj> categoryList){
		String categories = "";
		
		for(  int  i = 0 ; i < categoryList.size() ; i ++)
		{
			if(i == categoryList.size() - 1)
				categories = categories + categoryList.get(i).getCid();
			else
				categories = categories + categoryList.get(i).getCid() + ",";
		}
		
		/*//dbConn.delete("WordProblemsQuestions" , "category_id in " , new String[]{categories});
		String query = "delete from WordProblemsQuestions where category_id in (" + categories + ")";
		//Log.e("SchoolCurriculumLearnignCenterimpl", "Query inside deleteFromWordProblemsQuestionsCategoryId " + query);
		Cursor cursor =  dbConn.rawQuery(query, null);
 		this.closeCursor(cursor);*/
		String where = "category_id in (" + categories + ")";
		dbConn.delete("WordProblemsQuestions" , where, null);
	}
	
	/**
	 * This method insert the data into WordProblemsSubCategories table
	 * @param questionList
	 */
	public void insertIntoWordProblemsQuestions(ArrayList<WordProblemQuestionTransferObj> questionList){
		
		for(int i = 0 ; i < questionList.size() ; i ++ ){
				ContentValues contentValues = new ContentValues();
				contentValues.put("category_id", questionList.get(i).getCatId());
				contentValues.put("subcategory_id", questionList.get(i).getSubCatId());
				contentValues.put("question_id", questionList.get(i).getQuestionId());
				contentValues.put("question", questionList.get(i).getQuestion());
				contentValues.put("option1", questionList.get(i).getOpt1());
				contentValues.put("option2", questionList.get(i).getOpt2());
				contentValues.put("option3", questionList.get(i).getOpt3());
				contentValues.put("option4", questionList.get(i).getOpt4());
				contentValues.put("option5", questionList.get(i).getOpt5());
				contentValues.put("option6", questionList.get(i).getOpt6());
				contentValues.put("option7", questionList.get(i).getOpt7());
				contentValues.put("option8", questionList.get(i).getOpt8());
				
				contentValues.put("ansPrefix", questionList.get(i).getLeftAns());
				contentValues.put("ansSuffix", questionList.get(i).getRightAns());
				contentValues.put("image", questionList.get(i).getImage());
				contentValues.put("fill_in_blank_option", questionList.get(i).getFillIn());
				contentValues.put("avialableForTest", questionList.get(i).getForTest());
				contentValues.put("correctAnswers", questionList.get(i).getAns());
					
				if(dbConn.insert("WordProblemsQuestions", null, contentValues) > 0 ){
					//Log.e("SchoolCurriculumLearnignCenterimpl", " WordProblemLevelStar insert successfully!");
				}
		}
	}
	
	/**
	 * This method check image table exist or not
	 * @param
	 * @return
	 */
	public boolean isImageTableExist()
	{
		if(TEMP_PLAYER_OPERATION_FLAG)
			Log.e("TempPlayerOperation", "inside isTemparyPlayerExist(");
		
		Cursor cursor = dbConn.rawQuery("SELECT * FROM sqlite_master WHERE type = ? AND name = ?", 
	    											new String[] {"table", "SchoolCurriculumImageTable"});
	    if(cursor.moveToFirst())
	    {
	    	cursor.close();
	    	return true;
	    }
	    else
	    {
	    	cursor.close();
	    	return false;
	    }
	}
	
	/**
	 * This method creates the school curriculum image table
	 */
	public void createSchoolCurriculumImageTable(){
		String query = "create table SchoolCurriculumImageTable ( imageName text , image BLOB )";
		dbConn.execSQL(query);
	}
	
	/**
	 * This method insert the images into database for school curriculum
	 * @param imageName
	 * @param image
	 */
	public void insertIntoSchoolCurriculumImageTable(String imageName , byte [] image){
		
		//Log.e("SchoolCurriculumLearnignCenterimpl", " inside insert");
		
		ContentValues contentValues = new ContentValues();
		contentValues.put("imageName", imageName);
		contentValues.put("image", image);
		if(dbConn.insert("SchoolCurriculumImageTable", null, contentValues) > 0 ){
			/*Log.e("SchoolCurriculumLearnignCenterimpl", 
					" SchoolCurriculumLearnignCenterimpl insert successfully!"
							+ DownloadImageTread.counter ++);*/
		}else{
			//Log.e("", "Problem in insertion");
		}
	}
	
	/**
	 * This method return true if image already loaded
	 * @param imageName
	 * @return
	 */
	public boolean isImageAlreadyDawnloaded(String imageName){
		
		String query = "select * from SchoolCurriculumImageTable where imageName = '" + imageName +"'";
		Cursor cursor =  dbConn.rawQuery(query, null);
		
		if(cursor.moveToNext()){
			this.closeCursor(cursor);
			return true;
		}
		else{
			this.closeCursor(cursor);
			return false;
		}
	}
	
	/**
	 * This method get categories from database and return it corresponding to the player grade
	 * @param grade
	 */
	public ArrayList<CategoryListTransferObj>  getCategoriesForSchoolCurriculum(int grade){
		
		if(grade > 8)
			grade = 8;
		
		ArrayList<CategoryListTransferObj> categoryList = new ArrayList<CategoryListTransferObj>();
		
		String query = "select * from WordProblemCategories where grade_level = '"
		+ grade +"' order by category_name";
		Cursor cursor =  dbConn.rawQuery(query, null);
		
		while(cursor.moveToNext()){
			CategoryListTransferObj categoryData = new CategoryListTransferObj();
			categoryData.setCid(cursor.getInt(cursor.getColumnIndex("word_category_id")));
			categoryData.setName(cursor.getString(cursor.getColumnIndex("category_name")));
			/*categoryData.setCid(cursor.getInt(0));
			categoryData.setName(cursor.getString(1));*/
			categoryList.add(categoryData);
		}
		
		this.closeCursor(cursor);
		return categoryList;
	}

    /**
     * Return the grade list for which categories downloaded
     * @return
     */
    public ArrayList<Integer> getGradeListForWhichCategoriesDownloaded(){
        ArrayList<Integer> gradeList = new ArrayList<Integer>();
        String query = "SELECT DISTINCT grade_level from WordProblemCategories";
        Cursor cursor =  dbConn.rawQuery(query, null);
        while(cursor.moveToNext()){
            gradeList.add(cursor.getInt(cursor.getColumnIndex("grade_level")));
        }
        this.closeCursor(cursor);
        return gradeList;
    }

	/**
	 * This method get categories from database and return it corresponding to the player grade
	 * @param
	 */
	public String getCategorieNameByCatIdForSchoolCurriculum(String catId){
		String catName = null;
		String query = "select category_name from WordProblemCategories " +
				"where word_category_id = '"
		+ catId +"'";
		
		Cursor cursor =  dbConn.rawQuery(query, null);
		if(cursor.moveToNext()){
			catName = cursor.getString(cursor.getColumnIndex("category_name"));
		}
		this.closeCursor(cursor);
		return catName;
	}
	
	/**
	 * this method return the sub category data according to the category id
	 * @param cetegoryId
	 * @return
	 */
	public ArrayList<SubCatergoryTransferObj> getSubChildCategories(int cetegoryId){
		
		ArrayList<SubCatergoryTransferObj> subCategoryList = new ArrayList<SubCatergoryTransferObj>();
		//String query = "select distinct * from WordProblemsSubCategories where category_id = '" + cetegoryId +"'";
		String query = "select distinct * from WordProblemsSubCategories where category_id = '" 
				+ cetegoryId +"' order by sub_category_name";
		Cursor cursor =  dbConn.rawQuery(query, null);
		
		while(cursor.moveToNext()){
			SubCatergoryTransferObj subcategoryData = new SubCatergoryTransferObj();
			subcategoryData.setId(cursor.getInt(cursor.getColumnIndex("sub_category_id")));
			subcategoryData.setName(cursor.getString(cursor.getColumnIndex("sub_category_name")));
			subcategoryData.setUrl(cursor.getString(cursor.getColumnIndex("video_url")));
			subcategoryData.setMainCatId(cetegoryId);
			subCategoryList.add(subcategoryData);
		}
		
		this.closeCursor(cursor);
		return subCategoryList;
	}
	
	public String getSchoolCurriculumSubCatNameByCatIdAndSubCatId(String catId , String subCatId){
		String subCatName = null;
		String query = "select sub_category_name from WordProblemsSubCategories where category_id = '" 
				+ catId +"' and sub_category_id = '" + subCatId +"'";
		Cursor cursor =  dbConn.rawQuery(query, null);
		
		if(cursor.moveToNext()){
			subCatName = cursor.getString(cursor.getColumnIndex("sub_category_name"));
		}
		
		this.closeCursor(cursor);
		return subCatName;
	}
	
	/**
	 * This method return the subcat name by id
	 * @param id
	 * @return
	 */
	public String getSubChildCatNameById(int id){
		String subCatName = "";
		String query = "select sub_category_name from WordProblemsSubCategories where sub_category_id = '" 
				+ id +"'";
		Cursor cursor =  dbConn.rawQuery(query, null);
		if(cursor.moveToNext()){
			subCatName = cursor.getString(0);
		}
		return subCatName;
	}
	
	
	/**
	 * This method return the word problem data for school curriculum
	 * @param userId
	 * @param playerId
	 */
	public ArrayList<WordProblemLevelTransferObj> getWordProblemLevelData(String userId , String playerId){
	
		ArrayList<WordProblemLevelTransferObj> wordProblemLevelList = new ArrayList<WordProblemLevelTransferObj>();
		
		String query = "select * from WordProblemLevelStar where user_id = '" + userId 
						+"' and player_id = '" + playerId +"'";
		Cursor cursor =  dbConn.rawQuery(query, null);
		while(cursor.moveToNext()){
			WordProblemLevelTransferObj levelData = new WordProblemLevelTransferObj();
			levelData.setUserId(cursor.getString(cursor.getColumnIndex("user_id")));
			levelData.setPlayerId(cursor.getString(cursor.getColumnIndex("player_id")));
			levelData.setCategoryId(cursor.getInt(cursor.getColumnIndex("category_id")));
			levelData.setSubCategoryId(cursor.getInt(cursor.getColumnIndex("subCategory_id")));
			levelData.setStars(cursor.getInt(cursor.getColumnIndex("Star")));
			
			wordProblemLevelList.add(levelData);
		}
		
		this.closeCursor(cursor);
		return wordProblemLevelList;
	}
	
	/**
	 * This method return the word problem data for school curriculum
	 * @param userId
	 * @param playerId
	 */
	public ArrayList<WordProblemLevelTransferObj> getWordProblemLevelData(String userId , String playerId , int categoryId){
	
		ArrayList<WordProblemLevelTransferObj> wordProblemLevelList = new ArrayList<WordProblemLevelTransferObj>();
		
		String query = "select * from WordProblemLevelStar where user_id = '" + userId 
						+"' and player_id = '" + playerId +"' and category_id = '" + categoryId +"'";
		Cursor cursor =  dbConn.rawQuery(query, null);
		while(cursor.moveToNext()){
			WordProblemLevelTransferObj levelData = new WordProblemLevelTransferObj();
			levelData.setUserId(cursor.getString(cursor.getColumnIndex("user_id")));
			levelData.setPlayerId(cursor.getString(cursor.getColumnIndex("player_id")));
			levelData.setCategoryId(cursor.getInt(cursor.getColumnIndex("category_id")));
			levelData.setSubCategoryId(cursor.getInt(cursor.getColumnIndex("subCategory_id")));
			levelData.setStars(cursor.getInt(cursor.getColumnIndex("Star")));
			
			wordProblemLevelList.add(levelData);
		}
		
		this.closeCursor(cursor);
		return wordProblemLevelList;
	}
	
	/**
	 * This method get equation from data base accordign to the category and sub categiry id
	 * @param categoryId
	 * @param subCategoryId
	 */
	public ArrayList<WordProblemQuestionTransferObj> getQuestionAnswer(int categoryId , int subCategoryId){
		
		ArrayList<WordProblemQuestionTransferObj> questionAnswerList = 
				new ArrayList<WordProblemQuestionTransferObj>();
		
		String query = "select * from WordProblemsQuestions where category_id = '" + categoryId 
				+"' and subcategory_id = '" + subCategoryId +"'";
		
		Cursor cursor =  dbConn.rawQuery(query, null);
		
		while(cursor.moveToNext()){
			
			ArrayList<String> optionList = new ArrayList<String>();
			
			WordProblemQuestionTransferObj quetionObj = new WordProblemQuestionTransferObj();
			quetionObj.setCatId(cursor.getInt(cursor.getColumnIndex("category_id")));
			quetionObj.setSubCatId(cursor.getInt(cursor.getColumnIndex("subcategory_id")));
			quetionObj.setQuestionId(cursor.getInt(cursor.getColumnIndex("question_id")));
			quetionObj.setQuestion(cursor.getString(cursor.getColumnIndex("question")));
			
			optionList.add(cursor.getString(cursor.getColumnIndex("option1")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option2")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option3")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option4")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option5")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option6")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option7")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option8")));
			quetionObj.setOptionList(optionList);
			
			quetionObj.setLeftAns(cursor.getString(cursor.getColumnIndex("ansPrefix")));
			quetionObj.setRightAns(cursor.getString(cursor.getColumnIndex("ansSuffix")));
			quetionObj.setImage(cursor.getString(cursor.getColumnIndex("image")));
			quetionObj.setFillIn(cursor.getInt(cursor.getColumnIndex("fill_in_blank_option")));
			quetionObj.setForTest(cursor.getInt(cursor.getColumnIndex("avialableForTest")));
			quetionObj.setAns(cursor.getString(cursor.getColumnIndex("correctAnswers")));
			
			questionAnswerList.add(quetionObj);
		}
		
		this.closeCursor(cursor);
		
		return questionAnswerList;
	}
	
	/**
	 * Get question by question id
	 * @param questionId
	 * @return
	 */
	public WordProblemQuestionTransferObj getQuestionByQuestionId(int questionId){
		WordProblemQuestionTransferObj quetionObj = new WordProblemQuestionTransferObj();
		String query = "select * from WordProblemsQuestions where question_id = '" + questionId  +"'";
		Cursor cursor =  dbConn.rawQuery(query, null);
		
		if(cursor.moveToNext()){
			ArrayList<String> optionList = new ArrayList<String>();
						
			quetionObj.setCatId(cursor.getInt(cursor.getColumnIndex("category_id")));
			quetionObj.setSubCatId(cursor.getInt(cursor.getColumnIndex("subcategory_id")));
			quetionObj.setQuestionId(cursor.getInt(cursor.getColumnIndex("question_id")));
			quetionObj.setQuestion(cursor.getString(cursor.getColumnIndex("question")));
			
			optionList.add(cursor.getString(cursor.getColumnIndex("option1")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option2")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option3")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option4")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option5")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option6")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option7")));
			optionList.add(cursor.getString(cursor.getColumnIndex("option8")));
			quetionObj.setOptionList(optionList);
			
			quetionObj.setLeftAns(cursor.getString(cursor.getColumnIndex("ansPrefix")));
			quetionObj.setRightAns(cursor.getString(cursor.getColumnIndex("ansSuffix")));
			quetionObj.setImage(cursor.getString(cursor.getColumnIndex("image")));
			quetionObj.setFillIn(cursor.getInt(cursor.getColumnIndex("fill_in_blank_option")));
			quetionObj.setForTest(cursor.getInt(cursor.getColumnIndex("avialableForTest")));
			quetionObj.setAns(cursor.getString(cursor.getColumnIndex("correctAnswers")));
		}
		
		this.closeCursor(cursor);
		return quetionObj;
	}
	
	/**
	 * This method return the fill in value 
	 * @param questionId
	 * @return
	 */
	public int getFillIn(int questionId){
		int fill = 0;
		String query = "select fill_in_blank_option from WordProblemsQuestions where question_id = '" 
		+ questionId  +"'";
		Cursor cursor =  dbConn.rawQuery(query, null);
		if(cursor.moveToNext()){
			fill = cursor.getInt(cursor.getColumnIndex("fill_in_blank_option"));
		}
		this.closeCursor(cursor);
		return fill;
	}
	
	/**
	 * This method check image available in image table or not
	 * return true if found in table otherwise return false
	 * @param imageName
	 * @return
	 */
	public boolean checkImageAvailabity(String imageName){
		boolean isAvailable = false;
		String query = "select imageName from SchoolCurriculumImageTable where imageName = '"
						+ imageName	+"'";
		Cursor cursor =  dbConn.rawQuery(query, null);
		if(cursor.moveToNext())
			isAvailable = true;
		this.closeCursor(cursor);
		
		return isAvailable;
	}
	
	/**
	 * This method check image available in image table or not
	 * return true if found in table otherwise return false
	 * @param imageName
	 * @return
	 */
	public byte []  getImage(String imageName){
		byte [] image = null;
		String query = "select image from SchoolCurriculumImageTable where imageName = '"
						+ imageName	+"'";
		Cursor cursor =  dbConn.rawQuery(query, null);
		if(cursor.moveToNext())
			image = cursor.getBlob(cursor.getColumnIndex("image"));
		this.closeCursor(cursor);
		return image;
	}
	
	/**
	 * This method check the user data already exist or not
	 * if exist then return true
	 * otherwise false
	 */
	public boolean isWordProblemLevelStarExist(String userId , String playerId , int catId , int subCatId){
		
		boolean isExist = false;
		String query = "select * from WordProblemLevelStar where user_id = '" +  userId + "' and " +
				"player_id = '" + playerId + "' and category_id = '" + catId 
				 + "' and subCategory_id = '" + subCatId + "'";
		Cursor cursor = dbConn.rawQuery(query, null);
		
		if(cursor.moveToNext())
			isExist = true;
		else
			isExist = false;
		this.closeCursor(cursor);
		return isExist;
	}
	
	/**
	 * This method update the data into WordProblemLevelStar table
	 * @param wordProblemLevelData
	 */
	public void updateWordProblemLevelStar(WordProblemLevelTransferObj wordProblemLevelData){
		
		String where = 	" user_id = '" +  wordProblemLevelData.getUserId() + "' and " +
						"player_id = '" + wordProblemLevelData.getPlayerId() 
						+ "' and category_id = '" + wordProblemLevelData.getCategoryId() 
						+ "' and subCategory_id = '" + wordProblemLevelData.getSubCategoryId() + "'";
		
		ContentValues cv = new ContentValues();
		cv.put("Star", wordProblemLevelData.getStars());
		dbConn.update("WordProblemLevelStar", cv, where, null);
	}
	
	/**
	 * This method save data into WordProblemLevelStar
	 * @param wordProblemLevelData
	 */
	public void insertIntoWordProblemLevelStar(WordProblemLevelTransferObj wordProblemLevelData){
		ContentValues contentValues = new ContentValues();
		contentValues.put("user_id", wordProblemLevelData.getUserId());
		contentValues.put("player_id", wordProblemLevelData.getPlayerId());
		contentValues.put("category_id", wordProblemLevelData.getCategoryId());
		contentValues.put("subCategory_id",wordProblemLevelData.getSubCategoryId());
		contentValues.put("Star", wordProblemLevelData.getStars());
		dbConn.insert("WordProblemLevelStar", null, contentValues);
	}
	
	/**
	 * Save word problem play data when user not connected to the intenet
	 */
	public void insertIntoWordProblemResults(MathScoreForSchoolCurriculumTransferObj mathScoreObj){
		ContentValues contentValues = new ContentValues();
		contentValues.put("userId", mathScoreObj.getUserId());
		contentValues.put("playerId", mathScoreObj.getPlayerId());
		contentValues.put("problems",mathScoreObj.getProblems());
		dbConn.insert("WordProblemResults", null, contentValues);
	}
	
	/**
	 * This method return the math result data
	 * @param userId
	 * @param playerId
	 */
	public ArrayList<MathScoreForSchoolCurriculumTransferObj> getWordProblemResultData
												(String userId , String playerId){
		ArrayList<MathScoreForSchoolCurriculumTransferObj> mathResultList = 
				new ArrayList<MathScoreForSchoolCurriculumTransferObj>();
		String query = "select * from WordProblemResults where userId = '" + userId 
						+ "' and playerId = '" + playerId + "'";
		Cursor cursor = dbConn.rawQuery(query, null);
		
		while(cursor.moveToNext()){
			MathScoreForSchoolCurriculumTransferObj mathResult = new MathScoreForSchoolCurriculumTransferObj();
			mathResult.setUserId(cursor.getString(cursor.getColumnIndex("userId")));
			mathResult.setPlayerId(cursor.getString(cursor.getColumnIndex("playerId")));
			mathResult.setProblems(cursor.getString(cursor.getColumnIndex("problems")));
			mathResultList.add(mathResult);
		}
		this.closeCursor(cursor);
		return mathResultList;
	}
	
	/**
	 * This method delete the record from Math_result Table
	 * @param
	 */
	public void deleteFromMathResult(String userid , String playerid)
	{
		 String where = "userId = '"+ userid + "' and playerId = '" + playerid + "'";
		 dbConn.delete("WordProblemResults" , where , null);
	}
	
	/**
	 * This method delete he data from WordLearnCenterTime table
	 */
	public void deleteFromWordLearnCenterTime(){
		 dbConn.delete("WordLearnCenterTime" , null , null);
	}
	
	/**
	 * This method insert the time data into WordLearnCenterTime table
	 * @param catTimeList
	 */
	public void insertIntoWordLearnCenterTime(ArrayList<LearnCenterTimeForCategoryObj> catTimeList){
        try {
            if (catTimeList != null && catTimeList.size() > 0) {
                for (int i = 0; i < catTimeList.size(); i++) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("categ_id", catTimeList.get(i).getCatId());
                    contentValues.put("subCateg_id", catTimeList.get(i).getSuBCatId());
                    contentValues.put("time", catTimeList.get(i).getTime());
                    dbConn.insert("WordLearnCenterTime", null, contentValues);
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
	}
	
	/**
	 * This method return the average learn center time
	 * @param catId
	 * @param subCatId
	 * @return
	 */
	public long getLearnCenterTime(int catId , int subCatId){
		long time = 0;
		String query = "select time from WordLearnCenterTime where categ_id = '" + catId 
				+ "' and subCateg_id = '" + subCatId + "'";
		Cursor cursor = dbConn.rawQuery(query, null);
		if(cursor.moveToNext()){
			time = cursor.getLong(cursor.getColumnIndex("time"));
		}
		else{
			this.closeCursor(cursor);
			query = "select time from WordLearnCenterTime where categ_id = '" + 0 
					+ "' and subCateg_id = '" + 0 + "'";
			cursor = dbConn.rawQuery(query, null);
			if(cursor.moveToNext())
				time = cursor.getLong(cursor.getColumnIndex("time"));
		}
		this.closeCursor(cursor);
		return time;
	}
	
	/**
	 * This method insert the word problem level star data into
	 * LocalWordProblemLevels table when user play learning center word problem
	 * without Internet
	 * 
	 * @param userId
	 * @param playerId
	 * @param catId
	 * @param subCatId
	 * @param stars
	 */
	public void insertIntoLocalWordProblemLevels(String userId , String playerId , 
			int catId , int subCatId , int stars){
		ContentValues contentValues = new ContentValues();
		contentValues.put("user_id", userId);
		contentValues.put("player_id", playerId);
		contentValues.put("category_id",catId);
		contentValues.put("subCategory_id",subCatId);
		contentValues.put("Star",stars);
		dbConn.insert("LocalWordProblemLevels", null, contentValues);
	}
	
	/**
	 * This method check data exist in this table or not
	 * for updating it on server when net is connected
	 * @param userId
	 * @param playerId
	 * @return
	 */
	public boolean isDataExistInLocalWordProblemLevels(String userId , String playerId){
		boolean isExist = false;
		String query = "select * from LocalWordProblemLevels where user_id = '" +  userId + "' and " +
				"player_id = '" + playerId + "'";
		Cursor cursor = dbConn.rawQuery(query, null);
		if(cursor.moveToNext())
			isExist = true;
		else
			isExist = false;
		this.closeCursor(cursor);
		return isExist;
	}
	
	/**
	 * This method delete the data from LocalWordProblemLevels table
	 * @param userId
	 * @param playerId
	 */
	public void deleteFromLocalWordProblemLevels(String userId , String playerId){
		 String where = "user_id = '"+ userId + "' and player_id = '" + playerId + "'";
		 dbConn.delete("LocalWordProblemLevels" , where , null);	
	}
}
