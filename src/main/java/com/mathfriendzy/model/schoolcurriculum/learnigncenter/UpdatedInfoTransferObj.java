package com.mathfriendzy.model.schoolcurriculum.learnigncenter;

public class UpdatedInfoTransferObj {

	private int grade;
	private String updatedDate;
	
	public int getGrade() {
		return grade;
	}
	public void setGrade(int grade) {
		this.grade = grade;
	}
	public String getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	
}
