package com.mathfriendzy.model.schoolcurriculum.singlefriendzy;

import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mathfriendzy.model.schoolcurriculum.learnigncenter.UpdatedInfoTransferObj;
import com.mathfriendzy.model.singleFriendzy.ChallengerDataFromServerTranseferObj;
import com.mathfriendzy.model.singleFriendzy.ChallengerTransferObj;
import com.mathfriendzy.utils.CommonUtils;

import android.util.Log;

public class SingleFriendzyServerOperationForWordProblem {

    private final String TAG = this.getClass().getSimpleName();

    /**
     * This method get the define time for single friendzy
     */
    public SingleFriendzyTimeTransObj getSingleFriendzyDefineTime(){
        String action = "getTimeDefinedForFriendzy";
        String strUrl = COMPLETE_URL  + "action=" + action;
        return this.parseJsonGetSingleFriendzyDefineTime(CommonUtils.readFromURL(strUrl));
    }

    /**
     * This method parse getSingleFriendzyDefineTime json String
     * @param jsonString
     */
    private SingleFriendzyTimeTransObj parseJsonGetSingleFriendzyDefineTime(String jsonString){

        if(jsonString != null){
            SingleFriendzyTimeTransObj singleFriemdzyTime = new SingleFriendzyTimeTransObj();
            ArrayList<SingleFriendzyGradeTimeTransObj> singleFriendzyTimeList =
                    new ArrayList<SingleFriendzyGradeTimeTransObj>();
            try {
                JSONObject jsonObj = new JSONObject(jsonString);

                JSONArray jsonGradeTimeArray = jsonObj.getJSONArray("data");
                for(int i = 0 ; i < jsonGradeTimeArray.length() ; i ++ ){
                    JSONObject timeObj = jsonGradeTimeArray.getJSONObject(i);
                    SingleFriendzyGradeTimeTransObj gradeTimeObj = new SingleFriendzyGradeTimeTransObj();
                    gradeTimeObj.setGrade(timeObj.getInt("grade"));
                    gradeTimeObj.setTime(timeObj.getLong("time"));
                    singleFriendzyTimeList.add(gradeTimeObj);
                }
                singleFriemdzyTime.setTimeList(singleFriendzyTimeList);
                singleFriemdzyTime.setRemainingTime(jsonObj.getLong("timeRemain"));

            } catch (JSONException e) {
                Log.e(TAG, "inside parseJsonGetSingleFriendzyDefineTime Error while parsing : " + e.toString());
            }
            return singleFriemdzyTime;
        }else{
            return null;
        }
    }

    /**
     * This method getWordProblemChallenger for single friendzy
     * @param userId
     * @param playerId
     * @param grade
     * @param isTempPlayer
     */
    public ChallengerDataFromServerTranseferObj getWordProblemChallenger(String userId , String playerId , int grade , boolean isTempPlayer){
        String action = "getWordProblemChallenger";
        String strUrl = null;
        if(isTempPlayer){
            strUrl = COMPLETE_URL  + "action=" + action + "&" +
                    "grade=" + grade;
        }else{
            strUrl = COMPLETE_URL  + "action=" + action + "&" +
                    "userId=" + userId + "&" +
                    "playerId=" + playerId + "&" +
                    "grade=" + grade;
        }
        //Log.e(TAG, "inside getWordProblemChallenger url : " +  strUrl);
        return this.parseWordProblemChallenger(CommonUtils.readFromURL(strUrl));
    }

    /**
     * This method parse word problem challenger json
     * @param jsonString
     */
    private ChallengerDataFromServerTranseferObj parseWordProblemChallenger(String jsonString){
        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside parseWordProblemChallenger jsonString " + jsonString);

        if(jsonString != null){
            ChallengerDataFromServerTranseferObj challengerData = new ChallengerDataFromServerTranseferObj();
            ArrayList<ChallengerTransferObj> challenegreList = new ArrayList<ChallengerTransferObj>();

            try
            {
                JSONObject jObject = new JSONObject(jsonString);
                JSONObject jsonObject2 = jObject.getJSONObject("data");
                JSONArray jsonChallengerArray = jsonObject2.getJSONArray("challengers");

                for( int i = 0 ; i < jsonChallengerArray.length() ; i ++ )
                {
                    JSONObject jsonObj = jsonChallengerArray.getJSONObject(i);

                    ChallengerTransferObj challengerObj = new ChallengerTransferObj();

                    challengerObj.setFirst(jsonObj.getString("first"));
                    challengerObj.setLast(jsonObj.getString("last"));
                    challengerObj.setPlayerId(jsonObj.getString("playerId"));
                    challengerObj.setUserId(jsonObj.getString("userId"));
                    challengerObj.setSum(jsonObj.getString("sum"));
                    challengerObj.setCountryIso(jsonObj.getString("country"));
                    challengerObj.setIsFakePlayer(jsonObj.getInt("isFakePlayer"));

                    challenegreList.add(challengerObj);
                }

                challengerData.setPushNotificationDevice(jsonObject2.getString("pushNotifDevices"));
                challengerData.setChallengerPlayerList(challenegreList);
            }
            catch (JSONException e)
            {
                Log.e(TAG, "Error while parsing player " + e.toString());
                e.printStackTrace();
            }
            return challengerData;
        }else{
            return null;
        }
    }

    /**
     * This method return the question updated date on server
     * @return
     */
    public ArrayList<UpdatedInfoTransferObj> getWordProbleQuestionUpdatedData(){
        String action = "getWordQuestionsUpdateDates";
        String strUrl = COMPLETE_URL  + "action=" + action;
        //Log.e(TAG, "url: " + strUrl);
        return this.parseGetWordQuestionsUpdateDates(CommonUtils.readFromURL(strUrl));
    }

    /**
     * This method return the question updated date on server
     * @return
     */
    public ArrayList<UpdatedInfoTransferObj> getWordProbleQuestionUpdatedDateForSpanish(int lang){
        String action = "getWordQuestionsUpdateDatesLang";
        String strUrl = COMPLETE_URL  + "action=" + action + "&"
                + "lang=" + lang;
        return this.parseGetWordQuestionsUpdateDates(CommonUtils.readFromURL(strUrl));
    }

    /**
     * This method parse word problem question updated dateList and return it
     * @param jsonString
     * @return
     */
    private ArrayList<UpdatedInfoTransferObj> parseGetWordQuestionsUpdateDates(String jsonString){
        if(CommonUtils.LOG_ON)
            Log.e(TAG, "inside parseGetWordQuestionsUpdateDates jsonString " + jsonString);
        if(jsonString != null){
            ArrayList<UpdatedInfoTransferObj>  updatedList = new ArrayList<UpdatedInfoTransferObj>();
            try
            {
                JSONObject json = new JSONObject(jsonString);
                JSONArray updatedArray = json.getJSONArray("data");
                for(int j = 0 ; j < updatedArray.length() ; j ++ ){
                    JSONObject jsonjUpdatedObj = updatedArray.getJSONObject(j);
                    Iterator<String> iterator = jsonjUpdatedObj.keys();
                    UpdatedInfoTransferObj updatedData = new UpdatedInfoTransferObj();
                    int grade = Integer.parseInt(iterator.next());
                    updatedData.setGrade(grade);
                    updatedData.setUpdatedDate(jsonjUpdatedObj.getString(grade + ""));
                    updatedList.add(updatedData);
                }
            } catch (JSONException e) {
                Log.e(TAG, "inside parseGetWordQuestionsUpdateDates error while parsing " + e.toString());
            }
            return updatedList;
        }else{
            return null;
        }
    }

    /**
     * This method get the word problem question for playing single friendzy
     * @param userId
     * @param playerId
     */
    public ArrayList<SigleFriendzyQuestionFromServer> getWordProblemQuestionInFriendzy(String userId , String playerId){
        String action = "getWordProblemQuestionInFriendzy";
        String strUrl = COMPLETE_URL  + "action=" + action + "&" +
                "userId=" + userId + "&" +
                "playerId=" + playerId;
        //Log.e(TAG, "inside getWordProblemQuestionInFriendzy url : " + strUrl);
        return this.parseWordProbleQuestionJsonString(CommonUtils.readFromURL(strUrl));
    }

    /**
     * This method parse the jsonString for the word problem question from server
     * @param jsonString
     */
    private ArrayList<SigleFriendzyQuestionFromServer> parseWordProbleQuestionJsonString(String jsonString){
        //Log.e(TAG, "inside parseWordProbleQuestionJsonString " + jsonString);

        ArrayList<SigleFriendzyQuestionFromServer> questionList	=
                new ArrayList<SigleFriendzyQuestionFromServer>();

        try{
            JSONObject jsonObj = new JSONObject(jsonString);
            JSONArray jsonQuesitonArray = jsonObj.getJSONArray("data");

            for(int i = 0 ; i < jsonQuesitonArray.length() ; i ++ ){
                JSONObject jsonQuestionObj =  jsonQuesitonArray.getJSONObject(i);
                SigleFriendzyQuestionFromServer questionObj = new SigleFriendzyQuestionFromServer();
                questionObj.setUserId(jsonQuestionObj.getString("userId"));
                questionObj.setPlayerId(jsonQuestionObj.getString("playerId"));
                questionObj.setcId(jsonQuestionObj.getInt("cId"));
                questionObj.setQuestionId(jsonQuestionObj.getInt("questionId"));
                questionObj.setIsCorrect(jsonQuestionObj.getInt("isCorrect"));
                questionObj.setPoints(jsonQuestionObj.getInt("points"));
                questionObj.setTimeTaken(jsonQuestionObj.getInt("timeTaken"));
                questionList.add(questionObj);
            }

        }catch (JSONException e) {
            Log.e(TAG, "inside parseWordProbleQuestionJsonString error while parsing " + e.toString());
        }
        return questionList;
    }

    /**
     * This method add the math score on server when win or not for
     * playing single friendzy school curriculum
     * @param userId
     * @param playerId
     * @param problems
     * @param totalscore
     * @param isFake
     * @param challangerPlayerId
     * @param isWin
     * @param gameType
     */
    public void addWordProblemFriendzyScore(String userId , String playerId, String problems , int totalscore
            , int isFake , String challangerPlayerId , int isWin, String gameType){

        String action = "addWordProblemFriendzyScore";
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try
        {
            nameValuePairs.add(new BasicNameValuePair("action", action));
            nameValuePairs.add(new BasicNameValuePair("gameType", gameType));
            nameValuePairs.add(new BasicNameValuePair("userId", userId));
            nameValuePairs.add(new BasicNameValuePair("playerId", playerId));
            nameValuePairs.add(new BasicNameValuePair("problems", "<equations>" + problems
                    + "</equations>"));
            nameValuePairs.add(new BasicNameValuePair("totalScore", totalscore + ""));
            nameValuePairs.add(new BasicNameValuePair("wasCompetingWithFakePlayer", isFake + ""));
            nameValuePairs.add(new BasicNameValuePair("challengerPlayerId", challangerPlayerId));
            nameValuePairs.add(new BasicNameValuePair("win", isWin + ""));
        }
        catch (Exception e1)
        {
            Log.e(TAG, "Error in addMathWordProblemPlayLevelScore While parsing" + e1);
        }

		/*String action = "addWordProblemFriendzyScore";
		String strURL = COMPLETE_URL  + "action=" 	+ 	action 			+ "&" 
				+ "gameType=" 		+ 	gameType + "&"
				+ "userId="		+	userId	+ "&"
				+ "playerId="		+	playerId		+ "&"
				+ "problems="	+    "<equations>" + problems 
				+ "</equations>"   	+ "&"
				+ "totalScore="  + totalscore + "&"
				+ "wasCompetingWithFakePlayer=" + isFake+ "&"
				+ "challengerPlayerId="    + challangerPlayerId   + "&"
				+ "win="  + isWin ;*/


        this.parseJsonStringForAddMathScoreForSingleFriendzyForSchoolCurriculum
                (CommonUtils.readFromURL(nameValuePairs));
		/*Log.e(TAG, "url " + strURL);
		this.parseJsonStringForAddMathScoreForSingleFriendzyForSchoolCurriculum
   	 	(CommonUtils.readFromURL(strURL));*/
    }

    /**
     * This method add single friendzy school curriculum score on server
     * when user play the level which is the previous level to the highest level
     */
    public void addMAthScore(String gameType , String userId , String playerId ,
                             String problems , int totalscore){
        String action = "addWordProblemFriendzyScore";
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try
        {
            nameValuePairs.add(new BasicNameValuePair("action", action));
            nameValuePairs.add(new BasicNameValuePair("gameType", gameType));
            nameValuePairs.add(new BasicNameValuePair("userId", userId));
            nameValuePairs.add(new BasicNameValuePair("playerId", playerId));
            nameValuePairs.add(new BasicNameValuePair("problems", "<equations>" + problems
                    + "</equations>"));
            nameValuePairs.add(new BasicNameValuePair("totalScore", totalscore + ""));
        }
        catch (Exception e1)
        {
            Log.e(TAG, "Error in addMathWordProblemPlayLevelScore While parsing" + e1);
        }

        this.parseJsonStringForAddMathScoreForSingleFriendzyForSchoolCurriculum
                (CommonUtils.readFromURL(nameValuePairs));
    }

    /**
     * This method parse json string for add math score on server
     * @param jsonString
     */
    private void parseJsonStringForAddMathScoreForSingleFriendzyForSchoolCurriculum(String jsonString){
        //Log.e(TAG, "inside parseJsonStringForAddMathScoreForSingleFriendzyForSchoolCurriculum "
        // + " jsonString " + jsonString);
    }
}
