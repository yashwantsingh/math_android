package com.mathfriendzy.model.schoolcurriculum.singlefriendzy;

/**
 * This class contain time according to the grade
 * @author Yashwant Singh
 *
 */
public class SingleFriendzyGradeTimeTransObj {
	private int grade;
	private long time;
	
	public int getGrade() {
		return grade;
	}
	public void setGrade(int grade) {
		this.grade = grade;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	
	
}
