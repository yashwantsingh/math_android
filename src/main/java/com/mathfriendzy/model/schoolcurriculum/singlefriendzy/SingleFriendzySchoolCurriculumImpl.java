package com.mathfriendzy.model.schoolcurriculum.singlefriendzy;

import java.util.ArrayList;

import com.mathfriendzy.database.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class SingleFriendzySchoolCurriculumImpl {
	
	private SQLiteDatabase dbConn = null;
	private Context context 	  = null;
	
	public SingleFriendzySchoolCurriculumImpl(Context context){
		this.context = context;
	}
	
	/**
	 * Open the database connection
	 */
	public void openConnection()
	{
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
	}
	
	/**
	 * Close the database connection
	 */
	public void closeConnection()
	{
		dbConn.close();
	}
	
	/**
	 * This method delete the old time from the table
	 */
	public void deleteFromWordSingleFriendzyTime(){
		dbConn.delete("WordSingleFriendzyTime" , null , null);
	}
	
	/**
	 * This method insert the single friendzy time into database
	 * @param singleFriendzyTimeList
	 */
	public void insertIntoWordSingleFriendzyTime(ArrayList<SingleFriendzyGradeTimeTransObj> 
										singleFriendzyTimeList){
		for(int i = 0 ; i < singleFriendzyTimeList.size() ; i ++ ){
			ContentValues contentValues = new ContentValues();
			contentValues.put("grade", singleFriendzyTimeList.get(i).getGrade());
			contentValues.put("time",singleFriendzyTimeList.get(i).getTime());
			dbConn.insert("WordSingleFriendzyTime", null, contentValues);
		}
	}
	/**
	 * This method return the single friendzy time for play equations
	 * @param grade
	 * @return
	 */
	public long getSingleFriendzyTime(int grade){
		long time = 0;
		String query = "select time from WordSingleFriendzyTime where grade = '" + grade + "'";
		
		Cursor cursor = dbConn.rawQuery(query, null);
		if(cursor.moveToNext()){
			time = cursor.getLong(cursor.getColumnIndex("time"));
		}
		this.closeCursor(cursor);
		return time;
	}
	
	/**
	 * This method get the question ids from database for displaying the quaeion for
	 * single friendzy
	 * @param userId
	 * @param playerId
	 * @param grade
	 */
	public ArrayList<Integer> getQuestionIdsFromDatabase(String userId , String playerId , int grade){
		
		if(grade > 8)
			grade = 8;
		
		ArrayList<Integer> questionIds = new ArrayList<Integer>();
		String query = "SELECT question_id, max(random()) AS r " +
				"FROM WordProblemsQuestions where category_id IN " +
				"(Select word_category_id From WordProblemCategories " +
				"where grade_level = '" + grade + "') AND avialableForTest = 0 AND " +
				"question_id NOT IN (Select QuestionId FROM WordQuestionsPlayed WHERE " +
				"User_id = '" + userId + "' AND Player_id = '" + playerId + "') " +
				"GROUP BY category_id, subcategory_id";
		Cursor cursor = dbConn.rawQuery(query, null);
		while(cursor.moveToNext()){
			questionIds.add(cursor.getInt(cursor.getColumnIndex("question_id")));
		}
		this.closeCursor(cursor);
		
		return questionIds;
	}
	
	/**
	 * This method close the cursor
	 * @param cursor
	 */
	private void closeCursor(Cursor cursor){
		if(cursor != null)
			cursor.close();
	}
	
	/**
	 * This method delete the played question from WordQuestionsPlayed
	 * when user does not question min 10 for played on single friendzy
	 * @param userId
	 * @param playerId
	 */
	public void deleteFromWordQuestionsPlayed(String userId , String playerId){
		 String where = "User_id = '"+ userId + "' and Player_id = '" + playerId + "'";
		 dbConn.delete("WordQuestionsPlayed" , where , null);
	}
	
	/**
	 * This method insert the quaetions which are played by user on single friendzy
	 * for school curriculum 
	 * @param userId
	 * @param playerId
	 * @param questionId
	 */
	public void insertIntoWordQuestionsPlayed(String userId , String playerId , int questionId){
		ContentValues contentValues = new ContentValues();
		contentValues.put("User_id", userId);
		contentValues.put("Player_id",playerId);
		contentValues.put("QuestionId",questionId);
		dbConn.insert("WordQuestionsPlayed", null, contentValues);
	}
	
	/**
	 * This method return the questions ids from database for New Assessment test
	 * @param catId
	 * @param subCatIds
	 * @param aleadyFatchQuationIds
	 * @return
	 */
	public ArrayList<Integer> getQuestionId(int catId , String subCatIds ,
			ArrayList<Integer> aleadyFatchQuationIds){
		ArrayList<Integer> questionIds = new ArrayList<Integer>();
		
		String stringFromArray = "";
		
		for(  int  i = 0 ; i < aleadyFatchQuationIds.size() ; i ++)
		{
			if(i == aleadyFatchQuationIds.size() - 1)
				stringFromArray = stringFromArray + aleadyFatchQuationIds.get(i);
			else
				stringFromArray = stringFromArray + aleadyFatchQuationIds.get(i) + ",";
		}
		
		String query = "SELECT question_id, max(random()) AS r FROM WordProblemsQuestions " +
				"where (category_id = '" + catId + 
				"'And subcategory_id IN (" + subCatIds + 
				")) AND question_id NOT in (" + stringFromArray + 
				") GROUP BY category_id, subcategory_id";
		Cursor cursor = dbConn.rawQuery(query, null);
		while(cursor.moveToNext()){
			questionIds.add(cursor.getInt(cursor.getColumnIndex("question_id")));
		}
		this.closeCursor(cursor);
		return questionIds;
	}
}
