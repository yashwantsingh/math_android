package com.mathfriendzy.model.schoolcurriculum.singlefriendzy;

import java.util.ArrayList;

public class SingleFriendzyTimeTransObj {
	
	private long remainingTime;
	private ArrayList<SingleFriendzyGradeTimeTransObj> timeList;
	
	public long getRemainingTime() {
		return remainingTime;
	}
	public void setRemainingTime(long remainingTime) {
		this.remainingTime = remainingTime;
	}
	public ArrayList<SingleFriendzyGradeTimeTransObj> getTimeList() {
		return timeList;
	}
	public void setTimeList(ArrayList<SingleFriendzyGradeTimeTransObj> timeList) {
		this.timeList = timeList;
	}
}
