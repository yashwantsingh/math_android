package com.mathfriendzy.model.tutoringsessions;

import com.mathfriendzy.serveroperation.HttpResponseBase;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by root on 21/7/15.
 */
public class TutoringSessionsForStudentResponse extends HttpResponseBase implements Serializable{

    private String result;
    private int requestId;
    private String userId;
    private String playerId;
    private String reqDate;
    private String message;
    private int isConnected;
    private String tutorUid;
    private String tutorPid;
    private String chatDialogId;
    private int isAnonymous;
    private int rating;
    private String timeSpent;
    private int disconnected;
    private String studentImage;
    private String tutorImage;
    private String workAreaImage;
    private String lastUpdatedByTutor;
    private int opponentInput;
    private String tutorName;
    private int inActive;
    private String hwDate;
    private String hwTitle;
    private String queNo;
    private int timeSpentStudent;

    private String paidSession;
    private String paidSessionTitle;
    private int tutorOnline;

    private ArrayList<TutoringSessionsForStudentResponse> list;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getReqDate() {
        return reqDate;
    }

    public void setReqDate(String reqDate) {
        this.reqDate = reqDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getIsConnected() {
        return isConnected;
    }

    public void setIsConnected(int isConnected) {
        this.isConnected = isConnected;
    }

    public String getTutorUid() {
        return tutorUid;
    }

    public void setTutorUid(String tutorUid) {
        this.tutorUid = tutorUid;
    }

    public String getChatDialogId() {
        return chatDialogId;
    }

    public void setChatDialogId(String chatDialogId) {
        this.chatDialogId = chatDialogId;
    }

    public String getTutorPid() {
        return tutorPid;
    }

    public void setTutorPid(String tutorPid) {
        this.tutorPid = tutorPid;
    }

    public int getIsAnonymous() {
        return isAnonymous;
    }

    public void setIsAnonymous(int isAnonymous) {
        this.isAnonymous = isAnonymous;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getDisconnected() {
        return disconnected;
    }

    public void setDisconnected(int disconnected) {
        this.disconnected = disconnected;
    }

    public String getTimeSpent() {
        return timeSpent;
    }

    public void setTimeSpent(String timeSpent) {
        this.timeSpent = timeSpent;
    }

    public String getStudentImage() {
        return studentImage;
    }

    public void setStudentImage(String studentImage) {
        this.studentImage = studentImage;
    }

    public String getTutorImage() {
        return tutorImage;
    }

    public void setTutorImage(String tutorImage) {
        this.tutorImage = tutorImage;
    }

    public String getWorkAreaImage() {
        return workAreaImage;
    }

    public void setWorkAreaImage(String workAreaImage) {
        this.workAreaImage = workAreaImage;
    }

    public String getLastUpdatedByTutor() {
        return lastUpdatedByTutor;
    }

    public void setLastUpdatedByTutor(String lastUpdatedByTutor) {
        this.lastUpdatedByTutor = lastUpdatedByTutor;
    }

    public int getOpponentInput() {
        return opponentInput;
    }

    public void setOpponentInput(int opponentInput) {
        this.opponentInput = opponentInput;
    }

    public String getTutorName() {
        return tutorName;
    }

    public void setTutorName(String tutorName) {
        this.tutorName = tutorName;
    }

    public int getInActive() {
        return inActive;
    }

    public void setInActive(int inActive) {
        this.inActive = inActive;
    }

    public String getHwDate() {
        return hwDate;
    }

    public void setHwDate(String hwDate) {
        this.hwDate = hwDate;
    }

    public String getHwTitle() {
        return hwTitle;
    }

    public void setHwTitle(String hwTitle) {
        this.hwTitle = hwTitle;
    }

    public ArrayList<TutoringSessionsForStudentResponse> getList() {
        return list;
    }

    public void setList(ArrayList<TutoringSessionsForStudentResponse> list) {
        this.list = list;
    }

    public String getQueNo() {
        return queNo;
    }

    public void setQueNo(String queNo) {
        this.queNo = queNo;
    }

    public String getPaidSession() {
        return paidSession;
    }

    public void setPaidSession(String paidSession) {
        this.paidSession = paidSession;
    }

    public String getPaidSessionTitle() {
        return paidSessionTitle;
    }

    public void setPaidSessionTitle(String paidSessionTitle) {
        this.paidSessionTitle = paidSessionTitle;
    }

    public int getTimeSpentStudent() {
        return timeSpentStudent;
    }

    public void setTimeSpentStudent(int timeSpentStudent) {
        this.timeSpentStudent = timeSpentStudent;
    }

    public int getTutorOnline() {
        return tutorOnline;
    }

    public void setTutorOnline(int tutorOnline) {
        this.tutorOnline = tutorOnline;
    }
}
