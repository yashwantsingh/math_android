package com.mathfriendzy.model.assessmenttest;

import java.io.Serializable;
import java.util.ArrayList;

public class StandardsDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int stdId;
	String name;
	int grade;

	ArrayList<SubStandardsDto> subStandardList;
	
	//added for assessment report 
	int score;
	int round;
		
	public int getRound() {
		return round;
	}
	public void setRound(int round) {
		this.round = round;
	}
	
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	
	public int getStdId() {
		return stdId;
	}
	public void setStdId(int stdId) {
		this.stdId = stdId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getGrade() {
		return grade;
	}
	public void setGrade(int grade) {
		this.grade = grade;
	}
	public ArrayList<SubStandardsDto> getSubStandardList() {
		return subStandardList;
	}
	public void setSubStandardList(ArrayList<SubStandardsDto> subStandardList) {
		this.subStandardList = subStandardList;
	}
}
