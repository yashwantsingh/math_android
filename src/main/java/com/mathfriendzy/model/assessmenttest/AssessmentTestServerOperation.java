package com.mathfriendzy.model.assessmenttest;

import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.mathfriendzy.controller.assessmenttest.MathAssessmentTestResultObj;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;

import android.util.Log;

/**
 * This class for assessment test server operation
 * @author Yashwant Singh
 *
 */
public class AssessmentTestServerOperation {

	private final String TAG = this.getClass().getSimpleName();

	/**
	 * This method return the assessment date status
	 * @param userId
	 * @param playerId
	 */
	public AssessmentStatusDto  getAssessmentDateAndStatus(String userId , String playerId){
		//String action = "getAssessmentDateAndStatus";
		//changes for New Assessment
		String action = "getNewAssessmentDateAndStatus";
		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "userId="   + userId  + "&"
				+ "playerId=" + playerId;
		//Log.e(TAG, "url : " + strUrl);
		return this.parseAssessmentDataStatusJsonString(CommonUtils.readFromURL(strUrl));
	}

	/**
	 * This method parse the date status json string
	 * @param jsonString
	 */
	private AssessmentStatusDto parseAssessmentDataStatusJsonString(String jsonString){
		//Log.e(TAG,"json String " + jsonString);

		if(jsonString != null){
			AssessmentStatusDto assessmentStatusObj = new AssessmentStatusDto();
			ArrayList<StandardRoundsDto> standardRoundList = new ArrayList<StandardRoundsDto>();
			//String lastDate = "";

			try{
				JSONObject jsonObj = new JSONObject(jsonString);
				//lastDate  = jsonObj.getString("lastDate");

				JSONArray standardRoundArray = jsonObj.getJSONArray("standardRounds");
				for(int i = 0 ; i < standardRoundArray.length() ; i ++ ){
					JSONObject jsonStandardRoundObj = standardRoundArray.getJSONObject(i);
					StandardRoundsDto stanrdRound = new StandardRoundsDto();
					stanrdRound.setRounds(jsonStandardRoundObj.getInt("round"));
					stanrdRound.setStdId(jsonStandardRoundObj.getInt("stdId"));
					stanrdRound.setLastDate(jsonStandardRoundObj.getString("lastDate"));
					standardRoundList.add(stanrdRound);
				}
				//assessmentStatusObj.setLastPlayDate(lastDate);
				assessmentStatusObj.setStandardsRoundList(standardRoundList);

			}catch (JSONException e) {
				Log.e(TAG, "Inside parseAssessmentDataStatusJsonString , Error While parsing " + e.toString());
				return null;
			}
			return assessmentStatusObj;
		}else{
			return null;
		}
	}

	/**
	 * This method update assessment play score on server
	 * @param assessmentTestResultObj
	 */
	public void addTestAssessmentScore(MathAssessmentTestResultObj assessmentTestResultObj){

		//String action = "addTestAssessmentScore";
		//changes for New Assessment
		String action = "addNewAssessmentTestScore";

		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		try 
		{
			nameValuePairs.add(new BasicNameValuePair("action", action));
			nameValuePairs.add(new BasicNameValuePair("userId", assessmentTestResultObj.getUserId()));
			nameValuePairs.add(new BasicNameValuePair("playerId", assessmentTestResultObj.getPlayerId()));
			nameValuePairs.add(new BasicNameValuePair("grade", assessmentTestResultObj.getGrade() + ""));
			nameValuePairs.add(new BasicNameValuePair("date", assessmentTestResultObj.getDate()));
			nameValuePairs.add(new BasicNameValuePair("correctScored", assessmentTestResultObj
					.getCorrectScore() + ""));
			nameValuePairs.add(new BasicNameValuePair("problems", assessmentTestResultObj.getProblems()));

			//changes for New Assessment
			nameValuePairs.add(new BasicNameValuePair("standard", assessmentTestResultObj.getStandardId() + ""));
			nameValuePairs.add(new BasicNameValuePair("subStdScore", assessmentTestResultObj.getSubStandarsTimeAndScore()));
		}
		catch (Exception e1) 
		{			
			Log.e(TAG, "inside addTestAssessmentScore Error in assesssment score on server While parsing" + e1);
		}

		this.parseAddAssessmentTestScoreJsonString(CommonUtils.readFromURL(nameValuePairs));
		//this.parseAddAssessmentTestScoreJsonString(CommonUtils.readFromURLTemp(nameValuePairs));
	}

	/**
	 * This method parse the jsonString for assessment test play score
	 * @param jsonString
	 */
	private void parseAddAssessmentTestScoreJsonString(String jsonString){
		//Log.e(TAG , "inside parseAddAssessmentTestScoreJsonString jsonString " + jsonString);
	}

	/**
	 * This method get the update assessment date for standards
	 */
	public String getUpdatedDateAssessment(int lang){
		String action = "getUpdatedDateAssessment";
		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "lang="   + lang;

		//Log.e(TAG, "url " + strUrl);
		return this.parseGetUpadatedDateAssessment(CommonUtils.readFromURL(strUrl));
	}

	/**
	 * This method parse the assessment updated date jsonString
	 * @param jsonString
	 */
	private String parseGetUpadatedDateAssessment(String jsonString){
		//Log.e(TAG, "json String " + jsonString);

		if(jsonString != null){
			String resultDate = "";
			try {
				JSONObject jsonObj = new JSONObject(jsonString);
				resultDate = jsonObj.getString("data");

			} catch (JSONException e) {
				Log.e(TAG, "inside parseGetUpadatedDateAssessment Error while reading from server " + e.toString());
			}
			return resultDate;
		}else{
			return null;
		}
	}

	/**
	 * Get Assessment Standard from server
	 * @param lang
	 */
	public AssessmentStandardDto getAssessmentStandards(int lang){
		String action = "getAssessmentStandards";
		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "lang="   + lang;
		//Log.e(TAG, "url : " + strUrl);
		return this.parseJsonStringForAssessmentStandards(CommonUtils.readFromURL(strUrl));

	}

	/**
	 * This method parse the jsonString for getAssessmentStandards
	 * @param jsonString
	 */
	private AssessmentStandardDto parseJsonStringForAssessmentStandards(String jsonString){
		//Log.e(TAG, "JsonString " + jsonString);

		if(jsonString != null){
			AssessmentStandardDto assessmentStandard = new AssessmentStandardDto();

			ArrayList<StandardsDto> standardDataList = new ArrayList<StandardsDto>();
			ArrayList<CatagoriesDto> categoriesList  = new ArrayList<CatagoriesDto>();

			try {
				JSONObject jsonObj = new JSONObject(jsonString);
				JSONArray jsonDataArray 		= jsonObj.getJSONArray("data");
				JSONArray jsonCategoriesArray 	= jsonObj.getJSONArray("categories");

				for(int i = 0 ; i < jsonDataArray.length() ; i ++ ){
					JSONObject jsonDataObj = jsonDataArray.getJSONObject(i);
					StandardsDto stdObj = new StandardsDto();
					stdObj.setStdId(jsonDataObj.getInt("stdId"));
					stdObj.setName(jsonDataObj.getString("name"));
					stdObj.setGrade(jsonDataObj.getInt("grade"));

					ArrayList<SubStandardsDto> subStandardList = new ArrayList<SubStandardsDto>();
					JSONArray jsonSubStandardArray = jsonDataObj.getJSONArray("subStandards");
					for(int j = 0 ; j < jsonSubStandardArray.length() ; j ++ ){
						JSONObject jsonSubStandardObj = jsonSubStandardArray.getJSONObject(j);
						SubStandardsDto subStandardObj = new SubStandardsDto();
						subStandardObj.setStdId(jsonDataObj.getInt("stdId"));//from the standard obj
						subStandardObj.setSubStdId(jsonSubStandardObj.getInt("id"));
						subStandardObj.setSubStandardName(jsonSubStandardObj.getString("name"));
						subStandardList.add(subStandardObj);
					}

					stdObj.setSubStandardList(subStandardList);
					standardDataList.add(stdObj);
				}

				for(int k = 0 ; k < jsonCategoriesArray.length() ; k ++ ){
					JSONObject jsonCategoryObj = jsonCategoriesArray.getJSONObject(k);
					CatagoriesDto categoryObj = new CatagoriesDto();
					categoryObj.setStdId(jsonCategoryObj.getInt("stdId"));
					categoryObj.setSubStdId(jsonCategoryObj.getInt("subStd"));
					categoryObj.setCatId(jsonCategoryObj.getInt("categId"));
					categoryObj.setSubCatId(jsonCategoryObj.getString("subCategId"));
					categoriesList.add(categoryObj);
				}
				assessmentStandard.setCategoriesList(categoriesList);
				assessmentStandard.setStandardDataList(standardDataList);
			} catch (JSONException e) {
				Log.e(TAG, "inside parseJsonStringForAssessmentStandards Error while parsing : " + e.toString());
			}

			return assessmentStandard;
		}else{
			return null;
		}
	}

	/**
	 * This method get the assessment report for teacher
	 * @param grade
	 * @param userId
	 */
	public AssessmentReportForTeacherDto getAssessmentReportForTeacher(int grade , String userId){
		String action = "getAssessmentReportForTeacher";
		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "teacherId="   + userId + "&"
				+ "grade=" + grade;
		//Log.e(TAG, "url " + strUrl);

		return this.parseAssessmentReportForTeacher(CommonUtils.readFromURL(strUrl));
	}

	/**
	 * This method parse the json string for teacher report
	 * @param jsonString
	 */
	private AssessmentReportForTeacherDto parseAssessmentReportForTeacher(String jsonString){

		if(jsonString != null){
			AssessmentReportForTeacherDto assessmentReportForTeacher = new AssessmentReportForTeacherDto();
			ArrayList<StandardRoundsDto> standardRoundList = new ArrayList<StandardRoundsDto>();
			//Log.e(TAG, "jsonString " + jsonString);
			try {
				JSONObject jsonObj = new JSONObject(jsonString);
				JSONArray jsonDataArray = jsonObj.getJSONArray("data");
				for(int i = 0 ; i < jsonDataArray.length() ; i ++ ){
					JSONObject standardJsonObj = jsonDataArray.getJSONObject(i);
					StandardRoundsDto standardObj = new StandardRoundsDto();
					standardObj.setRounds(standardJsonObj.getInt("round"));
					standardObj.setScore(standardJsonObj.getInt("score"));
					standardObj.setStdId(standardJsonObj.getInt("stdId"));
					standardRoundList.add(standardObj);
				}
				assessmentReportForTeacher.setStandardRoundList(standardRoundList);
			} catch (JSONException e) {
				Log.e(TAG, "inside parseAssessmentReportForTeacher Error While Parsing : " + e.toString());
				return null;
			}
			return assessmentReportForTeacher;
		}else{
			return null;
		}
	}

	/**
	 * This method getDetailedScoreForAssessment from server 
	 * @param stdDto
	 * @param userId
	 */
	public DetailedScoreDto getDetailedScoreForAssessment(StandardsDto stdDto , String userId){
		String action = "getDetailedScoreForAssessment";
		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "teacherId="   + userId + "&"
				+ "grade=" + stdDto.getGrade() + "&"
				+ "round=" + stdDto.getRound() + "&"
				+ "standardId=" + stdDto.getStdId();
		//Log.e(TAG, "url : " + strUrl);
		return this.parseJsonStringForGetDetailedScoreForAssessment(CommonUtils.readFromURL(strUrl));

	}

	/**
	 * This method parse the jsonString for getDetailedScoreForAssessment
	 * @param jsonString
	 */
	private DetailedScoreDto parseJsonStringForGetDetailedScoreForAssessment(String jsonString){
		//Log.e(TAG, "JsonString " + jsonString);

		if(jsonString != null){
			DetailedScoreDto detailedScore = new DetailedScoreDto();
			ArrayList<StudentsScore> studentScoreList = new ArrayList<StudentsScore>();
			ArrayList<CCSSScore>     ccssScoreList    = new ArrayList<CCSSScore>();

			try {
				JSONObject jsonObj = new JSONObject(jsonString);
				JSONArray jsonStudentArray = jsonObj.getJSONArray("studentsScore");
				JSONArray jsonCCSSArray    = jsonObj.getJSONArray("ccssScore");

				for(int  i = 0 ; i < jsonStudentArray.length() ; i ++ ){
					JSONObject studentJsonObj = jsonStudentArray.getJSONObject(i);
					StudentsScore studentScore = new StudentsScore();
					studentScore.setUserId(studentJsonObj.getString("uid"));
					studentScore.setPlayerId(studentJsonObj.getString("pid"));
					studentScore.setName(studentJsonObj.getString("name"));
					studentScore.setTime(studentJsonObj.getInt("time"));
					studentScore.setScore(studentJsonObj.getInt("score"));

					JSONArray subStandardScoreJsonArray = studentJsonObj.getJSONArray("subStdScores");
					ArrayList<SubStandardsDto> subStandardScoreList = new ArrayList<SubStandardsDto>();

					for(int j = 0 ; j < subStandardScoreJsonArray.length() ; j ++ ){
						JSONObject subStandardJsonObj = subStandardScoreJsonArray.getJSONObject(j);
						SubStandardsDto subStandardObj = new SubStandardsDto();
						subStandardObj.setSubStdId(subStandardJsonObj.getInt("subStd"));
						subStandardObj.setTime(subStandardJsonObj.getInt("time"));
						subStandardObj.setScore(subStandardJsonObj.getInt("score"));
						subStandardScoreList.add(subStandardObj);
					}
					studentScore.setSubStandardScoreList(subStandardScoreList);
					studentScoreList.add(studentScore);
				}

				for(int i = 0 ; i < jsonCCSSArray.length() ; i ++ ){
					JSONObject jsonCCSSObj = jsonCCSSArray.getJSONObject(i);
					CCSSScore ccssScoreObj = new CCSSScore();
					ccssScoreObj.setId(jsonCCSSObj.getInt("id"));
					ccssScoreObj.setScore(jsonCCSSObj.getInt("score"));
					ccssScoreList.add(ccssScoreObj);
				}

				detailedScore.setStudentScoreList(studentScoreList);
				detailedScore.setCcssScoreList(ccssScoreList);

			} catch (JSONException e) {
				Log.e(TAG, "inside parseJsonStringForGetDetailedScoreForAssessment Error while parsing " + e.toString());
				return null;
			}
			return detailedScore;
		}else{
			return null;
		}
	}
}
