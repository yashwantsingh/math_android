package com.mathfriendzy.model.assessmenttest;

import java.io.Serializable;
import java.util.ArrayList;

public class StudentsScore implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String userId;
	private String playerId;
	private String name;
	private int time;
	private int score;
	
	private ArrayList<SubStandardsDto> subStandardScoreList;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public ArrayList<SubStandardsDto> getSubStandardScoreList() {
		return subStandardScoreList;
	}
	public void setSubStandardScoreList(
			ArrayList<SubStandardsDto> subStandardScoreList) {
		this.subStandardScoreList = subStandardScoreList;
	}
	
	
}
