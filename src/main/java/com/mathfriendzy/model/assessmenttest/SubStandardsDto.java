package com.mathfriendzy.model.assessmenttest;

import java.io.Serializable;


public class SubStandardsDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	int stdId;
	int subStdId;
	String subStandardName;
	
	//added for assessment score
	int score;
	int time;
	
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public int getStdId() {
		return stdId;
	}
	public void setStdId(int stdId) {
		this.stdId = stdId;
	}
	public int getSubStdId() {
		return subStdId;
	}
	public void setSubStdId(int subStdId) {
		this.subStdId = subStdId;
	}
	public String getSubStandardName() {
		return subStandardName;
	}
	public void setSubStandardName(String subStandardName) {
		this.subStandardName = subStandardName;
	}
}
