package com.mathfriendzy.model.assessmenttest;

public class StandardRoundsDto {
	
	private int stdId;
	private int rounds;
	//added for result report for teacher
	private int score;
	//added for play assessment multiple time if player want to play with other standard
	private String lastDate;
	
	
	public String getLastDate() {
		return lastDate;
	}
	public void setLastDate(String lastDate) {
		this.lastDate = lastDate;
	}
	public int getStdId() {
		return stdId;
	}
	public void setStdId(int stdId) {
		this.stdId = stdId;
	}
	
	public int getRounds() {
		return rounds;
	}
	public void setRounds(int rounds) {
		this.rounds = rounds;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
}
