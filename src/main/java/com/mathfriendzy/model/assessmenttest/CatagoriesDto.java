package com.mathfriendzy.model.assessmenttest;

import java.io.Serializable;
import java.util.ArrayList;

public class CatagoriesDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int stdId;
	private int subStdId;
	private int catId;
	private String subCatId;
	
	//for assign homework
	//private ArrayList<Integer> subCatList;
	
	public int getStdId() {
		return stdId;
	}
	public void setStdId(int stdId) {
		this.stdId = stdId;
	}
	public int getSubStdId() {
		return subStdId;
	}
	public void setSubStdId(int subStdId) {
		this.subStdId = subStdId;
	}
	public int getCatId() {
		return catId;
	}
	public void setCatId(int catId) {
		this.catId = catId;
	}
	public String getSubCatId() {
		return subCatId;
	}
	public void setSubCatId(String subCatId) {
		this.subCatId = subCatId;
	}
	/*public ArrayList<Integer> getSubCatList() {
		return subCatList;
	}
	public void setSubCatList(ArrayList<Integer> subCatList) {
		this.subCatList = subCatList;
	}*/
	
}
