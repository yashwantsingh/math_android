package com.mathfriendzy.model.assessmenttest;

import java.util.ArrayList;

public class AssessmentReportForTeacherDto {
	private ArrayList<StandardRoundsDto> standardRoundList;

	public ArrayList<StandardRoundsDto> getStandardRoundList() {
		return standardRoundList;
	}

	public void setStandardRoundList(ArrayList<StandardRoundsDto> standardRoundList) {
		this.standardRoundList = standardRoundList;
	}
	
}
