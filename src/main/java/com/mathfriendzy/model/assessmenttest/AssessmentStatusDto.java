package com.mathfriendzy.model.assessmenttest;

import java.util.ArrayList;

public class AssessmentStatusDto {
	private String lastPlayDate;
	private ArrayList<StandardRoundsDto> standardsRoundList;
	
	public String getLastPlayDate() {
		return lastPlayDate;
	}
	public void setLastPlayDate(String lastPlayDate) {
		this.lastPlayDate = lastPlayDate;
	}
	public ArrayList<StandardRoundsDto> getStandardsRoundList() {
		return standardsRoundList;
	}
	public void setStandardsRoundList(
			ArrayList<StandardRoundsDto> standardsRoundList) {
		this.standardsRoundList = standardsRoundList;
	}
}
