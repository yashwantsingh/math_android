package com.mathfriendzy.model.assessmenttest;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.mathfriendzy.controller.assessmenttest.MathAssessmentTestResultObj;
import com.mathfriendzy.database.Database;

public class AssessmentTestImpl {

	private SQLiteDatabase dbConn = null;
	private Context context 	  = null;

	public AssessmentTestImpl(Context context){
		this.context = context;
	}

	/**
	 * Open the database connection
	 */
	public void openConnection()
	{
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
	}

	/**
	 * Close the database connection
	 */
	public void closeConnection()
	{
		dbConn.close();
	}


	/**
	 * This method return the number of question required for assessment test
	 * @param grade
	 * @return
	 */
	public int getRequiredNumberOfQuestionForAssessmentTest(int grade){

		int requiredQuestion = 0;
		String query = "SELECT count(*) FROM WordProblemsSubCategories " +
				"Where category_id IN (Select word_category_id From " +
				"WordProblemCategories Where grade_level = '" + grade + "')";

		Cursor cursor = dbConn.rawQuery(query, null);
		if(cursor.moveToNext()){
			requiredQuestion = cursor.getInt(0);
		}
		this.closeCursor(cursor);

		return requiredQuestion;
	}

	private void closeCursor(Cursor cursor){
		if(cursor != null){
			cursor.close();
		}
	}

	/**
	 * This method return the last play assessment date 
	 * @param userId
	 * @param playerId
	 * @return
	 */
	public String getWordAssessmentLastInfo(String userId ,String playerId){
		String lastPlayDate = "";
		String query = "select last_played_date from WordAssessmentLastInfo where user_id = '" + userId 
				+ "' and player_id = '" + playerId + "'";
		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext()){
			lastPlayDate = cursor.getString(cursor.getColumnIndex("last_played_date"));
		}

		if(cursor != null)
			cursor.close();
		return lastPlayDate;
	}

	/**
	 * This method insert the new last play date
	 * @param userId
	 * @param playerId
	 */
	public void insertIntoWordAssessmentLastInfo(String userId , String playerId , String lastPlayDate){
		ContentValues contentValues = new ContentValues();
		contentValues.put("user_id", userId);
		contentValues.put("player_id", playerId);
		contentValues.put("last_played_date", lastPlayDate);
		dbConn.insert("WordAssessmentLastInfo", null, contentValues);
	}

	/**
	 * This method delete the last play date from assessment test
	 * @param userId
	 * @param playerId
	 */
	public void deleteFromWordAssessmentLastInfo(String userId , String playerId){
		String where = "user_id = '"+ userId + "' and player_id = '" + playerId + "'";
		dbConn.delete("WordAssessmentLastInfo" , where , null);
	}

	/**
	 * This method insert the word assessment play data into local database
	 * @param mathAssessmentTestResult
	 */
	public void insertIntoWordAssessmentTestResult(MathAssessmentTestResultObj mathAssessmentTestResult){

		ContentValues contentValues = new ContentValues();
		contentValues.put("user_id", mathAssessmentTestResult.getUserId());
		contentValues.put("player_id", mathAssessmentTestResult.getPlayerId());
		contentValues.put("grade", mathAssessmentTestResult.getGrade());
		contentValues.put("problems", mathAssessmentTestResult.getProblems());
		contentValues.put("isCompleted", mathAssessmentTestResult.getIsCompleted());
		contentValues.put("date", mathAssessmentTestResult.getDate());
		contentValues.put("questionsRemainToPlay", mathAssessmentTestResult.getQuestionRemaining());

		dbConn.insert("WordAssessmentTestResult", null, contentValues);
	}

	/**
	 * This method delete the previous record
	 * @param mathAssessmentTestResult
	 */
	public void deleteFromWordAssessmentTestResult(MathAssessmentTestResultObj mathAssessmentTestResult){
		String where = "user_id = '"+ mathAssessmentTestResult.getUserId()
				+ "' and player_id = '" + mathAssessmentTestResult.getPlayerId() 
				+ "' and grade = '" + mathAssessmentTestResult.getGrade() + "'";
		dbConn.delete("WordAssessmentTestResult" , where , null);
	}

	/**
	 * This method delete the previous record
	 * @param mathAssessmentTestResult
	 */
	public void deleteFromNewWordAssessmentTestResult(MathAssessmentTestResultObj mathAssessmentTestResult){
		String where = "user_id = '"+ mathAssessmentTestResult.getUserId()
				+ "' and player_id = '" + mathAssessmentTestResult.getPlayerId() 
				+ "' and grade = '" + mathAssessmentTestResult.getGrade() + "'";
		dbConn.delete("NewWordAssessmentTestResult" , where , null);
	}

	/**
	 * This method insert the word assessment play data into local database
	 * @param mathAssessmentTestResult
	 */
	public void insertIntoNewWordAssessmentTestResult(MathAssessmentTestResultObj mathAssessmentTestResult){

		ContentValues contentValues = new ContentValues();
		contentValues.put("user_id", mathAssessmentTestResult.getUserId());
		contentValues.put("player_id", mathAssessmentTestResult.getPlayerId());
		contentValues.put("grade", mathAssessmentTestResult.getGrade());
		contentValues.put("problems", mathAssessmentTestResult.getProblems());
		contentValues.put("isCompleted", mathAssessmentTestResult.getIsCompleted());
		contentValues.put("date", mathAssessmentTestResult.getDate());
		contentValues.put("questionsRemainToPlay", mathAssessmentTestResult.getQuestionRemaining());
		contentValues.put("standardId", mathAssessmentTestResult.getStandardId());
		contentValues.put("allQuestionIds", mathAssessmentTestResult.getSetSubStandardsQuestionJson());
		contentValues.put("correctScore", mathAssessmentTestResult.getCorrectScore());

		dbConn.insert("NewWordAssessmentTestResult", null, contentValues);
	}

	/**
	 * This method return the data if exist otherwise return null 
	 * @param userId
	 * @param playerId
	 * @return
	 */
	public MathAssessmentTestResultObj getAssessmentTestPlayData(String userId , String playerId){

		MathAssessmentTestResultObj mathAssessmentTestResult = null;

		String query = "select * from WordAssessmentTestResult where user_id = '" + userId 
				+ "' and player_id = '" + playerId + "'";
		Cursor cursor = dbConn.rawQuery(query, null);

		if(cursor.moveToNext()){
			mathAssessmentTestResult = new MathAssessmentTestResultObj();
			mathAssessmentTestResult.setUserId(cursor.getString(cursor.getColumnIndex("user_id")));
			mathAssessmentTestResult.setPlayerId(cursor.getString(cursor.getColumnIndex("player_id")));
			mathAssessmentTestResult.setGrade(cursor.getInt(cursor.getColumnIndex("grade")));
			mathAssessmentTestResult.setProblems(cursor.getString(cursor.getColumnIndex("problems")));
			mathAssessmentTestResult.setIsCompleted(cursor.getInt(cursor.getColumnIndex("isCompleted")));
			mathAssessmentTestResult.setDate(cursor.getString(cursor.getColumnIndex("date")));
			mathAssessmentTestResult.setQuestionRemaining(cursor.getString
					(cursor.getColumnIndex("questionsRemainToPlay")));
		}

		this.closeCursor(cursor);

		return mathAssessmentTestResult;
	}

	/**
	 * This method return the data if exist otherwise return null 
	 * @param userId
	 * @param playerId
	 * @return
	 */
	public MathAssessmentTestResultObj getNewAssessmentTestPlayData(String userId , String playerId){

		MathAssessmentTestResultObj mathAssessmentTestResult = null;

		String query = "select * from NewWordAssessmentTestResult where user_id = '" + userId 
				+ "' and player_id = '" + playerId + "'";
		Cursor cursor = dbConn.rawQuery(query, null);

		if(cursor.moveToNext()){
			mathAssessmentTestResult = new MathAssessmentTestResultObj();
			mathAssessmentTestResult.setUserId(cursor.getString(cursor.getColumnIndex("user_id")));
			mathAssessmentTestResult.setPlayerId(cursor.getString(cursor.getColumnIndex("player_id")));
			mathAssessmentTestResult.setGrade(cursor.getInt(cursor.getColumnIndex("grade")));
			mathAssessmentTestResult.setProblems(cursor.getString(cursor.getColumnIndex("problems")));
			mathAssessmentTestResult.setIsCompleted(cursor.getInt(cursor.getColumnIndex("isCompleted")));
			mathAssessmentTestResult.setDate(cursor.getString(cursor.getColumnIndex("date")));
			mathAssessmentTestResult.setQuestionRemaining(cursor.getString
					(cursor.getColumnIndex("questionsRemainToPlay")));
			mathAssessmentTestResult.setStandardId(cursor.getInt(cursor.getColumnIndex("standardId")));
			mathAssessmentTestResult.setSetSubStandardsQuestionJson
			(cursor.getString(cursor.getColumnIndex("allQuestionIds")));
			mathAssessmentTestResult.setCorrectScore(cursor.getInt(cursor.getColumnIndex("correctScore")));
		}

		this.closeCursor(cursor);

		return mathAssessmentTestResult;
	}

	//changes for New Assessment
	/**
	 * This method check equation is loaded in database or not corresponding to database
	 * @param grade
	 * @return
	 */
	public boolean isStandard(int grade){
		/*String query = "select * from WordAssessmentStandards where category_id in " +
				"(select word_category_id from " +
				" WordProblemCategories where grade_level = '" + grade + "')";*/

		if(grade > 8)
			grade = 8;

		String query  = "select * from WordAssessmentStandards where grade = '" + grade + "'";
		Cursor cursor =  dbConn.rawQuery(query, null);

		if(cursor.moveToNext()){
			this.closeCursor(cursor);
			return true;
		}
		else{
			this.closeCursor(cursor);
			return false;
		}
	}

	/**
	 * This method insert the data into the wordAssessmentStandards table
	 * @param standardDataList
	 */
	public void insertIntoWordAssessmentStandards(ArrayList<StandardsDto> standardDataList){
		for(int i = 0 ; i < standardDataList.size() ; i ++ ){
			ContentValues contentValues = new ContentValues();
			contentValues.put("id", standardDataList.get(i).getStdId());
			contentValues.put("name", standardDataList.get(i).getName());
			contentValues.put("grade", standardDataList.get(i).getGrade());
			dbConn.insert("WordAssessmentStandards", null, contentValues);
		}
	}

	/**
	 * This method insert the data into WordAssessmentSubStandards table
	 * @param subStandardList
	 */
	public void insertIntoWordAssessmentSubStandards(ArrayList<StandardsDto> standardDataList){
		for(int i = 0 ; i < standardDataList.size() ; i ++ ){
			for(int j = 0 ; j < standardDataList.get(i).getSubStandardList().size() ; j ++ ){
				ContentValues contentValues = new ContentValues();
				contentValues.put("stdId", standardDataList.get(i).getSubStandardList().get(j).getStdId());
				contentValues.put("subStdId", standardDataList.get(i).getSubStandardList().get(j).getSubStdId());
				contentValues.put("name",standardDataList.get(i).getSubStandardList().get(j).getSubStandardName());
				dbConn.insert("WordAssessmentSubStandards", null, contentValues);
			}
		}
	}

	/**
	 * This method insert the data into WordAssessmentSubStandards table
	 * @param subStandardList
	 */
	public void insertIntoWordAssessmentSubCategoriesInfo(ArrayList<CatagoriesDto> categoriesList){
		for(int i = 0 ; i < categoriesList.size() ; i ++ ){
			ContentValues contentValues = new ContentValues();
			contentValues.put("standard_id", categoriesList.get(i).getStdId());
			contentValues.put("sub_standard_id", categoriesList.get(i).getSubStdId());
			contentValues.put("word_categ_id",categoriesList.get(i).getCatId());
			contentValues.put("sub_categories",categoriesList.get(i).getSubCatId());
			dbConn.insert("WordAssessmentSubCategoriesInfo", null, contentValues);
		}
	}

	/**
	 * This method delete all the records from the WordAssessmentStandards table
	 */
	public void deleteFromWordAssessmentStandards(){
		dbConn.delete("WordAssessmentStandards" , null , null);
	}

	/**
	 * This method delete all the records from the WordAssessmentSubStandards table
	 */
	public void deleteFromWordAssessmentSubStandards(){
		dbConn.delete("WordAssessmentSubStandards" , null , null);
	}

	/**
	 * This method delete all the records from the WordAssessmentSubStandards table
	 */
	public void deleteFromWordAssessmentSubCategoriesInfo(){
		dbConn.delete("WordAssessmentSubCategoriesInfo" , null , null);
	}

	/**
	 * This method get standards from database by grade
	 * @param grade
	 * @return
	 */
	public ArrayList<StandardsDto> getStandardsByGrade(int grade){

		if(grade > 8)
			grade = 8;
		ArrayList<StandardsDto> standardsList = new ArrayList<StandardsDto>();
		String query  = "select * from WordAssessmentStandards where grade = '" + grade + "'";
		Cursor cursor =  dbConn.rawQuery(query, null);

		while(cursor.moveToNext()){
			StandardsDto standardObj = new StandardsDto();
			standardObj.setStdId(cursor.getInt(cursor.getColumnIndex("id")));
			standardObj.setName(cursor.getString(cursor.getColumnIndex("name")));
			standardObj.setGrade(cursor.getInt(cursor.getColumnIndex("grade")));
			standardsList.add(standardObj);
		}

		this.closeCursor(cursor);
		return standardsList;
	}

	/**
	 * This method return the std name by id
	 * @param stdId
	 * @return
	 */
	public String getStandardNameById(int stdId){
		String standardName = "";
		String query  = "select name from WordAssessmentStandards where id = '" + stdId + "'";
		Cursor cursor =  dbConn.rawQuery(query, null);
		if(cursor.moveToNext())
			standardName = cursor.getString(0);
		this.closeCursor(cursor);
		return standardName;
	}
	
	/**
	 * This method return the categories info by standard id
	 * @param standardId
	 * @return
	 */
	public ArrayList<CatagoriesDto> getSubCategoriesInfoByStandardId(int standardId){
		ArrayList<CatagoriesDto> categoryInfoList = new ArrayList<CatagoriesDto>();

		String query  = "select * from WordAssessmentSubCategoriesInfo where standard_id = '" + standardId + "'";
		Cursor cursor = dbConn.rawQuery(query, null);
		while(cursor.moveToNext()){
			CatagoriesDto categoryInfo = new CatagoriesDto();
			categoryInfo.setStdId(cursor.getInt(cursor.getColumnIndex("standard_id")));
			categoryInfo.setSubStdId(cursor.getInt(cursor.getColumnIndex("sub_standard_id")));
			categoryInfo.setCatId(cursor.getInt(cursor.getColumnIndex("word_categ_id")));
			categoryInfo.setSubCatId(cursor.getString(cursor.getColumnIndex("sub_categories")));
			categoryInfoList.add(categoryInfo);
		}
		this.closeCursor(cursor);
		return categoryInfoList;
	}

	/**
	 * This method return the categories info by standard id
	 * @param standardId
	 * @return
	 */
	public ArrayList<CatagoriesDto> getSubCategoriesInfoByStandardIdAndSubStdId(int standardId
			, int subStdId){
		ArrayList<CatagoriesDto> categoryInfoList = new ArrayList<CatagoriesDto>();

		String query  = "select * from WordAssessmentSubCategoriesInfo where standard_id = '" + standardId + "'" +
				"and sub_standard_id = '" + subStdId + "'";
		Cursor cursor = dbConn.rawQuery(query, null);
		while(cursor.moveToNext()){
			CatagoriesDto categoryInfo = new CatagoriesDto();
			categoryInfo.setStdId(cursor.getInt(cursor.getColumnIndex("standard_id")));
			categoryInfo.setSubStdId(cursor.getInt(cursor.getColumnIndex("sub_standard_id")));
			categoryInfo.setCatId(cursor.getInt(cursor.getColumnIndex("word_categ_id")));
			categoryInfo.setSubCatId(cursor.getString(cursor.getColumnIndex("sub_categories")));
			categoryInfoList.add(categoryInfo);
		}
		this.closeCursor(cursor);
		return categoryInfoList;
	}
	
	/**
	 * This method insert the data into WordAssessmentStandardsRoundInfo table
	 * @param subStandardList
	 */
	public void insertIntoWordAssessmentStandardsRoundInfo(ArrayList<StandardRoundsDto> standardRoundList
			, String userId , String playerId){
		for(int i = 0 ; i < standardRoundList.size() ; i ++ ){
			ContentValues contentValues = new ContentValues();
			contentValues.put("userId", userId);
			contentValues.put("playerId", playerId);
			contentValues.put("standardId",standardRoundList.get(i).getStdId());
			contentValues.put("round",standardRoundList.get(i).getRounds());
			contentValues.put("last_played_date",standardRoundList.get(i).getLastDate());
			dbConn.insert("WordAssessmentStandardsRoundInfo", null, contentValues);
		}
	}

	/**
	 * This method delete the previous record
	 * @param String userId , String playerId
	 */
	public void deleteFromWordAssessmentStandardsRoundInfo(String userId , String playerId){
		String where = "userId = '"+ userId
				+ "' and playerId = '" + playerId + "'";
		dbConn.delete("WordAssessmentStandardsRoundInfo" , where , null);
	}

	/**
	 * This method number of rounds which are played by player
	 * @param userId
	 * @param playerId
	 * @param stdId
	 * @return
	 */
	public int getRounds(String userId , String playerId , int stdId){
		int rounds = 1;
		String query  = "select round from WordAssessmentStandardsRoundInfo where standardId = '" + stdId + "'" +
				" and userId = '" + userId + "' and playerId = '" + playerId + "'";
		Cursor cursor = dbConn.rawQuery(query, null);
		if(cursor.moveToNext()){
			rounds = cursor.getInt(0);
		}
		this.closeCursor(cursor);
		return rounds;
	}

	/**
	 * This method number of rounds which are played by player
	 * @param userId
	 * @param playerId
	 * @param stdId
	 * @return
	 */
	public String getLastPlayDate(String userId , String playerId , int stdId){
		String lastPlayDate = "";
		String query  = "select last_played_date from WordAssessmentStandardsRoundInfo where standardId = '" + stdId + "'" +
				" and userId = '" + userId + "' and playerId = '" + playerId + "'";
		Cursor cursor = dbConn.rawQuery(query, null);
		if(cursor.moveToNext()){
			lastPlayDate = cursor.getString(0);
		}
		this.closeCursor(cursor);
		return lastPlayDate;
	}
	
	/**
	 * This method return the sub standard name
	 * @param stdId
	 * @param subStdId
	 * @return
	 */
	public String getSubStandardName(int stdId , int subStdId){
		String subStandardName = "";
		String query  = "select name from WordAssessmentSubStandards where stdId = '" + stdId + "'" +
				 " and subStdId = '" + subStdId + "'";
		Cursor cursor = dbConn.rawQuery(query, null);
		if(cursor.moveToNext()){
			subStandardName = cursor.getString(0);
		}
		this.closeCursor(cursor);
		return subStandardName;
	}
	
	/**
	 * This method return the sub standard List by std id
	 * @param stdId
	 * @return
	 */
	public ArrayList<SubStandardsDto> getSubStandardListByStdId(int stdId){
		ArrayList<SubStandardsDto> subStdList = new ArrayList<SubStandardsDto>();
		String query  = "select * from WordAssessmentSubStandards where stdId = '" + stdId + "'";
		Cursor cursor = dbConn.rawQuery(query, null);
		while(cursor.moveToNext()){
			SubStandardsDto subStdObj = new SubStandardsDto();
			subStdObj.setSubStdId(cursor.getInt(cursor.getColumnIndex("subStdId")));
			subStdObj.setSubStandardName(cursor.getString(cursor.getColumnIndex("name")));
			subStdList.add(subStdObj);
		}
		this.closeCursor(cursor);
		return subStdList;
	}
}
