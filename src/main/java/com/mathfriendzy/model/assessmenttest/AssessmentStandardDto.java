package com.mathfriendzy.model.assessmenttest;

import java.util.ArrayList;

public class AssessmentStandardDto {
	
	private ArrayList<StandardsDto> standardDataList;
	private ArrayList<CatagoriesDto> categoriesList;
	
	public ArrayList<StandardsDto> getStandardDataList() {
		return standardDataList;
	}
	public void setStandardDataList(ArrayList<StandardsDto> standardDataList) {
		this.standardDataList = standardDataList;
	}
	public ArrayList<CatagoriesDto> getCategoriesList() {
		return categoriesList;
	}
	public void setCategoriesList(ArrayList<CatagoriesDto> categoriesList) {
		this.categoriesList = categoriesList;
	}
}
