package com.mathfriendzy.model.assessmenttest;

import java.io.Serializable;
import java.util.ArrayList;

public class DetailedScoreDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ArrayList<StudentsScore> studentScoreList;
	private ArrayList<CCSSScore> ccssScoreList;
	
	public ArrayList<StudentsScore> getStudentScoreList() {
		return studentScoreList;
	}
	public void setStudentScoreList(ArrayList<StudentsScore> studentScoreList) {
		this.studentScoreList = studentScoreList;
	}
	public ArrayList<CCSSScore> getCcssScoreList() {
		return ccssScoreList;
	}
	public void setCcssScoreList(ArrayList<CCSSScore> ccssScoreList) {
		this.ccssScoreList = ccssScoreList;
	}
	
}
