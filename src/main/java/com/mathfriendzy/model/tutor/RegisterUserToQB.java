package com.mathfriendzy.model.tutor;

/**
 * Created by root on 24/3/15.
 */
public class RegisterUserToQB {

    private String login;//chat user name
    private String pass;//chat user name
    private String fullUserName;

    public String getLogin() {
        return login;
    }

    public String getPass() {
        return pass;
    }

    public String getFullUserName() {
        return fullUserName;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public void setFullUserName(String fullUserName) {
        this.fullUserName = fullUserName;
    }
}
