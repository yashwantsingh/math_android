package com.mathfriendzy.model.tutor;

/**
 * Created by root on 11/5/15.
 */
public class TutorTabIconVisibility {
    public boolean isShowDisconnectIcon;
    public boolean isShowHandIcon;
    public boolean isShowOnlineIcon;
}
