package com.mathfriendzy.model.tutor;

import com.mathfriendzy.serveroperation.HttpResponseBase;

/**
 * Created by root on 29/6/15.
 */
public class GetActiveTutorResponse extends HttpResponseBase{
    private String result;
    private String message;
    private int tutors;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getTutors() {
        return tutors;
    }

    public void setTutors(int tutors) {
        this.tutors = tutors;
    }
}
