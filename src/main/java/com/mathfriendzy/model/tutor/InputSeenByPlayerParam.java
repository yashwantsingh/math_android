package com.mathfriendzy.model.tutor;

/**
 * Created by root on 12/5/15.
 */
public class InputSeenByPlayerParam {

    private String action;
    private int charRequestId;
    private int isStudent;

    public int getCharRequestId() {
        return charRequestId;
    }

    public void setCharRequestId(int charRequestId) {
        this.charRequestId = charRequestId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getIsStudent() {
        return isStudent;
    }

    public void setIsStudent(int isStudent) {
        this.isStudent = isStudent;
    }
}
