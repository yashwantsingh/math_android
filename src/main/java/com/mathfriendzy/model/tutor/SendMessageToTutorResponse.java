package com.mathfriendzy.model.tutor;

import com.mathfriendzy.serveroperation.HttpResponseBase;

/**
 * Created by root on 26/3/15.
 */
public class SendMessageToTutorResponse extends HttpResponseBase {

    private String result;
    private int requestId;

    public String getResult() {
        return result;
    }

    public int getRequestId() {
        return requestId;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }
}
