package com.mathfriendzy.model.tutor;

import com.mathfriendzy.serveroperation.HttpResponseBase;

import java.io.Serializable;

public class FindTutorResponse extends HttpResponseBase implements Serializable{
	
	private boolean isTutorFind = false;//not in response, added to check tutor found or not
	
	private String result;
	private String fName;
	private String lname;
	private String schoolId;
	private String schoolName;
	private String grade;
	private String teacherUserId;
	private String teacherFirstName;
	private String teacherLastName;
	private String indexOfAppereance;
	private String parentUserId;
	private String playerId;
	private String completeLevel;
	private String coins;
	private String points;
	private String city;
	private String state;
	private String userName;
	private int isTutor;
	private String chatId;
	private String chatUserName;
	private String deviceIds;
	private String androidDeviceIds;
	
	
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getTeacherUserId() {
		return teacherUserId;
	}
	public void setTeacherUserId(String teacherUserId) {
		this.teacherUserId = teacherUserId;
	}
	public String getTeacherFirstName() {
		return teacherFirstName;
	}
	public void setTeacherFirstName(String teacherFirstName) {
		this.teacherFirstName = teacherFirstName;
	}
	public String getTeacherLastName() {
		return teacherLastName;
	}
	public void setTeacherLastName(String teacherLastName) {
		this.teacherLastName = teacherLastName;
	}
	public String getIndexOfAppereance() {
		return indexOfAppereance;
	}
	public void setIndexOfAppereance(String indexOfAppereance) {
		this.indexOfAppereance = indexOfAppereance;
	}
	public String getParentUserId() {
		return parentUserId;
	}
	public void setParentUserId(String parentUserId) {
		this.parentUserId = parentUserId;
	}
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public String getCompleteLevel() {
		return completeLevel;
	}
	public void setCompleteLevel(String completeLevel) {
		this.completeLevel = completeLevel;
	}
	public String getCoins() {
		return coins;
	}
	public void setCoins(String coins) {
		this.coins = coins;
	}
	public String getPoints() {
		return points;
	}
	public void setPoints(String points) {
		this.points = points;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getIsTutor() {
		return isTutor;
	}
	public void setIsTutor(int isTutor) {
		this.isTutor = isTutor;
	}
	public String getChatId() {
		return chatId;
	}
	public void setChatId(String chatId) {
		this.chatId = chatId;
	}
	public String getChatUserName() {
		return chatUserName;
	}
	public void setChatUserName(String chatUserName) {
		this.chatUserName = chatUserName;
	}
	public String getDeviceIds() {
		return deviceIds;
	}
	public void setDeviceIds(String deviceIds) {
		this.deviceIds = deviceIds;
	}
	public String getAndroidDeviceIds() {
		return androidDeviceIds;
	}
	public void setAndroidDeviceIds(String androidDeviceIds) {
		this.androidDeviceIds = androidDeviceIds;
	}
	public boolean getIsTutorFind() {
		return isTutorFind;
	}
	public void setIsTutorFind(boolean isTutorFind) {
		this.isTutorFind = isTutorFind;
	}
}
