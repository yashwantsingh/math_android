package com.mathfriendzy.model.tutor;

/**
 * Created by root on 6/5/15.
 */
public class StopWaitingForTutorParam {

    private String action;
    private int requestId;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }
}
