package com.mathfriendzy.model.tutor;

/**
 * Created by root on 8/5/15.
 */
public class EditTutorAreaParam {
    private String action;
    private int reqId;
    private int forSubject;
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getReqId() {
        return reqId;
    }

    public void setReqId(int reqId) {
        this.reqId = reqId;
    }

    public int getForSubject() {
        return forSubject;
    }

    public void setForSubject(int forSubject) {
        this.forSubject = forSubject;
    }
}
