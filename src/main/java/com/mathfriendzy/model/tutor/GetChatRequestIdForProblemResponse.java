package com.mathfriendzy.model.tutor;

import com.mathfriendzy.serveroperation.HttpResponseBase;

/**
 * Created by root on 6/4/15.
 */
public class GetChatRequestIdForProblemResponse extends HttpResponseBase{
    private String result;
    private int chatId;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getChatId() {
        return chatId;
    }

    public void setChatId(int chatId) {
        this.chatId = chatId;
    }
}
