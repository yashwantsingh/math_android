package com.mathfriendzy.model.tutor;

/**
 * Created by root on 30/12/15.
 */
public class GetMessagesForTutorRequestParam {
    private String action;
    private int reqId;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getReqId() {
        return reqId;
    }

    public void setReqId(int reqId) {
        this.reqId = reqId;
    }
}
