package com.mathfriendzy.model.tutor;

/**
 * Created by root on 8/6/15.
 */
public class UpdateTutorAreaImageName {

    private String action;
    private String requestId;
    private String imageName;
    private String studentUserId;
    private int shouldSendPN;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getStudentUserId() {
        return studentUserId;
    }

    public void setStudentUserId(String studentUserId) {
        this.studentUserId = studentUserId;
    }

    public int getShouldSendPN() {
        return shouldSendPN;
    }

    public void setShouldSendPN(int shouldSendPN) {
        this.shouldSendPN = shouldSendPN;
    }
}
