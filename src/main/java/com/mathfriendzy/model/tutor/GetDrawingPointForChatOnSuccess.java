package com.mathfriendzy.model.tutor;

/**
 * Created by root on 14/4/15.
 */
public interface GetDrawingPointForChatOnSuccess {
    void onSuccess(GetDrawingPointForChatResponse response);
}
