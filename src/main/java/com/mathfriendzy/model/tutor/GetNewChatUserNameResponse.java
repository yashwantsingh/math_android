package com.mathfriendzy.model.tutor;

import com.mathfriendzy.serveroperation.HttpResponseBase;

/**
 * Created by root on 24/3/15.
 */
public class GetNewChatUserNameResponse extends HttpResponseBase{
    private String result;
    private String chatUserName;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getChatUserName() {
        return chatUserName;
    }

    public void setChatUserName(String chatUserName) {
        this.chatUserName = chatUserName;
    }
}
