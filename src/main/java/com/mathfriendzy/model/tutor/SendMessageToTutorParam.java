package com.mathfriendzy.model.tutor;

/**
 * Created by root on 26/3/15.
 */
public class SendMessageToTutorParam {
    private String action;
    private int subjectId;
    private String data;

    public String getAction() {
        return action;
    }

    public String getData() {
        return data;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }
}
