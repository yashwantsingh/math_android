package com.mathfriendzy.model.tutor;

import android.os.Bundle;

import com.quickblox.users.model.QBUser;

/**
 * Created by root on 24/3/15.
 */
public interface RegisterUserOnQBCallback {
    void onSuccess(final QBUser qbUser, Bundle bundle);
}
