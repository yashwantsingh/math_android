package com.mathfriendzy.model.tutor;

/**
 * Created by root on 4/5/15.
 */
public class TutorSessionDisconnectedBuTutorParam {

    private String action;
    private int chatRequestId;
    private String tutorUid;
    private String tutorPid;
    private String date;
    private String name;

    private boolean isDiconnectTutorWithoutAnswer;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getChatRequestId() {
        return chatRequestId;
    }

    public void setChatRequestId(int chatRequestId) {
        this.chatRequestId = chatRequestId;
    }

    public String getTutorUid() {
        return tutorUid;
    }

    public void setTutorUid(String tutorUid) {
        this.tutorUid = tutorUid;
    }

    public String getTutorPid() {
        return tutorPid;
    }

    public void setTutorPid(String tutorPid) {
        this.tutorPid = tutorPid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDiconnectTutorWithoutAnswer() {
        return isDiconnectTutorWithoutAnswer;
    }

    public void setDiconnectTutorWithoutAnswer(boolean isDiconnectTutorWithoutAnswer) {
        this.isDiconnectTutorWithoutAnswer = isDiconnectTutorWithoutAnswer;
    }
}
