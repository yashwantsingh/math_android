package com.mathfriendzy.model.tutor;

/**
 * Created by root on 25/3/15.
 */
public class GetChatRequestParam {
    private String action;
    private int chatRequestId;

    public String getAction() {
        return action;
    }

    public int getChatRequestId() {
        return chatRequestId;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setChatRequestId(int chatRequestId) {
        this.chatRequestId = chatRequestId;
    }
}
