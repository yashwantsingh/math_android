package com.mathfriendzy.model.tutor;

import java.util.ArrayList;

/**
 * Created by root on 4/11/15.
 */
public class PhraseCatagory {
    private String response;
    private String date;

    private int catId;
    private String catName;
    private int forTutor = 1;
    private ArrayList<PhraseSubCatagory> subCatList = null;
    private ArrayList<PhraseCatagory> catList = null;
    private ArrayList<PhraseCatagory> studentCatList = null;

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public ArrayList<PhraseSubCatagory> getSubCatList() {
        return subCatList;
    }

    public void setSubCatList(ArrayList<PhraseSubCatagory> subCatList) {
        this.subCatList = subCatList;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<PhraseCatagory> getCatList() {
        return catList;
    }

    public void setCatList(ArrayList<PhraseCatagory> catList) {
        this.catList = catList;
    }

    public ArrayList<PhraseCatagory> getStudentCatList() {
        return studentCatList;
    }

    public void setStudentCatList(ArrayList<PhraseCatagory> studentCatList) {
        this.studentCatList = studentCatList;
    }

    public int getForTutor() {
        return forTutor;
    }

    public void setForTutor(int forTutor) {
        this.forTutor = forTutor;
    }
}
