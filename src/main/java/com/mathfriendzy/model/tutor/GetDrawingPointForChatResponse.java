package com.mathfriendzy.model.tutor;

import com.mathfriendzy.serveroperation.HttpResponseBase;

import java.util.ArrayList;

/**
 * Created by root on 14/4/15.
 */
public class GetDrawingPointForChatResponse extends HttpResponseBase{
    private String result;

    private String playerId;
    private String drawPoints;
    private String deviceSize;

    private ArrayList<GetDrawingPointForChatResponse> drawingList;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getDrawPoints() {
        return drawPoints;
    }

    public void setDrawPoints(String drawPoints) {
        this.drawPoints = drawPoints;
    }

    public String getDeviceSize() {
        return deviceSize;
    }

    public void setDeviceSize(String deviceSize) {
        this.deviceSize = deviceSize;
    }

    public ArrayList<GetDrawingPointForChatResponse> getDrawingList() {
        return drawingList;
    }

    public void setDrawingList(ArrayList<GetDrawingPointForChatResponse> drawingList) {
        this.drawingList = drawingList;
    }
}
