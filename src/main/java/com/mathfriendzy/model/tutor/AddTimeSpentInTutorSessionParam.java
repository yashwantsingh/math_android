package com.mathfriendzy.model.tutor;

/**
 * Created by root on 16/4/15.
 */
public class AddTimeSpentInTutorSessionParam {

    private String action;
    private int requestId;
    private int timeSpent;
    private boolean isShowDialog;
    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public int getTimeSpent() {
        return timeSpent;
    }

    public void setTimeSpent(int timeSpent) {
        this.timeSpent = timeSpent;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public boolean isShowDialog() {
        return isShowDialog;
    }

    public void setShowDialog(boolean isShowDialog) {
        this.isShowDialog = isShowDialog;
    }
}
