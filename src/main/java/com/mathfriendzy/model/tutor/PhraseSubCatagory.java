package com.mathfriendzy.model.tutor;

/**
 * Created by root on 4/11/15.
 */
public class PhraseSubCatagory {
    private int subCatId;
    private String subCatName;
    private int isSelectable;
    private int forFreeTutor;

    public int getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(int subCatId) {
        this.subCatId = subCatId;
    }

    public String getSubCatName() {
        return subCatName;
    }

    public void setSubCatName(String subCatName) {
        this.subCatName = subCatName;
    }

    public int getIsSelectable() {
        return isSelectable;
    }

    public void setIsSelectable(int isSelectable) {
        this.isSelectable = isSelectable;
    }

    public int getForFreeTutor() {
        return forFreeTutor;
    }

    public void setForFreeTutor(int forFreeTutor) {
        this.forFreeTutor = forFreeTutor;
    }
}
