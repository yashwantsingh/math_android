package com.mathfriendzy.model.tutor;

import com.mathfriendzy.serveroperation.HttpResponseBase;

import java.util.ArrayList;

/**
 * Created by root on 29/12/15.
 */
public class ChatMessageModel extends HttpResponseBase{
    private String senderId;
    private String message;
    private String name = "(null)";
    private int isTutor;

    private ArrayList<ChatMessageModel> messageList;
    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIsTutor() {
        return isTutor;
    }

    public void setIsTutor(int isTutor) {
        this.isTutor = isTutor;
    }

    public ArrayList<ChatMessageModel> getMessageList() {
        return messageList;
    }

    public void setMessageList(ArrayList<ChatMessageModel> messageList) {
        this.messageList = messageList;
    }

}
