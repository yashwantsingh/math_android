package com.mathfriendzy.model.tutor;

/**
 * Created by root on 14/4/15.
 */
public class GetDrawingPointForChatParam {

    private String action;
    private int requestId;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }
}
