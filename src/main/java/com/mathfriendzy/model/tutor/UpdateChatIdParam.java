package com.mathfriendzy.model.tutor;

/**
 * Created by root on 24/3/15.
 */
public class UpdateChatIdParam {

    private String action;
    private String userId;
    private String playerId;
    private String chatId;
    private String chatUserName;


    public String getAction() {
        return action;
    }

    public String getUserId() {
        return userId;
    }

    public String getPlayerId() {
        return playerId;
    }

    public String getChatId() {
        return chatId;
    }

    public String getChatUserName() {
        return chatUserName;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public void setChatUserName(String chatUserName) {
        this.chatUserName = chatUserName;
    }
}
