package com.mathfriendzy.model.tutor;

/**
 * Created by root on 6/4/15.
 */
public class GetChatRequestIdParam {
    private String action;
    private String hwId;
    private String customeHwId;
    private String userId;
    private String playerId;
    private String question;


    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getHwId() {
        return hwId;
    }

    public void setHwId(String hwId) {
        this.hwId = hwId;
    }

    public String getCustomeHwId() {
        return customeHwId;
    }

    public void setCustomeHwId(String customeHwId) {
        this.customeHwId = customeHwId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }
}
