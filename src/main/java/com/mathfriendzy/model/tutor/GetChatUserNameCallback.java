package com.mathfriendzy.model.tutor;

/**
 * Created by root on 24/3/15.
 */
public interface GetChatUserNameCallback {
    void onSuccess(String chatUserName);
}
