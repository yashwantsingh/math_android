package com.mathfriendzy.model.tutor;

/**
 * Created by root on 14/4/15.
 */
public class UpdateDrawPointsForPlayerParam {
    private String action;
    private int requestId;
    private String playerId;
    private String drawPoints;
    private String deviceSize;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getDrawPoints() {
        return drawPoints;
    }

    public void setDrawPoints(String drawPoints) {
        this.drawPoints = drawPoints;
    }

    public String getDeviceSize() {
        return deviceSize;
    }

    public void setDeviceSize(String deviceSize) {
        this.deviceSize = deviceSize;
    }
}
