package com.mathfriendzy.model.tutor;

import com.mathfriendzy.serveroperation.HttpResponseBase;

/**
 * Created by root on 25/3/15.
 */
public class GetChatRequestResponse extends HttpResponseBase{

    private String result;
    private int requestId;
    private String userId;
    private String playerId;
    private String reqDate;
    private String message;
    private int isConnected;
    private int tuturUid;
    private int tutorPid;
    private String chatDialogId;
    private int isAnonymous;
    private int rating;
    private String timeSpent;
    private String tutorName;
    private int disconnected;

    //new param
    private String studentImage;
    private String tutorImage;
    private String workAreaImage;
    private int lastUpdateByTutor;
    private int opponentInput;


    public String getResult() {
        return result;
    }

    public int getRequestId() {
        return requestId;
    }

    public String getUserId() {
        return userId;
    }

    public String getPlayerId() {
        return playerId;
    }

    public String getReqDate() {
        return reqDate;
    }

    public int getIsConnected() {
        return isConnected;
    }

    public String getMessage() {
        return message;
    }

    public int getTuturUid() {
        return tuturUid;
    }

    public int getTutorPid() {
        return tutorPid;
    }

    public String getChatDialogId() {
        return chatDialogId;
    }

    public int getIsAnonymous() {
        return isAnonymous;
    }

    public int getRating() {
        return rating;
    }

    public String getTimeSpent() {
        return timeSpent;
    }

    public String getTutorName() {
        return tutorName;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public void setReqDate(String reqDate) {
        this.reqDate = reqDate;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setIsConnected(int isConnected) {
        this.isConnected = isConnected;
    }

    public void setTuturUid(int tuturUid) {
        this.tuturUid = tuturUid;
    }

    public void setTutorPid(int tutorPid) {
        this.tutorPid = tutorPid;
    }

    public void setChatDialogId(String chatDialogId) {
        this.chatDialogId = chatDialogId;
    }

    public void setIsAnonymous(int isAnonymous) {
        this.isAnonymous = isAnonymous;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public void setTimeSpent(String timeSpent) {
        this.timeSpent = timeSpent;
    }

    public void setTutorName(String tutorName) {
        this.tutorName = tutorName;
    }

    public int getDisconnected() {
        return disconnected;
    }

    public void setDisconnected(int disconnected) {
        this.disconnected = disconnected;
    }

    public String getStudentImage() {
        return studentImage;
    }

    public void setStudentImage(String studentImage) {
        this.studentImage = studentImage;
    }

    public String getTutorImage() {
        return tutorImage;
    }

    public void setTutorImage(String tutorImage) {
        this.tutorImage = tutorImage;
    }

    public String getWorkAreaImage() {
        return workAreaImage;
    }

    public void setWorkAreaImage(String workAreaImage) {
        this.workAreaImage = workAreaImage;
    }

    public int getLastUpdateByTutor() {
        return lastUpdateByTutor;
    }

    public void setLastUpdateByTutor(int lastUpdateByTutor) {
        this.lastUpdateByTutor = lastUpdateByTutor;
    }

    public int getOpponentInput() {
        return opponentInput;
    }

    public void setOpponentInput(int opponentInput) {
        this.opponentInput = opponentInput;
    }
}
