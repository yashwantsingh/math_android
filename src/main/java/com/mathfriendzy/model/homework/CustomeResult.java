package com.mathfriendzy.model.homework;

import com.mathfriendzy.controller.homework.homwworkworkarea.AddUrlToWorkArea;
import com.mathfriendzy.model.resource.ResourceParam;
import com.mathfriendzy.model.resource.ResourceResponse;
import com.mathfriendzy.model.resource.SearchResourceResponse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class CustomeResult implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int customHwId;
	private String title;
	private int problem;
	private int allowChanges;
	private int showAns;
	private String score;
	
	private boolean isAlreadyPlayed;
	
	private ArrayList<CustomeAns> customeAnsList;
	private ArrayList<CustomePlayerAns> customePlayerAnsList;
	
	//for assignment sheet
	private String sheetName = "";
	
	//for layout visibility in assign homework adapter
	private boolean isLayoutVisible = false;
	
	
	//for creating pdf
	private ArrayList<String> pdfImagesList = null;
	private int clickedPosition = 0;

    private int numberOfActiveInput = 0;

    //for add resources
    private ArrayList<ResourceResponse> selectedResourceList = null;
    private String resourceAdded = "";
    private ResourceParam searchParam = null;
    private HashMap<String , SearchResourceResponse> dataList = null;
    private HashMap<String,ArrayList<ResourceResponse>> selectedListResources = null;
    //end add resources

    //for new assign homework change
    private boolean isNewHomework = false;

    //for question added
    private boolean isQuestionAdded = false;

    //for Add Links
    private ArrayList<AddUrlToWorkArea> links = null;

	public boolean isLayoutVisible() {
		return isLayoutVisible;
	}
	public void setLayoutVisible(boolean isLayoutVisible) {
		this.isLayoutVisible = isLayoutVisible;
	}
	public int getCustomHwId() {
		return customHwId;
	}
	public void setCustomHwId(int customHwId) {
		this.customHwId = customHwId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getProblem() {
		return problem;
	}
	public void setProblem(int problem) {
		this.problem = problem;
	}
	public int getAllowChanges() {
		return allowChanges;
	}
	public void setAllowChanges(int allowChanges) {
		this.allowChanges = allowChanges;
	}
	public int getShowAns() {
		return showAns;
	}
	public void setShowAns(int showAns) {
		this.showAns = showAns;
	}
	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}
	public ArrayList<CustomeAns> getCustomeAnsList() {
		return customeAnsList;
	}
	public void setCustomeAnsList(ArrayList<CustomeAns> customeAnsList) {
		this.customeAnsList = customeAnsList;
	}
	public ArrayList<CustomePlayerAns> getCustomePlayerAnsList() {
		return customePlayerAnsList;
	}
	public void setCustomePlayerAnsList(
			ArrayList<CustomePlayerAns> customePlayerAnsList) {
		this.customePlayerAnsList = customePlayerAnsList;
	}
	public boolean isAlreadyPlayed() {
		return isAlreadyPlayed;
	}
	public void setAlreadyPlayed(boolean isAlreadyPlayed) {
		this.isAlreadyPlayed = isAlreadyPlayed;
	}
	public String getSheetName() {
		return sheetName;
	}
	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}
	public ArrayList<String> getPdfImagesList() {
		return pdfImagesList;
	}
	public void setPdfImagesList(ArrayList<String> pdfImagesList) {
		this.pdfImagesList = pdfImagesList;
	}
	public int getClickedPosition() {
		return clickedPosition;
	}
	public void setClickedPosition(int clickedPosition) {
		this.clickedPosition = clickedPosition;
	}

    public int getNumberOfActiveInput() {
        return numberOfActiveInput;
    }

    public void setNumberOfActiveInput(int numberOfActiveInput) {
        this.numberOfActiveInput = numberOfActiveInput;
    }

    public ArrayList<ResourceResponse> getSelectedResourceList() {
        return selectedResourceList;
    }

    public void setSelectedResourceList(ArrayList<ResourceResponse> selectedResourceList) {
        this.selectedResourceList = selectedResourceList;
    }

    public String getResourceAdded() {
        return resourceAdded;
    }

    public void setResourceAdded(String resourceAdded) {
        this.resourceAdded = resourceAdded;
    }

    public boolean isNewHomework() {
        return isNewHomework;
    }

    public void setNewHomework(boolean isNewHomework) {
        this.isNewHomework = isNewHomework;
    }

    public ResourceParam getSearchParam() {
        return searchParam;
    }

    public void setSearchParam(ResourceParam searchParam) {
        this.searchParam = searchParam;
    }

    public HashMap<String, SearchResourceResponse> getDataList() {
        return dataList;
    }

    public void setDataList(HashMap<String, SearchResourceResponse> dataList) {
        this.dataList = dataList;
    }

    public HashMap<String, ArrayList<ResourceResponse>> getSelectedListResources() {
        return selectedListResources;
    }

    public void setSelectedListResources(HashMap<String, ArrayList<ResourceResponse>> selectedListResources) {
        this.selectedListResources = selectedListResources;
    }

    public boolean isQuestionAdded() {
        return isQuestionAdded;
    }

    public void setQuestionAdded(boolean isQuestionAdded) {
        this.isQuestionAdded = isQuestionAdded;
    }

    public ArrayList<AddUrlToWorkArea> getLinks() {
        return links;
    }

    public void setLinks(ArrayList<AddUrlToWorkArea> links) {
        this.links = links;
    }
}
