package com.mathfriendzy.model.homework;

import android.widget.TextView;

import java.util.ArrayList;

public class MultipleChoiceAnswer extends StudentAnsBase{
	
	
	private ArrayList<TextView> optionList;
			
	public ArrayList<TextView> getOptionList() {
		return optionList;
	}
	public void setOptionList(ArrayList<TextView> optionList) {
		this.optionList = optionList;
	}
}
