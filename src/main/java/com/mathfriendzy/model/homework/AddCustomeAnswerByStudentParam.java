package com.mathfriendzy.model.homework;

public class AddCustomeAnswerByStudentParam {
	
	private String action;
	private String userId;
	private String playerId;
	private String HWId;
	private String CustomHWId;
	private int numberOfQuestions;
	private int score;
	private String questionJson;
	
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public String getHWId() {
		return HWId;
	}
	public void setHWId(String hWId) {
		HWId = hWId;
	}
	public String getCustomHWId() {
		return CustomHWId;
	}
	public void setCustomHWId(String customHWId) {
		CustomHWId = customHWId;
	}
	public int getNumberOfQuestions() {
		return numberOfQuestions;
	}
	public void setNumberOfQuestions(int numberOfQuestions) {
		this.numberOfQuestions = numberOfQuestions;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public String getQuestionJson() {
		return questionJson;
	}
	public void setQuestionJson(String questionJson) {
		this.questionJson = questionJson;
	}
}
