package com.mathfriendzy.model.homework;

import java.io.Serializable;

public class PracticeResultSubCat implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int subCatId;
	private int problems;
	private String time;
	private String score;
	
	//at the setting listener on the subCat layout on the HomeWork quizz
	private String catId;
	
	public int getSubCatId() {
		return subCatId;
	}
	public void setSubCatId(int subCatId) {
		this.subCatId = subCatId;
	}
	public int getProblems() {
		return problems;
	}
	public void setProblems(int problems) {
		this.problems = problems;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}
	public String getCatId() {
		return catId;
	}
	public void setCatId(String catId) {
		this.catId = catId;
	}
}
