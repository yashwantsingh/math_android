package com.mathfriendzy.model.homework;

import java.io.Serializable;

public class GetHomeworksForStudentWithCustomParam implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String userId;
	private String playerId;
	private int offset;
	private String action;
	private String grade;

    private int limit;
    private String startDate;
    private String endDate;
    private String classId;

    private int forTeacher;

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public String getAction() {
		return action;
	}
    public void setAction(String action) {
        this.action = action;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public int getForTeacher() {
        return forTeacher;
    }

    public void setForTeacher(int forTeacher) {
        this.forTeacher = forTeacher;
    }
}
