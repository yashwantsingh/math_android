package com.mathfriendzy.model.homework;

import java.io.Serializable;
import java.util.ArrayList;

import com.mathfriendzy.serveroperation.HttpResponseBase;

public class GetHelpStudentListResponse extends HttpResponseBase implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String userId;
	private String playerId;
	private String workImageName;
	private String fName;
	private String lName;
	private int rating;
	private String studentTeacherMsg;
	private int yourRating;
	private String questionImage;
	
	public int getYourRating() {
		return yourRating;
	}
	public void setYourRating(int yourRating) {
		this.yourRating = yourRating;
	}
	private ArrayList<GetHelpStudentListResponse> studentList;
	
	public ArrayList<GetHelpStudentListResponse> getStudentList() {
		return studentList;
	}
	public void setStudentList(ArrayList<GetHelpStudentListResponse> studentList) {
		this.studentList = studentList;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public String getWorkImageName() {
		return workImageName;
	}
	public void setWorkImageName(String workImageName) {
		this.workImageName = workImageName;
	}
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	public String getlName() {
		return lName;
	}
	public void setlName(String lName) {
		this.lName = lName;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public String getStudentTeacherMsg() {
		return studentTeacherMsg;
	}
	public void setStudentTeacherMsg(String studentTeacherMsg) {
		this.studentTeacherMsg = studentTeacherMsg;
	}
	public String getQuestionImage() {
		return questionImage;
	}
	public void setQuestionImage(String questionImage) {
		this.questionImage = questionImage;
	}
}
