package com.mathfriendzy.model.homework;

import java.io.Serializable;
import java.util.ArrayList;

public class PracticeResult implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String catId;
	private int intCatId;
	
	private ArrayList<PracticeResultSubCat> practiceResultSubCatList;
	
	public String getCatId() {
		return catId;
	}
	public void setCatId(String catId) {
		this.catId = catId;
	}
	
	public int getIntCatId() {
		return intCatId;
	}
	public void setIntCatId(int intCatId) {
		this.intCatId = intCatId;
	}
	public ArrayList<PracticeResultSubCat> getPracticeResultSubCatList() {
		return practiceResultSubCatList;
	}
	public void setPracticeResultSubCatList(
			ArrayList<PracticeResultSubCat> practiceResultSubCatList) {
		this.practiceResultSubCatList = practiceResultSubCatList;
	}
}
