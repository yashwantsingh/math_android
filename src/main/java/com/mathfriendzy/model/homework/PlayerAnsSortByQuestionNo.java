package com.mathfriendzy.model.homework;

import java.util.Comparator;

public class PlayerAnsSortByQuestionNo implements Comparator<CustomePlayerAns>{

	@Override
	public int compare(CustomePlayerAns lhs, CustomePlayerAns rhs) {
		Integer que1 = Integer.parseInt(lhs.getQueNo());
		Integer que2 = Integer.parseInt(rhs.getQueNo());
		return (int) Math.signum(que1.compareTo(que2));
	}

}
