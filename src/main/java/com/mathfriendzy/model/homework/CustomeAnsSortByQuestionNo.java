package com.mathfriendzy.model.homework;

import java.util.Comparator;

public class CustomeAnsSortByQuestionNo implements Comparator<CustomeAns>{
	@Override
	public int compare(CustomeAns lhs, CustomeAns rhs) {
		Integer que1 = Integer.parseInt(lhs.getQueNo());
		Integer que2 = Integer.parseInt(rhs.getQueNo());
		return (int) Math.signum(que1.compareTo(que2));
	}
}
