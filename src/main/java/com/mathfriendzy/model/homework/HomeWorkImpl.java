package com.mathfriendzy.model.homework;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.mathfriendzy.database.Database;
import com.mathfriendzy.helper.MathFriendzyHelper;

import java.util.ArrayList;


public class HomeWorkImpl {

	private SQLiteDatabase dbConn = null;
	private Context context 	  = null;

	private final String HOMEWORK_TABLE = "Homework";

	private final String PRACTICE_SKILL_HW_TABLE = "PracticeSkillHW";
	private final String WORD_PROBLEM_HW_TABLE   = "WordProblemHW";
	private final String CUSTOME_HW_TABLE        = "CustomeHW";
    private final String USER_LINK_TABLE = "UserLinks";

	public HomeWorkImpl(Context context){
		this.context = context;
	}

	/**
	 * Open the database connection
	 */
	public void openConnection(){
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
	}

	/**
	 * Close the database connection
	 */
	public void closeConnection(){
		dbConn.close();
	}

	/**
	 * Insert the image into the home work image table
	 * @param imageName
	 */
	public void insertIntoHomeWorkImageTable(String imageName){
		try{
			ContentValues contentValues = new ContentValues();
			contentValues.put("imagename", imageName);
			dbConn.insert("homeworkimage", null, contentValues);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Fetch first index image name
	 * @return
	 */
	public String fetchFirstIndexImageName(){
		String fileName = null;
		String query = "select * from homeworkimage";
		Cursor cursor =  dbConn.rawQuery(query, null);
		if(cursor.moveToNext()){
			fileName = cursor.getString(cursor.getColumnIndex("imagename"));
		}
		this.closeCursor(cursor);
		return fileName;
	}

	/**
	 * Fetch first index image name
	 * @return
	 */
	public ArrayList<String> fetchAllImageName(){
		ArrayList<String> imageNaleList = new ArrayList<String>();
		String query = "select * from homeworkimage";
		Cursor cursor =  dbConn.rawQuery(query, null);
		while(cursor.moveToNext()){
			imageNaleList.add(cursor.getString(cursor.getColumnIndex("imagename")));
		}
		this.closeCursor(cursor);
		return imageNaleList;
	}

	/**
	 * Fetch first index image name
	 * @return
	 */
	public ArrayList<String> fetchAllImageNameWithReplaced(){
		ArrayList<String> imageNaleList = new ArrayList<String>();
		String imageName = null;
		String query = "select * from homeworkimage";
		Cursor cursor =  dbConn.rawQuery(query, null);
		while(cursor.moveToNext()){
			imageName = cursor.getString(cursor.getColumnIndex("imagename"));
			imageName = imageName.replaceAll(MathFriendzyHelper
					.HOME_WORK_IMAGE_PREFF, "");
			imageName = imageName.replaceAll(MathFriendzyHelper
					.HOME_WORK_QUE_IMAGE_PREFF, "");
			imageNaleList.add(imageName);
		}
		this.closeCursor(cursor);
		return imageNaleList;
	}
	
	/**
	 * Delete uploaded images
	 * @param imageName
	 */
	public void deleteUploadedImageFromDB(String imageName){
		String where = "imagename = '" + imageName + "'";
		dbConn.delete("homeworkimage" , where, null);
	}

	/**
	 * This method close cursor
	 * @param cursor
	 */
	private void closeCursor(Cursor cursor){
		if(cursor != null)
			cursor.close();
	}

	/**
	 * Delete the record from the Homework by used id and player id
	 * @param userId
	 * @param playerId
	 */
	public void deleteFromHomeworkByUserIDAndPlayerId(String userId , String playerId){
		String where = "user_id = '" + userId + "' and player_id = '" + playerId +"'";
		dbConn.delete(HOMEWORK_TABLE, where, null);
	}

	/**
	 *Insert the new data into Homework table
	 * @param userId
	 * @param playerId
	 * @param jsonString
	 */
	public void insertIntoHomeTable(String userId , String playerId , String jsonString){
		ContentValues cv = new ContentValues();
		cv.put("user_id", userId);
		cv.put("player_id", playerId);
		cv.put("homework_json", jsonString);
		dbConn.insert(HOMEWORK_TABLE, null, cv);
	}

	/**
	 * Return the response json String
	 * @param userId
	 * @param playerId
	 * @return
	 */
	public String getHomeworkJsonByUserIdAndPlayerId(String userId , String playerId){
		String jsonString = null;
		String where = "user_id = '" + userId + "' and player_id = '" + playerId +"'";
		String query = "select homework_json from " + HOMEWORK_TABLE + " where " + where;
		Cursor cursor = dbConn.rawQuery(query, null);
		if(cursor.moveToNext()){
			jsonString = cursor.getString(cursor.getColumnIndex("homework_json"));
		}
		this.closeCursor(cursor);
		return jsonString;
	}


	/**
	 * Insert the practice skill play homework into local DB when net is not connected
	 * @param param
	 */
	public void insertIntoPracticeSkillHW(SaveHomeWorkScoreParam param){
		ContentValues cv = new ContentValues();
		cv.put("time", param.getTime());
		cv.put("score", param.getScore());
		cv.put("homeworkId", param.getHWID());
		cv.put("userId", param.getUserId());
		cv.put("playerId", param.getPlayerId());
		cv.put("questionsCount", param.getQuesitonCount());
		cv.put("problems", param.getProblems());
		cv.put("points", param.getPoints());
		cv.put("coins", param.getCoinds());
		cv.put("mathCategoryId", param.getMathCatId());
		cv.put("mathSubCategoryId", param.getMathSubCatId());
		dbConn.insert(PRACTICE_SKILL_HW_TABLE, null, cv);
	}

	/**
	 * Insert the word problem play homework into local DB when net is not connected
	 * @param param
	 */
	public void insertIntoWordProblemHW(SaveHomeWorkWordPlayedDataParam param){
		ContentValues cv = new ContentValues();
		cv.put("userId", param.getUserId());
		cv.put("playerId", param.getPlayerId());
		cv.put("problems", param.getProblems());
		cv.put("categoryId", param.getCatId());
		cv.put("subCategoryId", param.getSubCatId());
		cv.put("time", param.getTime());
		cv.put("score", param.getScore());
		cv.put("homeworkId", param.getHWId());
		cv.put("points", param.getPints());
		cv.put("coins", param.getCoins());
		dbConn.insert(WORD_PROBLEM_HW_TABLE, null, cv);
	}

	/**
	 * Insert the custome play homework into local DB when net is not connected
	 * @param param
	 */
	public void insertIntoCustomeHW(AddCustomeAnswerByStudentParam param){
		ContentValues cv = new ContentValues();
		cv.put("userId", param.getUserId());
		cv.put("playerId", param.getPlayerId());
		cv.put("homeworkId", param.getHWId());
		cv.put("customHwId", param.getCustomHWId());
		cv.put("numOfQuestions", param.getNumberOfQuestions());
		cv.put("score", param.getScore());
		cv.put("questions", param.getQuestionJson());
		dbConn.insert(CUSTOME_HW_TABLE, null, cv);
	}

	/**
	 * Delete from PracticeSkillHW
	 * @param param
	 */
	public void deleteFromPracticeSkillHW(SaveHomeWorkScoreParam param){
		String where = "userId = ? and playerId = ? and homeworkId = ? and mathCategoryId = ?" +
				" and mathSubCategoryId = ?";
		dbConn.delete(PRACTICE_SKILL_HW_TABLE, where, 
				new String[]{param.getUserId() , param.getPlayerId() , param.getHWID()
				, param.getMathCatId() , param.getMathSubCatId() + ""});
	}

	/**
	 * Delete All the record when it uploaded on server
	 */
	public void deleteAllFromPracticeSkillHW(){
		dbConn.delete(PRACTICE_SKILL_HW_TABLE , null , null);
	}
	
	/**
	 * Delete from WordProblemHW
	 * @param param
	 */
	public void deleteFromWordProblemHW(SaveHomeWorkWordPlayedDataParam param){
		String where = "userId = ? and playerId = ? and homeworkId = ? and categoryId = ?" +
				" and subCategoryId = ?";
		dbConn.delete(WORD_PROBLEM_HW_TABLE, where, new String[]{param.getUserId() ,
				param.getPlayerId() , param.getHWId() , param.getCatId() + "", 
				param.getSubCatId() + ""});
	}

	/**
	 * Delete All the record when it uploaded on server
	 */
	public void deleteAllFromWordProblemHW(){
		dbConn.delete(WORD_PROBLEM_HW_TABLE , null , null);
	}
		
	/**
	 * Delete from CustomeHW
	 * @param param
	 */
	public void deleteFromCustomeHW(AddCustomeAnswerByStudentParam param){
		String where = "userId = ? and playerId = ? and homeworkId = ? and customHwId = ?";
		dbConn.delete(CUSTOME_HW_TABLE, where, new String[]{param.getUserId() , param.getPlayerId(),
				param.getHWId() , param.getCustomHWId()});
	}

	/**
	 * Delete All the record when it uploaded on server
	 */
	public void deleteAllFromCustomeHW(){
		dbConn.delete(CUSTOME_HW_TABLE , null , null);
	}
	
	/**
	 * Return the practice skill data
	 * @return
	 */
	public ArrayList<SaveHomeWorkScoreParam> getPracticeSkillHW(){
		ArrayList<SaveHomeWorkScoreParam> practiceList = 
				new ArrayList<SaveHomeWorkScoreParam>();
		String query = "select * from " + PRACTICE_SKILL_HW_TABLE;
		Cursor cursor =  dbConn.rawQuery(query, null);
		while(cursor.moveToNext()){
			SaveHomeWorkScoreParam param = new SaveHomeWorkScoreParam();
			param.setAction("addHomeworkMathsScore");
			param.setCoinds(cursor.getInt(cursor.getColumnIndex("coins")));
			param.setHWID(cursor.getString(cursor.getColumnIndex("homeworkId")));
			param.setMathCatId(cursor.getString(cursor.getColumnIndex("mathCategoryId")));
			param.setMathSubCatId(cursor.getInt(cursor.getColumnIndex("mathSubCategoryId")));
			param.setPlayerId(cursor.getString(cursor.getColumnIndex("playerId")));
			param.setPoints(cursor.getInt(cursor.getColumnIndex("points")));
			param.setProblems(cursor.getString(cursor.getColumnIndex("problems")));
			param.setQuesitonCount(cursor.getInt(cursor.getColumnIndex("questionsCount")));
			param.setScore(cursor.getString(cursor.getColumnIndex("score")));
			param.setTime(cursor.getString(cursor.getColumnIndex("time")));
			param.setUserId(cursor.getString(cursor.getColumnIndex("userId")));
			practiceList.add(param);
		}
		this.closeCursor(cursor);
		return practiceList;
	}

	/**
	 * Return the word problem data
	 * @return
	 */
	public ArrayList<SaveHomeWorkWordPlayedDataParam> getWordProblemHW(){
		ArrayList<SaveHomeWorkWordPlayedDataParam> wordList = 
				new ArrayList<SaveHomeWorkWordPlayedDataParam>();
		String query = "select * from " + WORD_PROBLEM_HW_TABLE;
		Cursor cursor =  dbConn.rawQuery(query, null);
		while(cursor.moveToNext()){
			SaveHomeWorkWordPlayedDataParam param = new SaveHomeWorkWordPlayedDataParam();
			param.setAction("addHomeworkMathWordProblemPlay");
			param.setCatId(cursor.getInt(cursor.getColumnIndex("categoryId")));
			param.setCoins(cursor.getInt(cursor.getColumnIndex("coins")));
			param.setHWId(cursor.getString(cursor.getColumnIndex("homeworkId")));
			param.setPints(cursor.getInt(cursor.getColumnIndex("points")));
			param.setPlayerId(cursor.getString(cursor.getColumnIndex("playerId")));
			param.setProblems(cursor.getString(cursor.getColumnIndex("problems")));
			param.setScore(cursor.getInt(cursor.getColumnIndex("score")));
			param.setSubCatId(cursor.getInt(cursor.getColumnIndex("subCategoryId")));
			param.setTime(cursor.getString(cursor.getColumnIndex("time")));
			param.setUserId(cursor.getString(cursor.getColumnIndex("userId")));
			wordList.add(param);
		}
		this.closeCursor(cursor);
		return wordList;
	}

	/**
	 * Return the custome data
	 * @return
	 */
	public ArrayList<AddCustomeAnswerByStudentParam> getCustomeHW(){
		ArrayList<AddCustomeAnswerByStudentParam> customeList = 
				new ArrayList<AddCustomeAnswerByStudentParam>();
		String query = "select * from " + CUSTOME_HW_TABLE;
		Cursor cursor =  dbConn.rawQuery(query, null);
		while(cursor.moveToNext()){
			AddCustomeAnswerByStudentParam param = new AddCustomeAnswerByStudentParam();
			param.setAction("addCustomAnswersByStdent");
			param.setCustomHWId(cursor.getString(cursor.getColumnIndex("customHwId")));
			param.setHWId(cursor.getString(cursor.getColumnIndex("homeworkId")));
			param.setNumberOfQuestions(cursor.getInt(cursor.getColumnIndex("numOfQuestions")));
			param.setPlayerId(cursor.getString(cursor.getColumnIndex("playerId")));
			param.setQuestionJson(cursor.getString(cursor.getColumnIndex("questions")));
			param.setScore(cursor.getInt(cursor.getColumnIndex("score")));
			param.setUserId(cursor.getString(cursor.getColumnIndex("userId")));
			customeList.add(param);
		}
		this.closeCursor(cursor);
		return customeList;
	}


    /**
     * Delete the record from the Homework by used id and player id
     */
    public void deleteFromUserLinks(SaveGDocLinkOnServerParam param){
        String where = "userId = '" + param.getUserId() + "' and playerId = '"
                + param.getPlayerId() + "' and homeworkId = '" + param.getHwId()
                + "' and customHwId = '" + param.getCusHwId() + "' and questionNo = '"
                + param.getQuestionNo() + "'";
        dbConn.delete(USER_LINK_TABLE, where, null);
    }

    /**
     * Insert the custome play homework into local DB when net is not connected
     * @param param
     */
    public void insertIntoUserLinks(SaveGDocLinkOnServerParam param){
        ContentValues cv = new ContentValues();
        cv.put("userId", param.getUserId());
        cv.put("playerId", param.getPlayerId());
        cv.put("homeworkId", param.getHwId());
        cv.put("customHwId", param.getCusHwId());
        cv.put("questionNo", param.getQuestionNo());
        cv.put("linkString", param.getLinks());
        dbConn.insert(USER_LINK_TABLE, null, cv);
    }


    /**
     * Return the custome data
     * @return
     */
    public ArrayList<SaveGDocLinkOnServerParam> getUserLinks(){
        ArrayList<SaveGDocLinkOnServerParam> userLinks =
                new ArrayList<SaveGDocLinkOnServerParam>();
        String query = "select * from " + USER_LINK_TABLE;
        Cursor cursor =  dbConn.rawQuery(query, null);
        while(cursor.moveToNext()){
            SaveGDocLinkOnServerParam param = new SaveGDocLinkOnServerParam();
            param.setAction("addGdocLinks");
            param.setUserId(cursor.getString(cursor.getColumnIndex("userId")));
            param.setPlayerId(cursor.getString(cursor.getColumnIndex("playerId")));
            param.setHwId(cursor.getString(cursor.getColumnIndex("homeworkId")));
            param.setCusHwId(cursor.getString(cursor.getColumnIndex("customHwId")));
            param.setQuestionNo(cursor.getString(cursor.getColumnIndex("questionNo")));
            param.setLinks(cursor.getString(cursor.getColumnIndex("linkString")));
            userLinks.add(param);
        }
        this.closeCursor(cursor);
        return userLinks;
    }
}
