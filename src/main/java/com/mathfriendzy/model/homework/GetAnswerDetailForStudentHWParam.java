package com.mathfriendzy.model.homework;

/**
 * Created by root on 3/2/16.
 */
public class GetAnswerDetailForStudentHWParam {
    private String action;
    private String userId;
    private String playerId;
    private String hwId;
    private String cusHWId;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getHwId() {
        return hwId;
    }

    public void setHwId(String hwId) {
        this.hwId = hwId;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getCusHWId() {
        return cusHWId;
    }

    public void setCusHWId(String cusHWId) {
        this.cusHWId = cusHWId;
    }
}
