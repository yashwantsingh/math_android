package com.mathfriendzy.model.homework;

import com.mathfriendzy.controller.homework.homwworkworkarea.AddUrlToWorkArea;

import java.io.Serializable;
import java.util.ArrayList;

public class StudentAnsBase implements Serializable{
	
	private String queId;
	private CustomeAns customeAns;
	private String userAns;
	private String imageName;
	private int firstTimeWrong;
	private String stdmessage;
	private int teacherCredit = 1;
	private boolean hasSeenAns;
	
	private boolean isUserAnsCorrect;
	
	private int isGetHelpFromOther;
	private int isWorkAreaPublic;
	
	private int isPrevAnsCorrect;
	private String questionImage;
	private int chatRequestId;

    //for second work area tab
    private String workInput;
    private String opponentInput;
    private String fixedWorkImage;
    private String fixedMessage;
    private String fixedQuestionImage;
    //end change

    //for adding button in isOnwork area
    private int isAnswerAvailable = 1;
    private ArrayList<AddUrlToWorkArea> urlLinks;

	public String getQueId() {
		return queId;
	}
	public void setQueId(String queId) {
		this.queId = queId;
	}
	public CustomeAns getCustomeAns() {
		return customeAns;
	}
	public void setCustomeAns(CustomeAns customeAns) {
		this.customeAns = customeAns;
	}
	public String getUserAns() {
		return userAns;
	}
	public void setUserAns(String userAns) {
		this.userAns = userAns;
	}
	public boolean isUserAnsCorrect() {
		return isUserAnsCorrect;
	}
	public void setUserAnsCorrect(boolean isUserAnsCorrect) {
		this.isUserAnsCorrect = isUserAnsCorrect;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public int getFirstTimeWrong() {
		return firstTimeWrong;
	}
	public void setFirstTimeWrong(int firstTimeWrong) {
		this.firstTimeWrong = firstTimeWrong;
	}
	public String getStdmessage() {
		return stdmessage;
	}
	public void setStdmessage(String stdmessage) {
		this.stdmessage = stdmessage;
	}
	public int isGetHelpFromOther() {
		return isGetHelpFromOther;
	}
	public void setGetHelpFromOther(int getHelpFromOther) {
		this.isGetHelpFromOther = getHelpFromOther;
	}
	public int isWorkAreaPublic() {
		return isWorkAreaPublic;
	}
	public void setWorkAreaPublic(int isWorkAreaPublic) {
		this.isWorkAreaPublic = isWorkAreaPublic;
	}
	public int getTeacherCredit() {
		return teacherCredit;
	}
	public void setTeacherCredit(int teacherCredit) {
		this.teacherCredit = teacherCredit;
	}
	public boolean isHasSeenAns() {
		return hasSeenAns;
	}
	public void setHasSeenAns(boolean hasSeenAns) {
		this.hasSeenAns = hasSeenAns;
	}
	public int isPrevAnsCorrect() {
		return isPrevAnsCorrect;
	}
	public void setPrevAnsCorrect(int isPrevAnsCorrect) {
		this.isPrevAnsCorrect = isPrevAnsCorrect;
	}
	public String getQuestionImage() {
		return questionImage;
	}
	public void setQuestionImage(String questionImage) {
		this.questionImage = questionImage;
	}

    public void setChatRequestId(int chatRequestId) {
        this.chatRequestId = chatRequestId;
    }

    public int getChatRequestId() {
        return chatRequestId;
    }

    public String getWorkInput() {
        return workInput;
    }

    public void setWorkInput(String workInput) {
        this.workInput = workInput;
    }

    public String getOpponentInput() {
        return opponentInput;
    }

    public void setOpponentInput(String opponentInput) {
        this.opponentInput = opponentInput;
    }

    public String getFixedWorkImage() {
        return fixedWorkImage;
    }

    public void setFixedWorkImage(String fixedWorkImage) {
        this.fixedWorkImage = fixedWorkImage;
    }

    public String getFixedMessage() {
        return fixedMessage;
    }

    public void setFixedMessage(String fixedMessage) {
        this.fixedMessage = fixedMessage;
    }

    public String getFixedQuestionImage() {
        return fixedQuestionImage;
    }

    public void setFixedQuestionImage(String fixedQuestionImage) {
        this.fixedQuestionImage = fixedQuestionImage;
    }

    public int getIsAnswerAvailable() {
        return isAnswerAvailable;
    }

    public void setIsAnswerAvailable(int isAnswerAvailable) {
        this.isAnswerAvailable = isAnswerAvailable;
    }

    public ArrayList<AddUrlToWorkArea> getUrlLinks() {
        return urlLinks;
    }

    public void setUrlLinks(ArrayList<AddUrlToWorkArea> urlLinks) {
        this.urlLinks = urlLinks;
    }
}
