package com.mathfriendzy.model.homework;

public class SaveRatingStarForHelpWorkAreaParam {

	private String userId;
	private String playerId;
	private String HWId;
	private String cutomeHWId;
	private String questionId;
	private String action;
	private int ratingStar;
	
	private String senderUserId;
	private String senderPlayerId;
	private String workImageName;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public String getHWId() {
		return HWId;
	}
	public void setHWId(String hWId) {
		HWId = hWId;
	}
	public String getCutomeHWId() {
		return cutomeHWId;
	}
	public void setCutomeHWId(String cutomeHWId) {
		this.cutomeHWId = cutomeHWId;
	}
	public String getQuestionId() {
		return questionId;
	}
	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public int getRatingStar() {
		return ratingStar;
	}
	public void setRatingStar(int ratingStar) {
		this.ratingStar = ratingStar;
	}
	public String getSenderUserId() {
		return senderUserId;
	}
	public void setSenderUserId(String senderUserId) {
		this.senderUserId = senderUserId;
	}
	public String getSenderPlayerId() {
		return senderPlayerId;
	}
	public void setSenderPlayerId(String senderPlayerId) {
		this.senderPlayerId = senderPlayerId;
	}
	public String getWorkImageName() {
		return workImageName;
	}
	public void setWorkImageName(String workImageName) {
		this.workImageName = workImageName;
	}
}
