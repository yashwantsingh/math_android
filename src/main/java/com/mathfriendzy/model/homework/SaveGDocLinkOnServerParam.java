package com.mathfriendzy.model.homework;

/**
 * Created by root on 25/8/16.
 */
public class SaveGDocLinkOnServerParam {
    private String action;
    private String hwId;
    private String cusHwId;
    private String playerId;
    private String userId;
    private String questionNo;
    private String links;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getHwId() {
        return hwId;
    }

    public void setHwId(String hwId) {
        this.hwId = hwId;
    }

    public String getCusHwId() {
        return cusHwId;
    }

    public void setCusHwId(String cusHwId) {
        this.cusHwId = cusHwId;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getQuestionNo() {
        return questionNo;
    }

    public void setQuestionNo(String questionNo) {
        this.questionNo = questionNo;
    }

    public String getLinks() {
        return links;
    }

    public void setLinks(String links) {
        this.links = links;
    }
}
