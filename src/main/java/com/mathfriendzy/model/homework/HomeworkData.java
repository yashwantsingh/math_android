package com.mathfriendzy.model.homework;

import com.mathfriendzy.model.homework.assignhomework.GetStudentByGradeResponse;
import com.mathfriendzy.model.registration.classes.ClassWithName;
import com.mathfriendzy.serveroperation.HttpResponseBase;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by root on 31/5/16.
 */
public class HomeworkData extends HttpResponseBase implements Serializable{
    private String teacherId;
    private String date;
    private String grade;
    private String message;
    private String creationDate;

    private ArrayList<GetStudentByGradeResponse> selectedStudentList = null;
    private ArrayList<CustomeResult> customeQuizList = null;
    private ArrayList<ClassWithName> userClasses = null;

    //for saved homework
    private ArrayList<PracticeResult> practiceResults = null;
    private ArrayList<WordResult> wordResults = null;

    //for get saved homework from server
    private ArrayList<HomeworkData> homeworkDatas = null;
    private int savedHwId;
    private String result;

    //for new homework changes
    private ArrayList<ClassWithName> classWithNames;
    private int lastClassAssignedForHw = 0;
    private boolean isClassArrayExist = false;
    //end changes

    public ArrayList<ClassWithName> getClassWithNames() {
        return classWithNames;
    }

    public void setClassWithNames(ArrayList<ClassWithName> classWithNames) {
        this.classWithNames = classWithNames;
    }

    public int getLastClassAssignedForHw() {
        return lastClassAssignedForHw;
    }

    public void setLastClassAssignedForHw(int lastClassAssignedForHw) {
        this.lastClassAssignedForHw = lastClassAssignedForHw;
    }

    public boolean isClassArrayExist() {
        return isClassArrayExist;
    }

    public void setClassArrayExist(boolean isClassArrayExist) {
        this.isClassArrayExist = isClassArrayExist;
    }

    //for check local
    private boolean isSavedHomeworkSelected = false;

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public ArrayList<GetStudentByGradeResponse> getSelectedStudentList() {
        return selectedStudentList;
    }

    public void setSelectedStudentList(ArrayList<GetStudentByGradeResponse> selectedStudentList) {
        this.selectedStudentList = selectedStudentList;
    }

    public ArrayList<CustomeResult> getCustomeQuizList() {
        return customeQuizList;
    }

    public void setCustomeQuizList(ArrayList<CustomeResult> customeQuizList) {
        this.customeQuizList = customeQuizList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<ClassWithName> getUserClasses() {
        return userClasses;
    }

    public void setUserClasses(ArrayList<ClassWithName> userClasses) {
        this.userClasses = userClasses;
    }

    public ArrayList<PracticeResult> getPracticeResults() {
        return practiceResults;
    }

    public void setPracticeResults(ArrayList<PracticeResult> practiceResults) {
        this.practiceResults = practiceResults;
    }

    public ArrayList<WordResult> getWordResults() {
        return wordResults;
    }

    public void setWordResults(ArrayList<WordResult> wordResults) {
        this.wordResults = wordResults;
    }

    public ArrayList<HomeworkData> getHomeworkDatas() {
        return homeworkDatas;
    }

    public void setHomeworkDatas(ArrayList<HomeworkData> homeworkDatas) {
        this.homeworkDatas = homeworkDatas;
    }

    public int getSavedHwId() {
        return savedHwId;
    }

    public void setSavedHwId(int savedHwId) {
        this.savedHwId = savedHwId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public boolean isSavedHomeworkSelected() {
        return isSavedHomeworkSelected;
    }

    public void setSavedHomeworkSelected(boolean isSavedHomeworkSelected) {
        this.isSavedHomeworkSelected = isSavedHomeworkSelected;
    }
}
