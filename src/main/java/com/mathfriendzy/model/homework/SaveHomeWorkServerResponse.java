package com.mathfriendzy.model.homework;

import java.io.Serializable;

import com.mathfriendzy.serveroperation.HttpResponseBase;

public class SaveHomeWorkServerResponse extends HttpResponseBase implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String result;
	private String finished;
	private String practiceTime;
	private String practiceScore;
	private String wordTime;
	private String wordScore;
	private String customeScore;
	private String avgScore;
	
	private String zeroCredit;
	private String halfCredit;
	private String fullCredit;
	private String totalQuestion;
	
	//for current play
	private String currentPlaySubCatScore;
	
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getFinished() {
		return finished;
	}
	public void setFinished(String finished) {
		this.finished = finished;
	}
	public String getPracticeTime() {
		return practiceTime;
	}
	public void setPracticeTime(String practiceTime) {
		this.practiceTime = practiceTime;
	}
	public String getPracticeScore() {
		return practiceScore;
	}
	public void setPracticeScore(String practiceScore) {
		this.practiceScore = practiceScore;
	}
	public String getWordTime() {
		return wordTime;
	}
	public void setWordTime(String wordTime) {
		this.wordTime = wordTime;
	}
	public String getWordScore() {
		return wordScore;
	}
	public void setWordScore(String wordScore) {
		this.wordScore = wordScore;
	}
	public String getCustomeScore() {
		return customeScore;
	}
	public void setCustomeScore(String customeScore) {
		this.customeScore = customeScore;
	}
	public String getAvgScore() {
		return avgScore;
	}
	public void setAvgScore(String avgScore) {
		this.avgScore = avgScore;
	}
	public String getCurrentPlaySubCatScore() {
		return currentPlaySubCatScore;
	}
	public void setCurrentPlaySubCatScore(String currentPlaySubCatScore) {
		this.currentPlaySubCatScore = currentPlaySubCatScore;
	}
	public String getZeroCredit() {
		return zeroCredit;
	}
	public void setZeroCredit(String zeroCredit) {
		this.zeroCredit = zeroCredit;
	}
	public String getHalfCredit() {
		return halfCredit;
	}
	public void setHalfCredit(String halfCredit) {
		this.halfCredit = halfCredit;
	}
	public String getFullCredit() {
		return fullCredit;
	}
	public void setFullCredit(String fullCredit) {
		this.fullCredit = fullCredit;
	}
	public String getTotalQuestion() {
		return totalQuestion;
	}
	public void setTotalQuestion(String totalQuestion) {
		this.totalQuestion = totalQuestion;
	}
	
}
