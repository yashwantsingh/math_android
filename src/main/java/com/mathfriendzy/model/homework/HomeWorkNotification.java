package com.mathfriendzy.model.homework;

import java.io.Serializable;

public class HomeWorkNotification implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String msg;
    private int isHomeWork;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int isHomeWork() {
        return isHomeWork;
    }

    public void setHomeWork(int isHomeWork) {
        this.isHomeWork = isHomeWork;
    }
}