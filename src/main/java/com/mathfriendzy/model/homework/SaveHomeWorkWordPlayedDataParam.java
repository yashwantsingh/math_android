package com.mathfriendzy.model.homework;

public class SaveHomeWorkWordPlayedDataParam {
	
	private String action;
	private String userId;
	private String playerId;
	private String problems;
	private int catId;
	private int subCatId;
	private String time;
	private int score;
	private String HWId;
	private int pints;
	private int coins;
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public String getProblems() {
		return problems;
	}
	public void setProblems(String problems) {
		this.problems = problems;
	}
	public int getCatId() {
		return catId;
	}
	public void setCatId(int catId) {
		this.catId = catId;
	}
	public int getSubCatId() {
		return subCatId;
	}
	public void setSubCatId(int subCatId) {
		this.subCatId = subCatId;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public String getHWId() {
		return HWId;
	}
	public void setHWId(String hWId) {
		HWId = hWId;
	}
	public int getPints() {
		return pints;
	}
	public void setPints(int pints) {
		this.pints = pints;
	}
	public int getCoins() {
		return coins;
	}
	public void setCoins(int coins) {
		this.coins = coins;
	}
	
	
	
}
