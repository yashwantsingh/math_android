package com.mathfriendzy.model.homework.secondworkarea;

/**
 * Created by root on 16/6/15.
 */
public class AddDuplicateWorkAreaParam {

    public static final int GET_HELP_PARAM = 1;
    public static final int SAW_ANSWER_PARAM = 2;

    private String action;
    private String hwId;
    private String customHWId;
    private String userId;
    private String playerId;
    private String question;
    private String workImageName;
    private String questionImageName;
    private String fixedWorkImageName;
    private String fixedMessage;
    private String fixedQuestionImage;
    private int gothelp;
    private int sawAnswer;


    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getHwId() {
        return hwId;
    }

    public void setHwId(String hwId) {
        this.hwId = hwId;
    }

    public String getCustomHWId() {
        return customHWId;
    }

    public void setCustomHWId(String customHWId) {
        this.customHWId = customHWId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getWorkImageName() {
        return workImageName;
    }

    public void setWorkImageName(String workImageName) {
        this.workImageName = workImageName;
    }

    public String getQuestionImageName() {
        return questionImageName;
    }

    public void setQuestionImageName(String questionImageName) {
        this.questionImageName = questionImageName;
    }

    public String getFixedWorkImageName() {
        return fixedWorkImageName;
    }

    public void setFixedWorkImageName(String fixedWorkImageName) {
        this.fixedWorkImageName = fixedWorkImageName;
    }

    public String getFixedMessage() {
        return fixedMessage;
    }

    public void setFixedMessage(String fixedMessage) {
        this.fixedMessage = fixedMessage;
    }

    public String getFixedQuestionImage() {
        return fixedQuestionImage;
    }

    public void setFixedQuestionImage(String fixedQuestionImage) {
        this.fixedQuestionImage = fixedQuestionImage;
    }

    public int getGothelp() {
        return gothelp;
    }

    public void setGothelp(int gothelp) {
        this.gothelp = gothelp;
    }

    public int getSawAnswer() {
        return sawAnswer;
    }

    public void setSawAnswer(int sawAnswer) {
        this.sawAnswer = sawAnswer;
    }
}
