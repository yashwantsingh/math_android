package com.mathfriendzy.model.homework;

/**
 * Created by root on 22/6/15.
 */
public class UpdateInputStatusForWorkAreaParam {
    private String action;
    private String userId;
    private String playerId;
    private String hwId;
    private String customHWId;
    private String question;
    private int isStudent;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getHwId() {
        return hwId;
    }

    public void setHwId(String hwId) {
        this.hwId = hwId;
    }

    public String getCustomHWId() {
        return customHWId;
    }

    public void setCustomHWId(String customHWId) {
        this.customHWId = customHWId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getIsStudent() {
        return isStudent;
    }

    public void setIsStudent(int isStudent) {
        this.isStudent = isStudent;
    }
}
