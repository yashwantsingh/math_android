package com.mathfriendzy.model.homework;

import android.widget.TextView;

public class CheckUserAnswer extends StudentAnsBase{

	private TextView txtYes;//true
	private TextView txtNo;//false

	public TextView getTxtYes() {
		return txtYes;
	}
	public void setTxtYes(TextView txtYes) {
		this.txtYes = txtYes;
	}
	public TextView getTxtNo() {
		return txtNo;
	}
	public void setTxtNo(TextView txtNo) {
		this.txtNo = txtNo;
	}
}
