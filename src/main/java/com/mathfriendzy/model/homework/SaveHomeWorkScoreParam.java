package com.mathfriendzy.model.homework;

public class SaveHomeWorkScoreParam {
	
	private String action;
	private String time;
	private String score;
	private String HWID;
	private String userId;
	private String playerId;
	private int quesitonCount;
	private String problems;
	private int points;
	private int coinds;
	private String mathCatId;
	private int mathSubCatId;
	
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}
	public String getHWID() {
		return HWID;
	}
	public void setHWID(String hWID) {
		HWID = hWID;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public int getQuesitonCount() {
		return quesitonCount;
	}
	public void setQuesitonCount(int quesitonCount) {
		this.quesitonCount = quesitonCount;
	}
	public String getProblems() {
		return problems;
	}
	public void setProblems(String problems) {
		this.problems = problems;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	public int getCoinds() {
		return coinds;
	}
	public void setCoinds(int coinds) {
		this.coinds = coinds;
	}
	public String getMathCatId() {
		return mathCatId;
	}
	public void setMathCatId(String mathCatId) {
		this.mathCatId = mathCatId;
	}
	public int getMathSubCatId() {
		return mathSubCatId;
	}
	public void setMathSubCatId(int mathSubCatId) {
		this.mathSubCatId = mathSubCatId;
	}
	
	
}
