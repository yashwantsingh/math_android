package com.mathfriendzy.model.homework;

import android.widget.LinearLayout;

public class UserFillAnswer extends StudentAnsBase{

	private LinearLayout txtView;

	public LinearLayout getTxtView() {
		return txtView;
	}
	public void setTxtView(LinearLayout txtView) {
		this.txtView = txtView;
	}
}
