package com.mathfriendzy.model.homework;

public class GetStudentsForWorkAreaHelpParam {
	
	private String userId;
	private String playerId;
	private String HWId;
	private String cutomeHWId;
	private String questionId;
	private String action;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public String getHWId() {
		return HWId;
	}
	public void setHWId(String hWId) {
		HWId = hWId;
	}
	public String getCutomeHWId() {
		return cutomeHWId;
	}
	public void setCutomeHWId(String cutomeHWId) {
		this.cutomeHWId = cutomeHWId;
	}
	public String getQuestionId() {
		return questionId;
	}
	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
}
