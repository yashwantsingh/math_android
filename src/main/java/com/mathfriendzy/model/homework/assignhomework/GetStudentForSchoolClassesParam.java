package com.mathfriendzy.model.homework.assignhomework;

/**
 * Created by root on 3/6/16.
 */
public class GetStudentForSchoolClassesParam {

    private String action;
    private String userId;
    private String classes;
    private boolean isDialogShow;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isDialogShow() {
        return isDialogShow;
    }

    public void setDialogShow(boolean isDialogShow) {
        this.isDialogShow = isDialogShow;
    }

    public String getClasses() {
        return classes;
    }

    public void setClasses(String classes) {
        this.classes = classes;
    }
}
