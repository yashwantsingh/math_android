package com.mathfriendzy.model.homework.assignhomework;

import com.mathfriendzy.serveroperation.HttpResponseBase;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class GetStudentByGradeResponse extends HttpResponseBase implements Serializable{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String fName;
    private String lName;
    private String parentUserId;
    private String playerId;
    private String profileImageId;

    //class title changes
    private int classId;
    //added for create assignment from teacher check homework
    private int subjectId;
    //end class title changes

    private boolean isSelected;

    private ArrayList<GetStudentByGradeResponse> studentList;

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getParentUserId() {
        return parentUserId;
    }

    public void setParentUserId(String parentUserId) {
        this.parentUserId = parentUserId;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getProfileImageId() {
        return profileImageId;
    }

    public void setProfileImageId(String profileImageId) {
        this.profileImageId = profileImageId;
    }

    public ArrayList<GetStudentByGradeResponse> getStudentList() {
        return studentList;
    }

    public void setStudentList(ArrayList<GetStudentByGradeResponse> studentList) {
        this.studentList = studentList;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    /*public boolean equals(Object obj){
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof GetStudentByGradeResponse)) {
            return false;
        }
        GetStudentByGradeResponse other = (GetStudentByGradeResponse) obj;
        return this.playerId.equals(other.playerId);
    }*/

    /**
     * Remove the duplicate student based on player id
     * @param students
     * @return
     */
    public static ArrayList<GetStudentByGradeResponse> removeDuplicateStudents
    (ArrayList<GetStudentByGradeResponse> students){
        ArrayList<GetStudentByGradeResponse> uniqueStudentList = new ArrayList<GetStudentByGradeResponse>();
        Map<String, GetStudentByGradeResponse> map = new LinkedHashMap<String, GetStudentByGradeResponse>();
        for (GetStudentByGradeResponse student : students) {
            if(!map.containsKey(student.getPlayerId()))
                map.put(student.getPlayerId(), student);
        }
        uniqueStudentList.addAll(map.values());
        return uniqueStudentList;
    }
}
