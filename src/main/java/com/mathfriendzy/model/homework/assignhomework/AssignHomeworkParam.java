package com.mathfriendzy.model.homework.assignhomework;

public class AssignHomeworkParam {

    private String action;
    private String userId;
    private String date;
    private String grade;
    private String studentJson;
    private String practiceCatJson;
    private String wordCatJson;
    private String customeDataJson;
    private String message;

    //for save homework on server
    private boolean isShowDialog;
    private int hwId;
    private String selectedClasses;

    private boolean isTeacherEditHomework;

    public String getAction() {
        return action;
    }
    public void setAction(String action) {
        this.action = action;
    }
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getGrade() {
        return grade;
    }
    public void setGrade(String grade) {
        this.grade = grade;
    }
    public String getStudentJson() {
        return studentJson;
    }
    public void setStudentJson(String studentJson) {
        this.studentJson = studentJson;
    }
    public String getPracticeCatJson() {
        return practiceCatJson;
    }
    public void setPracticeCatJson(String practiceCatJson) {
        this.practiceCatJson = practiceCatJson;
    }
    public String getWordCatJson() {
        return wordCatJson;
    }
    public void setWordCatJson(String wordCatJson) {
        this.wordCatJson = wordCatJson;
    }
    public String getCustomeDataJson() {
        return customeDataJson;
    }
    public void setCustomeDataJson(String customeDataJson) {
        this.customeDataJson = customeDataJson;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isShowDialog() {
        return isShowDialog;
    }

    public void setShowDialog(boolean isShowDialog) {
        this.isShowDialog = isShowDialog;
    }

    public int getHwId() {
        return hwId;
    }

    public void setHwId(int hwId) {
        this.hwId = hwId;
    }

    public String getSelectedClasses() {
        return selectedClasses;
    }

    public void setSelectedClasses(String selectedClasses) {
        this.selectedClasses = selectedClasses;
    }

    public boolean isTeacherEditHomework() {
        return isTeacherEditHomework;
    }

    public void setTeacherEditHomework(boolean isTeacherEditHomework) {
        this.isTeacherEditHomework = isTeacherEditHomework;
    }
}
