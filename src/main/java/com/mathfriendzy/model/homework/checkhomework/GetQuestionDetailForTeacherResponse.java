package com.mathfriendzy.model.homework.checkhomework;

import com.mathfriendzy.model.homework.CustomePlayerAns;
import com.mathfriendzy.serveroperation.HttpResponseBase;

/**
 * Created by root on 17/6/15.
 */
public class GetQuestionDetailForTeacherResponse extends HttpResponseBase{
    private String result;
    private CustomePlayerAns playerAns;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public CustomePlayerAns getPlayerAns() {
        return playerAns;
    }

    public void setPlayerAns(CustomePlayerAns playerAns) {
        this.playerAns = playerAns;
    }

/* private String quesNo;
    private String answer;
    private int isCorrect;
    private String workImage;
    private int firstTimeWrong;
    private int teacherCredit;
    private String message;
    private int workAreaPublic;
    private int getHelp;
    private String questionImage;
    private int chatRequestId;
    private String fixedWorkImage;
    private String fixedMessage;
    private String fixedQuestionImage;


    public String getQuesNo() {
        return quesNo;
    }

    public void setQuesNo(String quesNo) {
        this.quesNo = quesNo;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getIsCorrect() {
        return isCorrect;
    }

    public void setIsCorrect(int isCorrect) {
        this.isCorrect = isCorrect;
    }

    public String getWorkImage() {
        return workImage;
    }

    public void setWorkImage(String workImage) {
        this.workImage = workImage;
    }

    public int getFirstTimeWrong() {
        return firstTimeWrong;
    }

    public void setFirstTimeWrong(int firstTimeWrong) {
        this.firstTimeWrong = firstTimeWrong;
    }

    public int getTeacherCredit() {
        return teacherCredit;
    }

    public void setTeacherCredit(int teacherCredit) {
        this.teacherCredit = teacherCredit;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getWorkAreaPublic() {
        return workAreaPublic;
    }

    public void setWorkAreaPublic(int workAreaPublic) {
        this.workAreaPublic = workAreaPublic;
    }

    public int getGetHelp() {
        return getHelp;
    }

    public void setGetHelp(int getHelp) {
        this.getHelp = getHelp;
    }

    public String getQuestionImage() {
        return questionImage;
    }

    public void setQuestionImage(String questionImage) {
        this.questionImage = questionImage;
    }

    public int getChatRequestId() {
        return chatRequestId;
    }

    public void setChatRequestId(int chatRequestId) {
        this.chatRequestId = chatRequestId;
    }

    public String getFixedWorkImage() {
        return fixedWorkImage;
    }

    public void setFixedWorkImage(String fixedWorkImage) {
        this.fixedWorkImage = fixedWorkImage;
    }

    public String getFixedMessage() {
        return fixedMessage;
    }

    public void setFixedMessage(String fixedMessage) {
        this.fixedMessage = fixedMessage;
    }

    public String getFixedQuestionImage() {
        return fixedQuestionImage;
    }

    public void setFixedQuestionImage(String fixedQuestionImage) {
        this.fixedQuestionImage = fixedQuestionImage;
    }*/
}
