package com.mathfriendzy.model.homework.checkhomework;

import com.mathfriendzy.model.homework.CustomeResult;
import com.mathfriendzy.model.homework.PracticeResult;
import com.mathfriendzy.model.homework.WordResult;
import com.mathfriendzy.serveroperation.HttpResponseBase;

import java.io.Serializable;
import java.util.ArrayList;

public class GetDetailOfHomeworkWithCustomeResponse extends HttpResponseBase implements Serializable{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String parentId;
    private String playerId;
    private String fName;
    private String lName;
    private String practiceTime;
    private String practiceScore;
    private String wordTime;
    private String wordScore;
    private String customeScore;
    private String avgScore;
    private String zeroCredit;
    private String halfCredit;
    private String fullCredit;
    private String totalQuestions;
    private String chatId;
    private String chatUserName;

    //class title changes
    private int classId;
    private int subjectId;
    private String className;
    private String subjectName;
    //end class title change

    private ArrayList<PracticeResult> practiseResultList;
    private ArrayList<WordResult>     wordResultList;
    private ArrayList<CustomeResult>  customeresultList;

    private ArrayList<CustomeResult>  customeresultCatgoriesList;

    private ArrayList<GetDetailOfHomeworkWithCustomeResponse> resposeList = null;

    //for set grade for student detail activity only
    private String grade;
    private String dueDate;
    private String homeWorkId;

    //for assign from check homework by teahcer
    private boolean isSelected = false;

    public String getParentId() {
        return parentId;
    }
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
    public String getPlayerId() {
        return playerId;
    }
    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }
    public String getfName() {
        return fName;
    }
    public void setfName(String fName) {
        this.fName = fName;
    }
    public String getlName() {
        return lName;
    }
    public void setlName(String lName) {
        this.lName = lName;
    }
    public String getPracticeTime() {
        return practiceTime;
    }
    public void setPracticeTime(String practiceTime) {
        this.practiceTime = practiceTime;
    }
    public String getPracticeScore() {
        return practiceScore;
    }
    public void setPracticeScore(String practiceScore) {
        this.practiceScore = practiceScore;
    }
    public String getWordTime() {
        return wordTime;
    }
    public void setWordTime(String wordTime) {
        this.wordTime = wordTime;
    }
    public String getWordScore() {
        return wordScore;
    }
    public void setWordScore(String wordScore) {
        this.wordScore = wordScore;
    }
    public String getCustomeScore() {
        return customeScore;
    }
    public void setCustomeScore(String customeScore) {
        this.customeScore = customeScore;
    }
    public String getAvgScore() {
        return avgScore;
    }
    public void setAvgScore(String avgScore) {
        this.avgScore = avgScore;
    }
    public String getZeroCredit() {
        return zeroCredit;
    }
    public void setZeroCredit(String zeroCredit) {
        this.zeroCredit = zeroCredit;
    }
    public String getHalfCredit() {
        return halfCredit;
    }
    public void setHalfCredit(String halfCredit) {
        this.halfCredit = halfCredit;
    }
    public String getFullCredit() {
        return fullCredit;
    }
    public void setFullCredit(String fullCredit) {
        this.fullCredit = fullCredit;
    }
    public String getTotalQuestions() {
        return totalQuestions;
    }
    public void setTotalQuestions(String totalQuestions) {
        this.totalQuestions = totalQuestions;
    }
    public ArrayList<PracticeResult> getPractiseResultList() {
        return practiseResultList;
    }
    public void setPractiseResultList(ArrayList<PracticeResult> practiseResultList) {
        this.practiseResultList = practiseResultList;
    }
    public ArrayList<WordResult> getWordResultList() {
        return wordResultList;
    }
    public void setWordResultList(ArrayList<WordResult> wordResultList) {
        this.wordResultList = wordResultList;
    }
    public ArrayList<CustomeResult> getCustomeresultList() {
        return customeresultList;
    }
    public void setCustomeresultList(ArrayList<CustomeResult> customeresultList) {
        this.customeresultList = customeresultList;
    }
    public ArrayList<GetDetailOfHomeworkWithCustomeResponse> getResposeList() {
        return resposeList;
    }
    public void setResposeList(
            ArrayList<GetDetailOfHomeworkWithCustomeResponse> resposeList) {
        this.resposeList = resposeList;
    }
    public String getGrade() {
        return grade;
    }
    public void setGrade(String grade) {
        this.grade = grade;
    }
    public String getDueDate() {
        return dueDate;
    }
    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }
    public ArrayList<CustomeResult> getCustomeresultCatgoriesList() {
        return customeresultCatgoriesList;
    }
    public void setCustomeresultCatgoriesList(
            ArrayList<CustomeResult> customeresultCatgoriesList) {
        this.customeresultCatgoriesList = customeresultCatgoriesList;
    }
    public String getHomeWorkId() {
        return homeWorkId;
    }
    public void setHomeWorkId(String homeWorkId) {
        this.homeWorkId = homeWorkId;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getChatUserName() {
        return chatUserName;
    }

    public void setChatUserName(String chatUserName) {
        this.chatUserName = chatUserName;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
