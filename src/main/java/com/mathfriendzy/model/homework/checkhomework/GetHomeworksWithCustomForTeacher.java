package com.mathfriendzy.model.homework.checkhomework;

public class GetHomeworksWithCustomForTeacher {
	
	private String action;
	private String teacherId;
	private String startDate;
    private String endDate;
    private String classId;
    private int offset;
    private int limit;
    private int firstVisit;

	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getFirstVisit() {
        return firstVisit;
    }

    public void setFirstVisit(int firstVisit) {
        this.firstVisit = firstVisit;
    }
}
