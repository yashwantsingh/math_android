package com.mathfriendzy.model.homework.checkhomework;

public class GetDetailsOfHomeworkWithCustomParam {
	
	private String action;
	private String HWId;
	private int selectedClassId;

	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getHWId() {
		return HWId;
	}
	public void setHWId(String hWId) {
		HWId = hWId;
	}

    public int getSelectedClassId() {
        return selectedClassId;
    }

    public void setSelectedClassId(int selectedClassId) {
        this.selectedClassId = selectedClassId;
    }
}
