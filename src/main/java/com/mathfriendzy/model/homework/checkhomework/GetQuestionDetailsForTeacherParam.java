package com.mathfriendzy.model.homework.checkhomework;

/**
 * Created by root on 17/6/15.
 */
public class GetQuestionDetailsForTeacherParam {
    private String action;
    private String userId;
    private String playerId;
    private String hwId;
    private String customHWId;
    private String quesNo;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getHwId() {
        return hwId;
    }

    public void setHwId(String hwId) {
        this.hwId = hwId;
    }

    public String getCustomHWId() {
        return customHWId;
    }

    public void setCustomHWId(String customHWId) {
        this.customHWId = customHWId;
    }

    public String getQuesNo() {
        return quesNo;
    }

    public void setQuesNo(String quesNo) {
        this.quesNo = quesNo;
    }
}
