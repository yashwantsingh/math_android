package com.mathfriendzy.model.homework.checkhomework;

import com.mathfriendzy.model.homework.CustomeResult;
import com.mathfriendzy.model.homework.PracticeResult;
import com.mathfriendzy.model.homework.WordResult;
import com.mathfriendzy.model.registration.classes.ClassWithName;
import com.mathfriendzy.serveroperation.HttpResponseBase;

import java.io.Serializable;
import java.util.ArrayList;

public class GetHomeworksWithCustomForTeacherResponse extends HttpResponseBase implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String homeworkId;
	private String date;
	private String grade;
	private String practiceScore;
	private String wordScore;
	private String customScore;
	private String avgScore;

    //new Homework changes
    private String finished;
    private String message;
    private ArrayList<PracticeResult> practiseResultList;
    private ArrayList<WordResult>     wordResultList;
    private ArrayList<CustomeResult>  customeresultList;
    private ArrayList<ClassWithName> classWithNames;
    private int lastClassAssignedForHw = 0;
    private String result;
    private boolean isClassArrayExist = false;
    private String className;
    private String subjectName;
    //end changes


	private ArrayList<GetHomeworksWithCustomForTeacherResponse> homeworkWithCustomeList = null;

	public String getHomeworkId() {
		return homeworkId;
	}

	public void setHomeworkId(String homeworkId) {
		this.homeworkId = homeworkId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getPracticeScore() {
		return practiceScore;
	}

	public void setPracticeScore(String practiceScore) {
		this.practiceScore = practiceScore;
	}

	public String getWordScore() {
		return wordScore;
	}

	public void setWordScore(String wordScore) {
		this.wordScore = wordScore;
	}

	public String getCustomScore() {
		return customScore;
	}

	public void setCustomScore(String customScore) {
		this.customScore = customScore;
	}

	public String getAvgScore() {
		return avgScore;
	}

	public void setAvgScore(String avgScore) {
		this.avgScore = avgScore;
	}

	public ArrayList<GetHomeworksWithCustomForTeacherResponse> getHomeworkWithCustomeList() {
		return homeworkWithCustomeList;
	}

	public void setHomeworkWithCustomeList(
			ArrayList<GetHomeworksWithCustomForTeacherResponse> homeworkWithCustomeList) {
		this.homeworkWithCustomeList = homeworkWithCustomeList;
	}

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getFinished() {
        return finished;
    }

    public void setFinished(String finished) {
        this.finished = finished;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<PracticeResult> getPractiseResultList() {
        return practiseResultList;
    }

    public void setPractiseResultList(ArrayList<PracticeResult> practiseResultList) {
        this.practiseResultList = practiseResultList;
    }

    public ArrayList<WordResult> getWordResultList() {
        return wordResultList;
    }

    public void setWordResultList(ArrayList<WordResult> wordResultList) {
        this.wordResultList = wordResultList;
    }

    public ArrayList<CustomeResult> getCustomeresultList() {
        return customeresultList;
    }

    public void setCustomeresultList(ArrayList<CustomeResult> customeresultList) {
        this.customeresultList = customeresultList;
    }

    public ArrayList<ClassWithName> getClassWithNames() {
        return classWithNames;
    }

    public void setClassWithNames(ArrayList<ClassWithName> classWithNames) {
        this.classWithNames = classWithNames;
    }

    public int getLastClassAssignedForHw() {
        return lastClassAssignedForHw;
    }

    public void setLastClassAssignedForHw(int lastClassAssignedForHw) {
        this.lastClassAssignedForHw = lastClassAssignedForHw;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public boolean isClassArrayExist() {
        return isClassArrayExist;
    }

    public void setClassArrayExist(boolean isClassArrayExist) {
        this.isClassArrayExist = isClassArrayExist;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }
}
