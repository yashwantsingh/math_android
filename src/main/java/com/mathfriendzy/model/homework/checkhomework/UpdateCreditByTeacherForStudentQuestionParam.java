package com.mathfriendzy.model.homework.checkhomework;

/**
 * Created by root on 22/1/16.
 */
public class UpdateCreditByTeacherForStudentQuestionParam {
    private String action;
    private String dataJson;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getDataJson() {
        return dataJson;
    }

    public void setDataJson(String dataJson) {
        this.dataJson = dataJson;
    }
}
