package com.mathfriendzy.model.homework;

import java.io.Serializable;
import java.util.ArrayList;

public class CustomeAns implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String queNo;
	private String options;
	private String correctAns;
	private String ansSuffix;
	private int isLeftAnswer;
	private int fillInType;
	private int answerType;

	//private String multiPleChoiceUserAns;
	
	//for layout visibility in adapter
	private boolean isVisibleLayout = false;

	//for assign homeweork quiz
	private ArrayList<String> multipleChoiceOptionList;

    //for work area button added in layout
    private int isAnswerAvailable = 1;
    //for teacher check homework show correct and wrong answer counter
    private int correctAnsCounter = 0;
    private int wrongAnsCounter = 0;

    private String questionString = "";//default blank

	public boolean isVisibleLayout() {
		return isVisibleLayout;
	}
	public void setVisibleLayout(boolean isVisibleLayout) {
		this.isVisibleLayout = isVisibleLayout;
	}
	public String getQueNo() {
		return queNo;
	}
	public void setQueNo(String queNo) {
		this.queNo = queNo;
	}
	public String getOptions() {
		return options;
	}
	public void setOptions(String options) {
		this.options = options;
	}
	public String getCorrectAns() {
		return correctAns;
	}
	public void setCorrectAns(String correctAns) {
		this.correctAns = correctAns;
	}
	public String getAnsSuffix() {
		return ansSuffix;
	}
	public void setAnsSuffix(String ansSuffix) {
		this.ansSuffix = ansSuffix;
	}
	public int getIsLeftUnit() {
		return isLeftAnswer;
	}
	public void setIsLeftUnit(int isLateUnit) {
		this.isLeftAnswer = isLateUnit;
	}
	public int getFillInType() {
		return fillInType;
	}
	public void setFillInType(int fillInType) {
		this.fillInType = fillInType;
	}
	public int getAnswerType() {
		return answerType;
	}
	public void setAnswerType(int answerType) {
		this.answerType = answerType;
	}
	
	/*public String getMultiPleChoiceUserAns() {
		return multiPleChoiceUserAns;
	}
	public void setMultiPleChoiceUserAns(String multiPleChoiceUserAns) {
		this.multiPleChoiceUserAns = multiPleChoiceUserAns;
	}*/
	
	public ArrayList<String> getMultipleChoiceOptionList() {
		return multipleChoiceOptionList;
	}
	public void setMultipleChoiceOptionList(
			ArrayList<String> multipleChoiceOptionList) {
		this.multipleChoiceOptionList = multipleChoiceOptionList;
	}

    public int getIsAnswerAvailable() {
        return isAnswerAvailable;
    }

    public void setIsAnswerAvailable(int isAnswerAvailable) {
        this.isAnswerAvailable = isAnswerAvailable;
    }

    public int getCorrectAnsCounter() {
        return correctAnsCounter;
    }

    public void setCorrectAnsCounter(int correctAnsCounter) {
        this.correctAnsCounter = correctAnsCounter;
    }

    public int getWrongAnsCounter() {
        return wrongAnsCounter;
    }

    public void setWrongAnsCounter(int wrongAnsCounter) {
        this.wrongAnsCounter = wrongAnsCounter;
    }

    public String getQuestionString() {
        return questionString;
    }

    public void setQuestionString(String questionString) {
        this.questionString = questionString;
    }
}
