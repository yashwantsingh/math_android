package com.mathfriendzy.model.homework;

import java.util.Comparator;

public class PracticeSkillSortByCategoryId implements Comparator<PracticeResult>{
	@Override
	public int compare(PracticeResult lhs, PracticeResult rhs) {
		//return lhs.getIntCatId().compareTo(rhs.getIntCatId());
		Integer catId1 = lhs.getIntCatId();
		Integer catId2 = rhs.getIntCatId();
		return (int) Math.signum(catId1.compareTo(catId2));
	}
}
