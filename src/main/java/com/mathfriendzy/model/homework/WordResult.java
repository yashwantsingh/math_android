package com.mathfriendzy.model.homework;

import java.io.Serializable;
import java.util.ArrayList;

public class WordResult implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String catIg;
	private ArrayList<WordSubCatResult> subCatResultList;
	
	public String getCatIg() {
		return catIg;
	}
	public void setCatIg(String catIg) {
		this.catIg = catIg;
	}
	public ArrayList<WordSubCatResult> getSubCatResultList() {
		return subCatResultList;
	}
	public void setSubCatResultList(ArrayList<WordSubCatResult> subCatResultList) {
		this.subCatResultList = subCatResultList;
	}
	
	
}
