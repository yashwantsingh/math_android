package com.mathfriendzy.model.homework;


import com.mathfriendzy.model.registration.classes.ClassWithName;
import com.mathfriendzy.serveroperation.HttpResponseBase;

import java.io.Serializable;
import java.util.ArrayList;

public class GetHomeWorkForStudentWithCustomeResponse extends HttpResponseBase implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String homeWorkId;
	private String date;
	private String grade;
	private String finished;
	private String practiceTime;
	private String practiceScore;
	private String wordTime;
	private String wordScore;
	private String customeScore;
	private String avgScore;
	private String zeroCredit;
	private String halfCredit;
	private String fullCredit;
	private String totalQuestion;
	private String message;
	
	private ArrayList<PracticeResult> practiseResultList;
	private ArrayList<WordResult>     wordResultList;
	private ArrayList<CustomeResult>  customeresultList;

	private ArrayList<GetHomeWorkForStudentWithCustomeResponse> finalDataList;


    //for new homework changes
    private ArrayList<ClassWithName> classWithNames;
    private int lastClassAssignedForHw = 0;
    private String result;
    private boolean isClassArrayExist = false;
    private String className;
    private String subjectName;
    private int subId;
    //end changes

	//for offline homework
	private String jsonString = null;
	
	public String getHomeWorkId() {
		return homeWorkId;
	}
	public void setHomeWorkId(String homeWorkId) {
		this.homeWorkId = homeWorkId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getFinished() {
		return finished;
	}
	public void setFinished(String finished) {
		this.finished = finished;
	}
	public String getPracticeTime() {
		return practiceTime;
	}
	public void setPracticeTime(String practiceTime) {
		this.practiceTime = practiceTime;
	}
	public String getPracticeScore() {
		return practiceScore;
	}
	public void setPracticeScore(String practiceScore) {
		this.practiceScore = practiceScore;
	}
	public String getWordTime() {
		return wordTime;
	}
	public void setWordTime(String wordTime) {
		this.wordTime = wordTime;
	}
	public String getWordScore() {
		return wordScore;
	}
	public void setWordScore(String wordScore) {
		this.wordScore = wordScore;
	}
	public String getCustomeScore() {
		return customeScore;
	}
	public void setCustomeScore(String customeScore) {
		this.customeScore = customeScore;
	}
	public String getAvgScore() {
		return avgScore;
	}
	public void setAvgScore(String avgScore) {
		this.avgScore = avgScore;
	}
	public ArrayList<PracticeResult> getPractiseResultList() {
		return practiseResultList;
	}
	public void setPractiseResultList(ArrayList<PracticeResult> practiseResultList) {
		this.practiseResultList = practiseResultList;
	}
	public ArrayList<WordResult> getWordResultList() {
		return wordResultList;
	}
	public void setWordResultList(ArrayList<WordResult> wordResultList) {
		this.wordResultList = wordResultList;
	}
	public ArrayList<CustomeResult> getCustomeresultList() {
		return customeresultList;
	}
	public void setCustomeresultList(ArrayList<CustomeResult> customeresultList) {
		this.customeresultList = customeresultList;
	}
	public ArrayList<GetHomeWorkForStudentWithCustomeResponse> getFinalDataList() {
		return finalDataList;
	}
	public void setFinalDataList(
			ArrayList<GetHomeWorkForStudentWithCustomeResponse> finalDataList) {
		this.finalDataList = finalDataList;
	}
	public String getZeroCredit() {
		return zeroCredit;
	}
	public void setZeroCredit(String zeroCredit) {
		this.zeroCredit = zeroCredit;
	}
	public String getHalfCredit() {
		return halfCredit;
	}
	public void setHalfCredit(String halfCredit) {
		this.halfCredit = halfCredit;
	}
	public String getFullCredit() {
		return fullCredit;
	}
	public void setFullCredit(String fullCredit) {
		this.fullCredit = fullCredit;
	}
	public String getTotalQuestion() {
		return totalQuestion;
	}
	public void setTotalQuestion(String totalQuestion) {
		this.totalQuestion = totalQuestion;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getJsonString() {
		return jsonString;
	}
	public void setJsonString(String jsonString) {
		this.jsonString = jsonString;
	}

    public ArrayList<ClassWithName> getClassWithNames() {
        return classWithNames;
    }

    public void setClassWithNames(ArrayList<ClassWithName> classWithNames) {
        this.classWithNames = classWithNames;
    }

    public int getLastClassAssignedForHw() {
        return lastClassAssignedForHw;
    }

    public void setLastClassAssignedForHw(int lastClassAssignedForHw) {
        this.lastClassAssignedForHw = lastClassAssignedForHw;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public boolean isClassArrayExist() {
        return isClassArrayExist;
    }

    public void setClassArrayExist(boolean isClassArrayExist) {
        this.isClassArrayExist = isClassArrayExist;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public int getSubId() {
        return subId;
    }

    public void setSubId(int subId) {
        this.subId = subId;
    }
}
