package com.mathfriendzy.model.homework;

public class HWImage {

	private String workAreaImage;
	private String questionImage;
	
	public String getWorkAreaImage() {
		return workAreaImage;
	}
	public void setWorkAreaImage(String workAreaImage) {
		this.workAreaImage = workAreaImage;
	}
	public String getQuestionImage() {
		return questionImage;
	}
	public void setQuestionImage(String questionImage) {
		this.questionImage = questionImage;
	}
	
	
}
