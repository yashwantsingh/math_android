package com.mathfriendzy.model.homework;

import com.mathfriendzy.serveroperation.HttpResponseBase;

import java.util.ArrayList;

/**
 * Created by root on 3/2/16.
 */
public class GetStudentDetailAnswerResponse extends HttpResponseBase{

    private ArrayList<CustomePlayerAns> customePlayerAnsList;
    private int score;

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public ArrayList<CustomePlayerAns> getCustomePlayerAnsList() {
        return customePlayerAnsList;
    }

    public void setCustomePlayerAnsList(ArrayList<CustomePlayerAns> customePlayerAnsList) {
        this.customePlayerAnsList = customePlayerAnsList;
    }
}
