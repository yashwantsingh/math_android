package com.mathfriendzy.model.homework;

public class GetWorkAreaChatMsgParam {
	private String userId;
	private String playerId;
	private String questionNo;
	private String HWId;
	private String customeHWId;
	private String action;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public String getQuestionNo() {
		return questionNo;
	}
	public void setQuestionNo(String questionNo) {
		this.questionNo = questionNo;
	}
	public String getHWId() {
		return HWId;
	}
	public void setHWId(String hWId) {
		HWId = hWId;
	}
	public String getCustomeHWId() {
		return customeHWId;
	}
	public void setCustomeHWId(String customeHWId) {
		this.customeHWId = customeHWId;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
}
