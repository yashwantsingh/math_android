package com.mathfriendzy.model.homework;

/**
 * Created by root on 31/5/16.
 */
public class SelectAssignHomeworkClassParam {
    private boolean isShowForPrevSaveHomework = false;

    public boolean isShowForPrevSaveHomework() {
        return isShowForPrevSaveHomework;
    }

    public void setShowForPrevSaveHomework(boolean isShowForPrevSaveHomework) {
        this.isShowForPrevSaveHomework = isShowForPrevSaveHomework;
    }
}
