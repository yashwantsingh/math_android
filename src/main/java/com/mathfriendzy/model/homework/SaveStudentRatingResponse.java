package com.mathfriendzy.model.homework;

import java.io.Serializable;

import com.mathfriendzy.serveroperation.HttpResponseBase;

public class SaveStudentRatingResponse extends HttpResponseBase implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String result;
	private int avgRatingStar;
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public int getAvgRatingStar() {
		return avgRatingStar;
	}
	public void setAvgRatingStar(int avgRatingStar) {
		this.avgRatingStar = avgRatingStar;
	}
	
	
}
