package com.mathfriendzy.model.homework;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by root on 25/3/15.
 */
public class HomeworkWorkAreaExtras implements Serializable{
    private String teacherName;
    private int grade;
    private String dueDate;
    private CustomeResult customeResult;
    private GetHomeWorkForStudentWithCustomeResponse homeWorkQuizzData;
    private CustomeAns customeAns;
    private int index = 0;
    private  CustomePlayerAns playerAns;
    private String teacherStudentMsg = null;
    private int workareapublic = 1;
    private String playerAndQuestionImage = "";
    private String cropImageUriFromPdf = "";
    private ArrayList<String> downloadImageList = null;

    public String getTeacherName() {
        return teacherName;
    }

    public int getGrade() {
        return grade;
    }

    public String getDueDate() {
        return dueDate;
    }

    public CustomeResult getCustomeResult() {
        return customeResult;
    }

    public GetHomeWorkForStudentWithCustomeResponse getHomeWorkQuizzData() {
        return homeWorkQuizzData;
    }

    public CustomeAns getCustomeAns() {
        return customeAns;
    }

    public int getIndex() {
        return index;
    }

    public CustomePlayerAns getPlayerAns() {
        return playerAns;
    }

    public String getTeacherStudentMsg() {
        return teacherStudentMsg;
    }

    public int getWorkareapublic() {
        return workareapublic;
    }

    public String getPlayerAndQuestionImage() {
        return playerAndQuestionImage;
    }

    public String getCropImageUriFromPdf() {
        return cropImageUriFromPdf;
    }

    public ArrayList<String> getDownloadImageList() {
        return downloadImageList;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public void setHomeWorkQuizzData(GetHomeWorkForStudentWithCustomeResponse homeWorkQuizzData) {
        this.homeWorkQuizzData = homeWorkQuizzData;
    }

    public void setCustomeResult(CustomeResult customeResult) {
        this.customeResult = customeResult;
    }

    public void setCustomeAns(CustomeAns customeAns) {
        this.customeAns = customeAns;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void setPlayerAns(CustomePlayerAns playerAns) {
        this.playerAns = playerAns;
    }

    public void setTeacherStudentMsg(String teacherStudentMsg) {
        this.teacherStudentMsg = teacherStudentMsg;
    }

    public void setWorkareapublic(int workareapublic) {
        this.workareapublic = workareapublic;
    }

    public void setPlayerAndQuestionImage(String playerAndQuestionImage) {
        this.playerAndQuestionImage = playerAndQuestionImage;
    }

    public void setCropImageUriFromPdf(String cropImageUriFromPdf) {
        this.cropImageUriFromPdf = cropImageUriFromPdf;
    }

    public void setDownloadImageList(ArrayList<String> downloadImageList) {
        this.downloadImageList = downloadImageList;
    }
}
