package com.mathfriendzy.model.homework;

import com.mathfriendzy.controller.homework.homwworkworkarea.AddUrlToWorkArea;

import java.io.Serializable;
import java.util.ArrayList;

public class CustomePlayerAns implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String queNo;
	private String ans;
	private int isCorrect;
	private String workImage;
	private int firstTimeWrong;
	private String teacherCredit;
	private String message;
	
	private int isGetHelp;
	private int isWorkAreaPublic;
	
	private String questionImage;
	private int chatRequestedId;

    //for second work area tab
    private String fixedWorkImage;
    private String fixedMessage;
    private String fixedQuestionImage;
    private String workInput;
    private String opponentInput;
    //end for second work for check homework

    private int isAnswerAvailable = 1;


    /*//for teacher check homework show correct and wrong answer counter
    private int correctAnsCounter = 0;
    private int wrongAnsCounter = 0;*/
    private ArrayList<AddUrlToWorkArea> urlLinks;

	public String getQueNo() {
		return queNo;
	}
	public void setQueNo(String queNo) {
		this.queNo = queNo;
	}
	public String getAns() {
		return ans;
	}
	public void setAns(String ans) {
		this.ans = ans;
	}
	public int getIsCorrect() {
		return isCorrect;
	}
	public void setIsCorrect(int isCorrect) {
		this.isCorrect = isCorrect;
	}
	public String getWorkImage() {
		return workImage;
	}
	public void setWorkImage(String workImage) {
		this.workImage = workImage;
	}
	public int getFirstTimeWrong() {
		return firstTimeWrong;
	}
	public void setFirstTimeWrong(int firstTimeWrong) {
		this.firstTimeWrong = firstTimeWrong;
	}
	public String getTeacherCredit() {
		return teacherCredit;
	}
	public void setTeacherCredit(String teacherCredit) {
		this.teacherCredit = teacherCredit;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getIsGetHelp() {
		return isGetHelp;
	}
	public void setIsGetHelp(int isGetHelp) {
		this.isGetHelp = isGetHelp;
	}
	public int getIsWorkAreaPublic() {
		return isWorkAreaPublic;
	}
	public void setIsWorkAreaPublic(int isWorkAreaPublic) {
		this.isWorkAreaPublic = isWorkAreaPublic;
	}
	public String getQuestionImage() {
		return questionImage;
	}
	public void setQuestionImage(String questionImage) {
		this.questionImage = questionImage;
	}
	public int getChatRequestedId() {
		return chatRequestedId;
	}
	public void setChatRequestedId(int chatRequestedId) {
		this.chatRequestedId = chatRequestedId;
	}

    public String getFixedWorkImage() {
        return fixedWorkImage;
    }

    public void setFixedWorkImage(String fixedWorkImage) {
        this.fixedWorkImage = fixedWorkImage;
    }

    public String getFixedMessage() {
        return fixedMessage;
    }

    public void setFixedMessage(String fixedMessage) {
        this.fixedMessage = fixedMessage;
    }

    public String getFixedQuestionImage() {
        return fixedQuestionImage;
    }

    public void setFixedQuestionImage(String fixedQuestionImage) {
        this.fixedQuestionImage = fixedQuestionImage;
    }

    public String getWorkInput() {
        return workInput;
    }

    public void setWorkInput(String workInput) {
        this.workInput = workInput;
    }

    public String getOpponentInput() {
        return opponentInput;
    }

    public void setOpponentInput(String opponentInput) {
        this.opponentInput = opponentInput;
    }

    public int getIsAnswerAvailable() {
        return isAnswerAvailable;
    }

    public void setIsAnswerAvailable(int isAnswerAvailable) {
        this.isAnswerAvailable = isAnswerAvailable;
    }

    public ArrayList<AddUrlToWorkArea> getUrlLinks() {
        return urlLinks;
    }

    public void setUrlLinks(ArrayList<AddUrlToWorkArea> urlLinks) {
        this.urlLinks = urlLinks;
    }

    /*public int getCorrectAnsCounter() {
        return correctAnsCounter;
    }

    public void setCorrectAnsCounter(int correctAnsCounter) {
        this.correctAnsCounter = correctAnsCounter;
    }

    public int getWrongAnsCounter() {
        return wrongAnsCounter;
    }

    public void setWrongAnsCounter(int wrongAnsCounter) {
        this.wrongAnsCounter = wrongAnsCounter;
    }*/
}
