package com.mathfriendzy.model.registration;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.learningcenter.MathResultTransferObj;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.player.temp.TempPlayerOperation;
import com.mathfriendzy.model.registration.classes.ClassWithName;
import com.mathfriendzy.model.registration.classes.UserClasses;
import com.mathfriendzy.utils.CommonUtils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;

public class Register
{
    public static int SUCCESS      = 2;//registere user success
    public static int INVALID_CITY = 1;//invalid city
    public static int INVALID_ZIP  = 0;//invalid zip
    public static int INVALID_EMAIL = 3;//Invalid Email
    private ArrayList<UserPlayerDto> playerList = null;
    private RegistereUserDto regUserObj = null;

    SharedPreferences sharedPreference = null;
    private Context context = null;

    /**
     * Constructor
     * @param context
     */
    public Register(Context context)
    {
        this.context = context;
    }

    /**
     * This method register user on server and get the detail after registration and store into the database
     * @param regObj
     * @return
     */
    public int registerUserOnserver(RegistereUserDto regObj)
    {
        int resultValue 	   = 0;
        String action 		   = "createUser";

        String encodeFirstName = null;
        String encodeLastName  = null;
        String encodeGmail     = null;
        String encodePass      = null;
        String encodeCountryId = null;
        String encodeCountryIso= null;
        String encodeStateId   = null;
        String encodeStateCode = null;
        String encodedCity 	   = null;
        String encodePreferedLanguageId = null;
        String encodeSchoolId  = null;
        String encodeSchool    = null;
        String encodeIsParent  = null;
        String encodeValume    = null;
        String encodeCoin      = null;
        String encodeZip       = null;
        String encodedPlayers  = null;
        //String encodeUrl 	 = null;


        try
        {
            encodeFirstName= URLEncoder.encode(regObj.getFirstName(), "UTF-8");
            encodeLastName = URLEncoder.encode(regObj.getLastName(), "UTF-8");
            encodePass     = URLEncoder.encode(regObj.getPass(), "UTF-8");
            //encodeCountryId= URLEncoder.encode(regObj.getCountryId(), "UTF-8");
            //encodeCountryIso = URLEncoder.encode(regObj.getCountryIso(), "UTF-8");
            //encodeStateId  = URLEncoder.encode(regObj.getStateId(), "UTF-8");
            //encodeStateCode= URLEncoder.encode(regObj.getStateCode(), "UTF-8");
            encodedCity    = URLEncoder.encode(regObj.getCity(), "UTF-8");
            encodedPlayers = URLEncoder.encode(regObj.getPlayers(),"UTF-8");
            encodeSchool   = URLEncoder.encode(regObj.getSchoolName(),"UTF-8");
            //encodeValume   = URLEncoder.encode(regObj.getVolume(),"UTF-8");
            encodeZip      = URLEncoder.encode(regObj.getZip(),"UTF-8");
            //encodeCoin     = URLEncoder.encode(regObj.getCoins(),"UTF-8");
            //encodeIsParent = URLEncoder.encode(regObj.getIsParent() ,"UTF-8");
            //encodeSchoolId = URLEncoder.encode(regObj.getSchoolId(),"UTF-8");
            //encodePreferedLanguageId = URLEncoder.encode(regObj.getPreferedLanguageId(),"UTF-8");
        }
        catch (UnsupportedEncodingException e){
            Log.e("Register", "Error:Unicode String Exception");
        }

        String strURL = COMPLETE_URL  + "action=" 	+ 	action 			+ "&"
                + "fname=" 		+ 	encodeFirstName + "&"
                + "lname="		+	encodeLastName	+ "&"
                + "email="		+	regObj.getEmail()		+ "&"
                + "password="	+   encodePass    	+ "&"
                + "countryId="  + regObj.getCountryId() + "&"
                + "countryIso=" + regObj.getCountryIso()+ "&"
                + "stateId="    + regObj.getStateId()   + "&"
                + "stateCode="  + regObj.getStateCode() + "&"
                + "city="       + encodedCity           + "&"
                + "preferredLanguageId=" + regObj.getPreferedLanguageId() + "&"
                + "schoolId="   + regObj.getSchoolId()  + "&"
                + "schoolName=" + encodeSchool          + "&"
                + "isParent="   + regObj.getIsParent()  + "&"
                + "volume="     + regObj.getVolume()    + "&"
                + "coins="      + regObj.getCoins()     + "&"
                + "zip="        + encodeZip       + "&"
                + "appId="        + MathFriendzyHelper.getAppIdForRequest()  + "&"
                + "players="    + encodedPlayers;

        if(CommonUtils.LOG_ON){
            Log.e("", "registration url strURL " + strURL);
            Log.e("", "players " + regObj.getPlayers());
        }

		/*try 
		{
			encodeUrl    = URLEncoder.encode(strURL, "UTF-8");
		} 
		catch (UnsupportedEncodingException e) 
		{
			Log.e("Register", "Error:Unicode String Exception");
		}
		Log.e("URL", encodeUrl);*/
        //Log.e("URL", strURL);

        resultValue = this.parseJson(CommonUtils.readFromURL(strURL));

        if(resultValue == SUCCESS)
        {
            UserRegistrationOperation userObj = new UserRegistrationOperation(context);
            UserPlayerOperation userPlayerObj = new UserPlayerOperation(context);
            if(userObj.isUserTableExist())
            {
                userObj.insertUserData(regUserObj);
                userPlayerObj.deleteFromUserPlayer();
                userPlayerObj.insertUserPlayerData(playerList);
            }
            else
            {
                userObj.createUserTable(regUserObj);
                userPlayerObj.createUserPlayerTable(playerList);
            }

            //changes for updation for coins distribution

            TempPlayerOperation tempObj = new TempPlayerOperation(context);
            tempObj.deleteFromTempPlayer();
            tempObj.closeConn();
        }
        return resultValue;
    }

    /**
     * This method update the user information on server and get the detail after updation and store into database
     * @param regObj
     * @return
     */
    public int updateUserOnserver(RegistereUserDto regObj)
    {
        int resultValue 	= 0;
        String action 		= "createUser";
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try
        {
            String encodedCity = regObj.getCity();
            try
            {
                //encodeFirstName= URLEncoder.encode(regObj.getFirstName(), "UTF-8");
                //encodeLastName = URLEncoder.encode(regObj.getLastName(), "UTF-8");
                //encodePass     = URLEncoder.encode(regObj.getPass(), "UTF-8");
                //encodeCountryId= URLEncoder.encode(regObj.getCountryId(), "UTF-8");
                //encodeCountryIso = URLEncoder.encode(regObj.getCountryIso(), "UTF-8");
                //encodeStateId  = URLEncoder.encode(regObj.getStateId(), "UTF-8");
                //encodeStateCode= URLEncoder.encode(regObj.getStateCode(), "UTF-8");
                //encodedCity    = URLEncoder.encode(regObj.getCity(), "UTF-8");
                //encodedPlayers = URLEncoder.encode(regObj.getPlayers(),"UTF-8");
                //encodeSchool   = URLEncoder.encode(regObj.getSchoolName(),"UTF-8");
                //encodeValume   = URLEncoder.encode(regObj.getVolume(),"UTF-8");
                //encodeZip      = URLEncoder.encode(regObj.getZip(),"UTF-8");
                //encodeCoin     = URLEncoder.encode(regObj.getCoins(),"UTF-8");
                //encodeIsParent = URLEncoder.encode(regObj.getIsParent() ,"UTF-8");
                //encodeSchoolId = URLEncoder.encode(regObj.getSchoolId(),"UTF-8");
                //encodePreferedLanguageId = URLEncoder.encode(regObj.getPreferedLanguageId(),"UTF-8");
            }
            catch (Exception e){
                e.printStackTrace();
            }

            nameValuePairs.add(new BasicNameValuePair("action", action));
            nameValuePairs.add(new BasicNameValuePair("fname", regObj.getFirstName()));
            nameValuePairs.add(new BasicNameValuePair("lname", regObj.getLastName()));
            nameValuePairs.add(new BasicNameValuePair("email", regObj.getEmail()));
            nameValuePairs.add(new BasicNameValuePair("password", regObj.getPass()));
            nameValuePairs.add(new BasicNameValuePair("countryId", regObj.getCountryId()));
            nameValuePairs.add(new BasicNameValuePair("countryIso", regObj.getCountryIso()));
            nameValuePairs.add(new BasicNameValuePair("stateId", regObj.getStateId()));
            nameValuePairs.add(new BasicNameValuePair("stateCode", regObj.getStateCode()));
            nameValuePairs.add(new BasicNameValuePair("city", encodedCity));
            nameValuePairs.add(new BasicNameValuePair("preferredLanguageId", regObj.getPreferedLanguageId()));
            nameValuePairs.add(new BasicNameValuePair("schoolId", regObj.getSchoolId()));
            nameValuePairs.add(new BasicNameValuePair("schoolName", regObj.getSchoolName()));
            nameValuePairs.add(new BasicNameValuePair("isParent", regObj.getIsParent()));
            nameValuePairs.add(new BasicNameValuePair("volume", regObj.getVolume()));
            nameValuePairs.add(new BasicNameValuePair("coins", regObj.getCoins()));
            nameValuePairs.add(new BasicNameValuePair("zip", regObj.getZip()));
            nameValuePairs.add(new BasicNameValuePair("players", regObj.getPlayers()));
            nameValuePairs.add(new BasicNameValuePair("modifying", "1"));
            nameValuePairs.add(new BasicNameValuePair("userId", regObj.getUserId()));
            if(regObj.isFromAccountScreen()){
                nameValuePairs.add(new BasicNameValuePair("emailSubscription", regObj.getEmailSubscription()));
            }
        }
        catch (Exception e1)
        {
            Log.e("Register", "Error in addMathPlayLevelScore While parsing" + e1);
        }

        resultValue = this.parseJson(CommonUtils.readFromURL(nameValuePairs));

        if(resultValue == SUCCESS){
            UserRegistrationOperation userObj = new UserRegistrationOperation(context);
            UserPlayerOperation userPlayerObj = new UserPlayerOperation(context);
            if(userObj.isUserTableExist()){
                userObj.insertUserData(regUserObj);
                userPlayerObj.deleteFromUserPlayer();
                userPlayerObj.insertUserPlayerData(playerList);

            }else{
                userObj.createUserTable(regUserObj);
                userPlayerObj.createUserPlayerTable(playerList);
            }
        }
        return resultValue;
    }

	/*private void test(String tag , String value){
		Log.e(tag, value);
	}*/

    /**
     * This mathod parse the Json Data of the User from server after updation or registration
     * @param jsonString
     * @return
     */
    private int parseJson(String jsonString)
    {
        playerList = new ArrayList<UserPlayerDto>();
        regUserObj = new RegistereUserDto();

        int resultValue 			= 0;
        UserPlayerDto userPlayerObj = null;

        if(CommonUtils.LOG_ON)
            Log.e("Register Json String", jsonString);

        if(jsonString != null){
            try
            {
                JSONObject jObject = new JSONObject(jsonString);
                String result = 	jObject.getString("result");

                if(result.equals("success"))
                {
                    resultValue = SUCCESS;

                    JSONObject jsonObject2 = jObject.getJSONObject("data");

                    regUserObj.setFirstName(jsonObject2.getString("fname"));
                    regUserObj.setUserId(jsonObject2.getString("userId"));
                    regUserObj.setVolume(jsonObject2.getString("volume"));
                    regUserObj.setSchoolId(jsonObject2.getString("schoolId"));
                    regUserObj.setSchoolName(jsonObject2.getString("schoolName"));
                    //Log.e("SchoolName", jsonObject2.getString("schoolName") + " Hello");
                    regUserObj.setPreferedLanguageId(jsonObject2.getString("preferredLanguageId"));
                    regUserObj.setLastName(jsonObject2.getString("lname"));
                    regUserObj.setEmail(jsonObject2.getString("email"));
                    regUserObj.setPass(jsonObject2.getString("password"));
                    regUserObj.setIsParent(jsonObject2.getString("isParent"));
                    regUserObj.setCountryId(jsonObject2.getString("countryId"));
                    regUserObj.setStateId(jsonObject2.getString("stateId"));
                    regUserObj.setState(jsonObject2.getString("state"));
                    //Log.e("inside json parsing", jsonObject2.getString("state") + " stateName");
                    regUserObj.setCity(jsonObject2.getString("city"));
                    regUserObj.setCoins(jsonObject2.getString("coins"));
                    regUserObj.setZip(jsonObject2.getString("zip"));

                    try{
                        MathFriendzyHelper.saveSubscriptionExpireDate
                                (context, jsonObject2.getString("expireDate"));
                    }catch(Exception e){
                        e.printStackTrace();
                    }
					/*Log.e("FirstName", jsonObject2.getString("fname"));
				    Log.e("Userid", jsonObject2.getString("userId"));*/

                    JSONArray jsonPlayerObj = jsonObject2.getJSONArray("players");
                    for( int i = 0 ; i < jsonPlayerObj.length() ; i++)
                    {
                        JSONObject jsonObject = jsonPlayerObj.getJSONObject(i);
                        userPlayerObj = new UserPlayerDto();
                        userPlayerObj.setFirstname(jsonObject.getString("fName"));
                        userPlayerObj.setLastname(jsonObject.getString("lName"));
                        userPlayerObj.setSchoolId(jsonObject.getString("schoolId"));
                        userPlayerObj.setSchoolName(jsonObject.getString("schoolName"));
                        userPlayerObj.setGrade(jsonObject.getString("grade"));
                        userPlayerObj.setTeacherUserId(jsonObject.getString("teacherUserId"));
                        userPlayerObj.setTeacherFirstName(jsonObject.getString("teacherFirstName"));
                        userPlayerObj.setTeacheLastName(jsonObject.getString("teacherLastName"));
                        userPlayerObj.setIndexOfAppearance(jsonObject.getString("indexOfAppearance"));
                        userPlayerObj.setParentUserId(jsonObject.getString("parentUserId"));
                        userPlayerObj.setPlayerid(jsonObject.getString("playerId"));
                        userPlayerObj.setCompletelavel(jsonObject.getString("competeLevel"));
                        userPlayerObj.setProfileImage(jsonObject.getString("profileImageId").getBytes());
                        userPlayerObj.setImageName(jsonObject.getString("profileImageId"));//changes
                        userPlayerObj.setCoin(jsonObject.getString("coins"));
                        userPlayerObj.setPoints(jsonObject.getString("points"));
                        userPlayerObj.setCity(jsonObject.getString("city"));
                        userPlayerObj.setStateName(jsonObject.getString("state"));
                        userPlayerObj.setUsername(jsonObject.getString("userName"));
                        userPlayerObj.setIsTutor(jsonObject.getString("isTutor"));
                        userPlayerObj.setChatId(jsonObject.getString("chatId"));
                        userPlayerObj.setChatUserName(jsonObject.getString("chatUserName"));

                        userPlayerObj.setAllowPaidTutor(jsonObject.getString("allowPaidTutor"));
                        userPlayerObj.setPurchasedTime(jsonObject.getString("purchasedTime"));
                        //userPlayerObj.setStudentIdByTeacher(jsonObject.getString("studentIdByTeacher"));
                        playerList.add(userPlayerObj);
                    }

                    if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject2 , "schoolYear")){
                        regUserObj.setSchoolYear(jsonObject2.getString("schoolYear"));
                    }

                    if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject2 , "defaultGrade")){
                        regUserObj.setDefaultGrade(jsonObject2.getString("defaultGrade"));
                    }

                    regUserObj.setUserClasses(this.parseClassesForUser(jsonObject2));
                    MathFriendzyHelper.saveUserInSharedPreff(context , regUserObj);
                }
                else if(result.equals("fail"))
                {
                    if(jObject.getString("errorcode").equals("-9001"))
                        resultValue = INVALID_CITY;
                    else if(jObject.getString("errorcode").equals("-9002"))
                        resultValue = INVALID_ZIP;
                    else if(jObject.getString("errorcode").equals("1062"))
                        resultValue = INVALID_EMAIL;
                }
            }
            catch (JSONException e)
            {
                Log.e("Register ", "Error while parsing :" + e);
                Log.e("Register Json String", jsonString);
                return 4;
            }
        }
        return resultValue;
    }

    /**
     * This method delete the register user from the server
     * @param userId
     * @param playerId
     * @return
     */
    public String deleteRegisteredUser(String userId,String playerId)
    {
        String action = "deletePlayer";
        String strURL = COMPLETE_URL  + "action=" 	+ 	action 			+ "&"
                + "userId="   +   userId          + "&"
                + "playerId=" +   playerId;

        return this.parseDeleteUserJson(CommonUtils.readFromURL(strURL));
    }

    /**
     * This method parse
     * @param jsonString
     * @return
     */
    private String parseDeleteUserJson(String jsonString)
    {
        String result = "";
        //Log.e("Register Json String", jsonString);

        if(jsonString != null){
            try
            {
                JSONObject jObject = new JSONObject(jsonString);
                result = 	jObject.getString("result");
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            return result;
        }else{
            return null;
        }
    }


    /**
     * This method update score on server
     * @param mathObj
     */
    public void savePlayerScoreOnServer(ArrayList<MathResultTransferObj> mathObj)
    {
        String action = "addMathsScore";
        //String encodeProblems = "";
        for( int i = 0 ; i < mathObj.size() ; i ++ )
        {
			/*try 
			{
				encodeProblems= URLEncoder.encode(mathObj.get(i).getProblems(), "UTF-8");
			}
			catch (UnsupportedEncodingException e) 
			{
				Log.e("Register", "Error:Unicode String Exception");
			}

		String strURL = COMPLETE_URL  + "action=" + 	action 			+ "&" 
				+ "roundId=" 	+ 	mathObj.get(i).getRoundId()	+ "&"
				+ "isFirstRound="	+	mathObj.get(i).getIsFirstRound()	+ "&"
				+ "gameType="	+	mathObj.get(i).getGameType()		+ "&"
				+ "win="	+	mathObj.get(i).getIsWin()    + "&"
				+ "userId="  + mathObj.get(i).getUserId() + "&"
				+ "playerId=" + mathObj.get(i).getPlayerId()+ "&"
				+ "isFakePlayer="    + mathObj.get(i).getIsFakePlayer()   + "&"
				+ "problems="  + "<equations>" +  encodeProblems + "</equations>" ;*/
            //Log.e("Register", "inside savePlayerScoreonServer url " +  strURL);

            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            try
            {
                nameValuePairs.add(new BasicNameValuePair("action", action));
                nameValuePairs.add(new BasicNameValuePair("roundId", mathObj.get(i).getRoundId() + ""));
                nameValuePairs.add(new BasicNameValuePair("isFirstRound", mathObj.get(i).getIsFirstRound()));
                nameValuePairs.add(new BasicNameValuePair("gameType", mathObj.get(i).getGameType()));
                nameValuePairs.add(new BasicNameValuePair("win", mathObj.get(i).getIsWin() + ""));
                nameValuePairs.add(new BasicNameValuePair("userId", mathObj.get(i).getUserId()));
                nameValuePairs.add(new BasicNameValuePair("playerId", mathObj.get(i).getPlayerId()));
                nameValuePairs.add(new BasicNameValuePair("isFakePlayer", mathObj.get(i).getIsFakePlayer() + ""));
                nameValuePairs.add(new BasicNameValuePair("problems", "<equations>" + mathObj.get(i).getProblems() + "</equations>"));
            }
            catch (Exception e1)
            {
                Log.e("Register", "Error in addMathPlayLevelScore While parsing");
            }

            //this.parseScoreJson(CommonUtils.readFromURL(strURL));
            this.parseScoreJson(CommonUtils.readFromURL(nameValuePairs));
        }
    }

    private void parseScoreJson(String jsonString)
    {
        //Log.e("Register", "inside parse Score Json String " + jsonString);
    }

    /**
     * This method update score on server for login user
     * @param mathObj
     */
    public void savePlayerScoreOnServerForloginuser(MathResultTransferObj mathObj)
    {
        String action = "addMathsScore";

		/*String encodeProblems = "";

			try 
			{
				encodeProblems= URLEncoder.encode(mathObj.getProblems(), "UTF-8");
			}
			catch (UnsupportedEncodingException e) 
			{
				Log.e("Register", "Error:Unicode String Exception");
			}

		String strURL = COMPLETE_URL  + "action=" + 	action 			+ "&" 
				+ "roundId=" 	+ 	mathObj.getRoundId()	+ "&"
				+ "isFirstPlay="	+	mathObj.getIsFirstRound()	+ "&"
				+ "gameType="	+	mathObj.getGameType()		+ "&"
				+ "win="	+	mathObj.getIsWin()    + "&"
				+ "userId="  + mathObj.getUserId() + "&"
				+ "playerId=" + mathObj.getPlayerId()+ "&"
				+ "wasCompetingWithFakePlayer="    + mathObj.getIsFakePlayer()   + "&"
				+ "problems="  + "<equations>" +  encodeProblems + "</equations>" + "&"
				+ "totalScore=" + mathObj.getTotalScore();
		 */

        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try
        {
            nameValuePairs.add(new BasicNameValuePair("action", action));
            nameValuePairs.add(new BasicNameValuePair("roundId", mathObj.getRoundId() + ""));
            nameValuePairs.add(new BasicNameValuePair("isFirstPlay", mathObj.getIsFirstRound()));
            nameValuePairs.add(new BasicNameValuePair("gameType", mathObj.getGameType()));
            nameValuePairs.add(new BasicNameValuePair("win", mathObj.getIsWin() + ""));
            nameValuePairs.add(new BasicNameValuePair("userId", mathObj.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", mathObj.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("wasCompetingWithFakePlayer", mathObj.getIsFakePlayer() + ""));
            nameValuePairs.add(new BasicNameValuePair("problems", "<equations>" + mathObj.getProblems() + "</equations>"));
            nameValuePairs.add(new BasicNameValuePair("totalScore", mathObj.getTotalScore() + ""));
        }
        catch (Exception e1)
        {
            Log.e("Register", "Error in addMathPlayLevelScore While parsing");
        }

        //Log.e("Register", "inside savePlayerScoreOnServerForloginuser url " +  strURL);
        //this.parseScoreJson(CommonUtils.readFromURL(strURL));
        this.parseScoreJson(CommonUtils.readFromURL(nameValuePairs));

    }

    /**
     * This method update score on server for login user
     * @param mathObj
     */
    public void addMathPlayLevelScore(MathResultTransferObj mathObj)
    {
        String action = "addMathPlayLevelScore";

		/*try 
			{
				encodeProblems= URLEncoder.encode(mathObj.getProblems(), "UTF-8");
			}
			catch (UnsupportedEncodingException e) 
			{
				Log.e("Register", "Error:Unicode String Exception");
			}*/

		/*String strURL = COMPLETE_URL  + "action=" + 	action 			+ "&" 
				+ "roundId=" 	+ 	mathObj.getRoundId()	+ "&"
				+ "gameType="	+	mathObj.getGameType()		+ "&"
				+ "userId="  + mathObj.getUserId() + "&"
				+ "playerId=" + mathObj.getPlayerId()+ "&"
				+ "problems="  + "<equations>" +  encodeProblems + "</equations>" + "&"
				+ "totalScore=" + mathObj.getTotalScore() + "&"
				+ "equationType=" + mathObj.getEquationTypeId() + "&" 
				+ "level=" + mathObj.getLevel() + "&"
				+ "stars=" + mathObj.getStars() + "&"
				+ "totalPoints=" + mathObj.getTotalPoints() + "&"
				+ "coins=" + mathObj.getCoins();*/


        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

        try
        {
            nameValuePairs.add(new BasicNameValuePair("action", action));
            nameValuePairs.add(new BasicNameValuePair("roundId", mathObj.getRoundId() + ""));
            nameValuePairs.add(new BasicNameValuePair("gameType", mathObj.getGameType()));
            nameValuePairs.add(new BasicNameValuePair("userId", mathObj.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", mathObj.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("problems", "<equations>" +  mathObj.getProblems() + "</equations>"));
            nameValuePairs.add(new BasicNameValuePair("totalScore", mathObj.getTotalScore() + ""));
            nameValuePairs.add(new BasicNameValuePair("equationType", mathObj.getEquationTypeId() +""));
            nameValuePairs.add(new BasicNameValuePair("level", mathObj.getLevel() + ""));
            nameValuePairs.add(new BasicNameValuePair("stars", mathObj.getStars() + ""));
            nameValuePairs.add(new BasicNameValuePair("totalPoints", mathObj.getTotalPoints() + ""));
            nameValuePairs.add(new BasicNameValuePair("coins", mathObj.getCoins() +""));
        }
        catch (Exception e1)
        {
            Log.e("Register", "Error in addMathPlayLevelScore While parsing");
        }

        this.parseScoreJson(CommonUtils.readFromURL(nameValuePairs));
    }

    /**
     * This method add coins and points on server
     * @param playerObj
     */
    public void addPointsAndCoinsOnServerForloginUser(PlayerTotalPointsObj playerObj)
    {
        String action = "addPointsAndCoinsForPlayer";

        String strUrl = COMPLETE_URL  + "action=" + 	action 			+ "&"
                + "userId="   + playerObj.getUserId()  + "&"
                + "playerId=" + playerObj.getPlayerId() + "&"
                + "points="   + playerObj.getTotalPoints() + "&"
                + "coins="    + playerObj.getCoins() + "&"
                + "appId="	  + CommonUtils.APP_ID;

        //Log.e("Register", " inside addpoints and Score on server For login user" + strUrl);

        this.parseScoreJson(CommonUtils.readFromURL(strUrl));
    }


    /**
     * This method update score on server for login user
     * @param mathObj
     */
    public void savePlayerScoreOnServerForloginuserForSingleFriendzy(MathResultTransferObj mathObj)
    {
        String action = "addMathsScore";

		/*String encodeProblems = "";

			try 
			{
				encodeProblems= URLEncoder.encode(mathObj.getProblems(), "UTF-8");
			}
			catch (UnsupportedEncodingException e) 
			{
				Log.e("Register", "Error:Unicode String Exception");
			}

		String strURL = COMPLETE_URL  + "action=" + 	action 			+ "&" 
				+ "roundId=" 	+ 	mathObj.getRoundId()	+ "&"
				+ "isFirstPlay="	+	mathObj.getIsFirstRound()	+ "&"
				+ "gameType="	+	mathObj.getGameType()		+ "&"
				+ "win="	+	mathObj.getIsWin()    + "&"
				+ "userId="  + mathObj.getUserId() + "&"
				+ "playerId=" + mathObj.getPlayerId()+ "&"
				+ "wasCompetingWithFakePlayer="    + mathObj.getIsFakePlayer()   + "&"
				+ "problems="  + "<equations>" +  encodeProblems + "</equations>" + "&"
				+ "totalScore=" + mathObj.getTotalScore();
		 */

        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try
        {
            nameValuePairs.add(new BasicNameValuePair("action", action));
            nameValuePairs.add(new BasicNameValuePair("roundId", mathObj.getRoundId() + ""));
            nameValuePairs.add(new BasicNameValuePair("isFirstPlay", mathObj.getIsFirstRound()));
            nameValuePairs.add(new BasicNameValuePair("gameType", mathObj.getGameType()));
            nameValuePairs.add(new BasicNameValuePair("win", mathObj.getIsWin() + ""));
            nameValuePairs.add(new BasicNameValuePair("userId", mathObj.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("playerId", mathObj.getPlayerId()));
            nameValuePairs.add(new BasicNameValuePair("challengerPlayerId", mathObj.getChallengerId()));
            nameValuePairs.add(new BasicNameValuePair("wasCompetingWithFakePlayer", mathObj.getIsFakePlayer() + ""));
            nameValuePairs.add(new BasicNameValuePair("problems", "<equations>" + mathObj.getProblems() + "</equations>"));
            nameValuePairs.add(new BasicNameValuePair("totalScore", mathObj.getTotalScore() + ""));
        }
        catch (Exception e1)
        {
            Log.e("Register", "Error in addMathPlayLevelScore While parsing");
        }

        //Log.e("Register", "inside savePlayerScoreOnServerForloginuser url " +  strURL);
        //this.parseScoreJson(CommonUtils.readFromURL(strURL));
        this.parseScoreJson(CommonUtils.readFromURL(nameValuePairs));
    }

    private ArrayList<UserClasses> parseClassesForUser(JSONObject jsonObject){
        ArrayList<UserClasses> userClasseses = new ArrayList<UserClasses>();
        try{
            if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject , "classes")){
                JSONArray classJsonArray = jsonObject.getJSONArray("classes");
                for(int i = 0 ; i < classJsonArray.length() ; i ++ ){
                    JSONObject classJsonObject = classJsonArray.getJSONObject(i);
                    UserClasses userclass = new UserClasses();
                    userclass.setGrade(classJsonObject.getInt("grade"));
                    JSONArray jsonArrayWithClassDetail = classJsonObject.getJSONArray("classes");
                    ArrayList<ClassWithName> classWithNames = new ArrayList<ClassWithName>();
                    for(int j = 0 ; j < jsonArrayWithClassDetail.length() ; j ++ ){
                        JSONObject jsonClassObjWithClassName = jsonArrayWithClassDetail.getJSONObject(j);
                        ClassWithName classWithName = new ClassWithName();
                        classWithName.setClassId(jsonClassObjWithClassName.getInt("classId"));
                        classWithName.setClassName(jsonClassObjWithClassName.getString("className"));
                        classWithNames.add(classWithName);
                    }
                    userclass.setClassList(classWithNames);
                    userClasseses.add(userclass);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return userClasseses;
    }
}
