package com.mathfriendzy.model.registration;

import com.mathfriendzy.model.registration.classes.UserClasses;

import java.util.ArrayList;

public class RegistereUserDto
{
	private String userId;
	private String firstName;
	private String lastName;
	private String email;
	private String pass;
	private String countryId;
	private String countryIso;
	private String stateId;
	private String stateCode;
	private String city;
	private String preferedLanguageId;
	private String schoolId;
	private String isParent;
	private String isTeacher;
	private String isStudent;
	private String volume;
	private String zip;
	private String players;
	private String coins;
	private String schoolName;
	private String state;
	
	//for show ads
	private int isAdsDisable = 0;

    //added for weekly report
    private String emailSubscription = "";
    private boolean isFromAccountScreen = false;

    //class title changes
    private ArrayList<UserClasses> userClasses;
    private String schoolYear = null;
    private String defaultGrade = null;

	public int getIsAdsDisable() {
		return isAdsDisable;
	}
	public void setIsAdsDisable(int isAdsDisable) {
		this.isAdsDisable = isAdsDisable;
	}
	
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	private String AddedPlayers[];
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getIsTeacher() {
		return isTeacher;
	}
	public void setIsTeacher(String isTeacher) {
		this.isTeacher = isTeacher;
	}
	public String getIsStudent() {
		return isStudent;
	}
	public void setIsStudent(String isStudent) {
		this.isStudent = isStudent;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getCountryId() {
		return countryId;
	}
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	public String getCountryIso() {
		return countryIso;
	}
	public void setCountryIso(String countryIso) {
		this.countryIso = countryIso;
	}
	public String getStateId() {
		return stateId;
	}
	public void setStateId(String stateId) {
		this.stateId = stateId;
	}
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPreferedLanguageId() {
		return preferedLanguageId;
	}
	public void setPreferedLanguageId(String preferedLanguageId) {
		this.preferedLanguageId = preferedLanguageId;
	}
	public String getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
	}
	public String getIsParent() {
		return isParent;
	}
	public void setIsParent(String isParent) {
		this.isParent = isParent;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getPlayers() {
		return players;
	}
	public void setPlayers(String players) {
		this.players = players;
	}
	public String getCoins() {
		return coins;
	}
	public void setCoins(String coins) {
		this.coins = coins;
	}
	public String[] getAddedPlayers() {
		return AddedPlayers;
	}
	public void setAddedPlayers(String[] addedPlayers) {
		AddedPlayers = addedPlayers;
	}

    public String getEmailSubscription() {
        return emailSubscription;
    }

    public void setEmailSubscription(String emailSubscription) {
        this.emailSubscription = emailSubscription;
    }

    public boolean isFromAccountScreen() {
        return isFromAccountScreen;
    }

    public void setFromAccountScreen(boolean isFromAccountScreen) {
        this.isFromAccountScreen = isFromAccountScreen;
    }

    public ArrayList<UserClasses> getUserClasses() {
        return userClasses;
    }

    public void setUserClasses(ArrayList<UserClasses> userClasses) {
        this.userClasses = userClasses;
    }

    public String getSchoolYear() {
        return schoolYear;
    }

    public void setSchoolYear(String schoolYear) {
        this.schoolYear = schoolYear;
    }

    public String getDefaultGrade() {
        return defaultGrade;
    }

    public void setDefaultGrade(String defaultGrade) {
        this.defaultGrade = defaultGrade;
    }
}
