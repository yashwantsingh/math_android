package com.mathfriendzy.model.registration;

import java.io.Serializable;
import java.util.ArrayList;

public class UserPlayerDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private  String parentUserId ;
	private  String playerid;
	private  String firstname;
	private  String lastname;
	private  String username;
	private  String city;
	private  String coin;
	private  String completelavel;
	private  String grade;
	private  String points;
	protected byte[] profileImage;
	private String imageName;
	
	private  String schoolId;
	private  String schoolName;
	private  String teacherFirstName;
	private  String teacheLastName;
	private  String teacherUserId;
	private  String stateName;
	private  String indexOfAppearance;
	
	//for students account , add on 28 Apr 2014
	private String password;
	
	private String isTutor;
    private String chatId;
    private String chatUserName;
    private String studentIdByTeacher = "";

    //for professional tutoring
    private String allowPaidTutor;
    private String purchasedTime;


    //for my student list
    private boolean isStateChange = false;
    private int inActive = 0;

    //for manage student my student list
    private int tutoringTime = 0;


    //class title change
    private int classId;
    private int rating;
    private String subjects;
    private ArrayList<String> subjectList;


	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public String getParentUserId() {
		return parentUserId;
	}
	public void setParentUserId(String parentUserId) {
		this.parentUserId = parentUserId;
	}
	public String getPlayerid() {
		return playerid;
	}
	public void setPlayerid(String playerid) {
		this.playerid = playerid;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCoin() {
		return coin;
	}
	public void setCoin(String coin) {
		this.coin = coin;
	}
	public String getCompletelavel() {
		return completelavel;
	}
	public void setCompletelavel(String completelavel) {
		this.completelavel = completelavel;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getPoints() {
		return points;
	}
	public void setPoints(String points) {
		this.points = points;
	}
	public byte [] getProfileImage() {
		return profileImage;
	}
	public void setProfileImage(byte [] profileImage) {
		this.profileImage = profileImage;
	}
	public String getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public String getTeacherFirstName() {
		return teacherFirstName;
	}
	public void setTeacherFirstName(String teacherFirstName) {
		this.teacherFirstName = teacherFirstName;
	}
	public String getTeacheLastName() {
		return teacheLastName;
	}
	public void setTeacheLastName(String teacheLastName) {
		this.teacheLastName = teacheLastName;
	}
	public String getTeacherUserId() {
		return teacherUserId;
	}
	public void setTeacherUserId(String teacherUserId) {
		this.teacherUserId = teacherUserId;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getIndexOfAppearance() {
		return indexOfAppearance;
	}
	public void setIndexOfAppearance(String indexOfAppearance) {
		this.indexOfAppearance = indexOfAppearance;
	}

    public String getIsTutor() {
        return isTutor;
    }

    public String getChatId() {
        return chatId;
    }

    public String getChatUserName() {
        return chatUserName;
    }

    public void setIsTutor(String isTutor) {
        this.isTutor = isTutor;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public void setChatUserName(String chatUserName) {
        this.chatUserName = chatUserName;
    }

    public boolean isStateChange() {
        return isStateChange;
    }

    public void setStateChange(boolean isStateChange) {
        this.isStateChange = isStateChange;
    }

    public int getTutoringTime() {
        return tutoringTime;
    }

    public void setTutoringTime(int tutoringTime) {
        this.tutoringTime = tutoringTime;
    }

    public String getStudentIdByTeacher() {
        return studentIdByTeacher;
    }

    public void setStudentIdByTeacher(String studentIdByTeacher) {
        this.studentIdByTeacher = studentIdByTeacher;
    }

    public int getInActive() {
        return inActive;
    }

    public void setInActive(int inActive) {
        this.inActive = inActive;
    }

    public String getAllowPaidTutor() {
        return allowPaidTutor;
    }

    public void setAllowPaidTutor(String allowPaidTutor) {
        this.allowPaidTutor = allowPaidTutor;
    }

    public String getPurchasedTime() {
        return purchasedTime;
    }

    public void setPurchasedTime(String purchasedTime) {
        this.purchasedTime = purchasedTime;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getSubjects() {
        return subjects;
    }

    public void setSubjects(String subjects) {
        this.subjects = subjects;
    }

    public ArrayList<String> getSubjectList() {
        return subjectList;
    }

    public void setSubjectList(ArrayList<String> subjectList) {
        this.subjectList = subjectList;
    }
}
