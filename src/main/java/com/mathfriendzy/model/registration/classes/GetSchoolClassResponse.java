package com.mathfriendzy.model.registration.classes;

import com.mathfriendzy.serveroperation.HttpResponseBase;

import java.util.ArrayList;

/**
 * Created by root on 1/4/16.
 */
public class GetSchoolClassResponse extends HttpResponseBase{
    private String result;
    private ArrayList<UserClasses> userClasseses = null;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public ArrayList<UserClasses> getUserClasseses() {
        return userClasseses;
    }

    public void setUserClasseses(ArrayList<UserClasses> userClasseses) {
        this.userClasseses = userClasseses;
    }
}
