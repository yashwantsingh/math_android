package com.mathfriendzy.model.registration.classes;

import java.util.ArrayList;

/**
 * Created by root on 29/3/16.
 */
public class UserClasses {
    private int grade;
    private ArrayList<ClassWithName> classList;

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public ArrayList<ClassWithName> getClassList() {
        return classList;
    }

    public void setClassList(ArrayList<ClassWithName> classList) {
        this.classList = classList;
    }
}
