package com.mathfriendzy.model.registration.classes;

/**
 * Created by root on 1/4/16.
 */
public class GetSchoolClassesParam {
    private String action;
    private String userId;
    private String schoolYear;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSchoolYear() {
        return schoolYear;
    }

    public void setSchoolYear(String schoolYear) {
        this.schoolYear = schoolYear;
    }
}
