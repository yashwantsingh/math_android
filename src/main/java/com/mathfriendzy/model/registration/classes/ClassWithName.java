package com.mathfriendzy.model.registration.classes;

import com.android.internal.util.Predicate;

import java.io.Serializable;

/**
 * Created by root on 29/3/16.
 */
public class ClassWithName implements Serializable{

    private int classId;
    private String className;
    private String subject;
    private String startDate;
    private int grade;
    private String endDate;
    private int subjectId;

    //For adapter change
    private boolean isSelected = false;

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    /*Predicate<ClassWithName> subjectEquals(final ClassWithName search) {
        return new Predicate<ClassWithName>() {

            public boolean apply(ClassWithName dataPoint) {
                return dataPoint.getSubjectId() == search.getSubjectId();
            }
        };
    }*/
}
