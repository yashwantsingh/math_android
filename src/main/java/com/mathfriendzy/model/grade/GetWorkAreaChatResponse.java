package com.mathfriendzy.model.grade;

import com.mathfriendzy.serveroperation.HttpResponseBase;

public class GetWorkAreaChatResponse extends HttpResponseBase{
	private String result;
	private String data;
	
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
	
}
