package com.mathfriendzy.model.chooseAvtar;

import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;

import com.mathfriendzy.utils.CommonUtils;

public class AvtarServerOperation 
{
	private final String TAG = this.getClass().getSimpleName();
	
	public void saveAvtarOperationOnserver(String userId , String playerId , String avterId , int status)
	{
		String action = "SaveStatusOfAvatar";
		
		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "userId="   + userId  + "&"
				+ "playerId=" + playerId + "&"
				+ "avatarId=" + avterId + "&"
				+ "status=" + status;
		
		//Log.e(TAG, "inside saveAvtarOperationOnserver url " + strUrl);
		
		this.parseAvtarStatusJson(CommonUtils.readFromURL(strUrl));
	}
	
	private void parseAvtarStatusJson(String jsonString)
	{
		//Log.e(TAG, "inside parseAvtarStatusJson jsonString " + jsonString);
	}
	
	/**
	 * This metod save avtar on server
	 * @param userId
	 * @param playerId
	 * @param avtarIds
	 */
	public void saveAvtar(String userId , String playerId , String avtarIds)
	{
		String action = "saveAvatars";
		
		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "userId="   + userId  + "&"
				+ "playerId=" + playerId + "&"
				+ "avatarIds=" + avtarIds;
		
		//Log.e(TAG, "inside saveAvtar url " + strUrl);
		this.parseAvtarStatusJson(CommonUtils.readFromURL(strUrl));		
	}
}
