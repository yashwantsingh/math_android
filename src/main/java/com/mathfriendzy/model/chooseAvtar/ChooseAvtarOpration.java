package com.mathfriendzy.model.chooseAvtar;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mathfriendzy.database.Database;
import com.mathfriendzy.utils.BuildApp;
import com.mathfriendzy.utils.CommonUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.mathfriendzy.utils.ICommonUtils.CHHOSEAVTAR_OPERATION_LOG;
import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;

public class ChooseAvtarOpration 
{
	private SQLiteDatabase dbConn 	= null;
	private final String AVTAR_TABLE_NAME = "avtar";
	private final String AVTAR_ID		  = "id";
	private final String AVTAR_NAME       = "name";
	private final String AVTAR_PRICE	  = "price";
	private final String AVTAR_IMAGE	  = "image";
	
	
	//avtar status table table 
	
	private final String PLAYER_AVTAR_STATUS = "PlayerAvatarStatus";
	
	private String TAG = this.getClass().getSimpleName();
	
	/**
	 * getAvtar from server url
	 * @return
	 */
	public ArrayList<AvtarDTO> getAvtar()
	{
		if(CHHOSEAVTAR_OPERATION_LOG)
			Log.e(TAG, "inside getAvtar()");
		
		ArrayList<AvtarDTO> avtarDataList = new ArrayList<AvtarDTO>();
		String strUrl = COMPLETE_URL + "action=getAvatars";
			avtarDataList = this.parseAvtarJson(CommonUtils.readFromURL(strUrl));
			
		if(CHHOSEAVTAR_OPERATION_LOG)
			Log.e(TAG, "outside getAvtar()");
		
		return avtarDataList;
	}
	
	/**
	 * Parse AvtarJson
	 * @param jsonString
	 * @return
	 */
	private ArrayList<AvtarDTO> parseAvtarJson(String jsonString)
	{
		if(CHHOSEAVTAR_OPERATION_LOG)
			Log.e(TAG, "inside parseAvtarJson()");
		
		ArrayList<AvtarDTO> avtarDataList = new ArrayList<AvtarDTO>();
		AvtarDTO avtarObj = null;
		try 
		{
			JSONObject jObject = new JSONObject(jsonString);
			JSONArray jArray = jObject.getJSONArray("data");
			
			for(int i = 0; i < jArray.length() ; i++)
			{	
				avtarObj = new AvtarDTO();
				JSONObject jObject2 = jArray.getJSONObject(i);
				avtarObj.setId(jObject2.getString("id"));
				avtarObj.setName(jObject2.getString("name"));
				avtarObj.setPrice(jObject2.getString("price"));
				avtarDataList.add(avtarObj);
			}
		}
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
		
		if(CHHOSEAVTAR_OPERATION_LOG)
			Log.e(TAG, "outside parseAvtarJson()");
		
		return avtarDataList;
	}
	
	/**
	 * This method open connection with database
	 * @param context
	 */
	public void openConn(Context context)
	{
		if(CHHOSEAVTAR_OPERATION_LOG)
			Log.e(TAG, "inside openConn()");
		
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
		
		if(CHHOSEAVTAR_OPERATION_LOG)
			Log.e(TAG, "outside openConn()");
	}
	
	/**
	 * This methos close connection with database
	 */
	public void closeConn()
	{
		if(CHHOSEAVTAR_OPERATION_LOG)
			Log.e(TAG, "inside closeConn()");
		
		if(dbConn != null)
			dbConn.close();
		
		if(CHHOSEAVTAR_OPERATION_LOG)
			Log.e(TAG, "outside closeConn()");
	}
	
	/**
	 * Check for avtar table existence
	 * @return
	 */
	public boolean isAvtarTableExist()
	{	
		if(CHHOSEAVTAR_OPERATION_LOG)
			Log.e(TAG, "inside isAvtarTableExist()");
		
		Cursor cursor = dbConn.rawQuery("SELECT * FROM sqlite_master WHERE type = ? AND name = ?", 
	    											new String[] {"table", AVTAR_TABLE_NAME});
	    if(cursor.moveToFirst())
	    {
	    	if(cursor != null)
	    		cursor.close();
	    	return true;
	    }
	    else
	    {
	    	if(cursor != null)
	    		cursor.close();
	    	return false;
	    }
	}
	
	/**
	 * Create avtar table if not exists
	 */
	public void createAvtarTable()
	{
		if(CHHOSEAVTAR_OPERATION_LOG)
			Log.e(TAG, "inside createAvtarTable()");
		
		String query = "create table " + AVTAR_TABLE_NAME + " (" + AVTAR_ID + " text, " + AVTAR_NAME + " text , "
						+ AVTAR_PRICE + " text , " + AVTAR_IMAGE + "  BLOB )";
		
		dbConn.execSQL(query);
		
		if(CHHOSEAVTAR_OPERATION_LOG)
			Log.e(TAG, "outside createAvtarTable()");
	}
	
	/*public void insertIntoAvtarTable(ArrayList<AvtarDTO> avtarDataList)
	{
		if(CHHOSEAVTAR_OPERATION_LOG)
			Log.e(TAG, "inside insertIntoAvtarTable()");
		
		for( int i = 0 ; i < avtarDataList.size() ; i ++ )
		{		
			ContentValues contentValues = new ContentValues();
			contentValues.put(AVTAR_ID, avtarDataList.get(i).getId());
			contentValues.put(AVTAR_NAME, avtarDataList.get(i).getName());
			contentValues.put(AVTAR_PRICE, avtarDataList.get(i).getPrice());
			contentValues.put(AVTAR_IMAGE, avtarDataList.get(i).getImage());
					
			dbConn.insert(AVTAR_TABLE_NAME, null, contentValues);

			if(CHHOSEAVTAR_OPERATION_LOG)
				Log.e(TAG, "outside insertIntoAvtarTable()");
		}
	}*/
	
	/**
	 * Insert into avtar table
	 * @param avtarDataList
	 */
	public void insertAvtar(AvtarDTO avtarDataList)
	{
		if(CHHOSEAVTAR_OPERATION_LOG)
			Log.e(TAG, "inside insertIntoAvtarTable()");
		
		/*for( int i = 0 ; i < avtarDataList.size() ; i ++ )
		{*/	
		//Log.e("Image Name", avtarDataList.getName());
			ContentValues contentValues = new ContentValues();
			contentValues.put(AVTAR_ID, avtarDataList.getId());
			contentValues.put(AVTAR_NAME, avtarDataList.getName());
			contentValues.put(AVTAR_PRICE, avtarDataList.getPrice());
			contentValues.put(AVTAR_IMAGE, avtarDataList.getImage());
					
			dbConn.insert(AVTAR_TABLE_NAME, null, contentValues);

			if(CHHOSEAVTAR_OPERATION_LOG)
				Log.e(TAG, "outside insertIntoAvtarTable()");
		//}
	}
	
	/**
	 * Insert into avtar table
	 * @param avtarDataList
	 *//*
	public void insertAvtar(ArrayList<PurchasedAvtarDto> purchaseAvtarList)
	{
		for(int i = 0 ; i < purchaseAvtarList.size() ; i ++ )
		{
			
		}
	}
	*/
	
	/**
	 * Delete from avtar table
	 */
	public void deleteFromAvtar()
	{
		if(CHHOSEAVTAR_OPERATION_LOG)
			Log.e(TAG, "inside deleteFromAvtar()");
			
		dbConn.delete(AVTAR_TABLE_NAME , null , null);
		
		if(CHHOSEAVTAR_OPERATION_LOG)
			Log.e(TAG, "outside deleteFromAvtar()");
	}
	
	/**
	 * Get Avtar data from database table avtar
	 * @return
	 */
	public ArrayList<AvtarDTO> getAvtarData()
	{
		if(CHHOSEAVTAR_OPERATION_LOG)
			Log.e(TAG, "inside getAvtarData()");
	
		ArrayList<AvtarDTO> avtarDataList = new ArrayList<AvtarDTO>();
		AvtarDTO avtarObj = null;
		String query = " select distinct id,name,price,image from " + AVTAR_TABLE_NAME;
		Cursor cursor = dbConn.rawQuery(query, null);
		while(cursor.moveToNext())
		{
			avtarObj = new AvtarDTO();
			avtarObj.setId(cursor.getString(cursor.getColumnIndex(AVTAR_ID)));
			avtarObj.setName(cursor.getString(cursor.getColumnIndex(AVTAR_NAME)));
            //if(BuildApp.BUILD_APP_TYPE == BuildApp.AMAZON){
            avtarObj.setPrice("0");
            /*}else{
                avtarObj.setPrice(cursor.getString(cursor.getColumnIndex(AVTAR_PRICE)));
            }*/
			avtarObj.setImage(cursor.getBlob(cursor.getColumnIndex(AVTAR_IMAGE)));
			avtarDataList.add(avtarObj);
		}
		
		if(cursor != null)
			cursor.close();
		
		if(CHHOSEAVTAR_OPERATION_LOG)
			Log.e(TAG, "outside getAvtarData()");
		
		return avtarDataList;
	}
	
	/**
	 * Check avtar already exist or not
	 * @param avtarName
	 * @return
	 */
	public boolean isAvtarAlreadyExist(String avtarName){
		boolean isAvtarExist = false;
		String query = " select * from " + AVTAR_TABLE_NAME + " where " + AVTAR_NAME + " = '" + avtarName +"'";
		Cursor cursor = dbConn.rawQuery(query, null);
		if(cursor.moveToNext())
			isAvtarExist = true;
		if(cursor != null)
			cursor.close();
		return isAvtarExist;
	}
	
	/**
	 * Get Avtar Name by image
	 * @param
	 * @return
	 */
	public byte[] getAvtarImageByName(String imageName)
	{
		byte[] imgAvtrar = null;
		String query = "select " + AVTAR_IMAGE + " from " + AVTAR_TABLE_NAME + " where " +
				"" + AVTAR_NAME + " = '" + imageName + "' COLLATE NOCASE" ;
		Cursor cursor = dbConn.rawQuery(query, null);
		
		if(cursor.moveToNext())
		{		
			imgAvtrar = cursor.getBlob(cursor.getColumnIndex(AVTAR_IMAGE));
		}
		if(cursor != null)
			cursor.close();
		return imgAvtrar;
	}

	/**
	 * This method insert the avtar status
	 * @param userId
	 * @param playerId
	 * @param avtarId
	 * @param status
	 */
	public void insertIntoPlayerActarStatus(String userId , String playerId , String avtarId , int status)
	{
		ContentValues contentValues = new ContentValues();
		contentValues.put("USER_ID", userId);
		contentValues.put("PLAYER_ID", playerId);
		contentValues.put("AVATAR_ID", avtarId);
		contentValues.put("STATUS", status);
				
		dbConn.insert(PLAYER_AVTAR_STATUS, null, contentValues);
	}
	
	/**
	 * This method return the status 
	 * @param userId
	 * @param playerId
	 * @return
	 */
	public int getStatus(String userId , String playerId , String avtarId)
	{
		int status = 0;
		String query = " select STATUS from " + PLAYER_AVTAR_STATUS + " where USER_ID = '" + userId + "'" +
						" and PLAYER_ID = '" + playerId + "' and AVATAR_ID = '" + avtarId + "'";
		
		Cursor cursor = dbConn.rawQuery(query, null);
		
		if(cursor.moveToNext())
		{
			status = cursor.getInt(cursor.getColumnIndex("STATUS"));
		}
		
		if(cursor != null)
			cursor.close();
		
		return status;
	}
	

	/**
	 * This method return the status 
	 * @param userId
	 * @param playerId
	 * @return
	 */
	public ArrayList<String> getAvtarIds(String userId , String playerId)
	{
		ArrayList<String> avtaidList = new ArrayList<String>();
		
		String query = " select AVATAR_ID from " + PLAYER_AVTAR_STATUS + " where USER_ID = '" + userId + "'" +
						" and PLAYER_ID = '" + playerId + "'";
		
		Cursor cursor = dbConn.rawQuery(query, null);
		
		while(cursor.moveToNext())
		{
			avtaidList.add(cursor.getString(cursor.getColumnIndex("AVATAR_ID")));
		}
		
		if(cursor != null)
			cursor.close();
		
		return avtaidList;
	}
	
	
	/**
	 * Update table for player total points id and user id
	 * @param userId
	 * @param playerId
	 */
	public void updateplayerAvtarStatusForTempPlayer(String userId, String playerId)
	{
		
		/*String query = "update " + PLAYER_TOTAL_POINTS + " set USER_ID = '" + userId 
				+ "' and PLAYER_ID = '" + playerId + "'" + "  where PLAYER_ID = '" + 0 +"' and " +
				" USER_ID = '" + 0 + "'";
*/
		String where = "USER_ID = '0' and PLAYER_ID = '0'";
		ContentValues cv = new ContentValues();
		cv.put("USER_ID", userId);
		cv.put("PLAYER_ID", playerId);
		
		dbConn.update(PLAYER_AVTAR_STATUS, cv, where, null);
		
		/*Log.e("LearningCenterImpl", "updatePlayerTotalPointsForUserIdandPlayerId query" + query);
		dbConn.execSQL(query);*/
	}
	
}
