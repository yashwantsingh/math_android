package com.mathfriendzy.model.chooseAvtar;

public class AvtarDTO 
{
	private String id 	= null;
	private String name = null;
	private String price = null;
	private byte[] image = null; 
	
	public byte[] getImage() {
		return image;
	}
	public void setImage(byte[] image) {
		this.image = image;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
}
