package com.mathfriendzy.model.learningcenter;

import java.io.Serializable;

public class LearningCenterTransferObj implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int operationSignImg              			= 0;
	private int operationBackgroundImg        			= 0;
	private int LearningCenterMathOperationId   		= 0;
	private int defaultLearningCenterOperation 			= 0;
	
	private int equationsId                     		= 0;
	private int number1                     		    = 0;
	private int number2                     		    = 0;
	private int product                     		    = 0;
	private String operator                		        = null;
	
	private String number1Str                     		= null;
	private String number2Str                     		= null;
	private String productStr                     		= null;
	private String category                             = null;
	
	private boolean isDefaultLearningCenterOperation 	= false;
	private boolean isCorrectIncorrect              	= false;
	private String  LearningCenterOperation    			= null;
	
	//added
	private String mathOperationCategory                = null;
	private int mathOperationCategoryId                 = 0;
	
	//for homework
	private int totalProblem = 0;//added for assign homework
	private boolean isSelected = false;
	private int numberOfProblemSelected;
	
	public String getMathOperationCategory() {
		return mathOperationCategory;
	}

	public void setMathOperationCategory(String mathOperationCategory) {
		this.mathOperationCategory = mathOperationCategory;
	}

	public int getMathOperationCategoryId() {
		return mathOperationCategoryId;
	}

	public void setMathOperationCategoryId(int mathOperationCategoryId) {
		this.mathOperationCategoryId = mathOperationCategoryId;
	}
	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	

	public String getLearningCenterOperation() {
		return LearningCenterOperation;
	}

	public void setLearningCenterOperation(String learningCenterOperation) {
		LearningCenterOperation = learningCenterOperation;
	}

	public int getOperationSignImg() {
		return operationSignImg;
	}

	public void setOperationSignImg(int operationSignImg) {
		this.operationSignImg = operationSignImg;
	}

	public int getOperationBackgroundImg() {
		return operationBackgroundImg;
	}

	public void setOperationBackgroundImg(int operationBackgroundImg) {
		this.operationBackgroundImg = operationBackgroundImg;
	}

	public int getLearningCenterMathOperationId() {
		return LearningCenterMathOperationId;
	}

	public void setLearningCenterMathOperationId(
			int learningCenterMathOperationId) {
		LearningCenterMathOperationId = learningCenterMathOperationId;
	}

	public int getDefaultLearningCenterOperation() {
		return defaultLearningCenterOperation;
	}

	public void setDefaultLearningCenterOperation(
			int defaultLearningCenterOperation) {
		this.defaultLearningCenterOperation = defaultLearningCenterOperation;
	}

	public boolean isDefaultLearningCenterOperation() {
		return isDefaultLearningCenterOperation;
	}

	public void setDefaultLearningCenterOperation(
			boolean isDefaultLearningCenterOperation) {
		this.isDefaultLearningCenterOperation = isDefaultLearningCenterOperation;
	}

	public int getEquationsId() {
		return equationsId;
	}

	public void setEquationsId(int equationsId) {
		this.equationsId = equationsId;
	}

	public int getNumber1() {
		return number1;
	}

	public void setNumber1(int number1) {
		this.number1 = number1;
	}

	public int getNumber2() {
		return number2;
	}

	public void setNumber2(int number2) {
		this.number2 = number2;
	}

	public int getProduct() {
		return product;
	}

	public void setProduct(int product) {
		this.product = product;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public boolean isCorrectIncorrect() {
		return isCorrectIncorrect;
	}

	public void setCorrectIncorrect(boolean isCorrectIncorrect) {
		this.isCorrectIncorrect = isCorrectIncorrect;
	}

	public String getNumber1Str() {
		return number1Str;
	}

	public void setNumber1Str(String number1Str) {
		this.number1Str = number1Str;
	}

	public String getNumber2Str() {
		return number2Str;
	}

	public void setNumber2Str(String number2Str) {
		this.number2Str = number2Str;
	}

	public String getProductStr() {
		return productStr;
	}

	public void setProductStr(String productStr) {
		this.productStr = productStr;
	}

	public int getTotalProblem() {
		return totalProblem;
	}

	public void setTotalProblem(int totalProblem) {
		this.totalProblem = totalProblem;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public int getNumberOfProblemSelected() {
		return numberOfProblemSelected;
	}

	public void setNumberOfProblemSelected(int numberOfProblemSelected) {
		this.numberOfProblemSelected = numberOfProblemSelected;
	}
}
