package com.mathfriendzy.model.learningcenter.addition;

import java.util.ArrayList;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.mathfriendzy.database.Database;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;

public class AdditionImpl 
{
	private SQLiteDatabase dbConn 	  	    = null;
	private Database database               = null;
	
	private String MATH_OPERATIONS 					= "math_operations";
	private String MATH_OPERATION_CATAGORIES        = "math_operations_categories";
	private String MATH_OPERATION_DEFAULT_CATEGORY 	= "math_operations_default_categories";
	
	private String GRADE_LEVEL     					= "grade_levels_id";
	private String OPERATION_ID                     = "math_operations_id";
	private String MATH_OPERATION_CATEGORY_ID 		= "math_operations_categories_id";
	private String MATH_OPRATION_CATEGORY           = "math_operations_category";
	
	private String MATH_EQUATION_TABLE              = "math_equations";
	private String EQUATION_ID                      = "equations_id";
	private String NUM_1                            = "num1";
	private String NUM_2                            = "num2";
	private String OPERATOR                         = "operator";
	private String RESULT                           = "product";
	private String POINT_SUM                        = "points_sum";
	
	/**
	 * Constructor
	 * @param context
	 */
	public AdditionImpl(Context context)
	{
		database = new Database(context);
		database.open();		
	}
	
	/**
	 * This method open connection with database
	 */
	public void openConn()
	{
		dbConn = database.getConnection();
	}
	
	/**
	 * This method close the database connection
	 */
	public void closeConn()
	{
		if(dbConn != null)
			dbConn.close();
	}
	
	
	/**
	 * This method return the math operation category by operation Id
	 * @param operationID
	 * @return
	 */
	public ArrayList<LearningCenterTransferObj> getMathOperationCategoriesById(int operationID)
	{
		LearningCenterTransferObj learningObj = null;
		
		ArrayList<LearningCenterTransferObj> categories = new ArrayList<LearningCenterTransferObj>();
		
		String query = "SELECT " + MATH_OPERATION_CATEGORY_ID  + " , " + MATH_OPRATION_CATEGORY 
						+ " FROM " + MATH_OPERATION_CATAGORIES + 
						" where " + OPERATION_ID + " = '" + operationID + "'"; 
		
		Cursor cursor = dbConn.rawQuery(query, null);
		
		while(cursor.moveToNext())
		{ 
			learningObj = new LearningCenterTransferObj();
			learningObj.setMathOperationCategory(cursor.getString(cursor.getColumnIndex(MATH_OPRATION_CATEGORY)));
			learningObj.setMathOperationCategoryId(cursor.getInt(cursor.getColumnIndex(MATH_OPERATION_CATEGORY_ID)));
			categories.add(learningObj);
		}
		
		if(cursor != null)
			cursor.close();
		return categories;
	}
	
	
	/**
	 * Return the category id's list which is in both table math_operations_default_categories and 
	 * math_operations_categories_id
	 * @param grade
	 * @param operationID
	 * @return
	 */
	public ArrayList<Integer> getDefaultLearningCenterCategoryId(int grade,int operationID)
	{
		ArrayList<Integer> categoryIdList = new ArrayList<Integer>();
		
		String query = "SELECT math_operations_categories_id from math_operations_default_categories " +
						"where grade_levels_id = '" + grade + "'" + " and " +
						"math_operations_categories_id In (Select math_operations_categories_id from " +
						"math_operations_categories where math_operations_id = '" + operationID + "'" + ")";
		
		Cursor cursor = dbConn.rawQuery(query, null);
		while(cursor.moveToNext())
		{
			categoryIdList.add(cursor.getInt(0));
		}
		
		if(cursor != null)
			cursor.close();
		return categoryIdList;
	}
	
	
}
