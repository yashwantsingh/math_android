package com.mathfriendzy.model.learningcenter;

import java.util.Date;

public class MathScoreTransferObj 
{
	private int equationId;
	private int mathOperationID;
	private int lap;
	private Date startDateTime;
	private Date endDateTime;
	private int points;
	private String  answer;
	private int isAnswerCorrect;
	private int timeTakenToAnswer;
	private LearningCenterTransferObj equation;
	private String startDateTimeStr;
	private String endDateTimeStr;
	private String challengerPlayerId;
	
	public int getIsAnswerCorrect() {
		return isAnswerCorrect;
	}
	public void setIsAnswerCorrect(int isAnswerCorrect) {
		this.isAnswerCorrect = isAnswerCorrect;
	}
	public String getChallengerPlayerId() {
		return challengerPlayerId;
	}
	public void setChallengerPlayerId(String challengerPlayerId) {
		this.challengerPlayerId = challengerPlayerId;
	}
	public String getStartDateTimeStr() {
		return startDateTimeStr;
	}
	public void setStartDateTimeStr(String startDateTimeStr) {
		this.startDateTimeStr = startDateTimeStr;
	}
	public String getEndDateTimeStr() {
		return endDateTimeStr;
	}
	public void setEndDateTimeStr(String endDateTimeStr) {
		this.endDateTimeStr = endDateTimeStr;
	}
	public int getEquationId() {
		return equationId;
	}
	public void setEquationId(int equationId) {
		this.equationId = equationId;
	}
	public int getMathOperationID() {
		return mathOperationID;
	}
	public void setMathOperationID(int categoryId) {
		this.mathOperationID = categoryId;
	}
	public int getLap() {
		return lap;
	}
	public void setLap(int lap) {
		this.lap = lap;
	}
	public Date getStartDateTime() {
		return startDateTime;
	}
	public void setStartDateTime(Date startDateTime) {
		this.startDateTime = startDateTime;
	}
	public Date getEndDateTime() {
		return endDateTime;
	}
	public void setEndDateTime(Date endDateTime) {
		this.endDateTime = endDateTime;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public int getAnswerCorrect() {
		return isAnswerCorrect;
	}
	public void setAnswerCorrect(int isAnswerCorrect) {
		this.isAnswerCorrect = isAnswerCorrect;
	}
	public int getTimeTakenToAnswer() {
		return timeTakenToAnswer;
	}
	public void setTimeTakenToAnswer(int timeTakenToAnswer) {
		this.timeTakenToAnswer = timeTakenToAnswer;
	}
	public LearningCenterTransferObj getEquation() {
		return equation;
	}
	public void setEquation(LearningCenterTransferObj equation) {
		this.equation = equation;
	}
}
