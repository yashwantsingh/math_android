package com.mathfriendzy.model.learningcenter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.mathfriendzy.database.Database;
import com.mathfriendzy.utils.BuildApp;

import java.util.ArrayList;

public class LearningCenterimpl 
{
	//private Context context 				= null;
	private SQLiteDatabase dbConn 	  	    = null;
	private Database database               = null;

	//table
	private String MATH_OPERATIONS 					= "math_operations";
	private String MATH_OPERATION_CATAGORIES        = "math_operations_categories";
	private String MATH_OPERATION_DEFAULT_CATEGORY 	= "math_operations_default_categories";
	private String MATH_EQUATION_LEVEL_CATEGORY     = "math_equations_levels_categories";
	private String PLAYER_TOTAL_POINTS              = "PlayerTotalPoints";
	private String PLAYER_EQUATION_LEVEL            = "PlayerEquationLevel";
	private String PURCHASE_ITEMS                   = "PurchasedItems";

	//attributes
	private String GRADE_LEVEL     					= "grade_levels_id";
	private String OPERATION_ID                     = "math_operations_id";
	private String MATH_OPERATION_CATEGORY_ID 		= "math_operations_categories_id";
	private String MATH_OPRATION_CATEGORY           = "math_operations_category";

	private String MATH_EQUATION_TABLE              = "math_equations";
	private String EQUATION_ID                      = "equations_id";
	private String NUM_1                            = "num1";
	private String NUM_2                            = "num2";
	private String OPERATOR                         = "operator";
	private String RESULT                           = "product";
	private String POINT_SUM                        = "points_sum";

	//math result
	private String MATH_RESULT_TABLE                = "Math_Result";

	/**
	 * Constructor
	 * @param context
	 */
	public LearningCenterimpl(Context context)
	{
		database = new Database(context);
		database.open();		
	}

	/**
	 * This method open connection with database
	 */
	public void openConn()
	{
		dbConn = database.getConnection();
	}

	/**
	 * This method close the database conneciton
	 */
	public void closeConn()
	{
		if(dbConn != null)
			dbConn.close();
	}

	/**
	 * This method return the learning center function from the database
	 * @return
	 */
	public ArrayList<LearningCenterTransferObj> getLearningCenterFunctions() 
	{
		ArrayList<LearningCenterTransferObj> learningCenterFunctions = new ArrayList<LearningCenterTransferObj>();
		LearningCenterTransferObj learningCenterObj = null;

		String query = "SELECT math_operation,math_operations_id FROM " + MATH_OPERATIONS ; 
		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{
			learningCenterObj = new LearningCenterTransferObj();
			learningCenterObj.setLearningCenterOperation(cursor.getString(0).replace("/", " "));
			learningCenterObj.setLearningCenterMathOperationId(cursor.getInt(1));
			learningCenterFunctions.add(learningCenterObj);
		}

		if(cursor != null)
			cursor.close();
		return learningCenterFunctions;
	}

	public LearningCenterTransferObj getLearningCenterFunctionsByCatId(String catId){
		LearningCenterTransferObj learningCenterFunctions = new LearningCenterTransferObj();
		String query = "SELECT math_operation,math_operations_id FROM " 
				+ MATH_OPERATIONS +" where math_operations_id=" + catId; 
		Cursor cursor = dbConn.rawQuery(query, null);
		if(cursor.moveToNext()){
			learningCenterFunctions.setLearningCenterOperation(cursor.getString(0).replace("/", " "));
			learningCenterFunctions.setLearningCenterMathOperationId(cursor.getInt(1));
		}
		if(cursor != null)
			cursor.close();
		return learningCenterFunctions;
	}


	/**
	 * This method return the defaultCategoryId By grade
	 * @param gradeLevel
	 * @return
	 */
	public ArrayList<String> getDefaultCatagoryIdByGradeLevel(int gradeLevel)
	{
		if(gradeLevel > 8)
			gradeLevel = 8;
		ArrayList<String> categoryList = new ArrayList<String>();

		String query = "SELECT " + MATH_OPERATION_CATEGORY_ID + " FROM " + MATH_OPERATION_DEFAULT_CATEGORY + 
				" where " + GRADE_LEVEL + " = '" + gradeLevel + "'"; 
		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{ 
			categoryList.add(cursor.getString(cursor.getColumnIndex(MATH_OPERATION_CATEGORY_ID)));
		}

		if(cursor != null)
			cursor.close();
		return categoryList;
	}

	/**
	 * This method return the math operation category by operation Id
	 * @param operationID
	 * @return
	 */
	public ArrayList<LearningCenterTransferObj> getMathOperationCategoriesById(int operationID)
	{
		LearningCenterTransferObj learningObj = null;

		ArrayList<LearningCenterTransferObj> categories = new ArrayList<LearningCenterTransferObj>();

		String query = "SELECT " + MATH_OPERATION_CATEGORY_ID  + " , " + MATH_OPRATION_CATEGORY 
				+ " FROM " + MATH_OPERATION_CATAGORIES + 
				" where " + OPERATION_ID + " = '" + operationID + "'"; 

		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{ 
			learningObj = new LearningCenterTransferObj();
			learningObj.setMathOperationCategory(cursor.getString(cursor.getColumnIndex(MATH_OPRATION_CATEGORY)));
			learningObj.setMathOperationCategoryId(cursor.getInt(cursor.getColumnIndex(MATH_OPERATION_CATEGORY_ID)));
			categories.add(learningObj);
		}

		if(cursor != null)
			cursor.close();
		return categories;
	}

	/**
	 * Return the total problem in the fiven sub cat
	 * @param subCatId
	 * @return
	 */
	public int getTotalProblemInCat(int subCatId){
		int totalProblem = 0;
		String query = "SELECT count(*) as total FROM " + "math_equations_operations_categories"
					+ " WHERE math_operations_categories_id = " + subCatId;
		Cursor cursor = dbConn.rawQuery(query, null);
		if(cursor.moveToNext()){
			totalProblem = cursor.getInt(0);
		}
		if(totalProblem > 100){
			totalProblem = 100;
		}
		if(cursor != null)
			cursor.close();
		return totalProblem;
	}
	
	/**
	 * Return the subCatData by CatId and SubCatid
	 * @param catId
	 * @param subCatId
	 * @return
	 */
	public LearningCenterTransferObj getMathOperationCategoriesByCatIdAndSubCatId
	(String catId, String subCatId){
		LearningCenterTransferObj learningObj = null;
		String query = "SELECT " + MATH_OPERATION_CATEGORY_ID  + " , " + MATH_OPRATION_CATEGORY 
				+ " FROM " + MATH_OPERATION_CATAGORIES + 
				" where " + OPERATION_ID + " = '" 
				+ catId + "' and math_operations_categories_id ='" + subCatId + "'"; 
		Cursor cursor = dbConn.rawQuery(query, null);
		if(cursor.moveToNext()){ 
			learningObj = new LearningCenterTransferObj();
			learningObj.setMathOperationCategory(cursor.getString(cursor.getColumnIndex(MATH_OPRATION_CATEGORY)));
			learningObj.setMathOperationCategoryId(cursor.getInt(cursor.getColumnIndex(MATH_OPERATION_CATEGORY_ID)));
		}

		if(cursor != null)
			cursor.close();
		return learningObj;
	}

	/**
	 * Return the category id's list which is in both table math_operations_default_categories and 
	 * math_operations_categories_id
	 * @param grade
	 * @param operationID
	 * @return
	 */
	public ArrayList<Integer> getDefaultLearningCenterCategoryId(int grade,int operationID)
	{
		if(grade > 8)
			grade = 8;

		ArrayList<Integer> categoryIdList = new ArrayList<Integer>();

		String query = "SELECT math_operations_categories_id from math_operations_default_categories " +
				"where grade_levels_id = '" + grade + "'" + " and " +
				"math_operations_categories_id In (Select math_operations_categories_id from " +
				"math_operations_categories where math_operations_id = '" + operationID + "'" + ")";

		Cursor cursor = dbConn.rawQuery(query, null);
		while(cursor.moveToNext())
		{
			categoryIdList.add(cursor.getInt(0));
		}

		if(cursor != null)
			cursor.close();
		return categoryIdList;
	}

	/**
	 * This method return the equation id by the categories id list
	 * @param catagoryIdList
	 * @return
	 */
	public ArrayList<Integer> getEquationsId(ArrayList<Integer> catagoryIdList)
	{

		ArrayList<Integer> equationsIdList = new ArrayList<Integer>();

		String stringFromArray = "";

		for(  int  i = 0 ; i < catagoryIdList.size() ; i ++)
		{
			if(i == catagoryIdList.size() - 1)
				stringFromArray = stringFromArray + catagoryIdList.get(i);
			else
				stringFromArray = stringFromArray + catagoryIdList.get(i) + ",";
		}

		String query = "SELECT equations_id FROM math_equations_operations_categories " +
				"where math_operations_categories_id in(" + stringFromArray + ")";

		Cursor cursor = dbConn.rawQuery(query, null);
		while(cursor.moveToNext())
		{
			equationsIdList.add(cursor.getInt(0));
		}

		if(cursor != null)
			cursor.close();
		return equationsIdList;
	}

	/**
	 * This method return the equation by the equation id
	 * @param equationId
	 * @return
	 */
	public LearningCenterTransferObj getEquation(int equationId)
	{ 	
		LearningCenterTransferObj learningObj = new LearningCenterTransferObj();

		String query = "select * from " + MATH_EQUATION_TABLE + " where " + EQUATION_ID 
				+  " = '" + equationId + "'";
		//Log.e("Impl ", "Query " + query);
		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{
			learningObj.setEquationsId(cursor.getInt(cursor.getColumnIndex(EQUATION_ID)));
			learningObj.setNumber1Str(cursor.getString(cursor.getColumnIndex(NUM_1)));
			learningObj.setNumber2Str(cursor.getString(cursor.getColumnIndex(NUM_2)));
			learningObj.setOperator(cursor.getString(cursor.getColumnIndex(OPERATOR)));
			learningObj.setProductStr(cursor.getString(cursor.getColumnIndex(RESULT)));
		}

		if(cursor != null)
			cursor.close();
		return learningObj;
	}

	/**
	 * This method insert the data into Math Result Table
	 * @param mathResultTransferObj
	 */
	public void insertIntoMathResult(MathResultTransferObj mathResultTransferObj)
	{
		//Log.e("Learnign Center Impl", "inside insertIntoMathResult");

		ContentValues contentValues = new ContentValues();
		contentValues.put("roundId", mathResultTransferObj.getRoundId());
		contentValues.put("isFirstRound", mathResultTransferObj.getIsFirstRound());
		contentValues.put("gameType", mathResultTransferObj.getGameType());
		contentValues.put("isWin", mathResultTransferObj.getIsWin());
		contentValues.put("userId", mathResultTransferObj.getUserId());
		contentValues.put("playerId", mathResultTransferObj.getPlayerId());
		contentValues.put("isFakePlayer", mathResultTransferObj.getPlayerId());
		contentValues.put("problems", mathResultTransferObj.getProblems());

		dbConn.insert(MATH_RESULT_TABLE, null, contentValues);
	}

	/**
	 * get record from Math_Result table
	 * @return
	 */
	public ArrayList<MathResultTransferObj> getMathResultData()
	{
		ArrayList<MathResultTransferObj> mathResultTransferObjList = new ArrayList<MathResultTransferObj>();
		String query = "select * from " + MATH_RESULT_TABLE;
		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{
			MathResultTransferObj mathobj = new MathResultTransferObj();
			mathobj.setRoundId(cursor.getInt(cursor.getColumnIndex("roundId")));
			mathobj.setIsFirstRound(cursor.getString(cursor.getColumnIndex("isFirstRound")));
			mathobj.setGameType(cursor.getString(cursor.getColumnIndex("gameType")));
			mathobj.setIsWin(cursor.getInt(cursor.getColumnIndex("isWin")));
			mathobj.setUserId(cursor.getString(cursor.getColumnIndex("userId")));
			mathobj.setPlayerId(cursor.getString(cursor.getColumnIndex("playerId")));
			mathobj.setIsFakePlayer(cursor.getInt(cursor.getColumnIndex("isFakePlayer")));
			mathobj.setProblems(cursor.getString(cursor.getColumnIndex("problems")));

			mathResultTransferObjList.add(mathobj);
		}

		if(cursor != null)
			cursor.close();

		return mathResultTransferObjList;
	}

	/**
	 * get record from Math_Result table
	 * @return
	 */
	public ArrayList<MathResultTransferObj> getMathResultData(String userId , String playerId)
	{
		ArrayList<MathResultTransferObj> mathResultTransferObjList = new ArrayList<MathResultTransferObj>();
		String query = "select * from " + MATH_RESULT_TABLE + " where userId = '" + userId 
				+ "' and playerId = '" + playerId + "'";

		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{
			MathResultTransferObj mathobj = new MathResultTransferObj();
			mathobj.setRoundId(cursor.getInt(cursor.getColumnIndex("roundId")));
			mathobj.setIsFirstRound(cursor.getString(cursor.getColumnIndex("isFirstRound")));
			mathobj.setGameType(cursor.getString(cursor.getColumnIndex("gameType")));
			mathobj.setIsWin(cursor.getInt(cursor.getColumnIndex("isWin")));
			mathobj.setUserId(cursor.getString(cursor.getColumnIndex("userId")));
			mathobj.setPlayerId(cursor.getString(cursor.getColumnIndex("playerId")));
			mathobj.setIsFakePlayer(cursor.getInt(cursor.getColumnIndex("isFakePlayer")));
			mathobj.setProblems(cursor.getString(cursor.getColumnIndex("problems")));

			mathResultTransferObjList.add(mathobj);
		}

		if(cursor != null)
			cursor.close();

		return mathResultTransferObjList;
	}

	/**
	 * This method delete the record from Math_result Table
	 * @param
	 */
	public void deleteFromMathResult(String userid , String playerid)
	{
		String where = "userId = '"+ userid + "' and playerId = '" + playerid + "'";
		dbConn.delete(MATH_RESULT_TABLE , where , null);
	}

	/**
	 * This method return the categories list for the learning center level
	 * based on the operation id and level
	 * @param operationId
	 * @param level
	 * @return
	 */
	public ArrayList<Integer> getCategoryIdsFromMathEquationLeveCategory(int operationId , int level)
	{
		ArrayList<Integer> categories = new ArrayList<Integer>();

		String query = "SELECT CATEGORY_ID from math_equations_levels_categories  where EQUATION_ID = " 
				+  operationId + " AND LEVEL = " + level;

		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{
			categories.add(cursor.getInt(cursor.getColumnIndex("CATEGORY_ID")));
		}

		if(cursor != null)
			cursor.close();

		return categories;
	}

	/**
	 * This method return the categoryId,OperationId and equation id for solve equation with timer
	 * @param categoryIdList
	 * @return
	 */
	public ArrayList<MathEquationOperationCategorytransferObj> getMathEquationDataByCategories(ArrayList<Integer> categoryIdList)
	{
		ArrayList<MathEquationOperationCategorytransferObj> mathOperationList = new ArrayList<MathEquationOperationCategorytransferObj>();
		String stringFromArray = "";

		for(  int  i = 0 ; i < categoryIdList.size() ; i ++)
		{
			if(i == categoryIdList.size() - 1)
				stringFromArray = stringFromArray + categoryIdList.get(i);
			else
				stringFromArray = stringFromArray + categoryIdList.get(i) + ",";
		}

		String query = "SELECT category.*,opr_categ.math_operations_id from math_equations_operations_categories as category," +
				"math_operations_categories as opr_categ where category.math_operations_categories_id IN " +
				"("  + stringFromArray + ") and opr_categ.math_operations_categories_id = category.math_operations_categories_id";

		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{
			MathEquationOperationCategorytransferObj mathObj = new MathEquationOperationCategorytransferObj();
			mathObj.setCategoryId(cursor.getInt(cursor.getColumnIndex("math_operations_categories_id")));
			mathObj.setEquationId(cursor.getInt(cursor.getColumnIndex("equations_id")));
			mathObj.setOperationId(cursor.getInt(cursor.getColumnIndex("math_operations_id")));

			mathOperationList.add(mathObj);
		}

		if(cursor != null)
			cursor.close();

		return mathOperationList;
	}


	/**
	 * This method return the category Id list according to the digit type
	 * @param digit
	 * @return
	 */
	public ArrayList<Integer> getDigitType(String digit)
	{
		ArrayList<Integer> categoryList = new ArrayList<Integer>();

		String query = "select math_operations_categories_id from math_operations_categories where math_operations_category like '" 
				+ digit + "'";

		//Log.e("Learning Center impl", "inside get Digit type");

		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{
			categoryList.add(cursor.getInt(cursor.getColumnIndex("math_operations_categories_id")));
		}

		if(cursor != null)
			cursor.close();

		return categoryList;
	}

	/**
	 * This method insert into PlayerTotalPoints
	 * @param playerobj
	 */
	public void insertIntoPlayerTotalPoints(PlayerTotalPointsObj playerobj)
	{		
		ContentValues contentValues = new ContentValues();
		contentValues.put("USER_ID", playerobj.getUserId());
		contentValues.put("PLAYER_ID", playerobj.getPlayerId());
		contentValues.put("TOTAL_POINTS", playerobj.getTotalPoints());
		contentValues.put("COINS", playerobj.getCoins());
		contentValues.put("COMPETE_LEVEL", playerobj.getCompleteLevel());
		contentValues.put("PURCHASED_COINS", playerobj.getPurchaseCoins());

		dbConn.insert(PLAYER_TOTAL_POINTS, null, contentValues);
	}



	/**
	 * This method update the points of total player
	 * @param playerobj
	 */
	public void updatePlayerTotalPoints(PlayerTotalPointsObj playerobj)
	{
		String query = "update " + PLAYER_TOTAL_POINTS + " set TOTAL_POINTS = '" + playerobj.getTotalPoints() 
				+ " where PLAYER_ID = '" + playerobj.getPlayerId() +"'";

		dbConn.execSQL(query);


	}


	/**
	 * This method update the player coins
	 * @param coins
	 * @param userId
	 * @param playerId
	 */
	public void updateCoinsForPlayer(int coins , String userId , String playerId)
	{
		//Log.e("LearningCenterImpl", "insede updateCoinsForPlayer");

		String where = "USER_ID = '"+ userId + "' and PLAYER_ID = '" + playerId + "'";
		ContentValues cv = new ContentValues();
		cv.put("COINS", coins);

		dbConn.update(PLAYER_TOTAL_POINTS, cv, where, null);
	}


	/**
	 * Update table for player equation playerId and user id
	 * @param userId
	 * @param playerId
	 */
	public void updatePlayerEquationTabelForUserIdAndPlayerId(String userId, String playerId)
	{
		//Log.e("Learnign center impl", "inside update fro player equation " + userId + " , " + playerId);

		/*String query = "update " + PLAYER_EQUATION_LEVEL + " set USER_ID = '" + userId 
						+ "' and PLAYER_ID = '" + playerId + "'" + "  where PLAYER_ID = '" + 0 +"' and " +
						" USER_ID = '" + 0 + "'";

		Log.e("LearningCenterImpl", "updatePlayerEquationTabelForUserIdAndPlayerId query" + query);
		dbConn.execSQL(query);*/

		String where = "USER_ID = '0' and PLAYER_ID = '0'";
		ContentValues cv = new ContentValues();
		cv.put("USER_ID", userId);
		cv.put("PLAYER_ID", playerId);

		dbConn.update(PLAYER_EQUATION_LEVEL, cv, where, null);
	}


	/**
	 * Update table for player equation playerId and user id
	 * @param userId
	 * @param playerId
	 */
	public void updateMathResultForUserIdAndPlayerId(String userId, String playerId)
	{
		//Log.e("Learnign center impl", "inside update fro player equation " + userId + " , " + playerId);
		/*String query = "update " + MATH_RESULT_TABLE + " set userId = '" + userId 
						+ "' and playerId = '" + playerId + "'" + "  where playerId = '" + 0 +"' and " +
						" userId = '" + 0 + "'";

		Log.e("LearningCenterImpl", "updateMathResultForUserIdAndPlayerId query" + query);
		dbConn.execSQL(query);*/

		String where = "userId = '0' and playerId = '0'";
		ContentValues cv = new ContentValues();
		cv.put("userId", userId);
		cv.put("playerId", playerId);

		dbConn.update(MATH_RESULT_TABLE, cv, where, null);
	}

	/**
	 * Update table for player total points id and user id
	 * @param userId
	 * @param playerId
	 */
	public void updatePlayerTotalPointsForUserIdandPlayerId(String userId, String playerId)
	{

		/*String query = "update " + PLAYER_TOTAL_POINTS + " set USER_ID = '" + userId 
				+ "' and PLAYER_ID = '" + playerId + "'" + "  where PLAYER_ID = '" + 0 +"' and " +
				" USER_ID = '" + 0 + "'";
		 */
		String where = "USER_ID = '0' and PLAYER_ID = '0'";
		ContentValues cv = new ContentValues();
		cv.put("USER_ID", userId);
		cv.put("PLAYER_ID", playerId);

		dbConn.update(PLAYER_TOTAL_POINTS, cv, where, null);

		/*Log.e("LearningCenterImpl", "updatePlayerTotalPointsForUserIdandPlayerId query" + query);
		dbConn.execSQL(query);*/
	}

	/**
	 * This method insert the data into the player Equation level table
	 * @param playrEquationObj
	 */
	public void insertIntoPlayerEquationLevel(PlayerEquationLevelObj playrEquationObj)
	{
		ContentValues contentValues = new ContentValues();
		contentValues.put("USER_ID", playrEquationObj.getUserId());
		contentValues.put("PLAYER_ID", playrEquationObj.getPlayerId());
		contentValues.put("EQUATION_TYPE", playrEquationObj.getEquationType());
		contentValues.put("LEVEL", playrEquationObj.getLevel());
		contentValues.put("STARS", playrEquationObj.getStars());

		dbConn.insert(PLAYER_EQUATION_LEVEL, null, contentValues);
	}

	/**
	 * This method return true if it exist otherwise return false
	 * @param userid
	 * @param playerId
	 * @param level
	 * @param equationType
	 * @return
	 */
	public boolean isEquationPlayerExist(String userid,String playerId,int level,int equationType)
	{
		boolean isExist = false;
		String query = "select * from " + PLAYER_EQUATION_LEVEL + " where PLAYER_ID = '" +  playerId + "'"
				+ "and  USER_ID = '" + userid + "' and LEVEL = '" + level + "' and EQUATION_TYPE = '"
				+ equationType + "'";


		Cursor cursor = dbConn.rawQuery(query, null);

		if(cursor.moveToNext())
			isExist =  true;
		else 
			isExist =  false; 

		if(cursor != null)
			cursor.close();
		//Log.e("Learnign Center Impl ", "inseide isEquationplayerExist query " + query + " boolean value " + isExist);

		return isExist;
	}


	/**
	 * Update equation player level
	 * @param userid
	 * @param playerId
	 * @param level
	 * @param equationType
	 * @param stars
	 */
	public void updateEquationPlayer(String userid,String playerId,int level,int equationType, int stars)
	{
		String query = "update " + PLAYER_EQUATION_LEVEL + " set STARS = '" + stars +
				"' where PLAYER_ID = '" +  playerId + "'"
				+ "and  USER_ID = '" + userid + "' and LEVEL = '" + level + "' and EQUATION_TYPE = '"
				+ equationType + "'";

		dbConn.execSQL(query);
	}

	/**
	 * This method return the player level data
	 * @param playerId
	 * @return
	 */
	public ArrayList<PlayerEquationLevelObj> getPlayerEquationLevelDataByPlayerId(String playerId)
	{
		ArrayList<PlayerEquationLevelObj> playerDataList = new ArrayList<PlayerEquationLevelObj>();

		String query = "select * from " + PLAYER_EQUATION_LEVEL + " where PLAYER_ID = '" + playerId + "'";

		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{
			PlayerEquationLevelObj playerObj = new PlayerEquationLevelObj();
			playerObj.setUserId(cursor.getString(cursor.getColumnIndex("USER_ID")));
			playerObj.setPlayerId(cursor.getString(cursor.getColumnIndex("PLAYER_ID")));
			playerObj.setEquationType(cursor.getInt(cursor.getColumnIndex("EQUATION_TYPE")));
			playerObj.setLevel(cursor.getInt(cursor.getColumnIndex("LEVEL")));
			playerObj.setStars(cursor.getInt(cursor.getColumnIndex("STARS")));

			playerDataList.add(playerObj);
		}

		if(cursor != null)
			cursor.close();
		return playerDataList;
	}


	/**
	 * This method return true if player exist otherwise return false
	 * @param playerId
	 * @return
	 */
	public boolean isPlayerRecordExits(String playerId)
	{
		boolean isExist = false;
		String query = "select * from " + PLAYER_TOTAL_POINTS + " where PLAYER_ID = " +  playerId;

		Cursor cursor = dbConn.rawQuery(query, null);

		if(cursor.moveToNext())
			isExist =  true;
		else 
			isExist =  false; 

		if(cursor != null)
			cursor.close();
		return isExist;
	}


	public void updatePlayerPoints(int points , String playerId)
	{
		//Log.e("LearningServerOpreration", "inside updatePlayerpoints");
		String query = "update " + PLAYER_TOTAL_POINTS + " set TOTAL_POINTS = '" + points +
				"' where PLAYER_ID = '" + playerId + "'";
		dbConn.execSQL(query);

		/*ContentValues contentValues = new ContentValues();
		contentValues.put("TOTAL_POINTS", points);
		dbConn.update(PLAYER_TOTAL_POINTS, contentValues, "PLAYER_ID", new String[]{playerId+""});*/
	}


	/**
	 * This method update the player total points and complete level for single friendzy
	 * @param points
	 * @param completeLevel
	 * @param playerId
	 */
	public void updatePlayerPointsAndCmpleteLevel(int points , int completeLevel , String playerId)
	{	
		String where = "PLAYER_ID = '" + playerId + "'";
		ContentValues contentValues = new ContentValues();
		contentValues.put("TOTAL_POINTS", points);
		contentValues.put("COMPETE_LEVEL", completeLevel);

		dbConn.update(PLAYER_TOTAL_POINTS, contentValues, where, null);
	}

	/**
	 * This methodb return the player points before playing it
	 * @param playerId
	 */
	public PlayerTotalPointsObj getDataFromPlayerTotalPoints(String playerId)
	{
		PlayerTotalPointsObj playerObj = new PlayerTotalPointsObj();
		String query = "select * from " + PLAYER_TOTAL_POINTS + " where PLAYER_ID = " +  playerId;
		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{
			playerObj.setPlayerId(cursor.getString(cursor.getColumnIndex("PLAYER_ID")));
			playerObj.setTotalPoints(cursor.getInt(cursor.getColumnIndex("TOTAL_POINTS")));
			playerObj.setCoins(cursor.getInt(cursor.getColumnIndex("COINS")));
			playerObj.setCompleteLevel(cursor.getInt(cursor.getColumnIndex("COMPETE_LEVEL")));
			playerObj.setPurchaseCoins(cursor.getInt(cursor.getColumnIndex("PURCHASED_COINS")));
		}

		if(cursor != null)
			cursor.close();

		return playerObj;
	}

	/**
	 * This methodb return the player points before playing it
	 * @param playerId
	 *//*
	public PlayerTotalPointsObj getDataFromPlayerTotalPoints(String playerId , String userId)
	{
		PlayerTotalPointsObj playerObj = new PlayerTotalPointsObj();
		String query = "select * from " + PLAYER_TOTAL_POINTS + " where PLAYER_ID = '" +  playerId + "'"
				+ " and USER_ID = '" + userId + "'";
		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{
			playerObj.setPlayerId(cursor.getString(cursor.getColumnIndex("PLAYER_ID")));
			playerObj.setTotalPoints(cursor.getInt(cursor.getColumnIndex("TOTAL_POINTS")));
			playerObj.setCoins(cursor.getInt(cursor.getColumnIndex("COINS")));
			playerObj.setCompleteLevel(cursor.getInt(cursor.getColumnIndex("COMPETE_LEVEL")));
			playerObj.setPurchaseCoins(cursor.getInt(cursor.getColumnIndex("PURCHASED_COINS")));
		}

		if(cursor != null)
			cursor.close();

		return playerObj;
	}*/

	/**
	 * This method tells the existene of the player points in player total points
	 * @param playerId
	 * @return
	 */
	public boolean isPlayerTotalPointsExist(String playerId)
	{
		boolean isExist = false;
		String query = "select * from " + PLAYER_TOTAL_POINTS + " where PLAYER_ID = " +  playerId;
		Cursor cursor = dbConn.rawQuery(query, null);

		if(cursor.moveToNext())
			isExist = true;
		else
			isExist = false;

		if(cursor != null)
			cursor.close();
		return isExist;
	}

	/**
	 * This method insert the data into purchase table
	 * @param purchserItem
	 */
	public void insertIntoPurchaseItem(ArrayList<PurchaseItemObj> purchserItem)
	{
		for( int i = 0 ; i < purchserItem.size() ; i ++ )
		{
			ContentValues contentValues = new ContentValues();
			contentValues.put("USER_ID", purchserItem.get(i).getUserId());
			contentValues.put("Item_id", purchserItem.get(i).getItemId());
			contentValues.put("Status", purchserItem.get(i).getStatus());

			dbConn.insert(PURCHASE_ITEMS, null, contentValues);
		}
	}


	/**
	 * Update the purchased item for temp player if it become a login user player
	 * @param userId
	 */
	public void updatePurchasedItemTable(String userId)
	{	
		String where = "User_id = '0'";
		ContentValues cv = new ContentValues();
		cv.put("User_id", userId);
		dbConn.update(PURCHASE_ITEMS, cv, where, null);
	}

	/**
	 * this method return the hoghest level
	 * @param playerEquationObj
	 * @return
	 */
	public int getHighetsLevel(PlayerEquationLevelObj playerEquationObj)
	{
		int value = 0;
		String query = "select MAX(LEVEL) from " + PLAYER_EQUATION_LEVEL + " where PLAYER_ID = '" +  playerEquationObj.getPlayerId() 
				+ "'" + "and  USER_ID = '" + playerEquationObj.getUserId() + "' and EQUATION_TYPE = '" + playerEquationObj.getEquationType() + "'";
		Cursor cursor = dbConn.rawQuery(query, null);

		if(cursor.moveToNext())
			value =  cursor.getInt(0);

		if(cursor != null)
			cursor.close();

		return value;
	}


	/**
	 * This method return the purchase data from database
	 * @return
	 */
	public ArrayList<PurchaseItemObj>  getPurchaseItemData()
	{
		ArrayList<PurchaseItemObj> purchaseItemObj = new ArrayList<PurchaseItemObj>();

		String query = "select * from " + PURCHASE_ITEMS;
		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{
			PurchaseItemObj purchaseItem = new PurchaseItemObj();
			purchaseItem.setUserId(cursor.getString(cursor.getColumnIndex("USER_ID")));
			purchaseItem.setItemId(cursor.getInt(cursor.getColumnIndex("Item_id")));
			purchaseItem.setStatus(cursor.getInt(cursor.getColumnIndex("Status")));
			purchaseItemObj.add(purchaseItem);
		}

		if(cursor != null)
			cursor.close();

		return purchaseItemObj;
	}

	/**
	 * This method return the purchase data from database
	 * @return
	 */
	public ArrayList<String>  getPurchaseItemIdsByUserId(String userId)
	{
		ArrayList<String> purchaseItemObj = new ArrayList<String>();

		String query = "select Item_id from " + PURCHASE_ITEMS + " where User_id = '" + userId + "'";
		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{
			purchaseItemObj.add(cursor.getString(cursor.getColumnIndex("Item_id")));
		}

		if(cursor != null)
			cursor.close();

		return purchaseItemObj;
	}

	/**
	 * this method tell the status of the application
	 * @return
	 */
	public int getAppUnlockStatus(int itemId , String userId){

        if(BuildApp.BUILD_APP_TYPE == BuildApp.AMAZON){
            return 1;
        }

		int status = 0;
		String query = "select Status from " + PURCHASE_ITEMS + " where Item_id = '" + itemId + "'"
				+ " and USER_ID = '" + userId + "'";

		Cursor cursor = dbConn.rawQuery(query, null);
		if(cursor.moveToNext())
			status = cursor.getInt(cursor.getColumnIndex("Status"));
		if(cursor != null)
			cursor.close();
		return status;
	}


	/**
	 * This method call when user play learning center without connecting to the
	 * Internet then this method call when stars > 0 , this will insert into 
	 * the localPlayerTable
	 * @param userId
	 * @param playerId
	 * @param stars
	 * @param level
	 */
	public void inserIntoLocalPlayerLevels(String userId , String playerId ,int equtionType , 
			int stars , int level){
		ContentValues contentValues = new ContentValues();
		contentValues.put("USER_ID", userId);
		contentValues.put("PLAYER_ID", playerId);
		contentValues.put("EQUATION_TYPE", equtionType);
		contentValues.put("LEVEL", stars);
		contentValues.put("STARS", level);
		dbConn.insert("LocalPlayerLevels", null, contentValues);			
	}

	/**
	 * This method return true if data exist ptherwise false
	 * @param userId
	 * @param playerId
	 * @return
	 */
	public boolean isPlayerDataExistInLocalPlayerLevels(String userId , String playerId){
		boolean isDataExist = false;
		String query = "select * from LocalPlayerLevels where  USER_ID = '" + userId + "'"
				+ " and PLAYER_ID = '" + playerId + "'";
		Cursor cursor = dbConn.rawQuery(query, null);
		if(cursor.moveToNext()){
			isDataExist = true;
		}

		if(cursor != null)
			cursor.close();
		return isDataExist;
	}

	/**
	 * This method delete the data from LocalPlayerLevels according user id and player id
	 * @param userId
	 * @param playerId
	 */
	public void deleteFromLocalPlayerLevels(String userId , String playerId){
		String where = "USER_ID = '"+ userId + "' and PLAYER_ID = '" + playerId + "'";
		dbConn.delete("LocalPlayerLevels", where, null);
	}

	/**
	 * This method delete the record from Math_result Table
	 * @param
	 */
	public void deleteFromMathResult()
	{
		dbConn.delete(MATH_RESULT_TABLE , null , null);
	}


	/**
	 * This method delete the record from the player total pints based on player id
	 * @param playerId
	 */
	public void deleteFromPlayerTotalPoints(String playerId)
	{
		//Log.e("Learnig Center Impl ", " delete from total points");
		dbConn.delete(PLAYER_TOTAL_POINTS , "PLAYER_ID = ?" , new String[]{playerId});
	}


	/**
	 * This method delete the record from the player total points
	 * Except the temp player record if exist
	 * 
	 *  This method call when a user login , This method delete the record which already exist
	 *  and save current record ,  If temp player exist the delete all record except tepm player record
	 * @param
	 */
	public void deleteFromPlayerTotalPointsExceptTempPlayerData()
	{
		//Log.e("Learnig Center Impl ", " delete from total points");
		dbConn.delete(PLAYER_TOTAL_POINTS , "PLAYER_ID != ? and USER_ID != ?" , new String[]{"0","0"});
	}

	/**
	 * This method delete the record from the player total points
	 */
	public void deleteFromPlayerTotalPoints()
	{
		dbConn.delete(PLAYER_TOTAL_POINTS , null , null);
	}

	/**
	 * This method delete the record from the player total pints based on player id
	 * @param playerId
	 */
	public void deleteFromPlayerEruationLevel(String playerId)
	{
		dbConn.delete(PLAYER_EQUATION_LEVEL , "PLAYER_ID = ?" , new String[]{playerId});
	}

	/**
	 * This method delete the record from Math_result Table
	 * @param
	 */
	public void deleteFromPlayerEruationLevel()
	{
		dbConn.delete(PLAYER_EQUATION_LEVEL , null , null);
	}

	/**
	 * This method delete the record from the player total pints based on player id
	 * @param
	 */
	public void deleteFromPurchaseItem()
	{
		dbConn.delete(PURCHASE_ITEMS , null , null);
	}

	/**
	 * This method delete the record from the player total pints based on player id
	 * @param
	 */
	public void deleteFromPurchaseItem(String userId , int itemId)
	{
		String where = "User_id = '"+ userId + "' and Item_id = '" + itemId + "'";
		dbConn.delete(PURCHASE_ITEMS , where , null);
	}

	/**
	 * This method delete the record from the player total pints based on player id
	 * @param
	 */
	public void deleteFromPurchaseItem(int itemId)
	{
		dbConn.delete(PURCHASE_ITEMS , "Item_id=?" , new String[]{itemId + ""});
	}

	/**
	 * This method delete the record from the player total pints based on player id
	 * @param
	 */
	public void deleteFromPurchaseItem(String userId)
	{
		dbConn.delete(PURCHASE_ITEMS , "User_id=?" , new String[]{userId + ""});
	}
	
	
	//for update points on server saved , when Internet not connected when playing for login user
	
	/**
	 * This method return true if data exist ptherwise false
	 * @param userId
	 * @param playerId
	 * @return
	 */
	public boolean isPlayerDataExistInLocalEarnedScore(String userId , String playerId){
		boolean isDataExist = false;
		String query = "select * from LocalEarnedScore where  user_id = '" + userId + "'"
				+ " and player_id = '" + playerId + "'";
		Cursor cursor = dbConn.rawQuery(query, null);
		if(cursor.moveToNext()){
			isDataExist = true;
		}

		if(cursor != null)
			cursor.close();
		return isDataExist;
	}
	
	/**
	 * This method return true if data exist ptherwise false
	 * @param
	 * @param
	 * @return
	 */
	public ArrayList<MathResultTransferObj> getPlayedDataListFromLocalEarnedScore(){
		ArrayList<MathResultTransferObj> playerList = new  ArrayList<MathResultTransferObj>();
		String query = "select * from LocalEarnedScore";
		Cursor cursor = dbConn.rawQuery(query, null);
		while(cursor.moveToNext()){
			MathResultTransferObj mathResultObj = new MathResultTransferObj();
			mathResultObj.setUserId(cursor.getString(cursor.getColumnIndex("user_id")));
			mathResultObj.setPlayerId(cursor.getString(cursor.getColumnIndex("player_id")));
			mathResultObj.setTotalScore(cursor.getInt(cursor.getColumnIndex("points")));
			mathResultObj.setCoins(cursor.getInt(cursor.getColumnIndex("coins")));
			mathResultObj.setLevel(cursor.getInt(cursor.getColumnIndex("level")));
			playerList.add(mathResultObj);
		}

		if(cursor != null)
			cursor.close();
		return playerList;
	}
	
	/**
	 * Insert the data into LocalEarnedScore Table
	 * @param mathResultObj
	 */
	public void insertIntoLocalEarnedScore(MathResultTransferObj mathResultObj){
		ContentValues contentValues = new ContentValues();
		contentValues.put("user_id", mathResultObj.getUserId());
		contentValues.put("player_id", mathResultObj.getPlayerId());
		contentValues.put("points", mathResultObj.getTotalScore());
		contentValues.put("coins", mathResultObj.getCoins());
		contentValues.put("level", mathResultObj.getLevel());
		dbConn.insert("LocalEarnedScore", null, contentValues);	
	}
	
	/**
	 * This method delete the data from LocalEarnedScore according user id and player id
	 * @param userId
	 * @param playerId
	 */
	public void deleteFromLocalEarnedScore(String userId , String playerId){
		String where = "user_id = '"+ userId + "' and player_id = '" + playerId + "'";
		dbConn.delete("LocalEarnedScore", where, null);
	}
	
	/**
	 * This method delete the data from LocalEarnedScore according user id and player id
	 * @param
	 * @param
	 */
	public void deleteFromLocalEarnedScore(){
		dbConn.delete("LocalEarnedScore", null, null);
	}
	
	/**
	 * Get the data from the LocalEarnedScore by userid and player id
	 * @param userId
	 * @param playerId
	 * @return
	 */
	public MathResultTransferObj getLocalEarnedScoreDataByUserIdAndPlayerId
		(String userId , String playerId){
		MathResultTransferObj mathResultObj = new MathResultTransferObj();

		String query = "select * from LocalEarnedScore where  user_id = '" + userId + "'"
				+ " and player_id = '" + playerId + "'";
		Cursor cursor = dbConn.rawQuery(query, null);
		if(cursor.moveToNext()){
			mathResultObj.setUserId(cursor.getString(cursor.getColumnIndex("user_id")));
			mathResultObj.setPlayerId(cursor.getString(cursor.getColumnIndex("player_id")));
			mathResultObj.setTotalScore(cursor.getInt(cursor.getColumnIndex("points")));
			mathResultObj.setCoins(cursor.getInt(cursor.getColumnIndex("coins")));
			mathResultObj.setLevel(cursor.getInt(cursor.getColumnIndex("level")));
		}

		if(cursor != null)
			cursor.close();

		return mathResultObj;
	}
}
