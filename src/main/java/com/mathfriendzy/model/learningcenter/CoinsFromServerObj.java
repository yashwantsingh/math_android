package com.mathfriendzy.model.learningcenter;

public class CoinsFromServerObj 
{
	private int coinsRequired;
	private int coinsEarned;
	private int coinsPurchase;
	
	private int monthlyCoins;
	private int yearlyCoins;
	
	//added for wordProblem
	private int appUnlock;
	
	public int getAppUnlock() {
		return appUnlock;
	}
	
	public void setAppUnlock(int appUnlock) {
		this.appUnlock = appUnlock;
	}
	public int getMonthlyCoins() {
		return monthlyCoins;
	}
	public void setMonthlyCoins(int monthlyCoins) {
		this.monthlyCoins = monthlyCoins;
	}
	public int getYearlyCoins() {
		return yearlyCoins;
	}
	public void setYearlyCoins(int yearlyCoins) {
		this.yearlyCoins = yearlyCoins;
	}
		 
	public int getCoinsRequired() {
		return coinsRequired;
	}
	public void setCoinsRequired(int coinsRequired) {
		this.coinsRequired = coinsRequired;
	}
	public int getCoinsEarned() {
		return coinsEarned;
	}
	public void setCoinsEarned(int coinsEarned) {
		this.coinsEarned = coinsEarned;
	}
	public int getCoinsPurchase() {
		return coinsPurchase;
	}
	public void setCoinsPurchase(int coinsPurchase) {
		this.coinsPurchase = coinsPurchase;
	}
	
}
