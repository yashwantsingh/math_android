package com.mathfriendzy.model.learningcenter;

import java.util.ArrayList;

public class PlayerDataFromServerObj 
{
	private int points;
	private String items;
	private ArrayList<PlayerEquationLevelObj> playerLevelList;
	private int lockStatus;
	private int completeLevel;
	
	public int getCompleteLevel() {
		return completeLevel;
	}
	public void setCompleteLevel(int completeLevel) {
		this.completeLevel = completeLevel;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	public String getItems() {
		return items;
	}
	public void setItems(String items) {
		this.items = items;
	}
	public ArrayList<PlayerEquationLevelObj> getPlayerLevelList() {
		return playerLevelList;
	}
	public void setPlayerLevelList(ArrayList<PlayerEquationLevelObj> playerLevelList) {
		this.playerLevelList = playerLevelList;
	}
	public int getLockStatus() {
		return lockStatus;
	}
	public void setLockStatus(int lockStatus) {
		this.lockStatus = lockStatus;
	}
	
}
