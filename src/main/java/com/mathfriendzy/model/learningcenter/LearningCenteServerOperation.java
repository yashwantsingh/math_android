package com.mathfriendzy.model.learningcenter;

import android.util.Log;

import com.mathfriendzy.utils.CommonUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;

public class LearningCenteServerOperation 
{
	/**
	 * This method getDetail from server
	 * @param userId
	 * @param playerId
	 */
	public PlayerDataFromServerObj getPlayerDetailFromServer(String userId,String playerId)
	{
		String action = "getPlayerDetails";

		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "user_id="   + userId  + "&"
				+ "player_id=" + playerId;

		//Log.e("LearningCenteServerOperation", "url " + strUrl);

		return this.parseJsonForPlayerData(CommonUtils.readFromURL(strUrl));
	}

	/**
	 * This method parse json String which contain player detail
	 * @param jsonString
	 */
	private PlayerDataFromServerObj parseJsonForPlayerData(String jsonString)
	{
		//Log.e("LearningCenteServerOperation", "jsonString" + jsonString);

		if(jsonString != null){
			PlayerDataFromServerObj playerData = new PlayerDataFromServerObj();

			try 
			{
				JSONObject jObject = new JSONObject(jsonString);
				JSONObject jsonObject2 = jObject.getJSONObject("data");
				playerData.setPoints(jsonObject2.getInt("point"));
				playerData.setItems(jsonObject2.getString("items"));

				ArrayList<PlayerEquationLevelObj> playerList = new ArrayList<PlayerEquationLevelObj>();
				JSONArray jsonArray = jsonObject2.getJSONArray("level");

				for( int i = 0 ; i < jsonArray.length() ; i ++ )
				{
					PlayerEquationLevelObj playerEquationLevel = new PlayerEquationLevelObj();

					JSONObject jsonObject = jsonArray.getJSONObject(i);
					playerEquationLevel.setUserId(jsonObject.getString("uid"));
					playerEquationLevel.setPlayerId(jsonObject.getString("pid"));
					playerEquationLevel.setEquationType(jsonObject.getInt("eqnId"));
					playerEquationLevel.setLevel(jsonObject.getInt("level"));
					playerEquationLevel.setStars(jsonObject.getInt("stars"));

					playerList.add(playerEquationLevel);
				}

				playerData.setPlayerLevelList(playerList);
				playerData.setLockStatus(jsonObject2.getInt("appsUnlock"));

			}
			catch (JSONException e) 
			{
				e.printStackTrace();
			}

			return playerData;
		}else{
			return null;
		}
	}

	/**
	 * This method return the requaired coins and also the earned and purchase coins
	 * @param
	 * @return
	 */
	public CoinsFromServerObj getSubscritiptionInfo(String userId , String playerId)
	{		
		//String action = "getRequiredCoinsForPurchaseItem";
		String action = "getSubscriptionInfoForUser";//changes for required coins

		String strUrl = COMPLETE_URL  + "action=" + action + "&userId=" + userId 
				+ "&playerId=" + playerId;//changes for new popups

		//Log.e("url", strUrl);

		return this.parseCoindPurchasedJsonForSubscription(CommonUtils.readFromURL(strUrl));	
	}

	/**
	 * This method return the requaired coins and also the earned and purchase coins
	 * @param apiValue
	 * @return
	 */
	public CoinsFromServerObj getRequiredCoisForPurchase(String apiValue)
	{		
		//String action = "getRequiredCoinsForPurchaseItem";
		String action = "getNewRequiredCoinsForPurchaseItem";//changes for required coins

		//String strUrl = COMPLETE_URL  + "action=" + action + "&" + apiValue;
		String strUrl = COMPLETE_URL  + "action=" + action + "&" + apiValue 
				+ "&appId=" + CommonUtils.APP_ID;//changes for new popups

		//Log.e("url", strUrl);

		return this.parseCoindPurchasedJson(CommonUtils.readFromURL(strUrl));	
	}

	/**
	 * Parse json for get coins from server
	 * @param jsonString
	 * @return
	 */
	private CoinsFromServerObj parseCoindPurchasedJson(String jsonString) 
	{
		//Log.e("Sever operation for learnign center", "inside parsecoinsPurchase " + jsonString );

		if(jsonString != null){
			CoinsFromServerObj coinsFromServerobj = new CoinsFromServerObj();

			/*try 
		{
			JSONObject jObject = new JSONObject(jsonString);
			JSONObject jsonObject2 = jObject.getJSONObject("data");

			coinsFromServerobj.setCoinsRequired(jsonObject2.getInt("coinsRequired"));
			coinsFromServerobj.setCoinsEarned(jsonObject2.getInt("coinsEarned"));
			coinsFromServerobj.setCoinsPurchase(jsonObject2.getInt("coinsPurchsed"));

		}
		catch (JSONException e) 
		{
			coinsFromServerobj.setCoinsEarned(-1);
			coinsFromServerobj.setCoinsPurchase(-1);
			Log.e("LearningCenteServerOperation", "Do not getting data for temp player");
			//e.printStackTrace();
		}*/

			try 
			{
				JSONObject jObject = new JSONObject(jsonString);
				JSONObject jsonObject2 = jObject.getJSONObject("data");

				coinsFromServerobj.setCoinsRequired(jsonObject2.getInt("coinsRequired"));
				coinsFromServerobj.setCoinsPurchase(jsonObject2.getInt("coinsPurchsed"));
				coinsFromServerobj.setMonthlyCoins(jsonObject2.getInt("monthPrice"));
				coinsFromServerobj.setYearlyCoins(jsonObject2.getInt("yearPrice"));
				try{
					coinsFromServerobj.setCoinsEarned(jsonObject2.getInt("coinsEarned"));
				}
				catch (Exception e) {
					coinsFromServerobj.setCoinsEarned(-1);
				}

			}
			catch (JSONException e) 
			{
				coinsFromServerobj.setCoinsEarned(-1);
				coinsFromServerobj.setCoinsPurchase(0);
				coinsFromServerobj.setMonthlyCoins(0);
				coinsFromServerobj.setYearlyCoins(0);
				//Log.e("LearningCenteServerOperation", "Do not getting data for temp player");
				//e.printStackTrace();
			}
			return coinsFromServerobj;
		}else{
			return null;
		}
	}

	/**
	 * Get subscription data from server
	 * @param jsonString
	 * @return
	 */
	private CoinsFromServerObj parseCoindPurchasedJsonForSubscription(String jsonString) 
	{
		//Log.e("Sever operation for learnign center", "inside parsecoinsPurchase " + jsonString );

		if(jsonString != null){
			CoinsFromServerObj coinsFromServerobj = new CoinsFromServerObj();

			try 
			{
				JSONObject jObject = new JSONObject(jsonString);

				coinsFromServerobj.setCoinsEarned(jObject.getInt("coinsEarned"));
				coinsFromServerobj.setCoinsPurchase(jObject.getInt("coinsPurchsed"));
				coinsFromServerobj.setMonthlyCoins(jObject.getInt("monthPrice"));
				coinsFromServerobj.setYearlyCoins(jObject.getInt("yearPrice"));
				coinsFromServerobj.setAppUnlock(jObject.getInt("appsUnlock"));

			}
			catch (JSONException e) 
			{
				Log.e("Learning", "Error while parsing " + e.toString());
			}

			return coinsFromServerobj;
		}else{
			return null;
		}
	}


	public void addAllLevel(String levelXml)
	{
		String action = "addAllLevels";
		String strUrl = COMPLETE_URL  + "action=" + action + "&" 
				+ "allLevels=" +  levelXml ;

		//Log.e("LearningCenteServerOperation", "strUrl " + strUrl);

		this.parseJson(CommonUtils.readFromURL(strUrl));
	}

	private void parseJson(String jsonString)
	{
		//Log.e("LearningCenteServerOperation", "inside parseJson " + jsonString);
	}

	//code added by shilpi for friendzy chaellenge
	/**
	 * This method return the requaired coins and also the earned and purchase coins
	 * @param apiValue
	 * @return
	 */
	public CoinsFromServerObj getSubscriptionInfoForUser(String apiValue)
	{
		String action = "getSubscriptionInfoForUser";  
		String strUrl = COMPLETE_URL  + "action=" + action + "&" + apiValue + "&appId="+CommonUtils.APP_ID;
		//Log.e("LearningCenterServer", "url : "+ strUrl);  
		return this.parseCoinsSubscription(CommonUtils.readFromURL(strUrl)); 
	}

	private CoinsFromServerObj parseCoinsSubscription(String jsonString) 
	{
		//Log.e("Sever operation for learnign center", "inside parsecoinsPurchase " + jsonString ); 
		if(CommonUtils.LOG_ON)
			Log.e("Sever operation", "inside parsecoinsPurchase " + jsonString );
		
		if(jsonString != null){
			CoinsFromServerObj coinsFromServerobj = new CoinsFromServerObj();

			try 
			{
				JSONObject jObject = new JSONObject(jsonString);   
				int appStatus  = jObject.getInt("appsUnlock");
				coinsFromServerobj.setAppUnlock(appStatus);
				if(appStatus == 1)
				{
					return coinsFromServerobj;
				}
				//coinsFromServerobj.setCoinsRequired(jsonObject2.getInt("coinsRequired"));      
				coinsFromServerobj.setMonthlyCoins(jObject.getInt("monthPrice"));
				coinsFromServerobj.setYearlyCoins(jObject.getInt("yearPrice"));
				try{
					coinsFromServerobj.setCoinsEarned(jObject.getInt("coinsEarned"));
				}
				catch (Exception e) {
					coinsFromServerobj.setCoinsEarned(-1);

				}
				try{
					coinsFromServerobj.setCoinsPurchase(jObject.getInt("coinsPurchsed"));
				}
				catch (Exception e) {
					coinsFromServerobj.setCoinsPurchase(0);

				}
			}
			catch (JSONException e) 
			{
				coinsFromServerobj.setCoinsEarned(-1);
				coinsFromServerobj.setCoinsPurchase(0);
				coinsFromServerobj.setMonthlyCoins(0);
				coinsFromServerobj.setYearlyCoins(0);

				//Log.e("LearningCenteServerOperation", "Do not getting data for temp player");
				//e.printStackTrace();
			}

			return coinsFromServerobj;
		}else{
			return null;
		}
	}

}
