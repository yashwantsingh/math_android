package com.mathfriendzy.model.learningcenter;

public class PlayerEquationLevelObj 
{
	String userId;
	String playerId;
	int equationType;//operationId
	int level;
	int stars;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public int getEquationType() {
		return equationType;
	}
	public void setEquationType(int equationType) {
		this.equationType = equationType;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public int getStars() {
		return stars;
	}
	public void setStars(int stars) {
		this.stars = stars;
	}
}
