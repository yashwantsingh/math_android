package com.mathfriendzy.model.learningcenter;

public class PlayerTotalPointsObj 
{
	String playerId;
	String userId;
	int totalPoints;
	int coins;
	int completeLevel;
	int purchaseCoins;
	
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getTotalPoints() {
		return totalPoints;
	}
	public void setTotalPoints(int totalPoints) {
		this.totalPoints = totalPoints;
	}
	public int getCoins() {
		return coins;
	}
	public void setCoins(int coins) {
		this.coins = coins;
	}
	public int getCompleteLevel() {
		return completeLevel;
	}
	public void setCompleteLevel(int completeLevel) {
		this.completeLevel = completeLevel;
	}
	public int getPurchaseCoins() {
		return purchaseCoins;
	}
	public void setPurchaseCoins(int purchaseCoins) {
		this.purchaseCoins = purchaseCoins;
	}
}
