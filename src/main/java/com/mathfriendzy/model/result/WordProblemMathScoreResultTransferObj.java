package com.mathfriendzy.model.result;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class WordProblemMathScoreResultTransferObj {
	
	private String totalTime;
	private String allPoints;
	
	private ArrayList<WordProblemMathScoreObj> mathScoreList;
	
	//for show result by grade , school curriculum , on 21 May 2014
	private LinkedHashMap<Integer, ArrayList<WordProblemMathScoreObj>> finalMathListWithGradeKey;
	
	public String getTotalTime() {
		return totalTime;
	}
	public void setTotalTime(String totalTime) {
		this.totalTime = totalTime;
	}
	public String getAllPoints() {
		return allPoints;
	}
	public void setAllPoints(String allPoints) {
		this.allPoints = allPoints;
	}
	public ArrayList<WordProblemMathScoreObj> getMathScoreList() {
		return mathScoreList;
	}
	public void setMathScoreList(ArrayList<WordProblemMathScoreObj> mathScoreList) {
		this.mathScoreList = mathScoreList;
	}
	public LinkedHashMap<Integer, ArrayList<WordProblemMathScoreObj>> getFinalMathListWithGradeKey() {
		return finalMathListWithGradeKey;
	}
	public void setFinalMathListWithGradeKey(
			LinkedHashMap<Integer, ArrayList<WordProblemMathScoreObj>> finalMathListWithGradeKey) {
		this.finalMathListWithGradeKey = finalMathListWithGradeKey;
	}
	
	
}
