package com.mathfriendzy.model.result;

import static com.mathfriendzy.utils.ICommonUtils.HOST_NAME;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.mathfriendzy.controller.result.ResultActivity;
import com.mathfriendzy.utils.CommonUtils;

public class GetMathWordProblemScore extends AsyncTask<Void, Void, String>{

	private Context context;
	private int userid ;
	private int playerid ;
	private String playerName;
	private String date ;
	private String mathJsonString ;
	private boolean flag;
	private int playerGrade;
	private int playerPoints;
	private ProgressDialog pg = null;

	//for assessment
	private String imageName;

	GetMathWordProblemScore(int userid , int playerid , String playerName,
			String date , String mathJsonString , boolean flag , Context context, 
			int playerPoints, int playerGrade , String imageName){

		pg = CommonUtils.getProgressDialog(context);
		pg.show();

		this.userid = userid;
		this.playerid = playerid;
		this.playerName = playerName;
		this.date = date;
		this.mathJsonString = mathJsonString;
		this.flag = flag;
		this.context = context;
		this.playerPoints = playerPoints;
		this.playerGrade  = playerGrade;

		//for assessment
		this.imageName = imageName;

		/*UserPlayerOperation userPlayerOpr = new UserPlayerOperation(context);
		UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(playerid + "");
		if(userPlayerData != null && userPlayerData.getGrade().length() > 0)
			playerGrade = Integer.parseInt(userPlayerData.getGrade());
		else
			playerGrade = 1;*/
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
	}

	@Override
	protected String doInBackground(Void... params) {
		return getJsonFile(userid, playerid, date, playerGrade);
	}

	@Override
	protected void onPostExecute(String result) {

		pg.cancel();
		//Log.e("GetMathJson", " inside on post json String " + result);

		if(result != null){
			Intent intent = new Intent(context, ResultActivity.class);		
			intent.putExtra("userId", userid);
			intent.putExtra("playerId", playerid);
			intent.putExtra("playerName" , playerName);	
			intent.putExtra("date", date);
			intent.putExtra("jsonFile", mathJsonString);
			intent.putExtra("wordProblemJson", result);
			intent.putExtra("points", playerPoints);
			intent.putExtra("grade", playerGrade);
			intent.putExtra("flag", flag);
			intent.putExtra("imageName", imageName);
			context.startActivity(intent);
		}else{
			CommonUtils.showInternetDialog(context);
		}

		super.onPostExecute(result);
	}

	private String getJsonFile(int userid , int playerid,String date , int grade)
	{
		String str = "";
		StringBuilder stringBuilder = new StringBuilder("");
		URL url = null;
		try {

			/*url = new URL(HOST_NAME + "LeapAheadMultiFreindzy/index.php?" +
					"action=getMathWordProblemsScore&userId="+userid+"&playerId="+playerid+"&date="+date
					+"&grade=" + grade);*/

			//for show result by grade , school curriculum , on 21 May 2014
			/*userid = 1418;
			playerid = 12987;
			date="2014-01-31";*/

			url = new URL(HOST_NAME + "LeapAheadMultiFreindzy/index.php?" +
					"action=getMathWordProblemsScoreAllGrades&userId="+userid+"&playerId="+playerid+"&date="+date);
			
			BufferedReader ins = new BufferedReader(new InputStreamReader(url.openStream()));				
			try 
			{     
				while((str = ins.readLine()) != null)
				{				
					stringBuilder.append(str);
				}
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

		return stringBuilder.toString();
	}
}
