package com.mathfriendzy.model.result;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import com.mathfriendzy.controller.result.SelectDateActivity;
import com.mathfriendzy.utils.CommonUtils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import static com.mathfriendzy.utils.ICommonUtils.HOST_NAME;

public class JsonAsynTaskForDate extends AsyncTask<Void, Void, String> 
{
	Context context;
	int userid;
	int playerid;
	String playerName;
	int offset;
	int playerpoints = 0;
	int playerGrade  = 0;
	//for assessment
	private String imageName;
	//end changes
	ProgressDialog pd = null;

	public JsonAsynTaskForDate(Context context, int userid,
			int playerid, String playerName, int offset,
			int playerpoints , int playerGrade , String imageName){

		this.context 	= context;
		this.userid 	= userid;
		this.playerid 	= playerid;
		this.playerName = playerName;
		this.offset		= offset;
		this.playerpoints    = playerpoints;
		this.playerGrade     = playerGrade;

		//for assessment
		this.imageName = imageName;
		//end changes

		pd = CommonUtils.getProgressDialog(context);
		pd.show();

	}

	private String getJsonFile()
	{
		String str = "";
		StringBuilder stringBuilder = new StringBuilder("");
		URL url = null;
		try {

			/*url = new URL(HOST_NAME + "LeapAheadMultiFreindzy/index.php?" +
					"action=getMathScoreDates&userId="+userid+"&playerId="+playerid+"&offset="+offset);*/
			//changes for word problem
			url = new URL(HOST_NAME + "LeapAheadMultiFreindzy/index.php?" +
					"action=getAllMathScoreDates&userId="+userid+"&playerId="+playerid+"&offset="+offset);

			BufferedReader ins = new BufferedReader(new InputStreamReader(url.openStream()));				
			try 
			{     
				while((str = ins.readLine()) != null)
				{				
					stringBuilder.append(str);
				}
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return stringBuilder.toString();
	}

	@Override
	protected String doInBackground(Void... params)
	{		
		return getJsonFile();
	}


	@Override
	protected void onPostExecute(String result) 
	{
		pd.cancel();

		if(result != null){
			Intent intent = new Intent(context, SelectDateActivity.class);
			intent.putExtra("jsonFile", result);
			intent.putExtra("userId", userid);
			intent.putExtra("playerId", playerid);
			intent.putExtra("playerName" , playerName);
			intent.putExtra("offset", offset);
			intent.putExtra("points", playerpoints);
			intent.putExtra("grade", playerGrade);
			intent.putExtra("imageName", imageName);

			context.startActivity(intent);
		}else{
			CommonUtils.showInternetDialog(context);
		}

		super.onPostExecute(result);
	}

}
