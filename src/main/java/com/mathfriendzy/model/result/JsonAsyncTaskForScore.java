package com.mathfriendzy.model.result;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import com.mathfriendzy.utils.CommonUtils;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import static com.mathfriendzy.utils.ICommonUtils.HOST_NAME;

public class JsonAsyncTaskForScore extends AsyncTask<Void, Void, String>
{
	private ProgressDialog pg = null;
	Context context;
	int userid;
	int playerid;
	String date;
	String playerName;
	boolean flag;

	int playerPoints;
	int playerGrade;

	//for assessment
	private String imageName;
	public JsonAsyncTaskForScore(Context context, String date, int userid, 
			int playerid, String playerName, boolean flag , int playerPoints , int grade
			,String imageName){

		pg = CommonUtils.getProgressDialog(context);
		pg.show();

		this.context = context;
		this.userid = userid;
		this.playerid = playerid;
		this.date = date;	
		this.playerName	= playerName;
		this.flag		= flag;
		this.playerPoints = playerPoints;
		this.playerGrade  = grade;

		//for assessment
		this.imageName = imageName;
	}


	private String getJsonFile()
	{
		String str = "";
		StringBuilder stringBuilder = new StringBuilder("");
		URL url = null;
		try {

			url = new URL(HOST_NAME + "LeapAheadMultiFreindzy/index.php?" +
					"action=getMathsScore&userId="+userid+"&playerId="+playerid+"&date="+date);

			BufferedReader ins = new BufferedReader(new InputStreamReader(url.openStream()));				
			try 
			{     
				while((str = ins.readLine()) != null)
				{				
					stringBuilder.append(str);
				}
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return stringBuilder.toString();
	}

	@Override
	protected String doInBackground(Void... params)
	{		
		return getJsonFile();
	}


	@Override
	protected void onPostExecute(String result) 
	{
		pg.cancel();

		/*Intent intent = new Intent(context, ResultActivity.class);		
		intent.putExtra("userId", userid);
		intent.putExtra("playerId", playerid);
		intent.putExtra("playerName" , playerName);	
		intent.putExtra("date", date);
		intent.putExtra("jsonFile", result);	
		intent.putExtra("flag", flag);
		context.startActivity(intent);*/
		if(result != null){
			new GetMathWordProblemScore(userid, playerid, playerName, date, result, flag, context
					,playerPoints , playerGrade , imageName)
			.execute(null,null,null);
		}else{
			CommonUtils.showInternetDialog(context);
		}
		super.onPostExecute(result);
	}
}
