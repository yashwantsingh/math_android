package com.mathfriendzy.model.login;

public class PurchasedAvtarDto 
{
	private String userId;
	private String playerId;
	private String acterId;
	private int status;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public String getActerId() {
		return acterId;
	}
	public void setActerId(String acterId) {
		this.acterId = acterId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}
