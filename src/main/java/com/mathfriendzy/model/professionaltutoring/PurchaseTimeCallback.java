package com.mathfriendzy.model.professionaltutoring;

/**
 * Created by root on 10/9/15.
 */
public interface PurchaseTimeCallback {
    void onPurchase(UpdatePurchaseTimeResponse response);
}
