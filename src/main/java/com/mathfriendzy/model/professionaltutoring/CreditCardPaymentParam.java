package com.mathfriendzy.model.professionaltutoring;

/**
 * Created by root on 5/10/15.
 */
public class CreditCardPaymentParam {
    private String customParam;

    public String getCustomParam() {
        return customParam;
    }

    public void setCustomParam(String customParam) {
        this.customParam = customParam;
    }
}
