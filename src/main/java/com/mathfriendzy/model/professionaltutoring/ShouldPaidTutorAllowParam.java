package com.mathfriendzy.model.professionaltutoring;

/**
 * Created by root on 21/10/15.
 */
public class ShouldPaidTutorAllowParam {
    private String action;
    private String uid;
    private String pid;
    private String schoolId;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }
}
