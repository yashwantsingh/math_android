package com.mathfriendzy.model.professionaltutoring;

import com.mathfriendzy.serveroperation.HttpResponseBase;

/**
 * Created by root on 21/10/15.
 */
public class ShouldPaisTutorResponse extends HttpResponseBase{
    private String result;
    private String paidTutorAllow;
    private String studentWithNoSchoolAllowed;
    private String time;
    private String schoolAllowPaidTutor;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getPaidTutorAllow() {
        return paidTutorAllow;
    }

    public void setPaidTutorAllow(String paidTutorAllow) {
        this.paidTutorAllow = paidTutorAllow;
    }

    public String getStudentWithNoSchoolAllowed() {
        return studentWithNoSchoolAllowed;
    }

    public void setStudentWithNoSchoolAllowed(String studentWithNoSchoolAllowed) {
        this.studentWithNoSchoolAllowed = studentWithNoSchoolAllowed;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSchoolAllowPaidTutor() {
        return schoolAllowPaidTutor;
    }

    public void setSchoolAllowPaidTutor(String schoolAllowPaidTutor) {
        this.schoolAllowPaidTutor = schoolAllowPaidTutor;
    }
}
