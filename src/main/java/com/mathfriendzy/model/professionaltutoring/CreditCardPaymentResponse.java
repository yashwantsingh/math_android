package com.mathfriendzy.model.professionaltutoring;

import com.mathfriendzy.serveroperation.HttpResponseBase;

/**
 * Created by root on 5/10/15.
 */
public class CreditCardPaymentResponse extends HttpResponseBase{
    private String result;
    private String message;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
