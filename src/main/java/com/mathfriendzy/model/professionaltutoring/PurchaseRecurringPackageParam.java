package com.mathfriendzy.model.professionaltutoring;

/**
 * Created by root on 30/9/15.
 */
public class PurchaseRecurringPackageParam {

    private String amount;
    private String itemName;
    private String customParam;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getCustomParam() {
        return customParam;
    }

    public void setCustomParam(String customParam) {
        this.customParam = customParam;
    }
}

