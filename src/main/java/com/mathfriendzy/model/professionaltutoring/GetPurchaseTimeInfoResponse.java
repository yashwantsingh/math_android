package com.mathfriendzy.model.professionaltutoring;

import com.mathfriendzy.serveroperation.HttpResponseBase;

import java.io.Serializable;

/**
 * Created by root on 15/9/15.
 */
public class GetPurchaseTimeInfoResponse extends HttpResponseBase implements Serializable{
    String result;
    private int isTimePurchase;
    private int time;
    private String profileId;
    private String packageId;
    private String nextDate;
    private String isCancel;
    private String packageName;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getIsTimePurchase() {
        return isTimePurchase;
    }

    public void setIsTimePurchase(int isTimePurchase) {
        this.isTimePurchase = isTimePurchase;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String getNextDate() {
        return nextDate;
    }

    public void setNextDate(String nextDate) {
        this.nextDate = nextDate;
    }

    public String getIsCancel() {
        return isCancel;
    }

    public void setIsCancel(String isCancel) {
        this.isCancel = isCancel;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
}
