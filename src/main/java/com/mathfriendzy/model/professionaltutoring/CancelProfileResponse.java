package com.mathfriendzy.model.professionaltutoring;

import com.mathfriendzy.serveroperation.HttpResponseBase;

/**
 * Created by root on 28/9/15.
 */
public class CancelProfileResponse extends HttpResponseBase{
    private String response;
    private String error;
    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
