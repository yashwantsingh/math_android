package com.mathfriendzy.model.professionaltutoring;

import com.mathfriendzy.serveroperation.HttpResponseBase;

/**
 * Created by root on 7/9/15.
 */
public class GetPlayerPurchaseTimeResponse extends HttpResponseBase {
    private String result;
    private int time;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }
}
