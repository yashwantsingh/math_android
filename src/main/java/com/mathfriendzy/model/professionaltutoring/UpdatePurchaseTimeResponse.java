package com.mathfriendzy.model.professionaltutoring;

import com.mathfriendzy.serveroperation.HttpResponseBase;

/**
 * Created by root on 10/9/15.
 */
public class UpdatePurchaseTimeResponse extends HttpResponseBase{
    private String result;
    private int time;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }
}
