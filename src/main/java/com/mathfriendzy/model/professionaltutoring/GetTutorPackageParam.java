package com.mathfriendzy.model.professionaltutoring;

/**
 * Created by root on 7/9/15.
 */
public class GetTutorPackageParam {
    private String action;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
