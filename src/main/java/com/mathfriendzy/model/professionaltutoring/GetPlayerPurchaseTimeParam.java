package com.mathfriendzy.model.professionaltutoring;

/**
 * Created by root on 7/9/15.
 */
public class GetPlayerPurchaseTimeParam {

    private String action;
    private String uId;
    private String pId;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getpId() {
        return pId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }
}
