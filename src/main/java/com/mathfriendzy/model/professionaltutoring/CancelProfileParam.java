package com.mathfriendzy.model.professionaltutoring;

/**
 * Created by root on 28/9/15.
 */
public class CancelProfileParam {
    private String profileId;

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }
}
