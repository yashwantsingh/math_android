package com.mathfriendzy.model.professionaltutoring;

import com.mathfriendzy.serveroperation.HttpResponseBase;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by root on 7/9/15.
 */
public class GetPurchagePackagesResponse extends HttpResponseBase implements Serializable{

    public static final String SINGLE_TYPE = "Single";
    public static final String RECURRING_TYPE = "Recurring";

    private String result;
    private int id;
    private int termPrice;
    private int totalPrice;
    private int timePurchased;
    private String description;
    private String type;
    private int quantity = 1;

    private ArrayList<GetPurchagePackagesResponse> singleTypePackageList;
    private ArrayList<GetPurchagePackagesResponse> recurringTypePackageList;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTermPrice() {
        return termPrice;
    }

    public void setTermPrice(int termPrice) {
        this.termPrice = termPrice;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getTimePurchased() {
        return timePurchased;
    }

    public void setTimePurchased(int timePurchased) {
        this.timePurchased = timePurchased;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<GetPurchagePackagesResponse> getSingleTypePackageList() {
        return singleTypePackageList;
    }

    public void setSingleTypePackageList(ArrayList<GetPurchagePackagesResponse> singleTypePackageList) {
        this.singleTypePackageList = singleTypePackageList;
    }

    public ArrayList<GetPurchagePackagesResponse> getRecurringTypePackageList() {
        return recurringTypePackageList;
    }

    public void setRecurringTypePackageList(ArrayList<GetPurchagePackagesResponse> recurringTypePackageList) {
        this.recurringTypePackageList = recurringTypePackageList;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
