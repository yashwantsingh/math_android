package com.mathfriendzy.model.top100;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.mathfriendzy.database.Database;

public class TopListDatabase 
{
	private final String TOP_ID		 		= "ID";
	private final String TOP_LIST	 		= "TOP_LIST";
	private final String TOP_TABLE_NAME 	= "TOP100";

	private final String CREATE_TABLE	 	= "CREATE TABLE "+TOP_TABLE_NAME+"("+TOP_ID+" INTEGER, "
			+TOP_LIST+" TEXT)";

	private final String TAG  = this.getClass().getSimpleName();

	private SQLiteDatabase dbConn = null;

	/**
	 * use to create table Top100 by checking if it exist or not
	 * @param context
	 */
	public void createTableTop100(Context context)
	{
		this.openConn(context); 
		Cursor cursor = dbConn.rawQuery("SELECT * FROM sqlite_master WHERE type = ? AND name = ?", 
				new String[] {"table", TOP_TABLE_NAME});
		if(cursor.moveToNext())
		{  
			cursor.close();
		}
		else
		{	if(cursor != null)
				cursor.close();
			dbConn.execSQL(CREATE_TABLE);
		}
				
		this.closeConn();
	}

	public void deleteFileFromTopTable(Context context, int id)
	{
		openConn(context);
		String where = TOP_ID +" = '"+id+"'";
		dbConn.delete(TOP_TABLE_NAME, where, null);
		closeConn();
	}

	public void insertFileIntoTop100(TopBeans record, Context context)
	{
		openConn(context);

		ContentValues values = new ContentValues();
		values.put(TOP_ID, record.getId());
		values.put(TOP_LIST, record.getFile());

		dbConn.insert(TOP_TABLE_NAME, null, values);

		closeConn();
	}

	public String fetchJsonFile(String id, Context context)
	{
		String jsonFile = null;
		openConn(context);

		String query = "select "+TOP_LIST+" from " +TOP_TABLE_NAME+ " where "+TOP_ID+" = '"+id+"'";
		Cursor cursor = dbConn.rawQuery(query, null);

		if(cursor.moveToNext())
		{		
			jsonFile = cursor.getString(cursor.getColumnIndex(TOP_LIST));
		}
		if(cursor != null)
			cursor.close();

		closeConn();

		return jsonFile;
	}


	/**
	 * This method open the connection with the database
	 * @param context
	 */
	private void openConn(Context context)
	{
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
	}

	/**
	 * This method close the connection with the database
	 */
	private void closeConn()
	{
		if(dbConn != null)
			dbConn.close();
	}
}
