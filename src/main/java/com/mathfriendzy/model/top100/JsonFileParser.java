package com.mathfriendzy.model.top100;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.player.base.Player;
import com.mathfriendzy.model.states.States;
import com.mathfriendzy.utils.CommonUtils;


public class JsonFileParser
{
	Context context;

	public JsonFileParser(Context context)
	{
		this.context = context;
	}


	/**
	 * use to take list of top 100 student teacher and schools
	 * @param id for identify teacher, student and school
	 * @param jsonFile for parsing
	 * @return list of TopBeans
	 */
	public ArrayList<TopBeans> getTop100List(int id, String jsonFile)
	{
        //if(CommonUtils.LOG_ON)
            //Log.e("JsonFileParser" , "top 100 json " + jsonFile);

		ArrayList<TopBeans> list = new ArrayList<TopBeans>();
		try
		{
			JSONArray jsonArray	= null;
			Country country 	= new Country();
			States state		= new States();

			String iso 			= null;
			String stateId		= null;
			String stateName	= null;
			String city			= null;
			String name			= null;
			String sum			= null;
			String school		= null;

			JSONObject jsonObj = new JSONObject(jsonFile);
			jsonObj = jsonObj.getJSONObject("data");			
			if(id == 1)
			{
				jsonArray = jsonObj.getJSONArray("topPlayers");
			}
			else if(id == 2)
			{
				jsonArray = jsonObj.getJSONArray("topTeachers");	
			}
			else if(id == 3)
			{
				jsonArray = jsonObj.getJSONArray("topSchools");	
			}


			for(int i = 0; i < jsonArray.length(); i++)
			{
				TopBeans obj = new TopBeans();
				jsonObj 	= jsonArray.getJSONObject(i);

				if(id == 1)
				{
					name	 = jsonObj.getString("firstName")+" "+jsonObj.getString("lastName");
					sum 	 = jsonObj.getString("sum");
					city	 = jsonObj.getString("city");
					iso		 = jsonObj.getString("countryId");
					stateName= jsonObj.getString("stateId");
					stateId = jsonObj.getString("stateId");					
				} 

				else if(id == 2)
				{
					name	 	= jsonObj.getString("fName")+" "+jsonObj.getString("lName");
					sum 	 	= jsonObj.getString("sum");
					city	 	= jsonObj.getString("city");
					iso			= jsonObj.getString("country");
					stateName	= jsonObj.getString("state");
					stateId 	= jsonObj.getString("state");
					school		= jsonObj.getString("school");
				}
				else if(id == 3)
				{
					name	 	= jsonObj.getString("name");
					sum 	 	= jsonObj.getString("sum");
					city	 	= jsonObj.getString("city");
					iso			= jsonObj.getString("country");
					stateName	= jsonObj.getString("state");
					stateId 	= jsonObj.getString("state");

				} 

				obj.setName(name);
				obj.setSum(sum);				
				obj.setCity(city);
				obj.setSchool(school);
				if(isNumeric(iso)){
					iso = country.getCountryIsoByCountryId(jsonObj.getString("countryId"),context);
				}
				obj.setCountry(iso);				

				if(isNumeric(stateId)){
					if(iso.contains("US")){
						stateName = state.getUSStateCodeById(stateId, context);		
					}else if(iso.contains("CA")){
						stateName = state.getCanadaStateCodeById(stateId, context);
					}
				}
				obj.setState(stateName);

				list.add(obj);
			}

		}
		catch (JSONException e)
		{					
			e.printStackTrace();
		}

		return list;
	}//END getTop100List method



	/**
	 * use to check numeric string.
	 * @param
	 * @return true if numeric otherwise false
	 */
	public static boolean isNumeric(String str)  
	{  
		try  
		{  
			@SuppressWarnings("unused")
			double d = Double.parseDouble(str);  
		}  
		catch(NumberFormatException nfe)  
		{  
			return false;  
		}  
		return true;  
	}//END isNumeric method



	public ArrayList<Score> getMathScore(String jsonFile)
	{
		//Log.e("JsonFile Parser", "Json file " + jsonFile);

		JSONObject jsonObj;
		JSONArray jsonArray	= null;
		ArrayList<Score> scoreList = new ArrayList<Score>();
		try 
		{
			jsonObj = new JSONObject(jsonFile);
			jsonObj = jsonObj.getJSONObject("data");
			jsonArray = jsonObj.getJSONArray("mathsScore");

			for(int i = 0; i < jsonArray.length(); i++)
			{
				Score score = new Score();
				jsonObj 	= jsonArray.getJSONObject(i);
				score.setId(jsonObj.getString("mathOperationId"));
				score.setScore(jsonObj.getString("correctPercentage"));
				score.setTime(jsonObj.getString("time"));

				scoreList.add(score);
			}		

		} 
		catch (JSONException e){}

		return scoreList;		
	}


	public int getTotalTime(String jsonFile)
	{
		JSONObject jsonObj;
		String time		= "0";
		try 
		{
			jsonObj = new JSONObject(jsonFile);
			jsonObj = jsonObj.getJSONObject("data");
			time = jsonObj.getString("totalTime");

		} 
		catch (JSONException e){}

		return Integer.parseInt(time);

	}


	public String getTotalPoints(String jsonFile)
	{
		JSONObject jsonObj;
		String points	= "0";
		try 
		{
			jsonObj = new JSONObject(jsonFile);
			jsonObj = jsonObj.getJSONObject("data");
			points = jsonObj.getString("allPoints");

		} 
		catch (JSONException e){}

		return points;

	}


	public ArrayList<String> getDateList(String jsonFile)
	{
		JSONObject jsonObj;
		JSONArray jsonArray	= null;
		ArrayList<String> dateList = new ArrayList<String>();
		try 
		{
			jsonObj = new JSONObject(jsonFile);
			jsonArray = jsonObj.getJSONArray("data");

			for(int i = 0; i < jsonArray.length(); i++)
			{				
				String date	= jsonArray.getString(i);
				dateList.add(date);
			}		

		} 
		catch (JSONException e){}

		return dateList;		
	}


	/**
	 * use to fetch data of user from json string if there is no data related with user it returns null
	 * @param jsonFile
	 * @param context
	 * @return
	 */
	public static Player getUserDetail(String jsonFile, Context context)
	{
		//Log.e("JsonFileParser", "inside getUserDetail json string " + jsonFile);

		JSONObject jsonObj;
		JSONObject jsonObj1;
		JSONArray jsonArray = null;
		Player user = new Player();
		//ArrayList<Score> scoreList = new ArrayList<Score>();
		try 
		{

			jsonObj1 = new JSONObject(jsonFile);
			jsonArray = jsonObj1.getJSONArray("data");

			for(int i = 0; i < jsonArray.length(); i++)
			{  
				jsonObj  = jsonArray.getJSONObject(i);
				user.setFirstName(jsonObj.getString("fName"));
				user.setLastName(jsonObj.getString("lName"));
				user.setGrade(jsonObj.getInt("grade"));
				user.setPlayerId(jsonObj.getInt("playerId"));
				user.setProfileImageName(jsonObj.getString("profileImageId"));
				user.setCity(jsonObj.getString("city")); 

				user.setSchoolId(jsonObj.getInt("schoolId"));
				user.setSchoolName(jsonObj.getString("schoolName"));
				user.setTeacherUserId(jsonObj.getInt("teacherUserId"));
				user.setTeacherFirstName(jsonObj.getString("teacherFirstName"));
				user.setTeacherLastName(jsonObj.getString("teacherLastName"));
				user.setIndexOfAppearance(jsonObj.getInt("indexOfAppearance"));
				user.setParentUserId(jsonObj.getInt("parentUserId"));
				user.setCompeteLevel(jsonObj.getInt("competeLevel"));
				user.setCoins(jsonObj.getInt("coins"));
				user.setPoints(jsonObj.getInt("points"));
				user.setState(jsonObj.getString("state"));
				user.setUserName(jsonObj.getString("userName"));

			}

			//notification device list
			JSONObject ipodNitificationObj = null;
			JSONArray jsonNotificationArrayForIphone = jsonObj1.getJSONArray("notificationDevices");

			if(jsonNotificationArrayForIphone.length() > 0)
			{
				ipodNitificationObj = jsonNotificationArrayForIphone.getJSONObject(0);
				user.setIponPids(ipodNitificationObj.getString(user.getPlayerId() + ""));
			}

			user.setAndroidPids(jsonObj1.getString("androidPids"));

		} 
		catch (JSONException e)
		{
			Log.e("JsonFileParser", "inside getUserDetail Error while parsing : " + e.toString());
			// e.printStackTrace();
			return null;
		}

		return user;  
	}
}
