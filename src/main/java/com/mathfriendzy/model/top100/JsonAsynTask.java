package com.mathfriendzy.model.top100;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.mathfriendzy.controller.top100.Top100ListActivity;
import com.mathfriendzy.utils.CommonUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import static com.mathfriendzy.utils.ITextIds.JSON_URL;

public class JsonAsynTask extends AsyncTask<Void, Integer, String> 
{
	Context context;
	int id;
	ProgressDialog progressDialog;

	public JsonAsynTask(Context context, int id)
	{
		//Log.e("AsyncTask0", "Inside constructor");
		this.id = id;
		this.context = context;
		progressDialog = CommonUtils.getProgressDialog(context);
		progressDialog.show();
	}


	private String getJsonFile(int id)
	{
		String str = "";
		StringBuilder stringBuilder = new StringBuilder("");
		URL url = null;
		try {
			if(id == 1)
				url = new URL(JSON_URL+"StudentsList");
			if(id == 2)
				url = new URL(JSON_URL+"TeachersList");			
			if(id == 3)
				url = new URL(JSON_URL+"SchoolsList");

			BufferedReader ins = new BufferedReader(new InputStreamReader(url.openStream()));				
			try 
			{     
				while((str = ins.readLine()) != null)
				{				
					stringBuilder.append(str);
				}
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return stringBuilder.toString();
	}



	@Override
	protected String doInBackground(Void... params)
	{	
		return getJsonFile(id);
	}

	@Override
	protected void onPostExecute(String jsonFile)
	{
		progressDialog.cancel();
		if(jsonFile != null){
			Intent intent = new Intent(context, Top100ListActivity.class);
			TopListDatabase db   = new TopListDatabase();  
			TopBeans topObj = new TopBeans();
			db.deleteFileFromTopTable(context, id);  
			topObj.setId(String.valueOf(id));
			topObj.setFile(jsonFile);  
			db.insertFileIntoTop100(topObj, context);
			intent.putExtra("id", id);
			context.startActivity(intent);
            ((Activity)context).finish();
		}else{
			CommonUtils.showInternetDialog(context); 
		}
		super.onPostExecute(jsonFile);
	}
}
