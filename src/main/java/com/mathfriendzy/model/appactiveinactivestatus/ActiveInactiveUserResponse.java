package com.mathfriendzy.model.appactiveinactivestatus;

import com.mathfriendzy.serveroperation.HttpResponseBase;

/**
 * Created by root on 20/5/15.
 */
public class ActiveInactiveUserResponse extends HttpResponseBase{
    private String result;
    private int inActive;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getInActive() {
        return inActive;
    }

    public void setInActive(int inActive) {
        this.inActive = inActive;
    }
}
