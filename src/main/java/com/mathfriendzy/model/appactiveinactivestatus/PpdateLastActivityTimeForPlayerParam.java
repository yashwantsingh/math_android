package com.mathfriendzy.model.appactiveinactivestatus;

/**
 * Created by root on 20/5/15.
 */
public class PpdateLastActivityTimeForPlayerParam {
    private String action;
    private String userId;
    private String playerId;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }
}
