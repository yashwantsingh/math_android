package com.mathfriendzy.model.inapp;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;

import android.util.Log;

public class InAppServerOperation 
{
	private final String TAG = this.getClass().getSimpleName();

	/**
	 * This method get coins pack from server
	 */
	public  ArrayList<CoinsPacktransferObj> getCoinsPack()
	{
		String action = "getCoinsPacks";

		String strUrl = ICommonUtils.COMPLETE_URL + "action=" + action;

		//Log.e(TAG, "inside getCoinsPack url : " + strUrl);

		return this.parseCoinsPackJsonFromServer(CommonUtils.readFromURL(strUrl));
	}

	private ArrayList<CoinsPacktransferObj>  parseCoinsPackJsonFromServer(String jsonString)
	{
		//Log.e(TAG,  " inside parseCoinsPackJsonFromServer jsonString " + jsonString );

		if(jsonString != null){
			ArrayList<CoinsPacktransferObj> coinsPackList = new ArrayList<CoinsPacktransferObj>();

			try
			{
				JSONObject jsonObj = new JSONObject(jsonString);

				JSONArray coinsPackArray = jsonObj.getJSONArray("data");

				for(int i = 0 ; i < coinsPackArray.length() ; i ++ )
				{
					JSONObject coinsPackObj = coinsPackArray.getJSONObject(i);

					CoinsPacktransferObj coinsPack = new CoinsPacktransferObj();
					coinsPack.setId(coinsPackObj.getString("id"));
					coinsPack.setName(coinsPackObj.getString("name"));
					coinsPack.setCoins(coinsPackObj.getString("coins"));
					coinsPack.setType(coinsPackObj.getString("type"));
					coinsPack.setPrice(coinsPackObj.getString("price"));

					coinsPackList.add(coinsPack);
				}

			}
			catch (JSONException e) 
			{
				Log.e(TAG, "Error while parsing coins from server : error " + e.toString() );
			}

			return coinsPackList;
		}else{
			return null;
		}
	}

	/**
	 * This method save coins on server
	 * @param userId
	 * @param coins
	 */
	public void saveCoinsForUser(String userId , int coins)
	{
		String action = "saveCoinsForUser";

		String strUrl = ICommonUtils.COMPLETE_URL + "action=" + action + "&"
				+ "userId=" + userId + "&"
				+ "coins=" + coins;
		//Log.e(TAG, "inside saveCoinsForUser url : " + strUrl);
		this.parseSaveCoinsOnServerJsonString(CommonUtils.readFromURL(strUrl));
	}

	/**
	 * This method save coins on server
	 * @param userId
	 * @param coins
	 */
	public void saveCoinsForPlayer(String userId , String playerId , int coins)
	{
		String action = "saveCoinsForPlayer";

		String strUrl = ICommonUtils.COMPLETE_URL + "action=" + action + "&"
				+ "user_id=" + userId + "&"
				+ "player_id=" + playerId + "&"
				+ "playerCoins=" + coins;
		//Log.e(TAG, "inside saveCoinsForUser url : " + strUrl);

		this.parseSaveCoinsOnServerJsonString(CommonUtils.readFromURL(strUrl));
	}

	/**
	 * This method parse the json String
	 * @param jsonString
	 */
	private void parseSaveCoinsOnServerJsonString(String jsonString)
	{
		//Log.e(TAG, "inside parseSaveCoinsOnServerJsonString json String "  + jsonString);
	}
}
