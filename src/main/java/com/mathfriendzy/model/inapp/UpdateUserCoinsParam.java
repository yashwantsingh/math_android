package com.mathfriendzy.model.inapp;

/**
 * Created by root on 25/1/16.
 */
public class UpdateUserCoinsParam {
    private String action;
    private String userId;
    private int coins;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getCoins() {
        return coins;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }
}
