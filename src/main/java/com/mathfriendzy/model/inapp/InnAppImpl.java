package com.mathfriendzy.model.inapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mathfriendzy.database.Database;

public class InnAppImpl 
{
	//private Context context 				= null;
	private SQLiteDatabase dbConn 	  	    = null;
	private Database database               = null;
		
	/**
	 * Constructor
	 * @param context
	 */
	public InnAppImpl(Context context)
	{
		database = new Database(context);
		database.open();		
	}
	
	/**
	 * This method open connection with database
	 */
	public void openConn()
	{
		dbConn = database.getConnection();
	}
	
	/**
	 * This method close the database conneciton
	 */
	public void closeConn()
	{
		if(dbConn != null)
			dbConn.close();
	}
	
	public int isLiked(String userId , String playerId)
	{
		int isLike = 0;
		String query = "SELECT IS_LIKED from UserFreeCoins  where USER_ID = '" + userId 
						+ "' and PLAYER_ID = '" + playerId + "'"; 
		Cursor cursor = dbConn.rawQuery(query, null);
		
		if(cursor.moveToNext())
		{
			isLike = cursor.getInt(cursor.getColumnIndex("IS_LIKED"));
		}
		else
		{
			isLike = 0;
		}
		
		if(cursor != null)
			cursor.close();
		
		return isLike;
	}
	
	public void insertIntoUserFreeCoins(String userId , String playerId , int isLiked , int isRated)
	{
		//Log.e("InnAppImpl", "inside insertIntoUserFreeCoins");
		ContentValues contentValues = new ContentValues();
		contentValues.put("USER_ID", userId);
		contentValues.put("PLAYER_ID", playerId);
		contentValues.put("IS_LIKED", isLiked);
		contentValues.put("IS_RATED", isRated);
				
		dbConn.insert("UserFreeCoins", null, contentValues);
	}
}
