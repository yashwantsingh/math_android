package com.mathfriendzy.quickblox.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.core.QBEntityCallback;

public interface MyChatCallBack {
	void showMessage(QBChatMessage chatMessage);
	@SuppressWarnings("rawtypes")
	void joinSuccessFull(QBEntityCallback callback);
	@SuppressWarnings("rawtypes")
	void onError(List list);
	void stillJoiningGroupChat();
	void joinUnsuccessFull();
	void showProcessImage(QBChatMessage message);
    void onlineUsers(ArrayList<Integer> dialogOccupents , Collection<Integer> onlineGroupUsers );
}
