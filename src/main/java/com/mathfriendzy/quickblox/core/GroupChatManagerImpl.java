package com.mathfriendzy.quickblox.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.muc.DiscussionHistory;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.mathfriendzy.utils.CommonUtils;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBGroupChat;
import com.quickblox.chat.QBGroupChatManager;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBMessageListenerImpl;
import com.quickblox.chat.listeners.QBParticipantListener;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.chat.model.QBPresence;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBEntityCallbackImpl;

public class GroupChatManagerImpl extends QBMessageListenerImpl<QBGroupChat> implements ChatManager {

    private static final String TAG = "GroupChatManagerImpl";

    private QBGroupChatManager groupChatManager;
    private QBGroupChat groupChat;

    //changes by me
    private Context context;
    private MyChatCallBack myCallback = null;
    //private QBParticipantListener participantListener;
    private QBDialog dialog = null;

    private Handler onlineUserHandlers = new Handler();
    private final int GET_ONLINE_USERS_TIMER = 1000;

    public GroupChatManagerImpl(Context context , MyChatCallBack myCallback) {
        this.context = context;
        this.myCallback = myCallback;
        groupChatManager = QBChatService.getInstance().getGroupChatManager();
        //this.quickBloxIntializeParticipants();
    }

    @SuppressWarnings("rawtypes")
    public void joinGroupChat(QBDialog dialog, QBEntityCallback callback){
        this.dialog = dialog;
        groupChat = groupChatManager.createGroupChat(dialog.getRoomJid());
        join(groupChat, callback);
    }

    public QBGroupChatManager getChatManager(){
        return groupChatManager;
    }

    public QBDialog getJoinDialog(){
        return dialog;
    }

    @SuppressWarnings("rawtypes")
    private void join(final QBGroupChat groupChat, final QBEntityCallback callback) {
        DiscussionHistory history = new DiscussionHistory();
        history.setMaxStanzas(0);

        groupChat.join(history, new QBEntityCallbackImpl() {
            @Override
            public void onSuccess() {

                if(CommonUtils.LOG_ON)
                    Log.e("TAG" ,"join chat success");

                groupChat.addMessageListener(GroupChatManagerImpl.this);
                startGetOnlineUsersHandlers();
                //groupChat.addParticipantListener(participantListener);
                ((Activity)context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        callback.onSuccess();
                        myCallback.joinSuccessFull(callback);
                    }
                });
            }

            @Override
            public void onError(final List list) {
                ((Activity)context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        callback.onError(list);
                        myCallback.onError(list);
                    }
                });
            }
        });
    }

    @Override
    public void release() throws XMPPException {
        if (groupChat != null) {
            try {
                groupChat.leave();
            } catch (SmackException.
                    NotConnectedException nce){
                nce.printStackTrace();
            }
            groupChat.removeMessageListener(this);
            this.releaseGetOnlineUsersHandler();
        }
    }


    @Override
    public void sendMessage(QBChatMessage message) throws XMPPException, SmackException.NotConnectedException {
        if (groupChat != null) {
            try {
                groupChat.sendMessage(message);
            } catch (SmackException.NotConnectedException nce){
                nce.printStackTrace();
            } catch (IllegalStateException e){
                e.printStackTrace();
                myCallback.stillJoiningGroupChat();
            }
        } else {
            myCallback.joinUnsuccessFull();
        }
    }

    @Override
    public void processMessage(QBGroupChat groupChat, final QBChatMessage chatMessage) {
        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                myCallback.showMessage(chatMessage);
            }
        });
    }

    @Override
    public void processError(QBGroupChat groupChat, QBChatException error, QBChatMessage originMessage){

    }

    /**
     * Initialize participants
     */
    /*private void quickBloxIntializeParticipants() {
        if(participantListener == null) {
            participantListener = new QBParticipantListener() {
                @Override
                public void processPresence(QBGroupChat qbGroupChat,
                                            QBPresence qbPresence) {
                    try {
                        ArrayList<Integer> dialogOccupents = dialog.getOccupants();
                        Collection<Integer> onlineGroupUsers = qbGroupChat.getOnlineUsers();
                        onlineUsers(dialogOccupents , onlineGroupUsers);
                    } catch (XMPPException e) {
                        e.printStackTrace();
                    } catch(Exception e){
                        e.printStackTrace();
                    }
                }
            };
        }
    }*/


    private void startGetOnlineUsersHandlers(){
        this.releaseGetOnlineUsersHandler();
        onlineUserHandlers.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    ArrayList<Integer> dialogOccupents = dialog.getOccupants();
                    Collection<Integer> onlineGroupUsers = groupChat.getOnlineUsers();
                    onlineUsers(dialogOccupents , onlineGroupUsers);
                } catch (XMPPException e) {
                    //onlineUsers(null , null);
                    e.printStackTrace();
                } catch(Exception e){
                    //onlineUsers(null , null);
                    e.printStackTrace();
                }
                onlineUserHandlers.postDelayed(this , GET_ONLINE_USERS_TIMER);
            }
        } , GET_ONLINE_USERS_TIMER);
    }

    public void releaseGetOnlineUsersHandler(){
        if(onlineUserHandlers != null)
            onlineUserHandlers.removeCallbacksAndMessages(null);
    }

    private void onlineUsers(final ArrayList<Integer> dialogOccupents ,
                             final Collection<Integer> onlineGroupUsers){
        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                myCallback.onlineUsers(dialogOccupents , onlineGroupUsers);
            }
        });
    }
}
