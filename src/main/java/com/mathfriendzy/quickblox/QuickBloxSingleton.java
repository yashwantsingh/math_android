package com.mathfriendzy.quickblox;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.quickblox.chat.model.QBDialog;
import com.quickblox.users.model.QBUser;

public class QuickBloxSingleton {

	private static QuickBloxSingleton quicBloxSingleton;

	private QBUser currentUser;
	private Map<Integer, QBUser> dialogsUsers = new HashMap<Integer, QBUser>();

	public static QuickBloxSingleton getInstance(){
		if(quicBloxSingleton == null)
			quicBloxSingleton = new QuickBloxSingleton();
		return quicBloxSingleton;
	}

	public QBUser getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(QBUser currentUser) {
		this.currentUser = currentUser;
	}

	public Map<Integer, QBUser> getDialogsUsers() {
		return dialogsUsers;
	}

	public void setDialogsUsers(List<QBUser> setUsers) {
		dialogsUsers.clear();

		for (QBUser user : setUsers) {
			dialogsUsers.put(user.getId(), user);
		}
	}

	public void addDialogsUsers(List<QBUser> newUsers) {
		for (QBUser user : newUsers) {
			dialogsUsers.put(user.getId(), user);
		}
	}

	public Integer getOpponentIDForPrivateDialog(QBDialog dialog){
		Integer opponentID = -1;
		for(Integer userID : dialog.getOccupants()){
			if(!userID.equals(getCurrentUser().getId())){
				opponentID = userID;
				break;
			}
		}
		return opponentID;
	}
}
