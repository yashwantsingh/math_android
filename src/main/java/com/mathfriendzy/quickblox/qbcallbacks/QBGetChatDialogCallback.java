package com.mathfriendzy.quickblox.qbcallbacks;

import com.quickblox.chat.model.QBDialog;

/**
 * Created by root on 25/3/15.
 */
public interface QBGetChatDialogCallback {
    void getDialogSuccess(QBDialog dialog);
}
