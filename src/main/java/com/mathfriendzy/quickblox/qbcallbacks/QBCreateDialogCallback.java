package com.mathfriendzy.quickblox.qbcallbacks;

import android.os.Bundle;

import com.quickblox.chat.model.QBDialog;

/**
 * Created by root on 26/3/15.
 */
public interface QBCreateDialogCallback {

    void onSuccess(QBDialog dialog, Bundle args);
}
