package com.mathfriendzy.quickblox.qbcallbacks;

import android.os.Bundle;

import com.quickblox.auth.model.QBSession;
import com.quickblox.users.model.QBUser;

/**
 * Created by root on 24/3/15.
 */
public interface QBCreateSessionCallback {
    public void onSuccess(QBSession session, Bundle args , QBUser user);
}
