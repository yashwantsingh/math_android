package com.mathfriendzy.quickblox;

import java.util.Collection;
import java.util.List;

import org.jivesoftware.smack.SmackException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.mathfriendzy.listener.OnRequestComplete;
import com.mathfriendzy.model.tutor.RegisterUserOnQBCallback;
import com.mathfriendzy.model.tutor.RegisterUserToQB;
import com.mathfriendzy.quickblox.qbcallbacks.QBCreateSessionCallback;
import com.mathfriendzy.quickblox.qbcallbacks.QBOnLoginSuccess;
import com.mathfriendzy.utils.CommonUtils;
import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBSession;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBRoster;
import com.quickblox.chat.listeners.QBRosterListener;
import com.quickblox.chat.listeners.QBSubscriptionListener;
import com.quickblox.chat.model.QBPresence;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.QBSettings;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

public class QBCreateSession {

    static final int AUTO_PRESENCE_INTERVAL_IN_SECONDS = 30;
	/*private static final String APP_ID = "18403";
	private static final String AUTH_KEY = "BmJZpjrd8ycYw7q";
	private static final String AUTH_SECRET = "ZKABaNzf2UmsTCQ";
	private static final String USER_LOGIN = "sanjeev";
	private static final String USER_PASSWORD = "sanjeev123";*/

    //for ios
    private static final String APP_ID = "16818";
    private static final String AUTH_KEY = "8ADyzF5xRnrpQNw";
    private static final String AUTH_SECRET = "XatZxJnDDtRUdhS";

    private Context context = null;
    private QBChatService chatService;
    private QuickBloxSingleton quickBloxSingleton = null;
    public static QBRoster chatRoster = null;

    private final String TAG = this.getClass().getSimpleName();
    private static QBCreateSession qbSession = null;

    public static QBCreateSession getInstance(Context context){
        if(qbSession == null)
            qbSession = new QBCreateSession(context);
        return qbSession;
    }


    public QBCreateSession(Context context){
        this.context = context;
        quickBloxSingleton = QuickBloxSingleton.getInstance();
        //MathFriendzyHelper.initializeProcessDialog(context);
    }

    public void createSession(String userName , String pass ,
                              final QBCreateSessionCallback callback){
        //MathFriendzyHelper.showDialog();
        QBChatService.setDebugEnabled(false);
        QBSettings.getInstance().fastConfigInit(APP_ID, AUTH_KEY, AUTH_SECRET);
        if (!QBChatService.isInitialized()) {
            QBChatService.init(context);
        }

        chatService = QBChatService.getInstance();
        chatService.setReconnectionAllowed(true);

        final QBUser user = new QBUser();
        user.setLogin(userName);
        user.setPassword(pass);
        QBAuth.createSession(user, new QBEntityCallbackImpl<QBSession>(){
            @Override
            public void onSuccess(QBSession session, Bundle args) {
                user.setId(session.getUserId());
                quickBloxSingleton.setCurrentUser(user);
                callback.onSuccess(session , args , user);
                //MathFriendzyHelper.dismissDialog();
                //loginToChat(user);
            }

            @Override
            public void onError(List<String> errors) {
                Log.e(TAG , "loginUserToQuickBlox createSession success inside error");
                AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                dialog.setMessage("create session errors: " + errors).create().show();
            }
        });
    }

    private void createSession(final QBCreateSessionCallback callback){
        QBChatService.setDebugEnabled(false);
        QBSettings.getInstance().fastConfigInit(APP_ID, AUTH_KEY, AUTH_SECRET);
        if (!QBChatService.isInitialized()) {
            QBChatService.init(context);
        }
        chatService = QBChatService.getInstance();
        chatService.setReconnectionAllowed(true);

        QBAuth.createSession(new QBEntityCallbackImpl<QBSession>() {
            @Override
            public void onSuccess(final QBSession session, Bundle params) {
                callback.onSuccess(session , params , null);
            }
        });
    }

    public void registerUserOnQuickBlox(final RegisterUserToQB newQBUser ,
                                        final RegisterUserOnQBCallback callback) {
        //MathFriendzyHelper.showDialog();
        this.createSession(new QBCreateSessionCallback() {
            @Override
            public void onSuccess(final QBSession session, Bundle args, QBUser user) {
                final QBUser newqbUser = getQBUserToRegister(newQBUser);
                //showToast("before sign up " + newqbUser.getLogin() + " " + newqbUser.getPassword());
                QBUsers.signUp(newqbUser, new QBEntityCallbackImpl<QBUser>() {
                    @Override
                    public void onSuccess(final QBUser qbUser, Bundle bundle) {
                        //MathFriendzyHelper.dismissDialog();
                        //onRegisterSuccess(session, newqbUser, bundle, callback);
                        callback.onSuccess(qbUser , bundle);
                    }
                });
            }
        });
    }

    public void registerUserOnQuickBloxWithSignIn(final RegisterUserToQB newQBUser ,
                                                  final RegisterUserOnQBCallback callback) {
        this.createSession(new QBCreateSessionCallback() {
            @Override
            public void onSuccess(final QBSession session, Bundle args, QBUser user) {
                final QBUser newqbUser = getQBUserToRegister(newQBUser);
                QBUsers.signUp(newqbUser, new QBEntityCallbackImpl<QBUser>() {
                    @Override
                    public void onSuccess(final QBUser qbUser, Bundle bundle) {
                        onRegisterSuccess(session, newqbUser, bundle, callback);
                    }
                });
            }
        });
    }

    /*private void showToast(String mesasage){
        Log.e(TAG , mesasage);
        MathFriendzyHelper.showToast(context , mesasage);
    }*/

    private void onRegisterSuccess(final QBSession session , final QBUser qbUser,
                                   final Bundle bundle ,
                                   final RegisterUserOnQBCallback callback){
        final QBUser loginuser = new QBUser(qbUser.getLogin(), qbUser.getPassword());
        QBUsers.signIn(loginuser, new QBEntityCallbackImpl<QBUser>() {
            @Override
            public void onSuccess(QBUser user, Bundle params) {
                loginuser.setId(session.getUserId());
                quickBloxSingleton.setCurrentUser(loginuser);
                callback.onSuccess(user , bundle);
            }

            @Override
            public void onError(List<String> errors) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                dialog.setMessage("login errors: " + errors).create().show();
            }
        });
    }



    private QBUser getQBUserToRegister(RegisterUserToQB newQBUser){
        QBUser qbUser = new QBUser();
        qbUser.setLogin(newQBUser.getLogin());
        qbUser.setPassword(newQBUser.getPass());
        qbUser.setFullName(newQBUser.getFullUserName());
        return qbUser;
    }

   /* public void loginToChat(QBSession session){

    }*/

    /**
     * Login to chat
     * @param user
     */
    @SuppressWarnings("rawtypes")
    public void loginToChat(final QBUser user , final QBOnLoginSuccess callback){
        //MathFriendzyHelper.showDialog();
        try {
            chatService.login(user, new QBEntityCallbackImpl() {
                @Override
                public void onSuccess() {

                    try {
                        chatService.startAutoSendPresence(AUTO_PRESENCE_INTERVAL_IN_SECONDS);
                    } catch (SmackException.NotLoggedInException e) {
                        e.printStackTrace();
                    }

                    ((Activity) context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            chatRoster = chatService.getRoster
                                    (QBRoster.SubscriptionMode.mutual, subscriptionListener);
                            chatRoster.addRosterListener(rosterListener);

                            QBPresence presence = new QBPresence(QBPresence.Type.online,
                                    "I'm at home",
                                    1, QBPresence.Mode.available);
                            try {
                                chatRoster.sendPresence(presence);
                            } catch (SmackException.NotConnectedException e) {
                                e.printStackTrace();
                            }
                            //MathFriendzyHelper.dismissDialog();
                            callback.onloginSuccess();
                            //new QBDialogs(context).getChatDialogs();
                        }
                    });
                }

                @Override
                public void onError(List errors) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                    dialog.setMessage("chat login errors: " + errors).create().show();
                }
            });
        }catch(Exception e){
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setMessage("chat login errors exception: " + e.toString()).create().show();
        }
    }

    @SuppressWarnings("rawtypes")
    public void logoutFromChat(){
        try {
            if (chatService != null) {
                chatService.logout(new QBEntityCallbackImpl() {
                    @Override
                    public void onSuccess() {
                        chatService.destroy();
                        chatService = null;
                    }

                    @Override
                    public void onError(final List list) {

                    }
                });
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    public void logoutFromChat(final OnRequestComplete callback){
        try {
            if (chatService != null) {
                chatService.logout(new QBEntityCallbackImpl() {
                    @Override
                    public void onSuccess() {
                        if(CommonUtils.LOG_ON)
                            Log.e(TAG , "logout success ");
                        chatService.destroy();
                        chatService = null;
                        callback.onComplete();
                    }

                    @Override
                    public void onError(final List list) {
                        if(CommonUtils.LOG_ON)
                            Log.e(TAG , "logout success onError");
                        if (chatService != null)
                            chatService.destroy();
                        chatService = null;
                        callback.onComplete();
                    }
                });
            }else{
                if(CommonUtils.LOG_ON)
                    Log.e(TAG , "logout success else");
                if (chatService != null)
                    chatService.destroy();
                chatService = null;
                callback.onComplete();
            }
        }catch(Exception e){
            e.printStackTrace();
            if(CommonUtils.LOG_ON)
                Log.e(TAG , "logout success exception");
            if (chatService != null)
                chatService.destroy();
            chatService = null;
            callback.onComplete();
        }
    }

    /**
     * Delete Session
     */
    public void deleteSession(final OnRequestComplete callback){

        try {
            new AsyncTask<Void , Void , Void>(){
                @Override
                protected Void doInBackground(Void... params) {
                    try {
                        QBAuth.deleteSession();
                    } catch (QBResponseException e) {
                        if(CommonUtils.LOG_ON)
                            Log.e(TAG , "deleteSession QBResponseException");
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    if(CommonUtils.LOG_ON)
                        Log.e(TAG , "deleteSession Success");
                    callback.onComplete();
                    super.onPostExecute(aVoid);
                }
            }.execute();
        } catch(Exception e){
            if(CommonUtils.LOG_ON)
                Log.e(TAG , "deleteSession Exception");
            callback.onComplete();
            e.printStackTrace();
        }
    }

    /**
     * Check for login
     * @return
     */
    public boolean isLogIn(){
        if(chatService != null)
            return true;
        return false;
    }

    QBRosterListener rosterListener = new QBRosterListener() {
        @Override
        public void entriesDeleted(Collection<Integer> userIds) {
            Log.e(TAG, "entriesDeleted");
        }

        @Override
        public void entriesAdded(Collection<Integer> userIds) {
            Log.e(TAG, "entriesAdded");
        }

        @Override
        public void entriesUpdated(Collection<Integer> userIds) {
            Log.e(TAG, "entriesUpdated");
        }

        @Override
        public void presenceChanged(QBPresence presence) {
            Log.e(TAG, "presenceChanged");
        }
    };

    QBSubscriptionListener subscriptionListener = new QBSubscriptionListener() {
        @Override
        public void subscriptionRequested(int userId) {
            Log.e(TAG, "subscriptionRequested");
        }
    };
}
