package com.mathfriendzy.quickblox;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.CheckedOutputStream;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.quickblox.qbcallbacks.QBCreateDialogCallback;
import com.mathfriendzy.quickblox.qbcallbacks.QBGetChatDialogCallback;
import com.mathfriendzy.utils.CommonUtils;
import com.quickblox.chat.QBChat;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.request.QBPagedRequestBuilder;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

public class QBDialogs {

    private Context context = null;
    private QBRequestGetBuilder customObjectRequestBuilder = null;

    private final String TAG = this.getClass().getSimpleName();

    public QBDialogs(Context context){
        this.context = context;
        customObjectRequestBuilder = new QBRequestGetBuilder();
        customObjectRequestBuilder.setPagesLimit(100);
        //MathFriendzyHelper.initializeProcessDialog(context);
    }

    public void getChatDialogs(){
        //MathFriendzyHelper.showDialog();

        QBChatService.getChatDialogs(null, customObjectRequestBuilder,
                new QBEntityCallbackImpl<ArrayList<QBDialog>>() {
                    @Override
                    public void onSuccess(final ArrayList<QBDialog> dialogs, Bundle args) {

                        // collect all occupants ids
                        //
                        List<Integer> usersIDs = new ArrayList<Integer>();
                        for(QBDialog dialog : dialogs){
                            usersIDs.addAll(dialog.getOccupants());
                        }

                        // Get all occupants info
                        //
                        QBPagedRequestBuilder requestBuilder = new QBPagedRequestBuilder();
                        requestBuilder.setPage(1);
                        requestBuilder.setPerPage(usersIDs.size());
                        //
                        QBUsers.getUsersByIDs(usersIDs, requestBuilder, new QBEntityCallbackImpl<ArrayList<QBUser>>() {
                            @Override
                            public void onSuccess(ArrayList<QBUser> users, Bundle params) {
                                QuickBloxSingleton.getInstance().setDialogsUsers(users);
                                buildDialogs(dialogs);
                            }

                            @Override
                            public void onError(List<String> errors) {
                                AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                                dialog.setMessage("get occupants errors: " + errors).create().show();
                            }

                        });
                    }

                    @Override
                    public void onError(List<String> errors) {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                        dialog.setMessage("get dialogs errors: " + errors).create().show();
                    }
                });
    }

    /**
     * Get Chat dialog by dialog id
     * @param dialogId
     * @param callback
     */
    public void getChatDialogById(final String dialogId , final QBGetChatDialogCallback callback){

        if(CommonUtils.LOG_ON)
            Log.e(TAG , "inside getChatDialogById() " + dialogId);

       customObjectRequestBuilder.addParameter("_id" , dialogId);
       QBChatService.getChatDialogs(null, customObjectRequestBuilder,
       //QBChatService.getChatDialogs(null, null,
                new QBEntityCallbackImpl<ArrayList<QBDialog>>() {
                    @Override
                    public void onSuccess(final ArrayList<QBDialog> dialogs, Bundle args) {

                        if(CommonUtils.LOG_ON)
                            Log.e(TAG , "inside getChatDialogById() onSuccess() " + dialogs.size());

                        for(QBDialog dialog : dialogs){
                            if(dialogId.equalsIgnoreCase(dialog.getDialogId())){
                                callback.getDialogSuccess(dialog);
                                return ;
                            }
                        }
                        callback.getDialogSuccess(null);
                    }

                    @Override
                    public void onError(List<String> errors) {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                        dialog.setMessage("get dialog errors: " + errors).create().show();
                    }
                });
    }

    public void buildDialogs(ArrayList<QBDialog> dialogs){
        MathFriendzyHelper.dismissDialog();

        for(int i = 0 ; i < dialogs.size() ; i ++ ){
            QBDialog dialog = dialogs.get(i);
            if(dialog.getType().equals(QBDialogType.GROUP)){
                if(CommonUtils.LOG_ON)
                    Log.e(TAG, "group name " + dialog.getName());
            }else{
                // get opponent name for private dialog
                //
                Integer opponentID = QuickBloxSingleton.getInstance().getOpponentIDForPrivateDialog(dialog);
                QBUser user = QuickBloxSingleton.getInstance().getDialogsUsers().get(opponentID);
                if(user != null){
                    if(CommonUtils.LOG_ON)
                        Log.e(TAG, "group name " + user.getLogin() == null ? user.getFullName() : user.getLogin());
                }
                //checkForStatus(opponentID);
            }
        }
    }


    /**
     * Create the dialog
     * @param dialogName
     * @param occupentsIds
     * @param callback
     */
    public void createDialog(String dialogName , ArrayList<Integer> occupentsIds
            , final QBCreateDialogCallback callback){
        QBDialog dialogToCreate = new QBDialog();
        dialogToCreate.setName(dialogName);
        dialogToCreate.setType(QBDialogType.GROUP);
        dialogToCreate.setOccupantsIds(occupentsIds);
        QBChatService.getInstance().getGroupChatManager().createDialog(dialogToCreate,

                new QBEntityCallbackImpl<QBDialog>() {
                    @Override
                    public void onSuccess(QBDialog dialog, Bundle args) {
                        callback.onSuccess(dialog , args);
                    }

                    @Override
                    public void onError(List<String> errors) {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                        dialog.setMessage("dialog creation errors: " + errors).create().show();
                    }
                });
    }
}
