package com.mathfriendzy.customview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class CustomeViewPager extends ViewPager{

	public CustomeViewPager(Context context) {
		super(context);
	}

	public CustomeViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	float mStartDragX;
	OnSwipeOutListener mListener;


	public void setOnSwipeOutListener(OnSwipeOutListener listener) {
		mListener = listener;
	}


	/*@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		float x = ev.getX();
		switch (ev.getAction()) {
		case MotionEvent.ACTION_DOWN:
			mStartDragX = x;
			break;
		case MotionEvent.ACTION_MOVE:
			if (mStartDragX < x && getCurrentItem() == 0) {
				mListener.onSwipeOutAtStart();
			} else if (mStartDragX > x && getCurrentItem() == getAdapter().getCount() - 1) {
				mListener.onSwipeOutAtEnd();
			}
			break;
		}
		return super.onInterceptTouchEvent(ev);
	}*/

	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent(MotionEvent ev){
	    if(getCurrentItem()==getAdapter().getCount()-1){
	        final int action = ev.getAction();
	        float x = ev.getX();
	        switch(action & MotionEventCompat.ACTION_MASK){
	        case MotionEvent.ACTION_DOWN:
	            mStartDragX = x;
	            break;
	        case MotionEvent.ACTION_MOVE:
	            break;
	        case MotionEvent.ACTION_UP:
	            if (x<mStartDragX){
	                mListener.onSwipeOutAtEnd();
	            }else{
	                mStartDragX = 0;
	            }
	            break;
	        }
	    }else{
	        mStartDragX=0;
	    }
	    return super.onTouchEvent(ev);
	}    
	
	public interface OnSwipeOutListener {
		public void onSwipeOutAtStart();
		public void onSwipeOutAtEnd();
	}
}
