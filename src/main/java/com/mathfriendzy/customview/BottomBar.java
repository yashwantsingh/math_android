package com.mathfriendzy.customview;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.assessmenttest.report.ReportForPracticeAndAssessment;
import com.mathfriendzy.controller.information.InformationActivity;
import com.mathfriendzy.controller.moreapp.ActMoreApp;
import com.mathfriendzy.controller.player.CreateTeacherPlayerActivity;
import com.mathfriendzy.controller.player.CreateTempPlayerActivity;
import com.mathfriendzy.controller.player.LoginUserCreatePlayer;
import com.mathfriendzy.controller.player.LoginUserPlayerActivity;
import com.mathfriendzy.controller.player.PlayersActivity;
import com.mathfriendzy.controller.player.TeacherPlayer;
import com.mathfriendzy.controller.registration.ModifyRegistration;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.TeacherFunctionDialogListener;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.login.Login;
import com.mathfriendzy.model.player.temp.TempPlayerOperation;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.MathVersions;

import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;

/**
 * This class Creates bottom bar, Which is used in whole Applicaition
 * @author Yashwant Singh
 *
 */
@SuppressLint("NewApi")
public class BottomBar extends LinearLayout implements OnClickListener
{
    Context context;
    public static final int idBtnCustomAccount 		 = 9001;
    public static final int idBtnCustomMoreGreatApps = 9002;
    public static final int idBtnCustomInfo   		 = 9003;
    public static final int idBtnCustomPlayer 		 = 9004;

    private boolean isClickOnAccount = false;

    public BottomBar(Context context)
    {
        super(context);
        this.context = context;
        final LinearLayout.LayoutParams paramsLayout = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        this.setOrientation(LinearLayout.VERTICAL);
        this.setLayoutParams(paramsLayout);

    }// END TiltleBar( , )

    public BottomBar(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        // TODO Auto-generated constructor stub
    }


    // Constructor which called in starting
    public BottomBar(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        this.context = context;
        this.setOrientation(LinearLayout.HORIZONTAL);


        //*************Add Relative Layout For Account Dynamically********************
        RelativeLayout relativeLayoutAccount = new RelativeLayout(context);
        final LinearLayout.LayoutParams paramsRelativeLayoutAccount =
                new LinearLayout.LayoutParams( 0, LinearLayout.LayoutParams.MATCH_PARENT);
        paramsRelativeLayoutAccount.weight = 1.0f;
        this.addView(relativeLayoutAccount , paramsRelativeLayoutAccount);

        // Add Account button view dynamically
        Button btnAccount = new Button(context);
        btnAccount.setId(idBtnCustomAccount);
        btnAccount.setOnClickListener(this);
        // Set properties for dynamically generated button
        final RelativeLayout.LayoutParams paramsBtnAccount = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);

        paramsBtnAccount.addRule(RelativeLayout.CENTER_HORIZONTAL);
        paramsBtnAccount.addRule(RelativeLayout.CENTER_VERTICAL);
        btnAccount.setBackgroundResource(R.drawable.setting);
        relativeLayoutAccount.addView(btnAccount, paramsBtnAccount);

        //*************Add Relative Layout For More Great App Dynamically********************
        RelativeLayout relativeLayoutMoreGreatApp = new RelativeLayout(context);
        final LinearLayout.LayoutParams paramsRelativeLayoutMoreGreatApp =
                new LinearLayout.LayoutParams( 0, LinearLayout.LayoutParams.MATCH_PARENT);
        paramsRelativeLayoutMoreGreatApp.weight = 1.0f;
        this.addView(relativeLayoutMoreGreatApp , paramsRelativeLayoutMoreGreatApp);

        // Add More Great App  button view dynamically
        Button btnMoreGreatApp = new Button(context);
        btnMoreGreatApp.setId(idBtnCustomMoreGreatApps);
        btnMoreGreatApp.setOnClickListener(this);
        // Set properties for dynamically generated button
        final RelativeLayout.LayoutParams paramsBtnMoreGreatApp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        paramsBtnMoreGreatApp.addRule(RelativeLayout.CENTER_HORIZONTAL);
        paramsBtnMoreGreatApp.addRule(RelativeLayout.CENTER_VERTICAL);
        btnMoreGreatApp.setTextColor(Color.WHITE);
        btnMoreGreatApp.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        btnMoreGreatApp.setTypeface(null, Typeface.BOLD);
        btnMoreGreatApp.setText("+");
        btnMoreGreatApp.setBackgroundResource(R.drawable.btn_background_green);
        relativeLayoutMoreGreatApp.addView(btnMoreGreatApp, paramsBtnMoreGreatApp);

        //*************Add Relative Layout For Information Dynamically********************
        RelativeLayout relativeLayoutInformation = new RelativeLayout(context);
        final LinearLayout.LayoutParams paramsRelativeLayoutInformation =
                new LinearLayout.LayoutParams( 0, LinearLayout.LayoutParams.MATCH_PARENT);
        paramsRelativeLayoutInformation.weight = 1.0f;
        this.addView(relativeLayoutInformation , paramsRelativeLayoutInformation);

        // Add Information  button view dynamically
        Button btnInformation = new Button(context);
        btnInformation.setId(idBtnCustomInfo);
        btnInformation.setOnClickListener(this);
        // Set properties for dynamically generated button
        final RelativeLayout.LayoutParams paramsBtnInformation = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        paramsBtnInformation.addRule(RelativeLayout.CENTER_HORIZONTAL);
        paramsBtnInformation.addRule(RelativeLayout.CENTER_VERTICAL);
        btnInformation.setTextColor(Color.WHITE);
        btnInformation.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        btnInformation.setTypeface(null, Typeface.BOLD);
        btnInformation.setText("i");
        btnInformation.setBackgroundResource(R.drawable.btn_background_green);
        relativeLayoutInformation.addView(btnInformation, paramsBtnInformation);

        //*************Add Relative Layout For Player Dynamically********************
        RelativeLayout relativeLayoutPlayer = new RelativeLayout(context);
        final LinearLayout.LayoutParams paramsRelativeLayoutPlayer =
                new LinearLayout.LayoutParams( 0, LinearLayout.LayoutParams.MATCH_PARENT);
        paramsRelativeLayoutPlayer.weight = 1.0f;
        this.addView(relativeLayoutPlayer , paramsRelativeLayoutPlayer);

        // Add Player  button view dynamically
        Button btnPlayer = new Button(context);
        btnPlayer.setId(idBtnCustomPlayer);
        btnPlayer.setOnClickListener(this);
        // Set properties for dynamically generated button
        final RelativeLayout.LayoutParams paramsBtnPlayer = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        paramsBtnPlayer.addRule(RelativeLayout.CENTER_HORIZONTAL);
        paramsBtnPlayer.addRule(RelativeLayout.CENTER_VERTICAL);
        btnPlayer.setBackgroundResource(R.drawable.share);
        relativeLayoutPlayer.addView(btnPlayer, paramsBtnPlayer);


        //Check for there is need to show player or not
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.custom_bottom_bar);
        boolean isShowPlayer = a.getBoolean(R.styleable.custom_bottom_bar_show_player , true);

        if(!isShowPlayer){
            btnPlayer.setVisibility(View.INVISIBLE);
        }
        a.recycle();

        this.setBackgroundResource(R.drawable.topbar);

        this.setPlayerButtonVisibility(btnPlayer);
        this.setVisibilityOfPlusButton(relativeLayoutMoreGreatApp , btnMoreGreatApp);
    }

    private void setPlayerButtonVisibility(Button btnPlayer){
        if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS
                || MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_TUTOR_PLUS){
            if(MathFriendzyHelper.isGivenScreenOpen(".controller.main.MainActivity" , context)) {
                btnPlayer.setVisibility(View.VISIBLE);
            }else{
                btnPlayer.setVisibility(View.INVISIBLE);
            }
            btnPlayer.setBackgroundResource(R.drawable.cap_icon);
            return;
        }

        SharedPreferences sheredPreference = context.getSharedPreferences(LOGIN_SHARED_PREFF, 0);
        if(sheredPreference.getBoolean(IS_LOGIN, false)){
            UserRegistrationOperation userOprObj = new UserRegistrationOperation(context);
            RegistereUserDto regUserObj = userOprObj.getUserData();
            ActivityManager am = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
            String topActivityName =  am.getRunningTasks(1).get(0).topActivity.getClassName();
            if(topActivityName.equals("com.mathfriendzy.controller.main.MainActivity")
                    && !regUserObj.getIsParent().equals("0")){//0 for teacher
                btnPlayer.setVisibility(View.INVISIBLE);
            }
        }
        //end changes
    }

    private void setVisibilityOfPlusButton(RelativeLayout relativeLayoutMoreGreatApp , Button btnPlus){
        if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS){
            relativeLayoutMoreGreatApp.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case idBtnCustomInfo:
                context.startActivity(new Intent(context, InformationActivity.class));
                break;

            case idBtnCustomMoreGreatApps:
                Intent intent1 = new Intent(context, ActMoreApp.class);
                context.startActivity(intent1);
                break;
            case idBtnCustomPlayer:
                //new change for the Homework and MathTutor Plus
                if(MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_PLUS
                        || MathVersions.CURRENT_MATH_VERSION == MathVersions.MATH_TUTOR_PLUS){
                    if(this.showTeacherFunctionPopUp
                            (MathFriendzyHelper.DIALOG_SCHOOL_HELP_STUDENT,
                                    MathFriendzyHelper.getTreanslationTextById(context ,
                                            "lblHelpAStudent") , true))
                        return ;
                    MathFriendzyHelper.clickOnHelpAStudent(context);
                    return;
                }

                SharedPreferences sheredPreference = context.getSharedPreferences(LOGIN_SHARED_PREFF, 0);
                if(sheredPreference.getBoolean(IS_LOGIN, false)){

                    UserRegistrationOperation userOprObj = new UserRegistrationOperation(context);
                    RegistereUserDto regUserObj = userOprObj.getUserData();

                    ActivityManager am = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
                    //Log.e("hjg", "activity " + am.getRunningTasks(1).get(0).topActivity.getClassName());
                    String topActivityName =  am.getRunningTasks(1).get(0).topActivity.getClassName();
                    if(topActivityName.equals("com.mathfriendzy.controller.main.MainActivity")
                            && regUserObj.getIsParent().equals("0")){//0 for teacher
                        this.clickOnForNewAssessmentReport();
                    }else{
                        this.clickOnForNewAssessmentReport();
                    }
                }else{
                    this.clickOnForNewAssessmentReport();
                }
                break;

            case idBtnCustomAccount:
                if(!CommonUtils.isInternetConnectionAvailable(context))
                {
                    DialogGenerator dg = new DialogGenerator(context);
                    Translation transeletion = new Translation(context);
                    transeletion.openConnection();
                    dg.generateWarningDialog(transeletion
                            .getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
                    transeletion.closeConnection();
                }
                else{
                    isClickOnAccount = true;
                    this.chekValidLogin();
                }
                break;
            default:
                break;
        }
    }


    /**
     * This method check for temp player is exist or not
     */
    private void checkForTempPlayer()
    {
        TempPlayerOperation tempObj = new TempPlayerOperation(context);
        if(tempObj.isTemparyPlayerExist())
        {
            SharedPreferences sheredPreference = context.getSharedPreferences(LOGIN_SHARED_PREFF, 0);
            if(sheredPreference.getBoolean(IS_LOGIN, false))
            {
                UserRegistrationOperation userOprObj = new UserRegistrationOperation(context);
                RegistereUserDto regUserObj = userOprObj.getUserData();

                if(regUserObj.getEmail().length() > 1)
                {
                    new GetUserDetailByEmail(regUserObj.getEmail(), regUserObj.getPass()).execute(null,null,null);
                }
                else
                {
                    new GetUserDetailByUserId(regUserObj.getUserId(), regUserObj.getPass()).execute(null,null,null);
                }
            }
            else
            {
                if(tempObj.isTempPlayerDeleted())//if temp player exist then check for temp player is deleted or not from the table
                {
                    Intent intentCreate = new Intent(context,CreateTempPlayerActivity.class);
                    context.startActivity(intentCreate);
                }
                else
                {
                    Intent intent  = new Intent(context,PlayersActivity.class);
                    context.startActivity(intent);
                }
            }
            tempObj.closeConn();
        }
        else
        {
            tempObj.createTempPlayer("");//create temp player if not exists
        }

        //tempObj.closeConn();
    }

    /**
     * This method call when user click on for new assessment report
     * If user is as Teacher then this button available on mainScreen
     */
    private void clickOnForNewAssessmentReport(){
        context.startActivity(new Intent(context , ReportForPracticeAndAssessment.class));
    }

    /**
     * This method check for valid login
     */
    public void chekValidLogin()
    {
        SharedPreferences sheredPreference = context.getSharedPreferences(LOGIN_SHARED_PREFF, 0);
        if(!sheredPreference.getBoolean(IS_LOGIN, false))
        {
            Translation transeletion = new Translation(context);
            transeletion.openConnection();
            DialogGenerator dg = new DialogGenerator(context);
            dg.generateRegisterOrLogInDialog(transeletion
                    .getTranselationTextByTextIdentifier("alertMsgYouMustRegisterOrLogin"));
            transeletion.closeConnection();
        }
        else
        {
            UserRegistrationOperation userOprObj = new UserRegistrationOperation(context);
            RegistereUserDto regUserObj = userOprObj.getUserData();

            if(regUserObj.getEmail().length() > 1)
            {
                new GetUserDetailByEmail(regUserObj.getEmail(), regUserObj.getPass()).execute(null,null,null);
            }
            else
            {
                new GetUserDetailByUserId(regUserObj.getUserId(), regUserObj.getPass()).execute(null,null,null);
            }
            //for logout
		/*Intent intent = new Intent(context,ModifyRegistration.class);
			context.startActivity(intent);*/

			/*SharedPreferences.Editor editor = sheredPreference.edit();
			editor.putBoolean(IS_LOGIN, false);
			editor.commit();
			Log.e("Bottom Bar for logout", "Login Success");*/
        }
    }


    /**
     * This Asyncktask get User Detail from server
     * @author Yashwant Singh
     *
     */
    class GetUserDetailByEmail extends AsyncTask<Void, Void, Void>
    {
        private ProgressDialog pd 	= null;
        private String email 		= null;
        private String pass 		= null;
        private int result          = 0;


        GetUserDetailByEmail(String email, String pass)
        {
            this.email = email;
            this.pass  = pass;
        }

        @Override
        protected void onPreExecute()
        {
            pd = CommonUtils.getProgressDialog(context);
            pd.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            Login login = new Login(context);
            result = login.getUserDetailByEmail(email, pass);

            return null;

        }

        @Override
        protected void onPostExecute(Void resultValue)
        {
            pd.cancel();

            if(result == Login.SUCCESS){

                if(isClickOnAccount)
                {
                    Intent intent = new Intent(context,ModifyRegistration.class);
                    context.startActivity(intent);
                }
                else
                {
                    UserPlayerOperation userPlayer = new UserPlayerOperation(context);
                    if(userPlayer.isUserPlayersExist())
                    {

                        UserRegistrationOperation userObj = new UserRegistrationOperation(context);
                        if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
                        {
                            Intent intent = new Intent(context,CreateTeacherPlayerActivity.class);
                            context.startActivity(intent);
                        }
                        else
                        {
                            Intent intent = new Intent(context,LoginUserCreatePlayer.class);
                            context.startActivity(intent);
                        }
                    }
                    else
                    {
                        UserRegistrationOperation userObj = new UserRegistrationOperation(context);
                        if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
                        {
                            Intent intent = new Intent(context,TeacherPlayer.class);
                            context.startActivity(intent);
                        }
                        else
                        {
                            Intent intent = new Intent(context,LoginUserPlayerActivity.class);
                            context.startActivity(intent);
                        }
                    }
                }
            }else{
                showPopupForInvalidUser();
                logoutUserFromDevice();
            }
            super.onPostExecute(resultValue);
        }
    }


    /**
     * This method logout the user from the device
     */
    private void logoutUserFromDevice(){
        SharedPreferences sheredPreference = context.getSharedPreferences(LOGIN_SHARED_PREFF, 0);
        SharedPreferences.Editor editor = sheredPreference.edit();
        editor.putBoolean(IS_LOGIN, false);
        editor.commit();
    }

    /**
     * This method show popup for invalid user
     */
    private void showPopupForInvalidUser(){
        DialogGenerator dg = new DialogGenerator(context);
        Translation transeletion = new Translation(context);
        transeletion.openConnection();
        dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgEmailUsernameNotInSystem"));
        transeletion.closeConnection();
    }

    /**
     * This Asyncktask get User Detail from server
     * @author Yashwant Singh
     *
     */
    class GetUserDetailByUserId extends AsyncTask<Void, Void, Void>
    {
        private String userId 	= null;
        private String pass     	= null;
        private ProgressDialog pd 	= null;
        private int result          = 0;

        GetUserDetailByUserId(String userId , String pass)
        {
            this.userId = userId;
            this.pass     =  pass;
        }

        @Override
        protected void onPreExecute()
        {
            pd = CommonUtils.getProgressDialog(context);
            pd.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            Login login = new Login(context);
            result = login.getUserDetailByUserId(userId);
            return null;
        }

        @Override
        protected void onPostExecute(Void resultValue)
        {
            pd.cancel();

            if(result == Login.SUCCESS){
                if(isClickOnAccount)
                {
                    Intent intent = new Intent(context,ModifyRegistration.class);
                    context.startActivity(intent);
                }
                else
                {
                    UserPlayerOperation userPlayer = new UserPlayerOperation(context);
                    if(userPlayer.isUserPlayersExist())
                    {

                        UserRegistrationOperation userObj = new UserRegistrationOperation(context);
                        if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
                        {
                            Intent intent = new Intent(context,CreateTeacherPlayerActivity.class);
                            context.startActivity(intent);
                        }
                        else
                        {
                            Intent intent = new Intent(context,LoginUserCreatePlayer.class);
                            context.startActivity(intent);
                        }
                    }
                    else
                    {
                        UserRegistrationOperation userObj = new UserRegistrationOperation(context);
                        if(userObj.getUserData().getIsParent().equals("0"))//0 for teacher
                        {
                            Intent intent = new Intent(context,TeacherPlayer.class);
                            context.startActivity(intent);
                        }
                        else
                        {
                            Intent intent = new Intent(context,LoginUserPlayerActivity.class);
                            context.startActivity(intent);
                        }
                    }
                }
            }else{
                showPopupForInvalidUser();
                logoutUserFromDevice();
            }
            super.onPostExecute(resultValue);
        }
    }

    /**
     * Show the teacher function popup
     * @param popUpFor
     * @return
     */
    private boolean showTeacherFunctionPopUp(final String popUpFor
            , String txtTitle , boolean isShowWatchButton){
        if(MathFriendzyHelper.showTeacherFunctionPopup(context ,
                popUpFor , new TeacherFunctionDialogListener() {

                    @Override
                    public void clickOnWatchVedio(boolean isShow) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void clickOnDontShow(boolean isShow) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void clickOnClose(boolean isShow) {
                        clickOn(popUpFor);
                    }
                } , txtTitle , isShowWatchButton))
            return true;
        return false;
    }

    /**
     * Go after click on close button on Teacher function show vedio
     * @param dialogFor
     */
    private void clickOn(String dialogFor){
        if(MathFriendzyHelper.DIALOG_SCHOOL_HELP_STUDENT.equals(dialogFor)){
            MathFriendzyHelper.clickOnHelpAStudent(context);
        }
    }
}