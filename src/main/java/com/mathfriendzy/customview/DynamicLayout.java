package com.mathfriendzy.customview;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.R;
import com.mathfriendzy.controller.friendzy.StudentChallengeActivity;
import com.mathfriendzy.controller.player.EditRegisteredUserPlayer;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.MathResultTransferObj;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.registration.Register;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.model.result.JsonAsyncTaskForScore;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.LearnignCenterSchoolCurriculumServerOperation;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.MathScoreForSchoolCurriculumTransferObj;
import com.mathfriendzy.model.schoolcurriculum.learnigncenter.SchoolCurriculumLearnignCenterimpl;
import com.mathfriendzy.quickblox.QBCreateSession;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;

import java.util.ArrayList;
import java.util.Date;

import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_ID;

/**
 * This class Creates the dynamic layout
 * @author Yashwant Singh
 *
 */
public class DynamicLayout 
{	
	ArrayList<Button>   btnResultList = null;
	ArrayList<Button>   btnEditList   = null;
	ArrayList<ImageView> imgChekedList= null;

	private ImageView imgCheck = null;
	private boolean isChecked  = false;
	private ImageView imgCheckHoderImage =  null;

	private Context context = null;
	private boolean isTab	= false;

	private SharedPreferences sheredPreference = null;

	private ProgressDialog pd = null;

	//for account type
	/*private final int TEACHER = 1;
	private final int STUDENT = 2;
	private final int PARENT  = 3;*/

	public DynamicLayout(Context context)
	{
		this.context = context;
		isTab = context.getResources().getBoolean(R.bool.isTablet);

		sheredPreference = context.getSharedPreferences(IS_CHECKED_PREFF, 0);

		btnResultList = new ArrayList<Button>();
		btnEditList   = new ArrayList<Button>();
		imgChekedList = new ArrayList<ImageView>();
	}

	/**
	 * Create dynamic layout for displaying user player list
	 */
	public void createDyanamicLayoutForDisplayUserPlayer(final ArrayList<UserPlayerDto> userPlayerList , 
			LinearLayout userPlayer, boolean isBlack) 
	{
		int color = 0;
		if(isBlack)
			color = Color.BLACK;
		else
			color = Color.WHITE;
		/*LinearLayout userPlayer = (LinearLayout) findViewById(R.id.userPlayerLayout);*/

		for(int i = 0 ; i < userPlayerList.size() ; i++)
		{
			RelativeLayout.LayoutParams paramsImgCheck = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			if(isTab)
			{
				paramsImgCheck = new RelativeLayout.LayoutParams(70, 70);
			}
			paramsImgCheck.addRule(RelativeLayout.ALIGN_LEFT);
			paramsImgCheck.addRule(RelativeLayout.CENTER_VERTICAL);

			RelativeLayout layout = new RelativeLayout(context);

			imgCheck = new ImageView(context);
			imgCheck.setId(context.getResources().getInteger(R.integer.id_1));

			if(sheredPreference.getString(PLAYER_ID, "").equals(userPlayerList.get(i).getPlayerid()))
			{				
				imgCheck.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
				imgCheckHoderImage = imgCheck;
			}
			else
			{
				imgCheck.setBackgroundResource(R.drawable.mf_check_box_ipad);
			}

			imgChekedList.add(imgCheck);
			layout.addView(imgCheck,paramsImgCheck);

			imgCheck.setOnClickListener(new OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{	
					/*if(userPlayerList.size() == 1 && sheredPreference.getString(PLAYER_ID, "").equals(""))//if one player exist then no action perform)
					{

					}*/
					/*if(userPlayerList.size() > 1 && sheredPreference.getString(PLAYER_ID, "").equals(""))//if one player exist then no action perform)
					{*/
					SharedPreferences.Editor editor = sheredPreference.edit();

					for( int  i = 0 ; i < imgChekedList.size() ; i ++ )
					{
						if(v == imgChekedList.get(i))
						{						
							if(imgCheckHoderImage != null)
								imgCheckHoderImage.setBackgroundResource(R.drawable.mf_check_box_ipad);

							if(!isChecked)
							{															
								imgChekedList.get(i).setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
								editor.putString(PLAYER_ID,userPlayerList.get(i).getPlayerid());
								//added by shilpi
								editor.putString("userId", userPlayerList.get(i).getParentUserId());
								//end
								imgCheckHoderImage = imgChekedList.get(i);
								isChecked = !isChecked;
							}
							else
							{
								if(imgCheckHoderImage == imgChekedList.get(i))
								{
									//Log.e("", "inside if");
									imgChekedList.get(i).setBackgroundResource(R.drawable.mf_check_box_ipad);
									editor.clear();
									isChecked = !isChecked;
								}
								else
								{
									imgCheckHoderImage.setBackgroundResource(R.drawable.mf_check_box_ipad);
									imgCheckHoderImage = imgChekedList.get(i);
									imgChekedList.get(i).setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
									editor.putString(PLAYER_ID,userPlayerList.get(i).getPlayerid());
									//added by shilpi
									editor.putString("userId", userPlayerList.get(i).getParentUserId());
									//end
								}
							}
							editor.commit();
						}
					}
					//}
				}
			});

			RelativeLayout.LayoutParams paramsPlayerName = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			paramsPlayerName.addRule(RelativeLayout.RIGHT_OF,imgCheck.getId());
			TextView txtPlayerName = new TextView(context);
			txtPlayerName.setId(context.getResources().getInteger(R.integer.id_2));
			txtPlayerName.setText(userPlayerList.get(i).getFirstname() + " " 
					+ userPlayerList.get(i).getLastname().charAt(0)+".");
			txtPlayerName.setTextColor(color);
			if(isTab)
			{
				paramsPlayerName.setMargins(5, 15, 0, 0);
				txtPlayerName.setTextSize(20);
			}
			else
			{
				paramsPlayerName.setMargins(2, 5, 0, 0);
			}

			layout.addView(txtPlayerName,paramsPlayerName);



			RelativeLayout.LayoutParams paramsBtnEdit = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			paramsBtnEdit.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,RelativeLayout.TRUE);

			Button btnEdit = new Button(context);
			btnEdit.setId(context.getResources().getInteger(R.integer.id_4));
			if(isTab)
			{
				btnEdit.setBackgroundResource(R.drawable.pencil_small_ipad);
			}
			else
			{
				btnEdit.setBackgroundResource(R.drawable.edit_player_small);
			}
			btnEditList.add(btnEdit);
			layout.addView(btnEdit,paramsBtnEdit);

			btnEdit.setOnClickListener(new OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					if(!CommonUtils.isInternetConnectionAvailable(context)){
						DialogGenerator dg = new DialogGenerator(context);
						Translation transeletion = new Translation(context);
						transeletion.openConnection();
						dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
						transeletion.closeConnection();
					}
					else{
						for( int  j = 0 ; j < btnEditList.size() ; j ++ )
						{
							if(v == btnEditList.get(j))
							{
								Intent intent = new Intent(context,EditRegisteredUserPlayer.class);
								intent.putExtra("playerId", userPlayerList.get(j).getPlayerid());
								intent.putExtra("userId", userPlayerList.get(j).getParentUserId());
								intent.putExtra("imageName" , userPlayerList.get(j).getImageName());
								intent.putExtra("callingActivity", context.getClass().getSimpleName());
								context.startActivity(intent);
							}
						}
					}
				}
			});


			RelativeLayout.LayoutParams paramsBtnResult = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			paramsBtnResult.addRule(RelativeLayout.LEFT_OF,btnEdit.getId());
			paramsBtnResult.setMargins(0, 0, 10, 0);

			Button btnResult = new Button(context);
			btnResult.setId(context.getResources().getInteger(R.integer.id_3));

			if(isTab)
			{
				btnResult.setBackgroundResource(R.drawable.result_small_ipad);
			}
			else
			{
				btnResult.setBackgroundResource(R.drawable.result_player_small);
			}

			btnResultList.add(btnResult);
			layout.addView(btnResult,paramsBtnResult);

			btnResult.setOnClickListener(new OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					if(!CommonUtils.isInternetConnectionAvailable(context)){
						DialogGenerator dg = new DialogGenerator(context);
						Translation transeletion = new Translation(context);
						transeletion.openConnection();
						dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
						transeletion.closeConnection();
					}
					else{
						for( int  j = 0 ; j < btnResultList.size() ; j ++ )
						{
							if(v == btnResultList.get(j))
							{
								int playerid = Integer.parseInt(userPlayerList.get(j).getPlayerid());
								int userid  = Integer.parseInt(userPlayerList.get(j).getParentUserId());
								String playerName  = userPlayerList.get(j).getFirstname()+" "
										+userPlayerList.get(j).getLastname();
								String imageName = userPlayerList.get(j).getImageName();

								/*Date date  = new Date();

							       String zero1 = "";
							       String zero2 = "";
							       if(date.getMonth() < 9)
							        zero1 = "0";
							       if(date.getDate() <= 9)
							        zero2 = "0";
							       String playDate  = (date.getYear()+1900) + "-"+zero1 + (date.getMonth() + 1)
							       + "-"+ zero2 +date.getDate();
							       new JsonAsyncTaskForScore(context, playDate, userid, playerid, playerName, false)
							       .execute(null,null,null);*/
								clickOnResult(userid, playerid, playerName , imageName);
							}
						}
					}	
				}
			});
			userPlayer.addView(layout);
		}
	}

	/**
	 * Create dynamic layout for displaying user player list
	 */
	@SuppressLint("InflateParams")
	public void createDyanamicLayoutForDisplayUserPlayer(final ArrayList<UserPlayerDto>
	userPlayerList , LinearLayout userPlayer, boolean isBlack , int youare){

		final ArrayList<SelectPlayerViewHolde> viewHolderList = new ArrayList<SelectPlayerViewHolde>();
		for(int i = 0 ; i < userPlayerList.size() ; i ++ ){
			View view = LayoutInflater.from(context).inflate(R.layout.select_player_layout, null);
			SelectPlayerViewHolde vHolder = new SelectPlayerViewHolde();
			vHolder.studentLayout = (RelativeLayout) view.findViewById(R.id.studentLayout);
			vHolder.txtStudentName = (TextView) view.findViewById(R.id.txtStudentName);
			vHolder.imgEdit = (ImageView) view.findViewById(R.id.imgEdit);
			vHolder.imgResult = (ImageView) view.findViewById(R.id.imgResult);

			if(youare == 1){//1 for teacher
				vHolder.imgEdit.setVisibility(ImageView.GONE);
			}

			if(sheredPreference.getString(PLAYER_ID, "")
					.equals(userPlayerList.get(i).getPlayerid())){	
				vHolder.studentLayout.setBackgroundResource(R.drawable.blue_button_new_registration);
				vHolder.txtStudentName.setTextColor(context.getResources().getColor(R.color.WHITE));
			}else{
				vHolder.studentLayout.setBackgroundResource(R.drawable.white_button_new_registration);
				vHolder.txtStudentName.setTextColor(context.getResources().getColor(R.color.BLACK));
			}
			vHolder.txtStudentName.setText(userPlayerList.get(i).getFirstname() + " " 
					+ userPlayerList.get(i).getLastname().charAt(0)+".");

			vHolder.studentLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

                    QBCreateSession.getInstance(context).logoutFromChat();

					for(int i = 0 ; i < viewHolderList.size() ; i ++){
                        if(v == viewHolderList.get(i).studentLayout){
							viewHolderList.get(i).studentLayout.setBackgroundResource
							(R.drawable.blue_button_new_registration);
							viewHolderList.get(i).txtStudentName.setTextColor
							(context.getResources().getColor(R.color.WHITE));

							//set selected student
							SharedPreferences.Editor editor = sheredPreference.edit();
							editor.putString(PLAYER_ID,userPlayerList.get(i).getPlayerid());
							editor.putString("userId", userPlayerList.get(i).getParentUserId());
							editor.commit();

                            //disconnect when player change
                            MathFriendzyHelper.disconnectFromGlobalChatRoom();
						}else{
							viewHolderList.get(i).studentLayout.setBackgroundResource
							(R.drawable.white_button_new_registration);
							viewHolderList.get(i).txtStudentName.setTextColor
							(context.getResources().getColor(R.color.BLACK));
						}
					}
				}
			});

			vHolder.imgEdit.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					
					if(!CommonUtils.isInternetConnectionAvailable(context)){
						DialogGenerator dg = new DialogGenerator(context);
						Translation transeletion = new Translation(context);
						transeletion.openConnection();
						dg.generateWarningDialog(transeletion
								.getTranselationTextByTextIdentifier
								("alertMsgYouAreNotConnectedToTheInternet"));
						transeletion.closeConnection();
					}
					else{
						for( int  i = 0 ; i < viewHolderList.size() ; i ++ ){
							if(v == viewHolderList.get(i).imgEdit)	{
								Intent intent = new Intent(context,EditRegisteredUserPlayer.class);
								intent.putExtra("playerId", userPlayerList.get(i).getPlayerid());
								intent.putExtra("userId", userPlayerList.get(i).getParentUserId());
								intent.putExtra("imageName" , userPlayerList.get(i).getImageName());
								intent.putExtra("callingActivity", context.getClass()
										.getSimpleName());
								context.startActivity(intent);
							}
						}
					}
				}
			});

			vHolder.imgResult.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if(!CommonUtils.isInternetConnectionAvailable(context)){
						DialogGenerator dg = new DialogGenerator(context);
						Translation transeletion = new Translation(context);
						transeletion.openConnection();
						dg.generateWarningDialog
						(transeletion.getTranselationTextByTextIdentifier
								("alertMsgYouAreNotConnectedToTheInternet"));
						transeletion.closeConnection();
					}else{
						for(int i = 0 ; i < viewHolderList.size() ; i ++){
							if(v == viewHolderList.get(i).imgResult){
								int playerid = Integer.parseInt(userPlayerList.get(i)
										.getPlayerid());
								int userid  = Integer.parseInt(userPlayerList.get(i)
										.getParentUserId());
								String playerName  = userPlayerList.get(i).getFirstname()+" "
										+userPlayerList.get(i).getLastname();
								String imageName = userPlayerList.get(i).getImageName();
								clickOnResult(userid, playerid, playerName , imageName);
							}
						}		
					}
				}
			});

			userPlayer.addView(view);
			viewHolderList.add(vHolder);
		}
	}

	/**
	 * This method call when user click on result button
	 * @param
	 * @param
	 * @param playerName
	 */
	private void clickOnResult(int userid , int playerid , String playerName , String imageName){

		LearningCenterimpl learnignCenterImpl = new LearningCenterimpl(context);
		learnignCenterImpl.openConn();
		ArrayList<MathResultTransferObj> mathResultTransferObjList = learnignCenterImpl
				.getMathResultData(userid + "", playerid + "");
		learnignCenterImpl.deleteFromMathResult(userid + "", playerid + "");
		learnignCenterImpl.closeConn();

		SchoolCurriculumLearnignCenterimpl schoolImpl = new SchoolCurriculumLearnignCenterimpl(context);
		schoolImpl.openConnection();
		ArrayList<MathScoreForSchoolCurriculumTransferObj> mathResultList = schoolImpl
				.getWordProblemResultData(userid + "", playerid + "");
		schoolImpl.deleteFromMathResult(userid + "", playerid + "");
		schoolImpl.closeConnection();

		PlayerTotalPointsObj playerObj = null;
		LearningCenterimpl learningCenterimpl = new LearningCenterimpl(context);
		learningCenterimpl.openConn();
		playerObj = learningCenterimpl.getDataFromPlayerTotalPoints(playerid + "");
		learningCenterimpl.closeConn();

		int playerGrade = 0;
		UserPlayerOperation userPlayerOpr = new UserPlayerOperation(context);
		UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(playerid + "");
		playerGrade = Integer.parseInt(userPlayerData.getGrade());

		/*Log.e("", "learnign center math result " + mathResultTransferObjList.size());
		Log.e("", "word problem math reslut "    + mathResultList.size());*/

		Date date  = new Date();
		String zero1 = "";
		String zero2 = "";
		if(date.getMonth() < 9)
			zero1 = "0";
		if(date.getDate() <= 9)
			zero2 = "0";
		String playDate  = (date.getYear()+1900) + "-"+zero1 + (date.getMonth() + 1)
				+ "-"+ zero2 + date.getDate();

		if(mathResultTransferObjList.size() > 0 && mathResultList.size() > 0){
			new AddMathScroreOnServer(mathResultTransferObjList,mathResultList,//2 for both api call
					context, playDate, userid, playerid, playerName, false , 2,
					playerObj.getTotalPoints() , playerGrade , imageName).execute(null,null,null);
		}
		else if(mathResultTransferObjList.size() > 0){
			new AddMathScroreOnServer(mathResultTransferObjList,mathResultList,//1 for only addMthScore api call
					context, playDate, userid, playerid, playerName, false , 1
					,playerObj.getTotalPoints() , playerGrade , imageName).execute(null,null,null);
		}
		else if(mathResultList.size() > 0){
			new AddWordProblemScoreonServer(mathResultTransferObjList,mathResultList,//0 for only addMthScore for word problem api call
					context, playDate, userid, playerid, playerName, false , 0
					,playerObj.getTotalPoints() , playerGrade , imageName).execute(null,null,null);
		}
		else{
			new JsonAsyncTaskForScore(context, playDate, userid, playerid, playerName, false
					,playerObj.getTotalPoints() , playerGrade , imageName)
			.execute(null,null,null);
		}
	}

	/**
	 * Create dynamic layout for see answer activity
	 * @param playEquationList
	 * @param userAnswerList
	 * @param layout
	 */
	public void createDynamicLayoutForSeeAnswer(ArrayList<LearningCenterTransferObj> playEquationList , ArrayList<String> userAnswerList , RelativeLayout layout)
	{

		for(int i = 0 ; i < userAnswerList.size() ; i ++ )
		{
			RelativeLayout innerLayout = new RelativeLayout(context);

			RelativeLayout.LayoutParams txtSrParam = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			txtSrParam.addRule(RelativeLayout.ALIGN_PARENT_LEFT , RelativeLayout.TRUE);
			TextView txtSrNo = new TextView(context);
			txtSrNo.setText(i + 1 + ".");
			txtSrNo.setId(1000 + i);
			txtSrNo.setLayoutParams(txtSrParam);
			innerLayout.addView(txtSrNo);

			RelativeLayout.LayoutParams txtProblemLayout = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);

			txtProblemLayout.addRule(RelativeLayout.RIGHT_OF,txtSrNo.getId());
			TextView txtProblem = new TextView(context);
			txtProblem.setText(playEquationList.get(i).getNumber1Str() + " " 
					+ playEquationList.get(i).getOperator() + " "
					+ playEquationList.get(i).getNumber2Str() + " = " 
					+ playEquationList.get(i).getProductStr());
			txtProblem.setId(2000 + i);
			txtProblem.setLayoutParams(txtProblemLayout);
			innerLayout.addView(txtProblem);

			RelativeLayout.LayoutParams txtUserAnswerLayout = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			txtUserAnswerLayout.addRule(RelativeLayout.ALIGN_PARENT_RIGHT , RelativeLayout.TRUE);

			TextView txtUserAnswer = new TextView(context);
			txtUserAnswer.setText(userAnswerList.get(i));
			txtUserAnswer.setLayoutParams(txtUserAnswerLayout);
			innerLayout.addView(txtUserAnswer);

			layout.addView(innerLayout);
		}
	}

	/**
	 * Save Temp Player result on server
	 * @author Yashwant Singh
	 *
	 */
	class AddMathScroreOnServer extends AsyncTask<Void, Void, Void>
	{
		private ArrayList<MathResultTransferObj> mathResultTransferObjList  = null;
		private ArrayList<MathScoreForSchoolCurriculumTransferObj> mathResultList = null;
		private Context context;
		private String playDate;
		private int userid;
		private int playerid;
		private String playerName;
		boolean b;
		int     i;//for both api call or not

		private int playerpoints = 0 ;
		private int playerGrade = 0;

		//addedd for assessment 
		String imageName;
		AddMathScroreOnServer(ArrayList<MathResultTransferObj> mathResultTransferObjList, 
				ArrayList<MathScoreForSchoolCurriculumTransferObj> mathResultList, 
				Context context, String playDate, int userid, int playerid, String playerName, 
				boolean b, int i, int points, int playerGrade , String imageName)
				{			
			this.mathResultList = mathResultList;
			this.mathResultTransferObjList = mathResultTransferObjList;
			this.context        = context;
			this.playDate       = playDate;
			this.userid         = userid;
			this.playerid       = playerid;
			this.playerName     = playerName;
			this.b              = b;
			this.i              = i;

			this.playerpoints = points;
			this.playerGrade  = playerGrade;

			//for assessment test
			this.imageName = imageName;
				}

		@Override
		protected void onPreExecute() 
		{		
			pd = CommonUtils.getProgressDialog(context);
			pd.show();

			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			Register register = new Register(context);
			for(int i = 0 ; i < mathResultTransferObjList.size() ; i ++ ){
				MathResultTransferObj mathobj = mathResultTransferObjList.get(i);
				mathobj.setGameType("Play");
				register.savePlayerScoreOnServerForloginuser(mathobj);
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			pd.cancel();

			if(i == 2){
				new AddWordProblemScoreonServer(mathResultTransferObjList,mathResultList,//0 for only addMthScore for word problem api call
						context, playDate, userid, playerid, playerName, false , 0
						,playerpoints , playerGrade , imageName).execute(null,null,null);
			}else{
				new JsonAsyncTaskForScore(context, playDate, userid, playerid, playerName, false
						, playerpoints , playerGrade , imageName)
				.execute(null,null,null);	
			}
			super.onPostExecute(result);
		}
	}

	/**
	 * This class add word problem on server
	 * @author Yashwant Singh
	 *
	 */
	class AddWordProblemScoreonServer extends AsyncTask<Void, Void, Void>{

		private ArrayList<MathResultTransferObj> mathResultTransferObjList = null;
		private ArrayList<MathScoreForSchoolCurriculumTransferObj> mathResultList = null;
		private Context context;
		private String playDate;
		private int userid;
		private int playerid;
		private String playerName;
		boolean b;
		int     i;//for both api call or not
		private int playerPoints = 0;
		private int playerGrade  = 0;

		//for assessment test
		private String imageName;
		AddWordProblemScoreonServer(ArrayList<MathResultTransferObj> mathResultTransferObjList, 
				ArrayList<MathScoreForSchoolCurriculumTransferObj> mathResultList, Context context, 
				String playDate, int userid, int playerid, String playerName, boolean b, int i, 
				int playerpoints, int playerGrade , String imageName){

			this.mathResultList = mathResultList;
			this.mathResultTransferObjList = mathResultTransferObjList;
			this.context        = context;
			this.playDate       = playDate;
			this.userid         = userid;
			this.playerid       = playerid;
			this.playerName     = playerName;
			this.b              = b;
			this.i              = i;

			this.playerPoints = playerpoints;
			this.playerGrade  = playerGrade;
			//for assessment
			this.imageName = imageName;
		}

		@Override
		protected void onPreExecute() {
			pd = CommonUtils.getProgressDialog(context);
			pd.show();

			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			LearnignCenterSchoolCurriculumServerOperation serverObj = 
					new LearnignCenterSchoolCurriculumServerOperation();

			for( int i = 0 ; i < mathResultList.size() ; i ++ ){
				serverObj.addWordProblemScore(mathResultList.get(i), 0);
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			pd.cancel();

			new JsonAsyncTaskForScore(context, playDate, userid, playerid, playerName, false
					,playerPoints , playerGrade , imageName)
			.execute(null,null,null);	
		}
	}

	//added by shilpi for friendzy challenge
	/**
	 * Create dynamic layout for displaying user player list for friendzy notification
	 */
	public void createDynamicLayoutForFriendzy(final ArrayList<UserPlayerDto> userPlayerList , LinearLayout userPlayer,
			String strViewChallenge) {

		int color = Color.WHITE;

		for(int i = 0 ; i < userPlayerList.size() ; i++)
		{   
			RelativeLayout layout = new RelativeLayout(context);   

			RelativeLayout.LayoutParams paramsPlayerName = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			TextView txtPlayerName = new TextView(context);
			txtPlayerName.setId(context.getResources().getInteger(R.integer.id_6));
			txtPlayerName.setText(userPlayerList.get(i).getFirstname() + " " 
					+ userPlayerList.get(i).getLastname().charAt(0)+".");
			txtPlayerName.setTextColor(color);
			if(isTab)
			{
				paramsPlayerName.setMargins(5, 20, 0, 0);
				txtPlayerName.setTextSize(20);
			}
			else
			{
				paramsPlayerName.setMargins(2, 15, 0, 0);
			}

			layout.addView(txtPlayerName,paramsPlayerName);

			RelativeLayout.LayoutParams paramsbtnView = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			paramsbtnView.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,RelativeLayout.TRUE);
			paramsbtnView.setMargins(0, 10, 10, 10);
			Button btnView = new Button(context);
			btnView.setId(context.getResources().getInteger(R.integer.id_7));

			btnView.setTextColor(color);
			btnView.setText(strViewChallenge);
			if(isTab)
			{
				btnView.setTextSize(20);
				btnView.setBackgroundResource(R.drawable.gjs_play_again);
			}
			else
			{
				btnView.setBackgroundResource(R.drawable.green_1);
			}
			btnEditList.add(btnView);
			layout.addView(btnView,paramsbtnView);

			btnView.setOnClickListener(new OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					if(!CommonUtils.isInternetConnectionAvailable(context))
					{
						CommonUtils.showInternetDialog(context);   
					}
					else
					{
						for( int  j = 0 ; j < btnEditList.size() ; j ++ )
						{
							if(v == btnEditList.get(j))
							{
								SharedPreferences sharedPreffPlayerInfo  = context.getSharedPreferences(IS_CHECKED_PREFF, 0);
								SharedPreferences.Editor editor = sharedPreffPlayerInfo.edit();
								editor.putString("challengerId", "-1");
								editor.putString("playerId", userPlayerList.get(j).getPlayerid());
								editor.putString("userId", userPlayerList.get(j).getParentUserId());
								editor.commit();
								Intent intent = new Intent(context,StudentChallengeActivity.class);        
								context.startActivity(intent);
							}
						}
					}
				}
			});

			userPlayer.addView(layout);
		}
	}


	private class SelectPlayerViewHolde{
		private RelativeLayout studentLayout;
		private TextView txtStudentName;
		private ImageView imgEdit;
		private ImageView imgResult;
	}
}
