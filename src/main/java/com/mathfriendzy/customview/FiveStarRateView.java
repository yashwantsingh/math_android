package com.mathfriendzy.customview;

import android.app.Activity;
import android.content.Context;
import android.widget.ImageView;

import com.mathfriendzy.R;

/**
 * Created by root on 11/1/16.
 */
public class FiveStarRateView {

    private ImageView imgStar1 = null;
    private ImageView imgStar2 = null;
    private ImageView imgStar3 = null;
    private ImageView imgStar4 = null;
    private ImageView imgStar5 = null;

    private Activity activity = null;

    public FiveStarRateView(Context context){
        activity = (Activity) context;
    }

    public void setWidgetsReferences(){
        imgStar1 = (ImageView) activity.findViewById(R.id.imgStar1);
        imgStar2 = (ImageView) activity.findViewById(R.id.imgStar2);
        imgStar3 = (ImageView) activity.findViewById(R.id.imgStar3);
        imgStar4 = (ImageView) activity.findViewById(R.id.imgStar4);
        imgStar5 = (ImageView) activity.findViewById(R.id.imgStar5);
    }

    public void setRatingStart(int rating){
        switch (rating){
            case 0:
                imgStar1.setBackgroundResource(R.drawable.start_unselected);
                imgStar2.setBackgroundResource(R.drawable.start_unselected);
                imgStar3.setBackgroundResource(R.drawable.start_unselected);
                imgStar4.setBackgroundResource(R.drawable.start_unselected);
                imgStar5.setBackgroundResource(R.drawable.start_unselected);
                break;
            case 1:
                imgStar1.setBackgroundResource(R.drawable.star_selected);
                imgStar2.setBackgroundResource(R.drawable.start_unselected);
                imgStar3.setBackgroundResource(R.drawable.start_unselected);
                imgStar4.setBackgroundResource(R.drawable.start_unselected);
                imgStar5.setBackgroundResource(R.drawable.start_unselected);
                break;
            case 2:
                imgStar1.setBackgroundResource(R.drawable.star_selected);
                imgStar2.setBackgroundResource(R.drawable.star_selected);
                imgStar3.setBackgroundResource(R.drawable.start_unselected);
                imgStar4.setBackgroundResource(R.drawable.start_unselected);
                imgStar5.setBackgroundResource(R.drawable.start_unselected);
                break;
            case 3:
                imgStar1.setBackgroundResource(R.drawable.star_selected);
                imgStar2.setBackgroundResource(R.drawable.star_selected);
                imgStar3.setBackgroundResource(R.drawable.star_selected);
                imgStar4.setBackgroundResource(R.drawable.start_unselected);
                imgStar5.setBackgroundResource(R.drawable.start_unselected);
                break;
            case 4:
                imgStar1.setBackgroundResource(R.drawable.star_selected);
                imgStar2.setBackgroundResource(R.drawable.star_selected);
                imgStar3.setBackgroundResource(R.drawable.star_selected);
                imgStar4.setBackgroundResource(R.drawable.star_selected);
                imgStar5.setBackgroundResource(R.drawable.start_unselected);
                break;
            case 5:
                imgStar1.setBackgroundResource(R.drawable.star_selected);
                imgStar2.setBackgroundResource(R.drawable.star_selected);
                imgStar3.setBackgroundResource(R.drawable.star_selected);
                imgStar4.setBackgroundResource(R.drawable.star_selected);
                imgStar5.setBackgroundResource(R.drawable.star_selected);
                break;
        }
    }

}
