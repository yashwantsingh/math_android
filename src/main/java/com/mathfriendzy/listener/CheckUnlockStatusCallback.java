package com.mathfriendzy.listener;

import com.mathfriendzy.model.learningcenter.CoinsFromServerObj;

public interface CheckUnlockStatusCallback {
	void onRequestComplete(CoinsFromServerObj coinsFromServer);
}
