package com.mathfriendzy.listener;

/**
 * Created by root on 25/1/16.
 */
public interface OnPurchaseDone {
    void onPurchaseDone(boolean isDone);
}
