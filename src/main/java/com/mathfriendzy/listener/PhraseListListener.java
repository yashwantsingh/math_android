package com.mathfriendzy.listener;

import com.mathfriendzy.model.tutor.PhraseCatagory;

import java.util.ArrayList;

/**
 * Created by root on 4/11/15.
 */
public interface PhraseListListener {
    void onPhraseList(ArrayList<PhraseCatagory> phraseList);
}
