package com.mathfriendzy.listener;

public interface TeacherFunctionDialogListener {
	void clickOnClose(boolean isShow);
	void clickOnWatchVedio(boolean isShow);
	void clickOnDontShow(boolean isShow);
}
