package com.mathfriendzy.listener;

public interface OnScrollChange {
	void onScrolling(int state);
}
