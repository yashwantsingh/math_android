package com.mathfriendzy.listener;

import com.mathfriendzy.model.homework.CustomeResult;
import com.mathfriendzy.model.homework.GetHomeWorkForStudentWithCustomeResponse;
import com.mathfriendzy.model.homework.PracticeResultSubCat;
import com.mathfriendzy.model.homework.WordSubCatResult;

/**
 * Created by root on 22/6/16.
 */
public interface HomeworkClickListener {
    void onCustomClick(CustomeResult customeResult ,
                       GetHomeWorkForStudentWithCustomeResponse homeworkData , int index);
    void onPracticeClick(PracticeResultSubCat practiceResultSubCat ,
                         GetHomeWorkForStudentWithCustomeResponse homeworkData , int index);
    void onWordClick(WordSubCatResult subCatResult ,
                     GetHomeWorkForStudentWithCustomeResponse homeworkData , int index);
    void onHomeworkClick(int index);
}
