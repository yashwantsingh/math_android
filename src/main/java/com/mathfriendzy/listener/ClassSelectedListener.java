package com.mathfriendzy.listener;

import com.mathfriendzy.model.registration.classes.ClassWithName;

import java.util.ArrayList;

/**
 * Created by root on 25/5/16.
 */
public interface ClassSelectedListener {
    void onClassSelectionDone(ArrayList<ClassWithName> selectedClass);
    void onUpdatedClasses(ArrayList<ClassWithName> updatedList);
}
