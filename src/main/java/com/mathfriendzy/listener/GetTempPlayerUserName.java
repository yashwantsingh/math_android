package com.mathfriendzy.listener;

/**
 * Created by root on 7/10/15.
 */
public interface GetTempPlayerUserName {
    public void onDone(String userName);
}
