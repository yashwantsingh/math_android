package com.mathfriendzy.imageoperation;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import com.mathfriendzy.helper.HttpServerRequest;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.serveroperation.ServerDialogs;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageOperation {

	private static Bitmap bitmapImage = null;
	private static File newCropImageFile = null;
	
	/**
	 * Return the bitmap from the given url
	 * @param context
	 * @param filePath
	 * @return
	 */
	public static Bitmap getBitmapByFilePath(Context context , String filePath){
		return BitmapFactory.decodeFile(filePath);
	}

	/**
	 * Rotate the image based on given degree
	 * @param bitmap
	 * @param degree
	 * @return
	 */
	public static Bitmap rotateImage(Bitmap bitmap, float degree){
		//Log.e("ImageProcessing", "rotation angle is " + degree);
		Matrix matrix = new Matrix();
		matrix.postRotate(degree);
		return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
				bitmap.getHeight(), matrix, true);
	}

	/**
	 * Image oreintationvalidator
	 * @param bitmap
	 * @param path
	 * @return
	 */
	public static Bitmap imageOreintationValidator(Bitmap bitmap, String path) {
		ExifInterface ei	= null;
		try {
			ei = new ExifInterface(path);
			int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);

			switch (orientation) {
			case ExifInterface.ORIENTATION_ROTATE_90:
				bitmap = rotateImage(bitmap, 90);
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				bitmap = rotateImage(bitmap, 180);
				break;
			case ExifInterface.ORIENTATION_ROTATE_270:
				bitmap = rotateImage(bitmap, 270);
				break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bitmap;
	}

	/**
	 * Saved Edited Image into SD Card
	 * @param context
	 * @param fileName
	 * @param bitmap
	 */
	public static void saveImageInSdCard(Context context, String fileName, Bitmap bitmap
			, final HttpServerRequest request){

		try {
			FileOutputStream out = new FileOutputStream(fileName);
			bitmap.compress(Bitmap.CompressFormat.JPEG, 20, out);
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		scanMedia(context, fileName, request);
	}

	/**
	 * Scan the Media
	 * @param context
	 * @param fileName
	 * @param request
	 */
	public static void scanMedia(Context context , String fileName ,
			final HttpServerRequest request){
		MediaScannerConnection.scanFile(context, 
				new String[]{fileName}, 
				null, new MediaScannerConnection.OnScanCompletedListener() {

			@Override
			public void onScanCompleted(String path, Uri uri) {
				request.onRequestComplete();
			}
		});
	}

	/**
	 * Scan Media asyncktask
	 * @param context
	 * @param fileName
	 * @param request
	 */
	public static void scanMediaAsynckTask(final Context context , final String fileName ,	
			final HttpServerRequest request){
		new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				ImageOperation.scanMedia(context, fileName, 
						new HttpServerRequest() {

					@Override
					public void onRequestComplete() {

					}
				});
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				request.onRequestComplete();
				super.onPostExecute(result);
			}
		}.execute();
	}

	/**Crop the image
	 * @return returns true if crop supports by the device,otherwise false*/
	public static boolean performCropImage(final String imageUri , final Context context , 
			final int action){
		try {
			if(imageUri != null){

				new GetBitmap(new HttpResponseInterface() {
					@Override
					public void serverResponse(HttpResponseBase httpResponseBase,
							int requestCode) {
						try{

							if(bitmapImage != null){
								/*int height = bitmapImage.getHeight();
								int width  = bitmapImage.getWidth();*/
								bitmapImage = null;

								//call the standard crop action intent (the user device may not support it)
								Intent cropIntent = new Intent("com.android.camera.action.CROP");
								//indicate image type and Uri
								cropIntent.setDataAndType(Uri.parse(MathFriendzyHelper
										.IMAGE_FILE_PREFF + imageUri), "image/*");
								//set crop properties
								cropIntent.putExtra("crop", "true");
								//indicate aspect of desired crop

								/*if(height > width){
									cropIntent.putExtra("aspectX", 4);//for width 
									cropIntent.putExtra("aspectY", 6);//for height
									cropIntent.putExtra("outputX", 500);
									cropIntent.putExtra("outputY", 700);
								}else{
									cropIntent.putExtra("aspectX", 6);//for width 
									cropIntent.putExtra("aspectY", 4);//for height
									cropIntent.putExtra("outputX", 700);
									cropIntent.putExtra("outputY", 500);
								}*/

								cropIntent.putExtra("scale", false);
								//If not provided you will get black frame around your cropped image
								cropIntent.putExtra("scaleUpIfNeeded", false);
								//retrieve data on return
								cropIntent.putExtra("return-data", false);
								cropIntent.putExtra("outputFormat", 
										Bitmap.CompressFormat.PNG.toString());

								//File f = new File(imageUri);
								File f = createNewFile(null);
								//assign new path file
								newCropImageFile = f;
								
								try {
									f.createNewFile();
								} catch (IOException ex) {
									Log.e("io", ex.getMessage());  
								}
								cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
								((Activity)context).startActivityForResult(cropIntent, action);
							}
						}catch(ActivityNotFoundException anfe){
							//display an error message
							String errorMessage = "Whoops - your device doesn't support the crop action!";
							Toast toast = Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT);
							toast.show();
						}
					}
				} , imageUri , context).execute();
				return true;
			}
		}
		catch(ActivityNotFoundException anfe){
			//display an error message
			String errorMessage = "Whoops - your device doesn't support the crop action!";
			Toast toast = Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT);
			toast.show();
			return false;
		}
		return false;
	}

	/**
	 * Create new file
	 * @param prefix
	 * @return
	 */
	private static File createNewFile(String prefix){
		if(prefix == null || "".equalsIgnoreCase(prefix)){
			prefix = "IMG_";
		}
		File newDirectory = new File(MathFriendzyHelper.getMathFilePath() + "/Crop");
		if(!newDirectory.exists()){
			if(newDirectory.mkdir()){
				Log.d("Image Operation", newDirectory.getAbsolutePath() + " directory created");
			}
		}
		File file = new File(newDirectory,(prefix + System.currentTimeMillis() + ".png"));
		if(file.exists()){
			file.delete();
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return file;
	}

	/**
	 * Retuen the new File , Which is cropped.
	 * @return
	 */
	public static File getNewCropFile(){
		return newCropImageFile;
	}
	
	/**
	 * Set the new file to null , After used.
	 */
	public static void setNewCropFileToNull(){
		newCropImageFile = null;
	}
	
	/**
	 * This Asynck task get the image bitmap
	 * @author Yashwant Singh
	 *
	 */
	private static class GetBitmap extends AsyncTask<Void, Void, Void>{

		private HttpResponseInterface httpResponseInterface;
		private String imageUrl;
		private ProgressDialog pd;
		private Context context;

		public GetBitmap(HttpResponseInterface httpResponseInterface
				, String imageUrl , Context context) {
			this.imageUrl = imageUrl;
			this.httpResponseInterface = httpResponseInterface;
			this.context = context;  
		}

		@Override
		protected void onPreExecute() {

			pd = ServerDialogs.getProgressDialog(context, "Please wait...", 1);
			pd.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			bitmapImage = getBitmapByFilePath(context, imageUrl);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			pd.cancel();
			httpResponseInterface.serverResponse(null, 0);
			super.onPostExecute(result);
		}
	}
}
