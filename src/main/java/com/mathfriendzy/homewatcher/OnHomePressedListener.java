package com.mathfriendzy.homewatcher;

/**
 * Created by root on 18/5/16.
 */
public interface OnHomePressedListener {
    public void onHomePressed();
    public void onHomeLongPressed();
}
