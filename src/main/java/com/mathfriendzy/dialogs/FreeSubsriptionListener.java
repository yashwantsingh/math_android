package com.mathfriendzy.dialogs;

public interface FreeSubsriptionListener {
	void clickOnViewIntroduction();
	void clickOnViewStudentFeature();
	void clickOnViewTeacherFeature();
}
