/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.mathfriendzy;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.homework_friendzy";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "mathfriendzyplus";
  public static final int VERSION_CODE = 21;
  public static final String VERSION_NAME = "1.20";
}
