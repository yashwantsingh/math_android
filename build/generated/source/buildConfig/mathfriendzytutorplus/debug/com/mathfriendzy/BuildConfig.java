/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.mathfriendzy;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.math_tutor_plus";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "mathfriendzytutorplus";
  public static final int VERSION_CODE = 16;
  public static final String VERSION_NAME = "1.15";
}
